package com.sss.aut.service;

import java.util.Map;



public interface User 
{
  public String getUserName();
  
  public long getUserId();
  
  public UserData getUserData();
  
  public UserPermission getPermission(String screenId);
  
  public Map<String ,UserPermission> getPermission();
  
  public boolean isPermission();
  
}
