package com.sss.aut.service;

public interface UserAction {
  
  public String getName();
  
  public String getUri();
  
  public String getScreenId();
  
  public String getService();
  
  public String getAction();
}
