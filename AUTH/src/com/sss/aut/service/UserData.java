package com.sss.aut.service;

import java.util.Map;


public interface UserData 
{
  public long getUserId();

  public String getUserName();

  public String getFirstName();

  public String getLastName();

  public String getPrefixName();

  public long getGroupId();
  
  public String getUserFullName();
  
  public String getPositionName();
  
  public long getOrgId();
  
  public Map getRoleName();


}
