package com.sss.aut.service;



public interface UserPermission 
{
  public boolean isAuthorized(String action) throws Exception;
  
  public boolean isAdd();
  
  public boolean isView();
  
  public boolean isDelete();
  
  public boolean isEdit();
  
  public boolean isSearch();
  
  public boolean isPrint();
  
  public boolean isScan();
  
  public boolean isExtra1();
  
  public boolean isExtra2();
  
  public String getScreenId();
}
