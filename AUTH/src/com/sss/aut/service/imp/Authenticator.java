package com.sss.aut.service.imp; 
//
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Authenticator
{
  public Authenticator()
  {

  }

  public String isAuthorizable(String user, String password)
  {
	  String fromParam = "";
	    try
	    {
	      MessageDigest digest = MessageDigest.getInstance("SHA-256");
	      digest.update(password.getBytes("UTF-8"));
	      digest.update(user.getBytes("UTF-8"));
	      byte[] barray = digest.digest();
	      digest.update(password.getBytes("UTF-8"));
	      digest.update(barray);
	      barray = digest.digest();
	      StringBuffer sbuf = new StringBuffer(40);
	      for (int i = 0; i < barray.length; ++i)
	      {
	        String hex = Integer.toHexString((barray[i] + 256) & 0xFF).toUpperCase();
	        sbuf.append(hex.length() == 1 ? "0" + hex : hex);
	      }
	      fromParam = sbuf.toString();
	      System.out.println(sbuf.toString());
	    }
	    catch (UnsupportedEncodingException e)
	    {
	      throw new RuntimeException(e);
	    }
	    catch (NoSuchAlgorithmException e1)
	    {
	      throw new RuntimeException(e1);
	    }
	    
	    return fromParam;

  }

  public String getPasswordEncoding(String user, String password)
  {

    String fromParam = "";
    try
    {
      MessageDigest digest = MessageDigest.getInstance("SHA-1");
      digest.update(password.getBytes("UTF-8"));
      digest.update(user.getBytes("UTF-8"));
      byte[] barray = digest.digest();
      digest.update(password.getBytes("UTF-8"));
      digest.update(barray);
      barray = digest.digest();
      StringBuffer sbuf = new StringBuffer(40);
      for (int i = 0; i < barray.length; ++i)
      {
        String hex = Integer.toHexString((barray[i] + 256) & 0xFF).toUpperCase();
        sbuf.append(hex.length() == 1 ? "0" + hex : hex);
      }
      fromParam = sbuf.toString();
//      System.out.println(sbuf.toString());
    }
    catch (UnsupportedEncodingException e)
    {
      throw new RuntimeException(e);
    }
    catch (NoSuchAlgorithmException e1)
    {
      throw new RuntimeException(e1);
    }
    
    return fromParam;
  }
  
  public String getEncodeSHA256Password(String user, String password)
  {

	  String fromParam = "";
	    try
	    {
	        MessageDigest md = MessageDigest.getInstance("SHA-256");

	        md.update(password.getBytes("UTF-8"));
	        md.update(user.getBytes("UTF-8"));
	        byte[] dataBytes = new byte[1024];
	 
	        int nread = 0; 

	        byte[] mdbytes = md.digest();
	 
	        //convert the byte to hex format method 1
	        StringBuffer sb = new StringBuffer();
	        for (int i = 0; i < mdbytes.length; i++) {
	          sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
	        }
	        
	        fromParam = sb.toString();
	 
//	        StringBuffer hexString = new StringBuffer();
//	    	for (int i=0;i<mdbytes.length;i++) {
//	    	  hexString.append(Integer.toHexString(0xFF & mdbytes[i]));
//	    	}
	    }
	    catch (UnsupportedEncodingException e)
	    {
	      throw new RuntimeException(e);
	    }
	    catch (NoSuchAlgorithmException e1)
	    {
	      throw new RuntimeException(e1);
	    }
	    
	    return fromParam;
  }
  
  public String encodeSHA256(String password)
  {
	  String fromParam = "";
	    try
	    {
	        MessageDigest md = MessageDigest.getInstance("SHA-256");

	        md.update(password.getBytes("UTF-8"));
	        byte[] dataBytes = new byte[1024];
	 
	        int nread = 0; 
//	        while ((nread = fis.read(dataBytes)) != -1) {
//	          md.update(dataBytes, 0, nread);
//	        };
	        byte[] mdbytes = md.digest();
	 
	        //convert the byte to hex format method 1
	        StringBuffer sb = new StringBuffer();
	        for (int i = 0; i < mdbytes.length; i++) {
	          sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
	        }
	        
	        fromParam = sb.toString();
	 
//	        System.out.println("Hex format : " + sb.toString());
	 
	       //convert the byte to hex format method 2
//	        StringBuffer hexString = new StringBuffer();
//	    	for (int i=0;i<mdbytes.length;i++) {
//	    	  hexString.append(Integer.toHexString(0xFF & mdbytes[i]));
//	    	}
	 
//	    	System.out.println("Hex format : " + hexString.toString());
	    }
	    catch (UnsupportedEncodingException e)
	    {
	      throw new RuntimeException(e);
	    }
	    catch (NoSuchAlgorithmException e1)
	    {
	      throw new RuntimeException(e1);
	    }
	    
	    return fromParam;

  }
  
  public static void main(String arg[])
  {
    Authenticator suth = new Authenticator();
   // suth.getPasswordEncoding("APPLE002", "123");
    
//    String pass = suth.isAuthorizable("admin", "1234");
//    String passe = suth.getPasswordEncoding("shaii.kanith@gmail.com", "1234");
//    
//    System.out.println(passe);
//    System.out.println("F77BBCAA5A1E50AFF8B9D639BC3632030DC7970C");
    
    
    System.out.println(suth.encodeSHA256("1234"));
    System.out.println("03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4");
    
//    System.out.println("pass = " + suth.encodeSHA256("1234"));
//    
//    String passEncode = "03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4";
//    
//    System.out.println(suth.getEncodeSHA256Password("112344", passEncode));
    
  //  System.out.println("en = " + passe);
  }

}
