package com.sss.aut.service.imp;


import java.util.Map;

import com.sss.aut.service.User;
import com.sss.aut.service.UserData;
import com.sss.aut.service.UserPermission;

public class DefaultUser implements User , java.io.Serializable
{
      
   /**
   * 
   */
  private static final long serialVersionUID = 4929748515633688303L;
  private UserData userData;
   private long userId;
   private String userName;   
   
   private Map<String ,UserPermission> permission;
   
      
   public DefaultUser(long userId , String userName,UserData userData)
   {
      this.userData   = userData;
      this.userId = userId;
      this.userName = userName;
   }
   
   public DefaultUser(long userId , String userName,UserData userData , Map<String ,UserPermission>  permission)
   {
      this.userData   = userData;
      this.userId = userId;
      this.userName = userName;
      this.permission = permission;
   }

  /**
   * @author shaii
   * since Sep 10, 2009
   * @return the userData
   */
  public UserData getUserData()
  {
    return userData;
  }

  /**
   * @author shaii
   * since Sep 10, 2009
   * @param userData the userData to set
   */
  public void setUserData(UserData userData)
  {
    this.userData = userData;
  }



  public UserPermission getPermission(String screenId)
  {
    if(!permission.isEmpty())
    {
      return permission.get(screenId);
    }
    else
    {
      return null;
    }
    
    
  }

  /**
   * @author shaii
   * since Sep 11, 2009
   * @return the userId
   */
  public long getUserId()
  {
    return this.userId;
  }

  /**
   * @author shaii
   * since Sep 11, 2009
   * @return the userName
   */
  public String getUserName()
  {
    return this.userName;
  }


  public boolean isPermission()
  {
    boolean ret = false;
    if((!permission.isEmpty() ) && (permission.size() > 0))
    {
      ret = true;
    }

    return ret;
  }


  public Map<String, UserPermission> getPermission()
  {
    return this.permission;
  }

   

  
    
}
