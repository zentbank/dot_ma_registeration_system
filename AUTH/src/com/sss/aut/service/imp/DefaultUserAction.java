package com.sss.aut.service.imp;

import com.sss.aut.service.UserAction;

public class DefaultUserAction  implements UserAction
{
   /** Action name */ 
   private String name;
   
   private String uri;
   
   private String screenId;
   
   private String service;
   
   private String action;
   
   public DefaultUserAction( )
   {
    
   }
   
   public DefaultUserAction( Object securedResource , String username )
   {
      this.name = username;
   }
   
   
   public DefaultUserAction(String name , String uri)
   {
     this.name = name;
     this.uri  = uri;
   }
   
   public String getName(){return name;}
   
   public String getUri(){return uri;}

  /**
   * @return screenId
   */
  public String getScreenId()
  {
    return screenId;
  }

  /**
   * @param screenId the screenId to set
   */
  public void setScreenId(String screenId)
  {
    this.screenId = screenId;
  }

  /**
   * @return service
   */
  public String getService()
  {
    return service;
  }

  /**
   * @param service the service to set
   */
  public void setService(String service)
  {
    this.service = service;
  }

  /**
   * @return action
   */
  public String getAction()
  {
    return action;
  }

  /**
   * @param action the action to set
   */
  public void setAction(String action)
  {
    this.action = action;
  }

  /**
   * @param name the name to set
   */
  public void setName(String name)
  {
    this.name = name;
  }

  /**
   * @param uri the uri to set
   */
  public void setUri(String uri)
  {
    this.uri = uri;
  }
}
