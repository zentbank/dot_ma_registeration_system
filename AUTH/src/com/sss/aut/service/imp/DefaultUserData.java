package com.sss.aut.service.imp;



import java.util.Map;

import com.sss.aut.service.UserData;

public class DefaultUserData implements UserData,java.io.Serializable
{ 
  /**
   * 
   */
  private static final long serialVersionUID = 3281879877520930705L;
  private long userId;
  private String userName;
  private String userFullName;
  private String firstName;
  private String lastName;  
  private String prefixName; 
  
  private long groupId;
  private String positionName;
  
  private long orgId;
  private Map role;



public DefaultUserData(long userId, String userName, String userFullName,
		String firstName, String lastName, String prefixName, long groupId,
		String positionName, long orgId) {
	super();
	this.userId = userId;
	this.userName = userName;
	this.userFullName = userFullName;
	this.firstName = firstName;
	this.lastName = lastName;
	this.prefixName = prefixName;
	this.groupId = groupId;
	this.positionName = positionName;
	this.orgId = orgId;
}

public DefaultUserData(long userId, String userName, String userFullName,
		String firstName, String lastName, String prefixName,  Map role,
		String positionName, long orgId) {
	super();
	this.userId = userId;
	this.userName = userName;
	this.userFullName = userFullName;
	this.firstName = firstName;
	this.lastName = lastName;
	this.prefixName = prefixName;
	this.positionName = positionName;
	this.orgId = orgId;
}

/**
   * @author shaii
   * since Sep 10, 2009
   * @return the userId
   */
  public long getUserId()
  {
    return userId;
  }

  /**
   * @author shaii
   * since Sep 10, 2009
   * @return the userName
   */
  public String getUserName()
  {
    return userName;
  }

  /**
   * @author shaii
   * since Sep 10, 2009
   * @return the firstName
   */
  public String getFirstName()
  {
    return firstName;
  }

  /**
   * @author shaii
   * since Sep 10, 2009
   * @return the lastName
   */
  public String getLastName()
  {
    return lastName;
  }

  /**
   * @author shaii
   * since Sep 10, 2009
   * @return the prefixName
   */
  public String getPrefixName()
  {
    return prefixName;
  }



  public long getGroupId()
  {
    return groupId;
  }



public String getPositionName()
{
	return positionName;
}



public void setPositionName(String positionName)
{
	this.positionName = positionName;
}

public long getOrgId() {
	return orgId;
}

@Override
public String getUserFullName() {
	// TODO Auto-generated method stub
	return this.userFullName;
}

public Map getRoleName() {
	return role;
}




  
  

}
