package com.sss.aut.service.imp;


import com.sss.aut.service.UserPermission;

public class DefaultUserPermission implements UserPermission, java.io.Serializable
{
  /**
   * 
   */
  private static final long serialVersionUID = -4060447557007742565L;

  private String screenId;

  private boolean add;
  private boolean view;
  private boolean delete;
  private boolean edit;
  private boolean search;
  private boolean print;
  private boolean scan;
  private boolean extra1;
  private boolean extra2;

  public DefaultUserPermission()
  {
    this.add = false;
    this.view = true;
    this.delete = false;
    this.edit = false;
    this.search = false;
    this.print = false;
    this.scan = false;
    this.extra1 = false;
    this.extra2 = false;
  }

  public String getScreenId()
  {
    return screenId;
  }

  public void setScreenId(String screenId)
  {
    this.screenId = screenId;
  }

  /**
   * @return the search
   */
  public boolean isSearch()
  {
    return search;
  }

  /**
   * @param search
   *          the search to set
   */
  public void setSearch(boolean search)
  {
    this.search = search;
  }

  /**
   * @return the print
   */
  public boolean isPrint()
  {
    return print;
  }

  /**
   * @param print
   *          the print to set
   */
  public void setPrint(boolean print)
  {
    this.print = print;
  }

  /**
   * @return the scan
   */
  public boolean isScan()
  {
    return scan;
  }

  /**
   * @param scan
   *          the scan to set
   */
  public void setScan(boolean scan)
  {
    this.scan = scan;
  }

  /**
   * @return the extra1
   */
  public boolean isExtra1()
  {
    return extra1;
  }

  /**
   * @param extra1
   *          the extra1 to set
   */
  public void setExtra1(boolean extra1)
  {
    this.extra1 = extra1;
  }

  /**
   * @return the extra2
   */
  public boolean isExtra2()
  {
    return extra2;
  }

  /**
   * @param extra2
   *          the extra2 to set
   */
  public void setExtra2(boolean extra2)
  {
    this.extra2 = extra2;
  }

  /**
   * @param add
   *          the add to set
   */
  public void setAdd(boolean add)
  {
    this.add = add;
  }

  /**
   * @return the add
   */
  public boolean isAdd()
  {
    return add;
  }

  /**
   * @return the view
   */
  public boolean isView()
  {
    return view;
  }

  /**
   * @param view
   *          the view to set
   */
  public void setView(boolean view)
  {
    this.view = view;
  }

  /**
   * @return the delete
   */
  public boolean isDelete()
  {
    return delete;
  }

  /**
   * @param delete
   *          the delete to set
   */
  public void setDelete(boolean delete)
  {
    this.delete = delete;
  }

  /**
   * @return the edit
   */
  public boolean isEdit()
  {
    return edit;
  }

  /**
   * @param edit
   *          the edit to set
   */
  public void setEdit(boolean edit)
  {
    this.edit = edit;
  }

  public synchronized boolean isAuthorized(String action) throws Exception
  {
    boolean ret = false;
    /*

    Class<? extends Object> clazzOrg = this.getClass();

    Method methodOrg = clazzOrg.getMethod(action);

    Boolean res = (Boolean) methodOrg.invoke(this, null);

    ret = res.booleanValue();

    System.out.println("isAuthorized = " + res.booleanValue());
    */
    
    action = action.toUpperCase();

    if (action.indexOf("SAVE") == 0)
    {
      ret = isAdd();
    }
    else
    {
      if (action.indexOf("DELETE") == 0)
      {
        ret = isDelete();
      }
      else
      {
        if (action.indexOf("PRINT") == 0)
        {
          ret = isPrint();
        }
        else
        {
          if (action.indexOf("READ") == 0)
          {
            ret = isSearch();
          }
          else
          {
            ret = false;
          }
        }
      }
    }

    return ret;
  }

}
