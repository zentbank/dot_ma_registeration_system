package com.sss.aut.service.imp;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PasswordEncoding {
	 public static String isAuthorizable(String user, String password)
	  {
		  String fromParam = "";
		    try
		    {
		      MessageDigest digest = MessageDigest.getInstance("SHA-256");
		      digest.update(password.getBytes("UTF-8"));
		      digest.update(user.getBytes("UTF-8"));
		      byte[] barray = digest.digest();
		      digest.update(password.getBytes("UTF-8"));
		      digest.update(barray);
		      barray = digest.digest();
		      StringBuffer sbuf = new StringBuffer(40);
		      for (int i = 0; i < barray.length; ++i)
		      {
		        String hex = Integer.toHexString((barray[i] + 256) & 0xFF).toUpperCase();
		        sbuf.append(hex.length() == 1 ? "0" + hex : hex);
		      }
		      fromParam = sbuf.toString();
		      System.out.println(sbuf.toString());
		    }
		    catch (UnsupportedEncodingException e)
		    {
		      throw new RuntimeException(e);
		    }
		    catch (NoSuchAlgorithmException e1)
		    {
		      throw new RuntimeException(e1);
		    }
		    
		    return fromParam;

	  }

	  public static String getPasswordEncoding(String user, String password)
	  {

	    String fromParam = "";
	    try
	    {
	      MessageDigest digest = MessageDigest.getInstance("SHA-1");
	      digest.update(password.getBytes("UTF-8"));
	      digest.update(user.getBytes("UTF-8"));
	      byte[] barray = digest.digest();
	      digest.update(password.getBytes("UTF-8"));
	      digest.update(barray);
	      barray = digest.digest();
	      StringBuffer sbuf = new StringBuffer(40);
	      for (int i = 0; i < barray.length; ++i)
	      {
	        String hex = Integer.toHexString((barray[i] + 256) & 0xFF).toUpperCase();
	        sbuf.append(hex.length() == 1 ? "0" + hex : hex);
	      }
	      fromParam = sbuf.toString();
	      System.out.println(sbuf.toString());
	    }
	    catch (UnsupportedEncodingException e)
	    {
	      throw new RuntimeException(e);
	    }
	    catch (NoSuchAlgorithmException e1)
	    {
	      throw new RuntimeException(e1);
	    }
	    
	    return fromParam;
	  }
	  
	  public static String getSHA256PasswordEncoding(String user, String password)
	  {
	    String passEncode = PasswordEncoding.encodeSHA256(password);
	    
	    String fromParam = PasswordEncoding.toEncodeSHA256(user, passEncode);
	    
	    return fromParam;
	  }
	  
	  public static String toEncodeSHA256(String user, String password)
	  {

		  String fromParam = "";
		    try
		    {
		        MessageDigest md = MessageDigest.getInstance("SHA-256");

		        md.update(password.getBytes("UTF-8"));
		        md.update(user.getBytes("UTF-8"));
		        byte[] dataBytes = new byte[1024];
		 
		        int nread = 0; 

		        byte[] mdbytes = md.digest();
		 
		        //convert the byte to hex format method 1
		        StringBuffer sb = new StringBuffer();
		        for (int i = 0; i < mdbytes.length; i++) {
		          sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
		        }
		        
		        fromParam = sb.toString();
		 
//		        StringBuffer hexString = new StringBuffer();
//		    	for (int i=0;i<mdbytes.length;i++) {
//		    	  hexString.append(Integer.toHexString(0xFF & mdbytes[i]));
//		    	}
		    }
		    catch (UnsupportedEncodingException e)
		    {
		      throw new RuntimeException(e);
		    }
		    catch (NoSuchAlgorithmException e1)
		    {
		      throw new RuntimeException(e1);
		    }
		    
		    return fromParam;
	  }
	  
	  public static String encodeSHA256(String password)
	  {
		  String fromParam = "";
		    try
		    {
		        MessageDigest md = MessageDigest.getInstance("SHA-256");

		        md.update(password.getBytes("UTF-8"));
		        byte[] dataBytes = new byte[1024];
		 
		        int nread = 0; 
//		        while ((nread = fis.read(dataBytes)) != -1) {
//		          md.update(dataBytes, 0, nread);
//		        };
		        byte[] mdbytes = md.digest();
		 
		        //convert the byte to hex format method 1
		        StringBuffer sb = new StringBuffer();
		        for (int i = 0; i < mdbytes.length; i++) {
		          sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
		        }
		        
		        fromParam = sb.toString();
		 
//		        System.out.println("Hex format : " + sb.toString());
		 
		       //convert the byte to hex format method 2
//		        StringBuffer hexString = new StringBuffer();
//		    	for (int i=0;i<mdbytes.length;i++) {
//		    	  hexString.append(Integer.toHexString(0xFF & mdbytes[i]));
//		    	}
		 
//		    	System.out.println("Hex format : " + hexString.toString());
		    }
		    catch (UnsupportedEncodingException e)
		    {
		      throw new RuntimeException(e);
		    }
		    catch (NoSuchAlgorithmException e1)
		    {
		      throw new RuntimeException(e1);
		    }
		    
		    return fromParam;

	  }
	  
	  public static void main(String arg[])
	  {
	    
	    String passEncode = PasswordEncoding.encodeSHA256("2069");
	    
	    System.out.println(passEncode);
	    System.out.println("9552186d84ad5c6e459ce386f5ba884aa4219a0c763bc285da9edb9b03180f93");
	    
	    
	    System.out.println(PasswordEncoding.toEncodeSHA256("0115554012069", passEncode));
	    
	  //  System.out.println("en = " + passe);
	  }
}
