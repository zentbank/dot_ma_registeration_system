package sss.dot.tourism.domain;

// Generated Dec 19, 2014 3:10:09 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * ForeignLanguage generated by hbm2java
 */
@Entity
@Table(name = "FOREIGN_LANGUAGE")
public class ForeignLanguage implements java.io.Serializable {

	private long foreignId;
	private Country country;
	private Person person;
	private String recordStatus;
	private String createUser;
	private Date createDtm;
	private Date lastUpdDtm;
	private long traderId;
	private String lastUpdUser;

	public ForeignLanguage() {
	}

	public ForeignLanguage(long foreignId, Country country, Person person,
			long traderId) {
		this.foreignId = foreignId;
		this.country = country;
		this.person = person;
		this.traderId = traderId;
	}

	public ForeignLanguage(long foreignId, Country country, Person person,
			String recordStatus, String createUser, Date createDtm,
			Date lastUpdDtm, long traderId, String lastUpdUser) {
		this.foreignId = foreignId;
		this.country = country;
		this.person = person;
		this.recordStatus = recordStatus;
		this.createUser = createUser;
		this.createDtm = createDtm;
		this.lastUpdDtm = lastUpdDtm;
		this.traderId = traderId;
		this.lastUpdUser = lastUpdUser;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "FOREIGN_ID", unique = true, nullable = false)
	public long getForeignId() {
		return this.foreignId;
	}

	public void setForeignId(long foreignId) {
		this.foreignId = foreignId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FOREIGN_LANG", nullable = false)
	public Country getCountry() {
		return this.country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PERSON_ID", nullable = false)
	public Person getPerson() {
		return this.person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@Column(name = "RECORD_STATUS")
	public String getRecordStatus() {
		return this.recordStatus;
	}

	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}

	@Column(name = "CREATE_USER")
	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATE_DTM", length = 23)
	public Date getCreateDtm() {
		return this.createDtm;
	}

	public void setCreateDtm(Date createDtm) {
		this.createDtm = createDtm;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPD_DTM", length = 23)
	public Date getLastUpdDtm() {
		return this.lastUpdDtm;
	}

	public void setLastUpdDtm(Date lastUpdDtm) {
		this.lastUpdDtm = lastUpdDtm;
	}

	@Column(name = "TRADER_ID", nullable = false)
	public long getTraderId() {
		return this.traderId;
	}

	public void setTraderId(long traderId) {
		this.traderId = traderId;
	}

	@Column(name = "LAST_UPD_USER")
	public String getLastUpdUser() {
		return this.lastUpdUser;
	}

	public void setLastUpdUser(String lastUpdUser) {
		this.lastUpdUser = lastUpdUser;
	}

}
