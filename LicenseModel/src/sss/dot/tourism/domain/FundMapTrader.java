package sss.dot.tourism.domain;

// Generated Dec 19, 2014 3:10:09 PM by Hibernate Tools 3.4.0.CR1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * FundMapTrader generated by hbm2java
 */
@Entity
@Table(name = "FUND_MAP_TRADER")
public class FundMapTrader implements java.io.Serializable {

	private long fundMapTraderId;
	private Fund fund;
	private Trader trader;

	public FundMapTrader() {
	}

	public FundMapTrader(long fundMapTraderId, Fund fund, Trader trader) {
		this.fundMapTraderId = fundMapTraderId;
		this.fund = fund;
		this.trader = trader;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "FUND_MAP_TRADER_ID", unique = true, nullable = false)
	public long getFundMapTraderId() {
		return this.fundMapTraderId;
	}

	public void setFundMapTraderId(long fundMapTraderId) {
		this.fundMapTraderId = fundMapTraderId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FUND_ID", nullable = false)
	public Fund getFund() {
		return this.fund;
	}

	public void setFund(Fund fund) {
		this.fund = fund;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TRADER_ID", nullable = false)
	public Trader getTrader() {
		return this.trader;
	}

	public void setTrader(Trader trader) {
		this.trader = trader;
	}

}
