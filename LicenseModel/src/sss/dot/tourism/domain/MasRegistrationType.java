package sss.dot.tourism.domain;

// Generated Dec 19, 2014 3:10:09 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * MasRegistrationType generated by hbm2java
 */
@Entity
@Table(name = "MAS_REGISTRATION_TYPE")
public class MasRegistrationType implements java.io.Serializable {

	private long regTypeId;
	private String regTypeName;
	private String regTypeFlag;
	private String accountFlag;
	private String recordStatus;
	private String createUser;
	private Date createDtm;
	private String lastUpdUser;
	private Date lastUpdDtm;

	public MasRegistrationType() {
	}

	public MasRegistrationType(long regTypeId) {
		this.regTypeId = regTypeId;
	}

	public MasRegistrationType(long regTypeId, String regTypeName,
			String regTypeFlag, String accountFlag, String recordStatus,
			String createUser, Date createDtm, String lastUpdUser,
			Date lastUpdDtm) {
		this.regTypeId = regTypeId;
		this.regTypeName = regTypeName;
		this.regTypeFlag = regTypeFlag;
		this.accountFlag = accountFlag;
		this.recordStatus = recordStatus;
		this.createUser = createUser;
		this.createDtm = createDtm;
		this.lastUpdUser = lastUpdUser;
		this.lastUpdDtm = lastUpdDtm;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "REG_TYPE_ID", unique = true, nullable = false)
	public long getRegTypeId() {
		return this.regTypeId;
	}

	public void setRegTypeId(long regTypeId) {
		this.regTypeId = regTypeId;
	}

	@Column(name = "REG_TYPE_NAME")
	public String getRegTypeName() {
		return this.regTypeName;
	}

	public void setRegTypeName(String regTypeName) {
		this.regTypeName = regTypeName;
	}

	@Column(name = "REG_TYPE_FLAG")
	public String getRegTypeFlag() {
		return this.regTypeFlag;
	}

	public void setRegTypeFlag(String regTypeFlag) {
		this.regTypeFlag = regTypeFlag;
	}

	@Column(name = "ACCOUNT_FLAG")
	public String getAccountFlag() {
		return this.accountFlag;
	}

	public void setAccountFlag(String accountFlag) {
		this.accountFlag = accountFlag;
	}

	@Column(name = "RECORD_STATUS")
	public String getRecordStatus() {
		return this.recordStatus;
	}

	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}

	@Column(name = "CREATE_USER")
	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATE_DTM", length = 23)
	public Date getCreateDtm() {
		return this.createDtm;
	}

	public void setCreateDtm(Date createDtm) {
		this.createDtm = createDtm;
	}

	@Column(name = "LAST_UPD_USER")
	public String getLastUpdUser() {
		return this.lastUpdUser;
	}

	public void setLastUpdUser(String lastUpdUser) {
		this.lastUpdUser = lastUpdUser;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPD_DTM", length = 23)
	public Date getLastUpdDtm() {
		return this.lastUpdDtm;
	}

	public void setLastUpdDtm(Date lastUpdDtm) {
		this.lastUpdDtm = lastUpdDtm;
	}

}
