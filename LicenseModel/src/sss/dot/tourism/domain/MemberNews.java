package sss.dot.tourism.domain;

// Generated Dec 19, 2014 3:10:09 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * MemberNews generated by hbm2java
 */
@Entity
@Table(name = "MEMBER_NEWS")
public class MemberNews implements java.io.Serializable {

	private long memberNewsId;
	private MemberLicense memberLicense;
	private String memberNewsHeader;
	private String memberNewsDesc;
	private String recordStatus;
	private String createUser;
	private Date createDtm;
	private String lastUpdUser;
	private Date lastUpdDtm;
	private Date newsStartDate;
	private Date newsEndDate;
	private String mewsImgPath;
	private String traderType;

	public MemberNews() {
	}

	public MemberNews(long memberNewsId) {
		this.memberNewsId = memberNewsId;
	}

	public MemberNews(long memberNewsId, MemberLicense memberLicense,
			String memberNewsHeader, String memberNewsDesc,
			String recordStatus, String createUser, Date createDtm,
			String lastUpdUser, Date lastUpdDtm, Date newsStartDate,
			Date newsEndDate, String mewsImgPath, String traderType) {
		this.memberNewsId = memberNewsId;
		this.memberLicense = memberLicense;
		this.memberNewsHeader = memberNewsHeader;
		this.memberNewsDesc = memberNewsDesc;
		this.recordStatus = recordStatus;
		this.createUser = createUser;
		this.createDtm = createDtm;
		this.lastUpdUser = lastUpdUser;
		this.lastUpdDtm = lastUpdDtm;
		this.newsStartDate = newsStartDate;
		this.newsEndDate = newsEndDate;
		this.mewsImgPath = mewsImgPath;
		this.traderType = traderType;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "MEMBER_NEWS_ID", unique = true, nullable = false)
	public long getMemberNewsId() {
		return this.memberNewsId;
	}

	public void setMemberNewsId(long memberNewsId) {
		this.memberNewsId = memberNewsId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MEMBER_ID")
	public MemberLicense getMemberLicense() {
		return this.memberLicense;
	}

	public void setMemberLicense(MemberLicense memberLicense) {
		this.memberLicense = memberLicense;
	}

	@Column(name = "MEMBER_NEWS_HEADER")
	public String getMemberNewsHeader() {
		return this.memberNewsHeader;
	}

	public void setMemberNewsHeader(String memberNewsHeader) {
		this.memberNewsHeader = memberNewsHeader;
	}

	@Column(name = "MEMBER_NEWS_DESC")
	public String getMemberNewsDesc() {
		return this.memberNewsDesc;
	}

	public void setMemberNewsDesc(String memberNewsDesc) {
		this.memberNewsDesc = memberNewsDesc;
	}

	@Column(name = "RECORD_STATUS")
	public String getRecordStatus() {
		return this.recordStatus;
	}

	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}

	@Column(name = "CREATE_USER")
	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATE_DTM", length = 23)
	public Date getCreateDtm() {
		return this.createDtm;
	}

	public void setCreateDtm(Date createDtm) {
		this.createDtm = createDtm;
	}

	@Column(name = "LAST_UPD_USER")
	public String getLastUpdUser() {
		return this.lastUpdUser;
	}

	public void setLastUpdUser(String lastUpdUser) {
		this.lastUpdUser = lastUpdUser;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPD_DTM", length = 23)
	public Date getLastUpdDtm() {
		return this.lastUpdDtm;
	}

	public void setLastUpdDtm(Date lastUpdDtm) {
		this.lastUpdDtm = lastUpdDtm;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "NEWS_START_DATE", length = 23)
	public Date getNewsStartDate() {
		return this.newsStartDate;
	}

	public void setNewsStartDate(Date newsStartDate) {
		this.newsStartDate = newsStartDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "NEWS_END_DATE", length = 23)
	public Date getNewsEndDate() {
		return this.newsEndDate;
	}

	public void setNewsEndDate(Date newsEndDate) {
		this.newsEndDate = newsEndDate;
	}

	@Column(name = "MEWS_IMG_PATH")
	public String getMewsImgPath() {
		return this.mewsImgPath;
	}

	public void setMewsImgPath(String mewsImgPath) {
		this.mewsImgPath = mewsImgPath;
	}

	@Column(name = "TRADER_TYPE")
	public String getTraderType() {
		return this.traderType;
	}

	public void setTraderType(String traderType) {
		this.traderType = traderType;
	}

}
