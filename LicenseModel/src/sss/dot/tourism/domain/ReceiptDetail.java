package sss.dot.tourism.domain;

// Generated Dec 19, 2014 3:10:09 PM by Hibernate Tools 3.4.0.CR1

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * ReceiptDetail generated by hbm2java
 */
@Entity
@Table(name = "RECEIPT_DETAIL")
public class ReceiptDetail implements java.io.Serializable {

	private long receiptDetailId;
	private Receipt receipt;
	private MasFee masFee;
	private String feeName;
	private BigDecimal feeMny;
	private String recordStatus;
	private String createUser;
	private Date createDtm;
	private String lastUpdUser;
	private Date lastUpdDtm;

	public ReceiptDetail() {
	}

	public ReceiptDetail(long receiptDetailId) {
		this.receiptDetailId = receiptDetailId;
	}

	public ReceiptDetail(long receiptDetailId, Receipt receipt, MasFee masFee,
			String feeName, BigDecimal feeMny, String recordStatus,
			String createUser, Date createDtm, String lastUpdUser,
			Date lastUpdDtm) {
		this.receiptDetailId = receiptDetailId;
		this.receipt = receipt;
		this.masFee = masFee;
		this.feeName = feeName;
		this.feeMny = feeMny;
		this.recordStatus = recordStatus;
		this.createUser = createUser;
		this.createDtm = createDtm;
		this.lastUpdUser = lastUpdUser;
		this.lastUpdDtm = lastUpdDtm;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "RECEIPT_DETAIL_ID", unique = true, nullable = false)
	public long getReceiptDetailId() {
		return this.receiptDetailId;
	}

	public void setReceiptDetailId(long receiptDetailId) {
		this.receiptDetailId = receiptDetailId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "RECEIPT_ID")
	public Receipt getReceipt() {
		return this.receipt;
	}

	public void setReceipt(Receipt receipt) {
		this.receipt = receipt;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FEE")
	public MasFee getMasFee() {
		return this.masFee;
	}

	public void setMasFee(MasFee masFee) {
		this.masFee = masFee;
	}

	@Column(name = "FEE_NAME")
	public String getFeeName() {
		return this.feeName;
	}

	public void setFeeName(String feeName) {
		this.feeName = feeName;
	}

	@Column(name = "FEE_MNY")
	public BigDecimal getFeeMny() {
		return this.feeMny;
	}

	public void setFeeMny(BigDecimal feeMny) {
		this.feeMny = feeMny;
	}

	@Column(name = "RECORD_STATUS")
	public String getRecordStatus() {
		return this.recordStatus;
	}

	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}

	@Column(name = "CREATE_USER")
	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATE_DTM", length = 23)
	public Date getCreateDtm() {
		return this.createDtm;
	}

	public void setCreateDtm(Date createDtm) {
		this.createDtm = createDtm;
	}

	@Column(name = "LAST_UPD_USER")
	public String getLastUpdUser() {
		return this.lastUpdUser;
	}

	public void setLastUpdUser(String lastUpdUser) {
		this.lastUpdUser = lastUpdUser;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPD_DTM", length = 23)
	public Date getLastUpdDtm() {
		return this.lastUpdDtm;
	}

	public void setLastUpdDtm(Date lastUpdDtm) {
		this.lastUpdDtm = lastUpdDtm;
	}

}
