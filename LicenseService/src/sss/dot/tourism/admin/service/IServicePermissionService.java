package sss.dot.tourism.admin.service;

public interface IServicePermissionService {
	public boolean register(String ipAddress, String org) throws Exception;
	
	public boolean registerAsL2(String ipAddress, String org) throws Exception;
}
