package sss.dot.tourism.admin.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.domain.ServicePermission;
import sss.dot.tourism.license.dao.ServicePermissionDAO;

@Repository("servicePermissionService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ServicePermissionService implements IServicePermissionService {

	@Autowired
	private ServicePermissionDAO servicePermissionDAO;
	
	@Override
	public boolean register(String ipAddress, String org) throws Exception {
		
//		List<ServicePermission> listAll = this.servicePermissionDAO.findUser(ipAddress, org, false);
//		return !listAll.isEmpty();
		System.out.println("ipAddress: "+ipAddress+" org: "+org);
		return true;
	}

	@Override
	public boolean registerAsL2(String ipAddress, String org) throws Exception {
		List<ServicePermission> listAll = this.servicePermissionDAO.findUser(ipAddress, org, true);
		return !listAll.isEmpty();
	}

}
