package sss.dot.tourism.certificate.service;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import sss.dot.tourism.certificate.dao.ReqLicenseCertificateDAO;
import sss.dot.tourism.certificate.dao.ReqLicenseCertificateDocDAO;
import sss.dot.tourism.certificate.dto.ReqLicenseCertificateDTO;
import sss.dot.tourism.domain.MasDocument;
import sss.dot.tourism.domain.Person;
import sss.dot.tourism.domain.ReqLicenseCertificate;
import sss.dot.tourism.domain.ReqLicenseCertificateDoc;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.license.dao.TraderDAO;
import sss.dot.tourism.mas.dao.MasDocumentDAO;
import sss.dot.tourism.util.ConstantUtil;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.FileManage;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.PersonType;
import sss.dot.tourism.util.ProgressStatus;
import sss.dot.tourism.util.RecordStatus;
import sss.dot.tourism.util.ThaiBahtUtil;
import sss.dot.tourism.util.TraderCategory;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.User;

@Repository("reqLicenseCertificateService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ReqLicenseCertificateService implements
		IReqLicenseCertificateService {

	@Autowired
	private ReqLicenseCertificateDAO reqLicenseCertificateDAO;

	@Autowired
	private ReqLicenseCertificateDocDAO reqLicenseCertificateDocDAO;

	@Autowired
	private TraderDAO traderDAO;

	@Autowired
	private MasDocumentDAO masDocumentDAO;

	DateFormat df = DateUtils.getProcessDateFormatThai();

	@Override
	public Object getById(Object object, User user) throws Exception {
		if (!(object instanceof ReqLicenseCertificateDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		ReqLicenseCertificateDTO param = (ReqLicenseCertificateDTO) object;
		ReqLicenseCertificateDTO dto = new ReqLicenseCertificateDTO();

		ReqLicenseCertificate cer = (ReqLicenseCertificate) this.reqLicenseCertificateDAO
				.findByPrimaryKey(param.getCerId());

		List<Trader> listTrader = this.traderDAO.findTraderByLicenseNo(
				cer.getLicenseNo(), TraderType.TOUR_COMPANIES.getStatus(),
				RecordStatus.NORMAL.getStatus(), user.getUserData().getOrgId());

		if (!listTrader.isEmpty()) {
			Trader trader = listTrader.get(0);
			ObjectUtil.copy(cer, dto);
			ObjectUtil.copy(trader, dto);

			Person person = trader.getPerson();

			ObjectUtil.copy(person, dto);
			if (PersonType.CORPORATE.getStatus().equals(person.getPersonType())) {

				String traderOwnerName = person.getMasPrefix() == null ? ""
						: person.getMasPrefix().getPrefixName()
								+ person.getFirstName()
								+ (person.getMasPosfix() == null ? "" : person
										.getMasPosfix().getPostfixName());
				dto.setTraderOwnerName(traderOwnerName);
			} else {
				String traderOwnerName = person.getMasPrefix() == null ? ""
						: person.getMasPrefix().getPrefixName()
								+ person.getFirstName() + " "
								+ person.getLastName();
				dto.setTraderOwnerName(traderOwnerName);

			}

			dto.setTraderCategoryName(TraderCategory.getTraderCategory(
					dto.getTraderType(), dto.getTraderCategory()).getMeaning());
		}

		return dto;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveAll(Object object, User user) throws Exception {

		if (!(object instanceof ReqLicenseCertificateDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		ReqLicenseCertificateDTO param = (ReqLicenseCertificateDTO) object;

		if (!param.isCerIdNotEmpty()) {
			this.create(object, user);
		} else {
			this.update(object, user);
		}

	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	private void create(Object object, User user) throws Exception {

		if (!(object instanceof ReqLicenseCertificateDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		ReqLicenseCertificateDTO param = (ReqLicenseCertificateDTO) object;

		Trader trader = (Trader) this.traderDAO.findByPrimaryKey(param
				.getTraderId());

		ReqLicenseCertificate cer = new ReqLicenseCertificate();
		cer.setLicenseNo(trader.getLicenseNo());

		ObjectUtil.copy(param, cer);

		cer.setApproveStatus(ProgressStatus.WAITING.getStatus());
		cer.setRecordStatus(RecordStatus.NORMAL.getStatus());
		cer.setCreateUser(user.getUserName());
		cer.setCreateDtm(new Date());

		Long cerId = (Long) this.reqLicenseCertificateDAO.insert(cer);
		param.setCerId(cerId);

		this.addDoc(cer, user);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	private void addDoc(ReqLicenseCertificate cer, User user) throws Exception {

		List<MasDocument> listMasDoc = (List<MasDocument>) this.reqLicenseCertificateDocDAO
				.findMasDoc();

		if (!listMasDoc.isEmpty()) {

			for (MasDocument masDocument : listMasDoc) {
				ReqLicenseCertificateDoc doc = new ReqLicenseCertificateDoc();
				doc.setReqDocPath("");
				doc.setMasDocument(masDocument);
				doc.setReqLicenseCertificate(cer);

				doc.setRecordStatus(RecordStatus.NORMAL.getStatus());
				doc.setCreateDtm(new Date());
				doc.setCreateUser(user.getUserName());

				this.reqLicenseCertificateDocDAO.insert(doc);
			}
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	private void update(Object object, User user) throws Exception {

		if (!(object instanceof ReqLicenseCertificateDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		ReqLicenseCertificateDTO param = (ReqLicenseCertificateDTO) object;

		ReqLicenseCertificate cer = (ReqLicenseCertificate)this.reqLicenseCertificateDAO.findByPrimaryKey(param.getCerId());

		ObjectUtil.copy(param, cer);

		cer.setRecordStatus(RecordStatus.NORMAL.getStatus());
		cer.setLastUpdUser(user.getUserName());
		cer.setLastUpdDtm(new Date());

		this.reqLicenseCertificateDAO.update(cer);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(Object obj, User user) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public List getByPage(Object object, User user, int start, int limit)
			throws Exception {

		if (!(object instanceof ReqLicenseCertificateDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}

		ReqLicenseCertificateDTO param = (ReqLicenseCertificateDTO) object;

		List<Object[]> listAll = this.reqLicenseCertificateDAO.findByPaging(
				param, start, limit, user.getUserData().getOrgId());

		List<ReqLicenseCertificateDTO> list = new ArrayList<ReqLicenseCertificateDTO>();

		if (!listAll.isEmpty()) {
			for (Object[] obj : listAll) {

				ReqLicenseCertificateDTO dto = new ReqLicenseCertificateDTO();

				dto.setCerId(Long.valueOf(obj[8].toString()));
				dto.setLicenseNo(obj[0].toString());
				dto.setTraderName(obj[1].toString());
				dto.setTraderNameEn(obj[2].toString());
				if (obj[3] != null) {
					dto.setTraderCategory(TraderCategory.getMeaning(
							TraderType.TOUR_COMPANIES.getStatus(),
							obj[3].toString()));
				}
				if (obj[4] != null) {
					dto.setReqDate(df.format(obj[4]));
				}
				if (obj[5] != null) {
					dto.setApproveDate(df.format(obj[5]));
				}
				if (obj[6] != null) {
					dto.setApproveStatus(ProgressStatus.getMeaning(obj[6]
							.toString()));
				}
				if (obj[7] != null) {
					dto.setPrintDate(df.format(obj[7]));
				}

				list.add(dto);

			}
		}

		return list;
	}

	@Override
	public int countAll(Object object, User user, int start, int limit)
			throws Exception {

		if (!(object instanceof ReqLicenseCertificateDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}

		ReqLicenseCertificateDTO param = (ReqLicenseCertificateDTO) object;

		List<Object[]> listAll = this.reqLicenseCertificateDAO.findByPaging(
				param, start, limit, user.getUserData().getOrgId());

		return listAll.isEmpty() ? 0 : listAll.size();
	}

	@Override
	public List getDocuments(Object object, User user) throws Exception {

		if (!(object instanceof ReqLicenseCertificateDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}

		ReqLicenseCertificateDTO param = (ReqLicenseCertificateDTO) object;

		List<ReqLicenseCertificateDTO> list = new ArrayList<ReqLicenseCertificateDTO>();

		List<ReqLicenseCertificateDoc> listAll = (List<ReqLicenseCertificateDoc>) this.reqLicenseCertificateDocDAO
				.findAll(param);

		if (!listAll.isEmpty()) {

			for (ReqLicenseCertificateDoc doc : listAll) {

				ReqLicenseCertificateDTO dto = new ReqLicenseCertificateDTO();

				ObjectUtil.copy(doc, dto);
				ObjectUtil.copy(doc.getMasDocument(), dto);

				list.add(dto);

			}
		}
		return list;
	}
	

	@Override
	public Object getDocumentsById(Object object, User user) throws Exception {
		if (!(object instanceof ReqLicenseCertificateDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}

		ReqLicenseCertificateDTO param = (ReqLicenseCertificateDTO) object;
		
		ReqLicenseCertificateDoc reqDoc = (ReqLicenseCertificateDoc)this.reqLicenseCertificateDocDAO.findByPrimaryKey(param.getReqDocId());
		
		ReqLicenseCertificateDTO dto = new ReqLicenseCertificateDTO();
		
		ObjectUtil.copy(reqDoc, dto);
		ObjectUtil.copy(reqDoc.getReqLicenseCertificate(), dto);
		
		return dto;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveDocumentAll(Object object, List<MultipartFile> files, User user)
			throws Exception {
		
		if (!(object instanceof ReqLicenseCertificateDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		ReqLicenseCertificateDTO param = (ReqLicenseCertificateDTO) object;
//		mas doc
//		108	2	ใบรับรองบริษัท
//		109	1	สำเนาใบอนุญาตประกอบธุรกิจนำเที่ยว
//		110	3	หนังสือใบคำขอ
		
		List<ReqLicenseCertificateDoc> reqDocs = (List<ReqLicenseCertificateDoc>) this.reqLicenseCertificateDocDAO
				.findAll(param);
		
		if(!files.isEmpty()){
			MultipartFile file0 = files.get(0);
			
			if(!reqDocs.isEmpty()){
				for(ReqLicenseCertificateDoc doc: reqDocs){
					if(doc.getMasDocument().getMasDocId() == 109){
						this.saveDocument(doc.getReqDocId(), file0, user);
					}
				}
			}
			
			MultipartFile file1 = files.get(1);
			
			if(!reqDocs.isEmpty()){
				for(ReqLicenseCertificateDoc doc: reqDocs){
					if(doc.getMasDocument().getMasDocId() == 108){
						this.saveDocument(doc.getReqDocId(), file1, user);
					}
				}
			}
			
			MultipartFile file2 = files.get(2);
			
			if(!reqDocs.isEmpty()){
				for(ReqLicenseCertificateDoc doc: reqDocs){
					if(doc.getMasDocument().getMasDocId() == 110){
						this.saveDocument(doc.getReqDocId(), file2, user);
					}
				}
			}
		}
		
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveDocument(long reqDocId, MultipartFile documentFile,
			User user) throws Exception {

		ReqLicenseCertificateDoc reqDoc = (ReqLicenseCertificateDoc) this.reqLicenseCertificateDocDAO
				.findByPrimaryKey(reqDocId);

		ReqLicenseCertificate cer = reqDoc.getReqLicenseCertificate();

		String sap = java.io.File.separator;

		// TraderType
		String traderTypeName = "Business";
		String certificateDocFolder = "ReqLicenseCertificateDoc";
		// LicenseNo
		String licenseNoStr = cer.getLicenseNo();
		licenseNoStr = licenseNoStr.replaceAll("[.]", "");
		licenseNoStr = licenseNoStr.replaceAll("[/]", "");
		licenseNoStr = licenseNoStr.replaceAll("[-]", "");

		String realFileName = Calendar.getInstance().getTime().getTime() + "";

		Calendar cal = Calendar.getInstance();
		DateFormat f = DateUtils.getProcessDateFormatEng();

		String date = f.format(cal.getTime());
		String fold = date.replaceAll("[-]", "");

		// postfix FileName
		String extension = ".pdf";
		String originalFilename = documentFile.getOriginalFilename();
		int dot = originalFilename.lastIndexOf('.');
		if (dot != -1) {
			extension = originalFilename.substring(dot);
		}

		String root = ConstantUtil.ROOT_PATH;
		String path = root + certificateDocFolder + sap + traderTypeName + sap
				+ licenseNoStr + sap + fold;
		String fileName = sap + realFileName + extension;

		String fullpath = path + fileName;

		reqDoc.setReqDocPath(fullpath);

		if (documentFile != null) {

			// Upload File
			FileManage fm = new FileManage();
			fm.uploadFile(documentFile.getInputStream(), path, fileName);

		}
		
		this.reqLicenseCertificateDocDAO.update(reqDoc);

	}

	@Override
	public Object getReqCertificateDocx(Object object, User user) throws Exception {
		if (!(object instanceof ReqLicenseCertificateDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		ReqLicenseCertificateDTO param = (ReqLicenseCertificateDTO) object;
		ReqLicenseCertificateDTO dto = new ReqLicenseCertificateDTO();

		ReqLicenseCertificate cer = (ReqLicenseCertificate) this.reqLicenseCertificateDAO
				.findByPrimaryKey(param.getCerId());

		List<Trader> listTrader = this.traderDAO.findTraderByLicenseNo(
				cer.getLicenseNo(), TraderType.TOUR_COMPANIES.getStatus(),
				RecordStatus.NORMAL.getStatus(), user.getUserData().getOrgId());

		if (!listTrader.isEmpty()) {
			Trader trader = listTrader.get(0);
			ObjectUtil.copy(cer, dto);
			ObjectUtil.copy(trader, dto);

			Person person = trader.getPerson();

			ObjectUtil.copy(person, dto);
			if (PersonType.CORPORATE.getStatus().equals(person.getPersonType())) {

				String traderOwnerName = person.getMasPrefix() == null ? ""
						: person.getMasPrefix().getPrefixName()
								+ person.getFirstName()
								+ (person.getMasPosfix() == null ? "" : person
										.getMasPosfix().getPostfixName());
				dto.setTraderOwnerName(traderOwnerName);
			} else {
				String traderOwnerName = person.getMasPrefix() == null ? ""
						: person.getMasPrefix().getPrefixName()
								+ person.getFirstName() + " "
								+ person.getLastName();
				dto.setTraderOwnerName(traderOwnerName);

			}

			dto.setTraderCategoryName(TraderCategory.getTraderCategory(
					dto.getTraderType(), dto.getTraderCategory()).getMeaning());
			
			dto.setLicenseNo(ThaiBahtUtil.convertText2Numthai(trader.getLicenseNo()));
			dto.setLicenseNoEn(trader.getLicenseNo());
			dto.setTraderName(dto.getTraderOwnerName());
			dto.setTraderNameEn(trader.getTraderNameEn() + " CO.,LTD.");
			
			String ttAaDate = DateUtils.getDateWithMonthName(df.format(cer.getTtAaDate()==null?new Date():cer.getTtAaDate()));
			dto.setTtAaDate(ThaiBahtUtil.convertText2Numthai(ttAaDate));
			
			
			String bookDocToManagerNo = cer.getBookDocToManagerNo()==null? "กก":cer.getBookDocToManagerNo();
			dto.setBookDocToManagerNo(ThaiBahtUtil.convertText2Numthai(bookDocToManagerNo));
			String bookDocToManagerDate = DateUtils.getDateWithMonthName(df.format(cer.getBookDocManagerDate()==null?new Date():cer.getBookDocManagerDate()));
			dto.setBookDocManagerDate(ThaiBahtUtil.convertText2Numthai(bookDocToManagerDate));
				
			String bookNo = cer.getBookNo()==null? "กก":cer.getBookNo();
			dto.setBookNo(ThaiBahtUtil.convertText2Numthai(bookNo));
			String bookDate = DateUtils.getDateWithMonthName(df.format(cer.getBookDate()==null?new Date():cer.getBookDate()));
			dto.setBookDate(ThaiBahtUtil.convertText2Numthai(bookDate));
		}

		return dto;
	}

	@Override
	public Map<String, ?> getReqCertificateExcel(Object object, User user)
			throws Exception {
		if (!(object instanceof ReqLicenseCertificateDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		ReqLicenseCertificateDTO param = (ReqLicenseCertificateDTO) object;
		
		List<Object[]> listAll = this.reqLicenseCertificateDAO.findByPaging(
				param, 0, Integer.MAX_VALUE, user.getUserData().getOrgId());

		

		Map model = new HashMap();
		if (!listAll.isEmpty()) {
			List<ReqLicenseCertificateDTO> list = new ArrayList<ReqLicenseCertificateDTO>();
			for (Object[] obj : listAll) {

				ReqLicenseCertificateDTO dto = new ReqLicenseCertificateDTO();

				dto.setCerId(Long.valueOf(obj[8].toString()));
				
				dto = (ReqLicenseCertificateDTO)this.getReqCertificateDocx(dto, user);
				
				list.add(dto);
			}
			model.put("reqcertificate", list);
		}
		
		return model;
	}

}
