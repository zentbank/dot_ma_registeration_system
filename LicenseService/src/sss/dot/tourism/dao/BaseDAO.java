package sss.dot.tourism.dao;






import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import net.sf.cglib.reflect.FastClass;
import net.sf.cglib.reflect.FastMethod;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateObjectRetrievalFailureException;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;





/**
 * 
 * @author Jakkrit Sittiwerapong
 * @since 4 Dec 2008
 * @version 1.0.1
 * @category CommonService BaseDAO.java
 */
public abstract class BaseDAO extends HibernateDaoSupport implements IBaseDAO
{
  /**
   * 
   */
  private static final long serialVersionUID = 6731444393734858941L;

  protected Class<?> domainObj;

  protected Logger log = Logger.getLogger(this.getClass());
  
  
	@Autowired
    public void anyMethodName(SessionFactory sessionFactory)
    {
        setSessionFactory(sessionFactory);
    }

  /**
   * 
   * @param message
   */
  protected void debug(String message)
  {
    this.log.debug(message);
  }

  /**
   * 
   * @param message
   */
  protected void info(String message)
  {
    this.log.info(message);
  }

  /**
   * 
   * @param message
   */
  protected void warn(String message)
  {
    this.log.warn(message);
  }

  /**
   * 
   * @param message
   */
  protected void error(String message)
  {
    this.log.error(message);
  }

  /**
   * 
   * @param message
   * @param t
   */
  protected void error(String message, Throwable t)
  {
    this.log.error(message, t);
  }

  /**
   * @see com.sss.dao.IBaseDAO#clone(java.lang.Object)
   */
//  public Object clonePersistance(Object obj)
//  {
//    Object clone = null;
//    try
//    {
//      clone = ObjectUtil.deepClone(obj);
//    }
//    catch (Exception e)
//    {
//      this.error(e.getMessage(), e);
//    }
//    return clone;
//  }
  
  public Object findByPrimaryKey(String primaryKey)
  {
    return this.getHibernateTemplate().get(domainObj, primaryKey);
  }
  /**
   * 
   * @see com.sss.dao.IBaseDAO#findByPrimaryKey(java.math.BigDecimal)
   */
  public Object findByPrimaryKey(BigDecimal primaryKey)
  {
    return this.getHibernateTemplate().get(domainObj, primaryKey);
  }
  
  public Object findByPrimaryKey(Long primaryKey)
  {
    return this.getHibernateTemplate().get(domainObj, primaryKey);
  }
  public Object findByPrimaryKey(int primaryKey)
  {
    return this.getHibernateTemplate().get(domainObj, primaryKey);
  }
  /**
   * 
   * @see com.sss.dao.IBaseDAO#findByPrimaryKey(java.lang.Object)
   */
  public Object findByPrimaryKey(Object primaryKey)
  {
    return this.findByPrimaryKey((BigDecimal) primaryKey);
  }
  /**
   * 
   * @see com.sss.dao.IBaseDAO#insert(java.lang.Object)
   */
  public Object insert(Object persistence)
  {
    try
    {
      FastClass clazz = FastClass.create(persistence.getClass());

//      FastMethod method = clazz.getMethod("setRecordStatus", new Class[] { String.class });
//
//      method.invoke(persistence, new Object[] { "N" });

      FastMethod method2 = clazz.getMethod("setCreateDtm", new Class[] { Date.class });

      method2.invoke(persistence, new Object[] { new Date() });

    }
    catch (InvocationTargetException e)
    {

      this.error(e.getMessage(), e);

    }
    return this.getHibernateTemplate().save(persistence);
  }
  /**
   * 
   * @see com.sss.dao.IBaseDAO#update(java.lang.Object)
   */
  public void update(Object persistence)
  {

    try
    {
      FastClass clazz = FastClass.create(persistence.getClass());

      FastMethod method = clazz.getMethod("setLastUpdDtm", new Class[] { Date.class });

      method.invoke(persistence, new Object[] { new Date() });

    }
    catch (InvocationTargetException e)
    {

      this.error(e.getMessage(), e);

    }
    this.getHibernateTemplate().update(persistence);
  }
  /**
   * 
   * @see com.sss.dao.IBaseDAO#deleteWithStatus(java.lang.Object)
   */
  public void deleteWithStatus(Object persistence)
  {
    try
    {
      FastClass clazz = FastClass.create(persistence.getClass());

      FastMethod method = clazz.getMethod("setRecordStatus", new Class[] { String.class });

      method.invoke(persistence, new Object[] { "D" });

      update(persistence);

    }
    catch (InvocationTargetException e)
    {
      this.error(e.getMessage(), e);
    }

  }
  /**
   * 
   * @see com.sss.dao.IBaseDAO#delete(java.lang.Object)
   */
  public void delete(Object persistence)
  {
    FastClass clazz = FastClass.create(persistence.getClass());
    getHibernateTemplate().delete(persistence);
  }
  /**
   * 
   * @see com.sss.dao.IBaseDAO#refresh(java.lang.Object)
   */
  public void refresh(Object persistence)
  {
    this.getHibernateTemplate().refresh(persistence);
  }
  /**
   * 
   * @see com.sss.dao.IBaseDAO#findAll()
   */
  
  /*
  public List<?> findAll()
  {
    List<?> resultList = null;
    try
    {
      resultList = this.getHibernateTemplate().loadAll(domainObj);
    }
    catch (ObjectNotFoundException e)
    {
      this.error(e.getMessage(),e);
    }
    catch (HibernateObjectRetrievalFailureException e)
    {
      this.error(e.getMessage(),e);
    }
    return resultList;
  }
  */
  /**
   * 
   * @see com.sss.dao.IBaseDAO#findAll(com.sss.util.system.RecordStatus)
   */
  public List<?> findAll()
  {
    List<?> result = null;
    try
    {
      String primaryKey = this.getHibernateTemplate().getSessionFactory().getClassMetadata(domainObj).getIdentifierPropertyName();

      String hql = "from " + domainObj.getSimpleName() + "  order by " + primaryKey;

      result = this.getHibernateTemplate().find(hql);

    }
    catch (ObjectNotFoundException e)
    {
      this.error(e.getMessage(), e);
    }
    catch (HibernateObjectRetrievalFailureException e)
    {
      this.error(e.getMessage(), e);
    }
    return result;
  }
  

  /**
   * 
   * @see com.sss.dao.IBaseDAO#countAll()
   */
  public long countAll()
  {

    String hql = "select new map(count(*) as total) from " + domainObj.getSimpleName();

    Iterator itr = this.getHibernateTemplate().iterate(hql);

    return (itr.hasNext()) ? ((Long) itr.next()).longValue() : 0;
  }



  /**
   * 
   * @see com.sss.dao.IBaseDAO#findByAddPaging(java.lang.String, int, int)
   */
  public List<?> findByAddPaging(String hql, int firstRow, int pageSize)
  {

    Query q = this.getSession().createQuery(hql);

    q.setMaxResults(pageSize);

    q.setFirstResult(firstRow);

    return q.list();
  }
  /**
   * 
   * @see com.sss.dao.IBaseDAO#findByAddPaging(java.lang.String, int, int, java.lang.Object[])
   */
  public List<?> findByAddPaging(String hql, int firstRow, int pageSize, Object... params)
  {

    Query q = this.getSession().createQuery(hql);

    if (params.length > 0)
    {
      int size = params.length;

      for (int i = 0; i < size; i++)
      {
        Object param = params[i];

        if (param instanceof String)
        {

          q.setString(i, (String) param);

        }
        else if (param instanceof Date)
        {

          q.setDate(i, (Date) param);

        }
        else if (param instanceof Integer)
        {

          q.setInteger(i, (Integer) param);

        }
        else if (param instanceof Long)
        {

          q.setLong(i, (Long) param);

        }
        else if (param instanceof Double)
        {

          q.setDouble(i, (Double) param);

        }
        else if (param instanceof Float)
        {

          q.setFloat(i, (Float) param);

        }
        else if (param instanceof Boolean)
        {

          q.setBoolean(i, (Boolean) param);

        }
        else if (param instanceof BigDecimal)
        {// for field BigDecimal

          q.setBigDecimal(i, (BigDecimal) param);

        }
      }

    }

    q.setMaxResults(pageSize);

    q.setFirstResult(firstRow);

    return q.list();
  }

  /**
   * for create Criteria Query
   * 
   * @return
   */
  protected Criteria getCriteriaQuery()
  {
    return super.getSession().createCriteria(this.domainObj);
  }

  /**
   * for create DetachedCriteria Query
   * 
   * @return
   */
  protected DetachedCriteria getDetachedCriteriaQuery()
  {
    return DetachedCriteria.forClass(this.domainObj);
  }

  /**
   * for execute Criteria Query
   * 
   * @param criteria
   * @param firstRow
   * @param pageSize
   * @return
   */
  protected List<?> execute(Criteria criteria, int firstRow, int pageSize)
  {
    criteria.setFirstResult(firstRow);
    criteria.setMaxResults(pageSize);
    return criteria.list();
  }

  /**
   * for execute DetachedCriteria Query
   * 
   * @param criteria
   * @param firstRow
   * @param pageSize
   * @return
   */
  protected List<?> execute(DetachedCriteria criteria, int firstRow, int pageSize)
  {
    return super.getHibernateTemplate().findByCriteria(criteria, firstRow, pageSize);
  }

}
