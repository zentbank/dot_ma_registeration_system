package sss.dot.tourism.dao;





import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;






public interface IBaseDAO extends Serializable {
	/**
	 * 
	 * @param primaryKey
	 * @return
	 */
	public Object findByPrimaryKey(BigDecimal primaryKey);
	/**
	 * 
	 * @param primaryKey
	 * @return
	 */
	public Object findByPrimaryKey(Object primaryKey);
	/**
	 * 
	 * @return
	 */
	public List<?> findAll();


	/**
	 * 
	 * @return
	 */
	public long countAll();
	/**
	 * 
	 * @param persistence
	 * @return
	 */
	public Object insert(Object persistence);
	/**
	 * 
	 * @param persistence
	 */
	public void update(Object persistence);
	/**
	 * 
	 * @param persistence
	 */
	public void delete(Object persistence);
	/**
	 * 
	 * @param persistence
	 */
	public void refresh(Object persistence);
	/**
	 * 
	 * @param persistence
	 */
	public void deleteWithStatus(Object persistence);
	/**
	 * 
	 * @param recordStatus
	 * @return
	 */
//	public List<?> findAll(RecordStatus recordStatus);
	


	/**
	 * 
	 * @param o
	 * @return
	 */
	//public Object clonePersistance(Object obj);
	/**
	 * 
	 * @param hql
	 * @param firstRow
	 * @param pageSize
	 * @return
	 */
	public List<?> findByAddPaging(String hql, int firstRow, int pageSize);
	/**
	 * 
	 * @param hql
	 * @param firstRow
	 * @param pageSize
	 * @param params
	 * @return
	 */
	public List<?> findByAddPaging(String hql, int firstRow, int pageSize,Object... params);
	
}
