package sss.dot.tourism.license.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.AdmUser;
import sss.dot.tourism.domain.ServicePermission;

@Repository("servicePermissionDAO")
public class ServicePermissionDAO extends BaseDAO{

	public ServicePermissionDAO()
	{
		this.domainObj = ServicePermission.class;
	}
	
	public List<ServicePermission> findUser(String ipAddr, String orgName, boolean flagL2) throws Exception
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  ServicePermission as dep ");
		hql.append(" where dep.recordStatus = 'N' ");
		
		hql.append(" and dep.ipAddr = ? ");
		params.add(ipAddr);
		
		hql.append(" and dep.orgName = ? ");
		params.add(orgName);
		
		if(flagL2){
			hql.append(" and dep.flagL2 = '1' ");
		}
	
		
		return (List<ServicePermission>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
}
