package sss.dot.tourism.license.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.license.dto.LicenseDTO;
import sss.dot.tourism.registration.dto.RegistrationDTO;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.User;


@Repository("traderDAO")
public class TraderDAO extends BaseDAO{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6745094694356102567L;

	public TraderDAO()
	{
		this.domainObj = Trader.class;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Object[]> findAll(LicenseDTO param) throws Exception{
		   Map<String,Object> params = new HashMap<String,Object>();
		    
		    StringBuilder hql = new StringBuilder();
		    
		    hql.append(" SELECT  ");
		    hql.append("  	TR.LICENSE_NO AS LICENSE_NO, ");
		    hql.append("  	TR.LICENSE_STATUS AS LICENSE_STATUS, ");
		    hql.append("  	CASE TR.LICENSE_STATUS ");
		    hql.append("  		WHEN 'N' THEN 'ปกติ' ");
		    hql.append(" 		WHEN 'C' THEN 'ยกเลิก' ");
		    hql.append(" 		WHEN 'S' THEN 'พักใช้' ");
		    hql.append(" 		WHEN 'R' THEN 'เพิกถอน' ");
		    hql.append(" 		WHEN 'T' THEN 'TEMP' ");
		    hql.append(" 		WHEN 'D' THEN 'ไม่ผ่านการตรวจสอบ /ยกเลิกการจดทะเบียน' ");
		    hql.append("  	END AS LICENSE_STATUS_NAME, ");
		    hql.append(" 	TR.TRADER_TYPE AS TRADER_TYPE, ");
		    hql.append(" 	CASE  ");
		    hql.append(" 		WHEN TR.TRADER_TYPE = 'B'  ");
		    hql.append(" 		THEN 'ธุรกิจนำเที่ยว' ");
		    hql.append(" 		WHEN TR.TRADER_TYPE = 'G'  ");
		    hql.append(" 		THEN 'มัคคุเทศก์'  ");
		    hql.append(" 		WHEN TR.TRADER_TYPE = 'L'  ");
		    hql.append(" 		THEN 'ผู้นำเที่ยว' ");
		    hql.append(" 	END AS TRADER_TYPE_NAME, ");
		    hql.append(" 	ISNULL ( TR.TRADER_NAME , '' ) AS TRADER_NAME, ");
		    hql.append(" 	 ISNULL ( TR.TRADER_NAME_EN , '' ) AS TRADER_NAME_EN, ");
		    hql.append(" 	 CASE  ");
		    hql.append(" 	 	WHEN (P.PERSON_TYPE = 'C') THEN  ISNULL ( MF.PREFIX_NAME , '' ) + P.FIRST_NAME + ISNULL ( MO.POSTFIX_NAME , '' ) ");
		    hql.append(" 	 	WHEN (P.PERSON_TYPE = 'I') THEN  ISNULL ( MF.PREFIX_NAME , '' ) + P.FIRST_NAME +' '+ P.LAST_NAME ");
		    hql.append(" 	 END AS TRADER_OWNER_NAME, ");
		    hql.append(" 	 P.IDENTITY_NO AS IDENTITY_NO, ");
		    hql.append(" 	 TR.TRADER_CATEGORY	AS TRADER_CATEGORY, ");
		    hql.append(" 	CASE  ");
		    hql.append(" 		WHEN ((TR.TRADER_TYPE = 'B' ) AND ( TR.TRADER_CATEGORY = '100'	)) THEN 'OUTBOUND' ");
		    hql.append(" 		WHEN  ((TR.TRADER_TYPE = 'B' ) AND ( TR.TRADER_CATEGORY = '200'	)) THEN 'INBOUND' ");
		    hql.append(" 		WHEN  ((TR.TRADER_TYPE = 'B' ) AND ( TR.TRADER_CATEGORY = '300'	)) THEN 'ในประเทศ' ");
		    hql.append(" 		WHEN  ((TR.TRADER_TYPE = 'B' ) AND ( TR.TRADER_CATEGORY = '400'	)) THEN 'เฉพาะพื้นที่' ");
		    hql.append(" 		 ");
		    hql.append(" 		WHEN  ((TR.TRADER_TYPE = 'G' ) AND ( TR.TRADER_CATEGORY = '100')) THEN 'ประเภท ทั่วไป (ให้บริการนำเที่ยวแก่นักท่องเที่ยวไทยและต่างประเทศ)' ");
		    hql.append(" 		WHEN ((TR.TRADER_TYPE = 'G' ) AND ( TR.TRADER_CATEGORY = '101'	))  THEN 'ประเภท ทั่วไป (ให้บริการนำเที่ยวเฉพาะนักท่องเที่ยวไทย)' ");
		    hql.append(" 		WHEN ((TR.TRADER_TYPE = 'G' ) AND ( TR.TRADER_CATEGORY = '200'	))  THEN 'ประเภท เฉพาะพื้นที่ (ให้บริการนำเที่ยวแก่นักท่องเที่ยวไทยและต่างประเทศ)' ");
		    hql.append(" 		WHEN ((TR.TRADER_TYPE = 'G' ) AND ( TR.TRADER_CATEGORY = '201'	))  THEN 'ประเภท เฉพาะพื้นที่ (ให้บริการนำเที่ยวเฉพาะนักท่องเที่ยวไทย)' ");
		    hql.append(" 		WHEN ((TR.TRADER_TYPE = 'G' ) AND ( TR.TRADER_CATEGORY = '202'	))  THEN 'ประเภท เดินป่า (ให้บริการนำเที่ยวแก่นักท่องเที่ยวไทยและต่างประเทศในเขตป่า)' ");
		    hql.append(" 		WHEN ((TR.TRADER_TYPE = 'G' ) AND ( TR.TRADER_CATEGORY = '203'	))  THEN 'ประเภท ศิลปวัฒนธรรม (ให้บริการนำเที่ยวแก่นักท่องเที่ยวไทยและต่างประเทศด้านศิลปวัฒนธรรม)' ");
		    hql.append(" 		WHEN  ((TR.TRADER_TYPE = 'G' ) AND ( TR.TRADER_CATEGORY = '204')) THEN 'ประเภท ทางทะเล (ให้บริการนำเที่ยวแก่นักท่องเที่ยวไทยและต่างประเทศทางทะเล)' ");
		    hql.append(" 		WHEN  ((TR.TRADER_TYPE = 'G' ) AND ( TR.TRADER_CATEGORY = '205')) THEN 'ประเภท ทางทะเลชายฝั่ง (ให้บริการนำเที่ยวแก่นักท่องเที่ยวไทยและต่างประเทศทางทะเลชายฝั่ง ไม่เกิน 40 ไมล์ทะเล)' ");
		    hql.append(" 		WHEN ((TR.TRADER_TYPE = 'G' ) AND ( TR.TRADER_CATEGORY = '206'	)) THEN 'ประเภทเฉพาะ แหล่งท่องเที่ยวธรรมชาติ (ให้บริการนำเที่ยวแก่นักท่องเที่ยวไทยและต่างประเทศในแหล่งท่องเที่ยวธรรมชาติ)' ");
		    hql.append(" 		WHEN ((TR.TRADER_TYPE = 'G' ) AND ( TR.TRADER_CATEGORY = '207'	)) THEN 'ประเภทเฉพาะ วัฒนธรรมท้องถิ่น (ให้บริการนำเที่ยวแก่นักท่องเที่ยวไทยและต่างประเทศด้านวัฒนธรรมท้องถิ่น)' ");
		    hql.append(" 		WHEN ((TR.TRADER_TYPE = 'L' ) AND ( TR.TRADER_CATEGORY = '100'	)) THEN 'ตามกฎกระทรวง' ");
		    hql.append(" 		WHEN  ((TR.TRADER_TYPE = 'L' ) AND ( TR.TRADER_CATEGORY = '200'	)) THEN 'เป็นมัคคุเทศก์' ");
		    hql.append(" 		WHEN  ((TR.TRADER_TYPE = 'L' ) AND ( TR.TRADER_CATEGORY = '300'	)) THEN 'ผ่านการอบรม' ");
		    hql.append(" 	 END AS TRADER_CATEGORY_NAME, ");
		    hql.append("  	 TR.LICENSE_GUIDE_NO AS LICENSE_GUIDE_NO, ");
		    hql.append(" 	 TR.TRADER_AREA AS TRADER_AREA, ");
		    hql.append(" 	 MP.PROVINCE_NAME AS PROVINCE_NAME ");
		    
		    //หาด้วย province จะไม่ส่งให้
		    if(StringUtils.isEmpty(param.getLicenseProvinceName())){
		    	hql.append(" 	 , ");
		    	hql.append(" 	 dbo.fn_getLicenseAddress(TR.TRADER_ID) AS ADDRESS , ");
			    hql.append(" 	 dbo.fn_getCommittee(TR.PERSON_ID) AS COMMITTEE, ");
			    hql.append(" 	 dbo.fn_getRegistration(TR.LICENSE_NO) AS REGISTRATION_HIS ");
		    }else{
		    	hql.append(" 	 , ");
		    	hql.append(" 	 '' AS ADDRESS , ");
			    hql.append(" 	 '' AS COMMITTEE, ");
			    hql.append(" 	 '' AS REGISTRATION_HIS ");
		    }
		    
		    hql.append("  FROM TRADER TR ");
		    hql.append(" FULL JOIN PERSON P ");
		    hql.append(" ON(P.PERSON_ID = TR.PERSON_ID) ");
		    hql.append(" LEFT JOIN MAS_PROVINCE MP ");
		    hql.append(" ON(MP.PROVINCE_ID = TR.PROVINCE_ID) ");
		    hql.append(" LEFT JOIN MAS_PREFIX MF ");
		    hql.append(" ON(MF.PREFIX_ID = P.PREFIX_ID) ");
		    hql.append(" LEFT JOIN MAS_POSFIX MO ");
		    hql.append(" ON(MO.POSTFIX_ID = P.POSTFIX_ID) ");
		    
		    if(StringUtils.isNotEmpty(param.getLicenseProvinceName())){
		    	hql.append("  LEFT JOIN TRADER_ADDRESS TA ");
		    	hql.append("  ON(TA.TRADER_ID = TR.TRADER_ID) ");
		    	hql.append("  INNER JOIN MAS_PROVINCE TA_MP ");
		    	hql.append("  ON(TA_MP.PROVINCE_ID = TA.PROVINCE_ID) ");
		    }
		    
		    hql.append(" WHERE  ");
		    hql.append(" TR.BRANCH_TYPE IS NULL ");
		    hql.append(" AND TR.RECORD_STATUS = 'N' ");
		    
		    if(StringUtils.isNotEmpty(param.getLicenseType())){
		    	hql.append(" AND TR.TRADER_TYPE = :TRADER_TYPE ");
		    	params.put("TRADER_TYPE", param.getLicenseType());
		    }
		    
		    if(StringUtils.isNotEmpty(param.getLicenseNo())){
		    	hql.append(" AND TR.LICENSE_NO = :LICENSE_NO ");
		    	params.put("LICENSE_NO", param.getLicenseNo());
		    }
		    if(StringUtils.isNotEmpty(param.getLicenseIdentityNo())){
		    	hql.append("  AND P.IDENTITY_NO = :IDENTITY_NO ");
		    	params.put("IDENTITY_NO", param.getLicenseIdentityNo());
		    }
		    if(StringUtils.isNotEmpty(param.getLicenseProvinceName())){
		    	hql.append(" AND TA_MP.PROVINCE_NAME LIKE :PROVINCE_NAME ");
		    	hql.append(" AND TA.ADDRESS_TYPE = 'O' ");
		    	params.put("PROVINCE_NAME", "%"+param.getLicenseProvinceName()+"%");
		    }
		    
		    hql.append(" ORDER BY TR.LICENSE_NO; ");		 
		    
		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
//		    sqlQuery.setFirstResult(50);
//		    sqlQuery.setMaxResults(10);
		    
		    
		    sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING); //0
		    sqlQuery.addScalar("LICENSE_STATUS", Hibernate.STRING); //1
		    sqlQuery.addScalar("TRADER_TYPE", Hibernate.STRING); //2
		    sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING); //3
		    sqlQuery.addScalar("TRADER_NAME_EN", Hibernate.STRING); //4
		    sqlQuery.addScalar("TRADER_OWNER_NAME", Hibernate.STRING); //5
		    sqlQuery.addScalar("IDENTITY_NO", Hibernate.STRING); //6
		    sqlQuery.addScalar("TRADER_CATEGORY", Hibernate.STRING); //7
		    sqlQuery.addScalar("LICENSE_GUIDE_NO", Hibernate.STRING); //8
		    sqlQuery.addScalar("TRADER_AREA", Hibernate.STRING); //9
		    sqlQuery.addScalar("PROVINCE_NAME", Hibernate.STRING); //10
		    sqlQuery.addScalar("ADDRESS", Hibernate.STRING); //11
		    sqlQuery.addScalar("COMMITTEE", Hibernate.STRING); //12
		    sqlQuery.addScalar("REGISTRATION_HIS", Hibernate.STRING); //13
		    
		    sqlQuery.setProperties(params);
		    List<Object[]>  result = sqlQuery.list();
		    
		    return result;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Object[]> findComplaint(String licenseNo) throws Exception{
		   Map<String,Object> params = new HashMap<String,Object>();
		    
		    StringBuilder hql = new StringBuilder();
		    
		    hql.append(" SELECT   ");
		    hql.append(" CM.LICENSE_NO AS LICENSE_NO ");
		    hql.append(" , CP.COMPLAINT_DESC AS COMPLAINT_DESC  ");
		    hql.append(" ,  CONVERT(NVARCHAR(MAX), CP.COMPLAINT_DATE, 105) AS COMPLAINT_DATE  ");
		    hql.append(" ,dbo.fn_getComplaintProgressDetail(CM.COMPLAINT_ID) AS PROGRESS_DESC  ");
		    hql.append("  ,dbo.fn_getComplaintProgressDate(CM.COMPLAINT_ID) AS PROGRESS_DATE    ");
		    hql.append(" FROM COMPLAINT_MAP_TRADER CM  ");
		    hql.append(" INNER JOIN COMPLAINT_LICENSE CP  ");
		    hql.append(" ON(CP.COMPLAINT_ID = CM.COMPLAINT_ID)  ");
		    hql.append(" WHERE CM.LICENSE_NO = :LICENSE_NO ");
		    params.put("LICENSE_NO", licenseNo);
		    
		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
	    
		    
		    sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING); //0
		    sqlQuery.addScalar("COMPLAINT_DESC", Hibernate.STRING); //1
		    sqlQuery.addScalar("COMPLAINT_DATE", Hibernate.STRING); //2
		    sqlQuery.addScalar("PROGRESS_DESC", Hibernate.STRING); //3
		    sqlQuery.addScalar("PROGRESS_DATE", Hibernate.STRING); //4
		    
		    sqlQuery.setProperties(params);
		    List<Object[]>  result = sqlQuery.list();
		    
		    return result;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Object[]> findSuspension(String licenseNo) throws Exception{
		   Map<String,Object> params = new HashMap<String,Object>();
		    
		    StringBuilder hql = new StringBuilder();
		    
			hql.append(" SELECT ");
			hql.append(" ST.LICENSE_NO AS LICENSE_NO ");
			hql.append(" ,ACT.ACT_DESC AS  ACT_DESC");
			hql.append(" ,L.SUSPEND_STATUS AS SUSPEND_STATUS");
			hql.append(" ,CONVERT(NVARCHAR(MAX), L.SUSPEND_FROM, 105) AS SUSPEND_FROM ");
			hql.append(" ,CONVERT(NVARCHAR(MAX), L.SUSPEND_TO, 105) AS SUSPEND_TO");
			hql.append("  FROM SUSPENSION_LICENSE L ");
			hql.append(" INNER JOIN SUSPENSION_MAP_TRADER ST ");
			hql.append(" ON(ST.SUSPEND_ID = L.SUSPEND_ID) ");
			hql.append(" INNER JOIN SUSPENSION_DETAIL LD ");
			hql.append(" ON(LD.SUSPEND_ID = L.SUSPEND_ID) ");
			hql.append(" INNER JOIN MAS_ACT ACT ");
			hql.append(" ON(ACT.MAS_ACT_ID = LD.ACT) ");
			hql.append(" WHERE ST.LICENSE_NO = :LICENSE_NO ");
			hql.append(" AND L.SUSPEND_STATUS IN ('N', 'S') ");
			hql.append(" ORDER BY L.SUSPEND_FROM ");
		    params.put("LICENSE_NO", licenseNo);
		    
		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
	    
		    
		    sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING); //0
		    sqlQuery.addScalar("ACT_DESC", Hibernate.STRING); //1
		    sqlQuery.addScalar("SUSPEND_STATUS", Hibernate.STRING); //2
		    sqlQuery.addScalar("SUSPEND_FROM", Hibernate.STRING); //3
		    sqlQuery.addScalar("SUSPEND_TO", Hibernate.STRING); //4
		    
		    sqlQuery.setProperties(params);
		    List<Object[]>  result = sqlQuery.list();
		    
		    return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Trader> findTraderBranch(Trader trader)
	{
		ArrayList params = new ArrayList();
		
		StringBuilder hql = new StringBuilder();
		hql.append(" select tr from  Trader as tr ");
		hql.append(" where tr.traderByBranchParentId = ? ");
		params.add(trader);
		
		List<Trader> list = (List<Trader>) this.getHibernateTemplate().find(hql.toString(), params.toArray());

		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<Trader> findLicenseGuideAll(String identityNo,
			String licenseNo,
			String firstName, 
			String lastName,
			String traderRecordStatus,
			String personRecordStatus
			)
			throws Exception {
		
		ArrayList params = new ArrayList();
		
		StringBuilder hql = new StringBuilder();
		hql.append(" select tr from  Trader as tr ");
		hql.append(" inner join tr.person pe ");
		
		hql.append(" where tr.traderType = 'G' and tr.recordStatus = ? ");
		params.add(traderRecordStatus);
		
		hql.append(" and pe.recordStatus = ? ");
		params.add(personRecordStatus);

		if ((identityNo != null) && (!identityNo.equals(""))) {
			hql.append(" and pe.identityNo = ? ");
			params.add(identityNo);
		}
		
		if ((licenseNo != null) && (!licenseNo.equals(""))) {
			hql.append(" and tr.licenseNo = ? ");
			params.add(licenseNo);
		}

		if ((firstName != null) && (!firstName.equals(""))) {
			hql.append(" and pe.firstName like ? ");
			params.add("%"+firstName+"%");
		}
	
		if ((lastName != null) && (!lastName.equals(""))) {
			hql.append(" and pe.lastName like ? ");
			params.add("%"+lastName+"%");
		}
	

		List<Trader> list = (List<Trader>) this
				.getHibernateTemplate().find(hql.toString(), params.toArray());

		return list;
	}
	
	public List<Trader> findAllTrader(RegistrationDTO param)
	{
		ArrayList params = new ArrayList();
		
		StringBuilder hql = new StringBuilder();
		hql.append(" select tr from  Trader as tr ");
		hql.append(" inner join tr.person pe ");
		hql.append(" where tr.branchType is null ");
		
		if((param.getTraderName() != null) && (!param.getTraderName().isEmpty()))
		{
			hql.append(" and tr.traderName like  ? ");
			params.add("%"+param.getTraderName()+"%");
		}
		if((param.getTraderNameEn() != null) && (!param.getTraderNameEn().isEmpty()))
		{
			hql.append(" and tr.traderNameEn like  ? ");
			params.add("%"+param.getTraderName()+"%");
		}

		if ((param.getLicenseNo() != null) && (!param.getLicenseNo().equals(""))) {
			hql.append(" and tr.licenseNo = ? ");
			params.add(param.getLicenseNo());
		}
		
		if ((param.getTraderType() != null) && (!param.getTraderType().equals(""))) {
			hql.append(" and tr.traderType = ? ");
			params.add(param.getTraderType());
		}
		
		if ((param.getRecordStatus() != null) && (!param.getRecordStatus().equals(""))) {
			hql.append(" and tr.recordStatus = ? ");
			params.add(param.getRecordStatus());
		}
		
		if (param.getOrgId() > 0) {
			hql.append(" and tr.organization.orgId = ? ");
			params.add(param.getOrgId());
		}
		
		if((param.getIdentityNo() != null) && (!param.getIdentityNo().isEmpty()))
		{
			hql.append(" and pe.identityNo = ? ");
			params.add(param.getIdentityNo());
		}
		
		if((param.getFirstName() != null) && (!param.getFirstName().isEmpty()))
		{
			hql.append(" and pe.firstName like  ? ");
			params.add("%"+param.getFirstName()+"%");
		}
		if((param.getLastName() != null) && (!param.getLastName().isEmpty()))
		{
			hql.append(" and pe.lastName like  ? ");
			params.add("%"+param.getLastName()+"%");
		}
		
		
		List<Trader> list = (List<Trader>) this.getHibernateTemplate().find(hql.toString(), params.toArray());

		return list;
	}
	
	public List<Trader> findTraderByLicenseNo(String licenseNo, String traderType, String recordStatus, long orgId)
	{
		ArrayList params = new ArrayList();
		
		StringBuilder hql = new StringBuilder();
		hql.append(" from  Trader as tr ");
		hql.append(" where tr.branchType is null ");

		if ((licenseNo != null) && (!licenseNo.equals(""))) {
			hql.append(" and tr.licenseNo = ? ");
			params.add(licenseNo);
		}
		
		if ((traderType != null) && (!traderType.equals(""))) {
			hql.append(" and tr.traderType = ? ");
			params.add(traderType);
		}
		
		if ((recordStatus != null) && (!recordStatus.equals(""))) {
			hql.append(" and tr.recordStatus = ? ");
			params.add(recordStatus);
		}
		
		if (orgId > 0) {
			hql.append(" and tr.organization.orgId = ? ");
			params.add(orgId);
		}
		
		List<Trader> list = (List<Trader>) this.getHibernateTemplate().find(hql.toString(), params.toArray());

		return list;
	}
	
	public List<Trader> findLeaderByGuideLicenseNo(String guidelicenseNo, String traderType, String recordStatus, long orgId)
	{
		ArrayList params = new ArrayList();
		
		StringBuilder hql = new StringBuilder();
		hql.append(" from  Trader as tr ");
		hql.append(" where tr.branchType is null ");

		
		
		if ((traderType != null) && (!traderType.equals(""))) {
			hql.append(" and tr.traderType = ? ");
			params.add(traderType);
		}
		
		if ((recordStatus != null) && (!recordStatus.equals(""))) {
			hql.append(" and tr.recordStatus = ? ");
			params.add(recordStatus);
		}
		
		if (orgId > 0) {
			hql.append(" and tr.organization.orgId = ? ");
			params.add(orgId);
		}
		if ((guidelicenseNo != null) && (!guidelicenseNo.equals(""))) {
			hql.append(" and tr.licenseGuideNo = ? ");
			params.add(guidelicenseNo);
		}
		
		
		List<Trader> list = (List<Trader>) this.getHibernateTemplate().find(hql.toString(), params.toArray());

		return list;
	}
	
	public List<Trader> findTraderBetweenLicenseNo(String licenseNoFrom)
	{
		ArrayList params = new ArrayList();
		System.out.println("ON");
		StringBuilder hql = new StringBuilder();
		hql.append(" from  Trader as tr ");
		hql.append(" where tr.branchType is null ");

		if ((licenseNoFrom != null) && (!licenseNoFrom.equals(""))) {
			/*if ((licenseNoTo != null) && (!licenseNoTo.equals("")))
			{	
				hql.append(" and tr.licenseNo between ? ");
				params.add(licenseNoFrom);
				hql.append(" and ? ");
				params.add(licenseNoTo);
			}else
			{*/
				hql.append(" and tr.licenseNo = ? ");
				params.add(licenseNoFrom);
			//}
		}
		
		/*if ((licenseNoTo != null) && (!licenseNoTo.equals(""))) {
			if ((licenseNoFrom != null) && (!licenseNoFrom.equals("")))
			{	
				hql.append(" and tr.licenseNo between ? ");
				params.add(licenseNoFrom);
				hql.append(" and ? ");
				params.add(licenseNoTo);
			}else
			{
				hql.append(" and tr.licenseNo = ? ");
				params.add(licenseNoTo);
			}
		}*/
		System.out.println("OUT");
		/*if ((traderType != null) && (!traderType.equals(""))) {
			hql.append(" and tr.traderType = ? ");
			params.add(traderType);
		}
		
		if ((recordStatus != null) && (!recordStatus.equals(""))) {
			hql.append(" and tr.recordStatus = ? ");
			params.add(recordStatus);
		}*/
		
		/*if (orgId > 0) {
			hql.append(" and tr.organization.orgId = ? ");
			params.add(orgId);
		}*/
		
		List<Trader> list = (List<Trader>) this.getHibernateTemplate().find(hql.toString(), params.toArray());

		return list;
	}
	
	public List<Object[]> findTraderBetweenLicenseNo(RegistrationDTO dto, User user)throws Exception
	{
		Map<String,Object> parames = new HashMap<String,Object>();
	    
	    StringBuilder hql = new StringBuilder();
	    
	    hql.append(" SELECT TR.TRADER_ID AS TRADER_ID, ");
	    hql.append(" PE.PERSON_ID AS PERSON_ID, ");
	    hql.append(" TR.TRADER_NAME AS TRADER_NAME, ");
	    hql.append(" TR.LICENSE_NO AS LICENSE_NO ");

	    hql.append(" FROM TRADER TR ");
	 
	    hql.append(" INNER JOIN PERSON PE ");
	    hql.append(" ON(PE.PERSON_ID = TR.PERSON_ID) ");

	    hql.append(" INNER JOIN TRADER_ADDRESS TRA ");
	    hql.append(" ON(TRA.TRADER_ID = TR.TRADER_ID) ");


	    hql.append(" WHERE TRA.ADDRESS_TYPE = 'S' ");
	    hql.append(" and TR.BRANCH_TYPE IS NULL ");
	    hql.append(" and TR.RECORD_STATUS = 'N' ");
	    
	    
	    if((null != dto.getLicenseNo() &&(!dto.getLicenseNo().equals(""))))
	    {
	    	hql.append(" and TR.LICENSE_NO = :LICENSE_NO ");
    		parames.put("LICENSE_NO",dto.getLicenseNo());
	    }

	    if((null != dto.getLicenseNoFrom() &&(!dto.getLicenseNoFrom().equals(""))))
	    {
	    	if((null != dto.getLicenseNoTo() &&(!dto.getLicenseNoTo().equals(""))))
	    	{
	    		hql.append(" and TR.LICENSE_NO BETWEEN :LICENSE_NO ");
	    		parames.put("LICENSE_NO",dto.getLicenseNoFrom());
	    		hql.append(" and :LICENSE_NO1 ");
	    		parames.put("LICENSE_NO1",dto.getLicenseNoTo());
	    	}else
	    	{
	    		hql.append(" and TR.LICENSE_NO = :LICENSE_NO");
	    		parames.put("LICENSE_NO",dto.getLicenseNoFrom());
	    	}
	    	
	    }else if((null != dto.getLicenseNoTo() &&(!dto.getLicenseNoTo().equals(""))))
	    {
	    	if((null != dto.getLicenseNoFrom() &&(!dto.getLicenseNoFrom().equals(""))))
	    	{
	    		hql.append(" and TR.LICENSE_NO BETWEEN :LICENSE_NO ");
	    		parames.put("LICENSE_NO",dto.getLicenseNoFrom());
	    		hql.append(" and :LICENSE_NO1 ");
	    		parames.put("LICENSE_NO1",dto.getLicenseNoTo());
	    	}else
	    	{
	    		hql.append(" and TR.LICENSE_NO = :LICENSE_NO");
	    		parames.put("LICENSE_NO",dto.getLicenseNoTo());
	    	}
	    }
		
	    if((null != dto.getTraderType() && !dto.getTraderType().equals("")))
	    {
	    	hql.append(" and TR.TRADER_TYPE = :TRADER_TYPE");
	    	parames.put("TRADER_TYPE",dto.getTraderType());
	    }
	    
	    hql.append(" ORDER BY TR.LICENSE_NO");
	    
	    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());

	    
		sqlQuery.addScalar("TRADER_ID", Hibernate.LONG); //1
		sqlQuery.addScalar("PERSON_ID", Hibernate.LONG); //2
		sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING); //3
		sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING); //4
	    
	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
  
	    return result;
	}
	public List<Trader> findAllByTraderType(RegistrationDTO param)
	{
		ArrayList params = new ArrayList();
		
		StringBuilder hql = new StringBuilder();
		hql.append(" select tr from  Trader as tr ");
		hql.append(" inner join tr.person pe ");
		hql.append(" where tr.recordStatus = 'N' ");
		hql.append(" and tr.traderType in ('B','G','L') ");
		hql.append(" and getdate() <= tr.expireDate  ");
		
		List<Trader> list = (List<Trader>) this.getHibernateTemplate().find(hql.toString(), params.toArray());

		return list;
	}
	
	public List<Trader> findAllTraderName(RegistrationDTO param)
	{
		ArrayList params = new ArrayList();
		
		StringBuilder hql = new StringBuilder();
		hql.append(" select tr from  Trader as tr ");
		hql.append(" inner join tr.person pe ");
		hql.append(" where tr.branchType is null ");
		hql.append(" and tr.recordStatus in ('N','H','T') ");
		
		if((param.getTraderName() != null) && (!param.getTraderName().isEmpty()))
		{
			hql.append(" and tr.traderName like  ? ");
			params.add("%"+param.getTraderName()+"%");
		}
		if((param.getTraderNameEn() != null) && (!param.getTraderNameEn().isEmpty()))
		{
			hql.append(" and tr.traderNameEn like  ? ");
			params.add("%"+param.getTraderName()+"%");
		}

		if ((param.getLicenseNo() != null) && (!param.getLicenseNo().equals(""))) {
			hql.append(" and tr.licenseNo = ? ");
			params.add(param.getLicenseNo());
		}
		
		if ((param.getTraderType() != null) && (!param.getTraderType().equals(""))) {
			hql.append(" and tr.traderType = ? ");
			params.add(param.getTraderType());
		}

		
		List<Trader> list = (List<Trader>) this.getHibernateTemplate().find(hql.toString(), params.toArray());

		return list;
	}
	
	public List<Trader> findAllTraderCheckName(RegistrationDTO param)
	{
		ArrayList params = new ArrayList();
		
		StringBuilder hql = new StringBuilder();
		hql.append(" select tr from  Trader as tr ");
		hql.append(" inner join tr.person pe ");
		hql.append(" where tr.branchType is null ");
		hql.append(" and tr.recordStatus in ('N','H','T') ");
		
		/*if((param.getTraderName() != null) && (!param.getTraderName().isEmpty()))
		{
			hql.append(" and tr.traderName =  ? ");
			params.add(param.getTraderName());
		}
		if((param.getTraderNameEn() != null) && (!param.getTraderNameEn().isEmpty()))
		{
			hql.append(" and tr.traderNameEn like  ? ");
			params.add("%"+param.getTraderName()+"%");
		}*/
		
		if ((param.getTraderType() != null) && (!param.getTraderType().equals(""))) {
			hql.append(" and tr.traderType = ? ");
			params.add(param.getTraderType());
		}
		
		
		List<Trader> list = (List<Trader>) this.getHibernateTemplate().find(hql.toString(), params.toArray());

		return list;
	}
	
	
	public List<Object[]> findPersonByTraderId(RegistrationDTO dto)throws Exception
	{
		Map<String,Object> parames = new HashMap<String,Object>();
	    
	    StringBuilder hql = new StringBuilder();
	    
	    hql.append(" SELECT TR.TRADER_ID AS TRADER_ID, ");
	    hql.append(" PE.PERSON_ID AS PERSON_ID, ");
	    hql.append(" PE.PERSON_TYPE AS PERSON_TYPE, ");
	    hql.append(" PRE.PREFIX_NAME AS PREFIX_NAME, ");
	    hql.append(" PO.POSTFIX_NAME AS POSTFIX_NAME, ");
	    hql.append(" PE.FIRST_NAME AS FIRST_NAME, ");
	    hql.append(" PE.LAST_NAME AS LAST_NAME ");

	    hql.append(" FROM TRADER TR ");
	 
	    hql.append(" INNER JOIN PERSON PE ");
	    hql.append(" ON(PE.PERSON_ID = TR.PERSON_ID) ");

	    hql.append(" LEFT JOIN	MAS_PREFIX PRE ");
	    hql.append("		ON ");
	    hql.append("		PRE.PREFIX_ID = PE.PREFIX_ID ");
	    hql.append("		LEFT JOIN ");
	    hql.append("		MAS_POSFIX PO ");
	    hql.append("		ON ");
	    hql.append("		PO.POSTFIX_ID = PE.POSTFIX_ID ");
	    
	    hql.append(" WHERE TR.RECORD_STATUS = 'N' ");
	    hql.append(" AND PE.RECORD_STATUS = 'N' ");

		
	    if(dto.getPersonId() > 0)
	    {
	    	hql.append(" AND PE.PERSON_ID = :PERSON_ID");
	    	parames.put("PERSON_ID",dto.getPersonId());
	    }
	    
	    
	    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());

	    
		sqlQuery.addScalar("TRADER_ID", Hibernate.LONG); //1
		sqlQuery.addScalar("PERSON_ID", Hibernate.LONG); //2
		sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING); //3
		sqlQuery.addScalar("PREFIX_NAME", Hibernate.STRING); //4
		sqlQuery.addScalar("POSTFIX_NAME", Hibernate.STRING); //5
		sqlQuery.addScalar("FIRST_NAME", Hibernate.STRING); //6
		sqlQuery.addScalar("LAST_NAME", Hibernate.STRING); //7
	    
	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
  
	    return result;
	}
	@Deprecated
	public List<Object[]> findAllLicenseGuide()throws Exception
	{
		Map<String,Object> parames = new HashMap<String,Object>();
	    
	    StringBuilder hql = new StringBuilder();
	    
	    hql.append(" SELECT TR.LICENSE_NO AS LICENSE_NO, COUNT(TR.LICENSE_NO) AS C_LICENSE_NO ");
	    hql.append(" FROM TRADER TR ");
	    hql.append(" WHERE TR.TRADER_TYPE = 'G' ");
	    hql.append(" AND TR.BRANCH_TYPE IS NULL ");
	    hql.append(" AND TR.CREATE_USER = 'TranG' ");
	    hql.append(" AND TR.LICENSE_NO IS NOT NULL ");
//	    hql.append(" AND TR.ORG_ID = 2 ");
	    hql.append(" GROUP BY TR.LICENSE_NO ");
	    hql.append(" ORDER  BY TR.LICENSE_NO ");
	    
	    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());

		sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING);
		sqlQuery.addScalar("C_LICENSE_NO", Hibernate.INTEGER);

		
	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
  
	    return result;
	}
	
	@Deprecated
	public List<Trader> findLicense(String licenseNo, String traderType)
	{
		ArrayList params = new ArrayList();
		
		StringBuilder hql = new StringBuilder();
		hql.append(" from  Trader as tr ");
		hql.append(" where tr.branchType is null ");
		hql.append(" and tr.licenseNo is not null ");
		hql.append(" and tr.traderType = ? ");
		
		params.add(traderType);
		
		if(TraderType.TOUR_COMPANIES.getStatus().equals(traderType))
		{
			hql.append(" and tr.createUser = 'Tran' ");
		}
		else
		{
			hql.append(" and tr.createUser = 'TranG' ");
		}

		if ((licenseNo != null) && (!licenseNo.equals(""))) {
			hql.append(" and tr.licenseNo = ? ");
			params.add(licenseNo);
		}
		hql.append(" order by tr.effectiveDate asc ");
	
		
		List<Trader> list = (List<Trader>) this.getHibernateTemplate().find(hql.toString(), params.toArray());

		return list;
	}
	@Deprecated
	public List<Object[]> findLicenseNoAll()throws Exception
	{
		Map<String,Object> parames = new HashMap<String,Object>();
	    
	    StringBuilder hql = new StringBuilder();
	    
	    hql.append(" SELECT TR.LICENSE_NO AS LICENSE_NO, COUNT(TR.LICENSE_NO) AS C_LICENSE_NO ");
	    hql.append(" FROM TRADER TR ");
	    hql.append(" WHERE TR.TRADER_TYPE = 'B' ");
	    hql.append(" AND TR.BRANCH_TYPE IS NULL ");
	    hql.append(" AND TR.CREATE_USER = 'Tran' ");
	    hql.append(" AND TR.LICENSE_NO IS NOT NULL ");
//	    hql.append(" AND TR.ORG_ID = 2 ");
	    hql.append(" GROUP BY TR.LICENSE_NO ");
	    hql.append(" ORDER  BY TR.LICENSE_NO ");
	    
	    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());

		sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING);
		sqlQuery.addScalar("C_LICENSE_NO", Hibernate.INTEGER);

		
	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
  
	    return result;
	}
	
	public List<Object[]> findWarningPayFeesByMail() throws Exception
	{
		Map<String,Object> parames = new HashMap<String,Object>();
	    
	    StringBuilder hql = new StringBuilder();
	    
	    hql.append(" SELECT  ");
	    hql.append(" 	TD.TRADER_ID, ");
	    hql.append(" 	TD.LICENSE_NO, ");
	    hql.append(" 	TD.EXPIRE_DATE, ");
	    hql.append(" 	DF.DIFF, ");
	    hql.append(" 	TA.EMAIL ");
	    hql.append("  FROM TRADER TD ");
	    hql.append(" INNER JOIN ( ");
	    hql.append(" 	SELECT  ");
	    hql.append(" 		TR.TRADER_ID, ");
	    hql.append(" 		TR.LICENSE_NO,  ");
	    hql.append(" 		TR.EXPIRE_DATE,    ");
	    hql.append(" 		  DATEDIFF(day, CONVERT(DATE,GETDATE())   ,  CONVERT(DATE,TR.EXPIRE_DATE)  ) AS DIFF ");
	    hql.append(" 	FROM TRADER TR ");
	    hql.append(" 	WHERE TR.BRANCH_TYPE IS NULL ");
	    hql.append(" 	AND TR.RECORD_STATUS = 'N' ");
	    hql.append(" 	AND TR.LICENSE_STATUS = 'N' ");
	    hql.append(" 	AND TR.TRADER_TYPE = 'B' ");
	    hql.append(" ) DF ");
	    hql.append(" ON(DF.TRADER_ID = TD.TRADER_ID ) ");
	    hql.append(" INNER JOIN TRADER_ADDRESS TA ");
	    hql.append(" ON(TA.TRADER_ID = TD.TRADER_ID) ");
	    hql.append("   ");
	    hql.append(" WHERE TD.BRANCH_TYPE IS NULL ");
	    hql.append(" 	AND TD.RECORD_STATUS = 'N' ");
	    hql.append(" 	AND TD.LICENSE_STATUS = 'N' ");
	    hql.append(" 	AND TD.TRADER_TYPE = 'B' ");
	    hql.append("  	AND ( (DF.DIFF = 120) OR (DF.DIFF = 60) OR (DF.DIFF = 30) ) ");
	    hql.append("  	AND TA.ADDRESS_TYPE = 'O' ");
//	    hql.append("  	AND TD.LICENSE_NO = '11/03546' ");
		
	    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());

		sqlQuery.addScalar("TRADER_ID", Hibernate.LONG); // 0
		sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING);// 1
		sqlQuery.addScalar("EXPIRE_DATE", Hibernate.DATE);// 2
		sqlQuery.addScalar("DIFF", Hibernate.INTEGER);// 3
		sqlQuery.addScalar("EMAIL", Hibernate.STRING);// 4

		
//	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
	    
	    return result;
	}
	
	public List<Object[]> findTourExpire() throws Exception
	{
		Map<String,Object> parames = new HashMap<String,Object>();
	    
	    StringBuilder hql = new StringBuilder();
	    
	    hql.append(" SELECT  ");
	    hql.append(" 	TD.TRADER_ID, ");
	    hql.append(" 	TD.LICENSE_NO, ");
	    hql.append(" 	TD.EXPIRE_DATE, ");
	    hql.append(" 	DF.DIFF, ");
	    hql.append(" 	TD.ORG_ID ");
	    hql.append("  FROM TRADER TD ");
	    hql.append(" INNER JOIN ( ");
	    hql.append(" 	SELECT  ");
	    hql.append(" 		TR.TRADER_ID, ");
	    hql.append(" 		TR.LICENSE_NO,  ");
	    hql.append(" 		TR.EXPIRE_DATE,    ");
	    hql.append(" 		  DATEDIFF(day, CONVERT(DATE,GETDATE())   ,  CONVERT(DATE,TR.EXPIRE_DATE)  ) AS DIFF ");
	    hql.append(" 	FROM TRADER TR ");
	    hql.append(" 	WHERE TR.BRANCH_TYPE IS NULL ");
	    hql.append(" 	AND TR.RECORD_STATUS = 'N' ");
	    hql.append(" 	AND TR.LICENSE_STATUS = 'N' ");
	    hql.append(" 	AND TR.TRADER_TYPE = 'B' ");
	    hql.append(" ) DF ");
	    hql.append(" ON(DF.TRADER_ID = TD.TRADER_ID ) ");
	    hql.append("   ");
	    hql.append(" WHERE TD.BRANCH_TYPE IS NULL ");
	    hql.append(" 	AND TD.RECORD_STATUS = 'N' ");
	    hql.append(" 	AND TD.LICENSE_STATUS = 'N' ");
	    hql.append(" 	AND TD.TRADER_TYPE = 'B' ");
	    hql.append("  	AND DF.DIFF = -91 ");
	    
	    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());

		sqlQuery.addScalar("TRADER_ID", Hibernate.LONG); // 0
		sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING);// 1
		sqlQuery.addScalar("EXPIRE_DATE", Hibernate.DATE);// 2
		sqlQuery.addScalar("DIFF", Hibernate.INTEGER);// 3
		sqlQuery.addScalar("ORG_ID", Hibernate.LONG);// 4

		
//	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
	    
	    return result;
	}
	
	public List<Object[]> findGuideExpire() throws Exception
	{
		Map<String,Object> parames = new HashMap<String,Object>();
	    
	    StringBuilder hql = new StringBuilder();
	    
	    hql.append(" SELECT  ");
	    hql.append(" 	TD.TRADER_ID, ");
	    hql.append(" 	TD.LICENSE_NO, ");
	    hql.append(" 	TD.EXPIRE_DATE, ");
	    hql.append(" 	DF.DIFF, ");
	    hql.append(" 	TD.ORG_ID ");
	    hql.append("  FROM TRADER TD ");
	    hql.append(" INNER JOIN ( ");
	    hql.append(" 	SELECT  ");
	    hql.append(" 		TR.TRADER_ID, ");
	    hql.append(" 		TR.LICENSE_NO,  ");
	    hql.append(" 		TR.EXPIRE_DATE,    ");
	    hql.append(" 		  DATEDIFF(day, CONVERT(DATE,GETDATE())   ,  CONVERT(DATE,TR.EXPIRE_DATE)  ) AS DIFF ");
	    hql.append(" 	FROM TRADER TR ");
	    hql.append(" 	WHERE TR.BRANCH_TYPE IS NULL ");
	    hql.append(" 	AND TR.RECORD_STATUS = 'N' ");
	    hql.append(" 	AND TR.LICENSE_STATUS = 'N' ");
	    hql.append(" 	AND TR.TRADER_TYPE = 'G' ");
	    hql.append(" ) DF ");
	    hql.append(" ON(DF.TRADER_ID = TD.TRADER_ID ) ");
	    hql.append("   ");
	    hql.append(" WHERE TD.BRANCH_TYPE IS NULL ");
	    hql.append(" 	AND TD.RECORD_STATUS = 'N' ");
	    hql.append(" 	AND TD.LICENSE_STATUS = 'N' ");
	    hql.append(" 	AND TD.TRADER_TYPE = 'G' ");
	    hql.append("  	AND DF.DIFF = -1 ");
	    
	    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());

		sqlQuery.addScalar("TRADER_ID", Hibernate.LONG); // 0
		sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING);// 1
		sqlQuery.addScalar("EXPIRE_DATE", Hibernate.DATE);// 2
		sqlQuery.addScalar("DIFF", Hibernate.INTEGER);// 3
		sqlQuery.addScalar("ORG_ID", Hibernate.LONG);// 4


	    List<Object[]>  result = sqlQuery.list();
	    
	    return result;
	}
	
	public List<Object[]> findGuideAllExpire() throws Exception
	{
		Map<String,Object> parames = new HashMap<String,Object>();
	    
	    StringBuilder hql = new StringBuilder();
	    
	    hql.append(" SELECT  ");
	    hql.append(" 	TD.TRADER_ID, ");
	    hql.append(" 	TD.LICENSE_NO, ");
	    hql.append(" 	TD.EXPIRE_DATE, ");
	    hql.append(" 	DF.DIFF, ");
	    hql.append(" 	TD.ORG_ID ");
	    hql.append("  FROM TRADER TD ");
	    hql.append(" INNER JOIN ( ");
	    hql.append(" 	SELECT  ");
	    hql.append(" 		TR.TRADER_ID, ");
	    hql.append(" 		TR.LICENSE_NO,  ");
	    hql.append(" 		TR.EXPIRE_DATE,    ");
	    hql.append(" 		  DATEDIFF(day, CONVERT(DATE,GETDATE())   ,  CONVERT(DATE,TR.EXPIRE_DATE)  ) AS DIFF ");
	    hql.append(" 	FROM TRADER TR ");
	    hql.append(" 	WHERE TR.BRANCH_TYPE IS NULL ");
	    hql.append(" 	AND TR.RECORD_STATUS = 'N' ");
	    hql.append(" 	AND TR.LICENSE_STATUS = 'N' ");
	    hql.append(" 	AND TR.TRADER_TYPE = 'G' ");
	    hql.append(" ) DF ");
	    hql.append(" ON(DF.TRADER_ID = TD.TRADER_ID ) ");
	    hql.append("   ");
	    hql.append(" WHERE TD.BRANCH_TYPE IS NULL ");
	    hql.append(" 	AND TD.RECORD_STATUS = 'N' ");
	    hql.append(" 	AND TD.LICENSE_STATUS = 'N' ");
	    hql.append(" 	AND TD.TRADER_TYPE = 'G' ");
	    hql.append("  	AND DF.DIFF < 0 ");
	    
	    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());

		sqlQuery.addScalar("TRADER_ID", Hibernate.LONG); // 0
		sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING);// 1
		sqlQuery.addScalar("EXPIRE_DATE", Hibernate.DATE);// 2
		sqlQuery.addScalar("DIFF", Hibernate.INTEGER);// 3
		sqlQuery.addScalar("ORG_ID", Hibernate.LONG);// 4


	    List<Object[]>  result = sqlQuery.list();
	    
	    return result;
	}

}
