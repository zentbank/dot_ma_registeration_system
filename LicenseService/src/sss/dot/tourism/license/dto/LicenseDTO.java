package sss.dot.tourism.license.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;  
import javax.xml.bind.annotation.XmlElement;  
import javax.xml.bind.annotation.XmlRootElement;  

import org.hibernate.Hibernate;

@XmlRootElement(name="license")  
public class LicenseDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1368798296445194245L;
	private String licenseNo;
	private String licenseStatus;
	private String licenseType;
	private String licenseName;
	private String licenseNameEn;
	private String licenseOwnerName;
	private String licenseIdentityNo;
	private String licenseCategory;
	private String licenseGuideNo;
	private String licenseArea;
	private String licenseProvinceName;
	private String licenseAdress;
	private String licenseComittee;
	private String licenseRegistration;
	
	//complaint
	private String  complaintDesc;
	private String  complaintDate;
	private String  progressDesc;
	private String  progressDate;
	
	
	//suspension
	private String suspendDesc;
	private String suspendStatus;
	private String suspendFrom;
	private String suspendTo;
	 
	
	
	@XmlAttribute(name="licenseNo")  
	public String getLicenseNo() {
		return licenseNo;
	}
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	@XmlElement(name="licenseStatus")  
	public String getLicenseStatus() {
		return licenseStatus;
	}
	public void setLicenseStatus(String licenseStatus) {
		this.licenseStatus = licenseStatus;
	}
	@XmlElement(name="licenseType") 
	public String getLicenseType() {
		return licenseType;
	}
	public void setLicenseType(String licenseType) {
		this.licenseType = licenseType;
	}
	@XmlElement(name="licenseName") 
	public String getLicenseName() {
		return licenseName;
	}
	public void setLicenseName(String licenseName) {
		this.licenseName = licenseName;
	}
	@XmlElement(name="licenseNameEn") 
	public String getLicenseNameEn() {
		return licenseNameEn;
	}
	public void setLicenseNameEn(String licenseNameEn) {
		this.licenseNameEn = licenseNameEn;
	}
	@XmlElement(name="licenseOwnerName") 
	public String getLicenseOwnerName() {
		return licenseOwnerName;
	}
	public void setLicenseOwnerName(String licenseOwnerName) {
		this.licenseOwnerName = licenseOwnerName;
	}
	@XmlElement(name="licenseIdentityNo") 
	public String getLicenseIdentityNo() {
		return licenseIdentityNo;
	}
	public void setLicenseIdentityNo(String licenseIdentityNo) {
		this.licenseIdentityNo = licenseIdentityNo;
	}
	@XmlElement(name="licenseCategory")
	public String getLicenseCategory() {
		return licenseCategory;
	}
	public void setLicenseCategory(String licenseCategory) {
		this.licenseCategory = licenseCategory;
	}
	@XmlElement(name="licenseGuideNo")
	public String getLicenseGuideNo() {
		return licenseGuideNo;
	}
	public void setLicenseGuideNo(String licenseGuideNo) {
		this.licenseGuideNo = licenseGuideNo;
	}
	@XmlElement(name="licenseArea")
	public String getLicenseArea() {
		return licenseArea;
	}
	public void setLicenseArea(String licenseArea) {
		this.licenseArea = licenseArea;
	}
	@XmlElement(name="licenseProvinceName")
	public String getLicenseProvinceName() {
		return licenseProvinceName;
	}
	public void setLicenseProvinceName(String licenseProvinceName) {
		this.licenseProvinceName = licenseProvinceName;
	}
	@XmlElement(name="licenseAdress")
	public String getLicenseAdress() {
		return licenseAdress;
	}
	public void setLicenseAdress(String licenseAdress) {
		this.licenseAdress = licenseAdress;
	}
	@XmlElement(name="licenseComittee")
	public String getLicenseComittee() {
		return licenseComittee;
	}
	public void setLicenseComittee(String licenseComittee) {
		this.licenseComittee = licenseComittee;
	}
	@XmlElement(name="licenseRegistration")
	public String getLicenseRegistration() {
		return licenseRegistration;
	}
	public void setLicenseRegistration(String licenseRegistration) {
		this.licenseRegistration = licenseRegistration;
	}
	public String getComplaintDesc() {
		return complaintDesc;
	}
	public void setComplaintDesc(String complaintDesc) {
		this.complaintDesc = complaintDesc;
	}
	public String getComplaintDate() {
		return complaintDate;
	}
	public void setComplaintDate(String complaintDate) {
		this.complaintDate = complaintDate;
	}
	public String getProgressDesc() {
		return progressDesc;
	}
	public void setProgressDesc(String progressDesc) {
		this.progressDesc = progressDesc;
	}
	public String getProgressDate() {
		return progressDate;
	}
	public void setProgressDate(String progressDate) {
		this.progressDate = progressDate;
	}
	public String getSuspendDesc() {
		return suspendDesc;
	}
	public void setSuspendDesc(String suspendDesc) {
		this.suspendDesc = suspendDesc;
	}
	public String getSuspendStatus() {
		return suspendStatus;
	}
	public void setSuspendStatus(String suspendStatus) {
		this.suspendStatus = suspendStatus;
	}
	public String getSuspendFrom() {
		return suspendFrom;
	}
	public void setSuspendFrom(String suspendFrom) {
		this.suspendFrom = suspendFrom;
	}
	public String getSuspendTo() {
		return suspendTo;
	}
	public void setSuspendTo(String suspendTo) {
		this.suspendTo = suspendTo;
	}
	
	
	
}
