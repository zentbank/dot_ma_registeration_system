package sss.dot.tourism.license.service;

import java.util.List;

public interface ILicenseSerive {
	public List getLicense(Object object) throws Exception;
	
	public List getComplaint(Object object) throws Exception;
	
	public List getSuspend(Object object) throws Exception;
	
	
}
