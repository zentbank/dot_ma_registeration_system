package sss.dot.tourism.license.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.license.dao.TraderDAO;
import sss.dot.tourism.license.dto.LicenseDTO;

@Repository("licenseSerive")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class LicenseSerive implements ILicenseSerive {

	@Autowired
	private TraderDAO traderDAO;
	
	@Override
	public List getLicense(Object object) throws Exception {
		
		if(!(object instanceof LicenseDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถค้นหาข้อมูลการจดทะเบียนได้");
		}
		
		LicenseDTO param = (LicenseDTO) object;
		
		List<LicenseDTO> list = new ArrayList<LicenseDTO>();
		
		List<Object[]> listAll = this.traderDAO.findAll(param);
		
		if(!listAll.isEmpty()){
			for(Object[] obj: listAll){
				
				LicenseDTO dto = new LicenseDTO();
				
			    dto.setLicenseNo(obj[0]==null?"":obj[0].toString());
			    dto.setLicenseStatus(obj[1]==null?"":obj[1].toString());
			    dto.setLicenseType(obj[2]==null?"":obj[2].toString());
			    dto.setLicenseName(obj[3]==null?"":obj[3].toString());
			    dto.setLicenseNameEn(obj[4]==null?"":obj[4].toString());
			    dto.setLicenseOwnerName(obj[5]==null?"":obj[5].toString());
			    dto.setLicenseIdentityNo(obj[6]==null?"":obj[6].toString());
			    dto.setLicenseCategory(obj[7]==null?"":obj[7].toString());
			    dto.setLicenseGuideNo(obj[8]==null?"":obj[8].toString());
			    dto.setLicenseArea(obj[9]==null?"":obj[9].toString());
			    dto.setLicenseProvinceName(obj[10]==null?"":obj[10].toString());
			    dto.setLicenseAdress(obj[11]==null?"":obj[11].toString());
			    dto.setLicenseComittee(obj[12]==null?"":obj[12].toString());
			    dto.setLicenseRegistration(obj[13]==null?"":obj[13].toString());
				
				list.add(dto);
			}
		}
		
		return list;
	}

	@Override
	public List getComplaint(Object object) throws Exception {
		
		if(!(object instanceof LicenseDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถค้นหาข้อมูลการจดทะเบียนได้");
		}
		
		LicenseDTO param = (LicenseDTO) object;
		
		List<LicenseDTO> list = new ArrayList<LicenseDTO>();
		
		List<Object[]> listAll = this.traderDAO.findComplaint(param.getLicenseNo());
		
		if(!listAll.isEmpty()){
			for(Object[] obj: listAll){
				
				LicenseDTO dto = new LicenseDTO();
			    dto.setLicenseNo(obj[0]==null?"":obj[0].toString());
			    dto.setComplaintDesc(obj[1]==null?"":obj[1].toString());
			    dto.setComplaintDate(obj[2]==null?"":obj[2].toString());
			    dto.setProgressDesc(obj[3]==null?"":obj[3].toString());
			    dto.setProgressDate(obj[4]==null?"":obj[4].toString());
				list.add(dto);
			}
		}
		
		return list;
	}
	
	@Override
	public List getSuspend(Object object) throws Exception {
		
		if(!(object instanceof LicenseDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถค้นหาข้อมูลการจดทะเบียนได้");
		}
		
		LicenseDTO param = (LicenseDTO) object;
		
		List<LicenseDTO> list = new ArrayList<LicenseDTO>();
		
		List<Object[]> listAll = this.traderDAO.findSuspension(param.getLicenseNo());
		
		if(!listAll.isEmpty()){
			for(Object[] obj: listAll){
				
				LicenseDTO dto = new LicenseDTO();
			    dto.setLicenseNo(obj[0]==null?"":obj[0].toString());
			    dto.setSuspendDesc(obj[1]==null?"":obj[1].toString());
			    dto.setSuspendStatus(obj[2]==null?"":obj[2].toString());
			    dto.setSuspendFrom(obj[3]==null?"":obj[3].toString());
			    dto.setSuspendTo(obj[4]==null?"":obj[4].toString());
				list.add(dto);
			}
		}
		
		return list;
	}

}
