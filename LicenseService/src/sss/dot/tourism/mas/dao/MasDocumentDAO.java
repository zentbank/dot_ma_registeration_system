package sss.dot.tourism.mas.dao;



import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.MasDocument;

@Repository("masDocumentDAO")
public class MasDocumentDAO extends BaseDAO{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1610360359920656534L;
	
	public MasDocumentDAO()
	{
		this.domainObj = MasDocument.class;
	}

}
