package sss.dot.tourism.util;

public enum ActType {
	SUSPEND("SUS" , "การพักใช้ใบอนุญาต"),
	REVOKE("REV" , "การเพิกถอนใบอนุญาต");

	
	private final String status;
	private final String meaning;
	
	ActType(String status, String meaning){
		this.status = status;
		this.meaning = meaning;
	}
	
	/**
	 * @return the category
	 */
	public String getStatusString() {
		return status.toString();
	}
	public String getStatus() {
		return status;
	}
	/**
	 * @return the meaning
	 */
	public String getMeaning() {
		return meaning;
	}
	/**
	 * overloading
	 * @param flag
	 * @return
	 */
	public boolean equals(String status){
		ActType eStatus =  getActType(status);
		return super.equals(eStatus);
	}

	public static ActType getActType(String status) {
		ActType record = null;
	
		if(SUSPEND.getStatus().equals(status)){
			record = SUSPEND;
		}
		else if(REVOKE.getStatus().equals(status)){
			record = REVOKE;
		}
		
		
		return record;
	}
	public static String getMeaning(String status) {
		return getActType(status ).getMeaning();
	}
}
