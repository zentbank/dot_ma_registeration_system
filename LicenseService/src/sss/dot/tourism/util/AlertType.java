package sss.dot.tourism.util;

public enum AlertType {
	EMAIl("E" , "อีเมล"),
	POST("P" , "จดหมายลงทะเบียน");
	
	private final String status;
	private final String meaning;
	
	AlertType(String status, String meaning){
		this.status = status;
		this.meaning = meaning;
	}
	
	/**
	 * @return the category
	 */
	public String getStatusString() {
		return status.toString();
	}
	public String getStatus() {
		return status;
	}
	/**
	 * @return the meaning
	 */
	public String getMeaning() {
		return meaning;
	}
	/**
	 * overloading
	 * @param flag
	 * @return
	 */
	public boolean equals(String status){
		AlertType eStatus =  getAlertType(status);
		return super.equals(eStatus);
	}
	public static AlertType getAlertType(String status) {
		AlertType record = null;

		if(EMAIl.getStatus().equals(status)){
			record = EMAIl;
		}
		else if(POST.getStatus().equals(status)){
			record = POST;
		}
		
		
		return record;
	}
	public static String getMeaning(String status) {
		return getAlertType(status ).getMeaning();
	}
}
