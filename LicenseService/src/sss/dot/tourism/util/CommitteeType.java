package sss.dot.tourism.util;

/*
 * ประเภทกรรมการบริษัท
C=committee กรรมการบริษัท
S=sign กรรมการผู้มีอำนาจลงนาม*/
public enum CommitteeType {
	COMMITTEE("C","กรรมการบริษัท"),
	SIGN("S","กรรมการผู้มีอำนาจลงนาม");
	

	private final String status;
	private final String meaning;

	CommitteeType(String status,String meaning){
		this.status = status;
		this.meaning = meaning;
	}

	/**
	 * @return the status
	 */
	public String getStatusString() {
		return status.toString();
	}
	public String getStatus() {
		return status;
	}
	/**
	 * @return the meaning
	 */
	public String getMeaning() {
		return meaning;
	}
	/**
	 * overloading
	 * @param flag
	 * @return
	 */
	public boolean equals(String status){
		CommitteeType eStatus =  getCommitteeType(status);
		return super.equals(eStatus);
	}
	public static CommitteeType getCommitteeType(String status) {
		CommitteeType record = null;

		
		if(COMMITTEE.getStatus().equals(status)){
			record = COMMITTEE;
		}
		else if(SIGN.getStatus().equals(status)){
			record = SIGN;
		}
		
		return record;
	}
	public static String getMeaning(String status) {
		return getCommitteeType(status).getMeaning();
	}
}
