package sss.dot.tourism.util;



public enum EducationType {
	EDUCATION("E","จบการศึกษา"),
	TRAINING("T","ผ่านการอบรม");
	

	private final String status;
	private final String meaning;

	EducationType(String status,String meaning){
		this.status = status;
		this.meaning = meaning;
	}

	/**
	 * @return the status
	 */
	public String getStatusString() {
		return status.toString();
	}
	public String getStatus() {
		return status;
	}
	/**
	 * @return the meaning
	 */
	public String getMeaning() {
		return meaning;
	}
	/**
	 * overloading
	 * @param flag
	 * @return
	 */
	public boolean equals(String status){
		EducationType eStatus =  getEducationType(status);
		return super.equals(eStatus);
	}
	public static EducationType getEducationType(String status) {
		EducationType record = null;

		
		if(EDUCATION.getStatus().equals(status)){
			record = EDUCATION;
		}
		else if(TRAINING.getStatus().equals(status)){
			record = TRAINING;
		}
		
		return record;
	}
	public static String getMeaning(String status) {
		return getEducationType(status).getMeaning();
	}
}
