package sss.dot.tourism.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileManage {
	
	
	public FileManage()
	{
		
	}
	
	public boolean uploadFile(InputStream inputStream, String path, String fileName)
	{
		
		 try {  

			  OutputStream outputStream = null; 

			  File pathfile = new File(path);
			  if(!pathfile.exists())
			  {
				  pathfile.mkdirs();
			  }
			  
			   File newFile = new File(path+fileName); 
			   
			   if (!newFile.exists()) {  
				   newFile.createNewFile();  
			   }  
			   outputStream = new FileOutputStream(newFile);  
			   int read = 0;  
			   byte[] bytes = new byte[1024];  
			  
			   while ((read = inputStream.read(bytes)) != -1) {  
			    outputStream.write(bytes, 0, read);  
			   }
			   outputStream.close();
			   
			   return true;
			  } catch (IOException e) {  
			   // TODO Auto-generated catch block  
			   e.printStackTrace();  
			   return false;
			  }  

	}
	
	public void deleteFile(String fullpath)
	{
		File file = new File(fullpath);
		
		System.out.println("file.exists() = "+file.exists());
		
		if(file.exists())
		{
			file.delete();
		}
	}
	
}
