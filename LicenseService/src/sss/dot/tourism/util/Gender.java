package sss.dot.tourism.util;

public enum Gender {

	MALE("M","ชาย"),
	FEMALE("F","หญิง");
	

	private final String status;
	private final String meaning;

	Gender(String status,String meaning){
		this.status = status;
		this.meaning = meaning;
	}

	/**
	 * @return the status
	 */
	public String getStatusString() {
		return status.toString();
	}
	public String getStatus() {
		return status;
	}
	/**
	 * @return the meaning
	 */
	public String getMeaning() {
		return meaning;
	}
	/**
	 * overloading
	 * @param flag
	 * @return
	 */
	public boolean equals(String status){
		Gender eStatus =  getGender(status);
		return super.equals(eStatus);
	}
	public static Gender getGender(String status) {
		Gender record = null;
		
		if(MALE.getStatus().equals(status)){
			record = MALE;
		}
		else if(FEMALE.getStatus().equals(status)){
			record = FEMALE;
		}
		
		return record;
	}
	public static String getMeaning(String status) {
		return getGender(status).getMeaning();
	}
}
