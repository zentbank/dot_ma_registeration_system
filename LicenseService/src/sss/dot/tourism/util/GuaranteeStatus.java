package sss.dot.tourism.util;

/*
 * N=ปกติ
R=Refund คืนหลักประกัน
C=Change หลักประกันนี้ถูกยกเลิกเพราะมีการเปลี่ยนแปลงหลักประกัน*/
public enum GuaranteeStatus {
	NORMAL("N" , "ปกติ"),
	CHANGE("C" , "หลักประกันนี้ถูกยกเลิก"),
	REFUND("R" , "คืนหลักประกัน");
	
	private final String status;
	private final String meaning;
	
	GuaranteeStatus(String status, String meaning){
		this.status = status;
		this.meaning = meaning;
	}
	
	/**
	 * @return the category
	 */
	public String getStatusString() {
		return status.toString();
	}
	public String getStatus() {
		return status;
	}
	/**
	 * @return the meaning
	 */
	public String getMeaning() {
		return meaning;
	}
	/**
	 * overloading
	 * @param flag
	 * @return
	 */
	public boolean equals(String status){
		GuaranteeStatus eStatus =  getGuaranteeStatus(status);
		return super.equals(eStatus);
	}
	public static GuaranteeStatus getGuaranteeStatus(String status) {
		GuaranteeStatus record = null;
	
		if(NORMAL.getStatus().equals(status)){
			record = NORMAL;
		}
		else if(CHANGE.getStatus().equals(status)){
			record = CHANGE;
		}
		else if(REFUND.getStatus().equals(status)){
			record = REFUND;
		}
		
		return record;
	}
	public static String getMeaning(String status) {
		return getGuaranteeStatus(status ).getMeaning();
	}
}
