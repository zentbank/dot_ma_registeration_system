package sss.dot.tourism.util;

/*
 * C=Cash เงินสด
G=Bank Guarantee
B=Government bond พันธบัตรรัฐบาล
 * */
public enum GuaranteeType {
	CASH("C" , "เงินสด"),
	BANK("B" , "Bank Guarantee"),
	GOVERNMENT_BOUND("G" , "พันธบัตรรัฐบาล");
	
	private final String status;
	private final String meaning;
	
	GuaranteeType(String status, String meaning){
		this.status = status;
		this.meaning = meaning;
	}
	
	/**
	 * @return the category
	 */
	public String getStatusString() {
		return status.toString();
	}
	public String getStatus() {
		return status;
	}
	/**
	 * @return the meaning
	 */
	public String getMeaning() {
		return meaning;
	}
	/**
	 * overloading
	 * @param flag
	 * @return
	 */
	public boolean equals(String status){
		GuaranteeType eStatus =  getGuaranteeType(status);
		return super.equals(eStatus);
	}
	public static GuaranteeType getGuaranteeType(String status) {
		GuaranteeType record = null;
	
		if(CASH.getStatus().equals(status)){
			record = CASH;
		}
		else if(BANK.getStatus().equals(status)){
			record = BANK;
		}
		else if(GOVERNMENT_BOUND.getStatus().equals(status)){
			record = GOVERNMENT_BOUND;
		}
		
		return record;
	}
	public static String getMeaning(String status) {
		return getGuaranteeType(status ).getMeaning();
	}
}
