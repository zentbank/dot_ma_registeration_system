
package sss.dot.tourism.util;

public enum ProgressRole {
	PEOPLE("PE" , "ประชาชน"),
	RECEIPTLAW("RL" , "นิติกรรับเรื่อง"),
	MANAGERLAW("ML" , "หัวหน้านิติกร"),
	WORKERLAW("WL" , "นิติกรพิจารณาเรื่อง");
	
	private final String status;
	private final String meaning;
	
	ProgressRole(String status, String meaning){
		this.status = status;
		this.meaning = meaning;
	}
	
	public String getStatusString() {
		return status.toString();
	}
	public String getStatus() {
		return status;
	}
	public String getMeaning() {
		return meaning;
	}
	
	public boolean equals(String status){
		ProgressRole cStatus =  getProgressRole(status);
		return super.equals(cStatus);
	}
	public static ProgressRole getProgressRole(String status) {
		ProgressRole record = null;

		if(PEOPLE.getStatus().equals(status)){
			record = PEOPLE;
		}
		else if(RECEIPTLAW.getStatus().equals(status)){
			record = RECEIPTLAW;
		}
		else if(MANAGERLAW.getStatus().equals(status)){
			record = MANAGERLAW;
		}
		else if(WORKERLAW.getStatus().equals(status)){
			record = WORKERLAW;
		}
		return record;
	}
	public static String getMeaning(String status) {
		return getProgressRole(status).getMeaning();
	}
}
