package sss.dot.tourism.util;


/**
 * 
 * @author SHAII_MACBOOK
 *สถานะการพิมพ์ใบเสร็จ
W=รอพิมพืใบเสร็จ
P=พิมพ์ใบเสร็จแล้ว
C=ยกเลิกใบเสร็จ
 */
public enum ReceiptStatus {
	WAIT("W" , "รอพิมพืใบเสร็จ"),
	CANCEL("C" , "ยกเลิกใบเสร็จ"),
	PAY("P" , "พิมพ์ใบเสร็จแล้ว"),
	TEMP("T","อยู่ระหว่างดำเนินการ");
	
	private final String status;
	private final String meaning;
	
	ReceiptStatus(String status, String meaning){
		this.status = status;
		this.meaning = meaning;
	}
	
	/**
	 * @return the category
	 */
	public String getStatusString() {
		return status.toString();
	}
	public String getStatus() {
		return status;
	}
	/**
	 * @return the meaning
	 */
	public String getMeaning() {
		return meaning;
	}
	/**
	 * overloading
	 * @param flag
	 * @return
	 */
	public boolean equals(String status){
		ReceiptStatus eStatus =  getReceiptStatus(status);
		return super.equals(eStatus);
	}
	public static ReceiptStatus getReceiptStatus(String status) {
		ReceiptStatus record = null;
	
		if(WAIT.getStatus().equals(status)){
			record = WAIT;
		}
		else if(CANCEL.getStatus().equals(status)){
			record = CANCEL;
		}
		else if(PAY.getStatus().equals(status)){
			record = PAY;
		}
		else if(TEMP.getStatus().equals(status)){
			record = TEMP;
		}

		return record;
	}
	public static String getMeaning(String status) {
		return getReceiptStatus(status ).getMeaning();
	}
}
