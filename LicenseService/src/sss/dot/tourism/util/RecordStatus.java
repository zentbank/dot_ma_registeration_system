package sss.dot.tourism.util;
/*
 * สถานะของข้อมูล
N=ปัจจุบัน
T=Tempolary
H=History
D=disallow ไม่ผ่านการตรวจสอบ /ยกเลิกการจดทะเบียน
C=Cancel ยกเลิกใบอนุญาต*/
public enum RecordStatus{
	
	CANCEL("C","ยกเลิกใบอนุญาต"),
	NORMAL("N","ปัจจุบัน"),
	DISALLOW("D","ไม่ผ่านการตรวจสอบ /ยกเลิกการจดทะเบียน"),
	HISTORY("H","ประวัติ"),
	TEMP("T","อยู่ระหว่างจดทะเบียน");
	private final String status;
	private final String meaning;

	RecordStatus(String status,String meaning){
		this.status = status;
		this.meaning = meaning;
	}

	/**
	 * @return the status
	 */
	public String getStatusString() {
		return status.toString();
	}
	public String getStatus() {
		return status;
	}
	/**
	 * @return the meaning
	 */
	public String getMeaning() {
		return meaning;
	}
	/**
	 * overloading
	 * @param flag
	 * @return
	 */
	public boolean equals(String status){
		RecordStatus eStatus =  getRecordStatus(status);
		return super.equals(eStatus);
	}
	public static RecordStatus getRecordStatus(String status) {
		RecordStatus record = null;
		
		
		if(NORMAL.getStatus().equals(status)){
			record = NORMAL;
		}
		else if(CANCEL.getStatus().equals(status)){
			record = CANCEL;
		}
		else if(DISALLOW.getStatus().equals(status)){
			record = DISALLOW;
		}
		
		else if(TEMP.getStatus().equals(status)){
			record = TEMP;
		}
		else if(HISTORY.getStatus().equals(status)){
			record = HISTORY;
		}
		return record;
	}
	public static String getMeaning(String status) {
		return getRecordStatus(status).getMeaning();
	}
}
