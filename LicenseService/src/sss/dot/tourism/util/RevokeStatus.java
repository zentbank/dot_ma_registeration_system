package sss.dot.tourism.util;

public enum RevokeStatus {
	WAIT("W" , "อยู่ระหว่างดำเนินการเพิกถอน"),
	CANCEL("C" , "ยกเลิกการเพิกถอน"),
	REVOKE("R" , "เพิกถอนใบอนุญาต"),
	APPROVE("A" , "อนุมัติให้เพิกถอน"),
	NORMAL("N","ครบกำหนดการเพิกถอน");
	
	private final String status;
	private final String meaning;
	
	RevokeStatus(String status, String meaning){
		this.status = status;
		this.meaning = meaning;
	}
	
	/**
	 * @return the category
	 */
	public String getStatusString() {
		return status.toString();
	}
	public String getStatus() {
		return status;
	}
	/**
	 * @return the meaning
	 */
	public String getMeaning() {
		return meaning;
	}
	/**
	 * overloading
	 * @param flag
	 * @return
	 */
	public boolean equals(String status){
		RevokeStatus eStatus =  getRevokeStatus(status);
		return super.equals(eStatus);
	}
	public static RevokeStatus getRevokeStatus(String status) {
		RevokeStatus record = null;

		if(NORMAL.getStatus().equals(status)){
			record = NORMAL;
		}
		else if(CANCEL.getStatus().equals(status)){
			record = CANCEL;
		}
		else if(REVOKE.getStatus().equals(status)){
			record = REVOKE;
		}
		else if(WAIT.getStatus().equals(status)){
			record = WAIT;
		}
		else if(APPROVE.getStatus().equals(status)){
			record = APPROVE;
		}
		
		return record;
	}
	public static String getMeaning(String status) {
		return getRevokeStatus(status ).getMeaning();
	}
}
