package sss.dot.tourism.util;

public enum RoleStatus {

	KEY("KEY","เจ้าหน้าที่บันทึกข้อมูล"),
	LAW("LAW","นิติกร"),
	RECHECK("CHK","เจ้าหน้าที่ตรวจสอบความถูกต้อง"),
	SUPERVISOR("SUP","หัวหน้างานทะเบบียน"),
	MANAGER("MAN","หัวหน้ากลุ่มทะเบียน"),
	DIRECTOR("DIR","นายทะเบียน"),
	ACCOUNT("ACC","เจ้าหน้าที่การเงิน"),
	PRINT_LICENSE("PLI","Print License พิมพ์ใบอนุญาต"),
	PRINT_CARD("PCA","Print Card พิมพ์บัตรมัคคุเทศก์"),
	APPROVE("A","Approve นายทะเบียนอนุมัติและจ่ายเงินแล้ว"),
	REJECT("R","Reject นายทะเบียนไม่อนุมัติ"),
	CANCEL("C","Cancel ยกเลิกการจดทะเบียนิ");
	
	private final String status;
	private final String meaning;

	RoleStatus(String status,String meaning){
		this.status = status;
		this.meaning = meaning;
	}

	/**
	 * @return the status
	 */
	public String getStatusString() {
		return status.toString();
	}
	public String getStatus() {
		return status;
	}
	/**
	 * @return the meaning
	 */
	public String getMeaning() {
		return meaning;
	}
	/**
	 * overloading
	 * @param flag
	 * @return
	 */
	public boolean equals(String status){
		RoleStatus eStatus =  getRoleStatus(status);
		return super.equals(eStatus);
	}
	public static RoleStatus getRoleStatus(String status) {
		RoleStatus record = null;
		
		if(KEY.getStatus().equals(status)){
			record = KEY;
		}
		else if(LAW.getStatus().equals(status)){
			record = LAW;
		}
		else if(RECHECK.getStatus().equals(status)){
			record = RECHECK;
		}
		else if(SUPERVISOR.getStatus().equals(status)){
			record = SUPERVISOR;
		}
		else if(MANAGER.getStatus().equals(status)){
			record = MANAGER;
		}
		else if(DIRECTOR.getStatus().equals(status)){
			record = DIRECTOR;
		}
		else if(ACCOUNT.getStatus().equals(status)){
			record = ACCOUNT;
		}
		else if(PRINT_LICENSE.getStatus().equals(status)){
			record = PRINT_LICENSE;
		}
		else if(PRINT_CARD.getStatus().equals(status)){
			record = PRINT_CARD;
		}
		else if(APPROVE.getStatus().equals(status)){
			record = APPROVE;
		}
		else if(REJECT.getStatus().equals(status)){
			record = REJECT;
		}
		else if(CANCEL.getStatus().equals(status)){
			record = CANCEL;
		}
		return record;
	}
	public static String getMeaning(String status) {
		return getRoleStatus(status).getMeaning();
	}
}
