package sss.dot.tourism.util;

public enum TraderCategory {

	OUTBOUND("B","100" , "ทั่วไป"),//200,000
	INBOUND("B","200" , "นำเที่ยวจากต่างประเทศ"),//100,000
	COUNTRY("B","300" , "ในประเทศ"),//50,000
	AREA("B","400" , "เฉพาะพื้นที่"),//10,000
	GERNERAL_GUIDE("G","1" , "มัคคุเทศก์ทั่วไป"),
	AREA_GUIDE("G","2" , "มัคคุเทศก์เฉพาะ"),
	PLATINUM_GUIDE("G","100" , "มัคคุเทศก์ทั่วไป (ต่างประเทศ)"),
	GOLD_GUIDE("G","101" , "มัคคุเทศก์ทั่วไป (ไทย)"),
	PINK_GUIDE("G","200" , "มัคคุเทศก์เฉพาะ (ต่างประเทศ-เฉพาะพื้นที่)"),
	BLUE_GUIDE("G","201" , "มัคคุเทศก์เฉพาะ (ไทย-เฉพาะพื้นที่)"),
	GREEN_GUIDE("G","202" , "มัคคุเทศก์เฉพาะ (เดินป่า)"),
	RED_GUIDE("G","203" , "มัคคุเทศก์เฉพาะ (ศิลปะวัฒนธรรม)"),
	ORANGE_GUIDE("G","204" , "มัคคุเทศก์เฉพาะ (ทางทะเล)"),
	YELLOW_GUIDE("G","205" , "มัคคุเทศก์เฉพาะ (ทะเลชายฝั่ง)"),
	VIOLET_GUIDE("G","206" , "มัคคุเทศก์เฉพาะ (แหล่งท่องเที่ยวธรรมชาติ)"),
	BROWN_GUIDE("G","207" , "มัคคุเทศก์เฉพาะ (วัฒนธรรมท้องถิ่น)"),
	ACT_LEADER("L","100" , "ก่อนกฎกระทรวงฯ"),
	GUIDE_LEADER("L","200" , "กรณีมัคคุเทศก์"),
	TRAINED_LEADER("L","300" , "กรณีผ่านการอบรม");
	
	private final String traderType;
	private final String category;
	private final String meaning;

	TraderCategory(String traderType, String category,String meaning){
		this.traderType = traderType;
		this.category = category;
		this.meaning = meaning;
	}


	public String getStatus() {
		return category;
	}
	/**
	 * @return the meaning
	 */
	public String getMeaning() {
		return meaning;
	}
	/**
	 * overloading
	 * @param flag
	 * @return
	 */
	public boolean equals(String traderType, String category){
		TraderCategory eStatus =  getTraderCategory(traderType, category);
		return super.equals(eStatus);
	}
	public static TraderCategory getTraderCategory(String traderType, String category) {
		TraderCategory record = null;

		if(traderType.equals("B"))
		{
			if(OUTBOUND.getStatus().equals(category)){
				record = OUTBOUND;
			}
			else if(INBOUND.getStatus().equals(category)){
				record = INBOUND;
			}
			else if(COUNTRY.getStatus().equals(category)){
				record = COUNTRY;
			}
			else if(AREA.getStatus().equals(category)){
				record = AREA;
			}
		}
		
		if(traderType.equals("G"))
		{
			if(PLATINUM_GUIDE.getStatus().equals(category)){
				record = PLATINUM_GUIDE;
			}
			else if(GOLD_GUIDE.getStatus().equals(category)){
				record = GOLD_GUIDE;
			}
			else if(PINK_GUIDE.getStatus().equals(category)){
				record = PINK_GUIDE;
			}
			else if(BLUE_GUIDE.getStatus().equals(category)){
				record = BLUE_GUIDE;
			}
			else if(GREEN_GUIDE.getStatus().equals(category)){
				record = GREEN_GUIDE;
			}
			else if(RED_GUIDE.getStatus().equals(category)){
				record = RED_GUIDE;
			}
			else if(ORANGE_GUIDE.getStatus().equals(category)){
				record = ORANGE_GUIDE;
			}
			else if(YELLOW_GUIDE.getStatus().equals(category)){
				record = YELLOW_GUIDE;
			}
			else if(VIOLET_GUIDE.getStatus().equals(category)){
				record = VIOLET_GUIDE;
			}
			else if(BROWN_GUIDE.getStatus().equals(category)){
				record = BROWN_GUIDE;
			}
			else if(GERNERAL_GUIDE.getStatus().equals(category)){
				record = GERNERAL_GUIDE;
			}
			else if(AREA_GUIDE.getStatus().equals(category)){
				record = AREA_GUIDE;
			}
		}
		
		if(traderType.equals("L"))
		{
			if(ACT_LEADER.getStatus().equals(category)){
				record = ACT_LEADER;
			}
			else if(GUIDE_LEADER.getStatus().equals(category)){
				record = GUIDE_LEADER;
			}
			else if(TRAINED_LEADER.getStatus().equals(category)){
				record = TRAINED_LEADER;
			}
		}

		return record;
	}
	public static String getMeaning(String traderType, String category) {
		return getTraderCategory(traderType ,category).getMeaning();
	}
}
