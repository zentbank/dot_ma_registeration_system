package sss.dot.tourism.util;

public enum TraderType {

	TOUR_COMPANIES("B","ธุรกิจนำเที่ยว","100"),
	GUIDE("G","มัคคุเทศก์", "200"),
	LEADER("L","ผู้นำเที่ยว", "300");
	

	private final String status;
	private final String meaning;
	private final String code;

	TraderType(String status,String meaning,String code){
		this.status = status;
		this.meaning = meaning;
		this.code = code;
	}

	/**
	 * @return the status
	 */
	public String getStatusString() {
		return status.toString();
	}
	public String getStatus() {
		return status;
	}
	/**
	 * @return the meaning
	 */
	public String getMeaning() {
		return meaning;
	}
	
	
	public String getCode() {
		return code;
	}
	/**
	 * overloading
	 * @param flag
	 * @return
	 */
	public boolean equals(String status){
		TraderType eStatus =  getTraderType(status);
		return super.equals(eStatus);
	}
	public static TraderType getTraderType(String status) {
		TraderType record = null;
		
		if(TOUR_COMPANIES.getStatus().equals(status)){
			record = TOUR_COMPANIES;
		}
		else if(GUIDE.getStatus().equals(status)){
			record = GUIDE;
		}
		else if(LEADER.getStatus().equals(status)){
			record = LEADER;
		}		
		return record;
	}
	
	
	public static String getMeaning(String status) {
		return getTraderType(status).getMeaning();
	}
	
	public static String getCode(String status) {
		return getTraderType(status).getCode();
	}
	
	public static TraderType getTraderTypeByCode(String code) {
		TraderType record = null;
		
		if(TOUR_COMPANIES.getCode().equals(code)){
			record = TOUR_COMPANIES;
		}
		else if(GUIDE.getCode().equals(code)){
			record = GUIDE;
		}
		else if(LEADER.getCode().equals(code)){
			record = LEADER;
		}		
		return record;
	}
	
	public static String getType(String code) {
		return getTraderTypeByCode(code).getStatus();
	}
}
