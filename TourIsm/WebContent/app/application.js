Ext.define('tourism.Application', {
    name: 'tourism',

    extend: 'Ext.app.Application',

    views: [
        //Business
        'tourism.view.menu.TreeMenu'
        ,'tourism.view.CenterView'


        ,'tourism.view.business.registration.RegistrationFromSearch'
        ,'tourism.view.business.registration.RegistrationPanel'
        ,'tourism.view.business.registration.RegistrationGrid'

        ,'tourism.view.business.registration.registrationType.RegistrationTypeAddEditWindow'

        ,'tourism.view.business.registration.BusinessRegistrationAddEditForm'
        ,'tourism.view.business.registration.BusinessRegistrationAddEditWindow'
        ,'tourism.view.window.verification.VerificationWindow'
        ,'tourism.view.committeeprogress.CommitteeProgressWindow'


        //Guide
        ,'tourism.view.guide.registration.GuidePanel'
        ,'tourism.view.guide.registration.GuideRegistrationFormSearch'
        ,'tourism.view.guide.registration.GuideRegistrationGrid'
        ,'tourism.view.guide.registration.GuideRegistrationAddEditWindow'
        ,'tourism.view.guide.registration.GuideRegistrationAddEditForm'

        ,'tourism.view.guide.registration.registrationType.GuideRegistrationTypeAddEditForm'
        ,'tourism.view.guide.registration.registrationType.GuideRegistrationTypeAddEditWindow'
        //,'tourism.view.guide.registration.registrationType.GuideRegistrationTypeBusinessForm'

        ,'tourism.view.guide.registration.registrationTab.address.GuideTraderAddressAddEditForm'
        ,'tourism.view.guide.registration.registrationTab.address.GuideTraderAddressAddEditWindow'
        ,'tourism.view.guide.registration.registrationTab.address.GuideTraderAddressGrid'

        ,'tourism.view.guide.registration.registrationTab.guideType.GuideTypeAddEditForm'


        ,'tourism.view.guide.registration.registrationTab.guideTrain.GuideTrainGrid'
        ,'tourism.view.guide.registration.registrationTab.guideTrain.GuideTrainAddEditWindow'
        ,'tourism.view.guide.registration.registrationTab.guideTrain.GuideTrainAddEditForm'
        ,'tourism.view.guide.registration.registrationTab.guideTrain.GuideSearchTrainGrid'
        ,'tourism.view.guide.registration.registrationTab.guideTrain.GuideTrainTypeAddEditForm'

        //Tourleader
        ,'tourism.view.tourleader.registration.TourleaderPanel'
        ,'tourism.view.tourleader.registration.TourleaderRegistrationFormSearch'
        ,'tourism.view.tourleader.registration.TourleaderRegistrationGrid'

        ,'tourism.view.tourleader.registration.registrationType.RegistrationTypeAddEditForm'
        ,'tourism.view.tourleader.registration.registrationType.RegistrationTypeSearchBusinessForm'

        ,'tourism.view.tourleader.registration.TourleaderRegistrationAddEditWindow'
        ,'tourism.view.tourleader.registration.TourleaderRegistrationAddEditForm'

        //Tab
        ,'tourism.view.tourleader.registration.registrationTab.address.TraderAddressGrid'
        ,'tourism.view.tourleader.registration.registrationTab.address.TraderAddressAddEditWindow'
        ,'tourism.view.tourleader.registration.registrationTab.address.TraderAddressAddEditForm'

        ,'tourism.view.tourleader.registration.registrationTab.tourleaderType.TourleaderTypeAddEditForm'

        //End Tourleader

        //Window
        ,'tourism.view.window.registration.registrationTab.type.TypeAddGuideSearchWindow'
        ,'tourism.view.window.registration.registrationTab.type.TypeAddGuideSearchForm'
        ,'tourism.view.window.registration.registrationTab.type.TypeAddGuideGrid'

        ,'tourism.view.window.registration.registrationTab.type.TypeAddTrainSearchWindow'
        ,'tourism.view.window.registration.registrationTab.type.TypeAddTrainSearchForm'
        ,'tourism.view.window.registration.registrationTab.type.TypeAddTrainGrid'

        ,'tourism.view.window.registration.registrationTab.educationAndLanguage.LanguageGrid'
        ,'tourism.view.window.registration.registrationTab.educationAndLanguage.EducationGrid'

        //combobox
        ,'tourism.view.combo.MasProvinceCombo'
        ,'tourism.view.combo.MasAmphurCombo'
        ,'tourism.view.combo.MasTambolCombo'
        ,'tourism.view.combo.MasPrefixCombo'
        ,'tourism.view.combo.MasUniversityCombo'
        ,'tourism.view.combo.CourseCombo'

        //document registration
        ,'tourism.view.document.registration.RegistrationDocumentPanel'

        ,'tourism.view.document.registration.RegistrationDocumentBusinessFormSearch'
        ,'tourism.view.document.registration.RegistrationDocumentGuideFormSearch'
        ,'tourism.view.document.registration.RegistrationDocumentTourleaderFormSearch'

        ,'tourism.view.document.registration.RegistrationDocumentBusinessGrid'
        ,'tourism.view.document.registration.RegistrationDocumentGuideGrid'
        ,'tourism.view.document.registration.RegistrationDocumentTourleaderGrid'



        //COMPLAIN
        ,'tourism.view.complaint.ComplaintPanel'
        ,'tourism.view.complaint.ComplaintFAQFormSearch'
        ,'tourism.view.complaint.ComplaintFAQGrid'

        ,'tourism.view.complaint.ComplaintReceiveFormSearch'
        ,'tourism.view.complaint.ComplaintReceiveGrid'

        ,'tourism.view.complaint.ComplaintConsiderFormSearch'
        ,'tourism.view.complaint.ComplaintConsiderGrid'

        ,'tourism.view.complaint.AddWindow'

        //CHART
        ,'tourism.view.chart.business.type.ChartPiePanel'
        ,'tourism.view.chart.business.type.ChartFormSearch'
        ,'tourism.view.chart.business.type.PieBusinessTraderCategoryChart'
        ,'tourism.view.chart.business.type.PieBusinessOrgIdChart'

        ,'tourism.view.chart.business.all.ChartColumnPanel'
        ,'tourism.view.chart.business.all.ChartBusinessAllFormSearch'
        ,'tourism.view.chart.business.all.ColumnBusinessAllChart'

        ,'tourism.view.chart.business.newbusiness.ChartBusinessNewFormSearch'
        ,'tourism.view.chart.business.newbusiness.ColumnBusinessNewChart'

        ,'tourism.view.chart.business.renew.ChartBusinessRenewFormSearch'
        ,'tourism.view.chart.business.renew.ColumnBusinessRenewChart'

        ,'tourism.view.chart.business.guarantee.ChartBusinessGuaranteeFormSearch'
        ,'tourism.view.chart.business.guarantee.ColumnBusinessGuaranteeChart'

        ,'tourism.view.chart.business.suspensionAndrevoke.ChartBusinessSuspensionAndRevokeFormSearch'
        ,'tourism.view.chart.business.suspensionAndrevoke.ColumnBusinessSuspensionAndRevokeChart'

        ,'tourism.view.chart.guide.type.ChartColumnPanel'
        ,'tourism.view.chart.guide.type.ChartGuideTypeFormSearch'
        ,'tourism.view.chart.guide.type.ColumnGuideTypeChart'

        ,'tourism.view.chart.guide.all.ChartPiePanel'
        ,'tourism.view.chart.guide.all.ChartGuideAllFormSearch'
        ,'tourism.view.chart.guide.all.PieGuideAllChart'
        ,'tourism.view.chart.guide.all.ColumnGuideAllChart'

        ,'tourism.view.chart.guide.newguide.ChartGuideNewFormSearch'
        ,'tourism.view.chart.guide.newguide.ColumnGuideNewChart'

        ,'tourism.view.chart.guide.renew.ChartGuideRenewFormSearch'
        ,'tourism.view.chart.guide.renew.ColumnGuideRenewChart'

        ,'tourism.view.chart.guide.org.ChartPieOrgPanel'
        ,'tourism.view.chart.guide.org.ChartGuideOrgFormSearch'
        ,'tourism.view.chart.guide.org.PieGuideOrgChart'
        ,'tourism.view.chart.guide.org.ColumnGuideOrgChart'
        
        
        //sek add chart 19/09/2559      
        ,'tourism.view.report.ReportLanguageGuidePanel'
        ,'tourism.view.report.ReportLanguageGuideFormSearch'
        
        //sek add chart 23/09/2559
        ,'tourism.view.report.ReportProvinceBusinessPanel'
        ,'tourism.view.report.ReportProvinceBusinessFormSearch'
        ,'tourism.view.report.ReportStaticTourLeaderPanel'
        ,'tourism.view.report.ReportStaticTourLeaderFormSearch'
        ,'tourism.view.report.ReportBusinessRegistrationTypePanel'
        ,'tourism.view.report.ReportBusinessRegistrationTypeFormSearch'
        ,'tourism.view.report.ReportNewBusinessPanel'
        ,'tourism.view.report.ReportNewBusinessFormSearch'
        ,'tourism.view.report.ReportMasDeactypeBusinessPanel'
        ,'tourism.view.report.ReportMasDeactypeBusinessFormSearch'
        //end sek add
		
        //guidedetail
//        ,'tourism.view.guidedetail.GuideDetailAddEditWindow'
        ,'tourism.view.guidedetail.GuideDetailPanel'
        ,'tourism.view.guidedetail.GuideDetailFormSearch'
        ,'tourism.view.guidedetail.GuideDetailGrid'

        //CancelAccount
        ,'tourism.view.refundguarantee.account.ReceiptAccountCancleAddEditWindow'
    ],

    controllers: [
        //Menu
        'tourism.controller.menu.TreeMenuCrtl'

        ,'tourism.controller.business.registration.BusinessRegistrationController'
        ,'tourism.controller.business.registration.address.TraderAddressController'
        ,'tourism.controller.business.registration.registrationType.RegistrationTypeController'
        ,'tourism.controller.business.registration.tradercategory.TradercategoryCtrl'

        //lawyer
        ,'tourism.controller.registration.LawyerCtrl'
        ,'tourism.controller.registration.RecheckCtrl'
        ,'tourism.controller.registration.ManagerCtrl'
        ,'tourism.controller.registration.SupervisorCtrl'
        ,'tourism.controller.registration.DirectorCtrl'


        //Tourleader
        ,'tourism.controller.tourleader.registration.TourleaderRegistrationController'
        ,'tourism.controller.tourleader.registration.registrationType.RegistrationTypeController'

        ,'tourism.controller.tourleader.registration.registrationTab.address.TraderAddressController'
        ,'tourism.controller.business.registration.branch.BranchCtrl'
        ,'tourism.controller.business.registration.committee.CommitteeCtrl'

        ,'tourism.controller.tourleader.registration.registrationTab.tourleaderType.TourleaderTypeController'

        //Window
        ,'tourism.controller.window.registration.registrationTab.type.TypeAddGuideController'
        ,'tourism.controller.window.registration.registrationTab.type.TypeAddTrainController'


        //Guide
        ,'tourism.controller.guide.registration.GuideRegistrationController'
        ,'tourism.controller.guide.registration.registrationType.GuideRegistrationTypeController'
        ,'tourism.controller.guide.registration.registrationTab.address.GuideTraderAddressController'
        ,'tourism.controller.guide.registration.registrationTab.guideTrain.GuideTrainController'
        ,'tourism.controller.guide.registration.guideCatagory.GuideCatagoryController'

        //Account
        // ,'tourism.controller.account.AccountCtrl'
        ,'tourism.controller.account.ReceiptAccountCtrl'

        //Course
        ,'tourism.controller.course.CourseController'

        ,'tourism.controller.course.PersonTrainedController'

        //document registration
        ,'tourism.controller.document.registration.RegistrationDocumentCtrl'

        //Info
        ,'tourism.controller.info.InfoCtrl'
        ,'tourism.controller.info.SearchLicenseCtrl'
        ,'tourism.controller.info.SearchBusinessNameCtrl'

        //printlicense

        ,'tourism.controller.printlicense.PrintLicenseCtrl'

        // ,'tourism.view.controller.printlicense.PrintLicenseCtrl'

        //Complaint
        ,'tourism.controller.complaint.ComplaintCtrl'
        ,'tourism.controller.punishment.BusinessSuspensionCtrl'
        ,'tourism.controller.punishment.GuideSuspensionCtrl'
        ,'tourism.controller.punishment.TourLeaderSuspensionCtrl'
        ,'tourism.controller.punishment.BusinessRevokeCtrl'
        ,'tourism.controller.punishment.GuideRevokeCtrl'
        ,'tourism.controller.punishment.TourleaderRevokeCtrl'
        ,'tourism.controller.deactivate.BusinessDeactivateCtrl'
        ,'tourism.controller.deactivate.GuideDeactivateCtrl'
        ,'tourism.controller.deactivate.TourleaderDeactivateCtrl'

        //refund Guarantee
        ,'tourism.controller.refundguarantee.RefundguaranteeCtrl'


        //printcard
        ,'tourism.controller.printcard.PrintCardController'

        //printaddress
        ,'tourism.controller.printaddress.PrintAddressCtrl'


        //printreport
        ,'tourism.controller.report.ReportFeeGuideCtrl'
        ,'tourism.controller.report.ReportFeeBusinessGuideCtrl'
        ,'tourism.controller.report.ReportFeeInExTotalCtrl'
        ,'tourism.controller.report.ReportFeeTotalCtrl'
        ,'tourism.controller.report.ReportGuaranteeAndFeeCtrl'
        ,'tourism.controller.report.ReportGuaranteeAndFeeRegistrationTypeCtrl'
       ,'tourism.controller.report.ReportBusinessTourismByPlaceCtrl'
        
        //sek add
        ,'tourism.controller.report.ReportLanguageGuideCtrl'
        ,'tourism.controller.report.ReportProvinceBusinessCtrl'
        ,'tourism.controller.report.ReportStaticTourLeaderCtrl'
        ,'tourism.controller.report.ReportBusinessRegistrationTypeCtrl'
        ,'tourism.controller.report.ReportNewBusinessCtrl'
        ,'tourism.controller.report.ReportMasDeactypeBusinessCtrl'
        //end
        
        
        //Chart
        ,'tourism.controller.chart.ChartCtrl'

        ,'tourism.controller.admuser.AdmuserCtrl'

        ,'tourism.controller.account.ChangeGuarantee'
        ,'tourism.controller.ktbbank.KTBPaymentCtrl'


        ,'tourism.controller.license.info.ReorganizeLicenseCtrl'
        ,'tourism.controller.reqcertificate.ReqCertificateCtrl'
        ,'tourism.controller.reqpassport.ReqPassportCtrl'

        //GuideDetail
        ,'tourism.controller.guidedetail.GuidedetailController'
        ,'tourism.controller.joborder.JobOrderCtrl'
        ,'tourism.controller.document.RegDocumentCtrl'
        ,'tourism.controller.account.PaymentOnlineCtrl'
        ,'tourism.controller.reqtouroperator.ReqTourOperatorCtrl'

    ],

    stores: [
        'tourism.store.menu.MenuStore',
        'tourism.store.business.registration.BusinessRegistrationStore',
        // 'tourism.store.business.registration.address.TraderAddressStore',
        // 'tourism.store.business.registration.committee.CommitteeStore',

        'tourism.store.combo.BusinessRegistrationTypeStore',
        'tourism.store.combo.BusinessCategoryStore',
        'tourism.store.combo.GuideCategoryStore',
        'tourism.store.combo.GuideRegistrationTypeStore',
        'tourism.store.combo.TourleaderCategoryStore',
        'tourism.store.combo.TourleaderRegistrationTypeStore',

        //Combobox
        'tourism.store.combo.MasPrefixStore',
        'tourism.store.combo.MasProvinceStore',
        'tourism.store.combo.MasAmphurStore',
        'tourism.store.combo.MasTambolStore',
        'tourism.store.combo.CorporateTypeStore',
        'tourism.store.combo.MasPostfixStore',
        'tourism.store.combo.RegProgressStatusStore',
        'tourism.store.combo.RegKeyProgressStatusStore',
        'tourism.store.combo.AccountProgressStatus',


        'tourism.store.combo.MasUniversityStore'
        ,'tourism.store.combo.LanguageStore'
        ,'tourism.store.combo.CountryStrore'
        ,'tourism.store.combo.MasBankStore'
        ,'tourism.store.combo.CourseNameStore'

        ,'tourism.store.combo.LicenseStatusStore'
        ,'tourism.store.combo.OrganizationStore'

        ,'tourism.store.combo.TraderTypeStore'
        ,'tourism.store.combo.ComplaintStatus'
        ,'tourism.store.combo.SuspendStatus'
        ,'tourism.store.combo.MasComplaintTypeStore'
        ,'tourism.store.combo.AdmUserStore'
        ,'tourism.store.combo.ProgressStatusStore'
        ,'tourism.store.combo.DeactivateStatusStore'
        ,'tourism.store.combo.AlertStatusStore'
        ,'tourism.store.combo.RefundGuaranteeStatusStore'

        ,'tourism.store.combo.ComplaintFAQStatus'
        ,'tourism.store.combo.ComplaintConsiderStatus'
        ,'tourism.store.combo.ReovkeStatus'
        ,'tourism.store.combo.TraderTypeReportStore'

        ,'tourism.store.combo.ExpireDateStore'

        //Tourleader
        ,'tourism.store.tourleader.registration.TourleaderRegistrationStore'


        //Grid
        ,'tourism.store.grid.PersonTrainedStore'
        ,'tourism.store.grid.TraderStore'
        ,'tourism.store.grid.ForeignLanguageStore'
        ,'tourism.store.grid.EducationStore'

        ,'tourism.store.grid.ComplaintLicenseStore'



        //Guide
        ,'tourism.store.guide.registration.GuideRegistrationStore'

        //Account
        ,'tourism.store.combo.OfficerStore'

        //course
        ,'tourism.store.course.CourseStore'
        ,'tourism.store.course.PersonTrainStore'

        //printstatus
        ,'tourism.store.combo.PrintStatusStore'

        //printcardstatus
        ,'tourism.store.combo.PrintCardStatusStore'
        ,'tourism.store.printcard.PrintCardGuideStore'

        //chart
        ,'tourism.store.chart.ChartStore'

        //month
        ,'tourism.store.month.MonthStore'

        //committeeprogress
        ,'tourism.store.registration.CommitteeProgressStore'

        //guidedetail
        ,'tourism.store.guidedetail.combo.EducationLevelStore'
        ,'tourism.store.guidedetail.combo.AmateurLevelStore'
        ,'tourism.store.guidedetail.combo.ExperienceYearStore'
        ,'tourism.store.guidedetail.combo.WorkTypeStore'
        ,'tourism.store.guidedetail.combo.SalaryNameStore'
        ,'tourism.store.guidedetail.combo.CharacterTypeNameStore'
        ,'tourism.store.guidedetail.combo.ClubNameStore'

        ,'tourism.store.guidedetail.grid.GuideForeignLanguageStore'
        ,'tourism.store.guidedetail.combo.LanguageLevelStore'
        ,'tourism.store.guidedetail.grid.GuideDetailStore'
    ]
});
