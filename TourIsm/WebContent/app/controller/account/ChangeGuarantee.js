Ext.define('tourism.controller.account.ChangeGuarantee', {
	extend : 'Ext.app.Controller',
    refs: [

        {
            ref: '#account-business-change-guarantee-grid',
            selector: '#account-business-change-guarantee-grid'
        }
        ,
        {
            ref: '#account-business-change-guarantee-formsearch',
            selector: 'panel'
        }  
         ,
        {
            ref: '#account-business-guarantee-changeguarantee-form',
            selector: 'panel'
        }  
        
        
    ],	 
	init : function (application) {

        this.control({
        
            '#account-business-change-guarantee-grid actioncolumn': {
                itemclick: this.handleActionColumn
            },
            '#account-business-change-guarantee-formsearch button[action=searchLicenseChangeGuarantee]': {
                click: this.searchGuarantee
            },
            '#account-business-guarantee-changeguarantee-form button[action=saveChangeGuaranteeBtn]': {
                click: this.saveChangeGuarantee
            }
            

            
        });

		
	},
  searchGuarantee: function(btn)
  {
    
    var formSearch = btn.up('form');
    var values = formSearch.getValues();

    var dataParams = this.normalizeData(values);



    var grid = Ext.ComponentQuery.query('#account-business-change-guarantee-grid')[0];

     dataParams.page = '1';
     dataParams.start = '0';
     dataParams.limit = grid.getStore().pageSize;

    grid.getStore().load({
        params: dataParams,
        callback: function(records, operation, success) {
   
        },
        scope: this
    });
  }
  ,handleActionColumn: function(column, action, grid, rowIndex, colIndex, model, node)
  {
      var win = Ext.create('tourism.view.account.guarantee.ChangeGuaranteeWindow');
      win.show();

      var form = win.down('account-guarantee-changeguarantee-form');
      form.loadRecord(model);


      if(model.get('cashAccountMny') > 0)
      {
        var cashguaranteeFieldSet = form.down('account-cashguarantee-fieldser');
        cashguaranteeFieldSet.expand();


        var masBankByCashAccountBankId = form.getForm().findField('masBankByCashAccountBankId');
          masBankByCashAccountBankId.getStore().on('load',function(store, records, successful, eOpts){
            masBankByCashAccountBankId.setValue(model.get('masBankByCashAccountBankId'));
          },this,{single:true});
        masBankByCashAccountBankId.getStore().load();


      }

       if(model.get('bankGuaranteeMny') > 0)
      {
        var bankguaranteeFieldSet = form.down('account-bankguarantee-fieldser');
        bankguaranteeFieldSet.expand();


        var masBankByBankGuaranteeBankId = form.getForm().findField('masBankByBankGuaranteeBankId');
          masBankByBankGuaranteeBankId.getStore().on('load',function(store, records, successful, eOpts){
            masBankByBankGuaranteeBankId.setValue(model.get('masBankByBankGuaranteeBankId'));
          },this,{single:true});
        masBankByBankGuaranteeBankId.getStore().load();
      }      
                 
       if(model.get('governmentBondMny') > 0)
      {
        var governmentboundFieldSet = form.down('account-governmentbound-fieldser');
        governmentboundFieldSet.expand();
      } 
  }
  ,  normalizeData: function(dataObj)
  {
      // remove null data;
     for (field in dataObj) 
      {
        if (Ext.isEmpty(dataObj[field])) 
        {
          delete dataObj[field];
        }
        
      }

      return dataObj;
  }
  ,saveChangeGuarantee: function(btn)
  {
    var win = btn.up('window');

    var form = win.down('account-guarantee-changeguarantee-form');

      if (!form.isValid()) {

          var me = form,
          errorCmp, fields, errors;

          fields = me.getForm().getFields();
          errors = '';
          fields.each(function(field) {
              Ext.Array.forEach(field.getErrors(), function(error) {
                  // errors.push({name: field.getFieldLabel(), error: error});
                  errors += field.getFieldLabel()  +'<br>';
              });
          });
          // Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', errors);

          Ext.Msg.show({
             title:'กรุณาตรวจสอบข้อมูล',
             msg: errors,
             buttons: Ext.Msg.OK,
             icon: Ext.Msg.ERROR
          });
          return;
      }
        
        var values = form.getValues();


        var dataParams = this.normalizeData(values);

        var receiveName = form.getForm().findField('receiveName').getRawValue();
        dataParams.receiveName = receiveName;

         // fsubmit.submit({
       form.load({
            url: 'business/refundguarantee/change/save',
            method : 'POST',
            waitMsg: 'กรุณารอสักครู่..',
            params: dataParams,
            success: function(form, action) {

              var dataObj = action.result.data;
             
              // Ext.Msg.alert('', 'บันทึกข้อมูล');

            var noti = Ext.create('widget.uxNotification', {
              // title: 'Notification',
              position: 'tr',
              manager: 'instructions',
              // cls: 'ux-notification-light',
              // iconCls: 'ux-notification-icon-information',
              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
              closable: false,
              autoCloseDelay: 4000,
              width: 300,
              slideBackDuration: 500,
              slideInAnimation: 'bounceOut',
              slideBackAnimation: 'easeIn'
            });
            noti.show();

              var grid = Ext.ComponentQuery.query('#account-business-change-guarantee-grid')[0];
              var store = grid.getStore();
              store.reload();
             
              win.close();
        
            },
            failure: function(form, action) {
              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
            },
            scope: this 
        });
  }
 
});