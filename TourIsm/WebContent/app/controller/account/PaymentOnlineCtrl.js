Ext.define('tourism.controller.account.PaymentOnlineCtrl', {
	extend : 'Ext.app.Controller',

  

    refs: [    
    
       {  
            ref: '#account-payment-online-grid-001',
            selector: '#account-payment-online-grid-001'
        } ,{
            ref: '#account-payment-online-form-search-001',
            selector: 'panel'
        },{
            ref: '#receipt-account-payment-online-addedit-form-01',
            selector: 'panel'
        }
       
    ],	 
	init : function (application) {

        this.control({

             '#account-payment-online-form-search-001 button[action=searchOnlinePayment]': {
                click: this.searchOnlinePayment
            },'#account-payment-online-grid-001 actioncolumn': {
                itemclick: this.handleActionColumn
            }, '#receipt-account-payment-online-addedit-form-01 button[action=saveReceiptBtn]': {
                click: this.saveReceiptAccount
            }
            
        });

		
	}
	,searchOnlinePayment: function(btn)
	{
	  var form = btn.up('form'),
	  values = form.getValues();
	  var dataParams = this.normalizeData(values);
	
	  var grid = Ext.ComponentQuery.query('#account-payment-online-grid-001')[0];
	  var store = grid.getStore();
	  
	    store.load({
	        params: dataParams,
	        callback: function(records, operation, success) {
	         
	        },
	        scope: this
	    }); 
	
	}
	//End Oat Add
	
  ,normalizeData: function(dataObj)
  {
      // remove null data;
     for (field in dataObj) 
      {
        if (Ext.isEmpty(dataObj[field])) 
        {
          delete dataObj[field];
        }
        
      }

      return dataObj;
  }
  ,handleActionColumn : function(column, action, grid, rowIndex, colIndex, model, node) {

      var receiptWin = Ext.create('tourism.view.account.payment.PaymentOnlineAddEditWindow',{
          animateTarget: column.el,
              actionMode: 'guarantee',
              roleAction: 'account',
              receiptGrid: grid
        });
        
        receiptWin.show();

      var formReceipt = receiptWin.down('receipt-account-payment-online-addedit-form');


      
      if(model.get('traderType') == 'G')
      {
        //remove tab 1
        
        var guaranteeForm =  formReceipt.down('guarantee-account-form');
        formReceipt.down('tabpanel').remove(guaranteeForm);
      }
   
      
      var fsubmit = formReceipt.getForm();
      
      fsubmit.load({
            url: 'tourleader/account/registration/readPaymentOnlineDetail',
            method : 'POST',
            waitMsg: 'กรุณารอสักครู่..',
            params: {receiptId: model.get('receiptId')},
            success: function(form, action) {
              
                var dataObj = action.result.data;
                // formReceipt.loadRecord(dataObj);
                
              // console.log(dataObj);
              
                 //นำข้อมูลจากแถบบันทึกข้อมูลใบเสร็จรับเงิน ในช่อง ชื่อ-สกุล ผุ้จ่ายเงิน(บริษัท) ลงในแถบบันทึกข้อมูลหลักประกันช่องชื่อบัญชี(ทั้งเงินสดและหนังสือค้ำประกันธนาคาร) 
               try{
                formReceipt.getForm().findField('cashAccountName').setValue(dataObj.receiptName);
                formReceipt.getForm().findField('bankGuaranteeName').setValue(dataObj.receiptName);
               } catch(e) {

               }
                
                
                if(dataObj.cashAccountMny > 0)
                {
                  var cashguaranteeFieldSet = formReceipt.down('account-cashguarantee-fieldser');
                  cashguaranteeFieldSet.expand();

                      var masBankByCashAccountBankId = formReceipt.getForm().findField('masBankByCashAccountBankId');
                      masBankByCashAccountBankId.getStore().on('load',function(store, records, successful, eOpts){
                        masBankByCashAccountBankId.setValue(dataObj.masBankByCashAccountBankId);
                      },this,{single:true});
                    masBankByCashAccountBankId.getStore().load();
                }

                 if(dataObj.bankGuaranteeMny > 0)
                {
                  var bankguaranteeFieldSet = formReceipt.down('account-bankguarantee-fieldser');
                  bankguaranteeFieldSet.expand();

                    var masBankByBankGuaranteeBankId = formReceipt.getForm().findField('masBankByBankGuaranteeBankId');
                      masBankByBankGuaranteeBankId.getStore().on('load',function(store, records, successful, eOpts){
                        masBankByBankGuaranteeBankId.setValue(dataObj.masBankByBankGuaranteeBankId);
                      },this,{single:true});
                    masBankByBankGuaranteeBankId.getStore().load();
                }      
                           
                 if(dataObj.governmentBondMny > 0)
                {
                  var governmentboundFieldSet = formReceipt.down('account-governmentbound-fieldser');
                  governmentboundFieldSet.expand();
                }        

     
            },
            failure: function(form, action) {
              Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
            },
            scope: this // this = fsubmit
        });

      var receiptDetailGrid = receiptWin.down('account-receipt-payment-receiptdetail-grid');

      receiptDetailGrid.getStore().load({
        params: {receiptId: model.get('receiptId')},
        callback: function(records, operation, success) {
           
           
        },
        scope: this
      }); 
          
      
  }
  ,saveReceiptAccount: function(btn)
  {
    var receiptWin = btn.up('window');
    var receiptDetailGrid = receiptWin.down('account-receipt-payment-receiptdetail-grid');
    var listActModel = new Array();



    receiptDetailGrid.getStore().each(function(model){

        
            listActModel.push({
              receiptDetailId: model.get('receiptDetailId'),
              feeName: model.get('feeName'),
              feeMny: model.get('feeMny'),
              feeId: model.get('feeId')
            });
        
        
    });

    if(listActModel.length < 0)
    {
        Ext.Msg.alert('เกิดข้อผิดพลาด', 'ไม่มีจำนวนเงินค่าธรรมเนียม');
        return;
    }


    var formReceipt = receiptWin.down('receipt-account-payment-online-addedit-form');

      if (!formReceipt.isValid()) {

          var me = formReceipt,
          errorCmp, fields, errors;

          fields = me.getForm().getFields();
          errors = '';
          fields.each(function(field) {
              Ext.Array.forEach(field.getErrors(), function(error) {
                  // errors.push({name: field.getFieldLabel(), error: error});
                  errors += field.getFieldLabel()  +'<br>';
              });
          });
          // Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', errors);

          Ext.Msg.show({
             title:'กรุณาตรวจสอบข้อมูล',
             msg: errors,
             buttons: Ext.Msg.OK,
             icon: Ext.Msg.ERROR
          });
          return;
      }
      
      try{
        var formGuarantee = formReceipt.down('guarantee-account-form');
      
          //chk1
          var formCashguarantee = formGuarantee.down('account-cashguarantee-fieldser');
          //chk2
          var formBankguarantee = formGuarantee.down('account-bankguarantee-fieldser');
          //chk3
          var formGovernmentbound = formGuarantee.down('account-governmentbound-fieldser');
          
          if(formCashguarantee.collapsed == true && formBankguarantee.collapsed == true && formGovernmentbound.collapsed == true)
        {
            Ext.Msg.show({
                  title:'กรุณาตรวจสอบข้อมูล',
                  msg: 'กรุณาเลือกหลักประกัน',
                  buttons: Ext.Msg.OK,
                  icon: Ext.Msg.ERROR
               });
               return;
        }
      }catch(e){

      }



        var actStr =  Ext.encode(listActModel);
        var values = formReceipt.getValues();


        var dataParams = this.normalizeData(values);

        dataParams.receiptDetail = actStr;

        var receiveOfficerName = formReceipt.getForm().findField('receiveOfficerName').getRawValue();
        var authority = formReceipt.getForm().findField('authority').getRawValue();

        if(dataParams.traderType == 'B')
        {
          var receiveName = formReceipt.getForm().findField('receiveName').getRawValue();
          dataParams.receiveName = receiveName;
        }

        dataParams.receiveOfficerName = receiveOfficerName;
        dataParams.authority = authority;

         // fsubmit.submit({
       formReceipt.load({
            url: 'tourleader/account/registration/savePayment',
            method : 'POST',
            waitMsg: 'กรุณารอสักครู่..',
            params: dataParams,
            success: function(form, action) {

              var dataObj = action.result.data;
             
              // Ext.Msg.alert('', 'บันทึกข้อมูล');

            var noti = Ext.create('widget.uxNotification', {
              // title: 'Notification',
              position: 'tr',
              manager: 'instructions',
              // cls: 'ux-notification-light',
              // iconCls: 'ux-notification-icon-information',
              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
              closable: false,
              autoCloseDelay: 4000,
              width: 300,
              slideBackDuration: 500,
              slideInAnimation: 'bounceOut',
              slideBackAnimation: 'easeIn'
            });
           
            var receiptId = dataObj.receiptId;
           
           noti.on('close',function(){
            var reqUrl = "business/printReceipt/slip/read/?receiptId="+receiptId;
            window.open(reqUrl,"พิมพ์ใบเสร็จรับเงิน","toolbar=0 width=900 height=600");        
           });

            noti.show();

              var grid = receiptWin.receiptGrid;
              var store = grid.getStore();

              store.reload();
             
              receiptWin.close();
        
            },
            failure: function(form, action) {
              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
            },
            scope: this 
        });

  }
  
  
});