Ext.define('tourism.controller.account.ReceiptAccountCtrl', {
	extend : 'Ext.app.Controller',

  

    refs: [    
    
       {  
            ref: '#account-business-approvalprocess-grid',
            selector: '#account-business-account-grid'
        } 
         ,{  
            ref: '#account-guide-account-grid',
            selector: '#account-guide-account-grid'
        } 
         ,{
            ref: '#account-business-account-formsearch',
            selector: 'panel'
        }
         ,{
            ref: '#account-guide-account-formsearch',
            selector: 'panel'
        }
        
        ,{
            ref: '#account-registration-verification-form',
            selector: 'panel'
        }
        
        ,{
            ref: 'receipt-account-addedit-form',
            selector: 'panel'
        }
        ,{
            ref: '#receipt-account-payment-form',
            selector: 'panel'
        }
      
       
    ],	 
	init : function (application) {

        this.control({

             '#account-business-account-formsearch button[action=searchLicenseBetweenRegistration]': {
                click: this.searchBusinessLicense
            },
             '#account-guide-account-formsearch button[action=searchLicenseBetweenRegistration]': {
                click: this.searchGuideLicense
            },           
            '#account-business-account-grid actioncolumn': {
                itemclick: this.handleActionColumn
            },
            '#account-guide-account-grid actioncolumn': {
                itemclick: this.handleActionColumn
            },        
            
            'receipt-account-addedit-form button[action=saveReceiptBtn]': {
                click: this.saveReceiptAccount
            }
            
             //Oat Add 16/02/58
            ,'#account-cancle-account-formsearch button[action=searchLicenseWillCancle]': {
                click: this.searchCancleLicense
            },           
            '#account-cancle-account-grid actioncolumn': {
                itemclick: this.handleActionColumn
            },
            'receipt-account-cancle-addedit-form button[action=saveReceiptCancleBtn]': {
                click: this.saveReceiptAccountCancle
            },
            '#receipt-account-payment-form button[action=savePayment]': {
                click: this.savePayment
            }
            
            //End Oat Add
        });

		
	}

    //Oat Add 16/02/58
	,searchCancleLicense: function(btn)
	{
	  var form = btn.up('form'),
	  values = form.getValues();
	  var dataParams = this.normalizeData(values);
	  dataParams.traderType = 'B';
	
	  var grid = Ext.ComponentQuery.query('#account-cancle-account-grid')[0];
	  var store = grid.getStore();
	
	   dataParams.page = '1';
	   dataParams.start = '0';
	   dataParams.limit = store.pageSize;
	  
	    store.load({
	        params: dataParams,
	        callback: function(records, operation, success) {
	            // //console.log(success);
	      	  console.log(records);
	        },
	        scope: this
	    }); 
	
	}
	//End Oat Add
	
  ,normalizeData: function(dataObj)
  {
      // remove null data;
     for (field in dataObj) 
      {
        if (Ext.isEmpty(dataObj[field])) 
        {
          delete dataObj[field];
        }
        
      }

      return dataObj;
  }
  ,handleActionColumn : function(column, action, grid, rowIndex, colIndex, model, node) {

          
          if(action){
              switch(action){
              
            //Oat Add 23/02/58
              case 'openfeecanclewindow':

            	  
            	  var receiptWin = Ext.create('tourism.view.refundguarantee.account.ReceiptAccountCancleAddEditWindow',{
            		  animateTarget: column.el,
                      actionMode: 'guarantee',
                      roleAction: 'account',
                      receiptGrid: grid
            	  });
            	  
            	  receiptWin.show();

              var formReceipt = receiptWin.down('receipt-account-cancle-addedit-form');

              console.log(model);
              
                var guaranteeForm =  formReceipt.down('guarantee-account-form');
                formReceipt.down('tabpanel').remove(guaranteeForm);
              
              var fsubmit = formReceipt.getForm();
              
              fsubmit.load({
                    url: 'tourleader/account/registration/readreceiptcancle',
                    method : 'POST',
                    waitMsg: 'กรุณารอสักครู่..',
                    params: {regId:  model.get('regId'),receiptId: model.get('receiptId'),traderType: model.get('traderType'),registrationType: model.get('registrationType')},
                    success: function(form, action) {
                    	
                        var dataObj = action.result.data;
                        // formReceipt.loadRecord(dataObj);
                        
                    	 console.log(dataObj);
                    },
                    failure: function(form, action) {
                      Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
                    },
                    scope: this // this = fsubmit
                });

              var receiptDetailGrid = receiptWin.down('account-receipt-grid');

              receiptDetailGrid.getStore().load({
                params: {receiptId: model.get('receiptId'),traderType: model.get('traderType'),registrationType: 'R', regId: model.get('regId')},
                callback: function(records, operation, success) {
                   
                   
                },
                scope: this
              }); 
            	  
            	  // receiptWin.loadFormRecord(model);
            	  
              break;
              //End Oat Add

                  case 'openfeewindow':

                	  
                	  var receiptWin = Ext.create('tourism.view.account.ReceiptAccountAddEditWindow',{
                		  animateTarget: column.el,
                          actionMode: 'guarantee',
                          roleAction: 'account',
                          receiptGrid: grid
                	  });
                	  
                	  receiptWin.show();

                  var formReceipt = receiptWin.down('receipt-account-addedit-form');

            
                  
                  if(model.get('traderType') == 'G')
                  {
                    //remove tab 1
                    
                    var guaranteeForm =  formReceipt.down('guarantee-account-form');
                    formReceipt.down('tabpanel').remove(guaranteeForm);
                  }
                  //Oat Edit
                  //เพิ่มข้อมูลลงในช่องสาขา ในแถบบันทึกข้อมูลหลักประกัน เงินสด
                  else if(model.get('orgId') == '1')
                  {
                	  formReceipt.getForm().findField('cashAccountBankBranchName').setValue("ปทุมวัน");
                  }
                  else if(model.get('orgId') == '7')
                  {
                    formReceipt.getForm().findField('cashAccountBankBranchName').setValue("ปทุมวัน");
                  }
                  
                  //End Oat Edit
                  
                  var fsubmit = formReceipt.getForm();
                  
                  fsubmit.load({
                        url: 'tourleader/account/registration/readreceipt',
                        method : 'POST',
                        waitMsg: 'กรุณารอสักครู่..',
                        params: {regId:  model.get('regId'),receiptId: model.get('receiptId'),traderType: model.get('traderType'),registrationType: model.get('registrationType')},
                        success: function(form, action) {
                        	
                            var dataObj = action.result.data;
                            // formReceipt.loadRecord(dataObj);
                            
                        	// console.log(dataObj);
                        	
                        	//Oat Edit
                            //นำข้อมูลจากแถบบันทึกข้อมูลใบเสร็จรับเงิน ในช่อง ชื่อ-สกุล ผุ้จ่ายเงิน(บริษัท) ลงในแถบบันทึกข้อมูลหลักประกันช่องชื่อบัญชี(ทั้งเงินสดและหนังสือค้ำประกันธนาคาร) 
                           try{
                            formReceipt.getForm().findField('cashAccountName').setValue(dataObj.receiptName);
                            formReceipt.getForm().findField('bankGuaranteeName').setValue(dataObj.receiptName);
                           } catch(e) {

                           }
                            
                          	
                            //End Oat Edit
                          	
                            if(dataObj.cashAccountMny > 0)
                            {
                              var cashguaranteeFieldSet = formReceipt.down('account-cashguarantee-fieldser');
                              cashguaranteeFieldSet.expand();

                                  var masBankByCashAccountBankId = formReceipt.getForm().findField('masBankByCashAccountBankId');
                                  masBankByCashAccountBankId.getStore().on('load',function(store, records, successful, eOpts){
                                    masBankByCashAccountBankId.setValue(dataObj.masBankByCashAccountBankId);
                                  },this,{single:true});
                                masBankByCashAccountBankId.getStore().load();
                            }

                             if(dataObj.bankGuaranteeMny > 0)
                            {
                              var bankguaranteeFieldSet = formReceipt.down('account-bankguarantee-fieldser');
                              bankguaranteeFieldSet.expand();

                                var masBankByBankGuaranteeBankId = formReceipt.getForm().findField('masBankByBankGuaranteeBankId');
                                  masBankByBankGuaranteeBankId.getStore().on('load',function(store, records, successful, eOpts){
                                    masBankByBankGuaranteeBankId.setValue(dataObj.masBankByBankGuaranteeBankId);
                                  },this,{single:true});
                                masBankByBankGuaranteeBankId.getStore().load();
                            }      
                                       
                             if(dataObj.governmentBondMny > 0)
                            {
                              var governmentboundFieldSet = formReceipt.down('account-governmentbound-fieldser');
                              governmentboundFieldSet.expand();
                            }        

                 
                        },
                        failure: function(form, action) {
                          Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
                        },
                        scope: this // this = fsubmit
                    });

                  var receiptDetailGrid = receiptWin.down('account-receipt-grid');

                  receiptDetailGrid.getStore().load({
                    params: {receiptId: model.get('receiptId'),traderType: model.get('traderType'),registrationType: model.get('registrationType'), regId: model.get('regId')},
                    callback: function(records, operation, success) {
                       
                       
                    },
                    scope: this
                  }); 
                	  
                	  // receiptWin.loadFormRecord(model);
                	  
                  break;

                  case 'approvalprogress':

                    var progressWin = Ext.create('tourism.view.registrationprogress.RegistrationProgressWindow',{
                      id: 'account-registrationprogress-ProgressWindow',
                      animateTarget: column.el 
                      
                     });

                    progressWin.on('show', function(win){


                      var grid = win.down('registrationprogress-grid');
               
                      grid.getStore().load({
                        params: {regId: model.get('regId')},
                        callback: function(records, operation, success) {
                            
                        },
                        scope: this
                      }); 

                    },this);

                    progressWin.show();

                  break;
                  
                  case 'openPaymentwindow':
                	  
                	  
                	  var paymentWin = Ext.create('tourism.view.account.payment.PaymentWindow',{
                		  animateTarget: column.el,
                          actionMode: 'guarantee',
                          roleAction: 'account',
                          receiptGrid: grid,
                          feePaidStatus: model.get('feePaidStatus')
                	  });
                	  
                	  paymentWin.show();
                	  
                	  var form = paymentWin.down('receipt-account-payment-form');
               
                	  
                	  form.getForm().findField('totalFee').setValue(model.get('totalFee'));
                	  form.getForm().findField('totalFeePaid').setValue(model.get('totalFeePaid'));
                	  form.getForm().findField('paymentRecId').setValue(model.get('paymentRecId'));
                    if(!Ext.isEmpty(model.get('traderName'))){
                      form.getForm().findField('traderName').setValue(model.get('traderName'));
                    } else{
                      form.getForm().findField('traderName').setValue(model.get('traderOwnerName'));
                    }
                    
                    form.getForm().findField('paymentDate').setValue(model.get('paymentDate'));
                	  
                  break;

                case 'approvaleditregistration':

                    if(model.get('traderType') == 'B')
                    {
                      var winInfo = Ext.create('tourism.view.information.business.InfoBusinessWindow',{
                        id: 'account-information-business-registration-infowindow',
                        animateTarget: column.el,
                        actionMode: 'imformation',
                        roleAction: 'account'
                      });

                       winInfo.show();

                      winInfo.loadFormRecord(model);


                      winInfo.showConutry(model);

                      
                    }

                    if(model.get('traderType') == 'G')
                    {
                      var winInfo = Ext.create('tourism.view.information.guide.InfoGuideWindow',{
                          id: 'account-information-guide-registration-infowindow',
                          animateTarget: column.el,
                            actionMode: 'information',
                            roleAction: 'account'
                        }).show();
                      
                      ////console.log(model);

                        winInfo.loadFormRecord(model);
                    }
                   
                                    
                  break;
              }
          }
      
  }
  
  //Oat Add 23/02/58
  ,saveReceiptAccountCancle: function(btn)
  {
    var receiptWin = btn.up('window');
    var receiptDetailGrid = receiptWin.down('account-receipt-grid');
    var listActModel = new Array();

    receiptDetailGrid.getStore().each(function(model){

        
            listActModel.push({
              receiptDetailId: model.get('receiptDetailId'),
              feeName: model.get('feeName'),
              feeMny: model.get('feeMny'),
              feeId: model.get('feeId')
            });
        
        
    });

    if(listActModel.length < 0)
    {
        Ext.Msg.alert('เกิดข้อผิดพลาด', 'ไม่มีจำนวนเงินค่าธรรมเนียม');
        return;
    }


    var formReceipt = receiptWin.down('receipt-account-cancle-addedit-form');

      if (!formReceipt.isValid()) {

          var me = formReceipt,
          errorCmp, fields, errors;

          fields = me.getForm().getFields();
          errors = '';
          fields.each(function(field) {
              Ext.Array.forEach(field.getErrors(), function(error) {
                  // errors.push({name: field.getFieldLabel(), error: error});
                  errors += field.getFieldLabel()  +'<br>';
              });
          });
          // Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', errors);

          Ext.Msg.show({
             title:'กรุณาตรวจสอบข้อมูล',
             msg: errors,
             buttons: Ext.Msg.OK,
             icon: Ext.Msg.ERROR
          });
          return;
      }

        var actStr =  Ext.encode(listActModel);
        var values = formReceipt.getValues();

        console.log(values);

        var dataParams = this.normalizeData(values);

        dataParams.receiptDetail = actStr;

        var receiveOfficerName = formReceipt.getForm().findField('receiveOfficerName').getRawValue();
        var authority = formReceipt.getForm().findField('authority').getRawValue();

//        if(dataParams.traderType == 'B')
//        {
//          var receiveName = formReceipt.getForm().findField('receiveName').getRawValue();
//          dataParams.receiveName = receiveName;
//        }

        dataParams.receiveOfficerName = receiveOfficerName;
        dataParams.authority = authority;
        
        console.log(dataParams);
        
        //Save
       formReceipt.load({
            url: 'tourleader/account/registration/savereceiptaccountcancle',
            method : 'POST',
            waitMsg: 'กรุณารอสักครู่..',
            params: dataParams,
            success: function(form, action) {

            var dataObj = action.result.data;
             
            var noti = Ext.create('widget.uxNotification', {
              position: 'tr',
              manager: 'instructions',
              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
              closable: false,
              autoCloseDelay: 4000,
              width: 300,
              slideBackDuration: 500,
              slideInAnimation: 'bounceOut',
              slideBackAnimation: 'easeIn'
            });
           
            var receiptId = dataObj.receiptId;
           
           noti.on('close',function(){
        	  var reqUrl = "business/printReceipt/slip/read/?receiptId="+receiptId;
        	  window.open(reqUrl,"พิมพ์ใบเสร็จรับเงิน","toolbar=0 width=900 height=600");        
           });

            noti.show();

              var grid = receiptWin.receiptGrid;
              var store = grid.getStore();

              store.reload();
             
              receiptWin.close();
        
            },
            failure: function(form, action) {
              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
            },
            scope: this 
        });
  }
  //End Oat Add 23/02/58
  
  ,saveReceiptAccount: function(btn)
  {
    var receiptWin = btn.up('window');
    var receiptDetailGrid = receiptWin.down('account-receipt-grid');
    var listActModel = new Array();



    receiptDetailGrid.getStore().each(function(model){

        
            listActModel.push({
              receiptDetailId: model.get('receiptDetailId'),
              feeName: model.get('feeName'),
              feeMny: model.get('feeMny'),
              feeId: model.get('feeId')
            });
        
        
    });

    if(listActModel.length < 0)
    {
        Ext.Msg.alert('เกิดข้อผิดพลาด', 'ไม่มีจำนวนเงินค่าธรรมเนียม');
        return;
    }


    var formReceipt = receiptWin.down('receipt-account-addedit-form');

      if (!formReceipt.isValid()) {

          var me = formReceipt,
          errorCmp, fields, errors;

          fields = me.getForm().getFields();
          errors = '';
          fields.each(function(field) {
              Ext.Array.forEach(field.getErrors(), function(error) {
                  // errors.push({name: field.getFieldLabel(), error: error});
                  errors += field.getFieldLabel()  +'<br>';
              });
          });
          // Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', errors);

          Ext.Msg.show({
             title:'กรุณาตรวจสอบข้อมูล',
             msg: errors,
             buttons: Ext.Msg.OK,
             icon: Ext.Msg.ERROR
          });
          return;
      }
      
      //Oat Add 03/02/58
      try{
        var formGuarantee = formReceipt.down('guarantee-account-form');
      
          //chk1
          var formCashguarantee = formGuarantee.down('account-cashguarantee-fieldser');
          //chk2
          var formBankguarantee = formGuarantee.down('account-bankguarantee-fieldser');
          //chk3
          var formGovernmentbound = formGuarantee.down('account-governmentbound-fieldser');
          
          if(formCashguarantee.collapsed == true && formBankguarantee.collapsed == true && formGovernmentbound.collapsed == true)
        {
            Ext.Msg.show({
                  title:'กรุณาตรวจสอบข้อมูล',
                  msg: 'กรุณาเลือกหลักประกัน',
                  buttons: Ext.Msg.OK,
                  icon: Ext.Msg.ERROR
               });
               return;
        }
      }catch(e){

      }
      //End Oat Add



        var actStr =  Ext.encode(listActModel);
        var values = formReceipt.getValues();


        var dataParams = this.normalizeData(values);

        dataParams.receiptDetail = actStr;

        var receiveOfficerName = formReceipt.getForm().findField('receiveOfficerName').getRawValue();
        var authority = formReceipt.getForm().findField('authority').getRawValue();

        if(dataParams.traderType == 'B')
        {
          var receiveName = formReceipt.getForm().findField('receiveName').getRawValue();
          dataParams.receiveName = receiveName;
        }

        


        dataParams.receiveOfficerName = receiveOfficerName;
        dataParams.authority = authority;
        


         // fsubmit.submit({
       formReceipt.load({
            url: 'tourleader/account/registration/savereceiptaccount',
            method : 'POST',
            waitMsg: 'กรุณารอสักครู่..',
            params: dataParams,
            success: function(form, action) {

              var dataObj = action.result.data;
             
              // Ext.Msg.alert('', 'บันทึกข้อมูล');

            var noti = Ext.create('widget.uxNotification', {
              // title: 'Notification',
              position: 'tr',
              manager: 'instructions',
              // cls: 'ux-notification-light',
              // iconCls: 'ux-notification-icon-information',
              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
              closable: false,
              autoCloseDelay: 4000,
              width: 300,
              slideBackDuration: 500,
              slideInAnimation: 'bounceOut',
              slideBackAnimation: 'easeIn'
            });
           
            var receiptId = dataObj.receiptId;
           
           noti.on('close',function(){
        	  var reqUrl = "business/printReceipt/slip/read/?receiptId="+receiptId;
        	  window.open(reqUrl,"พิมพ์ใบเสร็จรับเงิน","toolbar=0 width=900 height=600");        
           });

            noti.show();

              var grid = receiptWin.receiptGrid;
              var store = grid.getStore();

              store.reload();
             
              receiptWin.close();
        
            },
            failure: function(form, action) {
              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
            },
            scope: this 
        });
  }
 , searchBusinessLicense: function(btn)
  {
    var form = btn.up('form'),
    values = form.getValues();
    var dataParams = this.normalizeData(values);
    dataParams.traderType = 'B';

    var grid = Ext.ComponentQuery.query('#account-business-account-grid')[0];
    var store = grid.getStore();

     dataParams.page = '1';
     dataParams.start = '0';
     dataParams.limit = store.pageSize;
    
      store.load({
          params: dataParams,
          callback: function(records, operation, success) {
              // //console.log(success);
        	  console.log(records);
          },
          scope: this
      }); 

  }
  ,searchGuideLicense: function(btn)
  {
    var form = btn.up('form'),
    values = form.getValues();
    var dataParams = this.normalizeData(values);
    dataParams.traderType = 'G';

    var grid = Ext.ComponentQuery.query('#account-guide-account-grid')[0];
    var store = grid.getStore();

     dataParams.page = '1';
     dataParams.start = '0';
     dataParams.limit = store.pageSize;

      store.load({
          params: dataParams,
          callback: function(records, operation, success) {
              // //console.log(success);
          },
          scope: this
      }); 
  }
  ,savePayment:function(btn){
	  
      var form    = btn.up('form');
      var win = btn.up('window');

      var model = form.getRecord(); //Ext.data.Model
      var values = form.getValues();//object {xx: q,ss:ff}

      
      if (!form.isValid()) {

          var me = form,
          errorCmp, fields, errors;

          fields = me.getForm().getFields();
          errors = '';
          fields.each(function(field) {
              Ext.Array.forEach(field.getErrors(), function(error) {
                  // errors.push({name: field.getFieldLabel(), error: error});
                  errors += field.getFieldLabel()  +'<br>';
              });
          });
          // Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', errors);

          Ext.Msg.show({
             title:'กรุณาตรวจสอบข้อมูล',
             msg: errors,
             buttons: Ext.Msg.OK,
             icon: Ext.Msg.ERROR
          });
          return;
      }


      var fsubmit = form.getForm();
      var ctrl = this; // scope

    
      var dataParams = this.normalizeData(values);
     
       fsubmit.load({
            url: 'business/account/registration/updatefeepaid',
            method : 'POST',
            waitMsg: 'กรุณารอสักครู่..',
            params: dataParams,
            success: function(form, action) {

              var dataObj = action.result.data;
             
              // Ext.Msg.alert('', 'บันทึกข้อมูล');
            var noti = Ext.create('widget.uxNotification', {
              // title: 'Notification',
              position: 'tr',
              manager: 'instructions',
              // cls: 'ux-notification-light',
              // iconCls: 'ux-notification-icon-information',
              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
              closable: false,
              autoCloseDelay: 4000,
              width: 300,
              slideBackDuration: 500,
              slideInAnimation: 'bounceOut',
              slideBackAnimation: 'easeIn'
            });
            noti.show();

              var grid = win.receiptGrid;
              var store = grid.getStore();

              store.reload();
              win.close();
        
            },
            failure: function(form, action) {
              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
            },
            scope: this 
        });
  }
});