Ext.define('tourism.controller.admuser.AdmuserCtrl', {
	extend : 'Ext.app.Controller',
    refs: [

        {
            ref: '#admin-admuser-grid',
            selector: '#admin-admuser-grid'
        }
        ,
        {
            ref: '#admin-admuser-formsearch',
            selector: 'panel'
        }
        ,
        {
            ref: '#admin-admuser-addeditform',
            selector: 'panel'
        }
        ,{
            ref: '#usersignature-admuser-signatureform',
            selector: 'panel'
        }
        ,{
            ref: '#admuser-admUserLoginForm-form-001',
            selector: 'panel'
        }
        
        
        
       
    ],	 
	init : function (application) {

        this.control({
                       
          
            '#admin-admuser-grid actioncolumn': {
                itemclick: this.handleActionColumn
            },
             '#admin-admuser-grid button[action=adduser]': {
                click: this.adduser
            },
            '#admin-admuser-formsearch button[action=searchAdmUser]': {
                click: this.searchAdmUser
            }
          
            ,'#admin-admuser-addeditform button[action=saveadmuser]': {
                click: this.saveadmuser
            }
            ,'#usersignature-admuser-signatureform button[action=saveSignature]': {
                click: this.saveSignature
            }
            ,'#admuser-admUserLoginForm-form-001 button[action=admLogin]': {
                click: this.admLogin
            }

            
        });

		
	},
  adduser: function(btn){


    var win = Ext.create('tourism.view.admuser.AdmUserAddEditWindow',{
      roleAction: 'admin'
    });
    win.show();

    var admmenuGrid = win.down('#admin-admuser-admmenu-grid');
    var officerGropGrid = win.down('#admin-admuser-officergroup-grid');


   

    admmenuGrid.getStore().load({
        callback: function(records, operation, success) {
            // //console.log(success);
        },
        scope: this
    }); 

    officerGropGrid.getStore().load({
        callback: function(records, operation, success) {
            // //console.log(success);
        },
        scope: this
    });

   var formadmuser = win.down('#admin-admuser-addeditform');
   formadmuser.getForm().findField('changePassword').setVisible(false);

  },
  searchAdmUser: function(btn)
  {
    var formSearch = btn.up('form');
    var values = formSearch.getValues();

    var dataParams = this.normalizeData(values);

    var grid = Ext.ComponentQuery.query('#admin-admuser-grid')[0];

     dataParams.page = '1';
     dataParams.start = '0';
     dataParams.limit = grid.getStore().pageSize;

    grid.getStore().load({
        params: dataParams,
        callback: function(records, operation, success) {
   
        },
        scope: this
    });
  },
  handleActionColumn: function(column, action, grid, rowIndex, colIndex, model, node)
  {
    if(action){

          switch(action){

            case 'edituser':

              var win = Ext.create('tourism.view.admuser.AdmUserAddEditWindow',{
                roleAction: 'admin'
              });
              win.show();

              var formadmuser = win.down('#admin-admuser-addeditform');
              

              var admmenuGrid = win.down('#admin-admuser-admmenu-grid');
              var officerGropGrid = win.down('#admin-admuser-officergroup-grid');

              admmenuGrid.getStore().load({
                  params: {userId: model.get('userId')},
                  callback: function(records, operation, success) {
                     
                  },
                  scope: this
              }); 

              officerGropGrid.getStore().load({
                 params: {userId: model.get('userId')},
                 callback: function(records, operation, success) {
                      // //console.log(success);
                  },
                  scope: this
              });

              formadmuser.loadRecord(model);

              var changePassword = formadmuser.getForm().findField('changePassword');
              changePassword.setVisible(true);

              var loginPassword = formadmuser.getForm().findField('loginPassword');
              loginPassword.setVisible(false);
              loginPassword.allowBlank = true;
              var loginPassword2 = formadmuser.getForm().findField('loginPassword2');
              loginPassword2.setVisible(false);
              loginPassword2.allowBlank = true;
              
              var userLogin = formadmuser.getForm().findField('userLogin');
              userLogin.setDisabled(true);

              var orgIdCombo = formadmuser.getForm().findField('orgId');
              var posIdCombo = formadmuser.getForm().findField('posId');

              //console.log(orgIdCombo);
              //console.log(model.get('orgId'));

              orgIdCombo.getStore().on('load', function(){
                orgIdCombo.setValue(model.get('orgId'));
              });

              orgIdCombo.getStore().load();
              
              console.log(model.get('posId'));
              
              posIdCombo.getStore().on('load', function(){
            	  posIdCombo.setValue(model.get('posId'));
                });

              posIdCombo.getStore().load();

              var prefixIdCombo = formadmuser.getForm().findField('prefixId');
              prefixIdCombo.getStore().on('load', function(){
                prefixIdCombo.setValue(model.get('prefixId'));
              });
              prefixIdCombo.getStore().load();
              


              changePassword.on('change', function( chke, newValue, oldValue, eOpts ){

                if(chke.getValue() == '1')
                {
                   loginPassword.setVisible(true);
                   loginPassword.setValue('');
                   loginPassword2.setVisible(true);

                   loginPassword.allowBlank = false;
                   loginPassword.allowBlank = false;
                }
                else
                {
                   loginPassword.setVisible(false);
                   loginPassword2.setVisible(false);

                    loginPassword.allowBlank = true;
                   loginPassword.allowBlank = true;
                }
              },this);
            break;

             case 'deleteuser':
                grid.getStore().remove(model);
                grid.getStore().sync();
             break;

             case 'addsignature':
                  

                  var win = Ext.create('tourism.view.admuser.AdmUserSignatureWindow',{
                
                    roleAction: 'usersignature'
                  });
                  win.show();

                  var form = win.down('admuser-signature-form');

                  form.getForm().findField('userId').setValue(model.get('userId'));
                  form.getForm().findField('userFullName').setValue(model.get('userFullName'));

             break;

          }
    }

  },
  saveadmuser: function(btn)
  {
    var win = btn.up('window');

    var admuser = win.down('#admin-admuser-addeditform');

    var userLogin = admuser.getForm().findField('userLogin');
    userLogin.setDisabled(false);

    if (!admuser.isValid()) {

        var me = admuser,
        errorCmp, fields, errors;

        fields = me.getForm().getFields();
        errors = '';
        fields.each(function(field) {
            Ext.Array.forEach(field.getErrors(), function(error) {
                // errors.push({name: field.getFieldLabel(), error: error});
                errors += field.getFieldLabel()  +'<br>';
            });
        });
        // Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', errors);

        Ext.Msg.show({
           title:'กรุณาตรวจสอบข้อมูล',
           msg: errors,
           buttons: Ext.Msg.OK,
           icon: Ext.Msg.ERROR
        });
        return;
    }

    var admmenuGrid = win.down('#admin-admuser-admmenu-grid');
    var officerGropGrid = win.down('#admin-admuser-officergroup-grid');

    var listUserMenu = new Array();
    var listUserGroup = new Array();



    admmenuGrid.getStore().each(function(model){

        if(model.get('active'))
        {
            listUserMenu.push({
              groupId: model.get('groupId')
              ,groupName: model.get('groupName')
            });
        }
        
    });

    // if(listUserMenu.length < 0)
    // {
    //     Ext.Msg.alert('เกิดข้อผิดพลาด', 'ไม่ได้เลือกเมนูการทำงาน');
    //     return;
    // }

    officerGropGrid.getStore().each(function(model){

        if(model.get('active'))
        {
            listUserGroup.push({
              groupId: model.get('groupId')
              ,groupName: model.get('groupName')
            });
        }
        
    });


    var values = admuser.getValues();
    var dataParams = this.normalizeData(values);

    delete dataParams['loginPassword2'];
     

    if(listUserMenu.length > 0)
    {
        var userMenu =  Ext.encode(listUserMenu);
        dataParams.userMenu = userMenu;
    }
    if(listUserGroup.length > 0)
    {
        var userGroup =  Ext.encode(listUserGroup);
        dataParams.userGroup = userGroup;
    }
    admuser.load({
      url: 'admin/admuser/saveadmuser',
      method : 'POST',
      waitMsg: 'กรุณารอสักครู่..',
      params: dataParams,
      success: function(form, action) {

        var dataObj = action.result.data;
       
        // Ext.Msg.alert('', 'บันทึกข้อมูล');

      var noti = Ext.create('widget.uxNotification', {
        // title: 'Notification',
        position: 'tr',
        manager: 'instructions',
        // cls: 'ux-notification-light',
        // iconCls: 'ux-notification-icon-information',
        html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
        closable: false,
        autoCloseDelay: 4000,
        width: 300,
        slideBackDuration: 500,
        slideInAnimation: 'bounceOut',
        slideBackAnimation: 'easeIn'
      });
      noti.show();

        var grid = Ext.ComponentQuery.query('#admin-admuser-grid')[0];
        var store = grid.getStore();

        store.reload();
       
        win.close();
  
      },
      failure: function(form, action) {
        Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
      },
      scope: this 
    });
  },
  normalizeData: function(dataObj)
  {
      // remove null data;
     for (field in dataObj) 
      {
        if (Ext.isEmpty(dataObj[field])) 
        {
          delete dataObj[field];
        }
        
      }

      return dataObj;
  }
  ,saveSignature :function(btn)
  {
       var win = btn.up('window');
    var formEvidence = btn.up('form');

     if (!formEvidence.isValid()) {

          var me = formEvidence,
          errorCmp, fields, errors;

          fields = me.getForm().getFields();
          errors = '';
          fields.each(function(field) {
              Ext.Array.forEach(field.getErrors(), function(error) {
                  // errors.push({name: field.getFieldLabel(), error: error});
                  errors += field.getFieldLabel()  +'<br>';
              });
          });
          // Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', errors);

          Ext.Msg.show({
             title:'กรุณาตรวจสอบข้อมูล',
             msg: errors,
             buttons: Ext.Msg.OK,
             icon: Ext.Msg.ERROR
          });
          return;
      }


         // fsubmit.submit({
       formEvidence.submit({
            url: 'admin/admuser/signature/save',
            waitMsg: 'กรุณารอสักครู่...',
            method : 'POST',
          
            success: function(form, action) {

              var dataObj = action.result.data;
             
              // Ext.Msg.alert('', 'บันทึกข้อมูล');

            var noti = Ext.create('widget.uxNotification', {
              // title: 'Notification',
              position: 'tr',
              manager: 'instructions',
              // cls: 'ux-notification-light',
              // iconCls: 'ux-notification-icon-information',
              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
              closable: false,
              autoCloseDelay: 4000,
              width: 300,
              slideBackDuration: 500,
              slideInAnimation: 'bounceOut',
              slideBackAnimation: 'easeIn'
            });
            noti.show();

             
              win.close();
        
            },
            failure: function(form, action) {
              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
            },
            scope: this 
        });    
  }
  ,admLogin: function(btn)
  {
      var win = btn.up('window');
      var formLogin = btn.up('form');

      if (!formLogin.isValid()) {

        var me = formLogin,
        errorCmp, fields, errors;

        fields = me.getForm().getFields();
        errors = '';
        fields.each(function(field) {
            Ext.Array.forEach(field.getErrors(), function(error) {
                // errors.push({name: field.getFieldLabel(), error: error});
                errors += field.getFieldLabel()  +'<br>';
            });
        });
        // Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', errors);

        Ext.Msg.show({
           title:'กรุณาตรวจสอบข้อมูล',
           msg: errors,
           buttons: Ext.Msg.OK,
           icon: Ext.Msg.ERROR
        });
        return;
      }


      var passwordField = formLogin.getForm().findField('loginPassword');
      var hash = CryptoJS.SHA256(passwordField.getValue());
      passwordField.setValue(hash.toString(CryptoJS.enc.Hex));

       // fsubmit.submit({
      formLogin.submit({
          url: 'admin/auth/user/dologin',
          waitMsg: 'กรุณารอสักครู่...',
          method : 'POST',
        
          success: function(form, action) {

            var dataObj = action.result;

            if(!dataObj.success)
            {
              Msg.error('กรุณาตรวจสอบข้อมูล');
            }
            else
            {
              win.close();
            }
           
            

          },
          failure: function(form, action) {
            Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
          },
          scope: this 
      });   
  }
  
});