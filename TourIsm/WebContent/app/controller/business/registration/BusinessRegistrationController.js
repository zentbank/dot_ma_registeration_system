Ext.define('tourism.controller.business.registration.BusinessRegistrationController', {
	extend : 'Ext.app.Controller',

   	stores: ['tourism.store.business.registration.BusinessRegistrationStore'],

    models: ['tourism.model.business.registration.BusinessRegistrationModel'],

    views: [
      'tourism.view.business.registration.RegistrationGrid',
      'tourism.view.business.registration.BusinessRegistrationAddEditForm',
      'tourism.view.business.registration.RegistrationFromSearch',
      'tourism.view.window.verification.VerificationForm',
      'tourism.view.information.business.InfoBusinessWindow',
      'tourism.view.info.SearchLicensePanel',
      'tourism.view.business.registration.BusinessNameSearchWindow',
      'tourism.view.business.registration.person.IndividualPersonFieldSet'
    ],

    refs: [    
    	 {  
            ref: 'business_registrationgrid',
            selector: 'business_registrationgrid'
        } 
        ,{
            ref: 'form-business-registration-addedit',
            selector: 'panel'
        }
        ,{
            ref: '#key-form-business-registration-addedit',
            selector: 'panel'
        }
        
        ,{
            ref: 'business_registrationfromsearch',
            selector: 'panel'
        }
        ,{
            ref: '#business-registration-verification-form',
            selector: 'panel'
        }
        ,{
        	ref: 'info-license-searchlicensepanel',
        	selector: 'panel'
        }
        ,{
        	ref: 'business-registration-person-individualpersonfieldset',
        	selector: 'panel'
        }
    ],	 
	init : function (application) {

        this.control({
       

            'business_registrationgrid button[action=add]': {
            	click: this.showBusinessRegistrationWindow
            },
            
   
            '#key-form-business-registration-addedit button[action=saveBusinessRegistration]': {
                click: this.saveBusinessRegistration
            },
            'business_registrationfromsearch button[action=searchTraderBetweenRegistration]': {
                click: this.searchTraderBetweenRegistration
            }, 
            'business_registrationgrid actioncolumn': {
                itemclick: this.handleActionColumn
            },
            '#business-registration-verification-form button[action=saveVerification]': {
                click: this.saveVerification
            },
            'form-business-registration-addedit button[action=ShowSearchNameSame]': {
                click: this.ShowSearchNameSame
            },
            'form-business-registration-addedit button[action=infoDetail]': {
                click: this.infoDetail
            },
            'form-business-registration-addedit button[action=loadDataFromIdCard]': {
                click: this.loadDataFromIdCard
            }
            ,
            'form-business-registration-addedit button[action=searchUserFromIDCardOnline]': {
                click: this.searchUserFromIDCardOnline
            }
            
        });

	},infoDetail: function(btn)
	{
		var form    = btn.up('form'); 
		
		var values = form.getValues();
		var identityNo = values.identityNo;
		
		if(!Ext.isEmpty(values.personTypeCorporate))
		{
			values.personType = 'C';
		}else
		{
			values.personType = 'I';
		}
		var personType = values.personType;
		var progressWin = Ext.create('tourism.view.committeeprogress.CommitteeProgressWindow',{
            id: 'business-committeeprogress-ProgressWindow',
            animateTarget: btn.el 
            
           });

          progressWin.on('show', function(win){

             var grid = win.down('committeeprogress-grid');
          
            grid.getStore().load({
              params: {personIdentityNo: identityNo,personType:personType},
              callback: function(records, operation, success) {
                  
              },
              scope: this
            }); 

          },this);

          progressWin.show();
		
	},ShowSearchNameSame: function(btn){
		var edit = Ext.create('tourism.view.business.registration.BusinessNameSearchWindow',{
	          animateTarget: btn.el
	         }).show();
	},
	showBusinessRegistrationWindow: function(btn, model) {

        
         var edit = Ext.create('tourism.view.business.registration.registrationType.RegistrationTypeAddEditWindow',{
          animateTarget: btn.el,
          
         }).show();
        
  },
  saveBusinessRegistration: function(btn, model)
  {
      var form    = btn.up('form');
      var win = btn.up('window');

      var model = form.getRecord(); //Ext.data.Model
      var values = form.getValues();//object {xx: q,ss:ff}

      
      if (!form.isValid()) {

          var me = form,
          errorCmp, fields, errors;

          fields = me.getForm().getFields();
          errors = '';
          fields.each(function(field) {
              Ext.Array.forEach(field.getErrors(), function(error) {
                  // errors.push({name: field.getFieldLabel(), error: error});
                  errors += field.getFieldLabel()  +'<br>';
              });
          });
          // Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', errors);

          Ext.Msg.show({
             title:'กรุณาตรวจสอบข้อมูล',
             msg: errors,
             buttons: Ext.Msg.OK,
             icon: Ext.Msg.ERROR
          });
          return;
      }


      var fsubmit = form.getForm();
      var ctrl = this; // scope

      if(!Ext.isEmpty(values.personTypeCorporate))
      {// กรรีที่เป็น บริษัท
        values.personType = 'C';
        values.prefixId = values.prefixIdCorp;
        values.firstName = values.firstNameCorp;
        values.identityNo = values.identityNoCorp;
        values.identityDate = values.identityDateCorp;
        values.provinceId = values.provinceIdCorp;        
      }
      else
      {
        values.personType = 'I';
      }

      var dataParams = this.normalizeData(values);
     
      // fsubmit.submit({
       fsubmit.load({
            url: 'business/registration/saveAllRegistration',
            method : 'POST',
            waitMsg: 'กรุณารอสักครู่..',
            params: dataParams,
            success: function(form, action) {

              var dataObj = action.result.data;
             
              // Ext.Msg.alert('', 'บันทึกข้อมูล');
            var noti = Ext.create('widget.uxNotification', {
              // title: 'Notification',
              position: 'tr',
              manager: 'instructions',
              // cls: 'ux-notification-light',
              // iconCls: 'ux-notification-icon-information',
              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
              closable: false,
              autoCloseDelay: 4000,
              width: 300,
              slideBackDuration: 500,
              slideInAnimation: 'bounceOut',
              slideBackAnimation: 'easeIn'
            });
            noti.show();

              var grid = Ext.ComponentQuery.query('business_registrationgrid')[0];
              var store = grid.getStore();

              if(win.actionMode == 'edit')
              {
                store.reload();
              }else
              {
                store.load({
                    params: {traderId: dataObj.traderId},
                    callback: function(records, operation, success) {
                        // //console.log(success);
                    },
                    scope: this
                }); 
              }



             
              win.close();
        
            },
            failure: function(form, action) {
              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
            },
            scope: this 
        });
  },
  searchTraderBetweenRegistration: function(btn)
  {
     var form = btn.up('form'),
      values = form.getValues();



     var dataParams = this.normalizeData(values);
     dataParams.traderRecordStatus = 'T';
     dataParams.regRecordStatus = 'T';
     dataParams.personRecordStatus = 'T';
     dataParams.traderType = 'B';
     

      
      var grid = Ext.ComponentQuery.query('business_registrationgrid')[0];
      var store = grid.getStore();
      
      

     dataParams.page = '1';
     dataParams.start = '0';
     dataParams.limit = store.pageSize;

      store.load({
          params: dataParams,
          callback: function(records, operation, success) {
              // //console.log(success);
          },
          scope: this
      }); 
  },
  normalizeData: function(dataObj)
  {
      // remove null data;
     for (field in dataObj) 
      {
        if (Ext.isEmpty(dataObj[field])) 
        {
          delete dataObj[field];
        }
        
      }

      return dataObj;
  }
  ,handleActionColumn : function(column, action, grid, rowIndex, colIndex, model, node) {
          
          if(action){
              switch(action){
                  case 'editregistration': 

                     var edit = Ext.create('tourism.view.business.registration.BusinessRegistrationAddEditWindow',{
                      animateTarget: column.el,
                      actionMode: 'edit',
                      roleAction: 'key',
                      traderType: 'business',
                      verificationGrid: grid
                     });

                     edit.show();

                     edit.loadDataModel(model);
                      
                  break;

                  case 'deletetraderaddress': 
                        var store = grid.getStore();
                        store.remove(model);
                        store.sync();
                  break;

                  case 'verification': 
                	
                    var edit = Ext.create('tourism.view.window.verification.VerificationWindow',{
                     id: 'business-registration-verification-form',
                     animateTarget: column.el,
                     actionMode: 'verification',
                     roleAction: 'key',
                     verificationGrid: grid
                    }).show();

//                    var form = edit.down('form');
//                    form.getForm().findField('regId').setValue(model.get('regId'));
                    
                    var form = edit.down('form');
	                 form.getForm().findField('regId').setValue(model.get('regId'));
	                 var grid = form.down('verification-reg-document-grid');
	                 grid.getStore().load({
	                 	  params: {
	                 		  traderId: model.get('traderId')
	                 		  ,recordStatus: 'N'
//	                 		  ,licenseType: 'G'
//	                 		  ,registrationType: model.get("registrationType")
	                 	  },
	                       callback: function(records, operation, success) {
//	                            //console.log(records);
	                       },
	                       scope: this
	                   });
                      
                  break;

                  case 'viewregistration':
                   
                    var winInfo = Ext.create('tourism.view.information.business.InfoBusinessWindow',{
                        id: 'business-information-business-registration-infowindow',
                        animateTarget: column.el,
                        actionMode: 'imformation',
                        roleAction: 'KEY'
                        ,registrationType: model.get('registrationType')
                      });

                      winInfo.show();

                      winInfo.loadFormRecord(model);
                  break;

                  case 'businessprogress':

                    var progressWin = Ext.create('tourism.view.registrationprogress.RegistrationProgressWindow',{
                      id: 'business-registrationprogress-ProgressWindow',
                      animateTarget: column.el 
                      
                     });

                    progressWin.on('show', function(win){


                      var grid = win.down('registrationprogress-grid');
                     
                      grid.getStore().load({
                        params: {regId: model.get('regId')},
                        callback: function(records, operation, success) {
                            
                        },
                        scope: this
                      }); 

                    },this);

                    progressWin.show();

                  break;
              }
          }
      
      },
      saveVerification: function(btn)
      {
        var form    = btn.up('form');
        var win = btn.up('window');
        var values = form.getValues();

        if (!form.isValid()) {

            var me = form,errorCmp, fields, errors;

            fields = me.getForm().getFields();
            errors = '';
            fields.each(function(field) {
                Ext.Array.forEach(field.getErrors(), function(error) {
                    errors += field.getFieldLabel()  +'<br>';
                });
            });

            Ext.Msg.show({
               title:'กรุณาตรวจสอบข้อมูล',
               msg: errors,
               buttons: Ext.Msg.OK,
               icon: Ext.Msg.ERROR
            });
            return;
        }


        var dataParams = this.normalizeData(values);
        
        var url = 'business/key/registration/verification';
        if(values.progressStatus == '0'){
        	url = 'business/registration/requireDocs';
        }

        
         // fsubmit.submit({
       form.load({
            url: url,
            method : 'POST',
            waitMsg: 'กรุณารอสักครู่..',
            params: dataParams,
            success: function(form, action) {

              var dataObj = action.result.data;
             
              // Ext.Msg.alert('', 'บันทึกข้อมูล');

            var noti = Ext.create('widget.uxNotification', {
              // title: 'Notification',
              position: 'tr',
              manager: 'instructions',
              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
              closable: false,
              autoCloseDelay: 4000,
              width: 300,
              slideBackDuration: 500,
              slideInAnimation: 'bounceOut',
              slideBackAnimation: 'easeIn'
            });
            noti.show();

              var grid = Ext.ComponentQuery.query('business_registrationgrid')[0];
              var store = grid.getStore();

             store.reload();
             
              win.close();
        
            },
            failure: function(form, action) {
              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
            },
            scope: this 
        });
      }
      ,searchUserFromIDCardOnline: function(btn){
		  var form = btn.up('form');
          var win = btn.up('window');
          var idcard = form.getForm().findField('identityNo').getValue();
          //-------- ใช้ mockup url เนื่องจากรอกรมท่องเที่ยวกับกรมการปกครอง เชื่อมโยงระบบกัน
		  var url = "http://27.254.57.223:8083/mockup/requestPersonData/"+idcard;
          var dataParams = "";
          form.load({
              url: url,
              method : 'GET',
              params: dataParams,
              waitMsg: 'กรุณารอสักครู่...',
              success: function(form, action) {
            	  var dataObj = action.result.data;
            	  console.log(dataObj);
            	  if("" != dataObj){
            		  form.findField('identityNo').setValue(dataObj.idcard);
                      var fullNameTH = dataObj.fullNameTH;
                      var fullNameEN = dataObj.fullNameEN;
                      var arrFullNameTH = fullNameTH.split(" ");
                      var arrFullNameEN = fullNameEN.split(" ");
                      form.findField('firstName').setValue(arrFullNameTH[1]);
                      form.findField('lastName').setValue(arrFullNameTH[3]);
                      form.findField('firstNameEn').setValue(arrFullNameEN[1]);
                      form.findField('lastNameEn').setValue(arrFullNameEN[3]);

                      var prefixIdCombo = form.findField('prefixId');
                      var prefixIdEnCombo = form.findField('prefixIdEn');
                      prefixIdCombo.getStore().on('load',function(store, records, successful, eOpts){
                        var prefixName = arrFullNameTH[0];
                        var prefixId = "";
                        if(prefixName == 'น.ส.' || prefixName == 'น.ส' || prefixName == 'นส.' || prefixName == 'นส' || prefixName == 'นางสาว'){
                          prefixId = 2;
                        }else if(prefixName == 'นาย'){
                          prefixId = 1;
                        }
                        prefixIdCombo.setValue(prefixId);
                        prefixIdEnCombo.setValue(prefixId);
                      },this,{single:true});
                      prefixIdCombo.getStore().load();

                      var gender = dataObj.gender;
                      if(gender == '1'){
                        form.findField('gender').setValue('M');
                      }else if(gender == '2'){
                        form.findField('gender').setValue('F');
                      }
                      var birthDate = dataObj.birthDate;
                      var formatedBirthDate = birthDate.substring(6,8)+"/"+birthDate.substring(4,6)+"/"+birthDate.substring(0,4);
                      form.findField('birthDate').setValue(formatedBirthDate);

                      var birthDateInDate = new Date(birthDate.substring(0,4),birthDate.substring(4,6),birthDate.substring(6,8));
                      var currentDate = new Date();
                      form.findField('ageYear').setValue(currentDate.getFullYear()-(birthDateInDate.getFullYear()-543));
                      
                      var expireDate = dataObj.expireDate;
                      var formatedExpireDate = expireDate.substring(6,8)+"/"+expireDate.substring(4,6)+"/"+expireDate.substring(0,4);
                      form.findField('identityNoExpire').setValue(formatedExpireDate);
            	  }else{
            		  Ext.Msg.alert('ไม่พบข้อมูล','Not found user');
            	  }
              },
              failure: function(form, action) {
                Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
              },
              scope: this 
          });
	  }, 
      loadDataFromIdCard: function(btn){
          var url = "http://localhost:8088/cardReader/read/idcard";
          var form = btn.up('form');
          var win = btn.up('window');
          var dataParams = "";
          form.load({
              url: url,
              method : 'GET',
              params: dataParams,
              waitMsg: 'กรุณารอสักครู่...',
              success: function(form, action) {
                var dataObj = action.result.data;
                console.log(dataObj);
                form.findField('identityNo').setValue(dataObj.idcard);
                var fullNameTH = dataObj.fullNameTH;
                var fullNameEN = dataObj.fullNameEN;
                var arrFullNameTH = fullNameTH.split(" ");
                var arrFullNameEN = fullNameEN.split(" ");
                form.findField('firstName').setValue(arrFullNameTH[1]);
                form.findField('lastName').setValue(arrFullNameTH[3]);
                form.findField('firstNameEn').setValue(arrFullNameEN[1]);
                form.findField('lastNameEn').setValue(arrFullNameEN[3]);

                var prefixIdCombo = form.findField('prefixId');
                var prefixIdEnCombo = form.findField('prefixIdEn');
                prefixIdCombo.getStore().on('load',function(store, records, successful, eOpts){
                  var prefixName = arrFullNameTH[0];
                  var prefixId = "";
                  if(prefixName == 'น.ส.' || prefixName == 'น.ส' || prefixName == 'นส.' || prefixName == 'นส' || prefixName == 'นางสาว'){
                    prefixId = 2;
                  }else if(prefixName == 'นาย'){
                    prefixId = 1;
                  }
                  prefixIdCombo.setValue(prefixId);
                  prefixIdEnCombo.setValue(prefixId);
                },this,{single:true});
                prefixIdCombo.getStore().load();

                var gender = dataObj.gender;
                if(gender == '1'){
                  form.findField('gender').setValue('M');
                }else if(gender == '2'){
                  form.findField('gender').setValue('F');
                }
                var birthDate = dataObj.birthDate;
                var formatedBirthDate = birthDate.substring(6,8)+"/"+birthDate.substring(4,6)+"/"+birthDate.substring(0,4);
                form.findField('birthDate').setValue(formatedBirthDate);

                
                var imageFile = win.down('image');
                console.log(imageFile);
                form.findField('imageFile').setValue(dataObj.imageRaw);
                imageFile.setSrc("data:image/png;base64,"+dataObj.imageRaw);

                // var expireDate = dataObj.expireDate;
                // var formatedExpireDate = expireDate.substring(6,8)+"/"+expireDate.substring(4,6)+"/"+expireDate.substring(0,4);
                // form.findField('identityNoExpire').setValue(formatedExpireDate);

                var issueDate = dataObj.issueDate;
                var formatedIssueDate = issueDate.substring(6,8)+"/"+issueDate.substring(4,6)+"/"+issueDate.substring(0,4);
                form.findField('identityDate').setValue(formatedIssueDate);
                
              },
              failure: function(form, action) {
                Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
              },
              scope: this 
          });
    }
      
});