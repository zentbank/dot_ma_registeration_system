Ext.define('tourism.controller.business.registration.address.TraderAddressController', {
	extend : 'Ext.app.Controller',

   	stores: ['tourism.store.business.registration.address.TraderAddressStore'],

    models: ['tourism.model.business.registration.address.TraderAddressModel'],

    views: [
      'tourism.view.business.registration.address.TraderAddressGrid',
      'tourism.view.business.registration.address.TraderAddressAddEditForm'
    ],

    refs: [    
    	{
            ref: 'business_registration_traderaddressgrid',
            selector: 'business_registration_traderaddressgrid'
        } ,
        {
            ref: 'business-registration-address-traderaddressaddedit-form',
            selector: 'panel'
        }
    ],	 
	init : function (application) {
    
        this.control({

            // 'tourleadergrid dataview': {
            //     itemdblclick: this.editUserAccount
            // },

            'business_registration_traderaddressgrid button[action=addTraderAddress]': {
            	click: this.showTraderAddressAddEditWindow
            },
            'business-registration-address-traderaddressaddedit-form button[action=saveTraderAddress]': {
                click: this.saveTraderAddress
            },
           'business_registration_traderaddressgrid actioncolumn': {
                itemclick: this.handleActionColumn
            }

            
        });

		
	},
	showTraderAddressAddEditWindow: function(grid, model) {

         var edit = Ext.create('tourism.view.business.registration.address.TraderAddressAddEditWindow').show();
         
    }
    ,saveTraderAddress: function(button) {

  		var win    = button.up('window');
  		var form   = win.down('form');

      if (!form.isValid()) {

          var me = form,
          errorCmp, fields, errors;

          fields = me.getForm().getFields();
          errors = '';
          fields.each(function(field) {
              Ext.Array.forEach(field.getErrors(), function(error) {
                  // errors.push({name: field.getFieldLabel(), error: error});
                  errors += field.getFieldLabel()  +'<br>';
              });
          });
          // Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', errors);

          Ext.Msg.show({
             title:'กรุณาตรวจสอบข้อมูล',
             msg: errors,
             buttons: Ext.Msg.OK,
             icon: Ext.Msg.ERROR
        });
          return;
      }

  		var model = form.getRecord(); //Ext.data.Model
  		var values = form.getValues();//object {xx: q,ss:ff}

      var provinceName = Ext.getCmp('business-registration-address-masprovince-combo').getRawValue();
      var amphurName = Ext.getCmp('business-registration-address-masamphur-combo').getRawValue();
      var tambolName = Ext.getCmp('business-registration-address-tambol-combo').getRawValue();

      values.provinceName = provinceName;
      values.amphurName = amphurName;
      values.tambolName = tambolName;

  		var grid = Ext.ComponentQuery.query('business_registration_traderaddressgrid')[0];        
  		if (!Ext.isEmpty(model)){
        //edit address
  			model.set(values);
  			
  		} else{
        //add address
  			
  			model = Ext.create('tourism.model.business.registration.address.TraderAddressModel');
  			model.set(values);
  			// model.setId(0);

        //set regId and traderId
        var registrationForm = Ext.ComponentQuery.query('form-business-registration-addedit')[0];
        var regId = registrationForm.getForm().findField('regId').getValue();
        var traderId = registrationForm.getForm().findField('traderId').getValue();

        model.set('regId', regId);
        model.set('traderId', traderId);
  			grid.getStore().insert(0, model);

        if(model.get('copyAddress'))
        {
          model2 = Ext.create('tourism.model.business.registration.address.TraderAddressModel');
          model2.set(values);

           /*
            O=office ที่ตั้งสำนักงาน
            S=shop ที่ตั้งเฉพาะการ (ที่ขายของหน้าร้าน)
            B=branch สาขาธุรกิจนำเที่ยว
          */

          if(values.addressType == "O")
          {
            model2.set("addressType", "S");
          }
          else
          {
            model2.set("addressType", "O");
          }

          model2.set('regId', regId);
          model2.set('traderId', traderId);
          
          grid.getStore().insert(0, model2);
        }
  		}


  		win.close();
  		grid.getStore().sync();
    },
  	handleActionColumn : function(column, action, grid, rowIndex, colIndex, model, node) {
          
          if(action){
              switch(action){
                  case 'edittraderaddress': 

                      var edit = Ext.create('tourism.view.business.registration.address.TraderAddressAddEditWindow');
                      edit.show();

                      if(model && (model instanceof tourism.model.business.registration.address.TraderAddressModel)){
                          
                          var fAddress = edit.down('form');
                          var provinceCombo = fAddress.getForm().findField('provinceId');
                          provinceCombo.getStore().on('load',function(store, records, successful, eOpts){
                            provinceCombo.setValue(model.get('provinceId'));
                          },this,{single:true});
                          provinceCombo.getStore().load();

                          var amphurCombo = fAddress.getForm().findField('amphurId');
                          amphurCombo.getStore().on('load',function(store, records, successful, eOpts){
                            amphurCombo.setValue(model.get('amphurId'));
                          },this,{single:true});
                          amphurCombo.getStore().load();

                          var tambolCombo = fAddress.getForm().findField('tambolId');
                          tambolCombo.getStore().on('load',function(store, records, successful, eOpts){
                            tambolCombo.setValue(model.get('tambolId'));
                          },this,{single:true});
                          tambolCombo.getStore().load();

                          edit.down('form').loadRecord(model);
                          edit.down('form').getForm().findField('copyAddress').hide();
                      }

                      
                  break;

                  case 'deletetraderaddress': 
                        var store = grid.getStore();
                        store.remove(model);
                        store.sync();
                  break;
              }
          }
      
      }
});