Ext.define('tourism.controller.business.registration.branch.BranchCtrl', {
	extend : 'Ext.app.Controller',

   	stores: ['tourism.store.business.registration.branch.BranchStore'],

    models: ['tourism.model.business.registration.branch.BranchModel'],

    views: [
      'tourism.view.business.registration.branch.BusinessBranchAddEditForm',
      'tourism.view.business.registration.branch.BusinessBranchGrid'
    ],

    refs: [    
    	{
            ref: 'business_registration_businessbranchgrid',
            selector: 'business_registration_businessbranchgrid'
        } ,
        {
            ref: 'business-registration-branch-branchaddedit-form',
            selector: 'panel'
        }
    ],	 
	init : function (application) {
    
        this.control({

            // 'tourleadergrid dataview': {
            //     itemdblclick: this.editUserAccount
            // },

            'business_registration_businessbranchgrid button[action=addBranch]': {
            	click: this.showBranchAddEditWindow
            },
            'business-registration-branch-branchaddedit-form button[action=saveBranch]': {
                click: this.saveBranch
            },
           'business_registration_businessbranchgrid actioncolumn': {
                itemclick: this.handleActionColumn
            }

            
        });

		
	},
	showBranchAddEditWindow: function(grid, model) {

         var edit = Ext.create('tourism.view.business.registration.branch.BusinessBranchAddEditWindow').show();
    }
    ,saveBranch: function(button) {

  		var win    = button.up('window');
  		var form   = win.down('form');

      if (!form.isValid()) {

          var me = form,
          errorCmp, fields, errors;

          fields = me.getForm().getFields();
          errors = '';
          fields.each(function(field) {
              Ext.Array.forEach(field.getErrors(), function(error) {
                  // errors.push({name: field.getFieldLabel(), error: error});
                  errors += field.getFieldLabel()  +'<br>';
              });
          });
          // Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', errors);

          Ext.Msg.show({
             title:'กรุณาตรวจสอบข้อมูล',
             msg: errors,
             buttons: Ext.Msg.OK,
             icon: Ext.Msg.ERROR
        });
          return;
      }

  		var model = form.getRecord(); //Ext.data.Model
  		var values = form.getValues();//object {xx: q,ss:ff}

      var provinceName = Ext.getCmp('business-registration-branch-masprovince-combo').getRawValue();
      var amphurName = Ext.getCmp('business-registration-branch-masamphur-combo').getRawValue();
      var tambolName = Ext.getCmp('business-registration-branch-tambol-combo').getRawValue();

      values.provinceName = provinceName;
      values.amphurName = amphurName;
      values.tambolName = tambolName;

  		var grid = Ext.ComponentQuery.query('business_registration_businessbranchgrid')[0];        
  		if (!Ext.isEmpty(model)){
        //edit address
  			model.set(values);
  			
  		} else{
        //add address
  			
  			model = Ext.create('tourism.model.business.registration.branch.BranchModel');
  			model.set(values);
  			// model.setId(0);

        //set regId and traderId
        var registrationForm = Ext.ComponentQuery.query('form-business-registration-addedit')[0];
  
        var traderId = registrationForm.getForm().findField('traderId').getValue();
   


        model.set('branchParentId', traderId);
  			grid.getStore().insert(0, model);

      }
  		win.close();
  		grid.getStore().sync();
    },
  	handleActionColumn : function(column, action, grid, rowIndex, colIndex, model, node) {
          
          if(action){
              switch(action){
                  case 'editbranch': 

                      var edit = Ext.create('tourism.view.business.registration.branch.BusinessBranchAddEditWindow');
                      edit.show();

                      if(model && (model instanceof tourism.model.business.registration.branch.BranchModel)){
                          
                          var fAddress = edit.down('form');
                          var provinceCombo = fAddress.getForm().findField('provinceId');
                          provinceCombo.getStore().on('load',function(store, records, successful, eOpts){
                            provinceCombo.setValue(model.get('provinceId'));
                          },this,{single:true});
                          provinceCombo.getStore().load();

                          var amphurCombo = fAddress.getForm().findField('amphurId');
                          amphurCombo.getStore().on('load',function(store, records, successful, eOpts){
                            amphurCombo.setValue(model.get('amphurId'));
                          },this,{single:true});
                          amphurCombo.getStore().load();

                          var tambolCombo = fAddress.getForm().findField('tambolId');
                          tambolCombo.getStore().on('load',function(store, records, successful, eOpts){
                            tambolCombo.setValue(model.get('tambolId'));
                          },this,{single:true});
                          tambolCombo.getStore().load();

                          edit.down('form').loadRecord(model);
                   
                      }

                      
                  break;

                  case 'deletebranch': 
                        var store = grid.getStore();
                        store.remove(model);
                        store.sync();
                  break;
              }
          }
      
      }
});