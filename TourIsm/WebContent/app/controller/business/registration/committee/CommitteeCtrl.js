Ext.define('tourism.controller.business.registration.committee.CommitteeCtrl', {
	extend : 'Ext.app.Controller',

   	stores: ['tourism.store.business.registration.committee.CommitteeStore'],

    models: ['tourism.model.business.registration.committee.CommitteeModel'],

    views: [
      'tourism.view.business.registration.committee.CommiteeAddEditForm'
      ,'tourism.view.business.registration.committee.CommitteeGrid'
    ],

    refs: [    
    	{
            ref: 'business-registration-committee-committeegrid',
            selector: 'business-registration-committee-committeegrid'
        } ,
        {
            ref: 'business-registration-committee-committeeaddedit-form',
            selector: 'panel'
        }
    ],	 
	init : function (application) {
    
        this.control({
            'business-registration-committee-committeegrid button[action=addcommittee]': {
            	click: this.showCommitteeAddEditWindow
            },
            'business-registration-committee-committeeaddedit-form button[action=saveCommittee]': {
                click: this.saveCommittee
            },
           'business-registration-committee-committeegrid actioncolumn': {
                itemclick: this.handleActionColumn
            }
            
        });

		
	},
	showCommitteeAddEditWindow: function(grid, model) {

         var edit = Ext.create('tourism.view.business.registration.committee.CommiteeAddEditWindow').show();

         var fCommittee = edit.down('form');

         var individualFieldSet = fCommittee.down('business-registration-committee-individualCommitteeFieldSet');
         individualFieldSet.expand();
    }
    ,saveCommittee: function(button) {

  		var win    = button.up('window');
  		var form   = win.down('form');

      if (!form.isValid()) {

          var me = form,
          errorCmp, fields, errors;

          fields = me.getForm().getFields();
          errors = '';
          fields.each(function(field) {
              Ext.Array.forEach(field.getErrors(), function(error) {
                  // errors.push({name: field.getFieldLabel(), error: error});
                  errors += field.getFieldLabel()  +'<br>';
              });
          });
          // Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', errors);

          Ext.Msg.show({
             title:'กรุณาตรวจสอบข้อมูล',
             msg: errors,
             buttons: Ext.Msg.OK,
             icon: Ext.Msg.ERROR
        });
          return;
      }

  		var model = form.getRecord(); //Ext.data.Model
  		var values = form.getValues();//object {xx: q,ss:ff}
  		//var params = this.n(values);
      var prefixName = '';
      var postfixName = '';

      if(!Ext.isEmpty(values.committeePersonTypeIndividual))
      {
        values.committeePersonType = 'I';

        prefixName = form.getForm().findField('prefixId').getRawValue();
        postfixName = form.getForm().findField('postfixId').getRawValue();

      }
      else
      {
        values.committeePersonType = 'C';

        prefixName = form.getForm().findField('prefixIdCorp').getRawValue();
        postfixName = form.getForm().findField('postfixId').getRawValue();

        values.prefixId = values.prefixIdCorp;
        values.committeeName = values.committeeNameCorp;
        values.committeeIdentityNo = values.committeeIdentityNoCorp;
        values.provinceId = values.provinceIdCorp;
        values.committeeIdentityDate = values.committeeIdentityDateCorp;
      }

      if(!Ext.isEmpty(values.committeeType))
      {
        values.committeeType = 'S';
      }
      else
      {
        values.committeeType = 'C';
      }

      values.committeeStatus = 'N';

      if(!Ext.isEmpty(postfixName))
      {
        values.postfixName = postfixName;
      }
      if(!Ext.isEmpty(prefixName))
      {
        values.prefixName = prefixName;
      }
      

  		var grid = Ext.ComponentQuery.query('business-registration-committee-committeegrid')[0];        
  		if (!Ext.isEmpty(model)){
        //edit address
  			model.set(values);
  			/*grid.getStore().load({
  	             params: {personId: model.get('personId'), recordStatus: model.get('traderRecordStatus')},
  	             callback: function(records, operation, success) {
  	                 // //console.log(success);
  	             },
  	             scope: this
  	         });*/
  		} else{
        //add address
  			
  			model = Ext.create('tourism.model.business.registration.committee.CommitteeModel');
  			model.set(values);

        var registrationForm = Ext.ComponentQuery.query('form-business-registration-addedit')[0];
        var regId = registrationForm.getForm().findField('regId').getValue();
        var traderId = registrationForm.getForm().findField('traderId').getValue();
        var personId = registrationForm.getForm().findField('personId').getValue();

        model.set('regId', regId);
        model.set('traderId', traderId);
        model.set('personId', personId);
        
  			grid.getStore().insert(0, model);
      }
  		win.close();
  		grid.getStore().sync();
  		/*grid.getStore().load({
             params: {personId: model.get('personId'), recordStatus: model.get('traderRecordStatus')},
             callback: function(records, operation, success) {
                 // //console.log(success);
             },
             scope: this
         }); */
    },
  	handleActionColumn : function(column, action, grid, rowIndex, colIndex, model, node) {
          
          if(action){
              switch(action){
                  case 'editcommitte': 

                      var edit = Ext.create('tourism.view.business.registration.committee.CommiteeAddEditWindow');
                      edit.show();

                      if(model && (model instanceof tourism.model.business.registration.committee.CommitteeModel)){
                          
                          var fCommittee = edit.down('form');

                          if(!Ext.isEmpty(model.get('amphurId')))
                          {
                            var amphurCombo = fCommittee.getForm().findField('amphurId');
                            amphurCombo.getStore().on('load',function(store, records, successful, eOpts){
                              amphurCombo.setValue(model.get('amphurId'));
                            },this,{single:true});
                            amphurCombo.getStore().load();
                          }

                          if(!Ext.isEmpty(model.get('postfixId')))
                          {
                            var postfixIdCombo = fCommittee.getForm().findField('postfixId');
                            postfixIdCombo.getStore().on('load',function(store, records, successful, eOpts){
                              postfixIdCombo.setValue(model.get('postfixId'));
                            },this,{single:true});
                            postfixIdCombo.getStore().load();
                          }
                          if(!Ext.isEmpty(model.get('taxProvinceId')))
                          {
                            var taxProvinceIdCombo = fCommittee.getForm().findField('taxProvinceId');
                            taxProvinceIdCombo.getStore().on('load',function(store, records, successful, eOpts){
                              taxProvinceIdCombo.setValue(model.get('taxProvinceId'));
                            },this,{single:true});
                            taxProvinceIdCombo.getStore().load();
                          }
                          if(!Ext.isEmpty(model.get('taxAmphurId')))
                          {
                            var taxAmphurIdCombo = fCommittee.getForm().findField('taxAmphurId');
                            taxAmphurIdCombo.getStore().on('load',function(store, records, successful, eOpts){
                              taxAmphurIdCombo.setValue(model.get('taxAmphurId'));
                            },this,{single:true});
                            taxAmphurIdCombo.getStore().load();
                          }
                         // if(model.get('committeeType') == 'S')
                         //  {
                         //    model.set('committeeType', 1);
                           
                         //  }

                          edit.down('form').loadRecord(model);

                           if(model.get('committeePersonType')  == 'I')
                          {
                            var individualFieldSet = fCommittee.down('business-registration-committee-individualCommitteeFieldSet');
                            individualFieldSet.expand();

                            if(!Ext.isEmpty(model.get('provinceId')))
                            {
                              var provinceCombo = fCommittee.getForm().findField('provinceId');
                              provinceCombo.getStore().on('load',function(store, records, successful, eOpts){
                                provinceCombo.setValue(model.get('provinceId'));
                              },this,{single:true});
                              provinceCombo.getStore().load();
                            }

                            if(!Ext.isEmpty(model.get('prefixId')))
                            {
                              var prefixIdCombo = fCommittee.getForm().findField('prefixId');
                              var prefixIdEnCombo = fCommittee.getForm().findField('prefixIdEn');
                              prefixIdCombo.getStore().on('load',function(store, records, successful, eOpts){
                                prefixIdCombo.setValue(model.get('prefixId'));
                                prefixIdEnCombo.setValue(model.get('prefixId'));
                              },this,{single:true});
                              prefixIdCombo.getStore().load();
                            }

                          }
                          else
                          {
                            var corporateFieldSet = fCommittee.down('business-registration-committee-corporateCommitteeFieldSet');
                            corporateFieldSet.expand();

                            if(!Ext.isEmpty(model.get('provinceId')))
                            {
                              var provinceCombo = fCommittee.getForm().findField('provinceIdCorp');
                              provinceCombo.getStore().on('load',function(store, records, successful, eOpts){
                                provinceCombo.setValue(model.get('provinceId'));
                              },this,{single:true});
                              provinceCombo.getStore().load();
                            }

                            if(!Ext.isEmpty(model.get('prefixId')))
                            {
                              var prefixIdCombo = fCommittee.getForm().findField('prefixIdCorp');
                              prefixIdCombo.getStore().on('load',function(store, records, successful, eOpts){
                                prefixIdCombo.setValue(model.get('prefixId'));
                              },this,{single:true});
                              prefixIdCombo.getStore().load();
                            }
                            if(!Ext.isEmpty(model.get('committeeName')))
                            {
                              var committeeNameTxt = fCommittee.getForm().findField('committeeNameCorp');
                               committeeNameTxt.setValue(model.get('committeeName'));
                            }
                            if(!Ext.isEmpty(model.get('committeeIdentityNo')))
                            {
                              var committeeIdentityNoCorpTxt = fCommittee.getForm().findField('committeeIdentityNoCorp');
                               committeeIdentityNoCorpTxt.setValue(model.get('committeeIdentityNo'));
                            }
                            if(!Ext.isEmpty(model.get('committeeIdentityDate')))
                            {
                              var committeeIdentityDateCorpTxt = fCommittee.getForm().findField('committeeIdentityDateCorp');
                               committeeIdentityDateCorpTxt.setValue(model.get('committeeIdentityDate'));
                            }
                          }

                           
                   
                      }

                      
                  break;

                  case 'deletecommitte': 
                        var store = grid.getStore();
                        store.remove(model);
                        store.sync();
                  break;
                  
                  case 'businessprogress':

                      var progressWin = Ext.create('tourism.view.committeeprogress.CommitteeProgressWindow',{
                        id: 'business-committeeprogress-ProgressWindow',
                        animateTarget: column.el 
                        
                       });

                      progressWin.on('show', function(win){

                    	 // var registrationForm = Ext.ComponentQuery.query('form-business-registration-addedit')[0];
                         // var regId = registrationForm.getForm().findField('regId').getValue();
                         // var traderId = registrationForm.getForm().findField('traderId').getValue();
                          //var personId = registrationForm.getForm().findField('personId').getValue();
                        var grid = win.down('committeeprogress-grid');
                       //console.log(model.get('committeeIdentityNo'));
                        grid.getStore().load({
                          params: {committeeIdentityNo: model.get('committeeIdentityNo'),committeePersonType:model.get('committeePersonType')},
                          callback: function(records, operation, success) {
                              
                          },
                          scope: this
                        }); 

                      },this);

                      progressWin.show();

                    break;
                
              }
          }
      
      }
});