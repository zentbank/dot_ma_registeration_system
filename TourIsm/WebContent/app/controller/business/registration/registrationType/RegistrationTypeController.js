Ext.define('tourism.controller.business.registration.registrationType.RegistrationTypeController', {
	extend : 'Ext.app.Controller',

   	stores: ['tourism.store.business.registration.BusinessRegistrationStore'],

    models: ['tourism.model.business.registration.BusinessRegistrationModel'],

    views: [
      'tourism.view.business.registration.registrationType.RegistrationTypeAddEditForm',
      'tourism.view.business.registration.registrationType.RegistrationTypeSearchBusinessForm'
    ],

    refs: [    
    	 {
            ref: 'business-registration-registrationtype-addeditform',
            selector: 'panel'
        } 
        ,{
            ref: '#business-registration-registrationtype-searchbusinessform',
            selector: 'panel'
        }
    ],	 
	init : function (application) {

        this.control({
            
            'business-registration-registrationtype-addeditform button[action=selectRegType]': {
            	click: this.selectRegistrationType
            },
             '#business-registration-registrationtype-searchbusinessform button[action=saveSearchType]': {
              click: this.saveSearchType
            }
            ,
             '#business-registration-registrationtype-searchbusinessform button[action=searchLicense]': {
              click: this.searchLicense
            }
            ,
             '#business-registration-registrationtype-searchbusinessform button[action=selectedLicenseNo]': {
              click: this.selectedLicenseNo
            }
            
        });

		
	},
	selectRegistrationType: function(button) {
		var win    = button.up('window'),
		form   = win.down('form'),
		model = form.getRecord(), //Ext.data.Model
		values = form.getValues();//object {xx: q,ss:ff}

    var animateTarget = win.animateTarget;

    if(values.registrationType != 'N')
    {
        var layout = win.getLayout();
        layout.setActiveItem(1);

        var biModel = Ext.create('tourism.model.business.registration.BusinessRegistrationModel');
        biModel.set('registrationType',values.registrationType);


        if(values.registrationType == 'R')
        {
          biModel.set('registrationTypeName', '<h1>ชำระค่าธรรมเนียมรายสองปี</h1>');
        }
        else if(values.registrationType == 'B')
        {
          biModel.set('registrationTypeName', '<h1>เพิ่มข้อมูลสาขา</h1>');
        }
        else if(values.registrationType == 'C')
        {
          biModel.set('registrationTypeName', '<h1>เปลี่ยนแปลงรายการ</h1>');
        }
        else if(values.registrationType == 'T')
        {
          biModel.set('registrationTypeName', '<h1>ออกใบแทน</h1>');
        }
        


        // var form = Ext.ComponentQuery.query('#business-registration-registrationtype-searchbusinessform')[0];
         var form = win.down('#business-registration-registrationtype-searchbusinessform');
        form.loadRecord(biModel);
        //loadRecord
    }
    else
    {
      var fsubmit = form.getForm();
      var ctrl = this; // scope
     
      // fsubmit.submit({
       fsubmit.load({
            url: 'business/registration/createregistration',
            method : 'POST',
            waitMsg: 'กรุณารอสักครู่...',
            params: {
                newStatus: 'test'
            },
            params: values,
            success: function(form, action) {

              var dataObj = action.result.data;

              var regAddEditWindow = ctrl.createBusinessRegistrationAddEditWindow(dataObj,animateTarget);

              regAddEditWindow.on('show', function(){
                
                win.close();

              },regAddEditWindow,{single: true});
              regAddEditWindow.show();
            },
            failure: function(form, action) {
              Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
            },
            scope: fsubmit // this = fsubmit
        });
        
        //win.close();
    }


		 
		
  },
  saveSearchType: function(button) {
    var win    = button.up('window');
    // var form = Ext.ComponentQuery.query('#business-registration-registrationtype-searchbusinessform')[0];  
    var form = win.down('#business-registration-registrationtype-searchbusinessform');

   
    model = form.getRecord(); //Ext.data.Model
    values = form.getValues();//object {xx: q,ss:ff}




    // open BusinessRegistrationAddEdit Window

     win.close();
    
  },
  createBusinessRegistrationAddEditWindow: function(values, animateTarget){

    var model =  Ext.create('tourism.model.business.registration.BusinessRegistrationModel',{
      
      regId: values.regId,
      registrationType: values.registrationType,
      registrationNo: values.registrationNo,
      registrationDate: values.registrationDate,      
      personId: values.personId,
      traderId: values.traderId,
      traderType: values.traderType,
      authorityName: values.authorityName

    });

    // model.set(values);

    var regAddEditWindow = Ext.create('tourism.view.business.registration.BusinessRegistrationAddEditWindow',{
      registrationModel: model,
      animateTarget: animateTarget,
      roleAction: 'key',
      traderType: 'business'
    });
    return regAddEditWindow;
  },
  searchLicense: function(btn)
  {
    var form = btn.up('form');
    var grid =  form.down('registrationtype-grid');

    var licenseNo = form.getForm().findField('licenseNo').getValue();

    if(Ext.isEmpty(licenseNo))
    {
          Ext.Msg.show({
             title:'เกิดข้อผิดพลาด',
             msg: 'กรุณากรอกข้อมูลเลขที่ใบอนุยาต',
             buttons: Ext.Msg.OK,
             icon: Ext.Msg.ERROR
          });
      return;
    }

   grid.getStore().load({
        params: { traderType: 'B', licenseNo: licenseNo},
        callback: function(records, operation, success) {
            
        },
        scope: this
    }); 

  },
  selectedLicenseNo: function(btn)
  {
    var win    = btn.up('window');

    var animateTarget = win.animateTarget;

    var form1 = btn.up('form');
    var grid =  form1.down('registrationtype-grid');
    var selection = grid.getSelectionModel().getSelection();

    if(selection.length < 1)
    {
        Ext.Msg.show({
           title:'เกิดข้อผิดพลาด',
           msg: 'กรุณาเลือกข้อมูลใบอนุยาต',
           buttons: Ext.Msg.OK,
           icon: Ext.Msg.ERROR
        });

        return;
    }

    var model = selection[0];

    
    var registrationType = form1.getForm().findField('registrationType').getValue();
    var myMask = new Ext.LoadMask(form1, {msg:"กรุณารอสักครู่..."});
    myMask.show();
    form1.load({
      url: 'business/registration/addnewregistration',
      method : 'POST',
      waitMsg: 'กรุณารอสักครู่...',
      params: {traderId: model.get('traderId') ,registrationType: registrationType},
      success: function(form, action) {
        myMask.hide();

        var edit = Ext.create('tourism.view.business.registration.BusinessRegistrationAddEditWindow',{
          animateTarget: animateTarget,
          actionMode: 'edit',
          roleAction: 'key',
          traderType: 'business',
          verificationGrid: grid
         });
         edit.show();

        var model =  Ext.create('tourism.model.business.registration.BusinessRegistrationModel');

        model.set(action.result.data);

        edit.loadDataModel(model);

        win.close();

      },
      failure: function(form, action) {
        myMask.hide();
        Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
      },
      scope: this // this = fsubmit
    });


  }
});