Ext.define('tourism.controller.business.registration.tradercategory.TradercategoryCtrl', {
	extend : 'Ext.app.Controller',

   	stores: ['tourism.store.business.registration.tradercategory.PlantripStore'],

    models: ['tourism.model.business.registration.tradercategory.PlantripModel'],

    views: [
      'tourism.view.business.registration.tradercategory.TraderCategoryForm',
      'tourism.view.business.registration.tradercategory.PlantripGrid'
    ],

    refs: [    
      {
            autoCreate: true,
            ref: 'business-registration-tradercategory-traderCategoryForm-traderCategory-radiogroup',
            selector: '#business-registration-tradercategory-traderCategoryForm-traderCategory-radiogroup', // itemId for first radio group
            xtype: 'Ext.form.RadioGroup'
      },
      {
            ref: 'business_registration_tradercategory_plantripgrid',
            selector: 'business_registration_tradercategory_plantripgrid'
      }
    ],	 
	init : function (application) {
    
    this.control({
        "#business-registration-tradercategory-traderCategoryForm-traderCategory-radiogroup": {
            change: this.onFirstbtngroupChange,
            afterrender: this.onFirstbtngroupAfterRender
        },
        'business_registration_tradercategory_plantripgrid button[action=addplantrip]': {
          click: this.addplantrip
        },
        'business_registration_tradercategory_plantripgrid actioncolumn': {
            itemclick: this.handleActionColumn
        }
    });

		
	},
  addplantrip: function(btn, model)
  {
      var grid = btn.up('grid');

      var  rowEditing = grid.getPlugin('rowediting-grid-plantripgrid');
      rowEditing.cancelEdit();

      var registrationForm = Ext.ComponentQuery.query('form-business-registration-addedit')[0];
    
      var traderId = registrationForm.getForm().findField('traderId').getValue();
      var plantripPerYear = registrationForm.getForm().findField('plantripPerYear').getValue();

      if((Ext.isEmpty(plantripPerYear)) || (plantripPerYear == 0))
      {
        Ext.Msg.alert('เกิดข้อผิดพลาด', 'กรุณากรอกข้อมูลแผนการจัดนำเที่ยว (ครั้ง/ปี)');
        return;
      }

      var plantripModel = Ext.create("tourism.model.business.registration.tradercategory.PlantripModel",
        {
          traderId: traderId,
          countryId: 41,
          plantripPerYear: plantripPerYear
        });

    
      grid.getStore().insert(0, plantripModel);
      rowEditing.startEdit(0,0);
  },
  onFirstbtngroupChange: function(field, newValue, oldValue, eOpts) {
      this.isShowCountry(newValue.traderCategory, field);
      var contrygrid = Ext.ComponentQuery.query('business_registration_tradercategory_plantripgrid')[0]; 
      contrygrid.getStore().removeAll();
      contrygrid.getStore().sync();
  },
  onFirstbtngroupAfterRender: function(radioGroupComp, eOpts) {

      var win    = radioGroupComp.up('window');
      form   = win.down('form');

      var traderCategorySelected = radioGroupComp.getValue();
      this.isShowCountry(traderCategorySelected.traderCategory, radioGroupComp);

  },
  isShowCountry: function(traderCategorySelected, radioGroupComp)
  {
      // var plantripPercontryTxt = form.getForm().findField('plantripPercontry');
      var contrygrid = Ext.ComponentQuery.query('business_registration_tradercategory_plantripgrid')[0]; 


      if(traderCategorySelected){
        switch(traderCategorySelected){
            case '100': 
              // plantripPercontryTxt.show();
              contrygrid.expand();

            break;

            case '200': 
              // plantripPercontryTxt.show();
              contrygrid.expand();

            break;

            case '300': 
              // plantripPercontryTxt.hide();
              contrygrid.collapse();

            break;

            case '400': 
              // plantripPercontryTxt.hide();
              contrygrid.collapse();
            break;
        }
      }
  },
  handleActionColumn : function(column, action, grid, rowIndex, colIndex, model, node) {
        
        if(action){
          var store = grid.getStore();   
          store.remove(model);
          store.sync();
        }
    
    }
    
});