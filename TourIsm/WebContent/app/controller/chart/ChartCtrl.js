Ext.define('tourism.controller.chart.ChartCtrl', {
	extend : 'Ext.app.Controller',

   	stores: ['tourism.store.registration.RegistrationStore'],

    models: ['tourism.model.registration.RegistrationModel'],

    views: [

    ]

	,refs: [

	]

	,init : function (application) {

        this.control({
        	
        	'chart-business-tradercategory-formsearch button[action=searchChart]': {
        		click: this.searchChart
            }
        	,'chart-business-tradercategory-formsearch button[action=printExcel]': {
        		click: this.printExcel
            }
        	
        	,'chart-business-all-formsearch button[action=searchBusinessAll]': {
        		click: this.searchBusinessAll
            }
        	,'chart-business-all-formsearch button[action=printExcel]': {
        		click: this.printExcel
            }
        	
        	,'chart-business-new-formsearch button[action=printExcel]': {
        		click: this.printExcel
        	}
        	,'chart-business-new-formsearch button[action=searchBusinessAll]': {
        		click: this.searchBusinessAll
            }
        	
        	,'chart-business-renew-formsearch button[action=printExcel]': {
        		click: this.printExcel
        	}
        	,'chart-business-renew-formsearch button[action=searchBusinessAll]': {
        		click: this.searchBusinessAll
            }
        	
        	,'chart-business-guarantee-formsearch button[action=printExcel]': {
        		click: this.printExcel
        	}
        	
        	,'chart-business-suspensionandrevoke-formsearch button[action=printExcel]': {
        		click: this.printExcel
        	}
        	
        	,'chart-guide-type-formsearch button[action=printExcel]': {
        		click: this.printExcel
        	}
        	
        	,'chart-guide-all-formsearch button[action=printExcel]': {
        		click: this.printExcel
        	}
        	,'chart-guide-all-formsearch button[action=searchChartGuideAll]': {
        		click: this.searchChartGuideAll
            }
        	
        	,'chart-guide-new-formsearch button[action=printExcel]': {
        		click: this.printExcel
        	}
        	
        	,'chart-guide-renew-formsearch button[action=printExcel]': {
        		click: this.printExcel
        	}
        	
        	,'chart-guide-org-formsearch button[action=searchChartGuideOrg]': {
        		click: this.searchChartGuideOrg
            }
        	,'chart-guide-org-formsearch button[action=printExcel]': {
        		click: this.printExcel
        	}
        	,'chart-license-detail-formsearch button[action=printLicenseDetail]': {
        		click: this.printLicenseDetail
        	}


        });
        
	}
	,printLicenseDetail: function(btn)
	{
		var form = btn.up('form');
		  
		  if (!form.isValid()) {
				var me = form,errorCmp, fields, errors;
	
		          fields = me.getForm().getFields();
		          errors = '';
		          fields.each(function(field) {
		              Ext.Array.forEach(field.getErrors(), function(error) {
		                  errors += field.getFieldLabel()  +'<br>';
		              });
		          });
		          
		          Ext.Msg.show({
		             title:'กรุณาตรวจสอบข้อมูล',
		             msg: errors,
		             buttons: Ext.Msg.OK,
		             icon: Ext.Msg.ERROR
		        });
		          return;
			}
		  
		  //Pie Chart
	      values = form.getValues();

	      window.open('tourleader/chart/excel/licenseDetail?traderType='
				+values.traderType+'&orgId='+values.orgId
				+'&provinceId='+values.provinceId
				,"Pic","width=300,height=300");
	}

	,searchChartGuideOrg: function(btn, model)
	{
		  var form = btn.up('form');
		  
		  if (!form.isValid()) {
				var me = form,errorCmp, fields, errors;
	
		          fields = me.getForm().getFields();
		          errors = '';
		          fields.each(function(field) {
		              Ext.Array.forEach(field.getErrors(), function(error) {
		                  errors += field.getFieldLabel()  +'<br>';
		              });
		          });
		          
		          Ext.Msg.show({
		             title:'กรุณาตรวจสอบข้อมูล',
		             msg: errors,
		             buttons: Ext.Msg.OK,
		             icon: Ext.Msg.ERROR
		        });
		          return;
			}
		  
		  //Pie Chart
	      values = form.getValues();
	      var dataParams = this.normalizeData(values);
	      dataParams.chartName = 'pieguideorgchart';
	    
	      var chartId =  '#chart-guide-org-chart-pie';
	      var formChart = Ext.ComponentQuery.query(chartId)[0];
	      var store = formChart.getStore();
	
	      //console.log(dataParams);
	     
	      var mask = new Ext.LoadMask(formChart,{msg:'กรุณารอซักครู่...', store: store});
	      
	      store.load({
	          params: dataParams,
	          callback: function(records, operation, success) {
	               //console.log(records);
	          },
	          scope: this
	      }); 
	      
	      //Column Chart
	      dataParams.chartName = 'columnguideorgchart';
	      
	      var chartColumnId =  '#chart-guide-org-chart-column';
		  var formColumnChart = Ext.ComponentQuery.query(chartColumnId)[0];
		  var storeOrgId = formColumnChart.getStore();
		  
		  //console.log(dataParams);
		  
		  var mask = new Ext.LoadMask(formColumnChart,{msg:'กรุณารอซักครู่...', store: storeOrgId});
		  
		  storeOrgId.load({
	          params: dataParams,
	          callback: function(records, operation, success) {
	               //console.log(records);
	          },
	          scope: this
	      }); 
		  
	}

	,searchChartGuideAll: function(btn, model)
	{
		  var form = btn.up('form');
		  
		  if (!form.isValid()) {
				var me = form,errorCmp, fields, errors;

		          fields = me.getForm().getFields();
		          errors = '';
		          fields.each(function(field) {
		              Ext.Array.forEach(field.getErrors(), function(error) {
		                  errors += field.getFieldLabel()  +'<br>';
		              });
		          });
		          
		          Ext.Msg.show({
		             title:'กรุณาตรวจสอบข้อมูล',
		             msg: errors,
		             buttons: Ext.Msg.OK,
		             icon: Ext.Msg.ERROR
		        });
		          return;
			}
		  
		  //Pie Chart
	      values = form.getValues();
	      var dataParams = this.normalizeData(values);
	      dataParams.chartName = 'pieguideallchart';
	    
	      var chartId =  '#chart-guide-all-chart-pie';
	      var formChart = Ext.ComponentQuery.query(chartId)[0];
	      var store = formChart.getStore();
	
	      //console.log(dataParams);
	      var mask = new Ext.LoadMask(formChart,{msg:'กรุณารอซักครู่...', store: store});
	     
	      store.load({
	          params: dataParams,
	          callback: function(records, operation, success) {
	               //console.log(records);
	          },
	          scope: this
	      }); 
	      
	      //Column Chart
	      dataParams.chartName = 'columnguideallchart';
	      
	      var chartColumnId =  '#chart-guide-all-chart-column';
		  var formColumnChart = Ext.ComponentQuery.query(chartColumnId)[0];
		  var storeOrgId = formColumnChart.getStore();
		  
		  //console.log(dataParams);
		  var mask = new Ext.LoadMask(formColumnChart,{msg:'กรุณารอซักครู่...', store: storeOrgId});
		  
		  storeOrgId.load({
	          params: dataParams,
	          callback: function(records, operation, success) {
	               //console.log(records);
	          },
	          scope: this
	      }); 
		  
	}

	,searchBusinessAll: function(btn, model)
	{
		  var form = btn.up('form');
		  
		  if (!form.isValid()) {
				var me = form,errorCmp, fields, errors;

		          fields = me.getForm().getFields();
		          errors = '';
		          fields.each(function(field) {
		              Ext.Array.forEach(field.getErrors(), function(error) {
		                  errors += field.getFieldLabel()  +'<br>';
		              });
		          });
		          
		          Ext.Msg.show({
		             title:'กรุณาตรวจสอบข้อมูล',
		             msg: errors,
		             buttons: Ext.Msg.OK,
		             icon: Ext.Msg.ERROR
		        });
		          return;
			}
		  
	      values = form.getValues();
	      var dataParams = this.normalizeData(values);
	      dataParams.chartName = btn.chartName;
	    
	      var panel = form.up('chart-column-business-panel');
	      var chart = panel.down('chart');
	      var store = chart.getStore();
	      
//	      var chartId =  '#chart-business-all-chart-column';
//	      var formChart = Ext.ComponentQuery.query(chartId)[0];
//	      var store = formChart.getStore();

	      //console.log(dataParams);
	      
	      var mask = new Ext.LoadMask(chart,{msg:'กรุณารอซักครู่...', store: store});
	     
	      store.load({
	          params: dataParams,
	          callback: function(records, operation, success) {
	               //console.log(records);
	          },
	          scope: this
	      }); 
	}

	,printExcel: function(btn, model)
	{
		var form = btn.up('form');
		values = form.getValues();
		
		if (!form.isValid()) {
			var me = form,errorCmp, fields, errors;

	          fields = me.getForm().getFields();
	          errors = '';
	          fields.each(function(field) {
	              Ext.Array.forEach(field.getErrors(), function(error) {
	                  errors += field.getFieldLabel()  +'<br>';
	              });
	          });
	          
	          Ext.Msg.show({
	             title:'กรุณาตรวจสอบข้อมูล',
	             msg: errors,
	             buttons: Ext.Msg.OK,
	             icon: Ext.Msg.ERROR
	        });
	          return;
		}
		
		window.open('tourleader/chart/excel/print?approveDateFrom='
				+values.approveDateFrom+'&approveDateTo='+values.approveDateTo
				+'&chartName='+btn.chartName+'&templateFileName='+btn.templateFileName
				,"Pic","width=200,height=200");
		
	}


	,searchChart: function(btn, model)
	{
		  var form = btn.up('form'),
	      values = form.getValues();
	      var dataParams = this.normalizeData(values);
	      dataParams.chartName = 'piebusinesstradercategorychart';
	    
	      var chartId =  '#chart-business-tradercategory-chart-pie';
	      var formChart = Ext.ComponentQuery.query(chartId)[0];
	      var store = formChart.getStore();

	      //console.log(dataParams);
	     
	      var mask = new Ext.LoadMask(formChart,{msg:'กรุณารอซักครู่...', store: store});
	      
	      store.load({
	          params: dataParams,
	          callback: function(records, operation, success) {
	               //console.log(records);
	          },
	          scope: this
	      }); 
	      
	      dataParams.chartName = 'piebusinessorgidchart';
	      
	      var chartOrgIdId =  '#chart-business-orgid-chart-pie';
		  var formOrgIdChart = Ext.ComponentQuery.query(chartOrgIdId)[0];
		  var storeOrgId = formOrgIdChart.getStore();
		  
		  //console.log(dataParams);
		  
		  var mask = new Ext.LoadMask(formOrgIdChart,{msg:'กรุณารอซักครู่...', store: storeOrgId});
		  
		  storeOrgId.load({
	          params: dataParams,
	          callback: function(records, operation, success) {
	               //console.log(records);
	          },
	          scope: this
	      }); 
		  
	}
	
	,
    normalizeData: function(dataObj)
    {
        // remove null data;
       for (field in dataObj) 
        {
          if (Ext.isEmpty(dataObj[field])) 
          {
            delete dataObj[field];
          }
          
        }

        return dataObj;
    }

});



















