Ext.define('tourism.controller.complaint.ComplaintCtrl', {
	extend : 'Ext.app.Controller',

   	stores: ['tourism.store.registration.RegistrationStore'],

    models: ['tourism.model.registration.RegistrationModel'],

    views: [
//            'tourism.view.complaint.addcomplaint.ComplaintAddEditForm'
    ]

	,refs: [
	{  
	    ref: 'complaint-receive-grid',
	    selector: 'complaint-receive-grid'
	} 
	,{  
	    ref: 'complaint-document-add-grid',
	    selector: 'complaint-document-add-grid'
	} 
//	,{
//		ref: 'complaint-addedit-form'
//		,selector: 'complaint-addedit-form'
//	}
//	,{  
//	    ref: 'document-tourleader-formsearch',
//	    selector: 'panel'
//	} 
	]

	,init : function (application) {

        this.control({
//        	'complaint-receive-grid button[action=addComplaint]': {
//                click: this.addComplaint
//            },
        	'info-license-complaint-searchlicensepanel button[action=addComplaint]': {
              click: this.addComplaint
              },
        	'complaint-receive-grid button[action=popupSearchLicense]': {
        		click: this.popupSearchLicense
        	}
//            'info-license-complaint-searchlicensepanel button[action=selectLicense]': {
//                click: this.selectLicenseNo
//            }
        	
	        ,'complaint-addedit-form button[action=saveComplaintReceive]': {
	            click: this.saveComplaintReceive
	        }
	        ,'complaint-addedit-form button[action=changeQuestionType]': {
	            click: this.changeQuestionType
	        }
	        
	        ,'complaint-receive-formsearch button[action=searchComplaintReceive]': {
	            click: this.searchComplaintReceive
	        }
	        ,'complaint-document-add-grid actioncolumn': {
                itemclick: this.handleActionColumn
            }
	        ,'complaint-receive-grid actioncolumn': {
                itemclick: this.handleActionColumn
            }
	        ,'complaint-document-info-grid actioncolumn': {
                itemclick: this.handleActionColumn
            }
	        ,
	        'complaint-document-consider-info-grid actioncolumn': {
                itemclick: this.handleActionColumn
	        }
	        ,
	        'complaint-consider-formsearch button[action=searchComplaintConsider]': {
	            click: this.searchComplaintConsider
	        }
	        ,'complaint-consider-grid actioncolumn': {
                itemclick: this.handleActionColumn
	        }
	        ,'complaint-progress-consider-grid button[action=addComplaintProgressConsider]': {
	            click: this.addComplaintProgressConsider
	        }
	        ,'complaint-progress-consider-addedit-form button[action=saveComplaintProgressConsider]': {
	            click: this.saveComplaintProgressConsider
	        }
	        ,'complaint-progress-consider-grid actioncolumn': {
                itemclick: this.handleActionColumn
	        }
	        ,'complaint-progress-consider-info-grid actioncolumn': {
                itemclick: this.handleActionColumn
	        }
	        ,'complaint-faq-formsearch button[action=searchQuestion]': {
            	click: this.searchQuestion
        	}
	        ,'complaint-faq-grid actioncolumn': {
                itemclick: this.handleActionColumn
	        }
	        ,'question_add_edit_form button[action=saveQuestion]': {
            	click: this.saveQuestion
	        }
	        ,'question_add_edit_form button[action=changeQuestionType]': {
            	click: this.changeQuestionType
	        }
        });
        
	}

	,saveQuestion: function(btn, model)
	{
		var win = btn.up('window');
		var form = win.down('form');
		if (!form.isValid()) {
			var me = form,errorCmp, fields, errors;

	          fields = me.getForm().getFields();
	          errors = '';
	          fields.each(function(field) {
	              Ext.Array.forEach(field.getErrors(), function(error) {
	                  errors += field.getFieldLabel()  +'<br>';
	              });
	          });
	          
	          Ext.Msg.show({
	             title:'กรุณาตรวจสอบข้อมูล',
	             msg: errors,
	             buttons: Ext.Msg.OK,
	             icon: Ext.Msg.ERROR
	        });
	          return;
		}
		
		var values = form.getValues();
        values.progressRole = btn.complaintProgress;

        var dataParams = this.normalizeData(values);
        //console.log(dataParams);
        
        var fsubmit = form.getForm();
        
        fsubmit.load({
            url: 'tourleader/complaint/saveQuestion',
            method : 'POST',
            params: dataParams,
            waitMsg: 'กรุณารอซักครู่...',
            success: function(form, action) {

            var dataObj = action.result.data;
            //console.log(dataObj);

            var noti = Ext.create('widget.uxNotification', {
              position: 'tr',
              manager: 'instructions',
              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
              closable: false,
              autoCloseDelay: 4000,
              width: 300,
              slideBackDuration: 500,
              slideInAnimation: 'bounceOut',
              slideBackAnimation: 'easeIn'
            });
            noti.show();
              
              var grid = Ext.ComponentQuery.query('complaint-faq-grid')[0];
              var store = grid.getStore();
              
              var panel = grid.up('complaint-panel');
              //console.log(panel);
              var formSearch = panel.down('complaint-faq-formsearch');
              //console.log(formSearch);

    	      var valuesSearch = formSearch.getValues();
    	      var dataParamsS = this.normalizeData(valuesSearch);
    	      dataParamsS.questionType = 'Q';
//    	      dataParamsS.complaintProgress = btn.complaintProgress;
    	    
    	      //console.log(valuesSearch);
    	   
    	      dataParamsS.page = '1';
    	      dataParamsS.start = '0';
    	  	  dataParamsS.limit = store.pageSize;
              
              //console.log(dataObj.complaintLicenseId);
                store.reload({
//                	params: {questionType: 'Q'},
                	params: dataParamsS,
                	callback: function(records, operation, success) {
                      //console.log(records);
                	},
                	scope: this
                });

              win.close();
        
            },
            failure: function(form, action) {
              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
            },
            scope: this 
        });
	}
	

	,searchQuestion: function(btn, model)
	{
//		//console.log(btn);
		var form = btn.up('form'),
	    values = form.getValues();
	    var dataParams = this.normalizeData(values);
	    dataParams.questionType = 'Q';
//	    dataParams.complaintProgress = form.complaintProgress;
	    
	    //console.log(dataParams);
	    
	    var gridId =  '#complaint-faq-'+form.complaintProgress+'-complaint-grid';
	    var grid = Ext.ComponentQuery.query(gridId)[0];
	    var store = grid.getStore();
	   
	     dataParams.page = '1';
	     dataParams.start = '0';
	     dataParams.limit = store.pageSize;

	    //console.log(dataParams);
	     
	      store.load({
	          params: dataParams,
	          callback: function(records, operation, success) {
	               //console.log(records);
	          },
	          scope: this
	      }); 
	}

	,saveComplaintProgressConsider: function(btn, model)
	{
		
		var win = btn.up('window');
		var form = win.down('form');
		if (!form.isValid()) {
			var me = form,errorCmp, fields, errors;

	          fields = me.getForm().getFields();
	          errors = '';
	          fields.each(function(field) {
	              Ext.Array.forEach(field.getErrors(), function(error) {
	                  errors += field.getFieldLabel()  +'<br>';
	              });
	          });
	          
	          Ext.Msg.show({
	             title:'กรุณาตรวจสอบข้อมูล',
	             msg: errors,
	             buttons: Ext.Msg.OK,
	             icon: Ext.Msg.ERROR
	        });
	          return;
		}
		
		var values = form.getValues();
        values.progressRole = btn.complaintProgress;

        var dataParams = this.normalizeData(values);
        //console.log(dataParams);
        
        var fsubmit = form.getForm();
        
        fsubmit.load({
            url: 'tourleader/complaint/saveComplaintProgressConsider',
            method : 'POST',
            params: dataParams,
            waitMsg: 'กรุณารอซักครู่...',
            success: function(form, action) {

            var dataObj = action.result.data;
            //console.log(dataObj);

            var noti = Ext.create('widget.uxNotification', {
              position: 'tr',
              manager: 'instructions',
              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
              closable: false,
              autoCloseDelay: 4000,
              width: 300,
              slideBackDuration: 500,
              slideInAnimation: 'bounceOut',
              slideBackAnimation: 'easeIn'
            });
            noti.show();
              
              var grid = Ext.ComponentQuery.query('complaint-progress-consider-grid')[0];
              var store = grid.getStore();
              
        
                store.reload({
                	params: {complaintLicenseId: dataObj.complaintLicenseId, progressRole: btn.complaintProgress},
                	callback: function(records, operation, success) {
                      //console.log(records);
                	},
                	scope: this
                });

              win.close();
        
            },
            failure: function(form, action) {
              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
            },
            scope: this 
        });
	}

	,addComplaintProgressConsider: function(btn, model)
	{
		var window = Ext.create('tourism.view.complaint.considercomplaint.ComplaintProgressConsiderAddEditWindow',{
			  animateTarget: btn.el
			  ,complaintProgress: btn.complaintProgress
       }).show();
	
		var complaintLicenseId = btn.up('form').getForm().findField('complaintLicenseId').getValue();
		window.down('form').getForm().findField('complaintLicenseId').setValue(complaintLicenseId);

	}

	,searchComplaintConsider: function(btn, model)
	{
	
		
		var form = btn.up('form'),
	    values = form.getValues();
	    var dataParams = this.normalizeData(values);
	    dataParams.questionType = 'C';
	    dataParams.complaintProgress = form.complaintProgress;
	    
	    var gridId =  '#complaint-consider-'+form.complaintProgress+'-complaint-grid';
	    var grid = Ext.ComponentQuery.query(gridId)[0];
	    var store = grid.getStore();
	   
	     dataParams.page = '1';
	     dataParams.start = '0';
	     dataParams.limit = store.pageSize;

	    //console.log(dataParams);
	     
	      store.load({
	          params: dataParams,
	          callback: function(records, operation, success) {
	               //console.log(records);
	          },
	          scope: this
	      }); 
	      
	}

	,searchComplaintReceive: function(btn, model)
	{
		var form = btn.up('form'),
	    values = form.getValues();
	    var dataParams = this.normalizeData(values);
	    dataParams.questionType = 'C';
	    dataParams.complaintProgress = form.complaintProgress;
	    
	    //console.log(values);
	    
	    var gridId =  '#complaint-receive-'+form.complaintProgress+'-complaint-grid';
	    var grid = Ext.ComponentQuery.query(gridId)[0];
	    var store = grid.getStore();
	   
	     dataParams.page = '1';
	     dataParams.start = '0';
	     dataParams.limit = store.pageSize;

	    //console.log(dataParams);
	     
	      store.load({
	          params: dataParams,
	          callback: function(records, operation, success) {
	               //console.log(records);
	          },
	          scope: this
	      }); 
	     
	}
	
	,changeQuestionType: function(btn, model)
	{
		var win = btn.up('window');
		var form = win.down('form');
		if (!form.isValid()) {
			var me = form,errorCmp, fields, errors;

	          fields = me.getForm().getFields();
	          errors = '';
	          fields.each(function(field) {
	              Ext.Array.forEach(field.getErrors(), function(error) {
	                  // errors.push({name: field.getFieldLabel(), error: error});
	                  errors += field.getFieldLabel()  +'<br>';
	              });
	          });

	          Ext.Msg.show({
	             title:'กรุณาตรวจสอบข้อมูล',
	             msg: errors,
	             buttons: Ext.Msg.OK,
	             icon: Ext.Msg.ERROR
	        });
	          return;
		}
		
//		var model = form.getRecord(); //Ext.data.Model
		var values = form.getValues();
		
		var licenseNo = form.getForm().findField('licenseNo').getValue();
		var traderName = form.getForm().findField('traderName').getValue();

		values.licenseNo = licenseNo;
		values.traderName = traderName;
		
		//console.log(values.questionType);
		
//		if(Ext.isEmpty(values.questionType))
//		{
//	        values.questionType = 'C';
//		}

        values.complaintProgress = btn.complaintProgress;
        var complaintProgress =  values.complaintProgress;
        var roleGroup =   btn.roleGroup;
        
        var questionType ="C";
        if(roleGroup == 'faq')
    	{
        	questionType =  'Q';
    	}
        
        var dataParams = this.normalizeData(values);

        //console.log(dataParams);

        var fsubmit = form.getForm();
        
        fsubmit.load({
            url: 'tourleader/complaint/saveChangeQuestionType',
            method : 'POST',
            params: dataParams,
            waitMsg: 'กรุณารอซักครู่...',
            success: function(form, action) {
            
            var dataObj = action.result.data;

            var noti = Ext.create('widget.uxNotification', {
              position: 'tr',
              manager: 'instructions',
              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
              closable: false,
              autoCloseDelay: 4000,
              width: 300,
              slideBackDuration: 500,
              slideInAnimation: 'bounceOut',
              slideBackAnimation: 'easeIn'
            });
            noti.show();
              
              var gridId =  '#complaint-'+roleGroup+'-'+complaintProgress+'-complaint-grid';
              //console.log(gridId);
    	      var grid = Ext.ComponentQuery.query(gridId)[0];
              var store = grid.getStore();

              var panel = grid.up('complaint-panel');
              var formSearch = panel.down('complaint-'+roleGroup+'-formsearch');

      	      var valuesSearch = formSearch.getValues();
      	      var dataParamsS = this.normalizeData(valuesSearch);
      	      dataParamsS.questionType = questionType;//'C';
      	      dataParamsS.complaintProgress = btn.complaintProgress;
      	      dataParamsS.page = '1';
      	      dataParamsS.start = '0';
      	  	  dataParamsS.limit = store.pageSize;
              
                store.load({
                	params: dataParamsS,
                	callback: function(records, operation, success) {
                      //console.log(records);
                	},
                	scope: this
                });

              win.close();
        
            },
            failure: function(form, action) {
              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
            },
            scope: this 
        });
        
	}

	,saveComplaintReceive: function(btn, model)
	{
		var win = btn.up('window');
		var form = win.down('form');
		if (!form.isValid()) {
			var me = form,errorCmp, fields, errors;

	          fields = me.getForm().getFields();
	          errors = '';
	          fields.each(function(field) {
	              Ext.Array.forEach(field.getErrors(), function(error) {
	                  // errors.push({name: field.getFieldLabel(), error: error});
	                  errors += field.getFieldLabel()  +'<br>';
	              });
	          });
	          // Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', errors);

	          Ext.Msg.show({
	             title:'กรุณาตรวจสอบข้อมูล',
	             msg: errors,
	             buttons: Ext.Msg.OK,
	             icon: Ext.Msg.ERROR
	        });
	          return;
		}
		
//		var model = form.getRecord(); //Ext.data.Model
		var values = form.getValues();
		
		var licenseNo = form.getForm().findField('licenseNo').getValue();
		var traderName = form.getForm().findField('traderName').getValue();

		values.licenseNo = licenseNo;
		values.traderName = traderName;
		
		if(Ext.isEmpty(values.questionType))
		{
	        values.questionType = 'C';
		}

        values.complaintProgress = btn.complaintProgress;
        var complaintProgress =  values.complaintProgress;

        var dataParams = this.normalizeData(values);

        
        var fsubmit = form.getForm();
        
        fsubmit.load({
            url: 'tourleader/complaint/saveComplaintReceive',
            method : 'POST',
            params: dataParams,
            waitMsg: 'กรุณารอซักครู่...',
            success: function(form, action) {
            
            var dataObj = action.result.data;

            var noti = Ext.create('widget.uxNotification', {
              position: 'tr',
              manager: 'instructions',
              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
              closable: false,
              autoCloseDelay: 4000,
              width: 300,
              slideBackDuration: 500,
              slideInAnimation: 'bounceOut',
              slideBackAnimation: 'easeIn'
            });
            noti.show();
              
              var gridId =  '#complaint-receive-'+complaintProgress+'-complaint-grid';
    	      var grid = Ext.ComponentQuery.query(gridId)[0];
              var store = grid.getStore();

              var panel = grid.up('complaint-panel');
//              //console.log(panel);
              var formSearch = panel.down('complaint-receive-formsearch');
//              //console.log(formSearch);

      	      var valuesSearch = formSearch.getValues();
      	      var dataParamsS = this.normalizeData(valuesSearch);
      	      dataParamsS.questionType = 'C';
      	      dataParamsS.complaintProgress = btn.complaintProgress;
      	    
//      	      //console.log(valuesSearch);
      	   
      	      dataParamsS.page = '1';
      	      dataParamsS.start = '0';
      	  	  dataParamsS.limit = store.pageSize;
              
//              if(win.actionMode == 'edit')
//              {
                store.load({
//                	params: {questionType: 'C', complaintProgress: btn.complaintProgress},
                	params: dataParamsS,
                	callback: function(records, operation, success) {
                      //console.log(records);
                	},
                	scope: this
                });
//              }else
//              {
//                store.load({
//                    params: {traderId: dataObj.traderId},
//                    callback: function(records, operation, success) {
//                         //console.log(records);
//                    },
//                    scope: this
//                }); 
//              }

              win.close();
        
            },
            failure: function(form, action) {
              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
            },
            scope: this 
        });
        
	}
	
//	,selectLicenseNo: function(btn)
//	  {
//	    var win = btn.up('window');
//	    //console.log(win);
//	    var grid = win.down('info-license-grid');
//	    var model = grid.getSelectionModel().getSelection()[0];
//
//	    //console.log(model);
//	    if(Ext.isEmpty(model))
//	    {
//	      Ext.Msg.alert('เกิดข้อผิดพลาด', 'กรุณาเลือกข้อมูล');
//	      return 
//	    }
//
////	    var layout = win.getLayout();
////	    layout.setActiveItem(1);
//	    
//	    //console.log(model);
//	    win.close();
//	    
//	  }
	
	//ค้นหาใบอนุญาติว่ามีอยู่จริงหรือไม่ก่อน แล้วค่อยดำเนินการต่อ
	,popupSearchLicense: function(btn){
	    var win = Ext.create('tourism.view.complaint.AddWindow',{
	      animateTarget: btn.el
		  ,complaintProgress: btn.complaintProgress
		  ,roleGroup: btn.roleGroup
	    });
	    win.show();

	}
	
	,addComplaint: function(btn, model)
	{
		var win = btn.up('window');
	    var grid = win.down('info-license-grid');
	    var model = grid.getSelectionModel().getSelection()[0];
	    
	    if(Ext.isEmpty(model))
	    {
	      Ext.Msg.alert('เกิดข้อผิดพลาด', 'กรุณาเลือกข้อมูล');
	      return 
	    }
	    
//	    //console.log(win.complaintProgress);
//	    //console.log(model);
//	    //console.log(model.get('traderType'));
//	    //console.log(model.get('traderTypeName'));
//	    //console.log(model.get('licenseNo'));
//	    //console.log(model.get('traderName'));
	    
	    var traderType = model.get('traderType');
	    var traderTypeName = model.get('traderTypeName');
	    var licenseNo = model.get('licenseNo');
	    var traderName = model.get('traderName');
	    var traderId = model.get('traderId');

	    if(Ext.isEmpty(model.get('traderName')))
	    {
	    	traderName = model.get('traderOwnerName');
	    	
	    }
	    
	    win.close();

		var complaintWindow = Ext.create('tourism.view.complaint.addcomplaint.ComplaintAddEditWindow',{
//			  animateTarget: btn.el,
			  complaintProgress: win.complaintProgress
			  ,roleGroup: win.roleGroup
         });
		
		var form = complaintWindow.down('form');
		var values = form.getValues();
		values.traderType = traderType;
		var dataParams = this.normalizeData(values);	
		
	    var fsubmit = form.getForm();
//        var ctrl = this; // scope
 

	    
        fsubmit.load({
            url: 'tourleader/complaint/createcomplaint',
            method : 'POST',
            params: dataParams,
            success: function(form, action) {

              var dataObj = action.result.data;
              
              complaintWindow.show();
      		  
      		  var model = Ext.create('tourism.model.grid.ComplaintLicenseModel');
      		  model.set(dataObj);
      		  
      		  //set for display field
      		  model.set('traderType',traderType);
      		  model.set('traderTypeName',traderTypeName);
      		  model.set('licenseNo',licenseNo);
      		  model.set('traderName',traderName);
      		  //sek edit traderId
      		  model.set('traderId',traderId);
      		  complaintWindow.loadFormRecord(model);

            },
            failure: function(form, action) {
              Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
            },
            scope: fsubmit // this = fsubmit
        });
		
	}
	
	,handleActionColumn : function(column, action, grid, rowIndex, colIndex, model, node) {

		if(action){
	        switch(action){
	        
	        	case 'viewcomplaintprogress':
	        		
	        		var gridf = grid.up('grid');
		        	var win = Ext.create('tourism.view.complaint.complaintprogress.ComplaintProgressWindow',{
	                    animateTarget: column.el,
	                    actionMode: 'edit'
	                    ,complaintProgress: gridf.complaintProgress
	                }).show();
		        	
		        	win.loadFormRecord(model);
		        	
	        	break;
	        
	        	case 'viewQuestion':
	        		var gridf = grid.up('grid');
		        	var win = Ext.create('tourism.view.complaint.question.QuestionInfoWindow',{
	                    animateTarget: column.el,
	                    actionMode: 'view'
	                    ,complaintProgress: gridf.complaintProgress
	                }).show();
		        	
		        	win.loadFormRecord(model);
	        	break;
	        
	        	case 'editQuestion':
	        		var gridf = grid.up('grid');
		        	var win = Ext.create('tourism.view.complaint.question.QuestionAddEditWindow',{
	                    animateTarget: column.el,
	                    actionMode: 'edit'
	                    ,complaintProgress: gridf.complaintProgress
	                    ,roleGroup: gridf.roleGroup
	                }).show();
		        	
		        	win.loadFormRecord(model);
	        	break;
	        
	        	case 'viewComplaintProgressConsider':
	        		
	        		var wininfo = Ext.create('tourism.view.complaint.viewcomplaint.ComplaintProgressConsiderInfoWindow',{
	        		}).show();
	        		
	        		wininfo.loadFormRecord(model);
	        		
	        	break;
	        
	        	case 'deleteComplaintProgressConsider':
	        		var store = grid.getStore();
                    store.remove(model);
                    store.sync();
	        	break;
	        	
	        	case 'editComplaintProgressConsider':
	        		
		        	var gridf = grid.up('grid');
		        	var win = Ext.create('tourism.view.complaint.considercomplaint.ComplaintProgressConsiderAddEditWindow',{
	                    animateTarget: column.el,
	                    actionMode: 'edit'
	                    ,complaintProgress: gridf.complaintProgress
	                }).show();
		        	
		        	win.loadFormRecord(model);
	        	
	        	break;
	        
	        	case 'editcomplaintconsider':
	        		
	        		var gridf = grid.up('grid');
		        	var considerWindow = Ext.create('tourism.view.complaint.considercomplaint.ComplaintConsiderWindow',{
	                    animateTarget: column.el,
	                    actionMode: 'edit'
	                    ,complaintProgress: gridf.complaintProgress
	                }).show();
		        	
//		        	//console.log(model);
		        	
		        	considerWindow.loadFormRecord(model);
	        	break;	
	        
		        case 'editcomplaintreceive': 
		        	var gridf = grid.up('grid');
		               
	            	var edit = Ext.create('tourism.view.complaint.addcomplaint.ComplaintAddEditWindow',{
	                    animateTarget: column.el,
	                    actionMode: 'edit'
	                    ,complaintProgress: gridf.complaintProgress
	                    ,roleGroup: gridf.roleGroup
	                }).show();
	            	
	            	//console.log(model);
	            	
	            	if(model && (model instanceof tourism.model.grid.ComplaintLicenseModel)){
	            		
	            		edit.loadFormRecord(model);
	            	}
	            
	            break;
	            
		        case 'viewcomplaintreceive': 
		        	
		        	var gridf = grid.up('grid');
		        	
		        	var view = Ext.create('tourism.view.complaint.viewcomplaint.ComplaintInfoWindow',{
	                    animateTarget: column.el,
	                    actionMode: 'edit'
	                    ,complaintProgress: gridf.complaintProgress
	                }).show();
		        	
//		        	//console.log(model);
		        	
		        	view.loadFormRecord(model);
		        	
		        break;
		        
		        case 'deletefile':

		        	var gridf = grid.up('grid');
		        	var form = gridf.up('form');
//		        	//console.log(form);
//		        	//console.log(model);
		        	
		        	//E, O
		        	var complaintDocType = form.getForm( ).findField('complaintDocType').getValue();    	
		        	
		        	var values = {};
		        	values.complaintDocId = model.get('complaintDocId');
		        	values.complaintDocPath = model.get('complaintDocPath');
		        	//console.log(values);
		            
		            var fsubmit = form.getForm();
		            
		            fsubmit.load({
		                url: 'tourleader/complaint/deleteDocument',
		                method : 'POST',
		                params: values,
		                waitMsg: 'กรุณารอซักครู่...',
		                success: function(form, action) {

		                var dataObj = action.result.data;
		                //console.log(dataObj);

		                var noti = Ext.create('widget.uxNotification', {
		                  position: 'tr',
		                  manager: 'instructions',
		                  html: '<b>ลบข้อมูลเรียบร้อยแล้ว</b>',
		                  closable: false,
		                  autoCloseDelay: 4000,
		                  width: 300,
		                  slideBackDuration: 500,
		                  slideInAnimation: 'bounceOut',
		                  slideBackAnimation: 'easeIn'
		                });
		                noti.show();
		          
		                  gridf.getStore().load({
		                      params: {complaintLicenseId: model.get('complaintLicenseId'), complaintDocType: complaintDocType},
		                      callback: function(records, operation, success) 
		                      {
		                                        
		                      },
		                      scope: this
		                  });
		            
		                },
		                failure: function(form, action) {
		                  Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
		                },
		                scope: this 
		            });
		        	
	        	break;
	        
	        	case 'viewfile':
	        	
	        		window.open('business/complaint/viewfile?complaintDocPath='+model.get('complaintDocPath'),"Pic","width=600,height=600");
	        	
	        	break;
	        
	        	case 'complaintdocumentUpload':
	        	
		        	var winUpload = Ext.create('tourism.view.complaint.addcomplaint.ComplaintDocumentUploadPanel',{
		        		id: 'complaint-receive-document-upload-window',
		        		roleAction: 'complaint',
	                    actionMode: 'upload',
	                    roleGroup: 'receive'
		        	}).show();
		        	
		        	var form = grid.up('form');

		        	if(form.complaintProgress == 'WL')
		        	{
		        		model.set('complaintDocType','O');
		        	}
		        	else
	        		{
		        		model.set('complaintDocType','E');   
		        		
			        	model.set('traderType', form.getForm().findField('traderType').getValue());
		        		model.set('licenseNo', form.getForm().findField('licenseNo').getValue());
		        		model.set('questionType', 'C');
	        		}
		        	
//		        	//console.log(model);
		        	
		        	winUpload.loadFormRecord(model);
	        	
	        	break;
	        }
		}
		
	}
	
	,
    normalizeData: function(dataObj)
    {
        // remove null data;
       for (field in dataObj) 
        {
          if (Ext.isEmpty(dataObj[field])) 
          {
            delete dataObj[field];
          }
          
        }

        return dataObj;
    }

});



















