Ext.define('tourism.controller.course.CourseController', {
	extend : 'Ext.app.Controller',

   	stores: ['tourism.store.course.CourseStore',
   	         ],

    models: ['tourism.model.grid.PersonTrainedModel'],

    views: [
      'tourism.view.course.CourseSearchForm'
      ,'Ext.ux.window.Notification'
    ],

    refs: [    
       {  
            ref: 'course_grid',
            selector: 'course_grid'
        } 
         ,{
            ref: 'course_addedit_form',
            selector: 'panel'
        }
         ,{
            ref: 'course-formsearch',
            selector: 'panel'
        }
       
    ],	 
	init : function (application) {

        this.control({

             'course_grid button[action=add]': {
                click: this.addCourse
            },
             'course_addedit_form button[action=saveCourse]': {
                click: this.saveCourse
            },
             'course-formsearch button[action=searchCourseBetweenRegistration]': {
            	 click: this.serachCourse
             },
              'course_grid actioncolumn': {
            	 itemclick: this.handleActionColumn
             }
            
        });

		
	},
	serachCourse: function(btn)
	{
		var form = btn.up('form');
		var win = btn.up('window');
		
		var values = form.getValues();
		
		var dataParams = this.normalizeData(values);
	        dataParams.eduRecordStatus = 'E';
	        dataParams.personRecordStatus = 'T';

	     var grid = Ext.ComponentQuery.query('course_grid')[0];
	     var store = grid.getStore();
	     
	     dataParams.page = '1';
	     dataParams.start = '0';
	     dataParams.limit = store.pageSize;
	  
	    store.load({
	    		params: dataParams,
	    		callback: function(records, operation, success) {
	                // //console.log(success);
	            },
	            scope: this
	    });
	},
	addCourse: function(btn)
	{
		var edit = Ext.create('tourism.view.course.CourseAddEditWindow',{
	          animateTarget: btn.el
	         }).show();
	},
	saveCourse: function(btn)
	{
		var form = btn.up('form');
		var win = btn.up('window');
		
		
		var values = form.getValues();
		
		  if (!form.isValid()) {

	          var me = form,
	          errorCmp, fields, errors;

	          fields = me.getForm().getFields();
	          errors = '';
	          fields.each(function(field) {
	              Ext.Array.forEach(field.getErrors(), function(error) {
	                  // errors.push({name: field.getFieldLabel(), error: error});
	                  errors += field.getFieldLabel()  +'<br>';
	              });
	          });
	          // Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', errors);

	          Ext.Msg.show({
	             title:'กรุณาตรวจสอบข้อมูล',
	             msg: errors,
	             buttons: Ext.Msg.OK,
	             icon: Ext.Msg.ERROR
	          });
	          return;
	      }
		
		
		//console.log(values);
		var dataParams = this.normalizeData(values);
	if(win.actionMode != 'edit')
	{	
		form.load({
			url: 'business/course/create',
			method: 'POST',
			params: dataParams,
			success: function(form,action){
				
				 var dataObj = action.result.data;
				 
				 
				 var noti = Ext.create('widget.uxNotification', {
		              // title: 'Notification',
		              position: 'tr',
		              manager: 'instructions',
		              // cls: 'ux-notification-light',
		              // iconCls: 'ux-notification-icon-information',
		              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
		              closable: false,
		              autoCloseDelay: 4000,
		              width: 300,
		              slideBackDuration: 500,
		              slideInAnimation: 'bounceOut',
		              slideBackAnimation: 'easeIn'
		            });
		            noti.show();
		            
		            var grid = Ext.ComponentQuery.query('course_grid')[0];
		            var store = grid.getStore();
		            
		            //if(win.actionMode == 'edit')
		            //  {
		             //   store.reload();
		            //  }else
		              {
		                store.load({
		                    params: {eduId: dataObj.eduId},
		                    callback: function(records, operation, success) {
		                        // //console.log(success);
		                    },
		                    scope: this,
		                }); 
		              }
		            win.close();
			},
			
			failure: function(form, action) {
	              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
	            },
	            scope: this,
		});
	}else
	{
		form.load({
			url: 'business/course/update',
			method: 'POST',
			params: dataParams,
			success: function(form,action){
				
				 var dataObj = action.result.data;
				 
				 
				 var noti = Ext.create('widget.uxNotification', {
		              // title: 'Notification',
		              position: 'tr',
		              manager: 'instructions',
		              // cls: 'ux-notification-light',
		              // iconCls: 'ux-notification-icon-information',
		              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
		              closable: false,
		              autoCloseDelay: 4000,
		              width: 300,
		              slideBackDuration: 500,
		              slideInAnimation: 'bounceOut',
		              slideBackAnimation: 'easeIn'
		            });
		            noti.show();
		            
		            var grid = Ext.ComponentQuery.query('course_grid')[0];
		            var store = grid.getStore();

		                store.load({
		                    params: {eduId: dataObj.eduId},
		                    callback: function(records, operation, success) {
		                        // //console.log(success);
		                    },
		                    scope: this,
		                }); 
		              
		            win.close();
			},
			
			failure: function(form, action) {
	              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
	            },
	            scope: this,
		});
	}
},
  normalizeData: function(dataObj)
  {
      // remove null data;
     for (field in dataObj) 
      {
        if (Ext.isEmpty(dataObj[field])) 
        {
          delete dataObj[field];
        }
        
      }

      return dataObj;
  },
  handleActionColumn: function(column, action, grid, rowIndex, colIndex, model, node)
  {
	  if(action)
	  {
		  switch(action)
		  { 
		  		case 'editcourse':
		  			
		  			var edit = Ext.create('tourism.view.course.CourseAddEditWindow',{
		  				animateTarget: column.el,
		  				actionMode: 'edit'
		  			}).show();
		  			
		  			if(model && (model instanceof tourism.model.grid.PersonTrainedModel))
		  			{
		  				var fedu = edit.down('form');
		  				fedu.loadRecord(model);

		  				if(!Ext.isEmpty(model.get('masUniversityId')))
		  				{
		  					var universityCombo = fedu.getForm().findField('masUniversityId');
		  					universityCombo.getStore().on('load',function(store, records, successful, eOpts){
		  						universityCombo.setValue(model.get('masUniversityId'));
		  					},this,{single:true});
		  					universityCombo.getStore().load();
		  				}else
		  				{
		  					var universityCombo = fedu.getForm().findField('masUniversityId');
		  					universityCombo.clearValue();
		  				}


		  				var countryIdCombo = fedu.getForm().findField('countryId');
		  					countryIdCombo.getStore().on('load',function(store, records, successful, eOpts){
		  						countryIdCombo.setValue(model.get('countryId'));
		  					},this,{single:true});
		  					countryIdCombo.getStore().load();
		  			}
		  			
		  		break;
		  		
		  		
		  		/*case 'deletecourse':
		  			var store = grid.getStore();
                    store.remove(model);
                    store.sync();
                break;*/
		  }
	  }
  }

});