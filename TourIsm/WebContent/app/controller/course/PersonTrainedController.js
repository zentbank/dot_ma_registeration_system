Ext.define('tourism.controller.course.PersonTrainedController', {
	extend : 'Ext.app.Controller',

   	stores: ['tourism.store.course.PersonTrainStore'],

    models: ['tourism.model.grid.PersonTrainedModel'],

    views: [
            'Ext.ux.window.Notification'
            ,'tourism.view.course.PersonCourseSearchForm'
    ],

    refs: [    
        {
            ref: 'person-course-formsearch',
            selector: 'panel'
        },
        {
        	ref: 'person-course-grid',
        	selector: 'person-course-grid'
        },
        {
        	ref: 'person_course_addedit_form',
        	selector: 'panel'
        }
       
    ],	 
	init : function (application) {

        this.control({
              'person-course-grid actioncolumn': {
            	 itemclick: this.handleActionColumn
             },
              'person-course-grid button[action=add]': {
            	 click: this.addPersonCourse
             },
              'person_course_addedit_form button[action=addPersonCourse]': {
            	  click: this.addPerson
             },
              'person-course-formsearch button[action=searchPersonTrained]': {
            	  click: this.searchPersonCourse
              }
        });

		
	},
	searchPersonCourse: function(btn)
	{
		var form = btn.up('form');
		var win = btn.up('window');
		
		var values = form.getValues();
		
		var eduId = form.getForm().findField('eduId').getValue();
		var identityNo = form.getForm().findField('identityNo').getValue();
		
		// if(eduId === null)
	 //    {

		// 	var errors = 'กรุณาเลือก หลักสูตรฝึกอบรม';
			
	 //    	Ext.Msg.show({
	 //             title:'กรุณาตรวจสอบข้อมูล',
	 //             msg: errors,
	 //             buttons: Ext.Msg.OK,
	 //             icon: Ext.Msg.ERROR
	 //          });
	 //          return;
	 //    }
		
		var dataParams = this.normalizeData(values);
//	    console.log(dataParams);    

	     var grid = Ext.ComponentQuery.query('person-course-grid')[0];
	     var store = grid.getStore();
	     
	     dataParams.page = '1';
	     dataParams.start = '0';
	     dataParams.limit = store.pageSize;
	  
	    store.load({
	    		params: dataParams,
	    		callback: function(records, operation, success) {
	                // //console.log(success);
	            },
	            scope: this
	    });
	},
	addPersonCourse: function(btn){
		var edit = Ext.create('tourism.view.course.PersonCourseAddEditWindow',{
	          animateTarget: btn.el
	         }).show();
	},
  normalizeData: function(dataObj)
  {
      // remove null data;
     for (field in dataObj) 
      {
        if (Ext.isEmpty(dataObj[field])) 
        {
          delete dataObj[field];
        }
        
      }

      return dataObj;
  },
  handleActionColumn: function(column, action, grid, rowIndex, colIndex, model, node)
  {
	  if(action)
	  {
		  switch(action)
		  { 
		  		case 'editperson':
		  			
		  			var edit = Ext.create('tourism.view.course.PersonCourseAddEditWindow',{
		  				animateTarget: column.el,
		  				actionMode: 'edit'
		  			}).show();
		  			
		  			if(model && (model instanceof tourism.model.grid.PersonTrainedModel))
		  			{
		  				var fperson = edit.down('form');
		  				fperson.loadRecord(model);
		  				
		  				//console.log(model);
		  				//courseName
		  				if(!Ext.isEmpty(model.get('eduId')))
                        {
                          var courseCombo = fperson.getForm().findField('eduId');
                          courseCombo.getStore().on('load',function(store, records, successful, eOpts){
                        	  courseCombo.setValue(model.get('eduId'));
                          },this,{single:true});
                          courseCombo.getStore().load();
                        }
                        else
                        {
                          var provinceCombo = fperson.getForm().findField('provinceId');
                          provinceCombo.clearValue();
                        }
		  				
		  				 //Province
                        if(!Ext.isEmpty(model.get('provinceId')))
                        {
                          var provinceCombo = fperson.getForm().findField('provinceId');
                          provinceCombo.getStore().on('load',function(store, records, successful, eOpts){
                            provinceCombo.setValue(model.get('provinceId'));
                          },this,{single:true});
                          provinceCombo.getStore().load();
                        }
                        else
                        {
                          var provinceCombo = fperson.getForm().findField('provinceId');
                          provinceCombo.clearValue();
                        }
                        
                      //Amphur
                        if(!Ext.isEmpty(model.get('amphurId')))
                        {
                          var amphurCombo = fperson.getForm().findField('amphurId');
                          amphurCombo.getStore().on('load',function(store, records, successful, eOpts){
                            amphurCombo.setValue(model.get('amphurId'));
                          },this,{single:true});
                          amphurCombo.getStore().load();
                        }
                        else
                        {
                           var amphurCombo = fperson.getForm().findField('amphurId');
                           amphurCombo.clearValue();
                        }
                        
                        
                        //Prefix
                        if(!Ext.isEmpty(model.get('prefixId')))
                        {
                          var prefixIdCombo = fperson.getForm().findField('prefixId');
                          var prefixIdEnCombo = fperson.getForm().findField('prefixIdEn');
                          prefixIdCombo.getStore().on('load',function(store, records, successful, eOpts){
                            prefixIdCombo.setValue(model.get('prefixId'));
                            prefixIdEnCombo.setValue(model.get('prefixId'));
                          },this,{single:true});
                          prefixIdCombo.getStore().load();
                        }
                        else
                        {
                          var prefixIdCombo = fperson.getForm().findField('prefixId');
                          var prefixIdEnCombo = fperson.getForm().findField('prefixIdEn');
                          prefixIdCombo.clearValue();
                          prefixIdEnCombo.clearValue();
                        }
		  			}
		  			
		  		break;
		  		
		  		
		  		case 'deleteperson':
		  			var store = grid.getStore();
                    store.remove(model);
                    store.sync();
                break;
		  }
	  }
  },
  addPerson: function(btn)
  {
	  var form = btn.up('form');
	  var win = btn.up('window');
	  
	  var values = form.getValues();
	  
	  if (!form.isValid()) {

          var me = form,
          errorCmp, fields, errors;

          fields = me.getForm().getFields();
          errors = '';
          fields.each(function(field) {
              Ext.Array.forEach(field.getErrors(), function(error) {
                  errors += field.getFieldLabel()  +'<br>';
              });
          });
          
          
          Ext.Msg.show({
             title:'กรุณาตรวจสอบข้อมูล',
             msg: errors,
             buttons: Ext.Msg.OK,
             icon: Ext.Msg.ERROR
          });
          return;
      }
	  
	  var dataParams = this.normalizeData(values);
	
	  if(win.actionMode != 'edit')
	{
	  form.load({
		  	url: 'business/course/addperson',
		  	method: 'POST',
			params: dataParams,
		  	success: function(form,action){
				
				 var dataObj = action.result.data;
				 
				 
				 var noti = Ext.create('widget.uxNotification', {
		              // title: 'Notification',
		              position: 'tr',
		              manager: 'instructions',
		              // cls: 'ux-notification-light',
		              // iconCls: 'ux-notification-icon-information',
		              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
		              closable: false,
		              autoCloseDelay: 4000,
		              width: 300,
		              slideBackDuration: 500,
		              slideInAnimation: 'bounceOut',
		              slideBackAnimation: 'easeIn'
		            });
		            noti.show();
		            
		            var grid = Ext.ComponentQuery.query('person-course-grid')[0];
		            var store = grid.getStore();
		            
		              
		                store.load({
		                    params: {personTrainedId: dataObj.personTrainedId},
		                    callback: function(records, operation, success) {
		                        // //console.log(success);
		                    },
		                    scope: this,
		                }); 
		              
		            win.close();
			},
			
			failure: function(form, action) {
	              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
	            },
	            scope: this,
		});
	}else
	{
		form.load({
		  	url: 'business/course/updateperson',
		  	method: 'POST',
			params: dataParams,
		  	success: function(form,action){
				
				 var dataObj = action.result.data;
				 
				 
				 var noti = Ext.create('widget.uxNotification', {
		              // title: 'Notification',
		              position: 'tr',
		              manager: 'instructions',
		              // cls: 'ux-notification-light',
		              // iconCls: 'ux-notification-icon-information',
		              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
		              closable: false,
		              autoCloseDelay: 4000,
		              width: 300,
		              slideBackDuration: 500,
		              slideInAnimation: 'bounceOut',
		              slideBackAnimation: 'easeIn'
		            });
		            noti.show();
		            
		            var grid = Ext.ComponentQuery.query('person-course-grid')[0];
		            var store = grid.getStore();
		            
		              
		                store.load({
		                    params: {personTrainedId: dataObj.personTrainedId},
		                    callback: function(records, operation, success) {
		                        // //console.log(success);
		                    },
		                    scope: this,
		                }); 
		              
		            win.close();
			},
			
			failure: function(form, action) {
	              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
	            },
	            scope: this,
		});
	}
  }
  
 
});