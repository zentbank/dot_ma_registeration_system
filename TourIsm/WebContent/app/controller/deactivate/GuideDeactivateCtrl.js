Ext
		.define(
				'tourism.controller.deactivate.GuideDeactivateCtrl',
				{
					extend : 'Ext.app.Controller',
					refs : [

					{
						ref : '#licenses-deactivate-guide-grid',
						selector : '#licenses-deactivate-guide-grid'
					}, {
						ref : '#licenses-deactivate-guide-formsearch',
						selector : 'panel'
					}, {
						ref : '#licenses-deactivate-guide-searchlicensepanel',
						selector : 'panel'
					}, {
						ref : '#licenses-deactivate-guide-formaddedit',
						selector : 'panel'
					}, {
						ref : '#licenses-deactivate-guide-alertform',
						selector : 'panel'
					}

					],
					init : function(application) {

						this
								.control({
									'#licenses-deactivate-guide-searchlicensepanel button[action=searchLicense]' : {
										click : this.searchLicense
									},
									'#licenses-deactivate-guide-searchlicensepanel button[action=selectedLicenseNo]' : {
										click : this.selectedLicenseNo
									},
									'#licenses-deactivate-guide-formaddedit button[action=addDeactivate]' : {
										click : this.addDeactivate
									},
									'#licenses-deactivate-guide-formaddedit button[action=approveDeactivate]' : {
										click : this.approveDeactivate
									},
									'#licenses-deactivate-guide-grid button[action=adddeactivate]' : {
										click : this.adddeactivate
									},
									'#licenses-deactivate-guide-grid actioncolumn' : {
										itemclick : this.handleActionColumn
									},
									'#licenses-deactivate-guide-formsearch button[action=searchDeactivate]' : {
										click : this.searchDeactivate
									}

								});

					},
					searchLicense : function(btn) {
						var form = btn.up('form');
						var grid = form.down('diactivate-searchLicenseGrid');

						var licenseNo = form.getForm().findField('licenseNo')
								.getValue();

						if (Ext.isEmpty(licenseNo)) {
							Ext.Msg.show({
								title : 'เกิดข้อผิดพลาด',
								msg : 'กรุณากรอกข้อมูลเลขที่ใบอนุยาต',
								buttons : Ext.Msg.OK,
								icon : Ext.Msg.ERROR
							});
							return;
						}

						grid.getStore().load({
							params : {
								traderType : 'G',
								licenseNo : licenseNo
							},
							callback : function(records, operation, success) {

							},
							scope : this
						});

					},

					adddeactivate : function(btn) {

						var win = Ext.create(
								'tourism.view.deactivate.AddDeactivateWindow',
								{
									traderType : 'G',
									traderTypeName : 'guide',
									roleAction : 'deactivate'
								});
						win.show();

//						var form = win.down('info-license-formsearch');
//
//						form.getForm().findField('traderType').setValue('G');
					},
					selectedLicenseNo : function(btn) {
						var win = btn.up('window');
						var grid = win.down('diactivate-searchLicenseGrid');
						var model = grid.getSelectionModel().getSelection()[0];

						var formSuspend = win
								.down('#licenses-deactivate-guide-formaddedit');

						var actGrid = win.down('deactivateType-grid');

						if (Ext.isEmpty(model)) {
							Ext.Msg.alert('เกิดข้อผิดพลาด', 'กรุณาเลือกข้อมูล');
							return 

						}

						// fsubmit.loadRecord(model);

						actGrid.getStore().load({
							params : {
								traderType : model.get('traderType')
							},
							callback : function(records, operation, success) {
								// //console.log(success);
							},
							scope : this
						});

						formSuspend
								.load({
									url : 'licenses/deactivate/guide/prepare',
									method : 'POST',
									waitMsg : 'กรุณารอสักครู่..',
									params : {
										traderId : model.get('traderId')
									},
									success : function(form, action) {

										var dataObj = action.result.data;

										var revokeModel = Ext
												.create('tourism.model.deactivate.DeactivateModel');

										revokeModel.set(dataObj);

										formSuspend.loadRecord(revokeModel);

									},
									failure : function(form, action) {
										Ext.Msg
												.alert(
														'Failed',
														action.result ? action.result.message
																: 'No response');
									},
									scope : formSuspend
								// this = fsubmit
								});

						var layout = win.getLayout();
						layout.setActiveItem(1);

						formSuspend.down(
								'#licenses-deactivate-guide-approveDeactivate')
								.setVisible(false);
						var renewLicenseStatus = formSuspend
								.down('#licenses-deactivate-guide-renewLicenseStatus-checkboxgroup');

						renewLicenseStatus.allowBlank = true;
						renewLicenseStatus.setVisible(false);

						var bookDate = formSuspend.getForm().findField(
								'bookDate');
						bookDate.allowBlank = true;
						bookDate.setVisible(false);

						var bookNo = formSuspend.getForm().findField('bookNo');
						bookNo.allowBlank = true;
						bookNo.setVisible(false);

					},
					addDeactivate : function(btn) {
						var win = btn.up('window');
						var actGrid = win.down('deactivateType-grid');
						var listActModel = new Array();

						actGrid
								.getStore()
								.each(
										function(model) {

											if (model.get('active')) {
												listActModel
														.push({
															masDeactivateTypeId : model
																	.get('masDeactivateTypeId'),
															deactivateName : model
																	.get('deactivateName')
														});
											}

										});

						if (listActModel.length < 0) {
							Ext.Msg.alert('เกิดข้อผิดพลาด', 'สาหตุการพักใช้');
							return;
						}

						var formSuspend = win
								.down('#licenses-deactivate-guide-formaddedit');

						if (!formSuspend.isValid()) {

							var me = formSuspend, errorCmp, fields, errors;

							fields = me.getForm().getFields();
							errors = '';
							fields.each(function(field) {
								Ext.Array.forEach(field.getErrors(), function(
										error) {
									// errors.push({name: field.getFieldLabel(),
									// error: error});
									errors += field.getFieldLabel() + '<br>';
								});
							});
							// Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล',
							// errors);

							Ext.Msg.show({
								title : 'กรุณาตรวจสอบข้อมูล',
								msg : errors,
								buttons : Ext.Msg.OK,
								icon : Ext.Msg.ERROR
							});
							return;
						}

						var actStr = Ext.encode(listActModel);

						var values = formSuspend.getValues();

						var dataParams = this.normalizeData(values);

						dataParams.masAct = actStr;

						// fsubmit.submit({
						formSuspend
								.load({
									url : 'licenses/deactivate/guide/adddeactivate',
									method : 'POST',
									waitMsg : 'กรุณารอสักครู่..',
									params : dataParams,
									success : function(form, action) {

										var dataObj = action.result.data;

										// Ext.Msg.alert('', 'บันทึกข้อมูล');

										var noti = Ext
												.create(
														'widget.uxNotification',
														{
															// title:
															// 'Notification',
															position : 'tr',
															manager : 'instructions',
															// cls:
															// 'ux-notification-light',
															// iconCls:
															// 'ux-notification-icon-information',
															html : '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
															closable : false,
															autoCloseDelay : 4000,
															width : 300,
															slideBackDuration : 500,
															slideInAnimation : 'bounceOut',
															slideBackAnimation : 'easeIn'
														});
										noti.show();

										var grid = Ext.ComponentQuery
												.query('#licenses-deactivate-guide-grid')[0];
										var store = grid.getStore();

										store.reload();

										win.close();

									},
									failure : function(form, action) {
										Ext.Msg
												.alert(
														'เกิดข้อผิดพลาด',
														action.result ? action.result.message
																: 'No response');
									},
									scope : this
								});

					},
					normalizeData : function(dataObj) {
						// remove null data;
						for (field in dataObj) {
							if (Ext.isEmpty(dataObj[field])) {
								delete dataObj[field];
							}

						}

						return dataObj;
					},
					handleActionColumn : function(column, action, grid,
							rowIndex, colIndex, model, node) {
						if (action) {
							switch (action) {
							case 'deactivatedetail':

								var win = Ext
										.create(
												'tourism.view.deactivate.AddDeactivateWindow',
												{
													traderType : 'G',
													traderTypeName : 'guide',
													roleAction : 'deactivate'
												});
								win.show();

								var layout = win.getLayout();
								layout.setActiveItem(1);

								var formSuspend = win
										.down('#licenses-deactivate-guide-formaddedit');

								var actGrid = win.down('deactivateType-grid');

								actGrid.getStore().load(
										{
											params : {
												deactiveId : model
														.get('deactiveId'),
												licenseType : model
														.get('traderType')
											},
											callback : function(records,
													operation, success) {

											},
											scope : this
										});

								formSuspend.loadRecord(model);

								formSuspend
										.down(
												'#licenses-deactivate-guide-approveDeactivate')
										.setVisible(false);
								var renewLicenseStatus = formSuspend
										.down('#licenses-deactivate-guide-renewLicenseStatus-checkboxgroup');

								renewLicenseStatus.allowBlank = true;
								renewLicenseStatus.setVisible(false);

								var bookDate = formSuspend.getForm().findField(
										'bookDate');
								bookDate.allowBlank = true;
								bookDate.setVisible(false);

								var bookNo = formSuspend.getForm().findField(
										'bookNo');
								bookNo.allowBlank = true;
								bookNo.setVisible(false);

								break;

							case 'approve':

								var win = Ext
										.create(
												'tourism.view.deactivate.AddDeactivateWindow',
												{
													traderType : 'G',
													traderTypeName : 'guide',
													roleAction : 'deactivate'
												});
								win.show();

								var layout = win.getLayout();
								layout.setActiveItem(1);

								var formSuspend = win
										.down('#licenses-deactivate-guide-formaddedit');

								var actGrid = win.down('deactivateType-grid');

								actGrid.getStore().load(
										{
											params : {
												deactiveId : model
														.get('deactiveId'),
												licenseType : model
														.get('traderType')
											},
											callback : function(records,
													operation, success) {

											},
											scope : this
										});

								formSuspend.loadRecord(model);

								formSuspend
										.down(
												'#licenses-deactivate-guide-addDeactivate')
										.setVisible(false);
								// formSuspend.down('#licenses-deactivate-guide-renewLicenseStatus-checkboxgroup').setVisible(false);

								// var bookDate =
								// formSuspend.getForm().findField('bookDate');
								// bookDate.allowBlank = false;
								// bookDate.setVisible(false);

								// var bookNo =
								// formSuspend.getForm().findField('bookNo');
								// bookNo.allowBlank = false;
								// bookNo.setVisible(false);
								break;

							case 'canceldeactivate':
								Ext.Msg
										.prompt(
												'สาเหตุการยกเลิก',
												'สาเหตุการยกเลิก:',
												function(btn, text) {

													if (btn == 'ok') {

														if (Ext.isEmpty(text)) {
															Ext.Msg
																	.alert(
																			'เกิดข้อผิดพลาด',
																			'กรุรากรอกข้อมูล สาเหตุการยกเลิก');
															return;
														}
														var formcancel = Ext
																.create('Ext.form.Panel');

														formcancel
																.load({
																	url : 'licenses/deactivate/guide/activate',
																	method : 'POST',
																	waitMsg : 'กรุณารอสักครู่...',
																	params : {
																		deactiveId : model
																				.get('deactiveId'),
																		remark : text
																	},
																	success : function(
																			form,
																			action) {

																		var grid = Ext.ComponentQuery
																				.query('#licenses-deactivate-guide-grid')[0];
																		var store = grid
																				.getStore();

																		store
																				.reload();

																	},
																	failure : function(
																			form,
																			action) {
																		Ext.Msg
																				.alert(
																						'เกิดข้อผิดพลาด',
																						action.result ? action.result.message
																								: 'No response');
																	},
																	scope : this
																});
													}
												});

								// Ext.MessageBox.confirm('ยืนยัน',
								// 'คุรต้องการยกเลิกการพักใช้นี้',
								// function(btn){
								// //console.log(btn);
								// if (btn == 'yes'){

								// }
								// });

								break;
							}
						}
					},
					approveDeactivate : function(btn) {
						var win = btn.up('window');
						var actGrid = win.down('deactivateType-grid');
						var listActModel = new Array();

						actGrid
								.getStore()
								.each(
										function(model) {

											if (model.get('active')) {
												listActModel
														.push({
															masDeactivateTypeId : model
																	.get('masDeactivateTypeId'),
															deactivateName : model
																	.get('deactivateName')
														});
											}

										});

						if (listActModel.length < 0) {
							Ext.Msg.alert('เกิดข้อผิดพลาด', 'สาหตุการพักใช้');
							return;
						}

						var formSuspend = win
								.down('#licenses-deactivate-guide-formaddedit');

						if (!formSuspend.isValid()) {

							var me = formSuspend, errorCmp, fields, errors;

							fields = me.getForm().getFields();
							errors = '';
							fields.each(function(field) {
								Ext.Array.forEach(field.getErrors(), function(
										error) {
									// errors.push({name: field.getFieldLabel(),
									// error: error});
									errors += field.getFieldLabel() + '<br>';
								});
							});
							// Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล',
							// errors);

							Ext.Msg.show({
								title : 'กรุณาตรวจสอบข้อมูล',
								msg : errors,
								buttons : Ext.Msg.OK,
								icon : Ext.Msg.ERROR
							});
							return;
						}

						var actStr = Ext.encode(listActModel);

						var values = formSuspend.getValues();

						var dataParams = this.normalizeData(values);

						dataParams.masAct = actStr;

						// fsubmit.submit({
						formSuspend
								.load({
									url : 'licenses/deactivate/guide/deactivate',
									method : 'POST',
									params : dataParams,
									waitMsg : 'กรุณารอสักครู่..',
									success : function(form, action) {

										var dataObj = action.result.data;

										// Ext.Msg.alert('', 'บันทึกข้อมูล');

										var noti = Ext
												.create(
														'widget.uxNotification',
														{
															// title:
															// 'Notification',
															position : 'tr',
															manager : 'instructions',
															// cls:
															// 'ux-notification-light',
															// iconCls:
															// 'ux-notification-icon-information',
															html : '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
															closable : false,
															autoCloseDelay : 4000,
															width : 300,
															slideBackDuration : 500,
															slideInAnimation : 'bounceOut',
															slideBackAnimation : 'easeIn'
														});
										noti.show();

										var grid = Ext.ComponentQuery
												.query('#licenses-deactivate-guide-grid')[0];
										var store = grid.getStore();

										store.reload();

										win.close();

									},
									failure : function(form, action) {
										Ext.Msg
												.alert(
														'เกิดข้อผิดพลาด',
														action.result ? action.result.message
																: 'No response');
									},
									scope : this
								});
					},
					searchDeactivate : function(btn) {
						var formSearch = btn.up('form');
						var values = formSearch.getValues();

						var dataParams = this.normalizeData(values);

						var grid = Ext.ComponentQuery
								.query('#licenses-deactivate-guide-grid')[0];

						dataParams.page = '1';
						dataParams.start = '0';
						dataParams.limit = grid.getStore().pageSize;
						dataParams.roleAction = 'deactivate';

						grid.getStore().load({
							params : dataParams,
							callback : function(records, operation, success) {

							},
							scope : this
						});
					}

				});