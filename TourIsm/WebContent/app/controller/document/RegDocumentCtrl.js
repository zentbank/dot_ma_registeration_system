Ext.define('tourism.controller.document.RegDocumentCtrl',{
	extend : 'Ext.app.Controller',

  refs:[{
   	 ref:'verification-reg-document-check-doc-form',
   	 selector:'panel'
  }],
  init : function (application) {

    this.control({
    	'verification-reg-document-grid actioncolumn': {
            itemclick: this.handleActionColumn
        },
        'verification-reg-document-check-doc-form button[action=saveCheckDocs]': {
            click: this.saveCheckDocs
        }
    });
   }
   ,handleActionColumn : function(column, action, grid, rowIndex, colIndex, model, node) {

		if(action){
	        switch(action){
	        
	        case 'considerDocs':
	        	
	        	var winUpload = Ext.create('tourism.view.window.verification.CheckDocumentWindow',{
	        		
	        	}).show();
	        	var form = winUpload.down('verification-reg-document-check-doc-form');
	        	if(Ext.isEmpty(model.get('masDocId'))){
	        		form.getForm().findField('masDocId').setValue(model.get('masDocId'));
	        	}
	        	
	        	form.getForm().findField('traderId').setValue(model.get('traderId'));
	        	form.getForm().findField('regDocId').setValue(model.get('regDocId'));
	        	form.getForm().findField('documentName').setValue(model.get('documentName'));
	        	
	        	;
	        	if(!Ext.isEmpty(model.get('docStatus'))){
	        		form.getForm().findField('docStatus').setValue(model.get('docStatus'));
	        	}
	        	if(!Ext.isEmpty(model.get('docRemark'))){
	        		form.getForm().findField('docRemark').setValue(model.get('docRemark'));
	        	}
	        	
	        	
	        break;
	        
	       
             
         }
		}
		
	},
	saveCheckDocs: function(btn){
		var win = btn.up('window');
		  
	    var form = win.down('verification-reg-document-check-doc-form');

	      if (!form.isValid()) {

	          var me = form,
	          errorCmp, fields, errors;

	          fields = me.getForm().getFields();
	          errors = '';
	          fields.each(function(field) {
	              Ext.Array.forEach(field.getErrors(), function(error) {
	                  errors += field.getFieldLabel()  +'<br>';
	              });
	          });

	          Ext.Msg.show({
	             title:'กรุณาตรวจสอบข้อมูล',
	             msg: errors,
	             buttons: Ext.Msg.OK,
	             icon: Ext.Msg.ERROR
	          });
	          return;
	      }
	        
	        var values = form.getValues();

	        var dataParams = this.normalizeData(values);

	         // fsubmit.submit({
	        form.load({
	            url: 'business/document/registration/updateCheckdocs',
	            method : 'POST',
	            waitMsg: 'กรุณารอสักครู่...',
	            params: dataParams,
	            success: function(form, action) {

	            	var dataObj = action.result.data;

		            var noti = Ext.create('widget.uxNotification', {
		              position: 'tr',
		              manager: 'instructions',
		              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
		              closable: false,
		              autoCloseDelay: 4000,
		              width: 300,
		              slideBackDuration: 500,
		              slideInAnimation: 'bounceOut',
		              slideBackAnimation: 'easeIn'
		            });
		            noti.show();
	
		            var grid = Ext.ComponentQuery.query('verification-reg-document-grid')[0];
		            var store = grid.getStore();
		            store.reload(); 
		            
		            win.close();
	                      
	            },
	            failure: function(form, action) {
	              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
	            },
	            scope: this 
	        });
	},
	 normalizeData: function(dataObj)
	  {
	     // remove null data;
	    for (field in dataObj) 
	     {
	       if (Ext.isEmpty(dataObj[field])) 
	       {
	         delete dataObj[field];
	       }
	       
	     }

	     return dataObj;
	  }
});