Ext.define('tourism.controller.document.registration.RegistrationDocumentCtrl', {
	extend : 'Ext.app.Controller',

   	stores: ['tourism.store.registration.RegistrationStore'],

    models: ['tourism.model.registration.RegistrationModel'],

    views: [
            'tourism.view.document.registration.RegistrationDocumentBusinessFormSearch'
            ,'tourism.view.document.registration.RegistrationDocumentGuideFormSearch'
            ,'tourism.view.document.registration.RegistrationDocumentTourleaderFormSearch'
            
    ]

	,refs: [
	{  
	    ref: 'document-business-formsearch',
	    selector: 'panel'
	} 
	,{  
	    ref: 'document-guide-formsearch',
	    selector: 'panel'
	} 
	,{  
	    ref: 'document-tourleader-formsearch',
	    selector: 'panel'
	} 
	]

	,init : function (application) {

        this.control({
        	'document-business-formsearch button[action=searchBusinessDocument]': {
                click: this.searchBusinessDocument
            }
	        ,'document-guide-formsearch button[action=searchGuideDocument]': {
	            click: this.searchGuideDocument
	        }
	        ,'document-tourleader-formsearch button[action=searchTourleaderDocument]': {
	            click: this.searchTourleaderDocument
	        }
	        ,'document-business-grid actioncolumn': {
                itemclick: this.handleActionColumn
            }
	        ,'document-guide-grid actioncolumn': {
                itemclick: this.handleActionColumn
            }
	        ,'document-tourleader-grid actioncolumn': {
                itemclick: this.handleActionColumn
            }
	        
	        ,'document-business-popup-grid actioncolumn': {
	        	itemclick: this.handleActionColumn
	        }
	        ,'document-guide-popup-grid actioncolumn': {
	        	itemclick: this.handleActionColumn
	        }
	        ,'document-tourleader-popup-grid actioncolumn': {
	        	itemclick: this.handleActionColumn
	        }
        });
        
	}

	,handleActionColumn : function(column, action, grid, rowIndex, colIndex, model, node) {

		if(action){
	        switch(action){
	        
	        case 'viewfile':
	        	
	        	window.open('business/document/registration/viewfile?docPath='+model.get('docPath'),"Pic","width=600,height=600");
	        	
	        break;
	        
	        case 'documentbusinessUpload':
	        	
	        	var winUpload = Ext.create('tourism.view.document.registration.DocumentUploadPanel',{
	        		id: 'business-registration-upload-window',
//                    animateTarget: column.el,
	        		roleAction: 'document',
                    actionMode: 'upload',
                    traderType: 'business'
	        	}).show();
	        	
	        	model.set('licenseType','B');
	        	
	        	//console.log(model);
	        	winUpload.loadFormRecord(model);
	        	
	        break;
	        
	        case 'documentguideUpload':
	        	
	        	var winUpload = Ext.create('tourism.view.document.registration.DocumentUploadPanel',{
	        		id: 'guide-registration-upload-window',
//                    animateTarget: column.el,
	        		roleAction: 'document',
                    actionMode: 'upload',
                    traderType: 'guide'
	        	}).show();
	        	
	        	model.set('licenseType','G');
	        	
	
	        	winUpload.loadFormRecord(model);
	        	
	        break;
	        
	        case 'documenttourleaderUpload':
	        	
	        	var winUpload = Ext.create('tourism.view.document.registration.DocumentUploadPanel',{
	        		id: 'tourleader-registration-upload-window',
//                    animateTarget: column.el,
	        		roleAction: 'document',
                    actionMode: 'upload',
                    traderType: 'tourleader'
	        	}).show();
	        	
	        	model.set('licenseType','L');
	        	
	        	winUpload.loadFormRecord(model);
	        	
	        break;
	        
	        case 'viewdocumentbusiness':
                
                var winInfo = Ext.create('tourism.view.information.business.InfoBusinessWindow',{
                    id: 'business-information-business-registration-infowindow',
                    animateTarget: column.el,
                    actionMode: '',
                    roleAction: 'document',
                  });
                
                  
                  winInfo.show();

                  winInfo.loadFormRecord(model);
              break;
              
		        case 'viewdocumentguide':
		        	
		        	var winInfo = Ext.create('tourism.view.information.guide.InfoGuideWindow',{
                		id: 'guide-information-guide-registration-infowindow',
                		animateTarget: column.el,
                      actionMode: 'information',
//                      roleAction: 'KEY',
                      roleAction: 'document'
                	});
	                 
	                  
	                  winInfo.show();

	                  winInfo.loadFormRecord(model);
		        
		        break;
		        
		        case 'viewdocumenttourleader':
		        	var winInfo = Ext.create('tourism.view.information.tourleader.InfoTourleaderWindow',{
                		id: 'tourleader-information-tourleader-registration-infowindow',
                		animateTarget: column.el,
                      actionMode: '',
//                      roleAction: '',
                      roleAction: 'document'
                	}).show();
                	 
	                  
	                  winInfo.show();
                	
                	winInfo.loadFormRecord(model);
		        break;
              
          }
		}
		
	}
	
	,searchBusinessDocument: function(btn){
		var form = btn.up('form'),
	    values = form.getValues();
	    var dataParams = this.normalizeData(values);
	    dataParams.traderType = 'B';
	    
	    var grid = Ext.ComponentQuery.query('#document-business-document-grid')[0];
	    var store = grid.getStore();
	   
	     dataParams.page = '1';
	     dataParams.start = '0';
	     dataParams.limit = store.pageSize;


	     
	      store.load({
	          params: dataParams,
	          callback: function(records, operation, success) {
	
	          },
	          scope: this
	      }); 
	     
	}
	
	,searchGuideDocument: function(btn){
		var form = btn.up('form'),
	    values = form.getValues();
	    var dataParams = this.normalizeData(values);
	    dataParams.traderType = 'G';
	    
	    var grid = Ext.ComponentQuery.query('#document-guide-document-grid')[0];
	    var store = grid.getStore();
	   
	     dataParams.page = '1';
	     dataParams.start = '0';
	     dataParams.limit = store.pageSize;

	    //console.log(dataParams);
	     
	      store.load({
	          params: dataParams,
	          callback: function(records, operation, success) {
	               //console.log(records);
	          },
	          scope: this
	      }); 
	     
	}
	
	,searchTourleaderDocument: function(btn){
		var form = btn.up('form'),
	    values = form.getValues();
	    var dataParams = this.normalizeData(values);
	    dataParams.traderType = 'L';
	    
	    var grid = Ext.ComponentQuery.query('#document-tourleader-document-grid')[0];
	    var store = grid.getStore();
	   
	     dataParams.page = '1';
	     dataParams.start = '0';
	     dataParams.limit = store.pageSize;

	    //console.log(dataParams);
	     
	      store.load({
	          params: dataParams,
	          callback: function(records, operation, success) {
	               //console.log(records);
	          },
	          scope: this
	      }); 
	     
	}
	
	,normalizeData: function(dataObj)
	  {
	      // remove null data;
	     for (field in dataObj) 
	      {
	        if (Ext.isEmpty(dataObj[field])) 
	        {
	          delete dataObj[field];
	        }
	        
	      }

	      return dataObj;
	  }

});














