Ext.define('tourism.controller.guide.registration.guideCatagory.GuideCatagoryController',{
	extend: 'Ext.app.Controller',
	stores: ['tourism.store.combo.GuideCategoryStore'],

    models: ['tourism.model.combo.GuideCategoryModel'],
    
    views: [
            'tourism.view.guide.registration.registrationTab.guideType.GuideTypeAddEditForm'
           ],
           
    refs:[    
            {
               autoCreate: true,
               ref: 'guide-registration-tradercategory-traderCategorygroup',
               selector: '#guide-registration-tradercategory-traderCategorygroup', // itemId for first radio group
               xtype: 'Ext.form.RadioGroup'
            },
            {
                ref: 'guide-type-registration-categorystore-combo',
                selector: '#guide-type-registration-categorystore-combo',
                xtype: 'combo',
          }
         ],
         
         init : function (application) {
        	    
        	    this.control({
        	        "#guide-registration-tradercategory-traderCategorygroup": {
        	            change: this.onFirstbtngroupChange,
        	            afterrender: this.onFirstbtngroupAfterRender
        	        }
        	    });
         },
         onFirstbtngroupChange: function(field, newValue, oldValue, eOpts) {
             this.isShowCountry(newValue.traderCategoryType, field);
             
             var combo = form.getForm().findField('guide-type-registration-categorystore-combo');
             combo.clearValue();
             combo.setRawValue("");
             
         },
         onFirstbtngroupAfterRender: function(radioGroupComp, eOpts) {

             var win    = radioGroupComp.up('window');
             form   = win.down('form');
             var traderCategorySelected = radioGroupComp.getValue();
             this.isShowCountry(traderCategorySelected.traderCategoryType, radioGroupComp);
         
         },
         isShowCountry: function(traderCategorySelected, radioGroupComp)
         {
        	 var win    = radioGroupComp.up('window');
             form   = win.down('form');

             var comboProvince = form.getForm().findField('guide-type-registration-masprovince-combo');
              
        	 var combo = form.getForm().findField('guide-type-registration-categorystore-combo');
        	 var store = combo.getStore();

             if(traderCategorySelected){
               switch(traderCategorySelected){
                   case '1': 
                	 store.removeAll();
                     store.add({"traderCategory":"100", "traderCategoryName":"ชนิดมัคคุเทศก์ทั่วไป (ต่างประเทศ)"});
                     store.add({"traderCategory":"101", "traderCategoryName":"ชนิดมัคคุเทศก์ทั่วไป (ไทย)"});
                     comboProvince.clearValue();
                     comboProvince.setRawValue("");
                   break;

                   case '2': 
                	   store.removeAll();
                	   store.add({"traderCategory":"200", "traderCategoryName":"มัคคุเทศก์เฉพาะ (ต่างประเทศ-เฉพาะพื้นที่)"});
                       store.add({"traderCategory":"201", "traderCategoryName":"มัคคุเทศก์เฉพาะ (ไทย-เฉพาะพื้นที่)"});
                  	   store.add({"traderCategory":"202", "traderCategoryName":"มัคคุเทศก์เฉพาะ (เดินป่า)"});
                 	   store.add({"traderCategory":"203", "traderCategoryName":"มัคคุเทศก์เฉพาะ (ศิลปะวัฒนธรรม)"});
                  	   store.add({"traderCategory":"204", "traderCategoryName":"มัคคุเทศก์เฉพาะ (ทางทะเล)"});                      
                  	   store.add({"traderCategory":"205", "traderCategoryName":"มัคคุเทศก์เฉพาะ (ทะเลชายฝั่ง)"});
                   	   store.add({"traderCategory":"206", "traderCategoryName":"มัคคุเทศก์เฉพาะ (แหล่งท่องเที่ยวธรรมชาติ)"});
                       store.add({"traderCategory":"207", "traderCategoryName":"มัคคุเทศก์เฉพาะ (วัฒนธรรมท้องถิ่น)"});
                    
                   break;
               }
             }
         }
});
