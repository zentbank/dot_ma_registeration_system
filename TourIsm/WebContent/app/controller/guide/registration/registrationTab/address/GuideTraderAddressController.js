Ext.define('tourism.controller.guide.registration.registrationTab.address.GuideTraderAddressController', {
	extend: 'Ext.app.Controller',
	stores: ['tourism.store.business.registration.address.TraderAddressStore'],
	models: ['tourism.model.business.registration.address.TraderAddressModel'],
	views: [
	        'tourism.view.guide.registration.registrationTab.address.GuideTraderAddressGrid'
	        ,'tourism.view.guide.registration.registrationTab.address.GuideTraderAddressAddEditForm'
	],
	refs: [    
	   	{
	           ref: 'guide_registration_registrationTab_guideaddressgrid',
	           selector: 'guide_registration_registrationTab_guideaddressgrid'
	       },
	       {
	           ref: 'guide_registration_address_guideaddressaddedit_form',
	           selector: 'guide_registration_address_guideaddressaddedit_form'
	       }
	   	],
	init: function(application) {
		this.control({
			'guide_registration_registrationTab_guideaddressgrid button[action=addTraderAddress]':{
				click: this.showTraderAddressAddEditWindow
			},
			'guide_registration_address_guideaddressaddedit_form button[action=saveTraderAddress]':
			{
					click: this.saveTraderAddress
			},
			'guide_registration_registrationTab_guideaddressgrid actioncolumn':
			{
				itemclick: this.handleActionColumn
			}
		});
	},
	showTraderAddressAddEditWindow: function(grid,model){
		var edit = Ext.create('tourism.view.guide.registration.registrationTab.address.GuideTraderAddressAddEditWindow').show();
		var registrationForm = Ext.ComponentQuery.query('form-guide-registration-addedit')[0];
		var fullAddress = registrationForm.getForm().findField('fullAddress').getValue();
		if(fullAddress != ""){
			var form = edit.down('form');
			form.getForm().findField('fullAddress').setValue(fullAddress);
			var arrFullAddress = fullAddress.split(" ");
			//-----[0] เลขที่
			//-----[1] หมู่ที่ เพราะว่ามีเว้นวรรค 1 ที่หลังคำหว่าหมู่ที่ ทำให้ลำดับขยับ
			form.getForm().findField('addressNo').setValue(arrFullAddress[0]);
			if(arrFullAddress[1] != ""){
				//-----[3] ??
				//-----[4] ??
				//-----[5] ถนน
				//-----[6] ตำบล/แขวง
				//-----[7] อำเภอ/เขต
				//-----[8] จังหวัด
				form.getForm().findField('moo').setValue(arrFullAddress[2]);
				if(arrFullAddress[5].search("ถนน") != -1){
					form.getForm().findField('roadName').setValue(arrFullAddress[5].replace("ถนน",""));
				}
				if(arrFullAddress[8].search("จังหวัด") != -1){
					var provinceCombo = form.getForm().findField('provinceId');
					var selectedProvinceId = "";
					provinceCombo.getStore().on('load',function(store, records, successful, eOpts){
						for(var i = 0; i< records.length; i++){
							var province = records[i].raw;
							if(province.provinceName.trim() == arrFullAddress[8].replace("จังหวัด","")){
								selectedProvinceId = province.provinceId;
								provinceCombo.setValue(selectedProvinceId);
							}
						}
	                	
	                },this,{single:true});
	                provinceCombo.getStore().load();

	                var amphurCombo = form.getForm().findField('amphurId');
	                var selectedAmphurId = "";
	                amphurCombo.getStore().on('load',function(store, records, successful, eOpts){
	                	var amphurFiltered = records;
	                	if("" != selectedProvinceId){
	                		amphurFiltered = records.filter(function(el) {
		                		return el.raw.provinceId == selectedProvinceId;
		                	});
	                	}
	                	for(var i = 0; i< amphurFiltered.length; i++){
							var amphur = amphurFiltered[i].raw;
							if(amphur.amphurName.trim() == arrFullAddress[7].replace("อำเภอ","")){
								selectedAmphurId = amphur.amphurId;
								amphurCombo.setValue(selectedAmphurId);
							}
						}
	                	
	                },this,{single:true});
	                amphurCombo.getStore().load();
	
	                var tambolCombo = form.getForm().findField('tambolId');
	                tambolCombo.getStore().on('load',function(store, records, successful, eOpts){
	                	var tambolFiltered = records;
	                	if("" != selectedAmphurId){
	                		tambolFiltered = records.filter(function(el) {
		                		return el.raw.amphurId == selectedAmphurId;
		                	});
	                	}
	                	for(var i = 0; i< tambolFiltered.length; i++){
							var tambol = tambolFiltered[i].raw;
							if(tambol.tambolName.trim() == arrFullAddress[6].replace("ตำบล","")){
								tambolCombo.setValue(tambol.tambolId);
							}
						}
	                },this,{single:true});
	                tambolCombo.getStore().load();
				}else if(arrFullAddress[8].search("กรุงเทพมหานคร") != -1){
					var provinceCombo = form.getForm().findField('provinceId');
					var selectedProvinceId = "";
					provinceCombo.getStore().on('load',function(store, records, successful, eOpts){
	                	for(var i = 0; i< records.length; i++){
							var province = records[i].raw;
							if(province.provinceName.trim() == arrFullAddress[8]){
								selectedProvinceId = province.provinceId;
								provinceCombo.setValue(selectedProvinceId);
							}
						}
	                },this,{single:true});
	                provinceCombo.getStore().load();

	                var amphurCombo = form.getForm().findField('amphurId');
	                var selectedAmphurId = "";
	                amphurCombo.getStore().on('load',function(store, records, successful, eOpts){
	                	var amphurFiltered = records;
	                	if("" != selectedProvinceId){
	                		amphurFiltered = records.filter(function(el) {
		                		return el.raw.provinceId == selectedProvinceId;
		                	});
	                	}
	                	for(var i = 0; i< amphurFiltered.length; i++){
							var amphur = amphurFiltered[i].raw;
							if(amphur.amphurName.trim() == arrFullAddress[7].replace("เขต","")){
								selectedAmphurId = amphur.amphurId;
								amphurCombo.setValue(selectedAmphurId);
							}
						}
	                },this,{single:true});
	                amphurCombo.getStore().load();
	
	                var tambolCombo = form.getForm().findField('tambolId');
	                tambolCombo.getStore().on('load',function(store, records, successful, eOpts){
	                	var tambolFiltered = records;
	                	if("" != selectedAmphurId){
	                		tambolFiltered = records.filter(function(el) {
		                		return el.raw.amphurId == selectedAmphurId;
		                	});
	                	}
	                	for(var i = 0; i< tambolFiltered.length; i++){
							var tambol = tambolFiltered[i].raw;
							if(tambol.tambolName.trim() == arrFullAddress[6].replace("แขวง","")){
								tambolCombo.setValue(tambol.tambolId);
							}
						}
	                },this,{single:true});
	                tambolCombo.getStore().load();
				}
			}else{
				//-----[2] ??
				//-----[3] ??
				//-----[4] ถนน
				//-----[5] ตำบล/แขวง
				//-----[6] อำเภอ/เขต
				//-----[7] จังหวัด
				if(arrFullAddress[4].search("ถนน") != -1){
					form.getForm().findField('roadName').setValue(arrFullAddress[4].replace("ถนน",""));
				}
				if(arrFullAddress[7].search("จังหวัด") != -1){
					var provinceCombo = form.getForm().findField('provinceId');
					var selectedProvinceId = "";
	                provinceCombo.getStore().on('load',function(store, records, successful, eOpts){
	                	for(var i = 0; i< records.length; i++){
							var province = records[i].raw;
							if(province.provinceName.trim() == arrFullAddress[7].replace("จังหวัด","")){
								selectedProvinceId = province.provinceId;
								provinceCombo.setValue(selectedProvinceId);
							}
						}
	                },this,{single:true});
	                provinceCombo.getStore().load();

	                var amphurCombo = form.getForm().findField('amphurId');
	                var selectedAmphurId = "";
	                amphurCombo.getStore().on('load',function(store, records, successful, eOpts){
	                	var amphurFiltered = records;
	                	if("" != selectedProvinceId){
	                		amphurFiltered = records.filter(function(el) {
		                		return el.raw.provinceId == selectedProvinceId;
		                	});
	                	}
	                	for(var i = 0; i< amphurFiltered.length; i++){
							var amphur = amphurFiltered[i].raw;
							if(amphur.amphurName.trim() == arrFullAddress[6].replace("อำเภอ","")){
								selectedAmphurId = amphur.amphurId;
								amphurCombo.setValue(selectedAmphurId);
							}
						}
	                },this,{single:true});
	                amphurCombo.getStore().load();
	
	                var tambolCombo = form.getForm().findField('tambolId');
	                tambolCombo.getStore().on('load',function(store, records, successful, eOpts){
	                	var tambolFiltered = records;
	                	if("" != selectedAmphurId){
	                		tambolFiltered = records.filter(function(el) {
		                		return el.raw.amphurId == selectedAmphurId;
		                	});
	                	}
	                	for(var i = 0; i< tambolFiltered.length; i++){
							var tambol = tambolFiltered[i].raw;
							if(tambol.tambolName.trim() == arrFullAddress[5].replace("ตำบล","")){
								tambolCombo.setValue(tambol.tambolId);
							}
						}
	                },this,{single:true});
	                tambolCombo.getStore().load();
				}else if(arrFullAddress[7].search("กรุงเทพมหานคร") != -1){
					var provinceCombo = form.getForm().findField('provinceId');
					var selectedProvinceId = "";
	                provinceCombo.getStore().on('load',function(store, records, successful, eOpts){
	                	for(var i = 0; i< records.length; i++){
							var province = records[i].raw;
							if(province.provinceName.trim() == arrFullAddress[7]){
								selectedProvinceId = province.provinceId;
								provinceCombo.setValue(selectedProvinceId);
							}
						}
	                },this,{single:true});
	                provinceCombo.getStore().load();
	                
	                var amphurCombo = form.getForm().findField('amphurId');
	                var selectedAmphurId = "";
	                amphurCombo.getStore().on('load',function(store, records, successful, eOpts){
	                	var amphurFiltered = records;
	                	if("" != selectedProvinceId){
	                		amphurFiltered = records.filter(function(el) {
		                		return el.raw.provinceId == selectedProvinceId;
		                	});
	                	}
	                	console.log(amphurFiltered);
	                	for(var i = 0; i< amphurFiltered.length; i++){
							var amphur = amphurFiltered[i].raw;
							if(amphur.amphurName.trim() == arrFullAddress[6].replace("เขต","")){
								selectedAmphurId = amphur.amphurId;
								amphurCombo.setValue(selectedAmphurId);
							}
						}
	                },this,{single:true});
	                amphurCombo.getStore().load();
	
	                var tambolCombo = form.getForm().findField('tambolId');
	                tambolCombo.getStore().on('load',function(store, records, successful, eOpts){
	                	var tambolFiltered = records;
	                	if("" != selectedAmphurId){
	                		tambolFiltered = records.filter(function(el) {
		                		return el.raw.amphurId == selectedAmphurId;
		                	});
	                	}
	                	for(var i = 0; i< records.length; i++){
							var tambol = records[i].raw;
							if(tambol.tambolName.trim() == arrFullAddress[5].replace("แขวง","")){
								tambolCombo.setValue(tambol.tambolId);
							}
						}
	                },this,{single:true});
	                tambolCombo.getStore().load();
				}
			}
		}
			
	},
	saveTraderAddress: function(button){
		var win    = button.up('window');
  		var form   = win.down('form');

      if (!form.isValid()) {

          var me = form,errorCmp, fields, errors;

          fields = me.getForm().getFields();
          errors = '';
          fields.each(function(field) {
              Ext.Array.forEach(field.getErrors(), function(error) {
                  // errors.push({name: field.getFieldLabel(), error: error});
                  errors += field.getFieldLabel()  +'<br>';
              });
          });
          // Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', errors);

          Ext.Msg.show({
             title:'กรุณาตรวจสอบข้อมูล',
             msg: errors,
             buttons: Ext.Msg.OK,
             icon: Ext.Msg.ERROR
        });
          return;
      }

  		var model = form.getRecord(); //Ext.data.Model
  		var values = form.getValues();//object {xx: q,ss:ff}

      var provinceName = Ext.getCmp('guide-registration-address-masprovince-combo').getRawValue();
      var amphurName = Ext.getCmp('guide-registration-address-masamphur-combo').getRawValue();
      var tambolName = Ext.getCmp('guide-registration-address-tambol-combo').getRawValue();

      values.provinceName = provinceName;
      values.amphurName = amphurName;
      values.tambolName = tambolName;
      //console.log(values);
    //  Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', values);
  		var grid = Ext.ComponentQuery.query('guide_registration_registrationTab_guideaddressgrid')[0];        
  		if (!Ext.isEmpty(model)){
        //edit address
  			model.set(values);
  			
  		} else{
        //add address
  			
  			model = Ext.create('tourism.model.business.registration.address.TraderAddressModel');
  			model.set(values);
  			// model.setId(0);

        //set regId and traderId
        var registrationForm = Ext.ComponentQuery.query('form-guide-registration-addedit')[0];
        var regId = registrationForm.getForm().findField('regId').getValue();
        var traderId = registrationForm.getForm().findField('traderId').getValue();

        model.set('regId', regId);
        model.set('traderId', traderId);
  			grid.getStore().insert(0, model);

        if(model.get('copyAddress'))
        {
          modelCopy = Ext.create('tourism.model.business.registration.address.TraderAddressModel');
          modelCopy.set(values);

          if(values.addressType == "O")
          {
        	  modelCopy.set("addressType", "S");
          }
          else
          {
        	  modelCopy.set("addressType", "O");
          }

          modelCopy.set('regId', regId);
          modelCopy.set('traderId', traderId);
          
          grid.getStore().insert(0, modelCopy);
        }
  		}


  		win.close();
  		grid.getStore().sync();
	},
	handleActionColumn : function(column, action, grid, rowIndex, colIndex, model, node) {
        
        if(action){
            switch(action){
                case 'edittraderaddress': 

                    var edit = Ext.create('tourism.view.guide.registration.registrationTab.address.GuideTraderAddressAddEditWindow');
                    edit.show();

                    if(model && (model instanceof tourism.model.business.registration.address.TraderAddressModel)){
                        
                        var fAddress = edit.down('form');
                        var provinceCombo = fAddress.getForm().findField('provinceId');
                        provinceCombo.getStore().on('load',function(store, records, successful, eOpts){
                          provinceCombo.setValue(model.get('provinceId'));
                        },this,{single:true});
                        provinceCombo.getStore().load();

                        var amphurCombo = fAddress.getForm().findField('amphurId');
                        amphurCombo.getStore().on('load',function(store, records, successful, eOpts){
                          amphurCombo.setValue(model.get('amphurId'));
                        },this,{single:true});
                        amphurCombo.getStore().load();

                        var tambolCombo = fAddress.getForm().findField('tambolId');
                        tambolCombo.getStore().on('load',function(store, records, successful, eOpts){
                          tambolCombo.setValue(model.get('tambolId'));
                        },this,{single:true});
                        tambolCombo.getStore().load();

                        edit.down('form').loadRecord(model);
                        edit.down('form').getForm().findField('copyAddress').hide();
                    }

                    
                break;

                case 'deletetraderaddress': 
                      var store = grid.getStore();
                      store.remove(model);
                      store.sync();
                break;
            }
        }
    
    }
});