Ext.define('tourism.controller.guide.registration.registrationTab.guideTrain.GuideTrainController',{
	extend: 'Ext.app.Controller',
	
	stores: ['tourism.store.grid.EducationStore'],
	
	//models: ['tourism.model.grid.PersonTrainedModel'],
	
	views: ['tourism.view.guide.registration.registrationTab.guideTrain.GuideTrainGrid'],
	
	refs:[
	      {
		 	ref: 'guide_registration_registrationTab_guidetraingrid',
            selector: 'guide_registration_registrationTab_guidetraingrid'
	      },
	      {
	    	ref: 'guide_registration_registrationtab_guidetrain_edit_form',
	    	selector:'panel'
	      }
	      
	      ],
	
	init: function(application){
		this.control({
			'guide_registration_registrationTab_guidetraingrid button[action=addGuideTrain]': {
            	click: this.showGuideTrainWindow
            },
            'guide_registration_registrationTab_guidetraingrid actioncolumn':{
            	itemclick: this.handleActionColumn
            },
            'guide_registration_registrationtab_guidetrain_edit_form button[action=searchGuideTrain]':{
            	click: this.searchGuideTrain
            },
            'guide_registration_registrationtab_guidetrain_edit_form button[action=saveGuideTrain]':{
            	click: this.saveGuideTrain
            }
		});
	},
	
	showGuideTrainWindow: function(grid,model){
		var edit = Ext.create('tourism.view.guide.registration.registrationTab.guideTrain.GuideTrainAddEditWindow');
		edit.show();
	},
	searchGuideTrain: function(button){
		var win  = button.up('window');
		var form = win.down('form');
		
		if (!form.isValid()) {
			var me = form,errorCmp, fields, errors;

	          fields = me.getForm().getFields();
	          errors = '';
	          fields.each(function(field) {
	              Ext.Array.forEach(field.getErrors(), function(error) {
	                  errors += field.getFieldLabel()  +'<br>';
	              });
	          });

	          Ext.Msg.show({
	             title:'กรุณาตรวจสอบข้อมูล',
	             msg: errors,
	             buttons: Ext.Msg.OK,
	             icon: Ext.Msg.ERROR
	        });
	          return;
		}
		
		var grid = Ext.ComponentQuery.query('guide_registration_guideaddtrainsearch_grid')[0]; 
		var store = grid.getStore();
		
		var values = form.getValues();
		var dataParams = this.normalizeData(values);	
		console.log(values);
		console.log(dataParams);
		
		store.load({
			scope: this,
			params: dataParams,
		    callback: function(records, operation, success) {
		        // the operation object
		        // contains all of the details of the load operation
		        console.log(records);
//		        //console.log(operation);
		        console.log(success);
		    }
		});
	},
	saveGuideTrain: function(button){

		var grid = Ext.ComponentQuery.query('guide_registration_guideaddtrainsearch_grid')[0];
		var model = grid.getSelectionModel().getSelection(); 
  	
		if(model.length == 1)
		{
			var win = button.up('window');
			//var form   = win.down('form');
			win.close();
	
			//var form = Ext.ComponentQuery.query('guide_registration_registrationTab_guidetrainaddedit')[0];
			var form = Ext.ComponentQuery.query('form-guide-registration-addedit')[0];
			var gridShow = Ext.ComponentQuery.query('guide_registration_registrationTab_guidetraingrid')[0];
			var modelShow = Ext.create('tourism.model.grid.EducationModel');
			
			//var eduId =  form.getForm().findField('eduId').setValue(model[0].get('eduId'));
		    var personId = form.getForm().findField('personId').getValue();
		    var educationType = 'T';
		     
		    modelShow.set('personId',personId);
		    modelShow.set('educationType',educationType);
		    modelShow.set('studyDate',model[0].get('studyDate'));
		    modelShow.set('graduationDate',model[0].get('graduationDate'));
		    modelShow.set('graduationYear',model[0].get('graduationYear'));
		    modelShow.set('masUniversityId',model[0].get('masUniversityId'));
		    modelShow.set('universityName',model[0].get('universityName'));
		    modelShow.set('graduationCourse',model[0].get('graduationCourse'));
		    modelShow.set('generationGraduate',model[0].get('generationGraduate'));
			
			
			
			gridShow.getStore().insert(0, modelShow);
			gridShow.getStore().sync();
			
		}else{
			
			errors = 'กรุณาเลือกข้อมูล';
			
			Ext.Msg.show({
	             title:'กรุณาตรวจสอบข้อมูล',
	             msg: errors,
	             buttons: Ext.Msg.OK,
	             icon: Ext.Msg.ERROR
	        });
		}
	},
	 handleActionColumn : function(column, action, grid, rowIndex, colIndex, model, node) {
	        
	        if(action){
	          var store = grid.getStore();   
	          store.remove(model);
	          store.sync();
	        }
	    
	    }
	    
	    //Oat add 13/10/57
	    ,normalizeData: function(dataObj)
	    {
		      // remove null data;
		     for (field in dataObj) 
		      {
		        if (Ext.isEmpty(dataObj[field])) 
		        {
		          delete dataObj[field];
		        }
		        
		      }
		
		      return dataObj;
	    }
	    //

	    
});