Ext.define('tourism.controller.guide.registration.registrationType.GuideRegistrationTypeController',{
	
	extend: 'Ext.app.Controller',
	
	stores: ['tourism.store.guide.registration.GuideRegistrationStore'],
	
	models: ['tourism.model.guide.registration.GuideRegistrationModel'], 
	
	views:
		[
		 'tourism.view.guide.registration.registrationType.GuideRegistrationTypeAddEditForm'
		// ,'tourism.view.guide.registration.registrationType.GuideRegistrationTypeSearchBusinessForm'
		,'tourism.view.business.registration.registrationType.RegistrationTypeSearchBusinessForm'
	],
	
	
	refs: [
	       {
	    	   ref: 'guide-registration-registrationType-addeditform',
	    	   selector: 'panel'
	       },
	       {
	    	   ref: 'guide-registration-registrationtype-searchbusinessform',
	    	   selector: 'panel'
	       },
	       {
	       		ref: '#guide-registration-registrationtype-searchGuideform',
	       		selector: 'panel'
	       }
	],
	
	init : function(application)
	{
		this.control(
				{
					'guide-registration-registrationType-addeditform button[action=selectRegType]':{
						click : this.selectRegistrationType
				},
				'guide-registration-registrationtype-searchbusinessform button[action=saveSearchType]': {
		              click: this.saveSearchType
		        },
		        '#guide-registration-registrationtype-searchGuideform button[action=searchLicense]': {
		              click: this.searchLicense
		         }
		         ,
	             '#guide-registration-registrationtype-searchGuideform button[action=selectedLicenseNo]': {
	              click: this.selectedLicenseNo
	            }
			
				});
		
	},

	selectRegistrationType: function(button)
	{
		var win = button.up('window'),
		form = win.down('form'),
		model = form.getRecord(), //Ext.data.Model
		values = form.getValues();//object {xx: q,ss:ff}
		
		//Sek Fix
		values.traderType = 'G';
		//
		
		if(values.registrationType != 'N')
		{
			var layout = win.getLayout();
	        layout.setActiveItem(1);
	        
	        var biModel = Ext.create('tourism.model.guide.registration.GuideRegistrationModel');
	        biModel.set('registrationType',values.registrationType);


	        if(values.registrationType == 'R')
	        {
	          biModel.set('registrationTypeName', '<h1>ต่ออายุใบอนุญาต</h1>');
	        }
	        
	        else if(values.registrationType == 'C')
	        {
	          biModel.set('registrationTypeName', '<h1>เปลี่ยนแปลงรายการ</h1>');
	        }
	        else if(values.registrationType == 'T')
	        {
	          biModel.set('registrationTypeName', '<h1>ออกใบแทน</h1>');
	        }

	        var form = win.down('#guide-registration-registrationtype-searchGuideform');
	        form.loadRecord(biModel);
		}else 
		{
			var fsubmit = form.getForm();
		    var ctrl = this; // scope
		     
		    //load person data
		    fsubmit.load({
	            url: 'guide/registration/createregistration',
	            method : 'POST',
	            waitMsg: 'กรุณารอสักครู่...',
	            params: {
	                newStatus: 'test'
	            },
	            params: values,
	            success: function(form, action) {
	
	              var dataObj = action.result.data;
	
	              var regAddEditWindow = ctrl.createGuideRegistrationAddEditWindow(dataObj);

	              var formGuideRegis = regAddEditWindow.down("form-guide-registration-addedit");
	              
	              regAddEditWindow.on('show', function(){
	                
	                win.close();

	                var personNationalityField =  formGuideRegis.getForm().findField('personNationality');
	        		personNationalityField.setValue("ไทย");

	        		// //console.log(personNationalityField);
	
	              },regAddEditWindow,{single: true});
	              
	              regAddEditWindow.show();
	            },
	            failure: function(form, action) {
	              Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
	            },
	            scope: fsubmit // this = fsubmit
	        });


		}
	},
	saveSearchType: function(button) {
	    var win    = button.up('window');
	    var form = Ext.ComponentQuery.query('guide-registration-registrationtype-searchbusinessform')[0];  


	   
	    model = form.getRecord(); //Ext.data.Model
	    values = form.getValues();//object {xx: q,ss:ff}

	  //  //console.log(form);
	  //  //console.log(values);


	    // open BusinessRegistrationAddEdit Window

	     win.close();
	    
	  },
	  createGuideRegistrationAddEditWindow: function(values){

	    var model =  Ext.create('tourism.model.guide.registration.GuideRegistrationModel');
	    
	    model.set(values);

	    var regAddEditWindow = Ext.create('tourism.view.guide.registration.GuideRegistrationAddEditWindow',{
            registrationModel: model,
            roleAction: 'key',
            traderType: 'guide'
	    });
	    return regAddEditWindow;
	  }
,
  searchLicense: function(btn)
  {
    var form = btn.up('form');
    var grid =  form.down('registrationtype-grid');

    var licenseNo = form.getForm().findField('licenseNo').getValue();

    if(Ext.isEmpty(licenseNo))
    {
          Ext.Msg.show({
             title:'เกิดข้อผิดพลาด',
             msg: 'กรุณากรอกข้อมูลเลขที่ใบอนุยาต',
             buttons: Ext.Msg.OK,
             icon: Ext.Msg.ERROR
          });
      return;
    }

   grid.getStore().load({
        params: { traderType: 'G', licenseNo: licenseNo},
        callback: function(records, operation, success) {
            
        },
        scope: this
    }); 

  },
  selectedLicenseNo: function(btn)
  {
    var win    = btn.up('window');

    var animateTarget = win.animateTarget;

    var form1 = btn.up('form');
    var grid =  form1.down('registrationtype-grid');
    var selection = grid.getSelectionModel().getSelection();

    if(selection.length < 1)
    {
        Ext.Msg.show({
           title:'เกิดข้อผิดพลาด',
           msg: 'กรุณาเลือกข้อมูลใบอนุยาต',
           buttons: Ext.Msg.OK,
           icon: Ext.Msg.ERROR
        });

        return;
    }

    var model = selection[0];

    
    var registrationType = form1.getForm().findField('registrationType').getValue();
    var myMask = new Ext.LoadMask(form1, {msg:"กรุณารอสักครู่..."});
    myMask.show();
    form1.load({
      url: 'business/registration/addnewregistration',
      method : 'POST',
      waitMsg: 'กรุณารอสักครู่...',
      params: {traderId: model.get('traderId') ,registrationType: registrationType},
      success: function(form, action) {
        myMask.hide();

		var edit = Ext.create('tourism.view.guide.registration.GuideRegistrationAddEditWindow',{
			animateTarget: animateTarget,
			roleAction: 'key',
            actionMode: 'edit',
            traderType: 'guide',
            verificationGrid: grid
		});
		edit.show();


        var model =  Ext.create('tourism.model.business.registration.BusinessRegistrationModel');

        model.set(action.result.data);

        edit.loadDataModel(model);

        win.close();

      },
      failure: function(form, action) {
        myMask.hide();
        Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
      },
      scope: this // this = fsubmit
    });


  }
});