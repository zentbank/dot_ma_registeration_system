Ext.define('tourism.controller.guidedetail.GuidedetailController',{
	extend: 'Ext.app.Controller',
	
//	stores: ['tourism.store.guidedetail.grid.GuideForeignLanguageStore'],
//	
//	models: ['tourism.model.guidedetail.GuideDetailModel'],
	
	views: ['tourism.view.guidedetail.GuideDetailGrid'
	        ,'tourism.view.guidedetail.GuideDetailFormSearch'
	        ,'tourism.view.guidedetail.GuideDetailAddEditWindow'
	        ,'tourism.view.window.verification.VerificationForm'
	        ],
	
	refs:[
	      {
	    	  ref: 'guidedetail_grid',
	    	  selector: 'guidedetail_grid'
	      },
	      {
	    	  ref: 'form-guidedetail-addedit',
	    	  selector: 'panel'
	      }
	      ,{
	    	  ref: 'guidedetail_formsearch',
	    	  selector: 'panel'
	      }
        
	      ],
	
	init: function(application){
		this.control({
			'guidedetail_grid button[action=add]': {
            	click: this.showGuideDetailWindow
            },
		    'form-guidedetail-addedit button[action=saveGuideDetail]':{
                click: this.saveGuideDetail
            },
		    'guidedetail_formsearch button[action=searchGuideDetail]':{
                click: this.searchGuideDetail
            },
            'guidedetail_grid actioncolumn': {
            	itemclick: this.handleActionColumn
            },
            'form-guidedetail-addedit button[action=searchLicenseGuideDetail]':{
                click: this.searchLicenseGuideDetail
            }
		});

	}

		,searchLicenseGuideDetail: function(btn,model){

			var form = btn.up('#key-form-guidedetail-addedit');
			
			model = form.getRecord(); //Ext.data.Model
			var valuesAll = form.getValues();//object {xx: q,ss:ff}
			
//			console.log(valuesAll);
			
			var guideId = valuesAll.guideId;
			var licenseNo = valuesAll.licenseNo;
			var values = {};
			values.guideId = guideId;
			values.licenseNo = licenseNo;
			
//			console.log(guideId);
//			console.log(licenseNo);
//			console.log(values);
			
			form.getForm().reset();

				var fsubmit = form.getForm();
			    var ctrl = this; // scope
			     
			    //load guidedetail data 
			    fsubmit.load({
			    	url: 'guide/guidedetail/searchlicenseguidedetail',
		            method : 'POST',
		            waitMsg: 'กรุณารอสักครู่...',
		            params: values,
		            success: function(form2, action) {
		            	
		              var dataObj = action.result.data;
		              
		              console.log(dataObj);
		              console.log(dataObj.prefixId);
		              
		            //Prefix
                      if(!Ext.isEmpty(dataObj.prefixId))
                      {
                        var prefixIdCombo = form.getForm().findField('prefixId');
                        var prefixIdEnCombo = form.getForm().findField('prefixIdEn');
                        prefixIdCombo.getStore().on('load',function(store, records, successful, eOpts){
                          prefixIdCombo.setValue(dataObj.prefixId);
                          prefixIdEnCombo.setValue(dataObj.prefixId);
                        },this,{single:true});
                        prefixIdCombo.getStore().load();
                      }
//                      else
//                      {
//                        var prefixIdCombo = fguide.getForm().findField('prefixId');
//                        var prefixIdEnCombo = fguide.getForm().findField('prefixIdEn');
//                        prefixIdCombo.clearValue();
//                        prefixIdEnCombo.clearValue();
//                      } 
		              
		            //Province
                      if(!Ext.isEmpty(dataObj.provinceId))
                      {
                        var provinceCombo = form.getForm().findField('provinceId');
                        //console.log(provinceCombo);
                        provinceCombo.getStore().on('load',function(store, records, successful, eOpts){
                          provinceCombo.setValue(dataObj.provinceId);
                        },this,{single:true});
                        provinceCombo.getStore().load();
                      }
//                      else
//                      {
//                        var provinceCombo = form.getForm().findField('provinceId');
//                        provinceCombo.clearValue();
//                      }
                      
                    //Amphur
                      if(!Ext.isEmpty(dataObj.amphurId))
                      {
                        var amphurCombo = form.getForm().findField('amphurId');
                        amphurCombo.getStore().on('load',function(store, records, successful, eOpts){
                          amphurCombo.setValue(dataObj.amphurId);
                        },this,{single:true});
                        amphurCombo.getStore().load();
                      }
//                      else
//                      {
//                         var amphurCombo = form.getForm().findField('amphurId');
//                         amphurCombo.clearValue();
//                      }
                      
                    //TambolId
                      if(!Ext.isEmpty(dataObj.tambolId))
                      {
                        var tambolCombo = form.getForm().findField('tambolId');
                        tambolCombo.getStore().on('load',function(store, records, successful, eOpts){
                          tambolCombo.setValue(dataObj.tambolId);
                        },this,{single:true});
                        tambolCombo.getStore().load();
                      }
//                      else
//                      {
//                         var tambolCombo = form.getForm().findField('tambolId');
//                         tambolCombo.clearValue();
//                      }
		              
//			              var guideDetailAddEditWindow = ctrl.createGuideDetailAddEditWindow(dataObj);
//			              
//			              guideDetailAddEditWindow.on('show', function(){
//			               	
//			              },guideDetailAddEditWindow,{single: true});
//			              
//			              guideDetailAddEditWindow.show();
		            },
		            failure: function(form, action) {
		              Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
		            },
		            scope: fsubmit // this = fsubmit
		        });
				
			
		}

	  ,handleActionColumn: function(column, action, grid, rowIndex, colIndex, model, node){
		  if(action)
		  {
			switch(action)
			{
			  case 'editguidedetail' :
					var edit = Ext.create('tourism.view.guidedetail.GuideDetailAddEditWindow',{
					   animateTarget: column.el,
			           actionMode: 'edit',
			           roleAction: 'key',
			           traderType: 'guide',
			           verificationGrid: grid
					});
					edit.show();
					
					if(model && (model instanceof tourism.model.guidedetail.GuideDetailModel))
					{
						console.log(model);
						var fguide = edit.down('form');
						fguide.loadRecord(model);
						
						//Prefix
                        if(!Ext.isEmpty(model.get('prefixId')))
                        {
                          var prefixIdCombo = fguide.getForm().findField('prefixId');
                          var prefixIdEnCombo = fguide.getForm().findField('prefixIdEn');
                          prefixIdCombo.getStore().on('load',function(store, records, successful, eOpts){
                            prefixIdCombo.setValue(model.get('prefixId'));
                            prefixIdEnCombo.setValue(model.get('prefixId'));
                          },this,{single:true});
                          prefixIdCombo.getStore().load();
                        }
                        else
                        {
                          var prefixIdCombo = fguide.getForm().findField('prefixId');
                          var prefixIdEnCombo = fguide.getForm().findField('prefixIdEn');
                          prefixIdCombo.clearValue();
                          prefixIdEnCombo.clearValue();
                        }
                        
                        var guideId = fguide.getForm().findField('guideId');
                        
                      //Province
                        if(!Ext.isEmpty(model.get('provinceId')))
                        {
                          var provinceCombo = fguide.getForm().findField('provinceId');
                          //console.log(provinceCombo);
                          provinceCombo.getStore().on('load',function(store, records, successful, eOpts){
                            provinceCombo.setValue(model.get('provinceId'));
                          },this,{single:true});
                          provinceCombo.getStore().load();
                        }
                        else
                        {
                          var provinceCombo = fguide.getForm().findField('provinceId');
                          provinceCombo.clearValue();
                        }
                        
                      //Amphur
                        if(!Ext.isEmpty(model.get('amphurId')))
                        {
                          var amphurCombo = fguide.getForm().findField('amphurId');
                          amphurCombo.getStore().on('load',function(store, records, successful, eOpts){
                            amphurCombo.setValue(model.get('amphurId'));
                          },this,{single:true});
                          amphurCombo.getStore().load();
                        }
                        else
                        {
                           var amphurCombo = fguide.getForm().findField('amphurId');
                           amphurCombo.clearValue();
                        }
                        
                      //TambolId
                        if(!Ext.isEmpty(model.get('tambolId')))
                        {
                          var tambolCombo = fguide.getForm().findField('tambolId');
                          tambolCombo.getStore().on('load',function(store, records, successful, eOpts){
                            tambolCombo.setValue(model.get('tambolId'));
                          },this,{single:true});
                          tambolCombo.getStore().load();
                        }
                        else
                        {
                           var tambolCombo = fguide.getForm().findField('tambolId');
                           tambolCombo.clearValue();
                        }
                        
                        //ภาษา
                    	var languagegrid = Ext.ComponentQuery.query('window_guidedetail_language_grid')[0];
                    	languagegrid.getStore().load({
                            params: {guideId: model.get('guideId')},
                            callback: function(records, operation, success) {
                            	
                            	console.log(records);                            
                            },
                            scope: this
                        });
                    	
					}
			  break;	
			 
			  case 'removeguidedetail' :	
				  
				  Ext.Msg.confirm('ลบ', 'คุณต้องการลบข้อมูลแบบรายงานตัวมัคคุเทศก์ใช่หรือไม่', function(btn, text){

					  console.log(btn);

	                  if (btn == 'yes')
	                  {
	                	  var panel = grid.up('#guidedetail-guide-panel');
	    				  var form = panel.down('#gridSubmit');
	    				  var fsubmit = form.getForm();
	    				  
	    				  fsubmit.load({
	    				    	url: 'guide/guidedetail/removeguidedetail',
	    			            method : 'POST',
	    			            waitMsg: 'กรุณารอสักครู่...',
	    			            params: {guideId: model.get('guideId')},
	    			            success: function(form, action) {
	    			            	
	    			              var dataObj = action.result.data;
	    			              console.log(dataObj);
	    			              
	    			              var noti = Ext.create('widget.uxNotification', {
	    			                  // title: 'Notification',
	    			                  position: 'tr',
	    			                  manager: 'instructions',
	    			                  // cls: 'ux-notification-light',
	    			                  // iconCls: 'ux-notification-icon-information',
	    			                  html: '<b>ลบข้อมูลเรียบร้อยแล้ว</b>',
	    			                  closable: false,
	    			                  autoCloseDelay: 4000,
	    			                  width: 300,
	    			                  slideBackDuration: 500,
	    			                  slideInAnimation: 'bounceOut',
	    			                  slideBackAnimation: 'easeIn'
	    			                });
	    			                noti.show();
	    			                
	    			                var store = grid.getStore();
	    			                store.reload();
	    	
	    			            },
	    			            failure: function(form, action) {
	    			              Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
	    			            },
	    			            scope: fsubmit // this = fsubmit
	    			        });
	                  }
				  });
				  
			  break;	
			}
		  }
	  }
	
	,searchGuideDetail: function(btn)
	  {
		    var form = btn.up('form');
			var values = form.getValues();
			console.log(values);
			
			var dataParams = this.normalizeData(values);	
			console.log(dataParams);
					
			var grid = Ext.ComponentQuery.query('guidedetail_grid')[0];
			var store = grid.getStore();
			
			dataParams.page = '1';
		    dataParams.start = '0';
		    dataParams.limit = store.pageSize;
			
			store.load({
				params:  dataParams,
				callback: function(records, operation, success) {
		          console.log(records);
		        },
		        scope: this
			});

	  },
	
	showGuideDetailWindow: function(btn)
	{
		var panel = btn.up('#guidedetail-guide-panel');
		var form = panel.down('#formSubmit');
		
		model = form.getRecord(); //Ext.data.Model
		values = form.getValues();//object {xx: q,ss:ff}
		
//		console.log(values);
//		console.log(form);
			var fsubmit = form.getForm();
		    var ctrl = this; // scope
		     
		    //load guidedetail data
		    fsubmit.load({
//	            url: 'guide/registration/createregistration',
		    	url: 'guide/guidedetail/createguidedetail',
	            method : 'POST',
	            waitMsg: 'กรุณารอสักครู่...',
	            params: values,
	            success: function(form, action) {
	            	
	              var dataObj = action.result.data;
	              console.log(dataObj);
	              
	              var guideDetailAddEditWindow = ctrl.createGuideDetailAddEditWindow(dataObj);

//	              var formGuideRegis = guideDetailAddEditWindow.down("form-guide-registration-addedit");
	              
	              guideDetailAddEditWindow.on('show', function(){
	               	
	              },guideDetailAddEditWindow,{single: true});
	              
	              guideDetailAddEditWindow.show();
	            },
	            failure: function(form, action) {
	              Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
	            },
	            scope: fsubmit // this = fsubmit
	        });

//		    var dataObj = {};
//		    var guideDetailAddEditWindow = ctrl.createGuideDetailAddEditWindow(dataObj);
//		    guideDetailAddEditWindow.show();
		
	},
	  createGuideDetailAddEditWindow: function(values){
	
	    var model =  Ext.create('tourism.model.guidedetail.GuideDetailModel');
	    
	    model.set(values);
	
	    var guideDetailAddEditWindow = Ext.create('tourism.view.guidedetail.GuideDetailAddEditWindow',{
	      id: 'key-window-guidedetail-addedit',	
	      guidedetailModel: model,
	      roleAction: 'key',
	      traderType: 'guide'
	    });
	    return guideDetailAddEditWindow;
	  },
	  
	saveGuideDetail: function(btn,model){
		console.log('saveGuideDetail');
		var win = btn.up('window');
		var form = win.down('form');
		
		var model = form.getRecord(); //Ext.data.Model
	    var values = form.getValues();//object {xx: q,ss:ff}
   
	    console.log(values);
	    
	      if (!form.isValid()) {

	          var me = form,errorCmp, fields, errors;

	          fields = me.getForm().getFields();
	          errors = '';
	          fields.each(function(field) {
	              Ext.Array.forEach(field.getErrors(), function(error) {
                  // errors.push({name: field.getFieldLabel(), error: error});
	                  errors += field.getFieldLabel()  +'<br>';
	              });
	          });
	          // Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', errors);

	          Ext.Msg.show({
	             title:'กรุณาตรวจสอบข้อมูล',
	             msg: errors,
	             buttons: Ext.Msg.OK,
	             icon: Ext.Msg.ERROR
	          });
	          return;
	      }
	      
	      var fsubmit = form.getForm();	     	      
//	      var dataParams = this.normalizeData(values);	  
	      var dataParams = values;
	      console.log(dataParams);
	      
        fsubmit.load({
            url: 'guide/guidedetail/saveAllGuideDetail',
            method : 'POST',
            params: dataParams,
            waitMsg: 'กรุณารอสักครู่...',
            success: function(form, action) {
              var dataObj = action.result.data;

            var noti = Ext.create('widget.uxNotification', {
              // title: 'Notification',
              position: 'tr',
              manager: 'instructions',
              // cls: 'ux-notification-light',
              // iconCls: 'ux-notification-icon-information',
              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
              closable: false,
              autoCloseDelay: 4000,
              width: 300,
              slideBackDuration: 500,
              slideInAnimation: 'bounceOut',
              slideBackAnimation: 'easeIn'
            });
            noti.show();

              var grid = Ext.ComponentQuery.query('guidedetail_grid')[0];
              var store = grid.getStore();

              if(win.actionMode == 'edit')
              {
                store.reload();
              }else
              {
                store.load({
                    params: {},
                    callback: function(records, operation, success) {
                         ////console.log(records);
                    },
                    scope: this
                }); 
              }

              win.close();
        
            },
            failure: function(form, action) {
              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
            },
            scope: this 
        });     
	},
	
	normalizeData: function(dataObj)
    {
       // remove null data;
       for (field in dataObj) 
       {
          if (Ext.isEmpty(dataObj[field])) 
          {
            delete dataObj[field];
          }
       }
       return dataObj;
    }
	
});
		
		
		