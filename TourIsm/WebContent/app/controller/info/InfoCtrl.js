Ext.define('tourism.controller.info.InfoCtrl', {
	extend : 'Ext.app.Controller',

   	stores: [
   
   	],

    models: [
    
    ],

    views: [
           // 'tourism.view.deactivate.AddDeactivateForm',
           // 'tourism.view.refundguarantee.RefundGuaranteeAddEditForm'
    ]

	,refs: [
		
		{  
		    ref: '#license-info-formsearch-business-info',
		    selector: 'panel'
		} 
		,{  
		    ref: '#license-info-formsearch-guide-info',
		    selector: 'panel'
		} 
		,{  
		    ref: '#license-info-formsearch-tourleader-onfo',
		    selector: 'panel'
		}
		
	]

	,init : function (application) {

        this.control({
        
	        '#license-info-formsearch-business-info button[action=searchBusinessInfo]': {
                click: this.searchBusinessInfo
            }
	        ,'#license-info-formsearch-guide-info button[action=searchGuideTourleaderLicense]': {
	            click: this.searchGuideInfo
	        }
	        ,'#license-info-formsearch-tourleader-info button[action=searchGuideTourleaderLicense]': {
	            click: this.searchTourleaderInfo
	        }
	        ,'#info-business-license-info-touris-guide-tourleader-grid actioncolumn': {
                itemclick: this.handleActionColumn
            }
	        ,'#info-guide-license-info-touris-guide-tourleader-grid actioncolumn': {
                itemclick: this.handleActionColumn
            }
	        ,'#info-tourleader-license-info-touris-guide-tourleader-grid actioncolumn': {
                itemclick: this.handleActionColumn
            }
	        
	        ,'#license-info-duedate-formsearch-business-fee button[action=searchBusinessInfoExpireDate]': {
                click: this.searchBusinessInfoExpireDate
            }
	        ,'#license-info-duedate-formsearch-business-fee button[action=printReportBusinessInfoExpireDate]':{
     	  		click: this.printReportBusinessInfoExpireDate
            }
	        
          
        });
        
	}

	,handleActionColumn : function(column, action, grid, rowIndex, colIndex, model, node) {
	
		switch(action)
		{
			//Oat Add 24/09/57
			case 'printLicenseInfo':
				if(!Ext.isEmpty(model.get('traderType'))){
			        switch(model.get('traderType')){
			        	
			        	case 'B':
			        		
			        		var traderId = model.get('traderId');
			        		var regId =  model.get('regId');
			        		var personId =  model.get('personId');
			        		var personType =  model.get('personType');
			        		
			        		var reqUrl = "business/info/reportPrintLicenseInfoDocx?"
			            		 +"traderId="+traderId
		        			 	 +"&regId="+regId
		        			 	 +"&personId="+personId
		        			 	 +"&personType="+personType;
			        		
			        		window.open(reqUrl,"พิมพ์รายงาน","toolbar=0 width=900 height=600");
		                
//		                  var winInfo = Ext.create('tourism.view.information.business.InfoBusinessWindow',{
//		                    id: 'business-information-business-registration-infowindow',
//		                    animateTarget: column.el,
//		                    actionMode: 'information',
//		                    roleAction: 'info',
//		                    registrationType: model.get('registrationType')
//		                  });
//		                  
//		                  winInfo.show();
//	
//		                  winInfo.loadFormRecord(model);
//	
//		                  var formRefundguarantee = winInfo.down('license-info-refundguarantee-form');
//		                  formRefundguarantee.loadRecord(model);
		                break;
		                
			        	case 'G':
			                
//			        		var winInfo = Ext.create('tourism.view.information.guide.InfoGuideWindow',{
//		                		id: 'guide-information-guide-registration-infowindow',
//		                		animateTarget: column.el,
//		                        actionMode: 'information',
//		                        roleAction: 'info'
//		                    	,registrationType: model.get('registrationType')
//		                	}).show();
//		                	
//		                	winInfo.loadFormRecord(model);
			            break;
			                
			        
			        }
				}
			break;
			//End Oat Add 24/09/57
			
			case 'editlicense':
				if(!Ext.isEmpty(model.get('traderType'))){
			        switch(model.get('traderType')){
			        
			        	case 'B':
		                
		                  var winInfo = Ext.create('tourism.view.information.business.InfoBusinessWindow',{
		                    id: 'business-information-business-registration-infowindow',
		                    animateTarget: column.el,
		                    actionMode: 'information',
		                    roleAction: 'info',
		                    registrationType: model.get('registrationType')
		                  });
		                  
		                  winInfo.show();

		                  winInfo.loadFormRecord(model);

		                  var formRefundguarantee = winInfo.down('license-info-refundguarantee-form');
		                  formRefundguarantee.loadRecord(model);
		                break;
		                
			        	case 'G':
			                
			        		var winInfo = Ext.create('tourism.view.information.guide.InfoGuideWindow',{
		                		id: 'guide-information-guide-registration-infowindow',
		                		animateTarget: column.el,
		                        actionMode: 'information',
		                        roleAction: 'info'
		                    	,registrationType: model.get('registrationType')
		//                      roleAction: 'supervisor',
		                	}).show();
		                	
		                	winInfo.loadFormRecord(model);
			            break;
			                
			        	case 'L':
			                
			        		var winInfo = Ext.create('tourism.view.information.tourleader.InfoTourleaderWindow',{
		                		id: 'tourleader-information-tourleader-registration-infowindow',
		                		animateTarget: column.el,
		                        actionMode: 'information',
		                        roleAction: 'info'
		                    	,registrationType: model.get('registrationType')
		//                      roleAction: 'supervisor',
		                	}).show();
		                	
		               

		                	winInfo.loadFormRecord(model);
			            break;
			                
			        
			        }
				}
			break;
			case 'cancelLicense':
				
				

				if(!Ext.isEmpty(model.get('traderType'))){
			        switch(model.get('traderType')){
			        
			        	case 'B':
		                
		                  var winInfo = Ext.create('Ext.window.Window', {
							id: 'cancel-information-business-registration-infowindow',
							model: model,
							width: 750,
            				height: 550,
            				bodyPadding: 5,
            				autoShow: true,
				            autoScroll: true,
				            modal: true,
							items: [
								{
									xtype: 'tabpanel',
				                    plain: true,
				                    frame: false,
				                    width: '100%',
            						height: '100%',
				                    border: false,
				                    items: [
				                    	{
				                            title: 'ยกเลิกใบอนุญาต',
				                            xtype: 'license-info-deactivate-form',
						                    id: 'cancel-information-deactivate-business-form',
						                    border: false,
						                    layout: 'fit',
						                    frame: false,
						                    traderType: 'B',
											traderTypeName: 'business',
											roleAction: 'deactivate'
				                                          
				                        },
				                        {
				                            title: 'คืนหลักประกัน',
				                            xtype: 'license-info-refundguarantee-form',
				                          	layout: 'fit',
				                            id: 'cancel-information-refundguarantee-form',
				                            border: false,
                   							frame: false
				                                          
				                        }
				                    ]
								}
							]

							
						  });
		                  
		                  winInfo.show();

		                  var formDeactivate = winInfo.down('#cancel-information-deactivate-business-form');
		                  formDeactivate.loadRecord(model);

		                  var formRefundguarantee = winInfo.down('#cancel-information-refundguarantee-form');
		                  formRefundguarantee.loadRecord(model);

		                 
		                break;
		                
			        	case 'G':

			        	var winInfo = Ext.create('Ext.window.Window', {
							id: 'cancel-information-guide-registration-infowindow',
							model: model,
							width: 750,
            				height: 550,
            				bodyPadding: 5,
            				autoShow: true,
				            autoScroll: true,
				            modal: true,
							items: [
								{
									xtype: 'tabpanel',
				                    plain: true,
				                    frame: false,
				                    width: '100%',
            						height: '100%',
				                    border: false,
				                    items: [
				                    	{
				                            title: 'ยกเลิกใบอนุญาต',
				                            xtype: 'license-info-deactivate-form',
						                    id: 'cancel-information-deactivate-guide-form',
						                    border: false,
						                    layout: 'fit',
						                    frame: false,
						                    traderType: 'B',
											traderTypeName: 'guide',
											roleAction: 'deactivate'
				                                          
				                        }
				                    ]
								}
							]

							
						  });
		                  
		                  winInfo.show();

		                  var formDeactivate = winInfo.down('#cancel-information-deactivate-guide-form');
		                  formDeactivate.loadRecord(model);
			                
			        		
			            break;
			                
			        	case 'L':

			        	var winInfo = Ext.create('Ext.window.Window', {
							id: 'cancel-information-tourleader-registration-infowindow',
							model: model,
							width: 750,
            				height: 550,
            				bodyPadding: 5,
            				autoShow: true,
				            autoScroll: true,
				            modal: true,
							items: [
								{
									xtype: 'tabpanel',
				                    plain: true,
				                    frame: false,
				                    width: '100%',
            						height: '100%',
				                    border: false,
				                    items: [
				                    	{
				                            title: 'ยกเลิกใบอนุญาต',
				                            xtype: 'license-info-deactivate-form',
						                    id: 'cancel-information-deactivate-tourleader-form',
						                    border: false,
						                    layout: 'fit',
						                    frame: false,
						                    traderType: 'B',
											traderTypeName: 'tourleader',
											roleAction: 'deactivate'
				                                          
				                        }
				                    ]
								}
							]

							
						  });
		                  
		                  winInfo.show();

		                  var formDeactivate = winInfo.down('#cancel-information-deactivate-tourleader-form');
		                  formDeactivate.loadRecord(model);
			                
			        		
			            break;
			                
			        
			        }
				}				
			break;
		}

		
	}
	
	,printReportBusinessInfoExpireDate: function(btn){
		var form = btn.up('form'),
	    values = form.getValues();
	    var dataParams = this.normalizeData(values);
	    dataParams.traderType = 'B';
	    
	    console.log(dataParams.expireDueDateFeeStatus);
	    console.log(dataParams.orgId);
	    console.log(dataParams.expireDateFrom);
	    console.log(dataParams.expireDateTo);
	    
	    var expireDueDateFeeStatus = typeof(dataParams.expireDueDateFeeStatus)=="undefined"?"":dataParams.expireDueDateFeeStatus;
	    var orgId = typeof(dataParams.orgId)=="undefined"?0:dataParams.orgId;
	    var expireDateFrom = typeof(dataParams.expireDateFrom)=="undefined"?"":dataParams.expireDateFrom;
	    var expireDateTo = typeof(dataParams.expireDateTo)=="undefined"?"":dataParams.expireDateTo;
	    
	    var reqUrl = "business/info/printCheckDueDateFeeExcel?"
   		 +"expireDueDateFeeStatus="+expireDueDateFeeStatus
   		 +"&orgId="+orgId
   		 +"&traderType="+dataParams.traderType
   		 +"&expireDateFrom="+expireDateFrom
	     +"&expireDateTo="+expireDateTo;

        window.open(reqUrl,"พิมพ์รายงาน","toolbar=0 width=900 height=600");

	}
	
	,searchBusinessInfoExpireDate: function(btn){
		var form = btn.up('form'),
	    values = form.getValues();
	    var dataParams = this.normalizeData(values);
	    dataParams.traderType = 'B';
	    
	    
	    var panel = form.up('#information-duedate-fee-panel');
	    var grid = panel.down('#fee-business-license-info-duedate-fee-grid');
	    var store = grid.getStore();
	   
	     dataParams.page = '1';
	     dataParams.start = '0';
	     dataParams.limit = store.pageSize;

	     console.log(dataParams);
	      store.load({
	          params: dataParams,
	          callback: function(records, operation, success) {
	        	  console.log(records);
	          },
	          scope: this
	      }); 
	}
		
	,searchBusinessInfo: function(btn){
		var form = btn.up('form'),
	    values = form.getValues();
	    var dataParams = this.normalizeData(values);
	    dataParams.traderType = 'B';
	    
	    
	    var panel = form.up('#information-tourlicense-panel-601');
	    var grid = panel.down('#info-business-license-info-touris-guide-tourleader-grid');
	    var store = grid.getStore();
	   
	     dataParams.page = '1';
	     dataParams.start = '0';
	     dataParams.limit = store.pageSize;

	     
	      store.load({
	          params: dataParams,
	          callback: function(records, operation, success) {
	        	  console.log(records);
	          },
	          scope: this
	      }); 
	}
	
	,searchGuideInfo: function(btn){
		var form = btn.up('form'),
	    values = form.getValues();
	    var dataParams = this.normalizeData(values);
	    dataParams.traderType = 'G';
	    
	    var panel = form.up('#information-guide-panel-042');
	    var grid = panel.down('#info-guide-license-info-touris-guide-tourleader-grid');
	    var store = grid.getStore();
	   
	     dataParams.page = '1';
	     dataParams.start = '0';
	     dataParams.limit = store.pageSize;

	     
	      store.load({
	          params: dataParams,
	          callback: function(records, operation, success) {

	          },
	          scope: this
	      }); 
	}
	
	,searchTourleaderInfo: function(btn){
		var form = btn.up('form'),
	    values = form.getValues();
	    var dataParams = this.normalizeData(values);
	    dataParams.traderType = 'L';
	    
	    var panel = form.up('#information-tourleader-panel-083');
	    var grid = panel.down('#info-tourleader-license-info-touris-guide-tourleader-grid');
	    var store = grid.getStore();
	   
	     dataParams.page = '1';
	     dataParams.start = '0';
	     dataParams.limit = store.pageSize;

	     
	      store.load({
	          params: dataParams,
	          callback: function(records, operation, success) {

	          },
	          scope: this
	      }); 
	}
	
	,
	  normalizeData: function(dataObj)
	  {
	      // remove null data;
	     for (field in dataObj) 
	      {
	        if (Ext.isEmpty(dataObj[field])) 
	        {
	          delete dataObj[field];
	        }
	        
	      }

	      return dataObj;
	  }
	  ,searchBusinessForSelect: function(btn)
	  {
	  	var win = btn.up('window');
		var form = win.down('form');
	    var values = form.getValues();
	    var dataParams = this.normalizeData(values);
	    dataParams.traderType = 'B';

	 
	    var grid = win.down('selector-business-grid');
	    var store = grid.getStore();
	        
	      store.load({
	          params: dataParams,
	          callback: function(records, operation, success) {
	               
	          },
	          scope: this
	      }); 
	  }


});