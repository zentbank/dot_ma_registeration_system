Ext.define('tourism.controller.info.SearchBusinessNameCtrl',{
	extend: 'Ext.app.Controller',
	refs: [
				{  
				    ref: 'info-business-name-formsearch',
				    selector: 'panel'
				} 
			],
	init : function (application) {

		        this.control({
		            // for search window only
		            'info-business-name-formsearch button[action=searchName]': {
		                click: this.searchName
		            }

		        });
		        
			},
	 searchName: function(btn)
			  {
			  	var win = btn.up('window');
				var form = win.down('form');
			    var values = form.getValues();
			    var dataParams = this.normalizeData(values);
			 
			    var grid = win.down('info-business-name-grid');
			    var store = grid.getStore();
			        
			      store.load({
			          params: dataParams,
			          callback: function(records, operation, success) {
			               
			          },
			          scope: this
			      }); 
			  }
			,normalizeData: function(dataObj)
			  {
			      // remove null data;
			     for (field in dataObj) 
			      {
			        if (Ext.isEmpty(dataObj[field])) 
			        {
			          delete dataObj[field];
			        }
			        
			      }

			      return dataObj;
			  }
});