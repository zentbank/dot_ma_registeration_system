Ext.define('tourism.controller.info.SearchLicenseCtrl', {
	extend : 'Ext.app.Controller'


	,refs: [
		{  
		    ref: 'info-license-grid',
		    selector: 'info-license-grid'
		} 
		,{  
		    ref: 'info-license-formsearch',
		    selector: 'panel'
		} 
	]

	,init : function (application) {

        this.control({
 
	        'info-license-grid actioncolumn': {
                itemclick: this.handleActionColumn
            }
            // for search window only
            ,'info-license-formsearch button[action=searchLicense]': {
                click: this.searchLicense
            }

        });
        
	}

	,handleActionColumn : function(column, action, grid, rowIndex, colIndex, model, node) {

	
		if(action){
	        switch(action){
	        
	        	case 'viewinfo':

		        	if(model.get('traderType') == 'B')
		        	{
		        		var winInfo = Ext.create('tourism.view.information.business.InfoBusinessWindow',{
		                    // id: 'business-information-business-registration-infowindow',
		                    animateTarget: column.el,
		                    actionMode: 'information',
		                    roleAction: 'KEY',
		                  });
	                  
	                  	winInfo.show();

	                  	winInfo.loadFormRecord(model);
		        	}

		        	if(model.get('traderType') == 'G')
		        	{
		        		var winInfo = Ext.create('tourism.view.information.guide.InfoGuideWindow',{
                			// id: 'supervisor-information-guide-registration-infowindow',
	                		animateTarget: column.el,
	                      	actionMode: 'information',
	                      	roleAction: 'supervisor',
	                	}).show();
	                	
	                	winInfo.loadFormRecord(model);
		        	}

		        	if(model.get('traderType') == 'L')
		        	{
		        		var winInfo = Ext.create('tourism.view.information.tourleader.InfoTourleaderWindow',{
	                		// id: 'supervisor-information-tourleader-registration-infowindow',
	                		animateTarget: column.el,
	                      actionMode: 'information',
	                      roleAction: 'supervisor',
	                	}).show();
	                		                	
	                	winInfo.loadFormRecord(model);
		        	}
   
                break;
	        
	        }
		}
	}
  ,searchLicense: function(btn)
  {
  	var win = btn.up('window');
	var form = win.down('form');
    var values = form.getValues();
    var dataParams = this.normalizeData(values);
 
    var grid = win.down('info-license-grid');
    var store = grid.getStore();
        
      store.load({
          params: dataParams,
          callback: function(records, operation, success) {
               
          },
          scope: this
      }); 
  }
,normalizeData: function(dataObj)
  {
      // remove null data;
     for (field in dataObj) 
      {
        if (Ext.isEmpty(dataObj[field])) 
        {
          delete dataObj[field];
        }
        
      }

      return dataObj;
  }

});