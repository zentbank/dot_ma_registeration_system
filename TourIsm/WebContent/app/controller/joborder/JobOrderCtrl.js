Ext.define('tourism.controller.joborder.JobOrderCtrl', {
	extend : 'Ext.app.Controller',
    refs: [
        {
            ref: '#joborder-JobOrder-grid',
            selector: '#joborder-JobOrder-grid'
        }
        ,
        {
            ref: '#joborder-JobOrderFormSearch-formsearch',
            selector: 'panel'
        }  
        ,
        {
            ref: '#joborder-JobOrderReportUploadFileFormSearch-formsearch',
            selector: 'panel'
        }  
    ],	 
	init : function (application) {

        this.control({
        
          
            '#joborder-JobOrderFormSearch-formsearch button[action=searchJobOrder]': {
                click: this.searchJobOrder
            },
            '#joborder-JobOrder-grid actioncolumn':{
              itemclick: this.handleActionColumn
           },
           '#joborder-JobOrderReportUploadFileFormSearch-formsearch button[action=printReportUploadFile]': {
               click: this.printReportUploadFile
           }
            
        });

		
	},
  searchJobOrder: function(btn)
  {
    
    var formSearch = btn.up('form');
    var values = formSearch.getValues();

    var dataParams = this.normalizeData(values);



    var grid = Ext.ComponentQuery.query('#joborder-JobOrder-grid')[0];

    grid.getStore().load({
        params: dataParams,
        callback: function(records, operation, success) {
   
        },
        scope: this
    });
  },
  printReportUploadFile: function(btn){
	  var form = btn.up('form');
	  var values = form.getValues();
	  //var dataParams = this.normalizeData(values);
    var dataParams = values;
	  var content = "";
	  content+="<form id='frmPrintReport' method='POST' action='business/joborder/printReportUploadFile' target='_blank'> ";
	  content+="	<input type='hidden' name='businessLicenseNo' id='businessLicenseNo' value='' /> ";
	  content+="	<input type='hidden' name='businessTraderName' id='businessTraderName' value='' /> ";
	  content+="	<input type='hidden' name='organizationId' id='organizationId' value='' /> ";
	  content+="</form> ";
	  var postWin = window.open("","พิมพ์รายงาน","toolbar=0 width=50 height=50");
	  postWin.document.write(content);
	  console.log(dataParams);
    postWin.document.getElementById('businessLicenseNo').value = dataParams.businessLicenseNo;
    postWin.document.getElementById('businessTraderName').value = dataParams.businessTraderName;  
    postWin.document.getElementById('organizationId').value = dataParams.orgId;  
   //  if("" != dataParams && dataParams.businessLicenseNo != ""){
		 //  postWin.document.getElementById('businessLicenseNo').value = dataParams.businessLicenseNo;
	  // }
	  // if("" != dataParams && dataParams.businessTraderName != ""){
		 //  postWin.document.getElementById('businessTraderName').value = dataParams.businessTraderName;  
	  // }
	  // if("" != dataParams && dataParams.orgId != ""){
		 //  postWin.document.getElementById('organizationId').value = dataParams.orgId;  
	  // }
	  postWin.document.getElementById('frmPrintReport').submit();
  },  
  normalizeData: function(dataObj)
  {
      // remove null data;
     for (field in dataObj) 
      {
        if (Ext.isEmpty(dataObj[field])) 
        {
          delete dataObj[field];
        }
        
      }

      return dataObj;
  },
  handleActionColumn: function(column, action, grid, rowIndex, colIndex, model, node)
  {
	 window.open('business/joborder/docx/read?jobId='+model.get('jobId'));
  }   
 
});