Ext.define('tourism.controller.ktbbank.KTBPaymentCtrl', {
	extend : 'Ext.app.Controller',
    refs: [

        {
            ref: '#ktbbank-KTBPayment-grid',
            selector: '#ktbbank-KTBPayment-grid'
        }
        ,
        {
            ref: '#ktbbank-KTBPaymentFormSearch-formsearch',
            selector: 'panel'
        }  
        ,{
            ref: '#ktbbank-prepareprintreceipt-form-001',
             selector: 'panel'
        },{
        	
        	ref: '#ktb-receipt-payment-form-001',
            selector: 'panel'
        }
       
        
        
    ],	 
	init : function (application) {

        this.control({
            'button[action=uploadFile]': {
                click: this.uploadFile
              }, 
              'button[action=savepayments]': {
                click: this.savepayments
              }, 
          
            '#ktbbank-KTBPaymentFormSearch-formsearch button[action=searchPayment]': {
                click: this.searchPayment
            },
            // '#account-business-guarantee-changeguarantee-form button[action=saveChangeGuaranteeBtn]': {
            //     click: this.saveChangeGuarantee
            // }
            // ,
             '#ktbbank-KTBPaymentFormSearch-formsearch button[action=uploadFile]':{
                  click: this.uploadFile
             } 
             ,
             '#ktbbank-KTBPaymentFormSearch-formsearch button[action=getPrintReceipt]':{
                  click: this.getPrintReceipt
             } 
             ,
             '#ktbbank-prepareprintreceipt-form-001 button[action=printReceiptBtn]':{
                  click: this.printReceiptBtn
             } ,
             '#ktbbank-KTBPayment-grid actioncolumn': {
                 itemclick: this.handleActionColumn
             },'#ktb-receipt-payment-form-001 button[action=receivepayment]':{
                  click: this.receivepayment
             } 
            
        });

		
	},
  savepayments: function(btn){
   
    var grid = btn.up('grid');
    var selectModels = grid.selModel.getSelection( );
    var listModel = new Array();

    Ext.Array.each(selectModels, function(model, index, self) {
      
      listModel.push({
        DReference1: model.get('DReference1'),
        DReference2: model.get('DReference2')
      });
    });
    var dataArray =  Ext.encode(listModel);



    Ext.MessageBox.wait('บันทึกข้อมูล ...');
    Ext.Ajax.request({
      url: 'business/ktb/payment/savePayments',
      params: {
          data: dataArray
      },
      // grid: grid,
      success: function(response){
          // var text = response.responseText;
          // this.grid.reload();
          Ext.MessageBox.hide();
          var grid = Ext.ComponentQuery.query('#ktbbank-KTBPayment-grid')[0];
           grid.getStore().reload();;

          // process server response here
      }
    }, this);
  }
  ,uploadFile: function(btn)
   {
      var winUpload = Ext.create('tourism.view.report.PaymentUploadPanel',{
          id: 'PaymentUploadPanel-registration-upload-window'
        });

      winUpload.show();
   },
  searchPayment: function(btn)
  {
    
    var formSearch = btn.up('form');

  if (!formSearch.isValid()) {

          var me = formSearch,
          errorCmp, fields, errors;

          fields = me.getForm().getFields();
          errors = '';
          fields.each(function(field) {
              Ext.Array.forEach(field.getErrors(), function(error) {
                  // errors.push({name: field.getFieldLabel(), error: error});
                  errors += field.getFieldLabel()  +'<br>';
              });
          });
          // Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', errors);

          Ext.Msg.show({
             title:'กรุณาตรวจสอบข้อมูล',
             msg: errors,
             buttons: Ext.Msg.OK,
             icon: Ext.Msg.ERROR
          });
          return;
      }

    var values = formSearch.getValues();

    var dataParams = this.normalizeData(values);



    var grid = Ext.ComponentQuery.query('#ktbbank-KTBPayment-grid')[0];

    grid.getStore().load({
        params: dataParams,
        callback: function(records, operation, success) {
   
        },
        scope: this
    });
  }
  ,handleActionColumn: function(column, action, grid, rowIndex, colIndex, model, node)
  {
	  
	  
	  var win = Ext.create('tourism.view.ktbbank.ReceivePaymentWindow',{
		 
		  ktbPaymentGrid: grid
	  });
	  
	  win.show();

	  var form = win.down('ktb-receipt-payment-form');
	  form.getForm().findField('progressDate').setValue(model.get('progressDate'));
    form.getForm().findField('paymentNo').setValue(model.get('paymentNo'));
    

    if(model.get('recordStatus') == 'N'){
      
      form.down('#ktb-receipt-payment-form-receivepayment-btn').setVisible(false);
    }

	  var paymentDetailGrid = win.down('ktbbank-receive-payment-grid');
	
	  paymentDetailGrid.getStore().load({
	    params: {progressDate: model.get('progressDate'), paymentNo: model.get('paymentNo')},
	    callback: function(records, operation, success) {
	       
	       
	    },
	    scope: this
	  }); 
  }
  ,receivepayment: function(btn){
	  var form    = btn.up('form');
      var win = btn.up('window');

      var model = form.getRecord(); //Ext.data.Model
      var values = form.getValues();//object {xx: q,ss:ff}

      
      if (!form.isValid()) {

          var me = form,
          errorCmp, fields, errors;

          fields = me.getForm().getFields();
          errors = '';
          fields.each(function(field) {
              Ext.Array.forEach(field.getErrors(), function(error) {
                  // errors.push({name: field.getFieldLabel(), error: error});
                  errors += field.getFieldLabel()  +'<br>';
              });
          });
          // Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', errors);

          Ext.Msg.show({
             title:'กรุณาตรวจสอบข้อมูล',
             msg: errors,
             buttons: Ext.Msg.OK,
             icon: Ext.Msg.ERROR
          });
          return;
      }


      var fsubmit = form.getForm();
      var ctrl = this; // scope

    
      var dataParams = this.normalizeData(values);
     
       fsubmit.load({
            url: 'business/ktb/payment/savePaymentDetail',
            method : 'POST',
            waitMsg: 'กรุณารอสักครู่..',
            params: dataParams,
            success: function(form, action) {

              var dataObj = action.result.data;
             
              // Ext.Msg.alert('', 'บันทึกข้อมูล');
            var noti = Ext.create('widget.uxNotification', {
              // title: 'Notification',
              position: 'tr',
              manager: 'instructions',
              // cls: 'ux-notification-light',
              // iconCls: 'ux-notification-icon-information',
              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
              closable: false,
              autoCloseDelay: 4000,
              width: 300,
              slideBackDuration: 500,
              slideInAnimation: 'bounceOut',
              slideBackAnimation: 'easeIn'
            });
            noti.show();

              var grid = Ext.ComponentQuery.query('#ktbbank-receive-payment-grid-001')[0];
              var store = grid.getStore();

              store.reload();
              win.close();
        
            },
            failure: function(form, action) {
              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
            },
            scope: this 
        });
  }
  ,  normalizeData: function(dataObj)
  {
      // remove null data;
     for (field in dataObj) 
      {
        if (Ext.isEmpty(dataObj[field])) 
        {
          delete dataObj[field];
        }
        
      }

      return dataObj;
  }
  
  ,getPrintReceipt: function()
  {

      var win = Ext.create('tourism.view.ktbbank.KTBPreparePrintReceiptWindow',{
          id: 'ktbbank-prepareprintreceipt-addeditwindow-001'
        });

      win.show();

  }
  ,printReceiptBtn: function(btn)
  {

    var receiptWin = btn.up('window');

    var formReceipt = receiptWin.down('#ktbbank-prepareprintreceipt-form-001');

      if (!formReceipt.isValid()) {

          var me = formReceipt,
          errorCmp, fields, errors;

          fields = me.getForm().getFields();
          errors = '';
          fields.each(function(field) {
              Ext.Array.forEach(field.getErrors(), function(error) {
                  // errors.push({name: field.getFieldLabel(), error: error});
                  errors += field.getFieldLabel()  +'<br>';
              });
          });
          // Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', errors);

          Ext.Msg.show({
             title:'กรุณาตรวจสอบข้อมูล',
             msg: errors,
             buttons: Ext.Msg.OK,
             icon: Ext.Msg.ERROR
          });
          return;
      }

      var values = formReceipt.getValues();

      var reqUrl = "business/ktb/payment/print/?orgId="+values.orgId
                    +'&receiveOfficerDate='+values.receiveOfficerDate
                    +'&receiveOfficerName='+values.receiveOfficerName
                    +'&authority='+values.authority;
            window.open(reqUrl,"พิมพ์ใบเสร็จรับเงิน","toolbar=0 width=900 height=600"); 
      }
 
});