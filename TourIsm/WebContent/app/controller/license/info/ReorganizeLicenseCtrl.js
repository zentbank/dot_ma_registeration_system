Ext.define('tourism.controller.license.info.ReorganizeLicenseCtrl', {
	extend : 'Ext.app.Controller',

   	stores: [],

    models: [],

    views: [
           
    ]

	,refs: [
		{  
		    ref: '#license-info-formsearch-business-reorganize',
		    selector: 'panel'
		} 
		,{  
		    ref: '#license-info-formsearch-guide-reorganize',
		    selector: 'panel'
		} 
		,{  
		    ref: '#license-info-formsearch-tourleader-reorganize',
		    selector: 'panel'
		}

		,{  
		    ref: '#reorganize-business-registration-addedit',
		    selector: 'panel'
		}
		,{  
		    ref: '#reorganize-guide-registration-addedit',
		    selector: 'panel'
		}
		,{  
		    ref: '#reorganize-tourleader-registration-addedit',
		    selector: 'panel'
		}

		,{  
            ref: '#reorganize-business-license-info-touris-guide-tourleader-grid',
            selector: '#recheck-business-approvalprocess-grid'
        },
        {  
            ref: '#reorganize-guide-license-info-touris-guide-tourleader-grid',
            selector: '#recheck-guide-approvalprocess-grid'
        },
        {  
            ref: '#reorganize-tourleader-license-info-touris-guide-tourleader-grid',
            selector: '#recheck-tourleader-approvalprocess-grid'
        }  
	]

	,init : function (application) {

        this.control({
        	'#license-info-formsearch-business-reorganize button[action=searchBusinessInfo]': {
                click: this.searchBusinessInfo
            }
	        ,'#license-info-formsearch-guide-reorganize button[action=searchGuideTourleaderLicense]': {
	            click: this.searchGuideInfo
	        }
	        ,'#license-info-formsearch-tourleader-reorganize button[action=searchGuideTourleaderLicense]': {
	            click: this.searchTourleaderInfo
	        }
	        ,'#reorganize-business-license-info-touris-guide-tourleader-grid actioncolumn': {
                itemclick: this.handleActionColumn
            }
	        ,'#reorganize-guide-license-info-touris-guide-tourleader-grid actioncolumn': {
                itemclick: this.handleActionColumn
            }
	        ,'#reorganize-tourleader-license-info-touris-guide-tourleader-grid actioncolumn': {
                itemclick: this.handleActionColumn
            }

         	,'#reorganize-business-registration-addedit button[action=saveBusinessRegistration]': {
                click: this.saveBusinessRegistration
            }
            
            ,'#reorganize-guide-registration-addedit button[action=saveGuideRegistration]': {
                click: this.saveGuideRegistration
            }
            ,'#reorganize-tourleader-registration-addedit button[action=saveTourleaderRegistration]': {
                click: this.saveTourleaderRegistration
            }
           
        });
        
	}

	,handleActionColumn : function(column, action, grid, rowIndex, colIndex, model, node) {
	
		if( !Ext.isEmpty(model.get('traderType')) && model.get('licenseStatus') != 'C'){
	        switch(model.get('traderType')){
	        
	        	case 'B':
                
                  var edit = Ext.create('tourism.view.business.registration.BusinessRegistrationAddEditWindow',{
                      animateTarget: column.el,
                      actionMode: 'edit',
                      roleAction: 'reorganize',
                      traderType: 'business',
                      verificationGrid: grid

                     });

                     edit.show();

                     edit.loadDataModel(model);
                break;
                
	        	case 'G':
	                
	        		var edit = Ext.create('tourism.view.guide.registration.GuideRegistrationAddEditWindow',{
                        animateTarget: column.el,
                            actionMode: 'edit',
                            roleAction: 'reorganize',
                            traderType: 'guide',
                            verificationGrid: grid
                        });
                        edit.show();

                        edit.loadDataModel(model);
	            break;
	                
	        	case 'L':
	                
	        		var edit = Ext.create('tourism.view.tourleader.registration.TourleaderRegistrationAddEditWindow',{
                            animateTarget: column.el,
                            actionMode: 'edit',
                            roleAction: 'reorganize',
                            traderType: 'tourleader',
                            verificationGrid: grid
                        });

                       edit.show();

                       edit.loadDataModel(model);
	            break;
	                
	        
	        }
		}
	}
		
	,searchBusinessInfo: function(btn){
		var form = btn.up('form'),
	    values = form.getValues();
	    var dataParams = this.normalizeData(values);
	    dataParams.traderType = 'B';
    
    
	    var panel = form.up('#reorganize-tourlicense-panel');
	    var grid = panel.down('#reorganize-business-license-info-touris-guide-tourleader-grid');
	    var store = grid.getStore();
	   
	     dataParams.page = '1';
	     dataParams.start = '0';
	     dataParams.limit = store.pageSize;

	     
	      store.load({
	          params: dataParams,
	          callback: function(records, operation, success) {
	        	  console.log(records);
	          },
	          scope: this
	      }); 
	}
	
	,searchGuideInfo: function(btn){
		var form = btn.up('form'),
	    values = form.getValues();
	    var dataParams = this.normalizeData(values);
	    dataParams.traderType = 'G';

	    
	    var panel = form.up('#reorganize-guide-panel');
	    var grid = panel.down('#reorganize-guide-license-info-touris-guide-tourleader-grid');
	    var store = grid.getStore();
	   
	     dataParams.page = '1';
	     dataParams.start = '0';
	     dataParams.limit = store.pageSize;

	     
	      store.load({
	          params: dataParams,
	          callback: function(records, operation, success) {

	          },
	          scope: this
	      }); 
	}
	
	,searchTourleaderInfo: function(btn){
		var form = btn.up('form'),
	    values = form.getValues();
	    var dataParams = this.normalizeData(values);
	    dataParams.traderType = 'L';

	    var panel = form.up('#reorganize-tourleader-panel');
	    var grid = panel.down('#reorganize-tourleader-license-info-touris-guide-tourleader-grid');
	    var store = grid.getStore();
	   
	     dataParams.page = '1';
	     dataParams.start = '0';
	     dataParams.limit = store.pageSize;

	     
	      store.load({
	          params: dataParams,
	          callback: function(records, operation, success) {

	          },
	          scope: this
	      }); 
	}
	
	,
	  normalizeData: function(dataObj)
	  {
	      // remove null data;
	     for (field in dataObj) 
	      {
	        if (Ext.isEmpty(dataObj[field])) 
	        {
	          delete dataObj[field];
	        }
	        
	      }

	      return dataObj;
	  }
   , saveBusinessRegistration: function(btn, model)
  {
      //console.log('saveBusinessRegistration');
      var form    = btn.up('form');
      var win = btn.up('window');

      var model = form.getRecord(); //Ext.data.Model
      var values = form.getValues();//object {xx: q,ss:ff}
      var grid = win.verificationGrid;

      
      if (!form.isValid()) {

          var me = form,
          errorCmp, fields, errors;

          fields = me.getForm().getFields();
          errors = '';
          fields.each(function(field) {
              Ext.Array.forEach(field.getErrors(), function(error) {
                  // errors.push({name: field.getFieldLabel(), error: error});
                  errors += field.getFieldLabel()  +'<br>';
              });
          });
       

          Ext.Msg.show({
             title:'กรุณาตรวจสอบข้อมูล',
             msg: errors,
             buttons: Ext.Msg.OK,
             icon: Ext.Msg.ERROR
          });
          return;
      }


      var fsubmit = form.getForm();
      var ctrl = this; // scope

      if(!Ext.isEmpty(values.personTypeCorporate))
      {// กรรีที่เป็น บริษัท
        values.personType = 'C';
        values.prefixId = values.prefixIdCorp;
        values.firstName = values.firstNameCorp;
        values.identityNo = values.identityNoCorp;
        values.identityDate = values.identityDateCorp;
        values.provinceId = values.provinceIdCorp;        
      }
      else
      {
        values.personType = 'I';
      }

      var dataParams = this.normalizeData(values);
     
      // fsubmit.submit({
       fsubmit.load({
            url: 'business/info/reorganize/save',
            method : 'POST',
            waitMsg: 'กรุณารอสักครู่..',
            params: dataParams,
            success: function(form, action) {

              var dataObj = action.result.data;
             
              // Ext.Msg.alert('', 'บันทึกข้อมูล');
            var noti = Ext.create('widget.uxNotification', {
              // title: 'Notification',
              position: 'tr',
              manager: 'instructions',
             
              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
              closable: false,
              autoCloseDelay: 4000,
              width: 300,
              slideBackDuration: 500,
              slideInAnimation: 'bounceOut',
              slideBackAnimation: 'easeIn'
            });
            noti.show();

              var store = grid.getStore();

              store.reload();


             
              win.close();
        
            },
            failure: function(form, action) {
              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
            },
            scope: this 
        });
  }
 ,  saveGuideRegistration: function(btn,model){
    //console.log('saveGuideRegistration');
    var win = btn.up('window');
    var form = win.down('form');
    
    var model = form.getRecord(); //Ext.data.Model
      var values = form.getValues();//object {xx: q,ss:ff}
      var grid = win.verificationGrid;
     
      var guideTrader = Ext.ComponentQuery.query('guide_registration_guidetype_guidetypeaddedit_form')[0];
     
      var provinceIdName = Ext.getCmp('guide-type-registration-masprovince-combo').getValue();
      var traderCategoryName = Ext.getCmp('guide-type-registration-categorystore-combo').getValue();
      var traderAreaName = guideTrader.getForm().findField('traderArea').getValue();
     
      values.traderCategory = traderCategoryName;
        
      if(provinceIdName !=null)
      {
        values.provinceGuideServiceId = provinceIdName;
      }else
      {
        values.provinceGuideServiceId = '';
      }
      
  
      if(traderAreaName !=null)
      {
        values.traderArea = traderAreaName;
      }else
      {
        values.traderArea = '';
      }
      
        if (!form.isValid()) {

            var me = form,errorCmp, fields, errors;

            fields = me.getForm().getFields();
            errors = '';
            fields.each(function(field) {
                Ext.Array.forEach(field.getErrors(), function(error) {
                  // errors.push({name: field.getFieldLabel(), error: error});
                    errors += field.getFieldLabel()  +'<br>';
                });
            });
            // Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', errors);

            Ext.Msg.show({
               title:'กรุณาตรวจสอบข้อมูล',
               msg: errors,
               buttons: Ext.Msg.OK,
               icon: Ext.Msg.ERROR
            });
            return;
        }
        
        var fsubmit = form.getForm();
        values.personType = 'I';
     
        var dataParams = this.normalizeData(values);
  
        
        fsubmit.load({
            url: 'business/info/reorganize/save',
            method : 'POST',
            waitMsg: 'กรุณารอสักครู่..',
            params: dataParams,
            success: function(form, action) {

              var dataObj = action.result.data;
             
              // Ext.Msg.alert('', 'บันทึกข้อมูล');

            var noti = Ext.create('widget.uxNotification', {
              // title: 'Notification',
              position: 'tr',
              manager: 'instructions',
              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
              closable: false,
              autoCloseDelay: 4000,
              width: 300,
              slideBackDuration: 500,
              slideInAnimation: 'bounceOut',
              slideBackAnimation: 'easeIn'
            });
            noti.show();

              var store = grid.getStore();

              store.reload();

              win.close();
        
            },
            failure: function(form, action) {
              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
            },
            scope: this 
        });
        
        
  }
  ,saveTourleaderRegistration: function(btn, model){
      //console.log('saveTourleaderRegistration');
      var win = btn.up('window');
      var form = win.down('form');
      
      var model = form.getRecord(); //Ext.data.Model
      var values = form.getValues();//object {xx: q,ss:ff}
      var grid = win.verificationGrid;
        

        
        var fsubmit = form.getForm();
        values.personType = 'I';
        
        var dataParams = this.normalizeData(values);
        
        fsubmit.load({
            url: 'business/info/reorganize/save',
            method : 'POST',
            waitMsg: 'กรุณารอสักครู่..',
            params: dataParams,
            success: function(form, action) {

              var dataObj = action.result.data;
        

            var noti = Ext.create('widget.uxNotification', {
              // title: 'Notification',
              position: 'tr',
              manager: 'instructions',
              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
              closable: false,
              autoCloseDelay: 4000,
              width: 300,
              slideBackDuration: 500,
              slideInAnimation: 'bounceOut',
              slideBackAnimation: 'easeIn'
            });
            noti.show();

              
              var store = grid.getStore();

              store.reload();

              win.close();
        
            },
            failure: function(form, action) {
              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
            },
            scope: this 
        });
        
    }


});