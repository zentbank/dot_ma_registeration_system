Ext.define('tourism.controller.menu.TreeMenuCrtl', {
  extend : 'Ext.app.Controller',

     stores: ['tourism.store.menu.MenuStore'],

    models: [],

    views: ['tourism.view.menu.TreeMenu'],

    refs: [
      {
            ref: 'treemenu',
            selector: 'treemenu'
      }
    ],
  init : function (application) {

    this.control({
        'treemenu': {
            itemclick: function(tree, model, ele, index, e, eOpts){

//            	alert(model.raw.menuId);

              var centerView = Ext.ComponentQuery.query('centerview')[0];

              var itemIndex = '';
              if(model.raw.menuId == 'B')
              {
                itemIndex = 0;
              }

              if(model.raw.menuId == 'G')
              {
                itemIndex = 1;
              }

              if(model.raw.menuId == 'T')
              {
                itemIndex = 2;
              }
              if(model.raw.menuId == 'LAWB')
              {
                itemIndex = 3;
                // var grid = Ext.ComponentQuery.query('#lawyer-approvalprocess-grid')[0];
                // grid.getStore().load();
              }
              if(model.raw.menuId == 'LAWG')
              {
                itemIndex = 4;
                // var grid = Ext.ComponentQuery.query('#lawyer-approvalprocess-grid')[0];
                // grid.getStore().load();
              }
              if(model.raw.menuId == 'LAWL')
              {
                itemIndex = 5;
                // var grid = Ext.ComponentQuery.query('#lawyer-approvalprocess-grid')[0];
                // grid.getStore().load();
              }

              if(model.raw.menuId == 'RECHKB')
              {
                itemIndex = 6;
                // var grid = Ext.ComponentQuery.query('#recheck-approvalprocess-grid')[0];
                // grid.getStore().load();
              }
              if(model.raw.menuId == 'RECHKG')
              {
                itemIndex = 7;
                // var grid = Ext.ComponentQuery.query('#recheck-approvalprocess-grid')[0];
                // grid.getStore().load();
              }
              if(model.raw.menuId == 'RECHKL')
              {
                itemIndex = 8;
                // var grid = Ext.ComponentQuery.query('#recheck-approvalprocess-grid')[0];
                // grid.getStore().load();
              }
              if(model.raw.menuId == 'SUPB')
              {
                itemIndex = 9;

              }
              if(model.raw.menuId == 'SUPG')
              {
                itemIndex = 10;

              }
              if(model.raw.menuId == 'SUPL')
              {
                itemIndex = 11;

              }
              if(model.raw.menuId == 'MANB')
              {
                itemIndex = 12;

              }
              if(model.raw.menuId == 'MANG')
              {
                itemIndex = 13;

              }
              if(model.raw.menuId == 'MANL')
              {
                itemIndex = 14;

              }
              if(model.raw.menuId == 'DIRB')
              {
                itemIndex = 15;

              }
              if(model.raw.menuId == 'DIRG')
              {
                itemIndex = 16;

              }
              if(model.raw.menuId == 'DIRL')
              {
                itemIndex = 17;

              }
              if(model.raw.menuId == 'ACCB')
              {
                itemIndex = 18;

              }
              if(model.raw.menuId == 'ACCG')
              {
                itemIndex = 19;

              }
              if(model.raw.menuId == 'COURSE')
              {
              itemIndex = 20;
              }
              if(model.raw.menuId == 'PERSONCOURSE')
              {
              itemIndex = 21;
              }
              if(model.raw.menuId == 'DOCB')
              {
              itemIndex = 22;
              }
              if(model.raw.menuId == 'DOCG')
              {
              itemIndex = 23;
              }
              if(model.raw.menuId == 'DOCL')
              {
              itemIndex = 24;
              }
              if(model.raw.menuId == 'PLB')
              {
                itemIndex = 25;
              }
              if(model.raw.menuId == 'PLG')
              {
              itemIndex = 26;
              }
              if(model.raw.menuId == 'PLL')
              {
                itemIndex = 27;
              }
              //ทะเบียนใบอนุญาต
              if(model.raw.menuId == 'PERMITB')
              {
                itemIndex = 28;
              }
              if(model.raw.menuId == 'PERMITG')
              {
                itemIndex = 29;
              }
              if(model.raw.menuId == 'PERMITL')
              {
                itemIndex = 30;
              }
              //เรื่องร้องเรียน
              if(model.raw.menuId == 'FAQ')
              {
                itemIndex = 31;
              }
              if(model.raw.menuId == 'COMPLAINRL')
              {
                itemIndex = 32;
              }
              if(model.raw.menuId == 'COMPLAINML')
              {
                itemIndex = 33;
              }
              if(model.raw.menuId == 'CONSIDER')
              {
                itemIndex = 34;
              }

              // if(model.raw.menuId == 'SUSPEND')
              // {
              //   itemIndex = 35;
              // }
              // if(model.raw.menuId == 'REVOKE')
              // {
              //   itemIndex = 36;
              // }

              if(model.raw.menuId == 'CARDG')
              {
                itemIndex = 35;
              }
              if(model.raw.menuId == 'CARDL')
              {
                itemIndex = 36;
              }
              if(model.raw.menuId == 'PADDR')
              {
                itemIndex = 37;
              }
              if(model.raw.menuId == 'SUSPENDBUSINESS')
              {
                itemIndex = 38;
              }
              if(model.raw.menuId == 'SUSPENDGUIDE')
              {
                itemIndex = 39;
              }
              if(model.raw.menuId == 'SUSPENDTOURLEADER')
              {
                itemIndex = 40;
              }
              if(model.raw.menuId == 'REVOKEBUSINESS')
              {
                itemIndex = 41;
              }
              if(model.raw.menuId == 'REVOKEGUIDE')
              {
                itemIndex = 42;
              }
              if(model.raw.menuId == 'REVOKETOURLEADER')
              {
                itemIndex = 43;
              }
              if(model.raw.menuId == 'DEACTIVATEBUSINESS')
              {
                itemIndex = 44;
              }
              if(model.raw.menuId == 'DEACTIVATEGUIDE')
              {
                itemIndex = 45;
              }
              if(model.raw.menuId == 'DEACTIVATETOURLEADER')
              {
                itemIndex = 46;
              }
              if(model.raw.menuId == 'REFUNGUARANTEE')
              {
                itemIndex = 47;
              }
              if(model.raw.menuId == 'REPORTFEEGUIDE')
              {
               itemIndex = 48;
              }
              //chart
              if(model.raw.menuId == 'CHART_BUSINESS_TRADER_CATEGORY')
              {
               itemIndex = 49;
              }
              if(model.raw.menuId == 'CHART_BUSINESS_ALL')
              {
               itemIndex = 50;
              }
              if(model.raw.menuId == 'CHART_BUSINESS_NEW')
              {
               itemIndex = 51;
              }
              if(model.raw.menuId == 'CHART_BUSINESS_RENEW')
              {
               itemIndex = 52;
              }
              if(model.raw.menuId == 'REPORTFEEBUSINESSGUIDE')
              {
               itemIndex = 53;
              }
              if(model.raw.menuId == 'CHART_BUSINESS_RECEIVE_GUARANTEE')
              {
               itemIndex = 54;
              }
              if(model.raw.menuId == 'CHART_BUSINESS_SUSPENSION_REVOKE')
              {
               itemIndex = 55;
              }
              if(model.raw.menuId == 'CHART_GUIDE_TYPE')
              {
               itemIndex = 56;
              }
              if(model.raw.menuId == 'CHART_GUIDE_ALL')
              {
               itemIndex = 57;
              }
              if(model.raw.menuId == 'CHART_GUIDE_NEW')
              {
               itemIndex = 58;
              }
              if(model.raw.menuId == 'CHART_GUIDE_RENEW')
              {
               itemIndex = 59;
              }
              if(model.raw.menuId == 'RPTACCFEETOTAL')
              {
               itemIndex = 60;
              }
              if(model.raw.menuId == 'RPTACCINEXPTOTAL')
              {
               itemIndex = 61;
              }
              if(model.raw.menuId == 'RPTGUARANTEETOTAL')
              {
               itemIndex = 62;
              }
              if(model.raw.menuId == 'RPTGUARANTEEA_FEE')
              {
               itemIndex = 63;
              }

              if(model.raw.menuId == 'ADMUSER')
              {
               itemIndex = 64;
              }
              if(model.raw.menuId == 'DEPNEWS')
              {
               itemIndex = 65;
              }
              if(model.raw.menuId == 'MASCOMPTYPE')
              {
               itemIndex = 66;
              }
              if(model.raw.menuId == 'MASACTTYPE')
              {
               itemIndex = 67;
              }
              if(model.raw.menuId == 'MASDEACTYPE')
              {
               itemIndex = 68;
              }
              if(model.raw.menuId == 'MASFEES')
              {
               itemIndex = 69;
              }

              //
              if(model.raw.menuId == 'CHART_GUIDE_ORG')
              {
               itemIndex = 70;
              }
              if(model.raw.menuId == 'CHART_TOURLEADER_NEW')
              {
               itemIndex = 71;
              }


              if(model.raw.menuId == 'CHANGE_BUSINESS_GUARANTEE')
              {
               itemIndex = 72;
              }

              if(model.raw.menuId == 'KTB_PAYMENT')
              {
               itemIndex = 73;
              }

              if(model.raw.menuId == 'CHART_BUSINESS_GUIDE_TOURLEADER_ALL')
              {
               itemIndex = 74;
              }
              if(model.raw.menuId == 'EDIT_TOUR_LICENSE')
              {
               itemIndex = 75;
              }
              if(model.raw.menuId == 'EDIT_GUIDE_LICENSE')
              {
               itemIndex = 76;
              }
              if(model.raw.menuId == 'EDIT_TOURLEADER_LICENSE')
              {
               itemIndex = 77;
              }
              if(model.raw.menuId == 'REQ_CERTIFICATE')
              {
               itemIndex = 78;
              }
              if(model.raw.menuId == 'REQ_CERTIFICATE_RPT')
              {
               itemIndex = 79;
              }
              if(model.raw.menuId == 'REQ_PASSPORT')
              {
               itemIndex = 80;
              }
              if(model.raw.menuId == 'REQ_PASSPORT_RPT')
              {
               itemIndex = 81;
              }
              if(model.raw.menuId == 'DUE_DATE_FEE')
              {
               itemIndex = 82;
              }
              if(model.raw.menuId == 'GUIDE_DETAIL')
              {
               itemIndex = 83;
              }
              if(model.raw.menuId == 'JOB_ORDER')
              {
               itemIndex = 84;
              }
              if(model.raw.menuId == 'ACCB_CANCLE')
              {
               itemIndex = 85;
              }
              if(model.raw.menuId == 'ACCO')
              {
               itemIndex = 86;

               var grid = Ext.ComponentQuery.query('#account-payment-online-grid-001')[0];
                grid.getStore().load();
              }
              if(model.raw.menuId == 'SUSPEND_ORDER')
              {
               itemIndex = 87;
              }
              if(model.raw.menuId == 'REQ_TOUR_OPERATOR')
              {
                itemIndex = 88;
              }

              if(model.raw.menuId == 'CHART_GUIDE_LANGUAGE')
              {
                itemIndex = 89;
              }
              
              if(model.raw.menuId == 'CHART_BUSINESS_PROVINCE')
              {
                itemIndex = 90;
              }
              
              if(model.raw.menuId == 'CHART_STATIC_TOURLEADER')
              {
                itemIndex = 91;
              }

              if(model.raw.menuId == 'CHART_BUSINESS_CHANGE')
              {
                itemIndex = 92;
              }
              
              if(model.raw.menuId == 'CHART_BUSINESS_NEW_REGIS')
              {
                itemIndex = 93;
              }
              
              if(model.raw.menuId == 'CHART_BUSINESS_MASDEACTYPE')
              {
                itemIndex = 94;
              }


              if(model.raw.menuId == 'GUIDE_TEMPLATE')
              {
               window.open('business/printcard/registration/tp_admin',"ออกแบบบัตร","toolbar=0 width=1270 height=722");
              }
              if(model.raw.menuId == 'RPT_JOBORDER')
              {
               itemIndex = 95;
              }
              if(model.raw.menuId == 'RPT_TOURISM_BUSINESS_BY_PLACE')
              {
               itemIndex = 96;
              }


              if(!Ext.isEmpty(itemIndex))
              {
                centerView.getLayout().setActiveItem(itemIndex);
              }


            }
        }
    });


  }
});
