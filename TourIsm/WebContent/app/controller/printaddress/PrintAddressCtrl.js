Ext.define('tourism.controller.printaddress.PrintAddressCtrl',{
	extend: 'Ext.app.Controller',
	 models: ['tourism.model.registration.RegistrationModel'],
	    
	    views: [
	            'tourism.view.printaddress.PrintAddressFormSearch',
	            'tourism.view.printaddress.PrintAddressFormSearchByLicense'
	           
	          ],
	          refs:[   
	                {
	               	 ref:'#printaddress-formsearch',
	               	 selector:'panel'
	                },
	                {
	               	 ref:'#printaddress-formsearchbylicense',
	               	 selector:'panel'
	                }
	                ],
	      init : function (application) {

	          this.control({
	                   	  	'#printaddress-formsearch button[action=searchPrintAddressBetweenRegistration]':{
	                   	  		click: this.searchPrintAddress
	                   	 	},
	                   	 	'#printaddress-formsearchbylicense button[action=printAddressFormSearchByLicense]':{
	                   	  		click: this.printAddressFormSearchByLicense
	                   	 	}
	                   	 	
	                    });
	         },
	         searchPrintAddress: function(btn)
	         {
	        	 var form = btn.up('form');
	        	 var licenseNoFrom =  form.getForm().findField('licenseNoFrom').getValue();
	        	 var licenseNoTo =  form.getForm().findField('licenseNoTo').getValue();
	        	 //if(!Ext.isEmpty(licenseNoTo) && licenseNoTo !=null)
	             //{
	        		 var reqUrl = "business/printaddress/registration/printAddressPdf?licenseNoFrom="+licenseNoFrom+"&licenseNoTo="+licenseNoTo;
	                 window.open(reqUrl,"พิมพ์ที่อยู่","toolbar=0 width=900 height=600");
	             //}else
	            // {
	            //	var reqUrl = "business/printaddress/registration/printAddressPdf?licenseNoFrom="+licenseNoFrom;
	             //   window.open(reqUrl,"พิมพ์บัตร","toolbar=0 width=900 height=600");
	           //  }
                 
	         },
	         printAddressFormSearchByLicense: function(btn)
	         {
	         	var form = btn.up('form');
	         	// var licenseNo =  form.getForm().findField('licenseNo');
	         	var arrLicNo =  form.getValues();


	         	var listModel = new Array();

	         	Ext.Array.each(arrLicNo.licenseNo, function(name, index, countriesItSelf) {
				   
	         		if(!Ext.isEmpty(name))
	         		{
	         			listModel.push({
			              licenseNo: name
			            });
	         		}
				    
				});

	         	

	         	var licenseNoStr =  Ext.encode(listModel);


	         	var reqUrl = "business/printaddress/registration/printAddressByLicensePdf?licenseNo="+licenseNoStr;
	            window.open(reqUrl,"พิมพ์ที่อยู่","toolbar=0 width=900 height=600");
	         },
	         normalizeData: function(dataObj)
	         {
	             // remove null data;
	            for (field in dataObj) 
	             {
	               if (Ext.isEmpty(dataObj[field])) 
	               {
	                 delete dataObj[field];
	               }
	               
	             }

	             return dataObj;
	         }
});