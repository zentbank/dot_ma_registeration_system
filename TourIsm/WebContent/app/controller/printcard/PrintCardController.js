Ext.define('tourism.controller.printcard.PrintCardController',{
	extend : 'Ext.app.Controller',
	//stores: ['tourism.store.registration.RegistrationStore'],

    models: ['tourism.model.registration.RegistrationModel'],
    
    views: [
            'tourism.view.printcard.PrintCardGrid',
            'tourism.view.printcard.PrintCardGuideFormSearch',
            'tourism.view.printcard.PrintCardTourLeaderFormSearch',
         
          ],
          refs:[
                {
               	 ref:'printcard-grid',
               	 selector:'printcard-grid'
                },               
                {
               	 ref:'#printcard-guide-printcard-formsearch',
               	 selector:'panel'
                },
                {
               	 ref:'#printcard-tourleader-printcard-formsearch',
               	 selector:'panel'
                }
                
                ],
      init : function (application) {

          this.control({
                   	  	'#printcard-tourleader-printcard-formsearch button[action=searchCardBetweenRegistration]':{
                   	  		click: this.searchTourLeaderCard
                   	 } ,
                   	 
                       	'#printcard-guide-printcard-formsearch button[action=searchCardBetweenRegistration]':{
                       		click: this.searchGuideCard
                     },
                     	'#printcard-tourleader-printcard-formsearch button[action=exportExcelCardTourLeaderBetweenRegistration]':{
                	  		click: this.printcardTourLeaderExcel
                	 } ,
                        '#printcard-guide-printcard-formsearch button[action=exportExcelCardBetweenRegistration]':{
                        	click: this.printcardGuideExcel
                     },
                     	'#printcard-guide-printcard-grid actioncolumn':{
                     		itemclick: this.handleActionColumn
                     },
                     	'#printcard-tourleader-printcard-grid actioncolumn':{
                     		itemclick: this.handleActionColumn
                     }
                    });
         },
         searchGuideCard: function(btn)
         {
           var form = btn.up('form'),
           values = form.getValues();
           var dataParams = this.normalizeData(values);
           //dataParams.traderType = 'G';
           
           var grid = Ext.ComponentQuery.query('#printcard-guide-printcard-grid')[0];
           var store = grid.getStore();

            dataParams.page = '1';
            dataParams.start = '0';
            dataParams.limit = store.pageSize;

             store.load({
                 params: dataParams,
                 callback: function(records, operation, success) {
                     // //console.log(success);
                 },
                 scope: this
             }); 
         }
         ,searchTourLeaderCard: function(btn)
         {
           var form = btn.up('form'),
           values = form.getValues();
           var dataParams = this.normalizeData(values);
           //dataParams.traderType = 'L';

           var grid = Ext.ComponentQuery.query('#printcard-tourleader-printcard-grid')[0];
           var store = grid.getStore();

            dataParams.page = '1';
            dataParams.start = '0';
            dataParams.limit = store.pageSize;

             store.load({
                 params: dataParams,
                 callback: function(records, operation, success) {
                     // //console.log(success);
                 },
                 scope: this
             }); 
         },
         normalizeData: function(dataObj)
         {
             // remove null data;
            for (field in dataObj) 
             {
               if (Ext.isEmpty(dataObj[field])) 
               {
                 delete dataObj[field];
               }
               
             }

             return dataObj;
         },
         handleActionColumn: function(column, action, grid, rowIndex, colIndex, model, node)
         {
        	 if(action){
      	        switch(action){
      	        
      	        case 'viewPrintCardguide':
	            //console.log(model);    
	        		var winInfo = Ext.create('tourism.view.information.guide.InfoGuideWindow',{
               		id: 'supervisor-information-guide-registration-infowindow',
               		animateTarget: column.el,
                     actionMode: '',
                     roleAction: '',
               	}).show();
	        		
               	winInfo.loadFormRecord(model);
	            break;
	                
	        	case 'viewPrintCardTourLeader':
	                
	        		var winInfo = Ext.create('tourism.view.information.tourleader.InfoTourleaderWindow',{
               		id: 'supervisor-information-tourleader-registration-infowindow',
               		animateTarget: column.el,
                     actionMode: '',
                     roleAction: '',
               	}).show();
               	
          
               	
               	winInfo.loadFormRecord(model);


	            break;
	            
	        	case 'pictureguideUpload':
		        	
		        	var winUpload = Ext.create('tourism.view.printcard.PrintCardUploadPicturePanel',{
		        		id: 'printcard-guide-picture-upload-window',

		        		roleAction: 'printcard',
	                    actionMode: 'upload',
	                    traderType: 'guide',
	                    grid: grid
		        	}).show();
		        	
		  
		        	
		        	// //console.log(model);
		        	winUpload.loadFormRecord(model);
		        	
		        break;
		        
		        case 'picturetourleaderUpload':
		        	
		        	var winUpload = Ext.create('tourism.view.printcard.PrintCardUploadPicturePanel',{
		        		id: 'printcard-tourleader-picture-upload-window',
//	                    animateTarget: column.el,
		        		roleAction: 'printcard',
	                    actionMode: 'upload',
	                    traderType: 'tourleader',
	                    grid: grid
		        	}).show();
		        	
		        	winUpload.loadFormRecord(model);
		        	
		        break;
		        
		        case 'takepicview':	//alert(model.get('traderType') )
              var url = 'business/printcard/registration/showTakePic'
                        // +model.get('printCardId')+'/'+model.get('licenseNo')
                        +'?printCardId='+model.get('printCardId')
                        +'&traderType='+model.get('traderType')
                        +'&identityNo='+model.get('identityNo')   
                        +'&licenseNo='+model.get('licenseNo')
                        +'&traderCategory='+model.get('traderCategory');
		        	window.open(url,"ถ่ายรูปมัคคุเทศก์","width=970,height=650");
		        	
		        break;
		        
		        case 'printCardGuide':
		        	// alert("sssss");
		        break;
      	        }
        	 }
      	        
         },
         printcardGuideExcel : function(btn)
         {
        	 var form = btn.up('form');
        	 
             var licenseNoFrom =  form.getForm().findField('licenseNoFrom').getValue();
             var licenseNoTo =  form.getForm().findField('licenseNoTo').getValue();
             var approveDateFrom =  form.getForm().findField('approveDateFrom').getRawValue();
             var approveDateTo =  form.getForm().findField('approveDateTo').getRawValue();
             var firstName =  form.getForm().findField('firstName').getValue();
             var lastName =  form.getForm().findField('lastName').getValue();
             var identityNo =  form.getForm().findField('identityNo').getValue();

             var traderCategory = '';
             if(!Ext.isEmpty(form.getForm().findField('traderCategory').getValue()))
             {
              traderCategory= form.getForm().findField('traderCategory').getValue();
             }
             var registrationType = '';
             if(!Ext.isEmpty(form.getForm().findField('registrationType').getValue()))
             {
              registrationType = form.getForm().findField('registrationType').getValue();
             }
             var printCardStatus = '';
             if(!Ext.isEmpty(form.getForm().findField('printCardStatus').getValue()))
             {
              printCardStatus = form.getForm().findField('printCardStatus').getValue();
             }
             var orgId = '';
             if(form.getForm().findField('orgId').getValue())
             {
                orgId =  form.getForm().findField('orgId').getValue();
             }
           
             //console.log("orgId " + orgId);
             var traderType = 'G';
         
             
             var reqUrl = "business/printcard/registration/printCardExcel?licenseNoFrom="+licenseNoFrom+"&licenseNoTo="
             +licenseNoTo+"&approveDateFrom="+approveDateFrom+"&approveDateTo="+approveDateTo+"&firstName="+firstName+"&lastName="
             +lastName+"&identityNo="+identityNo+"&registrationType="+registrationType+"&traderCategory="+traderCategory+"&printCardStatus="+printCardStatus
             +"&traderType="+traderType+"&orgId="+orgId;
             window.open(reqUrl,"พิมพ์บัตรมัคคุเทศน์","toolbar=0 width=900 height=600");

         },
         printcardTourLeaderExcel : function(btn)
         {
        	 var form = btn.up('form');
        	 
             var licenseNoFrom =  form.getForm().findField('licenseNoFrom').getValue();
             var licenseNoTo =  form.getForm().findField('licenseNoTo').getValue();
             var approveDateFrom =  form.getForm().findField('approveDateFrom').getRawValue();
             var approveDateTo =  form.getForm().findField('approveDateTo').getRawValue();
             var firstName =  form.getForm().findField('firstName').getValue();
             var lastName =  form.getForm().findField('lastName').getValue();
             var identityNo =  form.getForm().findField('identityNo').getValue();

              var traderCategory = '';
             if(!Ext.isEmpty(form.getForm().findField('traderCategory').getValue()))
             {
              traderCategory= form.getForm().findField('traderCategory').getValue();
             }
             var registrationType = '';
             if(!Ext.isEmpty(form.getForm().findField('registrationType').getValue()))
             {
              registrationType = form.getForm().findField('registrationType').getValue();
             }
             var printCardStatus = '';
             if(!Ext.isEmpty(form.getForm().findField('printCardStatus').getValue()))
             {
              printCardStatus = form.getForm().findField('printCardStatus').getValue();
             }
             var orgId = '';
             if(form.getForm().findField('orgId').getValue())
             {
                orgId =  form.getForm().findField('orgId').getValue();
             }

             var traderType = 'L';
             var reqUrl = "business/printcard/registration/printCardExcel?licenseNoFrom="+licenseNoFrom+"&licenseNoTo="
             +licenseNoTo+"&approveDateFrom="+approveDateFrom+"&approveDateTo="+approveDateTo+"&firstName="+firstName+"&lastName="
             +lastName+"&identityNo="+identityNo+"&registrationType="+registrationType+"&traderCategory="+traderCategory+"&printCardStatus="+printCardStatus
             +"&traderType="+traderType+"&orgId="+orgId;
             window.open(reqUrl,"พิมพ์บัตรมัคคุเทศน์","toolbar=0 width="+ screen.width +" height=600");
         }
});