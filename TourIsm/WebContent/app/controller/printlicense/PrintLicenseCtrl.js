Ext.define('tourism.controller.printlicense.PrintLicenseCtrl',{
	extend : 'Ext.app.Controller',

	stores: ['tourism.store.registration.RegistrationStore'],

    models: ['tourism.model.registration.RegistrationModel'],

    views: [
      'tourism.view.printlicense.PrintLicenseBusinessFormSearch',
      'tourism.view.printlicense.PrintLicenseGrid',
      'tourism.view.printlicense.PrintLicenseGuideGrid',
      'tourism.view.printlicense.PrintLicenseGuideFormSearch',
      'tourism.view.printlicense.PrintLicenseTourLeaderFormSearch',
      'tourism.view.printlicense.PrintLicenseBusinessWindow',
      'tourism.view.printlicense.PrintLicenseBusinessEditForm'
     // ,'Ext.ux.window.Notification'
    ],
    refs:[
         {
        	 ref:'printlicense-grid',
        	 selector:'printlicense-grid'
         },
         {
        	 ref:'printlicense-guide-grid',
        	 selector:'printlicense-guide-grid'
         },
         {
        	 ref:'#printlicense-business-printlicense-formsearch',
        	 selector:'panel' 
         },
         {
        	 ref:'#printlicense-guide-printlicense-formsearch',
        	 selector:'panel'
         },
         {
        	 ref:'#printlicense-tourleader-printlicense-formsearch',
        	 selector:'panel'
         },
         {
        	 ref:'printlicense-business-edit-form',
        	 selector:'panel'
         }
         
         ],
         
         init : function (application) {

             this.control({
            	  	'#printlicense-business-printlicense-formsearch button[action=searchLicenseBetweenRegistration]':{
            	  		click: this.searchBusinessLicense
            	 } ,
            	 
                	'#printlicense-guide-printlicense-formsearch button[action=searchLicenseBetweenRegistration]':{
                		click: this.searchGuideLicense
                 },
                 	'#printlicense-business-printlicense-grid actioncolumn':{
                 		itemclick: this.handleActionColumn
                 },
                 	'printlicense-business-edit-form button[action=printlicensePDF]':{
                 		click: this.printLicensePDF
                 },
              		'#printlicense-guide-printlicense-grid actioncolumn':{
              		itemclick: this.handleActionColumn
              	 },
                 
                 //Oat Add
                 '#printlicense-business-printlicense-formsearch button[action=printReportPrintLicense]':{
         	  		click: this.printReportPrintLicenseBusiness
                 },
                 '#printlicense-guide-printlicense-formsearch button[action=printReportPrintLicense]':{
          	  		click: this.printReportPrintLicenseGuide
                  } 
                 //End Oat Add
             });
         },
         //Oat Add
         printReportPrintLicenseBusiness: function(btn)
         {

        	 var form = btn.up('form');
        	 
        	 values = form.getValues();

             var dataParams = this.normalizeData(values);
             dataParams.traderType = 'B';
             
             var registrationNo = typeof(dataParams.registrationNo)=="undefined"?"":dataParams.registrationNo;
             var licenseNo = typeof(dataParams.licenseNo)=="undefined"?"":dataParams.licenseNo;
             var approveDateFrom = typeof(dataParams.approveDateFrom)=="undefined"?"":dataParams.approveDateFrom;
             var traderName = typeof(dataParams.traderName)=="undefined"?"":dataParams.traderName;
             var firstName = typeof(dataParams.firstName)=="undefined"?"":dataParams.firstName;
             var registrationType = typeof(dataParams.registrationType)=="undefined"?"":dataParams.registrationType;
             var approveDateTo = typeof(dataParams.approveDateTo)=="undefined"?"":dataParams.approveDateTo;
             var traderNameEn = typeof(dataParams.traderNameEn)=="undefined"?"":dataParams.traderNameEn;
             var traderCategory = typeof(dataParams.traderCategory)=="undefined"?"":dataParams.traderCategory;
             var printLicenseStatus = typeof(dataParams.printLicenseStatus)=="undefined"?"":dataParams.printLicenseStatus;
             var traderType = typeof(dataParams.traderType)=="undefined"?"":dataParams.traderType;
        	 
        	 var reqUrl = "business/printlicense/registration/reportprintlicenseExcel?"
        		 +"registrationNo="+registrationNo
        		 +"&licenseNo="+licenseNo
        		 +"&approveDateFrom="+approveDateFrom
        	 	 +"&traderName="+traderName
        	 	 +"&firstName="+firstName
        	 	 +"&registrationType="+registrationType
        	 	 +"&approveDateTo="+approveDateTo
        	 	 +"&traderNameEn="+traderNameEn
        	 	 +"&traderCategory="+traderCategory
        	 	 +"&printLicenseStatus="+printLicenseStatus
        	 	 +"&traderType="+traderType;
  
                 window.open(reqUrl,"พิมพ์รายงาน","toolbar=0 width=900 height=600");

         },
         
         printReportPrintLicenseGuide: function(btn)
         {
        	 var form = btn.up('form'); 

        	 values = form.getValues();

             var dataParams = this.normalizeData(values);
             dataParams.traderType = 'G';
             
             var registrationNo = typeof(dataParams.registrationNo)=="undefined"?"":dataParams.registrationNo;
             var licenseNo = typeof(dataParams.licenseNo)=="undefined"?"":dataParams.licenseNo;
             var approveDateFrom = typeof(dataParams.approveDateFrom)=="undefined"?"":dataParams.approveDateFrom;
             var firstName = typeof(dataParams.firstName)=="undefined"?"":dataParams.firstName;
             var identityNo = typeof(dataParams.identityNo)=="undefined"?"":dataParams.identityNo;
             var registrationType = typeof(dataParams.registrationType)=="undefined"?"":dataParams.registrationType;
             var approveDateTo = typeof(dataParams.approveDateTo)=="undefined"?"":dataParams.approveDateTo;
             var lastName = typeof(dataParams.lastName)=="undefined"?"":dataParams.lastName;
             var traderCategory = typeof(dataParams.traderCategory)=="undefined"?"":dataParams.traderCategory;
             var printLicenseStatus = typeof(dataParams.printLicenseStatus)=="undefined"?"":dataParams.printLicenseStatus;
             var traderType = typeof(dataParams.traderType)=="undefined"?"":dataParams.traderType;
        	 
        	 var reqUrl = "business/printlicense/registration/reportprintlicenseGuideExcel?"
        		 +"registrationNo="+registrationNo
        		 +"&licenseNo="+licenseNo
        		 +"&approveDateFrom="+approveDateFrom
        	 	 +"&firstName="+firstName
        	 	 +"&identityNo="+identityNo
        	 	 +"&registrationType="+registrationType
        	 	 +"&approveDateTo="+approveDateTo
        	 	 +"&lastName="+lastName
        	 	 +"&traderCategory="+traderCategory
        	 	 +"&printLicenseStatus="+printLicenseStatus
        	 	 +"&traderType="+traderType;
  
                 window.open(reqUrl,"พิมพ์รายงาน","toolbar=0 width=900 height=600");

         },
         //End Oat Add
         
         searchBusinessLicense: function(btn)
         {

           var form = btn.up('form'),
           values = form.getValues();

           var dataParams = this.normalizeData(values);
           dataParams.traderType = 'B';

           var grid = Ext.ComponentQuery.query('#printlicense-business-printlicense-grid')[0];
           var store = grid.getStore();
           
            dataParams.page = '1';
            dataParams.start = '0';
            dataParams.limit = store.pageSize;
           
             store.load({
                 params: dataParams,
                 callback: function(records, operation, success) {
                    console.log(records);
                 },
                 scope: this
             }); 

         }
         ,searchGuideLicense: function(btn)
         {
           var form = btn.up('form'),
           values = form.getValues();
           var dataParams = this.normalizeData(values);
           dataParams.traderType = 'G';

           var grid = Ext.ComponentQuery.query('#printlicense-guide-printlicense-grid')[0];
           var store = grid.getStore();

            dataParams.page = '1';
            dataParams.start = '0';
            dataParams.limit = store.pageSize;

             store.load({
                 params: dataParams,
                 callback: function(records, operation, success) {
                     // //console.log(success);
                 },
                 scope: this
             }); 
         }
         ,searchTourLeaderLicense: function(btn)
         {
           var form = btn.up('form'),
           values = form.getValues();
           var dataParams = this.normalizeData(values);
           dataParams.traderType = 'L';

           var grid = Ext.ComponentQuery.query('#printlicense-tourleader-printlicense-grid')[0];
           var store = grid.getStore();

            dataParams.page = '1';
            dataParams.start = '0';
            dataParams.limit = store.pageSize;

             store.load({
                 params: dataParams,
                 callback: function(records, operation, success) {
                     // //console.log(success);
                 },
                 scope: this
             }); 
         },
         normalizeData: function(dataObj)
         {
             // remove null data;
            for (field in dataObj) 
             {
               if (Ext.isEmpty(dataObj[field])) 
               {
                 delete dataObj[field];
               }
               
             }

             return dataObj;
         },
         handleActionColumn: function(column, action, grid, rowIndex, colIndex, model, node)
         {
        	 if(action){
     	        switch(action){
     	        
     	        	case 'viewPrintlicenseBusiness':
                     
                       var winInfo = Ext.create('tourism.view.information.business.InfoBusinessWindow',{
                         id: 'business-information-business-printlicense-infowindow',
                         animateTarget: column.el,
                         actionMode: 'information',
                         roleAction: '',
                       });
                       
                       winInfo.show();

                       winInfo.loadFormRecord(model);
                     break;
                     
     	        	case 'viewPrintlicenseguide':
     	                
     	        		var winInfo = Ext.create('tourism.view.information.guide.InfoGuideWindow',{
                     		id: 'supervisor-information-guide-registration-infowindow',
                     		animateTarget: column.el,
                           actionMode: 'information',
                           roleAction: '',
                     	}).show();
  
                     	winInfo.loadFormRecord(model);
     	            break;
     	                
     	        	case 'viewPrintlicensetourleader':
     	                
     	        		var winInfo = Ext.create('tourism.view.information.tourleader.InfoTourleaderWindow',{
                     		id: 'supervisor-information-tourleader-registration-infowindow',
                     		animateTarget: column.el,
                           actionMode: 'information',
                           roleAction: '',
                     	}).show();
                     	
                     	//console.log(model);
                     	
                     	winInfo.loadFormRecord(model);
     	            break;
     	                
     	        	case 'printLicenseBusiness':
     	        	
     	        		var printLicenseId =  model.get('printLicenseId');
     	        	
                        var reqUrl = "business/printlicense/registration/readform/"+printLicenseId;
                        window.open(reqUrl,"พิมพ์ใบอนุญาต","toolbar=0 width=1000 height=600");

     	        	break;
     	        	
     	        	case 'priceLicenseGuide':
     	        		var printLicenseId =  model.get('printLicenseId');
     	        	
                        var reqUrl = "business/printlicense/registration/readform/"+printLicenseId;
                        window.open(reqUrl,"พิมพ์ใบอนุญาต","toolbar=0 width=1000 height=600");

     	        	break;
     	        	
     	        	case 'approvalprogress':

                    	console.log(model.get('regId'));
     	        		
                        var progressWin = Ext.create('tourism.view.registrationprogress.RegistrationProgressWindow',{
                          id: 'license-registrationprogress-ProgressWindow',
                          animateTarget: column.el 
                          
                         });

                        progressWin.on('show', function(win){

                          var grid = win.down('registrationprogress-grid');
                       
                          grid.getStore().load({
                            params: {regId: model.get('regId')},
                            callback: function(records, operation, success) {
                                
                            },
                            scope: this
                          }); 

                        },this);

                        progressWin.show();

                      break;
     	        }
     		}
         },
         printLicensePDF: function(btn)
         {
        	/* Ext.Ajax.request({
        			url :'business/printlicense/registration/print',
        			method : 'GET',
        			//params : params,
        			isUpload:true,
        			success : function(response, option){
        				//this.gestioneRisposta(response);
        			      //console.log("success");
        			}
        			,failure :function(response, option){
        				//console.log("failure");
        			}
        			,scope : this
        		});*/
        	 window.open("form.jsp");
        	 var form = btn.up('form');
        	 values = form.getValues();
        	 this.normalizeData(values);
        	 var licenseNo = form.getForm().findField('licenseNo').getValue();
        	 var prefixName = form.getForm().findField('prefixId').getRawValue();
        	 var firstName = form.getForm().findField('firstName').getValue();
        	 var postfixName = form.getForm().findField('postfixName').getValue();
        	 var identityNo = form.getForm().findField('identityNo').getValue();
        	 var traderCategoryName = form.getForm().findField('traderCategoryName').getValue();
        	 var traderName = form.getForm().findField('traderName').getValue();
        	 var traderNameEn = form.getForm().findField('traderNameEn').getValue();
        	 var pronunciationName = form.getForm().findField('pronunciationName').getValue();
        	 var addressNo = form.getForm().findField('addressNo').getValue();
        	 var soi = form.getForm().findField('soi').getValue();
        	 var tambolName = form.getForm().findField('tambolId').getRawValue();
        	 var provinceName = form.getForm().findField('provinceId').getRawValue();
        	 var amphurName = form.getForm().findField('amphurId').getRawValue();
        	 var roadName = form.getForm().findField('roadName').getValue();
        	 var postCode = form.getForm().findField('postCode').getValue();
        	 
        	 
        	 window.open("business/printlicense/registration/print?licenseNo="+licenseNo+"&prefixName="
        	+prefixName+"&firstName="+firstName+"&postfixName="+postfixName+"&identityNo="+identityNo
        	+"&traderCategoryName="+traderCategoryName+"&traderName="+traderName+"&traderNameEn="+traderNameEn
        	+"&pronunciationName="+pronunciationName+"&addressNo="+addressNo+"&soi="+soi+"&tambolName="+tambolName
        	+"&provinceName="+provinceName+"&amphurName="+amphurName+"&roadName="+roadName+"&postCode="+postCode);
        	 

        	 
         }
});