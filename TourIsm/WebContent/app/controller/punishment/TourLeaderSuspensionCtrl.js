Ext.define('tourism.controller.punishment.TourLeaderSuspensionCtrl', {
  extend : 'Ext.app.Controller',
    refs: [

        {
            ref: '#punishment-suspension-tourleader-grid',
            selector: '#punishment-suspension-tourleader-grid'
        }
        ,{
            ref: '#suspend-alertWindow',
            selector: 'panel'
        },
        {
            ref: '#punishment-suspension-tourleader-formsearch',
            selector: 'panel'
        }
        ,
        {
            ref: '#punishment-suspension-tourleader-searchlicensepanel',
            selector: 'panel'
        }
        ,
        {
            ref: '#punishment-suspension-tourleader-formaddedit',
            selector: 'panel'
        }
        ,
        {
            ref: '#punishment-suspension-tourleader-alertform',
            selector: 'panel'
        }
        ,{
            ref: '#punishment-suspension-tourleader-evidence-grid',
            selector: '#punishment-suspension-tourleader-evidence-grid'
        }
        ,{
            ref: '#punishment-suspension-tourleader-evidence-form',
            selector: 'panel'
        }
        
       
    ],   
  init : function (application) {

        this.control({
                       
            '#punishment-suspension-tourleader-searchlicensepanel button[action=selectLicense]': {
                click: this.selectLicenseNo
            },
            '#punishment-suspension-tourleader-formaddedit button[action=addSuspension]': {
                click: this.addSuspension
            }
            ,
            '#punishment-suspension-tourleader-formaddedit button[action=approveSuspension]': {
                click: this.addSuspension
            }
            ,
            '#punishment-suspension-tourleader-grid button[action=addpunishment]': {
                click: this.addpunishment
            },
            '#punishment-suspension-tourleader-grid actioncolumn': {
                itemclick: this.handleActionColumn
            },
             '#punishment-suspension-tourleader-alertform button[action=savePostAlert]': {
                click: this.savePostAlert
            },
            '#punishment-suspension-tourleader-formsearch button[action=searchSuspend]': {
                click: this.searchSuspend
            }
            ,'#punishment-suspension-tourleader-evidence-grid button[action=addevidence]': {
                click: this.addevidence
            }
            ,'#punishment-suspension-tourleader-evidence-grid actioncolumn': {
                itemclick: this.evidenceHandleActionColumn
            }
            ,'#punishment-suspension-tourleader-evidence-form button[action=saveEvidence]': {
                click: this.saveEvidence
            }
            
        });

    
  },
  addpunishment: function(btn){


    var win = Ext.create('tourism.view.punishment.suspension.AddSuspensionWindow',{
      traderType: 'L',
      traderTypeName: 'tourleader',
      roleAction: 'suspension'
    });
    win.show();

    var form = win.down('info-license-formsearch');

    form.getForm().findField('traderType').setValue('L');
  },
  selectLicenseNo: function(btn)
  {
    var win = btn.up('window');
    var grid = win.down('info-license-grid');
    var model = grid.getSelectionModel().getSelection()[0];

    var formSuspend = win.down('#punishment-suspension-tourleader-formaddedit');

    var actGrid = win.down('punishment-act-grid');

    if(Ext.isEmpty(model))
    {
      Ext.Msg.alert('เกิดข้อผิดพลาด', 'กรุณาเลือกข้อมูล');
      return 
    }

    // fsubmit.loadRecord(model);

    actGrid.getStore().load({
        params: {traderType: model.get('traderType')},
        callback: function(records, operation, success) {
            // //console.log(success);
        },
        scope: this
    }); 

//evidence
  var tabPanel = formSuspend.down('tabpanel');

  tabPanel.remove('punishment-suspension-tourleader-evidence-grid');

   formSuspend.load({
        url: 'punishment/suspension/tourleader/prepare',
        method : 'POST',
        waitMsg: 'กรุณารอสักครู่..',
        params: {traderId: model.get('traderId')},
        success: function(form, action) {

          var dataObj = action.result.data;

          var suspendModel = Ext.create('tourism.model.punishment.SuspensionModel');

          suspendModel.set(dataObj);
          suspendModel.set('suspendPeriod', 6);

          formSuspend.loadRecord(suspendModel);

        },
        failure: function(form, action) {
          Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
        },
        scope: formSuspend // this = fsubmit
    });

   // เปิดปุ่มบันทึกปุ่มเดียว
    formSuspend.down('#punishment-suspension-tourleader-addSuspension').setVisible(true);

    formSuspend.down('#punishment-suspension-tourleader-approveSuspension').setVisible(false);
    formSuspend.down('#punishment-suspension-tourleader-printSuspension').setVisible(false);
    formSuspend.down('#punishment-suspension-tourleader-suspendStatus-checkboxgroup').setVisible(false);


    var layout = win.getLayout();
    layout.setActiveItem(1);

  },
  addSuspension: function(btn)
  {
    var win = btn.up('window');
    var actionMode = win.actionMode;
    var actGrid = win.down('punishment-act-grid');
    var listActModel = new Array();

    actGrid.getStore().each(function(model){

        //console.log(model.get('active'));
        if(model.get('active'))
        {
            listActModel.push({masActId: model.get('masActId')});
        }
        
    });

    if(listActModel.length < 0)
    {
        Ext.Msg.alert('เกิดข้อผิดพลาด', 'สาหตุการพักใช้');
        return;
    }


    var formSuspend = win.down('#punishment-suspension-tourleader-formaddedit');

      if (!formSuspend.isValid()) {

          var me = formSuspend,
          errorCmp, fields, errors;

          fields = me.getForm().getFields();
          errors = '';
          fields.each(function(field) {
              Ext.Array.forEach(field.getErrors(), function(error) {
                  // errors.push({name: field.getFieldLabel(), error: error});
                  errors += field.getFieldLabel()  +'<br>';
              });
          });
          // Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', errors);

          Ext.Msg.show({
             title:'กรุณาตรวจสอบข้อมูล',
             msg: errors,
             buttons: Ext.Msg.OK,
             icon: Ext.Msg.ERROR
          });
          return;
      }



        var actStr =  Ext.encode(listActModel);


        
        var values = formSuspend.getValues();


        var dataParams = this.normalizeData(values);

        dataParams.masAct = actStr;


         // fsubmit.submit({
       formSuspend.load({
            url: 'punishment/suspension/tourleader/addsuspension',
            method : 'POST',
            waitMsg: 'กรุณารอสักครู่...',
            params: dataParams,
            success: function(form, action) {

              var dataObj = action.result.data;
             
              // Ext.Msg.alert('', 'บันทึกข้อมูล');

            var noti = Ext.create('widget.uxNotification', {
              // title: 'Notification',
              position: 'tr',
              manager: 'instructions',
              // cls: 'ux-notification-light',
              // iconCls: 'ux-notification-icon-information',
              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
              closable: false,
              autoCloseDelay: 4000,
              width: 300,
              slideBackDuration: 500,
              slideInAnimation: 'bounceOut',
              slideBackAnimation: 'easeIn'
            });
            noti.show();

              var grid = Ext.ComponentQuery.query('#punishment-suspension-tourleader-grid')[0];
              var store = grid.getStore();
              //evidence
              if(actionMode == 'edit')
              {
                store.reload();
              }
              else
              {
                store.load({
                  params: {suspendId: dataObj.suspendId},
                  callback: function(records, operation, success) {
                     
                     
                  },
                  scope: this
                });
              }
             
              win.close();
        
            },
            failure: function(form, action) {
              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
            },
            scope: this 
        });

  },
  normalizeData: function(dataObj)
  {
      // remove null data;
     for (field in dataObj) 
      {
        if (Ext.isEmpty(dataObj[field])) 
        {
          delete dataObj[field];
        }
        
      }

      return dataObj;
  }
  ,handleActionColumn : function(column, action, grid, rowIndex, colIndex, model, node) {
     if(action){
          switch(action){
            case 'punishmentdetail':

              var win = Ext.create('tourism.view.punishment.suspension.AddSuspensionWindow',{
                  traderType: 'L',
                  traderTypeName: 'tourleader',
                  roleAction: 'suspension',
                  actionMode:'edit'
              });
              win.show();

              var layout = win.getLayout();
              layout.setActiveItem(1);

              var formSuspend = win.down('#punishment-suspension-tourleader-formaddedit');

              var actGrid = win.down('punishment-act-grid');


              actGrid.getStore().load({
                  params: {suspendId: model.get('suspendId'),traderType: model.get('traderType')},
                  callback: function(records, operation, success) {
                     
                     
                  },
                  scope: this
              }); 

              formSuspend.loadRecord(model);
              
              if(model.get('suspendStatus') == 'W')
              {
                
                formSuspend.down('#punishment-suspension-tourleader-approveSuspension').setVisible(false);
                formSuspend.down('#punishment-suspension-tourleader-addSuspension').setVisible(true);
                formSuspend.down('#punishment-suspension-tourleader-printSuspension').setVisible(true);
              }

              if(model.get('suspendStatus') == 'A')
              {
                formSuspend.down('#punishment-suspension-tourleader-approveSuspension').setVisible(false);
                formSuspend.down('#punishment-suspension-tourleader-addSuspension').setVisible(false);
                formSuspend.down('#punishment-suspension-tourleader-printSuspension').setVisible(false);

                formSuspend.down('#punishment-suspension-tourleader-suspendStatus-checkboxgroup').setVisible(false);
              }

              if(model.get('suspendStatus') == 'S')
              {
                formSuspend.down('#punishment-suspension-tourleader-approveSuspension').setVisible(false);
                formSuspend.down('#punishment-suspension-tourleader-addSuspension').setVisible(false);
                formSuspend.down('#punishment-suspension-tourleader-printSuspension').setVisible(false);

                formSuspend.down('#punishment-suspension-tourleader-suspendStatus-checkboxgroup').setVisible(false);
              }





              // evidence
              var evidencegrid = Ext.ComponentQuery.query('#punishment-suspension-tourleader-evidence-grid')[0];
              

              evidencegrid.getStore().load({
                  params: {suspendId: model.get('suspendId'),traderType: model.get('traderType')},
                  callback: function(records, operation, success) {

                  },
                  scope: this
              }); 

            break;

            case 'viewpunishmentdetail':
              var win = Ext.create('tourism.view.punishment.suspension.AddSuspensionWindow',{
                  traderType: 'L',
                  traderTypeName: 'tourleader',
                  roleAction: 'suspension',
                  actionMode:'edit'
              });
              win.show();

              var layout = win.getLayout();
              layout.setActiveItem(1);

              var formSuspend = win.down('#punishment-suspension-tourleader-formaddedit');

              var actGrid = win.down('punishment-act-grid');


              actGrid.getStore().load({
                  params: {suspendId: model.get('suspendId'),traderType: model.get('traderType')},
                  callback: function(records, operation, success) {
                     
                     
                  },
                  scope: this
              }); 

              formSuspend.loadRecord(model);

              formSuspend.down('#punishment-suspension-tourleader-addSuspension').setVisible(false);
              formSuspend.down('#punishment-suspension-tourleader-approveSuspension').setVisible(false);
              formSuspend.down('#punishment-suspension-tourleader-printSuspension').setVisible(false);

              formSuspend.down('#punishment-suspension-tourleader-suspendStatus-checkboxgroup').setVisible(false);

              // evidence
              var evidencegrid = Ext.ComponentQuery.query('#punishment-suspension-tourleader-evidence-grid')[0];
              

              evidencegrid.getStore().load({
                  params: {suspendId: model.get('suspendId'),traderType: model.get('traderType')},
                  callback: function(records, operation, success) {

                  },
                  scope: this
              }); 
            break;

            case 'suspendAlert':

              var winAlert = Ext.create('tourism.view.punishment.alert.AlertWindow',{
                id: 'punishment-suspension-tourleader-alertWindow',
                  traderType: 'L',
                  traderTypeName: 'tourleader',
                  roleAction: 'suspension'
              });
              winAlert.show();

              var alertform = winAlert.down('punishment-alert-alertform');

              if(model.get('suspendStatus') != 'A')
              {
                alertform.down('#punishment-suspension-tourleader-savePostAlert').setVisible(false);
              }

              alertform.load({
                  url: 'punishment/suspension/tourleader/alert/read',
                  method : 'POST',
                  waitMsg: 'กรุณารอสักครู่...',
                  params: {suspendId: model.get('suspendId')},
                  success: function(form, action) {


                    var dataObj = action.result.data;

                    var alertModel = Ext.create('tourism.model.punishment.SuspensionAlertModel');

                    alertModel.set(dataObj);
             
              
                  },
                  failure: function(form, action) {
                    Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
                  },
                  scope: this 
              });
            break;

            case 'viewsuspendAlert':

               var winAlert = Ext.create('tourism.view.punishment.alert.AlertWindow',{
                id: 'punishment-suspension-tourleader-alertWindow',
                  traderType: 'L',
                  traderTypeName: 'tourleader',
                  roleAction: 'suspension'
              });
              winAlert.show();

              var alertform = winAlert.down('punishment-alert-alertform');

              alertform.down('#punishment-suspension-tourleader-savePostAlert').setVisible(false);

              alertform.load({
                  url: 'punishment/suspension/tourleader/alert/read',
                  method : 'POST',
                  waitMsg: 'กรุณารอสักครู่...',
                  params: {suspendId: model.get('suspendId')},
                  success: function(form, action) {


                    var dataObj = action.result.data;

                    var alertModel = Ext.create('tourism.model.punishment.SuspensionAlertModel');

                    alertModel.set(dataObj);
             
              
                  },
                  failure: function(form, action) {
                    Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
                  },
                  scope: this 
              });
            break;

            case 'cancelsuspension':
              Ext.Msg.prompt('สาเหตุการยกเลิก', 'สาเหตุการยกเลิก:', function(btn, text){


                  if (btn == 'ok'){

                      if(Ext.isEmpty(text))
                      {
                        Ext.Msg.alert('เกิดข้อผิดพลาด', 'กรุรากรอกข้อมูล สาเหตุการยกเลิก');
                        return;
                      }
                      var formcancel = Ext.create('Ext.form.Panel');



                        formcancel.load({
                          url: 'punishment/suspension/tourleader/cancelsuspend',
                          method : 'POST',
                          waitMsg: 'กรุณารอสักครู่...',
                          params: {suspendId: model.get('suspendId'),cancelRemark:text},
                          success: function(form, action) {


                          var grid = Ext.ComponentQuery.query('#punishment-suspension-tourleader-grid')[0];
                          var store = grid.getStore();

                           store.reload();
                     
                      
                          },
                          failure: function(form, action) {
                            Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
                          },
                          scope: this 
                      });   
                  }
              });

              // Ext.MessageBox.confirm('ยืนยัน', 'คุรต้องการยกเลิกการพักใช้นี้', function(btn){
              //   //console.log(btn);
              //      if (btn == 'yes'){
                                            
              //       }
              // });


            break;
          }
      }
  }
  ,savePostAlert: function(btn)
  {
    var winAlert = btn.up('window');
    var alertform = winAlert.down('punishment-alert-alertform');

    if (!alertform.isValid()) {

        var me = alertform,
        errorCmp, fields, errors;

        fields = me.getForm().getFields();
        errors = '';
        fields.each(function(field) {
            Ext.Array.forEach(field.getErrors(), function(error) {
                // errors.push({name: field.getFieldLabel(), error: error});
                errors += field.getFieldLabel()  +'<br>';
            });
        });
        // Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', errors);

        Ext.Msg.show({
           title:'กรุณาตรวจสอบข้อมูล',
           msg: errors,
           buttons: Ext.Msg.OK,
           icon: Ext.Msg.ERROR
        });
        return;
    }

    var values = alertform.getValues();

    var dataParams = this.normalizeData(values);

    alertform.load({
      url: 'punishment/suspension/tourleader/suspension',
      method : 'POST',
      waitMsg: 'กรุณารอสักครู่...',
      params: dataParams,
      success: function(form, action) {


            var grid = Ext.ComponentQuery.query('#punishment-suspension-tourleader-grid')[0];
            var store = grid.getStore();

            store.reload();
           
            winAlert.close();

 
  
      },
      failure: function(form, action) {
        Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
      },
      scope: this 
    });
  },
  searchSuspend: function(btn)
  {
    var formSearch = btn.up('form');
    var values = formSearch.getValues();

    var dataParams = this.normalizeData(values);

    var grid = Ext.ComponentQuery.query('#punishment-suspension-tourleader-grid')[0];

    dataParams.page = '1';
    dataParams.start = '0';
    dataParams.limit = grid.getStore().pageSize;

    grid.getStore().load({
        params: dataParams,
        callback: function(records, operation, success) {
   
        },
        scope: this
    });
  }
  ,addevidence: function(btn)
  {
    var mainWin = btn.up('window');
    var formSuspend = mainWin.down('#punishment-suspension-tourleader-formaddedit');

    var licenseNo = formSuspend.getForm().findField('licenseNo').getValue();
    var suspendId = formSuspend.getForm().findField('suspendId').getValue();
    

    var win = Ext.create('tourism.view.punishment.evidence.EvidenceWindow',{
      traderType: 'L',
      traderTypeName: 'tourleader',
      roleAction: 'suspension'
    });
    win.show();

    var form = win.down('punishment-evidence-form');

    form.getForm().findField('traderType').setValue('L');
    form.getForm().findField('licenseNo').setValue(licenseNo);
    form.getForm().findField('suspendId').setValue(suspendId);
    form.getForm().findField('revokeId').setValue(0);
    form.getForm().findField('punishmentType').setValue('S');

    


  }
  ,saveEvidence: function(btn)
  {
    var win = btn.up('window');
    var formEvidence = btn.up('form');

     if (!formEvidence.isValid()) {

          var me = formEvidence,
          errorCmp, fields, errors;

          fields = me.getForm().getFields();
          errors = '';
          fields.each(function(field) {
              Ext.Array.forEach(field.getErrors(), function(error) {
                  // errors.push({name: field.getFieldLabel(), error: error});
                  errors += field.getFieldLabel()  +'<br>';
              });
          });
          // Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', errors);

          Ext.Msg.show({
             title:'กรุณาตรวจสอบข้อมูล',
             msg: errors,
             buttons: Ext.Msg.OK,
             icon: Ext.Msg.ERROR
          });
          return;
      }


         // fsubmit.submit({
       formEvidence.submit({
            url: 'punishment/suspension/evidence/save',
            method : 'POST',
            waitMsg: 'กรุณารอสักครู่...',
          
            success: function(form, action) {

              var dataObj = action.result.data;
             
              // Ext.Msg.alert('', 'บันทึกข้อมูล');

            var noti = Ext.create('widget.uxNotification', {
              // title: 'Notification',
              position: 'tr',
              manager: 'instructions',
              // cls: 'ux-notification-light',
              // iconCls: 'ux-notification-icon-information',
              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
              closable: false,
              autoCloseDelay: 4000,
              width: 300,
              slideBackDuration: 500,
              slideInAnimation: 'bounceOut',
              slideBackAnimation: 'easeIn'
            });
            noti.show();

              var grid = Ext.ComponentQuery.query('#punishment-suspension-tourleader-evidence-grid')[0];
              var store = grid.getStore();

              store.reload();
             
              win.close();
        
            },
            failure: function(form, action) {
              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
            },
            scope: this 
        });    
  },
  evidenceHandleActionColumn: function(column, action, grid, rowIndex, colIndex, model, node)
  {
     if(action){
          switch(action){
            case 'viewevidence':
              window.open('punishment/suspension/evidence/viewfile?punishmentId='+model.get('punishmentId'),"Pic","width=600,height=600");
            break;

            case 'deleteevidence':
              var grid = Ext.ComponentQuery.query('#punishment-suspension-tourleader-evidence-grid')[0];
              grid.getStore().remove(model);
              grid.getStore().sync();
            break;
          }
      }
  }
 
});