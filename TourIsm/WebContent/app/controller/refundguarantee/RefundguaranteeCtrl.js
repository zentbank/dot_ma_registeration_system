Ext.define('tourism.controller.refundguarantee.RefundguaranteeCtrl', {
	extend : 'Ext.app.Controller',
    refs: [

        {
            ref: 'refundguarantee-grid',
            selector: 'refundguarantee-grid'
        }
        ,
        {
            ref: 'refundguarantee-formsearch',
            selector: 'panel'
        }   
        ,
        {
            ref: 'refundguarantee-addeditform',
            selector: 'panel'
        }     
        
    ],	 
	init : function (application) {

        this.control({
        
            'refundguarantee-grid actioncolumn': {
                itemclick: this.handleActionColumn
            },
            'refundguarantee-formsearch button[action=searchRefundGuarantee]': {
                click: this.searchRefundGuarantee
            }
            ,
            'refundguarantee-addeditform button[action=saveRefundGuarantee]': {
                click: this.saveRefundGuarantee
            }

            
        });

		
	},
  searchRefundGuarantee: function(btn)
  {
    var formSearch = btn.up('form');
    var values = formSearch.getValues();

    var dataParams = this.normalizeData(values);



    var grid = Ext.ComponentQuery.query('refundguarantee-grid')[0];

     dataParams.page = '1';
     dataParams.start = '0';
     dataParams.limit = grid.getStore().pageSize;

    grid.getStore().load({
        params: dataParams,
        callback: function(records, operation, success) {
   
        },
        scope: this
    });
  }
  ,handleActionColumn: function(column, action, grid, rowIndex, colIndex, model, node)
  {
      var win = Ext.create('tourism.view.refundguarantee.RefundGuaranteeWindow');
      win.show();

      var form = win.down('refundguarantee-addeditform');
      form.loadRecord(model);
  }
  ,  normalizeData: function(dataObj)
  {
      // remove null data;
     for (field in dataObj) 
      {
        if (Ext.isEmpty(dataObj[field])) 
        {
          delete dataObj[field];
        }
        
      }

      return dataObj;
  }
  ,saveRefundGuarantee: function(btn)
  {
    var win = btn.up('window');

    var formSuspend = win.down('refundguarantee-addeditform');

      if (!formSuspend.isValid()) {

          var me = formSuspend,
          errorCmp, fields, errors;

          fields = me.getForm().getFields();
          errors = '';
          fields.each(function(field) {
              Ext.Array.forEach(field.getErrors(), function(error) {
                  // errors.push({name: field.getFieldLabel(), error: error});
                  errors += field.getFieldLabel()  +'<br>';
              });
          });
          // Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', errors);

          Ext.Msg.show({
             title:'กรุณาตรวจสอบข้อมูล',
             msg: errors,
             buttons: Ext.Msg.OK,
             icon: Ext.Msg.ERROR
          });
          return;
      }
        
        var values = formSuspend.getValues();


        var dataParams = this.normalizeData(values);

         // fsubmit.submit({
       formSuspend.load({
            url: 'business/refundguarantee/save',
            method : 'POST',
            waitMsg: 'กรุณารอสักครู่..',
            params: dataParams,
            success: function(form, action) {

              var dataObj = action.result.data;
             
              // Ext.Msg.alert('', 'บันทึกข้อมูล');

            var noti = Ext.create('widget.uxNotification', {
              // title: 'Notification',
              position: 'tr',
              manager: 'instructions',
              // cls: 'ux-notification-light',
              // iconCls: 'ux-notification-icon-information',
              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
              closable: false,
              autoCloseDelay: 4000,
              width: 300,
              slideBackDuration: 500,
              slideInAnimation: 'bounceOut',
              slideBackAnimation: 'easeIn'
            });
            noti.show();

              var grid = Ext.ComponentQuery.query('refundguarantee-grid')[0];
              var store = grid.getStore();
              store.reload();
             
              win.close();
        
            },
            failure: function(form, action) {
              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
            },
            scope: this 
        });
  }
 
});