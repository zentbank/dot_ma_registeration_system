Ext.define('tourism.controller.registration.DirectorCtrl', {
	extend : 'Ext.app.Controller',

   	stores: ['tourism.store.registration.RegistrationStore'],

    models: ['tourism.model.registration.RegistrationModel'],

    views: [
      'tourism.view.approvalprocess.ApprovalProcessGrid',
      // 'tourism.view.approvalprocess.ApprovalProcessFormSearch',
      'tourism.view.approvalprocess.ApprovalProcessBusinessFormSearch',
      'tourism.view.approvalprocess.ApprovalProcessGuideFormSearch',
      'tourism.view.approvalprocess.ApprovalProcessTourleaderFormSearch',
      'tourism.view.window.verification.VerificationForm',
      'tourism.view.registrationprogress.RegistrationProgressWindow'
    ],

    refs: [    
    	 // {  
      //       ref: '#director-approvalprocess-grid',
      //       selector: '#director-approvalprocess-grid'
      //   } 
       
      //   ,{
      //       ref: '#director-approvalprocess-formsearch',
      //       selector: 'panel'
      //   }
      //   ,{
      //       ref: '#director-registration-verification-form',
      //       selector: 'panel'
      //   }

       {  
            ref: '#director-business-approvalprocess-grid',
            selector: '#director-business-approvalprocess-grid'
        } 
         ,{  
            ref: '#director-guide-approvalprocess-grid',
            selector: '#director-guide-approvalprocess-grid'
        } ,{  
            ref: '#director-tourleader-approvalprocess-grid',
            selector: '#director-tourleader-approvalprocess-grid'
        }
         ,{
            ref: '#director-business-approvalprocess-formsearch',
            selector: 'panel'
        }
         ,{
            ref: '#director-guide-approvalprocess-formsearch',
            selector: 'panel'
        }
         ,{
            ref: '#director-tourleader-approvalprocess-formsearch',
            selector: 'panel'
        }
        ,{
            ref: '#director-registration-verification-form',
            selector: 'panel'
        }
       
    ],	 
	init : function (application) {

        this.control({
                       
            // '#director-approvalprocess-formsearch button[action=searchLicenseBetweenRegistration]': {
            //     click: this.searchLicenseBetweenRegistration
            // }, 
            // '#director-approvalprocess-grid actioncolumn': {
            //     itemclick: this.handleActionColumn
            // },
            // '#director-registration-verification-form button[action=saveVerification]': {
            //     click: this.saveVerification
            // }
             '#director-business-approvalprocess-formsearch button[action=searchLicenseBetweenRegistration]': {
                click: this.searchBusinessLicense
            },
             '#director-guide-approvalprocess-formsearch button[action=searchLicenseBetweenRegistration]': {
                click: this.searchGuideLicense
            },
             '#director-tourleader-approvalprocess-formsearch button[action=searchLicenseBetweenRegistration]': {
                click: this.searchTourleaderLicense
            },
            '#director-business-approvalprocess-grid actioncolumn': {
                itemclick: this.handleActionColumn
            },
            '#director-guide-approvalprocess-grid actioncolumn': {
                itemclick: this.handleActionColumn
            },
            '#director-tourleader-approvalprocess-grid actioncolumn': {
                itemclick: this.handleActionColumn
            },
            '#director-registration-verification-form button[action=saveVerification]': {
                click: this.saveVerification
            }
            
        });

		
	},
  searchBusinessLicense: function(btn)
  {
    var form = btn.up('form'),
    values = form.getValues();
    var dataParams = this.normalizeData(values);
    dataParams.traderType = 'B';

    var grid = Ext.ComponentQuery.query('#director-business-approvalprocess-grid')[0];
    var store = grid.getStore();

     dataParams.page = '1';
     dataParams.start = '0';
     dataParams.limit = store.pageSize;

      store.load({
          params: dataParams,
          callback: function(records, operation, success) {
              // //console.log(success);
          },
          scope: this
      }); 
  }
  ,searchGuideLicense: function(btn)
  {
    var form = btn.up('form'),
    values = form.getValues();
    var dataParams = this.normalizeData(values);
    dataParams.traderType = 'G';

    var grid = Ext.ComponentQuery.query('#director-guide-approvalprocess-grid')[0];
    var store = grid.getStore();

     dataParams.page = '1';
     dataParams.start = '0';
     dataParams.limit = store.pageSize;

      store.load({
          params: dataParams,
          callback: function(records, operation, success) {
              // //console.log(success);
          },
          scope: this
      }); 
  }
  ,searchTourleaderLicense: function(btn)
  {

    var form = btn.up('form'),
    values = form.getValues();
    var dataParams = this.normalizeData(values);
    dataParams.traderType = 'L';

    var grid = Ext.ComponentQuery.query('#director-tourleader-approvalprocess-grid')[0];
    var store = grid.getStore();

     dataParams.page = '1';
     dataParams.start = '0';
     dataParams.limit = store.pageSize;

      store.load({
          params: dataParams,
          callback: function(records, operation, success) {
              // //console.log(success);
          },
          scope: this
      }); 
  },
  normalizeData: function(dataObj)
  {
      // remove null data;
     for (field in dataObj) 
      {
        if (Ext.isEmpty(dataObj[field])) 
        {
          delete dataObj[field];
        }
        
      }

      return dataObj;
  }
  ,handleActionColumn : function(column, action, grid, rowIndex, colIndex, model, node) {

          
          if(action){
              switch(action){
                case 'approvaleditregistration':

                    if(model.get('traderType') == 'B')
                    {
                      var winInfo = Ext.create('tourism.view.information.business.InfoBusinessWindow',{
                        id: 'director-information-business-registration-infowindow',
                        animateTarget: column.el,
                        actionMode: 'imformation',
                        roleAction: 'director'
                        ,registrationType: model.get('registrationType')
                      });

                       winInfo.show();

                      winInfo.loadFormRecord(model);


                      winInfo.showConutry(model);

                      
                    }

                    if(model.get('traderType') == 'G')
                    {
                    	var winInfo = Ext.create('tourism.view.information.guide.InfoGuideWindow',{
                      		id: 'director-information-guide-registration-infowindow',
                      		animateTarget: column.el,
                            actionMode: 'information',
                            roleAction: 'director'
                            ,registrationType: model.get('registrationType')
                      	}).show();
                    	
                    	//console.log(model);

                        winInfo.loadFormRecord(model);
                    }
                    if(model.get('traderType') == 'L')
                    {
                    	var winInfo = Ext.create('tourism.view.information.tourleader.InfoTourleaderWindow',{
                      		id: 'director-information-tourleader-registration-infowindow',
                      		animateTarget: column.el,
                            actionMode: 'information',
                            roleAction: 'director'
                            ,registrationType: model.get('registrationType')
                      	}).show();
                      	
                      	//console.log(model);
                      	
                      	winInfo.loadFormRecord(model);
                    }
                                    
                  break;
                  
                  case 'approvalverification': 
                	  
                	 
                	  
                    var edit = Ext.create('tourism.view.window.verification.VerificationWindow',{
                     id: 'director-registration-verification-form',
                     animateTarget: column.el,
                     actionMode: 'verification',
                     roleAction: 'director',
                     verificationGrid: grid
                    }).show();

//                    var form = edit.down('form');
//                    form.getForm().findField('regId').setValue(model.get('regId'));
                    
                    var form = edit.down('form');
	                 form.getForm().findField('regId').setValue(model.get('regId'));
	                 var grid = form.down('verification-reg-document-grid');
	                 grid.getStore().load({
	                 	  params: {
	                 		  traderId: model.get('traderId')
	                 		  ,recordStatus: 'N'
//	                 		  ,licenseType: 'G'
//	                 		  ,registrationType: model.get("registrationType")
	                 	  },
	                       callback: function(records, operation, success) {
//	                            //console.log(records);
	                       },
	                       scope: this
	                   });
                  break;

                  case 'approvalprogress':

                    var progressWin = Ext.create('tourism.view.registrationprogress.RegistrationProgressWindow',{
                      id: 'director-registrationprogress-ProgressWindow',
                      animateTarget: column.el 
                      
                     });

                    progressWin.on('show', function(win){


                      var grid = win.down('registrationprogress-grid');
               
                      grid.getStore().load({
                        params: {regId: model.get('regId')},
                        callback: function(records, operation, success) {
                            
                        },
                        scope: this
                      }); 

                    },this);

                    progressWin.show();

                  break;
              }
          }
      
  },
  saveVerification: function(btn)
  {
    var form    = btn.up('form');
    var win = btn.up('window');
    var values = form.getValues();
    var grid = win.verificationGrid;


    if (!form.isValid()) {

            var me = form,errorCmp, fields, errors;

            fields = me.getForm().getFields();
            errors = '';
            fields.each(function(field) {
                Ext.Array.forEach(field.getErrors(), function(error) {
                    errors += field.getFieldLabel()  +'<br>';
                });
            });

            Ext.Msg.show({
               title:'กรุณาตรวจสอบข้อมูล',
               msg: errors,
               buttons: Ext.Msg.OK,
               icon: Ext.Msg.ERROR
            });
            return;
        }


    var dataParams = this.normalizeData(values);

    var url = 'business/director/registration/verification';
    if(values.progressStatus == '0'){
    	url = 'business/registration/requireDocs';
    }
     // fsubmit.submit({
   form.load({
        url: url,
        method : 'POST',
        waitMsg: 'กรุณารอสักครู่..',
        params: dataParams,
        success: function(form, action) {

          var dataObj = action.result.data;
         
          // Ext.Msg.alert('', 'บันทึกข้อมูล');
          var noti = Ext.create('widget.uxNotification', {
            // title: 'Notification',
            position: 'tr',
            manager: 'instructions',
            // cls: 'ux-notification-light',
            // iconCls: 'ux-notification-icon-information',
            html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
            closable: false,
            autoCloseDelay: 4000,
            width: 300,
            slideBackDuration: 500,
            slideInAnimation: 'bounceOut',
            slideBackAnimation: 'easeIn'
          });
          noti.show();

          // var grid = Ext.ComponentQuery.query('#director-approvalprocess-grid')[0];
          var store = grid.getStore();

         store.reload();
         
          win.close();
    
        },
        failure: function(form, action) {
          Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
        },
        scope: this 
    });
  }
 
});