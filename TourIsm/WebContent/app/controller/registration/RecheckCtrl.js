Ext.define('tourism.controller.registration.RecheckCtrl', {
	extend : 'Ext.app.Controller',

   	stores: ['tourism.store.registration.RegistrationStore'],

    models: ['tourism.model.registration.RegistrationModel'],

    views: [
      'tourism.view.approvalprocess.ApprovalProcessGrid',
      // 'tourism.view.approvalprocess.ApprovalProcessFormSearch',
      'tourism.view.approvalprocess.ApprovalProcessBusinessFormSearch',
      'tourism.view.approvalprocess.ApprovalProcessGuideFormSearch',
      'tourism.view.approvalprocess.ApprovalProcessTourleaderFormSearch',
      'tourism.view.window.verification.VerificationForm',
      'tourism.view.registrationprogress.RegistrationProgressWindow'
    ],

    refs: [    
    	  {  
            ref: '#recheck-business-approvalprocess-grid',
            selector: '#recheck-business-approvalprocess-grid'
        } 
         ,{  
            ref: '#recheck-guide-approvalprocess-grid',
            selector: '#recheck-guide-approvalprocess-grid'
        } ,{  
            ref: '#recheck-tourleader-approvalprocess-grid',
            selector: '#recheck-tourleader-approvalprocess-grid'
        }
         ,{
            ref: '#recheck-business-approvalprocess-formsearch',
            selector: 'panel'
        }
         ,{
            ref: '#recheck-guide-approvalprocess-formsearch',
            selector: 'panel'
        }
         ,{
            ref: '#recheck-tourleader-approvalprocess-formsearch',
            selector: 'panel'
        }
        ,{
            ref: '#recheck-registration-verification-form',
            selector: 'panel'
        },
        {
            ref: '#recheck-business-registration-addedit',
            selector: 'panel'
        },
        {
            ref: '#recheck-tourleader-registration-addedit',
            selector: 'panel'
        }
       
    ],	 
	init : function (application) {

        this.control({
                       
            '#recheck-business-approvalprocess-formsearch button[action=searchLicenseBetweenRegistration]': {
                click: this.searchBusinessLicense
            },
             '#recheck-guide-approvalprocess-formsearch button[action=searchLicenseBetweenRegistration]': {
                click: this.searchGuideLicense
            },
             '#recheck-tourleader-approvalprocess-formsearch button[action=searchLicenseBetweenRegistration]': {
                click: this.searchTourleaderLicense
            },
            '#recheck-business-approvalprocess-grid actioncolumn': {
                itemclick: this.handleActionColumn
            },
            '#recheck-guide-approvalprocess-grid actioncolumn': {
                itemclick: this.handleActionColumn
            },
            '#recheck-tourleader-approvalprocess-grid actioncolumn': {
                itemclick: this.handleActionColumn
            },
            '#recheck-registration-verification-form button[action=saveVerification]': {
                click: this.saveVerification
            },
            '#recheck-business-registration-addedit button[action=saveBusinessRegistration]': {
                click: this.saveBusinessRegistration
            },
            
            '#recheck-guide-registration-addedit button[action=saveGuideRegistration]': {
                click: this.saveGuideRegistration
            }
            ,'#recheck-tourleader-registration-addedit button[action=saveTourleaderRegistration]': {
                click: this.saveTourleaderRegistration
            }

            
        });

		
	},
    searchBusinessLicense: function(btn)
  {
    var form = btn.up('form'),
    values = form.getValues();
    var dataParams = this.normalizeData(values);
    dataParams.traderType = 'B';

    var grid = Ext.ComponentQuery.query('#recheck-business-approvalprocess-grid')[0];
    var store = grid.getStore();

     dataParams.page = '1';
     dataParams.start = '0';
     dataParams.limit = store.pageSize;

      store.load({
          params: dataParams,
          callback: function(records, operation, success) {
              // //console.log(success);
          },
          scope: this
      }); 
  }
  ,searchGuideLicense: function(btn)
  {
    var form = btn.up('form'),
    values = form.getValues();
    var dataParams = this.normalizeData(values);
    dataParams.traderType = 'G';

    var grid = Ext.ComponentQuery.query('#recheck-guide-approvalprocess-grid')[0];
    var store = grid.getStore();

     dataParams.page = '1';
     dataParams.start = '0';
     dataParams.limit = store.pageSize;

      store.load({
          params: dataParams,
          callback: function(records, operation, success) {
              // //console.log(success);
          },
          scope: this
      }); 
  }
  ,searchTourleaderLicense: function(btn)
  {

    var form = btn.up('form'),
    values = form.getValues();
    var dataParams = this.normalizeData(values);
    dataParams.traderType = 'L';

    var grid = Ext.ComponentQuery.query('#recheck-tourleader-approvalprocess-grid')[0];
    var store = grid.getStore();

     dataParams.page = '1';
     dataParams.start = '0';
     dataParams.limit = store.pageSize;

      store.load({
          params: dataParams,
          callback: function(records, operation, success) {
              // //console.log(success);
          },
          scope: this
      }); 
  },
  normalizeData: function(dataObj)
  {
      // remove null data;
     for (field in dataObj) 
      {
        if (Ext.isEmpty(dataObj[field])) 
        {
          delete dataObj[field];
        }
        
      }

      return dataObj;
  }
  ,handleActionColumn : function(column, action, grid, rowIndex, colIndex, model, node) {

          
          if(action){
              switch(action){
                case 'approvaleditregistration':

                    if(model.get('traderType') == 'B')
                    {
                      var winInfo = Ext.create('tourism.view.information.business.InfoBusinessWindow',{
                        id: 'recheck-information-business-registration-infowindow',
                        animateTarget: column.el,
                        actionMode: 'imformation',
                        roleAction: 'recheck'
                        ,registrationType: model.get('registrationType')
                      });

                       winInfo.show();

                      winInfo.loadFormRecord(model);
                    }

                    if(model.get('traderType') == 'G')
                    {
                    	var winInfo = Ext.create('tourism.view.information.guide.InfoGuideWindow',{
                      		id: 'recheck-information-guide-registration-infowindow',
                      		animateTarget: column.el,
                            actionMode: 'information',
                            roleAction: 'recheck'
                            ,registrationType: model.get('registrationType')
                      	}).show();

                        winInfo.loadFormRecord(model);
                    }
                    if(model.get('traderType') == 'L')
                    {
                    	var winInfo = Ext.create('tourism.view.information.tourleader.InfoTourleaderWindow',{
                      		id: 'recheck-information-tourleader-registration-infowindow',
                      		animateTarget: column.el,
                            actionMode: 'information',
                            roleAction: 'recheck'
                            ,registrationType: model.get('registrationType')
                      	}).show();
                      	
                      	
                      	winInfo.loadFormRecord(model);
                    }
                                    
                  break;
                  
                  case 'approvalverification': 
                      
                    var edit = Ext.create('tourism.view.window.verification.VerificationWindow',{
                     id: 'recheck-registration-verification-form',
                     animateTarget: column.el,
                     actionMode: 'verification',
                     roleAction: 'recheck',
                     verificationGrid: grid,
                     multipleJobs: model.get('multipleJobs')
                    }).show();

                    
                    var form = edit.down('form');
	                 form.getForm().findField('regId').setValue(model.get('regId'));
	                 var grid = form.down('verification-reg-document-grid');
	                 grid.getStore().load({
	                 	  params: {
	                 		  traderId: model.get('traderId')
	                 		  ,recordStatus: 'N'
//	                 		  ,licenseType: 'G'
//	                 		  ,registrationType: model.get("registrationType")
	                 	  },
	                       callback: function(records, operation, success) {
//	                            //console.log(records);
	                       },
	                       scope: this
	                   });
                  break;

                  case 'approvalprogress':



                    var progressWin = Ext.create('tourism.view.registrationprogress.RegistrationProgressWindow',{
                      id: 'recheck-registrationprogress-ProgressWindow',
                      animateTarget: column.el 
                      
                     });

                    progressWin.on('show', function(win){


                      var grid = win.down('registrationprogress-grid');
         
                      grid.getStore().load({
                        params: {regId: model.get('regId')},
                        callback: function(records, operation, success) {
                            
                        },
                        scope: this
                      }); 

                    },this);

                    progressWin.show();

                  break;

                 case 'editregistration': 

                    if(model.get('traderType') == 'B')
                    {
                      var edit = Ext.create('tourism.view.business.registration.BusinessRegistrationAddEditWindow',{
                      animateTarget: column.el,
                      actionMode: 'edit',
                      roleAction: 'recheck',
                      traderType: 'business',
                      verificationGrid: grid

                     });

                     edit.show();

                     edit.loadDataModel(model);
                    }

                    if(model.get('traderType') == 'G')
                    {
                        var edit = Ext.create('tourism.view.guide.registration.GuideRegistrationAddEditWindow',{
                        animateTarget: column.el,
                            actionMode: 'edit',
                            roleAction: 'recheck',
                            traderType: 'guide',
                            verificationGrid: grid
                        });
                        edit.show();

                        edit.loadDataModel(model);
                    }

                    if(model.get('traderType') == 'L')
                    {
                        var edit = Ext.create('tourism.view.tourleader.registration.TourleaderRegistrationAddEditWindow',{
                            animateTarget: column.el,
                            actionMode: 'edit',
                            roleAction: 'recheck',
                            traderType: 'tourleader',
                            verificationGrid: grid
                        });

                       edit.show();

                       edit.loadDataModel(model);
                    }
                     
                  break;
              }
          }
      
  },
  saveVerification: function(btn)
  {
    var form    = btn.up('form');
    var win = btn.up('window');
    var values = form.getValues();
    var grid = win.verificationGrid;


            if (!form.isValid()) {

            var me = form,
            errorCmp, fields, errors;

            fields = me.getForm().getFields();
            errors = '';
            fields.each(function(field) {
                Ext.Array.forEach(field.getErrors(), function(error) {
                    // errors.push({name: field.getFieldLabel(), error: error});
                    errors += field.getFieldLabel()  +'<br>';
                });
            });

            Ext.Msg.show({
               title:'กรุณาตรวจสอบข้อมูล',
               msg: errors,
               buttons: Ext.Msg.OK,
               icon: Ext.Msg.ERROR
            });
            return;
        }


    var dataParams = this.normalizeData(values);

    var url = 'business/recheck/registration/verification';
    if(values.progressStatus == '0'){
    	url = 'business/registration/requireDocs';
    }
   form.load({
        url: url,
        method : 'POST',
        waitMsg: 'กรุณารอสักครู่..',
        params: dataParams,
        success: function(form, action) {

          var dataObj = action.result.data;
         
        
          var noti = Ext.create('widget.uxNotification', {
            // title: 'Notification',
            position: 'tr',
            manager: 'instructions',
           
            html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
            closable: false,
            autoCloseDelay: 4000,
            width: 300,
            slideBackDuration: 500,
            slideInAnimation: 'bounceOut',
            slideBackAnimation: 'easeIn'
          });
          noti.show();

         
          var store = grid.getStore();

         store.reload();
         
          win.close();
    
        },
        failure: function(form, action) {
          Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
        },
        scope: this 
    });
  },
    saveBusinessRegistration: function(btn, model)
  {
      //console.log('saveBusinessRegistration');
      var form    = btn.up('form');
      var win = btn.up('window');

      var model = form.getRecord(); //Ext.data.Model
      var values = form.getValues();//object {xx: q,ss:ff}
      var grid = win.verificationGrid;

      
      if (!form.isValid()) {

          var me = form,
          errorCmp, fields, errors;

          fields = me.getForm().getFields();
          errors = '';
          fields.each(function(field) {
              Ext.Array.forEach(field.getErrors(), function(error) {
                  // errors.push({name: field.getFieldLabel(), error: error});
                  errors += field.getFieldLabel()  +'<br>';
              });
          });
       

          Ext.Msg.show({
             title:'กรุณาตรวจสอบข้อมูล',
             msg: errors,
             buttons: Ext.Msg.OK,
             icon: Ext.Msg.ERROR
          });
          return;
      }


      var fsubmit = form.getForm();
      var ctrl = this; // scope

      if(!Ext.isEmpty(values.personTypeCorporate))
      {// กรรีที่เป็น บริษัท
        values.personType = 'C';
        values.prefixId = values.prefixIdCorp;
        values.firstName = values.firstNameCorp;
        values.identityNo = values.identityNoCorp;
        values.identityDate = values.identityDateCorp;
        values.provinceId = values.provinceIdCorp;        
      }
      else
      {
        values.personType = 'I';
      }

      var dataParams = this.normalizeData(values);
     
      // fsubmit.submit({
       fsubmit.load({
            url: 'business/registration/saveAllRegistration',
            method : 'POST',
            waitMsg: 'กรุณารอสักครู่..',
            params: dataParams,
            success: function(form, action) {

              var dataObj = action.result.data;
             
              // Ext.Msg.alert('', 'บันทึกข้อมูล');
            var noti = Ext.create('widget.uxNotification', {
              // title: 'Notification',
              position: 'tr',
              manager: 'instructions',
             
              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
              closable: false,
              autoCloseDelay: 4000,
              width: 300,
              slideBackDuration: 500,
              slideInAnimation: 'bounceOut',
              slideBackAnimation: 'easeIn'
            });
            noti.show();

              var store = grid.getStore();

              store.reload();


             
              win.close();
        
            },
            failure: function(form, action) {
              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
            },
            scope: this 
        });
  }
 ,  saveGuideRegistration: function(btn,model){
    //console.log('saveGuideRegistration');
    var win = btn.up('window');
    var form = win.down('form');
    
    var model = form.getRecord(); //Ext.data.Model
      var values = form.getValues();//object {xx: q,ss:ff}
      var grid = win.verificationGrid;
     
      var guideTrader = Ext.ComponentQuery.query('guide_registration_guidetype_guidetypeaddedit_form')[0];
     
      var provinceIdName = Ext.getCmp('guide-type-registration-masprovince-combo').getValue();
      var traderCategoryName = Ext.getCmp('guide-type-registration-categorystore-combo').getValue();
      var traderAreaName = guideTrader.getForm().findField('traderArea').getValue();
     
      values.traderCategory = traderCategoryName;
        
      if(provinceIdName !=null)
      {
        values.provinceGuideServiceId = provinceIdName;
      }else
      {
        values.provinceGuideServiceId = '';
      }
      
  
      if(traderAreaName !=null)
      {
        values.traderArea = traderAreaName;
      }else
      {
        values.traderArea = '';
      }
      
        if (!form.isValid()) {

            var me = form,errorCmp, fields, errors;

            fields = me.getForm().getFields();
            errors = '';
            fields.each(function(field) {
                Ext.Array.forEach(field.getErrors(), function(error) {
                  // errors.push({name: field.getFieldLabel(), error: error});
                    errors += field.getFieldLabel()  +'<br>';
                });
            });
            // Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', errors);

            Ext.Msg.show({
               title:'กรุณาตรวจสอบข้อมูล',
               msg: errors,
               buttons: Ext.Msg.OK,
               icon: Ext.Msg.ERROR
            });
            return;
        }
        
        var fsubmit = form.getForm();
        values.personType = 'I';
     
        var dataParams = this.normalizeData(values);
  
        
        fsubmit.load({
            url: 'business/registration/saveAllRegistration',
            method : 'POST',
            waitMsg: 'กรุณารอสักครู่..',
            params: dataParams,
            success: function(form, action) {

              var dataObj = action.result.data;
             
              // Ext.Msg.alert('', 'บันทึกข้อมูล');

            var noti = Ext.create('widget.uxNotification', {
              // title: 'Notification',
              position: 'tr',
              manager: 'instructions',
              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
              closable: false,
              autoCloseDelay: 4000,
              width: 300,
              slideBackDuration: 500,
              slideInAnimation: 'bounceOut',
              slideBackAnimation: 'easeIn'
            });
            noti.show();

              var store = grid.getStore();

              store.reload();

              win.close();
        
            },
            failure: function(form, action) {
              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
            },
            scope: this 
        });
        
        
  }
  ,saveTourleaderRegistration: function(btn, model){
      //console.log('saveTourleaderRegistration');
      var win = btn.up('window');
      var form = win.down('form');
      
      var model = form.getRecord(); //Ext.data.Model
      var values = form.getValues();//object {xx: q,ss:ff}
      var grid = win.verificationGrid;
        

        
        var fsubmit = form.getForm();
        values.personType = 'I';
        
        var dataParams = this.normalizeData(values);
        
        fsubmit.load({
            url: 'business/registration/saveAllRegistration',
            method : 'POST',
            waitMsg: 'กรุณารอสักครู่..',
            params: dataParams,
            success: function(form, action) {

              var dataObj = action.result.data;
        

            var noti = Ext.create('widget.uxNotification', {
              // title: 'Notification',
              position: 'tr',
              manager: 'instructions',
              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
              closable: false,
              autoCloseDelay: 4000,
              width: 300,
              slideBackDuration: 500,
              slideInAnimation: 'bounceOut',
              slideBackAnimation: 'easeIn'
            });
            noti.show();

              
              var store = grid.getStore();

              store.reload();

              win.close();
        
            },
            failure: function(form, action) {
              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
            },
            scope: this 
        });
        
    }
});