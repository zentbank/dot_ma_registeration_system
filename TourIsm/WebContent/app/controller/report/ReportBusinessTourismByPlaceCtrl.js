Ext.define('tourism.controller.report.ReportBusinessTourismByPlaceCtrl',{
	extend: 'Ext.app.Controller',
	models: ['tourism.model.report.ReportModel'],
	views: [
            'tourism.view.report.ReportBusinessTourismByPlaceFormSearch'
          ],
          refs:[   
                {
               	 ref:'#reportBusinessTourismByPlace-formsearch',
               	 selector:'panel'
                }
                ],
      init : function (application) {

          this.control({
                   	  	'#reportBusinessTourismByPlace-formsearch button[action=printReportBusinessTourism]':{
                   	  		click: this.printReportBusinessTourism
                   	 } 
                    });
         },
         printReportBusinessTourism: function(btn){
        	 var form = btn.up('form');
        	 var values = form.getValues();
       	  	 var dataParams = this.normalizeData(values);
//       	  	 sconsole.log(dataParams);
       	  	 var provinceId = "";
       	  	 var amphurId = "";
       	  	 var tambolId = "";
       	  	 provinceId = (dataParams.provinceId == undefined)?"":dataParams.provinceId;
        	 amphurId = (dataParams.amphurId == undefined)?"":dataParams.amphurId;
        	 tambolId = (dataParams.tambolId == undefined)?"":dataParams.tambolId;
        	 
        	 var reqUrl = "business/reportBusinessTourismByPlace/printReport?provinceId="+provinceId+"&amphurId="+amphurId+"&tambolId="+tambolId;
//             console.log("reqUrl: "+reqUrl);    
        	 window.open(reqUrl,"พิมพ์รายงาน","toolbar=0 width=900 height=600");
        
             
         },
         normalizeData: function(dataObj)
         {
             // remove null data;
            for (field in dataObj) 
             {
               if (Ext.isEmpty(dataObj[field])) 
               {
                 delete dataObj[field];
               }
               
             }

             return dataObj;
         }
});