Ext.define('tourism.controller.report.ReportFeeBusinessGuideCtrl',{
	extend: 'Ext.app.Controller',
	models: ['tourism.model.report.ReportModel'],
	views: [
            'tourism.view.report.ReportFeeBusinessGuideFormSearch'
          ],
          refs:[   
                {
               	 ref:'#reportfeebusinessguide-formsearch',
               	 selector:'panel'
                }
                ],
      init : function (application) {

          this.control({
                   	  	'#reportfeebusinessguide-formsearch button[action=printReportFeeBusinessGuide]':{
                   	  		click: this.searchPrintReportFeebusinessGuide
                   	 } 
                    });
         },
         searchPrintReportFeebusinessGuide: function(btn)
         {
        	 var form = btn.up('form');
        	 if (!form.isValid()) {

   	          var me = form,errorCmp, fields, errors;

   	          fields = me.getForm().getFields();
   	          errors = '';
   	          fields.each(function(field) {
   	              Ext.Array.forEach(field.getErrors(), function(error) {
                     // errors.push({name: field.getFieldLabel(), error: error});
   	                  errors += field.getFieldLabel()  +'<br>';
   	              });
   	          });
   	          // Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', errors);

   	          Ext.Msg.show({
   	             title:'กรุณาตรวจสอบข้อมูล',
   	             msg: errors,
   	             buttons: Ext.Msg.OK,
   	             icon: Ext.Msg.ERROR
   	          });
   	          return;
   	      }
        	 var month =  form.getForm().findField('monthName').getValue();
        	 var year =  form.getForm().findField('year').getRawValue();
        	 
 
        	 var reqUrl = "business/reportfeebusinessguide/registration/reportfeebusinessguideExcel?month="+month+"&year="+year;
                 window.open(reqUrl,"พิมพ์รายงาน","toolbar=0 width=900 height=600");
        
             
         },
         normalizeData: function(dataObj)
         {
             // remove null data;
            for (field in dataObj) 
             {
               if (Ext.isEmpty(dataObj[field])) 
               {
                 delete dataObj[field];
               }
               
             }

             return dataObj;
         }
});