Ext.define('tourism.controller.report.ReportFeeGuideCtrl',{
	extend: 'Ext.app.Controller',
	models: ['tourism.model.registration.RegistrationModel'],
	views: [
            'tourism.view.report.ReportFeeGuideFormSearch'
          ],
          refs:[   
                {
               	 ref:'reportfeeguide-formsearch',
               	 selector:'panel'
                }
                ],
      init : function (application) {

          this.control({
                   	  	'reportfeeguide-formsearch button[action=printReportFeeGuide]':{
                   	  		click: this.searchPrintReportFeeGuide
                   	 } 
                    });
         },
         searchPrintReportFeeGuide: function(btn)
         {
        	 var form = btn.up('form');
        	 if (!form.isValid()) {

   	          var me = form,errorCmp, fields, errors;

   	          fields = me.getForm().getFields();
   	          errors = '';
   	          fields.each(function(field) {
   	              Ext.Array.forEach(field.getErrors(), function(error) {
                     // errors.push({name: field.getFieldLabel(), error: error});
   	                  errors += field.getFieldLabel()  +'<br>';
   	              });
   	          });
   	          // Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', errors);

   	          Ext.Msg.show({
   	             title:'กรุณาตรวจสอบข้อมูล',
   	             msg: errors,
   	             buttons: Ext.Msg.OK,
   	             icon: Ext.Msg.ERROR
   	          });
   	          return;
   	      }
        	 var dateFrom =  form.getForm().findField('receiptDateFrom').getRawValue();
        	 var dateToFake =  form.getForm().findField('receiptDateTo').getRawValue();

        	 var dateTo;
        	 if(!Ext.isEmpty(dateToFake))
        	 {
        		  dateTo = dateToFake;
        		  //console.log(dateTo);
             }else
             {
            	  dateTo = '0';
             }
        	 
//        	 var form = btn.up('form'),
//     	     values = form.getValues();
//     	     var dataParams = this.normalizeData(values);
//     	     console.log(dataParams.orgId);
        	 
        	 //Oat Add 07/10/57
     	     var orgId =  form.getForm().findField('orgId').getValue();
     	     if(orgId == null)
     	     {
     	       orgId = 0;
     	     }
     	     console.log(orgId);
     	     //

     	     //Oat Edit 07/10/57
        	 var reqUrl = "business/reportfeeguide/registration/reportfeeguideExcel?receiptDateFrom="
        		 +dateFrom+"&receiptDateTo="+dateTo+"&orgId="+orgId;
        	 //
             window.open(reqUrl,"พิมพ์รายงาน","toolbar=0 width=900 height=600");
        
             
         },
         normalizeData: function(dataObj)
         {
             // remove null data;
            for (field in dataObj) 
             {
               if (Ext.isEmpty(dataObj[field])) 
               {
                 delete dataObj[field];
               }
               
             }

             return dataObj;
         }
});