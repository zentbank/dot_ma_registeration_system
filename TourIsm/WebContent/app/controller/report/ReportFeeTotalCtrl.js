Ext.define('tourism.controller.report.ReportFeeTotalCtrl',{
	extend: 'Ext.app.Controller',
	models: ['tourism.model.registration.RegistrationModel'],
	views: [
            'tourism.view.report.ReportFeeTotalFormSearch'
          ],
          refs:[   
                {
               	 ref:'reportfeetotal-formsearch',
               	 selector:'panel'
                }
                ],
      init : function (application) {

          this.control({
                   	  	'reportfeetotal-formsearch button[action=printReportFeeTotal]':{
                   	  		click: this.searchPrintReportFeeTotal
                   	 } 
                    });
         },
         searchPrintReportFeeTotal: function(btn)
         {
        	 var form = btn.up('form');
        	 if (!form.isValid()) {

   	          var me = form,errorCmp, fields, errors;

   	          fields = me.getForm().getFields();
   	          errors = '';
   	          fields.each(function(field) {
   	              Ext.Array.forEach(field.getErrors(), function(error) {
                     // errors.push({name: field.getFieldLabel(), error: error});
   	                  errors += field.getFieldLabel()  +'<br>';
   	              });
   	          });
//   	           Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', errors);

   	          Ext.Msg.show({
   	             title:'กรุณาตรวจสอบข้อมูล',
   	             msg: errors,
   	             buttons: Ext.Msg.OK,
   	             icon: Ext.Msg.ERROR
   	          });
   	          return;
   	      }
        	 var dateFrom =  form.getForm().findField('receiptDateFrom').getRawValue();
        	 var orgId =  form.getForm().findField('orgId').getValue();
        	

        	 var reqUrl = "business/reportfeetotal/print/reportfeetotalExcel?receiptDateFrom="+dateFrom+"&orgId="+orgId;
                 window.open(reqUrl,'_blank');
        
             
         },
         normalizeData: function(dataObj)
         {
             // remove null data;
            for (field in dataObj) 
             {
               if (Ext.isEmpty(dataObj[field])) 
               {
                 delete dataObj[field];
               }
               
             }

             return dataObj;
         }
});