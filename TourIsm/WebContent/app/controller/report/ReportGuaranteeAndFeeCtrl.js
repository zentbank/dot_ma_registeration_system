Ext.define('tourism.controller.report.ReportGuaranteeAndFeeCtrl',{
	extend: 'Ext.app.Controller',
	models: ['tourism.model.registration.RegistrationModel'],
	views: [
            'tourism.view.report.ReportGuaranteeAndFeeFormSearch'
          ],
          refs:[   
                {
               	 ref:'reportguaranteeandfee-formsearch',
               	 selector:'panel'
                }
                ],
      init : function (application) {

          this.control({
                   	  	'reportguaranteeandfee-formsearch button[action=printReportGuaranteeAndFee]':{
                   	  		click: this.searchPrintReportGuaranteeAndFee
                   	 } 
                    });
         },
         searchPrintReportGuaranteeAndFee: function(btn)
         {
        	 var form = btn.up('form');
        	 if (!form.isValid()) {

   	          var me = form,errorCmp, fields, errors;

   	          fields = me.getForm().getFields();
   	          errors = '';
   	          fields.each(function(field) {
   	              Ext.Array.forEach(field.getErrors(), function(error) {
                     // errors.push({name: field.getFieldLabel(), error: error});
   	                  errors += field.getFieldLabel()  +'<br>';
   	              });
   	          });
   	          // Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', errors);

   	          Ext.Msg.show({
   	             title:'กรุณาตรวจสอบข้อมูล',
   	             msg: errors,
   	             buttons: Ext.Msg.OK,
   	             icon: Ext.Msg.ERROR
   	          });
   	          return;
   	      }
        	 var dateFrom =  form.getForm().findField('receiptDateFrom').getRawValue();
        	 var dateToFake =  form.getForm().findField('receiptDateTo').getRawValue();

        	 var dateTo;
        	 if(!Ext.isEmpty(dateToFake))
        	 {
        		  dateTo = dateToFake;
        		  //console.log(dateTo);
             }else
             {
            	  dateTo = '0';
             }
        	 
        	//Oat Add 30/10/57
     	       var orgId =  form.getForm().findField('orgId').getValue();
     	       if(orgId == null)
     	       {
     	         orgId = 0;
     	       }
     	       console.log(orgId);
     	       //

        	 //Oat Edit 30/10/57
//        	 var reqUrl = "business/reportfeetotal/registration/reportfeetotalExcel?receiptDateFrom="+dateFrom+"&receiptDateTo="+dateTo;
//        	 var reqUrl = "business/reportfeetotal/print/reportfeetotalExcel?receiptDateFrom="+dateFrom+"&receiptDateTo="+dateTo;
        	 var reqUrl = "business/account/registration/receipt/reportguaranteeandfee?receiveOfficerDateFrom="
        		 +dateFrom+"&receiveOfficerDateTo="+dateTo+"&orgId="+orgId;
        	 //End Oat Edit
        	 
        	 window.open(reqUrl,"พิมพ์รายงาน","toolbar=0 width=900 height=600");
        
             
         },
         normalizeData: function(dataObj)
         {
             // remove null data;
            for (field in dataObj) 
             {
               if (Ext.isEmpty(dataObj[field])) 
               {
                 delete dataObj[field];
               }
               
             }

             return dataObj;
         }
});