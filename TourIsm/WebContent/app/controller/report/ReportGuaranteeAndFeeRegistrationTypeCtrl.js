Ext.define('tourism.controller.report.ReportGuaranteeAndFeeRegistrationTypeCtrl',{
	extend: 'Ext.app.Controller',
	models: ['tourism.model.registration.RegistrationModel'],
	views: [
            'tourism.view.report.ReportGuaranteeAndFeeRegistrationTypeFormSearch'
          ],
          refs:[   
                {
               	 ref:'reportguaranteeandfeeregistrationtype-formsearch',
               	 selector:'panel'
                }
                ],
      init : function (application) {

          this.control({
                   	  	'reportguaranteeandfeeregistrationtype-formsearch button[action=printReport]':{
                   	  		click: this.searchPrintReportGuaranteeAndFee
                   	 } 
                     
                    });
         }
         ,
         searchPrintReportGuaranteeAndFee: function(btn)
         {
        	 var form = btn.up('form');
        	 if (!form.isValid()) {

   	          var me = form,errorCmp, fields, errors;

   	          fields = me.getForm().getFields();
   	          errors = '';
   	          fields.each(function(field) {
   	              Ext.Array.forEach(field.getErrors(), function(error) {
                     // errors.push({name: field.getFieldLabel(), error: error});
   	                  errors += field.getFieldLabel()  +'<br>';
   	              });
   	          });
   	          // Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', errors);

   	          Ext.Msg.show({
   	             title:'กรุณาตรวจสอบข้อมูล',
   	             msg: errors,
   	             buttons: Ext.Msg.OK,
   	             icon: Ext.Msg.ERROR
   	          });
   	          return;
   	      }
        	 var dateFrom = '';
           if(!Ext.isEmpty(form.getForm().findField('receiptDateFrom').getRawValue()))
           {
              dateFrom =  form.getForm().findField('receiptDateFrom').getRawValue();
           }
        
        	 var dateTo = '';
        	 if(!Ext.isEmpty(form.getForm().findField('receiptDateTo').getRawValue()))
        	 {
        		  dateTo = form.getForm().findField('receiptDateTo').getRawValue();
        		  
          }
        	 
        	 var traderType  = '';
           if(!Ext.isEmpty(form.getForm().findField('traderType').getValue()))
           {
              traderType  = form.getForm().findField('traderType').getValue();
           }
        	 var registrationType = '';
           if(!Ext.isEmpty(form.getForm().findField('registrationType').getValue()))
           {
            registrationType = form.getForm().findField('registrationType').getValue();
           }
           
           //Oat Add 07/10/57
   	       var orgId =  form.getForm().findField('orgId').getValue();
   	       if(orgId == null)
   	       {
   	         orgId = 0;
   	       }
   	       console.log(orgId);
   	       //

   	       //Oat Edit 07/10/57
        	 var reqUrl = "business/account/registration/receipt/reportsummaryfeegurantee?receiveOfficerDateFrom="
        		 +dateFrom+"&receiveOfficerDateTo="+dateTo+"&traderType="+traderType+"&registrationType="
        		 +registrationType+"&orgId="+orgId;
                 window.open(reqUrl,"พิมพ์รายงาน","toolbar=0 width=900 height=600");
          //
             
         },
         normalizeData: function(dataObj)
         {
             // remove null data;
            for (field in dataObj) 
             {
               if (Ext.isEmpty(dataObj[field])) 
               {
                 delete dataObj[field];
               }
               
             }

             return dataObj;
         }
});