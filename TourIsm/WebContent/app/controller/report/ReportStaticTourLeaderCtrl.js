Ext.define('tourism.controller.report.ReportStaticTourLeaderCtrl',{
	extend: 'Ext.app.Controller',
	models: ['tourism.model.registration.RegistrationModel'],
	views: [
            'tourism.view.report.ReportStaticTourLeaderFormSearch'
          ],
          refs:[   
                {
               	 ref:'reportstatictourleader-formsearch',
               	 selector:'panel'
                }
                ],
      init : function (application) {

          this.control({
                   	  	'reportstatictourleader-formsearch button[action=printReportStaticTourLeader]':{
                   	  		click: this.printReportStaticTourLeader
                   	 } 
                    });
         },
         printReportStaticTourLeader: function(btn)
         {
        	 var form = btn.up('form');
        	 if (!form.isValid()) {

   	          var me = form,errorCmp, fields, errors;

   	          fields = me.getForm().getFields();
   	          errors = '';
   	          fields.each(function(field) {
   	              Ext.Array.forEach(field.getErrors(), function(error) {
                     // errors.push({name: field.getFieldLabel(), error: error});
   	                  errors += field.getFieldLabel()  +'<br>';
   	              });
   	          });
   	          // Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', errors);

   	          Ext.Msg.show({
   	             title:'กรุณาตรวจสอบข้อมูล',
   	             msg: errors,
   	             buttons: Ext.Msg.OK,
   	             icon: Ext.Msg.ERROR
   	          });
   	          return;
   	      }
        	 var dateFrom =  form.getForm().findField('receiptDateFrom').getRawValue();
        	 var dateToFake =  form.getForm().findField('receiptDateTo').getRawValue();

        	 var dateTo;
        	 if(!Ext.isEmpty(dateToFake))
        	 {
        		  dateTo = dateToFake;
        		  //console.log(dateTo);
             }else
             {
            	  dateTo = '0';
             }
        	 
//        	 var form = btn.up('form'),
//     	     values = form.getValues();
//     	     var dataParams = this.normalizeData(values);
//     	     console.log(dataParams.orgId);
        	 
        	 //Sek Add 19/09/59
//     	     var provinceId =  form.getForm().findField('provinceId').getValue();
//     	     if(provinceId == null)
//     	     {
//     	    	provinceId = 0;
//     	     }
//     	     console.log(provinceId);
     	     //

     	    //Sek Add 19/09/59
        	 var reqUrl = "business/reportstatictourleader/print/printExcel?dateFrom="
        		 +dateFrom+"&dateTo="+dateTo;
        	 //
             window.open(reqUrl,"พิมพ์รายงาน","toolbar=0 width=900 height=600");
        
             
         },
         normalizeData: function(dataObj)
         {
             // remove null data;
            for (field in dataObj) 
             {
               if (Ext.isEmpty(dataObj[field])) 
               {
                 delete dataObj[field];
               }
               
             }

             return dataObj;
         }
});