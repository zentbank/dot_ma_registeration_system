Ext.define('tourism.controller.reqcertificate.ReqCertificateCtrl',{
	extend : 'Ext.app.Controller',

  refs:[{
   	 ref:'#reqdoc-business-reqcertificate-formsearch',
   	 selector:'panel'
  },
  {
    ref: '#reqdoc-business-reqcertificate-grid',
    selector: '#reqdoc-business-reqcertificate-grid'
  }, {
    ref: '#reqcertificate-reqdoc-business-searchlicensepanel',
    selector: 'panel'
  }, {
      ref: '#reqcertificate-reqdoc-business-formaddedit',
      selector: 'panel'
  },{
    ref: '#reqcertificate-reqdoc-business-document-grid',
    selector: '#reqcertificate-reqdoc-business-document-grid'
  }, {
      ref: '#reqcertificate-reqdoc-business-docreq-form',
      selector: 'panel'
  }],
  init : function (application) {

    this.control({
   	   '#reqdoc-business-reqcertificate-formsearch button[action=searchReqCer]':{
   	  		click: this.searchReqCer
     	 }, 
       '#reqdoc-business-reqcertificate-formsearch button[action=printReport]':{
          click: this.printReport
       }, 
       '#reqdoc-business-reqcertificate-grid actioncolumn':{
       		itemclick: this.handleActionColumn
       }, 
       '#reqcertificate-reqdoc-business-searchlicensepanel button[action=selectLicense]': {
          click: this.selectLicenseNo
      }, 
       '#reqcertificate-reqdoc-business-formaddedit button[action=addCertificate]': {
          click: this.saveReqCertificate
      },
      '#reqcertificate-reqdoc-business-formaddedit button[action=printCertificate]': {
          click: this.printCertificate
      },
      '#reqcertificate-reqdoc-business-document-grid actioncolumn':{
          itemclick: this.handleDocActionColumn
       }, 
      '#reqcertificate-reqdoc-business-docreq-form button[action=saveReqDocCertificate]': {
          click: this.saveReqDocCertificate
      }
    });
   },
      printReport: function(btn){
       var form = btn.up('form'),
       values = form.getValues();

       var reqUrl = "business/req/certificate/readexcel/?licenseNo="+values.licenseNo
                +'&reqDateFrom='+values.reqDateFrom
                +'&traderName='+values.traderName
                +'&reqDateTo='+values.reqDateTo;
      window.open(reqUrl); 
   },
  searchReqCer: function(btn) {
   var form = btn.up('form'),
   values = form.getValues();
   var dataParams = this.normalizeData(values);
   //dataParams.traderType = 'L';

   var grid = Ext.ComponentQuery.query('#reqdoc-business-reqcertificate-grid')[0];
   var store = grid.getStore();

    dataParams.page = '1';
    dataParams.start = '0';
    dataParams.limit = store.pageSize;

     store.load({
         params: dataParams,
         callback: function(records, operation, success) {
             // //console.log(success);
         },
         scope: this
     }); 
  },  
  selectLicenseNo: function(btn){

    var win = btn.up('window');
    var grid = win.down('info-license-grid');
    var model = grid.getSelectionModel().getSelection()[0];

    var formCertificate = win.down('#reqcertificate-reqdoc-business-formaddedit');
    formCertificate.loadRecord(model);

    var tabPanel = formCertificate.down('tabpanel');


    var layout = win.getLayout();
    layout.setActiveItem(1);

    formCertificate.down('#reqcertificate-reqdoc-business-printCertificate').setVisible(false);
    formCertificate.down('#reqcertificate-reqdoc-business-approveStatus-checkboxgroup').setVisible(false);
    formCertificate.down('#reqcertificate-reqdoc-business-approveDate').setVisible(false);

    var tabPanel = formCertificate.down('tabpanel');

    tabPanel.child('#reqcertificate-reqdoc-business-document-grid').tab.hide();

    win.setWidth(600);

  },
  normalizeData: function(dataObj)
  {
     // remove null data;
    for (field in dataObj) 
     {
       if (Ext.isEmpty(dataObj[field])) 
       {
         delete dataObj[field];
       }
       
     }

     return dataObj;
  },
  saveReqCertificate: function(btn){
    var win = btn.up('window');
  
    var formReqcertificate = win.down('#reqcertificate-reqdoc-business-formaddedit');

      if (!formReqcertificate.isValid()) {

          var me = formReqcertificate,
          errorCmp, fields, errors;

          fields = me.getForm().getFields();
          errors = '';
          fields.each(function(field) {
              Ext.Array.forEach(field.getErrors(), function(error) {
                  errors += field.getFieldLabel()  +'<br>';
              });
          });

          Ext.Msg.show({
             title:'กรุณาตรวจสอบข้อมูล',
             msg: errors,
             buttons: Ext.Msg.OK,
             icon: Ext.Msg.ERROR
          });
          return;
      }
        
        var values = formReqcertificate.getValues();

        var dataParams = this.normalizeData(values);

         // fsubmit.submit({
       formReqcertificate.load({
            url: 'business/req/certificate/save',
            method : 'POST',
            waitMsg: 'กรุณารอสักครู่...',
            params: dataParams,
            success: function(form, action) {

              var dataObj = action.result.data;

            var noti = Ext.create('widget.uxNotification', {
              position: 'tr',
              manager: 'instructions',
              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
              closable: false,
              autoCloseDelay: 4000,
              width: 300,
              slideBackDuration: 500,
              slideInAnimation: 'bounceOut',
              slideBackAnimation: 'easeIn'
            });
            noti.show();

            //เอกสาร
            var tabPanel = formReqcertificate.down('tabpanel');
            tabPanel.child('#reqcertificate-reqdoc-business-document-grid').tab.show();

            tabPanel.setActiveTab(1);

            var grid = Ext.ComponentQuery.query('#reqdoc-business-reqcertificate-grid')[0];
            var store = grid.getStore();

            store.reload(); 

            if(win.mode == 'adddata'){

              formReqcertificate.down('#reqcertificate-reqdoc-business-printCertificate').setVisible(true);
              
              formReqcertificate.down('#reqcertificate-reqdoc-business-approveStatus-checkboxgroup').setVisible(true);
              formReqcertificate.down('#reqcertificate-reqdoc-business-approveDate').setVisible(true);

              var gridDocument = Ext.ComponentQuery.query('#reqcertificate-reqdoc-business-document-grid')[0];
                gridDocument.getStore().load({
                params: {
                    cerId: dataObj.cerId
                },
                callback: function(records, operation, success) {
                    // do something after the load finishes
                },
                scope: this
              });

            }else{
              win.close();
            }
                      
            },
            failure: function(form, action) {
              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
            },
            scope: this 
        });
  },
  handleActionColumn: function(column, action, grid, rowIndex, colIndex, model, node) {
   if(action){
        switch(action){
        
          case 'editreqcer':

          var win = Ext.create('tourism.view.reqcertificate.ReqCertificateWindow',{
              id: 'reqcertificate-window-id001',
              traderType: 'business',
              roleAction: 'reqdoc',
              mode: 'editdata'
            });
            win.show();
            
            var formCertificate = win.down('#reqcertificate-reqdoc-business-formaddedit');

            var tabPanel = formCertificate.down('tabpanel');
            var layout = win.getLayout();
            layout.setActiveItem(1);
            win.setWidth(600);

            var form = formCertificate.getForm();

            form.load({
              url: 'business/req/certificate/readById',
              method : 'POST',
              waitMsg: 'กรุณารอสักครู่...',
              params: {cerId: model.get('cerId')},
              success: function(form, action) {

                var dataObj = action.result.data;

                        
              },
              failure: function(form, action) {
                Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
              },
              scope: this 
            });

            var gridDocument = Ext.ComponentQuery.query('#reqcertificate-reqdoc-business-document-grid')[0];
            gridDocument.getStore().load({
            params: {
                cerId: model.get('cerId')
            },
            callback: function(records, operation, success) {
                // do something after the load finishes
            },
            scope: this
          });
          break;

        }
   }
        
  },
  // req docs certificate
  handleDocActionColumn: function(column, action, grid, rowIndex, colIndex, model, node) {
     if(action){
        switch(action){
         case 'addReqDoc':

                var win = Ext.create('tourism.view.reqcertificate.ReqDocWindow',{
                  id: 'reqcertificate-docreq-window-id001',
                  traderType: 'business',
                  roleAction: 'reqdoc',
                  mode: 'adddata'
                });
                win.show();

                var formDocReq = win.down('#reqcertificate-reqdoc-business-docreq-form');

                formDocReq.loadRecord(model);
         break;
         case 'viewReqDoc':
                  window.open('business/req/certificate/viewReqDoc?reqDocId='+model.get('reqDocId'),"Pic","width=600,height=600");
        break;
      }
    }
  },
  saveReqDocCertificate: function(btn){
    var win = btn.up('window');
    var formReqDoc = btn.up('form');

     if (!formReqDoc.isValid()) {

          var me = formReqDoc,
          errorCmp, fields, errors;

          fields = me.getForm().getFields();
          errors = '';
          fields.each(function(field) {
              Ext.Array.forEach(field.getErrors(), function(error) {
                  errors += field.getFieldLabel()  +'<br>';
              });
          });
         
          Ext.Msg.show({
             title:'กรุณาตรวจสอบข้อมูล',
             msg: errors,
             buttons: Ext.Msg.OK,
             icon: Ext.Msg.ERROR
          });
          return;
      }


         // fsubmit.submit({
       formReqDoc.submit({
            url: 'business/req/certificate/saveDocument',
            waitMsg: 'กรุณารอสักครู่...',
            method : 'POST',
          
            success: function(form, action) {

              var dataObj = action.result.data;
             

            var noti = Ext.create('widget.uxNotification', {
              position: 'tr',
              manager: 'instructions',
              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
              closable: false,
              autoCloseDelay: 4000,
              width: 300,
              slideBackDuration: 500,
              slideInAnimation: 'bounceOut',
              slideBackAnimation: 'easeIn'
            });
            noti.show();

              var grid = Ext.ComponentQuery.query('#reqcertificate-reqdoc-business-document-grid')[0];
              var store = grid.getStore();

              store.reload();
             
              win.close();
        
            },
            failure: function(form, action) {
              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
            },
            scope: this 
        });  
  },
  printCertificate: function(btn)
  {

    var win = btn.up('window');
    var form = btn.up('form');

    var cerId = form.getForm().findField('cerId').getValue();
     window.open('business/req/certificate/docx/reqcertificateDocx?cerId='+cerId);
  }
});