Ext.define('tourism.controller.reqpassport.ReqPassportCtrl',{
	extend : 'Ext.app.Controller',

 // traderType: 'tourleader',
 //              roleAction: 'subpassport',
  refs:[{
      ref: '#reqpassport-subpassport-tourleader-formaddedit',
      selector: 'panel'
  }, {
    ref: '#reqpassport-subpassport-tourleader-searchlicensepanel',
    selector: 'panel'
  }, {
    ref: '#reqpassport-subpassport-business-searchlicensepanel',
    selector: 'panel'
  }, {

    ref: '#subpassport-tourleader-reqpassport-grid',
    selector: '#subpassport-tourleader-reqpassport-grid'
  }, {
     ref:'#subpassport-tourleader-reqpassport-formsearch',
     selector:'panel'
  }, {
    ref: '#reqpassport-subpassport-tourleader-document-grid',
    selector: '#reqpassport-subpassport-tourleader-document-grid'
  }, {
    ref: '#reqcertificate-subpassport-tourleader-docreq-form',
    selector: 'panel'
  }],
  init : function (application) {

    this.control({
  
       '#reqpassport-subpassport-tourleader-searchlicensepanel button[action=selectLicense]': {
          click: this.selectLeaderLicenseNo
      },
      '#reqpassport-subpassport-business-searchlicensepanel button[action=selectLicense]': {
          click: this.selectTourLicenseNo
      }, 
       '#reqpassport-subpassport-tourleader-formaddedit button[action=saveReqPassport]': {
          click: this.saveReqPassport
      },
      '#reqpassport-subpassport-tourleader-formaddedit button[action=printPassport]': {
          click: this.printPassport
      },
      '#subpassport-tourleader-reqpassport-grid actioncolumn':{
          itemclick: this.handleActionColumn
      },
     
      '#reqpassport-subpassport-tourleader-document-grid actioncolumn':{
          itemclick: this.handleDocActionColumn
       },  
      '#subpassport-tourleader-reqpassport-formsearch button[action=searchReqPassport]':{
          click: this.searchReqPassport
       }, 
       '#subpassport-tourleader-reqpassport-formsearch button[action=printReport]':{
          click: this.printReport
       }, 

      '#reqcertificate-subpassport-tourleader-docreq-form button[action=saveReqDocCertificate]': {
          click: this.saveReqDocCertificate
      }
    });
   },
   printReport: function(btn){
       var form = btn.up('form'),
       values = form.getValues();

       var reqUrl = "business/req/passport/readexcel/?leaderLicenseNo="+values.leaderLicenseNo
                +'&reqIdCard='+values.reqIdCard
                +'&reqDateFrom='+values.reqDateFrom
                +'&reqDateTo='+values.reqDateTo
                +'&guideLicenseNo='+values.guideLicenseNo
                +'&passportNo='+values.passportNo
                +'&personName='+values.personName;
      window.open(reqUrl); 
   },
  searchReqPassport: function(btn) {
   var form = btn.up('form'),
   values = form.getValues();
   var dataParams = this.normalizeData(values);
   //dataParams.traderType = 'L';

   var grid = Ext.ComponentQuery.query('#subpassport-tourleader-reqpassport-grid')[0];
   var store = grid.getStore();

    dataParams.page = '1';
    dataParams.start = '0';
    dataParams.limit = store.pageSize;

     store.load({
         params: dataParams,
         callback: function(records, operation, success) {
             // //console.log(success);
         },
         scope: this
     }); 
  },  
  selectLeaderLicenseNo: function(btn){

    var win = btn.up('window');
    var grid = win.down('info-license-grid');
    var model = grid.getSelectionModel().getSelection()[0];

    var formReqpassport = win.down('#reqpassport-subpassport-tourleader-formaddedit');
    // formReqpassport.loadRecord(model);


    var layout = win.getLayout();
    layout.setActiveItem(1);

    formReqpassport.load({
        url: 'business/req/passport/readByLeaderLicense',
        method : 'POST',
        waitMsg: 'กรุณารอสักครู่...',
        params: {guideLicenseNo: model.get('licenseNo'), leaderLicenseNo: model.get('licenseNo'), traderType: model.get('traderType')},
        success: function(form, action) {

          var dataObj = action.result.data;
        },
        failure: function(form, action) {
          Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
        },
        scope: this 
    });

    formReqpassport.down('#reqpassport-subpassport-tourleader-printPassport').setVisible(false);
    var tabPanel = formReqpassport.down('tabpanel');
    tabPanel.child('#reqpassport-subpassport-tourleader-document-grid').tab.hide();

    win.setWidth(650);

  },  
  selectTourLicenseNo: function(btn){

    var win = btn.up('window');
    var grid = win.down('info-license-grid');
    var model = grid.getSelectionModel().getSelection()[0];

    var formReqpassport = Ext.ComponentQuery.query('#reqpassport-subpassport-tourleader-formaddedit')[0];

    formReqpassport.getForm().findField('traderId').setValue(model.get('traderId'));
    formReqpassport.getForm().findField('licenseNo').setValue(model.get('licenseNo'));
    formReqpassport.getForm().findField('traderName').setValue(model.get('traderName'));
    formReqpassport.getForm().findField('traderNameEn').setValue(model.get('traderNameEn'));
    formReqpassport.getForm().findField('traderCategoryName').setValue(model.get('traderCategoryName'));
    formReqpassport.getForm().findField('traderOwnerName').setValue(model.get('traderOwnerName'));
    formReqpassport.getForm().findField('identityNo').setValue(model.get('identityNo'));

    win.close();
  },
  normalizeData: function(dataObj)
  {
     // remove null data;
    for (field in dataObj) 
     {
       if (Ext.isEmpty(dataObj[field])) 
       {
         delete dataObj[field];
       }
       
     }

     return dataObj;
  },
  saveReqPassport: function(btn){
    var win = btn.up('window');
  
    var formReqpassport = win.down('#reqpassport-subpassport-tourleader-formaddedit');

      if (!formReqpassport.isValid()) {

          var me = formReqpassport,
          errorCmp, fields, errors;

          fields = me.getForm().getFields();
          errors = '';
          fields.each(function(field) {
              Ext.Array.forEach(field.getErrors(), function(error) {
                  errors += field.getFieldLabel()  +'<br>';
              });
          });

          Ext.Msg.show({
             title:'กรุณาตรวจสอบข้อมูล',
             msg: errors,
             buttons: Ext.Msg.OK,
             icon: Ext.Msg.ERROR
          });
          return;
      }
        
        var values = formReqpassport.getValues();

        var bform = formReqpassport.getForm();


        values.bookNo = bform.findField('bookNo').getValue();
        // values.bookDate = bform.findField('bookDate').getRawValue();
        // values.reqDate = bform.findField('reqDate').getRawValue();
        values.passportNo = bform.findField('passportNo').getValue();
        values.reqIdCard = bform.findField('reqIdCard').getValue();
        values.agentsAssociation = bform.findField('agentsAssociation').getValue();
        values.tourLicenseNo = bform.findField('licenseNo').getValue();
        values.guideLicenseNo = bform.findField('guideLicenseNo').getValue();
        values.leaderLicenseNo = bform.findField('leaderLicenseNo').getValue();
        values.personName = bform.findField('personName').getValue();
        values.traderName = bform.findField('traderName').getValue();

        var dataParams = this.normalizeData(values);

         // fsubmit.submit({
       formReqpassport.load({
            url: 'business/req/passport/save',
            method : 'POST',
            waitMsg: 'กรุณารอสักครู่...',
            params: dataParams,
            success: function(form, action) {

              var dataObj = action.result.data;

            var noti = Ext.create('widget.uxNotification', {
              position: 'tr',
              manager: 'instructions',
              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
              closable: false,
              autoCloseDelay: 4000,
              width: 300,
              slideBackDuration: 500,
              slideInAnimation: 'bounceOut',
              slideBackAnimation: 'easeIn'
            });
            noti.show();

            
            var grid = Ext.ComponentQuery.query('#subpassport-tourleader-reqpassport-grid')[0];
            
            var store = grid.getStore();
            

            // store.reload(); 

            if(win.mode == 'adddata'){
              var reqPassportModel = Ext.create('tourism.model.reqpassport.ReqPassportModel', {
                reqPassId: dataObj.reqPassId,
                bookNo: dataObj.bookNo,
                bookDate: dataObj.bookDate,
                passportNo: dataObj.passportNo,
                reqIdCard: dataObj.reqIdCard,
                agentsAssociation: dataObj.agentsAssociation,
                tourLicenseNo: dataObj.tourLicenseNo,
                guideLicenseNo: dataObj.guideLicenseNo,
                leaderLicenseNo: dataObj.leaderLicenseNo,
                personName: dataObj.personName,
                traderName: dataObj.traderName
            });

              store.insert( 0, reqPassportModel );
              grid.getSelectionModel().select(reqPassportModel);
              store.commitChanges( );

              formReqpassport.getForm().findField('reqPassId').setValue(dataObj.reqPassId);

              // เอกสาร
              var tabPanel = formReqpassport.down('tabpanel');
              tabPanel.child('#reqpassport-subpassport-tourleader-document-grid').tab.show();
              tabPanel.setActiveTab(1);
              formReqpassport.down('#reqpassport-subpassport-tourleader-printPassport').setVisible(true);


              var gridDocument = Ext.ComponentQuery.query('#reqpassport-subpassport-tourleader-document-grid')[0];
              gridDocument.getStore().setProxy({

                type: 'ajax',
                actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
                api: {
                    read: 'business/req/passport/readDocument'
                },
                reader: {
                  type: 'json',
                  root: 'list',
                  totalProperty: 'totalCount',
                  successProperty: 'success',
                  messageProperty: 'message'
                },
                listeners: {
                    exception: function(proxy, response, operation){

                        Ext.MessageBox.show({
                            title: 'REMOTE EXCEPTION',
                            msg: operation.getError(),
                            icon: Ext.MessageBox.ERROR,
                            buttons: Ext.Msg.OK
                        });
                    }
                }
            });

            gridDocument.getStore().load({
                scope: this,
                params: {reqPassId: dataObj.reqPassId }
            });

          }else{
            var model = formReqpassport.getRecord();
            grid.getSelectionModel().select(model);
            win.close();
          }
            
                      
            },
            failure: function(form, action) {
              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
            },
            scope: this 
        });
  },
  handleActionColumn: function(column, action, grid, rowIndex, colIndex, model, node) {
   if(action){
        switch(action){
        
          case 'editreqcer':

            var win = Ext.create('tourism.view.reqpassport.ReqPassportWindow',{
              traderType: 'tourleader',
              roleAction: 'subpassport',
              mode: 'editdata'
            });
            win.show();
          
            var formReqpassport = win.down('#reqpassport-subpassport-tourleader-formaddedit');

            var tabPanel = formReqpassport.down('tabpanel');
            var layout = win.getLayout();
            layout.setActiveItem(1);
            win.setWidth(650);

            var form = formReqpassport.getForm();

            form.load({
              url: 'business/req/passport/readById',
              method : 'POST',
              waitMsg: 'กรุณารอสักครู่...',
              params: {reqPassId: model.get('reqPassId')},
              success: function(form, action) {

                var dataObj = action.result.data;

                form.findField('licenseNo').setValue(dataObj.tourLicenseNo);
                form.findField('leaderLicenseNo').setDisabled(true);

                var tourFieldSet = formReqpassport.down('#reqpassport-addeditform-business-fieldset');
                tourFieldSet.expand();
              },
              failure: function(form, action) {
                Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
              },
              scope: this 
            });

            var gridDocument = Ext.ComponentQuery.query('#reqpassport-subpassport-tourleader-document-grid')[0];
            gridDocument.getStore().setProxy({

                type: 'ajax',
                actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
                api: {
                    read: 'business/req/passport/readDocument'
                },
                reader: {
                  type: 'json',
                  root: 'list',
                  totalProperty: 'totalCount',
                  successProperty: 'success',
                  messageProperty: 'message'
                },
                listeners: {
                    exception: function(proxy, response, operation){

                        Ext.MessageBox.show({
                            title: 'REMOTE EXCEPTION',
                            msg: operation.getError(),
                            icon: Ext.MessageBox.ERROR,
                            buttons: Ext.Msg.OK
                        });
                    }
                }
            });

            gridDocument.getStore().load({
                scope: this,
                params: {reqPassId: model.get('reqPassId')}
            });
        break;

        }
   }
        
  },
  // req docs certificate
  handleDocActionColumn: function(column, action, grid, rowIndex, colIndex, model, node) {
     if(action){
        switch(action){
         case 'addReqDoc':

                var win = Ext.create('tourism.view.reqcertificate.ReqDocWindow',{
                  id: 'reqpassport-docreq-window-id001',
                  traderType: 'tourleader',
                  roleAction: 'subpassport',
                  mode: 'adddata'
                });
                win.show();

                var formDocReq = win.down('#reqcertificate-subpassport-tourleader-docreq-form');

                formDocReq.loadRecord(model);

                // var form = formDocReq.getForm();
                // form.findField('reqDocId').setValue(model.get('reqPassDocId'));
                // form.findField('documentName').setValue(model.get('documentName'));

                // console.log(model.get('reqPassDocId'));
                
         break;
         case 'viewReqDoc':
                  window.open('business/req/passport/viewReqDoc?reqPassDocId='+model.get('reqDocId'),"Pic","width=600,height=600");
        break;
      }
    }
  },
  saveReqDocCertificate: function(btn){
    var win = btn.up('window');
    var formReqDoc = btn.up('form');

     if (!formReqDoc.isValid()) {

          var me = formReqDoc,
          errorCmp, fields, errors;

          fields = me.getForm().getFields();
          errors = '';
          fields.each(function(field) {
              Ext.Array.forEach(field.getErrors(), function(error) {
                  errors += field.getFieldLabel()  +'<br>';
              });
          });
         
          Ext.Msg.show({
             title:'กรุณาตรวจสอบข้อมูล',
             msg: errors,
             buttons: Ext.Msg.OK,
             icon: Ext.Msg.ERROR
          });
          return;
      }


         // fsubmit.submit({
       formReqDoc.submit({
            url: 'business/req/passport/saveDocument',
            waitMsg: 'กรุณารอสักครู่...',
            method : 'POST',
          
            success: function(form, action) {

              // var dataObj = action.result.data;
             

            var noti = Ext.create('widget.uxNotification', {
              position: 'tr',
              manager: 'instructions',
              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
              closable: false,
              autoCloseDelay: 4000,
              width: 300,
              slideBackDuration: 500,
              slideInAnimation: 'bounceOut',
              slideBackAnimation: 'easeIn'
            });
            noti.show();

            var grid = Ext.ComponentQuery.query('#reqpassport-subpassport-tourleader-document-grid')[0];
            var store = grid.getStore();
            store.reload();
            win.close();
        
            },
            failure: function(form, action) {
              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
            },
            scope: this 
        });  
  },
  printPassport: function(btn)
  {

    var win = btn.up('window');
    var form = btn.up('form');

    var reqPassId = form.getForm().findField('reqPassId').getValue();
     window.open('business/req/passport/docx/passportDocx?reqPassId='+reqPassId);
  }
});