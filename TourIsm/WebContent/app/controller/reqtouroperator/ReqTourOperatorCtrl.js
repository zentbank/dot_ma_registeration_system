Ext.define('tourism.controller.reqtouroperator.ReqTourOperatorCtrl',{
  extend : 'Ext.app.Controller',

  // traderType: 'tourleader',
  //              roleAction: 'subpassport',
  refs:[{
    ref: '#reqtouroperator-reqtouroperator-officer-formaddedit',
    selector: 'panel'
  },{
    ref: '#reqtouroperator-officer-reqtouroperator-formsearch',
    selector: 'panel'
  },{
    ref: '#reqtouroperator-reqtouroperator-officer-document-grid',
    selector: '#reqtouroperator-reqtouroperator-officer-document-grid'
  },{
      ref: '#reqtouroperator-officerdoc-reqtouroperator-docreq-form',
      selector: '#reqtouroperator-officerdoc-reqtouroperator-docreq-formd'
  },{
      ref: '#reqtouroperator-officer-reqtouroperator-grid',
      selector: '#reqtouroperator-officer-reqtouroperator-grid'
  }],
  init : function (application) {

    this.control({

      '#reqtouroperator-reqtouroperator-officer-formaddedit button[action=saveReqTourOpt]': {
        click: this.saveReqTourOpt
      },
      '#reqtouroperator-officer-reqtouroperator-formsearch button[action=searchReqTourOperator]': {
        click: this.searchReqTourOperator
      },
      '#reqtouroperator-reqtouroperator-officer-document-grid actioncolumn':{
        itemclick: this.handleDocActionColumn
      },
        '#reqtouroperator-officerdoc-reqtouroperator-docreq-form button[action=saveReqDoc]': {
      click: this.saveReqDoc
        },'#reqtouroperator-officer-reqtouroperator-grid actioncolumn':{
            itemclick: this.handleActionColumn
        },
      '#reqtouroperator-reqtouroperator-officer-formaddedit button[action=printTourOperator]': {
        click: this.printTourOperator
    }

    });
  }
  ,normalizeData: function(dataObj)
  {
    // remove null data;
    for (field in dataObj)
    {
      if (Ext.isEmpty(dataObj[field]))
      {
        delete dataObj[field];
      }

    }

    return dataObj;
  },
  searchReqTourOperator: function(btn) {
    var form = btn.up('form'),
        values = form.getValues();
    var dataParams = this.normalizeData(values);
    //dataParams.traderType = 'L';
    var grid = Ext.ComponentQuery.query('#reqtouroperator-officer-reqtouroperator-grid')[0];
    var store = grid.getStore();

    dataParams.page = '1';
    dataParams.start = '0';
    dataParams.limit = store.pageSize;

    store.load({
      params: dataParams,
      callback: function(records, operation, success) {
        // //console.log(success);
      },
      scope: this
    });
  },
  saveReqTourOpt: function(btn) {
    var win = btn.up('window');

  var formReqtouroperator = win.down('#reqtouroperator-reqtouroperator-officer-formaddedit');

    if (!formReqtouroperator.isValid()) {

      var me = formReqtouroperator,
          errorCmp, fields, errors;

      fields = me.getForm().getFields();
      errors = '';
      fields.each(function(field) {
        Ext.Array.forEach(field.getErrors(), function(error) {
          errors += field.getFieldLabel()  +'<br>';
        });
      });

      Ext.Msg.show({
        title:'กรุณาตรวจสอบข้อมูล',
        msg: errors,
        buttons: Ext.Msg.OK,
        icon: Ext.Msg.ERROR
      });
      return;
    }

    var values = formReqtouroperator.getValues();

    var dataParams = this.normalizeData(values);

    // fsubmit.submit({
    formReqtouroperator.load({
      url: 'business/reqtouroperator/save',
      method : 'POST',
      waitMsg: 'กรุณารอสักครู่...',
      params: dataParams,
      success: function(form, action) {

        var dataObj = action.result.data;

        var noti = Ext.create('widget.uxNotification', {
          position: 'tr',
          manager: 'instructions',
          html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
          closable: false,
          autoCloseDelay: 4000,
          width: 300,
          slideBackDuration: 500,
          slideInAnimation: 'bounceOut',
          slideBackAnimation: 'easeIn'
        });
        noti.show();

        //เอกสาร
        var tabPanel = formReqtouroperator.down('tabpanel');
        tabPanel.child('#reqtouroperator-reqtouroperator-officer-document-grid').tab.show();

        tabPanel.setActiveTab(1);

        var grid = Ext.ComponentQuery.query('#reqtouroperator-officer-reqtouroperator-grid')[0];
        var store = grid.getStore();

        store.reload();

        if(win.mode == 'adddata'){

          formReqtouroperator.down('#reqtouroperator-reqtouroperator-officer-printReqTourOpt').setVisible(true);
          var gridDocument = Ext.ComponentQuery.query('#reqtouroperator-reqtouroperator-officer-document-grid')[0];
          gridDocument.getStore().load({
            params: {
              taxId: dataObj.taxId
            },
            callback: function(records, operation, success) {
              // do something after the load finishes
            },
            scope: this
          });

        }else{
          win.close();
        }

      },
      failure: function(form, action) {
        Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
      },
      scope: this
    });
  },
  // req docs certificate
  handleDocActionColumn: function(column, action, grid, rowIndex, colIndex, model, node) {
    if(action){
      switch(action){
        case 'addReqDoc':

          var win = Ext.create('tourism.view.reqtouroperator.ReqTourOptDocWindow',{
            id: 'reqtouroperator-docreq-window-id001',
            traderType: 'reqtouroperator',
            roleAction: 'officerdoc',
            mode: 'adddata'
          });
          win.show();

          var formDocReq = win.down('#reqtouroperator-officerdoc-reqtouroperator-docreq-form');


          formDocReq.loadRecord(model);
          break;
        case 'viewReqDoc':
          window.open('business/reqtouroperator/doc/viewReqDoc?reqDocId='+model.get('reqDocId'),"Pic","width=600,height=600");
          break;
      }
    }
  },
  saveReqDoc: function(btn){
    var win = btn.up('window');
      var formReqDoc = win.down('#reqtouroperator-officerdoc-reqtouroperator-docreq-form');

    if (!formReqDoc.isValid()) {

      var me = formReqDoc,
          errorCmp, fields, errors;

      fields = me.getForm().getFields();
      errors = '';
      fields.each(function(field) {
        Ext.Array.forEach(field.getErrors(), function(error) {
          errors += field.getFieldLabel()  +'<br>';
        });
      });

      Ext.Msg.show({
        title:'กรุณาตรวจสอบข้อมูล',
        msg: errors,
        buttons: Ext.Msg.OK,
        icon: Ext.Msg.ERROR
      });
      return;
    }


    // fsubmit.submit({
    formReqDoc.submit({
      url: 'business/reqtouroperator/doc/saveDocument',
      waitMsg: 'กรุณารอสักครู่...',
      method : 'POST',

      success: function(form, action) {

        var dataObj = action.result.data;


        var noti = Ext.create('widget.uxNotification', {
          position: 'tr',
          manager: 'instructions',
          html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
          closable: false,
          autoCloseDelay: 4000,
          width: 300,
          slideBackDuration: 500,
          slideInAnimation: 'bounceOut',
          slideBackAnimation: 'easeIn'
        });
        noti.show();

        var grid = Ext.ComponentQuery.query('#reqtouroperator-reqtouroperator-officer-document-grid')[0];
        var store = grid.getStore();

        store.reload();

        win.close();

      },
      failure: function(form, action) {
        Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
      },
      scope: this
    });
  },
  handleActionColumn: function(column, action, grid, rowIndex, colIndex, model, node) {


    var win = Ext.create('tourism.view.reqtouroperator.ReqTourOperatorWindow',{
      id: 'reqtouroperator-tour-window-id001',
      traderType: 'officer',
      roleAction: 'reqtouroperator',
      mode: 'editdata'
    });
    win.show();

    var formreqtouroperator = win.down('#reqtouroperator-reqtouroperator-officer-formaddedit');

//    formreqtouroperator.down('#reqtouroperator-reqtouroperator-officer-printReqTourOpt').setVisible(true);
//    var tabPanel = formreqtouroperator.down('tabpanel');
//    tabPanel.child('#reqtouroperator-reqtouroperator-officer-document-grid').tab.hide();

//    var tabPanel = formreqtouroperator.down('tabpanel');
//    var layout = win.getLayout();
//    layout.setActiveItem(1);
//    win.setWidth(600);

    var form = formreqtouroperator.getForm();

    form.load({
      url: 'business/reqtouroperator/readById',
      method : 'POST',
      waitMsg: 'กรุณารอสักครู่...',
      params: {taxId: model.get('taxId')},
      success: function(form, action) {

        var dataObj = action.result.data;

      },
      failure: function(form, action) {
        Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
      },
      scope: this
    });

    var gridDocument = Ext.ComponentQuery.query('#reqtouroperator-reqtouroperator-officer-document-grid')[0];
    gridDocument.getStore().load({
      params: {
        taxId: model.get('taxId')
      },
      callback: function(records, operation, success) {
        // do something after the load finishes
      },
      scope: this
    });
  },
  printTourOperator: function(btn)
  {

    var win = btn.up('window');
    var form = btn.up('form');

    var taxId = form.getForm().findField('taxId').getValue();
    window.open('business/reqtouroperator/docx?taxId='+taxId);
  }
});
