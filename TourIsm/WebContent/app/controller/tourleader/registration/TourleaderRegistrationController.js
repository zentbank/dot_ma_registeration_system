Ext.define('tourism.controller.tourleader.registration.TourleaderRegistrationController',{
	extend: 'Ext.app.Controller',
	stores: ['tourism.store.tourleader.registration.TourleaderRegistrationStore'],
	models: ['tourism.model.tourleader.registration.TourleaderRegistrationModel'],
	
	views: ['tourism.view.tourleader.registration.TourleaderRegistrationGrid'
	        ,'tourism.view.tourleader.registration.TourleaderRegistrationFormSearch'
          ,'tourism.view.window.verification.VerificationForm'
	],
	
	refs: [    
	    	{
	            ref: 'tourleader_registrationgrid',
	            selector: 'tourleader_registrationgrid'
	        }
	    	  ,{
	            ref: 'tourleader_registrationformsearch',
	            selector: 'panel'
	        }
          ,{
              ref: '#tourleader-registration-verification-form',
              selector: 'panel'
          }
          ,{
              ref: '#key-addedit-tourleader-registration-form',
              selector: 'panel'
          }
	],	 
	init : function (application) {
        this.control({
            'tourleader_registrationgrid button[action=add]': {
            	click: this.showTourleaderRegistrationWindow
            }
            
            ,'tourleader_registrationformsearch button[action=searchTraderBetweenRegistration]':{
            	click: this.searchTraderBetweenRegistration
            }
            ,'tourleader_registrationgrid actioncolumn': {
                itemclick: this.handleActionColumn
            },
            '#tourleader-registration-verification-form button[action=saveVerification]': {
                click: this.saveVerification
            },
            
            '#key-addedit-tourleader-registration-form button[action=saveTourleaderRegistration]':{
              click: this.saveTourleaderRegistration
            },
            '#key-addedit-tourleader-registration-form button[action=loadDataFromIdCard]':{
              click: this.loadDataFromIdCard
            }
            ,
            '#key-addedit-tourleader-registration-form button[action=searchUserFromIDCardOnline]': {
                click: this.searchUserFromIDCardOnline
            }
           
            
        });
	}
	
	,handleActionColumn : function(column, action, grid, rowIndex, colIndex, model, node) {
		if(action){
            switch(action){
            
                case 'editregistration': 
               
                	var edit = Ext.create('tourism.view.tourleader.registration.TourleaderRegistrationAddEditWindow',{
                        animateTarget: column.el,
                        actionMode: 'edit',
                        roleAction: 'key',
                        traderType: 'tourleader',
                        verificationGrid: grid
                    }).show();
                	
                	if(model && (model instanceof tourism.model.business.registration.BusinessRegistrationModel)){
                        
                        var freg = edit.down('form');
                        freg.loadRecord(model);

                        var imageFieldSet = edit.down('image');
                        // console.log(model);
                        imageFieldSet.setSrc("data:image/png;base64,"+model.get("imageFile"));

                        //Province
                        if(!Ext.isEmpty(model.get('provinceId')))
                        {
                          var provinceCombo = freg.getForm().findField('provinceId');
                          provinceCombo.getStore().on('load',function(store, records, successful, eOpts){
                            provinceCombo.setValue(model.get('provinceId'));
                          },this,{single:true});
                          provinceCombo.getStore().load();
                        }
                        else
                        {
                          var provinceCombo = freg.getForm().findField('provinceId');
                          provinceCombo.clearValue();
                        }
                        
                        //Amphur
                        if(!Ext.isEmpty(model.get('amphurId')))
                    	{
                    	  var amphurCombo = freg.getForm().findField('amphurId'); 
                    	  amphurCombo.getStore().on('load',function(){
                    		  
                    	  },this,{single:true});
                    	  amphurCombo.getStore().load();
                        }
                        else
                    	{
                        	var amphurCombo = freg.getForm().findField('amphurId'); 
                        	amphurCombo.clearValue();
                    	}

                        //Prefix
                        if(!Ext.isEmpty(model.get('prefixId')))
                        {
                          var prefixIdCombo = freg.getForm().findField('prefixId');
                          var prefixIdEnCombo = freg.getForm().findField('prefixIdEn');
                          prefixIdCombo.getStore().on('load',function(store, records, successful, eOpts){
                            prefixIdCombo.setValue(model.get('prefixId'));
                            prefixIdEnCombo.setValue(model.get('prefixId'));
                          },this,{single:true});
                          prefixIdCombo.getStore().load();
                        }
                        else
                        {
                          var prefixIdCombo = freg.getForm().findField('prefixId');
                          var prefixIdEnCombo = freg.getForm().findField('prefixIdEn');
                          prefixIdCombo.clearValue();
                          prefixIdEnCombo.clearValue();
                        }
                        
                        //ที่อยู่
                        var traderaddressgrid = Ext.ComponentQuery.query('tourleader_registration_registrationTab_traderaddressgrid')[0];
                        
                        traderaddressgrid.getStore().load({
                            params: {traderId: model.get('traderId'), recordStatus: model.get('traderRecordStatus')},
                            callback: function(records, operation, success) {
                            	//console.log(records);
                            
                            },
                            scope: this
                        }); 
                        
                        //ประเภทการจดทะเบียน
                    	if(model.get('traderCategory') == '200')
                		{
                		  var traderGuideDetail = model.get('traderGuideDetail');
                		  var label200 = Ext.getCmp('tourleader_registration_tourleaderType_200_html');
              			  label200.setText(traderGuideDetail);

                		}
                    	if(model.get('traderCategory') == '300')
                		{
                		  var traderTrainedDetail = model.get('traderTrainedDetail');
                		  var label300 = Ext.getCmp('tourleader_registration_tourleaderType_300_html');
              			  label300.setText(traderTrainedDetail);
                		}
                    	
                    	//การศึกษา
                    	var educationgrid = Ext.ComponentQuery.query('window_registration_registrationTab_education_grid')[0];
                    	educationgrid.getStore().load({
                            params: {personId: model.get('personId'), educationType: 'E', recordStatus: model.get('traderRecordStatus')},
                            callback: function(records, operation, success) 
                            {
                            	//console.log(records);                            
                            },
                            scope: this
                        });
                    	
                    	//ภาษา
                    	var languagegrid = Ext.ComponentQuery.query('window_registration_registrationTab_language_grid')[0];
                    	languagegrid.getStore().load({
                            params: {personId: model.get('personId'), recordStatus: model.get('traderRecordStatus')},
                            callback: function(records, operation, success) {
                        
                            },
                            scope: this
                        });
                    	
                    	
                	}
                	
                break;

                case 'verification': 
                	
                var edit = Ext.create('tourism.view.window.verification.VerificationWindow',{
                 id: 'tourleader-registration-verification-form',
                 animateTarget: column.el,
                 actionMode: 'verification'
                }).show();

                var form = edit.down('form');
                form.getForm().findField('regId').setValue(model.get('regId'));
              break;
              
                case 'informationregistration': 
                	var edit = Ext.create('tourism.view.information.registration.tourleader.InformationRegistrationTourleaderWindow',{
                        animateTarget: column.el,
                        actionMode: 'edit'
                    }).show();
                	
                	break;

              case 'progress':

                    var progressWin = Ext.create('tourism.view.registrationprogress.RegistrationProgressWindow',{
                      id: 'tourleader-registrationprogress-ProgressWindow',
                      animateTarget: column.el 
                      
                     });

                    progressWin.on('show', function(win){


                      var grid = win.down('registrationprogress-grid');
                     
                      grid.getStore().load({
                        params: {regId: model.get('regId')},
                        callback: function(records, operation, success) {
                            
                        },
                        scope: this
                      }); 

                    },this);

                    progressWin.show();

                  break;

             case 'viewregistration':
                   
                  var winInfo = Ext.create('tourism.view.information.tourleader.InfoTourleaderWindow',{
                	  id: 'supervisor-information-tourleader-registration-infowindow',
                	  animateTarget: column.el,
                      actionMode: 'information',
                      roleAction: 'supervisor'
                      ,registrationType: model.get('registrationType')
                	}).show();
                	
                	//console.log(model);
                	
                	winInfo.loadFormRecord(model);
                	
             break;
            }
		}
		
	}
	
	,searchTraderBetweenRegistration: function(btn){
		var form = btn.up('form');
		var values = form.getValues();

		
		var dataParams = this.normalizeData(values);	
		
		//Oat Fix
		dataParams.traderType = 'L';

				
		var grid = Ext.ComponentQuery.query('tourleader_registrationgrid')[0];
		var store = grid.getStore();
		
		dataParams.page = '1';
	    dataParams.start = '0';
	    dataParams.limit = store.pageSize;
		
		store.load({
			params:  dataParams,
			callback: function(records, operation, success) {
	          //console.log(records);
	        },
	        scope: this
		});

	}
	
	,showTourleaderRegistrationWindow: function(btn) {
		var edit = Ext.create('tourism.view.tourleader.registration.registrationType.RegistrationTypeAddEditWindow',{animateTarget: btn.el}).show();  			   
    }
    
    ,saveTourleaderRegistration: function(btn, model){
    	var win = btn.up('window');
    	var form = win.down('form');
    	
    	var model = form.getRecord(); //Ext.data.Model
        var values = form.getValues();//object {xx: q,ss:ff}

        
        if (!form.isValid()) {

            var me = form,
            errorCmp, fields, errors;

            fields = me.getForm().getFields();
            errors = '';
            fields.each(function(field) {
                Ext.Array.forEach(field.getErrors(), function(error) {
                    // errors.push({name: field.getFieldLabel(), error: error});
                    errors += field.getFieldLabel()  +'<br>';
                });
            });
            // Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', errors);

            Ext.Msg.show({
               title:'กรุณาตรวจสอบข้อมูล',
               msg: errors,
               buttons: Ext.Msg.OK,
               icon: Ext.Msg.ERROR
            });
            return;
        }
        
        var fsubmit = form.getForm();
        values.personType = 'I';
        
        var dataParams = this.normalizeData(values);
        
        
        fsubmit.load({
            url: 'business/registration/saveAllRegistration',
            method : 'POST',
            waitMsg: 'กรุณารอสักครู่...',
            params: dataParams,
            success: function(form, action) {

              var dataObj = action.result.data;
             
              // Ext.Msg.alert('', 'บันทึกข้อมูล');

            var noti = Ext.create('widget.uxNotification', {
              // title: 'Notification',
              position: 'tr',
              manager: 'instructions',
              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
              closable: false,
              autoCloseDelay: 4000,
              width: 300,
              slideBackDuration: 500,
              slideInAnimation: 'bounceOut',
              slideBackAnimation: 'easeIn'
            });
            noti.show();

              var grid = Ext.ComponentQuery.query('tourleader_registrationgrid')[0];
              var store = grid.getStore();

              if(win.actionMode == 'edit')
              {
                store.reload();
              }else
              {
                store.load({
                    params: {traderId: dataObj.traderId},
                    callback: function(records, operation, success) {
                        
                    },
                    scope: this
                }); 
              }

              win.close();
        
            },
            failure: function(form, action) {
              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
            },
            scope: this 
        });
        
    },
    normalizeData: function(dataObj)
    {
        // remove null data;
       for (field in dataObj) 
        {
          if (Ext.isEmpty(dataObj[field])) 
          {
            delete dataObj[field];
          }
          
        }

        return dataObj;
    },
      saveVerification: function(btn)
      {
        var form    = btn.up('form');
        var win = btn.up('window');
        var values = form.getValues();

        if (!form.isValid()) {

            var me = form,errorCmp, fields, errors;

            fields = me.getForm().getFields();
            errors = '';
            fields.each(function(field) {
                Ext.Array.forEach(field.getErrors(), function(error) {
                    errors += field.getFieldLabel()  +'<br>';
                });
            });

            Ext.Msg.show({
               title:'กรุณาตรวจสอบข้อมูล',
               msg: errors,
               buttons: Ext.Msg.OK,
               icon: Ext.Msg.ERROR
            });
            return;
        }


        var dataParams = this.normalizeData(values);

        var url = 'tourleader/key/registration/verification';
        if(values.progressStatus == '0'){
        	url = 'business/registration/requireDocs';
        }

         // fsubmit.submit({
       form.load({
            url: url,
            method : 'POST',
            waitMsg: 'กรุณารอสักครู่..',
            params: dataParams,
            success: function(form, action) {

              var dataObj = action.result.data;
             
              // Ext.Msg.alert('', 'บันทึกข้อมูล');

            var noti = Ext.create('widget.uxNotification', {
              // title: 'Notification',
              position: 'tr',
              manager: 'instructions',
              html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
              closable: false,
              autoCloseDelay: 4000,
              width: 300,
              slideBackDuration: 500,
              slideInAnimation: 'bounceOut',
              slideBackAnimation: 'easeIn'
            });
            noti.show();

              var grid = Ext.ComponentQuery.query('tourleader_registrationgrid')[0];
              var store = grid.getStore();

             store.reload();
             
              win.close();
        
            },
            failure: function(form, action) {
              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
            },
            scope: this 
        });
      }
    ,searchUserFromIDCardOnline: function(btn){
		var form = btn.up('form');
        var win = btn.up('window');
        var idcard = form.getForm().findField('identityNo').getValue();
        //-------- ใช้ mockup url เนื่องจากรอกรมท่องเที่ยวกับกรมการปกครอง เชื่อมโยงระบบกัน
		var url = "http://27.254.57.223:8083/mockup/requestPersonData/"+idcard;
        var dataParams = "";
        form.load({
            url: url,
            method : 'GET',
            params: dataParams,
            waitMsg: 'กรุณารอสักครู่...',
            success: function(form, action) {
          	  var dataObj = action.result.data;
          	  console.log(dataObj);
          	  if("" != dataObj){
          		  form.findField('identityNo').setValue(dataObj.idcard);
                    var fullNameTH = dataObj.fullNameTH;
                    var fullNameEN = dataObj.fullNameEN;
                    var arrFullNameTH = fullNameTH.split(" ");
                    var arrFullNameEN = fullNameEN.split(" ");
                    form.findField('firstName').setValue(arrFullNameTH[1]);
                    form.findField('lastName').setValue(arrFullNameTH[3]);
                    form.findField('firstNameEn').setValue(arrFullNameEN[1]);
                    form.findField('lastNameEn').setValue(arrFullNameEN[3]);

                    var prefixIdCombo = form.findField('prefixId');
                    var prefixIdEnCombo = form.findField('prefixIdEn');
                    prefixIdCombo.getStore().on('load',function(store, records, successful, eOpts){
                      var prefixName = arrFullNameTH[0];
                      var prefixId = "";
                      if(prefixName == 'น.ส.' || prefixName == 'น.ส' || prefixName == 'นส.' || prefixName == 'นส' || prefixName == 'นางสาว'){
                        prefixId = 2;
                      }else if(prefixName == 'นาย'){
                        prefixId = 1;
                      }
                      prefixIdCombo.setValue(prefixId);
                      prefixIdEnCombo.setValue(prefixId);
                    },this,{single:true});
                    prefixIdCombo.getStore().load();

                    var gender = dataObj.gender;
                    if(gender == '1'){
                      form.findField('gender').setValue('M');
                    }else if(gender == '2'){
                      form.findField('gender').setValue('F');
                    }
                    var birthDate = dataObj.birthDate;
                    var formatedBirthDate = birthDate.substring(6,8)+"/"+birthDate.substring(4,6)+"/"+birthDate.substring(0,4);
                    form.findField('birthDate').setValue(formatedBirthDate);

                    var birthDateInDate = new Date(birthDate.substring(0,4),birthDate.substring(4,6),birthDate.substring(6,8));
                    var currentDate = new Date();
                    form.findField('ageYear').setValue(currentDate.getFullYear()-(birthDateInDate.getFullYear()-543));
                    
                    var expireDate = dataObj.expireDate;
                    var formatedExpireDate = expireDate.substring(6,8)+"/"+expireDate.substring(4,6)+"/"+expireDate.substring(0,4);
                    form.findField('identityNoExpire').setValue(formatedExpireDate);
          	  }else{
          		  Ext.Msg.alert('ไม่พบข้อมูล','Not found user');
          	  }
            },
            failure: function(form, action) {
              Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
            },
            scope: this 
        });
	  }
    , loadDataFromIdCard: function(btn){
          var url = "http://localhost:8088/cardReader/read/idcard";
          var form = btn.up('form');
          var win = btn.up('window');
          var dataParams = "";
          form.load({
              url: url,
              method : 'GET',
              params: dataParams,
              waitMsg: 'กรุณารอสักครู่...',
              success: function(form, action) {
                var dataObj = action.result.data;
                console.log(dataObj);
                form.findField('identityNo').setValue(dataObj.idcard);
                var fullNameTH = dataObj.fullNameTH;
                var fullNameEN = dataObj.fullNameEN;
                var arrFullNameTH = fullNameTH.split(" ");
                var arrFullNameEN = fullNameEN.split(" ");
                form.findField('firstName').setValue(arrFullNameTH[1]);
                form.findField('lastName').setValue(arrFullNameTH[3]);
                form.findField('firstNameEn').setValue(arrFullNameEN[1]);
                form.findField('lastNameEn').setValue(arrFullNameEN[3]);

                var prefixIdCombo = form.findField('prefixId');
                var prefixIdEnCombo = form.findField('prefixIdEn');
                prefixIdCombo.getStore().on('load',function(store, records, successful, eOpts){
                  var prefixName = arrFullNameTH[0];
                  var prefixId = "";
                  if(prefixName == 'น.ส.' || prefixName == 'น.ส' || prefixName == 'นส.' || prefixName == 'นส' || prefixName == 'นางสาว'){
                    prefixId = 2;
                  }else if(prefixName == 'นาย'){
                    prefixId = 1;
                  }
                  prefixIdCombo.setValue(prefixId);
                  prefixIdEnCombo.setValue(prefixId);
                },this,{single:true});
                prefixIdCombo.getStore().load();

                var gender = dataObj.gender;
                if(gender == '1'){
                  form.findField('gender').setValue('M');
                }else if(gender == '2'){
                  form.findField('gender').setValue('F');
                }
                var birthDate = dataObj.birthDate;
                var formatedBirthDate = birthDate.substring(6,8)+"/"+birthDate.substring(4,6)+"/"+birthDate.substring(0,4);
                form.findField('birthDate').setValue(formatedBirthDate);

                var birthDateInDate = new Date(birthDate.substring(0,4),birthDate.substring(4,6),birthDate.substring(6,8));
                var currentDate = new Date();
                form.findField('ageYear').setValue(currentDate.getFullYear()-(birthDateInDate.getFullYear()-543));
                var imageFile = win.down('image');
                console.log(imageFile);
                form.findField('imageFile').setValue(dataObj.imageRaw);
                imageFile.setSrc("data:image/png;base64,"+dataObj.imageRaw);

                var expireDate = dataObj.expireDate;
                var formatedExpireDate = expireDate.substring(6,8)+"/"+expireDate.substring(4,6)+"/"+expireDate.substring(0,4);
                form.findField('identityNoExpire').setValue(formatedExpireDate);
                
              },
              failure: function(form, action) {
                Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
              },
              scope: this 
          });
    }
    
    
    
});

















