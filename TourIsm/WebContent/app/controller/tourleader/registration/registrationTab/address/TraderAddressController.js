Ext.define('tourism.controller.tourleader.registration.registrationTab.address.TraderAddressController',{
	extend: 'Ext.app.Controller',
	stores: ['tourism.store.business.registration.address.TraderAddressStore'],
	models: ['tourism.model.business.registration.address.TraderAddressModel'],
	views: [
	        'tourism.view.tourleader.registration.registrationTab.address.TraderAddressGrid'
	        ,'tourism.view.tourleader.registration.registrationTab.address.TraderAddressAddEditForm'
	],
	refs: [    
	{
        ref: 'tourleader_registration_registrationTab_traderaddressgrid',
        selector: 'tourleader_registration_registrationTab_traderaddressgrid'
    },
    {
        ref: 'tourleader_registration_address_traderaddressaddedit_form',
        selector: 'tourleader_registration_address_traderaddressaddedit_form'
    }
	],	
	init: function(application){
		this.control({
			'tourleader_registration_registrationTab_traderaddressgrid button[action=addTraderAddress]':{
				click: this.showTraderAddressAddEditWindow
			},
            'tourleader_registration_address_traderaddressaddedit_form button[action=saveTraderAddress]': {
                click: this.saveTraderAddress
            },
            'tourleader_registration_registrationTab_traderaddressgrid actioncolumn': {
                itemclick: this.handleActionColumn
            }
		});
	},
	showTraderAddressAddEditWindow: function(grid, model){
		var edit = Ext.create('tourism.view.tourleader.registration.registrationTab.address.TraderAddressAddEditWindow').show();
		var registrationForm = Ext.ComponentQuery.query('form-tourleader-registration-addedit')[0];
		var fullAddress = registrationForm.getForm().findField('fullAddress').getValue();
		if(fullAddress != ""){
			var form = edit.down('form');
			form.getForm().findField('fullAddress').setValue(fullAddress);
			var arrFullAddress = fullAddress.split(" ");
			//-----[0] เลขที่
			//-----[1] หมู่ที่ เพราะว่ามีเว้นวรรค 1 ที่หลังคำหว่าหมู่ที่ ทำให้ลำดับขยับ
			form.getForm().findField('addressNo').setValue(arrFullAddress[0]);
			if(arrFullAddress[1] != ""){
				//-----[3] ??
				//-----[4] ??
				//-----[5] ถนน
				//-----[6] ตำบล/แขวง
				//-----[7] อำเภอ/เขต
				//-----[8] จังหวัด
				form.getForm().findField('moo').setValue(arrFullAddress[2]);
				if(arrFullAddress[5].search("ถนน") != -1){
					form.getForm().findField('roadName').setValue(arrFullAddress[5].replace("ถนน",""));
				}
				if(arrFullAddress[8].search("จังหวัด") != -1){
					var provinceCombo = form.getForm().findField('provinceId');
					var selectedProvinceId = "";
					provinceCombo.getStore().on('load',function(store, records, successful, eOpts){
						for(var i = 0; i< records.length; i++){
							var province = records[i].raw;
							if(province.provinceName.trim() == arrFullAddress[8].replace("จังหวัด","")){
								selectedProvinceId = province.provinceId;
								provinceCombo.setValue(selectedProvinceId);
							}
						}
	                	
	                },this,{single:true});
	                provinceCombo.getStore().load();

	                var amphurCombo = form.getForm().findField('amphurId');
	                var selectedAmphurId = "";
	                amphurCombo.getStore().on('load',function(store, records, successful, eOpts){
	                	var amphurFiltered = records;
	                	if("" != selectedProvinceId){
	                		amphurFiltered = records.filter(function(el) {
		                		return el.raw.provinceId == selectedProvinceId;
		                	});
	                	}
	                	for(var i = 0; i< amphurFiltered.length; i++){
							var amphur = amphurFiltered[i].raw;
							if(amphur.amphurName.trim() == arrFullAddress[7].replace("อำเภอ","")){
								selectedAmphurId = amphur.amphurId;
								amphurCombo.setValue(selectedAmphurId);
							}
						}
	                	
	                },this,{single:true});
	                amphurCombo.getStore().load();
	
	                var tambolCombo = form.getForm().findField('tambolId');
	                tambolCombo.getStore().on('load',function(store, records, successful, eOpts){
	                	var tambolFiltered = records;
	                	if("" != selectedAmphurId){
	                		tambolFiltered = records.filter(function(el) {
		                		return el.raw.amphurId == selectedAmphurId;
		                	});
	                	}
	                	for(var i = 0; i< tambolFiltered.length; i++){
							var tambol = tambolFiltered[i].raw;
							if(tambol.tambolName.trim() == arrFullAddress[6].replace("ตำบล","")){
								tambolCombo.setValue(tambol.tambolId);
							}
						}
	                },this,{single:true});
	                tambolCombo.getStore().load();
				}else if(arrFullAddress[8].search("กรุงเทพมหานคร") != -1){
					var provinceCombo = form.getForm().findField('provinceId');
					var selectedProvinceId = "";
					provinceCombo.getStore().on('load',function(store, records, successful, eOpts){
	                	for(var i = 0; i< records.length; i++){
							var province = records[i].raw;
							if(province.provinceName.trim() == arrFullAddress[8]){
								selectedProvinceId = province.provinceId;
								provinceCombo.setValue(selectedProvinceId);
							}
						}
	                },this,{single:true});
	                provinceCombo.getStore().load();

	                var amphurCombo = form.getForm().findField('amphurId');
	                var selectedAmphurId = "";
	                amphurCombo.getStore().on('load',function(store, records, successful, eOpts){
	                	var amphurFiltered = records;
	                	if("" != selectedProvinceId){
	                		amphurFiltered = records.filter(function(el) {
		                		return el.raw.provinceId == selectedProvinceId;
		                	});
	                	}
	                	for(var i = 0; i< amphurFiltered.length; i++){
							var amphur = amphurFiltered[i].raw;
							if(amphur.amphurName.trim() == arrFullAddress[7].replace("เขต","")){
								selectedAmphurId = amphur.amphurId;
								amphurCombo.setValue(selectedAmphurId);
							}
						}
	                },this,{single:true});
	                amphurCombo.getStore().load();
	
	                var tambolCombo = form.getForm().findField('tambolId');
	                tambolCombo.getStore().on('load',function(store, records, successful, eOpts){
	                	var tambolFiltered = records;
	                	if("" != selectedAmphurId){
	                		tambolFiltered = records.filter(function(el) {
		                		return el.raw.amphurId == selectedAmphurId;
		                	});
	                	}
	                	for(var i = 0; i< tambolFiltered.length; i++){
							var tambol = tambolFiltered[i].raw;
							if(tambol.tambolName.trim() == arrFullAddress[6].replace("แขวง","")){
								tambolCombo.setValue(tambol.tambolId);
							}
						}
	                },this,{single:true});
	                tambolCombo.getStore().load();
				}
			}else{
				//-----[2] ??
				//-----[3] ??
				//-----[4] ถนน
				//-----[5] ตำบล/แขวง
				//-----[6] อำเภอ/เขต
				//-----[7] จังหวัด
				if(arrFullAddress[4].search("ถนน") != -1){
					form.getForm().findField('roadName').setValue(arrFullAddress[4].replace("ถนน",""));
				}
				if(arrFullAddress[7].search("จังหวัด") != -1){
					var provinceCombo = form.getForm().findField('provinceId');
					var selectedProvinceId = "";
	                provinceCombo.getStore().on('load',function(store, records, successful, eOpts){
	                	for(var i = 0; i< records.length; i++){
							var province = records[i].raw;
							if(province.provinceName.trim() == arrFullAddress[7].replace("จังหวัด","")){
								selectedProvinceId = province.provinceId;
								provinceCombo.setValue(selectedProvinceId);
							}
						}
	                },this,{single:true});
	                provinceCombo.getStore().load();

	                var amphurCombo = form.getForm().findField('amphurId');
	                var selectedAmphurId = "";
	                amphurCombo.getStore().on('load',function(store, records, successful, eOpts){
	                	var amphurFiltered = records;
	                	if("" != selectedProvinceId){
	                		amphurFiltered = records.filter(function(el) {
		                		return el.raw.provinceId == selectedProvinceId;
		                	});
	                	}
	                	for(var i = 0; i< amphurFiltered.length; i++){
							var amphur = amphurFiltered[i].raw;
							if(amphur.amphurName.trim() == arrFullAddress[6].replace("อำเภอ","")){
								selectedAmphurId = amphur.amphurId;
								amphurCombo.setValue(selectedAmphurId);
							}
						}
	                },this,{single:true});
	                amphurCombo.getStore().load();
	
	                var tambolCombo = form.getForm().findField('tambolId');
	                tambolCombo.getStore().on('load',function(store, records, successful, eOpts){
	                	var tambolFiltered = records;
	                	if("" != selectedAmphurId){
	                		tambolFiltered = records.filter(function(el) {
		                		return el.raw.amphurId == selectedAmphurId;
		                	});
	                	}
	                	for(var i = 0; i< tambolFiltered.length; i++){
							var tambol = tambolFiltered[i].raw;
							if(tambol.tambolName.trim() == arrFullAddress[5].replace("ตำบล","")){
								tambolCombo.setValue(tambol.tambolId);
							}
						}
	                },this,{single:true});
	                tambolCombo.getStore().load();
				}else if(arrFullAddress[7].search("กรุงเทพมหานคร") != -1){
					var provinceCombo = form.getForm().findField('provinceId');
					var selectedProvinceId = "";
	                provinceCombo.getStore().on('load',function(store, records, successful, eOpts){
	                	for(var i = 0; i< records.length; i++){
							var province = records[i].raw;
							if(province.provinceName.trim() == arrFullAddress[7]){
								selectedProvinceId = province.provinceId;
								provinceCombo.setValue(selectedProvinceId);
							}
						}
	                },this,{single:true});
	                provinceCombo.getStore().load();
	                
	                var amphurCombo = form.getForm().findField('amphurId');
	                var selectedAmphurId = "";
	                amphurCombo.getStore().on('load',function(store, records, successful, eOpts){
	                	var amphurFiltered = records;
	                	if("" != selectedProvinceId){
	                		amphurFiltered = records.filter(function(el) {
		                		return el.raw.provinceId == selectedProvinceId;
		                	});
	                	}
	                	console.log(amphurFiltered);
	                	for(var i = 0; i< amphurFiltered.length; i++){
							var amphur = amphurFiltered[i].raw;
							if(amphur.amphurName.trim() == arrFullAddress[6].replace("เขต","")){
								selectedAmphurId = amphur.amphurId;
								amphurCombo.setValue(selectedAmphurId);
							}
						}
	                },this,{single:true});
	                amphurCombo.getStore().load();
	
	                var tambolCombo = form.getForm().findField('tambolId');
	                tambolCombo.getStore().on('load',function(store, records, successful, eOpts){
	                	var tambolFiltered = records;
	                	if("" != selectedAmphurId){
	                		tambolFiltered = records.filter(function(el) {
		                		return el.raw.amphurId == selectedAmphurId;
		                	});
	                	}
	                	for(var i = 0; i< records.length; i++){
							var tambol = records[i].raw;
							if(tambol.tambolName.trim() == arrFullAddress[5].replace("แขวง","")){
								tambolCombo.setValue(tambol.tambolId);
							}
						}
	                },this,{single:true});
	                tambolCombo.getStore().load();
				}
			}
		}
	},
	saveTraderAddress: function(button) {

		var win = button.up('window');
		var form = win.down('form');
		if (!form.isValid()) {
			var me = form,errorCmp, fields, errors;

	          fields = me.getForm().getFields();
	          errors = '';
	          fields.each(function(field) {
	              Ext.Array.forEach(field.getErrors(), function(error) {
	                  // errors.push({name: field.getFieldLabel(), error: error});
	                  errors += field.getFieldLabel()  +'<br>';
	              });
	          });
	          // Ext.MessageBox.alert('กรุณาตรวจสอบข้อมูล', errors);

	          Ext.Msg.show({
	             title:'กรุณาตรวจสอบข้อมูล',
	             msg: errors,
	             buttons: Ext.Msg.OK,
	             icon: Ext.Msg.ERROR
	        });
	          return;
		}
		
		var model = form.getRecord(); //Ext.data.Model
		var values = form.getValues();
		//console.log(values);
		
		var provinceName = Ext.getCmp('tourleader-registration-address-masprovince-combo').getRawValue();
	    var amphurName = Ext.getCmp('tourleader-registration-address-masamphur-combo').getRawValue();
	    var tambolName = Ext.getCmp('tourleader-registration-address-tambol-combo').getRawValue();
	    
	    values.provinceName = provinceName;
	    values.amphurName = amphurName;
	    values.tambolName = tambolName;
	    //console.log(values);
	    									 
	    var grid = Ext.ComponentQuery.query('tourleader_registration_registrationTab_traderaddressgrid')[0]; 
	    if(!Ext.isEmpty(model)){
	    	model.set(values);
	    }else{
	    	model = Ext.create('tourism.model.business.registration.address.TraderAddressModel');
	    	model.set(values);
	    	
	    	//set regId and traderId
	    	var form = Ext.ComponentQuery.query('form-tourleader-registration-addedit')[0];
	    	var regId = form.getForm().findField('regId').getValue();
	    	var traderId = form.getForm().findField('traderId').getValue();

	    	model.set('regId', regId);
	    	model.set('traderId', traderId);
	    	
	    	grid.getStore().insert(0, model);
	    	
	    	if(model.get('copyAddress')){
	    		modelC = Ext.create('tourism.model.business.registration.address.TraderAddressModel');
	    		modelC.set(values);
	    		
	    		if(values.addressType == "O"){
	    			modelC.set('addressType', "S")
	    		}else{
	    			modelC.set('addressType', "O")
	    		}
	    		
	    		modelC.set('regId', regId);
	    		modelC.set('traderId', traderId);
	    		
	    		grid.getStore().insert(0, modelC);
	    	}
	    }
//	    var gridT = Ext.ComponentQuery.query('tourleader_registration_registrationTab_traderaddressgrid')[0]; 
//	    var modelT = grid.getStore().getAt(0);
//	    var data = modelT.getData(true);//.get('provinceName');
//	    alert(data);
//	    //console.log(data);

	    win.close();
	    grid.getStore().sync();
	},
  	handleActionColumn : function(column, action, grid, rowIndex, colIndex, model, node) {
        
        if(action){
            switch(action){
                case 'edittraderaddress': 

                    var edit = Ext.create('tourism.view.tourleader.registration.registrationTab.address.TraderAddressAddEditWindow');
                    edit.show();

                    if(model && (model instanceof tourism.model.business.registration.address.TraderAddressModel)){
                        
                        var fAddress = edit.down('form');
                        var provinceCombo = fAddress.getForm().findField('provinceId');
                        provinceCombo.getStore().on('load',function(store, records, successful, eOpts){
                          provinceCombo.setValue(model.get('provinceId'));
                        },this,{single:true});
                        provinceCombo.getStore().load();

                        var amphurCombo = fAddress.getForm().findField('amphurId');
                        amphurCombo.getStore().on('load',function(store, records, successful, eOpts){
                          amphurCombo.setValue(model.get('amphurId'));
                        },this,{single:true});
                        amphurCombo.getStore().load();

                        var tambolCombo = fAddress.getForm().findField('tambolId');
                        tambolCombo.getStore().on('load',function(store, records, successful, eOpts){
                          tambolCombo.setValue(model.get('tambolId'));
                        },this,{single:true});
                        tambolCombo.getStore().load();

                        edit.down('form').loadRecord(model);
                        edit.down('form').getForm().findField('copyAddress').hide();
                    }

                    
                break;

                case 'deletetraderaddress': 
                      var store = grid.getStore();
                      store.remove(model);
                      store.sync();
                break;
            }
        }
    
    }
	
});










