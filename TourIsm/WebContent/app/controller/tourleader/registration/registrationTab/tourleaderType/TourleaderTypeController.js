Ext.define('tourism.controller.tourleader.registration.registrationTab.tourleaderType.TourleaderTypeController',{
	extend : 'Ext.app.Controller',
	
	views: [
	        'tourism.view.tourleader.registration.registrationTab.tourleaderType.TourleaderTypeAddEditForm'
	],
	
	refs: [
    {
    	ref: 'tourleader_registration_registrationTab_tourleadertypeaddedit_form',
    	selector: 'panel'
    }     
	],	
	
	init: function(application){
		this.control({
			'tourleader_registration_registrationTab_tourleadertypeaddedit_form radio[action=addTourleaderType]':{
				focus: this.showTourleaderTypeAddEditWindow
			}
		});
	},
	
	showTourleaderTypeAddEditWindow: function(component, event){
		
		
		var tourleadertypeForm = Ext.ComponentQuery.query('tourleader_registration_registrationTab_tourleadertypeaddedit_form')[0];
		
		tourleadertypeForm.getForm().findField('traderCategory').setValue(component.inputValue);
		
		if(component.inputValue == '200')
		{
			Ext.create('tourism.view.window.registration.registrationTab.type.TypeAddGuideSearchWindow').show();
			
			var label300 = Ext.getCmp('tourleader_registration_tourleaderType_300_html');
			label300.setText('');
//		}else if(value.traderCategory == '300')
		}else if(component.inputValue == '300')
		{
			Ext.create('tourism.view.window.registration.registrationTab.type.TypeAddTrainSearchWindow').show();
			
			var label200 = Ext.getCmp('tourleader_registration_tourleaderType_200_html');
			label200.setText('');
//		}else if(value.traderCategory == '100'){
		}else if(component.inputValue == '100')
		{
			var label200 = Ext.getCmp('tourleader_registration_tourleaderType_200_html');
			label200.setText('');
			
			var label300 = Ext.getCmp('tourleader_registration_tourleaderType_300_html');
			label300.setText('');
		}
	}
	
});











