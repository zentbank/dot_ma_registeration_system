Ext.define('tourism.controller.tourleader.registration.registrationType.RegistrationTypeController', {
	extend : 'Ext.app.Controller',
    stores: ['tourism.store.tourleader.registration.TourleaderRegistrationStore'],
	models: ['tourism.model.tourleader.registration.TourleaderRegistrationModel'],
    views: [
      'tourism.view.tourleader.registration.registrationType.RegistrationTypeAddEditForm',
      // 'tourism.view.tourleader.registration.registrationType.RegistrationTypeSearchBusinessForm'
      'tourism.view.business.registration.registrationType.RegistrationTypeSearchBusinessForm'
    ],
    refs: [    
	    	{
	            ref: 'tourleader-registration-registrationtype-addeditform',
	            selector: 'panel'
	        }
	        // ,
	        // {
	        //     ref: 'tourleader-registration-registrationtype-searchbusinessform',
	        //     selector: 'panel'
	        // }
	
	        ,
	        {
	            ref: '#tourleader-registration-registrationtype-searchtourleaderform',
	            selector: 'panel'
	        }
    ],	 
    
	init : function (application) {
        this.control({
            'tourleader-registration-registrationtype-addeditform button[action=selectRegType]': {
              click: this.selectRegistrationType
            },
            'tourleader-registration-registrationtype-searchbusinessform button[action=saveSearchType]': {
              click: this.saveSearchType
            },
	        '#tourleader-registration-registrationtype-searchtourleaderform button[action=searchLicense]': {
	              click: this.searchLicense
	         },
             '#tourleader-registration-registrationtype-searchtourleaderform button[action=selectedLicenseNo]': {
              click: this.selectedLicenseNo
            }
        });
	},
	
	selectRegistrationType: function(button) {
		var win = button.up('window'),
		form = win.down('form'),
		model = form.getRecord(), //Ext.data.Model
		values = form.getValues();//object {xx: q,ss:ff}
		
		//Oat Fix
		values.traderType = 'L';
		//
		
//		//console.log(values);
	    if(values.registrationType != 'N')
	    {
	        var layout = win.getLayout();
	        layout.setActiveItem(1);
	
	        var toModel = Ext.create('tourism.model.business.registration.BusinessRegistrationModel');
	        toModel.set('registrationType',values.registrationType);
	 
	        
	        if(values.registrationType == 'C')
	        {
	          toModel.set('registrationTypeName', '<h1>เปลี่ยนแปลงรายการ</h1>');
	        }
	        else if(values.registrationType == 'T')
	        {
	          toModel.set('registrationTypeName', '<h1>ออกใบแทน</h1>');
	        }

	        var form = win.down('#tourleader-registration-registrationtype-searchtourleaderform');
	        form.loadRecord(toModel);

	        // var form = Ext.ComponentQuery.query('tourleader-registration-registrationtype-searchbusinessform')[0];
	        // form.loadRecord(toModel);
	        //loadRecord
	    }
	    else
	    {
	      var fsubmit = form.getForm();
	      var ctrl = this; // scope
	
	       fsubmit.load({
	            url: 'tourleader/registration/createregistration',
	            method : 'POST',
	            params: {
	                newStatus: 'test'
	            },
	            params: values,
	            success: function(form, action) {
	
	              var dataObj = action.result.data;
	
	              var regAddEditWindow = ctrl.createTourleaderRegistrationAddEditWindow(dataObj);
	
	              regAddEditWindow.on('show', function(){
	                
	                win.close();
	
	              },regAddEditWindow,{single: true});
	              
	              regAddEditWindow.show();
	            },
	            failure: function(form, action) {
	              Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
	            },
	            scope: fsubmit // this = fsubmit
	        });
	      
	    }
	  },
  
  saveSearchType: function(button) {
    var win = button.up('window');
    var form = Ext.ComponentQuery.query('tourleader-registration-registrationtype-searchbusinessform')[0];  

    model = form.getRecord(); //Ext.data.Model
    values = form.getValues();//object {xx: q,ss:ff}

   
     win.close();    
  },
  
  createTourleaderRegistrationAddEditWindow: function(values){
    var model =  Ext.create('tourism.model.tourleader.registration.TourleaderRegistrationModel');
    model.set(values);

    var regAddEditWindow = Ext.create('tourism.view.tourleader.registration.TourleaderRegistrationAddEditWindow',{
      registrationModel: model,
      roleAction: 'key',
      traderType: 'tourleader'
    });
    return regAddEditWindow;
  }
,
  searchLicense: function(btn)
  {
    var form = btn.up('form');
    var grid =  form.down('registrationtype-grid');

    var licenseNo = form.getForm().findField('licenseNo').getValue();

    if(Ext.isEmpty(licenseNo))
    {
          Ext.Msg.show({
             title:'เกิดข้อผิดพลาด',
             msg: 'กรุณากรอกข้อมูลเลขที่ใบอนุยาต',
             buttons: Ext.Msg.OK,
             icon: Ext.Msg.ERROR
          });
      return;
    }

   grid.getStore().load({
        params: { traderType: 'L', licenseNo: licenseNo},
        callback: function(records, operation, success) {
            
        },
        scope: this
    }); 

  },
  selectedLicenseNo: function(btn)
  {
    var win    = btn.up('window');

    var animateTarget = win.animateTarget;

    var form1 = btn.up('form');
    var grid =  form1.down('registrationtype-grid');
    var selection = grid.getSelectionModel().getSelection();

    if(selection.length < 1)
    {
        Ext.Msg.show({
           title:'เกิดข้อผิดพลาด',
           msg: 'กรุณาเลือกข้อมูลใบอนุยาต',
           buttons: Ext.Msg.OK,
           icon: Ext.Msg.ERROR
        });

        return;
    }

    var model = selection[0];

    
    var registrationType = form1.getForm().findField('registrationType').getValue();
    var myMask = new Ext.LoadMask(form1, {msg:"กรุณารอสักครู่..."});
    myMask.show();
    form1.load({
      url: 'business/registration/addnewregistration',
      method : 'POST',
      params: {traderId: model.get('traderId') ,registrationType: registrationType},
      success: function(form, action) {
        myMask.hide();

		
    	var edit = Ext.create('tourism.view.tourleader.registration.TourleaderRegistrationAddEditWindow',{
            animateTarget: animateTarget,
             actionMode: 'edit',
              roleAction: 'key',
              traderType: 'tourleader',
              verificationGrid: grid

        });
		edit.show();


        var model =  Ext.create('tourism.model.business.registration.BusinessRegistrationModel');

        model.set(action.result.data);

        edit.loadDataModel(model);

        win.close();

      },
      failure: function(form, action) {
        myMask.hide();
        Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
      },
      scope: this // this = fsubmit
    });


  }
});