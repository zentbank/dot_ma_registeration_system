Ext.define('tourism.controller.window.registration.registrationTab.type.TypeAddGuideController',{
	extend : 'Ext.app.Controller',

	views: [
	        'tourism.view.window.registration.registrationTab.type.TypeAddGuideSearchForm'
	],
	
	refs: [
    {
    	ref: 'window_registration_typeaddguidesearch_form',
    	selector: 'panel'
    }     
	],	
	
	init: function(application){
		this.control({
			'window_registration_typeaddguidesearch_form button[action=searchTypeGuide]':{
				click: this.searchTypeGuide
			},
			'window_registration_typeaddguidesearch_form button[action=saveTypeGuide]':{
				click: this.saveTypeGuide
			}
		});
	},
	
	searchTypeGuide: function(button){
		var win = button.up('window');
		var form = win.down('form');
		if (!form.isValid()) {
			var me = form,errorCmp, fields, errors;

	          fields = me.getForm().getFields();
	          errors = '';
	          fields.each(function(field) {
	              Ext.Array.forEach(field.getErrors(), function(error) {
	                  errors += field.getFieldLabel()  +'<br>';
	              });
	          });

	          Ext.Msg.show({
	             title:'กรุณาตรวจสอบข้อมูล',
	             msg: errors,
	             buttons: Ext.Msg.OK,
	             icon: Ext.Msg.ERROR
	        });
	          return;
		}

		var grid = Ext.ComponentQuery.query('window_registration_typeaddguidesearch_grid')[0]; 
		var store = grid.getStore();
		var values = form.getValues();

		
		store.load({
			scope: this,
			params: values,
		    callback: function(records, operation, success) {
		        // the operation object
		        // contains all of the details of the load operation
		        //console.log(records);
//		        //console.log(operation);
//		        //console.log(success);
		    }
		});
	},
	
	saveTypeGuide: function(button){
		var grid = Ext.ComponentQuery.query('window_registration_typeaddguidesearch_grid')[0]; 
		var model = grid.getSelectionModel().getSelection();
		
		if(model.length == 1)
		{
			var win = button.up('window');
			win.close();

			var form = Ext.ComponentQuery.query('tourleader_registration_registrationTab_tourleadertypeaddedit_form')[0];
			form.getForm().findField('licenseGuideNo').setValue(model[0].get('licenseGuideNo'));
			form.getForm().findField('traderGuideId').setValue(model[0].get('traderGuideId'));;
			form.getForm().findField('personGuideId').setValue(model[0].get('personGuideId'));;
			//console.log(form.getForm().findField('licenseGuideNo').getValue());
			//console.log(form.getForm().findField('traderGuideId').getValue());
			//console.log(form.getForm().findField('personGuideId').getValue());
			
			var label200 = Ext.getCmp('tourleader_registration_tourleaderType_200_html');
			label200.setText('ใบอนุญาตเลขที่: '+model[0].get('licenseNo')+'  ชื่อ-สกุล: '+model[0].get('personFullName')+'  เพศ: '+model[0].get('genderName')+'  ประเภทมัคคุเทศก์: '+model[0].get('traderCategoryName')+'  ชนิดมัคคุเทศก์: '+model[0].get('traderCategoryGuideTypeName'));

		}else{
			
			errors = 'กรุณาเลือกข้อมูล';
			
			Ext.Msg.show({
	             title:'กรุณาตรวจสอบข้อมูล',
	             msg: errors,
	             buttons: Ext.Msg.OK,
	             icon: Ext.Msg.ERROR
	        });
		}
		
	}
	
	
});










