Ext.define('tourism.controller.window.registration.registrationTab.type.TypeAddTrainController',{
	extend : 'Ext.app.Controller',

	views: [
	        'tourism.view.window.registration.registrationTab.type.TypeAddTrainSearchForm'
	],
	
	refs: [
    {
    	ref: 'window_registration_typeaddtrainsearch_form',
    	selector: 'panel'
    }     
	],	
	
	init: function(application){
		this.control({
			'window_registration_typeaddtrainsearch_form button[action=searchTypeTrain]':{
				click: this.searchTypeTrain
			},
			'window_registration_typeaddtrainsearch_form button[action=saveTypeTrain]':{
				click: this.saveTypeTrain
			}
		});
	},
	
	searchTypeTrain: function(button){
		var win = button.up('window');
		var form = win.down('form');
		if (!form.isValid()) {
			var me = form,errorCmp, fields, errors;

	          fields = me.getForm().getFields();
	          errors = '';
	          fields.each(function(field) {
	              Ext.Array.forEach(field.getErrors(), function(error) {
	                  errors += field.getFieldLabel()  +'<br>';
	              });
	          });

	          Ext.Msg.show({
	             title:'กรุณาตรวจสอบข้อมูล',
	             msg: errors,
	             buttons: Ext.Msg.OK,
	             icon: Ext.Msg.ERROR
	        });
	          return;
		}

		var grid = Ext.ComponentQuery.query('window_registration_typeaddtrainsearch_grid')[0]; 
		var store = grid.getStore();
		var values = form.getValues();
//		//console.log(values);
		
		store.load({
			scope: this,
			params: values,
		    callback: function(records, operation, success) {
		        // the operation object
		        // contains all of the details of the load operation
		        //console.log(records);
//		        //console.log(operation);
//		        //console.log(success);
		    }
		});
		
	},
	
	saveTypeTrain: function(button){
		var grid = Ext.ComponentQuery.query('window_registration_typeaddtrainsearch_grid')[0]; 
		var model = grid.getSelectionModel().getSelection();
//		//console.log(model);
		
//		var store = grid.getStore();
//		//console.log(store);
//		//console.log(store.getCount());
//		var model = store.getAt(0);
//		//console.log(model);
		
		if(model.length == 1)
		{
			var win = button.up('window');
			win.close();

			var form = Ext.ComponentQuery.query('tourleader_registration_registrationTab_tourleadertypeaddedit_form')[0];
			form.getForm().findField('eduId').setValue(model[0].get('eduId'));;
			//console.log(form.getForm().findField('eduId').getValue());
			
			var label300 = Ext.getCmp('tourleader_registration_tourleaderType_300_html');
			label300.setText('ชื่อหลักสูตร: '+model[0].get('graduationCourse')+'  รุ่นที่: '+model[0].get('generationGraduate')+'  สถาบัน: '+model[0].get('universityName')+'  วันที่อบรม: '+model[0].get('studyDate')+'-'+model[0].get('graduationDate'));

		}else{
			
			errors = 'กรุณาเลือกข้อมูล';
			
			Ext.Msg.show({
	             title:'กรุณาตรวจสอบข้อมูล',
	             msg: errors,
	             buttons: Ext.Msg.OK,
	             icon: Ext.Msg.ERROR
	        });
		}
		
	}
	
});










