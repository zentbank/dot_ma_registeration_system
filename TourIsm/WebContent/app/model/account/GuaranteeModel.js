Ext.define('tourism.model.account.GuaranteeModel',{
	extend: 'Ext.data.Model',
	fields:[
	        {
	        	name: 'guaranteeId',
	        	persist: true
	        },
	        {
	        	name: 'traderId'
	        },
	        {
	        	name: 'masBankId'
	        },
	        {
	        	name: 'guaranteeType'
	        },
	        {
	        	name: 'accountId'
	        },
	        {
	        	name: 'accountName'
	        },
	        {
	        	name: 'receiveDate'
	        },
	        {
	        	name: 'receiveName'
	        },
	        {
	        	name: 'refundName'
	        },
	        {
	        	name: 'refundType'
	        },
	        {
	        	name: 'refundMny'
	        },
	        {
	        	name: 'refundInterest'
	        },
	        {
	        	name: 'refundDate'
	        },
	        {
	        	name: 'refundRemark'
	        },
	        {
	        	name: 'guaranteeStatus'
	        },
	        {
	        	name: 'guaranteeRemark'
	        },
	        {
	        	name: 'guaranteeMny'
	        },
	        {
	        	name: 'guaranteeInterest'
	        },
	        {
	        	name: 'gauranteeRecordStatus'
	        },
	        {
	        	name: 'traderRecordStatus'
	        },
	        {
	        	name: 'bankBranchName'
	        },
	        {
	        	name: 'accountExpireDate'
	        }
	  ]
});