Ext.define('tourism.model.account.ReceiptModel',{
	extend: 'Ext.data.Model',
	fields:[
	        { name: 'receiptId' , persist: true},
			{ name: 'bookNo'},
			{ name: 'receiptNo'},
			{ name: 'receiptName'},
			{ name: 'receiveOfficerName'},
			{ name: 'receiveOfficerDate'},
			{ name: 'authority'},
			{ name: 'receiptStatus'},
			{ name: 'receiptRemark'},
			{ name: 'regId'},
			{ name: 'traderTypeName'},
			{ name: 'traderType'}
	  ]
});