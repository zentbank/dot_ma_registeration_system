Ext.define('tourism.model.admuser.AdmGroupModel', {
extend: 'Ext.data.Model',
fields: [
	{name: 'groupId'  ,persist: true},
	{name: 'groupName' },
	{name: 'groupRole' },
	{name: 'officerGroup' },
	{name: 'userId'},
	{name: 'active'},
	{name: 'userId'}
]
});