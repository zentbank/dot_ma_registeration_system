Ext.define('tourism.model.admuser.AdmUserModel', {
extend: 'Ext.data.Model',
fields: [
	{name: 'userId'  ,persist: true},
	{name: 'prefixId' },
	{name: 'prefixName' },
	{name: 'orgId' },
	{name: 'orgName' },
	{name: 'userName' },
	{name: 'userLastname' },
	{name: 'userLogin' },
	{name: 'loginPassword' },
	{name: 'userFullName' },
	{name: 'groupId' },
	{name: 'groupName' },
	{name: 'groupRole' },
	{name: 'userMenu' },
	{name: 'userGroup' },
	{name: 'posId'},
	{name: 'posName'}
]
});