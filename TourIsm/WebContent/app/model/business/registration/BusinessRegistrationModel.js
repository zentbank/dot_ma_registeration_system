Ext.define('tourism.model.business.registration.BusinessRegistrationModel', {
	extend: 'Ext.data.Model',
	fields: [
		{
			name: 'regId',
			persist: true
		},
		{
			name: 'registrationType'
		},{
			name: 'registrationTypeName'
		},
		{
			name: 'registrationProgress'
		},
		{
			name: 'registrationDate'
		},
		{
			name: 'registrationDateFrom'
		},
		{
			name: 'registrationDateTo'
		},
		{
			name: 'registrationNo'
		},
		{
			name: 'remark'
		},
		{
			name: 'orgId'
		},
		{
			name: 'orgName'
		},
		{
			name: 'traderId'
		},
		{
			name: 'traderType'
		},{
			name: 'traderTypeName'
		},
		{
			name: 'licenseNo'
		},
		{
			name: 'traderName'
		},
		{
			name: 'traderNameEn'
		},
		{
			name: 'pronunciationName'
		},
		{
			name: 'traderCategory'
		},
		{
			name: 'traderCategoryName'
		},
		{
			name: 'licenseGuideNo'
		},
		{
			name: 'traderArea'
		},
		{
			name: 'licenseStatus'
		},
		{
			name: 'effectiveDate'
		},
		{
			name: 'expireDate'
		},
		{
			name: 'printLicense'
		},
		{
			name: 'printCard'
		},
		{
			name: 'personId'
		},{
			name: 'personType'
		},{
			name: 'identityNo'
		},{
			name: 'prefixId'
		},{
			name: 'prefixName'
		},{
			name: 'prefixIdEn'
		},{
			name: 'prefixIdCorp'
		},{
			name: 'postfixId'
		},{
			name: 'postfixName'
		},{
			name: 'firstName'
		},{
			name: 'lastName'
		},{
			name: 'firstNameEn'
		},{
			name: 'lastNameEn'
		},{
			name: 'firstNameCorp'
		},{
			name: 'identityNoCorp'
		},{
			name: 'identityDateCorp'
		},{
			name: 'taxIdentityNo'
		},{
			name: 'taxProvinceId'
		},{
			name: 'taxAmphurId'
		},{
			name: 'corporateType'
		},{
			name: 'committeeNameSign'
		},
		{ name: 'committeeName2' },
		{ name: 'committeeName1'},
		{
			name: 'provinceIdCorp'
		},{
			name: 'birthDate'
		},{
			name: 'personNationality'
		},{
			name: 'gender'
		},{
			name: 'provinceId'
		},{
			name: 'identityDate'
		},{
			name: 'amphurId'
		},{
			name: 'planId'
		},{
			name: 'areaName'
		},{
			name: 'plantripPerYear'
		},
		{
			name: 'authorityName'
		},
		{
			name: 'traderAddress'
		},{
			name: 'traderOwnerName'
		},{
			name: 'regRecordStatus'
		},{
			name: 'traderRecordStatus'
		},{
			name: 'rersonRecordStatus'
		},{
			name: 'roleAction'
		},{
			name: 'progressStatus'
		}
		
		//Oat Add
		,{name: 'traderGuideDetail'}
		,{name: 'traderTrainedDetail'}
		,{name: 'ageYear'}
		,{name: 'passportNo'}
		,{name: 'identityNoExpire'}
		,{name: 'traderGuideId'}
		,{name: 'personGuideId'}
		,{name: 'eduId'}
		,{
			name: 'taxAmphurName'
		},{
			name: 'taxProvinceName'
		},{
			name: 'provinceName'
		},{
			name: 'amphurName'
		},{
			name: 'prefixNameEn'
		},{
			name: 'progressBackStatus'
		},{
			name: 'progressStatusName'
		}
		
		 	,{name: 'graduationCourse'}
			,{name: 'generationGraduate'}
			,{name: 'universityName'}
			,{name: 'studyDate'}
			
			,{name: 'personFullName'}
			,{name: 'genderName'}
			,{name: 'traderCategoryGuideTypeName'}
			
			,{name: 'traderOwnerNameEn'}
		//
			//sek add
			,{
				name : 'provinceGuideServiceId'
			 },
			 {
				name : 'traderCategoryType'
			 },
			 {
				 name: 'provinceGuideServiceName'
			 },
			 {
			 	name: 'multipleJobs'
			 },
			 {
			 	name: 'roleColor'
			 }
			 ,
            {
            	name: 'imageFile'
            }
	]
});