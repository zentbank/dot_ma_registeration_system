Ext.define('tourism.model.business.registration.address.TraderAddressModel', {
	extend: 'Ext.data.Model',
	fields: [
		{
			name: 'addressId',
			persist: true
		},
		{
			name: 'addressType'
		},
		{
			name: 'branchNo'
		},
		{
			name: 'regId'
		},
		{
			name: 'traderId'
		},
		{
			name: 'printLicense'
		},
		{
			name: 'addressNo'
		},
		{
			name: 'buildingName'
		},
		{
			name: 'buildingNameEn'
		},
		{
			name: 'floor'
		},
		{
			name: 'roomNo'
		},
		{
			name: 'moo'
		},
		{
			name: 'villageName'
		},
		{
			name: 'villageNameEn'
		},
		{
			name: 'soi'
		},
		{
			name: 'roadName'
		},
		{
			name: 'roadNameEn'
		},
		{
			name: 'postCode'
		},
		{
			name: 'telephone'
		},
		{
			name: 'mobileNo'
		},
		{
			name: 'fax'
		},
		{
			name: 'email'
		},
		{
			name: 'website'
		},
		{
			name: 'amphurId'
		},
		{
			name: 'amphurName'
		},
		{
			name: 'provinceId'
		},
		{
			name: 'provinceName'
		},{
			name: 'tambolId'
		},{
			name: 'tambolName'
		},{
			name: 'traderId'
		},{
			name: 'personId'
		},

		{ name: 'addressNoEn'},
		{ name: 'floorEn'},
		{ name: 'roomNoEn'},
		{ name: 'mooEn'},
		{ name: 'soiEn'},
		{ name: 'postCodeEn'},
		{ name: 'telephoneEn'},
		{ name: 'mobileNoEn'},
		{ name: 'faxEn'},
		{ name: 'emailEn'},
		{ name: 'websiteEn'},
		{ name: 'amphurIdEn'},
		{ name: 'amphurNameEn'},
		{ name: 'provinceIdEn'},
		{ name: 'provinceNameEn'},
		{ name: 'tambolIdEn'},
		{ name: 'tambolNameEn'},
		{ name: 'recordStatus'},
		{ name: 'traderType'}

	]
});