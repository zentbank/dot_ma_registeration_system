Ext.define('tourism.model.business.registration.tradercategory.PlantripModel', {
	extend: 'Ext.data.Model',
	fields: [
		{ name: 'planId' ,persist: true },
		{ name: 'traderId' },
		{ name: 'countryId' },
		{ name: 'countryName' },
		{ name: 'countryNameEn' },
		{ name: 'areaName' },
		{ name: 'plantripPerYear' },
		{ name: 'recordStatus' }
	]
});

