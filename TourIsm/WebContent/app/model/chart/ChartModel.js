Ext.define('tourism.model.chart.ChartModel', {
	extend: 'Ext.data.Model',
	fields: [
		{name: 'traderCategoryName'}
		,{name: 'count'}
		,{name: 'orgName'}
		
		,{name: 'newCount'}
		
		,{name: 'licenseStatusName'}
		,{name: 'count1'}
		,{name: 'count2'}
		,{name: 'count3'}
		,{name: 'count4'}
		,{name: 'count5'}
		
		,{name: 'outBoundCount'}
		,{name: 'inBoundCount'}
		,{name: 'countryCount'}
		,{name: 'areaCount'}
		
		,{name: 'outBoundRevokeCount'}
		,{name: 'inBoundRevokeCount'}
		,{name: 'countryRevokeCount'}
		,{name: 'areaRevokeCount'}
		,{name: 'countRevoke'}
		
		,{name: 'registrationTypeName'}

	]
});