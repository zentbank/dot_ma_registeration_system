Ext.define('tourism.model.combo.AdmUserModel', {
	extend : 'Ext.data.Model',
	fields : [ {
		name : 'userId',
		persist : true
	}, {
		name : 'userName'
	}, {
		name : 'userLastname'
	}, {
		name : 'userFullName'
	}, {
		name : 'groupRole'
	}, {
		name : 'posId'
	} ]
});