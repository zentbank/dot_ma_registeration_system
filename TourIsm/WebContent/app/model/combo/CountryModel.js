Ext.define('tourism.model.combo.CountryModel', {
	extend: 'Ext.data.Model',
	fields: [
		{
	    	 name: 'countryId',
	    	 persist: true
	     },
	     {
	    	 name: 'countryName'
	     },
	     {
	    	 name: 'countryNameEn'
	     }
	]
});