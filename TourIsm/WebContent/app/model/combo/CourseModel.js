Ext.define('tourism.model.combo.CourseModel',{
	extend: 'Ext.data.Model',
	fields: [
	         	{
	         		name: 'eduId',
	         		persist: true	
	         	},
	         	{
	         		name: 'courseName'
	         	}
	         ]
});