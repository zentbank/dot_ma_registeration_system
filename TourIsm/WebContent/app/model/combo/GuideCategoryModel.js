Ext.define('tourism.model.combo.GuideCategoryModel',{
	extend: 'Ext.data.Model',
	fields: [
     {
    	 name: 'traderCategory',
    	 type: 'string',
    	 persist: true
     },
     {
    	 name: 'traderCategoryName',
         type: 'string'
     }
     
	]
});