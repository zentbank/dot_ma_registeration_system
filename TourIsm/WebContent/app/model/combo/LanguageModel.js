Ext.define('tourism.model.combo.LanguageModel', {
	extend: 'Ext.data.Model',
	fields: [
    {
    	 name: 'countryId',
    	 persist: true
     },
     {
    	 name: 'countryName'
     },
     {
    	 name: 'countryNameEn'
     },
     {
    	 name: 'language'
     },
     {
    	 name: 'count'
     }
	]
});