Ext.define('tourism.model.combo.MasAmphurModel', {
	extend: 'Ext.data.Model',
	fields: [
		{
			name: 'amphurId',
			persist: true
		},
		{
			name: 'amphurName'
		},
		{
			name: 'provinceId'
		},
		{
			name: 'amphurNameEn'
		}

		

	]
});