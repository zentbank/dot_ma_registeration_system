Ext.define('tourism.model.combo.MasBankModel', {
	extend: 'Ext.data.Model',
	fields: [
		{
			name: 'masBankId',
			persist: true
		},
		{
			name: 'bankName'
		}

	]
});