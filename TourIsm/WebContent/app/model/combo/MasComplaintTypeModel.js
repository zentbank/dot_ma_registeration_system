Ext.define('tourism.model.combo.MasComplaintTypeModel', {
	extend: 'Ext.data.Model',
	fields: [
		{
			name: 'masComplaintTypeId',
			persist: true
		},
		{
			name: 'complaintTypeName'
		}

	]
});