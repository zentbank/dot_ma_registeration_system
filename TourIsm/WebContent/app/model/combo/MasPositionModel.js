Ext.define('tourism.model.combo.MasPositionModel', {
	extend : 'Ext.data.Model',
	fields : [ {
		name : 'posId',
		persist : true
	}, {
		name : 'posName'
	}]
});