Ext.define('tourism.model.combo.MasPostfixModel', {
	extend: 'Ext.data.Model',
	fields: [
		{
			name: 'postfixId',
			persist: true
		},
		{
			name: 'postfixName'
		},
		{
			name: 'postfixNameEN'
		}

	]
});