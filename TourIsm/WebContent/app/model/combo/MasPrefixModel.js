Ext.define('tourism.model.combo.MasPrefixModel', {
	extend: 'Ext.data.Model',
	fields: [
		{
			name: 'prefixId',
			persist: true
		},
		{
			name: 'prefixName'
		},
		{
			name: 'prefixNameEn'
		}

	]
});