Ext.define('tourism.model.combo.MasProvinceModel', {
	extend: 'Ext.data.Model',
	fields: [
		{
			name: 'provinceId',
			persist: true
		},
		{
			name: 'provinceName'
		},
		{
			name: 'provinceNameEn'
		}

	]
});