Ext.define('tourism.model.combo.MasTambolModel', {
	extend: 'Ext.data.Model',
	fields: [
		{
			name: 'tambolId',
			persist: true
		},
		{
			name: 'tambolName'
		},
		{
			name: 'amphurId'
		},
		{
			name: 'tambolNameEn'
		}

		

	]
});