Ext.define('tourism.model.combo.MasUniversityModel', {
	extend: 'Ext.data.Model',
	fields: [
		{
			name: 'masUniversityId',
			persist: true
		},
		{
			name: 'universityName'
		}

	]
});