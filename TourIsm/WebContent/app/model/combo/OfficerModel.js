Ext.define('tourism.model.combo.OfficerModel', {
	extend: 'Ext.data.Model',
	fields: [
		{
			name: 'officerId',
			persist: true
		},
		{
			name: 'officerName'
		},
		{
			name: 'officerType'
		},
		{
			name: 'groupRole'
		},
		{
			name: 'officerGroup'
		}

	]
});