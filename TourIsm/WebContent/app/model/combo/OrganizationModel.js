Ext.define('tourism.model.combo.OrganizationModel', {
	extend: 'Ext.data.Model',
	fields: [
		{
			name: 'orgId',
			persist: true
		},
		{
			name: 'orgName'
		}

	]
});