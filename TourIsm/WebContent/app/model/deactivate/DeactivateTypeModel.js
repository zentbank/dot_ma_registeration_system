Ext.define('tourism.model.deactivate.DeactivateTypeModel', {
extend: 'Ext.data.Model',
fields: [
	{name:"masDeactivateTypeId" ,persist: true},
	{name:"licenseType"},
	{name:"deactivateName"},
	{name:"deactiveId"},
	{name:"deactivateDtlId"},
	{name:"active"}

]
});