Ext.define('tourism.model.grid.ComplaintDocumentModel', {
	extend: 'Ext.data.Model',
	fields: [
		
		{
			name: 'complaintDocId',
			persist: true
		}
//		,{name: 'complaintId'}
		,{name: 'complaintLicenseId'}
		,{name: 'complaintDocName'}
		,{name: 'complaintDocType'}
		
		,{ name: 'havefile'}
		,{name: 'complaintDocPath'}
		,{name: 'complaintCardId'}
		
		,{name: 'traderType'}
		,{name: 'licenseNo'}
		,{name: 'questionType'}
	]
});