Ext.define('tourism.model.grid.ComplaintLicenseModel', {
	extend: 'Ext.data.Model',
	fields: [

		{
			name: 'complaintLicenseId',
			persist: true
		}
		,{name: 'userId'}
		,{name: 'masComplaintTypeId'}
		,{name: 'questionType'}
		,{name: 'complaintDesc'}
		,{name: 'complaintCardId'}
		,{name: 'complaintName'}
		,{name: 'complaintTel'}
		,{name: 'complaintEmail'}
		,{name: 'complaintNo'}
		,{name: 'complaintDate'}
		,{name: 'complaintStatus'}
		,{name: 'authorityComment'}
		,{name: 'complaintProgress'}
		
		,{name: 'complaintStatusName'}
		
		,{name: 'licenseNo'}
		,{name: 'traderName'}
		,{name: 'traderType'}
		
		,{name: 'viewData'}
		,{name: 'complaintTypeName'}
		,{name: 'userFullName'}
		,{name: 'traderTypeName'}
		
		,{name: 'authorityDate'}
		,{
			name: 'officerAsOwner'
		},
		{
			name: 'traderId'
		}
		
//		,{name: 'complaintDocName'}
//		,{name: 'complaintDocType'}
//		
//		,{ name: 'havefile'}
//		,{name: 'complaintDocPath'}
		
	]
});