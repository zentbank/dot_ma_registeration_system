Ext.define('tourism.model.grid.ComplaintProgressModel', {
	extend: 'Ext.data.Model',
	fields: [

		{
			name: 'comProgressId',
			persist: true
		}
		,{name: 'complaintLicenseId'}
		,{name: 'progressRole'}
		,{name: 'progressStatus'}
		,{name: 'progressDesc'}
		,{name: 'progressDate'}
		,{name: 'authority'}
		,{name: 'progressStatusName'}
		
		,{name: 'edit'}
		
	]
});