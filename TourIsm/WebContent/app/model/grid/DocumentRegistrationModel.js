Ext.define('tourism.model.grid.DocumentRegistrationModel', {
	extend: 'Ext.data.Model',
	fields: [
		{
			name: 'masDocId',
			persist: true
		},
		{
			name: 'documentName'
		}
		,{ name: 'branchTypeName'}
		,{ name: 'traderId'}
		,{ name: 'havefile'}
		,{ name: 'regDocId'}
		,{name: 'licenseType'}
		,{name: 'traderType'}
		,{name: 'docPath'}
		,{name: 'complaintId'}
		,{name: 'complaintDocName'}
		,{name: 'complaintDocType'}
		,{name: 'docStatus'}
		,{name: 'docRemark'}
	]
});