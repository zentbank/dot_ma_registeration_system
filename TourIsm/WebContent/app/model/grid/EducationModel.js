Ext.define('tourism.model.grid.EducationModel', {
	extend: 'Ext.data.Model',
	fields: [
		{
			name: 'eduId',
			persist: true
		},
		{
			name: 'masUniversityId'
		},
		{
			name: 'masEducationLevelId'
		},
		{
			name: 'personId'
		},
		{
			name: 'educationType'
		},
		{
			name: 'studyDate'
		},
		{
			name: 'graduationDate'
		},
		{
			name: 'graduationYear'
		},
		{
			name: 'graduationCourse'
		},
		{
			name: 'educationMajor'
		},
		{
			name: 'generationGraduate'
		},
		{
			name: 'universityName'
		}
		,{name: 'educationLevelName'}
		,{name: 'eduRecordStatus'}
		,{name: 'personRecordStatus'}
	]
});