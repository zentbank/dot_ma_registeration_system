Ext.define('tourism.model.grid.ForeignLanguageModel',{
	extend: 'Ext.data.Model',
	fields: [
     {
    	 name: 'foreignId',
		 persist: true
     },
     {
    	 name: 'countryId'
     },
     {
    	 name: 'countryName'
     },
     {
    	 name: 'countryNameEn'
     },
     {
    	 name: 'language'
     },
     {
    	 name: 'personId'
     }
     ,
     {
    	 name: 'traderId'
     }
     
     ]

});












