Ext.define('tourism.model.grid.PersonImgModel',{
	extend:'Ext.data.Model',
	fields: [
	 		{
	 			name: 'personImgId',
	 			persist: true
	 		},
	 		//{
	 		//	name: 'documentName'
	 		//}
	 		,{ name: 'branchTypeName'}
	 		,{ name: 'traderId'}
	 		,{ name: 'regId'}
	 		,{ name: 'havefile'}
	 		//,{ name: 'regDocId'}
	 		
	 		,{name: 'licenseType'}
	 		,{name: 'traderType'}
	 		,{name: 'imgPath'}
	 		
	 		//,{name: 'complaintId'}
	 		//,{name: 'complaintDocName'}
	 		//,{name: 'complaintDocType'}
	 	]
});