Ext.define('tourism.model.grid.PersonTrainedModel',{
	extend: 'Ext.data.Model',
	fields: [
     {
    	 name: 'personTrainedId',
		 persist: true
     },
     {
    	 name: 'identityNo'
     },
     {
    	 name: 'identityDate'
     },
     {
    	 name: 'passportNo'
     },
     {
    	 name: 'firstName'
     },
     {
    	 name: 'lastName'
     },
     {
    	 name: 'firstNameEn'
     },
     {
    	 name: 'lastNameEn'
     },
     {
    	 name: 'prefixId'
     },
     {
    	 name: 'gender'
     },
     {
    	 name: 'personNationality'
     },
     {
    	 name: 'ageYear'
     },
     {
    	 name: 'identityNoExpire'
     },
     {
    	 name: 'taxIdentityNo'
     },
     {
    	name: 'taxProvinceId' 
     },
     {
      	name: 'provinceId' 
     },
     {
      	name: 'taxAmphurId' 
     },
     {
       	name: 'amphurId' 
     },
     {
    	 name: 'eduId'
     },
     {
    	 name: 'educationType'
     },
     {
    	 name: 'studyDate'
     },
     {
    	 name: 'graduationDate'
     },
     {
    	 name: 'graduationYear'
     },
     {
    	 name: 'graduationCourse'
     },
     {
    	 name: 'generationGraduate'
     },
     {
    	 name: 'recordStatus'
     },
     {
    	 name: 'masUniversityId'
     },
     {
    	 name: 'masEducationLevelId'
     },
     {
    	 name: 'universityName'
     },
     {
    	 name: 'personId'
     },
     {
    	 name: 'regId'
     },
     {
    	 name: 'traderId'
     },
     {
    	 name: 'prefixName'
     },
     {
    	 name: 'fullName'
     },
     {
    	 name: 'courseName'
     },
     {
    	 name: 'provinceName'
     },
     {
    	 name: 'amphurName'
     },
     {
    	 name: 'birthDate'
     },
     {
        name: 'countryId'
     }
     
     ]

});












