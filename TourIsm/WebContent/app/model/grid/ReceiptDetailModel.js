Ext.define('tourism.model.grid.ReceiptDetailModel',{
	extend: 'Ext.data.Model',
	fields: [
 		{
 			name: 'receiptDetailId',
 			persist: true
 		},
 		{
 			name: 'feeName'
 		},
 		{
 			name: 'feeMny'
 		}
 	
 		,{name: 'feeId'}
 		,{name: 'regId'}
 		,{name: 'bookNo'}
 		,{name: 'receiptNo'}
 		,{name: 'receiptName'}

 		,{name: 'receiveOfficerName'}
 		,{name: 'receiveOfficerDate'}
 		,{name: 'authority'}
 		,{name: 'receiptId'}

  		,{name: 'registrationType'}
 		,{name: 'traderType'}
 		
	 ]	
});