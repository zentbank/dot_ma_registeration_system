Ext.define('tourism.model.grid.TraderModel',{
	extend: 'Ext.data.Model',
	fields: [
//"firstName": "คณิศร์", "lastName": "มัธยางกูล", "prefixId": 0, "amphurId": 0, "taxAmphurId": 0, "provinceId": 0, 
//"taxProvinceId": 0, "postfixId": 0, 

    {
    	name: 'regId'
    },
    {
    	name: 'orgId'
    },
	{
    	 name: 'traderId',
		 persist: true
     },
     {
    	 name: 'traderType'
     },
     {
    	 name: 'licenseNo'
     },
     {
    	 name: 'traderName'
     },
     {
    	 name: 'traderNameEn'
     },
     {
    	 name: 'pronunciationName'
     },
     {
    	 name: 'traderCategory'
     },
     {
    	 name: 'traderCategoryName'
     },
     {
    	 name: 'traderCategoryGuideType'
     },
     {
    	 name: 'traderCategoryGuideTypeName'
     },
     {
    	 name: 'licenseGuideNo'
     },
     {
    	 name: 'traderGuideId'
     },
     {
    	 name: 'personGuideId'
     },
     {
    	 name: 'traderArea'
     },
     {
    	name: 'licenseStatus' 
     },
     {
      	name: 'printLicense' 
     },
     {
      	name: 'printCard' 
     },
     {
       	name: 'personId' 
     },
     {
    	 name: 'personType'
     },
     {
    	 name: 'identityNo'
     },
     {
    	 name: 'passportNo'
     },
     {
    	 name: 'firstNameEn'
     },
     {
    	 name: 'lastNameEn'
     },
     {
    	 name: 'personFullName'
     },
     {
    	 name: 'gender'
     },
     {
    	 name: 'genderName'
     },
     {
    	 name: 'personNationality'
     },
     {
    	 name: 'corporateType'
     },
     {
    	 name: 'taxIdentityNo'
     },
     {
    	 name: 'planId'
     }
     
     ]

});












