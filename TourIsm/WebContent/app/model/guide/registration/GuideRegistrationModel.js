Ext.define('tourism.model.guide.registration.GuideRegistrationModel',{
	extend: 'Ext.data.Model',
	fields: [
	         {
	        	 name: 'regId',
	        	 persist: true
	         },
	         {
	        	 name: 'registrationType'
	         },
	         {
	        	 name: 'registrationProgress'
	         },
	         {
	        	 name: 'registrationDate'
	         },
	         {
	        	 name: 'registrationNo'
	         },
	         {
	        	 name: 'remark'
	         },
	         {
			name: 'personNationality'
			},
	         {
	        	 name: 'traderId'
	         },
	         {
	        	 name: 'traderType'
	         },
	         {
	        	 name: 'licenseNo'
	         },
	         {
	        	 name: 'traderName'
	         },
	         {
	        	 name: 'traderNameEn'
	         },
	         {
	        	 name: 'pronunciationName'
	         },
	         {
	        	 name: 'licenseGuideNo'
	         },
	         {
	        	 name: 'licenseStatus'
	         },
	         {
	        	 name: 'effectiveDate'
	         },
	         {
	        	 name: 'expireDate'
	         },
	         {
	        	 name: 'printLicense'
	         },
	         {
	        	 name: 'printCard'
	         },
	         {
	        	 name: 'personId'
	         },
	         {
	        	 name: 'personType'
	         },
	         {
	        	 name: 'identityNo'
	         },
	         {
	        	 name: 'firstName'
	         },
	         {
	        	 name: 'firstNameEn' 
	         },
	         {
	        	 name: 'lastName'
	         },
	         {
	        	 name: 'lastNameEn'
	         },
	         {
	        	 name: 'plantripPerYear'
	         },
	 		{
	 			name: 'authorityName'
	 		},
	 		{
	 			name: 'ageYear'
	 		},
	 		{
	 			name: 'registrationTypeName'
	 		},
	 		{
	 			name: 'traderAddress'
	 		},
	 		{
	 			name: 'traderOwnerName'
	 		},
	 		{
	 			name: 'traderOwnerNameEn'
	 		},
            {
            	name: 'traderRecordStatus'
            },
            {
            	name: 'identityNoExpire'
            },
            {
            	name: 'passportNo'
            },
            {
            	name: 'birthDate'
            },
            {
            	name: 'universityName'
            },
            {
            	name: 'educationLevelName'
            },
            {
            	name: 'masEducationLevelId'
            },
            {
            	name: 'masUniversityId'
            },
            {
            	name: 'imageFile'
            }
	]
});