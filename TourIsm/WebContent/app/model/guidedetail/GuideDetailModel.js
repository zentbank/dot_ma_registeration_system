Ext.define('tourism.model.guidedetail.GuideDetailModel',{
	extend: 'Ext.data.Model',
	fields: [
	         {
	        	 name: 'guideId',
	        	 persist: true
	         },
	         {
	        	name: 'prefixId' 
	         },
	         {
		        name: 'firstName' 
			 },
			 {
				name: 'firstNameEn' 
		     },
		     {
		        name: 'birthDate' 
	         },
		     {
		        name: 'ageYear' 
	         },
	         {
	        	name: 'lastName' 
	         },
	         {
		        name: 'lastNameEn' 
		     },
	         {
        	 	 name: 'licenseNo'
             },
	         {
            	 name: 'effectiveDate'
	         },
	         {
	        	 name: 'expireDate'
	         },
	         {
	        	 name: 'addressNo'
	         },
	         {
	        	 name: 'villageName'
	         },
	         {
	        	 name: 'moo'
	         },
	         {
	        	 name: 'soi'
	         },
	         {
	        	 name: 'roadName'
	         },
	         {
	        	 name: 'tambolId'
	         },
	         {
	        	 name: 'amphurId'
	         },
	         {
	        	 name: 'provinceId'
	         },
	         {
	        	 name: 'postCode'
	         },
	         {
	        	 name: 'telephone'
	         },
	         {
	        	 name: 'mobileNo'
	         },
	         {
	        	 name: 'fax'
	         },
	         {
	        	 name: 'email'
	         },
	         {
	        	 name: 'educationLevelName'
	         },
	         {
	        	 name: 'educationLevelNameOth'
	         },
	         {
	        	 name: 'amateurLevel'
	         },
	         {
	        	 name: 'amateurLevelName'
	         },
	         {
	        	 name: 'experienceYear'
	         },
	         {
	        	 name: 'workType'
	         },
	         {
	        	 name: 'workTypeName'
	         },
	         {
	        	 name: 'salaryName'
	         },
	         {
	        	 name: 'characterTypeName'
	         },
	         {
	        	 name: 'characterTypeNameOth'
	         },
	         {
	        	 name: 'clubName'
	         },
	         {
	        	 name: 'clubNameTxt'
	         },
	         {
	        	 name: 'fullName'
	         },
	         {
	        	 name: 'registerDtm'
	         }
	         
	   
	]
});