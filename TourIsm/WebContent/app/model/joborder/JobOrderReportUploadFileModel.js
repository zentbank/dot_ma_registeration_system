Ext.define('tourism.model.joborder.JobOrderReportUploadFileModel',{
	extend: 'Ext.data.Model',
	fields: [
		{name : 'jobOrderId'},
		{name : 'jobOrderNo'},
		{name : 'jobOrderDate'},
		{name : 'businessLicenseNo'},
		{name : 'businessTraderName'},
		{name : 'outboundBusinessTourismName'},
		{name : 'guideName'},
		{name : 'guideLicenseNo'},
		{name : 'guideWage'},
		{name : 'arrivalDate'},
		{name : 'departureDate'},
		{name : 'touristPersonNationality'},
		{name : 'touristPersonTotal'},
		{name : 'plantripDate'},
		{name : 'plantripPlace'},
		{name : 'plantripProvinceName'},
		{name : 'plantripHotelName'},
		{name : 'plantripHotelProvinceName'},
		{name : 'organizationName'}
    ]

});