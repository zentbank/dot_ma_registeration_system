Ext.define('tourism.model.ktbbank.KTBPaymentModel',{
	extend: 'Ext.data.Model',
	fields: [
     { name: 'ktbPayId'},
    { name: 'licenseNo'},
    { name: 'traderType'},
    { name: 'HSeqNo'},
    { name: 'HPostDate'},
    { name: 'DSeqNo'},
    { name: 'DTranDate'},
    { name: 'DCustName'},
    { name: 'DReference1'},
    { name: 'DReference2'},
    { name: 'DTranAmt'},
    { name: 'DReceiptNo'},
    { name: 'progressDate'},
    { name: 'recordStatus'},
    
    { name: 'createUser'},
    { name: 'feeRegistrationType'},
    { name: 'personName'},
    { name: 'recordStatus'},
    { name: 'recordStatusText'},
    { name: 'paymentNo'},
    { name: 'DReference3'} ,
    { name: 'orgName'},
    { name: 'DTranDateFrom'},
    { name: 'DTranDateTo'}

    
     ]

});