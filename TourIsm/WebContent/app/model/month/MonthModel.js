Ext.define('tourism.model.month.MonthModel',{
	extend: 'Ext.data.Model',
	fields:[
	        {
	        	name: 'monthId',
	        	persist: true
	        },
	        {
	        	name: 'monthName'
	        }
	]        
});