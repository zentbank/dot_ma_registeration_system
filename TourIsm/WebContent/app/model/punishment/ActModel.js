Ext.define('tourism.model.punishment.ActModel', {
	extend: 'Ext.data.Model',
	fields: [
		{name: 'masActId' ,persist: true},
		{name: 'actType'},
		{name: 'actDesc'},
		{name: 'traderType'},
		{name: 'suspendId'},
		{name: 'active'},
		{name: 'revokeId'}
		
	]
});