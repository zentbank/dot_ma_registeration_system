Ext.define('tourism.model.punishment.EvidenceModel', {
	extend: 'Ext.data.Model',
	fields: [
		{name: 'punishmentId' ,persist: true},
		{name: 'punishmentDocName'},
		{name: 'punishmentDocPath'},
		{name: 'punishmentType'},
		{name: 'suspendId'},
		{name: 'revokeId'},
		{name: 'traderType'},
		{name: 'licenseNo'},
		{name: 'traderTypeName'}
	]
});