Ext.define('tourism.model.punishment.SuspensionAlertModel', {
	extend: 'Ext.data.Model',
	fields: [
		{name: 'suspendId' },
		{name: 'suspensionNo'},
		{name: 'suspensionDate'},
		{name: 'suspendPeriod'},
		{name: 'suspendStatus'},
		{name: 'suspendFrom'},
		{name: 'suspendTo'},
		{name: 'cancelDate'},
		{name: 'cancelRemark'},
		{name: 'suspendDetail'},
		{name: 'officerName'},
		{name: 'suspendAlertId' ,persist: true},
		{name: 'alertType'},
		{name: 'alertDate'},
		{name: 'responseDate'},
		{name: 'alertStatus'},
		{name: 'emailAlertDate'},
		{name: 'emailResponseDate'},
		{name: 'emailAlertStatus'},
		{name: 'revokeId'},
		{name: 'revokeAlertId'}
		
		
	]

});