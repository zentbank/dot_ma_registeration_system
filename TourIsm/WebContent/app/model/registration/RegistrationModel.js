Ext.define('tourism.model.registration.RegistrationModel', {
	extend: 'Ext.data.Model',
	fields: [
		{
			name: 'regId',
			persist: true
		},
		{
			name: 'registrationType'
		},{
			name: 'registrationTypeName'
		},
		{
			name: 'registrationProgress'
		},
		{
			name: 'registrationDate'
		},
		{
			name: 'registrationDateFrom'
		},
		{
			name: 'registrationDateTo'
		},
		{
			name: 'registrationNo'
		},
		{
			name: 'remark'
		},
		{
			name: 'orgId'
		},
		{
			name: 'orgName'
		},
		{
			name: 'traderId'
		},
		{
			name: 'traderType'
		},{
			name:'traderTypeName'
		},
		{
			name: 'licenseNo'
		},
		{
			name: 'traderName'
		},
		{
			name: 'traderNameEn'
		},
		{
			name: 'pronunciationName'
		},
		{
			name: 'traderCategory'
		},
		{
			name: 'traderCategoryName'
		},
		{
			name: 'licenseGuideNo'
		},
		{
			name: 'traderArea'
		},
		{
			name: 'licenseStatus'
		},
		{
			name: 'effectiveDate'
		},
		{
			name: 'expireDate'
		},
		{
			name: 'printLicense'
		},
		{
			name: 'printCard'
		},{name: 'printCardId'},
		{
			name: 'personId'
		},{
			name: 'personType'
		},{
			name: 'identityNo'
		},{
			name: 'prefixId'
		},{
			name: 'prefixName'
		},{
			name: 'prefixIdEn'
		},{
			name: 'prefixIdCorp'
		},{
			name: 'postfixId'
		},{
			name: 'postfixName'
		},{
			name: 'firstName'
		},{
			name: 'lastName'
		},{
			name: 'firstNameEn'
		},{
			name: 'lastNameEn'
		},{
			name: 'firstNameCorp'
		},{
			name: 'identityNoCorp'
		},{
			name: 'identityDateCorp'
		},{
			name: 'taxIdentityNo'
		},{
			name: 'taxProvinceId'
		},{
			name: 'taxAmphurId'
		},{
			name: 'corporateType'
		},{
			name: 'committeeNameSign'
		},
		{ name: 'committeeName2' },
		{ name: 'committeeName1'},
		{
			name: 'provinceIdCorp'
		},{
			name: 'birthDate'
		},{
			name: 'personNationality'
		},{
			name: 'gender'
		},{
			name: 'provinceId'
		},{
			name: 'identityDate'
		},{
			name: 'amphurId'
		},{
			name: 'planId'
		},{
			name: 'areaName'
		},{
			name: 'plantripPerYear'
		},
		{
			name: 'authorityName'
		},
		{
			name: 'traderAddress'
		},{
			name: 'traderOwnerName'
		},{
			name: 'regRecordStatus'
		},{
			name: 'traderRecordStatus'
		},{
			name: 'rersonRecordStatus'
		},{
			name: 'roleAction'
		},{
			name: 'progressStatus'
		},{
			name: 'taxAmphurName'
		},{
			name: 'taxProvinceName'
		},{
			name: 'provinceName'
		},{
			name: 'amphurName'
		},{
			name: 'prefixNameEn'
		}
		,{name: 'traderGuideDetail'}
		,{name: 'traderTrainedDetail'}
		,{name: 'ageYear'}
		,{name: 'passportNo'}
		,{name: 'identityNoExpire'}
		,{name: 'traderGuideId'}
		,{name: 'personGuideId'}
		,{name: 'eduId'}
		,{name: 'graduationCourse'}
		,{name: 'generationGraduate'}
		,{name: 'universityName'}
		,{name: 'studyDate'}
		,{name: 'personFullName'}
		,{name: 'genderName'}
		,{name: 'traderCategoryGuideTypeName'}
		,{name: 'progressBackStatus'}
		,{name: 'traderOwnerNameEn'}

		,{name: 'provinceGuideServiceName'}
		
		,{name: 'approveDate'},
		{
			name: 'approveDateFrom'
		},
		{
			name: 'approveDateTo'
		},
		{
        	name: 'guaranteeId'
        },
        {
        	name: 'traderId'
        },
        {
        	name: 'masBankId'
        },
        {
        	name: 'guaranteeType'
        },
        {
        	name: 'accountId'
        },
        {
        	name: 'accountName'
        },
        {
        	name: 'receiveDate'
        },
        {
        	name: 'receiveName'
        },
        {
        	name: 'refundName'
        },
        {
        	name: 'refundType'
        },
        {
        	name: 'refundMny'
        },
        {
        	name: 'refundInterest'
        },
        {
        	name: 'refundDate'
        },
        {
        	name: 'refundRemark'
        },
        {
        	name: 'guaranteeStatus'
        },
        {
        	name: 'guaranteeRemark'
        },
        {
        	name: 'guaranteeMny'
        },
        {
        	name: 'guaranteeInterest'
        },
        {
        	name: 'gauranteeRecordStatus'
        },
        {
        	name: 'traderRecordStatus'
        },
        {
        	name: 'bankBranchName'
        },
        {
        	name: 'accountExpireDate'
        }
		,{name: 'receiptId'}
		,{name: 'bookNo'}
		,{name: 'receiptNo'}
		,{name: 'receiptStatus'}
		,
		{
			name: 'printLicenseStatus'
		},
		{
			name: 'printLicenseStatusName'
		},
		{
			name: 'printLicenseDate'
		}
		,{name: 'licenseStatusName'}
		,{name: 'headerInfo'}
		,{name: 'detailAction'}
		,{name: 'title'}
		,{name: 'addressNo'}
		,{name: 'soi'}
		,{name: 'roadName'}
		,{name: 'tambolId'}
		,{name: 'postCode'}
		,{name: 'today'}
		,{name: 'todayForm'}
		,{
			name: 'printLicenseId'
		},{name: 'day'}
		,{name: 'month'}
		,{name: 'year'}
		,{name: 'dayF'}
		,{name: 'monthF'}
		,{name: 'yearF'}
		,{name: 'printCardStatusName'}
		,{name: 'printCardStatus'}
		,{name: 'licenseNoTo'}
		,{name: 'licenseNoFrom'}
		,{name: 'personImgId'}
		,{name: 'imgPath'}
		,{name: 'imgRecordStatus'}
		,{name: 'imgAction'}
		,{name: 'receiptDateFrom'}
		,{name: 'receiptDateTo'}
		,{ name: 'multipleJobs'}
		,{name:'guideName'}
		//sek add
		,{
			name : 'provinceGuideServiceId'
		 },
		 {
			name : 'traderCategoryType'
		 },
		 {
			 name: 'provinceGuideServiceName'
		 },{
			 name:'traderProvinceName'
		 },
		 {
		 	name: 'roleColor'
		 }
		 ,{ name: 'deactiveId'}
		,{ name: 'remark2'}
		,{ name: 'deactivateRemark'}
		,{ name: 'deactivateResonRemark'}
		,{ name: 'approveStatus'}
		,{ name: 'approveStatusName'}
		,{ name: 'deactivateBookNo'}
		,{ name: 'bookDate'}
		,{ name: 'renewLicenseStatus'}
		,{ name: 'deactivateNo'}
		,{ name: 'deactivateDate'}
		,{ name: 'officerName'}
		,{ name: 'officerId'}
		,{ name: 'deactivateTypeName'}
		,{name: 'imgData'}
		,{name: 'feePaidStatus'}
		,{name: 'feePaidStatusText'}
		,{name: 'diffFee'}
		,{name: 'paymentDate'}
		,{name: 'paymentStatus'}
		,{name: 'paymentStatusText'}
		,{name: 'totalFeePaid'}
		,{name: 'totalFee'}
		,{name: 'paymentRecId'}
		,{name: 'billPaymentId'}
		,{name: 'billPaymentNo'}
		,{name: 'imageFile'}
		
	]
});