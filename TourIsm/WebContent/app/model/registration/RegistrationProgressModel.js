Ext.define('tourism.model.registration.RegistrationProgressModel', {
	extend: 'Ext.data.Model',
	fields: [

		{name: 'regProgId' ,persist: true},
		{name: 'progress'},
		{name: 'progressStatus'},
		{name: 'progressDesc'},
		{name: 'progressDate'},
		{name: 'regId'},
		{name: 'userId'},
		{name: 'authorityName'},
		{name: 'orgId'},	
		{name: 'orgName'},
		{name: 'progressStatusName'},
		{ name: 'multipleJobs'},
		{name: 'progressStatusManager'}
		
	]
});