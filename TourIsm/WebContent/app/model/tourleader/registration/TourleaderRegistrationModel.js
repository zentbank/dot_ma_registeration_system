Ext.define('tourism.model.tourleader.registration.TourleaderRegistrationModel',{
	extend: 'Ext.data.Model',
	fields: [
	  {
		name:'regId'
	  },
	  {
	    name:'registrationType'
	  },
	  {
	    name:'registrationNo'
	  },
	  {
	    name:'registrationDate'
	  },
	  {
		  name: 'authorityName'
	  },
	  {
		  name: 'traderId',
	  },
	  {
		name: 'traderType'
	  },
	  {
		  name: 'personId'
	  },
	  {
		name: 'personNationality'
	  },
      {
    	name: 'imageFile'
      }
	]
});