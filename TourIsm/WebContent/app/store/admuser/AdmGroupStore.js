Ext.define('tourism.store.admuser.AdmGroupStore', {
	extend : 'Ext.data.Store',
	requires : [
		'tourism.model.admuser.AdmGroupModel',
		'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json',
        'Ext.data.writer.Json'
	],
	model : 'tourism.model.admuser.AdmGroupModel',
 
});