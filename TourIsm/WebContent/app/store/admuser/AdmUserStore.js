Ext.define('tourism.store.admuser.AdmUserStore', {
	extend : 'Ext.data.Store',
	requires : [
		'tourism.model.admuser.AdmUserModel',
		'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json',
        'Ext.data.writer.Json'
	],
	model : 'tourism.model.admuser.AdmUserModel',
 
});