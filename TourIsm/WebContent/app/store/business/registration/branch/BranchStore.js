Ext.define('tourism.store.business.registration.branch.BranchStore', {
	extend : 'Ext.data.Store',
	requires : [
		'tourism.model.business.registration.branch.BranchModel',
		'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json',
        'Ext.data.writer.Json'
	],
	// storeId : 'business-registrationstore',
	// autoLoad : true,
	model : 'tourism.model.business.registration.branch.BranchModel',
	pageSize : 20,
    proxy: {
	    type: 'ajax',
	    actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
	    api: {
	        read: 'business/branch/read',
	        update: 'business/branch/update',
	        create: 'business/branch/create',
	        destroy: 'business/branch/delete'
	    },
	    reader: {
	        type: 'json',
	        // metaProperty: '',
	        root: 'list',
	        // idProperty: 'emailId',
	        totalProperty: 'totalCount',
	        successProperty: 'success',
	        messageProperty: 'message'
	    },
	    writer: {
	        type: 'json',
	        writeAllFields: true, ////just send changed fields
	        allowSingle: false, //always wrap in an array
	        batch: false
	    },
	    listeners: {
	        exception: function(proxy, response, operation){

	            Ext.MessageBox.show({
	                title: 'REMOTE EXCEPTION',
	                msg: operation.getError(),
	                icon: Ext.MessageBox.ERROR,
	                buttons: Ext.Msg.OK
	            });
	        }
	    }
	}
});