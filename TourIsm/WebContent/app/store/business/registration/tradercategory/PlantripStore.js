Ext.define('tourism.store.business.registration.tradercategory.PlantripStore', {
	extend : 'Ext.data.Store',
	requires : [
		'tourism.model.business.registration.tradercategory.PlantripModel',
		'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json',
        'Ext.data.writer.Json'
	],
	model : 'tourism.model.business.registration.tradercategory.PlantripModel',
    proxy: {
	    type: 'ajax',
	    actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
	    api: {
	        read: 'business/plantrip/read',
	        update: 'business/plantrip/update',
	        create: 'business/plantrip/create',
	        destroy: 'business/plantrip/delete'
	    },
	    reader: {
	        type: 'json',
	        root: 'list',
	        totalProperty: 'totalCount',
	        successProperty: 'success',
	        messageProperty: 'message'
	    },
	    writer: {
	        type: 'json',
	        // encode: true,
	        writeAllFields: true, ////just send changed fields
	        // root: 'data',
	        allowSingle: false, //always wrap in an array
	        batch: false
	     
	    },
	    listeners: {
	        exception: function(proxy, response, operation){

	            Ext.MessageBox.show({
	                title: 'REMOTE EXCEPTION',
	                msg: operation.getError(),
	                icon: Ext.MessageBox.ERROR,
	                buttons: Ext.Msg.OK
	            });
	        }
	    }
	}
});