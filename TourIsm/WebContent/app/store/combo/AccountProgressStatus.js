Ext.define('tourism.store.combo.AccountProgressStatus', {
	extend : 'Ext.data.Store',
    fields: ['regProgressStatus', 'regProgressStatusName'],
    data : [
        {"regProgressStatus":"W", "regProgressStatusName":"ยังไม่พิมพ์ใบเสร็จ"},
        {"regProgressStatus":"A", "regProgressStatusName":"พิมพ์ใบเสร็จแล้ว"},
        {"regProgressStatus":"G", "regProgressStatusName":"เรื่องที่ยังไม่ส่งมา"}
    ]
});