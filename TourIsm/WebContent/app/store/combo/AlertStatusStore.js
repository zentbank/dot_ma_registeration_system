Ext.define('tourism.store.combo.AlertStatusStore', {
	extend : 'Ext.data.Store',
    fields: ['alertStatus', 'alertStatusName'],
    data : [
        {"alertStatus":"W", "alertStatusName":"รอการตอบรับ"},
        {"alertStatus":"A", "alertStatusName":"ตอบรับแล้ว"}
    ]
});