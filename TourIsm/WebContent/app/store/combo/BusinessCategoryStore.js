/*
ประเภทการทำธุรกิจนำเที่ยว
100=OUTBOUND
200=INBOUND
300=ในประเทศ
400=เฉพาะพื้นที่
*/
Ext.define('tourism.store.combo.BusinessCategoryStore', {
	extend : 'Ext.data.Store',
    fields: ['traderCategory', 'traderCategoryName'],
    data : [
        {"traderCategory":"100", "traderCategoryName":"ประเภททั่วไป"},
        {"traderCategory":"200", "traderCategoryName":"นำเที่ยวจากต่างประเทศ"},
        {"traderCategory":"300", "traderCategoryName":"ในประเทศ"},
        {"traderCategory":"400", "traderCategoryName":"เฉพาะพื้นที่"}
    ]
});
