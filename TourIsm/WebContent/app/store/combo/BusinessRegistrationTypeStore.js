/*
ประเภทการจดทะเบียน
ประเภทการขึ้นทะเบียนใบอนุญาต
N=จดทะเบียนใหม่
R=ชำระค่าธรรมเนียมราย 2 ปี /ต่ออายุ
B=เพิ่มข้อมูลสาขา
C=เปลี่ยนแปลงรายการ
T=Temporary ออกใบแทน
*/
Ext.define('tourism.store.combo.BusinessRegistrationTypeStore', {
	extend : 'Ext.data.Store',
    fields: ['registrationType', 'registrationTypeName'],
    data : [
        {"registrationType":"N", "registrationTypeName":"จดทะเบียนใหม่"},
        {"registrationType":"R", "registrationTypeName":"ชำระค่าธรรมเนียมราย 2 ปี"},
        {"registrationType":"B", "registrationTypeName":"เพิ่มข้อมูลสาขา"},
        {"registrationType":"C", "registrationTypeName":"เปลี่ยนแปลงรายการ"},
        {"registrationType":"T", "registrationTypeName":"ออกใบแทน"}
    ]
});
