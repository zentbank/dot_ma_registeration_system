Ext.define('tourism.store.combo.ComplaintFAQStatus', {
	extend : 'Ext.data.Store',
    fields: ['complaintStatus', 'complaintStatusName'],
    data : [
        {"complaintStatus":"S", "complaintStatusName":"ตอบข้อซักถามแล้ว"},
        {"complaintStatus":"A", "complaintStatusName":"ข้อซักถามใหม่"}
    ]
});