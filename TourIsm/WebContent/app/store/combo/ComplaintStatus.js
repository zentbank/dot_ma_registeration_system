Ext.define('tourism.store.combo.ComplaintStatus', {
	extend : 'Ext.data.Store',
    fields: ['complaintStatus', 'complaintStatusName'],
    data : [
        {"complaintStatus":"W", "complaintStatusName":"อยู่ระหว่างพิจารณาเรื่องร้องเรียน"},
        {"complaintStatus":"S", "complaintStatusName":"ตอบข้อซักถามหรือยุติเรื่อง"},
        {"complaintStatus":"R", "complaintStatusName":"ไม่รับเรื่องร้องเรียนนี้"},
        {"complaintStatus":"G", "complaintStatusName":"รับเรื่องร้องเรียน"},
        {"complaintStatus":"A", "complaintStatusName":"เรื่องร้องเรียนใหม่"}
    ]
});