Ext.define('tourism.store.combo.CorporateTypeStore', {
	extend : 'Ext.data.Store',
    fields: ['corporateType', 'corporateTypeName'],
    data : [
        {"corporateType":"1", "corporateTypeName":"บริษัทจำกัด"},
        {"corporateType":"2", "corporateTypeName":"ห้างหุ้นส่วนจำกัด"},
        {"corporateType":"3", "corporateTypeName":"บริษัทจำกัด มหาชน"}
       
    ]
});