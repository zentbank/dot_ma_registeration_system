Ext.define('tourism.store.combo.CountryStrore', {
    extend : 'Ext.data.Store',
    model: 'tourism.model.combo.CountryModel',
    requires: [
         'tourism.model.combo.CountryModel'
    ],
    proxy: {
         type: 'ajax',
         actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
         url: 'business/combo/country',
         reader: {
             type: 'json',
             root: 'list'
         }
     }
     // ,
     // autoLoad: true
 });