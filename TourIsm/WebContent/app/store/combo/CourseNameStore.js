Ext.define('tourism.store.combo.CourseNameStore',{
	extend: 'Ext.data.Store',
	model: 'tourism.model.combo.CourseModel',
	requires: [
	             'tourism.model.combo.CourseModel'
	        ],
	        proxy: {
	             type: 'ajax',
	             actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
	             url: 'business/combo/course',
	             reader: {
	                 type: 'json',
	                 root: 'list'
	             }
	         }
});