Ext.define('tourism.store.combo.DeactivateStatusStore', {
	extend : 'Ext.data.Store',
    fields: ['approveStatus', 'approveStatusName'],
    data : [
        {"approveStatus":"W", "approveStatusName":"รับเรื่องรออนุมัติ"},
        {"approveStatus":"A", "approveStatusName":"อนุมัติแล้ว"},
        {"approveStatus":"C", "approveStatusName":"ยกเลิกการยกเลิกใบอนุญาต"}
    ]
});
