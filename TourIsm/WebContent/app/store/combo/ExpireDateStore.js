Ext.define('tourism.store.combo.ExpireDateStore', {
	extend : 'Ext.data.Store',
    fields: ['expireDueDateFeeStatus', 'expireDueDateFeeStatusName'],
    data : [
        {"expireDueDateFeeStatus":"E1", "expireDueDateFeeStatusName":"ครบกำหนดชำระค่าธรรมเนียม 120 วัน"},
        {"expireDueDateFeeStatus":"E2", "expireDueDateFeeStatusName":"ขาดชำระค่าธรรมเนียม 3 เดือน"},
        {"expireDueDateFeeStatus":"E3", "expireDueDateFeeStatusName":"ขาดชำระค่าธรรมเนียม 9 เดือน"}
       
    ]
});