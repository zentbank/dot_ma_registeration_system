/*
ประเภทมัคคุเทศก์
100=ชนิดมัคคุเทศก์ทั่วไป (ต่างประเทศ)
101=ชนิดมัคคุเทศก์ทั่วไป (ไทย)
201=มัคคุเทศก์เฉพาะ (ต่างประเทศ-เฉพาะพื้นที่)
202=มัคคุเทศก์เฉพาะ (ไทย-เฉพาะพื้นที่)
203=มัคคุเทศก์เฉพาะ (เดินป่า)
204=มัคคุเทศก์เฉพาะ (ศิลปะวัฒนธรรม)
205=มัคคุเทศก์เฉพาะ (ทางทะเล)
206=มัคคุเทศก์เฉพาะ (ทะเลชายฝั่ง)
207=มัคคุเทศก์เฉพาะ (แหล่งท่องเที่ยวธรรมชาติ)
208=มัคคุเทศก์เฉพาะ (วัฒนธรรมท้องถิ่น)
*/
Ext.define('tourism.store.combo.GuideCategoryStore', {
	extend : 'Ext.data.Store',
	model: 'tourism.model.combo.GuideCategoryModel',
   // fields: ['traderCategory', 'traderCategoryName'],
	requires : [
	    		'tourism.model.combo.GuideCategoryModel'
	    	],
    data : [
        {"traderCategory":"100", "traderCategoryName":"ชนิดมัคคุเทศก์ทั่วไป (ต่างประเทศ)"},
        {"traderCategory":"101", "traderCategoryName":"ชนิดมัคคุเทศก์ทั่วไป (ไทย)"},

        {"traderCategory":"200", "traderCategoryName":"มัคคุเทศก์เฉพาะ (ต่างประเทศ-เฉพาะพื้นที่)"},
        {"traderCategory":"201", "traderCategoryName":"มัคคุเทศก์เฉพาะ (ไทย-เฉพาะพื้นที่)"},
        {"traderCategory":"202", "traderCategoryName":"มัคคุเทศก์เฉพาะ (เดินป่า)"},
        {"traderCategory":"203", "traderCategoryName":"มัคคุเทศก์เฉพาะ (ศิลปะวัฒนธรรม)"},
        {"traderCategory":"204", "traderCategoryName":"มัคคุเทศก์เฉพาะ (ทางทะเล)"},
        {"traderCategory":"205", "traderCategoryName":"มัคคุเทศก์เฉพาะ (ทะเลชายฝั่ง)"},
        {"traderCategory":"206", "traderCategoryName":"มัคคุเทศก์เฉพาะ (แหล่งท่องเที่ยวธรรมชาติ)"},
        {"traderCategory":"207", "traderCategoryName":"มัคคุเทศก์เฉพาะ (วัฒนธรรมท้องถิ่น)"}

    ]
});
