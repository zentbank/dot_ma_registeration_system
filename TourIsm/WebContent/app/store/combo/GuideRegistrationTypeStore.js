/*
ประเภทการจดทะเบียน
ประเภทการขึ้นทะเบียนใบอนุญาต
N=จดทะเบียนใหม่
R=ต่ออายุ
C=เปลี่ยนแปลงรายการ
*/
Ext.define('tourism.store.combo.GuideRegistrationTypeStore', {
	extend : 'Ext.data.Store',
    fields: ['registrationType', 'registrationTypeName'],
    data : [
        {"registrationType":"N", "registrationTypeName":"จดทะเบียนใหม่"},
        {"registrationType":"R", "registrationTypeName":"ต่ออายุ"},
        {"registrationType":"C", "registrationTypeName":"เปลี่ยนแปลงรายการ"},
        {"registrationType":"T", "registrationTypeName":"ออกใบแทน"}
    ]
});
