Ext.define('tourism.store.combo.LicenseStatusStore', {
	extend : 'Ext.data.Store',
    fields: ['licenseStatus', 'licenseStatusName'],
    data : [
        {"licenseStatus":"N", "licenseStatusName":"ปกติ"},
        {"licenseStatus":"C", "licenseStatusName":"ยกเลิก"},
        {"licenseStatus":"S", "licenseStatusName":"พักใช้"},
        {"licenseStatus":"R", "licenseStatusName":"เพิกถอน"},
//        {"licenseStatus":"T", "licenseStatusName":""},
        {"licenseStatus":"D", "licenseStatusName":"ไม่ผ่านการตรวจสอบ/ยกเลิกการจดทะเบียน"},
        {"licenseStatus":"E", "licenseStatusName":"หมดอายุตามกฎหมาย"}
       
    ]
});