 Ext.define('tourism.store.combo.MasAmphurStore', {
    extend : 'Ext.data.Store',
    model: 'tourism.model.combo.MasAmphurModel',
    requires: [
         'tourism.model.combo.MasAmphurModel'
    ],
    proxy: {
         type: 'ajax',
         actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
         url: 'business/combo/amphur',
         reader: {
             type: 'json',
             root: 'list'
         }
     }
     // ,
     // autoLoad: true
 });