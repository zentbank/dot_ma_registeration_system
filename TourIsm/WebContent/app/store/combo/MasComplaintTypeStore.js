
 Ext.define('tourism.store.combo.MasComplaintTypeStore', {
    extend : 'Ext.data.Store',
    model: 'tourism.model.combo.MasComplaintTypeModel',
    requires: [
         'tourism.model.combo.MasComplaintTypeModel'
    ],
    proxy: {
         type: 'ajax',
         actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
         url: 'business/combo/complainttype',
         reader: {
             type: 'json',
             root: 'list'
         }
     }
     // ,
     // autoLoad: true
 });