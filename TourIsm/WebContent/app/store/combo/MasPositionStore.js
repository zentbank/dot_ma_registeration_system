 Ext.define('tourism.store.combo.MasPositionStore', {
    extend : 'Ext.data.Store',
    model: 'tourism.model.combo.MasPositionModel',
    requires: [
         'tourism.model.combo.MasPositionModel'
    ],
    proxy: {
         type: 'ajax',
         actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
         url: 'business/combo/position',
         reader: {
             type: 'json',
             root: 'list'
         }
     }
 });