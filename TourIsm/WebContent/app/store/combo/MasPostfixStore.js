 /*

{
    xtype: 'combo',
    fieldLabel: 'คำนำหน้าชื่อ',
    store: 'tourism.store.combo.MasPrefixStore',
    queryMode: 'remote',
    displayField: 'prefixName',
    valueField: 'prefixId',
    hiddenName: 'prefixId',
    triggerAction: 'all'
}
 */

 Ext.define('tourism.store.combo.MasPostfixStore', {
    extend : 'Ext.data.Store',
    model: 'tourism.model.combo.MasPostfixModel',
    requires: [
         'tourism.model.combo.MasPostfixModel'
    ],
    proxy: {
         type: 'ajax',
         actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
         url: 'business/combo/postfix',
         reader: {
             type: 'json',
             root: 'list'
         }
     }
     // ,
     // autoLoad: true
 });