 Ext.define('tourism.store.combo.MasProvinceStore', {
    extend : 'Ext.data.Store',
    model: 'tourism.model.combo.MasProvinceModel',
    requires: [
         'tourism.model.combo.MasProvinceModel'
    ],
    proxy: {
         type: 'ajax',
         actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
         url: 'business/combo/province',
         reader: {
             type: 'json',
             root: 'list'
         }
     }
     // ,
     // autoLoad: true
 });