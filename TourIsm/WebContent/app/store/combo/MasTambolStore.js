 Ext.define('tourism.store.combo.MasTambolStore', {
    extend : 'Ext.data.Store',
    model: 'tourism.model.combo.MasTambolModel',
    requires: [
         'tourism.model.combo.MasTambolModel'
    ],
    proxy: {
         type: 'ajax',
         actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
         url: 'business/combo/tambol',
         reader: {
             type: 'json',
             root: 'list'
         }
     }
     // ,
     // autoLoad: true
 });