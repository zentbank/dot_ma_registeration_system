Ext.define('tourism.store.combo.MasUniversityStore', {
    extend : 'Ext.data.Store',
    model: 'tourism.model.combo.MasUniversityModel',
    requires: [
         'tourism.model.combo.MasUniversityModel'
    ],
    proxy: {
         type: 'ajax',
         actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
         url: 'tourleader/combo/university',
         reader: {
             type: 'json',
             root: 'list'
         }
     }
 });