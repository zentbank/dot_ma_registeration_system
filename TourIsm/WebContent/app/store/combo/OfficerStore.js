Ext.define('tourism.store.combo.OfficerStore',{
	extend: 'Ext.data.Store'
	,requires : [
    		'tourism.model.combo.OfficerModel',
    		'Ext.data.proxy.Ajax',
            'Ext.data.reader.Json',
            'Ext.data.writer.Json'
    	],
    	
    	model : 'tourism.model.combo.OfficerModel',
    	
        proxy: {
    	    type: 'ajax',
    	    actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
    	    api: {
    	        read: 'tourleader/account/registration/officer'
    	    },
    	    reader: {
    	        type: 'json',
    	        // metaProperty: '',
    	        root: 'list',
    	        // idProperty: 'emailId',
    	        totalProperty: 'totalCount',
    	        successProperty: 'success',
    	        messageProperty: 'message'
    	    },
    	    writer: {
    	        type: 'json',
    	        // encode: true,
    	        writeAllFields: true, ////just send changed fields
    	        // root: 'data',
    	        allowSingle: false, //always wrap in an array
    	        batch: false
    	    },
    	    listeners: {
    	        exception: function(proxy, response, operation){

    	            Ext.MessageBox.show({
    	                title: 'REMOTE EXCEPTION',
    	                msg: operation.getError(),
    	                icon: Ext.MessageBox.ERROR,
    	                buttons: Ext.Msg.OK
    	            });
    	        }
    	    }
    	}
});