

 Ext.define('tourism.store.combo.OrganizationStore', {
    extend : 'Ext.data.Store',
    model: 'tourism.model.combo.OrganizationModel',
    requires: [
         'tourism.model.combo.OrganizationModel'
    ],
    proxy: {
         type: 'ajax',
         actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
         url: 'business/combo/organization',
         reader: {
             type: 'json',
             root: 'list'
         }
     }
     // ,
     // autoLoad: true
 });