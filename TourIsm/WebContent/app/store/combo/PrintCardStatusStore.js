Ext.define('tourism.store.combo.PrintCardStatusStore', {
	extend: 'Ext.data.Store',
	fields: ['printCardStatus', 'printCardStatusName'],
    data : [
        {"printCardStatus":"W", "printCardStatusName":"ยังไม่ได้พิมพ์บัตร"},
        {"printCardStatus":"A", "printCardStatusName":"พิมพ์บัตรแล้ว"}
    ]
	
});