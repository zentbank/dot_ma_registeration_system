Ext.define('tourism.store.combo.PrintStatusStore', {
	extend: 'Ext.data.Store',
	fields: ['printLicenseStatus', 'printLicenseStatusName'],
    data : [
        {"printLicenseStatus":"W", "printLicenseStatusName":"ยังไม่พิมพ์ใบอนุญาต"},
        {"printLicenseStatus":"A", "printLicenseStatusName":"พิมพ์ใบอนุญาตแล้ว"}
    ]
	
});