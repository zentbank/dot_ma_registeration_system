Ext.define('tourism.store.combo.ProgressStatusStore', {
	extend : 'Ext.data.Store',
    fields: ['progressStatus', 'progressStatusName'],
    data : [
        {"progressStatus":"W", "progressStatusName":"อยู่ระหว่างดำเนินการ"},
        {"progressStatus":"S", "progressStatusName":"ยุติเรื่องร้องเรียน"}
    ]
});