Ext.define('tourism.store.combo.RefundGuaranteeStatusStore', {
	extend : 'Ext.data.Store',
    fields: ['guaranteeStatus', 'guaranteeStatusName'],
    data : [
        // {"guaranteeStatus":"N", "guaranteeStatusName":"รอการตอบรับ"},
        {"guaranteeStatus":"R", "guaranteeStatusName":"คืนหลักประกันแล้ว"},
        {"guaranteeStatus":"N", "guaranteeStatusName":"ปกติ"}
    ]
});