

Ext.define('tourism.store.combo.RegProgressStatusStore', {
	extend : 'Ext.data.Store',
    fields: ['regProgressStatus', 'regProgressStatusName'],
    data : [
        {"regProgressStatus":"W", "regProgressStatusName":"รับเรื่องแล้ว"},
        {"regProgressStatus":"A", "regProgressStatusName":"ส่งเรื่องแล้ว"},
        {"regProgressStatus":"G", "regProgressStatusName":"เรื่องที่ยังไม่ส่งมา"}
    ]
});