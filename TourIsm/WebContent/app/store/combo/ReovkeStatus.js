Ext.define('tourism.store.combo.ReovkeStatus', {
	extend : 'Ext.data.Store',
    fields: ['revokeStatus', 'revokeStatusName'],
    data : [
        {"revokeStatus":"R", "revokeStatusName":"เพิกถอนใบอนุญาต"},
        {"revokeStatus":"C", "revokeStatusName":"ยกเลิกการเพิกถอน"},
         {"revokeStatus":"N", "revokeStatusName":"ครบกำหนดการเพิกถอน"},
        {"revokeStatus":"W", "revokeStatusName":"อยู่ระหว่างดำเนินการเพิกถอน"},
        {"revokeStatus":"A", "revokeStatusName":"อนุมัติให้เพิกถอน"}
    ]
});