Ext.define('tourism.store.combo.SuspendStatus', {
	extend : 'Ext.data.Store',
    fields: ['suspendStatus', 'suspendStatusName'],
    data : [
        {"suspendStatus":"S", "suspendStatusName":"พักใช้ใบอนุญาต"},
        {"suspendStatus":"C", "suspendStatusName":"ยกเลิกการพักใช้"},
         {"suspendStatus":"N", "suspendStatusName":"ครบกำหนดการพักใช้"},
        {"suspendStatus":"W", "suspendStatusName":"อยู่ระหว่างดำเนินการพักใช้"},
        {"suspendStatus":"A", "suspendStatusName":"อนุมัติให้พักใช้"}
    ]
});