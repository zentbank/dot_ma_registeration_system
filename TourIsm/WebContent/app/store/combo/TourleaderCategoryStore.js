/*
ผู้นำเที่ยว
100=ตามกฎกระทรวง
200=เป็นมัคคุเทศก์
300=ผ่านการอบรม
400=จบการศึกษาด้านการท่องเที่ยว
*/
Ext.define('tourism.store.combo.TourleaderCategoryStore', {
	extend : 'Ext.data.Store',
    fields: ['traderCategory', 'traderCategoryName'],
    data : [
        {"traderCategory":"100", "traderCategoryName":"ตามกฎกระทรวง"},
        {"traderCategory":"200", "traderCategoryName":"เป็นมัคคุเทศก์"},
        {"traderCategory":"300", "traderCategoryName":"ผ่านการอบรม"},
        {"traderCategory":"400", "traderCategoryName":"จบการศึกษาด้านการท่องเที่ยว"}

    ]
});
