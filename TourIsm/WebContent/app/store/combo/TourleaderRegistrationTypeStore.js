/*
ประเภทการจดทะเบียน
ประเภทการขึ้นทะเบียนใบอนุญาต
N=จดทะเบียนใหม่
C=เปลี่ยนแปลงรายการ
T=Temporary ออกใบแทน
*/
Ext.define('tourism.store.combo.TourleaderRegistrationTypeStore', {
	extend : 'Ext.data.Store',
    fields: ['registrationType', 'registrationTypeName'],
    data : [
        {"registrationType":"N", "registrationTypeName":"จดทะเบียนใหม่"},
        {"registrationType":"C", "registrationTypeName":"เปลี่ยนแปลงรายการ"},
        {"registrationType":"T", "registrationTypeName":"ออกใบแทน"},
        
    ]
});
