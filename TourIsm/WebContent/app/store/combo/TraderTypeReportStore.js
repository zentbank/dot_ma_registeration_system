Ext.define('tourism.store.combo.TraderTypeReportStore', {
	extend : 'Ext.data.Store',
    fields: ['traderType', 'traderTypeName'],
    data : [
        {"traderType":"B", "traderTypeName":"ธุรกิจนำเที่ยว"},
        {"traderType":"G", "traderTypeName":"มัคคุเทศก์"}
    ]
});