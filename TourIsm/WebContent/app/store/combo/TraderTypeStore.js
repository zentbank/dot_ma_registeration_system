Ext.define('tourism.store.combo.TraderTypeStore', {
	extend : 'Ext.data.Store',
    fields: ['traderType', 'traderTypeName'],
    data : [
        {"traderType":"B", "traderTypeName":"ธุรกิจนำเที่ยว"},
        {"traderType":"G", "traderTypeName":"มัคคุเทศก์"},
        {"traderType":"L", "traderTypeName":"ผู้นำเที่ยว"}
       
    ]
});