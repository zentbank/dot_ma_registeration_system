Ext.define('tourism.store.course.PersonTrainStore',{
	extend: 'Ext.data.Store',
	requires: [
	           'tourism.model.grid.EducationModel',
	           'Ext.data.proxy.Ajax',
	           'Ext.data.reader.Json',
	           'Ext.data.writer.Json'
	          ],
    model : 'tourism.model.grid.PersonTrainedModel',
    
   pageSize: 20,
    
    proxy: {
	    type: 'ajax',
	    actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
	    api: {
	        read: 'business/course/readperson',
	        //update: 'tourleader/education/update',
	       // create: 'tourleader/education/create',
	        destroy: 'tourleader/course/deleteperson'
	    },
	    reader: {
	        type: 'json',
	        // metaProperty: '',
	        root: 'list',
	        // idProperty: 'emailId',
	        totalProperty: 'totalCount',
	        successProperty: 'success',
	        messageProperty: 'message'
	    },
	    writer: {
	        type: 'json',
	        // encode: true,
	        writeAllFields: true, ////just send changed fields
	        // root: 'data',
	        allowSingle: false, //always wrap in an array
	        batch: false
	    },
	    listeners: {
	        exception: function(proxy, response, operation){

	            Ext.MessageBox.show({
	                title: 'REMOTE EXCEPTION',
	                msg: operation.getError(),
	                icon: Ext.MessageBox.ERROR,
	                buttons: Ext.Msg.OK
	            });
	        }
	    }
	}
});