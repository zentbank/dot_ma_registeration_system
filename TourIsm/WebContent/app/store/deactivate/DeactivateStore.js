Ext.define('tourism.store.deactivate.DeactivateStore', {
	extend : 'Ext.data.Store',
	requires : [
		'tourism.model.deactivate.DeactivateModel',
		'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json',
        'Ext.data.writer.Json'
	]
 
});