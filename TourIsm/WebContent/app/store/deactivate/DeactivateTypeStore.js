Ext.define('tourism.store.deactivate.DeactivateTypeStore', {
	extend : 'Ext.data.Store',
	requires : [
		'tourism.model.deactivate.DeactivateTypeModel',
		'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json',
        'Ext.data.writer.Json'
	],
	model : 'tourism.model.deactivate.DeactivateTypeModel',
 
});