Ext.define('tourism.store.grid.DocumentRegistrationStore', {
	extend : 'Ext.data.Store',
	requires : [
		'tourism.model.grid.DocumentRegistrationModel',
		'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json',
        'Ext.data.writer.Json'
	],
	
	
	groupField: 'branchTypeName',
	model : 'tourism.model.grid.DocumentRegistrationModel',
	pageSize : 20,
	
    proxy: {
	    type: 'ajax',
	    actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
	    api: {
	        read: 'business/document/registration/readDocument',
	        update: 'business/document/registration/updateDocument',
	        create: 'business/document/registration/createDocument',
	        destroy: 'business/document/registration/deleteDocument'
	    },
	    reader: {
	        type: 'json',
	        // metaProperty: '',
	        root: 'list',
	        // idProperty: 'emailId',
	        totalProperty: 'totalCount',
	        successProperty: 'success',
	        messageProperty: 'message'
	    },
	    writer: {
	        type: 'json',
	        // encode: true,
	        writeAllFields: true, ////just send changed fields
	        // root: 'data',
	        allowSingle: false, //always wrap in an array
	        batch: false
	    },
	    listeners: {
	        exception: function(proxy, response, operation){

	            Ext.MessageBox.show({
	                title: 'REMOTE EXCEPTION',
	                msg: operation.getError(),
	                icon: Ext.MessageBox.ERROR,
	                buttons: Ext.Msg.OK
	            });
	        }
	    }
	}
});