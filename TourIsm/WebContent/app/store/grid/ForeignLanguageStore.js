Ext.define('tourism.store.grid.ForeignLanguageStore', {
	extend : 'Ext.data.Store',
	requires : [
		'tourism.model.grid.ForeignLanguageModel',
		'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json',
        'Ext.data.writer.Json'
	],
	
	model : 'tourism.model.grid.ForeignLanguageModel',
	pageSize : 20,
	
    proxy: {
	    type: 'ajax',
	    actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
	    api: {
	        read: 'tourleader/foreignlanguage/read',
	        update: 'tourleader/foreignlanguage/update',
	        create: 'tourleader/foreignlanguage/create',
	        destroy: 'tourleader/foreignlanguage/delete'
	    },
	    reader: {
	        type: 'json',
	        // metaProperty: '',
	        root: 'list',
	        // idProperty: 'emailId',
	        totalProperty: 'totalCount',
	        successProperty: 'success',
	        messageProperty: 'message'
	    },
	    writer: {
	        type: 'json',
	        // encode: true,
	        writeAllFields: true, ////just send changed fields
	        // root: 'data',
	        allowSingle: false, //always wrap in an array
	        batch: false
	    },
	    listeners: {
	        exception: function(proxy, response, operation){

	            Ext.MessageBox.show({
	                title: 'REMOTE EXCEPTION',
	                msg: operation.getError(),
	                icon: Ext.MessageBox.ERROR,
	                buttons: Ext.Msg.OK
	            });
	        }
	    }
	}
});