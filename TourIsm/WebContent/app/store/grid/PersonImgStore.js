Ext.define('tourism.store.grid.PersonImgStore',{
	extend : 'Ext.data.Store',
	requires : [
		'tourism.model.grid.PersonImgModel',
		'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json',
        'Ext.data.writer.Json'
	],
	
	
	groupField: 'branchTypeName',
	model : 'tourism.model.grid.PersonImgModel',
	pageSize : 20,
	
    proxy: {
	    type: 'ajax',
	    actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
	    api: {
	        read: 'business/printcard/registration/readImg',
	        update: 'business/printcard/registration/updateImg',
	        create: 'business/printcard/registration/createImg',
	        destroy: 'business/printcard/registration/deleteImg'
	    },
	    reader: {
	        type: 'json',
	        // metaProperty: '',
	        root: 'list',
	        // idProperty: 'emailId',
	        totalProperty: 'totalCount',
	        successProperty: 'success',
	        messageProperty: 'message'
	    },
	    writer: {
	        type: 'json',
	        // encode: true,
	        writeAllFields: true, ////just send changed fields
	        // root: 'data',
	        allowSingle: false, //always wrap in an array
	        batch: false
	    },
	    listeners: {
	        exception: function(proxy, response, operation){

	            Ext.MessageBox.show({
	                title: 'REMOTE EXCEPTION',
	                msg: operation.getError(),
	                icon: Ext.MessageBox.ERROR,
	                buttons: Ext.Msg.OK
	            });
	        }
	    }
	}
});