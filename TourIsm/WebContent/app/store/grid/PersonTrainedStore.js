Ext.define('tourism.store.grid.PersonTrainedStore', {
    extend : 'Ext.data.Store',
    model: 'tourism.model.grid.PersonTrainedModel',
    requires: [
         'tourism.model.grid.PersonTrainedModel',
         'Ext.data.proxy.Ajax',
         'Ext.data.reader.Json',
         'Ext.data.writer.Json'
    ],
    pageSize : 20,
    proxy: {
         type: 'ajax',
         actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
         url: 'tourleader/personTrained/read',
         reader: {
             type: 'json',
             root: 'list'
         }
     }
 });