Ext.define('tourism.store.grid.ReceiptDetailStore',{
	extend: 'Ext.data.Store'
	,requires : [
    		'tourism.model.grid.ReceiptDetailModel',
    		'Ext.data.proxy.Ajax',
            'Ext.data.reader.Json',
            'Ext.data.writer.Json'
    	],
    	
//    	groupField: 'feeName',
    	model : 'tourism.model.grid.ReceiptDetailModel',
//    	pageSize : 20,
    	
        proxy: {
    	    type: 'ajax',
    	    actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
    	    api: {
    	        read: 'tourleader/account/registration/receiptdetailfee'
    	        // update: 'tourleader/account/registration/updatereceipt',
    	        // create: 'tourleader/account/registration/create',
    	        // destroy: 'tourleader/account/registration/delete'
    	    },
    	    reader: {
    	        type: 'json',
    	        // metaProperty: '',
    	        root: 'list',
    	        // idProperty: 'emailId',
    	        totalProperty: 'totalCount',
    	        successProperty: 'success',
    	        messageProperty: 'message'
    	    },
    	    writer: {
    	        type: 'json',
    	        // encode: true,
    	        writeAllFields: true, ////just send changed fields
    	        // root: 'data',
    	        allowSingle: false, //always wrap in an array
    	        batch: false
    	    },
    	    listeners: {
    	        exception: function(proxy, response, operation){

    	            Ext.MessageBox.show({
    	                title: 'REMOTE EXCEPTION',
    	                msg: operation.getError(),
    	                icon: Ext.MessageBox.ERROR,
    	                buttons: Ext.Msg.OK
    	            });
    	        }
    	    }
    	}
});