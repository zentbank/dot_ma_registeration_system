Ext.define('tourism.store.grid.TraderStore', {
    extend : 'Ext.data.Store',
    model: 'tourism.model.grid.TraderModel',
    requires: [
         'tourism.model.grid.TraderModel',
         'Ext.data.proxy.Ajax',
         'Ext.data.reader.Json',
         'Ext.data.writer.Json'
    ],
    pageSize : 20,
    proxy: {
         type: 'ajax',
         actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
         url: 'tourleader/registration/toruleader/loadguide',
         reader: {
             type: 'json',
             root: 'list'
         }
     }
 });