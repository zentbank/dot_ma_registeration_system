Ext.define('tourism.store.guide.registration.GuideRegistrationLoadIdCardStore', {
    extend : 'Ext.data.Store',
    model: 'tourism.model.guide.registration.GuideRegistrationModel',
    requires: [
         'tourism.model.guide.registration.GuideRegistrationModel',
         'Ext.data.proxy.Ajax',
         'Ext.data.reader.Json',
         'Ext.data.writer.Json'
    ],
	storeId: 'guide-registrationLoadIdCardStore',
	groupField: 'registrationTypeName',
	pageSize: 20,
    proxy: {
         type: 'ajax',
         actionMethods: {create: 'POST', read: 'GET', update: 'POST', destroy: 'POST'},
         url: 'http://localhost:8088/cardReader/read/idcard',
         reader: {
             type: 'json',
             root: 'list'
         }
     }
 });