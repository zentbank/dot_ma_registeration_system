Ext.define('tourism.store.guide.registration.GuideRegistrationStore',{
	extend: 'Ext.data.Store',
	requires: [
	           'tourism.model.guide.registration.GuideRegistrationModel',
	           'Ext.data.proxy.Ajax',
	           'Ext.data.reader.Json',
	           'Ext.data.writer.Json'
	],
	storeId: 'guide-registrationStore',
	groupField: 'registrationTypeName',
	model: 'tourism.model.business.registration.BusinessRegistrationModel',
	pageSize: 20,

	proxy: {
		type: 'ajax',
	    actionMethods:	{create: 'POST', read: 'POST', update: 'POST' , destroy: 'POST'},
	    api: {
	        // read: 'guide/registration/readtraderbetweenregistration',
	        read: 'business/key/registration/read'
	        //update: 'guide/registration/update',
	        //create: 'guide/registration/create'
	    },
	reader: {
	     type: 'json',
	        // metaProperty: '',
	     root: 'list',
	        // idProperty: 'emailId',
	     totalProperty: 'totalCount',
	     successProperty: 'success',
	     messageProperty: 'message'
	    },
	writer: {
	     type: 'json',
	        // encode: true,
	     writeAllFields: true, ////just send changed fields
	        // root: 'data',
	     allowSingle: false, //always wrap in an array
	     batch: false
	        // ,
	        // writeRecords: function(request, data){
	        // 	//console.log("writeRecords="+data);
	        // 	//console.log(data);
	        // 	// var dataArray = new Object;
	        // 	// dataArray["org.sss.sms.dto.TransmissionDTO"] = data;

	        // 	request.jsonData = data;
	        // 	return request;
	        // }
	    },
	listeners: {
	     exception: function(proxy, response, operation){

	    	 Ext.MessageBox.show({
	                title: 'REMOTE EXCEPTION',
	                msg: operation.getError(),
	                icon: Ext.MessageBox.ERROR,
	                buttons: Ext.Msg.OK
	         });
	        }
	    }
	    
	}
	
	
});