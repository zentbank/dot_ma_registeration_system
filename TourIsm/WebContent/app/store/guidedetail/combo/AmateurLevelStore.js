Ext.define('tourism.store.guidedetail.combo.AmateurLevelStore', {
	extend: 'Ext.data.Store',
	fields: ['amateurLevel','amateurLevelName'],
    data : [
        {"amateurLevel":"A","amateurLevelName":"ประกอบเป็นอาชีพหลัก"},
        {"amateurLevel":"B","amateurLevelName":"ประกอบอาชีพเป็นบางครั้งบางคราว"},
        {"amateurLevel":"C","amateurLevelName":"ไม่ได้ประกอบอาชีพเป็นมัคคุเทศก์เลย"}
    ]
	
});