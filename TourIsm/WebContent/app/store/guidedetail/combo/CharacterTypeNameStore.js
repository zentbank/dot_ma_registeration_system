Ext.define('tourism.store.guidedetail.combo.CharacterTypeNameStore', {
	extend: 'Ext.data.Store',
	fields: ['characterTypeName'],
    data : [
        {"characterTypeName":"การนำเที่ยว"},
        {"characterTypeName":"Transfer"},
        {"characterTypeName":"การนำเที่ยว/Transfer"}
    ]
	
});