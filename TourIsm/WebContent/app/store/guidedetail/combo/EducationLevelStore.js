Ext.define('tourism.store.guidedetail.combo.EducationLevelStore', {
	extend: 'Ext.data.Store',
	fields: ['educationLevelName'],
    data : [
        {"educationLevelName":"ต่ำกว่าปริญญาตรี"},
        {"educationLevelName":"ปริญญาตรี"},
        {"educationLevelName":"ปริญญาโท"},
        {"educationLevelName":"สูงกว่าปริญญาโท"}
        //,{"educationLevelName":"อื่นๆโปรดระบุ"}
    ]
	
});