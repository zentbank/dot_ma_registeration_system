Ext.define('tourism.store.guidedetail.combo.ExperienceYearStore', {
	extend: 'Ext.data.Store',
	fields: ['experienceYear'],
    data : [
        {"experienceYear":"น้อยกว่า 1 ปี"},
        {"experienceYear":"1 - 3 ปี"},
        {"experienceYear":"4 - 6 ปี"},
        {"experienceYear":"7 - 9 ปี"}
        ,{"experienceYear":"10 - 12 ปี"}
        ,{"experienceYear":"มากกว่า 12 ปีขึ้นไป"}
    ]
	
});