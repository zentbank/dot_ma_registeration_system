Ext.define('tourism.store.guidedetail.combo.LanguageLevelStore', {
	extend: 'Ext.data.Store',
	fields: ['languageLevel','languageLevelName'],
    data : [
        {"languageLevel":"A","languageLevelName":"ดีมาก"},
        {"languageLevel":"B","languageLevelName":"ดี"},
        {"languageLevel":"C","languageLevelName":"พอใช้"}
    ]
	
});