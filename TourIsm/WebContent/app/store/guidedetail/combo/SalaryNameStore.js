Ext.define('tourism.store.guidedetail.combo.SalaryNameStore', {
	extend: 'Ext.data.Store',
	fields: ['salaryName'],
    data : [
        {"salaryName":"ต่ำกว่า 10,000 บาท"},
        {"salaryName":"10,001 - 15,000 บาท"},
        {"salaryName":"15,001 - 20,000 บาท"},
        {"salaryName":"20,000 - 25,000 บาท"}
        ,{"salaryName":"25,001 - 30,000 บาท"}
        ,{"salaryName":"มากกว่า 30,000 บาท"}
    ]
	
});