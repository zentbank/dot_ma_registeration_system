Ext.define('tourism.store.guidedetail.combo.WorkTypeStore', {
	extend: 'Ext.data.Store',
	fields: ['workType','workTypeName'],
    data : [
        {"workType":"A","workTypeName":"เป็นมัคคุเทศก์อิสระ"},
        {"workType":"B","workTypeName":"เป็นมัคคุเทศก์ประจำของบริษัทนำเที่ยว"},
        {"workType":"C","workTypeName":"อื่นๆ"}
    ]
	
});