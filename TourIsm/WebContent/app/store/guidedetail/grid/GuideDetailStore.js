Ext.define('tourism.store.guidedetail.grid.GuideDetailStore',{
	extend: 'Ext.data.Store',
	requires: [
	           'tourism.model.guidedetail.GuideDetailModel',
	           'Ext.data.proxy.Ajax',
	           'Ext.data.reader.Json',
	           'Ext.data.writer.Json'
	],
	storeId: 'guidedetail-store',
	groupField: 'registrationTypeName',
	model: 'tourism.model.guidedetail.GuideDetailModel',
	pageSize: 20,

	proxy: {
		type: 'ajax',
	    actionMethods:	{create: 'POST', read: 'POST', update: 'POST' , destroy: 'POST'},
	    api: {
	        read: 'guide/guidedetail/key/read'
	    },
	reader: {
	     type: 'json',
	     root: 'list',
	     totalProperty: 'totalCount',
	     successProperty: 'success',
	     messageProperty: 'message'
	    },
	writer: {
	     type: 'json',
	        // encode: true,
	     writeAllFields: true, ////just send changed fields
	        // root: 'data',
	     allowSingle: false, //always wrap in an array
	     batch: false
	    },
	listeners: {
	     exception: function(proxy, response, operation){

	    	 Ext.MessageBox.show({
	                title: 'REMOTE EXCEPTION',
	                msg: operation.getError(),
	                icon: Ext.MessageBox.ERROR,
	                buttons: Ext.Msg.OK
	         });
	        }
	    }
	    
	}
	
	
});