Ext.define('tourism.store.ktbbank.KTBPaymentStore', {
	extend : 'Ext.data.Store',
	requires : [
		'tourism.model.ktbbank.KTBPaymentModel',
		'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json',
        'Ext.data.writer.Json'
	],
	model : 'tourism.model.ktbbank.KTBPaymentModel'

});