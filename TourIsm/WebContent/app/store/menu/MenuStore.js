Ext.define('tourism.store.menu.MenuStore' ,{
    extend: 'Ext.data.TreeStore',
    alias : 'widget.menestore',
    
    root: {
      text: 'สำนักทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์',
      expanded: true
    },
    autoLoad: true,
    proxy: {
      type: 'ajax',
      actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
      api: {
        read: 'licenses/menu/read'
      },
      listeners: {
          exception: function(proxy, response, operation){

              Ext.MessageBox.show({
                  title: 'REMOTE EXCEPTION',
                  msg: operation.getError(),
                  icon: Ext.MessageBox.ERROR,
                  buttons: Ext.Msg.OK
              });
          }
      }
  }

});    
