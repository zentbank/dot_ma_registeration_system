Ext.define('tourism.store.month.MonthStore',{
	extend : 'Ext.data.Store',
	model : 'tourism.model.month.MonthModel',
	data : [
	        {"monthId":"0", "monthName":""},
	        {"monthId":"1", "monthName":"มกราคม"},
	        {"monthId":"2", "monthName":"กุมภาพันธ์"},
	        {"monthId":"3", "monthName":"มีนาคม"},
	        {"monthId":"4", "monthName":"เมษายน"},
	        {"monthId":"5", "monthName":"พฤษภาคม"},
	        {"monthId":"6", "monthName":"มิถุนายน"},
	        {"monthId":"7", "monthName":"กรกฎาคม"},
	        {"monthId":"8", "monthName":"สิงหาคม"},
	        {"monthId":"9", "monthName":"กันยายน"},
	        {"monthId":"10", "monthName":"ตุลาคม"},
	        {"monthId":"11", "monthName":"พฤศจิกายน"},
	        {"monthId":"12", "monthName":"ธันวาคม"}
	    ]
});