Ext.define('tourism.store.printcard.PrintCardGuideStore', {
	extend : 'Ext.data.Store',
	requires : [
		'tourism.model.registration.RegistrationModel',
		'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json',
        'Ext.data.writer.Json'
	],
	

	model : 'tourism.model.registration.RegistrationModel'
	
});