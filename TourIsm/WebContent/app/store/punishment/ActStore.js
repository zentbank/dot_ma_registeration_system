Ext.define('tourism.store.punishment.ActStore', {
	extend : 'Ext.data.Store',
	requires : [
		'tourism.model.punishment.ActModel',
		'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json',
        'Ext.data.writer.Json'
	],
	// storeId : 'punishment-suspensionstore',
	model : 'tourism.model.punishment.ActModel',
	// pageSize : 20,
	// autoLoad: true,
 //    proxy: {
	//     type: 'ajax',
	//     actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
	//     api: {
	//         read: 'business/punishment/masact/read'
	//     },
	//     reader: {
	//         type: 'json',
	//         // metaProperty: '',
	//         root: 'list',
	//         // idProperty: 'emailId',
	//         totalProperty: 'totalCount',
	//         successProperty: 'success',
	//         messageProperty: 'message'
	//     },
	//     writer: {
	//         type: 'json',
	//         // encode: true,
	//         writeAllFields: true, ////just send changed fields
	//         // root: 'data',
	//         allowSingle: false, //always wrap in an array
	//         batch: false
	//     },
	//     listeners: {
	//         exception: function(proxy, response, operation){

	//             Ext.MessageBox.show({
	//                 title: 'REMOTE EXCEPTION',
	//                 msg: operation.getError(),
	//                 icon: Ext.MessageBox.ERROR,
	//                 buttons: Ext.Msg.OK
	//             });
	//         }
	//     }
	// }
});