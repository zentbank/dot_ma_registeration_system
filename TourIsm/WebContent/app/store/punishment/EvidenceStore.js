Ext.define('tourism.store.punishment.EvidenceStore', {
	extend : 'Ext.data.Store',
	requires : [
		'tourism.model.punishment.EvidenceModel',
		'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json',
        'Ext.data.writer.Json'
	],

	model : 'tourism.model.punishment.EvidenceModel',

});