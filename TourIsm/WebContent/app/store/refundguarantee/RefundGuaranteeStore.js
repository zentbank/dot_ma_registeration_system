Ext.define('tourism.store.refundguarantee.RefundGuaranteeStore', {
	extend : 'Ext.data.Store',
	requires : [
		'tourism.model.refundguarantee.RefundGuaranteeModel',
		'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json',
        'Ext.data.writer.Json'
	],

	model : 'tourism.model.refundguarantee.RefundGuaranteeModel'

});