Ext.define('tourism.store.registration.CommitteeProgressStore', {
	extend : 'Ext.data.Store',
	requires : [
		'tourism.model.registration.CommitteeProgressModel',
		'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json',
        'Ext.data.writer.Json'
	],
	model : 'tourism.model.registration.CommitteeProgressModel',
    proxy: {
	    type: 'ajax',
	    actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
	    api: {
	        read: 'business/registration/progress/statuscommittee'
	    },
	    reader: {
	        type: 'json',
	        // metaProperty: '',
	        root: 'list',
	        // idProperty: 'emailId',
	        totalProperty: 'totalCount',
	        successProperty: 'success',
	        messageProperty: 'message'
	    },
	    listeners: {
	        exception: function(proxy, response, operation){

	            Ext.MessageBox.show({
	                title: 'REMOTE EXCEPTION',
	                msg: operation.getError(),
	                icon: Ext.MessageBox.ERROR,
	                buttons: Ext.Msg.OK
	            });
	        }
	    }
	}
});