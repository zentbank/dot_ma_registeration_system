Ext.define('tourism.store.tourleader.registration.TourleaderRegistrationStore',{
	extend : 'Ext.data.Store',
	requires : [
		'tourism.model.tourleader.registration.TourleaderRegistrationModel',
		'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json',
        'Ext.data.writer.Json'
	],
	storeId : 'tourleader-registrationstore',
	groupField: 'registrationTypeName',
	model : 'tourism.model.business.registration.BusinessRegistrationModel',
	pageSize : 20,
    proxy: {
	    type: 'ajax',
	    actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
	    api: {
	    	// read: 'tourleader/registration/readtraderbetweenregistration'
	    	read: 'business/key/registration/read'
//	        read: 'tourleader/registration/read',
//	        update: 'tourleader/registration/update',
//	        create: 'tourleader/registration/create'
	    },
	    reader: {
	        type: 'json',
	        // metaProperty: '',
	        root: 'list',
	        // idProperty: 'emailId',
	        totalProperty: 'totalCount',
	        successProperty: 'success',
	        messageProperty: 'message'
	    },
	    writer: {
	        type: 'json',
	        // encode: true,
	        writeAllFields: true, ////just send changed fields
	        // root: 'data',
	        allowSingle: false, //always wrap in an array
	        batch: false
	    },
	    listeners: {
	        exception: function(proxy, response, operation){
	            Ext.MessageBox.show({
	                title: 'REMOTE EXCEPTION',
	                msg: operation.getError(),
	                icon: Ext.MessageBox.ERROR,
	                buttons: Ext.Msg.OK
	            });
	        }
	    }
	}
});