Ext.define('tourism.view.CenterView' ,{
    extend: 'Ext.panel.Panel',
    alias : 'widget.centerview',
    requires : [
        'tourism.view.business.registration.RegistrationPanel',
        'tourism.view.approvalprocess.ApprovalProcessPanel',
        'tourism.view.account.AccountPanel',
        'tourism.view.course.CoursePanel',
        'tourism.view.course.PersonCoursePanel',
        'tourism.view.document.registration.RegistrationDocumentPanel',
        'tourism.view.printlicense.PrintLicensePanel',
        'tourism.view.printcard.PrintCardPanel',
        'tourism.view.printaddress.PrintAddressPanel',
        'tourism.view.punishment.SuspendPanel',
        'tourism.view.deactivate.DeactivatePanel',
        'tourism.view.report.ReportFeeGuidePanel',
        'tourism.view.refundguarantee.RefundGuaranteePanel',
        'tourism.view.report.ReportFeeBusinessGuidePanel',
        'tourism.view.report.ReportFeeTotalPanel',
        'tourism.view.report.ReportFeeInExTotalPanel',
        'tourism.view.report.ReportGuaranteeAndFeePanel',
        'tourism.view.report.ReportGuaranteeAndFeeRegistrationTypePanel',
        'tourism.view.report.ReportBusinessTourismByPlacePanel',
        'tourism.view.admuser.AdmUserPanel',
        'tourism.view.account.guarantee.ChangeGuaranteePanel',
        'tourism.view.ktbbank.KTBPaymentPanel',
        'tourism.view.chart.LicenseDetailPanel',
        'tourism.view.license.info.InfoLicensePanel',
        'tourism.view.reqcertificate.ReqCertificatePanel',
        'tourism.view.reqpassport.ReqPassportPanel'
        ,'tourism.view.license.info.CheckDueDateFeePanel'
        ,'tourism.view.guidedetail.GuideDetailPanel'
        ,'tourism.view.joborder.JobOrderPanel'
        ,'tourism.view.joborder.JobOrderReportUploadFilePanel'
        ,'tourism.view.refundguarantee.account.AccountCanclePanel'
        ,'tourism.view.account.payment.PaymentOnlinePanel'
        ,'tourism.view.report.SuspendOrderPanel'
        ,'tourism.view.reqtouroperator.ReqTourOperatorPanel'
    ],
   layout: 'card',

    activeItem: 28,
//    activeItem: 88,

    items:[{
            //ธุรกิจนำเที่ยว0
            xtype: 'business-registrationPanel'
        },{
            // มัคคุเทศก์1
            //id: 'card-1',
            xtype: 'guide-registrationPanel'
        },{
            //ผู้นำเที่ยว2
            xtype: 'tourleader-registrationPanel'
        },{// นิติกร3
            xtype: 'approvalprocess-panel',
            id: 'lawyer-business-approvalprocess-panel',
            roleAction: 'lawyer',
            traderType: 'business'

        },{//4
            xtype: 'approvalprocess-panel',
            id: 'lawyer-guide-approvalprocess-panel',
            roleAction: 'lawyer',
            traderType: 'guide'

        },{//5
            xtype: 'approvalprocess-panel',
            id: 'lawyer-tourleader-approvalprocess-panel',
            roleAction: 'lawyer',
            traderType: 'tourleader'

        },{// recheck6
            xtype: 'approvalprocess-panel',
            id: 'recheck-business-approvalprocess-panel',
            roleAction: 'recheck',
            traderType: 'business'

        },{//7
            xtype: 'approvalprocess-panel',
            id: 'recheck-guide-approvalprocess-panel',
            roleAction: 'recheck',
            traderType: 'guide'

        },{//8
            xtype: 'approvalprocess-panel',
            id: 'recheck-tourleader-approvalprocess-panel',
            roleAction: 'recheck',
            traderType: 'tourleader'

        },{//หัวหน้างานทะเบียน9
            xtype: 'approvalprocess-panel',
            id: 'supervisor-business-approvalprocess-panel',
            roleAction: 'supervisor',
            traderType: 'business'

        },{
            xtype: 'approvalprocess-panel',
            id: 'supervisor-guide-approvalprocess-panel',
            roleAction: 'supervisor',
            traderType: 'guide'

        },{
            xtype: 'approvalprocess-panel',
            id: 'supervisor-tourleader-approvalprocess-panel',
            roleAction: 'supervisor',
            traderType: 'tourleader'

        },{//หัวหน้ากลุ่มทะเบียน12
            xtype: 'approvalprocess-panel',
            id: 'manager-business-approvalprocess-panel',
            roleAction: 'manager',
            traderType: 'business'

        },{
            xtype: 'approvalprocess-panel',
            id: 'manager-guide-approvalprocess-panel',
            roleAction: 'manager',
            traderType: 'guide'

        },{
            xtype: 'approvalprocess-panel',
            id: 'manager-tourleader-approvalprocess-panel',
            roleAction: 'manager',
            traderType: 'tourleader'

        },{// นายทะเบียน15
            xtype: 'approvalprocess-panel',
            id: 'director-business-approvalprocess-panel',
            roleAction: 'director',
            traderType: 'business'

        },{
            xtype: 'approvalprocess-panel',
            id: 'director-guide-approvalprocess-panel',
            roleAction: 'director',
            traderType: 'guide'

        },{
            xtype: 'approvalprocess-panel',
            id: 'director-tourleader-approvalprocess-panel',
            roleAction: 'director',
            traderType: 'tourleader'

        },{// การเงิน18
            xtype: 'account-panel',
            id: 'account-business-account-panel',
            roleAction: 'account',
            traderType: 'business'

        },{
            xtype: 'account-panel',
            id: 'account-guide-account-panel',
            roleAction: 'account',
            traderType: 'guide'

        },
        { //หลักสูตรฝึกอบรม
            xtype: 'course-panel'
        },
        {
            xtype: 'person-panel'
        }

        ,
        { //เอกสารประกอบ
            xtype: 'regis-document-panel',
             id: 'document-business-panel',
             roleAction: 'document',
             traderType: 'business'
        }
        ,{
            xtype: 'regis-document-panel',
            id: 'document-guide-panel',
            roleAction: 'document',
            traderType: 'guide'
        }
        ,{
            xtype: 'regis-document-panel',
            id: 'document-tourleader-panel',
            roleAction: 'document',
            traderType: 'tourleader'
        },
        {// พิมพ์ใบอนุญาต25
            xtype: 'printlicense-panel',
                id: 'printlicense-business-printlicense-panel',
                roleAction: 'printlicense',
                traderType: 'business'

        },
        {
            xtype: 'printlicense-panel',
                id: 'printlicense-guide-printlicense-panel',
                roleAction: 'printlicense',
                traderType: 'guide'
        },
        {
            xtype: 'printlicense-panel',
                id: 'printlicense-tourleader-printlicense-panel',
                roleAction: 'printlicense',
                traderType: 'tourleader'
        }
        //ทะเบียนใบอนุญาต
       //  ,
       //  {//28
       //  	xtype: 'info-panel',
                // id: 'info-key-business-panel',
                // roleAction: 'info',
                // roleGroup: 'key',
                // traderType: 'business'
       //  }
       //  ,
       //  {
       //  	xtype: 'info-panel',
                // id: 'info-permit-guide-panel',
                // roleAction: 'info',
                // roleGroup: 'permit',
                // traderType: 'guide'
       //  }
       //  ,
       //  {
       //  	xtype: 'info-panel',
                // id: 'info-permit-tourleader-panel',
                // roleAction: 'info',
                // roleGroup: 'permit',
                // traderType: 'tourleader'
       //  }
      //ทะเบียนใบอนุญาต
        ,{//28
          xtype: 'license-info-tour-guide-tourleader-panel',
          id: 'information-tourlicense-panel-601',
          roleAction: 'info',
          roleGroup: 'key',
          traderType: 'business',
          storeUrl: 'business/info/read'
        }
        ,
        {//29
          xtype: 'license-info-tour-guide-tourleader-panel',
          id: 'information-guide-panel-042',
          roleAction: 'info',
          roleGroup: 'permit',
          traderType: 'guide',
          storeUrl: 'business/info/read'
        }
        ,
        {//30
          xtype: 'license-info-tour-guide-tourleader-panel',
          id: 'information-tourleader-panel-083',
          roleAction: 'info',
          roleGroup: 'permit',
          traderType: 'tourleader',
          storeUrl: 'business/info/read'
        }
        //เรื่องร้องเรียน
        //31
        ,
        {
            xtype: 'complaint-panel',
                id: 'complaint-faq-panel',
                roleAction: 'complaint',
                roleGroup: 'faq'
                ,complaintProgress: 'RL'
        }
        ,
        {
            xtype: 'complaint-panel',
                id: 'complaint-receive-rl-panel',
                roleAction: 'complaint',
                roleGroup: 'receive',
                complaintProgress: 'RL'
        }
        ,
        {
            xtype: 'complaint-panel',
                id: 'complaint-receive-ml-panel',
                roleAction: 'complaint',
                roleGroup: 'receive',
                complaintProgress: 'ML'
        }
        ,
        {
            xtype: 'complaint-panel',
                id: 'complaint-consider-panel',
                roleAction: 'complaint',
                roleGroup: 'consider',
                complaintProgress: 'WL'
        },

      //35
        {
            xtype: 'printcard-panel',
                id: 'printcard-guide-panel',
                roleAction: 'printcard',
                //roleGroup: 'printcard',
                traderType: 'guide'
        },
      //36
        {
            xtype: 'printcard-panel',
                id: 'printcard-tourleader-panel',
                roleAction: 'printcard',
                //roleGroup: 'printcard',
                traderType: 'tourleader'
        },
        //37
        {
            xtype: 'printaddress-panel',
                id: 'printaddress-all-panel',
                roleAction: 'printaddress',
                //roleGroup: 'printcard',
                //traderType: 'tourleader'
        },
        //38 SUSPENDBUSINESS
        {
          xtype: 'punishment-suspension-panel',
          id: 'punishment-suspension-business-panel',
          roleAction: 'suspension',
          traderType: 'B',
          traderTypeName: 'business'
        },
        //39 SUSPENDGUIDE
        {
          xtype: 'punishment-suspension-panel',
          id: 'punishment-suspension-guide-panel',
          roleAction: 'suspension',
          traderType: 'G',
          traderTypeName: 'guide'
        },
        //40 SUSPENDTOURLEADER
        {
          xtype: 'punishment-suspension-panel',
          id: 'punishment-suspension-tourleader-panel',
          roleAction: 'suspension',
          traderType: 'L',
          traderTypeName: 'tourleader'
        },
        //41 REVOKEBUSINESS
        {
          xtype: 'punishment-suspension-panel',
          id: 'punishment-revoke-business-panel',
          roleAction: 'revoke',
          traderType: 'B',
          traderTypeName: 'business'
        },
        //42 REVOKEGUIDE
        {
          xtype: 'punishment-suspension-panel',
          id: 'punishment-revoke-guide-panel',
          roleAction: 'revoke',
          traderType: 'G',
          traderTypeName: 'guide'
        },
        //43 REVOKETOURLEADER
        {
          xtype: 'punishment-suspension-panel',
          id: 'punishment-revoke-tourleader-panel',
          roleAction: 'revoke',
          traderType: 'L',
          traderTypeName: 'tourleader'
        },
        //44 DEACTIVATEBUSINESS
        {
          xtype: 'deactivate-panel',
          id: 'deactivate-business-panel',
          roleAction: 'deactivate',
          traderType: 'B',
          traderTypeName: 'business'
        },
        //45 DEACTIVATEEGUIDE
        {
          xtype: 'deactivate-panel',
          id: 'deactivate-guide-panel',
          roleAction: 'deactivate',
          traderType: 'G',
          traderTypeName: 'guide'
        },
        //46 DEACTIVATEETOURLEADER
        {
          xtype: 'deactivate-panel',
          id: 'deactivate-tourleader-panel',
          roleAction: 'deactivate',
          traderType: 'L',
          traderTypeName: 'tourleader'
        },
        //47 REFUNGUARANTEE
        {

          xtype: 'refundguarantee-panel',
          id: 'refundguarantee-panel-panel'
        },
        //48 REPORTFEEGUIDE รายงานการรับค่าธรรมเนียมมัคคุเทศก์
        {
            xtype: 'reportfeeguide-panel',
            id: 'reportfee-guide-panel',
            roleAction: 'reportfeeguide',
            traderType: 'G',
            traderTypeName: 'guide'
        }

        //CHART
        //49
        ,{
            xtype: 'chart-pie-business-panel',
                id: 'chart-business-tradercategory-panel',
                roleAction: 'chart',
             roleGroup: 'tradercategory',
             traderType: 'business'
        }
        //50
        ,{
            xtype: 'chart-column-business-panel',
                id: 'chart-business-all-panel',
                roleAction: 'chart',
             roleGroup: 'all',
             traderType: 'business'
        }
        //51
        ,{
            xtype: 'chart-column-business-panel',
                id: 'chart-business-new-panel',
                roleAction: 'chart',
             roleGroup: 'new',
             traderType: 'business'
        }
        //52
        ,{
            xtype: 'chart-column-business-panel',
                id: 'chart-business-renew-panel',
                roleAction: 'chart',
             roleGroup: 'renew',
             traderType: 'business'
        }
       //53 REPORTFEEBUSINESSGUIDE รายงานทะเบียนคุมการจัดเก็บรายได้ค่าธรรมเนียม
        ,{
            xtype: 'reportfeebusinessguide-panel',
                id: 'report-business-guide-panel',
                roleAction: 'reportfeebusinessguide'//,
             //roleGroup: 'renew',
             //traderType: 'business'
        }
        //54
        ,{
            xtype: 'chart-column-business-panel',
                id: 'chart-business-guarantee-panel',
                roleAction: 'chart',
             traderType: 'business',
            roleGroup: 'guarantee'
        }
        //55
        ,{
            xtype: 'chart-column-business-panel',
                id: 'chart-business-suspensionandrevoke-panel',
                roleAction: 'chart',
             roleGroup: 'suspensionandrevoke',
             traderType: 'business'
        }
        //56
        ,{
            xtype: 'chart-column-guide-panel',
                id: 'chart-guide-type-panel',
                roleAction: 'chart',
                traderType: 'guide',
             roleGroup: 'type'

        }
        //57
        ,{
            xtype: 'chart-pie-guide-panel',
                id: 'chart-guide-all-panel',
                roleAction: 'chart',
                traderType: 'guide',
             roleGroup: 'all'

        }
        //58
        ,{
            xtype: 'chart-column-guide-panel',
                id: 'chart-guide-new-panel',
                roleAction: 'chart',
                traderType: 'guide',
             roleGroup: 'new'

        }
        //59
        ,{
            xtype: 'chart-column-guide-panel',
                id: 'chart-guide-renew-panel',
                roleAction: 'chart',
                traderType: 'guide',
             roleGroup: 'renew'

        }
       //60 RPTACCFEETOTAL รายละเอียดการส่งเงินค่าธรรมเนียมประจำวัน
        ,
        {
          xtype: 'reportfeetotal-panel',
        id: 'report-feetotal-panel',
        roleAction: 'reportfeetotal'
        }
        //61
        ,
        {
          xtype: 'reportfeeinextotal-panel',
        id: 'report-reportfeeinextotal-panel',
        roleAction: 'reportfeeinextotal'//,
        }
        //62 RPTGUARANTEETOTAL สรุปรายงานการรับหลักประกันและค่าธรรมเนียม
        ,
        {
          xtype: 'reportguaranteeandfee-panel',
        id: 'report-guarantee-fee-panel',
        roleAction: 'reportguaranteeandfee'//,
        }
        //63 RPTGUARANTEEA_FEE รายงานการรับหลักประกันและค่าธรรมเนียมชนิดต่างๆ
        ,
        {
          xtype: 'reportguaranteeandfeeregistrationtype-panel',
        id: 'report-guarantee-fee-registrationtype-panel',
        roleAction: 'reportguaranteeandfeeregistrationtype'//,
        }
        //64
        ,{
           xtype: 'admuser-panel',
           id:'admin-admuser-panel',
           roleAction: 'admin'

        }
        //65
        ,{
           html: 'DEPNEWS'

        }
        //66 MASCOMPTYPE
        ,{
           html: 'MASCOMPTYPE'

        }
        //67
        ,{
           html: 'MASACTTYPE'

        }
        //68
        ,{
           html: 'MASDEACTYPE'

        }
        //69
        ,{
           html: 'MASFEES'

        }
        //70 CHART
        ,{
//            html: 'CHART_GUIDE_ORG'
            xtype: 'chart-pie-guide-org-panel',
                  id: 'chart-guide-org-panel',
                  roleAction: 'chart',
                  traderType: 'guide',
                 roleGroup: 'org'
         }
        //71
        ,{
            html: 'CHART_TOURLEADER_NEW'

         }
        //72
         ,{
            xtype: 'account-guarantee-change-guarantee-panel',
            id: 'account-business-guarantee-change-guarantee-panel',
            roleAction: 'account'
        }
        //73
        ,{
             xtype: 'ktbbank-KTBPaymentPanel',
             id: 'ktbbank-KTBPaymentPanel'
        }
        //74
        ,{//ตารางแสดงรายละเอียดใบอนุญาตธุรกิจนำเที่ยวมัคคุเทศก์ และผู้นำเที่ยว
            xtype: 'report-license-detail-panel',
            id: 'report-license-detail-panel-009'
        }

        ,{//75
          xtype: 'license-info-tour-guide-tourleader-panel',
          id: 'reorganize-tourlicense-panel',
          roleAction: 'reorganize',
          roleGroup: 'reorganizetourlicense',
          traderType: 'business',
          storeUrl: 'business/info/reorganize/read'
        }
        ,
        {//76
          xtype: 'license-info-tour-guide-tourleader-panel',
          id: 'reorganize-guide-panel',
          roleAction: 'reorganize',
          roleGroup: 'reorganizeguidelicense',
          traderType: 'guide',
          storeUrl: 'business/info/reorganize/read'
        }
        ,
        {//77
          xtype: 'license-info-tour-guide-tourleader-panel',
          id: 'reorganize-tourleader-panel',
          roleAction: 'reorganize',
          roleGroup: 'reorganizetourleaderlicense',
          traderType: 'tourleader',
          storeUrl: 'business/info/reorganize/read'
        }, {//78 ขอหนังสือรับรองใบอนุญาตธุรกิจนำเที่ยว
          xtype: 'reqcertificate-panel',
          id: 'reqdoc-business-reqcertificate-panel',
          roleAction: 'reqdoc',
          traderType: 'business'
        },{
          html: 'รายงานการขอREQ_CERTIFICATE_RPT'
        }, {// 80 การขอหนังสือเดินทางเล่ม 2
          xtype: 'reqpassport-panel',
          id: 'subpassport-business-reqpassport-panel',
          roleAction: 'subpassport',
          traderType: 'tourleader'
        },{ // 81
          html: 'รายงานการขอหนังสือเดินทางเล่ม 2'
        }
        ,{//82
            xtype: 'license-info-duedate-fee-panel',
            id: 'information-duedate-fee-panel',
            roleAction: 'fee',
            roleGroup: 'key',
            traderType: 'business',
            storeUrl: 'business/info/readCheckDueDateFee'
          }
        ,{//83
            xtype: 'guidedetail-panel'
            ,id: 'guidedetail-guide-panel'
          }, //84
          {
            xtype: 'joborder-JobOrderPanel',
            id: 'joborder-JobOrderPanel-084'
          }
          , //85
          {
              xtype: 'account-cancle-panel',
              id: 'account-cancle-account-panel',
              roleAction: 'account',
              traderType: 'cancle'

          }, //86
          {
            xtype: 'account-payment-online-panel',
            id: 'account-payment-online-panel-086',
            roleAction: 'account',
          },
          //87
          {
            xtype: 'report-suspend-order-form',
            id: 'report-suspend-order-form-087',
            roleAction: 'suspendorder',
          }, {// 88 การขอหนังสือรับรองประกอบธุรกิจนำเที่ยว
              xtype: 'reqtouroperator-panel',
              id: 'reqtouroperator-business-reqtouroperator-panel',
              roleAction: 'reqtouroperator',
              traderType: 'officer'
          }
          ,{//sek add 89 ตารางแสดงสถิติจำนวนมัคคุเทศก์ จำแนกตามภาษา
            xtype: 'reportlanguageguide-panel',
              id: 'report-language-guide-panel',
              roleAction: 'reportlanguageguide'
          },{//sek add 90 ตารางแสดงสถิติจำนวนธุรกิจนำเที่ยว จำแนกตามจังหวัด
            xtype: 'reportprovincebusiness-panel',
              id: 'report-province-business-panel',
              roleAction: 'reportprovincebusiness'
          },{//sek add 91 ตารางแสดงสถิติจำนวนผู้ขอขึ้นทะเบียนเป็นผู้นำเที่ยว
            xtype: 'reportstatictourleader-panel',
              id: 'report-static-tourleader-panel',
              roleAction: 'reportstatictourleader'
          },{//sek add 92 ตารางแสดงสถิติจำนวนการออกใบอนุญาตประกอบธุรกิจนำเที่ยว กรณีมีการเปลี่ยนแปลและเพิ่มสาขา
            xtype: 'reportbusinessregistrationtype-panel',
              id: 'report-business-registrationtype-panel',
              roleAction: 'reportbusinessregistrationtype',
          }
          ,{//sek add 93 ตารางแสดงรายละเอียดธุรกิจนำเที่ยว จดใหม่
            xtype: 'reportnewbusiness-panel',
              id: 'report-new-business-panel',
              roleAction: 'reportnewbusiness'         
          }
          ,{//sek add 94 รายชื่อบริษัทที่ยกเลิกธุรกิจนำเที่ยว และขอรับคืนหลักประกัน
            xtype: 'reportmasdeactypebusiness-panel',
              id: 'report-masdeactype-business-panel',
              roleAction: 'reportmasdeactypebusiness'         
          }, //JobOrderReportUploadFile - 95
          {
              xtype: 'joborder-JobOrderReportUploadFilePanel',
              id: 'joborder-JobOrderReportUploadFilePanel'
          }, //JobOrderReportUploadFile - 96
          {
              xtype: 'reportBusinessTourismByPlace-panel',
              id: 'reportBusinessTourismByPlace-panel'
          }
        ]

});







