Ext.define('tourism.view.Main', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
        'Ext.layout.CardLayout',
        'tourism.view.admuser.AdmUserLoginForm'
    ],
    
    xtype: 'app-main',

    layout: {
        type: 'border'
    },
    style: 'background: #F0F0F0;',
    items: [
    {
        region: 'west',
        xtype: 'treemenu',
        title: 'เมนูการทำงาน',
        tools: [
                    {
                        type:'gear',
                     
                        handler: function(event, toolEl, cmp , tool) {
                           var menu = cmp.down('menu'); 
                            if (!menu) {
                                menu = Ext.create('Ext.menu.Menu', {
                                    items: [
                                    {
                                        text:'เข้าสู่ระบบ' ,
                                        iconCls: 'icon-expand24-xsmall' , 
                                        handler: function()
                                        { 
                                            var admUserLoginWin = Ext.create('Ext.window.Window',{
                                                    title:'เข้าสู่ระบบ',
                                                    autoShow: true,
                                                    modal: true,
                                                    items :[
                                                        {
                                                            xtype: 'admuser-admUserLoginForm-form',
                                                            id: 'admuser-admUserLoginForm-form-001',
                                                            frame: false,
                                                            border: false
                                                        }
                                                    ]
                                              });
                                              
                                              admUserLoginWin.show();
                                        }
                                    },
                                    {
                                        text:'ออกจากระบบ' , 
                                        iconCls: 'icon-collapse24-xsmall', 
                                        handler: function()
                                        {
                                           Ext.Ajax.request({
                                               url: 'admin/auth/user/logout',
                                               method: 'POST',
                                               success: function(response, opts) {
                                                  var obj = Ext.decode(response.responseText);
                                                  window.location.href="login.jsp";
                                                  
                                               },
                                               failure: function(response, opts) {
                                                  
                                               }
                                            });
                                        }
                                    }]
                                });
                            }
                            menu.showBy(tool);;
                        }
                        
                    
                    }
        ],
        padding: '0px 2px 5px 5px',
        collapsible: true,   // make collapsible
        width: 280,
        frame: false,
        border: false,
        split: true,
        splitterResize: true
    },{
        region: 'center',
        xtype: 'centerview',
        plain: true,
        padding: '0px 5px 5px 0px',
        frame: false,
        border: false

    }]
});