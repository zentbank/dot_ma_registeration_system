Ext.define('tourism.view.Viewport', {
    extend: 'Ext.container.Viewport',
    requires:[
        'Ext.layout.container.Fit',
        'tourism.view.Main'
    ],

    layout: {
        type: 'fit'
    },

    items: [{
        // margin:'0px 0px 0px 0px',
        xtype: 'app-main'
    }]
});
