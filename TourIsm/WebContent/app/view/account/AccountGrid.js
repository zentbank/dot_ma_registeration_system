Ext
		.define(
				'tourism.view.account.AccountGrid',
				{
					extend : 'Ext.grid.GridPanel',
					alias : 'widget.account-grid',
					stripeRows : true,
					requires : [ 'Ext.grid.Column',
							'tourism.store.registration.RegistrationStore',
							'Ext.toolbar.Paging', 'Ext.grid.plugin.RowEditing',
							'Ext.grid.column.Template',
							'Ext.grid.column.Action',
							'Ext.grid.plugin.RowExpander',
							'Ext.grid.feature.Grouping',
							'tourism.view.account.payment.PaymentWindow',
							'tourism.view.account.ReceiptAccountAddEditWindow' ],
					initComponent : function() {
						var registrationStore = Ext
								.create(
										'tourism.store.registration.RegistrationStore',
										{
											storeId : this.storeId,
											pageSize : this.storepageSize,
											groupField : 'registrationTypeName',
											roleAction : this.roleAction,
											traderType : this.traderType,
											proxy : {
												type : 'ajax',
												actionMethods : {
													create : 'POST',
													read : 'POST',
													update : 'POST',
													destroy : 'POST'
												},
												api : {
													read : this.storeUrl
												},
												reader : {
													type : 'json',
													root : 'list',
													totalProperty : 'totalCount',
													successProperty : 'success',
													messageProperty : 'message'
												},
												listeners : {
													exception : function(proxy,
															response, operation) {

														Ext.MessageBox
																.show({
																	title : 'REMOTE EXCEPTION',
																	msg : operation
																			.getError(),
																	icon : Ext.MessageBox.ERROR,
																	buttons : Ext.Msg.OK
																});
													}
												}
											},
											listeners : {
												scope : this,
												// single: true,
												beforeload : function(store,
														operation, eOpts) {
													var formId = '#'
															+ store.roleAction
															+ '-'
															+ store.traderType
															+ '-account-formsearch';

													var formSearch = Ext.ComponentQuery
															.query(formId)[0];
													var values = formSearch
															.getValues();

													for (field in values) {
														if (Ext
																.isEmpty(values[field])) {
															delete values[field];
														}

													}
													store.proxy.extraParams = values;
												}
											}
										});

						var columns = this.getBusinessColumn();
						if (this.traderType == 'guide') {
							columns = this.getGuideColumn();
						}

						Ext.apply(this, {
							store : registrationStore,
							dockedItems : [ {
								xtype : 'pagingtoolbar',
								store : registrationStore,
								dock : 'bottom',
								displayInfo : true

							} ],
							viewConfig: {
								stripeRows:true ,
								getRowClass: function(record, index) {                            
									if (record.get('roleColor') == 'true') {
									    return "online-block";
									}
								}
							},
							columns : columns
						});

						this.callParent(arguments);

					},

					features : [ {
						ftype : 'grouping',
						groupHeaderTpl : '{name} ({rows.length} รายการ)',
						hideGroupedHeader : true,
						startCollapsed : false
					// ,
					// id: 'restaurantGrouping'
					} ],
					getGuideColumn : function() {
						return [
								{
									header : 'regId',
									dataIndex : 'regId',
									hidden : true
								},
								{
									header : 'เลขที่ใบอนุญาต',
									dataIndex : 'licenseNo',
									flex : 1
								},
								{
									header : 'ชื่อผู้ขอใบอนุญาต',
									dataIndex : 'traderOwnerName',
									flex : 2
								},
								{
									header : 'ประเภทการจดทะเบียน',
									dataIndex : 'registrationTypeName',
									flex : 2
								},
								{
									header : 'เลขที่รับเรื่อง',
									dataIndex : 'registrationNo',
									flex : 1
								},
								 {
								 	header : 'วันที่รับเรื่อง',
								 	dataIndex : 'registrationDate',
								 	flex : 1
								 },
//								
							 {
									header : 'บันทึกใบเสร็จ',
									xtype : 'actioncolumn',
									width : 100,
									align : 'center',
									items : [ {

										getClass : function(v, meta, rec) {
											if (rec.get('roleAction') == '1') {
												this.items[0].tooltip = 'ยังไม่พิมพ์ใบเสร็จ';
												return 'icon-notes-xsmall';
											} else if (rec.get('roleAction') == 'P') {
												this.items[0].tooltip = 'พิมพ์ใบเสร็จแล้ว';
												return 'icon-checked-xsmall';
											} else {
												this.items[0].tooltip = 'ยังไม่ส่งเรื่อง';
												return '';
											}
										},
										handler : function(grid, rowIndex,
												colIndex, node, e, record,
												rowNode) {

											this.fireEvent('itemclick', this,
													'openfeewindow', grid,
													rowIndex, colIndex, record,
													node);

										}
									} ],
									renderer : function(value, meta, rec) {
										if (rec.get('roleColor') == '1') {
											meta.style = "background-color:#CCFFCC;";
										}
									}
								},
								{
									header : '',
									xtype : 'actioncolumn',
									width : 50,
									align : 'center',
									items : [ {

										getClass : function(v, meta, rec) {
											if (rec.get('progressBackStatus') != 'B') {
												this.items[0].tooltip = 'รับเรื่อง';
												return 'icon-comment-xsmall';
											} else {
												this.items[0].tooltip = 'เรื่องตีกลับ';
												return 'icon-comment-xsmall-red';
											}
										},
										handler : function(grid, rowIndex,
												colIndex, node, e, rec,
												rowNode) {

											this.fireEvent('itemclick', this,
													'approvalprogress', grid,
													rowIndex, colIndex, rec,
													node);
										}
									} ]
								},
								{
									header : '',
									xtype : 'actioncolumn',
									width : 50,
									align : 'center',
									items : [ {
										iconCls : 'icon-edit-xsmall',
										tooltip : 'รายละเอียด',
										handler : function(grid, rowIndex,
												colIndex, node, e, rec,
												rowNode) {

											this.fireEvent('itemclick', this,
													'approvaleditregistration',
													grid, rowIndex, colIndex,
													rec, node);

										}
									} ]
								} ];
					},
					getBusinessColumn : function() {
						return [
								{
									header : 'regId',
									dataIndex : 'regId',
									hidden : true
								},
								{
									header : 'เลขที่ใบอนุญาต',
									dataIndex : 'licenseNo',
									flex : 1
								},
								{
									header : 'ชื่อธุรกิจนำเที่ยว',
									dataIndex : 'traderName',
									flex : 2
								},
								{
									header : 'ประเภทการจดทะเบียน',
									dataIndex : 'registrationTypeName',
									flex : 2
								},
								{
									header : 'เลขที่รับเรื่อง',
									dataIndex : 'registrationNo',
									flex : 1
								},
								 {
								 	header : 'วันที่รับเรื่อง',
								 	dataIndex : 'registrationDate',
								 	flex : 1
								 },
//								{
//									header : 'สถานะ',
//									dataIndex : 'paymentStatusText',
//									flex : 2
//								},
//								{
//									header : 'วันที่ชำระเงิน',
//									dataIndex : 'paymentDate',
//									flex : 1
//								},
//								{
//									header : 'รับชำระเงิน',
//									xtype : 'actioncolumn',
//									width : 100,
//									align : 'center',
//									items : [ {
//
//										getClass : function(v, meta, rec) {
//											var ret = '';
//											if ((rec.get('feePaidStatus') == '1') || (rec.get('feePaidStatus') == '2')) {
//												this.items[0].tooltip = '';
//												ret = 'icon-feepayment-xsmall';
//											} else {
//												this.items[0].tooltip = 'ชำระครบถ้วน';
//												ret = '';
//											}
//											return ret;
//										},
//										handler : function(grid, rowIndex,
//												colIndex, node, e, rec,
//												rowNode) {
//											if ((rec.get('feePaidStatus') == '1') || (rec.get('feePaidStatus') == '2')) {
//												this.fireEvent('itemclick',
//														this,
//														'openPaymentwindow',
//														grid, rowIndex,
//														colIndex, rec, node);
//											}
//
//										}
//									} ],
//									renderer : function(value, meta, rec) {
//										if (rec.get('roleColor') == '1') {
//											meta.style = "background-color:#CCFFCC;";
//										}
//									}
//								},
								{
									header : 'บันทึกใบเสร็จ',
									xtype : 'actioncolumn',
									width : 100,
									align : 'center',
									items : [ {

										getClass : function(v, meta, rec) {
											if (rec.get('roleAction') == '1') {
												this.items[0].tooltip = 'ยังไม่พิมพ์ใบเสร็จ';
												return 'icon-notes-xsmall';
											} else if (rec.get('roleAction') == 'P') {
												this.items[0].tooltip = 'พิมพ์ใบเสร็จแล้ว';
												return 'icon-checked-xsmall';
											} else {
												this.items[0].tooltip = 'ยังไม่ส่งเรื่อง';
												return '';
											}
										},
										handler : function(grid, rowIndex,
												colIndex, node, e, record,
												rowNode) {

											this.fireEvent('itemclick', this,
													'openfeewindow', grid,
													rowIndex, colIndex, record,
													node);

										}
									} ],
									renderer : function(value, meta, rec) {
										if (rec.get('roleColor') == '1') {
											meta.style = "background-color:#CCFFCC;";
										}
									}
								},
//								{
//									header : 'พิมพ์ใบเสร็จ',
//									xtype : 'actioncolumn',
//									width : 100,
//									align : 'center',
//									items : [ {
//
//										getClass : function(v, meta, rec) {
//											var ret = '';
//											if (rec.get('feePaidStatus') == '0') {
//
//												if (rec.get('roleAction') == '1') {
//													this.items[0].tooltip = 'ยังไม่พิมพ์ใบเสร็จ';
//													ret = 'icon-notes-xsmall';
//												} else if (rec
//														.get('roleAction') == 'P') {
//													this.items[0].tooltip = 'พิมพ์ใบเสร็จแล้ว';
//													ret = 'icon-checked-xsmall';
//												} else {
//													this.items[0].tooltip = 'ยังไม่ส่งเรื่อง';
//													ret = '';
//												}
//											}
//
//											return ret;
//										},
//										handler : function(grid, rowIndex,
//												colIndex, node, e, rec,
//												rowNode) {
//											if (rec.get('feePaidStatus') == '0') {
//												this.fireEvent('itemclick',
//														this, 'openfeewindow',
//														grid, rowIndex,
//														colIndex, rec, node);
//											}
//
//										}
//									} ],
//									renderer : function(value, meta, rec) {
//										if (rec.get('roleColor') == '1') {
//											meta.style = "background-color:#CCFFCC;";
//										}
//									}
//								},
								{
									header : '',
									xtype : 'actioncolumn',
									width : 50,
									align : 'center',
									items : [ {

										getClass : function(v, meta, rec) {
											if (rec.get('progressBackStatus') != 'B') {
												this.items[0].tooltip = 'รับเรื่อง';
												return 'icon-comment-xsmall';
											} else {
												this.items[0].tooltip = 'เรื่องตีกลับ';
												return 'icon-comment-xsmall-red';
											}
										},
										handler : function(grid, rowIndex,
												colIndex, node, e, rec,
												rowNode) {
											this.fireEvent('itemclick', this,
													'approvalprogress', grid,
													rowIndex, colIndex, rec,
													node);
										}
									} ]
								},
								{
									header : '',
									xtype : 'actioncolumn',
									width : 50,
									align : 'center',
									items : [ {
										iconCls : 'icon-edit-xsmall',
										tooltip : 'รายละเอียด',
										handler : function(grid, rowIndex,
												colIndex, node, e, rec,
												rowNode) {

											this.fireEvent('itemclick', this,
													'approvaleditregistration',
													grid, rowIndex, colIndex,
													rec, node);

										}
									} ]
								} ];
					}
				// ,
				// afterRender: function()
				// {
				// this.callParent(arguments);

				// this.getStore().load();

				// }
				});