Ext.define('tourism.view.account.AccountPanel', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
        'tourism.view.account.AccountBusinessFormSearch',
        'tourism.view.account.AccountGrid',
        'tourism.view.account.AccountGuideFormSearch'
    ],
    
    xtype: 'account-panel',
    initComponent: function(){

    	var formSearch = this.getBusinessFormSearch(this.roleAction, this.traderType);

    	if(this.traderType == 'guide')
    	{
    		formSearch = this.getGuideFormSearch(this.roleAction ,this.traderType);
    	}
    	Ext.apply(this, {
    		layout: {
		        type: 'border'
		    },
		    items: [{
		        region: 'north',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items:[formSearch]
		    },{
		        region: 'center',
		        xtype: 'panel',
		        layout: 'fit',
		        frame: false,
		        border: false,
		        items:[{
		            // title: 'การทำรายการใบอนุญาตธุรกิจนำเที่ยว',
		            xtype: 'account-grid',
		            id: this.roleAction+'-'+this.traderType+'-account-grid',
		            // padding: '5px 5px 5px 5px',
		            autoScroll: true,
		            // frame: false 
		            border: false,
		            storeId: this.roleAction+'-'+this.traderType+'-account-store',
		            storepageSize: 20,
		            storeUrl: 'business/'+this.roleAction+'/registration/read',
		            roleAction: this.roleAction,
		            traderType: this.traderType
		            
		        }]
		    }]
    	});

    	this.callParent(arguments);
    },
    getBusinessFormSearch: function(roleAction, traderType)
    {
    	return {
    		
	            title:'ค้นหาการทำรายการใบอนุญาตธุรกิจนำเที่ยว',
	            collapsible: true,   // make collapsible
	            // collapsed : true,
	            xtype: 'account-business-formsearch',
	            id: roleAction+'-'+traderType+'-account-formsearch',
	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false
    	};
    },
    getGuideFormSearch: function(roleAction ,traderType)
    {
    	return {
    		
	            title:'ค้นหาการทำรายการใบอนุญาตมัคคุเทศก์',
	            collapsible: true,   // make collapsible
	            // collapsed : true,
	            xtype: 'account-guide-formsearch',
	            id: roleAction+'-'+traderType+'-account-formsearch',
	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false
    	};
    }
});