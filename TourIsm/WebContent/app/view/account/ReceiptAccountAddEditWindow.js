Ext.define('tourism.view.account.ReceiptAccountAddEditWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.receipt-account-addeditwindow',

    requires: [
        'tourism.view.account.ReceiptAccountAddEditForm'
    ],

    title : 'บันทึกข้อมูลหลักประกัน',
    layout: 'fit',
    width: 500,
    height: 500,
    //True to make the window modal and mask everything behind it when displayed
    modal: true,
    // iconCls: 'icon-form',

    initComponent: function() {

        // Ext.apply(this, {

        // });
        this.items = [
                {
                    xtype: 'receipt-account-addedit-form',
                    // id: 'receipt-account-addedit-form',
                    frame: false,
                    border: false
                }
        ];

        this.callParent(arguments);
    }

});
