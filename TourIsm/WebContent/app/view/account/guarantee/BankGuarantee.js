Ext.define('tourism.view.account.guarantee.BankGuarantee', {
    extend: 'Ext.form.FieldSet',
    requires:[
        'Ext.form.FieldContainer',
        'Ext.form.field.VTypes',
        'Ext.form.*'
    ],
    
    xtype: 'account-bankguarantee-fieldser',

     initComponent: function(){

        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

        var store = Ext.create('tourism.store.combo.MasBankStore',{
			  storeId: 'registration-account-bank-store'
		  }); 
        
         Ext.apply(this, {
            checkboxToggle:true,
            checkboxName : 'guaranteeTypeBankGuarantee',
            title: 'หนังสือค้ำประกันธนาคาร',
            defaultType: 'textfield',
            collapsed: true,
            layout: 'anchor',
            defaults: {
                anchor: '100%'
            },
            items :
            [
                {
                    xtype: 'textfield',
                    name: 'bankGuaranteeId',
                    fieldLabel: 'เลขที่หนังสือค้ำประกัน',
                    allowBlank: true,
                    // id: 'bank-accountId',
                    afterLabelTextTpl: required,
                    tabIndex : 6
                },
                {
                    xtype: 'textfield',
                    name: 'bankGuaranteeName',
                    fieldLabel: 'ชื่อบัญชี',
                    // id: 'bank-accountName',
                    allowBlank: true,
                    afterLabelTextTpl: required,
                    tabIndex : 7
                }
                ,{
                	
                	
            	    xtype: 'combo',
            	    fieldLabel: 'ธนาคาร',
            	    store: store,
            	    queryMode: 'remote',
            	    displayField: 'bankName',
            	    valueField: 'masBankId',
            	    hiddenName: 'masBankByBankGuaranteeBankId',
            	    triggerAction: 'all',
            	    allowBlank: true,
            	    // id:'bank-gaurantee',
            	    name: 'masBankByBankGuaranteeBankId',
                    afterLabelTextTpl: required,
                    tabIndex : 8
               
                },
                {
                    xtype: 'numberfield',
                    fieldLabel: 'จำนวนเงิน',
                    name: 'bankGuaranteeMny',
                    //maxLength: 13,
                    hideTrigger: true,
                    keyNavEnabled: false,
                    mouseWheelEnabled: false,
                    allowBlank: true,
                    tabIndex : 9,
                    afterLabelTextTpl: required,
                    value: 0,
                    renderer: function(value)
                    {
                        return Ext.util.Format.currency(value);
                    }

                }
            ]
         });

        this.callParent(arguments);
     }

});