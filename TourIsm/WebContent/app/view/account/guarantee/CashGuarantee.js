Ext.define('tourism.view.account.guarantee.CashGuarantee', {
    extend: 'Ext.form.FieldSet',
    requires:[
        'Ext.form.FieldContainer',
        'Ext.form.field.VTypes',
        'Ext.form.*',
        'tourism.store.combo.MasBankStore',
        'tourism.model.combo.MasBankModel'
        
    ],
    
    xtype: 'account-cashguarantee-fieldser',

     initComponent: function(){

        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

        var store = Ext.create('tourism.store.combo.MasBankStore',{
			  storeId: 'registration-account-cash-store'
		  }); 
        
         Ext.apply(this, {
            checkboxToggle:true,
            id: 'chkcash',
            checkboxName : 'guaranteeTypeCash',
            title: 'เงินสด, Cashier Check',
            defaultType: 'textfield',
            collapsed: true,
            layout: 'anchor',
            defaults: {
                anchor: '100%'
            },
            items :
            [
                {
                    xtype: 'textfield',
                    name: 'cashAccountId',
                    fieldLabel: 'เลขที่บัญชี',
                    allowBlank: true,
                    // id: 'cash-accountId',
                    afterLabelTextTpl: required,
                    tabIndex : 1 
                },
                {
                    xtype: 'textfield',
                    name: 'cashAccountName',
                    // id: 'cash-accountName',
                    fieldLabel: 'ชื่อ่บัญชี',
                    allowBlank: true,
                    afterLabelTextTpl: required,
                    tabIndex : 2
                }
                ,{
            	    xtype: 'combo',
            	    fieldLabel: 'ธนาคาร',
            	    store: store,
            	    queryMode: 'remote',
            	    displayField: 'bankName',
            	    valueField: 'masBankId',
            	    hiddenName: 'masBankByCashAccountBankId',
            	    triggerAction: 'all',
            	    allowBlank: true,
            	    // id:'cash-gaurantee',
            	    name: 'masBankByCashAccountBankId',
                    afterLabelTextTpl: required,
                    tabIndex : 3
                   
                }
                ,{
                	xtype: 'textfield',
                	name: 'cashAccountBankBranchName',
                    fieldLabel: 'สาขา',
                    allowBlank: true,
                    afterLabelTextTpl: required,
                    tabIndex : 4
                   
                },
                {
                    xtype: 'numberfield',
                    fieldLabel: 'จำนวนเงิน',
                    name: 'cashAccountMny',
                    //maxLength: 13,
                    hideTrigger: true,
                    keyNavEnabled: false,
                    mouseWheelEnabled: false,
                    allowBlank: true,
                    tabIndex : 5,
                    afterLabelTextTpl: required,
                    value: 0,
                    renderer: function(value)
                    {
                        return Ext.util.Format.currency(value);
                    }
                }
            ]
         });

        this.callParent(arguments);
     }

});