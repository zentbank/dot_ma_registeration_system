Ext.define('tourism.view.account.guarantee.ChangeGuaranteeForm',{
    extend: 'Ext.form.Panel'
    ,alias: 'widget.account-guarantee-changeguarantee-form'
    ,requires: [
        'Ext.data.*'
        ,'Ext.form.*'
        ,'tourism.view.account.guarantee.GuaramteeAccountForm'
    ]
    ,bodyPadding: 5
    ,autoScroll: true
    ,initComponent: function(){
        Ext.apply(this, {
            width: '100%',
            height: '100%',
            items: [
            {
                title: 'เปลี่ยนแปลงข้อมูลหลักประกัน',
                xtype: 'guarantee-account-form',
                frame: false,
                border: false
            }]
            ,buttons: [{
                text: 'บันทึกข้อมูล',
                action: 'saveChangeGuaranteeBtn'
            }]  
        });

        this.callParent(arguments);
    }
        
});