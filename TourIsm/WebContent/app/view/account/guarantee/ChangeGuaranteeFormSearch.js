Ext.define('tourism.view.account.guarantee.ChangeGuaranteeFormSearch' ,{
    // extend: 'Ext.form.FieldSet',
    extend: 'Ext.form.FormPanel',
    alias : 'widget.account-business-changeguarantee-formsearch',
    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.Time',
        'Ext.form.CheckboxGroup',
        'Ext.layout.container.HBox',
        'Ext.form.FieldSet'
    ],
    bodyPadding: 10,
    // width: '100%',
    items: [
        {
                xtype: 'container',
                // title: 'Payment',
                layout: 'anchor',
                defaults: {
                    anchor: '100%',
                    labelWidth: 200
                },
                items: [
                    {
                        xtype: 'hidden',
                        name: 'traderType',
                        value: 'B'
                    },
                   
                    
                    {
                        xtype: 'container',
                        layout: 'hbox',
                        layoutCongig: {
                             pack:'center',
                             align:'middle'
                        },
                        items: [
                            {
                                xtype: 'container',
                                // title: 'Payment',
                                flex: 1,
                                layout: 'anchor',
                                defaults: {
                                    anchor: '100%',
                                    labelWidth: 200
                                },
                                items: [
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'เลขที่ใบอนุญาต',
                                        name: 'licenseNo'
                                    },
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'ชื่อธุรกิจนำเที่ยว(TH)',
                                        // labelWidth: 150,
                                        name: 'traderName'
                                    },
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'ชื่อผู้ขอใบอนุญาต',
                                        // labelWidth: 150,
                                        name: 'firstName'
                                    }
                                ]
                            },{
                                xtype: 'splitter'
                            },
                            {
                                xtype: 'container',
                                flex: 1,
                                layout: 'anchor',
                                defaults: {
                                    anchor: '100%',
                                    labelWidth: 200
                                },
                                items: [
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'เลขที่นิติบุคคล/บัตรประชาชน',
                                        name: 'identityNo'
                                    },
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'ชื่อธุรกิจนำเที่ยว(EN)',
                                        // labelWidth: 150,
                                        name: 'traderNameEn'
                                    },
                                     {
                                        xtype: 'combo',
                                        fieldLabel: 'ประเภทธุรกิจนำเที่ยว',
                                        store: 'tourism.store.combo.BusinessCategoryStore',
                                        queryMode: 'local',
                                        displayField: 'traderCategoryName',
                                        valueField: 'traderCategory',
                                        hiddenName: 'traderCategory',
                                        name :'traderCategory'
                                    }
                                ]
                            }
                        ]
                    }
                    
                ]
        },
        {
            xtype: 'toolbar',
            border: false,
            padding: '6px 0 6px 0px',
            items: [{
                xtype: 'button',
                text : 'ค้นหา',
                action: 'searchLicenseChangeGuarantee'
            }]
        }

    ]
});    
