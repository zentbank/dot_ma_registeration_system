Ext.define('tourism.view.account.guarantee.ChangeGuaranteeGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.account-guarantee-change-guarantee-grid',
		stripeRows: true,
		requires: [
			'Ext.grid.Column',
			'tourism.store.registration.RegistrationStore',
			'Ext.toolbar.Paging',
			'Ext.grid.plugin.RowEditing',
			'Ext.grid.column.Template',
			'Ext.grid.column.Action',
			'Ext.grid.plugin.RowExpander',
			'Ext.grid.feature.Grouping',
			'tourism.view.account.guarantee.ChangeGuaranteeWindow'

		],
		initComponent: function(){
			var registrationStore = Ext.create('tourism.store.refundguarantee.RefundGuaranteeStore',{
				storeId: this.storeId,
				pageSize : this.storepageSize,
				// groupField: 'registrationTypeName',
				
				proxy: {
					type: 'ajax',
					actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
					api: {
					    read: this.storeUrl
					},
					reader: {
					    type: 'json',
					    root: 'list',
					    totalProperty: 'totalCount',
					    successProperty: 'success',
					    messageProperty: 'message'
					},
					listeners: {
					    exception: function(proxy, response, operation){

					        Ext.MessageBox.show({
					            title: 'REMOTE EXCEPTION',
					            msg: operation.getError(),
					            icon: Ext.MessageBox.ERROR,
					            buttons: Ext.Msg.OK
					        });
					    }
					}
				},
				listeners: {
					scope: this,
					// single: true,
					beforeload: function( store, operation, eOpts )
				        {				        	
				        	var formId = '#account-business-change-guarantee-formsearch';
				        
				        	var formSearch = Ext.ComponentQuery.query(formId)[0];
				        	var values = formSearch.getValues();
								
				        	 for (field in values) 
						      {
						        if (Ext.isEmpty(values[field])) 
						        {
						          delete values[field];
						        }
						        
						      }
						    store.proxy.extraParams = values;
				        }
				}
			});


			Ext.apply(this, {
				store: registrationStore,
				dockedItems:[
					{
						xtype: 'pagingtoolbar',
						store: registrationStore,
						dock: 'bottom',
						displayInfo: true

					}
				],
				columns: [
										{
						header: 'guaranteeId',
						dataIndex: 'guaranteeId',
						hidden: true
					},
					{
						header: 'เลขที่ใบอนุญาต',
						dataIndex: 'licenseNo',
						flex: 1
					},					
					{
						header: 'ชื่อธุรกิจนำเที่ยว',
						dataIndex: 'traderName',
						flex: 2
					},
					{
						header: 'ประเภทธุรกิจนำเที่ยว',
						dataIndex: 'traderCategoryName',
						flex: 1
					},
					{
						header: 'สถานะการคืนหลักประกัน',
						dataIndex: 'guaranteeStatusName',
						flex: 1
					},
					 {
			        	header: 'เปลี่ยนแปลงหลักประกัน',
			            xtype: 'actioncolumn',
			           	flex: 1,
			           	align : 'center',
			            items: [{
			                iconCls: 'icon-edit-xsmall',
			                tooltip: 'เปลี่ยนแปลงหลักประกัน',
				            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
			                   this.fireEvent('itemclick', this, 'changeguarantee', grid, rowIndex, colIndex, record, node);
				            		
							}
			            }]
			        }
				]
			});

			this.callParent(arguments);


		}
	});