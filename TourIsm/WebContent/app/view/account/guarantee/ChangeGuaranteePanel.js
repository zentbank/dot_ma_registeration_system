Ext.define('tourism.view.account.guarantee.ChangeGuaranteePanel', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
        'tourism.view.account.guarantee.ChangeGuaranteeFormSearch',
        'tourism.view.account.guarantee.ChangeGuaranteeGrid'
    ],
    
    xtype: 'account-guarantee-change-guarantee-panel',
    initComponent: function(){

    
    	Ext.apply(this, {
    		layout: {
		        type: 'border'
		    },
		    items: [{
		        region: 'north',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items:[{
    		
		            title:'ค้นหาใบอนุญาตธุรกิจนำเที่ยว',
		            collapsible: true,   // make collapsible
		            // collapsed : true,
		            xtype: 'account-business-changeguarantee-formsearch',
		            id: 'account-business-change-guarantee-formsearch',
		            // padding: '5px 5px 0px 5px',
		            frame: false,
		            border: false
		    	}]
		    },{
		        region: 'center',
		        xtype: 'panel',
		        layout: 'fit',
		        frame: false,
		        border: false,
		        items:[{
		            // title: 'การทำรายการใบอนุญาตธุรกิจนำเที่ยว',
		            xtype: 'account-guarantee-change-guarantee-grid',
		            id: 'account-business-change-guarantee-grid',
		            // padding: '5px 5px 5px 5px',
		            autoScroll: true,
		            // frame: false 
		            border: false,
		            storeId: 'account-business-change-guarantee-grid-store',
		            storepageSize: 20,
		            storeUrl: 'business/refundguarantee/change/read'
		            
		        }]
		    }]
    	});

    	this.callParent(arguments);
    }
});