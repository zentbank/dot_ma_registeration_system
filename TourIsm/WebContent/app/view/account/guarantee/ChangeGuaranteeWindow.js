Ext.define('tourism.view.account.guarantee.ChangeGuaranteeWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.account-guarantee-changeguarantee-window',

    requires: [
        'tourism.view.account.guarantee.ChangeGuaranteeForm'
    ],

    title : 'บันทึกข้อมูลหลักประกัน',
    layout: 'fit',
    width: 500,
    height: 500,
    //True to make the window modal and mask everything behind it when displayed
    modal: true,
    // iconCls: 'icon-form',

    initComponent: function() {

        // Ext.apply(this, {

        // });
        this.items = [
                {
                    xtype: 'account-guarantee-changeguarantee-form',
                    id: 'account-business-guarantee-changeguarantee-form',
                    frame: false,
                    border: false
                }
        ];

        this.callParent(arguments);
    },

});
