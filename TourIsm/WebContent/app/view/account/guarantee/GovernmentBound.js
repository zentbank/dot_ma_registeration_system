Ext.define('tourism.view.account.guarantee.GovernmentBound', {
    extend: 'Ext.form.FieldSet',
    requires:[
        'Ext.form.FieldContainer',
        'Ext.form.field.VTypes',
        'Ext.form.*'
    ],
    
    xtype: 'account-governmentbound-fieldser',

     initComponent: function(){

        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
 
         Ext.apply(this, {
            checkboxToggle:true,
            checkboxName : 'guaranteeTypeGovernmentBound',
            title: 'พันธบัตรรัฐบาล',
            defaultType: 'textfield',
            collapsed: true,
            layout: 'anchor',
            defaults: {
                anchor: '100%'
            },
            items :
            [
                {
                    xtype: 'textfield',
                    name: 'governmentBondId',
                    fieldLabel: 'เลขที่พันธบัตร',
                    // id: 'governmentbound-accountId',
                    allowBlank: true,
                    afterLabelTextTpl: required,
                    tabIndex : 10
                },
                {
                    xtype: 'datefield',
                    fieldLabel: 'วันหมดอายุ',
                    name: 'governmentBondExpireDate',
                    // id: 'governmentbound-expire-date',
                    flex: 1,
                    format: 'd/m/B',
                    allowBlank: true,
                    tabIndex : 11
                },
                {
                    xtype: 'numberfield',
                    fieldLabel: 'จำนวนเงิน',
                    name: 'governmentBondMny',
                    //maxLength: 13,
                    hideTrigger: true,
                    keyNavEnabled: false,
                    mouseWheelEnabled: false,
                    allowBlank: true,
                    tabIndex : 12,
                    afterLabelTextTpl: required,
                    value: 0,
                    renderer: function(value)
                    {
                        return Ext.util.Format.currency(value);
                    }
                }
                
            ]
         });

        this.callParent(arguments);
     }

});