Ext.define('tourism.view.account.guarantee.GuaramteeAccountForm' ,{
    extend: 'Ext.container.Container',
   // alias : 'widget.account-guarantee-formaddedit',
    xtype: 'guarantee-account-form',
    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.Time',
        'Ext.form.CheckboxGroup',
        'Ext.form.FieldSet',
        'Ext.util.Positionable',
        'Ext.form.FieldContainer',
        'Ext.form.field.VTypes',
        'tourism.view.account.guarantee.CashGuarantee',
        'tourism.view.account.guarantee.BankGuarantee',
        'tourism.view.account.guarantee.GovernmentBound'
    ],
    initComponent: function(){
    	var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

    	var officeStore = Ext.create('tourism.store.combo.OfficerStore',{
            id:'GuaramteeAccountForm-officeStore'
            ,listeners: {
                beforeload: function( store, operation, eOpts )
                    {
         
                        store.proxy.extraParams = {groupRole: 'OFFACC'};
                    }
            }
        });
    	
    Ext.apply(this,{	
    bodyPadding: 10,
    items: [
                {
                	xtype: 'container',
                	// title: 'Payment',
                	layout: 'anchor',
                	defaults: {
                		anchor: '100%',
                		labelWidth: 150
                },
                items: [
                       
                        {
                        	xtype: 'hiddenfield',
                        	name: 'guaranteeId'
                        },
                        {	
                        	xtype: 'displayfield',
                        	fieldLabel: 'ประเภทใบอนุญาต',
                        	name: 'guaranteeCategoryName',
                        	anchor: '100%'
                        },
                        {
                        	xtype: 'account-cashguarantee-fieldser'
                        },
                        {
                        	xtype: 'account-bankguarantee-fieldser'
                        },
                        {
                        	xtype: 'account-governmentbound-fieldser'
                        },
                        {
                            xtype: 'numberfield',
                            fieldLabel: 'รวมเงินหลักประกัน',
                            name: 'guaranteeMny',
                            //maxLength: 13,
                            hideTrigger: true,
                            keyNavEnabled: false,
                            mouseWheelEnabled: false,
                            allowBlank: false,
                            renderer: function(value)
                            {
                                return Ext.util.Format.currency(value);
                            }
                        },  
                        {
                        	xtype: 'datefield',
                        	fieldLabel: 'วันที่รับเงิน',
                        	name: 'receiveDate',
                        	format: 'd/m/B',
                        	allowBlank: false,
                        	afterLabelTextTpl: required
                        }
                        ,{
                            xtype: 'combo',
                            fieldLabel: 'ผู้รับเงิน',
                            name: 'receiveName',
                            store: officeStore,
                            queryMode: 'remote',
                            displayField: 'officerName',
                            valueField: 'officerId',
                            hiddenName: 'officerId',
                            triggerAction: 'all',
                            flex: 1,
                            allowBlank: false,
                            afterLabelTextTpl: required
                        }
                        ]
                }
                
            ]   
    });
    this.callParent(arguments);
    }
    ,afterRender: function()
    {
        
        this.callParent(arguments);

        var form = this.up('form');

        var guaranteeMnyNumberField = form.getForm().findField('guaranteeMny');

        var cashAccountMnyNumberField = form.getForm().findField('cashAccountMny');
        cashAccountMnyNumberField.addListener('blur', function(field, eOpts ,opt){
            var total = 0;
            total = field.getValue() + bankGuaranteeMnyNumberField.getValue() + governmentBondMnyNumberField.getValue();
            guaranteeMnyNumberField.setValue(total);
        });

        var bankGuaranteeMnyNumberField = form.getForm().findField('bankGuaranteeMny');
        bankGuaranteeMnyNumberField.addListener('blur', function(field, eOpts ,opt){
            var total = 0;
            total = field.getValue() + cashAccountMnyNumberField.getValue() + governmentBondMnyNumberField.getValue();
            guaranteeMnyNumberField.setValue(total);
        });
        var governmentBondMnyNumberField = form.getForm().findField('governmentBondMny');
         governmentBondMnyNumberField.addListener('blur', function(field, eOpts ,opt){
            var total = 0;
            total = field.getValue() + cashAccountMnyNumberField.getValue() + bankGuaranteeMnyNumberField.getValue();
            guaranteeMnyNumberField.setValue(total);
        });


        
        var cashguaranteeFieldSet = this.down('account-cashguarantee-fieldser');
                
        cashguaranteeFieldSet.addListener('expand', function( f, eOpts ){
            
            var cashAccountId = form.getForm().findField('cashAccountId');
            cashAccountId.allowBlank = false;
            
            var cashAccountName = form.getForm().findField('cashAccountName');
            cashAccountName.allowBlank = false;

            var masBankByCashAccountBankId = form.getForm().findField('masBankByCashAccountBankId');
            masBankByCashAccountBankId.allowBlank = false;
            
            var cashAccountBankBranchName = form.getForm().findField('cashAccountBankBranchName');
            cashAccountBankBranchName.allowBlank = false;
            
            var cashAccountMny = form.getForm().findField('cashAccountMny');
            cashAccountMny.allowBlank = false;
            
            
        },this);

        cashguaranteeFieldSet.addListener('collapse', function( f, eOpts ){

            var cashAccountId = form.getForm().findField('cashAccountId');
            cashAccountId.allowBlank = true;
            cashAccountId.setValue('');
            
            var cashAccountName = form.getForm().findField('cashAccountName');
            cashAccountName.allowBlank = true;
            cashAccountName.setValue('');

            var masBankByCashAccountBankId = form.getForm().findField('masBankByCashAccountBankId');
            masBankByCashAccountBankId.allowBlank = true;
            masBankByCashAccountBankId.setValue('');
            
            var cashAccountBankBranchName = form.getForm().findField('cashAccountBankBranchName');
            cashAccountBankBranchName.allowBlank = true;
            cashAccountBankBranchName.setValue('');
            
            var cashAccountMny = form.getForm().findField('cashAccountMny');
            cashAccountMny.allowBlank = true;
            cashAccountMny.setValue(0);

            var total = 0;
            total = governmentBondMnyNumberField.getValue() + cashAccountMnyNumberField.getValue() + bankGuaranteeMnyNumberField.getValue();
            guaranteeMnyNumberField.setValue(total);

        },this);

        var bankguaranteeFieldSet = this.down('account-bankguarantee-fieldser');
        
        bankguaranteeFieldSet.addListener('expand', function( f, eOpts ){
                                   
            var bankGuaranteeId = form.getForm().findField('bankGuaranteeId');
            bankGuaranteeId.allowBlank = false;
           
            
            var bankGuaranteeName = form.getForm().findField('bankGuaranteeName');
            bankGuaranteeName.allowBlank = false;
            

            var masBankByBankGuaranteeBankId = form.getForm().findField('masBankByBankGuaranteeBankId');
            masBankByBankGuaranteeBankId.allowBlank = false;
        
            
            var bankGuaranteeMny = form.getForm().findField('bankGuaranteeMny');
            bankGuaranteeMny.allowBlank = false;
          

        },this);

        bankguaranteeFieldSet.addListener('collapse', function( f, eOpts ){
                                   
            var bankGuaranteeId = form.getForm().findField('bankGuaranteeId');
            bankGuaranteeId.allowBlank = true;
            bankGuaranteeId.setValue('');
            
            var bankGuaranteeName = form.getForm().findField('bankGuaranteeName');
            bankGuaranteeName.allowBlank = true;
            bankGuaranteeName.setValue('');

            var masBankByBankGuaranteeBankId = form.getForm().findField('masBankByBankGuaranteeBankId');
            masBankByBankGuaranteeBankId.allowBlank = true;
            masBankByBankGuaranteeBankId.setValue('');
            
            
            var bankGuaranteeMny = form.getForm().findField('bankGuaranteeMny');
            bankGuaranteeMny.allowBlank = true;
            bankGuaranteeMny.setValue(0);

            var total = 0;
            total = governmentBondMnyNumberField.getValue() + cashAccountMnyNumberField.getValue() + bankGuaranteeMnyNumberField.getValue();
            guaranteeMnyNumberField.setValue(total);


        },this);
        
        var governmentboundFieldSet = this.down('account-governmentbound-fieldser');
        
        governmentboundFieldSet.addListener('expand', function( f, eOpts ){
                                    
              var governmentBondId = form.getForm().findField('governmentBondId');
            governmentBondId.allowBlank = false;
           
            
            var governmentBondExpireDate = form.getForm().findField('governmentBondExpireDate');
            governmentBondExpireDate.allowBlank = false;
            

            var governmentBondMny = form.getForm().findField('governmentBondMny');
            governmentBondMny.allowBlank = false;
        

        },this);

        governmentboundFieldSet.addListener('collapse', function( f, eOpts ){
                                    
            var governmentBondId = form.getForm().findField('governmentBondId');
            governmentBondId.allowBlank = true;
            governmentBondId.setValue('');
            
            var governmentBondExpireDate = form.getForm().findField('governmentBondExpireDate');
            governmentBondExpireDate.allowBlank = true;
            governmentBondExpireDate.setValue('');           
            
            var governmentBondMny = form.getForm().findField('governmentBondMny');
            governmentBondMny.allowBlank = true;
     

            var total = guaranteeMnyNumberField.getValue();
            total = total - governmentBondMny.getValue();
            governmentBondMny.setValue(0);

            var total = 0;
            total = governmentBondMnyNumberField.getValue() + cashAccountMnyNumberField.getValue() + bankGuaranteeMnyNumberField.getValue();
            guaranteeMnyNumberField.setValue(total);
        

        },this);


    }

});  


