Ext.define('tourism.view.account.payment.PaymentForm',{
    extend: 'Ext.form.Panel'
    ,alias: 'widget.receipt-account-payment-form'
    ,requires: [
        'Ext.data.*'
        ,'Ext.form.*'
        
    ]
    ,bodyPadding: 5
    ,autoScroll: true
    ,initComponent: function(){
    	var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
    	
    	var paymentLabel = 'ชำระเพิ่ม';
    	if(this.feePaidStatus == '1'){
    		paymentLabel = 'คืนเงิน';
    	}

    	
	    Ext.apply(this,{	
		    bodyPadding: 10,
		    items: [{
	        	xtype: 'container',
	        	// title: 'Payment',
	        	layout: 'anchor',
	        	defaults: {
	        		anchor: '100%',
	        		labelWidth: 150
	        	},
	            items: [{
                	xtype: 'hiddenfield',
                	name: 'paymentRecId'
                },{	
	            	xtype: 'displayfield',
	            	fieldLabel: 'ชือ',
	            	name: 'traderName',
	            	anchor: '100%'
	            },{	
	            	xtype: 'displayfield',
	            	fieldLabel: 'วันที่ชำระเงิน',
	            	name: 'paymentDate',
	            	anchor: '100%'
	            },{	
	            	xtype: 'displayfield',
	            	fieldLabel: 'จำนวนเงินที่พึงชำระ',
	            	name: 'totalFee',
	            	anchor: '100%'
	            },
	            {	
	            	xtype: 'displayfield',
	            	fieldLabel: 'จำนวนเงินที่ชำระ',
	            	name: 'totalFeePaid',
	            	anchor: '100%'
	            },
	            {
	                xtype: 'numberfield',
	                fieldLabel: paymentLabel,
	                name: 'diffFee',
	                //maxLength: 13,
	                hideTrigger: true,
	                keyNavEnabled: false,
	                mouseWheelEnabled: false,
	                allowBlank: false,
	                renderer: function(value)
	                {
	                    return Ext.util.Format.currency(value);
	                },
	                listeners: {
	                	blur: function(comp, evn, obj){
	                		var form = comp.up('form');
	                		var totalFee = new Number(form.getForm().findField('totalFee').getValue());
	                   	  	var totalFeePaid = new Number(form.getForm().findField('totalFeePaid').getValue());
	                   	  	var feeMny = 0;
	                   	  
	                   	 var value = new Number(comp.getValue());
//	                   	  	if(Ext.isEmpty(comp.getValue())){
//	                   	  		value = 0;
//	                   	  	}
	                   	 var value = comp.getValue();
	                   	
	                   	  	totalFeePaid = totalFeePaid.valueOf() + value.valueOf();
	                   	  	feeMny = totalFee - totalFeePaid;
	                   	  	form.getForm().findField('feeMny').setValue(feeMny);
	                	}
	                }
	            },  
	            {	
	            	xtype: 'displayfield',
	            	fieldLabel: 'จำนวนเงินคงค้าง',
	            	name: 'feeMny',
	            	anchor: '100%'
	            }]
	        }]  
		    ,buttons: [{
	            text: 'บันทึกข้อมูลและพิมพ์ใบเสร็จ',
	            action: 'savePayment'
	        }] 
	    });

        this.callParent(arguments);
    }
        
});



