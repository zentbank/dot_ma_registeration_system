Ext.define('tourism.view.account.payment.PaymentOnlineAddEditWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.receipt-account-payment-online-add-edit-window',

    requires: [
        'tourism.view.account.payment.PaymentOnlineAddEditForm'
    ],

    title : 'พิมพ์ใบเสร็จรับเงิน',
    layout: 'fit',
    width: 500,
    height: 500,
    //True to make the window modal and mask everything behind it when displayed
    modal: true,
    // iconCls: 'icon-form',

    initComponent: function() {

        // Ext.apply(this, {

        // });
        this.items = [
                {
                    xtype: 'receipt-account-payment-online-addedit-form',
                    id: 'receipt-account-payment-online-addedit-form-01',
                    frame: false,
                    border: false
                }
        ];

        this.callParent(arguments);
    }

});
