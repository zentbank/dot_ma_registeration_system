Ext.define('tourism.view.account.payment.PaymentOnlineFormSearch' ,{
    extend: 'Ext.form.FormPanel',
    alias : 'widget.account-payment-online-form-search',
    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.Time',
        'Ext.form.CheckboxGroup',
        'Ext.layout.container.HBox',
        'Ext.form.FieldSet',
        'Ext.util.Positionable'
    ],
    bodyPadding: 10,
    
    items: [{
        xtype: 'container',
        layout: 'anchor',
        items: [{
            xtype: 'container',
            layout: 'hbox',
            layoutCongig: {
                 pack:'center',
                 align:'middle'
            },
            items: [
                {
                    xtype: 'container',
                    // title: 'Payment',
                    flex: 1,
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%',
                        labelWidth: 150
                    },
                    items: [{
                       xtype: 'datefield',
                        fieldLabel: 'วันที่ชำระเงิน',
                        name: 'receiveOfficerDateFrom',
                        allowBlank: false,
                        format: 'd/m/B'
                    }]
                },{
                    xtype: 'splitter'
                },
                {
                    xtype: 'container',
                    flex: 1,
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%',
                        labelWidth: 150
                    },
                    items: [{
                        xtype: 'datefield',
                        fieldLabel: 'ถึงวันที่',
                        name: 'receiveOfficerDateTo',
                        allowBlank: false,
                        format: 'd/m/B'
                    } ]
                }
            ]
        }]      
    },
    {
        xtype: 'toolbar',
        border: false,
        padding: '6px 0 6px 0px',
        items: [
            {
                xtype: 'button',
                text : 'ค้นหา',
                action: 'searchOnlinePayment'
            }
        ]
    }]

	
});    
