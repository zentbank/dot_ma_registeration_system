Ext.define('tourism.view.account.payment.PaymentOnlineGrid', {
    extend: 'Ext.grid.GridPanel',
    alias: 'widget.account-payment-online-grid',
    stripeRows: true,
    requires: [
        'Ext.grid.Column',
        'tourism.model.account.ReceiptModel',
        'Ext.toolbar.Paging', 
        'Ext.grid.plugin.RowEditing',
        'Ext.grid.column.Template',
        'Ext.grid.column.Action',
        'Ext.grid.plugin.RowExpander',
        'Ext.grid.feature.Grouping',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json',
        'Ext.data.writer.Json',
        'tourism.view.account.payment.PaymentOnlineAddEditWindow'
    ],
    initComponent: function () {

       

        var store = Ext.create('Ext.data.Store', {
            storeId: 'account-payment-online-grid-store-001',
            groupField: 'traderTypeName',
            model : 'tourism.model.account.ReceiptModel',
            proxy: {
                type: 'ajax',
                actionMethods: {
                    create: 'POST',
                    read: 'POST',
                    update: 'POST',
                    destroy: 'POST'
                },
                api: {
                    read: 'business/account/registration/readPaymentOnlineForPrintReceipt'
                },
                reader: {
                    type: 'json',
                    root: 'list',
                    totalProperty: 'totalCount',
                    successProperty: 'success',
                    messageProperty: 'message'
                },
                listeners: {
                    exception: function (proxy,
                        response, operation) {

                        Ext.MessageBox
                            .show({
                                title: 'REMOTE EXCEPTION',
                                msg: operation
                                    .getError(),
                                icon: Ext.MessageBox.ERROR,
                                buttons: Ext.Msg.OK
                            });
                    }
                }
            },
            listeners: {
                scope: this,
                // single: true,
                beforeload: function (store,
                    operation, eOpts) {
                    var formId = '#account-payment-online-form-search-001';

                    var formSearch = Ext.ComponentQuery.query(formId)[0];
                    var values = formSearch.getValues();

                    for (field in values) {
                        if (Ext.isEmpty(values[field])) {
                            delete values[field];
                        }

                    }
                    store.proxy.extraParams = values;
                }
            }
        });



        Ext.apply(this, {
            store: store,
            features: [{
                ftype: 'grouping',
                groupHeaderTpl: '{name} ({rows.length} รายการ)',
                hideGroupedHeader: true,
                startCollapsed: false
                    // ,
                    // id: 'restaurantGrouping'
             }],
            columns: [{
                header: 'hiddenfield',
                dataIndex: 'receiptId',
                hidden: true
                    
            }, {
                header: 'เล่มที่',
                dataIndex: 'bookNo',
                flex: 1
            }, {
                header: 'เลขที่ใบเสร็จรับเงิน',
                dataIndex: 'receiptNo',
                flex: 1
            },{
                header: 'ประเภทใบอนุญาต',
                dataIndex: 'traderTypeName',
                flex: 1
            },{
                header: 'ชื่อ',
                dataIndex: 'receiptName',
                flex: 2
            },{
                header: 'วันที่ชำระเงิน',
                dataIndex: 'receiveOfficerDate',
                flex: 1
            },{
                header: 'พิมพ์ใบเสร็จ',
                xtype: 'actioncolumn',
                width: 100,
                align: 'center',
                items: [{

                    getClass: function (v, meta, rec) {
                        this.items[0].tooltip = 'ยังไม่พิมพ์ใบเสร็จ';
                        return 'icon-notes-xsmall';
                    },
                    handler: function (grid, rowIndex, colIndex, node, e, record,rowNode) {
                        
                        this.fireEvent('itemclick', this, 'openfeewindow', grid,rowIndex, colIndex, record, node);
                    }
             }],
            renderer: function (value, meta, rec) {
                    if (rec.get('roleColor') == '1') {
                        meta.style = "background-color:#CCFFCC;";
                    }
                }
            }]

        });

        this.callParent(arguments);

    }
    // , 
    // afterRender: function()
    // {
    //     this.callParent(arguments);
        
    //     this.getStore().load();

        
    // }

    
});
