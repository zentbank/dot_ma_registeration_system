Ext.define('tourism.view.account.payment.PaymentOnlinePanel', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
        'tourism.view.account.payment.PaymentOnlineFormSearch',
        'tourism.view.account.payment.PaymentOnlineGrid'
    ],
    
    xtype: 'account-payment-online-panel',
    initComponent: function(){

    
    	Ext.apply(this, {
    		layout: {
		        type: 'border'
		    },
		    items: [{
		        region: 'north',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items:[{
    		
			            title:'ค้นหาข้อมูลการชำระเงิน',
			            collapsible: true,   // make collapsible
			            // collapsed : true,
			            xtype: 'account-payment-online-form-search',
			            id: 'account-payment-online-form-search-001',
			            // padding: '5px 5px 0px 5px',
			            frame: false,
			            border: false
		    	}]
		    },{
		        region: 'center',
		        xtype: 'panel',
		        layout: 'fit',
		        frame: false,
		        border: false,
		        items:[{
		            // title: 'การทำรายการใบอนุญาตธุรกิจนำเที่ยว',
		            xtype: 'account-payment-online-grid',
		            id: 'account-payment-online-grid-001',
		            // padding: '5px 5px 5px 5px',
		            autoScroll: true,
		            // frame: false 
		            border: false
		            
		        }]
		    }]
    	});

    	this.callParent(arguments);
    }
});