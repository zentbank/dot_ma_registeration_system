Ext.define('tourism.view.account.payment.PaymentReceiptDetailGrid',{
	extend: 'Ext.grid.Panel',
	alias: 'widget.account-receipt-payment-receiptdetail-grid',
	stripeRows: true,
	requires: [
       'Ext.grid.*',
       'Ext.data.*',
       'Ext.util.*',
       'Ext.state.*',
       'Ext.form.*',
       'Ext.grid.feature.Summary',
       
       'tourism.store.grid.ReceiptDetailStore',
       'tourism.model.grid.ReceiptDetailModel'
	],
	
	initComponent: function(){

		var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
	        clicksToMoveEditor: 1,
	        autoCancel: false
	    });
		
		
		
		 var store = Ext.create('Ext.data.Store', {
            storeId: 'account-payment-online-grid-receiptdetail-001',
            model : 'tourism.model.grid.ReceiptDetailModel',
            proxy: {
                type: 'ajax',
                actionMethods: {
                    create: 'POST',
                    read: 'POST',
                    update: 'POST',
                    destroy: 'POST'
                },
                api: {
                    read: 'tourleader/account/registration/paymentdetailfee'
                },
                reader: {
                    type: 'json',
                    root: 'list',
                    totalProperty: 'totalCount',
                    successProperty: 'success',
                    messageProperty: 'message'
                },
                listeners: {
                    exception: function (proxy,
                        response, operation) {

                        Ext.MessageBox
                            .show({
                                title: 'REMOTE EXCEPTION',
                                msg: operation
                                    .getError(),
                                icon: Ext.MessageBox.ERROR,
                                buttons: Ext.Msg.OK
                            });
                    }
                }
            }
        });

		Ext.apply(this,{
			store: store,
			features: [{
	            ftype: 'summary',
	            dock: 'bottom'
			}],
			height: 150
			 ,columns:[
	         {
	             header: 'ประเภทการชำระเงิน',
	             dataIndex: 'feeName',
	             flex: 3,
	             editor: {
	                 // defaults to textfield if no xtype is supplied
	                 allowBlank: false
	             }
	         	,summaryType: 'count'
		        ,summaryRenderer: function(value){
		            return ' รวม';
		        }
	         },
	         {
	             header: 'จำนวนเงิน',
	             dataIndex: 'feeMny',
	             flex: 1,
	         	align : 'right',
	             editor: {
	                 // defaults to textfield if no xtype is supplied
	                 allowBlank: false
	             }
	         	 ,renderer: function(value, metaData, record, rowIdx, colIdx, store, view){
	                return Ext.util.Format.number(value, '0,000.00');
	             }
	         	 ,summaryType: 'sum'
	         	 ,summaryRenderer: function(value, summaryData, dataIndex) {
	                return Ext.util.Format.number(value, '0,000.00'); //+ ' บาท';
	            }
	         	,field: {
	                xtype: 'numberfield'
	                ,allowBlank: false
	            }
	         	 
	         }
			
			 ],
			 
             plugins: [rowEditing]
		});
		
		
		this.callParent(arguments);
	}

});







