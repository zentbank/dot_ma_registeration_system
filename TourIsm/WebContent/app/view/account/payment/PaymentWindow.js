Ext.define('tourism.view.account.payment.PaymentWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.receipt-account-payment-window',

    requires: [
        'tourism.view.account.payment.PaymentForm'
    ],

    title : 'ชำระเงิน',
    layout: 'fit',
    width: 300,
    height: 350,
    //True to make the window modal and mask everything behind it when displayed
    modal: true,
    // iconCls: 'icon-form',

    initComponent: function() {

        // Ext.apply(this, {

        // });
        this.items = [
                {
                    xtype: 'receipt-account-payment-form',
                     id: 'receipt-account-payment-form',
                    frame: false,
                    border: false,
                    feePaidStatus: this.feePaidStatus
                    
                }
        ];

        this.callParent(arguments);
    }

});
