Ext.define('tourism.view.account.receipt.ReceiptForm',{
	extend: 'Ext.container.Container'
	,alias: 'widget.receipt-addedit-form'
	,requires: [
		'Ext.data.*',
		'Ext.form.*'
		,'tourism.view.account.receipt.ReceiptGrid'
		
    ]
    ,bodyPadding: 10

    ,initComponent: function(){

        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

        var receiveOfficerNameStore = Ext.create('tourism.store.combo.OfficerStore',{
            id:'ReceiptForm-receiveOfficerName-officeStore'
            ,listeners: {
                beforeload: function( store, operation, eOpts )
                    {
         
                        store.proxy.extraParams = {groupRole: 'OFFACC'};
                    }
            }
        });

        var authorityStore = Ext.create('tourism.store.combo.OfficerStore',{
            id:'ReceiptForm-authority-officeStore'
            ,listeners: {
                beforeload: function( store, operation, eOpts )
                    {
         
                        store.proxy.extraParams = {groupRole: 'OFFACC'};
                    }
            }
        });

        Ext.apply(this, {
            width: '100%',
            height: '100%',
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            } ,   
             layout: 'anchor',
             layoutCongig: {
                  pack:'center',
                  align:'middle'
             }
             ,defaults: {
                    anchor: '100%',
                    labelWidth: 150
             }
            ,items: [
                {
                    xtype: 'hiddenfield',
                    name: 'regId'
                },
                {
                    xtype: 'hiddenfield',
                    name: 'traderId'
                },
                {
                    xtype: 'hiddenfield',
                    name: 'traderType'
                },
                {
                    xtype: 'hiddenfield',
                    name: 'receiptStatus'
                },
                {
                    xtype: 'hiddenfield',
                    name: 'receiptId'
                    
                },
                 {
                     xtype: 'container',
                     layout: 'hbox',
                     layoutCongig: {
                          pack:'center',
                          align:'middle'
                     }
                     ,defaults: {
                            anchor: '100%',
                            labelWidth: 100
                     }
                     ,items: [
                         {
                            xtype: 'container',
                            flex: 1,
                            layout: 'anchor',
                            defaults: {
                                anchor: '100%',
                                labelWidth: 100
                            },
                            items:[
                            
                                {
                                    xtype: 'displayfield',
                                    fieldLabel: 'เล่มที่',
                                    name: 'bookNo'
                    //                ,anchor: '100%'
                                }
           
                            ]
                         }
                         ,{
                                 xtype: 'splitter'
                        }
                        ,{
                            xtype: 'container',
                            flex: 1,
                            layout: 'anchor',
                            defaults: {
                                anchor: '100%',
                                labelWidth: 100
                            },
                            items: [
                                 {
                                 
                                     xtype: 'displayfield',
                                     labelWidth: 100,
                                     fieldLabel: 'เลขที่ใบเสร็จ',
                                     name: 'receiptNo',
                                     anchor: '100%'
                                 }
                            ]
                     
                 
                        }
                    ] 
                 }           //             
                 ,{
                        xtype: 'displayfield',
                        fieldLabel: 'ลำดับการยื่นเรื่อง',
                        name: 'registrationNo'
        //                ,anchor: '100%'
                    }
                    ,{
                        xtype: 'displayfield',
                        fieldLabel: 'ประเภท',
                        name: 'traderCategoryName'
        //                ,anchor: '100%'
                    }
                    ,{
                        xtype: 'textfield',
                        fieldLabel: 'ชื่อ-สกุล ผุ้จ่ายเงิน',
                        name: 'receiptName',
                        allowBlank: false,
                        afterLabelTextTpl: required,
                        anchor: '100%'
                    } 
                 ,{
                     
                     xtype: 'account-receipt-grid'
                     ,border: false
            //       ,frame: false   
                 }
                 ,{
                     xtype: 'splitter'
                 }
                 ,{
                     xtype: 'splitter'
                 }
                 ,{
                     xtype: 'container',
                     layout: 'hbox',
                     layoutCongig: {
                          pack:'center',
                          align:'middle'
                     }
                     ,items: [
                     {
                        xtype: 'container',
                        flex: 1,
                        layout: 'anchor',
                        defaults: {
                            anchor: '100%',
                            labelWidth: 70
                        },
                        items:[
                        {
                           xtype: 'datefield',
                           name: 'receiveOfficerDate',
                           fieldLabel: 'วันที่รับเงิน',
                           format: 'd/m/B',
                           allowBlank: false,
                           afterLabelTextTpl: required,
                           value: new Date() 
                        }
                        ,{
                            xtype: 'combo',
                            fieldLabel: 'ผู้รับเงิน',
                            name: 'receiveOfficerName',
                            store: receiveOfficerNameStore,
                            queryMode: 'remote',
                            displayField: 'officerName',
                            valueField: 'officerId',
                            hiddenName: 'officerId',
                            triggerAction: 'all',
                            allowBlank: false,
                            afterLabelTextTpl: required
                        }
                        ]
                     }
                     ,{
                        xtype: 'container',
                        flex: 1,
                        layout: 'anchor',
                        defaults: {
                            anchor: '100%',
                            labelWidth: 70
                        },
                        items:[

                            {
                                xtype: 'combo',
                                fieldLabel: 'ผู้ตรวจสอบ',
                                name: 'authority',
                                store: authorityStore,
                                queryMode: 'remote',
                                displayField: 'officerName',
                                valueField: 'officerId',
                                hiddenName: 'officerId',
                                triggerAction: 'all',
                                allowBlank: false,
                                afterLabelTextTpl: required
                            }


                        ]
                     }
                    
                     ]
                 }
                         
                 ]
  
        });

        this.callParent(arguments);
    }
		

		
});