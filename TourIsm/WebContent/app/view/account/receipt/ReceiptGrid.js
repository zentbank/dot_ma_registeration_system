Ext.define('tourism.view.account.receipt.ReceiptGrid',{
	extend: 'Ext.grid.Panel',
	alias: 'widget.account-receipt-grid',
	stripeRows: true,
	requires: [
       'Ext.grid.*',
       'Ext.data.*',
       'Ext.util.*',
       'Ext.state.*',
       'Ext.form.*',
       'Ext.grid.feature.Summary',
       
       'tourism.store.grid.ReceiptDetailStore',
       'tourism.model.grid.ReceiptDetailModel'
	],
	
	initComponent: function(){

		var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
	        clicksToMoveEditor: 1,
	        autoCancel: false
//			,listeners: {
//				edit: function(editor, context, eOpts) {
//					context.grid.getStore().sync();
//				}
//			}
	    });
		
		var store = Ext.create('tourism.store.grid.ReceiptDetailStore',{
			  storeId: 'account-receipt-receiptdetail-store'
		  });
		
		Ext.apply(this,{
			store: store,
			features: [{
	            ftype: 'summary',
	            dock: 'bottom'
			}],
			height: 150
			 ,columns:[
	         {
	             header: 'ประเภทการชำระเงิน',
	             dataIndex: 'feeName',
	             flex: 3,
	             editor: {
	                 // defaults to textfield if no xtype is supplied
	                 allowBlank: false
	             }
	         	,summaryType: 'count'
		        ,summaryRenderer: function(value){
		            return ' รวม';
		        }
	         },
	         {
	             header: 'จำนวนเงิน',
	             dataIndex: 'feeMny',
	             flex: 1,
	         	align : 'right',
	             editor: {
	                 // defaults to textfield if no xtype is supplied
	                 allowBlank: false
	             }
	         	 ,renderer: function(value, metaData, record, rowIdx, colIdx, store, view){
	                return Ext.util.Format.number(value, '0,000.00');
	             }
	         	 ,summaryType: 'sum'
	         	 ,summaryRenderer: function(value, summaryData, dataIndex) {
	                return Ext.util.Format.number(value, '0,000.00'); //+ ' บาท';
	            }
	         	,field: {
	                xtype: 'numberfield'
	                ,allowBlank: false
	            }
	         	 
	         }
			
			 ],
			 
             plugins: [rowEditing]
		});
		
		
		this.callParent(arguments);
	}

});







