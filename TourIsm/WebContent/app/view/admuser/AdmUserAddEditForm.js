Ext.define('tourism.view.admuser.AdmUserAddEditForm', {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.data.*',
        'Ext.form.*',
        'Ext.tab.Tab',
        'tourism.view.admuser.AdmGroupGrid'
    ],
    alias: 'widget.admuser-addeditform',
    // title: 'ธุรกิจนำเที่ยว',
    bodyPadding: 5,
    autoScroll: true,

    initComponent: function(){

        var masprefixstore =  Ext.create('tourism.store.combo.MasPrefixStore',{
                                storeId:'admuser-masprefixstore-store'
        });

        var oganizationStore =  Ext.create('tourism.store.combo.OrganizationStore',{
                                storeId:'admuser-OrganizationStore-store'
        });
        var positionStore =  Ext.create('tourism.store.combo.MasPositionStore',{
            storeId:'admuser-MasPositionStore-store'
        });

        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
        
        Ext.apply(this, {
            width: '100%',
            height: '100%',
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },

            items: 
            [ 
                {
                    xtype:'container',
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%'
                    },
                    items:
                    [
                        {
                            
                            xtype: 'container',
                            layout: 'hbox',
                            layoutCongig: {
                                 pack:'center',
                                 align:'middle'
                            },
                            items: 
                            [
                                {
                                    xtype: 'hidden',
                                    name: 'userId'
                                },
                                {
                                    // left hand side
                                    xtype: 'container',
                                    layout: 'anchor',
                                    flex: 2,
                                    defaults: {
                                        anchor: '100%'
                                    },
                                    items:
                                    [
                                        {
                                            // ชื่อ
                                            xtype: 'fieldcontainer',
                                            labelAlign: 'left',
                                            fieldLabel: 'ชื่อ-นามสกุล',
                                            afterLabelTextTpl: required,
                                            // labelWidth: 100,
                                            layout: 'hbox',
                                            items: [
                                                {
                                                    xtype: 'masprefixcombo',
                                                    store: masprefixstore,
                                                    queryMode: 'remote',
                                                    displayField: 'prefixName',
                                                    valueField: 'prefixId',
                                                    hiddenName: 'prefixId',
                                                    name: 'prefixId',
                                                    triggerAction: 'all',
                                                    flex:1,
                                                    allowBlank: false,
                                                    tabIndex : 1,
                                                    prefixType: 'I'
                                   
                                                },
                                                {
                                                    xtype: 'splitter'
                                                },
                                                {
                                                    xtype: 'textfield',
                                                    name: 'userName',
                                                    flex: 2,
                                                    allowBlank: false,
                                                    tabIndex : 2
                                                }
                                            ]
                                        }
                                        
                                        
                                    ]
                                }//end left hand side
                                , {
                                    xtype: 'splitter'
                                }
                                ,{
                                    // right hand side
                                    xtype: 'container',
                                    layout: 'anchor',
                                    flex: 1,
                                    defaults: {
                                        anchor: '100%'
                                    },
                                    items:
                                    [
                                        {
                                            // นามสกุล(ภาษาไทย)
                                            xtype: 'textfield',
                                            // fieldLabel: 'นามสกุล',
                                            name: 'userLastname',
                                            anchor: '100%',
                                            labelAlign: 'left',
                                            allowBlank: false,
                                            afterLabelTextTpl: required,
                                            tabIndex : 3
                                        }
                                        
                                                                           
                                    ]
                                } // end right hand side
                            ]
                        }
                        ,{
                            xtype: 'combo',
                            fieldLabel: 'สำนักงาน',
                            store: oganizationStore,
                            queryMode: 'remote',
                            displayField: 'orgName',
                            afterLabelTextTpl: required,
                            valueField: 'orgId',
                            hiddenName: 'orgId',
                            name :'orgId',
                            tabIndex : 4
                        },{
                            xtype: 'combo',
                            fieldLabel: 'ตำแหน่ง',
                            store: positionStore,
                            queryMode: 'remote',
                            displayField: 'posName',
                            afterLabelTextTpl: required,
                            valueField: 'posId',
                            hiddenName: 'posId',
                            name :'posId',
                            tabIndex : 4
                        }
                        ,{
                           
                            xtype: 'textfield',
                            fieldLabel: 'ชื่อผู้ใช้งาน',
                            name: 'userLogin',
                            labelAlign: 'left',
                            allowBlank: false,
                            afterLabelTextTpl: required,
                            tabIndex : 5
                        }
                        ,
                        {
                            xtype: 'checkbox',
                            boxLabel  : 'เปลี่ยนรหัสผ่าน',
                            name      : 'changePassword',
                            inputValue: '1'
                        }
                        ,{
                            // นามสกุล(ภาษาไทย)
                            xtype: 'textfield',
                            fieldLabel: 'รหัสผ่าน',
                            name: 'loginPassword',
                            labelAlign: 'left',
                            allowBlank: false,
                            inputType: 'password',
                            afterLabelTextTpl: required,
                            tabIndex : 6
                        }
                        ,{
                            // นามสกุล(ภาษาไทย)
                            xtype: 'textfield',
                            fieldLabel: 'ยืนยันรหัสผ่าน',
                            name: 'loginPassword2',
                            labelAlign: 'left',
                            allowBlank: false,
                            inputType: 'password',
                            afterLabelTextTpl: required,
                            tabIndex : 7,
                            listeners: {
                                blur: function( field, evt, eOpts){
                                    var form = field.up('form');
                                    var loginPasswordField = form.getForm().findField('loginPassword');

                                    if(loginPasswordField.getValue() !== field.getValue())
                                    {
                                        Ext.MessageBox.show({
                                            title: 'REMOTE EXCEPTION',
                                            msg: 'รหัสผ่านไม่ตรงกัน',
                                            icon: Ext.MessageBox.ERROR,
                                            buttons: Ext.Msg.OK
                                        });

                                        loginPasswordField.setValue('');
                                        field.setValue('');
                                    }
                                }
                            }
                        }
                        ,{
                            xtype: 'tabpanel',
                            plain: true,
                            frame: false,
                            border: false,
                            items: [
                                {
                                    title: 'เมนูการทำงาน',
                                    layout: 'fit',
                                    frame: false,
                                    border: false,
                                   
                                    items:[
                                        {
                                            xtype: 'admuser-admgroup-grid',
                                            id: this.roleAction+'-admuser-admmenu-grid',
                                            padding: '5px 0px 5px 0px',
                                            
                                            // frame: false 
                                            anchor: '100% 100%',
                                            border: false,
                                            autoScroll: true,
                                            storeId: this.roleAction+'-admuser-admmenu-store',
                                            storepageSize: 20,
                                            storeUrl: 'admin/admuser/admmenu/read',
                                            roleAction: this.roleAction                 
                                        }
                                ]
                                },
                                {
                                    title: 'กลุ่มงาน',
                                    layout: 'fit',
                                    frame: false,
                                    border: false,
                                   
                                    items:[
                                        {
                                            xtype: 'admuser-admgroup-grid',
                                            id: this.roleAction+'-admuser-officergroup-grid',
                                            padding: '5px 0px 5px 0px',
                                         
                                            // frame: false 
                                            border: false,
                                            autoScroll: true,
                                            storeId: this.roleAction+'-admuser-officergroup-store',
                                            storepageSize: 20,
                                            storeUrl: 'admin/admuser/officergroup/read',
                                            roleAction: this.roleAction                 
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                        
                }
            ]
            , buttons: [{
                text: 'บันทึกข้อมูล',
                action: 'saveadmuser'
               
            }, {
                text: 'ยกเลิก',
                // width: 150,
                scope: this,
                handler: this.onResetClick
            }]    
        });
        this.callParent(arguments);
    }
    ,onResetClick: function(){
        this.up('window').close();
    }
    
});