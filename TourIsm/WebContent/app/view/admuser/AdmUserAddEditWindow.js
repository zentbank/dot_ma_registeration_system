Ext.define('tourism.view.admuser.AdmUserAddEditWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.admuser-add-window',

    requires: [
       
        'tourism.view.admuser.AdmUserAddEditForm'
    ],
    
    initComponent: function() {

        Ext.apply(this, {
            title:'ผู้ใช้งาน',
            layout: 'fit',
            width: 600,
            height: 500,
            autoShow: true,
            modal: true,
            items :[{
                    // title: 'บันทึกข้อมูลการพักใช้',
                    xtype: 'admuser-addeditform',
                    id: this.roleAction+'-admuser-addeditform',
                    border: false,
                    frame: false,
                    roleAction: this.roleAction
                }
            ]
        });

        this.callParent(arguments);
    },

});
