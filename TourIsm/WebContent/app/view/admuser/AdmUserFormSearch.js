Ext.define('tourism.view.admuser.AdmUserFormSearch' ,{
    // extend: 'Ext.form.FieldSet',
    extend: 'Ext.form.FormPanel',
    alias : 'widget.admuser-formsearch',
    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.Time',
        'Ext.form.CheckboxGroup',
        'Ext.layout.container.HBox',
        'Ext.form.FieldSet'
    ],
    bodyPadding: 10,
    initComponent: function(){

        Ext.apply(this, {
            items: [
                {
                        xtype: 'container',
                        // title: 'Payment',
                        layout: 'anchor',
                        defaults: {
                            anchor: '100%',
                            labelWidth: 150
                        },
                        items: [
                            {
                                xtype: 'container',
                                layout: 'hbox',
                                layoutCongig: {
                                     pack:'center',
                                     align:'middle'
                                },
                                items: [
                                    {
                                        xtype: 'container',
                                        // title: 'Payment',
                                        flex: 1,
                                        layout: 'anchor',
                                        defaults: {
                                            anchor: '100%',
                                            labelWidth: 150
                                        },
                                        items: [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'ชื่อ',
                                                name: 'userName'
                                            }
                                            ,{
                                                xtype: 'combo',
                                                fieldLabel: 'สำนักงาน',
                                                store: 'tourism.store.combo.OrganizationStore',
                                                queryMode: 'remote',
                                                displayField: 'orgName',
                                                valueField: 'orgId',
                                                hiddenName: 'orgId',
                                                name :'orgId'
                                            }
                                           
                                        ]
                                    },{
                                        xtype: 'splitter'
                                    },
                                    {
                                        xtype: 'container',
                                        flex: 1,
                                        layout: 'anchor',
                                        defaults: {
                                            anchor: '100%',
                                            labelWidth: 150
                                        },
                                        items: [
                                            {
		                                        xtype: 'textfield',
		                                        fieldLabel: 'นามสกุล',
		                                        name: 'userLastname'
		                                    }
                                             // ,{
                                            //     xtype: 'combo',
                                            //     fieldLabel: 'กลุ่มผู้ใช้งาน',
                                            //     store: 'tourism.store.combo.OrganizationStore',
                                            //     queryMode: 'remote',
                                            //     displayField: 'groupName',
                                            //     valueField: 'groupId',
                                            //     hiddenName: 'groupId',
                                            //     name :'groupId'
                                            // }
                                        ]
                                    }
                                ]
                            }
                            
                            
                        ]
                },
                {
                    xtype: 'toolbar',
                    border: false,
                    padding: '6px 0 6px 0px',
                    items: [{
                        xtype: 'button',
                        text : 'ค้นหา',
                        action: 'searchAdmUser'
                    }]
                }

            ]            
        });

        this.callParent(arguments);
    }
    // width: '100%',

});    
