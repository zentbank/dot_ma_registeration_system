Ext.define('tourism.view.admuser.AdmUserGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.admuser-grid',
		stripeRows: true,
		requires: [
			'Ext.grid.Column',
			'tourism.store.admuser.AdmUserStore',
			'Ext.toolbar.Paging',
			'Ext.grid.plugin.RowEditing',
			'Ext.grid.column.Template',
			'Ext.grid.column.Action',
			'Ext.grid.plugin.RowExpander',
			'Ext.grid.feature.Grouping',
			'tourism.view.admuser.AdmUserAddEditWindow',
			'tourism.view.admuser.AdmUserSignatureWindow',
			'tourism.store.combo.MasPositionStore'
	
	
		],
		initComponent: function(){

	
			var columnModel = this.getColumn();


			var admUserStore = Ext.create('tourism.store.admuser.AdmUserStore',{
					storeId: this.storeId,
					roleAction: this.roleAction,
		            pageSize : this.storepageSize,
				    proxy: {
					    type: 'ajax',
					    actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
					    api: {
					        read: this.storeUrl,
					        destroy: this.destroy
					    },
					    reader: {
					        type: 'json',
					        // metaProperty: '',
					        root: 'list',
					        // idProperty: 'emailId',
					        totalProperty: 'totalCount',
					        successProperty: 'success',
					        messageProperty: 'message'
					    },
					    writer: {
					        type: 'json',
					        // encode: true,
					        writeAllFields: true, ////just send changed fields
					        // root: 'data',
					        allowSingle: false, //always wrap in an array
					        batch: false
					    },
					    listeners: {
					        exception: function(proxy, response, operation){

					            Ext.MessageBox.show({
					                title: 'REMOTE EXCEPTION',
					                msg: operation.getError(),
					                icon: Ext.MessageBox.ERROR,
					                buttons: Ext.Msg.OK
					            });
					        }
					    }

					}
					,listeners: {
						beforeload: function( store, operation, eOpts )
					        {					        	
					        	var formId = '#'+store.roleAction+'-admuser-formsearch';

					        	var formSearch = Ext.ComponentQuery.query(formId)[0];
					        	var values = formSearch.getValues();
   								
					        	 for (field in values) 
							      {
							        if (Ext.isEmpty(values[field])) 
							        {
							          delete values[field];
							        }
							        
							      }
							    store.proxy.extraParams = values;
					        }
					}
			});
	   

			Ext.apply(this, {
				store: admUserStore,
				dockedItems:[
					{
						xtype: 'pagingtoolbar',
						store: admUserStore,
						dock: 'bottom',
						displayInfo: true,
						items: [
							{
									text: 'เพิ่มผู้ใช้งาน',
									action: 'adduser'
			        		}
						]

					}
				],
				columns: columnModel
			});

			this.callParent(arguments);

		},
        getColumn: function()
        {
        	return [
					{
						header: 'userId',
						dataIndex: 'userId',
						hidden: true
					},
					
					{
						header: 'ชื่อ',
						dataIndex: 'userFullName',
						flex: 3
					},
					{
						header: 'ตำแหน่ง',
						dataIndex: 'posName',
						flex: 2
					},
					{
						header: 'สำนักงาน',
						dataIndex: 'orgName',
						flex: 2
					},
					{
			        	header: 'ลายเซ็น',
			            xtype: 'actioncolumn',
			           	width:60,
			           	align : 'center',
			            items: [{
			                iconCls: 'icon-edit-xsmall',
			                tooltip: 'เพิ่มลายเซ็น',
				            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {

				            		this.fireEvent('itemclick', this, 'addsignature', grid, rowIndex, colIndex, record, node);
							}
			            }]
			        },
			        {
			        	header: 'แก้ไข',
			            xtype: 'actioncolumn',
			           	width:60,
			           	align : 'center',
			            items: [{
			                iconCls: 'icon-edit-xsmall',
			                tooltip: 'แก้ไขผู้ใช้งาน',
				            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {

				            		this.fireEvent('itemclick', this, 'edituser', grid, rowIndex, colIndex, record, node);
							}
			            }]
			        },
			        {
			        	header: 'ลบ',
			            xtype: 'actioncolumn',
			           	width:60,
			           	align : 'center',
			            items: [{
			                iconCls: 'icon-delete-page-xsmall',
			                tooltip: 'ลบผู้ใช้งาน',
				            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
			                   
			                    this.fireEvent('itemclick', this, 'deleteuser', grid, rowIndex, colIndex, record, node);
				            		
							}
			            }]
			        }
				];
        }
	});