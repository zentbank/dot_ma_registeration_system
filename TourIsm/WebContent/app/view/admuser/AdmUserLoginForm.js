Ext.define('tourism.view.admuser.AdmUserLoginForm',{
    extend: 'Ext.form.Panel'
    ,alias: 'widget.admuser-admUserLoginForm-form'
    ,requires: [
        'Ext.data.*',
        'Ext.form.*'
        
    ],

    initComponent: function(){

        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

       
        
        Ext.apply(this, {
            width: '100%',
            height: '100%',
            bodyPadding: 10,
            
            defaultType: 'textfield',
            defaults: {
                anchor: '100%'
            },

            items: 
            [
                {
                    allowBlank: false,
                    fieldLabel: 'ชื่อผู้ใช้งาน',
                    name: 'userLogin',
                    emptyText: 'ชื่อผู้ใช้งาน',
                    afterLabelTextTpl: required
                },
                {
                    allowBlank: false,
                    fieldLabel: 'รหัสผ่าน',
                    name: 'loginPassword',
                    emptyText: 'รหัสผ่าน',
                    inputType: 'password',
                    afterLabelTextTpl: required
                }
               
            ],

            buttons: [{
                text: 'เข้าสู่ระบบ',
                scope: this,
                action: 'admLogin'
            }, {
                text: 'ยกเลิก',
                // width: 150,
                scope: this,
                handler: this.onResetClick
            }]    
        });
        this.callParent(arguments);
    }
    ,onResetClick: function(){
        this.up('window').close();
    }

        

        
});