Ext.define('tourism.view.admuser.AdmUserPanel', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
        'tourism.view.admuser.AdmUserFormSearch',
        'tourism.view.admuser.AdmUserGrid'

        
    ],
    alias : 'widget.admuser-panel',
    initComponent: function(){

    	Ext.apply(this, {
    		layout: {
		        type: 'border'
		    },
		    items: [{
		        region: 'north',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items:[{
	            title:'ผู้ใช้งานระบบ',
	            collapsible: true,   // make collapsible
	            collapsed : false,
	            xtype: 'admuser-formsearch',
	            id: this.roleAction+'-admuser-formsearch',

	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false,
	            roleAction: this.roleAction
			}]
		    },{
		        region: 'center',
		        xtype: 'panel',
		        layout: 'fit',
		        frame: false,
		        border: false,
		        items:[
		        {
		        
		            xtype: 'admuser-grid',
		            id: this.roleAction+'-admuser-grid',
		            // padding: '5px 5px 5px 5px',
		            autoScroll: true,
		            // frame: false 
		            border: false,
		            storeId: this.roleAction+'-admuser-store',
		            storepageSize: 20,
		            storeUrl: 'admin/admuser/read',
		            roleAction: this.roleAction,
		            destroy: 'admin/admuser/delete'
		        }
		        ]
		    }]
    	});

    	this.callParent(arguments);
    }
});