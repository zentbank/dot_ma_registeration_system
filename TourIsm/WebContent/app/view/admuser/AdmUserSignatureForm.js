Ext.define('tourism.view.admuser.AdmUserSignatureForm', {
    extend: 'Ext.form.Panel',
    alias : 'widget.admuser-signature-form',
    requires: [
        'Ext.data.*',
        'Ext.form.*',
        'Ext.tab.Tab'
        
    ],
   
    bodyPadding: 5,
    autoScroll: true,

    initComponent: function(){

     var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
        
        Ext.apply(this, {
            width: '100%',
            height: '100%',
            // layout: 'anchor',
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },
            items: 
            [
                {
                    xtype: 'hiddenfield',
                    name: 'userId'
                },

                {
                    xtype: 'textfield',
                    fieldLabel: 'ชื่อ',
                    name: 'userFullName',
                    anchor: '100%',
                    allowBlank: false
                }
                ,{
                    xtype: 'filefield',
                    // id: 'form-file',
                    emptyText: 'เลือกเอกสารที่ต้องการ',
                    fieldLabel: 'ลายเซ็น',
                    name: 'signatureFile',
                    allowBlank: false,
                    afterLabelTextTpl: required,
                    anchor: '100%',
                    buttonText: '',
                    buttonConfig: {
                        iconCls: 'upload-icon'
                    }
                }
            ],

            buttons: [{
                text: 'บันทึก',
                scope: this,
                action: 'saveSignature',
                id: 'admuer-form-saveSignature'
            }, {
                text: 'ยกเลิก',
                // width: 150,
                scope: this,
                handler: this.onResetClick
            }]    
        });
        this.callParent(arguments);
    }
    ,onResetClick: function(){
        this.up('window').close();
    }
});