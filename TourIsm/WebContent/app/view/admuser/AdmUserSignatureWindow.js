Ext.define('tourism.view.admuser.AdmUserSignatureWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.admuser-signature-add-window',

    requires: [
        
        'tourism.view.admuser.AdmUserSignatureForm'
    ],
    
    initComponent: function() {
            

        Ext.apply(this, {
            title:'ลายเซ็น',
            activeItem: 0,
            autoShow: true,
            width: 400,
            autoShow: true,
            modal: true,
            items :[
                {
                    xtype: 'admuser-signature-form',
                    id: this.roleAction+'-admuser-signatureform',
                    border: false,
                    frame: false,
                    roleAction: this.roleAction
                }
            ]
        });

        this.callParent(arguments);
    }

});
