Ext.define('tourism.view.approvalprocess.ApprovalProcessBusinessFormSearch' ,{
    // extend: 'Ext.form.FieldSet',
    extend: 'Ext.form.FormPanel',
    alias : 'widget.approvalprocess-business-formsearch',
    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.Time',
        'Ext.form.CheckboxGroup',
        'Ext.layout.container.HBox',
        'Ext.form.FieldSet'
    ],
    bodyPadding: 10,
    // width: '100%',
    items: [
        {
                xtype: 'container',
                // title: 'Payment',
                layout: 'anchor',
                defaults: {
                    anchor: '100%',
                    labelWidth: 150
                },
                items: [
                    {
                        xtype: 'hidden',
                        name: 'traderType',
                        value: 'B'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'เลขที่รับเรื่อง',
                        name: 'registrationNo',
                        anchor: '50%'
                    },
                    
                    {
                        xtype: 'container',
                        layout: 'hbox',
                        layoutCongig: {
                             pack:'center',
                             align:'middle'
                        },
                        items: [
                            {
                                xtype: 'container',
                                // title: 'Payment',
                                flex: 1,
                                layout: 'anchor',
                                defaults: {
                                    anchor: '100%',
                                    labelWidth: 150
                                },
                                items: [
                                    {
                                        xtype: 'datefield',
                                        fieldLabel: 'วันที่รับเรื่อง',
                                        // labelWidth: 150,
                                        name: 'registrationDateFrom',
                                        format: 'd/m/B'
                                    },
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'ชื่อธุรกิจนำเที่ยว(TH)',
                                        // labelWidth: 150,
                                        name: 'traderName'
                                    },
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'ชื่อผู้ขอใบอนุญาต',
                                        // labelWidth: 150,
                                        name: 'firstName'
                                    },
                                    {
                                        xtype: 'combo',
                                        fieldLabel: 'ประเภทการทำรายการ',
                                        store: 'tourism.store.combo.BusinessRegistrationTypeStore',
                                        queryMode: 'local',
                                        displayField: 'registrationTypeName',
                                        valueField: 'registrationType',
                                        hiddenName: 'registrationType',
                                        name: 'registrationType'
                                    }
                                ]
                            },{
                                xtype: 'splitter'
                            },
                            {
                                xtype: 'container',
                                flex: 1,
                                layout: 'anchor',
                                defaults: {
                                    anchor: '100%',
                                    labelWidth: 150
                                },
                                items: [
                                    {   
                                        xtype: 'datefield',
                                        fieldLabel: 'ถึงวันที่',
                                        
                                        name: 'registrationDateTo',
                                        flex: 1,
                                        format: 'd/m/B'
                                        // ,value: Ext.Date.add(new Date(), Ext.Date.YEAR, 543), //2554 ณ ปัจจุบัน
                                       
                                    },
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'ชื่อธุรกิจนำเที่ยว(EN)',
                                        // labelWidth: 150,
                                        name: 'traderNameEn'
                                    },
                                     {
                                        xtype: 'combo',
                                        fieldLabel: 'ประเภทธุรกิจนำเที่ยว',
                                        store: 'tourism.store.combo.BusinessCategoryStore',
                                        queryMode: 'local',
                                        displayField: 'traderCategoryName',
                                        valueField: 'traderCategory',
                                        hiddenName: 'traderCategory',
                                        name :'traderCategory'
                                    },
                                     {
                                        xtype: 'combo',
                                        fieldLabel: 'สถานะเรื่อง',
                                        store: 'tourism.store.combo.RegProgressStatusStore',
                                        queryMode: 'local',
                                        displayField: 'regProgressStatusName',
                                        valueField: 'regProgressStatus',
                                        hiddenName: 'regProgressStatus',
                                        name: 'regProgressStatus'
                                    }
                                ]
                            }
                        ]
                    }
                    
                ]
        },
        {
            xtype: 'toolbar',
            border: false,
            padding: '6px 0 6px 0px',
            items: [{
                xtype: 'button',
                text : 'ค้นหา',
                action: 'searchLicenseBetweenRegistration'
            }]
        }

    ]
});    
