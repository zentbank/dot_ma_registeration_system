Ext.define('tourism.view.approvalprocess.ApprovalProcessFormSearch' ,{
    // extend: 'Ext.form.FieldSet',
    extend: 'Ext.form.FormPanel',
    alias : 'widget.approvalprocess-formsearch',
    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.Time',
        'Ext.form.CheckboxGroup',
        'Ext.layout.container.HBox',
        'Ext.form.FieldSet'
    ],
    bodyPadding: 10,
    // width: '100%',
    items: [
        {
            xtype: 'container',
            layout: 'hbox',
            layoutCongig: {
                 pack:'center',
                 align:'middle'
            },
            items: [
                {
                   xtype: 'fieldset',
                   // title: 'ข้อมูลใบอนุญาต',
                   flex:1,
                   border: false,
                   // labelWidth: 400,
                   defaults: {
                    anchor: '100%',
                    labelWidth: 150
                   },
                   items: [
                       
                         {
                            xtype: 'textfield',
                            fieldLabel: 'เลขที่รับเรื่อง',
                            name: 'registrationNo'
                        },
                        {
                            xtype: 'datefield',
                            fieldLabel: 'วันที่รับเรื่อง',
                            name: 'registrationDateFrom',
                            // strict: true,
                            format: 'd/m/B'
                            // ,value: Ext.Date.add(new Date(), Ext.Date.YEAR, 543), //2554 ณ ปัจจุบัน
                           
                        },
                        {
                            xtype: 'checkboxgroup',
                            fieldLabel: 'สถานะ',
                            // Arrange checkboxes into two columns, distributed vertically
                            columns: 3,
                            vertical: true,
                            items: [
                                { boxLabel: 'ยังไม่ได้ส่งเรื่อง',  name: 'approveStatusw', inputValue: 'W' },
                                { boxLabel: 'ส่งเรื่องแล้ว',  name: 'approveStatusa', inputValue: 'A' },
                                { boxLabel: 'เรื่องตีกลับ',  name: 'approveStatusr', inputValue: 'R' }
                                
                            ]
                        }
                        
                   ]
                },
                {
                   xtype: 'fieldset',
                   // title: 'ข้อมูลใบอนุญาต',
                   flex:1,
                   border: false,
                   // anchor :'100%',
                   // labelWidth: 400,
                   defaults: {
                    anchor: '100%',
                    labelWidth: 150
                   },
                   items: [
                        {
                            xtype: 'combo',
                            fieldLabel: 'ประเภทการทำรายการ',
                            store: 'tourism.store.combo.BusinessRegistrationTypeStore',
                            queryMode: 'local',
                            displayField: 'registrationTypeName',
                            valueField: 'registrationType',
                            hiddenName: 'registrationType',
                            name: 'registrationType'
                        },
                        {
                            xtype: 'datefield',
                            fieldLabel: 'ถึงวันที่',
                            name: 'registrationDateTo',
                            format: 'd/m/B'
                            // ,value: Ext.Date.add(new Date(), Ext.Date.YEAR, 543), //2554 ณ ปัจจุบัน
                           
                        }
                   ]
                }

            ]
        },
        {
            xtype: 'toolbar',
            border: false,
            padding: '6px 0 6px 0px',
            items: [{
                xtype: 'button',
                text : 'ค้นหา',
                action: 'searchLicenseBetweenRegistration'
                // iconCls: 'icon-search',
                // itemId: 'search-business-registrationfromsearch-btn'
            }]
        }

    ]

});    
