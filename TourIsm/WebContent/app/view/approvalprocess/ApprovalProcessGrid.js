Ext.define('tourism.view.approvalprocess.ApprovalProcessGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.approvalprocess-grid',
		stripeRows: true,
		requires: [
			'Ext.grid.Column',
			'tourism.store.registration.RegistrationStore',
			'Ext.toolbar.Paging',
			'Ext.grid.plugin.RowEditing',
			'Ext.grid.column.Template',
			'Ext.grid.column.Action',
			'Ext.grid.plugin.RowExpander',
			'Ext.grid.feature.Grouping',
			'tourism.view.business.registration.BusinessRegistrationAddEditWindow',
			'tourism.view.guide.registration.GuideRegistrationAddEditWindow',
			'tourism.view.tourleader.registration.TourleaderRegistrationAddEditWindow'
		],
		initComponent: function(){
			var registrationStore = Ext.create('tourism.store.registration.RegistrationStore',{
				storeId: this.storeId,
				pageSize : this.storepageSize,
				groupField: 'registrationTypeName',
				roleAction: this.roleAction,
		        traderType: this.traderType,
				proxy: {
					type: 'ajax',
					actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
					api: {
					    read: this.storeUrl
					},
					reader: {
					    type: 'json',
					    root: 'list',
					    totalProperty: 'totalCount',
					    successProperty: 'success',
					    messageProperty: 'message'
					},
					listeners: {
					    exception: function(proxy, response, operation){

					        Ext.MessageBox.show({
					            title: 'REMOTE EXCEPTION',
					            msg: operation.getError(),
					            icon: Ext.MessageBox.ERROR,
					            buttons: Ext.Msg.OK
					        });
					    }
					}
				}
				,listeners: {
					scope: this,
					// single: true,
					beforeload: function( store, operation, eOpts )
				        {
				        					        	
				        	var formId = '#' + store.roleAction+'-'+store.traderType+'-approvalprocess-formsearch';


				    
				        	var formSearch = Ext.ComponentQuery.query(formId)[0];
				        	var values = formSearch.getValues();
								
				        	 for (field in values) 
						      {
						        if (Ext.isEmpty(values[field])) 
						        {
						          delete values[field];
						        }
						        
						      }
						    store.proxy.extraParams = values;
				        }
				}
			});

			Ext.apply(this, {
				store: registrationStore,
				dockedItems:[
					{
						xtype: 'pagingtoolbar',
						store: registrationStore,
						dock: 'bottom',
						displayInfo: true

					}
				],
				viewConfig: {
					stripeRows:true ,
					getRowClass: function(record, index) {                            
						if (record.get('roleColor') == 'true') {
						    return "online-block";
						}
					}
				},
				columns: [
					{
						header: 'regId',
						dataIndex: 'regId',
						hidden: true
					},
					{
						header: 'เลขที่ใบอนุญาต',
						dataIndex: 'licenseNo',
						flex: 1
					},
					{
						header: 'ชื่อ',
						dataIndex: 'traderName',
						flex: 2
					},
					{
						header: 'ประเภทการจดทะเบียน',
						dataIndex: 'registrationTypeName',
						flex: 2
					},
					{
						header: 'เลขที่รับเรื่อง',
						dataIndex: 'registrationNo',
						flex: 1
					},
					{
						header: 'วันที่รับเรื่อง',
						dataIndex: 'registrationDate',
						flex: 1
					},
			        {
			        	header: '',
			            xtype: 'actioncolumn',
			           	width:50,
			           	align : 'center',
			            items: [{
			           
			                 getClass: function(v, meta, rec) {          
			                    if (rec.get('progressBackStatus') != 'B') {
			                        this.items[0].tooltip = 'รับเรื่อง';
			                        return 'icon-comment-xsmall';
			                    } else {
			                        this.items[0].tooltip = 'เรื่องตีกลับ';
			                        return 'icon-comment-xsmall-red';
			                    }
			                },
				            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {

				                this.fireEvent('itemclick', this, 'approvalprogress', grid, rowIndex, colIndex, record, node);
				            }
			            }]
			        },
			        {
			        	header: '',
			            xtype: 'actioncolumn',
			           	width:50,
			           	align : 'center',
			            items: [{
			                // iconCls: 'icon-send-xsmall',
			                // tooltip: 'การผ่านเรื่อง',
			                getClass: function(v, meta, rec) {          
			                    if (rec.get('roleAction') == '1') {
			                        this.items[0].tooltip = 'การผ่านเรื่อง';
			                        this.tdCls = 'red';
			                        return 'icon-send-xsmall';
			                    } else {
			                        this.items[0].tooltip = 'ผ่านเรื่องแล้ว';
			                        return '';
			                    }
			                },
				            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
				            	if (record.get('roleAction') == '1') {
				            		this.fireEvent('itemclick', this, 'approvalverification', grid, rowIndex, colIndex, record, node);
				            	}

				                
				            }
			            }]
			            ,renderer : function(value, meta, rec) {
						     if (rec.get('roleColor') == '1') {
						        meta.style = "background-color:#CCFFCC;";
						    } 
						}
			        },
			        {
			        	header: '',
			            xtype: 'actioncolumn',
			           	width:50,
			           	align : 'center',
			            items: [{
			                // iconCls: 'icon-edit-xsmall',
			                // tooltip: 'รายละเอียด',
  							getClass: function(v, meta, rec) {    
  								// console.log("rec.get('roleAction'): "+rec.get('roleAction'));      
			                    if (rec.get('roleAction') == '1') {
			                        this.items[0].tooltip = 'แก้ไข';
			                        return 'icon-edit-xsmall';
			                    } else {
			                        this.items[0].tooltip = 'รายละเอียด';
			                        return 'icon-notes-xsmall';
			                    }
			                },			        
				            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
				            	// console.log("record.get('roleAction'): "+record.get('roleAction'));
				            	if (record.get('roleAction') == '1') {
				            		// console.log("record.get('registrationProgress'): "+record.get('registrationProgress'));
				            		if(record.get('registrationProgress') == 'CHK')
				            		{
				            			 this.fireEvent('itemclick', this, 'editregistration', grid, rowIndex, colIndex, record, node);
				            		}
				            		else
				            		{
				            			 this.fireEvent('itemclick', this, 'approvaleditregistration', grid, rowIndex, colIndex, record, node);
				            		}
			                        
			                    } else {
			                        this.fireEvent('itemclick', this, 'approvaleditregistration', grid, rowIndex, colIndex, record, node);
			                    }
				            }
			            }]
			        }
				]
			});

			this.callParent(arguments);


		}
        ,features: [{
	        ftype: 'grouping',
	        groupHeaderTpl: '{name} ({rows.length} รายการ)',
	        hideGroupedHeader: true,
	        startCollapsed: false
	    }]
	});