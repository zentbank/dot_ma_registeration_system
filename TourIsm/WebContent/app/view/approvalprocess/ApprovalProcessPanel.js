Ext.define('tourism.view.approvalprocess.ApprovalProcessPanel', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
        'tourism.view.approvalprocess.ApprovalProcessBusinessFormSearch',
        'tourism.view.approvalprocess.ApprovalProcessGrid',
        'tourism.view.approvalprocess.ApprovalProcessGuideFormSearch',
        'tourism.view.approvalprocess.ApprovalProcessTourleaderFormSearch'
    ],
    
    xtype: 'approvalprocess-panel',
    initComponent: function(){

    	var formSearch = this.getBusinessFormSearch(this.roleAction);

    	if(this.traderType == 'guide')
    	{
    		formSearch = this.getGuideFormSearch(this.roleAction);
    	}

    	if(this.traderType == 'tourleader')
    	{
    		formSearch = this.getToruleaderFormSearch(this.roleAction);
    	}
    	Ext.apply(this, {
    		layout: {
		        type: 'border'
		    },
		    items: [{
		        region: 'north',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items:[formSearch
		       
		        ]
		    },{
		        region: 'center',
		        xtype: 'panel',
		        layout: 'fit',
		        frame: false,
		        border: false,
		        items:[{
		            // title: 'การทำรายการใบอนุญาตธุรกิจนำเที่ยว',
		            xtype: 'approvalprocess-grid',
		            id: this.roleAction+'-'+this.traderType+'-approvalprocess-grid',
		            // padding: '5px 5px 5px 5px',
		            autoScroll: true,
		            // frame: false 
		            border: false,
		            storeId: this.roleAction+'-'+this.traderType+'-approvalprocess-store',
		            storepageSize: 20,
		            storeUrl: 'business/'+this.roleAction+'/registration/read',
		            roleAction: this.roleAction,
		            traderType: this.traderType
		            
		        }]
		    }]
    	});

    	this.callParent(arguments);
    },
    getBusinessFormSearch: function(roleAction)
    {
    	return {
    		
	            title:'ค้นหาการทำรายการใบอนุญาตธุรกิจนำเที่ยว',
	            collapsible: true,   // make collapsible
	            // collapsed : true,
	            xtype: 'approvalprocess-business-formsearch',
	            id: roleAction+'-business'+'-approvalprocess-formsearch',
	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false
    	};
    },
    getGuideFormSearch: function(roleAction)
    {
    	return {
    		
	            title:'ค้นหาการทำรายการใบอนุญาตมัคคุเทศก์',
	            collapsible: true,   // make collapsible
	            // collapsed : true,
	            xtype: 'approvalprocess-guide-formsearch',
	            id: roleAction+'-guide'+'-approvalprocess-formsearch',
	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false
    	};
    },
    getToruleaderFormSearch: function(roleAction)
    {
    	return {
    		
	            title:'ค้นหาการทำรายการใบอนุญาตผู้นำเที่ยว',
	            collapsible: true,   // make collapsible
	            // collapsed : true,
	            xtype: 'approvalprocess-tourleader-formsearch',
	            id: roleAction+'-tourleader'+'-approvalprocess-formsearch',
	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false
    	};
    }
});