Ext.define('tourism.view.business.registration.BusinessNameSearchWindow',{
	extend: 'Ext.window.Window',
	alias : 'widget.business-name-search_window',

    requires: [
        'tourism.view.info.SearchBusinessNamePanel'
    ],
    initComponent: function() {
    	
    

    Ext.apply(this,{
    	title:'ค้นหารายชื่อใกล้เคียง',
        // layout: 'fit',
        layout: 'card',
        activeItem: 0,
        autoShow: true,
        width: 800,
        height: 600,
        autoShow: true,
        modal: true,
        items :[
       
            {
                // title: 'เลือกใบอนุญาต',
                xtype: 'info-license-searchbusinessnamepanel',
                id: 'registration-business-searchnamepanel',
                border: false,
                frame: false
            }
    
        ]
    });
    this.callParent(arguments);
    }
});