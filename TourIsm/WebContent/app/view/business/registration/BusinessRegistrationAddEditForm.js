Ext.define('tourism.view.business.registration.BusinessRegistrationAddEditForm', {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.data.*',
        'Ext.form.*',
        'Ext.tab.Tab',
        'Ext.tip.QuickTipManager',
        'Ext.ux.statusbar.StatusBar',
        'Ext.ux.statusbar.ValidationStatus',
        'tourism.view.business.registration.person.PersonForm'
    ],
    xtype: 'form-business-registration-addedit',
    // title: 'ธุรกิจนำเที่ยว',
    bodyPadding: 5,
    autoScroll: true,
    initComponent: function(){

        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
   
        Ext.apply(this, {
            width: '100%',
            height: '100%',
           // bbar: statusbar,
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },
            items: [ {
                xtype: 'container',
                // title: 'Payment',
                layout: 'anchor',
                defaults: {
                    anchor: '100%'
                },
                items: [{
                    xtype: 'container',
                    layout: 'hbox',
                    margin: '0 0 5 0',
                    items: [
                        {
                            xtype: 'hiddenfield',
                            name: 'regId'
                        },
                        {
                            xtype: 'hiddenfield',
                            name: 'registrationType'
                        },
                        {
                            xtype: 'textfield',
                            name: 'registrationNo',
                            // labelAlign: 'left',
                            fieldLabel: 'เลขที่รับเรื่อง',
                            flex: 1,
                            allowBlank: false
                        },{
                            xtype: 'datefield',
                            fieldLabel: 'วันที่รับเรื่อง',
                            name: 'registrationDate',
                            flex: 1,
                            format: 'd/m/B',
                            allowBlank: false
                            // ,
                            // margin: '0 0 0 5'
                            // ,value: Ext.Date.add(new Date(), Ext.Date.YEAR, 543), //2554 ณ ปัจจุบัน
                           
                        },{
                            xtype: 'splitter'
                        },{
                            xtype: 'textfield',
                            name: 'authorityName',
                            fieldLabel: 'ผู้รับเรื่อง',
                            flex: 1,
                            allowBlank: false,
                            margin: '0 0 0 5'
                        }
                    ]
                },
                {
                    xtype: 'hiddenfield',
                    name: 'traderId'
                },
                {
                    xtype: 'hiddenfield',
                    name: 'personId'
                },
                {
                    xtype: 'hiddenfield',
                    name: 'traderType'
                },
                {
                    xtype: 'textfield',
                    name: 'traderName',
                    fieldLabel: 'ชื่อสำนักงานประกอบธุรกิจนำเที่ยว (ภาษาไทย)',
                    labelAlign: 'top',
                    allowBlank: false,
                    afterLabelTextTpl: required,
                    listeners: {
                       blur: function(value){
                    	   var form = Ext.create('Ext.form.Panel');
                    	   var fthis = this.up('form');
                    	   var traderName = this.getValue();
                    	   form.load({
                               url: 'business/registration/checkname/verification',
                               method : 'POST',
                               model: 'tourism.model.registration.RegistrationModel',
                               params: {traderName:traderName},
                               success: function(form, action) {
                            	   
                                var dataObj = action.result.data;
                                var title = fthis.getForm().findField('statusCheck');
                                if(dataObj.check == 'false')
                                {
                                	title.setValue('<span style="color:red;font-weight:bold">ชื่อสำนักงานประกอบธุรกิจนำเที่ยว (ภาษาไทย) นี้มีอยู่แล้วในระบบ</span>');
                                }else{
                                	title.setValue('<span style="color:green;font-weight:bold">ชื่อสำนักงานประกอบธุรกิจนำเที่ยว (ภาษาไทย) นี้สามารถใช้ในการจดทะเบียนได้</span>')
                              }
                                
                             
                               
                               },
                               failure: function(form, action) {
                                 Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
                               },
                               scope: this 
                           }); 	   
                       }
                    }
                },
              {
                    xtype: 'textfield',
                    name: 'traderNameEn',
                    fieldLabel: 'ชื่อสำนักงานประกอบธุรกิจนำเที่ยว (ภาษาต่างประเทศ)',
                    labelAlign: 'top',
                    allowBlank: false,
                    afterLabelTextTpl: required,
                    listeners: {
                        blur: function(value){
                           var form = Ext.create('Ext.form.Panel');
                           var fthis = this.up('form');
                     	   var traderNameEn = value.getValue();
 
                     	  form.load({
                                url: 'business/registration/checkname/verificationen',
                                method : 'POST',
                                model: 'tourism.model.registration.RegistrationModel',
                                params: {traderNameEn:traderNameEn},
                                success: function(form, action) {

                                  var dataObj = action.result.data;
                                  var title = fthis.getForm().findField('statusCheck');
                                  if(dataObj.check == 'false')
                                  {
                                  	title.setValue('<span style="color:red;font-weight:bold">ชื่อสำนักงานประกอบธุรกิจนำเที่ยว (ภาษาต่างประเทศ) นี้มีอยู่แล้วในระบบ</span>');
                                  }else{
                                  	title.setValue('<span style="color:green;font-weight:bold">ชื่อสำนักงานประกอบธุรกิจนำเที่ยว (ภาษาต่างประเทศ) นี้สามารถใช้ในการจดทะเบียนได้</span>')
                                  }
                                 
                                },
                                failure: function(form, action) {
                                  Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
                                },
                                scope: this 
                            });
                        }
                     }
                }, {
                    xtype: 'textfield',
                    name: 'pronunciationName',
                    fieldLabel: 'ภาษาต่างประเทศอ่านเป็นภาษาไทยว่า',
                    labelAlign: 'top',
                    allowBlank: false,
                    afterLabelTextTpl: required
                },{
                    xtype: 'toolbar',
                    border: false,
                    padding: '6px 0 6px 0px',
                    items: [{
                        xtype: 'button',
                        text: 'ค้นหาชื่อใกล้เคียง',
                        action: 'ShowSearchNameSame'
                        
                    },
                    {
                        xtype: 'displayfield',
                        name: 'statusCheck',
                        clear: true
                    }
                 
                    ]
                }]  
            },{
                    xtype: 'tabpanel',
                    // width: 400,
                    // height: 200,
                    plain: true,
                    frame: false,
                    border: false,
                 
                   items: [
                        {
                            title: 'ข้อมูลผู้ประกอบการ',
                            xtype: 'business-registration-person-tab',
                                          
                        },
                        {
                            title: 'ที่ตั้งสำนักงาน',
                            layout: 'fit',
                            frame: false,
                            border: false,
                           
                            items:[
                                {
                                    xtype: 'business_registration_traderaddressgrid',
                                    border: false
                                 
                                }
                            ]
                        }
                        ,{
                            title: 'รายชื่อกรรมการ',
                            layout: 'anchor',
                            frame: false,
                            border: false,
                           
                            items:[
                                {
                                    xtype: 'business-registration-committee-committeegrid',
                                    anchor: '100%',
                                    border: false,
                                    frame: false
                                 
                                }, {
                                    xtype: 'splitter'
                                },
                                { 
                                  xtype: 'textareafield',
                                  name: 'committeeName2',
                                  anchor: '100%',
                                  fieldLabel: 'กรรมการผู้มีอำนาจลงนาม',
                                  labelWidth: 170
                                  //text: 'เพิ่มข้อมูลกรรการ',
                                  //action: 'addcommittee'
                                }
                            ]
                        }
                        ,{
                            title: 'ประเภทธุรกิจนำเที่ยว',
                            xtype: 'business-registration-tradercategory-tradercategoryform-tab',
                                          
                        }
                        ,{
                            title: 'สาขา',
                            layout: 'fit',
                            frame: false,
                            border: false,
                           
                            items:[
                                {
                                    xtype: 'business_registration_businessbranchgrid',
                                    border: false
                                 
                                }
                            ]
                        }
                        
                    ]
                }
        ],

            buttons: [{
                text: 'บันทึก',
                scope: this,
                action: 'saveBusinessRegistration'
            }] 
        });
        this.callParent();
    }
    ,onResetClick: function(){
        this.getForm().reset();
    }
    ,onCompleteClick: function(){
        var form = this.getForm();
        if (form.isValid()) {
            Ext.MessageBox.alert('Submitted Values', form.getValues(true));
        }
    }
    ,afterRender: function()
    {
        this.callParent(arguments);
        //add registration
        if(!Ext.isEmpty(this.registrationModel) )
        {
            this.loadRecord(this.registrationModel);
        }

        
    }
});