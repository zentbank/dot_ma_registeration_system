Ext.define('tourism.view.business.registration.BusinessRegistrationAddEditWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.business_registrationaddedit_window',

    requires: [
        'tourism.view.business.registration.BusinessRegistrationAddEditForm'
    ],

    title : 'การทำรายการธุรกิจนำเที่ยว',
    layout: 'fit',
    // autoShow: true,
    width: 750,
    height: 550,
    //True to make the window modal and mask everything behind it when displayed
    modal: true,
    // iconCls: 'icon-form',

    initComponent: function() {


        var formPanel = {
                    xtype: 'form-business-registration-addedit',
                    id: 'key-form-business-registration-addedit',
                    frame: false,
                    border: false,
                    registrationModel: this.registrationModel
                };

        if(this.roleAction != 'key')
        {
          formPanel = {
                    xtype: 'form-business-registration-addedit',
                    frame: false,
                    id: this.roleAction+'-'+this.traderType+'-registration-addedit',
                    border: false
                };
        }
      

        this.items = [
                formPanel 
        ];

        this.callParent(arguments);
    },
    loadDataModel: function(model)
    {

          
          var freg = this.down('form');
          freg.loadRecord(model);

          var imageFieldSet = this.down('image');
          imageFieldSet.setSrc("data:image/png;base64,"+model.get("imageFile"));
                        

          if(!Ext.isEmpty(model.get('amphurId')))
          {
            var amphurCombo = freg.getForm().findField('amphurId');
            amphurCombo.getStore().on('load',function(store, records, successful, eOpts){
              amphurCombo.setValue(model.get('amphurId'));
            },this,{single:true});
            amphurCombo.getStore().load();
          }
          else
          {
             var amphurCombo = freg.getForm().findField('amphurId');
             amphurCombo.clearValue();
          }

          if(!Ext.isEmpty(model.get('postfixId')))
          {
            var postfixIdCombo = freg.getForm().findField('postfixId');
            postfixIdCombo.getStore().on('load',function(store, records, successful, eOpts){
              postfixIdCombo.setValue(model.get('postfixId'));
            },this,{single:true});
            postfixIdCombo.getStore().load();
          }
          else
          {
            var postfixIdCombo = freg.getForm().findField('postfixId');
            postfixIdCombo.clearValue();
          }
          if(!Ext.isEmpty(model.get('taxProvinceId')))
          {
            var taxProvinceIdCombo = freg.getForm().findField('taxProvinceId');
            taxProvinceIdCombo.getStore().on('load',function(store, records, successful, eOpts){
              taxProvinceIdCombo.setValue(model.get('taxProvinceId'));
            },this,{single:true});
            taxProvinceIdCombo.getStore().load();
          }
          else
          {
            var taxProvinceIdCombo = freg.getForm().findField('taxProvinceId');
            taxProvinceIdCombo.clearValue();
          }
          if(!Ext.isEmpty(model.get('taxAmphurId')))
          {
            var taxAmphurIdCombo = freg.getForm().findField('taxAmphurId');
            taxAmphurIdCombo.getStore().on('load',function(store, records, successful, eOpts){
              taxAmphurIdCombo.setValue(model.get('taxAmphurId'));
            },this,{single:true});
            taxAmphurIdCombo.getStore().load();
          }
          else
          {
            var taxAmphurIdCombo = freg.getForm().findField('taxAmphurId');
            taxAmphurIdCombo.clearValue();
          }
          
          if(model.get('personType')  == 'I')
          {
            var individualFieldSet = freg.down('business-registration-person-individualpersonfieldset');
            individualFieldSet.expand();

            if(!Ext.isEmpty(model.get('provinceId')))
            {
              var provinceCombo = freg.getForm().findField('provinceId');
              provinceCombo.getStore().on('load',function(store, records, successful, eOpts){
                provinceCombo.setValue(model.get('provinceId'));
              },this,{single:true});
              provinceCombo.getStore().load();
            }
            else
            {
              var provinceCombo = freg.getForm().findField('provinceId');
              provinceCombo.clearValue();
            }

            if(!Ext.isEmpty(model.get('prefixId')))
            {
              var prefixIdCombo = freg.getForm().findField('prefixId');
              var prefixIdEnCombo = freg.getForm().findField('prefixIdEn');
              prefixIdCombo.getStore().on('load',function(store, records, successful, eOpts){
                prefixIdCombo.setValue(model.get('prefixId'));
                prefixIdEnCombo.setValue(model.get('prefixId'));
              },this,{single:true});
              prefixIdCombo.getStore().load();
            }
            else
            {
              var prefixIdCombo = freg.getForm().findField('prefixId');
              var prefixIdEnCombo = freg.getForm().findField('prefixIdEn');
              prefixIdCombo.clearValue();
              prefixIdEnCombo.clearValue();
            }
          }
          else
          {


            var corporateFieldSet = freg.down('business-registration-person-corporatepersonfieldset');
            corporateFieldSet.expand();

            if(!Ext.isEmpty(model.get('provinceId')))
            {
              var provinceCombo = freg.getForm().findField('provinceIdCorp');
              provinceCombo.getStore().on('load',function(store, records, successful, eOpts){
                provinceCombo.setValue(model.get('provinceId'));
              },this,{single:true});
              provinceCombo.getStore().load();
            }else
            {
              var provinceCombo = freg.getForm().findField('provinceIdCorp');
              provinceCombo.clearValue();
            }

            if(!Ext.isEmpty(model.get('prefixId')))
            {
              var prefixIdCombo = freg.getForm().findField('prefixIdCorp');
              prefixIdCombo.getStore().on('load',function(store, records, successful, eOpts){
                prefixIdCombo.setValue(model.get('prefixId'));
              },this,{single:true});
              prefixIdCombo.getStore().load();
            }else
            {
               var prefixIdCombo = freg.getForm().findField('prefixIdCorp');
              prefixIdCombo.clearValue();
            }
            if(!Ext.isEmpty(model.get('firstName')))
            {
              var committeeNameTxt = freg.getForm().findField('firstNameCorp');
               committeeNameTxt.setValue(model.get('firstName'));
            }
            if(!Ext.isEmpty(model.get('identityNo')))
            {
              var committeeIdentityNoCorpTxt = freg.getForm().findField('identityNoCorp');
               committeeIdentityNoCorpTxt.setValue(model.get('identityNo'));
            }
            if(!Ext.isEmpty(model.get('identityDate')))
            {
              var committeeIdentityDateCorpTxt = freg.getForm().findField('identityDateCorp');
               committeeIdentityDateCorpTxt.setValue(model.get('identityDate'));
            }       
          }

        

          var traderaddressgrid = Ext.ComponentQuery.query('business_registration_traderaddressgrid')[0];
         
          traderaddressgrid.getStore().load({
              params: {traderId: model.get('traderId'), recordStatus: model.get('traderRecordStatus')},
              callback: function(records, operation, success) {
                  // //console.log(success);
              },
              scope: this
          }); 
          var committeegrid = Ext.ComponentQuery.query('business-registration-committee-committeegrid')[0];
         
          committeegrid.getStore().load({
              params: {personId: model.get('personId'), recordStatus: model.get('traderRecordStatus')},
              callback: function(records, operation, success) {
                  // //console.log(success);
              },
              scope: this
          }); 


      

          var businessbranchgrid = Ext.ComponentQuery.query('business_registration_businessbranchgrid')[0];
         
          businessbranchgrid.getStore().load({
              params: {branchParentId: model.get('traderId'), recordStatus: model.get('traderRecordStatus')},
              callback: function(records, operation, success) {
                  // //console.log(success);
              },
              scope: this
          }); 

          var plantripgrid = Ext.ComponentQuery.query('business_registration_tradercategory_plantripgrid')[0];
         
          plantripgrid.getStore().load({
              params: {traderId: model.get('traderId'), recordStatus: model.get('traderRecordStatus')},
              callback: function(records, operation, success) {
                  // //console.log(success);
              },
              scope: this
          }); 


        
      
    }

});
