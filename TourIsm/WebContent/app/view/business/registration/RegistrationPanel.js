Ext.define('tourism.view.business.registration.RegistrationPanel', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border'
        // ,
        // 'tourism.view.business.registration.RegistrationFromSearch',
        // 'tourism.view.business.registration.RegistrationGrid'
    ],
    
    xtype: 'business-registrationPanel',
    layout: {
        type: 'border'
    },
    items: [{
        region: 'north',
        xtype: 'panel',
        // title:'ค้นหาการทำรายการธุรกิจนำเที่ยว',
        // collapsible: true,   // make collapsible
        // width: 120,
        frame: false,
        border: false,
        items:[{
            title:'ค้นหาการทำรายการธุรกิจนำเที่ยว',
            collapsible: true,   // make collapsible
             xtype: 'business_registrationfromsearch',
             id: 'business-registration-formsearch-key',
                  // padding: '5px 5px 0px 5px',
             frame: false,
             border: false


        }]
    },{
        region: 'center',
        xtype: 'panel',
        layout: 'fit',
        frame: false,
        border: false,
        items:[{
            title: 'การทำรายการใบอนุญาตธุรกิจนำเที่ยว',
            xtype: 'business_registrationgrid',
            id: 'business-registrationgrid',
            // padding: '0px 5px 5px 5px',
            autoScroll: true,
            // frame: false 
            border: false
            
        }]
    }]
});