Ext.define('tourism.view.business.registration.address.TraderAddressAddEditForm', {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.data.*',
        'Ext.form.*',
        'Ext.tab.Tab'
    ],
    xtype: 'business-registration-address-traderaddressaddedit-form',
    // title: 'ธุรกิจนำเที่ยว',
    bodyPadding: 5,
    autoScroll: true,

    initComponent: function(){

        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

        var provinceStore =  Ext.create('tourism.store.combo.MasProvinceStore',{
                                storeId:'business-registration-address-masprovince-store'
                            });

        var  tambolStore  = Ext.create('tourism.store.combo.MasTambolStore',{
                                storeId:'business-registration-address-tambol-store'
                            });
        var amphurStore =   Ext.create('tourism.store.combo.MasAmphurStore',{
                                storeId:'business-registration-address-masamphurEN-store'
                            });
        
        Ext.apply(this, {
            width: '100%',
            height: '100%',
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },

            items: 
            [ 
                {
                    xtype: 'container',
                    // title: 'Payment',
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%'
                    },
                    items: [
                        {
                            xtype: 'radiogroup',
                            fieldLabel: 'ประเภทที่ตั้งสำนักงาน',
                            labelWidth: 150,
                            allowBlank: false,
                            afterLabelTextTpl: required,
                            // Arrange checkboxes into two columns, distributed vertically
                            columns: 2,
                            vertical: true,
                            items: [
                            /*
                                O=office ที่ตั้งสำนักงาน
                                S=shop ที่ตั้งเฉพาะการ (ที่ขายของหน้าร้าน)
                                B=branch สาขาธุรกิจนำเที่ยว
                            */

                                { boxLabel: 'ที่ตั้งสำนักงาน',  name: 'addressType', inputValue: 'O' },
                                { boxLabel: 'ที่ตั้งเฉพาะการ',  name: 'addressType', inputValue: 'S' }
                                
                            ],
                            listeners: {
                                change: this.onSelectAddressType
                            }
                        },
                        {
                            xtype:'fieldset',
                            title: 'ที่ตั้งสำนักงาน (ภาษาไทย)',
                            layout: 'anchor',
                            defaults: {
                                anchor: '100%'
                            },
                            items:
                            [
                                {
                                    
                                   xtype: 'container',
                                    layout: 'hbox',
                                    layoutCongig: {
                                         pack:'center',
                                         align:'middle'
                                    },
                                    items: 
                                    [
                                        {
                                            // left hand side
                                            xtype: 'container',
                                            layout: 'anchor',
                                            flex: 1,
                                            defaults: {
                                                anchor: '100%'
                                            },
                                            items:
                                            [
                                                {
                                                    // ตั้งอยู่เลขที่
                                                    xtype: 'textfield',
                                                    name: 'addressNo',
                                                    fieldLabel: 'ตั้งอยู่เลขที่',
                                                    allowBlank: false,
                                                    afterLabelTextTpl: required,
                                                    tabIndex : 1 // เวลากด tab
                                                }// end  ตั้งอยู่เลขที่
                                                ,{
                                                    // ชั้น
                                                    xtype: 'textfield',
                                                    name: 'floor',
                                                    fieldLabel: 'ชั้น',
                                                    tabIndex : 3
                                                }// end  ชั้น
                                                ,{
                                                    // หมู่ที่
                                                    xtype: 'textfield',
                                                    name: 'moo',
                                                    fieldLabel: 'หมู่ที่',
                                                    tabIndex : 5
                                                }// end  หมู่ที่
                                                ,{
                                                    // ตรอก/ซอย
                                                    xtype: 'textfield',
                                                    name: 'soi',
                                                    fieldLabel: 'ตรอก/ซอย',
                                                    tabIndex : 7
                                                }// end  ตรอก/ซอย
                                                ,{
                                                    // จังหวัด (TH)
                                                    // xtype: 'combo',
                                                    xtype: 'masprovincecombo',
                                                    fieldLabel: 'จังหวัด',
                                                    id : 'business-registration-address-masprovince-combo',
                                                    store: provinceStore,
                                                    displayField: 'provinceName',
                                                    valueField: 'provinceId',
                                                    hiddenName: 'provinceId',
                                                    name: 'provinceId',
                                                    allowBlank: false,
                                                    afterLabelTextTpl: required,
                                                    masamphurComboId:'business-registration-address-masamphur-combo',
                                                    queryParam: 'provinceName',
                                                    tabIndex : 9
                                                    
                                                    
                                                }// end จังหวัด (TH)
                                                ,{
                                                     // ตำบล่
                                                    // xtype: 'combo',
                                                    xtype: 'mastambolcombo',
                                                    fieldLabel: 'ตำบล',
                                                    id : 'business-registration-address-tambol-combo',
                                                    store: tambolStore,
                                                    // queryMode: 'remote',
                                                    displayField: 'tambolName',
                                                    valueField: 'tambolId',
                                                    hiddenName: 'tambolId',
                                                    name: 'tambolId',
                                                    // triggerAction: 'all',
                                                    allowBlank: false,
                                                    afterLabelTextTpl: required,
                                                    queryParam: 'tambolName',
                                                    masamphurComboId: 'business-registration-address-masamphur-combo'
                                                    ,tabIndex : 11
                                                   
                                                }// end ตำบล่
                                                ,{
                                                    // โทรศัพท์
                                                    xtype: 'textfield',
                                                    name: 'telephone',
                                                    fieldLabel: 'โทรศัพท์',
                                                    // maxLength: 15,
                                                    //   minLength: 9,
                                                    //   enforceMaxLength: true,
                                                    //   maskRe: /[\d\-]/,
                                                    //   regex: /^\d{9}(\-\d{2,3}|\-\d{4})?$/,
                                                    tabIndex : 13
                                                }// end  โทรศัพท์
                                                ,{
                                                    // โทรสาร
                                                    xtype: 'textfield',
                                                    name: 'fax',
                                                    fieldLabel: 'โทรสาร',
                                                    // maxLength: 15,
                                                    //   minLength: 9,
                                                    //   enforceMaxLength: true,
                                                    //   maskRe: /[\d\-]/,
                                                    //   regex: /^\d{9}(\-\d{2,3}|\-\d{4})?$/,
                                                    tabIndex : 15
                                                }// end  โทรสาร
                                                ,{
                                                    // เวปไซด์
                                                    xtype: 'textfield',
                                                    name: 'website',
                                                    fieldLabel: 'เวปไซด์',
                                                    tabIndex : 17
                                                }// end  เวปไซด์
                                            ]
                                        }//end left hand side
                                        , {
                                            xtype: 'splitter'
                                        }
                                        ,{
                                            // right hand side
                                            xtype: 'container',
                                            layout: 'anchor',
                                            flex: 1,
                                            defaults: {
                                                anchor: '100%'
                                            },
                                            items:
                                            [
                                                {
                                                    // อาคาร
                                                    xtype: 'textfield',
                                                    name: 'buildingName',
                                                    fieldLabel: 'อาคาร',
                                                    tabIndex : 2
                                                }// end  อาคาร
                                                ,{
                                                    // ห้องเลขที่
                                                    xtype: 'textfield',
                                                    name: 'roomNo',
                                                    fieldLabel: 'ห้องเลขที่',
                                                    tabIndex : 4
                                                }// end  ห้องเลขที่
                                                ,{
                                                    // หมู่บ้าน
                                                    xtype: 'textfield',
                                                    name: 'villageName',
                                                    fieldLabel: 'หมู่บ้าน',
                                                    tabIndex : 6
                                                }// end  หมู่บ้าน
                                                ,{
                                                    // ถนน
                                                    xtype: 'textfield',
                                                    name: 'roadName',
                                                    fieldLabel: 'ถนน',
                                                    tabIndex : 8
                                                }// end  ถนน
                                                ,{
                                                     // อำเภอ/เขต
                                                    // xtype: 'combo',
                                                    xtype: 'masamphurcombo',
                                                    fieldLabel: 'อำเภอ/เขต',
                                                    id : 'business-registration-address-masamphur-combo',
                                                    store: amphurStore,
                                                    // queryMode: 'remote',
                                                    displayField: 'amphurName',
                                                    valueField: 'amphurId',
                                                    hiddenName: 'amphurId',
                                                    name: 'amphurId',
                                                    // triggerAction: 'all',
                                                    allowBlank: false,
                                                    afterLabelTextTpl: required,
                                                    queryParam: 'amphurName',
                                                    masprovinceComboId: 'business-registration-address-masprovince-combo',
                                                    mastambolComboId: 'business-registration-address-tambol-combo',
                                                    tabIndex : 10
                                                   
                                                }// end อำเภอ/เขต 
                                                ,{
                                                    // รหัสไปรษณี
                                                    xtype: 'textfield',
                                                    name: 'postCode',
                                                    fieldLabel: 'รหัสไปรษณ์',
                                                    allowBlank: false,
                                                    afterLabelTextTpl: required,
                                                    maxLength: 5,
                                                    minLength: 5,
                                                    enforceMaxLength: true,
                                                    maskRe: /\d/,
                                                    tabIndex : 12
                                                }// end รหัสไปรษณี
                                                ,{
                                                    // โทรศัพท์มือถือ
                                                    xtype: 'textfield',
                                                    name: 'mobileNo',
                                                    fieldLabel: 'โทรศัพท์มือถือ',
                                                    maxLength: 10,
                                                    minLength: 10,
                                                    enforceMaxLength: true,
                                                    maskRe: /\d/,
                                                    tabIndex : 14
                                                }// end  โทรศัพท์มือถือ
                                                ,{
                                                    // อีเมล์
                                                    xtype: 'textfield',
                                                    name: 'email',
                                                    fieldLabel: 'อีเมล์',
                                                    // allowBlank: false,
                                                    // afterLabelTextTpl: required,
                                                    tabIndex : 16,
                                                    vtype: 'email'
                                                }// end  อีเมล์

                                            ]
                                        } // end right hand side
                                    ]
                                }
                            ]
                                
                        },
                        {
                            xtype:'fieldset',
                            title: 'ที่ตั้งสำนักงาน (ภาษาอังกฤษ)',
                            layout: 'anchor',
                            defaults: {
                                anchor: '100%'
                            },
                            items:[
                            {
                                    
                                   xtype: 'container',
                                    layout: 'hbox',
                                    layoutCongig: {
                                         pack:'center',
                                         align:'middle'
                                    },
                                    items: 
                                    [
                                        {
                                            // left hand side
                                            xtype: 'container',
                                            layout: 'anchor',
                                            flex: 1,
                                            defaults: {
                                                anchor: '100%'
                                            },
                                            items:
                                            [
                                                {
                                                    // ตั้งอยู่เลขที่
                                                    xtype: 'textfield',
                                                    name: 'addressNoEn',
                                                    fieldLabel: 'No.',
                                                    allowBlank: false,
                                                    afterLabelTextTpl: required,
                                                    tabIndex : 18 // เวลากด tab
                                                }// end  ตั้งอยู่เลขที่
                                                ,{
                                                    // ชั้น
                                                    xtype: 'textfield',
                                                    name: 'floorEn',
                                                    fieldLabel: 'Floor',
                                                    tabIndex : 20
                                                }// end  ชั้น
                                                ,{
                                                    // หมู่ที่
                                                    xtype: 'textfield',
                                                    name: 'mooEn',
                                                    fieldLabel: 'Moo',
                                                    tabIndex : 22
                                                }// end  หมู่ที่
                                                ,{
                                                    // ตรอก/ซอย
                                                    xtype: 'textfield',
                                                    name: 'soiEn',
                                                    fieldLabel: 'Trok/Soi',
                                                    tabIndex : 24
                                                }// end  ตรอก/ซอย
                                                ,{
                                                    // จังหวัด (TH)
                                                    // xtype: 'combo',
                                                    xtype: 'masprovincecombo',
                                                    fieldLabel: 'Province',
                                                    // labelAlign: 'top',
                                                    id : 'business-registration-address-masprovinceEN-combo',
                                                    // store: 'tourism.store.combo.MasProvinceStore',
                                                    store: provinceStore,
                                                    displayField: 'provinceNameEn',
                                                    valueField: 'provinceId',
                                                    hiddenName: 'provinceIdEn',
                                                    name: 'provinceIdEn',
                                                    allowBlank: false,
                                                    afterLabelTextTpl: required,
                                                    masamphurComboId:'business-registration-address-masamphurEN-combo',
                                                    queryParam: 'provinceNameEn',
                                                    tabIndex : 26
                                                    
                                                }// end จังหวัด (TH)
                                                ,{
                                                     // ตำบล่
                                                    // xtype: 'combo',
                                                    xtype: 'mastambolcombo',
                                                    fieldLabel: 'Tambol/Khweng',
                                                    // labelAlign: 'top',
                                                    id : 'business-registration-address-tambolEN-combo',
                                                    // store: 'tourism.store.combo.MasTambolStore',
                                                    store: tambolStore,
                                                    // queryMode: 'remote',
                                                    displayField: 'tambolNameEn',
                                                    valueField: 'tambolId',
                                                    hiddenName: 'tambolIdEn',
                                                    name: 'tambolIdEn',
                                                    // triggerAction: 'all',
                                                    allowBlank: false,
                                                    afterLabelTextTpl: required,
                                                    queryParam: 'tambolNameEn',
                                                    masamphurComboId: 'business-registration-address-masamphurEN-combo',
                                                    tabIndex : 28
                                                   
                                                }// end ตำบล่
                                                ,{
                                                    // โทรศัพท์
                                                    xtype: 'textfield',
                                                    name: 'telephoneEn',
                                                    fieldLabel: 'Telephone',
                                                    // maxLength: 15,
                                                    //   minLength: 9,
                                                    //   enforceMaxLength: true,
                                                    //   maskRe: /[\d\-]/,
                                                    //  regex: /^\d{9}(\-\d{2,3}|\-\d{4})?$/,
                                                    tabIndex : 30
                                                }// end  โทรศัพท์
                                                ,{
                                                    // โทรสาร
                                                    xtype: 'textfield',
                                                    name: 'faxEn',
                                                    fieldLabel: 'Fax',
                                                    // maxLength: 15,
                                                    //   minLength: 9,
                                                    //   enforceMaxLength: true,
                                                    //   maskRe: /[\d\-]/,
                                                    //   regex: /^\d{9}(\-\d{2,3}|\-\d{4})?$/,
                                                    tabIndex : 32
                                                }// end  โทรสาร
                                                ,{
                                                    // เวปไซด์
                                                    xtype: 'textfield',
                                                    name: 'websiteEn',
                                                    fieldLabel: 'Website',
                                                    tabIndex : 34
                                                }// end  เวปไซด์
                                            ]
                                        }//end left hand side
                                        , {
                                            xtype: 'splitter'
                                        }
                                        ,{
                                            // right hand side
                                            xtype: 'container',
                                            layout: 'anchor',
                                            flex: 1,
                                            defaults: {
                                                anchor: '100%'
                                            },
                                            items:
                                            [
                                                {
                                                    // อาคาร
                                                    xtype: 'textfield',
                                                    name: 'buildingNameEn',
                                                    fieldLabel: 'Building',
                                                    tabIndex : 19
                                                }// end  อาคาร
                                                ,{
                                                    // ห้องเลขที่
                                                    xtype: 'textfield',
                                                    name: 'roomNoEn',
                                                    fieldLabel: 'Room',
                                                    tabIndex : 21
                                                }// end  ห้องเลขที่
                                                ,{
                                                    // หมู่บ้าน
                                                    xtype: 'textfield',
                                                    name: 'villageNameEn',
                                                    fieldLabel: 'Muban',
                                                    tabIndex : 23
                                                }// end  หมู่บ้าน
                                                ,{
                                                    // ถนน
                                                    xtype: 'textfield',
                                                    name: 'roadNameEn',
                                                    fieldLabel: 'Road',
                                                    tabIndex : 25
                                                }// end  ถนน
                                                ,{
                                                     // อำเภอ/เขต
                                                    // xtype: 'combo',
                                                    xtype: 'masamphurcombo',
                                                    fieldLabel: 'Amphur/Khet',
                                                    // labelAlign: 'top',
                                                    id : 'business-registration-address-masamphurEN-combo',
                                                    store: amphurStore,
                                                    // queryMode: 'remote',
                                                    displayField: 'amphurNameEn',
                                                    valueField: 'amphurId',
                                                    hiddenName: 'amphurIdEn',
                                                    name: 'amphurIdEn',
                                                    // triggerAction: 'all',
                                                    allowBlank: false,
                                                    afterLabelTextTpl: required,
                                                    queryParam: 'amphurNameEn',
                                                    masprovinceComboId: 'business-registration-address-masprovinceEN-combo',
                                                    mastambolComboId: 'business-registration-address-tambolEN-combo',
                                                    tabIndex : 27
                                                   
                                                }// end อำเภอ/เขต 
                                                ,{
                                                    // รหัสไปรษณี
                                                    xtype: 'textfield',
                                                    name: 'postCodeEn',
                                                    fieldLabel: 'Postcode',
                                                    allowBlank: false,
                                                    maxLength: 5,
                                                    minLength: 5,
                                                    enforceMaxLength: true,
                                                    maskRe: /\d/,
                                                    afterLabelTextTpl: required,
                                                    tabIndex : 29
                                                }// end รหัสไปรษณี
                                                ,{
                                                    // โทรศัพท์มือถือ
                                                    xtype: 'textfield',
                                                    name: 'mobileNoEn',
                                                    // maxLength: 10,
                                                    // minLength: 10,
                                                    // enforceMaxLength: true,
                                                    // maskRe: /\d/,
                                                    // fieldLabel: 'Mobile',
                                                    tabIndex : 31
                                                }// end  โทรศัพท์มือถือ
                                                ,{
                                                    // อีเมล์
                                                    xtype: 'textfield',
                                                    name: 'emailEn',
                                                    fieldLabel: 'Email',
                                                    // allowBlank: false,
                                                    // afterLabelTextTpl: required,
                                                    tabIndex : 33
                                                }// end  อีเมล์

                                            ]
                                        } // end right hand side
                                    ]
                                }
                            ]
                                
                        },
                        {
                            xtype: 'checkbox',
                            boxLabel  : 'คัดลอก ที่ตั้งเฉพาะการ',
                            name      : 'copyAddress',
                            inputValue: '1',
                            id        : 'business-registration-address-copyAddress-checkbox',
                            tabIndex : 35
                        }
                    ]
                    
                }
            ],

            buttons: [{
                text: 'บันทึก',
                scope: this,
                action: 'saveTraderAddress'
                // handler: this.onCompleteClick
            }, {
                text: 'ยกเลิก',
                // width: 150,
                scope: this,
                handler: this.onResetClick
            }]    
        });
        this.callParent(arguments);
    }
    ,afterRender: function()
    {
        this.callParent(arguments);

        var form = this;

        //set addressNoEN
        var addressNoTextField = form.getForm().findField('addressNo');
        addressNoTextField.addListener("blur" ,function(textf, eOpts )
        { 
            var values = textf.up('form').getValues();
            var addressNoEnTextfield = textf.up('form').getForm().findField('addressNoEn');

            if(!Ext.isEmpty(addressNoEnTextfield))
            {
                addressNoEnTextfield.setValue(values.addressNo);

            }

        },this);

        //set floorEn
        var floorTextField = form.getForm().findField('floor');
        floorTextField.addListener("blur" ,function(textf, eOpts )
        { 
            var values = textf.up('form').getValues();
            var floorEnTextfield = textf.up('form').getForm().findField('floorEn');

            if(!Ext.isEmpty(floorEnTextfield))
            {
                floorEnTextfield.setValue(values.floor);

            }

        },this);

        //set floorEn
        var roomNoTextField = form.getForm().findField('roomNo');
        roomNoTextField.addListener("blur" ,function(textf, eOpts )
        { 
            var values = textf.up('form').getValues();
            var roomNoEnTextfield = textf.up('form').getForm().findField('roomNoEn');

            if(!Ext.isEmpty(roomNoEnTextfield))
            {
                roomNoEnTextfield.setValue(values.roomNo);

            }

        },this);

        //set mooEn
        var mooTextField = form.getForm().findField('moo');
        mooTextField.addListener("blur" ,function(textf, eOpts )
        { 
            var values = textf.up('form').getValues();
            var mooEnTextfield = textf.up('form').getForm().findField('mooEn');

            if(!Ext.isEmpty(mooEnTextfield))
            {
                mooEnTextfield.setValue(values.moo);

            }

        },this);

        //set postCodeEn
        var postCodeTextField = form.getForm().findField('postCode');
        postCodeTextField.addListener("blur" ,function(textf, eOpts )
        { 
            var values = textf.up('form').getValues();
            var postCodeEnTextfield = textf.up('form').getForm().findField('postCodeEn');

            if(!Ext.isEmpty(postCodeEnTextfield))
            {
                postCodeEnTextfield.setValue(values.postCode);

            }

        },this);

        //set telephoneEn
        var telephoneTextField = form.getForm().findField('telephone');
        telephoneTextField.addListener("blur" ,function(textf, eOpts )
        { 
            var values = textf.up('form').getValues();
            var telephoneEnTextfield = textf.up('form').getForm().findField('telephoneEn');

            if(!Ext.isEmpty(telephoneEnTextfield))
            {
                telephoneEnTextfield.setValue(values.telephone);

            }

        },this);

        //set mobileNoEn
        var mobileNoTextField = form.getForm().findField('mobileNo');
        mobileNoTextField.addListener("blur" ,function(textf, eOpts )
        { 
            var values = textf.up('form').getValues();
            var mobileNoEnTextfield = textf.up('form').getForm().findField('mobileNoEn');

            if(!Ext.isEmpty(mobileNoEnTextfield))
            {
                mobileNoEnTextfield.setValue(values.mobileNo);

            }

        },this);

        //set mobileNoEn
        var faxTextField = form.getForm().findField('fax');
        faxTextField.addListener("blur" ,function(textf, eOpts )
        { 
            var values = textf.up('form').getValues();
            var faxEnTextfield = textf.up('form').getForm().findField('faxEn');

            if(!Ext.isEmpty(faxEnTextfield))
            {
                faxEnTextfield.setValue(values.fax);

            }

        },this);

        //set mobileNoEn
        var emailTextField = form.getForm().findField('email');
        emailTextField.addListener("blur" ,function(textf, eOpts )
        { 
            var values = textf.up('form').getValues();
            var emailEnTextfield = textf.up('form').getForm().findField('emailEn');

            if(!Ext.isEmpty(emailEnTextfield))
            {
                emailEnTextfield.setValue(values.email);

            }

        },this);

        //set websiteEn
        var websiteTextField = form.getForm().findField('website');
        websiteTextField.addListener("blur" ,function(textf, eOpts )
        { 
            var values = textf.up('form').getValues();
            var websiteEnTextfield = textf.up('form').getForm().findField('websiteEn');

            if(!Ext.isEmpty(websiteEnTextfield))
            {
                websiteEnTextfield.setValue(values.website);

            }

        },this);

        //set provinceIdEn value
        var masProvinceCombo = form.getForm().findField('provinceId');
        masProvinceCombo.addListener("blur" ,function(combo, eOpts )
        { 
            var values = combo.up('form').getValues();
            
            // //console.log(values);
            // var masprovinceENCombo = Ext.getCmp('business-registration-address-masprovinceEN-combo');

            var masprovinceENCombo = combo.up('form').getForm().findField('provinceIdEn');

            if(!Ext.isEmpty(masprovinceENCombo))
            {
                masprovinceENCombo.setValue(values.provinceId);

            }

        },this);

         //set amphurIdEn value
        var masAmphurCombo = form.getForm().findField('amphurId');
        masAmphurCombo.addListener("blur" ,function(combo, eOpts )
        { 
            var values = combo.up('form').getValues();
            
            var amphurIdEnCombo = combo.up('form').getForm().findField('amphurIdEn');

            if(!Ext.isEmpty(amphurIdEnCombo))
            {
                amphurIdEnCombo.setValue(values.amphurId);

            }

        },this);    

         //set tambolIdEn value
        var masTambolCombo = form.getForm().findField('tambolId');
        masTambolCombo.addListener("blur" ,function(combo, eOpts )
        { 
            var values = combo.up('form').getValues();
            
            var tambolIdEnCombo = combo.up('form').getForm().findField('tambolIdEn');

            if(!Ext.isEmpty(tambolIdEnCombo))
            {
                tambolIdEnCombo.setValue(values.tambolId);

            }

        },this);        
    }
    ,onResetClick: function(){
        this.up('window').close();
    },
    
    onCompleteClick: function(){
        var form = this.getForm();
        if (form.isValid()) {
            Ext.MessageBox.alert('Submitted Values', form.getValues(true));
        }
    },
    onSelectAddressType: function(radio, newValue, oldValue, eOpts)
    {
        
        var copyAddress = Ext.getCmp('business-registration-address-copyAddress-checkbox');
        
        /*
                        O=office ที่ตั้งสำนักงาน
                                S=shop ที่ตั้งเฉพาะการ (ที่ขายของหน้าร้าน)
                                B=branch สาขาธุรกิจนำเที่ยว*/
        if(newValue.addressType == "S")
        {
            copyAddress.setBoxLabel('คัดลอกไปยัง ที่ตั้งสำนักงาน');
            
        }
        else
        {
            copyAddress.setBoxLabel('คัดลอกไปยัง ที่ตั้งเฉพาะการ');
        }

    }
    // ,
    
    // onMailingAddrFieldChange: function(field){
    //     var copyToBilling = this.down('[name=billingSameAsMailing]').getValue(),
    //         copyField = this.down('[name=' + field.billingFieldName + ']');

    //     if (copyToBilling) {
    //         copyField.setValue(field.getValue());
    //     } else {
    //         copyField.clearInvalid();
    //     }
    // },
    
    /**
     * Enables or disables the billing address fields according to whether the checkbox is checked.
     * In addition to disabling the fields, they are animated to a low opacity so they don't take
     * up visual attention.
     */
    // onSameAddressChange: function(box, checked){
    //     var fieldset = box.ownerCt;
    //     Ext.Array.forEach(fieldset.previousSibling().query('textfield'), this.onMailingAddrFieldChange, this);
    //     Ext.Array.forEach(fieldset.query('textfield'), function(field) {
    //         field.setDisabled(checked);
    //         // Animate the opacity on each field. Would be more efficient to wrap them in a container
    //         // and animate the opacity on just the single container element, but IE has a bug where
    //         // the alpha filter does not get applied on position:relative children.
    //         // This must only be applied when it is not IE6, as it has issues with opacity when cleartype
    //         // is enabled
    //         if (!Ext.isIE6) {
    //             field.el.animate({opacity: checked ? 0.3 : 1});
    //         }
    //     });
    // }
});