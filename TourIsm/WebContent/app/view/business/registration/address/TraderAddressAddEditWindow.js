Ext.define('tourism.view.business.registration.address.TraderAddressAddEditWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.business_registration_address_addedit_window',

    requires: [
        // 'tourism.view.business.registration.BusinessRegistrationAddEditForm'
    ],

    title : 'เพิ่มแก้ไขข้อมุลที่ตั้งสำนักงาน',
    layout: 'fit',
    autoShow: true,
    width: 600,
    height: 600,
    //True to make the window modal and mask everything behind it when displayed
    modal: true,
    
    // iconCls: 'icon-form',

    initComponent: function() {

        this.items = [
            {
                xtype: 'business-registration-address-traderaddressaddedit-form',
                frame: false,
                border: false
            }
            
        ];

        this.callParent(arguments);

        // this.on('close',function(){alert('close');});
        // this.on('destroy',function(){
            
        //     // Ext.data.StoreManager.removeAtKey('tourism.store.business.registration.address.TraderAddressStore');

        //     Ext.data.StoreManager.each(function(item){
        //         //console.log(item);
        //     });
        // });
    },

});
