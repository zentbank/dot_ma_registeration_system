Ext.define('tourism.view.business.registration.address.TraderAddressGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.business_registration_traderaddressgrid',
		
		stripeRows: true,
		requires: [
			'Ext.grid.Column',
			// 'tourism.store.business.registration.address.TraderAddressStore',
			'Ext.toolbar.Paging',
			'Ext.grid.plugin.RowEditing',
			'Ext.grid.column.Template',
			'Ext.grid.column.Action',
			'Ext.selection.CheckboxModel'
			// ,
			,'tourism.store.business.registration.address.TraderAddressStore'
			,'tourism.view.business.registration.address.TraderAddressAddEditWindow'
        	// ,'tourism.view.business.registration.address.TraderAddressAddEditForm'


		],
		initComponent: function(){

			// create every new form 
			var store = Ext.create('tourism.store.business.registration.address.TraderAddressStore',{
				storeId: 'business-registration-traderaddressstore'
			});

			Ext.apply(this, {
				store: store,
				// selType: 'checkboxmodel',
				columns: [
					{
						header: 'addressId',
						dataIndex: 'addressId',
						hidden: true
					},
					{
						header: 'ประเภทที่ตั้ง',
						dataIndex: 'addressType',
						flex: 1,
						renderer: function(value, metaData, model){
							if (!value) {
							    return "";
							}
							        /*
                                O=office ที่ตั้งสำนักงาน
                                S=shop ที่ตั้งเฉพาะการ (ที่ขายของหน้าร้าน)
                                B=branch สาขาธุรกิจนำเที่ยว
                            */
                            
							if(value == "S")
							{
								return 'ที่ตั้งเฉพาะการ';
							}
							if(value == "O")
							{
								return 'ที่ตั้งสำนักงาน';
							}
							
						}
					},
					{
						header: 'เลขที่',
						dataIndex: 'addressNo',
						flex: 1
					},
					{
						header: 'ตำบล',
						dataIndex: 'tambolName',
						flex: 1
					},
			        {
						header: 'อำเภอ',
						dataIndex: 'amphurName',
						flex: 1
					},
					{
						header: 'จังหวัด',
						dataIndex: 'provinceName',
						flex: 1
					},
			        {
			        	header: '',
			            xtype: 'actioncolumn',
			            width: 50,
			            align : 'center',
			           	// flex: 1,
			            items: [{
			                iconCls: 'icon-edit-xsmall',
			                tooltip: 'รายละเอียด',
				            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
				                this.fireEvent('itemclick', this, 'edittraderaddress', grid, rowIndex, colIndex, record, node);
				            }
			            }]
			        },
			        {
			        	header: '',
			            xtype: 'actioncolumn',
			           	// flex: 1,
			           	width: 50,
			           	align : 'center',
			            items: [{
			                iconCls: 'icon-delete-xsmall',
			                tooltip: 'ลบ',
				            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
				                this.fireEvent('itemclick', this, 'deletetraderaddress', grid, rowIndex, colIndex, record, node);
				            }
			            }]
			        }
				]
				,dockedItems: [{
				    xtype: 'toolbar',
				    dock: 'top',
				    // ui: 'footer',
				    // defaults: {minWidth: minButtonWidth},
				    items: [
				        { xtype: 'component', flex: 1 },
				        { 
				        	xtype: 'button',
				        	text: 'เพิ่มที่ตั้งสำนักงาน',
				        	action: 'addTraderAddress'
				        }
				        
				    ]
				}]
			});

			this.callParent();
		}
	});