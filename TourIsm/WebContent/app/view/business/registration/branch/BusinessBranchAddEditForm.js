Ext.define('tourism.view.business.registration.branch.BusinessBranchAddEditForm', {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.data.*',
        'Ext.form.*',
        'Ext.tab.Tab'
    ],
    xtype: 'business-registration-branch-branchaddedit-form',
    // title: 'ธุรกิจนำเที่ยว',
    bodyPadding: 5,
    autoScroll: true,

    initComponent: function(){

        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

        var provinceStore =  Ext.create('tourism.store.combo.MasProvinceStore',{
                                storeId:'business-registration-branch-masprovince-store'
                            });

        var  tambolStore  = Ext.create('tourism.store.combo.MasTambolStore',{
                                storeId:'business-registration-branch-tambol-store'
                            });
        var amphurStore =   Ext.create('tourism.store.combo.MasAmphurStore',{
                                storeId:'business-registration-branch-masamphurEN-store'
                            });
        
        Ext.apply(this, {
            width: '100%',
            height: '100%',
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },

            items: 
            [ 
                {
                    xtype: 'container',
                    // title: 'Payment',
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%'
                    },
                    items: [
                        {
                            xtype: 'hiddenfield',
                            name: 'branchType',
                            value: '1'
                        },
                        {
                            xtype: 'hiddenfield',
                            name: 'addressType',
                            value: 'S'
                        }
                        ,{
                            // สาขาที่
                            xtype: 'textfield',
                            name: 'traderBranchNo',
                            fieldLabel: 'สาขาที่',
                            allowBlank: false,
                            anchor: '49%',
                            afterLabelTextTpl: required,
                            tabIndex : 1 // เวลากด tab
                        }// end  สาขาที่
                        ,{
                                    
                                   xtype: 'container',
                                    layout: 'hbox',
                                    layoutCongig: {
                                         pack:'center',
                                         align:'middle'
                                    },
                                    items: 
                                    [
                                        {
                                            // left hand side
                                            xtype: 'container',
                                            layout: 'anchor',
                                            flex: 1,
                                            defaults: {
                                                anchor: '100%'
                                            },
                                            items:
                                            [
                                                {
                                                    // ตั้งอยู่เลขที่
                                                    xtype: 'textfield',
                                                    name: 'addressNo',
                                                    fieldLabel: 'ตั้งอยู่เลขที่',
                                                    allowBlank: false,
                                                    afterLabelTextTpl: required,
                                                    tabIndex : 2 // เวลากด tab
                                                }// end  ตั้งอยู่เลขที่
                                                ,{
                                                    // ชั้น
                                                    xtype: 'textfield',
                                                    name: 'floor',
                                                    fieldLabel: 'ชั้น',
                                                    tabIndex : 4
                                                }// end  ชั้น
                                                ,{
                                                    // หมู่ที่
                                                    xtype: 'textfield',
                                                    name: 'moo',
                                                    fieldLabel: 'หมู่ที่',
                                                    tabIndex : 6
                                                }// end  หมู่ที่
                                                ,{
                                                    // ตรอก/ซอย
                                                    xtype: 'textfield',
                                                    name: 'soi',
                                                    fieldLabel: 'ตรอก/ซอย',
                                                    tabIndex : 8
                                                }// end  ตรอก/ซอย
                                                ,{
                                                    // จังหวัด (TH)
                                                    // xtype: 'combo',
                                                    xtype: 'masprovincecombo',
                                                    fieldLabel: 'จังหวัด',
                                                    id : 'business-registration-branch-masprovince-combo',
                                                    store: provinceStore,
                                                    displayField: 'provinceName',
                                                    valueField: 'provinceId',
                                                    hiddenName: 'provinceId',
                                                    name: 'provinceId',
                                                    allowBlank: false,
                                                    afterLabelTextTpl: required,
                                                    masamphurComboId:'business-registration-branch-masamphur-combo',
                                                    queryParam: 'provinceName',
                                                    tabIndex : 10
                                                  
                                                    
                                                }// end จังหวัด (TH)
                                                ,{
                                                     // ตำบล่
                                                    // xtype: 'combo',
                                                    xtype: 'mastambolcombo',
                                                    fieldLabel: 'ตำบล',
                                                    id : 'business-registration-branch-tambol-combo',
                                                    store: tambolStore,
                                                    // queryMode: 'remote',
                                                    displayField: 'tambolName',
                                                    valueField: 'tambolId',
                                                    hiddenName: 'tambolId',
                                                    name: 'tambolId',
                                                    // triggerAction: 'all',
                                                    allowBlank: false,
                                                    afterLabelTextTpl: required,
                                                    queryParam: 'tambolName',
                                                    masamphurComboId: 'business-registration-branch-masamphur-combo'
                                                    ,tabIndex : 12
                                                   
                                                }// end ตำบล่
                                                ,{
                                                    // โทรศัพท์
                                                    xtype: 'textfield',
                                                    name: 'telephone',
                                                    fieldLabel: 'โทรศัพท์',
                                                    tabIndex : 14
                                                }// end  โทรศัพท์
                                                ,{
                                                    // โทรสาร
                                                    xtype: 'textfield',
                                                    name: 'fax',
                                                    fieldLabel: 'โทรสาร',
                                                    tabIndex : 16
                                                }// end  โทรสาร
                                                ,{
                                                    // เวปไซด์
                                                    xtype: 'textfield',
                                                    name: 'website',
                                                    fieldLabel: 'เวปไซด์',
                                                    tabIndex : 18
                                                }// end  เวปไซด์
                                            ]
                                        }//end left hand side
                                        , {
                                            xtype: 'splitter'
                                        }
                                        ,{
                                            // right hand side
                                            xtype: 'container',
                                            layout: 'anchor',
                                            flex: 1,
                                            defaults: {
                                                anchor: '100%'
                                            },
                                            items:
                                            [
                                                
                                                {
                                                    // อาคาร
                                                    xtype: 'textfield',
                                                    name: 'buildingName',
                                                    fieldLabel: 'อาคาร',
                                                    tabIndex : 3
                                                }// end  อาคาร
                                                ,{
                                                    // ห้องเลขที่
                                                    xtype: 'textfield',
                                                    name: 'roomNo',
                                                    fieldLabel: 'ห้องเลขที่',
                                                    tabIndex : 5
                                                }// end  ห้องเลขที่
                                                ,{
                                                    // หมู่บ้าน
                                                    xtype: 'textfield',
                                                    name: 'villageName',
                                                    fieldLabel: 'หมู่บ้าน',
                                                    tabIndex : 7
                                                }// end  หมู่บ้าน
                                                ,{
                                                    // ถนน
                                                    xtype: 'textfield',
                                                    name: 'roadName',
                                                    fieldLabel: 'ถนน',
                                                    tabIndex : 9
                                                }// end  ถนน
                                                ,{
                                                     // อำเภอ/เขต
                                                    // xtype: 'combo',
                                                    xtype: 'masamphurcombo',
                                                    fieldLabel: 'อำเภอ/เขต',
                                                    id : 'business-registration-branch-masamphur-combo',
                                                    store: amphurStore,
                                                    // queryMode: 'remote',
                                                    displayField: 'amphurName',
                                                    valueField: 'amphurId',
                                                    hiddenName: 'amphurId',
                                                    name: 'amphurId',
                                                    // triggerAction: 'all',
                                                    allowBlank: false,
                                                    afterLabelTextTpl: required,
                                                    queryParam: 'amphurName',
                                                    masprovinceComboId: 'business-registration-branch-masprovince-combo',
                                                    mastambolComboId: 'business-registration-branch-tambol-combo',
                                                    tabIndex : 11
                                                   
                                                }// end อำเภอ/เขต 
                                                ,{
                                                    // รหัสไปรษณี
                                                    xtype: 'textfield',
                                                    name: 'postCode',
                                                    fieldLabel: 'รหัสไปรษณี',
                                                    allowBlank: false,
                                                    afterLabelTextTpl: required,
                                                    tabIndex : 13
                                                }// end รหัสไปรษณี
                                                ,{
                                                    // โทรศัพท์มือถือ
                                                    xtype: 'textfield',
                                                    name: 'mobileNo',
                                                    fieldLabel: 'โทรศัพท์มือถือ',
                                                    tabIndex : 15
                                                }// end  โทรศัพท์มือถือ
                                                ,{
                                                    // อีเมล์
                                                    xtype: 'textfield',
                                                    name: 'email',
                                                    fieldLabel: 'อีเมล์',
                                                    // allowBlank: false,
                                                    // afterLabelTextTpl: required,
                                                    tabIndex : 17,
                                                    vtype: 'email'
                                                }// end  อีเมล์

                                            ]
                                        } // end right hand side
                                    ]
                                }
                    ]
                    
                }
            ],

            buttons: [{
                text: 'บันทึก',
                scope: this,
                action: 'saveBranch'
            }, {
                text: 'ยกเลิก',
                // width: 150,
                scope: this,
                handler: this.onResetClick
            }]    
        });
        this.callParent(arguments);
    }
    ,onResetClick: function(){
         this.up('window').close();
    }
});