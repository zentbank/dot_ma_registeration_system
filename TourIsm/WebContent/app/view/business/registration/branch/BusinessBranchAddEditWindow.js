Ext.define('tourism.view.business.registration.branch.BusinessBranchAddEditWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.business_registration_branch_addedit_window',

    requires: [
        'tourism.view.business.registration.branch.BusinessBranchAddEditForm'
    ],

    title : 'เพิ่มแก้ไขข้อมุลที่ตั้งสำนักงาน',
    layout: 'fit',
    autoShow: true,
    width: 600,
    // height: 500,
    //True to make the window modal and mask everything behind it when displayed
    modal: true,
    
    // iconCls: 'icon-form',

    initComponent: function() {

        this.items = [
            {
                xtype: 'business-registration-branch-branchaddedit-form',
                frame: false,
                border: false
            }
            
        ];

        this.callParent(arguments);
    },

});
