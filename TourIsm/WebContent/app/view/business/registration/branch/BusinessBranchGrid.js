Ext.define('tourism.view.business.registration.branch.BusinessBranchGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.business_registration_businessbranchgrid',
		
		stripeRows: true,
		requires: [
			'Ext.grid.Column',
			'tourism.store.business.registration.branch.BranchStore',
			'Ext.toolbar.Paging',
			'Ext.grid.plugin.RowEditing',
			'Ext.grid.column.Template',
			'Ext.grid.column.Action',
			'tourism.view.business.registration.branch.BusinessBranchAddEditWindow'
		],
		initComponent: function(){

			// create every new form 
			var store = Ext.create('tourism.store.business.registration.branch.BranchStore',{
				storeId: 'business-registration-branchstore'
			});

			Ext.apply(this, {
				store: store,

				columns: [
					{
						header: 'addressId',
						dataIndex: 'addressId',
						hidden: true
					},
					{
						header: 'สาขาที่',
						dataIndex: 'traderBranchNo',
						flex: 1
					},
					{
						header: 'โทรศัพท์',
						dataIndex: 'telephone',
						flex: 1
					},
					{
						header: 'ตำบล',
						dataIndex: 'tambolName',
						flex: 1
					},
			        {
						header: 'อำเภอ',
						dataIndex: 'amphurName',
						flex: 1
					},
					{
						header: 'จังหวัด',
						dataIndex: 'provinceName',
						flex: 1
					},
			        {
			        	header: '',
			            xtype: 'actioncolumn',
			           	width: 50,
			           	align : 'center',
			            items: [{
			                iconCls: 'icon-edit-xsmall',
			                tooltip: 'แก้ไข',
				            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
				                this.fireEvent('itemclick', this, 'editbranch', grid, rowIndex, colIndex, record, node);
				            }
			            }]
			        },
			        {
			        	header: '',
			            xtype: 'actioncolumn',
			           	width: 50,
			           	align : 'center',
			            items: [{
			                iconCls: 'icon-delete-xsmall',
			                tooltip: 'ลบ',
				            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
				                this.fireEvent('itemclick', this, 'deletebranch', grid, rowIndex, colIndex, record, node);
				            }
			            }]
			        }
				]
				,dockedItems: [{
				    xtype: 'toolbar',
				    dock: 'top',
				    // ui: 'footer',
				    // defaults: {minWidth: minButtonWidth},
				    items: [
				        { xtype: 'component', flex: 1 },
				        { 
				        	xtype: 'button',
				        	text: 'เพิ่มข้อมูลสาขา',
				        	action: 'addBranch'
				        	
				        }
				    ]
				}]
			});

			this.callParent();
		}
	});