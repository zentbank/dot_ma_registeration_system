Ext.define('tourism.view.business.registration.committee.CommiteeAddEditForm', {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.data.*',
        'Ext.form.*',
        'Ext.tab.Tab'
        // ,
        // 'tourism.view.business.registration.committee.CorporateCommitteeFieldSet',
        // 'tourism.view.business.registration.committee.IndividualCommitteeFieldSet'
    ],
    xtype: 'business-registration-committee-committeeaddedit-form',
    // title: 'ธุรกิจนำเที่ยว',
    bodyPadding: 5,
    autoScroll: true,

    initComponent: function(){

        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

        var masprefixstore =  Ext.create('tourism.store.combo.MasPrefixStore',{
                                storeId:'business-registration-committee-masprefixstore-store'
                            });
        
        Ext.apply(this, {
            width: '100%',
            height: '100%',
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },

            items: 
            [
                {
                    xtype: 'hiddenfield',
                    name: 'committeeId'
                }
                ,{
                    xtype: 'hiddenfield',
                    name: 'regId'
                },{
                    xtype: 'hiddenfield',
                    name: 'traderId'
                },{
                    xtype: 'hiddenfield',
                    name: 'personId'
                }
                ,{
                    xtype: 'business-registration-committee-individualCommitteeFieldSet'
                }
                ,{
                    xtype: 'business-registration-committee-corporateCommitteeFieldSet'
                }
                ,{
                    xtype: 'checkbox',
                    boxLabel  : 'กรรมการผู้มีอำนาจลงนาม',
                    name      : 'committeeType',
                    inputValue: 'S',
                    id        : 'business-registration-address-committeeType-checkbox',
                    tabIndex : 25
                }
            ],

            buttons: [{
                text: 'บันทึก',
                scope: this,
                action: 'saveCommittee'
                // handler: this.onCompleteClick
            }, {
                text: 'ยกเลิก',
                // width: 150,
                scope: this,
                handler: this.onResetClick
            }]    
        });
        this.callParent(arguments);
    }
    ,afterRender: function()
    {
        this.callParent(arguments);

        var prefixCombo = this.getForm().findField('prefixId');
        prefixCombo.addListener("blur" ,function(combo, eOpts )
        { 
            var prefixId = combo.getValue();


            var prefixIdEnCombo = combo.up('form').getForm().findField('prefixIdEn');

            if(!Ext.isEmpty(prefixIdEnCombo))
            {
                prefixIdEnCombo.setValue(prefixId);

            }

        },this);

        var form = this;
                   

        var individualFieldSet = this.down('business-registration-committee-individualCommitteeFieldSet');

        individualFieldSet.addListener('expand', function( f, eOpts ){
                                    // this = form
            var corporateFieldSet = this.down('business-registration-committee-corporateCommitteeFieldSet');
            
            var prefixIdCorp = this.getForm().findField('prefixIdCorp');
            prefixIdCorp.allowBlank =  true;

            var committeeNameCorp = this.getForm().findField('committeeNameCorp');
            committeeNameCorp.allowBlank =  true;

            var committeeIdentityNoCorp = this.getForm().findField('committeeIdentityNoCorp');
            committeeIdentityNoCorp.allowBlank =  true;

            var corporateType = this.getForm().findField('corporateType');
            corporateType.allowBlank =  true;

            //-----------

            var committeeIdentityNo = this.getForm().findField('committeeIdentityNo');
            committeeIdentityNo.allowBlank =  false;

            var prefixId = this.getForm().findField('prefixId');
            prefixId.allowBlank =  false;

            var committeeName = this.getForm().findField('committeeName');
            committeeName.allowBlank =  false;

            var prefixIdEn = this.getForm().findField('prefixIdEn');
            prefixIdEn.allowBlank =  false;

            var committeeNameEn = this.getForm().findField('committeeNameEn');
            committeeNameEn.allowBlank =  false;

            var birthDate = this.getForm().findField('birthDate');
            birthDate.allowBlank =  false;

            var committeeLastname = this.getForm().findField('committeeLastname');
            committeeLastname.allowBlank =  false;

            var committeeLastnameEn = this.getForm().findField('committeeLastnameEn');
            committeeLastnameEn.allowBlank =  false;

            var committeeNationality = this.getForm().findField('committeeNationality');
            committeeNationality.allowBlank =  false;
                       
            corporateFieldSet.collapse();

        },this);

        var corporateFieldSet = this.down('business-registration-committee-corporateCommitteeFieldSet');

        corporateFieldSet.addListener('expand', function( f, eOpts ){

            var individualFieldSet = this.down('business-registration-committee-individualCommitteeFieldSet');

            var committeeIdentityNo = this.getForm().findField('committeeIdentityNo');
            committeeIdentityNo.allowBlank =  true;

            var prefixId = this.getForm().findField('prefixId');
            prefixId.allowBlank =  true;

            var committeeName = this.getForm().findField('committeeName');
            committeeName.allowBlank =  true;

            var prefixIdEn = this.getForm().findField('prefixIdEn');
            prefixIdEn.allowBlank =  true;

            var committeeNameEn = this.getForm().findField('committeeNameEn');
            committeeNameEn.allowBlank =  true;

            var birthDate = this.getForm().findField('birthDate');
            birthDate.allowBlank =  true;

            var committeeLastname = this.getForm().findField('committeeLastname');
            committeeLastname.allowBlank =  true;

            var committeeLastnameEn = this.getForm().findField('committeeLastnameEn');
            committeeLastnameEn.allowBlank =  true;

            var committeeNationality = this.getForm().findField('committeeNationality');
            committeeNationality.allowBlank =  true;


            //----------

            var prefixIdCorp = this.getForm().findField('prefixIdCorp');
            prefixIdCorp.allowBlank =  false;

            var committeeNameCorp = this.getForm().findField('committeeNameCorp');
            committeeNameCorp.allowBlank =  false;

            var committeeIdentityNoCorp = this.getForm().findField('committeeIdentityNoCorp');
            committeeIdentityNoCorp.allowBlank =  false;

            var corporateType = this.getForm().findField('corporateType');
            corporateType.allowBlank =  false;

            individualFieldSet.collapse();
        },this);

        corporateFieldSet.setVisible(false);
        
    }
    ,onResetClick: function(){
         this.up('window').close();
    }
});