Ext.define('tourism.view.business.registration.committee.CommiteeAddEditWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.business-registration-committee-addedit-window',

    requires: [
        // 'tourism.view.business.registration.BusinessRegistrationAddEditForm'
    ],

    title : 'เพิ่มแก้ไขข้อมุลกรรมการบริษัท',
    layout: 'fit',
    autoShow: true,
    width: 700,
    height: 500,
    //True to make the window modal and mask everything behind it when displayed
    modal: true,
    
    // iconCls: 'icon-form',

    initComponent: function() {

        this.items = [
            {
                xtype: 'business-registration-committee-committeeaddedit-form',
                frame: false,
                border: false
            }
            
        ];

        this.callParent(arguments);
    },

});
