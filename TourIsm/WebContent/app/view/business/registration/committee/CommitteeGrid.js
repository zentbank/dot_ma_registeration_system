Ext.define('tourism.view.business.registration.committee.CommitteeGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.business-registration-committee-committeegrid',
		
		stripeRows: true,
		requires: [
			'Ext.grid.Column',
			'Ext.toolbar.Paging',
			'Ext.grid.plugin.RowEditing',
			'Ext.grid.column.Template',
			'Ext.grid.column.Action',
			'Ext.grid.column.RowNumberer',
			
			'tourism.store.business.registration.committee.CommitteeStore',
			'tourism.view.business.registration.committee.CorporateCommitteeFieldSet',
        	'tourism.view.business.registration.committee.IndividualCommitteeFieldSet',
			'tourism.view.business.registration.BusinessRegistrationAddEditForm',
			'tourism.view.business.registration.committee.CommiteeAddEditWindow'
		],
		initComponent: function(){
			var normal = "ปกติ";
			// create every new form 
			var store = Ext.create('tourism.store.business.registration.committee.CommitteeStore',{
				storeId: 'business-registration-committee-committeeStore'
			});

			Ext.apply(this, {
				store: store,

				columns: [
					{
						header: 'committeeId',
						dataIndex: 'committeeId',
						hidden: true
					},
					{xtype: 'rownumberer'},
					{
						header: 'สถานะ',
						//dataIndex: 'committeeStatus',
						//flex: 1,
						xtype: 'actioncolumn',
						width: 50,
			            align : 'center',
			           	// flex: 1,
			            items: [{
			            	getClass: function(v, meta, rec) {          
			                    if (rec.get('committeeStatus') == 'N') {
			                        this.items[0].tooltip = 'ปกติ';
			                        return 'icon-checked-xsmall';
			                    }else if (rec.get('committeeStatus') == 'R') {
									this.items[0].tooltip = 'อยู่ระหว่างยกเลิก';
			                        return 'icon-security-xsmall';
			                    }  else if (rec.get('committeeStatus') == 'S') {
			                        this.items[0].tooltip = 'อยู่ระหว่างพักใช้';
			                        return 'icon-security-xsmall';
			                    }
			                },
			                handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
				                this.fireEvent('itemclick', this, 'businessprogress', grid, rowIndex, colIndex, record, node);

				                
				            }
			            }]
					},					
					{
						header: 'ชื่อ-นามสกุล',
						dataIndex: 'committeeName',
						flex: 3,
						renderer: function(value, metaData, model){
							
							var name =  model.get('prefixName') + "" + model.get('committeeName');
							var listName =  model.get('committeeLastname');

							if(model.get('committeePersonType') == 'C')
							{
								listName = listName + model.get('postfixName')
							}
							
							return name +' '+listName;
							
						}
					},
					{
						header: 'ประเภทบุคคล',
						dataIndex: 'committeePersonType',
						flex: 1,
						renderer: function(value, metaData, model){
							
							if (!value) {
							    return "";
							}
							/*
                           I=Individual บุคคลธรรมดา
							C=Corporation นิติบุคคล
                            */
							if(value == 'I')
							{
								return 'บุคคลธรรมดา';
							}
							if(value == 'C')
							{
								return 'นิติบุคคล';
							}
							
						}
					},
					{
						header: 'ประเภทกรรมการ',
						dataIndex: 'committeeType',
						flex: 2,
						renderer: function(value, metaData, model){
							
							if (!value) {
							    return "";
							}
							/*
                         		ประเภทกรรมการบริษัท
								C=committee กรรมการบริษัท
								S=sign กรรมการผู้มีอำนาจลงนาม
                            */
							if(value == 'C')
							{
								return 'กรรมการบริษัท';
							}
							if(value == 'S')
							{
								return 'กรรมการผู้มีอำนาจลงนาม';
							}
							
						}
					},	
			        {
			        	header: '',
			            xtype: 'actioncolumn',
			            width: 50,
			            align : 'center',
			           	// flex: 1,
			            items: [{
			                iconCls: 'icon-edit-xsmall',
			                tooltip: 'รายละเอียด',
				            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
				                this.fireEvent('itemclick', this, 'editcommitte', grid, rowIndex, colIndex, record, node);
				            }
			            }]
			        },
			        {
			        	header: '',
			            xtype: 'actioncolumn',
			            align : 'center',
			           	// flex: 1,
			           	width: 50,
			            items: [{
			                iconCls: 'icon-delete-xsmall',
			                tooltip: 'ลบ',
				            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
				                this.fireEvent('itemclick', this, 'deletecommitte', grid, rowIndex, colIndex, record, node);
				            }
			            }]
			        }
				]
				,dockedItems: [{
				    xtype: 'toolbar',
				    dock: 'top',
				    // ui: 'footer',
				    // defaults: {minWidth: minButtonWidth},
				    items: [
				        { xtype: 'component', flex: 1 },
				        { 
				        	xtype: 'button',
				        	text: 'เพิ่มข้อมูลกรรการ',
				        	action: 'addcommittee'
				        }
				    ]
				}
				// ,
				// {
				//     xtype: 'toolbar',
				//     dock: 'bottom',
				//     // ui: 'footer',
				//     // defaults: {minWidth: minButtonWidth},
				//     items: [
				        
				//         { 
				//         	xtype: 'textareafield',
				//         	name: 'committeeName2',
				//         	width: 500,
				//         	height: 100,
				//         	fieldLabel: 'กรรมการผู้มีอำนาจลงนาม',
				//         	labelWidth: 170
				//         	//text: 'เพิ่มข้อมูลกรรการ',
				//         	//action: 'addcommittee'
				//         }
				//     ]
				// }
				]
			});

			this.callParent();
		}
	});