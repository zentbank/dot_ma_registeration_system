Ext.define('tourism.view.business.registration.committee.IndividualCommitteeFieldSet', {
    extend: 'Ext.form.FieldSet',
    requires:[
        'Ext.form.FieldContainer',
        'Ext.form.field.VTypes'
    ],
    
    xtype: 'business-registration-committee-individualCommitteeFieldSet',

     initComponent: function(){

        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

        var masprefixstore =  Ext.create('tourism.store.combo.MasPrefixStore',{
                                storeId:'business-registration-committee-individual-masprefixstore-store'
        });

        var provinceStore =  Ext.create('tourism.store.combo.MasProvinceStore',{
                                storeId:'business-registration-committee-individual-masprovince-store'
                            });

        var amphurStore =   Ext.create('tourism.store.combo.MasAmphurStore',{
                                storeId:'business-registration-committee-individual-masamphurEN-store'
                            });

         Ext.apply(this, {
            checkboxToggle:true,
            checkboxName : 'committeePersonTypeIndividual',
            title: 'บุคคลธรรมดา',
            defaultType: 'textfield',
            collapsed: true,
            layout: 'anchor',
            defaults: {
                anchor: '100%'
            },
            items :
            [

                {
                    // หมายเลขบัตรประชาชนเลขที่
                    xtype: 'textfield',
                    name: 'committeeIdentityNo',
                    fieldLabel: 'หมายเลขบัตรประชาชน',
                    labelAlign: 'top',
                    allowBlank: false,
                    anchor: '50%',
                    afterLabelTextTpl: required,
                    tabIndex : 1, // เวลากด tab
                    maxLength: 13,
                    minLength: 13,
                    enforceMaxLength: true,
                    maskRe: /\d/
                },
                {
                    xtype:'container',
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%'
                    },
                    items:
                    [
                        {
                            
                            xtype: 'container',
                            layout: 'hbox',
                            layoutCongig: {
                                 pack:'center',
                                 align:'middle'
                            },
                            items: 
                            [
                                {
                                    // left hand side
                                    xtype: 'container',
                                    layout: 'anchor',
                                    flex: 1,
                                    defaults: {
                                        anchor: '100%'
                                    },
                                    items:
                                    [
                                        {
                                            // ชื่อ
                                            xtype: 'fieldcontainer',
                                            labelAlign: 'top',
                                            fieldLabel: 'ชื่อ(ไทย)',
                                            afterLabelTextTpl: required,
                                            // labelWidth: 100,
                                            layout: 'hbox',
                                            items: [
                                                {
                                                    xtype: 'masprefixcombo',
                                                    // fieldLabel: 'คำนำหน้าชื่อ',
                                                    store: masprefixstore,
                                                    queryMode: 'remote',
                                                    displayField: 'prefixName',
                                                    valueField: 'prefixId',
                                                    hiddenName: 'prefixId',
                                                    name: 'prefixId',
                                                    tabIndex: 2,
                                                    flex: 1,
                                                    triggerAction: 'all',
                                                    allowBlank: false,
                                                    prefixType: 'I'
                                                }, {
                                                    xtype: 'splitter'
                                                }, {
                                                    xtype: 'textfield',
                                                    name: 'committeeName',
                                                    tabIndex: 3,
                                                    flex: 2,
                                                    allowBlank: false
                                                }
                                            ]
                                        },
                                        {
                                            // ชื่อ
                                            xtype: 'fieldcontainer',
                                            labelAlign: 'top',
                                            fieldLabel: 'ชื่อ(อังกฤษ)',
                                            afterLabelTextTpl: required,
                                            // labelWidth: 100,
                                            layout: 'hbox',
                                            items: [
                                                {
                                                    xtype: 'masprefixcombo',
                                                    // fieldLabel: 'คำนำหน้าชื่อ',
                                                    store: masprefixstore,
                                                    queryMode: 'remote',
                                                    displayField: 'prefixNameEn',
                                                    valueField: 'prefixId',
                                                    hiddenName: 'prefixIdEn',
                                                    name: 'prefixIdEn',
                                                    tabIndex: 5,
                                                    flex: 1,
                                                    triggerAction: 'all',
                                                    allowBlank: false,
                                                    prefixType: 'I'
                                                }, {
                                                    xtype: 'splitter'
                                                }, {
                                                    xtype: 'textfield',
                                                    name: 'committeeNameEn',
                                                    tabIndex: 6,
                                                    flex: 2,
                                                    allowBlank: false
                                                }
                                            ]
                                        },
                                        {
                                            // เกิดวันที่
                                            xtype: 'datefield',
                                            fieldLabel: 'เกิดวันที่',
                                            labelAlign: 'top',
                                            format: 'd/m/B',
                                            tabIndex: 8,
                                            name: 'birthDate',
                                            // anchor: '49%',
                                            allowBlank: false,
                                            afterLabelTextTpl: required

                                        }// end เกิดวันที่
                                        
                                    ]
                                }//end left hand side
                                , {
                                    xtype: 'splitter'
                                }
                                ,{
                                    // right hand side
                                    xtype: 'container',
                                    layout: 'anchor',
                                    flex: 1,
                                    defaults: {
                                        anchor: '100%'
                                    },
                                    items:
                                    [
                                        {
                                            // นามสกุล
                                            xtype: 'textfield',
                                            name: 'committeeLastname',
                                            fieldLabel: 'นามสกุล(ไทย)',
                                            labelAlign: 'top',
                                            allowBlank: false,
                                            afterLabelTextTpl: required,
                                            tabIndex: 4
                                        }// end  นามสกุล
                                        ,{
                                            // นามสกุล
                                            xtype: 'textfield',
                                            name: 'committeeLastnameEn',
                                            fieldLabel: 'นามสกุล(อังกฤษ)',
                                            labelAlign: 'top',
                                            allowBlank: false,
                                            afterLabelTextTpl: required,
                                            tabIndex: 7
                                        }
                                        ,{
                                            // สัญชาติ
                                            xtype: 'textfield',
                                            name: 'committeeNationality',
                                            fieldLabel: 'สัญชาติ',
                                            allowBlank: false,
                                            afterLabelTextTpl: required,
                                            labelAlign: 'top',
                                            value: 'ไทย',
                                            tabIndex: 9
                                        }// end  สัญชาติ
                                                                           
                                    ]
                                } // end right hand side
                            ]
                        }
                        ,{
                            xtype: 'radiogroup',
                            fieldLabel: 'เพศ',
                            // labelAlign: 'top',
                            // labelWidth: 150,
                            // allowBlank: false,
                            afterLabelTextTpl: required,
                            // Arrange checkboxes into two columns, distributed vertically
                            columns: 2,
                            anchor: '50%',
                            vertical: true,
                            items: [
                            /*
                                M=Male
                                F=Female
                            */

                                { boxLabel: 'ชาย', tabIndex: 10,  name: 'gender', inputValue: 'M' , checked: true },
                                { boxLabel: 'หญิง',tabIndex: 11,  name: 'gender', inputValue: 'F' }
                                
                            ]
                        }
                        ,{
                            
                            xtype: 'container',
                            layout: 'hbox',
                            layoutCongig: {
                                 pack:'center',
                                 align:'middle'
                            },
                            items: 
                            [
                                {
                                    // left hand side
                                    xtype: 'container',
                                    layout: 'anchor',
                                    flex: 1,
                                    defaults: {
                                        anchor: '100%'
                                    },
                                    items:
                                    [
                                        {
                                            xtype: 'masprovincecombo',
                                            fieldLabel: 'ออกให้ ณ จังหวัด',
                                            labelAlign: 'top',
                                            id : 'business-committee-individual-province-combo',
                                            store: provinceStore,
                                            displayField: 'provinceName',
                                            valueField: 'provinceId',
                                            hiddenName: 'provinceId',
                                            name:'provinceId',
                                            tabIndex: 12,
                                            // allowBlank: false,
                                            // afterLabelTextTpl: required,
                                            masamphurComboId:'business-committee-individual-masamphur-combo',
                                            queryParam: 'provinceName',
                                            
                                        }// end ออกให้โดยสรรพากรจังหวัด
                                        ,{
                                            // เมื่อวันที่
                                            xtype: 'datefield',
                                            fieldLabel: 'เมื่อวันที่',
                                            labelAlign: 'top',
                                            format: 'd/m/B',
                                            name: 'committeeIdentityDate',
                                            tabIndex: 14
                                            // ,
                                            // allowBlank: false,
                                            // afterLabelTextTpl: required

                                        }// end เมื่อวันที่
                                    ]
                                }//end left hand side
                                , {
                                    xtype: 'splitter'
                                }
                                ,{
                                    // right hand side
                                    xtype: 'container',
                                    layout: 'anchor',
                                    flex: 1,
                                    defaults: {
                                        anchor: '100%'
                                    },
                                    items:
                                    [
                                       {
                                            xtype: 'masamphurcombo',
                                            fieldLabel: 'เขตพื้นที่',
                                            labelAlign: 'top',
                                            id : 'business-committee-individual-masamphur-combo',
                                            store: amphurStore,
                                            // queryMode: 'remote',
                                            displayField: 'amphurName',
                                            valueField: 'amphurId',
                                            hiddenName: 'amphurId',
                                            name: 'amphurId',
                                            tabIndex: 13,
                                            // triggerAction: 'all',
                                            // allowBlank: false,
                                            // afterLabelTextTpl: required,
                                            queryParam: 'amphurName',
                                            masprovinceComboId: 'business-committee-individual-province-combo',
                                            mastambolComboId: 'business-committee-individual-tambol-combo'
                                           
                                        }// end เขตพื้นที่ 
                                    ]
                                } // end right hand side
                            ]
                        }
                    ]
                        
                }
            ]
         });

        this.callParent(arguments);
     }

});