Ext.define('tourism.view.business.registration.person.CorporatePersonFieldSet', {
    extend: 'Ext.form.FieldSet',
    requires:[
        'Ext.form.FieldContainer',
        'Ext.form.field.VTypes'
    ],
    
    xtype: 'business-registration-person-corporatepersonfieldset',

     initComponent: function(){

        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
       var masprefixstore =  Ext.create('tourism.store.combo.MasPrefixStore',{
                        storeId:'business-registration-person-corp-masprefixstore-store'
                    });

       var provinceStoreCorp =  Ext.create('tourism.store.combo.MasProvinceStore',{
                storeId:'business-registration-person-corp-masprovince-store'
            });


        var amphurStoreCorp =   Ext.create('tourism.store.combo.MasAmphurStore',{
                                storeId:'business-registration-person-corp-masamphur-store'
                            });

         Ext.apply(this, {
            checkboxToggle:true,
            checkboxName : 'personTypeCorporate',
            title: 'นิติบุคคล',
            defaultType: 'textfield',
            collapsed: true,
            layout: 'anchor',
            defaults: {
                anchor: '100%'
            },
            items :
            [
                {
                    // left hand side
                    xtype: 'container',
                    layout: 'anchor',
                    flex: 1,
                    defaults: {
                        anchor: '100%'
                    },
                    items:[
                        {
                            // ข้าพเจ้า
                            xtype: 'fieldcontainer',
                            labelAlign: 'top',
                            fieldLabel: 'ชื่อนิติบุคคล',
                            afterLabelTextTpl: required,
                            // labelWidth: 100,
                            layout: 'hbox',
                            items: [
                                {
                                    xtype: 'masprefixcombo',
                                    // fieldLabel: 'คำนำหน้าชื่อ',
                                    store: masprefixstore,
                                    queryMode: 'remote',
                                    displayField: 'prefixName',
                                    valueField: 'prefixId',
                                    hiddenName: 'prefixIdCorp',
                                    name: 'prefixIdCorp',
                                    tabIndex: 15,
                                    flex: 1,
                                    triggerAction: 'all',
                                    allowBlank: false,
                                    prefixType: 'C'
                                }, {
                                    xtype: 'splitter'
                                }, {
                                    xtype: 'textfield',
                                    name: 'firstNameCorp',
                                    tabIndex: 16,
                                    flex: 2,
                                    allowBlank: false
                                }, {
                                    xtype: 'splitter'
                                }, {
                                    xtype: 'combo',
                                    // fieldLabel: 'คำนำหน้าชื่อ',
                                    store: 'tourism.store.combo.MasPostfixStore',
                                    queryMode: 'remote',
                                    displayField: 'postfixName',
                                    valueField: 'postfixId',
                                    hiddenName: 'postfixId',
                                    name: 'postfixId',
                                    tabIndex: 17,
                                    flex: 1,
                                    triggerAction: 'all'
                                    // ,
                                    // allowBlank: false
                                }
                            ]
                        }// end ข้าพเจ้า
                    ]
                }// end left hand side
                ,{
                    xtype: 'container',
                    layout: 'hbox',
                    layoutCongig: {
                         pack:'center',
                         align:'middle'
                    },
                    items:
                    [
                        {
                            // ทะเบียนนิติบุคคลเลขที่
                            xtype: 'textfield',
                            fieldLabel: 'ทะเบียนนิติบุคคลเลขที่',
                            name: 'identityNoCorp',
                            labelAlign: 'top',
                            allowBlank: false,
                            afterLabelTextTpl: required,
                            flex: 1,
                            tabIndex: 18,
                            maxLength: 13,
                            minLength: 10,
                            enforceMaxLength: true,
                            maskRe: /\d/

                        }// end // ทะเบียนนิติบุคคลเลขที่,
                        ,{
                            xtype: 'splitter'
                        }
                        ,{
                            xtype: 'combo',
                            fieldLabel: 'ประเภทนิติบุคคล',
                            labelAlign: 'top',
                            store: 'tourism.store.combo.CorporateTypeStore',
                            queryMode: 'local',
                            displayField: 'corporateTypeName',
                            valueField: 'corporateType',
                            hiddenName: 'corporateType',
                            name: 'corporateType',
                            flex: 1,
                            tabIndex: 19,
                            allowBlank: false,
                            afterLabelTextTpl: required
                        }
                    ]
                }
                ,{
                    // โดย
                    xtype: 'textfield',
                    labelAlign: 'top',
                    fieldLabel: 'โดย',
                    tabIndex: 20,
                    name: 'committeeNameSign',
                    allowBlank: true,
                    // afterLabelTextTpl: required

                }// end โดย
                ,{
                    xtype: 'container',
                    layout: 'hbox',
                    layoutCongig: {
                         pack:'center',
                         align:'middle'
                    },
                    items:
                    [                       
                        {
                            xtype: 'masprovincecombo',
                            fieldLabel: 'ออกให้ ณ สำนักงานทะเบียน',
                            labelAlign: 'top',
                            // id : 'business-committee-corpotate-masprovince-combo',
                            store: provinceStoreCorp,
                            displayField: 'provinceName',
                            valueField: 'provinceId',
                            hiddenName: 'provinceIdCorp',
                            name: 'provinceIdCorp',
                            flex: 1,
                            tabIndex: 21,
                            allowBlank: true,
                            // afterLabelTextTpl: required,
                            queryParam: 'provinceName',
                            
                        }// end ออกให้โดยสรรพากรจังหวัด
                        , {
                            xtype: 'splitter'
                        }
                        ,{
                            // เมื่อวันที่
                            xtype: 'datefield',
                            fieldLabel: 'เมื่อวันที่',
                            labelAlign: 'top',
                            format: 'd/m/B',
                            name: 'identityDateCorp',
                            flex: 1,
                            tabIndex: 22,
                            allowBlank: true
                            // afterLabelTextTpl: required

                        }// end เมื่อวันที่
                        
                    ]
                }


               ,{
                   // เลขประจำตัวผู้เสียภาษีอากร
                   xtype: 'textfield',
                   labelAlign: 'top',
                   fieldLabel: 'เลขประจำตัวผู้เสียภาษีอากร',
                   name: 'taxIdentityNo',
                   maxLength: 13,
                   minLength: 10,
                   enforceMaxLength: true,
                   tabIndex: 23,
                   anchor: '50%',
                   hidden: true,
                   allowBlank: true
                   // ,
                   // allowBlank: true,
                   // afterLabelTextTpl: required,
                   
               }// end ใบทะเบียนภาษีมูลค่าเพิ่ม (ถ้ามี) หรือเลขประจำตัวผู้เสียภาษีอากร
               ,{
                   // ออกให้ ณ จังหวัดหรือเขตพื้นที่
                   xtype: 'container',
                   layout: 'hbox',
                   layoutCongig: {
                        pack:'center',
                        align:'middle'
                   },
                   items:
                   [
                       {
                           // left hand side
                           xtype: 'container',
                           layout: 'anchor',
                           flex: 1,
                           defaults: {
                               anchor: '100%'
                           },
                           items:[
                               {
                                   xtype: 'masprovincecombo',
                                   fieldLabel: 'ออกให้ ณ จังหวัด',
                                   labelAlign: 'top',
                                   id : 'business-registration-person-corp-masprovince-combo',
                                   store: provinceStoreCorp,
                                   displayField: 'provinceName',
                                   valueField: 'provinceId',
                                   hiddenName: 'taxProvinceId',
                                   name: 'taxProvinceId',
                                   tabIndex: 24,
                                   allowBlank: true,
                                   // afterLabelTextTpl: required,
                                   masamphurComboId:'business-registration-person-corp-masamphur-combo',
                                   queryParam: 'provinceName',
                                   hidden: true
                                   
                               }// end ออกให้โดยสรรพากรจังหวัด
                           ]
                       }// end left hand side
                       ,
                       {
                           xtype: 'splitter'
                       }
                       ,{
                           // right hand site
                           xtype: 'container',
                           layout: 'anchor',
                           flex: 1,
                           defaults: {
                               anchor: '100%'
                           },
                           items:[
                               {
                                   xtype: 'masamphurcombo',
                                   fieldLabel: 'เขตพื้นที่',
                                   labelAlign: 'top',
                                   id : 'business-registration-person-corp-masamphur-combo',
                                   store: amphurStoreCorp,
                                   // queryMode: 'remote',
                                   displayField: 'amphurName',
                                   valueField: 'amphurId',
                                   hiddenName: 'taxAmphurId',
                                   name: 'taxAmphurId',
                                   tabIndex: 25,
                                   triggerAction: 'all',
                                   allowBlank: true,
                                   // afterLabelTextTpl: required,
                                   queryParam: 'amphurName',
                                   masprovinceComboId: 'business-registration-person-corp-masprovince-combo',
                                   hidden: true
                                  
                               }// end เขตพื้นที่ 
                           ]
                       }// right hand site
                   ]
               }// end // ออกให้ ณ จังหวัดหรือเขตพื้นที่
           
            ]
         });

        this.callParent(arguments);
     }

});