Ext.define('tourism.view.business.registration.person.IndividualPersonFieldSet', {
    extend: 'Ext.form.FieldSet',
    requires:[
        'Ext.form.FieldContainer',
        'Ext.form.field.VTypes',
        'Ext.tip.QuickTipManager',
        'Ext.ux.statusbar.StatusBar',
        'Ext.ux.statusbar.ValidationStatus'
    ],
    
    xtype: 'business-registration-person-individualpersonfieldset',

     initComponent: function(){

        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

        var masprefixstore =  Ext.create('tourism.store.combo.MasPrefixStore',{
                                storeId:'business-registration-person-individual-masprefixstore-store'
        });

        var provinceStore =  Ext.create('tourism.store.combo.MasProvinceStore',{
                                storeId:'business-registration-person-individual-masprovince-store'
                            });

        var amphurStore =   Ext.create('tourism.store.combo.MasAmphurStore',{
                                storeId:'business-registration-person-individual-masamphurEN-store'
                            });

         Ext.apply(this, {
            checkboxToggle:true,
            checkboxName : 'personTypeIndividual',
            title: 'บุคคลธรรมดา',
            defaultType: 'textfield',
            collapsed: true,
            layout: 'anchor',
            defaults: {
                anchor: '100%'
            },
            items :
            [{
             	xtype: 'container',
             	layout: 'hbox',
             
             items :
                 [ {
                       xtype: 'hiddenfield',
                       name: 'imageFile'
                   },{
                    // หมายเลขบัตรประชาชนเลขที่
                    xtype: 'textfield',
                    name: 'identityNo',
                    fieldLabel: 'หมายเลขบัตรประชาชน',
                    maxLength: 13,
                    minLength: 10,
                    enforceMaxLength: true,
                    maskRe: /\d/,
                    //labelAlign: 'top',
                    labelWidth: 150,
                    allowBlank: false,
                    anchor: '50%',
                    afterLabelTextTpl: required,
                    tabIndex : 1, // เวลากด tab
                    listeners: {
                        blur: function(value){
                     	   var form = Ext.create('Ext.form.Panel');
                     	  
                     	  var fthis = this.up('form'); 
                     	   var identityNo = this.getValue();
 
                     	   form.load({
                                url: 'business/registration/checkidentityNo/verification',
                                method : 'POST',
                                model: 'tourism.model.registration.RegistrationModel',
                                params: {identityNo:identityNo},
                                success: function(form, action) {
                             	   
                                 var dataObj = action.result.data;
                                 var title = fthis.getForm().findField('statusCheckIdentityNo');
                                 var bshow = Ext.getCmp('button-show');
                                 
                                 if(dataObj.check == 'true')
                                 {
                                	//title.setFieldLabel('<span style="color:red;font-weight:bold"><A HREF="https://www.google.co.th">อยู่ระหว่างพักใช้หรือเพิกถอน</A></span>');
                                	// title.setFieldLabel('<span style="color:black;font-weight:bold">สถานะ</span>');
                                	 title.setValue('<span style="color:red;font-weight:bold">อยู่ระหว่างพักใช้หรือเพิกถอน</span>');
                                	 bshow.show();
                                 }else{
                                //	title.setFieldLabel('<span style="color:black;font-weight:bold">สถานะ</span>');
                                	title.setValue('<span style="color:green;font-weight:bold">ผ่านการตรวจสอบ</span>');  
                                	bshow.hide();
                               }
                               
                                },
                                failure: function(form, action) {
                                  Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
                                },
                                scope: this 
                            }); 	   
                        }
                     }
                },
                {
                	xtype: 'displayfield',
                    name: 'statusCheckIdentityNo',
                    labelWidth: 170,
                  
                },
                {
                	xtype: 'button',
                	name: 'buttonTh',
                	id: 'button-show',
                    text: 'ดูรายละเอียด',
                    action: 'infoDetail',
                    hidden: true
                },
                {
	    	   		xtype: 'splitter'
	    	   	},
	    	   	{
	    	   		xtype: 'button',
    	   			iconCls: 'icon-search-user-xsmall',
                    action: 'searchUserFromIDCardOnline'
	    	   	},
                {
	    	   		xtype: 'splitter'
	    	   	},
                {
                    xtype: 'button',
                    text: 'ค้นหาข้อมูลจากบัตรประชาชน',
                    margin: '0 5 0 0',
                    action: 'loadDataFromIdCard'
                }
              
            ]
            },
                {
                    xtype:'container',
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%'
                    },
                    items:
                    [
                        {
                            
                            xtype: 'container',
                            layout: 'hbox',
                            layoutCongig: {
                                 pack:'center',
                                 align:'middle'
                            },
                            items: 
                            [
                                {
                                    // left hand side
                                    xtype: 'container',
                                    layout: 'anchor',
                                    flex: 1,
                                    defaults: {
                                        anchor: '100%'
                                    },
                                    items:
                                    [
                                        {
                                            // ชื่อ
                                            xtype: 'fieldcontainer',
                                            labelAlign: 'top',
                                            fieldLabel: 'ชื่อ(ไทย)',
                                            afterLabelTextTpl: required,
                                            // labelWidth: 100,
                                            layout: 'hbox',
                                            items: [
                                                {
                                                    xtype: 'masprefixcombo',
                                                    // fieldLabel: 'คำนำหน้าชื่อ',
                                                    store: masprefixstore,
                                                    queryMode: 'remote',
                                                    displayField: 'prefixName',
                                                    valueField: 'prefixId',
                                                    hiddenName: 'prefixId',
                                                    name: 'prefixId',
                                                    tabIndex: 2,
                                                    flex: 1,
                                                    triggerAction: 'all',
                                                    allowBlank: false,
                                                    prefixType: 'I'
                                                }, {
                                                    xtype: 'splitter'
                                                }, {
                                                    xtype: 'textfield',
                                                    name: 'firstName',
                                                    tabIndex: 3,
                                                    flex: 2,
                                                    allowBlank: false
                                                }
                                            ]
                                        },
                                        {
                                            // ชื่อ
                                            xtype: 'fieldcontainer',
                                            labelAlign: 'top',
                                            fieldLabel: 'ชื่อ(อังกฤษ)',
                                            afterLabelTextTpl: required,
                                            // labelWidth: 100,
                                            layout: 'hbox',
                                            items: [
                                                {
                                                    xtype: 'masprefixcombo',
                                                    // fieldLabel: 'คำนำหน้าชื่อ',
                                                    store: masprefixstore,
                                                    queryMode: 'remote',
                                                    displayField: 'prefixNameEn',
                                                    valueField: 'prefixId',
                                                    hiddenName: 'prefixIdEn',
                                                    name: 'prefixIdEn',
                                                    tabIndex: 5,
                                                    flex: 1,
                                                    triggerAction: 'all',
                                                    allowBlank: false,
                                                    prefixType: 'I'
                                                }, {
                                                    xtype: 'splitter'
                                                }, {
                                                    xtype: 'textfield',
                                                    name: 'firstNameEn',
                                                    tabIndex: 6,
                                                    flex: 2,
                                                    allowBlank: false
                                                }
                                            ]
                                        },
                                        {
                                            // เกิดวันที่
                                            xtype: 'datefield',
                                            fieldLabel: 'เกิดวันที่',
                                            labelAlign: 'top',
                                            format: 'd/m/B',
                                            tabIndex: 8,
                                            name: 'birthDate',
                                            // anchor: '49%',
                                            allowBlank: false,
                                            afterLabelTextTpl: required

                                        }// end เกิดวันที่
                                        
                                    ]
                                }//end left hand side
                                , {
                                    xtype: 'splitter'
                                }
                                ,{
                                    // right hand side
                                    xtype: 'container',
                                    layout: 'anchor',
                                    flex: 1,
                                    defaults: {
                                        anchor: '100%'
                                    },
                                    items:
                                    [
                                        {
                                            // นามสกุล
                                            xtype: 'textfield',
                                            name: 'lastName',
                                            fieldLabel: 'นามสกุล(ไทย)',
                                            labelAlign: 'top',
                                            allowBlank: false,
                                            afterLabelTextTpl: required,
                                            tabIndex: 4
                                        }// end  นามสกุล
                                        ,{
                                            // นามสกุล
                                            xtype: 'textfield',
                                            name: 'lastNameEn',
                                            fieldLabel: 'นามสกุล(อังกฤษ)',
                                            labelAlign: 'top',
                                            allowBlank: false,
                                            afterLabelTextTpl: required,
                                            tabIndex: 7
                                        }
                                        ,{
                                            // สัญชาติ
                                            xtype: 'textfield',
                                            name: 'personNationality',
                                            fieldLabel: 'สัญชาติ',
                                            allowBlank: false,
                                            afterLabelTextTpl: required,
                                            value: 'ไทย',
                                            labelAlign: 'top',
                                            tabIndex: 9
                                        }// end  สัญชาติ
                                                                           
                                    ]
                                } // end right hand side
                            ]
                        }
                        ,{
                            xtype: 'radiogroup',
                            fieldLabel: 'เพศ',
                            afterLabelTextTpl: required,
                            // Arrange checkboxes into two columns, distributed vertically
                            columns: 2,
                            anchor: '50%',
                            vertical: true,
                            items: [
                      

                                { boxLabel: 'ชาย', tabIndex: 10,  name: 'gender', inputValue: 'M' , checked: true },
                                { boxLabel: 'หญิง',tabIndex: 11,  name: 'gender', inputValue: 'F' }
                                
                            ]
                        }
                        ,{
                            
                            xtype: 'container',
                            layout: 'hbox',
                            layoutCongig: {
                                 pack:'center',
                                 align:'middle'
                            },
                            items: 
                            [
                                {
                                    // left hand side
                                    xtype: 'container',
                                    layout: 'anchor',
                                    flex: 1,
                                    defaults: {
                                        anchor: '100%'
                                    },
                                    items:
                                    [
                                        {
                                            xtype: 'masprovincecombo',
                                            fieldLabel: 'ออกให้ ณ จังหวัด',
                                            labelAlign: 'top',
                                            id : 'business-registration-person-individual-province-combo',
                                            store: provinceStore,
                                            displayField: 'provinceName',
                                            valueField: 'provinceId',
                                            hiddenName: 'provinceId',
                                            name:'provinceId',
                                            tabIndex: 12,
                                            // allowBlank: false,
                                            // afterLabelTextTpl: required,
                                            masamphurComboId:'business-registration-person-individual-masamphur-combo',
                                            queryParam: 'provinceName',
                                            
                                        }// end ออกให้โดยสรรพากรจังหวัด
                                        ,{
                                            // เมื่อวันที่
                                            xtype: 'datefield',
                                            fieldLabel: 'เมื่อวันที่',
                                            labelAlign: 'top',
                                            format: 'd/m/B',
                                            name: 'identityDate',
                                            tabIndex: 14
                                            // ,
                                            // allowBlank: false,
                                            // afterLabelTextTpl: required

                                        }// end เมื่อวันที่
                                    ]
                                }//end left hand side
                                , {
                                    xtype: 'splitter'
                                }
                                ,{
                                    // right hand side
                                    xtype: 'container',
                                    layout: 'anchor',
                                    flex: 1,
                                    defaults: {
                                        anchor: '100%'
                                    },
                                    items:
                                    [
                                       {
                                            xtype: 'masamphurcombo',
                                            fieldLabel: 'เขตพื้นที่',
                                            labelAlign: 'top',
                                            id : 'business-registration-person-individual-masamphur-combo',
                                            store: amphurStore,
                                            // queryMode: 'remote',
                                            displayField: 'amphurName',
                                            valueField: 'amphurId',
                                            hiddenName: 'amphurId',
                                            name: 'amphurId',
                                            tabIndex: 13,
                                            triggerAction: 'all',
                                            allowBlank: true,
                                            // afterLabelTextTpl: required,
                                            queryParam: 'amphurName',
                                            masprovinceComboId: 'business-registration-person-individual-province-combo',
                                            mastambolComboId: 'business-registration-person-individual-tambol-combo'
                                           
                                        }// end เขตพื้นที่ 
                                        ,{
                                    
                                             xtype: 'fieldset',
                                             itemId: 'imageWrapper',
                                             margin: '0 0 0',
                                             layout: 'anchor',
                                             items: [{
                                                xtype: 'image',
                                                itemId: 'imageFile',
                                                margin: '0 90px 0',
                                                listeners:{
                                                    afterrender:function(img, a, obj){
                                                        img.getEl().dom.style.width = '130px'; 
                                                        img.getEl().dom.style.height = '150px'; 
                                                    }
                                                }
                                             }]
                                        }
                                    ]
                                } // end right hand side
                            ]
                        }
                    ]
                        
                }
            ]
         });

        this.callParent(arguments);
     }
});