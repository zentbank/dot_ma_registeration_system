Ext.define('tourism.view.business.registration.person.PersonForm', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.form.FieldSet',
        'Ext.form.FieldContainer',
        'Ext.form.field.VTypes'
        ,'tourism.view.business.registration.person.CorporatePersonFieldSet'
        ,'tourism.view.business.registration.person.IndividualPersonFieldSet'
    ],
    xtype: 'business-registration-person-tab',
   initComponent: function(){

        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
        
        Ext.apply(this, {
            // width: 800,
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },
            defaults: {
                    anchor: '100%'
            },
            items: [
                {
                    xtype:'business-registration-person-individualpersonfieldset'
                   
                },
                {
                    xtype: 'business-registration-person-corporatepersonfieldset'
                }
                
                
            ]

        
        });
        
        this.callParent();
    }
    ,afterRender: function()
    {
       this.callParent(arguments);

        var masterForm = this.up('form');

        var prefixCombo = masterForm.getForm().findField('prefixId');
        prefixCombo.addListener("blur" ,function(combo, eOpts )
        { 
            var prefixId = combo.getValue();


            var prefixIdEnCombo = combo.up('form').getForm().findField('prefixIdEn');

            if(!Ext.isEmpty(prefixIdEnCombo))
            {
                prefixIdEnCombo.setValue(prefixId);

            }

        },masterForm);

       
                   

        var individualFieldSet = masterForm.down('business-registration-person-individualpersonfieldset');

        individualFieldSet.addListener('expand', function( f, eOpts ){
                                    // this = masterForm
            var corporateFieldSet = this.down('business-registration-person-corporatepersonfieldset');
            
            var prefixIdCorp = this.getForm().findField('prefixIdCorp');
            prefixIdCorp.clearValue();
            prefixIdCorp.allowBlank =  true;

            var firstNameCorp = this.getForm().findField('firstNameCorp');
            firstNameCorp.setValue("");
            firstNameCorp.allowBlank =  true;

            var identityNoCorp = this.getForm().findField('identityNoCorp');
            identityNoCorp.setValue("");
            identityNoCorp.allowBlank =  true;

            var corporateType = this.getForm().findField('corporateType');
            corporateType.setValue("");
            corporateType.allowBlank =  true;

            var taxIdentityNoText = this.getForm().findField('taxIdentityNo');
            taxIdentityNoText.setValue("");
            taxIdentityNoText.allowBlank =  true;

            

            //-----------

            var identityNo = this.getForm().findField('identityNo');
            identityNo.allowBlank =  false;

            var prefixId = this.getForm().findField('prefixId');
            prefixId.allowBlank =  false;

            var firstName = this.getForm().findField('firstName');
            firstName.allowBlank =  false;

            var prefixIdEn = this.getForm().findField('prefixIdEn');
            prefixIdEn.allowBlank =  false;

            var firstNameEn = this.getForm().findField('firstNameEn');
            firstNameEn.allowBlank =  false;

            var birthDate = this.getForm().findField('birthDate');
            birthDate.allowBlank =  false;

            var lastName = this.getForm().findField('lastName');
            lastName.allowBlank =  false;

            var lastNameEn = this.getForm().findField('lastNameEn');
            lastNameEn.allowBlank =  false;

            var personNationality = this.getForm().findField('personNationality');
            //Oat Add 22/09/57
            personNationality.setValue("ไทย");
            //
            personNationality.allowBlank =  false;
                       
            corporateFieldSet.collapse();

        },masterForm);

        var corporateFieldSet = masterForm.down('business-registration-person-corporatepersonfieldset');

        corporateFieldSet.addListener('expand', function( f, eOpts ){

            var individualFieldSet = this.down('business-registration-person-individualpersonfieldset');

            var identityNo = this.getForm().findField('identityNo');
            identityNo.setValue("");
            identityNo.allowBlank =  true;

            var prefixId = this.getForm().findField('prefixId');
            prefixId.clearValue();
            prefixId.allowBlank =  true;

            var firstName = this.getForm().findField('firstName');
            firstName.setValue("");
            firstName.allowBlank =  true;

            var prefixIdEn = this.getForm().findField('prefixIdEn');
            prefixIdEn.clearValue();
            prefixIdEn.allowBlank =  true;

            var firstNameEn = this.getForm().findField('firstNameEn');
            firstNameEn.setValue("");
            firstNameEn.allowBlank =  true;

            var birthDate = this.getForm().findField('birthDate');
            birthDate.setValue("");
            birthDate.allowBlank =  true;

            var lastName = this.getForm().findField('lastName');
            lastName.setValue("");
            lastName.allowBlank =  true;

            var lastNameEn = this.getForm().findField('lastNameEn');
            lastNameEn.setValue("");
            lastNameEn.allowBlank =  true;

            var personNationality = this.getForm().findField('personNationality');
            personNationality.setValue("");
            personNationality.allowBlank =  true;


            //----------

            var prefixIdCorp = this.getForm().findField('prefixIdCorp');
            prefixIdCorp.allowBlank =  false;

            var firstNameCorp = this.getForm().findField('firstNameCorp');
            firstNameCorp.allowBlank =  false;

            var identityNoCorp = this.getForm().findField('identityNoCorp');
            identityNoCorp.allowBlank =  false;

            var corporateType = this.getForm().findField('corporateType');
            corporateType.allowBlank =  false;

            // var taxIdentityNoText = this.getForm().findField('taxIdentityNo');
            // taxIdentityNoText.allowBlank =  false;

            individualFieldSet.collapse();
        },masterForm);
        
    }
});