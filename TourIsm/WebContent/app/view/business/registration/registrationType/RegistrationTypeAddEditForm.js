Ext.define('tourism.view.business.registration.registrationType.RegistrationTypeAddEditForm', {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.data.*',
        'Ext.form.*',
        'Ext.tab.Tab'
    ],
    alias: 'widget.business-registration-registrationtype-addeditform',
    // title: 'ธุรกิจนำเที่ยว',
    bodyPadding: 5,
    autoScroll: true,

    initComponent: function(){

        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
        
        Ext.apply(this, {
            width: '100%',
            height: '100%',
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },

            items: 
            [ 
                {
                    xtype: 'container',
                    // title: 'Payment',
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%',
                        labelWidth: 150
                    },
                    items: 
                    [ 
                        {
                            /*
                            ประเภทใบอนุญาต
                            B=ธุรกิจนำเที่ยว
                            G=มัคคุเทศก์
                            L=ผู้นำเที่ยว
                            */
                            xtype: 'hiddenfield',
                            name: 'traderType',
                            value: 'B'
                        },
                        {
                            xtype: 'radiogroup',
                            fieldLabel: 'ประเภทการทำรายการ',
                            // labelWidth: 150,
                            allowBlank: false,
                            afterLabelTextTpl: required,
                            // Arrange checkboxes into two columns, distributed vertically
                            columns: 1,
                            vertical: true,
                            items: [
                            /*
                                  {"registrationType":"N", "registrationTypeName":"จดทะเบียนใหม่"},
                                    {"registrationType":"R", "registrationTypeName":"ชำระค่าธรรมเนียมราย 2 ปี"},
                                    {"registrationType":"B", "registrationTypeName":"เพิ่มข้อมูลสาขา"},
                                    {"registrationType":"C", "registrationTypeName":"เปลี่ยนแปลงรายการ"},
                                    {"registrationType":"T", "registrationTypeName":"ออกใบแทน"}
                            */

                                { boxLabel: 'จดทะเบียนใหม่',  name: 'registrationType', inputValue: 'N' },
                                { boxLabel: 'ชำระค่าธรรมเนียมราย 2 ปี',  name: 'registrationType', inputValue: 'R' },
                                { boxLabel: 'เพิ่มข้อมูลสาขา',  name: 'registrationType', inputValue: 'B' },
                                { boxLabel: 'เปลี่ยนแปลงรายการ',  name: 'registrationType', inputValue: 'C' },
                                { boxLabel: 'ออกใบแทน',  name: 'registrationType', inputValue: 'T' }
                                
                            ]
                        }
                    ]
                }
            ],

            buttons: [{
                text: 'ดำเนินการต่อ',
                action: 'selectRegType'
                // scope: this,
                // handler: this.onCompleteClick
            }]    
        });
        this.callParent(arguments);
    }
    
});