Ext.define('tourism.view.business.registration.registrationType.RegistrationTypeAddEditWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.business-registration-registrationtype-addeditwindow',

    requires: [
        'tourism.view.business.registration.registrationType.RegistrationTypeAddEditForm',
        'tourism.view.business.registration.registrationType.RegistrationTypeSearchBusinessForm'
    ],
    
    initComponent: function() {


        Ext.apply(this, {
            title : 'เลือกประเภทการทำรายการ',
            // layout: 'fit',
            layout: 'card',
            activeItem: 0,
            autoShow: true,
            width: 400,
            height: 400,
            autoShow: true,
            modal: true,
            items :[
                {
                    xtype: 'business-registration-registrationtype-addeditform',
                    border: false,
                    frame: false
                },
                {
                   xtype: 'business-registration-registrationtype-searchbusinessform',
                   id: 'business-registration-registrationtype-searchbusinessform',
                   border: false,
                    frame: false
                }
            ]
        });

        this.callParent(arguments);
    },

});
