Ext.define('tourism.view.business.registration.registrationType.RegistrationTypeGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.registrationtype-grid',
		stripeRows: true,
		requires: [
			'Ext.grid.Column',
			'tourism.store.registration.RegistrationStore',
			'Ext.grid.column.Template'
			

			
		],
		initComponent: function(){
			var registrationStore = Ext.create('tourism.store.registration.RegistrationStore',{
				// storeId: 'registrationtype-store',
				// pageSize : this.storepageSize,
				// groupField: 'registrationTypeName',
				proxy: {
					type: 'ajax',
					actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
					api: {
					    read: 'business/registration/trader/readbylicenseno'
					},
					reader: {
					    type: 'json',
					    root: 'list',
					    totalProperty: 'totalCount',
					    successProperty: 'success',
					    messageProperty: 'message'
					},
					listeners: {
					    exception: function(proxy, response, operation){

					        Ext.MessageBox.show({
					            title: 'REMOTE EXCEPTION',
					            msg: operation.getError(),
					            icon: Ext.MessageBox.ERROR,
					            buttons: Ext.Msg.OK
					        });
					    }
					}
				}
			});

			Ext.apply(this, {
				store: registrationStore,
				selType: 'checkboxmodel',
				mode: 'SINGLE',
				columns: [
					{
						header: 'traderId',
						dataIndex: 'traderId',
						hidden: true
					},
					{
						header: 'ชื่อธุรกิจนำเที่ยว',
						dataIndex: 'traderName',
						flex: 2,
						renderer: function(value, metaData, model){

							var renderer = '<p><b>ออกใบอนุญาตให้: '+model.get('traderOwnerName')+'</b></p>';
							renderer = renderer + '<p><b>ประเภทธุรกิจนำเที่ยว: '+model.get('traderCategoryName')+'</b></p>';
							renderer = renderer + '<p><b>ชื่อธุรกิจนำเที่ยว:</b></p>';
							renderer = renderer + '<p><b>'+model.get('traderName')+'</b></p>';
							renderer = renderer + '<p><b> ('+model.get('traderNameEn')+')</b></p>';
							renderer = renderer + '<p><b> วันเริ่มต้น</b></p>';
							renderer = renderer + '<p><b> ('+model.get('effectiveDate')+')</b></p>';
							renderer = renderer + '<p><b> วันที่ครบกำหนดชำระค่าธรรมเนียม</b></p>';
							renderer = renderer + '<p><b> '+model.get('expireDate')+'</b></p>';

							


							if(model.get('traderType') == 'G')
							{
								var renderer = '<p><b>ออกใบอนุญาตให้: '+model.get('traderOwnerName')+'</b></p>';
								renderer = renderer + '<p><b>ประเภทมัคคุเทศน์: '+model.get('traderCategoryGuideTypeName')+'</b></p>';
								renderer = renderer + '<p><b>ชนิดมัคคุเทศน์: '+model.get('traderCategoryName')+'</b></p>';
								renderer = renderer + '<p><b>ชื่อมัคคุเทศน์: '+model.get('traderOwnerName')+'</b></p>';
								renderer = renderer + '<p><b>ชื่อมัคคุเทศน์(ภาษาอังกฤษ): '+model.get('traderOwnerNameEn')+'</b></p>';
								renderer = renderer + '<p><b>บัตรประจำตัวเลขที่: '+model.get('identityNo')+'</b></p>';
								renderer = renderer + '<p><b> วันเริ่มต้น</b></p>';
								renderer = renderer + '<p><b> ('+model.get('effectiveDate')+')</b></p>';
								renderer = renderer + '<p><b> วันที่สิ้นสุด</b></p>';
								renderer = renderer + '<p><b> '+model.get('expireDate')+'</b></p>';
					
							}

							if(model.get('traderType') == 'L')
							{
								var renderer = '<p><b>ออกใบอนุญาตให้: '+model.get('traderOwnerName')+'</b></p>';
								renderer = renderer + '<p><b>ประเภทผู้นำเที่ยว: '+model.get('traderCategoryName')+'</b></p>';
								renderer = renderer + '<p><b>ชื่อผู้นำเที่ยว: '+model.get('traderOwnerName')+'</b></p>';
								renderer = renderer + '<p><b>ชื่อผู้นำเที่ยว(ภาษาอังกฤษ): '+model.get('traderOwnerNameEn')+'</b></p>';
								renderer = renderer + '<p><b>บัตรประจำตัวเลขที่: '+model.get('identityNo')+'</b></p>';

				
							}
							
							return renderer;
							
						}
						
					},
					{
						header: 'สถานะใบอนุญาต',
						dataIndex: 'licenseStatus',
						flex: 1,
						renderer: function(value, metaData, model){
							if(value == "S")
							{
								return '<span class="tour-fg-color-redLight">พักใช้ใบอนุญาต</span>';
							}
							if(value == "R")
							{
								return '<span class="tour-fg-color-redLight">เพิกถอนใบอนุญาต</span>';
							}
							if(value == "C")
							{
								return '<span class="tour-fg-color-redLight">ยกเลิก</span>';
							}
							if(value == "N")
							{
								return '<span class="tour-fg-color-green">ปกติ</span>';
							}
							if(value == "D")
							{
								return '<span class="tour-fg-color-lighten">ไม่ผ่านการตรวจสอบ/ยกเลิกการจดทะเบียน</span>';
							}
							if(value == "E")
							{
								return '<span class="tour-fg-color-redLight">หมดอายุตามกฎหมาย</span>';
							}
						}
					}
				]
			});

			this.callParent(arguments);


		}
		// ,
		//  plugins: [{
  //           ptype: 'rowexpanderat',
  //           pluginId: 'rowexpanderregistertype',
  //           // insertAt: 1, 
  //           rowBodyTpl : new Ext.XTemplate(
  //           	'<div id="tpl-rowexpander-registertype-registration">',
  //           	'<tpl switch="traderType">',
		//             '<tpl case="B">',
		//                 '<p><b>ออกใบอนุญาตให้: {traderOwnerName}</b></p>',
	 //            		'<p><b>ทะเบียนนิติบุคคลเลขที่: {identityNo}</b></p>',
	 //            		'<p><b>ประเภทธุรกิจนำเที่ยว: {traderCategoryName}</b></p>',
		//             	'<p><b>ชื่อธุรกิจนำเที่ยว (ภาษาไทย): {traderName}</b></p>',
		//                 '<p><b>ชื่อธุรกิจนำเที่ยว (ภาษาต่างประเทศ): {traderNameEn}</b> </p>',
		//                 '<p><font size="2">{traderAddress}</font></p>',
		//             '<tpl case="G">',
		            	
		//             	'<p><b>ออกใบอนุญาตให้: {traderOwnerName}</b></p>',
	 //            		'<p><b>หมายเลขบัตรประชาชน: {identityNo}</b></p>',
	 //            		'<p><b>ประเภทมัคคุเทศก์: {traderCategoryName}</b></p>',
		//                 '<p><font size="2">{traderAddress}</font></p>',
		//             '<tpl default>',
		            	
		//             	'<p><b>ออกใบอนุญาตให้: {traderOwnerName}</b></p>',
	 //            		'<p><b>หมายเลขบัตรประชาชน: {identityNo}</b></p>',
	 //            		'<p><b>ประเภทมัคคุเทศก์: {traderCategoryName}</b></p>',
		//                 '<p><font size="2">{traderAddress}</font></p>',
		//         '</tpl>',
            		
  //               '<div>'
  //           )
  //       },{
		// 				ptype: 'rowediting',
		// 				pluginId: 'rowediting-grid-registerType',
		// 				// clicksToEdit: 2,
		// 				autoCancel: false
						
		// 			}]
	});