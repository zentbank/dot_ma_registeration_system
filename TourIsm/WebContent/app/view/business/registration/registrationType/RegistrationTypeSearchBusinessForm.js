Ext.define('tourism.view.business.registration.registrationType.RegistrationTypeSearchBusinessForm', {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.data.*',
        'Ext.form.*',
        'Ext.tab.Tab',
        'tourism.view.business.registration.registrationType.RegistrationTypeGrid'
    ],
    alias: 'widget.business-registration-registrationtype-searchbusinessform',
    // title: 'ธุรกิจนำเที่ยว',
    bodyPadding: 5,
    autoScroll: true,
    initComponent: function(){

        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
        
        Ext.apply(this, {
            width: '100%',
            height: '100%',
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },
           
            items: 
            [ 
                {
                    xtype: 'container',
                    // title: 'Payment',
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%',
                        labelWidth: 150
                    },
                    items: 
                    [
                        {
                            xtype: 'hiddenfield',
                            name: 'registrationType'
                        },{
                            xtype: 'displayfield',
                            name: 'registrationTypeName'
                            
                        },
                        {
                            xtype: 'container',
                            layout: 'hbox',
                            items: [
                                {
                                     xtype: 'textfield',
                                     name: 'licenseNo',
                                     fieldLabel: 'เลขที่ใบอนุญาต',
                                     flex: 3
                                    
                                },{
                                    xtype: 'splitter'
                                }, {

                                    xtype: 'button',
                                    text: 'ค้นหา',
                                    flex: 1,
                                    action: 'searchLicense'
                                    // ,
                                    // tooltip: 'Find Previous Row',
                                    // handler: this.onPreviousClick,
                                    // scope: this
                                }
                            ]

                        },
                        {
                            xtype: 'splitter'
                        },
                        {
                            xtype: 'container',
                            layout: 'fit',
                            items: [
                                {
                                    xtype: 'registrationtype-grid',
                                    border: false,
                                    frame: false,
                                    heigth: '100%'
                                }
                            ]

                        }
                    ]
                }
            ],

            buttons: [{
                text: 'ดำเนินการต่อ',
                action: 'selectedLicenseNo'
                // scope: this,
                // handler: this.onCompleteClick
            }]    
        });
        this.callParent();
    }
   
    
  
  
});