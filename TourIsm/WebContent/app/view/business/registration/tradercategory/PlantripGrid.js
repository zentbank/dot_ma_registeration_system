Ext.define('tourism.view.business.registration.tradercategory.PlantripGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.business_registration_tradercategory_plantripgrid',
		
		stripeRows: true,
		requires: [
			'Ext.grid.Column',
			'tourism.store.business.registration.tradercategory.PlantripStore',
			'Ext.toolbar.Paging',
			'Ext.grid.plugin.RowEditing',
			'Ext.grid.column.Template',
			'Ext.grid.column.Action',
			'Ext.grid.feature.Summary'
		],
		initComponent: function(){

			// create every new form 
			var countryStrore = Ext.create('tourism.store.combo.CountryStrore',{
				storeId: 'business-registration-tradercategory-mascountrystrore'
			});
			countryStrore.load();

			var plantripStore = Ext.create('tourism.store.business.registration.tradercategory.PlantripStore',{
				storeId: 'business-registration-tradercategory-plantripStore'
			});

			Ext.apply(this, {
				store: plantripStore,
				height: 160,
				autoScroll: true,
				collapsed : false,
				collapseMode: 'mini',
				collapseDirection : 'bottom',
				hideHeaders: true,
				features: [{
		            ftype: 'summary',
		            dock: 'bottom'
				}],
				plugins:[
					{
						ptype: 'rowediting',
						pluginId: 'rowediting-grid-plantripgrid',
						clicksToEdit: 2,
						autoCancel: false,
						listeners: {
							edit: function(editor, context, eOpts) {
								context.grid.getStore().sync();
							}
						}
					}
				], 
				columns: [
					{xtype: 'rownumberer'},
					{
			                header: 'ประเทศ',
			            
			                dataIndex: 'countryId',
			                flex: 1,
							summaryType: 'count',

							summaryRenderer: function(value, summaryData, dataIndex) {
			                	return 'นำเที่ยวรวม ' + value + ' ประเทศ';
			            	},
			                editor: 
			                {
			                	xtype: 'combo',
			                    typeAhead: true,
			                    triggerAction: 'all',
			                    valueField: 'countryId',
			                    displayField: 'countryName',
			                    allowBlank: false,
			                    store: countryStrore
		                	},
		                	renderer: function(value, metaData, gridmodel, rowIndex,colIndex,gridstore,gridview){
		                		var countryName = value;
		                		countryStrore.each(function(model){
		                			if(model.get('countryId') == value)
		                			{
		                				countryName = model.get('countryName');
		                			}
		                		});
		                		
						        return countryName;
						    }
		            }

			        ,{
			        	header: 'ลบ',
			        	width: 50,
			           	align : 'center',
			            xtype: 'actioncolumn',
			           	// flex: 1,
			            items: [{
			                iconCls: 'icon-delete-xsmall',
			                tooltip: 'ลบ',
				            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
				                this.fireEvent('itemclick', this, 'deleteplantrip', grid, rowIndex, colIndex, record, node);
				            }
			            }]
			        }
				]
				,dockedItems: [{
				    xtype: 'toolbar',
				    dock: 'top',
				 
				    items: [
				        { xtype: 'component', flex: 1 },
				        { 
				        	xtype: 'button',
				        	text: 'เพิ่มประเทศ',
				        	action: 'addplantrip'
				        }
				    ]
				}]
			});

			this.callParent(arguments);
		}
	});