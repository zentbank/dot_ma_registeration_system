Ext.define('tourism.view.business.registration.tradercategory.TraderCategoryForm', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.form.FieldSet',
        'Ext.form.FieldContainer',
        'Ext.form.field.VTypes'
    ],
    xtype: 'business-registration-tradercategory-tradercategoryform-tab',
    initComponent: function(){

        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
        
        Ext.apply(this, {
            // width: 800,
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },
            defaults: {
                    anchor: '100%'
            },
            items: [
                {
                            xtype: 'radiogroup',
                            fieldLabel: 'ประเภทการทำธุรกิจนำเที่ยว',
                            allowBlank: false,
                            afterLabelTextTpl: required,
                            labelWidth: 200,
                            itemId: 'business-registration-tradercategory-traderCategoryForm-traderCategory-radiogroup',
                            // fieldLabel: 'สถานะ',
                            // Arrange checkboxes into two columns, distributed vertically
                            columns: 1,
                            vertical: false,
                            items: [
                                { boxLabel: 'เฉพาะพื้นที่',  name: 'traderCategory', inputValue: '400' },
                                { boxLabel: 'ในประเทศ',  name: 'traderCategory', inputValue: '300' },
                                { boxLabel: 'นำเที่ยวจากต่างประเทศ',  name: 'traderCategory', inputValue: '200' },
                                
                                //Oat Edit userไม่ต้องการให้เลือก
//                                { boxLabel: 'ต่างประเทศ OUTBOUND',  name: 'traderCategory', inputValue: '100' ,checked: true  }
                                { boxLabel: 'ทั่วไป',  name: 'traderCategory', inputValue: '100' }
                                //End Oat Edit
                            ]
                 },
               
                 {
                    xtype: 'textfield',
                    fieldLabel: 'จัดนำเที่ยวจำนวน (ครั้ง/ปี)',
                    name: 'plantripPerYear',
                    labelWidth: 200,
                    allowBlank: false,
                    anchor: '100%',
                    afterLabelTextTpl: required,
                }
                ,{

                    // left hand side
                    xtype: 'container',
                    title: 'แผนการนำเที่ยว',
                    layout: 'anchor',
                    // flex: 1,
                    // defaults: {
                    //     anchor: '100%'
                    // },
                    frame: false,
                    border: false,
                    items:[
                        {
                            xtype:'business_registration_tradercategory_plantripgrid',
                            // title: 'นำเที่ยวประเทศ',
                            frame: false,
                            border: false,
                            anchor: '100% 100%'
                        }
                    ]
                }
               
            ]

        
        });
        
        this.callParent(arguments);
    }
});