Ext.define('tourism.view.chart.LicenseDetailFormSearch' ,{
    extend: 'Ext.form.FormPanel',
    alias : 'widget.chart-license-detail-formsearch',
    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.Time',
        'Ext.form.CheckboxGroup',
        'Ext.layout.container.HBox',
        'Ext.form.FieldSet'
    ],
    bodyPadding: 10,
    // width: '100%',
    items: [
        {
                xtype: 'container',
                // title: 'Payment',
                layout: 'anchor',
                defaults: {
                    anchor: '70%',
                    labelWidth: 250
                    
                },
                items: [  
                        
						
                        {
                            xtype: 'combo',
                            fieldLabel: 'ประเภทใบอนุญาต',
                            store: 'tourism.store.combo.TraderTypeStore',
                            queryMode: 'local',
                            displayField: 'traderTypeName',
                            valueField: 'traderType',
                            hiddenName: 'traderType',
                            name :'traderType',
                            allowBlank: false,
                            afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>'
                            // anchor: '50%',
                        },
						{
                            xtype: 'combo',
                            fieldLabel: 'สำนักงานทะเบียน',
                            store: 'tourism.store.combo.OrganizationStore',
                            queryMode: 'remote',
                            displayField: 'orgName',
                            valueField: 'orgId',
                            hiddenName: 'orgId',
                            name :'orgId'
                        },
                        {
                            xtype: 'combo',
                            fieldLabel: 'จังหวัด',
                            store: 'tourism.store.combo.MasProvinceStore',
                            queryMode: 'remote',
                            displayField: 'provinceName',
                            valueField: 'provinceId',
                            hiddenName: 'provinceId',
                            name :'provinceId'
                        },
                    
                ]
        },
        {
            xtype: 'toolbar',
            border: false,
            padding: '6px 0 6px 0px',
            items: [
                {
                    xtype: 'button',
                    text : 'พิมพ์รายงาน',
                    action: 'printLicenseDetail'
                   
                }
            ]
        }

    ]
});    





