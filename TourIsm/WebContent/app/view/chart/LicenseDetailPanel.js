Ext.define('tourism.view.chart.LicenseDetailPanel', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
        'tourism.view.chart.LicenseDetailFormSearch'
    ],
    
    xtype: 'report-license-detail-panel',
    
    initComponent: function(){
    	
    	
    	Ext.apply(this, {
    		layout: {
		        type: 'border'
		    }
		    ,items: [
		    
		    ,{
		    	region: 'center',
		        xtype: 'chart-license-detail-formsearch',
		        // id: this.roleAction+'-'+this.traderType+'-'+this.roleGroup+'-chart-column',
		        // layout: 'fit',
		        frame: false,
		        border: false
		    }
		    ]
    	});

    	this.callParent(arguments);
    }
   
    
});