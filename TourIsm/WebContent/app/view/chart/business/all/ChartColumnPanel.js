Ext.define('tourism.view.chart.business.all.ChartColumnPanel', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
    ],
    
    xtype: 'chart-column-business-panel',
    
    initComponent: function(){
    	
    	var formSearch = this.getChartColumnBusinessAllFormSearch(this.roleAction, this.roleGroup, this.traderType);
    	
    	if(this.roleGroup == 'new')
    	{
    		formSearch = this.getChartColumnBusinessNewFormSearch(this.roleAction, this.roleGroup, this.traderType);
    	}
    	
    	if(this.roleGroup == 'renew')
    	{
    		formSearch = this.getChartColumnBusinessRenewFormSearch(this.roleAction, this.roleGroup, this.traderType);
    	}
    	
    	if(this.roleGroup == 'suspensionandrevoke')
    	{
    		formSearch = this.getChartColumnBusinessSuspensionAndRevokeFormSearch(this.roleAction, this.roleGroup, this.traderType);
    	}
    	
    	if(this.roleGroup == 'guarantee')
    	{
    		formSearch = this.getChartColumnBusinessGuaranteeFormSearch(this.roleAction, this.roleGroup, this.traderType);
    	}
    	
    	Ext.apply(this, {
    		layout: {
		        type: 'border'
		    }
		    ,items: [
		    {
		        region: 'north',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items:[formSearch]
		    	,roleAction: this.roleAction
		    	,traderType: this.traderType
		    	,roleGroup: this.roleGroup
		    }
		    ,{
		    	region: 'center',
		        xtype: this.roleAction+'-'+this.traderType+'-'+this.roleGroup+'-column',
		        id: this.roleAction+'-'+this.traderType+'-'+this.roleGroup+'-chart-column',
		        layout: 'fit',
		        frame: false,
		        border: false
		    }
		    ]
    	});

    	this.callParent(arguments);
    }
    ,getChartColumnBusinessAllFormSearch: function(roleAction, roleGroup, traderType)
    {
    	return {
    		
	            title:'ตารางแสดงผู้มาดำเนินการเกี่ยวกับธุรกิจนำเที่ยวในภาพรวม',
	            collapsible: true, 
	            xtype: roleAction+'-'+traderType+'-'+roleGroup+'-formsearch',
	            id: roleAction+'-'+traderType+'-'+roleGroup+'-chart-formsearch',
	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false,
	            roleGroup : this.roleGroup,
	            traderType: this.traderType
    	};
    }
    ,getChartColumnBusinessNewFormSearch: function(roleAction, roleGroup, traderType)
    {
    	return {
    		
	            title:'ตารางแสดงจำนวนผู้มายื่นเรื่องขอรับใบอนุญาตประกอบธุรกิจนำเที่ยว(รายใหม่)',
	            collapsible: true, 
	            xtype: roleAction+'-'+traderType+'-'+roleGroup+'-formsearch',
	            id: roleAction+'-'+traderType+'-'+roleGroup+'-chart-formsearch',
	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false,
	            roleGroup : this.roleGroup,
	            traderType: this.traderType
    	};
    }
    ,getChartColumnBusinessRenewFormSearch: function(roleAction, roleGroup, traderType)
    {
    	return {
    		
	            title:'ตารางแสดงจำนวนผู้มายื่นขอชำระค่าธรรมเนียมประกอบธุรกิจนำเที่ยว ราย 2 ปี',
	            collapsible: true, 
	            xtype: roleAction+'-'+traderType+'-'+roleGroup+'-formsearch',
	            id: roleAction+'-'+traderType+'-'+roleGroup+'-chart-formsearch',
	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false,
	            roleGroup : this.roleGroup,
	            traderType: this.traderType
    	};
    }
    ,getChartColumnBusinessSuspensionAndRevokeFormSearch: function(roleAction, roleGroup, traderType)
    {
    	return {
    		
	            title:'ตารางแสดงจำนวนพักใช้และถูกเพิกถอนใบอนุญาตประกอบธุรกิจนำเที่ยว',
	            collapsible: true, 
	            xtype: roleAction+'-'+traderType+'-'+roleGroup+'-formsearch',
	            id: roleAction+'-'+traderType+'-'+roleGroup+'-chart-formsearch',
	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false,
	            roleGroup : this.roleGroup,
	            traderType: this.traderType
    	};
    }
    
    ,getChartColumnBusinessGuaranteeFormSearch: function(roleAction, roleGroup, traderType)
    {
    	return {
    		
	            title:'ตารางแสดง ยกเลิกและรับหลักประกันคืนและขอรับหลักประกันคืน(กรณีถูกเพิกถอน)',
	            collapsible: true,
	            xtype: roleAction+'-'+traderType+'-'+roleGroup+'-formsearch',
	            id: roleAction+'-'+traderType+'-'+roleGroup+'-chart-formsearch',
	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false,
	            roleGroup : this.roleGroup,
	            traderType: this.traderType
    	};
    }
    
});