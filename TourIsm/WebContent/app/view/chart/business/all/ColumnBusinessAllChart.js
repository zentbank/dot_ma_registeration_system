Ext.define('tourism.view.chart.business.all.ColumnBusinessAllChart',{
	extend: 'Ext.chart.Chart',
	alias: 'widget.chart-business-all-column',
	stripeRows: true,
	requires: [
		'Ext.chart.*',
		'Ext.Window', 
		'Ext.layout.container.Fit', 
		'Ext.fx.target.Sprite', 
		'Ext.window.MessageBox'
	],
	initComponent: function(){

		var store = Ext.create('tourism.store.chart.ChartStore',{
			
		});

		  Ext.apply(this,{
			  style: 'background:#fff',
	            animate: true,
	            shadow: true,
	            legend: {
	                position: 'right'
	            },
	            store: store,
	            axes: [{
	                type: 'Numeric',
	                position: 'left',
	                fields: ['count1','count2','count3','count4','count5'],
	                label: {
	                    renderer: Ext.util.Format.numberRenderer('0,0')
	                },
	                grid: true,
	                minimum: 0
	            }, {
	                type: 'Category',
	                position: 'bottom',
	                fields: ['licenseStatusName']
	            }],
	            series: [{
	                type: 'column',
	                axis: 'left',
	                highlight: true,
	                label: {
	                  	display: 'insideEnd',
	                  	'text-anchor': 'middle',
	                    field: ['count1','count2','count3','count4','count5'],
	                    renderer: Ext.util.Format.numberRenderer('0'),
	                    orientation: 'vertical',
	                    color: '#333'
	                },
	                xField: ['licenseStatusName'],
	                yField: ['count1','count2','count3','count4','count5'],
	                title: ['สำนักงานทะเบียนฯ กลาง', 'สาขาภาคเหนือ(เชียงใหม่)','สาขาภาคตะวันออกเฉียงเหนือ(นครราชสีมา)', 'สาขาภาคใต้ เขต ๑(สงขลา)', 'สาขาภาคใต้ เขต ๒(ภูเก็ต)']
	            }]
	        });
		  
		  this.callParent();
		}
	
		
	});
	
	
	
	
	
	
	
	
	
