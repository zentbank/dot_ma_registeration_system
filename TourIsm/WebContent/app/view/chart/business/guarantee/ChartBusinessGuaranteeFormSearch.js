Ext.define('tourism.view.chart.business.guarantee.ChartBusinessGuaranteeFormSearch' ,{
    extend: 'Ext.form.FormPanel',
    alias : 'widget.chart-business-guarantee-formsearch',
    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.Time',
        'Ext.form.CheckboxGroup',
        'Ext.layout.container.HBox',
        'Ext.form.FieldSet'
    ],
    bodyPadding: 10,
    // width: '100%',
    items: [
        {
                xtype: 'container',
                // title: 'Payment',
                layout: 'anchor',
                defaults: {
                    anchor: '70%',
                    labelWidth: 250
                    
                },
                items: [  
                        
						{
						    xtype: 'datefield',
						    fieldLabel: 'วันที่',
						    // labelWidth: 150,
						    name: 'approveDateFrom',
						    format: 'd/m/B'
						    ,allowBlank: false
						    ,afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>'
						},
						{
						    xtype: 'datefield',
						    fieldLabel: 'ถึงวันที่',
						    // labelWidth: 150,
						    name: 'approveDateTo',
						    format: 'd/m/B'
						}
                    
                ]
        },
        {
            xtype: 'toolbar',
            border: false,
            padding: '6px 0 6px 0px',
            items: 
            [
//            {
//                xtype: 'button',
//                text : 'ค้นหา',
//                action: 'searchBusinessAll'
//                ,chartName: 'columnbusinessrenewchart'
//            },
            {
                xtype: 'button',
                text : 'พิมพ์รายงาน',
                action: 'printExcel'
                ,chartName : 'excelbusinessguaranteecolumnchart'
                ,templateFileName: 'businessguaranteedetail.xls'
            }
            ]
        }

    ]
});    





