Ext.define('tourism.view.chart.business.newbusiness.ColumnBusinessNewChart',{
	extend: 'Ext.chart.Chart',
	alias: 'widget.chart-business-new-column',
	stripeRows: true,
	requires: [
		'Ext.chart.*',
		'Ext.Window', 
		'Ext.layout.container.Fit', 
		'Ext.fx.target.Sprite', 
		'Ext.window.MessageBox'
	],
	initComponent: function(){

		var store = Ext.create('tourism.store.chart.ChartStore',{
			
		});

		  Ext.apply(this,{
			  style: 'background:#fff',
	            animate: true,
	            shadow: true,
	            legend: {
	                position: 'right'
	            },
	            store: store,
	            axes: [{
	                type: 'Numeric',
	                position: 'left',
	                fields: ['outBoundCount','inBoundCount','countryCount','areaCount'],
	                label: {
	                    renderer: Ext.util.Format.numberRenderer('0,0')
	                },
//	                title: 'Number of Hits',
	                grid: true,
	                minimum: 0
	            }, {
	                type: 'Category',
	                position: 'bottom',
	                fields: ['orgName'],
//	                title: 'Month of the Year'
	            }],
	            series: [{
	                type: 'column',
	                axis: 'left',
	                highlight: true,
//	                tips: {
//	                  trackMouse: true,
//	                  width: 140,
//	                  height: 28,
//	                  renderer: function(storeItem, item) {
//	                    this.setTitle(storeItem.get('name') + ': ' + storeItem.get('data1') + ' $');
//	                  }
//	                },
	                label: {
	                  display: 'insideEnd',
	                  'text-anchor': 'middle',
	                    field: ['outBoundCount','inBoundCount','countryCount','areaCount'],
	                    renderer: Ext.util.Format.numberRenderer('0'),
	                    orientation: 'vertical',
	                    color: '#333'
	                },
	                xField: ['orgName'],
	                yField: ['outBoundCount','inBoundCount','countryCount','areaCount']
	                ,title: ['OUTBOUND', 'INBOUND','ในประเทศ', 'เฉพาะพื้นที่']
	            }]
	        });
		  
		  this.callParent();
		}
	
		
	});
	
	
	
	
	
	
	
	
	
