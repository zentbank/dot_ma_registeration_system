Ext.define('tourism.view.chart.business.renew.ColumnBusinessRenewChart',{
	extend: 'Ext.chart.Chart',
	alias: 'widget.chart-business-renew-column',
	stripeRows: true,
	requires: [
		'Ext.chart.*',
		'Ext.Window', 
		'Ext.layout.container.Fit', 
		'Ext.fx.target.Sprite', 
		'Ext.window.MessageBox'
	],
	initComponent: function(){

		var store = Ext.create('tourism.store.chart.ChartStore',{
			
		});

		  Ext.apply(this,{
			  style: 'background:#fff',
	            animate: true,
	            shadow: true,
	            legend: {
	                position: 'right'
	            },
	            store: store,
	            axes: [{
	                type: 'Numeric',
	                position: 'left',
	                fields: ['outBoundCount','inBoundCount','countryCount','areaCount'],
	                label: {
	                    renderer: Ext.util.Format.numberRenderer('0,0')
	                },
	                grid: true,
	                minimum: 0
	            }, {
	                type: 'Category',
	                position: 'bottom',
	                fields: ['orgName']
	            }],
	            series: [{
	                type: 'column',
	                axis: 'left',
	                highlight: true,
	                label: {
	                   display: 'insideEnd',
	                   'text-anchor': 'middle',
	                   field: ['outBoundCount','inBoundCount','countryCount','areaCount'],
	                   renderer: Ext.util.Format.numberRenderer('0'),
	                   orientation: 'vertical',
	                   color: '#333'
	                },
	                xField: ['orgName'],
	                yField: ['outBoundCount','inBoundCount','countryCount','areaCount']
	                ,title: ['ทั่วไป', 'นำเที่ยวจากต่างประเทศ','ในประเทศ', 'เฉพาะพื้นที่']
	            }]
	        });
		  
		  this.callParent();
		}
	
		
	});
	
	
	
	
	
	
	
	
	
