Ext.define('tourism.view.chart.business.suspensionAndrevoke.ChartBusinessSuspensionAndRevokeFormSearch' ,{
    extend: 'Ext.form.FormPanel',
    alias : 'widget.chart-business-suspensionandrevoke-formsearch',
    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.Time',
        'Ext.form.CheckboxGroup',
        'Ext.layout.container.HBox',
        'Ext.form.FieldSet'
    ],
    bodyPadding: 10,
    items: [
        {
                xtype: 'container',
                layout: 'anchor',
                defaults: {
                    anchor: '70%',
                    labelWidth: 250
                    
                },
                items: [  
                        
						{
						    xtype: 'datefield',
						    fieldLabel: 'วันที่',
						    name: 'approveDateFrom',
						    format: 'd/m/B'
						    ,allowBlank: false
						    ,afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>'
						},
						{
						    xtype: 'datefield',
						    fieldLabel: 'ถึงวันที่',
						    name: 'approveDateTo',
						    format: 'd/m/B'
						}
                    
                ]
        },
        {
            xtype: 'toolbar',
            border: false,
            padding: '6px 0 6px 0px',
            items: 
            [
            {
                xtype: 'button',
                text : 'พิมพ์รายงาน',
                action: 'printExcel',
                chartName : 'excelbusinesssuspensionandrevokecolumnchart',
                templateFileName: 'businesssuspensionandrevokedetail.xls'
            }
            ]
        }

    ]
});    





