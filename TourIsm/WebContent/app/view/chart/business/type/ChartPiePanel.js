Ext.define('tourism.view.chart.business.type.ChartPiePanel', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
    ],
    
    xtype: 'chart-pie-business-panel',
    
    initComponent: function(){
    	
    	var formSearch = this.getChartBusinessTraderCategoryFormSearch(this.roleAction, this.roleGroup, this.traderType);
    	
    	Ext.apply(this, {
    		layout: {
		        type: 'border'
		    }
		    ,items: [
		    {
		        region: 'north',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items:[formSearch],
		    	roleAction: this.roleAction,
		    	traderType: this.traderType,
		    	roleGroup: this.roleGroup
		    }
			,{
				region: 'center',
				layout: 'fit',
			    frame: false,
			    border: false,
				xtype: 'tabpanel',
	            plain: true,
	            frame: false,
	            border: false,
	            items: 
	            [
	            {
	            	title: 'การจดทะเบียน',
	            	xtype: 'chart-business-tradercategory-pie',
	            	id: 'chart-business-tradercategory-chart-pie'
	             }
	            ,{
	            	title: 'สาขา',
	            	xtype: 'chart-business-orgid-pie',
	            	id: 'chart-business-orgid-chart-pie'
	             }
	            ]
			}
		    ]
    	});

    	this.callParent(arguments);
    }
    ,getChartBusinessTraderCategoryFormSearch: function(roleAction, roleGroup, traderType)
    {
    	return {
    		
	            title:'ตารางแสดงจำนวนของธุรกิจนำเที่ยวจำแนกตามประเภท',
	            collapsible: true,
	            xtype: 'chart-business-tradercategory-formsearch',
	            id: roleAction+'-'+traderType+'-'+roleGroup+'-chart-formsearch',
	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false,
	            roleGroup : this.roleGroup,
	            traderType: this.traderType
    	};
    }
    
});








