Ext.define('tourism.view.chart.guide.all.ChartPiePanel', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
    ],
    
    xtype: 'chart-pie-guide-panel',
    
    initComponent: function(){
    	
    	var formSearch = this.getChartGuideAllFormSearch(this.roleAction, this.roleGroup, this.traderType);
    	
    	Ext.apply(this, {
    		layout: {
		        type: 'border'
		    }
		    ,items: [
		    {
		        region: 'north',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items:[formSearch]
		    	,roleAction: this.roleAction
		    	,traderType: this.traderType
		    	,roleGroup: this.roleGroup
		    }
			,{
				region: 'center',
				layout: 'fit',
			    frame: false,
			    border: false,
				xtype: 'tabpanel',
	            plain: true,
	            frame: false,
	            border: false,
	            items: 
	            [
	            {
	            	title: 'กราฟ',
	            	xtype: 'chart-guide-all-pie',
	            	id: 'chart-guide-all-chart-pie'
	             }
	            ,{
	            	title: 'แผนภูมิ',
	            	xtype: 'chart-guide-all-column',
	            	id: 'chart-guide-all-chart-column'
	             }
	            ]
			}
		    
		    ]
    	});

    	this.callParent(arguments);
    }
    ,getChartGuideAllFormSearch: function(roleAction, roleGroup, traderType)
    {
    	return {
    		
	            title:'ตารางแสดงจำนวนผู้ที่ได้รับใบอนุญาตเป็นมัคคุเทศก์ ในภาพรวม',
	            collapsible: true,   // make collapsible
	            // collapsed : true,
	            xtype: 'chart-guide-all-formsearch',
	            id: roleAction+'-'+traderType+'-'+roleGroup+'-chart-formsearch',
	            padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false
	            ,roleGroup : this.roleGroup
	            ,traderType: this.traderType
    	};
    }
    
});








