Ext.define('tourism.view.chart.guide.all.PieGuideAllChart',{
	extend: 'Ext.chart.Chart',
	alias: 'widget.chart-guide-all-pie',
	stripeRows: true,
	requires: [
		'Ext.chart.*',
		'Ext.Window', 
		'Ext.layout.container.Fit', 
		'Ext.fx.target.Sprite', 
		'Ext.window.MessageBox'
	],
	initComponent: function(){

		var store = Ext.create('tourism.store.chart.ChartStore',{
			
		});
		
//		store.load({
//			params: {chartName: 'piebusinessorgidchart'},
//            callback: function(records, operation, success) 
//            {
//            	//console.log(records);                            
//            },
//            scope: this
//        });
		
		var donut = false;
        
		  
		  Ext.apply(this,{
			  
			    animate: true,
	            store: store,
	            shadow: true,
	            legend: {
	                position: 'right'
//	                	,renderer: function(storeItem, item) {
		                    //calculate percentage.
//		                    var total = 0;
//		                    store.each(function(rec) {
//		                        total += rec.get('count');
//		                    });
//		                    this.setTitle(storeItem.get('traderCategoryName') + ': ' + Math.round(storeItem.get('count') / total * 100) + '%');
//	                		this.
//	                	}
	            },
	            insetPadding: 60,
	            theme: 'Base:gradients',
	            series: [{
	                type: 'pie',
	                field: 'count',
	                showInLegend: true,
	                donut: donut,
	                tips: {
	                  trackMouse: true,
	                  width: 250,
	                  height: 28,
	                  renderer: function(storeItem, item) {
	                    //calculate percentage.
	                    var total = 0;
	                    store.each(function(rec) {
	                        total += rec.get('count');
	                    });
	                    this.setTitle(storeItem.get('traderCategoryName') + ': ' + Math.round(storeItem.get('count') / total * 100) + '%');
	                  }
	                },
	                highlight: {
	                  segment: {
	                    margin: 20
	                  }
	                },
	                label: {
	                    field: 'traderCategoryName',
	                    display: 'rotate',
	                    contrast: true,
	                    font: '18px Arial'
	                }
	            }]
		  
		  });
		  
		  this.callParent();
		}
	
		
	});
	
	
	
	
	
	
	
	
	
