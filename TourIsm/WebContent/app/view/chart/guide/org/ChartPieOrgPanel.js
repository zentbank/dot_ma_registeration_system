Ext.define('tourism.view.chart.guide.org.ChartPieOrgPanel', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
    ],
    
    xtype: 'chart-pie-guide-org-panel',
    
    initComponent: function(){
    	
    	var formSearch = this.getChartGuideOrgFormSearch(this.roleAction, this.roleGroup, this.traderType);
    	
    	Ext.apply(this, {
    		layout: {
		        type: 'border'
		    }
		    ,items: [
		    {
		        region: 'north',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items:[formSearch]
		    	,roleAction: this.roleAction
		    	,traderType: this.traderType
		    	,roleGroup: this.roleGroup
		    }
			,{
				region: 'center',
				layout: 'fit',
			    frame: false,
			    border: false,
				xtype: 'tabpanel',
	            plain: true,
	            frame: false,
	            border: false,
	            items: 
	            [
	            {
	            	title: 'กราฟ',
	            	xtype: 'chart-guide-org-pie',
	            	id: 'chart-guide-org-chart-pie'
	             }
	            ,{
	            	title: 'แผนภูมิ',
	            	xtype: 'chart-guide-org-column',
	            	id: 'chart-guide-org-chart-column'
	             }
	            ]
			}
		    
		    ]
    	});

    	this.callParent(arguments);
    }
    ,getChartGuideOrgFormSearch: function(roleAction, roleGroup, traderType)
    {
    	return {
    		
	            title:'รายงานการจดทะเบียนมัคคุเทศก์จำแนกเป็นรายภาค',
	            collapsible: true,   // make collapsible
	            // collapsed : true,
	            xtype: 'chart-guide-org-formsearch',
	            id: roleAction+'-'+traderType+'-'+roleGroup+'-chart-formsearch',
	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false
	            ,roleGroup : this.roleGroup
	            ,traderType: this.traderType
    	};
    }
    
});








