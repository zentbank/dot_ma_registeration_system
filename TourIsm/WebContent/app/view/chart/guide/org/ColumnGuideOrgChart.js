Ext.define('tourism.view.chart.guide.org.ColumnGuideOrgChart',{
	extend: 'Ext.chart.Chart',
	alias: 'widget.chart-guide-org-column',
	stripeRows: true,
	requires: [
		'Ext.chart.*',
		'Ext.Window', 
		'Ext.layout.container.Fit', 
		'Ext.fx.target.Sprite', 
		'Ext.window.MessageBox'
	],
	initComponent: function(){

		var store = Ext.create('tourism.store.chart.ChartStore',{
			
		});
		
//		store.load({
//			params: {chartName: 'piebusinesstradercategorychart'},
//            callback: function(records, operation, success) 
//            {
//            	//console.log(records);                            
//            },
//            scope: this
//        });

		  Ext.apply(this,{
			  style: 'background:#fff',
	            animate: true,
	            shadow: true,
	            legend: {
	                position: 'right'
	            },
	            store: store,
	            axes: [{
	                type: 'Numeric',
	                position: 'left',
	                fields: ['count1','count2','count3','count4','count5'],
	                label: {
	                    renderer: Ext.util.Format.numberRenderer('0,0')
	                },
//	                title: 'Number of Hits',
	                grid: true,
	                minimum: 0
	            }, {
	                type: 'Category',
	                position: 'bottom',
	                fields: ['licenseStatusName'],
//	                title: 'Month of the Year'
	            }],
	            series: [{
	                type: 'column',
	                axis: 'left',
	                highlight: true,
//	                tips: {
//	                  trackMouse: true,
//	                  width: 140,
//	                  height: 28,
//	                  renderer: function(storeItem, item) {
//	                    this.setTitle(storeItem.get('name') + ': ' + storeItem.get('data1') + ' $');
//	                  }
//	                },
	                label: {
	                  display: 'insideEnd',
	                  'text-anchor': 'middle',
	                    field: ['count1','count2','count3','count4','count5'],
	                    renderer: Ext.util.Format.numberRenderer('0'),
	                    orientation: 'vertical',
	                    color: '#333'
	                },
	                xField: ['licenseStatusName'],
	                yField: ['count1','count2','count3','count4','count5']
//	                ,title: ['สำนักงานทะเบียนฯ กลาง', 'สาขาภาคเหนือ(เชียงใหม่)','สาขาภาคตะวันออกเฉียงเหนือ(นครราชสีมา)', 'สาขาภาคใต้ เขต ๑(สงขลา)', 'สาขาภาคใต้ เขต ๒(ภูเก็ต)']
	            }]
	        });
		  
		  this.callParent();
		}
	
		
	});
	
	
	
	
	
	
	
	
	
