Ext.define('tourism.view.chart.guide.renew.ColumnGuideRenewChart',{
	extend: 'Ext.chart.Chart',
	alias: 'widget.chart-guide-renew-column',
	stripeRows: true,
	requires: [
		'Ext.chart.*', 
		'Ext.Window', 
		'Ext.layout.container.Fit', 
		'Ext.fx.target.Sprite', 
		'Ext.window.MessageBox'
	],
	initComponent: function(){
		var store = Ext.create('tourism.store.chart.ChartStore',{
			
		});

		  Ext.apply(this,{
			  style: 'background:#fff',
	            animate: true,
	            shadow: true,
	            legend: {
	                position: 'right'
	            },
	            store: store
	            
	        });
		  
		  this.callParent();
		}
	
		
	});
	
	
	
	
	
	
	
	
	
