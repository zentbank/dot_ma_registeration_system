Ext.define('tourism.view.chart.guide.type.ChartColumnPanel', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
    ],
    
    xtype: 'chart-column-guide-panel',
    
    initComponent: function(){
    	
    	var formSearch = this.getChartColumnGuideTypeFormSearch(this.roleAction, this.roleGroup, this.traderType);
    	
    	if(this.roleGroup == 'new')
    	{
    		formSearch = this.getChartColumnGuideNewFormSearch(this.roleAction, this.roleGroup, this.traderType);
    	}
    	
    	if(this.roleGroup == 'renew')
    	{
    		formSearch = this.getChartColumnGuideRenewFormSearch(this.roleAction, this.roleGroup, this.traderType);
    	}
    	
    	
    	Ext.apply(this, {
    		layout: {
		        type: 'border'
		    }
		    ,items: [
		    {
		        region: 'north',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items:[formSearch],
		    	roleAction: this.roleAction,
		    	traderType: this.traderType,
		    	roleGroup: this.roleGroup
		    }
		    ,{
		    	region: 'center',
		        xtype: this.roleAction+'-'+this.traderType+'-'+this.roleGroup+'-column',
		        id: this.roleAction+'-'+this.traderType+'-'+this.roleGroup+'-chart-column',
		        layout: 'fit',
		        frame: false,
		        border: false
		    }
		    ]
    	});

    	this.callParent(arguments);
    }
    ,getChartColumnGuideTypeFormSearch: function(roleAction, roleGroup, traderType)
    {
    	return {
    		
	            title:'ตารางแสดงภาพรวม มัคคุเทศก์ ทั้งส่วนกลาง และสาขา จำแนกตามประเภท',
	            collapsible: true,   // make collapsible
	            // collapsed : true,
	            xtype: roleAction+'-'+traderType+'-'+roleGroup+'-formsearch',
	            id: roleAction+'-'+traderType+'-'+roleGroup+'-chart-formsearch',
	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false
	            ,roleGroup : this.roleGroup
	            ,traderType: this.traderType
    	};
    }
    ,getChartColumnGuideNewFormSearch: function(roleAction, roleGroup, traderType)
    {
    	return {
    		
	            title:'สรุปจำนวนผู้ขอรับใบอนุญาตเป็นมัคคุเทศก์(ยื่นใหม่) จำแนกตามประเภทใบอนุญาต และสำนักทะเบียนฯ',
	            collapsible: true,   // make collapsible
	            // collapsed : true,
	            xtype: roleAction+'-'+traderType+'-'+roleGroup+'-formsearch',
	            id: roleAction+'-'+traderType+'-'+roleGroup+'-chart-formsearch',
	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false
	            ,roleGroup : this.roleGroup
	            ,traderType: this.traderType
    	};
    }
    ,getChartColumnGuideRenewFormSearch: function(roleAction, roleGroup, traderType)
    {
    	return {
    		
	            title:'มัคคุเทศก์ ต่ออายุ',
	            collapsible: true,   // make collapsible
	            // collapsed : true,
	            xtype: roleAction+'-'+traderType+'-'+roleGroup+'-formsearch',
	            id: roleAction+'-'+traderType+'-'+roleGroup+'-chart-formsearch',
	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false
	            ,roleGroup : this.roleGroup
	            ,traderType: this.traderType
    	};
    }
    
});








