/*
{
     // หรือสรรพากรเขตพื้นที่
    // xtype: 'combo',
    xtype: 'masamphurcombo',
    fieldLabel: 'หรือสรรพากรเขตพื้นที่',
    labelAlign: 'top',
    id : 'business-person-individual-masamphur-combo',
    store: 'tourism.store.combo.MasAmphurStore',
    // queryMode: 'remote',
    displayField: 'amphurName',
    valueField: 'amphurId',
    hiddenName: 'amphurId',
    // triggerAction: 'all',
    allowBlank: false,
    afterLabelTextTpl: required,
    queryParam: 'amphurName',
    masprovinceComboId: 'business-person-individual-masprovince-combo'
   
}// end หรือสรรพากรเขตพื้นที่ 
*/
Ext.define('tourism.view.combo.MasAmphurCombo' ,{
    extend: 'Ext.form.field.ComboBox',
    alias : 'widget.masamphurcombo',
    initComponent: function()
    {

         Ext.apply(this, {
            queryMode: 'remote',
            triggerAction: 'all',
            typeAhead: true,
            listeners: {
                select: {
                    fn: function( combo, records, eOpts )
                    { 
                        var mastambolCombo = Ext.getCmp(this.mastambolComboId);

                        if(!Ext.isEmpty(mastambolCombo))
                        {
                            mastambolCombo.clearValue();
                            mastambolCombo.setRawValue("");

                            mastambolCombo.getStore().load({
                                params: {amphurId: combo.getValue()},
                                callback: function(records, operation, success) {
                                    // //console.log(success);
                                },
                                scope: this
                            }); 

                        }
                        

                    }
                }
                ,beforequery : {
                    fn: function( queryPlan, eOpts)
                    {
                    // queryPlan.provinceId = 10;
                    var masprovinceCombo = Ext.getCmp(this.masprovinceComboId);

                    if(!Ext.isEmpty(masprovinceCombo.getValue()))
                    {
                        queryPlan.combo.store.proxy.extraParams = {
                          provinceId: masprovinceCombo.getValue()
                        } 
                    }
                                                                  
                    }
                }
            }
        
        });

        this.callParent();
    }

});