/*
{
     // หรือสรรพากรเขตพื้นที่
    // xtype: 'combo',
    xtype: 'masamphurcombo',
    fieldLabel: 'หรือสรรพากรเขตพื้นที่',
    labelAlign: 'top',
    id : 'business-person-individual-masamphur-combo',
    store: 'tourism.store.combo.MasAmphurStore',
    // queryMode: 'remote',
    displayField: 'amphurName',
    valueField: 'amphurId',
    hiddenName: 'amphurId',
    // triggerAction: 'all',
    allowBlank: false,
    afterLabelTextTpl: required,
    queryParam: 'amphurName',
    masprovinceComboId: 'business-person-individual-masprovince-combo'
   
}// end หรือสรรพากรเขตพื้นที่ 
*/
Ext.define('tourism.view.combo.MasPrefixCombo' ,{
    extend: 'Ext.form.field.ComboBox',
    alias : 'widget.masprefixcombo',
    initComponent: function()
    {

         Ext.apply(this, {
            queryMode: 'remote',
            triggerAction: 'all',
            typeAhead: true,
            listeners: {
                beforequery : {
                    fn: function( queryPlan, eOpts)
                    {
                      
                        if(!Ext.isEmpty(queryPlan.combo.prefixType))
                        {
                            queryPlan.combo.store.proxy.extraParams = {
                              prefixType: queryPlan.combo.prefixType
                            } 
                        
                        }
                   
                    }
                }
            }
        
        });

        this.callParent();
    }

});