Ext.define('tourism.view.combo.MasProvinceCombo' ,{
    extend: 'Ext.form.field.ComboBox',
    alias : 'widget.masprovincecombo',
    initComponent: function()
    {

         Ext.apply(this, {
            queryMode: 'remote',
            triggerAction: 'all',
            typeAhead: true,
            listeners: {
                select: {
                    fn: function( combo, records, eOpts )
                    { 
                        var masamphurCombo = Ext.getCmp(this.masamphurComboId);

                        if(!Ext.isEmpty(masamphurCombo))
                        {
                            masamphurCombo.clearValue();
                            masamphurCombo.setRawValue("");

                            masamphurCombo.getStore().load({
                                params: {provinceId: combo.getValue()},
                                callback: function(records, operation, success) {
                                    // //console.log(success);
                                },
                                scope: this
                            }); 
                        }

                    }
                }
            }
        
        });

        this.callParent(arguments);
    }

});