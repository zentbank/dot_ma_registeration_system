/*
,{
     // หรือสรรพากรเขตพื้นที่
    // xtype: 'combo',
    xtype: 'mastambolcombo',
    fieldLabel: 'sd',
    labelAlign: 'top',
    id : 'business-person-individual-tambol-combo',
    store: 'tourism.store.combo.MasTambolStore',
    // queryMode: 'remote',
    displayField: 'tambolName',
    valueField: 'tambolId',
    hiddenName: 'tambolId',
    // triggerAction: 'all',
    allowBlank: false,
    afterLabelTextTpl: required,
    queryParam: 'tambolName',
    masamphurComboId: 'business-person-individual-masamphur-combo'
   
}// end หรือสรรพากรเขตพื้นที่ 
*/
Ext.define('tourism.view.combo.MasTambolCombo' ,{
    extend: 'Ext.form.field.ComboBox',
    alias : 'widget.mastambolcombo',
    initComponent: function()
    {

         Ext.apply(this, {
            queryMode: 'remote',
            triggerAction: 'all',
            typeAhead: true,
            listeners: {
                beforequery : {
                    fn: function( queryPlan, eOpts)
                    {
                        // queryPlan.provinceId = 10;
                        var masamphurCombo = Ext.getCmp(this.masamphurComboId);

                        if(!Ext.isEmpty(masamphurCombo.getValue()))
                        {
                            queryPlan.combo.store.proxy.extraParams = {
                              amphurId: masamphurCombo.getValue()
                            } 
                        }
                                                                  
                    }
                }
            }
        
        });

        this.callParent();
    }

});