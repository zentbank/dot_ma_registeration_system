Ext.define('tourism.view.combo.MasUniversityCombo' ,{
    extend: 'Ext.form.field.ComboBox',
    alias : 'widget.masuniversitycombo',
    initComponent: function()
    {

         Ext.apply(this, {
            queryMode: 'remote',
            triggerAction: 'all',
            typeAhead: true,
            listeners: {
                select: {
                    fn: function( combo, records, eOpts )
                    { 
                        var masCourseCombo = Ext.getCmp(this.masCourseComboId);

                        if(!Ext.isEmpty(masCourseCombo))
                        {
                        	masCourseCombo.clearValue();
                        	masCourseCombo.setRawValue("");
                        	console.log("masUniversityId: "+combo.getValue());
                        	masCourseCombo.getStore().load({
                        		params: {masUniversityId: combo.getValue()},
                                callback: function(records, operation, success) {
                                    // //console.log(success);
                                },
                                scope: this
                            }); 
                        }

                    }
                }
            }
        
        });

        this.callParent(arguments);
    }

});