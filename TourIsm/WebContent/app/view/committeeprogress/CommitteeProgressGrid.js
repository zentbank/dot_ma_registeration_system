Ext.define('tourism.view.committeeprogress.CommitteeProgressGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.committeeprogress-grid',
		stripeRows: true,
		requires: [
			'Ext.grid.Column',
			'Ext.toolbar.Paging',
			'Ext.grid.plugin.RowEditing',
			'Ext.grid.column.Template',
			'Ext.grid.column.Action',
			'Ext.grid.plugin.RowExpander',
			'tourism.store.registration.CommitteeProgressStore'
		],
		initComponent: function(){
			var committeeProgressStore = Ext.create('tourism.store.registration.CommitteeProgressStore',{
				
			});

			Ext.apply(this, {
				store: committeeProgressStore,
				
			});

			this.callParent(arguments);


		},
		columns: [
			{
				header: ' ',
				dataIndex: 'committeeStatusName',
				flex: 1
			}
			,{
	        	header: '',
	            xtype: 'actioncolumn',
	           	width:50,
	           	align : 'center',
	            items: [{
	                // iconCls: 'icon-send-xsmall',
	                // tooltip: 'การผ่านเรื่อง',
	                getClass: function(v, meta, rec) {          
	                    if (rec.get('committeeStatus') == 'N') {
	                        return 'icon-checked-xsmall';
	                    }if (rec.get('committeeStatus') == 'R') {
	                        return 'icon-page_warning-xsmall';
	                    } if (rec.get('committeeStatus') == 'S') {
	                        return 'icon-page_warning-xsmall';
	                    } 
	                }
	            }]
	        }
		],
		 plugins: [{
            ptype: 'rowexpander',
            rowBodyTpl : new Ext.XTemplate(
            	'<div>',
            		'<p><b>{committeeStatusName}</b></p>',
            		'<p><b>{committeeOwnerName}</b></p>',
            		'<p><b>{period}</b></p>',
            		'<p><b>ระหว่างวันที่&nbsp;{punishmentDateCommittee}&nbsp;ถึงวันที่&nbsp;{punishmentExpireDateCommittee}</b></p>',
	            	//'<p><b>โดย&nbsp;{authorityName}</b></p>',
                '<div>'
            )
        }]
	});