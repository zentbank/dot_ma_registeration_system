Ext.define('tourism.view.committeeprogress.CommitteeProgressWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.committeeprogress-window',

    requires: [
        'tourism.view.committeeprogress.CommitteeProgressGrid'
    ],

    title : 'สถานะของผู้ขอหรือกรรมการของบริษัทจดทะเบียนใบอนุญาต',
    layout: 'fit',
    // autoShow: true,
    width: 400,
    height: 500,
    //True to make the window modal and mask everything behind it when displayed
    modal: true,
    // iconCls: 'icon-form',

    initComponent: function() {

        this.items = [
                {
                    xtype: 'committeeprogress-grid',
                    frame: false,
                    border: false
                }
        ];

        this.callParent(arguments);
    }
});
