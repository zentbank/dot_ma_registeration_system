Ext.define('tourism.view.complaint.AddWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.add-window',

    requires: [
//        'tourism.view.info.SearchLicensePanel',
		'tourism.view.complaint.SearchLicensePanel',
        'tourism.view.punishment.suspension.AddSuspensionForm',
        'tourism.view.punishment.revoke.AddRevokeForm',
        'tourism.view.punishment.evidence.EvidenceGrid',
        'tourism.view.punishment.evidence.EvidenceWindow'
    ],
    
    initComponent: function() {

        Ext.apply(this, {
            title:'ค้นหาใบอนุญาต',
            // layout: 'fit',
            layout: 'card',
            activeItem: 0,
            autoShow: true,
            width: 750,
            height: 550,
            autoShow: true,
            modal: true,
            items :[
                {
                    // title: 'เลือกใบอนุญาต',
                    xtype: 'info-license-complaint-searchlicensepanel',
                    id: 'info-license-complaint-info-searchlicensepanel',
                    border: false,
                    frame: false
                }//,formModel
            ]
        });

        this.callParent(arguments);
    },

});
