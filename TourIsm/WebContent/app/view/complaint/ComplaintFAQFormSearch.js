Ext.define('tourism.view.complaint.ComplaintFAQFormSearch' ,{
    extend: 'Ext.form.FormPanel',
    alias : 'widget.complaint-faq-formsearch',
    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.Time',
        'Ext.form.CheckboxGroup',
        'Ext.layout.container.HBox',
        'Ext.form.FieldSet'
    ],
    bodyPadding: 10,
    // width: '100%',
    items: [
        {
                xtype: 'container',
                // title: 'Payment',
                layout: 'anchor',
                defaults: {
                    anchor: '100%',
                    labelWidth: 150
                },
                items: [      
                    {
                        xtype: 'container',
                        layout: 'hbox',
                        layoutCongig: {
                             pack:'center',
                             align:'middle'
                        },
                        items: [
                            {
                                xtype: 'container',
                                // title: 'Payment',
                                flex: 1,
                                layout: 'anchor',
                                defaults: {
                                    anchor: '100%',
                                    labelWidth: 150
                                },
                                items: [
                                    {
                                    	xtype: 'datefield',
                                        fieldLabel: 'วันที่ซักถามตั้งแต่วันที่',
                                        // labelWidth: 150,
                                        name: 'complaintDateFrom',
                                        format: 'd/m/B'
                                    },
                                    {
                                    	xtype: 'textfield',
                                        fieldLabel: 'รายละเอียดเรื่อง',
                                        name: 'complaintDesc'
                                    }
//                                    ,{
//                                    	xtype: 'combo',
//                                        fieldLabel: 'ประเภทใบอนุญาต',
//                                        store: 'tourism.store.combo.TraderTypeStore',
//                                        queryMode: 'local',
//                                        displayField: 'traderTypeName',
//                                        valueField: 'traderType',
//                                        hiddenName: 'traderType',
//                                        name :'traderType'
//                                    }
                                ]
                            },{
                                xtype: 'splitter'
                            },
                            {
                                xtype: 'container',
                                flex: 1,
                                layout: 'anchor',
                                defaults: {
                                    anchor: '100%',
                                    labelWidth: 200
                                },
                                items: [
                                    {
                                    	xtype: 'datefield',
                                        fieldLabel: 'ถึงวันที่',
                                        name: 'complaintDateTo',
                                        format: 'd/m/B'
                                    }
                                     ,{
                                        xtype: 'combo',
                                        fieldLabel: 'สถานะ',
                                        store: 'tourism.store.combo.ComplaintFAQStatus',
                                        queryMode: 'remote',
                                        displayField: 'complaintStatusName',
                                        valueField: 'complaintStatus',
                                        hiddenName: 'complaintStatus',
                                        name :'complaintStatus'
                                    }
                                ]
                            }
                        ]
                    }
                    
                ]
        },
        {
            xtype: 'toolbar',
            border: false,
            padding: '6px 0 6px 0px',
            items: [{
                xtype: 'button',
                text : 'ค้นหา',
                action: 'searchQuestion'
            }]
        }

    ]
});    





