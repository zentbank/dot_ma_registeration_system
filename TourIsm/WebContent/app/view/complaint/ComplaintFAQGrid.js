Ext.define('tourism.view.complaint.ComplaintFAQGrid',{
	extend: 'Ext.grid.GridPanel',
	alias: 'widget.complaint-faq-grid',
	stripeRows: true,
	requires: [
		'Ext.grid.Column',
		'Ext.toolbar.Paging',
		'Ext.grid.plugin.RowEditing',
		'Ext.grid.column.Template',
		'Ext.grid.column.Action',
		'Ext.grid.plugin.RowExpander',
		'Ext.grid.feature.Grouping'
		,'tourism.store.grid.ComplaintLicenseStore'
		
		,'tourism.view.complaint.question.QuestionAddEditWindow'
		,'tourism.view.complaint.question.QuestionInfoWindow'
	]
	,initComponent: function(){
		
		var complaintLicenseStore = Ext.create('tourism.store.grid.ComplaintLicenseStore',{
			storeId: this.storeId
		});
		
		Ext.apply(this, {
			store: complaintLicenseStore
			,columns: [
			{
				header: 'ชื่อ-นามสกุลผู้ซักถาม',
				dataIndex: 'complaintName',
				flex: 2
			},
			{
				header: 'หมายเลขโทรศัพท์',
				dataIndex: 'complaintTel',
				flex: 1
			},
			{
				header: 'วันที่ซักถาม',
				dataIndex: 'complaintDate',
				flex: 1
			},
			{
				header: 'วันที่ตอบข้อซักถาม',
				dataIndex: 'authorityDate',
				flex: 1
			},
			{
				header: 'สถานะ',
				dataIndex: 'complaintStatusName',
				flex: 1
			}
			, {
	        	header: '',
	            xtype: 'actioncolumn',
	           	width:50,
	           	align : 'center',
	            items: [{
//	                iconCls: 'icon-edit-xsmall',
//	                tooltip: 'รายละเอียด',
	                getClass: function(v, meta, rec) {          
	                   if (rec.get('viewData')) {
	                	   this.items[0].tooltip = 'รายละเอียด';
	                       return 'icon-notes-xsmall';   
	                   }
	                   else
                	   {
	                       this.items[0].tooltip = 'แก้ไขข้อมูล';
                           return 'icon-edit-xsmall';
                	   }
		                   
		            },
		            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
		            	
		            	if(record.get("viewData"))
	            		{
		            		this.fireEvent('itemclick', this, 'viewQuestion', grid, rowIndex, colIndex, record, node);
	            		}   
		            	else
	            		{
		            		this.fireEvent('itemclick', this, 'editQuestion', grid, rowIndex, colIndex, record, node);
	            		}
		            }
	            }]
	        }
			]
		
			,dockedItems:[
			{
				xtype: 'pagingtoolbar',
				store: complaintLicenseStore,
				dock: 'bottom',
				displayInfo: true

			}
			]
			
		});
		
		this.callParent(arguments);
		
	}
	
//	,plugins: [{
//	   ptype: 'rowexpander',
//	   pluginId: 'rowexpanderTourleader',
//	   rowBodyTpl : new Ext.XTemplate(
//	   	'<div id="tpl-rowexpander-document-registration-{regId}">',
//	            '<p><b>ออกใบอนุญาตให้: {traderOwnerName}</b></p>',
//	       		'<p><b>ทะเบียนนิติบุคคลเลขที่: {identityNo}</b></p>',
//	       		'<p><b>ประเภทธุรกิจนำเที่ยว: {traderCategoryName}</b></p>',
//	           	'<p><b>ชื่อธุรกิจนำเที่ยว (ภาษาไทย): {traderName}</b></p>',
//	            '<p><b>ชื่อธุรกิจนำเที่ยว (ภาษาต่างประเทศ): {traderNameEn}</b> </p>',
//	            '<p><font size="2">{traderAddress}</font></p>',
//	   		
//	       '<div>'
//	   )
//	}]
//	
//    ,features: [{
//        ftype: 'grouping',
//        groupHeaderTpl: '{name} ({rows.length} รายการ)',
//        hideGroupedHeader: true,
//        startCollapsed: false
//    }]
	
});











