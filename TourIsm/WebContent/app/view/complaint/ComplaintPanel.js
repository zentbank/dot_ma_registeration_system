Ext.define('tourism.view.complaint.ComplaintPanel', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
    ],
    
    xtype: 'complaint-panel',
    initComponent: function(){
    	
    	var formSearch = this.getFAQFormSearch(this.roleAction, this.roleGroup, this.complaintProgress);

    	if(this.roleGroup == 'receive')
    	{
    		formSearch = this.getReceiveFormSearch(this.roleAction, this.roleGroup, this.complaintProgress);
    	}
    	
    	if(this.roleGroup == 'consider')
    	{
    		formSearch = this.getConsiderFormSearch(this.roleAction, this.roleGroup, this.complaintProgress);
    	}
    	Ext.apply(this, {
    		layout: {
		        type: 'border'
		    }
		    ,items: [
		    {
		        region: 'north',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items:[formSearch]
		    }
		    ,{
		        region: 'center',
		        xtype: 'panel',
		        layout: 'fit',
		        frame: false,
		        border: false,
		        items:[{
		            // title: 'การทำรายการใบอนุญาตธุรกิจนำเที่ยว',
		            xtype: this.roleAction+'-'+this.roleGroup+'-grid',
		            id: this.roleAction+'-'+this.roleGroup+'-'+this.complaintProgress+'-complaint-grid',
		            // padding: '5px 5px 5px 5px',
		            autoScroll: true,
		            // frame: false 
		            border: false
	                ,complaintProgress : this.complaintProgress
	                ,roleGroup : this.roleGroup
		        }]
		    }
		    ]
    	});

    	this.callParent(arguments);
    }
    ,getFAQFormSearch: function(roleAction, roleGroup, complaintProgress)
    {
    	return {
    		
	            title:'ค้นหาตอบข้อซักถาม',
	            collapsible: true,   // make collapsible
	            // collapsed : true,
	            xtype: 'complaint-faq-formsearch',
	            id: roleAction+'-'+roleGroup+'-'+complaintProgress+'-complaint-formsearch',
	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false
	            ,complaintProgress : this.complaintProgress
    	};
    }
    ,getReceiveFormSearch: function(roleAction, roleGroup, complaintProgress)
    {
    	return {
    		
	            title:'ค้นหารับเรื่องร้องเรียน',
	            collapsible: true,   // make collapsible
	            // collapsed : true,
	            xtype: 'complaint-receive-formsearch',
	            id: roleAction+'-'+roleGroup+'-'+complaintProgress+'-complaint-formsearch',
	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false
	            ,complaintProgress : this.complaintProgress
    	};
    }
    ,getConsiderFormSearch: function(roleAction, roleGroup, complaintProgress){
    	return {
    		
            title:'ค้นหาพิจารณาเรื่องร้องเรียน',
            collapsible: true,   // make collapsible
            // collapsed : true,
            xtype: 'complaint-consider-formsearch',
            id: roleAction+'-'+roleGroup+'-complaint-formsearch',
            // padding: '5px 5px 0px 5px',
            frame: false,
            border: false
            ,complaintProgress : this.complaintProgress
    	};
    }
});








