Ext.define('tourism.view.complaint.ComplaintReceiveFormSearch' ,{
    extend: 'Ext.form.FormPanel',
    alias : 'widget.complaint-receive-formsearch',
    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.Time',
        'Ext.form.CheckboxGroup',
        'Ext.layout.container.HBox',
        'Ext.form.FieldSet'
    ],
    bodyPadding: 10,
    // width: '100%',
    items: [
        {
                xtype: 'container',
                // title: 'Payment',
                layout: 'anchor',
                defaults: {
                    anchor: '70%',
                    labelWidth: 250
                    
                },
                items: [      
//                    {
//                        xtype: 'container',
//                        layout: 'hbox',
//                        layoutCongig: {
//                             pack:'center',
//                             align:'middle'
//                        },
//                        items: [
//                            {
//                                xtype: 'container',
//                                // title: 'Payment',
//                                flex: 1,
//                                layout: 'anchor',
//                                defaults: {
//                                    anchor: '100%',
//                                    labelWidth: 150
//                                },
//                                items: [
                                    {
                                    	xtype: 'textfield',
                                		fieldLabel: 'เลขที่ใบอนุญาต',
                                        name: 'licenseNo',	
                                    }
                                    ,{
                                    	xtype: 'combo',
                                        fieldLabel: 'ประเภทใบอนุญาต',
                                        store: 'tourism.store.combo.TraderTypeStore',
                                        queryMode: 'local',
                                        displayField: 'traderTypeName',
                                        valueField: 'traderType',
                                        hiddenName: 'traderType',
                                        name :'traderType'
                                    }
                                    ,{
                                    	xtype: 'textfield',
                                		fieldLabel: 'ชื่อธุรกิจนำเที่ยว/มัคคุเทศก์/ผู้นำเที่ยว',
                                        name: 'traderName',	
                                    }
                                    ,{
                                    	xtype: 'textfield',
                                		fieldLabel: 'ชื่อผู้ร้องเรียน',
                                        name: 'complaintName',	
                                    }
                                    ,{
                                    	xtype: 'combo',
                                        fieldLabel: 'ประเภทเรื่องร้องเรียน',
                                        store: 'tourism.store.combo.MasComplaintTypeStore',
                                        queryMode: 'remote',
                                        displayField: 'complaintTypeName',
                                        valueField: 'masComplaintTypeId',
                                        hiddenName: 'masComplaintTypeId',
                                        name :'masComplaintTypeId'
                                    }
                                    ,{
                                        xtype: 'combo',
                                        fieldLabel: 'สถานะ',
                                        store: 'tourism.store.combo.ComplaintStatus',
                                        queryMode: 'remote',
                                        displayField: 'complaintStatusName',
                                        valueField: 'complaintStatus',
                                        hiddenName: 'complaintStatus',
                                        name :'complaintStatus'
                                    }
                                    ,{
                                    	xtype: 'datefield',
                                        fieldLabel: 'วันที่ร้องเรียนจากวันที่',
                                        // labelWidth: 150,
                                        name: 'complaintDateFrom',
                                        format: 'd/m/B'
                                    }
                                    ,{
                                    	xtype: 'datefield',
                                        fieldLabel: 'ถึงวันที่',
                                        name: 'complaintDateTo',
                                        format: 'd/m/B'
                                    }
                                    
//                                ]
//                            },{
//                                xtype: 'splitter'
//                            },
//                            {
//                                xtype: 'container',
//                                flex: 1,
//                                layout: 'anchor',
//                                defaults: {
//                                    anchor: '100%',
//                                    labelWidth: 200
//                                },
//                                items: [
//                                    {
//                                    	xtype: 'combo',
//                                        fieldLabel: 'ประเภทใบอนุญาต',
//                                        store: 'tourism.store.combo.TraderTypeStore',
//                                        queryMode: 'local',
//                                        displayField: 'traderTypeName',
//                                        valueField: 'traderType',
//                                        hiddenName: 'traderType',
//                                        name :'traderType'
//                                    }
//                                ]
//                            }
//                        ]
//                    }
                    
                ]
        },
        {
            xtype: 'toolbar',
            border: false,
            padding: '6px 0 6px 0px',
            items: [{
                xtype: 'button',
                text : 'ค้นหา',
                action: 'searchComplaintReceive'
            }]
        }

    ]
});    





