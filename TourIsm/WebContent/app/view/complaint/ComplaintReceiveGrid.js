Ext.define('tourism.view.complaint.ComplaintReceiveGrid',{
	extend: 'Ext.grid.GridPanel',
	alias: 'widget.complaint-receive-grid',
	stripeRows: true,
	requires: [
		'Ext.grid.Column',
		'Ext.toolbar.Paging',
		'Ext.grid.plugin.RowEditing',
		'Ext.grid.column.Template',
		'Ext.grid.column.Action',
		'Ext.grid.plugin.RowExpander',
		'Ext.grid.feature.Grouping'
//		,'tourism.store.registration.RegistrationStore'
		,'tourism.view.complaint.viewcomplaint.ComplaintInfoWindow'
		,'tourism.view.complaint.addcomplaint.ComplaintAddEditWindow'
		,'tourism.view.complaint.complaintprogress.ComplaintProgressWindow'
	]
	,initComponent: function(){
		
		var complaintLicenseStore = Ext.create('tourism.store.grid.ComplaintLicenseStore',{
			storeId: this.storeId
//			,pageSize : this.storepageSize,
//			proxy: {
//				type: 'ajax',
//				actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
//				api: {
//				    read: this.storeUrl
//				},
//				reader: {
//				    type: 'json',
//				    root: 'list',
//				    totalProperty: 'totalCount',
//				    successProperty: 'success',
//				    messageProperty: 'msg'
//				},
//				listeners: {
//				    exception: function(proxy, response, operation){
//
//				        Ext.MessageBox.show({
//				            title: 'REMOTE EXCEPTION',
//				            msg: operation.getError(),
//				            icon: Ext.MessageBox.ERROR,
//				            buttons: Ext.Msg.OK
//				        });
//				    }
//				}
//			}
		});
		
		Ext.apply(this, {
			store: complaintLicenseStore
			,columns: [
			{
				header: 'regId',
				dataIndex: 'regId',
				hidden: true
			},
			{
				header: 'เลขที่ใบอนุญาต',
				dataIndex: 'licenseNo',
				flex: 1
			},
			{
				header: 'ชื่อผู้ถูกร้องเรียน',
				dataIndex: 'traderName',
				flex: 2
			},
			{
				header: 'ชื่อผู้ร้องเรียน',
				dataIndex: 'complaintName',
				flex: 2
			},
			{
				header: 'วันที่ร้องเรียน',
				dataIndex: 'complaintDate',
				flex: 1
			},
			{
				header: 'สถานะ',
				dataIndex: 'complaintStatusName',
				flex: 1
			}
			
			,{
				header: '',
	            xtype: 'actioncolumn',
	           	width:50,
	           	align : 'center',
	            items: [{
	            	
//	                iconCls: 'icon-notes-xsmall',
//	                tooltip: 'รายละเอียด',
	                
	                getClass: function(v, meta, rec) {        
	                	   this.items[0].tooltip = 'สถานะ';
	                       return 'icon-comment-xsmall';   
		                   
		            },
		            
		            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
		            	this.fireEvent('itemclick', this, 'viewcomplaintprogress', grid, rowIndex, colIndex, record, node);
		            }

	            }]
			}
			
			, {
	        	header: '',
	            xtype: 'actioncolumn',
	           	width:50,
	           	align : 'center',
	            items: [{
//	                iconCls: 'icon-edit-xsmall',
//	                tooltip: 'รายละเอียด',
	                getClass: function(v, meta, rec) {          
	                   if (rec.get('viewData')) {
	                	   this.items[0].tooltip = 'รายละเอียด';
	                       return 'icon-notes-xsmall';   
	                   }
	                   else
                	   {
	                       this.items[0].tooltip = 'แก้ไขข้อมูล';
                           return 'icon-edit-xsmall';
                	   }
		                   
		            },
		            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
		            	
		            	if(record.get("viewData"))
	            		{
		            		this.fireEvent('itemclick', this, 'viewcomplaintreceive', grid, rowIndex, colIndex, record, node);
	            		}   
		            	else
	            		{
		            		this.fireEvent('itemclick', this, 'editcomplaintreceive', grid, rowIndex, colIndex, record, node);
	            		}
		            }
	            }]
	        }
			]
		
			,dockedItems:[
			{
				xtype: 'pagingtoolbar',
				store: complaintLicenseStore,
				dock: 'bottom',
				displayInfo: true
				,items:[
					{
						text: 'เพิ่มเรื่องร้องเรียน'
//							,action: 'addComplaint'
						,action: 'popupSearchLicense'
						,complaintProgress : this.complaintProgress
						,roleGroup : this.roleGroup
						,hidden: this.complaintProgress == 'ML'?true:false
					}
        		]
			}
			]
			
		});
		
		this.callParent(arguments);
		
	}
	
});











