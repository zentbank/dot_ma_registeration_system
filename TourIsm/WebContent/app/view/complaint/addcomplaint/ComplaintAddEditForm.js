Ext.define('tourism.view.complaint.addcomplaint.ComplaintAddEditForm',{
	extend: 'Ext.form.Panel',
    requires: [
       'Ext.data.*',
       'Ext.form.*',
       'Ext.tab.Tab'
       
       ,'tourism.view.complaint.addcomplaint.ComplaintDocumentGrid'
       ,'tourism.store.combo.MasComplaintTypeStore'  
       ,'tourism.store.combo.AdmUserStore'
    ],
    xtype: 'complaint-addedit-form',
    bodyPadding: 5,
    autoScroll: true,
    initComponent: function(){
        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

        var officerStore = Ext.create('tourism.store.combo.AdmUserStore',{
            id:'complaint-officer-store'
            ,proxy: {
                type: 'ajax',
                actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
                api: {
                    read: 'tourleader/combo/officergroup'
                },
                reader: {
                    type: 'json',
                    // metaProperty: '',
                    root: 'list',
                    // idProperty: 'emailId',
                    totalProperty: 'totalCount',
                    successProperty: 'success',
                    messageProperty: 'msg'
                },
                writer: {
                    type: 'json',
                    // encode: true,
                    writeAllFields: true, ////just send changed fields
                    // root: 'data',
                    allowSingle: false, //always wrap in an array
                    batch: false
                },
                listeners: {
                    exception: function(proxy, response, operation){

                        Ext.MessageBox.show({
                            title: 'REMOTE EXCEPTION',
                            msg: operation.getError(),
                            icon: Ext.MessageBox.ERROR,
                            buttons: Ext.Msg.OK
                        });
                    }
                }
            }
            ,listeners: {
                beforeload: function( store, operation, eOpts )
                    {
         
                        store.proxy.extraParams = {groupRole: 'OFFLAW'};
                    }
            }
        });
        
        Ext.apply(this,{
        	width: '100%',
            height: '100%',
            fieldDefaults: {
                labelAlign: 'left',
                msgTarget: 'qtip'
            },
            
            items:[
            {
            	xtype: 'container',
            	layout: 'anchor',
            	defaults: 
            	{
                    anchor: '100%'
                    ,labelWidth: 150
                },
                
                items:[
                {
                	html: '<html><tr><td><h3>ข้อมูลใบอนุญาต</h3></td></tr></html>'
                	,border: false
                }
                ,{
					 xtype: 'hiddenfield',
					 name: 'complaintLicenseId'
				}
                ,
                {
					 xtype: 'hiddenfield',
					 name: 'complaintDocType',
					 value: 'E'
				}
                ,
                {
					xtype: 'hiddenfield',
					name: 'traderId'
				}

                ,{
					 xtype: 'hiddenfield',
					 name: 'traderType'
				}
                ,{  	
                	xtype: 'displayfield',
                	fieldLabel: 'ประเภทใบอนุญาต:',
             	    name: 'traderTypeName'
//             	    	,labelWidth: 200
                }
				,{  	
                	xtype: 'displayfield',
                	fieldLabel: 'เลขที่ใบอนุญาต:',
             	    name: 'licenseNo'
//             	    	,labelWidth: 200
                }
				,{  	
                	xtype: 'displayfield',
                	fieldLabel: 'ชื่อธุรกิจนำเที่ยว/มัคคุเทศก์/ผู้นำเที่ยว:',
             	    name: 'traderName'
//             	    	,labelWidth: 200
                }
                
                ,{
                	html: '<html><tr><td><h3>บันทึกเรื่อง</h3></td></tr></html>'
                    ,border: false
                 }
                ,{
                	 xtype     : 'textareafield',
//                     grow      : true,
                     name      : 'complaintDesc',
                     fieldLabel: 'รายละเอียดเรื่อง'
//                     anchor    : '100%'
                     ,allowBlank: false
                     ,afterLabelTextTpl: required
                }
                ,
                {
                	xtype: 'textfield',
            		fieldLabel: 'หมายเลขบัตรประชาชน/หนังสือเดินทาง',
                    name: 'complaintCardId'
                    ,allowBlank: false,
                    afterLabelTextTpl: required,
                    maxLength: 13,
                    minLength: 13,
                     enforceMaxLength: true,
                     maskRe: /\d/
                }
                ,{
                	xtype: 'textfield',
            		fieldLabel: 'ชื่อ-นามสกุล',
                    name: 'complaintName'
                    ,allowBlank: false
                    ,afterLabelTextTpl: required
                }
                ,{
                	xtype: 'textfield',
            		fieldLabel: 'หมายเลขโทรศัพท์',
                    name: 'complaintTel'
                    ,allowBlank: false
                    ,afterLabelTextTpl: required,
                    maxLength: 15,
                    minLength: 9,
                    enforceMaxLength: true,
                    maskRe: /[\d\-]/,
                    regex: /^\d{9,10}(\-\d{2,3}|\-\d{4})?$/
                }
                ,{
                	xtype: 'textfield',
            		fieldLabel: 'อีเมล์',
                    name: 'complaintEmail',	
                    vtype: 'email'
                    ,allowBlank: false
                    ,afterLabelTextTpl: required
                }
                ,{
                	html: '<html><tr><td><h3>เอกสารแนบ</h3></td></tr></html>'
                    ,border: false
                 }
                ,
                {
                	xtype: 'complaint-document-add-grid'
                		
                }
                ]
                
            }
            ,{
            	xtype: 'container',
            	layout: 'anchor',
            	defaults: 
            	{
                    anchor: '100%'
                    ,labelWidth: 150
                },
                
                items:[
				{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'}
                ,{
                	 xtype: 'radio',
                	 id: 'radio1',
                	 boxLabel  : 'รับเรื่องร้องเรียน',
                	 name: 'complaintStatus',
                     inputValue: 'G',
                     listeners: {
	                     change: {
//	                         element: 'el', //bind to the underlying el property on the panel
	                         fn: function(radio1, newValue, oldValue, eOpts){ 
	                        	 
	                        	 //console.log(radio1.getValue());
	                        	 
	                        	 if(radio1.getValue())
	                        	{
		                        	 radio1.up('form').getForm().findField('masComplaintTypeId').allowBlank = false;
		                        	 radio1.up('form').getForm().findField('authorityComment').allowBlank = true;
		                        	 
	                        	}
	                        	 
	                         }
	                     }
	                 }
                
//                     handler: function(radio1){
//	                	 radio1.up('form').getForm().findField('masComplaintTypeId').allowBlank = false;
//	                 }
//	                 action: 'addTourleaderType',
	            }
                ,{
	            	xtype: 'combo',
	                fieldLabel: 'ประเภทเรื่องร้องเรียน',
	                store: 'tourism.store.combo.MasComplaintTypeStore',
	                queryMode: 'remote',
	                displayField: 'complaintTypeName',
	                valueField: 'masComplaintTypeId',
	                hiddenName: 'masComplaintTypeId',
	                name :'masComplaintTypeId'
//	                ,allowBlank: false
	                ,afterLabelTextTpl: required
                }
                ,{
	               	 xtype: 'radio',
	               	 id: 'radio2',
	            	 boxLabel  : 'ไม่รับเรื่องร้องเรียน',
	            	 name: 'complaintStatus',
	                 inputValue: 'R',
	                 listeners: {
	                     change: {
//	                         element: 'el', //bind to the underlying el property on the panel
	                         fn: function(radio1, t, eOpts){ 
//	                        	 //console.log(this.up('complaint-addedit-form').getForm().findField('complaintStatus').getValue());
//	                        	 this.up('form').getForm().findField('masComplaintTypeId').allowBlank = true;
	                        	 
	                        	 //console.log(radio1.getValue());
	                        	 
	                        	 if(radio1.getValue())
	                        	 {
		                        	 radio1.up('form').getForm().findField('masComplaintTypeId').allowBlank = true;
		                        	 radio1.up('form').getForm().findField('authorityComment').allowBlank = false;
	                        	 }
	                        	 
	                         }
	                     }
	                 }
//	                 handler: function(radio1){
//	                	 radio1.up('form').getForm().findField('masComplaintTypeId').allowBlank = true;
//	                 }
//                   action: 'addTourleaderType',
                }
                ,{
               	 	xtype     : 'textareafield',
                    name      : 'authorityComment',
                    fieldLabel: 'สาเหตุ'
//                    ,allowBlank: false
                    ,afterLabelTextTpl: required
               }
               ,{
	               	 xtype: 'checkbox',
	            	 boxLabel  : 'ส่งเรื่องไปกระบวนการตอบข้อซักถาม',
	            	 name: 'questionType',
	                 inputValue: 'Q',
//                  action: 'addTourleaderType',
	                 listeners: {
	                     change: {
//	                         element: 'el', //bind to the underlying el property on the panel
	                         fn: function(chk1, newValue, oldValue, eOpts){ 
//	                        	 //console.log(chk1);
//	                        	 //console.log(Ext.getCmp('btn1').complaintProgress);
	                        	 if(newValue)
                        		 {
	                        		 Ext.getCmp('btn1').setVisible(false);
	                        		 Ext.getCmp('btn2').setVisible(true);
	                        		 
	                        		 Ext.getCmp('radio1').setVisible(false);
	                        		 Ext.getCmp('radio2').setVisible(false);
	                        		 
	                        		 Ext.getCmp('radio1').setValue(false);
	                        		 Ext.getCmp('radio2').setValue(false);
	                        		 
	                        		 chk1.up('form').getForm().findField('masComplaintTypeId').allowBlank = true;
	                        		 chk1.up('form').getForm().findField('authorityComment').allowBlank = true;
	                        		 
	                        		 chk1.up('form').getForm().findField('masComplaintTypeId').setVisible(false);
	                        		 chk1.up('form').getForm().findField('authorityComment').setVisible(false);
	                        		 
	                        		 //ซ่อนนิติกรผู้รับผิดชอบเรื่อง
	                        		 if(Ext.getCmp('btn1').complaintProgress == 'ML')
                        			 {
	                        			 chk1.up('form').getForm().findField('userId').allowBlank = true;
	                        			 chk1.up('form').getForm().findField('userId').setVisible(false);
                        			 }
	                        		 
                        		 }
	                        	 else
                        		 {
	                        		 Ext.getCmp('btn1').setVisible(true);
	                        		 Ext.getCmp('btn2').setVisible(false);
	                        		 
	                        		 Ext.getCmp('radio1').setVisible(true);
	                        		 Ext.getCmp('radio2').setVisible(true);
	                        		 chk1.up('form').getForm().findField('masComplaintTypeId').setVisible(true);
	                        		 chk1.up('form').getForm().findField('authorityComment').setVisible(true);
	                        		 
	                        		//แสดงนิติกรผู้รับผิดชอบเรื่อง
	                        		 if(Ext.getCmp('btn1').complaintProgress == 'ML')
                        			 {
	                        			 chk1.up('form').getForm().findField('userId').allowBlank = false;
	                        			 chk1.up('form').getForm().findField('userId').setVisible(true);
                        			 }
                        		 }
	                         }
	                     }
	                 }
               }
               ,{
            	    xtype: 'combo',
	                fieldLabel: 'นิติกรผู้รับผิดชอบเรื่อง',
	                store: officerStore,
	                queryMode: 'remote',
	                displayField: 'userFullName',
	                valueField: 'userId',
	                hiddenName: 'userId',
	                name :'userId',
	                hidden: this.complaintProgress=='RL'?true:false
	                ,allowBlank: this.complaintProgress=='RL'?true:false
	                ,afterLabelTextTpl: required
	                		
//	                ,allowBlank: this.up('form').getForm().findField('complaintStatus').getValue()=='G'?false:true
               }
               ]
                
        	}
		    ],
            buttons: [{
                text: 'บันทึก',
                scope: this,
                action: 'saveComplaintReceive'
                ,complaintProgress: this.complaintProgress	
                ,id: 'btn1'
//                ,handler: function()
//                {
//                	,hidden: this.up('form').getForm().findField('questionType').getValue()?true:false
//                }
           
            },
            {
                text: 'ส่งเรื่องไปกระบวนการตอบข้อซักถาม',
                scope: this,
                action: 'changeQuestionType'
                ,complaintProgress: this.complaintProgress	
                ,roleGroup: this.roleGroup
                ,id: 'btn2'
                ,hidden: true
//                ,handler: function()
//                {
//                	,hidden: this.up('form').getForm().findField('questionType').getValue()?false:true
//                }
            }]    
        
        });
        this.callParent();
    },
    
    onResetClick: function(){
        this.getForm().reset();
    },
    
//    afterRender: function()
//    {
//	    this.callParent(arguments);
//	    
//	    //console.log(this.complaintLicenseId);
//	    this.getForm().findField('complaintLicenseId').setValue(this.complaintLicenseId);
	    
	    
	    //add complaint
//	    //console.log(complaintModel);
//        if(!Ext.isEmpty(this.complaintModel) )
//        {
//            this.loadRecord(this.complaintModel);
//    	    //console.log(this.complaintModel);  
//        }
//	    this.loadRecord(this.registrationModel); 
//    }
    
});





