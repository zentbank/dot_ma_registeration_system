Ext.define('tourism.view.complaint.addcomplaint.ComplaintDocumentGrid',{
	extend: 'Ext.grid.Panel',
	alias: 'widget.complaint-document-add-grid',
	stripeRows: true,
	requires: [
      'Ext.grid.Column',   
      'Ext.toolbar.Paging',
	  'Ext.grid.plugin.RowEditing',
	  'Ext.grid.column.Template',
	  'Ext.grid.column.Action',
	  'Ext.grid.plugin.RowExpander'
	  ,'tourism.store.grid.ComplaintDocumentStore'
	  ,'Ext.grid.feature.Grouping'
	  ,'tourism.view.complaint.addcomplaint.ComplaintDocumentUploadPanel'
	],
	initComponent: function(){
		
	  var columnUpload = 
	  		{
	        	header: '',
	            xtype: 'actioncolumn',
	           	width:50,
	           	align : 'center',
	            items: [{
	                iconCls: 'icon-upload-xsmall',
	                tooltip: 'แนบเอกสาร',
		            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
		            	this.fireEvent('itemclick', this, 'complaintdocumentUpload', grid, rowIndex, colIndex, record, node);
		            }
	            }]
//	  			,hidden: this.roleAction == 'document'?false:true
	       };
	  
	  var columnView = 
	  		{
	        	header: '',
	            xtype: 'actioncolumn',
	           	width:50,
	           	align : 'center',
	            items: [{
	            	getClass: function(v, meta, rec) {          
	                   if (rec.get('havefile')) {
	                        this.items[0].tooltip = 'ดูเอกสาร';
	                        return 'icon-checked-xsmall';
	                    }
	                },
		                
		            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
		            	if(record.get("havefile"))
	            		{
		            		this.fireEvent('itemclick', this, 'viewfile', grid, rowIndex, colIndex, record, node);
	            		}        		
		            }
	                
	            }]
	        };
	  
	  var columnDelete = 
		{
			header: '',
			xtype: 'actioncolumn',
         	width:50,
         	align : 'center',
         	items: [{
          	getClass: function(v, meta, rec) {          
                 if (rec.get('havefile')) {
                      this.items[0].tooltip = 'ลบเอกสาร';
                      return 'icon-delete-page-xsmall';
                  }
              },
	                
	            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
	            	if(record.get("havefile"))
          		{
	            		this.fireEvent('itemclick', this, 'deletefile', grid, rowIndex, colIndex, record, node);
          		}        		
	            }
              
          }]
      };
	  
	  
	  var store = Ext.create('tourism.store.grid.ComplaintDocumentStore',{
		  storeId: 'grid-complaint-receive-document-store'
	  });
	  
	  Ext.apply(this,{
		 store: store,
		 columns:[
		 {
			 text: 'complaintDocId'
		     ,dataIndex: 'complaintDocId'
		     ,hidden: true
		 }
		 ,{
			 text: 'complaintLicenseId'
			 ,dataIndex: 'complaintLicenseId'
		     ,hidden: true
		 }
		 ,{
			 text: 'เอกสาร',
			 dataIndex: 'complaintDocName',
			 flex: 1
		 }
		 
		 ,columnUpload
		 ,columnView
		 ,columnDelete
		 ]

	      ,tbar: [{
	         text: 'เพิ่มเอกสารแนบ',
	         scope: this,
	         handler: function(btn){
			        var form = btn.up('form');
			    	var complaintLicenseId = form.getForm().findField('complaintLicenseId').getValue();
			    	
					grid = btn.up('grid');
			        
			        var rec = Ext.create("tourism.model.grid.ComplaintDocumentModel",{
			        	complaintDocId: ''
			        	,complaintLicenseId: complaintLicenseId
			        	,complaintDocName: 'ชื่อเอกสาร'
			        });
			        
			        grid.getStore().insert(0, rec);
			        
             }
	      }]
	  
	  });
	  
	  this.callParent();
	}

	
});