Ext.define('tourism.view.complaint.addcomplaint.ComplaintDocumentUploadPanel',{
	extend: 'Ext.window.Window',
	alias: 'widget.complaint_document_upload_window',
	requires: [
//	           'tourism.view.information.tourleader.InfoTourleaderForm'
	],
	title: 'File Upload Form',
	layout: 'fit',
	width: 500,
//	height: 600,
	//True to make the window modal and mask everything behind it when displayed
    modal: true,
    
    initComponent: function(){
    	
    	this.items =[
    	    {
    	    		xtype: 'form',
    	            bodyPadding: '10 10 0',

    	            defaults: {
    	                anchor: '100%',
    	                allowBlank: false,
    	                msgTarget: 'side',
    	                labelWidth: 150
    	            },
    	            
    	    		frame: false,
                    border: false,
                    id: this.roleAction+'-'+this.actionMode+'-'+this.roleGroup+'-panel',
                    
    	            items: [
					{
						xtype: 'hiddenfield'
						,name: 'complaintDocId'
					}
					,
					{
						xtype: 'hiddenfield'
						,name: 'complaintLicenseId'
					}
					,
					{
						xtype: 'hiddenfield'
						,name: 'complaintDocType'
					}
					,
					{
						xtype: 'hiddenfield'
						,name: 'traderType'
					}
					,
					{
						xtype: 'hiddenfield'
						,name: 'licenseNo'
					}
					,
					{
						xtype: 'hiddenfield'
						,name: 'questionType'
					}
    	            ,
    	            {
    	                xtype: 'textfield',
    	                fieldLabel: 'ชื่อเอกสารแนบ'
    	                ,name: 'complaintDocName'
    	            },
    	            {
    	                xtype: 'filefield',
    	                id: 'form-file',
    	                emptyText: 'Select an file',
    	                fieldLabel: 'File',
    	                name: 'file',
    	                buttonText: '',
    	                buttonConfig: {
    	                    iconCls: 'upload-icon'
    	                }
    	            }
    	    		]

    	            ,buttons: [{
    	                text: 'Save',
    	                handler: function(btn){
    	                    var form = this.up('window').down('form').getForm();
    	                    
    	                    //console.log(form.getValues());
    	                    
    	                    if(form.isValid()){
//    	                    	//console.log(form.getValues());
    	                        form.submit({
    	                            url: 'business/complaint/fileupload',
    	                            method: 'POST',
    	                            waitMsg: 'Uploading your file...',
    	                            success: function(form, action) {
    	                            	
    	                            	 //console.log(action.result);
    	                            	
    	                            	// Ext.Msg.alert('', 'บันทึกข้อมูล');
    	               	             var noti = Ext.create('widget.uxNotification', {
    	               	               // title: 'Notification',
    	               	               position: 'tr',
    	               	               manager: 'instructions',
    	               	               // cls: 'ux-notification-light',
    	               	               // iconCls: 'ux-notification-icon-information',
    	               	               html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
    	               	               closable: false,
    	               	               autoCloseDelay: 4000,
    	               	               width: 300,
    	               	               slideBackDuration: 500,
    	               	               slideInAnimation: 'bounceOut',
    	               	               slideBackAnimation: 'easeIn'
    	               	             });
    	               	             
       	               	            noti.on('show',function(){
  	               	            	   
  	               	            	   	  var grid = Ext.ComponentQuery.query('complaint-document-add-grid')[0];     
		  	                              grid.getStore().load({
		  	                            	  params: {
		  	                            		   complaintLicenseId: this.up('window').down('form').getForm().findField('complaintLicenseId').getValue() 
		  	                            		   , complaintDocType: this.up('window').down('form').getForm().findField('complaintDocType').getValue() 
		  	                            	  },
		  	                                  callback: function(records, operation, success) {
		  	                                       //console.log(records);
		  	                                  },
		  	                                  scope: this
		  	                              }); 
		  	                              
		  	                            this.up('window').close();
  	               	                },btn);
    	               	             
    	               	             noti.show();
    	               	            
    	                            	
    	                            }
    	                        	,failure: function(form, action)
    	                        	{
    	                        		switch (action.failureType) {
    	                                case Ext.form.action.Action.CLIENT_INVALID:
    	                                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
    	                                    break;
    	                                case Ext.form.action.Action.CONNECT_FAILURE:
    	                                    Ext.Msg.alert('Failure', 'Ajax communication failed');
    	                                    break;
    	                                case Ext.form.action.Action.SERVER_INVALID:
    	                                   Ext.Msg.alert('Failure', "ไม่สามารถบันทึกข้อมูลได้");//action.result.msg);
    	                        		}
    	                        	}
    	                        });
    	                    }
    	                    
    	                }
    	            } 
    	    	]

    	    }
    	];
    	
    	this.callParent(arguments);
    }
	,loadFormRecord: function(model){
		var info = this.down('form');
        info.loadRecord(model);
        
//        //console.log(model);
//        //console.log(model.get('traderType'));
//        //console.log(model.get('licenseNo'));
//        //console.log(model.get('questionType'));
        
        info.getForm().findField('traderType').setValue(model.get('traderType'));
        info.getForm().findField('licenseNo').setValue(model.get('licenseNo'));
        info.getForm().findField('questionType').setValue(model.get('questionType'));
        
	}
	
});















