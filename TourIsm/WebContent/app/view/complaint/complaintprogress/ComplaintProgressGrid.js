Ext.define('tourism.view.complaint.complaintprogress.ComplaintProgressGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.complaintprogress-grid',
		stripeRows: true,
		requires: [
			'Ext.grid.Column',
			'Ext.toolbar.Paging',
			'Ext.grid.plugin.RowEditing',
			'Ext.grid.column.Template',
			'Ext.grid.column.Action',
			'Ext.grid.plugin.RowExpander',
			'tourism.store.grid.ComplaintProgressProgressStore'
		],
		initComponent: function(){
			var complaintProgressStore = Ext.create('tourism.store.grid.ComplaintProgressProgressStore',{
			});

			Ext.apply(this, {
				store: complaintProgressStore,
				
			});

			this.callParent(arguments);


		},
		columns: [
			{
				header: ' ',
				dataIndex: 'progressStatusName',
				flex: 1
			}
			,{
	        	header: '',
	            xtype: 'actioncolumn',
	           	width:50,
	           	align : 'center',
//	            items: [{
	                // iconCls: 'icon-send-xsmall',
	                // tooltip: 'การผ่านเรื่อง',
//	                getClass: function(v, meta, rec) {          
//	                    if (rec.get('progressStatus') == 'S') {
//	                        
//	                        return 'icon-checked-xsmall';
//	                    }if (rec.get('progressStatus') == 'R') {
//	                        
//	                        return 'icon-page_warning-xsmall';
//	                    } if (rec.get('progressStatus') == 'W') {
//	                        
//	                        return 'icon-wait-xsmall';
//	                    } 
//	                    if (rec.get('progressStatus') == 'A') {
//	                        
//	                        return 'icon-wait-xsmall';
//	                    } if (rec.get('progressStatus') == 'G') {
//	                        
//	                        return 'icon-wait-xsmall';
//	                    } 
//	                }
//	            }]
	        }
		],
		 plugins: [{
            ptype: 'rowexpander',
            rowBodyTpl : new Ext.XTemplate(
            	'<div>',
            		'<p><b>{progressDesc}</b></p>',
            		'<p><b>วันที่&nbsp;{progressDate}</b></p>',
            		'<p><b>{authority}</b></p>',
                '<div>'
            )
        }]
	});