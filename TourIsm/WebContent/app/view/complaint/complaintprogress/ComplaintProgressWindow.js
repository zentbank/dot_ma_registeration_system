Ext.define('tourism.view.complaint.complaintprogress.ComplaintProgressWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.complaintprogress-window',

    requires: [
        'tourism.view.complaint.complaintprogress.ComplaintProgressGrid'
    ],

    title : 'สถานะเรื่องร้องเรียน',
    layout: 'fit',
    // autoShow: true,
    width: 400,
    height: 500,
    //True to make the window modal and mask everything behind it when displayed
    modal: true,
    // iconCls: 'icon-form',

    initComponent: function() {

        this.items = [
                {
                    xtype: 'complaintprogress-grid',
                    frame: false,
                    border: false
                }
        ];

        this.callParent(arguments);
    }

	,loadFormRecord: function(model){
		var grid = this.down('grid');
		
		//console.log(grid);
		
		grid.getStore().load({
            params: {complaintLicenseId: model.get('complaintLicenseId')},
            callback: function(records, operation, success) 
            {
            	//console.log(records);                            
            },
            scope: this
		
	    });
		
	}

});
