Ext.define('tourism.view.complaint.considercomplaint.ComplaintConsiderWindow',{
	extend: 'Ext.window.Window',
	alias: 'widget.complaint_considercomplaint_window',
	requires: [
	           'tourism.view.complaint.considercomplaint.ComplaintConsiderForm'
	],
	title: 'รายละเอียด',
	layout: 'fit',
	width:  500,
	height: 650,
	//True to make the window modal and mask everything behind it when displayed
    modal: true,
    
    initComponent: function(){
    	
    	this.items =[
    	    {
    	    	xtype: 'complaint-considercomplaint-formpanel',
    	    	id: 'complaint-considercomplaint-formpanel',
                frame: false,
                border: false
               ,complaintProgress: this.complaintProgress
    	    }
    	];
    	
    	this.callParent(arguments);
    }

	,loadFormRecord: function(model)
	{
//		//console.log(model);
//		//console.log(this.complaintProgress);
		var info = this.down('form');
		
		info.getForm().findField('complaintLicenseId').setValue(model.get('complaintLicenseId'));
		
//		var infocomplaintdetailpanel = info.down('complaint-viewcomplaint-detail-info-panel');
//		
//		infocomplaintdetailpanel.on('afterrender',function(cmp){
//         	cmp.loadRecord({
//         		traderType: model.get('traderType')
//         		,licenseNo: model.get('licenseNo')
//         		,traderName: model.get('traderName')
//         		,complaintDesc: model.get('complaintDesc')
//         		,complaintCardId: model.get('complaintCardId')
//         		,complaintName: model.get('complaintName')
//         		,complaintTel: model.get('complaintTel')
//         		,complaintEmail: model.get('complaintEmail')
//         		,complaintTypeName: model.get('complaintTypeName')
//         		,userFullName: model.get('userFullName')
//         		,traderTypeName: model.get('traderTypeName')
//         	});
//         });
		
		//รายละเอียดเรื่องร้องเรียน
        info.loadRecord(model);
		
		//เอกสารแนบ
		var complaintdocumentinfogrid = info.down('complaint-document-info-grid');
		complaintdocumentinfogrid.getStore().load({
            params: {complaintLicenseId: model.get('complaintLicenseId')},
            callback: function(records, operation, success) 
            {
            	//console.log(records);                            
            },
            scope: this
		
	    });
		
		//พิจารณาเรื่อง
		//สถานะเรื่องร้องเรียน
		var complaintprogressconsidergrid = info.down('complaint-progress-consider-grid');
		complaintprogressconsidergrid.getStore().load({
            params: {complaintLicenseId: model.get('complaintLicenseId'), progressRole: this.complaintProgress},
            callback: function(records, operation, success) 
            {
            	//console.log(records);                            
            },
            scope: this
		
	    });
		
		//ComplaintDocument เอกสารคำสั่ง
        var complaintdocumentgrid = info.down('complaint-document-add-grid');
        complaintdocumentgrid.getStore().load({
            params: {complaintLicenseId: model.get('complaintLicenseId'), complaintDocType: 'O'},
            callback: function(records, operation, success) 
            {
            	//console.log(records);                            
            },
            scope: this
        });
		
	}

});














