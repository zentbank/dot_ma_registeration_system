Ext.define('tourism.view.complaint.considercomplaint.ComplaintProgressConsiderAddEditForm',{
	extend: 'Ext.form.Panel',
    requires: [
       'Ext.data.*',
       'Ext.form.*',
       'Ext.tab.Tab'
       
       ,'tourism.store.combo.ProgressStatusStore'
    ],
    xtype: 'complaint-progress-consider-addedit-form',
    bodyPadding: 5,
    autoScroll: true,
    initComponent: function(){
        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
        
        Ext.apply(this,{
        	width: '100%',
            height: '100%',
            fieldDefaults: {
                labelAlign: 'left',
                msgTarget: 'qtip'
            },
            
            items:[
            {
            	xtype: 'container',
            	layout: 'anchor',
            	defaults: 
            	{
                    anchor: '100%'
                    ,labelWidth: 150
                },
                
                items:[
                {
  					 xtype: 'hiddenfield',
  					 name: 'comProgressId'
      			}
                ,{
					 xtype: 'hiddenfield',
					 name: 'complaintLicenseId'
				}
                ,{  	
                	xtype: 'datefield',
             	    name: 'progressDate',
             	    fieldLabel: 'วันที่',
             	    flex: 1,
             	    format: 'd/m/B',
                    allowBlank: false
                }
                ,{
	            	xtype: 'combo',
	                fieldLabel: 'สถานะ',
	                store: 'tourism.store.combo.ProgressStatusStore',
	                queryMode: 'local',
	                displayField: 'progressStatusName',
	                valueField: 'progressStatus',
	                hiddenName: 'progressStatus',
	                name :'progressStatus'
	                ,allowBlank: false
                }
                ,{
                	 xtype     : 'textareafield',
                     name      : 'progressDesc',
                     fieldLabel: 'ผลการสอบสวน'
                     ,allowBlank: false
                }
                ]
                
            }
            
		    ],
            buttons: [{
                text: 'บันทึก',
                scope: this
                ,action: 'saveComplaintProgressConsider'
                ,complaintProgress: this.complaintProgress	
           
            }]    
        
        });
        this.callParent();
    },
    
    onResetClick: function(){
        this.getForm().reset();
    },
    
});





