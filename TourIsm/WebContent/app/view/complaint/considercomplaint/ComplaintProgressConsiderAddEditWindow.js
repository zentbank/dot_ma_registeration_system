Ext.define('tourism.view.complaint.considercomplaint.ComplaintProgressConsiderAddEditWindow',{
	extend: 'Ext.window.Window',
	alias: 'widget.complaint_progress_consider_addedit_window',
	requires: [
	           'tourism.view.complaint.considercomplaint.ComplaintProgressConsiderAddEditForm'
	],
	title: 'การพิจารณาเรื่องร้องเรียน',
	layout: 'fit',
	width: 500,
	height: 400,
	//True to make the window modal and mask everything behind it when displayed
    modal: true,
    
    initComponent: function(){
    	
    	this.items =[
    	    {
    	    	xtype: 'complaint-progress-consider-addedit-form',
    	    	id: 'complaint-progress-consider-addedit-form',
                frame: false,
                border: false
                ,complaintProgress: this.complaintProgress
    	    }
    	];
    	
    	this.callParent(arguments);
    }

	,loadFormRecord: function(model){
		var form = this.down('form');
		form.loadRecord(model);
		
//		form.getForm().findField('complaintLicenseId').setValue(model.get('complaintLicenseId'));
//		
//		//ประเภทเรื่องร้องเรียน
//		if(model.get('masComplaintTypeId') == '0')
//		{
//			complaintForm.getForm().findField('masComplaintTypeId').clearValue();
//			complaintForm.getForm().findField('masComplaintTypeId').setRawValue('');
//		}
//		else
//		{
//			var masComplaintTypeIdCombo = complaintForm.getForm().findField('masComplaintTypeId');
//			masComplaintTypeIdCombo.getStore().on('load',function(store, records, successful, eOpts){
//				masComplaintTypeIdCombo.setValue(model.get('masComplaintTypeId'));
//            },this,{single:true});
//			masComplaintTypeIdCombo.getStore().load();
//		}
//    	
//		//นิติกรผู้รับผิดชอบเรื่อง
//    	if(model.get('userId') == '0')
//		{
//    		complaintForm.getForm().findField('userId').clearValue();
//    		complaintForm.getForm().findField('userId').setRawValue('');
//		}
//    	else
//		{
//    		var userIdCombo = complaintForm.getForm().findField('userId');
//    		userIdCombo.getStore().on('load',function(store, records, successful, eOpts){
//    			userIdCombo.setValue(model.get('userId'));
//            },this,{single:true});
//    		userIdCombo.getStore().load();
//		}
//    	
//    	//TraderType ประเภทใบอนุญาต
//        if(!Ext.isEmpty(model.get('traderType')))
//        {
//          var traderTypeCombo = complaintForm.getForm().findField('traderType');
//          traderTypeCombo.getStore().on('load',function(store, records, successful, eOpts){
//        	  traderTypeCombo.setValue(model.get('traderType'));
//          },this,{single:true});
//          traderTypeCombo.getStore().load();
//        }
//        
//        //ComplaintDocument เอกสารแนบ
//        var complaintdocumentgrid = complaintForm.down('grid');
//        complaintdocumentgrid.getStore().load({
//            params: {complaintLicenseId: model.get('complaintLicenseId'), complaintDocType: 'E'},
//            callback: function(records, operation, success) 
//            {
//            	//console.log(records);                            
//            },
//            scope: this
//        });

	}

	
});















