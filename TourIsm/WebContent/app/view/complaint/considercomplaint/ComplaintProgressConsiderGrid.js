Ext.define('tourism.view.complaint.considercomplaint.ComplaintProgressConsiderGrid',{
	extend: 'Ext.grid.GridPanel',
	alias: 'widget.complaint-progress-consider-grid',
	stripeRows: true,
	requires: [
		'Ext.grid.Column',
		'Ext.toolbar.Paging',
		'Ext.grid.plugin.RowEditing',
		'Ext.grid.column.Template',
		'Ext.grid.column.Action',
		'Ext.grid.plugin.RowExpander',
		'Ext.grid.feature.Grouping'
		
		,'tourism.store.grid.ComplaintProgressStore'
		,'tourism.view.complaint.considercomplaint.ComplaintProgressConsiderAddEditWindow'
	],
	initComponent: function(){

		  var store = Ext.create('tourism.store.grid.ComplaintProgressStore',{
			  storeId: 'complaint-progress-consider-store'
		  });
		  
		  Ext.apply(this,{
			 store: store,
			 columns:[
			 {
				 text: 'comProgressId'
				 ,dataIndex: 'comProgressId'
			    ,hidden: true
			 }
			 ,{
				 text: 'complaintLicenseId'
				 ,dataIndex: 'complaintLicenseId'
			     ,hidden: true
			 }
			 ,{
					xtype: 'rownumberer'
			 }
			 ,{
				 text: 'วันที่',
				 dataIndex: 'progressDate',
				 flex: 2
			 }
			 ,{
				 text: 'สถานะ',
				 dataIndex: 'progressStatusName',
				 flex: 2
			 }
			 ,{
				 text: 'ผลการสอบสวน',
				 dataIndex: 'progressDesc',
				 flex: 2
			 },{
	        	text: 'แก้ไข',
	            xtype: 'actioncolumn',
	            width: 50,
	            items: [
	            {
					getClass: function(v, meta, rec) {          
					    if (rec.get('edit')) {
					         this.items[0].tooltip = 'แก้ไข';
					         return 'icon-edit-xsmall';
					     }
					 },
					 
//		            	iconCls: 'icon-edit-xsmall',
//		                tooltip: 'แก้ไข',
		            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
		            	if (record.get('edit')) {
		            		this.fireEvent('itemclick', this, 'editComplaintProgressConsider', grid, rowIndex, colIndex, record, node);
		            
		            	}
		            }
	            }]
	         }
//			 ,{
//	        	 text: 'ลบ',
//	        	 xtype: 'actioncolumn',
//	        	 width: 50,
//	        	 items: [
//	        	 {
//	        		iconCls: 'icon-delete-xsmall',
//	                tooltip: 'ลบ',
//		            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
//		                this.fireEvent('itemclick', this, 'deleteComplaintProgressConsider', grid, rowIndex, colIndex, record, node);
//		            }
//	        	 }
//	        	 ]
//	         }
			 ]
	
		      ,tbar: [{
		         text: 'เพิ่มการพิจารณาเรื่อง',
		         scope: this,
		         action: 'addComplaintProgressConsider'
		         ,complaintProgress: this.complaintProgress	
//		         handler: function(btn){
//				        var form = btn.up('form');
//				    	var complaintLicenseId = form.getForm().findField('complaintLicenseId').getValue();
//				    	
//						grid = btn.up('grid');
//				        
//				        var rec = Ext.create("tourism.model.grid.ComplaintDocumentModel",{
//				        	complaintDocId: ''
//				        	,complaintLicenseId: complaintLicenseId
//				        	,complaintDocName: 'ชื่อเอกสาร'
//				        });
//				        
//				        grid.getStore().insert(0, rec);
//				        
//	           }
		      }]
		  
		  });
		  
		  this.callParent();
		}
	
		
	});
	
	
	
	
	
	
	
	
	
