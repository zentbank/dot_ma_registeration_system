Ext.define('tourism.view.complaint.question.QuestionAddEditForm', {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.data.*',
        'Ext.form.*'

        ,'tourism.view.complaint.viewcomplaint.ComplaintDocumentInfoGrid'
        
    ],
    xtype: 'question_add_edit_form',
    // title: 'ธุรกิจนำเที่ยว',
    bodyPadding: 5,
    autoScroll: true,
    initComponent: function(){

        
        Ext.apply(this, {
            width: '100%',
            height: '100%',
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },

            items: [
               {
            	    xtype: 'container',
	               	layout: 'anchor',
	               	defaults: 
	               	{
	                       anchor: '100%'
	                       ,labelWidth: 150
	                },
	                items: [
					{
						 xtype: 'hiddenfield',
						 name: 'complaintLicenseId'
					},
					{  	
	                	xtype: 'displayfield',
	                	fieldLabel: 'ประเภทใบอนุญาต:',
	             	    name: 'traderTypeName'
	             	    	,labelWidth: 200
	                }
					,{  	
	                	xtype: 'displayfield',
	                	fieldLabel: 'เลขที่ใบอนุญาต:',
	             	    name: 'licenseNo'
	             	    	,labelWidth: 200
	                }
					,{  	
	                	xtype: 'displayfield',
	                	fieldLabel: 'ชื่อธุรกิจนำเที่ยว/มัคคุเทศก์/ผู้นำเที่ยว:',
	             	    name: 'traderName'
	             	    	,labelWidth: 200
	                }
					,{  	
	                	xtype: 'displayfield',
	                	fieldLabel: 'เรื่องร้องเรียน:',
	             	    name: 'complaintDesc'
	             	    	,labelWidth: 200
	                }
					,{  	
	                	xtype: 'displayfield',
	                	fieldLabel: 'หมายเลขบัตรประชาชน/หนังสือเดินทาง:',
	             	    name: 'complaintCardId'
	             	    	,labelWidth: 200
	                }
					,{  	
	                	xtype: 'displayfield',
	                	fieldLabel: 'ชื่อ-นามสกุล:',
	             	    name: 'complaintName'
	             	    	,labelWidth: 200
	                }
					,{  	
	                	xtype: 'displayfield',
	                	fieldLabel: 'หมายเลขโทรศัพท์:',
	             	    name: 'complaintTel'
	             	    	,labelWidth: 200
	                }
					,{  	
	                	xtype: 'displayfield',
	                	fieldLabel: 'อีเมล์:',
	             	    name: 'complaintEmail'
	             	    	,labelWidth: 200
	                }
					,{xtype: 'splitter'}
					
					//เอกสารแนบ
	                ,{
	                	xtype: 'complaint-document-info-grid'
	                }
	                ,{xtype: 'splitter'},{xtype: 'splitter'}
					,{
		               	 xtype: 'radio',
		            	 boxLabel  : 'ตอบข้อซักถาม',
		            	 name: 'complaintStatus',
		                 inputValue: 'S',
		                 id        : 'radioQ1',
		                 listeners: {
		                     click: {
		                         element: 'el', //bind to the underlying el property on the panel
		                         fn: function(){ 
		                             var radio2 = Ext.getCmp('radioQ2');
		                             radio2.setValue(false);
		                             
		                             Ext.getCmp('btnQ1').setVisible(true);
	                        		 Ext.getCmp('btnQ2').setVisible(false);
		                         }
		                     }
		                 }
	                }
	                ,{
	               	 	xtype     : 'textareafield',
	                    name      : 'authorityComment',
	                    fieldLabel: ''
	                }
					,{
		               	 xtype: 'radio',
		            	 boxLabel  : 'ส่งเรื่องไปกระบวนการร้องเรียน',
		            	 name: 'questionType',
		                 inputValue: 'C',
		                 id        : 'radioQ2',
		                 listeners: {
		                     click: {
		                         element: 'el', //bind to the underlying el property on the panel
		                         fn: function(){ 
		                        	 var radio1 = Ext.getCmp('radioQ1');
		                             radio1.setValue(false);
		                             
		                             Ext.getCmp('btnQ1').setVisible(false);
	                        		 Ext.getCmp('btnQ2').setVisible(true);
		                         }
		                     }
		                 }
					
	                }

	                ]
               }
               ],
               buttons: [{
                   text: 'บันทึก',
                   scope: this,
                   action: 'saveQuestion'
                   ,complaintProgress: this.complaintProgress	
                   ,id: 'btnQ1'
               },
               {
                   text: 'ส่งเรื่องไปกระบวนการร้องเรียน',
                   scope: this,
                   action: 'changeQuestionType'
                   ,complaintProgress: this.complaintProgress	
                   ,roleGroup: this.roleGroup
                   ,id: 'btnQ2'
                   ,hidden: true
               }]      
        });

        this.callParent(arguments);
    }
  
});