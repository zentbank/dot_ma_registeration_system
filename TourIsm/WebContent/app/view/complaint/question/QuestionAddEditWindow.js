Ext.define('tourism.view.complaint.question.QuestionAddEditWindow',{
	extend: 'Ext.window.Window',
	alias: 'widget.question_add_edit_window',
	requires: [
	           'tourism.view.complaint.question.QuestionAddEditForm'
	],
	title: 'รายละเอียด',
	layout: 'fit',
	width:  500,
	height: 600,
	//True to make the window modal and mask everything behind it when displayed
    modal: true,
    
    initComponent: function(){
    	
    	this.items =[
    	    {
    	    	xtype: 'question_add_edit_form',
    	    	id: 'question_add_edit_form',
                frame: false,
                border: false
               ,complaintProgress: this.complaintProgress
               ,roleGroup: this.roleGroup
    	    }
    	];
    	
    	this.callParent(arguments);
    }

	,loadFormRecord: function(model)
	{
		var info = this.down('form');
        
        //รายละเอียดเรื่องร้องเรียน
        info.loadRecord(model);
		
		//เอกสารแนบ
		var complaintdocumentinfogrid = info.down('complaint-document-info-grid');
		complaintdocumentinfogrid.getStore().load({
            params: {complaintLicenseId: model.get('complaintLicenseId'), complaintDocType: 'E'},
            callback: function(records, operation, success) 
            {
            	//console.log(records);                            
            },
            scope: this
		
	    });
		
	}

});














