Ext.define('tourism.view.complaint.question.QuestionInfoForm', {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.data.*',
        'Ext.form.*'

        ,'tourism.view.complaint.viewcomplaint.ComplaintDocumentInfoGrid'
        
    ],
    xtype: 'question_info_form',
    // title: 'ธุรกิจนำเที่ยว',
    bodyPadding: 5,
    autoScroll: true,
    initComponent: function(){

        
        Ext.apply(this, {
            width: '100%',
            height: '100%',
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },

            items: [
               {
            	    xtype: 'container',
	               	layout: 'anchor',
	               	defaults: 
	               	{
	                       anchor: '100%'
	                       ,labelWidth: 150
	                },
	                items: [
					{
						 xtype: 'hiddenfield',
						 name: 'complaintLicenseId'
					},
					{  	
	                	xtype: 'displayfield',
	                	fieldLabel: 'ประเภทใบอนุญาต:',
	             	    name: 'traderTypeName'
	             	    	,labelWidth: 200
	                }
					,{  	
	                	xtype: 'displayfield',
	                	fieldLabel: 'เลขที่ใบอนุญาต:',
	             	    name: 'licenseNo'
	             	    	,labelWidth: 200
	                }
					,{  	
	                	xtype: 'displayfield',
	                	fieldLabel: 'ชื่อธุรกิจนำเที่ยว/มัคคุเทศก์/ผู้นำเที่ยว:',
	             	    name: 'traderName'
	             	    	,labelWidth: 200
	                }
					,{  	
	                	xtype: 'displayfield',
	                	fieldLabel: 'เรื่องร้องเรียน:',
	             	    name: 'complaintDesc'
	             	    	,labelWidth: 200
	                }
					,{  	
	                	xtype: 'displayfield',
	                	fieldLabel: 'หมายเลขบัตรประชาชน/หนังสือเดินทาง:',
	             	    name: 'complaintCardId'
	             	    	,labelWidth: 200
	                }
					,{  	
	                	xtype: 'displayfield',
	                	fieldLabel: 'ชื่อ-นามสกุล:',
	             	    name: 'complaintName'
	             	    	,labelWidth: 200
	                }
					,{  	
	                	xtype: 'displayfield',
	                	fieldLabel: 'หมายเลขโทรศัพท์:',
	             	    name: 'complaintTel'
	             	    	,labelWidth: 200
	                }
					,{  	
	                	xtype: 'displayfield',
	                	fieldLabel: 'อีเมล์:',
	             	    name: 'complaintEmail'
	             	    	,labelWidth: 200
	                }
					,{xtype: 'splitter'}
					
					//เอกสารแนบ
	                ,{
	                	xtype: 'complaint-document-info-grid'
	                }
	                ,{xtype: 'splitter'},{xtype: 'splitter'}
	                
	                ,{  	
	                	xtype: 'displayfield',
	                	fieldLabel: 'ตอบข้อซักถาม:',
	             	    name: 'authorityComment'
	             	    	,labelWidth: 200
	                }
	
	                
	                ]
               }
               ]
            
        });

        this.callParent(arguments);
    }
  
});