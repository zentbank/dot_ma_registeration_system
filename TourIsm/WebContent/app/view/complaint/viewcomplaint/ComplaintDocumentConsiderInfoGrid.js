Ext.define('tourism.view.complaint.viewcomplaint.ComplaintDocumentConsiderInfoGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.complaint-document-consider-info-grid',
//		store: 'tourism.store.grid.ComplaintDocumentStore',
		border: 0,
		stripeRows: true,
		requires: [
			'Ext.grid.Column',
			'Ext.grid.column.Template',
			'Ext.grid.column.Action',
			'Ext.grid.plugin.RowExpander'
		],
		initComponent: function(){

			var store = Ext.create('tourism.store.grid.ComplaintDocumentStore',{
				 storeId: 'complaint-document-info-store'
			});
			
			Ext.apply(this, {
				store: store,
				columns: [
					{
						xtype: 'rownumberer'
					},
					{
						header: 'ชื่อเอกสาร',
						dataIndex: 'complaintDocName',
						flex: 1
					},
					{
			        	header: '',
			            xtype: 'actioncolumn',
			           	width:50,
			           	align : 'center',
			            items: [{
			            	getClass: function(v, meta, rec) {          
			                   if (rec.get('havefile')) {
			                        this.items[0].tooltip = 'ดูเอกสาร';
			                        return 'icon-checked-xsmall';
			                    }
			                },
				                
				            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
				            	if(record.get("havefile"))
			            		{
				            		this.fireEvent('itemclick', this, 'viewfile', grid, rowIndex, colIndex, record, node);
			            		}        		
				            }
			                
			            }]
			        }
				]
			});

			this.callParent();
		}

	});