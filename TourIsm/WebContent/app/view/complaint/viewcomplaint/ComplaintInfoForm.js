Ext.define('tourism.view.complaint.viewcomplaint.ComplaintInfoForm', {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.data.*',
        'Ext.form.*',
        'Ext.tab.Tab'
        
//        ,'tourism.view.complaint.viewcomplaint.ComplaintInfoPanel'
//        ,'tourism.view.complaint.viewcomplaint.ComplaintDetailInfoPanel'
        ,'tourism.view.complaint.viewcomplaint.ComplaintDocumentInfoGrid'
        
        ,'tourism.view.complaint.viewcomplaint.ComplaintProgressConsiderInfoGrid'
        ,'tourism.view.complaint.viewcomplaint.ComplaintDocumentConsiderInfoGrid'
        
    ],
    xtype: 'complaint-viewcomplaint-formpanel',
    // title: 'ธุรกิจนำเที่ยว',
    bodyPadding: 5,
    autoScroll: true,
    initComponent: function(){

        
        Ext.apply(this, {
            width: '100%',
            height: '100%',
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },

            items: [
                {
                    xtype: 'tabpanel',
//                     width: 400,
//                    height: 650,
                    plain: true,
                    frame: false,
                    border: false,
                   items: [
                        {
                            title: 'พิจารณาเรื่อง',
                            layout: 'anchor',
                            frame: false,
                            border: false,
                           
                            items:[
							{
								html: '<html><tr><td><h3>สถานะเรื่องร้องเรียน</h3></td></tr></html>'
								,border: false
							},
                            {
                            	xtype: 'complaint-progress-consider-info-grid'
                            },
							{
								html: '<html><tr><td><h3>เอกสารคำสั่ง</h3></td></tr></html>'
								,border: false
							},
                            {
                            	xtype: 'complaint-document-consider-info-grid'
                            }
                            ]
                        }
                        ,{
                       	 	title: 'รายละเอียดเรื่องร้องเรียน',
	                       	layout: 'anchor',
	                     	frame: false,
	                     	border: false,
//	                     	labelWidth: 250,
                            items: [
							{  	
			                	xtype: 'displayfield',
			                	fieldLabel: 'ประเภทใบอนุญาต:',
			             	    name: 'traderTypeName'
			             	    	,labelWidth: 200
			                }
							,{  	
			                	xtype: 'displayfield',
			                	fieldLabel: 'เลขที่ใบอนุญาต:',
			             	    name: 'licenseNo'
			             	    	,labelWidth: 200
			                }
							,{  	
			                	xtype: 'displayfield',
			                	fieldLabel: 'ชื่อธุรกิจนำเที่ยว/มัคคุเทศก์/ผู้นำเที่ยว:',
			             	    name: 'traderName'
			             	    	,labelWidth: 200
			                }
							,{  	
			                	xtype: 'displayfield',
			                	fieldLabel: 'เรื่องร้องเรียน:',
			             	    name: 'complaintDesc'
			             	    	,labelWidth: 200
			                }
							,{  	
			                	xtype: 'displayfield',
			                	fieldLabel: 'หมายเลขบัตรประชาชน/หนังสือเดินทาง:',
			             	    name: 'complaintCardId'
			             	    	,labelWidth: 200
			                }
							,{  	
			                	xtype: 'displayfield',
			                	fieldLabel: 'ชื่อ-นามสกุล:',
			             	    name: 'complaintName'
			             	    	,labelWidth: 200
			                }
							,{  	
			                	xtype: 'displayfield',
			                	fieldLabel: 'หมายเลขโทรศัพท์:',
			             	    name: 'complaintTel'
			             	    	,labelWidth: 200
			                }
							,{  	
			                	xtype: 'displayfield',
			                	fieldLabel: 'อีเมล์:',
			             	    name: 'complaintEmail'
			             	    	,labelWidth: 200
			                }
							,{  	
			                	xtype: 'displayfield',
			                	fieldLabel: 'ประเภทเรื่องร้องเรียน:',
			             	    name: 'complaintTypeName'
			             	    	,labelWidth: 200
			                }
							,{  	
			                	xtype: 'displayfield',
			                	fieldLabel: 'นิติกรผู้รับผิดชอบเรื่อง:',
			             	    name: 'userFullName'
			             	    	,labelWidth: 200
			                }
					
							//เอกสารแนบ
	                        ,{
	                        	xtype: 'complaint-document-info-grid'
	                        }

                            
                            ]
                        }

                    ]
                }
        ]   
        });

        this.callParent(arguments);
    }
  
});