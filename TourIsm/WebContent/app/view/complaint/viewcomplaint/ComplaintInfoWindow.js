Ext.define('tourism.view.complaint.viewcomplaint.ComplaintInfoWindow',{
	extend: 'Ext.window.Window',
	alias: 'widget.complaint_viewcomplaint_window',
	requires: [
	           'tourism.view.complaint.viewcomplaint.ComplaintInfoForm'
	],
	title: 'รายละเอียด',
	layout: 'fit',
	width:  500,
	height: 650,
	//True to make the window modal and mask everything behind it when displayed
    modal: true,
    
    initComponent: function(){
    	
    	this.items =[
    	    {
    	    	xtype: 'complaint-viewcomplaint-formpanel',
    	    	id: 'complaint-viewcomplaint-formpanel',
                frame: false,
                border: false
               ,complaintProgress: this.complaintProgress
    	    }
    	];
    	
    	this.callParent(arguments);
    }

	,loadFormRecord: function(model)
	{
//		//console.log(model);
		var info = this.down('form');
		
		//พิจารณาเรื่อง
		//สถานะเรื่องร้องเรียน
		var complaintprogressconsiderinfogrid = info.down('complaint-progress-consider-info-grid');
		complaintprogressconsiderinfogrid.getStore().load({
            params: {complaintLicenseId: model.get('complaintLicenseId'), progressRole: 'WL'},
            callback: function(records, operation, success) 
            {
            	//console.log(records);                            
            },
            scope: this
		
	    });
		
		//ComplaintDocument เอกสารคำสั่ง
        var complaintdocumentconsiderinfogrid = info.down('complaint-document-consider-info-grid');
        complaintdocumentconsiderinfogrid.getStore().load({
            params: {complaintLicenseId: model.get('complaintLicenseId'), complaintDocType: 'O'},
            callback: function(records, operation, success) 
            {
            	//console.log(records);                            
            },
            scope: this
        });
		
//		var infocomplaintdetailpanel = info.down('complaint-viewcomplaint-detail-info-panel');
//		
//		infocomplaintdetailpanel.on('afterrender',function(cmp){
//         	cmp.loadRecord({
//         		traderType: model.get('traderType')
//         		,licenseNo: model.get('licenseNo')
//         		,traderName: model.get('traderName')
//         		,complaintDesc: model.get('complaintDesc')
//         		,complaintCardId: model.get('complaintCardId')
//         		,complaintName: model.get('complaintName')
//         		,complaintTel: model.get('complaintTel')
//         		,complaintEmail: model.get('complaintEmail')
//         		,complaintTypeName: model.get('complaintTypeName')
//         		,userFullName: model.get('userFullName')
//         		,traderTypeName: model.get('traderTypeName')
//         	});
//         });
        
        //รายละเอียดเรื่องร้องเรียน
        info.loadRecord(model);
		
		//เอกสารแนบ
		var complaintdocumentinfogrid = info.down('complaint-document-info-grid');
		complaintdocumentinfogrid.getStore().load({
            params: {complaintLicenseId: model.get('complaintLicenseId'), complaintDocType: 'E'},
            callback: function(records, operation, success) 
            {
            	//console.log(records);                            
            },
            scope: this
		
	    });
		
	}

});














