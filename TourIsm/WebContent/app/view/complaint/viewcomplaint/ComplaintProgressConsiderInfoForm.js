Ext.define('tourism.view.complaint.viewcomplaint.ComplaintProgressConsiderInfoForm',{
	extend: 'Ext.form.Panel',
    requires: [
       'Ext.data.*',
       'Ext.form.*',
       'Ext.tab.Tab'
       
    ],
    xtype: 'complaint-progress-consider-info-form',
    bodyPadding: 5,
    autoScroll: true,
    initComponent: function(){
        
        Ext.apply(this,{
        	width: '100%',
            height: '100%',
            fieldDefaults: {
                labelAlign: 'left',
                msgTarget: 'qtip'
            },
            
            items:[
            {
            	xtype: 'container',
            	layout: 'anchor',
            	defaults: 
            	{
                    anchor: '100%'
                    ,labelWidth: 150
                },
                
                items:[
                {  	
                	xtype: 'displayfield',
                	fieldLabel: 'วันที่',
             	    name: 'progressDate'
                }
                ,{
                	xtype: 'displayfield'
                	,fieldLabel: 'สถานะ'
                	,name: 'progressStatusName'
                }
                ,{
                	 xtype     : 'displayfield',
                     name      : 'progressDesc',
                     fieldLabel: 'ผลการสอบสวน'
                }
                ]
                
            }
            
		    ]  
        
        });
        this.callParent();
    },
    
    onResetClick: function(){
        this.getForm().reset();
    },
    
});





