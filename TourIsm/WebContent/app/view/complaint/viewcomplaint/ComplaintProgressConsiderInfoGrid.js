Ext.define('tourism.view.complaint.viewcomplaint.ComplaintProgressConsiderInfoGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.complaint-progress-consider-info-grid',
//		store: 'tourism.store.grid.ComplaintDocumentStore',
		border: 0,
		stripeRows: true,
		requires: [
			'Ext.grid.Column',
			'Ext.grid.column.Template',
			'Ext.grid.column.Action',
			'Ext.grid.plugin.RowExpander'
			,'tourism.view.complaint.viewcomplaint.ComplaintProgressConsiderInfoWindow'
		],
		initComponent: function(){
			
			var store = Ext.create('tourism.store.grid.ComplaintProgressStore',{
				  storeId: 'complaint-progress-consider-info-store'
			  });
			
			Ext.apply(this, {
				store: store,
				columns: [
					{
						xtype: 'rownumberer'
					},
					{
						header: 'วันที่',
						dataIndex: 'progressDate',
						flex: 1
					},
					{
						header: 'สถานะ',
						dataIndex: 'progressStatusName',
						flex: 1
					},
					{
						header: 'ผลการสอบสวน',
						dataIndex: 'progressDesc',
						flex: 1
					}
					,{
			        	text: '',
			            xtype: 'actioncolumn',
			            width: 50,
			            items: [{
			            	iconCls: 'icon-notes-xsmall',
			                tooltip: 'รายละเอียด',
				            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
				                this.fireEvent('itemclick', this, 'viewComplaintProgressConsider', grid, rowIndex, colIndex, record, node);
				            }
			            }]
			         }
					
				]
			});

			this.callParent();
		}

	});