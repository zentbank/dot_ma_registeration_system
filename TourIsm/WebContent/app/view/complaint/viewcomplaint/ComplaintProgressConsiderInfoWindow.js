Ext.define('tourism.view.complaint.viewcomplaint.ComplaintProgressConsiderInfoWindow',{
	extend: 'Ext.window.Window',
	alias: 'widget.complaint_progress_consider_info_window',
	requires: [
	           'tourism.view.complaint.viewcomplaint.ComplaintProgressConsiderInfoForm'
	],
	title: 'รายละเอียด',
	layout: 'fit',
	width:  500,
	height: 300,
	//True to make the window modal and mask everything behind it when displayed
    modal: true,
    
    initComponent: function(){
    	
    	this.items =[
    	    {
    	    	xtype: 'complaint-progress-consider-info-form',
    	    	id: 'complaint-progress-consider-info-form',
                frame: false,
                border: false
               ,complaintProgress: this.complaintProgress
    	    }
    	];
    	
    	this.callParent(arguments);
    }

	,loadFormRecord: function(model)
	{
		var form = this.down('form');
		form.loadRecord(model);
	}

});














