Ext.define('tourism.view.course.CourseAddEditForm',
				{
					extend : 'Ext.form.Panel',
					alias : 'widget.course_addedit_form',
					requires : [ 'Ext.data.*', 'Ext.form.*', 'Ext.tab.Tab' ],

					bodyPadding : 5,
					autoScroll : true,

					initComponent : function() {
						var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

						var universityStore = Ext.create(
								'tourism.store.combo.MasUniversityStore', {
									storeId : 'guide-train-university-store'
								});
						var store = Ext.create(
								'tourism.store.course.CourseStore', {});

						var storeLanguage = Ext.create('tourism.store.combo.LanguageStore',{
							  storeId: 'guide-train-language-store'
						  });

						Ext.apply(
										this,
										{
											width : '100%',
											height : '100%',
											fieldDefaults : {
												labelAlign : 'left',
												// labelWidth: 50,
												msgTarget : 'qtip'
											},

											items : [ {

												xtype : 'container',
												layout : 'anchor',
												defaults : {
													anchor : '50%'
												},
												items : [
												        {
												        	xtype: 'hiddenfield',
												        	name: 'eduId'
												        },
														{
															xtype : 'textfield',
															name : 'graduationCourse',
															fieldLabel : 'ชื่อหลักสูตรฝึกอบรม',
															labelWidth : 150,
															allowBlank : false,
															afterLabelTextTpl : required
														},
														{
															xtype : 'textfield',
															name : 'generationGraduate',
															fieldLabel : 'รุ่นที่',
															labelWidth : 150,
															allowBlank : false,
															afterLabelTextTpl : required
														},
														{
															xtype : 'combo',
															name : 'countryId',
															fieldLabel : 'ภาษา',
															labelWidth : 150,
															store : storeLanguage,
															queryMode : 'remote',
															displayField : 'language',
															valueField : 'countryId',
															hiddenName : 'countryId',
															triggerAction : 'all',
															flex : 1,
															afterLabelTextTpl : required,
															allowBlank : false
														},
														{
															xtype : 'combo',
															name : 'masUniversityId',
															fieldLabel : 'สถาบัน',
															labelWidth : 150,
															store : universityStore,
															queryMode : 'remote',
															displayField : 'universityName',
															valueField : 'masUniversityId',
															hiddenName : 'masUniversityId',
															triggerAction : 'all',
															flex : 1,
															afterLabelTextTpl : required,
															allowBlank : false
														},
														{
															xtype : 'datefield',
															name : 'studyDate',
															fieldLabel : 'วันที่อบรม',
															labelWidth : 150,
															flex : 1,
															format : 'd/m/B',
															afterLabelTextTpl : required,
															allowBlank : false
														},
														{
															xtype : 'datefield',
															name : 'graduationDate',
															fieldLabel : 'ถึงวันที่',
															labelWidth : 150,
															flex : 1,
															format : 'd/m/B',
															afterLabelTextTpl : required,
															allowBlank : false
														} ]
											}/*,
											{
												xtype: 'panel',
												html: 'sss'
											}*/
											],

											buttons : [ {
												text : 'บันทึก',
												scope : this,
												action : 'saveCourse'
											}, {
												text : 'ยกเลิก',
												scope : this,
												handler : this.onResetClick
											} ]
										});
						this.callParent(arguments);
					},
					onResetClick : function() {
						// this.getForm().reset();
						var win = this.up('window');
						win.close();
					}

				});
