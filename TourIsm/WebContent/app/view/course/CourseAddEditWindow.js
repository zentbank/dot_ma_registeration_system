Ext.define('tourism.view.course.CourseAddEditWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.course_addedit_window',

    requires: [
        'tourism.view.course.CourseAddEditForm'
    ],

    title : 'เพิ่มหลักสูตรฝึกอบรม',
    layout: 'fit',

    //True to make the window modal and mask everything behind it when displayed
    modal: true,


    initComponent: function() {


        this.items = [
            {
                xtype: 'course_addedit_form',
                frame: false,
                border: false
                //registrationModel: this.registrationModel
            }
            
        ];

        this.callParent(arguments);
    },

});
