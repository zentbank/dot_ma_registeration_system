Ext.define('tourism.view.course.CourseGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.course_grid',
		
		stripeRows: true,
		requires: [
			'Ext.grid.Column',
			'tourism.store.course.CourseStore',
			'Ext.toolbar.Paging',
			'Ext.grid.plugin.RowEditing',
			'Ext.grid.column.Template',
			'Ext.grid.column.Action',
			'Ext.grid.plugin.RowExpander',
			'tourism.view.course.CourseAddEditWindow'
		],
		initComponent: function(){
		

			var store = Ext.create('tourism.store.course.CourseStore',{
				listeners: {
					scope: this,
					// single: true,
					beforeload: function( store, operation, eOpts )
				        {

				        	var formId = '#guide-tourleader-course-formsearch';
				        
				        	var formSearch = Ext.ComponentQuery.query(formId)[0];
				        	var values = formSearch.getValues();
								
				        	 for (field in values) 
						      {
						        if (Ext.isEmpty(values[field])) 
						        {
						          delete values[field];
						        }
						        
						      }
						    store.proxy.extraParams = values;
				        }
				}
			});

			Ext.apply(this, {
				store: store,
				dockedItems:[
					{
						xtype: 'pagingtoolbar',
						store: store,
						dock: 'bottom',
						displayInfo: true,
						items:[
							{

									text: 'เพิ่มหลักสูตรฝึกอบรม',
									action: 'add'
			        		}
		        		]
					}
				]
			});

			this.callParent(arguments);
		},
		columns: [
			{
				header: 'eduId',
				dataIndex: 'eduId',
				hidden: true
			},
			{
				header: 'ชื่อหลักสูตรฝึกอบรม',
				dataIndex: 'graduationCourse',
				flex: 2
			},
			{
				header: 'รุ่นที่',
				dataIndex: 'generationGraduate',
				flex: 0.5,
				align : 'center'
			},
			{
				header: 'มหาวิทยาลัย',
				dataIndex: 'universityName',
				flex: 2
			},
			{
				header: 'วันที่อบรม',
				dataIndex: 'studyDate',
				flex: 2,
				renderer: function(value, metaData, model)
				 {
	               return model.get('studyDate')+'-'+model.get('graduationDate');
				 }
			},
			/*{
				header: '',
				xtype: 'actioncolumn',
				width: 50,
				align: 'center',
				items: [{
					iconCls: 'icon-adduser-xsmall',
					tooltip: 'เพิ่มบุคคล',
					handler: function(grid, rowIndex, colIndex, node, e, record, rowNode) {
						this.fireEvent('itemclick',this,'addpersoncourse',grid, rowIndex, colIndex, record, node);
					}
				}]
			},*/
	        {
	        	header: '',
	            xtype: 'actioncolumn',
	           	width:50,
	           	align : 'center',
	            items: [{
	            	iconCls: 'icon-edit-xsmall',
	                tooltip: 'แก้ไข',
		            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
		                this.fireEvent('itemclick', this, 'editcourse', grid, rowIndex, colIndex, record, node);
		            }
	            }]
	        }/*,
	        {
	        	header: '',
	            xtype: 'actioncolumn',
	           	width:50,
	           	align : 'center',
	            items: [{
	                 iconCls: 'icon-delete-xsmall',
	                 tooltip: 'ลบ',
		            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {		            	
		            		this.fireEvent('itemclick', this, 'deletecourse', grid, rowIndex, colIndex, record, node);
		            }
	            }]
	        }*/
		]
        
});