Ext.define('tourism.view.course.CoursePanel', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
        'tourism.view.course.CourseSearchForm',
        'tourism.view.course.CourseGrid'
        //'tourism.view.account.AccountGuideFormSearch'
    ],
    
    xtype: 'course-panel',
    initComponent: function(){

    	Ext.apply(this, {
    		layout: {
		        type: 'border'
		    },
		    items: [{
		        region: 'north',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items: [
		                {
		                	xtype: 'course-formsearch',
		                	title: 'ค้นหาหลักสูตรฝึกอบรม',
		                	collapsible: true,   // make collapsible
		    	            // collapsed : true,
		    	            // padding: '5px 5px 0px 5px',
		    	            id: 'guide-tourleader-course-formsearch',
		    	            frame: false,
		    	            border: false
		                }
		        ]
		    },{
		        region: 'center',
		        xtype: 'panel',
		        layout: 'fit',
		        frame: false,
		        border: false,
		        items:[{
		        	xtype: 'course_grid',
		        	title: 'รายการหลักสูตรฝึกอบรม',
		            // padding: '5px 5px 5px 5px',
		            autoScroll: true,
		            border: false
		        }]
		    }]
    	});

    	this.callParent(arguments);
    }
});