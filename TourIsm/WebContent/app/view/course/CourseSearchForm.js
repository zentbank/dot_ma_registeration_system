Ext.define('tourism.view.course.CourseSearchForm' ,{
    extend: 'Ext.form.FormPanel',
    alias : 'widget.course-formsearch',
    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.Time',
        'Ext.form.CheckboxGroup',
        'Ext.layout.container.HBox',
        'Ext.form.FieldSet',
        'Ext.util.Positionable'
    ],
    bodyPadding: 10,
    
    items: [
                {
            xtype: 'container',
            layout: 'anchor',
            defaults: {
                anchor: '100%',
                labelWidth: 150
            },
            items: [
					                {
					                    xtype: 'textfield',
					                    fieldLabel: 'ชื่อหลักสูตรการฝึกอบรม',
					                    name: 'graduationCourse',
					                    anchor: '60%'
					                },
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'รุ่นที่',
                                        name: 'generationGraduate',
                                        format: 'd/m/B',
                                        anchor: '60%'	
                                    },
                                    {
										xtype : 'combo',
										//id : 'course-train-university-store',
										name : 'masUniversityId',
										fieldLabel : 'สถาบัน',
										store : 'tourism.store.combo.MasUniversityStore',
										labelWidth: 150,
										queryMode : 'remote',
										displayField : 'universityName',
										valueField : 'masUniversityId',
										hiddenName : 'masUniversityId',
										triggerAction : 'all',
										flex : 1,
										allowBlank : false,
										anchor: '60%'
									},
									{
                                        xtype: 'datefield',
                                        fieldLabel: 'วันที่อบรม',
                                        name: 'studyDate',
                                        format: 'd/m/B',
                                        anchor: '60%'	
                                    },
                                    {
                                        xtype: 'datefield',
                                        fieldLabel: 'ถึงวันที่',
                                        name: 'graduationDate',
                                        format: 'd/m/B',
                                        anchor: '60%'	
                                    }
                        
                     ]      
        },
        {
            xtype: 'toolbar',
            border: false,
            padding: '6px 0 6px 0px',
            items: [
            {
            	xtype: 'button',
                text : 'ค้นหา',
                action: 'searchCourseBetweenRegistration'
            }/*,
            {
            	xtype: 'button',
            	text: 'ยกเลิก',
            	handler: function() {
                    this.up('form').getForm().reset();
                }
            }*/
            ]
        }

    ]
	
});    
