Ext.define('tourism.view.course.PersonCourseAddEditForm',
{
					extend : 'Ext.form.Panel',
					alias : 'widget.person_course_addedit_form',
					requires : [
						'Ext.data.*',
						'Ext.form.*',
						'Ext.tab.Tab'
					],

					bodyPadding : 10,
					autoScroll : true,

					initComponent : function() {
						var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

						var courseStore = Ext.create(
								'tourism.store.combo.CourseNameStore', {
									storeId : 'guide-course-store'
								});
						var store = Ext.create('tourism.store.course.PersonTrainStore', {});       
						var provinceStore =  Ext.create('tourism.store.combo.MasProvinceStore',{
							storeId:'registration-masprovince-combo'
						});
						
						var universityStore = Ext.create(
								'tourism.store.combo.MasUniversityStore', {
									storeId : 'guide-train-university-store'
								});
						
						var amphurStore =   Ext.create('tourism.store.combo.MasAmphurStore',{
							storeId:'registration-address-masamphur-store'
						});
     
						var prefixStore = Ext.create('tourism.store.combo.MasPrefixStore',{});
      
						Ext.apply(this,{
							layout: 'anchor',
							items: [
						
									 {
					        	    	   xtype: 'hiddenfield',
					        	    	   name:  'personTrainedId'
					        	    },

					        	    {
										xtype : 'masuniversitycombo',
										name : 'masUniversityId',
										id : 'person-course-add-edit-mas-university-combo',
										fieldLabel : 'สถาบัน',
										labelWidth : 170,
										anchor: '100%',
										store : universityStore,
										queryMode : 'remote',
										displayField : 'universityName',
										valueField : 'masUniversityId',
										hiddenName : 'masUniversityId',
//										triggerAction : 'all',
										flex : 1,
										afterLabelTextTpl : required,
										masCourseComboId : 'person-course-add-edit-mas-course-combo',
										allowBlank : false
									},
			             			{
					                    	xtype: 'coursecombo',
						                    name: 'eduId',
						                    id : 'person-course-add-edit-mas-course-combo',
						                    fieldLabel: 'หลักสูตรการฝึกอบรม',
						                    store: courseStore,
						                    queryMode : 'remote',
											displayField : 'courseName',
											valueField : 'eduId',
											hiddenName : 'eduId',
											labelWidth: 170,
//											triggerAction : 'all',
											anchor: '100%',
											tabIndex : 1,
											allowBlank: false,
											masUniversityComboId : 'person-course-add-edit-mas-university-combo',
											afterLabelTextTpl: required
			                        },
			                        {
			                        	xtype: 'container',
			                        	layout: 'hbox',
			                        	items:[
			                        			{
			                        				xtype: 'fieldcontainer',
			                        				layout: 'anchor',
			                        				flex:1,
			                        				defaults:
			                        				{
			                        					anchor: '100%',
			                        					labelWidth: 170
			                        				},
			                        				items:[
			                        						 {
								                			    	   xtype: 'textfield',
								                			    	   fieldLabel: 'บัตรประจำตัวประชาชนเลขที่',
								                			    	   name: 'identityNo',
								                			    	   allowBlank: false,
								                			    	   afterLabelTextTpl: required,
								                			    	   tabIndex : 2
								                			  },
								                			  {
								                			  			xtype: 'fieldcontainer',
								                			  			fieldLabel: 'ชื่อ(ภาษาไทย)',
								                			    	    afterLabelTextTpl: required,
								                			    	    layout: 'hbox',
								                			  			items: [
								                			  						{
								                			  							xtype: 'combo',
										                			    	   			store: prefixStore,
										                			    	   			queryMode: 'remote',
										                			    	   		    displayField: 'prefixName',
										                			    	   		    valueField: 'prefixId',
										                			    	   		    hiddenName: 'prefixId',
										                			    	   		    name: 'prefixId',
										                			    	   		    triggerAction: 'all',
										                			    	   			flex: 1,
										                			    	   			allowBlank: false,					                			    	   	
										                			    	   			tabIndex : 4
								                			  						},
								                			  						
								                			  						{
												                			    	   		xtype: 'splitter'
												                			    	},
												                			    	{
												                			    	   		xtype: 'textfield',
												                			    	   		name: 'firstName',
												    	                                    flex: 2,
												    	                                    allowBlank: false,
												    	                                    afterLabelTextTpl: required,
												    	                                    tabIndex : 5
												                			    	}
								                			  						
								                			  			]
								                			  			
								                			  },               			  
							                			       {
							                			    	   xtype: 'fieldcontainer',
							                			    	   labelAlign: 'left',
							                			    	   fieldLabel: 'ชื่อ(ภาษาอังกฤษ)',
							                			    	   layout: 'hbox',
							                			    	   		items: [{
							                			    	   			xtype: 'combo',
							                			    	   			store: prefixStore,
							                			    	   		    queryMode: 'remote',
							                			    	   		    displayField: 'prefixNameEn',
							                			    	   		    valueField: 'prefixId',
							                			    	   		    hiddenName: 'prefixIdEn',
							                			    	   		    triggerAction: 'all',
							                			    	   		    name: 'prefixIdEn',
							                			    	   			flex:1,
							                			    	   			tabIndex : 7
							                			    	   	},
							                			    	   	{
							                			    	   		xtype: 'splitter'
							                			    	   	},
							                			    	   	{
							                			    	   		xtype: 'textfield',
							                			    	   		name: 'firstNameEn',
							    	                                    flex: 2,
							    	                                    tabIndex : 8
							                			    	   	}
							                			    	   
							                			    	   	]
							                			       
							                			       },{
							       	                            xtype: 'radiogroup',
							    	                            fieldLabel: 'เพศ',
							    	                            // Arrange checkboxes into two columns, distributed vertically
							    	                            columns: 2,
							    	                            tabIndex : 10,
							    	                            vertical: true,
							    	                            items: [
							    	                                { boxLabel: 'ชาย',  name: 'gender', inputValue: 'M', checked: true },
							    	                                { boxLabel: 'หญิง',  name: 'gender', inputValue: 'W' } 
							    	                            ]
							    	                        },{
							    	                        	 xtype: 'datefield',
							    	                             fieldLabel: 'เกิดวันที่',
							    	                             name: 'birthDate',
							    	                             format: 'd/m/B',
							    				                 tabIndex : 11
							    	                        },{
							    	                        	  xtype: 'masprovincecombo',
							    	                              id : 'guide-registration-masprovince-combo',
							    	                              fieldLabel: 'จังหวัด',
							    	                              store: provinceStore,
							    	                              displayField: 'provinceName',
							    	                              valueField: 'provinceId',
							    	                              hiddenName: 'provinceId',
							    	                              name: 'provinceId',
							    	                              allowBlank: false,
							    	                              afterLabelTextTpl: required,
							    	                              masamphurComboId:'guide-registration-masamphur-combo',
							    	                              queryParam: 'provinceName',
							    	                              tabIndex : 13
							                                },{
							    	                        	 xtype: 'datefield',
							    	                             fieldLabel: 'หมดอายุวันที่',
							    	                             name: 'identityNoExpire',
							    	                             format: 'd/m/B',
							    				                 tabIndex : 15
							    	                        }
			                        				]
			                        			},
			                        			{
			                        				xtype:'splitter'
			                        			},
			                        			{
			                        				xtype: 'fieldcontainer',
			                        				layout: 'anchor',
			                        				flex:1,
			                        				defaults: {
			                   					     anchor: '100%',
			                   					     labelWidth: 170
			                   					    },
			                        				items:[
			                        						 {
								    							    xtype: 'textfield',
								    							    fieldLabel: 'หนังสือเดินทางเลขที่',
								    							    name: 'passportNo',
								    			                    allowBlank: true,
								    			                    // afterLabelTextTpl: required,
								    			                    tabIndex : 3
								    						 },
								    	                        {
								    	                            // นามสกุล(ภาษาไทย)
								    	                            xtype: 'textfield',
								    	                            fieldLabel: 'นามสกุล(ภาษาไทย)',
								    	                            name: 'lastName',
								    	                            labelAlign: 'left',
								    			                    allowBlank: false,
								    			                    tabIndex : 6
								    	                        },
								    	                        {
								    	                            // นามสกุล(ภาษาอังกฤษ)
								    	                            xtype: 'textfield',
								    	                            fieldLabel: 'นามสกุล(ภาษาอังกฤษ)',
								    	                            name: 'lastNameEn',
								    	                            labelAlign: 'left',
								    			                    tabIndex : 9
								    	                        },
								    	                        {
								                                	// สัญชาติ
								    	                        	xtype: 'fieldcontainer',
								    	                            layout: 'hbox',
								    	                            items:[
								                                    {
								    		                            xtype: 'label',
								    		                            text: 'สัญชาติ',
								    		                            labelAlign: 'left'
								                                    },{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'}
								                                    ,{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'}
								                                    ,{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'}
								                                    ,{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'}
								                                    ,{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'}
								                                    ,{xtype: 'splitter'},{xtype: 'splitter'}
								                                    ,{
								    		                            xtype: 'label',
								    		                            text: 'ไทย',
								    		                            labelAlign: 'left'
								                                    }
								    	                            ]

								    	                        },{xtype: 'splitter'},
								    	                        {
								    	                            // อายุ
								    	                        	xtype: 'fieldcontainer',
								    	                            layout: 'hbox',
								    	                            items:[
								    	                            {
								    		                            xtype: 'textfield',
								    		                            labelWidth: 170,
								    		                            width: 350,
								    		                            fieldLabel: 'อายุ',
								    		                            name: 'ageYear',
								    		                            labelAlign: 'left',
								    				                    tabIndex : 12
								    	                            },{xtype: 'splitter'},{xtype: 'splitter'},
								                                    {
								    	                            	 xtype: 'label',
								    			                         text: 'ปี',
								    			                         labelAlign: 'left'
								    	                            }
								    	                     
								    	                            ]
								    	                        },{
								    	                        	  xtype: 'masamphurcombo',
								    	                        	  fieldLabel: 'อำเภอ/เขต',
								    	                              id : 'guide-registration-masamphur-combo',
								    	                              store: amphurStore,
								    	                              displayField: 'amphurName',
								    	                              valueField: 'amphurId',
								    	                              hiddenName: 'amphurId',
								    	                              name: 'amphurId',
								    	                              allowBlank: false,
								    	                              afterLabelTextTpl: required,
								    	                              tabIndex : 14,
								    	                              queryParam: 'amphurName',
								    	                              masprovinceComboId: 'guide-registration-masprovince-combo'
								                                }
			                        				]
			                        			}
			                        	]
			                        }
							     ],

									buttons : [ {
										text : 'บันทึก',
										scope : this,
										action : 'addPersonCourse',
										tabIndex : 16
									}, {
										text : 'ยกเลิก',
										scope : this,
										handler : this.onResetClick,
										tabIndex : 17
									} ]

						});


						this.callParent(arguments);
					},
					afterRender: function(){
						this.callParent(arguments);
						
						var form = this;
						
						 var preFixId = form.getForm().findField('prefixId');
						 preFixId.addListener("blur" ,function(textf, eOpts){
						 	var values  =  textf.up('form').getValues();
						 	var preFixIdEn = textf.up('form').getForm().findField('prefixIdEn');
							
							if((!Ext.isEmpty(preFixId)))
						 	{
						 		preFixIdEn.setValue(values.prefixId);
						 	}
						 },this);
						
					},
					onResetClick : function() {
						// this.getForm().reset();
						var win = this.up('window');
						win.close();
					}

				});
