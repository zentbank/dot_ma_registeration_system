Ext.define('tourism.view.course.PersonCourseAddEditWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.person_course_addedit_window',

    requires: [
        'tourism.view.course.PersonCourseAddEditForm'
    ],

    title : 'เพิ่มบุคคลตามหลักสูตรฝึกอบรม',
    layout: 'fit',
    width: 800,
    //True to make the window modal and mask everything behind it when displayed
    modal: true,


    initComponent: function() {


        this.items = [
            {
                xtype: 'person_course_addedit_form',
                frame: false,
                border: false
                //registrationModel: this.registrationModel
            }
            
        ];

        this.callParent(arguments);
    },

});
