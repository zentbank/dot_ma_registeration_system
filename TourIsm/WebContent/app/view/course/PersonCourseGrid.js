Ext.define('tourism.view.course.PersonCourseGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.person-course-grid',
		
		stripeRows: true,
		requires: [
			'Ext.grid.Column',
			'tourism.store.course.PersonTrainStore',
			'Ext.toolbar.Paging',
			'Ext.grid.plugin.RowEditing',
			'Ext.grid.column.Template',
			'Ext.grid.column.Action',
			'Ext.grid.plugin.RowExpander',
			'tourism.view.course.PersonCourseAddEditWindow'
		],
		initComponent: function(){
			var store = Ext.create('tourism.store.course.PersonTrainStore',{
				listeners: {
					scope: this,
					// single: true,
					beforeload: function( store, operation, eOpts )
				        {

				        	var formId = '#guide-tourleader-person-course-formsearch';
				        
				        	var formSearch = Ext.ComponentQuery.query(formId)[0];
				        	var values = formSearch.getValues();
								
				        	 for (field in values) 
						      {
						        if (Ext.isEmpty(values[field])) 
						        {
						          delete values[field];
						        }
						        
						      }
						    store.proxy.extraParams = values;
				        }
				}
			});

			Ext.apply(this, {
				store: store,
				dockedItems:[
					{
						xtype: 'pagingtoolbar',
						store: store,
						dock: 'bottom',
						displayInfo: true,
						items:[
							{
									text: 'เพิ่มหลักสูตรฝึกอบรม',
									action: 'add'
			        		}
		        		]
					}
				]
			});

			this.callParent(arguments);
		},
		columns: [
			{
				header: 'eduId',
				dataIndex: 'eduId',
				hidden: true
			},
			{
				header: 'provinceId',
				dataIndex: 'provinceId',
				hidden: true
			},
			{
				header: 'prefixId',
				dataIndex: 'prefixId',
				hidden: true
			},
			{
				header: 'amphurId',
				dataIndex: 'amphurId',
				hidden: true
			},
			{
				header: 'amphurId',
				dataIndex: 'amphurId',
				hidden: true
			},
			{
				header: 'personTrainedId',
				dataIndex: 'personTrainedId',
				hidden: true
			},
			{
				header: 'ชื่อ-นามสกุล',
				dataIndex: 'fullName',
				flex: 2
			},
			{
				header: 'เลขบัตรประจำประชาชน',
				dataIndex: 'identityNo',
				flex: 2
			},
			{
				header: 'ชื่อหลักสูตรฝึกอบรม-รุ่นที่',
				dataIndex: 'courseName',
				flex: 2.5/*,
				renderer: function(value, metaData, model)
				 {
	               return model.get('graduationCourse')+'  รุ่นที่ '+model.get('generationGraduate');
				 }*/
			},
			{
				header: 'มหาวิทยาลัย',
				dataIndex: 'universityName',
				flex: 2
			},
			{
				header: 'วันที่อบรม',
				dataIndex: 'studyDate',
				flex: 2,
				renderer: function(value, metaData, model)
				 {
	               return model.get('studyDate')+'-'+model.get('graduationDate');
				 }
			},
	        {
	        	header: '',
	            xtype: 'actioncolumn',
	           	width:50,
	           	align : 'center',
	            items: [{
	            	iconCls: 'icon-edit-xsmall',
	                tooltip: 'แก้ไข',
		            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
		                this.fireEvent('itemclick', this, 'editperson', grid, rowIndex, colIndex, record, node);
		            }
	            }]
	        },
	        {
	        	header: '',
	            xtype: 'actioncolumn',
	           	width:50,
	           	align : 'center',
	            items: [{
	                 iconCls: 'icon-delete-xsmall',
	                 tooltip: 'ลบ',
		            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {		            	
		            		this.fireEvent('itemclick', this, 'deleteperson', grid, rowIndex, colIndex, record, node);
		            }
	            }]
	        }
		]
        
});