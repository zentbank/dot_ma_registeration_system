Ext.define('tourism.view.course.PersonCoursePanel', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
        'tourism.view.course.PersonCourseSearchForm',
        'tourism.view.course.PersonCourseGrid'
        //'tourism.view.account.AccountGuideFormSearch'
    ],
    
    xtype: 'person-panel',
    initComponent: function(){

    	Ext.apply(this, {
    		layout: {
		        type: 'border'
		    },
		    items: [{
		        region: 'north',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items: [
		                {
		                	xtype: 'person-course-formsearch',
		                	title: 'ค้นหาบุคคลหลักสูตรฝึกอบรม',
		                	collapsible: true,   // make collapsible
		                	id: 'guide-tourleader-person-course-formsearch',
		    	            // collapsed : true,
		    	            // padding: '5px 5px 0px 5px',
		    	            frame: false,
		    	            border: false
		                }
		        ]
		    },{
		        region: 'center',
		        xtype: 'panel',
		        layout: 'fit',
		        frame: false,
		        border: false,
		        items:[{
		        	xtype: 'person-course-grid',
		        	title: 'รายการบุคคลหลักสูตรฝึกอบรม',
		            // padding: '5px 5px 5px 5px',
		            autoScroll: true,
		            border: false
		        }]
		    }]
    	});

    	this.callParent(arguments);
    }
});