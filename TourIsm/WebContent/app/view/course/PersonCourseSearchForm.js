Ext.define('tourism.view.course.PersonCourseSearchForm' ,{
    extend: 'Ext.form.FormPanel',
    alias : 'widget.person-course-formsearch',
    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.Time',
        'Ext.form.CheckboxGroup',
        'Ext.layout.container.HBox',
        'Ext.form.FieldSet',
        'Ext.util.Positionable'
    ],
    bodyPadding: 10,
    
    items: [
                {
            xtype: 'container',
            layout: 'anchor',
            defaults: {
                anchor: '100%',
                labelWidth: 150
            },
            items: [				
					                {
					                    xtype: 'textfield',
					                    fieldLabel: 'เลขบัตรประจำตัวประชาชน',
					                    name: 'identityNo',
					                    anchor: '60%',
					                    maxLength: '13',
					                },
					                {
					                    xtype: 'textfield',
					                    fieldLabel: 'ชื่อ',
					                    name: 'firstName',
					                    anchor: '60%'
					                },
					                {
					                    xtype: 'textfield',
					                    fieldLabel: 'นามสกุล',
					                    name: 'lastName',
					                    anchor: '60%'
					                },

					        	    {
										xtype : 'masuniversitycombo',
										name : 'masUniversityId',
										id: 'person-course-mas-university-combo',
										fieldLabel : 'สถาบัน',
										anchor: '60%',
										labelWidth : 150,
										store : 'tourism.store.combo.MasUniversityStore',
										queryMode : 'remote',
										displayField : 'universityName',
										valueField : 'masUniversityId',
										hiddenName : 'masUniversityId',
//										triggerAction : 'all',
										flex : 1,
//										afterLabelTextTpl : required,
										masCourseComboId : 'person-course-mas-course-combo',
										allowBlank : false
									},

					                {
					                    xtype: 'coursecombo',
					                    name: 'eduId',
					                    id: 'person-course-mas-course-combo',
					                    fieldLabel: 'หลักสูตรการฝึกอบรม',
					                    anchor: '60%',
					                    labelWidth : 150,
					                    store: 'tourism.store.combo.CourseNameStore',
					                    queryMode : 'remote',
										displayField : 'courseName',
										valueField : 'eduId',
										hiddenName : 'eduId',
//										triggerAction : 'all',
                                        allowBlank: true,
                                        masUniversityComboId: 'person-course-mas-university-combo',
          //                               ,
										// afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>',
										flex : 1
					                }
                     ]      
        },
        {
            xtype: 'toolbar',
            border: false,
            padding: '6px 0 6px 0px',
            items: [
            {
            	xtype: 'button',
                text : 'ค้นหา',
                action: 'searchPersonTrained'
            }/*,
            {
            	xtype: 'button',
            	text: 'ยกเลิก',
            	handler: function() {
                    this.up('form').getForm().reset();
                }
            }*/
            ]
        }

    ]
	
});    
