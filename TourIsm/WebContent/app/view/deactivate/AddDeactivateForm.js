Ext.define('tourism.view.deactivate.AddDeactivateForm', {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.data.*',
        'Ext.form.*',
        'Ext.tab.Tab',
        'tourism.view.deactivate.DeactivateTypeGrid'
    ],
    alias: 'widget.deactivate-addeditform',
    // title: 'ธุรกิจนำเที่ยว',
    bodyPadding: 5,
    autoScroll: true,

    initComponent: function(){

        var licenseDetail;

        if(this.traderType == 'B')
        {
            licenseDetail = this.businessDetail();
        }
        else
        {
            licenseDetail = this.personDetail();
        }


        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
        
        Ext.apply(this, {
            width: '100%',
            height: '100%',
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },

            items: 
            [ 
                {
                    xtype: 'container',
                    // title: 'Payment',
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%',
                        labelWidth: 250
                    },
                    items: 
                    [ 
                        {
                            xtype: 'container',
                            layout: 'hbox',
                            margin: '0 0 5 0',
                            items: [
                                {
                                    xtype: 'hiddenfield',
                                    name: 'deactiveId'
                                },
                                {
                                    xtype: 'hiddenfield',
                                    name: 'traderId'
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'deactivateNo',
                                    // labelAlign: 'left',
                                    fieldLabel: 'เลขที่รับเรื่อง',
                                    flex: 1,
                                    allowBlank: false,
                                    afterLabelTextTpl: required
                                },{
                                    xtype: 'splitter'
                                },{
                                    xtype: 'datefield',
                                    fieldLabel: 'วันที่รับเรื่อง',
                                    name: 'deactivateDate',
                                    flex: 1,
                                    format: 'd/m/B',
                                    allowBlank: false,
                                    afterLabelTextTpl: required
                                   
                                },{
                                    xtype: 'splitter'
                                },{
                                    xtype: 'textfield',
                                    name: 'officerName',
                                    fieldLabel: 'ผู้รับเรื่อง',
                                    flex: 1,
                                    allowBlank: false,
                                    margin: '0 0 0 5',
                                    allowBlank: false,
                                    afterLabelTextTpl: required
                                }
                            ]
                        }, 
                        // รายละเอียด license
                        licenseDetail,
                        //
                         
                        {
                           html: 'สาเหตุการยกเลิก :',
                           // flex: 1,
                           frame: false, 
                            border: false
                        },{
                            xtype: 'splitter'
                        }, {
                            xtype: 'deactivateType-grid',
                            autoScroll: true,
                            frame: false, 
                            border: false,
                            roleAction: this.roleAction,
                            traderType: this.traderType,
                            traderTypeName: this.traderTypeName,
                            storeId: 'licenses-'+this.roleAction+'-'+this.traderTypeName+'-store',
                            storeUrl: 'licenses/'+this.roleAction+'/'+this.traderTypeName+'/deactivatetype/read'
                        }
                        ,{
                            xtype: 'splitter'
                        }, {
                            xtype: 'textareafield',
                            name: 'deactivateResonRemark',
                            fieldLabel: 'หมายเหตุ'
                        },{
                            xtype: 'radiogroup',
                            id: 'licenses-'+this.roleAction+'-'+this.traderTypeName+'-approveStatus-checkboxgroup',
                            fieldLabel: 'สถานะการอนุมัติ',
                            allowBlank: false,
                            afterLabelTextTpl: required,
                            columns: 1,
                            vertical: true,
                            items: [
                                { boxLabel: 'รับเรื่องรออนุมัติ', name: 'approveStatus', inputValue: 'W' , checked: true},
                                { boxLabel: 'อนุมัติแล้ว', name: 'approveStatus', inputValue: 'A' }
                               
                            ],
                            listeners: {
                                
                                change : {
                                    fn: function(radiogroup, newValue, oldValue, eOpts){ 
                                        
                                        if(newValue.approveStatus == 'W')
                                        {


                                            var formSuspend = radiogroup.up('deactivate-addeditform'); 

                                            formSuspend.down('#licenses-deactivate-'+formSuspend.traderTypeName+'-approveDeactivate').setVisible(false);
                                            formSuspend.down('#licenses-deactivate-'+formSuspend.traderTypeName+'-addDeactivate').setVisible(true);

                                            var renewLicenseStatus =  formSuspend.down('#licenses-deactivate-'+formSuspend.traderTypeName+'-renewLicenseStatus-checkboxgroup');

                                            renewLicenseStatus.allowBlank =  true;
                                            renewLicenseStatus.setVisible(false);

                                            var bookDate = formSuspend.getForm().findField('bookDate');
                                            bookDate.allowBlank =  true;
                                            bookDate.setVisible(false);
                                            bookDate.setValue('');

                                            var bookNo = formSuspend.getForm().findField('bookNo');
                                            bookNo.allowBlank =  true;
                                            bookNo.setVisible(false);
                                            bookDate.setValue('');

                                        }
                                        else if(newValue.approveStatus == 'A')
                                        {
                                            var formSuspend = radiogroup.up('deactivate-addeditform'); 

                                            formSuspend.down('#licenses-deactivate-'+formSuspend.traderTypeName+'-approveDeactivate').setVisible(true);
                                            formSuspend.down('#licenses-deactivate-'+formSuspend.traderTypeName+'-addDeactivate').setVisible(false);
                                            var renewLicenseStatus =  formSuspend.down('#licenses-deactivate-'+formSuspend.traderTypeName+'-renewLicenseStatus-checkboxgroup');

                                            renewLicenseStatus.allowBlank =  false;
                                            renewLicenseStatus.setVisible(true);

                                            var bookDate = formSuspend.getForm().findField('bookDate');
                                            bookDate.allowBlank =  false;
                                            bookDate.setVisible(true);

                                            var bookNo = formSuspend.getForm().findField('bookNo');
                                            bookNo.allowBlank =  false;
                                            bookNo.setVisible(true);
                                        }
                                    }
                                }
                               
                            }
                            
                        },{
                            xtype: 'datefield',
                            fieldLabel: 'วันที่อนุมัติ',
                            name: 'bookDate',
                            format: 'd/m/B',
                            allowBlank: false,
                            afterLabelTextTpl: required
                        }, {
                            xtype: 'textfield',
                            name: 'bookNo',
                            fieldLabel: 'เลขที่หนังสือ',
                            allowBlank: false,
                            afterLabelTextTpl: required
                        },{
                            xtype: 'radiogroup',
                            id: 'licenses-'+this.roleAction+'-'+this.traderTypeName+'-renewLicenseStatus-checkboxgroup',
                            fieldLabel: 'ข้าพเจ้า',
                            allowBlank: false,
                            afterLabelTextTpl: required,
                            columns: 1,
                            vertical: true,
                            items: [
                                { boxLabel: 'ประสงค์ยื่นคำขอรับใบอนุญาตฯใหม่ต่อไป', name: 'renewLicenseStatus', inputValue: '1' },
                                { boxLabel: 'ไม่ประสงค์ยื่นคำขอรับใบอนุญาตฯใหม่', name: 'renewLicenseStatus', inputValue: '2', checked: true }
                               
                            ]
                        }
                    ]
                }
            ],

            buttons: [{
                text: 'บันทึกข้อมูล',
                action: 'addDeactivate',
                id: 'licenses-'+this.roleAction+'-'+this.traderTypeName+'-addDeactivate'
            },{
                text: 'อนุมัติ',
                action: 'approveDeactivate',
                id: 'licenses-'+this.roleAction+'-'+this.traderTypeName+'-approveDeactivate'
            }, {
                text: 'ยกเลิก',
                // width: 150,
                scope: this,
                handler: this.onResetClick
            }]    
        });
        this.callParent(arguments);
    },
    businessDetail: function()
    {
        return {
                    xtype: 'container',
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%',
                        labelWidth: 250
                    },
                    margin: '0 0 5 0',
                    items: [
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'เลขที่ใบอนุญาต',
                            name: 'licenseNo'
                           
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'ชื่อประกอบธุรกิจนำเที่ยว',
                            name: 'traderName'
                           
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'ชื่อประกอบธุรกิจนำเที่ยว(ภาษาต่างประเทศ)',
                            name: 'traderNameEn'
                           
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'ผู้ขอจดทะเบียน',
                            name: 'traderOwnerName'
                           
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'เลขที่นิติบุคคล',
                            name: 'identityNo'
                           
                        },
                        
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'ประเภทธุรกิจนำเที่ยว',
                            name: 'traderCategoryName'
                           
                        }
                    ]
                };
    }
    ,personDetail: function()
    {
        return {
                    xtype: 'container',
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%',
                        labelWidth: 250
                    },
                    margin: '0 0 5 0',
                    items: [
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'เลขที่ใบอนุญาต',
                            name: 'licenseNo'
                           
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'ผู้ขอจดทะเบียน',
                            name: 'traderOwnerName'
                           
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'เลขที่นิติบุคคล',
                            name: 'identityNo'
                           
                        },
                        
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'ประเภทการจดทะเบียน',
                            name: 'traderCategoryName'
                           
                        }
                    ]
                };
    },onResetClick: function(){
        this.up('window').close();
    }
    
});