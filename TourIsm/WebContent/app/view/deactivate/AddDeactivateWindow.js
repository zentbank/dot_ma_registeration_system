Ext.define('tourism.view.deactivate.AddDeactivateWindow', {
	extend : 'Ext.window.Window',
	alias : 'widget.deactivate-add-window',

	requires : [
	// 'tourism.view.punishment.suspension.SelectorBusinessPanel',
	// 'tourism.view.info.SearchLicensePanel',
	'tourism.view.deactivate.SearchLicenseForm',
			'tourism.view.deactivate.AddDeactivateForm' ],

	initComponent : function() {
		var formModel = {

			xtype : 'deactivate-addeditform',
			id : 'licenses-' + this.roleAction + '-' + this.traderTypeName
					+ '-formaddedit',
			border : false,
			frame : false,
			roleAction : this.roleAction,
			traderType : this.traderType,
			traderTypeName : this.traderTypeName
		};

		// var searchlicensepanel =
		// Ext.create('tourism.view.info.SearchLicensePanel',{
		// id:
		// 'licenses-'+this.roleAction+'-'+this.traderTypeName+'-searchlicensepanel',
		// border: false,
		// frame: false,
		// roleAction: 'deactivate'
		// });
		var searchlicensepanel = Ext.create(
				'tourism.view.deactivate.SearchLicenseForm', {
					id : 'licenses-' + this.roleAction + '-'
							+ this.traderTypeName + '-searchlicensepanel',
					border : false,
					frame : false,
					roleAction : 'deactivate'
				});

		// var licensegrid = searchlicensepanel.down('info-license-grid');
		// var licensestore = licensegrid.getStore();
		// licensestore.on('beforeload',function(store, operation, eOpts){
		// store.proxy.extraParams = {roleAction:'deactivate'};
		// });

		Ext.apply(this, {
			title : 'ยกเลิกใบอนุญาต',
			// layout: 'fit',
			layout : 'card',
			activeItem : 0,
			autoShow : true,
			width : 750,
			height : 550,
			autoShow : true,
			modal : true,
			items : [

			searchlicensepanel, formModel

			]
		});

		this.callParent(arguments);
	},

});
