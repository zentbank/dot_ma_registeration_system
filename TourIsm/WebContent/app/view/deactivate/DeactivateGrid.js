Ext.define('tourism.view.deactivate.DeactivateGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.deactivate-grid',
		stripeRows: true,
		requires: [
			'Ext.grid.Column',
			'tourism.store.deactivate.DeactivateStore',
			'Ext.toolbar.Paging',
			'Ext.grid.plugin.RowEditing',
			'Ext.grid.column.Template',
			'Ext.grid.column.Action',
			'Ext.grid.plugin.RowExpander',
			'Ext.grid.feature.Grouping',
			'tourism.view.deactivate.AddDeactivateWindow'
	
	
		],
		initComponent: function(){

	
			var columnModel = this.getColumn();

			

			var deactivateStore = Ext.create('tourism.store.deactivate.DeactivateStore',{
					storeId: this.storeId,
					model : 'tourism.model.deactivate.DeactivateModel',
					roleAction: this.roleAction,
		            traderType: this.traderType,
		            pageSize : this.storepageSize,
		            traderTypeName: this.traderTypeName,
				    proxy: {
					    type: 'ajax',
					    actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
					    api: {
					        read: this.storeUrl
					    },
					    reader: {
					        type: 'json',
					        // metaProperty: '',
					        root: 'list',
					        // idProperty: 'emailId',
					        totalProperty: 'totalCount',
					        successProperty: 'success',
					        messageProperty: 'message'
					    },
					    writer: {
					        type: 'json',
					        // encode: true,
					        writeAllFields: true, ////just send changed fields
					        // root: 'data',
					        allowSingle: false, //always wrap in an array
					        batch: false
					    },
					    listeners: {
					        exception: function(proxy, response, operation){

					            Ext.MessageBox.show({
					                title: 'REMOTE EXCEPTION',
					                msg: operation.getError(),
					                icon: Ext.MessageBox.ERROR,
					                buttons: Ext.Msg.OK
					            });
					        }
					    }

					}
					,listeners: {
						beforeload: function( store, operation, eOpts )
					        {					        	
					        	var formId = '#licenses-'+store.roleAction+'-'+store.traderTypeName+'-formsearch';

					        	var formSearch = Ext.ComponentQuery.query(formId)[0];
					        	var values = formSearch.getValues();
   								
					        	 for (field in values) 
							      {
							        if (Ext.isEmpty(values[field])) 
							        {
							          delete values[field];
							        }
							        
							      }
							    store.proxy.extraParams = values;
					        }
					}
			});
	   

			Ext.apply(this, {
				store: deactivateStore,
				dockedItems:[
					{
						xtype: 'pagingtoolbar',
						store: deactivateStore,
						dock: 'bottom',
						displayInfo: true,
						items: [
							{
									text: 'เพิ่มการยกเลิกใบอนุญาต',
									action: 'adddeactivate'
			        		}
						]

					}
				],
				columns: columnModel
			});

			this.callParent(arguments);

		},
		 plugins: [{
            ptype: 'rowexpander',
            // pluginId: 'rowexpanderTourleader',
            rowBodyTpl : new Ext.XTemplate(
            	'<div>',
            	'<tpl switch="traderType">',
		            '<tpl case="B">',
		                '<p><b>ออกใบอนุญาตให้: {traderOwnerName}</b></p>',
	            		'<p><b>ทะเบียนนิติบุคคลเลขที่: {identityNo}</b></p>',
	            		'<p><b>ประเภทธุรกิจนำเที่ยว: {traderCategoryName}</b></p>',
		            	'<p><b>ชื่อธุรกิจนำเที่ยว (ภาษาไทย): {traderName}</b></p>',
		                '<p><b>ชื่อธุรกิจนำเที่ยว (ภาษาต่างประเทศ): {traderNameEn}</b> </p>',
		                '<p><b>เลขที่รับเรื่อง: {deactivateNo}</b> </p>',
		                '<p><b>วันที่รับเรื่อง : {deactivateDate}</b> </p>',
		                // '<p><font size="2">{traderAddress}</font></p>',
		            '<tpl case="G">',
		            	'<p><b>ออกใบอนุญาตให้: {traderOwnerName}</b></p>',
	            		'<p><b>หมายเลขบัตรประชาชน: {identityNo}</b></p>',
	            		'<p><b>ประเภทมัคคุเทศก์: {traderCategoryName}</b></p>',
	            		'<p><b>เลขที่รับเรื่อง: {deactivateNo}</b> </p>',
		                '<p><b>วันที่รับเรื่อง : {deactivateDate}</b> </p>',
		                // '<p><font size="2">{traderAddress}</font></p>',
		            '<tpl default>',
		            	'<p><b>ออกใบอนุญาตให้: {traderOwnerName}</b></p>',
	            		'<p><b>หมายเลขบัตรประชาชน: {identityNo}</b></p>',
	            		'<p><b>ประเภทมัคคุเทศก์: {traderCategoryName}</b></p>',
	            		'<p><b>เลขที่รับเรื่อง: {deactivateNo}</b> </p>',
		                '<p><b>วันที่รับเรื่อง : {deactivateDate}</b> </p>',
		                // '<p><font size="2">{traderAddress}</font></p>',
		        '</tpl>',
            		
                '<div>'
            )
        }],
        getColumn: function()
        {
        	return [
					{
						header: 'deactiveId',
						dataIndex: 'deactiveId',
						hidden: true
					},
					{
						header: 'ใบอนุญาต',
						dataIndex: 'traderTypeName',
						hidden: true
					},
					{
						header: 'เลขที่ใบอนุญาต',
						dataIndex: 'licenseNo',
						flex: 1
					},
					{
						header: 'เลขที่หนังสือ',
						dataIndex: 'bookNo',
						flex: 1
					},
					{
						header: 'วันที่อนุมัติ',
						dataIndex: 'bookDate',
						flex: 1
					},
					{
						header: 'สถานะ',
						dataIndex: 'approveStatus',
						flex: 2,
						renderer: function(value, metaData, model){
							
							        /*
                                   	
	WAIT("W" , "รับเรื่องรออนุมัติ"),
	ACCEPT("A" , "อนุมัติแล้ว"),
	CANCEL("C", "ยกเลิกการยกเลิก");
	
                            */
                            
							if(value == "A")
							{
								return '<span class="tour-fg-color-redLight">อนุมัติแล้ว</span>';
							}
							if(value == "C")
							{
								return '<span class="tour-fg-color-teal">ยกเลิกการอนุมัติ</span>';
							}
							if(value == "W")
							{
								return '<span class="tour-fg-color-yellow">รับเรื่องรออนุมัติ</span>';
							}
							
						}
					},
				
			        {
			        	header: 'อนุมัติ',
			            xtype: 'actioncolumn',
			           	width:100,
			           	align : 'center',
			            items: [{
			                iconCls: 'icon-edit-xsmall',
			                tooltip: 'อนุมัติการยกเลิก',
				            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {

				            		this.fireEvent('itemclick', this, 'approve', grid, rowIndex, colIndex, record, node);
							}
			            }]
			        },
			        {
			        	header: 'ยกเลิก',
			            xtype: 'actioncolumn',
			           	width:60,
			           	align : 'center',
			            items: [{
			                // iconCls: 'icon-edit-xsmall',
			                // tooltip: 'รายละเอียด',
			               	getClass: function(v, meta, rec) {          
			                   

			                     this.items[0].tooltip = 'ยกเลิกการพักใช้';
			                        return 'icon-edit-xsmall';
			                },
				            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
			                   
			                    this.fireEvent('itemclick', this, 'canceldeactivate', grid, rowIndex, colIndex, record, node);
				            		
							}
			            }]
			        }
				];
        }
	});