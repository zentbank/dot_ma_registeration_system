Ext.define('tourism.view.deactivate.DeactivateTypeGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.deactivateType-grid',
		
		// stripeRows: true,
		requires: [
			'Ext.grid.Column',
			'Ext.grid.column.Template',
			'Ext.grid.column.Action',
			'tourism.store.deactivate.DeactivateTypeStore'


		],
		initComponent: function(){

			// create every new form 
			var store = Ext.create('tourism.store.deactivate.DeactivateTypeStore',{
					storeId: this.storeId,
				    proxy: {
					    type: 'ajax',
					    actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
					    api: {
					        read: this.storeUrl
					    },
					    reader: {
					        type: 'json',
					        // metaProperty: '',
					        root: 'list',
					        // idProperty: 'emailId',
					        totalProperty: 'totalCount',
					        successProperty: 'success',
					        messageProperty: 'message'
					    },
					    writer: {
					        type: 'json',
					        // encode: true,
					        writeAllFields: true, ////just send changed fields
					        // root: 'data',
					        allowSingle: false, //always wrap in an array
					        batch: false
					    },
					    listeners: {
					        exception: function(proxy, response, operation){

					            Ext.MessageBox.show({
					                title: 'REMOTE EXCEPTION',
					                msg: operation.getError(),
					                icon: Ext.MessageBox.ERROR,
					                buttons: Ext.Msg.OK
					            });
					        }
					    }
					}
			});

			Ext.apply(this, {
				store: store,
				hideHeaders: true,
				hideMode: 'display' ,
				rowLines: false,
				plugins:[
					{
						ptype: 'rowediting',
						pluginId: 'rowediting-grid-deactivateType',
						clicksToEdit: 2,
						autoCancel: false
					}
				], 
				columns: [
					{
						header: 'masDeactivateTypeId',
						dataIndex: 'masDeactivateTypeId',
						hidden: true
					},
					{
						header: 'deactiveId',
						dataIndex: 'deactiveId',
						hidden: true
					},
					{
			            xtype: 'checkcolumn',
			            header: '',
			            dataIndex: 'active',
			            width: 30,
			            stopSelection: false
			        },
			        
					{
						header: '',
						dataIndex: 'deactivateName',
						flex: 4,
						editor: {
			                 // defaults to textfield if no xtype is supplied
			                 allowBlank: false
			             }
					}			
				]
				
			});

			this.callParent(arguments);
		}
	});