Ext.define('tourism.view.deactivate.PersonDeactivateFormSearch' ,{
    // extend: 'Ext.form.FieldSet',
    extend: 'Ext.form.FormPanel',
    alias : 'widget.person-deactivate-formsearch',
    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.Time',
        'Ext.form.CheckboxGroup',
        'Ext.layout.container.HBox',
        'Ext.form.FieldSet'
    ],
    bodyPadding: 10,
    initComponent: function(){

        Ext.apply(this, {
            items: [
                {
                        xtype: 'container',
                        // title: 'Payment',
                        layout: 'anchor',
                        defaults: {
                            anchor: '100%',
                            labelWidth: 250
                        },
                        items: [
                            {
                                xtype: 'container',
                                layout: 'hbox',
                                layoutCongig: {
                                     pack:'center',
                                     align:'middle'
                                },
                                items: [
                                    {
                                        xtype: 'container',
                                        // title: 'Payment',
                                        flex: 1,
                                        layout: 'anchor',
                                        defaults: {
                                            anchor: '100%',
                                            labelWidth: 250
                                        },
                                        items: [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'เลขที่ใบอนุญาต',
                                                name: 'licenseNo'
                                            }
                                        ]
                                    },{
                                        xtype: 'splitter'
                                    },
                                    {
                                        xtype: 'container',
                                        flex: 1,
                                        layout: 'anchor',
                                        defaults: {
                                            anchor: '100%',
                                            labelWidth: 250
                                        },
                                        items: [
                                            {
		                                        xtype: 'textfield',
		                                        fieldLabel: 'เลขบัตรประชาชน',
		                                        name: 'identityNo'
		                                    }
                                        ]
                                    }
                                ]
                            },
                            {
                                xtype: 'container',
                                layout: 'hbox',
                                layoutCongig: {
                                     pack:'center',
                                     align:'middle'
                                },
                                items: [
                                    {
                                        xtype: 'container',
                                        // title: 'Payment',
                                        flex: 1,
                                        layout: 'anchor',
                                        defaults: {
                                            anchor: '100%',
                                            labelWidth: 250
                                        },
                                        items: [
                                             {
                                                xtype: 'textfield',
                                                fieldLabel: 'ชื่อผู้ขออนุญาต',
                                                name: 'firstName',
                                                anchor: '100%'
                                            },
                                             {
                                                xtype: 'combo',
                                                fieldLabel: 'สถานะเรื่อง',
                                                store: 'tourism.store.combo.DeactivateStatusStore',
                                                queryMode: 'local',
                                                displayField: 'approveStatusName',
                                                valueField: 'approveStatus',
                                                hiddenName: 'approveStatus',
                                                name :'approveStatus'
                                            }
                                        ]
                                    },{
                                        xtype: 'splitter'
                                    },
                                    {
                                        xtype: 'container',
                                        flex: 1,
                                        layout: 'anchor',
                                        defaults: {
                                            anchor: '100%',
                                            labelWidth: 250
                                        },
                                        items: [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'นามสกุล',
                                                name: 'lastName'
                                            }
                                            
                                        ]
                                    }
                                ]
                            }
                            
                        ]
                },
                {
                    xtype: 'toolbar',
                    border: false,
                    padding: '6px 0 6px 0px',
                    items: [{
                        xtype: 'button',
                        text : 'ค้นหา',
                        action: 'searchDeactivate'
                    }]
                }

            ]            
        });

        this.callParent(arguments);
    }
    // width: '100%',

});    
