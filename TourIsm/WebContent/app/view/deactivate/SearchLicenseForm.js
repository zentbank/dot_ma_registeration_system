Ext.define('tourism.view.deactivate.SearchLicenseForm', {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.data.*',
        'Ext.form.*',
        'Ext.tab.Tab',
        'tourism.view.deactivate.SearchLicenseGrid'
    ],
    alias: 'widget.deactivate-searchLicenseForm',
    bodyPadding: 5,
    autoScroll: true,
    initComponent: function(){
        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
        Ext.apply(this, {
            width: '100%',
            height: '100%',
            fieldDefaults: {
                labelAlign: 'left',
                msgTarget: 'qtip'
            },
            items: [ {
                xtype: 'container',
                layout: 'anchor',
                defaults: {
                    anchor: '100%',
                    labelWidth: 150
                },
                items: 
                [{
                    xtype: 'container',
                    layout: 'hbox',
                    items: [{
                         xtype: 'textfield',
                         name: 'licenseNo',
                         fieldLabel: 'เลขที่ใบอนุญาต',
                         flex: 3
                        
                    },{
                        xtype: 'splitter'
                    }, {

                        xtype: 'button',
                        text: 'ค้นหา',
                        flex: 1,
                        action: 'searchLicense'
                    }]

                },
                {
                    xtype: 'splitter'
                },
                {
                    xtype: 'container',
                    layout: 'fit',
                    items: [
                        {
                            xtype: 'diactivate-searchLicenseGrid',
                            border: false,
                            frame: false,
                            heigth: '100%'
                        }
                    ]

                }]
            }],
            buttons: [{
                text: 'ดำเนินการต่อ',
                action: 'selectedLicenseNo'
            }]    
        });
        this.callParent();
    }
});