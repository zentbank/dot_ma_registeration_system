Ext.define('tourism.view.deactivate.SearchLicenseGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.diactivate-searchLicenseGrid',
		stripeRows: true,
		requires: [
			'Ext.grid.Column',
			'tourism.store.registration.RegistrationStore',
			'Ext.grid.column.Template'
		],
		initComponent: function(){
			var registrationStore = Ext.create('tourism.store.registration.RegistrationStore',{
				proxy: {
					type: 'ajax',
					actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
					api: {
					    read: 'business/registration/trader/readbylicenseno'
					},
					reader: {
					    type: 'json',
					    root: 'list',
					    totalProperty: 'totalCount',
					    successProperty: 'success',
					    messageProperty: 'message'
					},
					listeners: {
					    exception: function(proxy, response, operation){

					        Ext.MessageBox.show({
					            title: 'REMOTE EXCEPTION',
					            msg: operation.getError(),
					            icon: Ext.MessageBox.ERROR,
					            buttons: Ext.Msg.OK
					        });
					    }
					}
				}
			});

			Ext.apply(this, {
				store: registrationStore,
				selType: 'checkboxmodel',
				mode: 'SINGLE',
				columns: [
					{
						header: 'traderId',
						dataIndex: 'traderId',
						hidden: true
					},
					{
						header: 'ชื่อ',
						dataIndex: 'traderName',
						flex: 2,
						renderer: function(value, metaData, model){

							var renderer = '<p><b>ออกใบอนุญาตให้: '+model.get('traderOwnerName')+'</b></p>';
							renderer = renderer + '<p><b>ประเภทธุรกิจนำเที่ยว: '+model.get('traderCategoryName')+'</b></p>';
							renderer = renderer + '<p><b>ชื่อธุรกิจนำเที่ยว:</b></p>';
							renderer = renderer + '<p><b>'+model.get('traderName')+'</b></p>';
							renderer = renderer + '<p><b> ('+model.get('traderNameEn')+')</b></p>';
							renderer = renderer + '<p><b> วันเริ่มต้น</b></p>';
							renderer = renderer + '<p><b> ('+model.get('effectiveDate')+')</b></p>';
							renderer = renderer + '<p><b> วันที่ครบกำหนดชำระค่าธรรมเนียม</b></p>';
							renderer = renderer + '<p><b> '+model.get('expireDate')+'</b></p>';

							


							if(model.get('traderType') == 'G')
							{
								var renderer = '<p><b>ออกใบอนุญาตให้: '+model.get('traderOwnerName')+'</b></p>';
								renderer = renderer + '<p><b>ประเภทมัคคุเทศน์: '+model.get('traderCategoryGuideTypeName')+'</b></p>';
								renderer = renderer + '<p><b>ชนิดมัคคุเทศน์: '+model.get('traderCategoryName')+'</b></p>';
								renderer = renderer + '<p><b>ชื่อมัคคุเทศน์: '+model.get('traderOwnerName')+'</b></p>';
								renderer = renderer + '<p><b>ชื่อมัคคุเทศน์(ภาษาอังกฤษ): '+model.get('traderOwnerNameEn')+'</b></p>';
								renderer = renderer + '<p><b>บัตรประจำตัวเลขที่: '+model.get('identityNo')+'</b></p>';
								renderer = renderer + '<p><b> วันเริ่มต้น</b></p>';
								renderer = renderer + '<p><b> ('+model.get('effectiveDate')+')</b></p>';
								renderer = renderer + '<p><b> วันที่สิ้นสุด</b></p>';
								renderer = renderer + '<p><b> '+model.get('expireDate')+'</b></p>';
					
							}

							if(model.get('traderType') == 'L')
							{
								var renderer = '<p><b>ออกใบอนุญาตให้: '+model.get('traderOwnerName')+'</b></p>';
								renderer = renderer + '<p><b>ประเภทผู้นำเที่ยว: '+model.get('traderCategoryName')+'</b></p>';
								renderer = renderer + '<p><b>ชื่อผู้นำเที่ยว: '+model.get('traderOwnerName')+'</b></p>';
								renderer = renderer + '<p><b>ชื่อผู้นำเที่ยว(ภาษาอังกฤษ): '+model.get('traderOwnerNameEn')+'</b></p>';
								renderer = renderer + '<p><b>บัตรประจำตัวเลขที่: '+model.get('identityNo')+'</b></p>';

				
							}
							
							return renderer;
							
						}
						
					},
					{
						header: 'สถานะใบอนุญาต',
						dataIndex: 'licenseStatus',
						flex: 1,
						renderer: function(value, metaData, model){
							if(value == "S")
							{
								return '<span class="tour-fg-color-redLight">พักใช้ใบอนุญาต</span>';
							}
							if(value == "R")
							{
								return '<span class="tour-fg-color-redLight">เพิกถอนใบอนุญาต</span>';
							}
							if(value == "C")
							{
								return '<span class="tour-fg-color-redLight">ยกเลิก</span>';
							}
							if(value == "N")
							{
								return '<span class="tour-fg-color-green">ปกติ</span>';
							}
							if(value == "D")
							{
								return '<span class="tour-fg-color-lighten">ไม่ผ่านการตรวจสอบ/ยกเลิกการจดทะเบียน</span>';
							}
							
						}
					}
				]
			});

			this.callParent(arguments);
		}
	});