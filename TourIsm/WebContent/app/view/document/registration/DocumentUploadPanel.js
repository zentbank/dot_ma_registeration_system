Ext.define('tourism.view.document.registration.DocumentUploadPanel',{
	extend: 'Ext.window.Window',
	alias: 'widget.document_registration_upload_window',
	requires: [
//	           'tourism.view.information.tourleader.InfoTourleaderForm'
	],
	title: 'File Upload Form',
	layout: 'fit',
	width: 500,
//	height: 600,
	//True to make the window modal and mask everything behind it when displayed
    modal: true,
    
    initComponent: function(){
    	
    	this.items =[
    	    {
    	    		xtype: 'form',
    	            bodyPadding: '10 10 0',

    	            defaults: {
    	                anchor: '100%',
    	                allowBlank: false,
    	                msgTarget: 'side',
    	                labelWidth: 50
    	            },
    	            
    	    		frame: false,
                    border: false,
                    id: this.roleAction+'-'+this.actionMode+'-'+this.traderType+'-panel',
                    
    	            items: [
					{
						 xtype: 'hiddenfield',
					  name: 'traderType'
					}
					,		
					{
						 xtype: 'hiddenfield',
					   name: 'licenseType'
					}
					,      
					{
						 xtype: 'hiddenfield',
					    name: 'regDocId'
					}
					,
    	            {
    	            	 xtype: 'hiddenfield',
                         name: 'traderId'
    	            }
    	            ,{
    	            	xtype: 'hiddenfield'
    	            	,name: 'masDocId'
    	            }
    	            ,{
    	                xtype: 'displayfield',
    	                fieldLabel: 'เอกสาร'
    	                ,name: 'documentName'
    	            },
    	            {
    	                xtype: 'filefield',
    	                id: 'form-file',
    	                emptyText: 'Select an file',
    	                fieldLabel: 'File',
    	                name: 'file',
    	                buttonText: '',
    	                buttonConfig: {
    	                    iconCls: 'upload-icon'
    	                }
    	            }
    	    		]

    	            ,buttons: [{
    	                text: 'Save',
    	                handler: function(btn){
    	                    var form = this.up('window').down('form').getForm();
    	                    if(form.isValid()){
    	                        form.submit({
    	                            url: 'business/document/registration/fileupload',
    	                            method: 'POST',
    	                            waitMsg: 'Uploading your file...',
    	                            success: function(form, action) {
    	                            	
    	                            	 //console.log(action.result);
    	                            	
    	                            	// Ext.Msg.alert('', 'บันทึกข้อมูล');
    	               	             var noti = Ext.create('widget.uxNotification', {
    	               	               // title: 'Notification',
    	               	               position: 'tr',
    	               	               manager: 'instructions',
    	               	               // cls: 'ux-notification-light',
    	               	               // iconCls: 'ux-notification-icon-information',
    	               	               html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
    	               	               closable: false,
    	               	               autoCloseDelay: 4000,
    	               	               width: 300,
    	               	               slideBackDuration: 500,
    	               	               slideInAnimation: 'bounceOut',
    	               	               slideBackAnimation: 'easeIn'
    	               	             });
    	               	             
       	               	            noti.on('show',function(){
       	               	            	
       	               	            	var traderType = this.up('window').down('form').getForm().findField('traderType').getValue();
       	               	            	var gridId = 'document-'+traderType+'-popup-grid';
  	               	            	   
  	               	            	   	  var grid = Ext.ComponentQuery.query(gridId)[0];     
		  	                              grid.getStore().load({
		  	                            	  params: {
		  	                            		  traderId: this.up('window').down('form').getForm().findField('traderId').getValue() 
		  	                            		  ,recordStatus: 'N'
		  	                            		  ,licenseType: this.up('window').down('form').getForm().findField('licenseType').getValue()
		  	                            		  ,registrationType: 'N'
		  	                            	  },
		  	                                  callback: function(records, operation, success) {
		  	                                       //console.log(records);
		  	                                  },
		  	                                  scope: this
		  	                              }); 
		  	                              
		  	                            this.up('window').close();
  	               	                },btn);
    	               	             
    	               	             noti.show();
    	               	            
    	                            	
    	                            }
    	                        	,failure: function(form, action)
    	                        	{
    	                        		switch (action.failureType) {
    	                                case Ext.form.action.Action.CLIENT_INVALID:
    	                                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
    	                                    break;
    	                                case Ext.form.action.Action.CONNECT_FAILURE:
    	                                    Ext.Msg.alert('Failure', 'Ajax communication failed');
    	                                    break;
    	                                case Ext.form.action.Action.SERVER_INVALID:
    	                                   Ext.Msg.alert('Failure', "ไม่สามารถบันทึกไฟล์ข้อมูลเอกสารได้");//action.result.msg);
    	                        		}
    	                        	}
    	                        });
    	                    }
    	                }
    	            } 
//    	            	,{
//    	                text: 'Reset',
//    	                handler: function() {
//    	                	alert(this.up('window').down('form').getForm().findField('traderId').getValue());
//    	                	alert(this.up('window').down('form').getForm().findField('licenseType').getValue());
//    	                	
////    	                    this.up('window').down('form').getForm().reset();
//    	                }
//    	            }
    	    	]

    	    }
    	];
    	
    	this.callParent(arguments);
    }
	,loadFormRecord: function(model){
		var info = this.down('form');
        info.loadRecord(model);
        
        info.getForm().findField('traderType').setValue(this.traderType);
        
        
	}
	
});















