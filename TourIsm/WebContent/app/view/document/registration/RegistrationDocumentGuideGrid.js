Ext.define('tourism.view.document.registration.RegistrationDocumentGuideGrid',{
	extend: 'Ext.grid.GridPanel',
	alias: 'widget.document-guide-grid',
	stripeRows: true,
	requires: [
		'Ext.grid.Column',
		'Ext.toolbar.Paging',
		'Ext.grid.plugin.RowEditing',
		'Ext.grid.column.Template',
		'Ext.grid.column.Action',
		'Ext.grid.plugin.RowExpander',
		'Ext.grid.feature.Grouping'
		,'tourism.store.registration.RegistrationStore'
		,'tourism.view.information.guide.InfoGuideWindow'
	]
	,initComponent: function(){
		
		var documentRegistrationStore = Ext.create('tourism.store.registration.RegistrationStore',{
			storeId: this.storeId,
			pageSize : this.storepageSize,
			groupField: 'licenseNo',
		    roleAction: this.roleAction,
            traderType: this.traderType,
			proxy: {
				type: 'ajax',
				actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
				api: {
				    read: this.storeUrl
				},
				reader: {
				    type: 'json',
				    root: 'list',
				    totalProperty: 'totalCount',
				    successProperty: 'success',
				    messageProperty: 'message'
				},
				listeners: {
				    exception: function(proxy, response, operation){

				        Ext.MessageBox.show({
				            title: 'REMOTE EXCEPTION',
				            msg: operation.getError(),
				            icon: Ext.MessageBox.ERROR,
				            buttons: Ext.Msg.OK
				        });
				    }
				}
			}
			,listeners: {
				scope: this,
				// single: true,
				beforeload: function( store, operation, eOpts )
			        {

			        	         
			        	var formId = '#'+store.roleAction+'-'+store.traderType+'-document-formsearch';
			        
			        	var formSearch = Ext.ComponentQuery.query(formId)[0];
			        	var values = formSearch.getValues();
							
			        	 for (field in values) 
					      {
					        if (Ext.isEmpty(values[field])) 
					        {
					          delete values[field];
					        }
					        
					      }
					    store.proxy.extraParams = values;
			        }
			}
		});
		
		Ext.apply(this, {
			store: documentRegistrationStore
			,columns: [
			{
				header: 'regId',
				dataIndex: 'regId',
				hidden: true
			},
			{
				header: 'ชื่อ-นามสกุล',
				dataIndex: 'traderOwnerName',
				flex: 2
			},
			{
				header: 'เลขที่ใบอนุญาต',
				dataIndex: 'licenseNo',
				flex: 2
			},
			{
				header: 'วันที่อนุมัติ',
				dataIndex: 'approveDate',
				flex: 1
			},
			{
				header: 'ประเภทการจดทะเบียน',
				dataIndex: 'registrationTypeName',
				flex: 1
			}
			, {
	        	header: '',
	            xtype: 'actioncolumn',
	           	width:50,
	           	align : 'center',
	            items: [{
	                iconCls: 'icon-edit-xsmall',
	                tooltip: 'รายละเอียด',
		            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {

		            		this.fireEvent('itemclick', this, 'viewdocumentguide', grid, rowIndex, colIndex, record, node);

		            }
	            }]
	        }
			]
		
			,dockedItems:[
			{
				xtype: 'pagingtoolbar',
				store: documentRegistrationStore,
				dock: 'bottom',
				displayInfo: true

			}
			]
			
		});
		
		this.callParent(arguments);
		
	}
	
// 	,plugins: [{
// 	   ptype: 'rowexpander',
// 	   pluginId: 'rowexpanderTourleader',
// 	   rowBodyTpl : new Ext.XTemplate(
// 	   	'<div id="tpl-rowexpander-document-registration-{regId}">',
// //	   	'<tpl switch="traderType">',
// //	           '<tpl case="B">',
// //	            '<p><b>ออกใบอนุญาตให้: {traderOwnerName}</b></p>',
// //	       		'<p><b>ทะเบียนนิติบุคคลเลขที่: {identityNo}</b></p>',
// //	       		'<p><b>ประเภทธุรกิจนำเที่ยว: {traderCategoryName}</b></p>',
// //	           	'<p><b>ชื่อธุรกิจนำเที่ยว (ภาษาไทย): {traderName}</b></p>',
// //	            '<p><b>ชื่อธุรกิจนำเที่ยว (ภาษาต่างประเทศ): {traderNameEn}</b> </p>',
// //	            '<p><font size="2">{traderAddress}</font></p>',
// //	           '<tpl case="G">',
// 	           	'<p><b>ออกใบอนุญาตให้: {traderOwnerName}</b></p>',
// 	       		'<p><b>หมายเลขบัตรประชาชน: {identityNo}</b></p>',
// 	       		'<p><b>ประเภทมัคคุเทศก์: {traderCategoryName}</b></p>',
// 	               '<p><font size="2">{traderAddress}</font></p>',
// //	           '<tpl default>',
// //	           	'<p><b>ออกใบอนุญาตให้: {traderOwnerName}</b></p>',
// //	       		'<p><b>หมายเลขบัตรประชาชน: {identityNo}</b></p>',
// //	       		'<p><b>ประเภทมัคคุเทศก์: {traderCategoryName}</b></p>',
// //	               '<p><font size="2">{traderAddress}</font></p>',
// //	       '</tpl>',
	   		
// 	       '<div>'
// 	   )
// 	}]
	
    ,features: [{
        ftype: 'grouping',
        groupHeaderTpl: '{name}',
        hideGroupedHeader: true,
        startCollapsed: false
    }]
	
});











