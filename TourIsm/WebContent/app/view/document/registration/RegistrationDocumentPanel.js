Ext.define('tourism.view.document.registration.RegistrationDocumentPanel', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
        'tourism.view.document.registration.RegistrationDocumentBusinessFormSearch',
        'tourism.view.document.registration.RegistrationDocumentGuideFormSearch',
        'tourism.view.document.registration.RegistrationDocumentTourleaderFormSearch'
//        'tourism.view.account.AccountGrid',
//        'tourism.view.account.AccountGuideFormSearch'
    ],
    
    xtype: 'regis-document-panel',
    initComponent: function(){

    	var formSearch = this.getBusinessFormSearch(this.roleAction);

    	if(this.traderType == 'guide')
    	{
    		formSearch = this.getGuideFormSearch(this.roleAction);
    	}
    	
    	if(this.traderType == 'tourleader')
    	{
    		formSearch = this.getTourleaderFormSearch(this.roleAction);
    	}
    	Ext.apply(this, {
    		layout: {
		        type: 'border'
		    },
		    items: [{
		        region: 'north',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items:[formSearch]
		    }
		    ,{
		        region: 'center',
		        xtype: 'panel',
		        layout: 'fit',
		        frame: false,
		        border: false,
		        items:[{
		            // title: 'การทำรายการใบอนุญาตธุรกิจนำเที่ยว',
		            xtype: this.roleAction+'-'+this.traderType+'-grid',
		            id: this.roleAction+'-'+this.traderType+'-document-grid',
		            // padding: '5px 5px 5px 5px',
		            autoScroll: true,
		            // frame: false 
		            border: false,
		            storeId: this.roleAction+'-'+this.traderType+'-document-store',
		            storepageSize: 4,
		            storeUrl: 'business/'+this.roleAction+'/registration/read',
		            roleAction: this.roleAction,
		            traderType: this.traderType
		            
		        }]
		    }
		    ]
    	});

    	this.callParent(arguments);
    }
    ,getBusinessFormSearch: function(roleAction)
    {
    	return {
    		
	            title:'ค้นหาการทำรายการใบอนุญาตธุรกิจนำเที่ยว',
	            collapsible: true,   // make collapsible
	            // collapsed : true,
	            xtype: 'document-business-formsearch',
	            id: roleAction+'-business'+'-document-formsearch',
	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false
    	};
    },
    getGuideFormSearch: function(roleAction)
    {
    	return {
    		
	            title:'ค้นหาการทำรายการใบอนุญาตมัคคุเทศก์',
	            collapsible: true,   // make collapsible
	            // collapsed : true,
	            xtype: 'document-guide-formsearch',
	            id: roleAction+'-guide'+'-document-formsearch',
	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false
    	};
    }
    ,getTourleaderFormSearch: function(roleAction){
    	return {
    		
            title:'ค้นหาการทำรายการใบอนุญาตผู้นำเที่ยว',
            collapsible: true,   // make collapsible
            // collapsed : true,
            xtype: 'document-tourleader-formsearch',
            id: roleAction+'-tourleader'+'-document-formsearch',
            // padding: '5px 5px 0px 5px',
            frame: false,
            border: false
    	};
    }
});