Ext.define('tourism.view.document.registration.business.DocumentBusinessGrid',{
	extend: 'Ext.grid.Panel',
	alias: 'widget.document-business-popup-grid',
	stripeRows: true,
	requires: [
      'Ext.grid.Column',   
      'Ext.toolbar.Paging',
	  'Ext.grid.plugin.RowEditing',
	  'Ext.grid.column.Template',
	  'Ext.grid.column.Action',
	  'Ext.grid.plugin.RowExpander'
	  ,'tourism.store.grid.DocumentRegistrationStore',
	  'Ext.grid.feature.Grouping'
	  ,'tourism.view.document.registration.DocumentUploadPanel'
	],
	initComponent: function(){
		
	  var columnUpload = 
	  		{
	        	header: '',
	            xtype: 'actioncolumn',
	           	width:50,
	           	align : 'center',
	            items: [{
	                iconCls: 'icon-upload-xsmall',
	                tooltip: 'แนบเอกสาร',
		            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
		            	this.fireEvent('itemclick', this, 'documentbusinessUpload', grid, rowIndex, colIndex, record, node);
		            }
	            }]
	  			,hidden: this.roleAction == 'document'?false:true
	       };
	  
	  var columnView = 
	  		{
	        	header: '',
	            xtype: 'actioncolumn',
	           	width:50,
	           	align : 'center',
	            items: [{
	            	getClass: function(v, meta, rec) {          
	                   if (rec.get('havefile')) {
	                        this.items[0].tooltip = 'ดูเอกสาร';
	                        return 'icon-checked-xsmall';
	                    }
	                },
		                
		            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
		            	if(record.get("havefile"))
	            		{
		            		this.fireEvent('itemclick', this, 'viewfile', grid, rowIndex, colIndex, record, node);
	            		}        		
		            }
	                
	            }]
	        };
	  
	  
	  var store = Ext.create('tourism.store.grid.DocumentRegistrationStore',{
		  storeId: 'document-business-popup-store'
	  });
	  
	  Ext.apply(this,{
		 store: store,
		 columns:[
		 {
			text: 'masDocId',
			dataIndex: 'masDocId',
			hidden: true
		 }
		 ,{
			 text: 'traderId'
			 ,dataIndex: 'traderId'
		     ,hidden: true
		 }
		 ,{
			 text: 'เอกสาร',
			 dataIndex: 'documentName',
			 flex: 1
		 }
		 
		 ,columnUpload
		 ,columnView
		 
//		 ,{
//	        	header: '',
//	            xtype: 'actioncolumn',
//	           	width:50,
//	           	align : 'center',
//	            items: [{
//	                iconCls: 'icon-upload-xsmall',
//	                tooltip: 'แนบเอกสาร',
//		            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
//		            	this.fireEvent('itemclick', this, 'documentbusinessUpload', grid, rowIndex, colIndex, record, node);
//		            }
//	            }]
//	        }
//		 ,{
//	        	header: '',
//	            xtype: 'actioncolumn',
//	           	width:50,
//	           	align : 'center',
//	            items: [{
////	                iconCls: 'icon-checked-xsmall',
////	                tooltip: 'ดูเอกสาร',
//	            	getClass: function(v, meta, rec) {          
//	                   if (rec.get('havefile')) {
//	                        this.items[0].tooltip = 'ดูเอกสาร';
//	                        return 'icon-checked-xsmall';
//	                    }
////	                    else {
////	                        this.items[0].tooltip = '';
////	                        return '';
////	                    }
//	                },
//		                
//		            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
//		            	if(record.get("havefile"))
//	            		{
//		            		this.fireEvent('itemclick', this, 'viewfile', grid, rowIndex, colIndex, record, node);
//	            		}        		
//		            }
//	                
//	            }]
//	        }
		 ]
	  
	  });
	  
	  this.callParent();
	}

	,features: [{
	    ftype: 'grouping',
	    groupHeaderTpl: '{name} ({rows.length} รายการ)',
	    hideGroupedHeader: true,
	    startCollapsed: false
	}]
	
});