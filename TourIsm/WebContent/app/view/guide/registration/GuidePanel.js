Ext.define('tourism.view.guide.registration.GuidePanel', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
        'tourism.view.guide.registration.GuideRegistrationFormSearch',
        'tourism.view.guide.registration.GuideRegistrationGrid'
    ],
    
    xtype: 'guide-registrationPanel',
    layout: {
        type: 'border'
    },
    items: [{
        region: 'north',
        xtype: 'panel',
        frame: false,
        border: false,
        items:[{
        	title: 'ค้นหาข้อมูลการทำรายการมัคคุเทศก์',
            collapsible: true,   // make collapsible
        	xtype: 'guide_registrationformsearch',
            id: 'guide-registration-formsearch-key',
            // padding: '5px 5px 0px 5px',
            frame: false,
            border: false

        }]
    },{
        region: 'center',
        xtype: 'panel',
        layout: 'fit',
        frame: false,
        border: false,
        items:[{
            title: 'การทำรายการใบอนุญาตมัคคุเทศก์',
            xtype: 'guide_registrationgrid',
            id: 'guide-registrationgrid',
            // padding: '0px 5px 5px 5px',
            autoScroll: true,
            border: false
        }]
    }]
});