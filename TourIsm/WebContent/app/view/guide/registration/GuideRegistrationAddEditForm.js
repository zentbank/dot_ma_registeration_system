Ext.define('tourism.view.guide.registration.GuideRegistrationAddEditForm', {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.data.*',
        'Ext.form.*',
        'Ext.tab.Tab',
        'tourism.view.guide.registration.registrationTab.address.GuideTraderAddressGrid',
        'tourism.view.guide.registration.registrationTab.guideType.GuideTypeAddEditForm',
        'tourism.view.guide.registration.registrationTab.guideTrain.GuideTrainGrid',
        'tourism.view.guide.registration.registrationTab.guideTrain.GuideTrainTypeAddEditForm',
        'tourism.view.window.registration.registrationTab.educationAndLanguage.LanguageGrid',
       'tourism.view.window.registration.registrationTab.educationAndLanguage.EducationGrid',
       'tourism.view.guide.registration.registrationTab.guideTrain.GuideTrainGrid'
    ],
    xtype: 'form-guide-registration-addedit',
    // title: 'มัคคุเทศน์',
    bodyPadding: 5,
    autoScroll: true,
    initComponent: function(){

        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
        
        var provinceStore =  Ext.create('tourism.store.combo.MasProvinceStore',{
            storeId:'guide-registration-masprovince-combo'
        });

        var amphurStore =   Ext.create('tourism.store.combo.MasAmphurStore',{
            storeId:'guide-registration-address-masamphur-store'
        });
        
        Ext.apply(this, {
            width: '100%',
            height: '100%',
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },

            items: [ {
                xtype: 'container',
                // title: 'Payment',
                layout: 'anchor',
                defaults: {
                    anchor: '100%'
                },
                items: [{
                    xtype: 'container',
                    layout: 'hbox',
                    items: [
                        {
                	   xtype: 'hiddenfield',
                	   name: 'regId'
                   },{
                	   xtype: 'hiddenfield',
                	   name: 'registrationType'
                   },
                   {
                       xtype: 'hiddenfield',
                       name: 'traderId'
                   },
                   {
                       xtype: 'hiddenfield',
                       name: 'traderType'
                   },{
                	   xtype: 'hiddenfield',
                	   name: 'personId'
                   },
                   {
                	   xtype: 'hiddenfield',
                	   name: 'graduationCourse'
                   },{
                	   xtype: 'hiddenfield',
                	   name: 'generationGraduate'
                   },
                   {
                	   xtype: 'hiddenfield',
                	   name: 'masUniversityId'
                   },
                   {
                	   xtype: 'hiddenfield',
                	   name: 'studyDate'
                   },
                   {
                	   xtype: 'hiddenfield',
                	   name: 'graduationYear'
                   },
                   {
                	   xtype: 'hiddenfield',
                	   name: 'graduationDate'
                   },
                   {
                	   xtype: 'hiddenfield',
                	   name: 'universityName'
                   },{
                       xtype: 'hiddenfield',
                       name: 'imageFile'
                   },{
                       xtype: 'hiddenfield',
                       name: 'fullAddress'
                   },
                        {
                            xtype: 'textfield',
                            name: 'registrationNo',
                            // labelAlign: 'left',
                            fieldLabel: 'เลขที่รับเรื่อง',
                            flex: 1,
                            allowBlank: false,
                            tabIndex : 1
                        },{
                            xtype: 'datefield',
                            fieldLabel: 'วันที่รับเรื่อง',
                            name: 'registrationDate',
                            flex: 1,
                            format: 'd/m/Y',
                            allowBlank: false,
                            tabIndex : 2
                            // ,
                            // margin: '0 0 0 5'
                            // ,value: Ext.Date.add(new Date(), Ext.Date.YEAR, 543), //2554 ณ ปัจจุบัน
                           
                        },{
                            xtype: 'splitter'
                        },{
                            xtype: 'textfield',
                            name: 'authorityName',
                            fieldLabel: 'ผู้รับเรื่อง',
                            flex: 1,
                            allowBlank: false,
                            tabIndex : 3
                          //  margin: '0 0 0 5'
                        }
                    ]
                }, 
                {
                    xtype: 'splitter'
                },
                {
                	xtype : 'container',
                	layout: 'hbox',
                	items: [
                		{
                			xtype: 'fieldset',
                			flex: 1,
                			border: false,
                			defaults: {
                				anchor: '100%',
                				labelWidth: 170
                			},
                			items:[
                			       {
                			    	   xtype: 'fieldcontainer',
                			    	   labelAlign: 'left',
                			    	   fieldLabel: 'บัตรประจำตัวเลขที่',
                			    	   layout: 'hbox',
                			    	   		items: [{
                			    	   			xtype: 'textfield',
	            			    	   			name: 'identityNo',
	            			    	   			allowBank: false,
	                                            afterLabelTextTpl: required,
	                                            flex:2,
	                                            maxLength: 13,
	                                            minLength: 13,
	                                            enforceMaxLength: true,
	                                            maskRe: /\d/,
	                     			    	   	tabIndex : 4
	                			    	   	},
	                			    	   	{
	                			    	   		xtype: 'splitter'
	                			    	   	},
	                			    	   	{
	                			    	   		xtype: 'button',
                			    	   			iconCls: 'icon-search-user-xsmall margin-left-15',
                			                    flex: 1,
                			                    action: 'searchUserFromIDCardOnline'
	                			    	   	}
                			    	   	]
                			    	 
                			       },
//                			       {
//                			    	   xtype: 'textfield',
//                			    	   fieldLabel: 'บัตรประจำตัวเลขที่',
//                			    	   name: 'identityNo',
//                			    	   allowBank: false,
//                                       afterLabelTextTpl: required,
//                                       maxLength: 13,
//                                       minLength: 13,
//                                        enforceMaxLength: true,
//                                        maskRe: /\d/,
//                			    	   tabIndex : 4
//                			       },
                			       {
                			    	   xtype: 'fieldcontainer',
                			    	   labelAlign: 'left',
                			    	   fieldLabel: 'ชื่อ(ภาษาไทย)',
                			    	   layout: 'hbox',
                			    	   		items: [{
                			    	   			xtype: 'masprefixcombo',
                			    	   			store: 'tourism.store.combo.MasPrefixStore',
                			    	   			queryMode: 'remote',
                			    	   		    displayField: 'prefixName',
                			    	   		    valueField: 'prefixId',
                			    	   		    hiddenName: 'prefixId',
                			    	   		    name: 'prefixId',
                			    	   		    triggerAction: 'all',
                			    	   			flex:1,
                			    	   			allowBlank: false,
                                                afterLabelTextTpl: required,
                			    	   			tabIndex : 6,
                                                prefixType: 'I'
                			    	   	},
                			    	   	{
                			    	   		xtype: 'splitter'
                			    	   	},
                			    	   	{
                			    	   		xtype: 'textfield',
                			    	   		name: 'firstName',
    	                                    flex: 2,
    	                                    allowBlank: false,
                                            afterLabelTextTpl: required,
    	                                    tabIndex : 7
                			    	   	}
                			    	   	]
                			       },
                			       {
                			    	   xtype: 'fieldcontainer',
                			    	   labelAlign: 'left',
                			    	   fieldLabel: 'ชื่อ(ภาษาอังกฤษ)',
                			    	   layout: 'hbox',
                			    	   		items: [{
                			    	   			xtype: 'masprefixcombo',
                			    	   			store: 'tourism.store.combo.MasPrefixStore',
                			    	   		    queryMode: 'remote',
                			    	   		    displayField: 'prefixNameEn',
                			    	   		    valueField: 'prefixId',
                			    	   		    hiddenName: 'prefixIdEn',
                			    	   		    triggerAction: 'all',
                			    	   		    name: 'prefixIdEn',
                			    	   			flex:1,
                			    	   			allowBlank: false,
                                                afterLabelTextTpl: required,
                			    	   			tabIndex : 9,
                                                prefixType: 'I'
                			    	   	},
                			    	   	{
                			    	   		xtype: 'splitter'
                			    	   	},
                			    	   	{
                			    	   		xtype: 'textfield',
                			    	   		name: 'firstNameEn',
    	                                    flex: 2,
    	                                    allowBlank: false,
                                            afterLabelTextTpl: required,
    	                                    tabIndex : 10
                			    	   	}
                			    	   
                			    	   	]
                			       
                			       },{
       	                            xtype: 'radiogroup',
    	                            fieldLabel: 'เพศ',
    	                            // Arrange checkboxes into two columns, distributed vertically
    	                            columns: 2,
    	                            tabIndex : 12,
    	                            vertical: true,
    	                            items: [
    	                                { boxLabel: 'ชาย',  name: 'gender', inputValue: 'M', checked: true },
    	                                { boxLabel: 'หญิง',  name: 'gender', inputValue: 'F' } 
    	                            ]
    	                        },{
    	                        	 xtype: 'datefield',
    	                             fieldLabel: 'เกิดวันที่',
    	                             name: 'birthDate',
    	                             format: 'd/m/B',
    				                 allowBlank: false,
                                     afterLabelTextTpl: required,
    				                 tabIndex : 14
    	                        },{
    	                        	  xtype: 'masprovincecombo',
    	                              id : 'guide-registration-masprovince-combo',
    	                              fieldLabel: 'ออกให้ ณ จังหวัด',
    	                              store: provinceStore,
    	                              displayField: 'provinceName',
    	                              valueField: 'provinceId',
    	                              hiddenName: 'provinceId',
    	                              name: 'provinceId',
    	                           //   allowBlank: false,
    	                              afterLabelTextTpl: required,
    	                              masamphurComboId:'guide-registration-masamphur-combo',
    	                              queryParam: 'provinceName',
    	                              tabIndex : 16
                                },{
    	                        	 xtype: 'datefield',
    	                             fieldLabel: 'หมดอายุวันที่',
    	                             name: 'identityNoExpire',
    	                             format: 'd/m/B',
    				            //     allowBlank: false,
    				                 tabIndex : 18
    	                        }
                			]
                		},
                		{
    						xtype: 'fieldset',
    					    flex:1,
    					    border: false,
    					    defaults: {
    					     anchor: '100%',
    					     labelWidth: 170
    					    },
    					    items: [
    							{
    							    xtype: 'textfield',
    							    fieldLabel: 'หมายเลขหนังสือเดินทางเลขที่',
    							    name: 'passportNo',
                                    maxLength: 10,
                                    minLength: 7,
                                    // enforceMaxLength: true,
                                        // maskRe: /\d/,
    			                    // allowBlank: false,
    			                    tabIndex : 5
    							},
    	                        {
    	                            // นามสกุล(ภาษาไทย)
    	                            xtype: 'textfield',
    	                            fieldLabel: 'นามสกุล(ภาษาไทย)',
    	                            name: 'lastName',
    	                            labelAlign: 'left',
                                    afterLabelTextTpl: required,
    			                    allowBlank: false,
    			                    tabIndex : 8
    	                        },
    	                        {
    	                            // นามสกุล(ภาษาอังกฤษ)
    	                            xtype: 'textfield',
    	                            fieldLabel: 'นามสกุล(ภาษาอังกฤษ)',
    	                            name: 'lastNameEn',
    	                            labelAlign: 'left',
    			                    allowBlank: false,
                                    afterLabelTextTpl: required,
    			                    tabIndex : 11
    	                        },
    	                        {
                                // นามสกุล(ภาษาอังกฤษ)
                                    xtype: 'textfield',
                                    fieldLabel: 'สัญชาติ',
                                    name: 'personNationality',
                                    labelAlign: 'left',
                                    value: 'ไทย',
                                    afterLabelTextTpl: required,
                                    allowBlank: false
                                    // ,tabIndex : 9
                                },{xtype: 'splitter'},
    	                        {
    	                            // อายุ
    	                        	xtype: 'fieldcontainer',
    	                            layout: 'hbox',
    	                            items:[
    	                            {
    		                            xtype: 'textfield',
    		                            labelWidth: 170,
    		                            width: 350,
    		                            fieldLabel: 'อายุ',
    		                            name: 'ageYear',
    		                            labelAlign: 'left',
    				                    allowBlank: false,
                                        afterLabelTextTpl: required,
    				                    tabIndex : 15
    	                            },{xtype: 'splitter'},{xtype: 'splitter'},
                                    {
    	                            	 xtype: 'label',
    			                         text: 'ปี',
    			                         labelAlign: 'left'
    	                            }
    	                     
    	                            ]
    	                        },{
    	                        	  xtype: 'masamphurcombo',
    	                        	  fieldLabel: 'อำเภอ/เขต/สังกัด',
    	                              id : 'guide-registration-masamphur-combo',
    	                              store: amphurStore,
    	                              displayField: 'amphurName',
    	                              valueField: 'amphurId',
    	                              hiddenName: 'amphurId',
    	                              name: 'amphurId',
    	                          //    allowBlank: false,
    	                              afterLabelTextTpl: required,
    	                              queryParam: 'amphurName',
    	                              masprovinceComboId: 'guide-registration-masprovince-combo',
    	                              tabIndex : 17	  
                                },{
                                    
                                     xtype: 'fieldset',
                                     itemId: 'imageWrapper',
                                     margin: '0 0 0',
                                     layout: 'anchor',
                                     items: [{
                                        xtype: 'image',
                                        itemId: 'imageFile',
                                        margin: '0 90px 0',
                                        listeners:{
                                            afterrender:function(img, a, obj){
                                                img.getEl().dom.style.width = '130px'; 
                                                img.getEl().dom.style.height = '150px'; 
                                            }
                                        }
                                     }]
                                }
    	                        ]
    					}
                		]
                }	
                ]
            }
            ,{
                    xtype: 'tabpanel',
                    plain: true,
                    frame: false,
                    border: false,
                    defaults: {
                        bodyPadding: 5
                    },
                   items: [
                        {
                            title: 'การอบรม',
                            layout: 'fit',
                        	frame: false,
                        	border: false,
                            items:[
                                {
                                    xtype:'guide_registration_registrationTab_guidetraingrid',
                                    border: false
                                 
                                }
                            ]
                            
                        }
                        ,
                        {
                            title: 'ที่อยู่',
                            layout: 'anchor',
                        	frame: false,
                        	border: false,
                            items:[
                                {
                                    xtype: 'guide_registration_registrationTab_guideaddressgrid',
                                    border: false
                                 
                                }
                            ]
                        }
                        ,{
                            title: 'ประเภทมัคคุเทศก์',
                            layout: 'anchor',
                        	frame: false,
                        	border: false,
                        	
                            items:[
                                {
                                    xtype: 'guide_registration_guidetype_guidetypeaddedit_form',
                                    frame: false,
                                    border: false       
                                }
                            ]
                        }
                        ,{
                            title: 'การศึกษาและภาษา', 
                            layout: 'anchor',
                        	frame: false,
                        	border: false,
                             items:[
                                 {
                                	 xtype: 'window_registration_registrationTab_education_grid'
                                 }  ,
                                 {xtype: 'splitter'},{xtype: 'splitter'}
                                 ,
                                 {
                                	 xtype: 'window_registration_registrationTab_language_grid'
                                 }
                                 ]
                        }
                    ]
                }
        ],

            buttons: [{
                text: 'บันทึก',
                scope: this,
                action: 'saveGuideRegistration',
                tabIndex : 24
            },
            {
                text: 'ค้นหาข้อมูลจากบัตรประชาชน',
                scope: this,
                action: 'loadDataFromIdCard',
                tabIndex : 24
            }]  
        });
        this.callParent();
    }
    ,onResetClick: function(){
        this.getForm().reset();
    }
    ,onCompleteClick: function(){
        var form = this.getForm();
        if (form.isValid()) {
            Ext.MessageBox.alert('Submitted Values', form.getValues(true));
        }
    },
    afterRender: function()
    {
	    this.callParent(arguments);
	    
	    var form = this;
		
		 var preFixId = form.getForm().findField('prefixId');
		 preFixId.addListener("blur" ,function(textf, eOpts){
		 	var values  =  textf.up('form').getValues();
		 	var preFixIdEn = textf.up('form').getForm().findField('prefixIdEn');
			
			if((!Ext.isEmpty(preFixId)))
		 	{
		 		preFixIdEn.setValue(values.prefixId);
		 	}
		 },this);
	    //add registration
        if(!Ext.isEmpty(this.registrationModel) )
        {
        	console.log(this.registrationModel);
            this.loadRecord(this.registrationModel);
    	    
        } 
    }
    
});