Ext.define('tourism.view.guide.registration.GuideRegistrationAddEditWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.guide_registrationaddedit_window',

    requires: [
        'tourism.view.guide.registration.GuideRegistrationAddEditForm'
    ],

    title : 'จดทะเบียนขอใบอนุญาตใหม่ของมัคคุเทศก์',
    layout: 'fit',
    // autoShow: true,
    width: 750,
    height: 750,
    //True to make the window modal and mask everything behind it when displayed
    modal: true,
    // iconCls: 'icon-form',

    initComponent: function() {


        var formPanel = {
                    xtype: 'form-guide-registration-addedit',
                    id: 'key-form-guide-registration-addedit',
                    frame: false,
                    border: false,
                    registrationModel: this.registrationModel
        };

        if(this.roleAction != 'key')
        {
          formPanel = {
                    xtype: 'form-guide-registration-addedit',
                    frame: false,
                    id: this.roleAction+'-'+this.traderType+'-registration-addedit',
                    border: false
                };
        }
        
        this.items = [
                formPanel 
        ];



        this.callParent(arguments);
    },
    loadDataModel: function(model)
    {

        var freg = this.down('form');
        freg.loadRecord(model);

        //Image
        var imageFieldSet = this.down('image');
        // console.log(model);
        imageFieldSet.setSrc("data:image/png;base64,"+model.get("imageFile"));
        
        
        // //console.log(freg.loadRecord(model));
      //Province
        if(!Ext.isEmpty(model.get('provinceId')))
        {
          var provinceCombo = freg.getForm().findField('provinceId');
          // //console.log(provinceCombo);
          provinceCombo.getStore().on('load',function(store, records, successful, eOpts){
            provinceCombo.setValue(model.get('provinceId'));
          },this,{single:true});
          provinceCombo.getStore().load();
        }
        else
        {
          var provinceCombo = freg.getForm().findField('provinceId');
          provinceCombo.clearValue();
        }
        
      //Amphur
        if(!Ext.isEmpty(model.get('amphurId')))
        {
          var amphurCombo = freg.getForm().findField('amphurId');
          amphurCombo.getStore().on('load',function(store, records, successful, eOpts){
            amphurCombo.setValue(model.get('amphurId'));
          },this,{single:true});
          amphurCombo.getStore().load();
        }
        else
        {
           var amphurCombo = freg.getForm().findField('amphurId');
           amphurCombo.clearValue();
        }
        
        
        //Prefix
        if(!Ext.isEmpty(model.get('prefixId')))
        {
          var prefixIdCombo = freg.getForm().findField('prefixId');
          var prefixIdEnCombo = freg.getForm().findField('prefixIdEn');
          prefixIdCombo.getStore().on('load',function(store, records, successful, eOpts){
            prefixIdCombo.setValue(model.get('prefixId'));
            prefixIdEnCombo.setValue(model.get('prefixId'));
          },this,{single:true});
          prefixIdCombo.getStore().load();
        }
        else
        {
          var prefixIdCombo = freg.getForm().findField('prefixId');
          var prefixIdEnCombo = freg.getForm().findField('prefixIdEn');
          prefixIdCombo.clearValue();
          prefixIdEnCombo.clearValue();
        }
        //การอบรม
        var tradertrain = Ext.ComponentQuery.query('guide_registration_registrationTab_guidetraingrid')[0];
        tradertrain.getStore().load({
            params: {personId: model.get('personId'), educationType:'T', recordStatus: model.get('traderRecordStatus')},
            callback: function(records, operation, success) {
                // //console.log(records);
            },
            scope: this
        });

        
        //ที่อยู่
        var traderaddressgrid = Ext.ComponentQuery.query('guide_registration_registrationTab_guideaddressgrid')[0];
        
        traderaddressgrid.getStore().load({
            params: {traderId: model.get('traderId'), recordStatus: model.get('traderRecordStatus')},
            callback: function(records, operation, success) {
                // //console.log(records);
            
            },
            scope: this
        }); 
       
        var traderCategoryName = Ext.getCmp('guide-type-registration-categorystore-combo');
        
        traderCategoryName.on('afterrender',function(cmp){
        	cmp.setValue(model.get('traderCategory'));
        });
        
        

      //ประเภทการจดทะเบียน
        var tradercategory = Ext.ComponentQuery.query('guide_registration_guidetype_guidetypeaddedit_form')[0];
        var traderAreaName = tradercategory.getForm().findField('traderArea');
        if(model.get('traderArea') != '')
        {
             traderAreaName.setValue(model.get('traderArea'));
        }
        
        traderAreaName.setValue(model.get('traderArea'));
        
        if(model.get('traderCategory') == '100' || model.get('traderCategory') == '101')
        {
            freg.getForm().findField('traderCategoryType').setValue(1);
        }else
        {
            freg.getForm().findField('traderCategoryType').setValue(2);
        }


        


        
      //provinceGuideServiceId
        if(!Ext.isEmpty(model.get('provinceGuideServiceId')) )
        {
          var provinceGuideCombo = freg.getForm().findField('provinceGuideServiceId');
          provinceGuideCombo.getStore().on('load',function(store, records, successful, eOpts){
              if(model.get('provinceGuideServiceId') == '0')
                  {
                  provinceGuideCombo.clearValue();
                  }else
                  {
            	  	provinceGuideCombo.setValue(model.get('provinceGuideServiceId'));
            	  	provinceGuideCombo.on('afterrender',function(cmp){
                    	cmp.setValue(model.get('provinceGuideServiceId'));
                    });
                  }
          },this,{single:true});
          provinceGuideCombo.getStore().load();
        }
        else
        {
          var provinceCombo = freg.getForm().findField('provinceGuideServiceId');
          provinceCombo.clearValue();
        }
        
        
        
        
        //การศึกษา
        var educationgrid = Ext.ComponentQuery.query('window_registration_registrationTab_education_grid')[0];
        educationgrid.getStore().load({
            params: {personId: model.get('personId'), educationType: 'E', recordStatus: model.get('traderRecordStatus')},
            callback: function(records, operation, success) 
            {
                // //console.log(records);                            
            },
            scope: this
        });
        
        //ภาษา
        var languagegrid = Ext.ComponentQuery.query('window_registration_registrationTab_language_grid')[0];
        languagegrid.getStore().load({
            params: {personId: model.get('personId'), recordStatus: model.get('traderRecordStatus')},
            callback: function(records, operation, success) {
                
                // //console.log(records);                            
            },
            scope: this
        });

        var personNationalityField =  freg.getForm().findField('personNationality');
        personNationalityField.setValue("ไทย");

    }

});
