Ext.define('tourism.view.guide.registration.GuideRegistrationGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.guide_registrationgrid',
		//static store in Ext.data.StoreManager
		store: 'tourism.store.guide.registration.GuideRegistrationStore',
		model: 'tourism.model.guide.registration.GuideRegistrationModel',
		stripeRows: true,

		requires: [
			'Ext.grid.Column',
			'Ext.toolbar.Paging',
			'Ext.grid.plugin.RowEditing',
			'Ext.grid.column.Template',
			'Ext.grid.column.Action',
			'Ext.grid.plugin.RowExpander'
			// ,'tourism.view.window.verification.guide.GuideVerificationWindow'
		]		
		,initComponent: function(){
			var registrationstore = Ext.create('tourism.store.business.registration.BusinessRegistrationStore',{
				id: 'guide-registration-main-grid-store',
				listeners: {
					scope: this,
					// single: true,
					beforeload: function( store, operation, eOpts )
				        {
				        	var formSearch = Ext.ComponentQuery.query('#guide-registration-formsearch-key')[0];
				        	var values = formSearch.getValues();
								
				        	 for (field in values) 
						      {
						        if (Ext.isEmpty(values[field])) 
						        {
						          delete values[field];
						        }
						        
						      }
						    store.proxy.extraParams = values;
				        }
				}
			});

			Ext.apply(this, {
				// store: 'tourism.store.business.registration.BusinessRegistrationStore',
				store: registrationstore,
				dockedItems:[
					{
						xtype: 'pagingtoolbar',
						// store: 'tourism.store.business.registration.BusinessRegistrationStore',
						store: registrationstore,
						dock: 'bottom',
						displayInfo: true,
						items:[
							{

									text: 'เพิ่มรายการมัคคุเทศก์',
									action: 'add'
								
			        		}
		        		]
					}
					
				],
				viewConfig: {
					stripeRows:true ,
					getRowClass: function(record, index) {                            
						if (record.get('roleColor') == 'true') {
						    return "online-block";
						}
					}
				},
				columns: [
				{
					header: 'regId',
					dataIndex: 'regId',
					hidden: true
				},
				{
					header: 'เลขที่ใบอนุญาต',
					dataIndex: 'licenseNo',
					flex: 1
				},
				{
					header: 'ชื่อผู้ขอใบอนุญาต',
					dataIndex: 'traderOwnerName',
					flex: 2
				},
				{
					header: 'ประเภทการจดทะเบียน',
					dataIndex: 'registrationTypeName',
					flex: 2
				},

				{
					header: 'เลขที่รับเรื่อง',
					dataIndex: 'registrationNo',
					flex: 1
				},
				{
					header: 'วันที่รับเรื่อง',
					dataIndex: 'registrationDate',
					flex: 1
				},
		        {
					header: '',
		            xtype: 'actioncolumn',
		           	width:50,
		           	align : 'center',
		        	//header: 'สถานะเรื่อง',
		            items: [{
		                getClass: function(v, meta, rec) {          
		                    if (rec.get('progressBackStatus') != 'B') {
		                        this.items[0].tooltip = 'รับเรื่อง';
		                        return 'icon-comment-xsmall';
		                    } else {
		                        this.items[0].tooltip = 'เรื่องตีกลับ';
		                        return 'icon-comment-xsmall-red';
		                    }
		                },
			            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
			                this.fireEvent('itemclick', this, 'progress', grid, rowIndex, colIndex, record, node);
			            }
		            }]
		        },
		        {
		        	header: '',
		            xtype: 'actioncolumn',
		           	width:50,
		           	align : 'center',
		            items: [{
		                // iconCls: 'icon-send-xsmall',
		                // tooltip: 'การผ่านเรื่อง',
		                getClass: function(v, meta, rec) {          
		                    if (rec.get('registrationProgress') === 'KEY') {
		                        this.items[0].tooltip = 'การผ่านเรื่อง';
		                        return 'icon-send-xsmall';
		                    } else {
		                        this.items[0].tooltip = 'ผ่านเรื่องแล้ว';
		                        return '';
		                    }
		                },
			            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
			            	if (record.get('registrationProgress') === 'KEY') {
			            		this.fireEvent('itemclick', this, 'verification', grid, rowIndex, colIndex, record, node);
			            	}
			                
			            }
		            }]
		        },
		        {
		        	header: '',
		            xtype: 'actioncolumn',
		           	width:50,
		           	align : 'center',
		            items: [{
		                iconCls: 'icon-edit-xsmall',
		                tooltip: 'รายละเอียด',
			            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
			            	if (record.get('registrationProgress') == 'KEY') {
			            		this.fireEvent('itemclick', this, 'editregistration', grid, rowIndex, colIndex, record, node);
			            	}
			            	else
			            	{
			            		this.fireEvent('itemclick', this, 'viewregistration', grid, rowIndex, colIndex, record, node);
			            	}		            	
			            	//this.fireEvent('itemclick', this, 'informationregistration', grid, rowIndex, colIndex, record, node);
			         
			            }
		            }]
		        }
			],
			});

			this.callParent(arguments);


		}	
       ,features: [{
	        ftype: 'grouping',
	        groupHeaderTpl: '{name} ({rows.length} รายการ)',
	        hideGroupedHeader: true,
	        startCollapsed: false
	        // ,
	        // id: 'restaurantGrouping'
	    }]
	});