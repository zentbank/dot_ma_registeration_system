Ext.define('tourism.view.guide.registration.registrationTab.address.GuideTraderAddressAddEditForm',{
	extend: 'Ext.form.Panel',
	requires: [
       'Ext.data.*',
       'Ext.form.*',
       'Ext.tab.Tab'
	],
	xtype: 'guide_registration_address_guideaddressaddedit_form',
	bodyPadding: 5,
    autoScroll: true,
    
    initComponent: function(){
    	var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
    	
    	var provinceStore =  Ext.create('tourism.store.combo.MasProvinceStore',{
             storeId:'guide-registration-address-masprovince-store'
        });

		var tambolStore  = Ext.create('tourism.store.combo.MasTambolStore',{
		     storeId:'guide-registration-address-tambol-store'
		});
		var amphurStore =   Ext.create('tourism.store.combo.MasAmphurStore',{
		     storeId:'guide-registration-address-masamphur-store'
		});
    	
    	Ext.apply(this,{
			width: '100%',
	        height: '100%',
	        fieldDefaults: {
	            labelAlign: 'left',
	            // labelWidth: 50,
	            msgTarget: 'qtip'
	        },
	
	        items:[
	        {
	        	xtype: 'container',
	        	layout: 'anchor',
	        	defaults: {
	        		anchor: '100%'
	        	},
	        	items: [
				{
					xtype: 'fieldset',
	        		border: false,
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%'
                    },
                    items:[{
                    	xtype: 'textfield',
                    	name: 'fullAddress',
                        fieldLabel: 'ที่อยู่จากทะเบียนราษฎร์',
                        readOnly: true,
                        skipValue:true
                    }]
				},
	        	{
	        		xtype: 'radiogroup',
	        		fieldLabel: 'ประเภทที่ตั้ง',
	        		labelWidth: 150,
	        		allowBank: false,
	        		afterLabelTextTpl: required,
	        		columns: 2,
	        		vertical: true,
	        		items: [
	        		{
	        			boxLabel: 'ที่อยู่ตามทะเบียนบ้าน', name: 'addressType', inputValue: 'O', checked: true
	        		},{
	        			boxLabel: 'ภูมิลำเนาเฉพาะการ', name: 'addressType', inputValue: 'S'
	        		}
	        		],
	        		listeners: {
	        			change: this.onSelectAddressType
	        		}
	        	},
	        	{
	        		xtype: 'fieldset',
	        		border: false,
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%'
                    },
                    items:[
                    {
                      xtype: 'container',
                      layout: 'hbox',
                      layoutCongig: {
                          pack:'center',
                          align:'middle'
                     },
                      items: [
                      {
                    	  xtype: 'container',
                          layout: 'anchor',
                          flex: 1,
                          defaults: {
                              anchor: '100%'
                          },
                          items:[
                          {
                            xtype: 'textfield',
                            name: 'addressNo',
                            fieldLabel: 'ตั้งอยู่เลขที่',
                            allowBlank: false,
                            afterLabelTextTpl: required,
                            tabIndex : 100
//                            ,value: '55/199'
                          },{
                              // ชั้น
                              xtype: 'textfield',
                              name: 'floor',
                              fieldLabel: 'ชั้น',
                              tabIndex : 102
                          },{
                              // หมู่ที่
                              xtype: 'textfield',
                              name: 'moo',
                              fieldLabel: 'หมู่ที่',
                              tabIndex : 104
                          },{
                              // ตรอก/ซอย
                              xtype: 'textfield',
                              name: 'soi',
                              fieldLabel: 'ตรอก/ซอย',
                              tabIndex : 106
                          },{
                              // จังหวัด (TH)
                              xtype: 'masprovincecombo',
                              fieldLabel: 'จังหวัด',
                              id : 'guide-registration-address-masprovince-combo',
                              store: provinceStore,
                              displayField: 'provinceName',
                              valueField: 'provinceId',
                              hiddenName: 'provinceId',
                              name: 'provinceId',
                              allowBlank: false,
                              afterLabelTextTpl: required,
                              masamphurComboId:'guide-registration-address-masamphur-combo',
                              queryParam: 'provinceName',
                              tabIndex : 108        
                          },{
                               // ตำบล
                              xtype: 'mastambolcombo',
                              fieldLabel: 'ตำบล/แขวง',
                              id : 'guide-registration-address-tambol-combo',
                              store: tambolStore,
                              displayField: 'tambolName',
                              valueField: 'tambolId',
                              hiddenName: 'tambolId',
                              name: 'tambolId',
                              allowBlank: false,
                              afterLabelTextTpl: required,
                              masamphurComboId: 'guide-registration-address-masamphur-combo',
                              queryParam: 'tambolName',
                              tabIndex : 110
                             
                          },{
                              // โทรศัพท์
                              xtype: 'textfield',
                              name: 'telephone',
                              fieldLabel: 'โทรศัพท์',
                              // maxLength: 15,
                              // minLength: 9,
                              // enforceMaxLength: true,
                              // maskRe: /[\d\-]/,
                              // regex: /^\d{9}(\-\d{2,3}|\-\d{4})?$/,
                              tabIndex : 112
                          },{
                              // โทรสาร
                              xtype: 'textfield',
                              name: 'fax',
                              fieldLabel: 'โทรสาร',
                              // maxLength: 15,
                              // minLength: 9,
                              // enforceMaxLength: true,
                              // maskRe: /[\d\-]/,
                              // regex: /^\d{9}(\-\d{2,3}|\-\d{4})?$/,
                              tabIndex : 114
                          },{
                              // เวปไซด์
                              xtype: 'textfield',
                              name: 'website',
                              fieldLabel: 'เวปไซต์',
                              tabIndex : 116
                          }
                          ]
              
                      },{
                          xtype: 'splitter'
                      },{
                    	  xtype: 'container',
                          layout: 'anchor',
                          flex: 1,
                          defaults: {
                              anchor: '100%'
                          },
                          items:[
                          {
                            xtype: 'textfield',
                            name: 'buildingName',
                            fieldLabel: 'อาคาร',
                            tabIndex : 101
                          },{
                              // ห้องเลขที่
                              xtype: 'textfield',
                              name: 'roomNo',
                              fieldLabel: 'ห้องเลขที่',
                              tabIndex : 103
                          },{
                              // หมู่บ้าน
                              xtype: 'textfield',
                              name: 'villageName',
                              fieldLabel: 'หมู่บ้าน',
                              tabIndex : 105
                          },{
                              // ถนน
                              xtype: 'textfield',
                              name: 'roadName',
                              fieldLabel: 'ถนน',
                              tabIndex : 107
                          },{
                               // อำเภอ/เขต
                              xtype: 'masamphurcombo',
                              fieldLabel: 'อำเภอ/เขต',
                              id : 'guide-registration-address-masamphur-combo',
                              store: amphurStore,
                              displayField: 'amphurName',
                              valueField: 'amphurId',
                              hiddenName: 'amphurId',
                              name: 'amphurId',
                              allowBlank: false,
                              afterLabelTextTpl: required,
                              queryParam: 'amphurName',
                              masprovinceComboId: 'guide-registration-address-masprovince-combo',
                              mastambolComboId: 'guide-registration-address-tambol-combo',
                              tabIndex : 109
                             
                          },{
                              // รหัสไปรษณีย์
                              xtype: 'textfield',
                              name: 'postCode',
                              fieldLabel: 'รหัสไปรษณีย์',
                              allowBlank: false,
                              afterLabelTextTpl: required,
                              maxLength: 5,
                              minLength: 5,
                              enforceMaxLength: true,
                              maskRe: /\d/,
                              tabIndex : 111
//                              ,value: '50000'
                          },{
                              // โทรศัพท์มือถือ
                              xtype: 'textfield',
                              name: 'mobileNo',
                              fieldLabel: 'โทรศัพท์มือถือ',
                              // maxLength: 10,
                              // minLength: 10,
                              // enforceMaxLength: true,
                              // maskRe: /\d/,
                              tabIndex : 113
                          },{
                              // อีเมล์
                              xtype: 'textfield',
                              name: 'email',
                              fieldLabel: 'อีเมล์',
                              // allowBlank: false,
                              // afterLabelTextTpl: required,
                              tabIndex : 115,
                              vtype: 'email'
//                              ,value: 'sek.beck@gmail.com'
                          }
                          ]
              
                      }												
                      ]
                    
                    }
                    ]
	        	},{
                    xtype: 'checkbox',
                    boxLabel  : 'คัดลอกไปยัง ภูมิลำเนาเฉพาะการ',
                    name      : 'copyAddress',
                    inputValue: '1',
                    id        : 'guide-registration-address-copyAddress-checkbox',
                    tabIndex : 117
                }
	        	]
	        }
	        ],

            buttons: [{
                text: 'บันทึก',
                scope: this,
                action: 'saveTraderAddress'
            }, {
                text: 'ยกเลิก',
                scope: this,
                handler: this.onResetClick
            }] 
    	});
    	this.callParent(arguments);
    },
    onSelectAddressType: function(radio, newValue, oldValue, eOpts)
    {
        var copyAddress = Ext.getCmp('guide-registration-address-copyAddress-checkbox');
        
        if(newValue.addressType == "S")
        {
            copyAddress.setBoxLabel('คัดลอกไปยัง ที่อยู่ตามทะเบียนบ้าน');
            
        }
        else
        {
            copyAddress.setBoxLabel('คัดลอกไปยัง ภูมิลำเนาเฉพาะการ');
        }

    },
    onResetClick: function(){
        this.getForm().reset();
    }
	
});









