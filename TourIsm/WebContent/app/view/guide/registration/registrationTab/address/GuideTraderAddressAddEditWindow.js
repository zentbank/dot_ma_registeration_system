Ext.define('tourism.view.guide.registration.registrationTab.address.GuideTraderAddressAddEditWindow',{
	extend: 'Ext.window.Window',
	alias: 'widget.guide_registration_address_addedit_window',
	requires: [
	  'tourism.view.guide.registration.registrationTab.address.GuideTraderAddressAddEditForm'
	],
	title: 'เพิ่มแก้ไขข้อมุลที่อยู่',
	layout: 'fit',
    autoShow: true,
    width: 600,
    height: 500,
    //True to make the window modal and mask everything behind it when displayed
    modal: true,
    
    initComponent: function(){
    	this.items = [
    	{
    		xtype: 'guide_registration_address_guideaddressaddedit_form',
    		frame: false,
            border: false
    	}
    	];
    	
    	this.callParent(arguments);
    }

	
});









