Ext.define('tourism.view.guide.registration.registrationTab.address.GuideTraderAddressGrid',{
	extend: 'Ext.grid.Panel',
	alias: 'widget.guide_registration_registrationTab_guideaddressgrid',
	stripeRows: true,
	requires: [
      'Ext.grid.Column',   
      'Ext.toolbar.Paging',
	  'Ext.grid.plugin.RowEditing',
	  'Ext.grid.column.Template',
	  'Ext.grid.column.Action',
	  'Ext.grid.plugin.RowExpander',
	  'tourism.store.business.registration.address.TraderAddressStore',
	],
	initComponent: function(){
	  var store = Ext.create('tourism.store.business.registration.address.TraderAddressStore',{
		  storeId: 'tourleader-registration-traderaddressstore'
	  });
	  
	  Ext.apply(this,{
		 store: store,
		 columns:[
		 {
			text: 'addressId',
			dataIndex: 'addressId',
			hidden: true
		 },{
			 text: 'ประเภทที่ตั้ง',
			 dataIndex: 'addressType',
			 flex: 1,
			 renderer: function(value, metaData, model){
				if (!value) {
				    return "";
				}
			 /*
                 O ที่อยู่ตามทะเบียนบ้าน
                 S ภูมิลำเนาเฉพาะการ
             */

             //console.log(value);
				if(value == "O")
				{
					return 'ที่อยู่ตามทะเบียนบ้าน';
				}
				if(value == "S")
				{
					return 'ภูมิลำเนาเฉพาะการ';
				}
				if(value == "C")
				{
					return 'ที่อยู่ที่ติดต่อได้';
				}
				
			}
		 },{
			text: 'โทรศัพท์',
			dataIndex: 'telephone',
			flex: 1
		 },{
			text: 'ตำบล/แขวง',
			dataIndex: 'tambolName',
			flex: 1
		 },{ 
			text: 'อำเภอ/เขต',
			dataIndex: 'amphurName',
			flex: 1
		 },{
			text: 'จังหวัด',
			dataIndex: 'provinceName',
			flex: 1
		 },{
        	text: 'แก้ไข',
            xtype: 'actioncolumn',
            width: 50,
            items: [{
            	iconCls: 'icon-edit-xsmall',
                tooltip: 'แก้ไข',
	            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
	                this.fireEvent('itemclick', this, 'edittraderaddress', grid, rowIndex, colIndex, record, node);
	            }
            }]
         },{
        	 text: 'ลบ',
        	 xtype: 'actioncolumn',
        	 width: 50,
        	 items: [
        	 {
        		iconCls: 'icon-delete-xsmall',
                tooltip: 'ลบ',
	            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
	                this.fireEvent('itemclick', this, 'deletetraderaddress', grid, rowIndex, colIndex, record, node);
	            }
        	 }
        	 ]
         }
		 ],
		 dockedItems: [
		 {
			 xtype: 'toolbar',
			 dock: 'top',
			 items: [
			 {
				 xtype: 'component', flex: 1 
		     },{
				 xtype: 'button',
				 text: 'เพิ่มข้อมูลที่อยู่',
				 action: 'addTraderAddress'
			 }
			 ]
		 }
		 ]
	  
	  });
	  
	  this.callParent();
	}
	
});