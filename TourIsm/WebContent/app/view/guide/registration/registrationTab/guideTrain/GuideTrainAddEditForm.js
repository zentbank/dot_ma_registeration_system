Ext.define('tourism.view.guide.registration.registrationTab.guideTrain.GuideTrainAddEditForm',{
	extend: 'Ext.form.Panel',
	requires: [
	           	'Ext.data.*',
	           	'Ext.form.*',
	      		'Ext.tab.Tab',
	      		'tourism.view.guide.registration.registrationTab.guideTrain.GuideSearchTrainGrid'
	],
	xtype: 'guide_registration_registrationtab_guidetrain_edit_form',
	bodyPadding: 5,
    autoScroll: true,
    
    initComponent: function(){
    	var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
    	
    	
    	var universityStore = Ext.create('tourism.store.combo.MasUniversityStore',{
			storeId: 'guide-train-university-store'
		});
    	Ext.apply(this,{
    		
    		width: '100%',
	        height: '100%',
	        fieldDefaults: {
	        labelAlign: 'left',
            msgTarget: 'qtip'
	        },
    		
	        items:[
	              {
	            	  xtype: 'container',
	            	  layout: 'anchor',
	  	        	  defaults: {
	  	        	  anchor: '50%'
	  	        	},
	  	        	items:[
	  	        	    {
		  	        	     xtype: 'textfield',
		  	                 name: 'identityNo',
		  	                 fieldLabel: 'หมายเลขบัตรประชาชน',
		  	                 labelWidth: 150
		  	                 ,allowBlank: false
		  	                 ,afterLabelTextTpl: required
		  	                 ,maxLength: 13
		  	                 ,minLength: 13
		  	                 ,enforceMaxLength: true
		  	                 ,maskRe: /\d/
		  	             },
		  	             {
		  	            	 xtype: 'combo',
		  	            	 id : 'guide-train-university-store',
		                     name: 'masUniversityId',
		                     fieldLabel: 'สถาบัน',
		                     labelWidth: 150,
		                     store: 'tourism.store.combo.MasUniversityStore',
		                     queryMode: 'remote',
		                     displayField: 'universityName',
		                     valueField: 'masUniversityId',
		                     hiddenName: 'masUniversityId',
		                     triggerAction: 'all',
		                     flex: 1,
		                     // afterLabelTextTpl: required,
		                     allowBlank: true
		  	             },
		  	             {
		  	            	 xtype: 'datefield',
		              	     name: 'studyDate',
		              	     fieldLabel: 'วันที่อบรม',
		              	     labelWidth: 150,
		              	     flex: 1,
		                     format: 'd/m/B',
		                     // afterLabelTextTpl: required,
		                     allowBlank: true
		  	             },
		  	             {
		  	            	 xtype: 'datefield',
		             	     name: 'graduationDate',
		             	     fieldLabel: 'ถึงวันที่',
		             	     labelWidth: 150,
		             	     flex: 1,
		                     format: 'd/m/B',
		                     // afterLabelTextTpl: required,
		                     allowBlank: true
		  	             }
		  	        ]
	              },
	              {
	            	  xtype: 'button',
			        	text: 'ค้นหา',
			        	action: 'searchGuideTrain'
	              },
	              {xtype: 'splitter'},{xtype: 'splitter'},
	              {
	  	        	xtype: 'container',
	  	        	layout: 'anchor',
	  	        	defaults: {
	  	        		anchor: '100%'
	  	        	},
	  	        	items: [
	  	        	{
	                    xtype: 'guide_registration_guideaddtrainsearch_grid',
	                    border: false
	                  }
	  	        	]
	  	        }
	        ],
	        buttons: [{
                text: 'บันทึก',
                scope: this,
                action: 'saveGuideTrain'
            }] 
    	});
    	
    	this.callParent(arguments);
    }
    
});