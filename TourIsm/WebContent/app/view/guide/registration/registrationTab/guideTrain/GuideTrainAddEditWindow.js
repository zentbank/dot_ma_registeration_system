Ext.define('tourism.view.guide.registration.registrationTab.guideTrain.GuideTrainAddEditWindow',{
	extend: 'Ext.window.Window',
	alias: 'widget.registration_registrationtab_guidetrain_window',
	requires:[
	       'tourism.view.guide.registration.registrationTab.guideTrain.GuideTrainAddEditForm'
	 ],
	          title: 'การฝึกอบรม',
	          layout: 'fit',
	          width: 750,
	          height: 550,
	          //True to make the window modal and mask everything behind it when displayed
	          modal: true,
	
	          initComponent: function(){
		 
		 this.items = [
		            {
		            	xtype: 'guide_registration_registrationtab_guidetrain_edit_form',
		                frame: false,
		                border: false
		                
		            }  
		 ];
		 this.callParent(arguments);
	 }

});