Ext.define('tourism.view.guide.registration.registrationTab.guideTrain.GuideTrainGrid',{
	extend: 'Ext.grid.Panel',
	alias: 'widget.guide_registration_registrationTab_guidetraingrid',
	stripeRows: true,
	requires: [
      'Ext.grid.Column',   
      'Ext.toolbar.Paging',
	  'Ext.grid.plugin.RowEditing',
	  'Ext.grid.column.Template',
	  'Ext.grid.column.Action',
	  'Ext.grid.plugin.RowExpander',
	  'Ext.form.field.Number',
	  'tourism.store.grid.EducationStore'
	],
	initComponent: function(){
	  var guidetraingridEducationStore = Ext.create('tourism.store.grid.EducationStore',{
		  storeId: 'guide-registration-educationstore-0098'
	  });
	  
	  Ext.apply(this,{
		 store: guidetraingridEducationStore,
		 columns:
		 [
			 {
				text: 'personTrainedId',
				dataIndex: 'personTrainedId',
				hidden: true
			 },
			 {
				text: 'eduId',
				dataIndex: 'eduId',
				hidden: true
			 },
			 {
				text: 'personId',
				dataIndex: 'personId',
				hidden: true
			 },
			 {
				text: 'traderId',
				dataIndex: 'traderId',
				hidden: true
			  },
			  {
				 text: 'regId',
				 dataIndex: 'regId',
				 hidden: true
			  },{
				 text: 'หลักสูตรที่',
				 xtype: 'rownumberer',
				 align: 'center',
				 flex: 1
			 },{
				text: 'ชื่อหลักสูตร',
				dataIndex: 'graduationCourse',
				flex: 2
			 },{
				text: 'รุ่นที่',
				dataIndex: 'generationGraduate',
				flex: 0.5
			 },{ 
				text: 'สถาบัน',
				dataIndex: 'universityName',
				flex: 2
			 },{
				text: 'วันที่อบรม',
				alignText: 'center',
				dataIndex: 'studyDate',
				flex: 2,
				renderer: function(value, metaData, model)
				 {
	               
	               return model.get('studyDate')+'-'+model.get('graduationDate');
				 }
			 },{
	        	 text: 'ลบ',
	        	 xtype: 'actioncolumn',
	        	 width: 50,
	        	 items: [
	        	 {
	        	    iconCls: 'icon-delete-xsmall',
	                tooltip: 'ลบ',
		            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
		                this.fireEvent('itemclick', this, 'deleteguidetrain', grid, rowIndex, colIndex, record, node);
		            }
	        	 }
	        	 ]
	         }
		 ],
		 dockedItems: [
		 {
			 xtype: 'toolbar',
			 dock: 'top',
			 items: [
			 // {
				//  xtype: 'component', flex: 1 
		  //    },
		     {
				 xtype: 'button',
				 text: 'เพิ่มข้อมูลการอบรม',
				 action: 'addGuideTrain'
			 }
			 ]
		 }
		 ]
	  
	  });
	  
	  this.callParent();
	}
	
});