Ext.define('tourism.view.guide.registration.registrationTab.guideTrain.GuideTrainTypeAddEditForm',{
	extend: 'Ext.form.Panel',
	//alias: 'widget.guide_registration_registrationTab_guidetrainaddedit',
	
	 requires: [
	            'Ext.data.*',
	            'Ext.form.*',
	            'Ext.tab.Tab',
	        	'tourism.view.guide.registration.registrationTab.guideTrain.GuideTrainGrid'
	        ],
	xtype: 'guide_registration_registrationTab_guidetrainaddedit',
	bodyPadding: 5,
	autoScroll: true,
	initComponent: function()
	{
		Ext.apply(this,{
			width: '100%',
	        height: '100%',
	        fieldDefaults: {
	        labelAlign: 'left',
            msgTarget: 'qtip'
	        },
	        defaults: {
                anchor: '100%'
        	},
	        items:[
	               {
	            	      xtype: 'container',
		            	  layout: 'fit',
		  	        	 
		  	        	items:[
								{
									xtype: 'hiddenfield',
									name: 'eduId'
								}, 
								{
									xtype: 'hiddenfield',
									name: 'licenseGuideNo'
								},
								{
									xtype: 'hiddenfield',
									name: 'traderGuideId'
								},
								{
									xtype: 'hiddenfield',
									name: 'personGuideId'
								}
								,
			  	        	    {
			  	        	    	xtype:'guide_registration_registrationTab_guidetraingrid',
			  	        	    	border: false
			  	        	    }
			  	        	  ]
	               }
	            ]
		});
		 this.callParent(arguments);
	}
});