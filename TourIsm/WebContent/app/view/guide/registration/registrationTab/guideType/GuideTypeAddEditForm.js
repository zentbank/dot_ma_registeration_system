Ext.define('tourism.view.guide.registration.registrationTab.guideType.GuideTypeAddEditForm', {
    extend: 'Ext.form.Panel',
    requires:[
        'Ext.form.FieldSet',
        'Ext.form.FieldContainer',
        'Ext.form.field.VTypes'
    ],
    xtype: 'guide_registration_guidetype_guidetypeaddedit_form',
   initComponent: function(){

        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
        
        var provinceStore =  Ext.create('tourism.store.combo.MasProvinceStore',{
            storeId:'guide-type-registration-masprovince-combo'
        });
//        provinceStore.load();
        var guideCategoryStore =  Ext.create('tourism.store.combo.GuideCategoryStore',{
            storeId:'tourism.store.combo.GuideCategoryStore-combo-00998'
        });
        
        
        Ext.apply(this, {
            // width: 800,
           
            layout: 'anchor',
            defaults: {
                    anchor: '100%',
                    labelWidth: 150
            },
            items: [
{
                    xtype: 'radiogroup',
                    allowBlank: false,
                    fieldLabel: 'ประเภทมัคคุเทศก์',
                    
                    afterLabelTextTpl: required,
                    
                    itemId: 'guide-registration-tradercategory-traderCategorygroup',
                    id: 'guide-tradercategory-traderCategorygroup',
                    // Arrange checkboxes into two columns, distributed vertically
                    columns: 1,
                    //checked: 1,
                    vertical: true,
                    items: [
                        //Oat Edit ไม่ต้องการให้เช็คไว้
                        { boxLabel: 'ทั่วไป',  name: 'traderCategoryType', inputValue: '1'},
//                        { boxLabel: 'ทั่วไป',  name: 'traderCategoryType', inputValue: '1', checked: true},
                        //End Oat Edit
                        { boxLabel: 'เฉพาะพื้นที่',  name: 'traderCategoryType', inputValue: '2' } 
                    ]

                },
                {
                	xtype:'splitter'
                }, {
                	xtype:'splitter'
                }, {
                	xtype:'splitter'
                }, {
                	xtype:'splitter'
                },
               
                {
                    xtype: 'combo',
                    fieldLabel: 'ชนิดมัคคุเทศก์',
                    
                    store: guideCategoryStore,
                    id: 'guide-type-registration-categorystore-combo',
                    queryMode: 'local',
                    afterLabelTextTpl: required,
                    displayField: 'traderCategoryName',
                    valueField: 'traderCategory',
                    hiddenName: 'traderCategory',
                    name:'traderCategory',
                    allowBlank: false
                },
                
                {
                	xtype:'splitter'
                }, {
                	xtype:'splitter'
                }, {
                	xtype:'splitter'
                }, {
                	xtype:'splitter'
                },
                {
                    xtype: 'combo',
                    fieldLabel: 'พื้นที่จังหวัด',
                    
                    store: provinceStore,
                    id: 'guide-type-registration-masprovince-combo',
                    queryMode: 'remote',
                    displayField: 'provinceName',
                    valueField: 'provinceId',
                    hiddenName: 'provinceId',
                    name: 'provinceGuideServiceId',
                    triggerAction: 'all',
                    flex: 1
                  //  allowBlank: false
                },
                {
                	xtype:'splitter'
                },
                {
                	xtype:'splitter'
                },
                {
                	xtype:'splitter'
                },
                {
                	xtype:'splitter'
                },
                {
                    xtype: 'textfield',
                    
                    fieldLabel: 'หรือพื้นที่ให้บริการ',
                    name: 'traderArea',
                   // allowBlank: false
                }
                
            ]

        
        });
        
        this.callParent();
    }
});