Ext.define('tourism.view.guide.registration.registrationType.GuideRegistrationTypeAddEditForm' ,{
	
	extend: 'Ext.form.Panel',
	
	requires: [
	       'Ext.data.*',
	       'Ext.form.*',
	       'Ext.tab.Tab'  
	],
	
	alias:'widget.guide-registration-registrationType-addeditform',
	
	bodyPadding: 5,
    autoScroll: true,
    
    initComponent: function() {
    	
    	var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
   
    	Ext.apply(this,{
    		
    		 width: '100%',
             height: '100%',
             fieldDefaults: {
            	  labelAlign: 'left',
                  labelWidth: 150,
                  msgTarget: 'qtip'
             },
             items: 
                 [ 
                     {
                         /*
                         ประเภทใบอนุญาต
                         B=ธุรกิจนำเที่ยว
                         G=มัคคุเทศก์
                         L=ผู้นำเที่ยว
                         */
                         xtype: 'hiddenfield',
                         name: 'traderType',
                         value: 'G'
                     },
                     {
                         xtype: 'radiogroup',
                         fieldLabel: 'ประเภทการทำรายการ',
                         // labelWidth: 150,
                         allowBlank: false,
                         afterLabelTextTpl: required,
                         // Arrange checkboxes into two columns, distributed vertically
                         columns: 1,
                         vertical: true,
                         checked: 1,
                         items: [
                           
                      		{ boxLabel: 'จดทะเบียนใบอนุญาตใหม่',  name: 'registrationType', inputValue: 'N',checked: true },
                            { boxLabel: 'ต่ออายุใบอนุญาต', name: 'registrationType' ,inputValue: 'R'},
                            { boxLabel: 'เปลี่ยนแปลงรายการใบอนุญาต',  name: 'registrationType', inputValue: 'C' },
                            { boxLabel: 'ออกใบแทน',  name: 'registrationType', inputValue: 'T' }//,
//                            { boxLabel: 'ขอบัตรใหม่(บัตรชำรุด/สูญหาย)',  name: 'registrationType', inputValue: 'D' },
//                            { boxLabel: 'เปลี่ยนชื่อ-สกุล',  name: 'registrationType', inputValue: 'Q' }
                         ]
                     }
                 ],
                 buttons: [{
                     text: 'ดำเนินการต่อ',
                     action: 'selectRegType'
                     //scope: this,
                     //handler: this.onCompleteClick
                 }]    
             });
             this.callParent(arguments);
         }
         ,
         
         onResetClick: function(){
             this.getForm().reset();
         }
         
/*         onCompleteClick: function(btn){
             var form = this.getForm();
             if (form.isValid()) {
                 var win    = btn.up('window');
                 var layout = win.getLayout();
                 layout.setActiveItem(1);
             }
         }*/
  });
