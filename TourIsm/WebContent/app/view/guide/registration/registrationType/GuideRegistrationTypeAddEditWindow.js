Ext.define('tourism.view.guide.registration.registrationType.GuideRegistrationTypeAddEditWindow',{
	extend: 'Ext.window.Window',
	
	alias: 'widget.guide-registration-registrationtype-addeditwindow',
	
	requires: [
	         'tourism.view.guide.registration.registrationType.GuideRegistrationTypeAddEditForm'
	        // ,'tourism.view.guide.registration.registrationType.GuideRegistrationTypeSearchBusinessForm'
	        ,'tourism.view.business.registration.registrationType.RegistrationTypeSearchBusinessForm'
	],
	
	initComponent: function() {
		
		Ext.apply(this,{
			title: 'เลือกประเภทการทำรายการ',
			layout: 'card',
			activeItem: 0,
            autoShow: true,
            width: 400,
            height: 400,
            autoShow: true,
            modal: true,
            items :[
                    {
                        xtype: 'guide-registration-registrationType-addeditform',
                        border: false,
                        frame: false
                    },
	                {
	                   xtype: 'business-registration-registrationtype-searchbusinessform',
	                   id: 'guide-registration-registrationtype-searchGuideform',
	                   registerType: 'guide',
	                   border: false,
	                   frame: false
	                }
                ]
		});
		this.callParent(arguments);
	},
});