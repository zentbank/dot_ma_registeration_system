Ext.define('tourism.view.guidedetail.GuideDetailAddEditForm', {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.data.*',
        'Ext.form.*',
        'Ext.tab.Tab',
        'tourism.view.guidedetail.GuideDetailLanguageGrid'
    ],
    xtype: 'form-guidedetail-addedit',
    // title: 'มัคคุเทศน์',
    bodyPadding: 5,
    autoScroll: true,
    initComponent: function(){

        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
        
        var provinceStore =  Ext.create('tourism.store.combo.MasProvinceStore',{
            storeId:'guide-registration-address-masprovince-store'
       });

		var tambolStore  = Ext.create('tourism.store.combo.MasTambolStore',{
		     storeId:'guide-registration-address-tambol-store'
		});
		var amphurStore =   Ext.create('tourism.store.combo.MasAmphurStore',{
		     storeId:'guide-registration-address-masamphur-store'
		});
        
        Ext.apply(this, {
            width: '100%',
            height: '100%',
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },

            items: [ {
                xtype: 'container',
                // title: 'Payment',
                layout: 'anchor',
                defaults: {
                    anchor: '100%'
                },
                items: [
                   {
                	xtype : 'container',
                	layout: 'hbox',
                	items: [
                		{
                			xtype: 'fieldset',
                			flex: 1,
                			border: false,
                			defaults: {
                				anchor: '100%',
                				labelWidth: 200
                			},
                			items:[
								   {
									   xtype: 'hiddenfield',
									   name: 'guideId'
								   },
                			       {
                			    	   xtype: 'fieldcontainer',
                			    	   labelAlign: 'left',
                			    	   fieldLabel: '1.ชื่อ ภาษาไทย(นาย/นาง/นางสาว)',
                			    	   layout: 'hbox',
                			    	   		items: [{
                			    	   			xtype: 'masprefixcombo',
                			    	   			store: 'tourism.store.combo.MasPrefixStore',
                			    	   			queryMode: 'remote',
                			    	   		    displayField: 'prefixName',
                			    	   		    valueField: 'prefixId',
                			    	   		    hiddenName: 'prefixId',
                			    	   		    name: 'prefixId',
                			    	   		    triggerAction: 'all',
                			    	   			flex:1,
                			    	   			allowBlank: false,
                                                afterLabelTextTpl: required,
                			    	   			tabIndex : 1,
                                                prefixType: 'I'
                			    	   	},
                			    	   	{
                			    	   		xtype: 'splitter'
                			    	   	},
                			    	   	{
                			    	   		xtype: 'textfield',
                			    	   		name: 'firstName',
    	                                    flex: 2,
    	                                    allowBlank: false,
                                            afterLabelTextTpl: required,
    	                                    tabIndex : 2
                			    	   	}
                			    	   	]
                			       },
                			       {
                			    	   xtype: 'fieldcontainer',
                			    	   labelAlign: 'left',
                			    	   fieldLabel: '  ชื่อ ภาษาอังกฤษ(Mr./Mrs./Miss)',
                			    	   layout: 'hbox',
                			    	   		items: [{
                			    	   			xtype: 'masprefixcombo',
                			    	   			store: 'tourism.store.combo.MasPrefixStore',
                			    	   		    queryMode: 'remote',
                			    	   		    displayField: 'prefixNameEn',
                			    	   		    valueField: 'prefixId',
                			    	   		    hiddenName: 'prefixIdEn',
                			    	   		    triggerAction: 'all',
                			    	   		    name: 'prefixIdEn',
                			    	   			flex:1,
                			    	   			allowBlank: false,
                                                afterLabelTextTpl: required,
                			    	   			tabIndex : 4,
                                                prefixType: 'I'
                			    	   	},
                			    	   	{
                			    	   		xtype: 'splitter'
                			    	   	},
                			    	   	{
                			    	   		xtype: 'textfield',
                			    	   		name: 'firstNameEn',
    	                                    flex: 2,
    	                                    allowBlank: false,
                                            afterLabelTextTpl: required,
    	                                    tabIndex : 5
                			    	   	}
                			    	   
                			    	   	]
                			       
                			       }
    	                        ,{
    	                        	 xtype: 'datefield',
    	                             fieldLabel: 'วัน/เดือน/ปี เกิด',
    	                             name: 'birthDate',
    	                             format: 'd/m/B',
    				                 allowBlank: false,
                                     afterLabelTextTpl: required,
    				                 tabIndex : 7
    	                        }
                			]
                		},
                		{
    						xtype: 'fieldset',
    					    flex:1,
    					    border: false,
    					    defaults: {
    					     anchor: '100%',
    					     labelWidth: 170
    					    },
    					    items: [
    	                        {
    	                            // นามสกุล(ภาษาไทย)
    	                            xtype: 'textfield',
    	                            fieldLabel: 'สกุล(ภาษาไทย)',
    	                            name: 'lastName',
    	                            labelAlign: 'left',
                                    afterLabelTextTpl: required,
    			                    allowBlank: false,
    			                    tabIndex : 3
    	                        },
    	                        {
    	                            // นามสกุล(ภาษาอังกฤษ)
    	                            xtype: 'textfield',
    	                            fieldLabel: 'สกุล(ภาษาอังกฤษ)',
    	                            name: 'lastNameEn',
    	                            labelAlign: 'left',
    			                    allowBlank: false,
                                    afterLabelTextTpl: required,
    			                    tabIndex : 6
    	                        }
    	                        ,{xtype: 'splitter'}
    	                        ,{
    	                            // อายุ
    	                        	xtype: 'fieldcontainer',
    	                            layout: 'hbox',
    	                            items:[
    	                            {
    		                            xtype: 'textfield',
    		                            labelWidth: 170,
    		                            width: 350,
    		                            fieldLabel: 'อายุ',
    		                            name: 'ageYear',
    		                            labelAlign: 'left',
    				                    allowBlank: false,
                                        afterLabelTextTpl: required,
    				                    tabIndex : 8
    	                            },{xtype: 'splitter'},{xtype: 'splitter'},
                                    {
    	                            	 xtype: 'label',
    			                         text: 'ปี',
    			                         labelAlign: 'left'
    	                            }
    	                     
    	                            ]
    	                        }
    	                        
    	                        ]
    					}
                		]
                }	
                ,{
                    xtype: 'container',
                    layout: 'hbox',
                    layoutCongig: {
                         pack:'center',
                         align:'middle'
                    },
                    items: [
                       {
							xtype: 'fieldset',
							flex: 1,
							border: false,
							defaults: {
//								anchor: '100%',
								anchor: '50%',
								labelWidth: 200
							},
							 items:[
							    {
				                    xtype: 'textfield',
				                    fieldLabel: '2.เลขที่ใบอนุญาตเป็นมัคคุเทศก์',
				                    name: 'licenseNo',
				                    labelAlign: 'left',
				                    afterLabelTextTpl: required,
				                    allowBlank: false,
				                    maxLength: 8,
		                            minLength: 8,
		                            enforceMaxLength: true,
				                    maskRe: /[\d\-]/,
				                    tabIndex : 9
							    }
							 ]
                       }
//                       ,{
//							xtype: 'fieldset',
//							flex: 1,
//							border: false,
//							defaults: {
//								anchor: '30%',
//								labelWidth: 50
//							},
//							 items:[
//							    {
//							    	xtype: 'button',
//							    	text : 'ค้นหาข้อมูลใบอนุญาต',
//							    	scope: this,
////							    	scale   : 'small',
//							    	action: 'searchLicenseGuideDetail'
//							    }
//							    ]
//                       }
                       ]
                }  
                ,{
                    xtype: 'container',
                    layout: 'hbox',
                    layoutCongig: {
                         pack:'center',
                         align:'middle'
                    },
                    items: [
                       {
							xtype: 'fieldset',
							flex: 1,
							border: false,
							defaults: {
								anchor: '100%',
								labelWidth: 200
							},
							 items:[
								{
	   	                        	 xtype: 'datefield',
	   	                             fieldLabel: 'วันออกบัตร',
	   	                             name: 'effectiveDate',
	   	                             format: 'd/m/B',
	   	                             afterLabelTextTpl: required,
	   	                             allowBlank: false,
	   				                 tabIndex : 10
	    	                    }  
							]
                        }//fieldset1
                        ,{
	   						xtype: 'fieldset',
	   					    flex:1,
	   					    border: false,
	   					    defaults: {
	   					     anchor: '100%',
	   					     labelWidth: 170
	   					    },
	   					    items: [
								{
									xtype: 'datefield',
								    fieldLabel: 'บัตรหมดอายุ',
								    name: 'expireDate',
								    format: 'd/m/B',
								    afterLabelTextTpl: required,
								    allowBlank: false,
								    tabIndex : 11
								}
	   					    ]
                        }//fieldset2
                    ]
                }//hbox
                   
                ,{
                    xtype: 'container',
                    layout: 'hbox',
                    layoutCongig: {
                         pack:'center',
                         align:'middle'
                    },
                    items: [
                       {
							xtype: 'fieldset',
							flex: 1,
							border: false,
							defaults: {
								anchor: '100%',
								labelWidth: 200
							},
							 items:[
								{	   				                 
	   				              xtype: 'textfield',
	                              name: 'addressNo',
	                              fieldLabel: '3. ที่อยู่ปัจจุบัน บ้านเลขที่',
	                              allowBlank: false,
	                              afterLabelTextTpl: required,
	                              tabIndex : 12
	    	                    }  
								,{
									xtype: 'textfield',
		                            name: 'moo',
		                            fieldLabel: 'หมู่ที่',
		                            tabIndex : 14
								}
								,{
									xtype: 'textfield',
	                                name: 'roadName',
	                                fieldLabel: 'ถนน',
	                                tabIndex : 16
								}
								,{
		                               // อำเภอ/เขต
		                              xtype: 'masamphurcombo',
		                              fieldLabel: 'อำเภอ/เขต',
		                              id : 'guide-registration-address-masamphur-combo',
		                              store: amphurStore,
		                              displayField: 'amphurName',
		                              valueField: 'amphurId',
		                              hiddenName: 'amphurId',
		                              name: 'amphurId',
		                              allowBlank: false,
		                              afterLabelTextTpl: required,
		                              queryParam: 'amphurName',
		                              masprovinceComboId: 'guide-registration-address-masprovince-combo',
		                              mastambolComboId: 'guide-registration-address-tambol-combo',
		                              tabIndex : 18
		                             
		                          }
								,{
		                              // รหัสไปรษณีย์
		                              xtype: 'textfield',
		                              name: 'postCode',
		                              fieldLabel: 'รหัสไปรษณีย์',
		                              allowBlank: false,
		                              afterLabelTextTpl: required,
		                              maxLength: 5,
		                              minLength: 5,
		                              enforceMaxLength: true,
		                              maskRe: /\d/,
		                              tabIndex : 20
//		                              ,value: '50000'
		                          }
								,{
		                              // โทรศัพท์มือถือ
		                              xtype: 'textfield',
		                              name: 'mobileNo',
		                              fieldLabel: 'โทรศัพท์มือถือ',
		                              tabIndex : 22
		                          }
								,{
		                              // อีเมล์
		                              xtype: 'textfield',
		                              name: 'email',
		                              fieldLabel: 'อีเมล์',
		                              tabIndex : 24,
		                              vtype: 'email'
//		                              ,value: 'sek.beck@gmail.com'
		                          }
							]
                        }//fieldset1
                        ,{
	   						xtype: 'fieldset',
	   					    flex:1,
	   					    border: false,
	   					    defaults: {
	   					     anchor: '100%',
	   					     labelWidth: 170
	   					    },
	   					    items: [
								{								    
								  xtype: 'textfield',
	                              name: 'villageName',
	                              fieldLabel: 'หมู่บ้าน',
	                              tabIndex : 13
								}
								,{
							      xtype: 'textfield',
	                              name: 'soi',
	                              fieldLabel: 'ตรอก/ซอย',
	                              tabIndex : 15
								}
								,{
		                              // จังหวัด (TH)
		                              xtype: 'masprovincecombo',
		                              fieldLabel: 'จังหวัด',
		                              id : 'guide-registration-address-masprovince-combo',
		                              store: provinceStore,
		                              displayField: 'provinceName',
		                              valueField: 'provinceId',
		                              hiddenName: 'provinceId',
		                              name: 'provinceId',
		                              allowBlank: false,
		                              afterLabelTextTpl: required,
		                              masamphurComboId:'guide-registration-address-masamphur-combo',
		                              queryParam: 'provinceName',
		                              tabIndex : 17      
		                          }
								,{
									// ตำบล
		                              xtype: 'mastambolcombo',
		                              fieldLabel: 'ตำบล',
		                              id : 'guide-registration-address-tambol-combo',
		                              store: tambolStore,
		                              displayField: 'tambolName',
		                              valueField: 'tambolId',
		                              hiddenName: 'tambolId',
		                              name: 'tambolId',
		                              allowBlank: false,
		                              afterLabelTextTpl: required,
		                              masamphurComboId: 'guide-registration-address-masamphur-combo',
		                              queryParam: 'tambolName',
		                              tabIndex : 19
								},{
		                              // โทรศัพท์
		                              xtype: 'textfield',
		                              name: 'telephone',
		                              fieldLabel: 'โทรศัพท์',
		                              // maxLength: 15,
		                              // minLength: 9,
		                              // enforceMaxLength: true,
		                              // maskRe: /[\d\-]/,
		                              // regex: /^\d{9}(\-\d{2,3}|\-\d{4})?$/,
		                              tabIndex : 21
		                          },{
		                              // โทรสาร
		                              xtype: 'textfield',
		                              name: 'fax',
		                              fieldLabel: 'โทรสาร',
		                              tabIndex : 23
		                          }
	   					    ]
                        }//fieldset2
                    ]
                }//hbox
                ,{
                    xtype: 'container',
                    layout: 'hbox',
                    layoutCongig: {
                         pack:'center',
                         align:'middle'
                    },
                    items: [
                       {
							xtype: 'fieldset',
							flex: 1,
							border: false,
							defaults: {
								anchor: '100%',
								labelWidth: 200
							},
							 items:[
							    {
                                    xtype: 'combo',
                                    fieldLabel: '4. ระดับการศึกษา',
                                    store: 'tourism.store.guidedetail.combo.EducationLevelStore',
                                    queryMode: 'local',
                                    displayField: 'educationLevelName',
                                    valueField: 'educationLevelName',
                                    hiddenName: 'educationLevelName',
                                    name: 'educationLevelName',
                                    //value: 'W',
        	                        tabIndex: 25
                                }
							    ,{
		                              xtype: 'textfield',
		                              name: 'educationLevelNameOth',
		                              fieldLabel: 'อื่นๆโปรดระบุ',
		                              tabIndex : 26
							    }
							 ]
                       },
                       {
							xtype: 'fieldset',
							flex: 1,
							border: false,
							defaults: {
								anchor: '100%',
								labelWidth: 200
							},
							 items:[
							    {
                                   xtype: 'combo',
                                   fieldLabel: '5. ปัจจุบันประกอบอาชีพมัคคุเทศก์อยู่หรือไม่',
                                   store: 'tourism.store.guidedetail.combo.AmateurLevelStore',
                                   queryMode: 'local',
                                   displayField: 'amateurLevelName',
                                   valueField: 'amateurLevel',
                                   hiddenName: 'amateurLevel',
                                   name: 'amateurLevel',
                                   //value: 'W',
       	                        tabIndex: 27
                               }
							    
							 ]
                      }
                       ]
                  },
                {
                    xtype: 'container',
                    layout: 'hbox',
                    layoutCongig: {
                         pack:'center',
                         align:'middle'
                    },
                    items: [
                       {
							xtype: 'fieldset',
							flex: 1,
							border: false,
							defaults: {
								anchor: '100%',
								labelWidth: 200
							},
							 items:[
							    {
                                    xtype: 'combo',
                                    fieldLabel: '7. ระยะเวลาในการประกอบอาชีพมัคคุเทศก์(จนถึงปัจจุบัน)',
                                    store: 'tourism.store.guidedetail.combo.ExperienceYearStore',
                                    queryMode: 'local',
                                    displayField: 'experienceYear',
                                    valueField: 'experienceYear',
                                    hiddenName: 'experienceYear',
                                    name: 'experienceYear',
                                    //value: 'W',
        	                        tabIndex: 28
                                }
							    
							 ]
                       },
                       {
							xtype: 'fieldset',
							flex: 1,
							border: false,
							defaults: {
								anchor: '100%',
								labelWidth: 200
							},
							 items:[
							    {
                                   xtype: 'combo',
                                   fieldLabel: '8. ลักษณะของการทำงาน',
                                   store: 'tourism.store.guidedetail.combo.WorkTypeStore',
                                   queryMode: 'local',
                                   displayField: 'workTypeName',
                                   valueField: 'workType',
                                   hiddenName: 'workType',
                                   name: 'workType',
                                   //value: 'W',
       	                        tabIndex: 29
                               }
							    ,{
		                              xtype: 'textfield',
		                              name: 'workTypeName',
		                              fieldLabel: 'โปรดระบุ',
		                              tabIndex : 30
							    }
							    
							 ]
                      }
                       ]
                  }
                  ,{
                      xtype: 'container',
                      layout: 'hbox',
                      layoutCongig: {
                           pack:'center',
                           align:'middle'
                      },
                      items: [
                         {
  							xtype: 'fieldset',
  							flex: 1,
  							border: false,
  							defaults: {
  								anchor: '100%',
  								labelWidth: 200
  							},
  							 items:[
  							    {
                                      xtype: 'combo',
                                      fieldLabel: '9. รายได้ต่อเดือนเฉพาะอาชีพเป็นมัคคุเทศก์',
                                      store: 'tourism.store.guidedetail.combo.SalaryNameStore',
                                      queryMode: 'local',
                                      displayField: 'salaryName',
                                      valueField: 'salaryName',
                                      hiddenName: 'salaryName',
                                      name: 'salaryName',
          	                        tabIndex: 31
                                  }
  							    
  							 ]
                         }
                         ,{
  							xtype: 'fieldset',
  							flex: 1,
  							border: false,
  							defaults: {
  								anchor: '100%',
  								labelWidth: 200
  							},
  							 items:[
  							    {
                                     xtype: 'combo',
                                     fieldLabel: '10. ลักษณะการทำงานในฐานะมัคคุเทศก์ของท่านมีหน้าที่อะไรบ้าง',
                                     store: 'tourism.store.guidedetail.combo.CharacterTypeNameStore',
                                     queryMode: 'local',
                                     displayField: 'characterTypeName',
                                     valueField: 'characterTypeName',
                                     hiddenName: 'characterTypeName',
                                     name: 'characterTypeName',
                                     //value: 'W',
         	                        tabIndex: 32
                                 }
  							    ,{
  		                              xtype: 'textfield',
  		                              name: 'characterTypeNameOth',
  		                              fieldLabel: 'อื่นๆโปรดระบุ',
  		                              tabIndex : 33
  							    }
  							    
  							 ]
                        }
                         ]
                    }
                  ,{
                      xtype: 'container',
                      layout: 'hbox',
                      layoutCongig: {
                           pack:'center',
                           align:'middle'
                      },
                      items: [
                         {
  							xtype: 'fieldset',
  							flex: 1,
  							border: false,
  							defaults: {
  								anchor: '100%',
  								labelWidth: 200
  							},
  							 items:[
  							    {
                                      xtype: 'combo',
                                      fieldLabel: '11. สมาคม/ชมรมมัคคุเทศก์ ที่ท่านเป็นสมาชิก',
                                      store: 'tourism.store.guidedetail.combo.ClubNameStore',
                                      queryMode: 'local',
                                      displayField: 'clubName',
                                      valueField: 'clubName',
                                      hiddenName: 'clubName',
                                      name: 'clubName',
          	                        tabIndex: 34
                                }
  							   ,{
		                              xtype: 'textfield',
		                              name: 'clubNameTxt',
		                              fieldLabel: 'โปรดระบุ',
		                              tabIndex : 35
							    }
  							    
  							 ]
                         },
                         {
   							xtype: 'fieldset',
   							flex: 1,
   							border: false,
   							defaults: {
   								anchor: '100%',
   								labelWidth: 200
   							},
   							 items:[
								{
									xtype: 'datefield',
								    fieldLabel: 'วันที่มารายงานตัวมัคคุเทศก์',
								    name: 'registerDtm',
								    format: 'd/m/B',
								    afterLabelTextTpl: required,
								    allowBlank: false,
								    tabIndex : 36
								}
   							    
   							 ]
                          }
                      
                         ]
                    }
                ]
            }
            ,{
                    xtype: 'tabpanel',
                    plain: true,
                    frame: false,
                    border: false,
                 
                    defaults: {
                        bodyPadding: 5
                    },
                   items: [
                        ,{
                            title: 'ภาษา', 
                            layout: 'anchor',
                        	frame: false,
                        	border: false,
                             items:[
                                 {
                                	 xtype: 'window_guidedetail_language_grid'
                                 }
                                 ]
                        }
                    ]
                }
        ],

            buttons: [{
                text: 'บันทึก',
                scope: this,
                action: 'saveGuideDetail',
                tabIndex : 50
            }]    
        });
        this.callParent();
    }
    ,onResetClick: function(){
        this.getForm().reset();
    }
//    ,onCompleteClick: function(){
//        var form = this.getForm();
//        if (form.isValid()) {
//            Ext.MessageBox.alert('Submitted Values', form.getValues(true));
//        }
//    }
    ,afterRender: function()
    {
	    this.callParent(arguments);
	    var form = this;
		var preFixId = form.getForm().findField('prefixId');
		 
		preFixId.addListener("blur" ,function(textf, eOpts)
		{
		 	var values  =  textf.up('form').getValues();
		 	var preFixIdEn = textf.up('form').getForm().findField('prefixIdEn');
			
			if((!Ext.isEmpty(preFixId)))
		 	{
		 		preFixIdEn.setValue(values.prefixId);
		 	}
		 },this);
	    //add registration
        if(!Ext.isEmpty(this.guidedetailModel) )
        {
        	console.log(this.guidedetailModel);
        	
        	if(this.guidedetailModel.get('prefixId') == 0)
        	{
        		this.guidedetailModel.set('prefixId', undefined);
        	}
        	if(this.guidedetailModel.get('provinceId') == 0)
        	{
        		this.guidedetailModel.set('provinceId', undefined);
        		this.guidedetailModel.set('amphurId', undefined);
        		this.guidedetailModel.set('tambolId', undefined);
        	}
        	
            this.loadRecord(this.guidedetailModel);
//            console.log(form.getForm().findField('guideId').getValue());
//            console.log(this.guidedetailModel.get('guideId'));
//            form.getForm().findField('guideId').setValue(this.guidedetailModel.get('guideId'));
//            console.log(form.getForm().findField('guideId').getValue());
        } 
    }
  
    
    
});