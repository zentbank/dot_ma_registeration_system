Ext.define('tourism.view.guidedetail.GuideDetailAddEditWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.guidedetail_addedit_window',

    requires: [
               'tourism.view.guidedetail.GuideDetailAddEditForm'
    ],

    title : 'เพิ่มแบบรายงานตัวมัคคุเทศก์',
    layout: 'fit',
    // autoShow: true,
    width: 950,
    height: 550,
    //True to make the window modal and mask everything behind it when displayed
    modal: true,
    // iconCls: 'icon-form',

    initComponent: function() {


        var formPanel = {
                    xtype: 'form-guidedetail-addedit',
                    id: 'key-form-guidedetail-addedit',
                    frame: false,
                    border: false,
                    guidedetailModel: this.guidedetailModel
        };
        
        this.items = [
                formPanel 
        ];

        this.callParent(arguments);
    }

});