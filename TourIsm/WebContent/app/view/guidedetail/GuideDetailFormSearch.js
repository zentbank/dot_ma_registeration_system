Ext.define('tourism.view.guidedetail.GuideDetailFormSearch' ,{
    extend: 'Ext.form.FormPanel',
    alias : 'widget.guidedetail_formsearch',
    requires: [
        'Ext.form.field.Date',
//        'Ext.form.field.Time',
        'Ext.form.CheckboxGroup',
        'Ext.layout.container.HBox',
        'Ext.form.FieldSet',
        'Ext.util.Positionable'
    ],
    bodyPadding: 10,
    // width: '100%',
    items: [
//                {
//            xtype: 'container',
//            layout: 'anchor',
//            defaults: {
//                anchor: '100%',
//                labelWidth: 150
//            },
//            items: [
         
                    {
                        xtype: 'container',
                        layout: 'hbox',
                        layoutCongig: {
                             pack:'center',
                             align:'middle'
                        },
                        items: [
                            {
                                xtype: 'container',
                                // title: 'Payment',
                                flex: 1,
                                layout: 'anchor',
                                defaults: {
                                    anchor: '100%',
                                    labelWidth: 150
                                },
                                items: [
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'เลขที่ใบอนุญาต',
                                        name: 'licenseNo',
//                                        id: 'licenseNo2',
                                        allowBlank: true
                                    },
                                    {
                                       xtype: 'textfield',
                                        fieldLabel: 'ชื่อ(ภาษาไทย)',
                                       name: 'firstName',
//                                       id: 'firstName2',
                                       allowBlank: true
                                   },
                                   {
                                       xtype: 'combo',
                                       fieldLabel: 'ภาษา',
                                       store: 'tourism.store.combo.LanguageStore',
                                       queryMode: 'remote',
                                       displayField: 'language',
                                       valueField: 'countryId',
                                       hiddenName: 'countryId',
                                       name: 'countryId'
                                   },
                                     {
                                       xtype: 'combo',
                                       fieldLabel: 'ประกอบอาชีพมัคคุเทศก์',
                                       store: 'tourism.store.guidedetail.combo.AmateurLevelStore',
                                       queryMode: 'local',
                                       displayField: 'amateurLevelName',
                                       valueField: 'amateurLevel',
                                       hiddenName: 'amateurLevel',
                                       name: 'amateurLevel'
                                     }
                                ]
                            },
                            {
                                xtype: 'splitter'
                            },
                            {
                                xtype: 'container',
                                flex: 1,
                                layout: 'anchor',
                                defaults: {
                                    anchor: '100%',
                                    labelWidth: 150
                                },
                                items: [
								    {   
									  xtype: 'datefield',
									  fieldLabel: 'บัตรหมดอายุ',
									  name: 'expireDate',
//									  id: 'expireDate2',
									  format: 'd/m/B'
									},
									{
									    // นามสกุล
									    xtype: 'textfield',
									    fieldLabel: 'นามสกุล(ภาษาไทย)',
									    name: 'lastName',
//									    id: 'lastName2',
									    labelAlign: 'left'
									 },
                                     {
                                       xtype: 'combo',
                                       fieldLabel: 'ระดับความสามารถภาษา',
                                       store: 'tourism.store.guidedetail.combo.LanguageLevelStore',
                                       queryMode: 'local',
                                       displayField: 'languageLevelName',
                                       valueField: 'languageLevel',
                                       hiddenName: 'languageLevel',
                                       name: 'languageLevel'
                                     },
                                     {
                                         xtype: 'combo',
                                         fieldLabel: 'ลักษณะของการทำงาน',
                                         store: 'tourism.store.guidedetail.combo.WorkTypeStore',
                                         queryMode: 'local',
                                         displayField: 'workTypeName',
                                         valueField: 'workType',
                                         hiddenName: 'workType',
                                         name: 'workType'
                                       }
									   
                                ]
                            }
                        ]
//                    }
//            ]
        },
        {
            xtype: 'toolbar',
            border: false,
            padding: '6px 0 6px 0px',
            items: [{
            	xtype: 'button',
                text : 'ค้นหา',
                action: 'searchGuideDetail'  
            }]
        }

    ]

});    
