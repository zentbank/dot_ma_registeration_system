Ext.define('tourism.view.guidedetail.GuideDetailGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.guidedetail_grid',
		store: 'tourism.store.guidedetail.grid.GuideDetailStore',
		model: 'tourism.model.guidedetail.GuideDetailModel',
		stripeRows: true,

		requires: [
			'Ext.grid.Column',
			'Ext.toolbar.Paging',
			'Ext.grid.plugin.RowEditing',
			'Ext.grid.column.Template',
			'Ext.grid.column.Action',
			'Ext.grid.plugin.RowExpander'
		]		
		,initComponent: function(){
			var guiddetailstore = Ext.create('tourism.store.guidedetail.grid.GuideDetailStore',{
				id: 'guidedetail-main-grid-store',
				listeners: {
					scope: this,
					// single: true,
					beforeload: function( store, operation, eOpts )
				        {
				        	var formSearch = Ext.ComponentQuery.query('#guidedetail-formsearch-key')[0];
				        	var values = formSearch.getValues();
								
				        	 for (field in values) 
						      {
						        if (Ext.isEmpty(values[field])) 
						        {
						          delete values[field];
						        }
						        
						      }
						    store.proxy.extraParams = values;
				        }
				}
			});

			Ext.apply(this, {
				store: guiddetailstore,
				dockedItems:[
					{
						xtype: 'pagingtoolbar',
						store: guiddetailstore,
						dock: 'bottom',
						displayInfo: true,
						items:[
							{
									text: 'เพิ่มรายการมัคคุเทศก์',
									action: 'add'
			        		}
		        		]
					}
				],
				columns: [
				{
					header: 'guideId',
					dataIndex: 'guideId',
					hidden: true
				},
				{
					header: 'เลขที่ใบอนุญาต',
					dataIndex: 'licenseNo',
					flex: 1
				},
				{
					header: 'ชื่อ-สกุล',
					dataIndex: 'fullName',
					flex: 2
				}
		        ,{
		        	header: '',
		            xtype: 'actioncolumn',
		           	width:100,
		           	align : 'center',
		            items: [{
		                iconCls: 'icon-edit-xsmall',
		                tooltip: 'รายละเอียด',
			            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
			            	this.fireEvent('itemclick', this, 'editguidedetail', grid, rowIndex, colIndex, record, node);
			            }
		            }]
		        },
		        {
	                xtype: 'actioncolumn',
	                header: 'ลบ',
	            	align : 'center',
	                width:50,
	                items: [{
	                	iconCls: 'icon-delete-xsmall',
	                    tooltip: 'ลบ',
//	                    scope: this,
	                    handler: function(grid, rowIndex, colIndex, node, e, record, rowNode) {
			            	this.fireEvent('itemclick', this, 'removeguidedetail', grid, rowIndex, colIndex, record, node);
			            }
	                }]
	            }
			],
			});

			this.callParent(arguments);


		}	
       ,features: [{
	        ftype: 'grouping',
	        groupHeaderTpl: '{name} ({rows.length} รายการ)',
	        hideGroupedHeader: true,
	        startCollapsed: false
	        // ,
	        // id: 'restaurantGrouping'
	    }]
	});