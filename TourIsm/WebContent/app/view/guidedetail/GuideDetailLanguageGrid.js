Ext.define('tourism.view.guidedetail.GuideDetailLanguageGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.window_guidedetail_language_grid',
		requires: [
			'Ext.grid.Column',
			'tourism.store.guidedetail.grid.GuideForeignLanguageStore',
			'Ext.toolbar.Paging',
			'Ext.grid.plugin.RowEditing',
			'Ext.form.field.Number',
			'Ext.form.field.Date'
		],
		
		
		initComponent: function(){

			var store = Ext.create('tourism.store.guidedetail.grid.GuideForeignLanguageStore',{
				  storeId: 'window-guidedetail-foreignlanguage-store',
				  id: 'window-guidedetail-foreignlanguage-store-id'
			  });	

			// store.load();
			
		  var storeLanguage = Ext.create('tourism.store.combo.LanguageStore',{
			  storeId: 'window-guidedetail-language-store'
		  });
		  
		  var storeLanguageLevel = Ext.create('tourism.store.guidedetail.combo.LanguageLevelStore',{
			  storeId: 'window-guidedetail-languagelevel-store'
		  });
		  
		  storeLanguage.load();

			Ext.apply(this,{
				store: store,
//				selType : 'rowmodel',
				height: 200,
				plugins:[
				{
					ptype: 'rowediting',
					pluginId: 'rowediting-grid-packagegrid',
					clicksToEdit: 1,
					autoCancel: false,
					listeners: {
						edit: function(editor, context, eOpts) {
							context.grid.getStore().sync();
						}
					}
				}
				], 
				columns: [	
				
				{
					xtype: 'rownumberer'
				},
				{
	                header: 'ภาษา',
	                dataIndex: 'countryId',
	                flex: 3,
	                editor: 
	                {
	                	xtype: 'combo',
	                	id: 'language_grid_country_combo',
	                    typeAhead: true,
	                    triggerAction: 'all',
	                    valueField: 'countryId',
	                    displayField: 'language',
	                    store: storeLanguage,
	                    allowBlank: false
                	}
                	,
                	renderer: function(value, metaData, gridmodel, rowIndex,colIndex,gridstore,gridview){
                		var language = value;
                		storeLanguage.each(function(lang){
                			if(lang.get('countryId') == value)
                			{
                				language = lang.get('language');
                			}
                		});
				        return language;
				    }
	            },
				{
	                header: 'ระดับความสามารถ',
	                dataIndex: 'languageLevel',
	                flex: 3,
	                editor: 
	                {
	                	xtype: 'combo',
	                	id: 'languagelevel_grid_combo',
	                    typeAhead: true,
	                    triggerAction: 'all',
	                    valueField: 'languageLevel',
	                    displayField: 'languageLevelName',
	                    store: storeLanguageLevel,
	                    allowBlank: false
                	}
                	,
                	renderer: function(value, metaData, gridmodel, rowIndex,colIndex,gridstore,gridview){
                		var language = value;
                		storeLanguageLevel.each(function(lang){
                			if(lang.get('languageLevel') == value)
                			{
                				language = lang.get('languageLevelName');
                			}
                		});
				        return language;
				    }
	            }
				, {
	                xtype: 'actioncolumn',
	                header: 'ลบ',
	                sortable: false,
	                menuDisabled: true,
	                flex: 1,
	                items: [{
//	                    icon: 'resources/images/icons/fam/delete.gif',
	                	iconCls: 'icon-delete-xsmall',
	                    tooltip: 'Language-Remove',
	                    scope: this,
	                    handler: this.onRemoveClick
	                }]
	            }
				],
				tbar: [{
					text: 'เพิ่มความสามารถใช้ภาษาต่างประเทศ',
                	scope: this,
                	handler: function(btn){
				        var form = btn.up('form');
				    	var guideId = form.getForm().findField('guideId').getValue();
				    	console.log(guideId);
						grid = btn.up('grid');
				        var  rowEditing = grid.getPlugin('rowediting-grid-packagegrid');
				        rowEditing.cancelEdit();
				        
				        var rec = Ext.create("tourism.model.guidedetail.GuideForeignLanguageModel",{
				        	guideId: guideId
				        	,countryId: ' '
				        	,languageLevel: ' '
				        });
				        
				        grid.getStore().insert(0, rec);
				        rowEditing.startEdit(0,0);
				        
				        //default language
				        var countryCombo = Ext.getCmp('language_grid_country_combo');
				        countryCombo.getStore().on('load',function(store, records, successful, eOpts){
				        	countryCombo.setValue(41);
		                    },this,{single:true});
				        countryCombo.getStore().load();
				        
				        //default languageLevel
				        var languageLevelCombo = Ext.getCmp('languagelevel_grid_combo');
				        languageLevelCombo.getStore().on('load',function(store, records, successful, eOpts){
				        	languageLevelCombo.setValue("A");
		                    },this,{single:true});
				        languageLevelCombo.getStore().load();
				        
				        
	                }
				}
				]
				
			});

			this.callParent(arguments);
		},
		
	    onRemoveClick: function(grid, rowIndex){
	        this.getStore().removeAt(rowIndex);
	        this.getStore().sync();
	    }
		
	});














