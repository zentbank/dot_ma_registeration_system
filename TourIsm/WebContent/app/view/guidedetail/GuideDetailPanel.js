Ext.define('tourism.view.guidedetail.GuideDetailPanel', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
        'tourism.view.guidedetail.GuideDetailFormSearch',
        'tourism.view.guidedetail.GuideDetailGrid'
    ],
    
    xtype: 'guidedetail-panel',
    layout: {
        type: 'border'
    },
    items: [
        {
        	xtype: 'form',
        	id:'formSubmit'
        },
        {
        	xtype: 'form',
        	id:'gridSubmit'
        },
        {
        region: 'north',
        xtype: 'panel',
        frame: false,
        border: false,
        items:[{
        	title: 'ค้นหาข้อมูลการรายงานตัวมัคคุเทศก์',
            collapsible: true,   // make collapsible
        	xtype: 'guidedetail_formsearch',
            id: 'guidedetail-formsearch-key',
            frame: false,
            border: false
        }]
    }
    ,{
        region: 'center',
        xtype: 'panel',
        layout: 'fit',
        frame: false,
        border: false,
        items:[{
            title: 'การรายงานตัวมัคคุเทศก',
            xtype: 'guidedetail_grid',
            id: 'guidedetail_grid',
            // padding: '0px 5px 5px 5px',
            autoScroll: true,
            border: false
        }]
    }
    ]
});