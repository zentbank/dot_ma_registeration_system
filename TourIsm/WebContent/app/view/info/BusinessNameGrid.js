Ext.define('tourism.view.info.BusinessNameGrid',{
	extend: 'Ext.grid.GridPanel',
	alias: 'widget.info-business-name-grid',
	stripeRows: true,
	requires: [
		'Ext.grid.Column',
		'Ext.toolbar.Paging',
		'Ext.grid.column.Template',
		'Ext.grid.column.Action',
		'tourism.store.registration.RegistrationStore'
	]
	,initComponent: function(){
		
			var nameStore = Ext.create('tourism.store.registration.RegistrationStore',{

					roleAction: this.roleAction,
					traderTypeName: this.traderTypeName,
					pageSize: this.storepageSize,
				proxy: {
					type: 'ajax',
					actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
					api: {
					    read: 'business/license/trader/readName'
					},
					reader: {
				        type: 'json',
				        // metaProperty: '',
				        root: 'list',
				        // idProperty: 'emailId',
				        totalProperty: 'totalCount',
				        successProperty: 'success',
				        messageProperty: 'message'
				    },
				    writer: {
				        type: 'json',
				        // encode: true,
				        writeAllFields: true, ////just send changed fields
				        // root: 'data',
				        allowSingle: false, //always wrap in an array
				        batch: false
				    },
				    listeners: {
				        exception: function(proxy, response, operation){

				            Ext.MessageBox.show({
				                title: 'REMOTE EXCEPTION',
				                msg: operation.getError(),
				                icon: Ext.MessageBox.ERROR,
				                buttons: Ext.Msg.OK
				            });
				            }
				        }
				} 
				,listeners: {
					beforeload: function( store, operation, eOpts )
				        {
				        	// //console.log(store.baseParam);
				        	// //console.log(store);
				        	// store.proxy.extraParams = {test: 123};

				        	var formId = '#info-'+store.roleAction+'-'+store.traderTypeName+'-formsearch';

				        	var formSearch = Ext.ComponentQuery.query(formId)[0];
				        	var values = formSearch.getValues();
								
				        	 for (field in values) 
						      {
						        if (Ext.isEmpty(values[field])) 
						        {
						          delete values[field];
						        }
						        
						      }
						    store.proxy.extraParams = values;
				        }
					}
				
			});
		
		Ext.apply(this, {
			store: nameStore,
			//selType: 'checkboxmodel',
			//mode: 'SINGLE',
			columns: [
			{
				header: 'traderId',
				dataIndex: 'traderId',
				hidden: true
			},
			{
				header: 'เลขที่ใบอนุญาต',
				dataIndex: 'licenseNo',
				flex: 1
			},
			{
				header: 'ชื่อธุรกิจนำเที่ยว',
				dataIndex: 'traderName',
				flex: 3
			},
			{
				header: 'ประเภทใบอนุญาต',
				dataIndex: 'traderCategoryName',
				flex: 1
			},
			{
				header: 'วันที่ออกใบอนุญาต',
				dataIndex: 'effectiveDate',
				flex: 1
			},
			{
				header: 'วันที่ครบกำหนดใบอนุญาต',
				dataIndex: 'expireDate',
				flex: 1
			},
			]
			,dockedItems:[
			{
				xtype: 'pagingtoolbar',
				store: nameStore,
				dock: 'bottom',
				displayInfo: true

			}
			]
			
		});
		
		this.callParent(arguments);
		
	},
	 plugins: [{
        ptype: 'rowexpander',
        // pluginId: 'rowexpanderTourleader',
        rowBodyTpl : new Ext.XTemplate(
        	'<div>',
        	'<tpl switch="traderType">',
	            '<tpl case="B">',
	                '<p><b>ออกใบอนุญาตให้: {traderOwnerName}</b></p>',
            		'<p><b>ทะเบียนนิติบุคคลเลขที่: {identityNo}</b></p>',
            		'<p><b>ประเภทธุรกิจนำเที่ยว: {traderCategoryName}</b></p>',
	            	'<p><b>ชื่อธุรกิจนำเที่ยว (ภาษาไทย): {traderName}</b></p>',
	                '<p><b>ชื่อธุรกิจนำเที่ยว (ภาษาต่างประเทศ): {traderNameEn}</b> </p>',
	            '<tpl case="G">',
	            	'<p><b>ออกใบอนุญาตให้: {traderOwnerName}</b></p>',
            		'<p><b>หมายเลขบัตรประชาชน: {identityNo}</b></p>',
            		'<p><b>ประเภทมัคคุเทศก์: {traderCategoryName}</b></p>',
	                
	            '<tpl default>',
	            	'<p><b>ออกใบอนุญาตให้: {traderOwnerName}</b></p>',
            		'<p><b>หมายเลขบัตรประชาชน: {identityNo}</b></p>',
            		'<p><b>ประเภทมัคคุเทศก์: {traderCategoryName}</b></p>',
	             
	        '</tpl>',
        		
            '<div>'
        )
    }]
	
});











