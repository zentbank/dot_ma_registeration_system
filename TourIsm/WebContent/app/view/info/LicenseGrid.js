Ext.define('tourism.view.info.LicenseGrid',{
	extend: 'Ext.grid.GridPanel',
	alias: 'widget.info-license-grid',
	stripeRows: true,
	requires: [
		'Ext.grid.Column',
		'Ext.toolbar.Paging',
		'Ext.grid.column.Template',
		'Ext.grid.column.Action',
		'tourism.store.registration.RegistrationStore',
		'tourism.view.information.business.InfoBusinessWindow',
		'tourism.view.information.guide.InfoGuideWindow',
		'tourism.view.information.tourleader.InfoTourleaderWindow'
	]
	,initComponent: function(){
		
			var licenseStore = Ext.create('tourism.store.registration.RegistrationStore',{
			
				proxy: {
					type: 'ajax',
					actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
					api: {
					    read: 'business/license/trader/readAllLicense'
					},
					reader: {
					    type: 'json',
					    root: 'list',
					    totalProperty: 'totalCount',
					    successProperty: 'success',
					    messageProperty: 'message'
					},
					listeners: {
					    exception: function(proxy, response, operation){

					        Ext.MessageBox.show({
					            title: 'REMOTE EXCEPTION',
					            msg: operation.getError(),
					            icon: Ext.MessageBox.ERROR,
					            buttons: Ext.Msg.OK
					        });
					    }
					}
				}
			});
		
		Ext.apply(this, {
			store: licenseStore,
			selType: 'checkboxmodel',
			mode: 'SINGLE',
			columns: [
			{
				header: 'traderId',
				dataIndex: 'traderId',
				hidden: true
			},
			{
				header: 'เลขที่ใบอนุญาต',
				dataIndex: 'licenseNo',
				flex: 1
			},
			{
				header: 'ชื่อ',
				dataIndex: 'traderName',
				flex: 3
			},
			{
				header: 'สถานะใบอนุญาต',
				dataIndex: 'licenseStatus',
				flex: 1,
				renderer: function(value, metaData, model){
					
					/*
					        {"licenseStatus":"N", "licenseStatusName":"ปกติ"},
        {"licenseStatus":"C", "licenseStatusName":"ยกเลิก"},
        {"licenseStatus":"S", "licenseStatusName":"พักใช้"},
        {"licenseStatus":"R", "licenseStatusName":"เพิกถอน"},
//        {"licenseStatus":"T", "licenseStatusName":""},
        {"licenseStatus":"D", "licenseStatusName":"ไม่ผ่านการตรวจสอบ/ยกเลิกการจดทะเบียน"}*/
                    
					if(value == "S")
					{
						return '<span class="tour-fg-color-redLight">พักใช้ใบอนุญาต</span>';
					}
					if(value == "R")
					{
						return '<span class="tour-fg-color-redLight">เพิกถอนใบอนุญาต</span>';
					}
					if(value == "C")
					{
						return '<span class="tour-fg-color-redLight">ยกเลิก</span>';
					}
					if(value == "N")
					{
						return '<span class="tour-fg-color-green">ปกติ</span>';
					}
					if(value == "D")
					{
						return '<span class="tour-fg-color-lighten">ไม่ผ่านการตรวจสอบ/ยกเลิกการจดทะเบียน</span>';
					}
					if(value == "E")
					{
						return '<span class="tour-fg-color-redLight">หมดอายุตามกฎหมาย</span>';
					}
				}
			}
			,{
	        	header: '',
	            xtype: 'actioncolumn',
	           	width:50,
	           	align : 'center',
	            items: [{
	                iconCls: 'icon-edit-xsmall',
	                tooltip: 'รายละเอียด',
		            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
		            	this.fireEvent('itemclick', this, 'viewinfo', grid, rowIndex, colIndex, record, node);
		            }
	            }]
	        }
			]
		
			,dockedItems:[
			{
				xtype: 'pagingtoolbar',
				store: licenseStore,
				dock: 'bottom',
				displayInfo: true

			}
			]
			
		});
		
		this.callParent(arguments);
		
	},
	 plugins: [{
        ptype: 'rowexpander',
        // pluginId: 'rowexpanderTourleader',
        rowBodyTpl : new Ext.XTemplate(
        	'<div>',
        	'<tpl switch="traderType">',
	            '<tpl case="B">',
	                '<p><b>ออกใบอนุญาตให้: {traderOwnerName}</b></p>',
            		'<p><b>ทะเบียนนิติบุคคลเลขที่: {identityNo}</b></p>',
            		'<p><b>ประเภทธุรกิจนำเที่ยว: {traderCategoryName}</b></p>',
	            	'<p><b>ชื่อธุรกิจนำเที่ยว (ภาษาไทย): {traderName}</b></p>',
	                '<p><b>ชื่อธุรกิจนำเที่ยว (ภาษาต่างประเทศ): {traderNameEn}</b> </p>',
	            '<tpl case="G">',
	            	'<p><b>ออกใบอนุญาตให้: {traderOwnerName}</b></p>',
            		'<p><b>หมายเลขบัตรประชาชน: {identityNo}</b></p>',
            		'<p><b>ประเภทมัคคุเทศก์: {traderCategoryName}</b></p>',
	                
	            '<tpl default>',
	            	'<p><b>ออกใบอนุญาตให้: {traderOwnerName}</b></p>',
            		'<p><b>หมายเลขบัตรประชาชน: {identityNo}</b></p>',
            		'<p><b>ประเภทมัคคุเทศก์: {traderCategoryName}</b></p>',
	             
	        '</tpl>',
        		
            '<div>'
        )
    }]
	
});











