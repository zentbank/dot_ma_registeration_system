Ext.define('tourism.view.info.SearchBusinessNameForm' ,{
    extend: 'Ext.form.FormPanel',
    alias : 'widget.info-business-name-formsearch',
    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.Time',
        'Ext.form.CheckboxGroup',
        'Ext.layout.container.HBox',
        'Ext.form.FieldSet',
        'tourism.store.combo.TraderTypeStore'
    ],
    bodyPadding: 10,
    // width: '100%',
    items: [
        {
                xtype: 'container',
                // title: 'Payment',
                layout: 'anchor',
                defaults: {
                    anchor: '100%',
                    labelWidth: 150
                },
                items: [
                        {
                            xtype: 'textfield',
                            name: 'traderName',
                            fieldLabel: 'ชื่อสำนักงานประกอบธุรกิจนำเที่ยว (ภาษาไทย)',
                            labelAlign: 'top',
                            allowBlank: false,
                            tabIndex: 1
                            //afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>'
                        }, {
                            xtype: 'textfield',
                            name: 'traderNameEn',
                            fieldLabel: 'ชื่อสำนักงานประกอบธุรกิจนำเที่ยว (ภาษาต่างประเทศ)',
                            labelAlign: 'top',
                            allowBlank: false,
                            tabIndex: 2
                            //afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>'
                        }
                 
                    
                ]
        },
        {
            xtype: 'toolbar',
            border: false,
            padding: '6px 0 6px 0px',
            items: [{
                xtype: 'button',
                text : 'ค้นหาชื่อใกล้เคียง',
                action: 'searchName',
                tabIndex: 3
            }]
        }

    ]
});    
