Ext.define('tourism.view.info.SearchBusinessNamePanel', {
     extend: 'Ext.container.Container',
     alias : 'widget.info-license-searchbusinessnamepanel',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
        'tourism.view.info.SearchBusinessNameForm',
        'tourism.view.info.BusinessNameGrid'
       
    ],
    initComponent: function(){
    	
	    var formSearch = {
            // collapsible: true,   // make collapsible
            // collapsed : false,
            xtype: 'info-business-name-formsearch',
            id: 'info-'+'searchname'+'-'+'business'+'-formsearch',
            
            padding: '5px 5px 0px 5px',
            frame: false,
            border: false,
            roleAction: 'searchname',
            traderType: 'B',
            traderTypeName: 'business'
		};

    	Ext.apply(this, {
    		layout: {
		        type: 'border'
		    },
		    items: [{
		        region: 'north',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items:[formSearch]
		    },{
		        region: 'center',
		        xtype: 'panel',
		        layout: 'fit',
		        frame: false,
		        border: false,
		        items:[
		        {
		        	//id: 'punishment-suspension-business-grid',
		            xtype: 'info-business-name-grid',
		            id: 'info-'+'searchname'+'-'+'business'+'-grid',
		            padding: '5px 5px 5px 5px',
		            autoScroll: true,
		            // frame: false 
		            border: false,
		            storeId: 'info-'+'searchname'+'-'+'business'+'-store',
		            storepageSize: 10,
		            storeUrl: 'business/license/trader/readName',
		            roleAction: 'searchname',
		            traderType: 'B',
		            traderTypeName: 'business'
		        }
		        ]
		    }]
    	});

    	this.callParent(arguments);
    }
    
});