Ext.define('tourism.view.info.SearchLicenseForm' ,{
    extend: 'Ext.form.FormPanel',
    alias : 'widget.info-license-formsearch',
    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.Time',
        'Ext.form.CheckboxGroup',
        'Ext.layout.container.HBox',
        'Ext.form.FieldSet',
        'tourism.store.combo.TraderTypeStore'
    ],
    bodyPadding: 10,
    // width: '100%',
    items: [
        {
                xtype: 'container',
                // title: 'Payment',
                layout: 'anchor',
                defaults: {
                    anchor: '100%',
                    labelWidth: 150
                },
                items: [
                     {
                        xtype: 'combo',
                        fieldLabel: 'ประเภทใบอนุญาต',
                        store: 'tourism.store.combo.TraderTypeStore',
                        queryMode: 'local',
                        displayField: 'traderTypeName',
                        valueField: 'traderType',
                        hiddenName: 'traderType',
                        name :'traderType',
                        anchor: '50%',
                    },
                    {
                        xtype: 'container',
                        layout: 'hbox',
                        layoutCongig: {
                             pack:'center',
                             align:'middle'
                        },
                        items: [
                            {
                                xtype: 'container',
                                // title: 'Payment',
                                flex: 1,
                                layout: 'anchor',
                                defaults: {
                                    anchor: '100%',
                                    labelWidth: 150
                                },
                                items: [
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'เลขที่ใบอนุญาต',
                                        name: 'licenseNo'
                                    }
                                    ,
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'เลขที่นิติบุคคล/บัตรประชาชน',
                                        // labelWidth: 150,
                                        name: 'identityNo'
                                    }
                                    
                                ]
                            },{
                                xtype: 'splitter'
                            },
                            {
                                xtype: 'container',
                                flex: 1,
                                layout: 'anchor',
                                defaults: {
                                    anchor: '100%',
                                    labelWidth: 200
                                },
                                items: [
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'ชื่อใบอนุญาต(ธุรกิจ)',
                                        // labelWidth: 150,
                                        name: 'traderName'
                                    },
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'ชื่อผู้ขอใบอนุญาต',
                                        // labelWidth: 150,
                                        name: 'firstName'
                                    }
                                   
                                ]
                            }
                        ]
                    }
                    
                ]
        },
        {
            xtype: 'toolbar',
            border: false,
            padding: '6px 0 6px 0px',
            items: [{
                xtype: 'button',
                text : 'ค้นหา',
                action: 'searchLicense'
            }]
        }

    ]
});    
