Ext.define('tourism.view.info.SearchLicensePanel', {
     extend: 'Ext.form.Panel',
     alias : 'widget.info-license-searchlicensepanel',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
        'tourism.view.info.SearchLicenseForm',
        'tourism.view.info.LicenseGrid'
       
    ],
    initComponent: function(){

        Ext.apply(this, {
            layout: {
                type: 'border'
            },
            items: [{
                region: 'north',
                xtype: 'panel',
                frame: false,
                border: false,
                items:[{
                     xtype: 'info-license-formsearch',
                     padding: '5px 5px 0px 5px',
                     frame: false,
                     border: false
                }]
            },{
                region: 'center',
                xtype: 'panel',
                layout: 'fit',
                frame: false,
                border: false,
                items:[{
                    xtype: 'info-license-grid',
                    padding: '0px 5px 5px 5px',
                    autoScroll: true,
                    frame: false ,
                    border: false
                }]
            }]
            ,buttons: [{
                text: 'ดำเนินการต่อ',
                action: 'selectLicense'
            }]                  
        });

 
        this.callParent();
    }
    
});