Ext.define('tourism.view.information.business.InfoBusinessForm', {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.data.*',
        'Ext.form.*',
        'Ext.tab.Tab',
        'tourism.view.information.business.TraderBusinessNameInfo',
        'tourism.view.information.address.TraderAddressGrid',
        'tourism.view.information.committee.CommitteeInfoGrid',
        'tourism.view.information.branch.BranchInfoGrid',
        'tourism.view.information.tradercategory.BusinessCategoryInfoPanel',
        'tourism.view.information.person.BusinessPersonFormInfo'
        
        ,'tourism.view.document.registration.business.DocumentBusinessGrid'
        ,'tourism.view.information.history.HistoryGrid'
        ,'tourism.view.information.suspension.SuspensionGrid'
    ],
    xtype: 'information-businee-formpanel',
    bodyPadding: 5,
    autoScroll: true,
    initComponent: function(){

        Ext.apply(this, {
            width: '100%',
            height: '100%',
            fieldDefaults: {
                labelAlign: 'left',
                msgTarget: 'qtip'
            },

            items: [
                {
                    xtype: 'information-business-tradername-container'
                    ,roleAction : this.roleAction
                    ,registrationType: this.registrationType
                },{
                    xtype: 'tabpanel',
                    plain: true,
                    frame: false,
                    border: false,
                   items: [
                        {
                            title: 'ข้อมูลผู้ประกอบการ',
                            frame: false,
                            border: false,
                            xtype: 'information-person-forminfo'
                                          
                        },
                        {
                            title: 'หลักประกัน',
                            layout: 'fit',
                            frame: false,
                            border: false,
                            items:[
                                {
                                    xtype: 'license-info-refundguarantee-form',
                                    id: 'information-refundguarantee-form-001',
                                    border: false,
                                    frame: false
                                }
                            ]
                        },
                        {
                            title: 'ที่ตั้งสำนักงาน',
                            layout: 'fit',
                            frame: false,
                            border: false,
                           
                            items:[
                                {
                                    xtype: 'information-address-taderaddress-grid',
                                    // id: 'information-address-taderaddress-grid',
                                    border: false
                                }
                            ]
                        },
                        {
                            title: 'รายชื่อกรรมการ',
                            layout: 'anchor',
                            frame: false,
                            border: false,
                           
                            items:[
                                {
                                    xtype: 'information-committee-committeeinfogrid',
                                    border: false
                                 
                                } 
                                ,{
                                    xtype: 'splitter'
                                },
                                {
                                    xtype: 'displayfield',
                                    name: 'committeeName2',
                                    anchor: '100%',
                                    fieldLabel: 'กรรมการผู้มีอำนาจลงนาม',
                                    labelWidth: 170
                                }
                            ]
                        }
                       ,{
                            title: 'ประเภทธุรกิจนำเที่ยว',
                            frame: false,
                            border: false,
                           
                            xtype: 'information-business-category-panel'
                                          
                        },{
                            title: 'สาขา',
                            layout: 'fit',
                            frame: false,
                            border: false,
                           
                            items:[
                                {
                                    xtype: 'information-branch-infogrid',
                                    border: false
                                 
                                }
                            ]
                        }
                        ,{
                            title: 'เรื่องร้องเรียน',
                            layout: 'anchor',
                        	frame: false,
                        	border: false,
                        	items: [
                        	{
//                        		xtype: 'information-suspension-info-grid'
                        		xtype: 'information-suspension-complaint-info-grid'
                        	}
                        	]
                        }
                        ,{
                            title: 'ประวัติการจดทะเบียน',
                            layout: 'anchor',
                        	frame: false,
                        	border: false,
                        	items: [
                        	{
                        		xtype: 'information-history-info-grid'
                        	}
                        	]
                        }
                        ,{
                        	title: 'เอกสารประกอบ',
                            layout: 'fit',
                            frame: false,
                            border: false,
                            items:[
                                {
                              	  xtype: 'document-business-popup-grid',
                                    border: false
                                    
                                    ,roleAction: this.roleAction
                                }
                            ]
                        }
                    ]
                }
        ]   
        });

        this.callParent(arguments);
    }
  
});