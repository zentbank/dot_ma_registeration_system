Ext.define('tourism.view.information.business.InfoBusinessWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.information-window',

    requires: [
        'tourism.view.information.business.InfoBusinessForm'
    ],

    title : 'รายละเอียด',
    layout: 'fit',
    // autoShow: true,
    width: 750,
    height: 500,
    //True to make the window modal and mask everything behind it when displayed
    modal: true,
    // iconCls: 'icon-form',

    initComponent: function() {

        // Ext.apply(this, {

        // });
        this.items = [
                {
                    xtype: 'information-businee-formpanel',
                    id: 'information-business-formpanel',
                    frame: false,
                    border: false,
                    registrationModel: this.registrationModel
                    ,roleAction: this.roleAction
                    ,registrationType: this.registrationType
                }
        ];

        this.callParent(arguments);
    },
    showConutry: function(model)
    {
       var contrygrid = this.down('information-tradercategory-businessplantrip-gridinfo');

      if(!Ext.isEmpty(model.get('traderCategory'))){
        switch(model.get('traderCategory')){
            case '100': 
              // plantripPercontryTxt.show();
              contrygrid.expand();
     
              contrygrid.getStore().load({
                
                  params: {traderId: model.get('traderId'), recordStatus: model.get('traderRecordStatus')},
                  callback: function(records, operation, success) {
                      // //console.log(success);
                  },
                  scope: this
              });  

            break;

            case '200': 
              // plantripPercontryTxt.show();
              contrygrid.expand();
     
              contrygrid.getStore().load({
                
                  params: {traderId: model.get('traderId'), recordStatus: model.get('traderRecordStatus')},
                  callback: function(records, operation, success) {
                      // //console.log(success);
                  },
                  scope: this
              });



            break;

            case '300': 
              // plantripPercontryTxt.hide();
              contrygrid.collapse();

            break;

            case '400': 
              // plantripPercontryTxt.hide();
              contrygrid.collapse();
            break;
        }
      }
    },
    loadFormRecord: function(model)
    {
        var info = this.down('form');

        info.loadRecord(model);

        //Image
        var imageFieldSet = this.down('image');
        // console.log(model);
        imageFieldSet.setSrc("data:image/png;base64,"+model.get("imageFile"));
              
        //console.log(model);
        if(model.get('personType')  == 'I')
        {
          var individualFieldSet = info.down('information-person-individual-personinfo');
          individualFieldSet.expand();
        }
        else
        {
          var corporateFieldSet = info.down('information-person-corporate-personinfo');
          corporateFieldSet.expand();


          if(!Ext.isEmpty(model.get('provinceName')))
          {
            info.getForm().findField('provinceNameCorp').setValue(model.get('provinceName'));
          }

         
          if(!Ext.isEmpty(model.get('traderOwnerName')))
          {
           
             info.getForm().findField('traderOwnerNameCrop').setValue(model.get('traderOwnerName'));
          }
          if(!Ext.isEmpty(model.get('identityNo')))
          {
           
             info.getForm().findField('identityNoCorp').setValue(model.get('identityNo'));
          }
          if(!Ext.isEmpty(model.get('identityDate')))
          {
             info.getForm().findField('identityDateCorp').setValue(model.get('identityDate'));
          }    
        }

        //ที่ตั้งสนง.
        var traderaddressgrid =  info.down('information-address-taderaddress-grid');  
        traderaddressgrid.getStore().load({
            params: {traderId: model.get('traderId'), recordStatus: model.get('traderRecordStatus')},
            callback: function(records, operation, success) {
                // //console.log(success);
            },
            scope: this
        }); 

        //รายชื่อกรรมการ
        var committeeInfoGrid =  info.down('information-committee-committeeinfogrid');
        committeeInfoGrid.getStore().load({
            params: {personId: model.get('personId'), recordStatus: model.get('traderRecordStatus')},
            callback: function(records, operation, success) {
                // //console.log(success);
            },
            scope: this
        });   
        
        //สาขา
        var branchInfoGrid =  info.down('information-branch-infogrid'); 
        branchInfoGrid.getStore().load({
            params: {branchParentId: model.get('traderId'), recordStatus: model.get('traderRecordStatus')},
            callback: function(records, operation, success) {
                // //console.log(success);
            },
            scope: this
        });  

        this.showConutry(model);
        
        //เอกสารประกอบ
        var grid = info.down('document-business-popup-grid');
        grid.getStore().load({
      	  params: {
      		  traderId: model.get('traderId')
      		  ,recordStatus: 'N'
      		  ,licenseType: 'B'
      		  ,registrationType: model.get("registrationType")
      	  },
            callback: function(records, operation, success) {
//                 //console.log(records);
            },
            scope: this
        }); 
        
      //ประวัติการจดทะเบียน
      if(model.get('licenseNo') != "")
 	  {
        var historyinfogrid = info.down('information-history-info-grid');
        historyinfogrid.getStore().load({
        	params: {traderType: model.get('traderType'), licenseNo: model.get('licenseNo')},
        	callback: function(records, operation, success){
        		
        	},
        	scope: this
        });
 	  }
      
//      //console.log(model);
      if(model.get("registrationType") == 'N' && this.roleAction != 'info')
      {
    	  var cmp = info.down('tabpanel').getComponent(5);
    	  info.down('tabpanel').remove(cmp, true);
    	  
    	  // var cmp2 = info.down('tabpanel').getComponent(6);
    	  // info.down('tabpanel').remove(cmp2, true);
      }
      else
 	  {
 	  
	    	//เรื่องร้องเรียน
	          var dataParams = {};
	  	    dataParams.questionType = 'C';
	  	    dataParams.complaintProgress = 'INFO';
	  	    dataParams.licenseNo = model.get('licenseNo');
	  	    dataParams.traderType = model.get('traderType');

	  	    var grid = info.down('information-suspension-complaint-info-grid');
	  	    var store = grid.getStore();
	  	   
	  	     dataParams.page = '1';
	  	     dataParams.start = '0';
	  	     dataParams.limit = store.pageSize;

//	  	    //console.log(dataParams);

	  	      store.load({
	  	          params: dataParams,
	  	          callback: function(records, operation, success) {
	  	            
	  	          },
	  	          scope: this
	  	      }); 
	    	  
	  }

    }

});
