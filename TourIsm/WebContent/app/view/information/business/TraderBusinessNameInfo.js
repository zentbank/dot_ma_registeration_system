Ext.define('tourism.view.information.business.TraderBusinessNameInfo', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.form.FieldSet',
        'Ext.form.FieldContainer',
        'Ext.form.field.VTypes',
        'Ext.data.*',
        'Ext.form.*'
    ],
    xtype: 'information-business-tradername-container',
   initComponent: function(){
  
        Ext.apply(this, {
            // width: 800,
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },
            defaults: {
                    anchor: '100%'
            },
            items: [{
                xtype: 'container',
                // title: 'Payment',
                layout: 'anchor',
                defaults: {
                    anchor: '100%'
                },
                items: [{
                    xtype: 'container',
                    layout: 'hbox',
                    /*
                    top margin is 25px
                    right margin is 50px
                    bottom margin is 75px
                    left margin is 100px
                    */
                    margin: '0 0 5 0',
                    items: [
                        {
                            xtype: 'hiddenfield',
                            name: 'regId'
                        },
                        {
                            xtype: 'hiddenfield',
                            name: 'registrationType'
                        },
                        {
                            xtype: 'displayfield',
                            name: 'registrationNo',
                            // labelAlign: 'left',
                            fieldLabel: 'เลขที่รับเรื่อง',
                            flex: 1
                            ,hidden: (this.roleAction != 'info' && this.registrationType =='N')?false:true
                        },{
                            xtype: 'displayfield',
                            fieldLabel: 'วันที่รับเรื่อง',
                            name: 'registrationDate',
                            flex: 1
                            ,hidden: (this.roleAction != 'info' && this.registrationType =='N')?false:true
                            // ,
                            // margin: '0 0 0 5'
                            // ,value: Ext.Date.add(new Date(), Ext.Date.YEAR, 543), //2554 ณ ปัจจุบัน
                           
                        },{
                            xtype: 'splitter'
                        },{
                            xtype: 'displayfield',
                            name: 'authorityName',
                            fieldLabel: 'ผู้รับเรื่อง',
                            flex: 1,
                            margin: '0 0 0 5'
                            ,hidden: (this.roleAction != 'info' && this.registrationType =='N')?false:true
                        }
                    ]
                },
                {
	                xtype: 'container',
	                // title: 'Payment',
	                layout: 'anchor',
	                defaults: {
	                    anchor: '100%'
	                },
	                items: [{
	                    xtype: 'container',
	                    layout: 'hbox',
	                    margin: '0 0 5 0',
	                    items: [
	                        {
	                            xtype: 'displayfield',
	                            name: 'licenseNo',
	                            // labelAlign: 'left',
	                            fieldLabel: 'เลขที่ใบอนุญาต',
	                            flex: 1
	                            ,hidden: (this.roleAction == 'info' || this.registrationType !='N')?false:true
	                        },{
	                            xtype: 'displayfield',
	                            fieldLabel: 'สถานะใบอนุญาต',
	                            name: 'licenseStatusName',
	                            flex: 1
	                            ,hidden: (this.roleAction == 'info' || this.registrationType !='N')?false:true
	                        }
	                    ]
	                }]
                },
                {
                    xtype: 'hiddenfield',
                    name: 'traderId'
                },
                {
                    xtype: 'hiddenfield',
                    name: 'personId'
                },
                {
                    xtype: 'hiddenfield',
                    name: 'traderType'
                },
                {
                    xtype: 'displayfield',
                    name: 'traderName',
                    labelWidth: 300,
                    fieldLabel: 'ชื่อสำนักงานประกอบธุรกิจนำเที่ยว (ภาษาไทย)',
                    labelAlign: 'left'
                }, {
                    xtype: 'displayfield',
                    name: 'traderNameEn',
                    labelWidth: 300,
                    fieldLabel: 'ชื่อสำนักงานประกอบธุรกิจนำเที่ยว (ภาษาต่างประเทศ)',
                    labelAlign: 'left'
                }, {
                    xtype: 'displayfield',
                    name: 'pronunciationName',
                    labelWidth: 300,
                    fieldLabel: 'ภาษาต่างประเทศอ่านเป็นภาษาไทยว่า',
                    labelAlign: 'left'
                }]
            }]

        
        });
        
        this.callParent(arguments);
    }
});