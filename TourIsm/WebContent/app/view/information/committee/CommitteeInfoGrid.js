Ext.define('tourism.view.information.committee.CommitteeInfoGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.information-committee-committeeinfogrid',
		
		stripeRows: true,
		requires: [
			'Ext.grid.Column',
			'Ext.grid.column.Template',
			'Ext.grid.column.Action',
			'Ext.grid.plugin.RowExpander'
			
		],
		initComponent: function(){

			// create every new form 
			var store = Ext.create('tourism.store.business.registration.committee.CommitteeStore',{
				storeId: 'information-business-committee-store'
			});

			Ext.apply(this, {
				store: store,

				columns: [
					{
						header: 'committeeId',
						dataIndex: 'committeeId',
						hidden: true
					},

					{xtype: 'rownumberer'}
					,{
						header: 'สถานะ',
						//dataIndex: 'committeeStatus',
						//flex: 1,
						xtype: 'actioncolumn',
						width: 50,
			            align : 'center',
			           	// flex: 1,
			            items: [{
			            	getClass: function(v, meta, rec) {          
			                    if (rec.get('committeeStatus') == 'N') {
			                        this.items[0].tooltip = 'ปกติ';
			                        return 'icon-checked-xsmall';
			                    }else if (rec.get('committeeStatus') == 'R') {
									this.items[0].tooltip = 'อยู่ระหว่างยกเลิก';
			                        return 'icon-security-xsmall';
			                    }  else if (rec.get('committeeStatus') == 'S') {
			                        this.items[0].tooltip = 'อยู่ระหว่างพักใช้';
			                        return 'icon-security-xsmall';
			                    }
			                },
			                handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
			                     var progressWin = Ext.create('tourism.view.committeeprogress.CommitteeProgressWindow',{
			                        id: 'approvaleprogress-business-committeeprogress-ProgressWindow'
			                        // ,
			                        // animateTarget: column.el 
			                        
			                       });

			                      	progressWin.on('show', function(win){

				                        var grid = win.down('committeeprogress-grid');
				                      
				                        grid.getStore().load({
				                          params: {committeeIdentityNo: record.get('committeeIdentityNo'),committeePersonType:record.get('committeePersonType')},
				                          callback: function(records, operation, success) {
				                              
				                          },
				                          scope: this
				                        }); 

			                      },this);

			                      progressWin.show();
				            }
			            }]
					}

					// ,{
					// 	header: 'สถานะ',
					// 	dataIndex: 'committeeStatus',
					// 	flex: 1,
					// 	renderer: function(value, metaData, model){
							
					// 		if (!value) {
					// 		    return "";
					// 		}
							
     //                         N=ปกติ
					// 		S=อยู่ระหว่างพักใช้
					// 		R=Revoke เพิกถอน
                            
					// 		if(value == 'N')
					// 		{
					// 			return 'ปกติ';
					// 		}
					// 		if(value == 'S')
					// 		{
					// 			return 'อยู่ระหว่างพักใช้';
					// 		}
					// 		if(value == 'R')
					// 		{
					// 			return 'อยู่ระหว่างเพิกถอน';
					// 		}
							
					// 	}
					// }

					,{
						header: 'ชื่อ-นามสกุล',
						dataIndex: 'committeeName',
						flex: 2,
						renderer: function(value, metaData, model){
							
							var name =  model.get('prefixName') + "" + model.get('committeeName');
							var listName =  model.get('committeeLastname');

							if(model.get('committeePersonType') == 'C')
							{
								listName = listName + model.get('postfixName')
							}

							var committeeName = name +' '+listName;

							return Ext.String.format(
					            '<a href="http://www.ratchakitcha.soc.go.th/RKJ/index/index/announce/search.jsp" target="_blank">{0}</a>',
					            committeeName
					        );
							
													
						}
					},
					{
						header: 'ประเภทบุคคล',
						dataIndex: 'committeePersonType',
						flex: 2,
						renderer: function(value, metaData, model){
							
							if (!value) {
							    return "";
							}
							/*
                           I=Individual บุคคลธรรมดา
							C=Corporation นิติบุคคล
                            */
							if(value == 'I')
							{
								return 'บุคคลธรรมดา';
							}
							if(value == 'C')
							{
								return 'นิติบุคคล';
							}
							
						}
					},
					{
						header: 'ประเภทกรรมการ',
						dataIndex: 'committeeType',
						flex: 2,
						renderer: function(value, metaData, model){
							
							if (!value) {
							    return "";
							}
							/*
                         		ประเภทกรรมการบริษัท
								C=committee กรรมการบริษัท
								S=sign กรรมการผู้มีอำนาจลงนาม
                            */
							if(value == 'C')
							{
								return 'กรรมการบริษัท';
							}
							if(value == 'S')
							{
								return 'กรรมการผู้มีอำนาจลงนาม';
							}
							
						}
					}	
				]
				
			});

			this.callParent();
		},
		  plugins: [{
            ptype: 'rowexpander',
            // pluginId: 'rowexpanderinformation',
            rowBodyTpl : new Ext.XTemplate(
				
				'<tpl if="this.isCommitteeTypeIndividual(committeePersonType)">',
					'<div class="x-container x-container-default"',
					'	style="width: 778px;">',
					'	<span  style="display: table; width: 100%; table-layout: fixed;">',
					'		<div style="display: table-cell; height: 100%; vertical-align: top;" class="">',
					'			<div',
					'				class="x-splitter x-splitter-default x-splitter-horizontal x-unselectable"',
					'				style="width: 778px; height: 5px;" ></div>',
					'			<div class="x-container x-container-default x-box-layout-ct"',
					'				 style="width: 778px;">',
					'				<div  class="x-box-inner "',
					'					role="presentation" style="width: 778px; height: 200px;">',
					'					<div  class="x-box-target"',
					'						style="width: 778px;">',
					'						<fieldset class="x-fieldset x-box-item x-fieldset-default"',
					'							style="border-width: 0px; right: auto; left: 0px; margin: 0px; width: 389px; top: 0px;"',
					'							>',
					'							<div  class="x-fieldset-body "',
					'								style="width: 369px;">',
					'								<span ',
					'									style="display: table; width: 100%; table-layout: fixed;"><div',
					'										',
					'										style="display: table-cell; height: 100%; vertical-align: top;"',
					'										class="">',
					'										<table',
					'											class="x-field x-table-plain x-form-item x-form-type-text x-field-default x-anchor-form-item x-form-dirty"',
					'											cellpadding="0" ',
					'											style="table-layout: fixed; width: 369px;">',
					'											<tbody>',
					'												<tr role="presentation" ',
					'													class="x-form-item-input-row">',
					'													<td role="presentation" ',
					'														style="" valign="top" halign="left" width="155"',
					'														class="x-field-label-cell"><label',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 150px; margin-right: 5px;" unselectable="on">หมายเลขบัตรประชาชน</label></td>',
					'													<td role="presentation" class="x-form-item-body  "',
					'														colspan="2"',
					'														style="width: 100%;"><label',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 150px; margin-right: 5px;" unselectable="on">{committeeIdentityNo}</label></td>',
					'												</tr>',
					'											</tbody>',
					'										</table>',
					'										<table',
					'											class="x-container x-form-fieldcontainer x-table-plain x-form-item x-container-default x-anchor-form-item"',
					'											cellpadding="0"  data-errorqtip=""',
					'											style="table-layout: fixed; width: 369px;">',
					'											<tbody>',
					'												<tr role="presentation" ',
					'													class="x-form-item-input-row">',
					'													<td role="presentation" ',
					'														style="" valign="top" halign="left" width="155"',
					'														class="x-field-label-cell"><label',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 150px; margin-right: 5px;" unselectable="on">ชื่อ-นามสกุล(ไทย)</label></td>',
					'													<td role="presentation" class="x-form-item-body  "',
					'														colspan="2"',
					'														style="width: 100%;"><label',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 150px; margin-right: 5px;" unselectable="on">{prefixName}{committeeName}&nbsp;{committeeLastname}</label></td>',
					'												</tr>',
					'											</tbody>',
					'										</table>',
					'										<table',
					'											class="x-container x-form-fieldcontainer x-table-plain x-form-item x-container-default x-anchor-form-item"',
					'											cellpadding="0"  data-errorqtip=""',
					'											style="table-layout: fixed; width: 369px;">',
					'											<tbody>',
					'												<tr role="presentation" ',
					'													class="x-form-item-input-row">',
					'													<td role="presentation" ',
					'														style="" valign="top" halign="left" width="155"',
					'														class="x-field-label-cell"><label',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 150px; margin-right: 5px;" unselectable="on">ชื่อ-นามสกุล(อังกฤษ)</label></td>',
					'													<td role="presentation" class="x-form-item-body  "',
					'														colspan="2"',
					'														style="width: 100%;"><label',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 150px; margin-right: 5px;" unselectable="on">{prefixNameEn}{committeeNameEn}&nbsp;{committeeLastnameEn}</label></td>',
					'												</tr>',
					'											</tbody>',
					'										</table>',
					'										<table',
					'											class="x-container x-form-fieldcontainer x-table-plain x-form-item x-container-default x-anchor-form-item"',
					'											cellpadding="0"  data-errorqtip=""',
					'											style="table-layout: fixed; width: 369px;">',
					'											<tbody>',
					'												<tr role="presentation" ',
					'													class="x-form-item-input-row">',
					'													<td role="presentation" ',
					'														style="" valign="top" halign="left" width="155"',
					'														class="x-field-label-cell"><label',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 150px; margin-right: 5px;" unselectable="on">เกิดวันที่</label></td>',
					'													<td role="presentation" class="x-form-item-body  "',
					'														colspan="2"',
					'														style="width: 100%;"><label',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 150px; margin-right: 5px;" unselectable="on">{birthDate}</label></td>',
					'												</tr>',
					'											</tbody>',
					'										</table>',
					'										<table',
					'											class="x-container x-form-fieldcontainer x-table-plain x-form-item x-container-default x-anchor-form-item"',
					'											cellpadding="0"  data-errorqtip=""',
					'											style="table-layout: fixed; width: 369px;">',
					'											<tbody>',
					'												<tr role="presentation" ',
					'													class="x-form-item-input-row">',
					'													<td role="presentation" ',
					'														style="" valign="top" halign="left" width="155"',
					'														class="x-field-label-cell"><label',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 150px; margin-right: 5px;" unselectable="on">เพศ</label></td>',
					'													<td role="presentation" class="x-form-item-body  "',
					'														colspan="2"',
					'														style="width: 100%;"><label',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 150px; margin-right: 5px;" unselectable="on">{gender:this.isMale}</label></td>',
					'												</tr>',
					'											</tbody>',
					'										</table>',
					'										<table',
					'											class="x-container x-form-fieldcontainer x-table-plain x-form-item x-container-default x-anchor-form-item"',
					'											cellpadding="0"  data-errorqtip=""',
					'											style="table-layout: fixed; width: 369px;">',
					'											<tbody>',
					'												<tr role="presentation" ',
					'													class="x-form-item-input-row">',
					'													<td role="presentation" ',
					'														style="" valign="top" halign="left" width="155"',
					'														class="x-field-label-cell"><label',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 150px; margin-right: 5px;" unselectable="on">ออกให้ ณ จังหวัด</label></td>',
					'													<td role="presentation" class="x-form-item-body  "',
					'														colspan="2"',
					'														style="width: 100%;"><label',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 150px; margin-right: 5px;" unselectable="on">{provinceName}</label></td>',
					'												</tr>',
					'											</tbody>',
					'										</table>',
					'										<table',
					'											class="x-container x-form-fieldcontainer x-table-plain x-form-item x-container-default x-anchor-form-item"',
					'											cellpadding="0"  data-errorqtip=""',
					'											style="table-layout: fixed; width: 369px;">',
					'											<tbody>',
					'												<tr role="presentation" ',
					'													class="x-form-item-input-row">',
					'													<td role="presentation" ',
					'														style="" valign="top" halign="left" width="155"',
					'														class="x-field-label-cell"><label',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 150px; margin-right: 5px;" unselectable="on">เมื่อวันที่:</label></td>',
					'													<td role="presentation" class="x-form-item-body  "',
					'														colspan="2"',
					'														style="width: 100%;"><label',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 150px; margin-right: 5px;" unselectable="on">{committeeIdentityDate}</label></td>',
					'												</tr>',
					'											</tbody>',
					'										</table>',
					'																				',
					'									</div></span>',
					'							</div>',
					'						</fieldset>',
					'						<fieldset class="x-fieldset x-box-item x-fieldset-default"',
					'							style="border-width: 0px; right: auto; left: 389px; margin: 0px; width: 389px; top: 0px;">',
					'							<div  class="x-fieldset-body "',
					'								style="width: 369px;">',
					'								<span ',
					'									style="display: table; width: 100%; table-layout: fixed;"><div',
					'										',
					'										style="display: table-cell; height: 100%; vertical-align: top;"',
					'										class="">',
					'										<table',
					'											class="x-field x-table-plain x-form-item x-form-type-text x-field-default x-anchor-form-item x-form-dirty"',
					'											cellpadding="0" ',
					'											style="table-layout: fixed; width: 369px;">',
					'											<tbody>',
					'												<tr role="presentation" ',
					'													class="x-form-item-input-row">',
					'													<td role="presentation" ',
					'														style="" valign="top" halign="left" width="175"',
					'														class="x-field-label-cell"><label',
					'														for=""',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 170px; margin-right: 5px;" unselectable="on">&nbsp;</label></td>',
					'													<td role="presentation" class="x-form-item-body  "',
					'														colspan="2"',
					'														style="width: 100%;"><label',
					'														for=""',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 170px; margin-right: 5px;" unselectable="on">&nbsp;</label></td>',
					'												</tr>',
					'											</tbody>',
					'										</table>							',
					'										<table',
					'											class="x-field x-table-plain x-form-item x-form-type-text x-field-default x-anchor-form-item x-form-dirty"',
					'											cellpadding="0" ',
					'											style="table-layout: fixed; width: 369px;">',
					'											<tbody>',
					'												<tr role="presentation" ',
					'													class="x-form-item-input-row">',
					'													<td role="presentation" ',
					'														style="" valign="top" halign="left" width="175"',
					'														class="x-field-label-cell"><label',
					'														for=""',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 170px; margin-right: 5px;" unselectable="on">&nbsp;</label></td>',
					'													<td role="presentation" class="x-form-item-body  "',
					'														colspan="2"',
					'														style="width: 100%;"><label',
					'														for=""',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 170px; margin-right: 5px;" unselectable="on">&nbsp;</label></td>',
					'												</tr>',
					'											</tbody>',
					'										</table>',
					'										<table',
					'											class="x-field x-table-plain x-form-item x-form-type-text x-field-default x-anchor-form-item x-form-dirty"',
					'											cellpadding="0" ',
					'											style="table-layout: fixed; width: 369px;">',
					'											<tbody>',
					'												<tr role="presentation" ',
					'													class="x-form-item-input-row">',
					'													<td role="presentation" ',
					'														style="" valign="top" halign="left" width="175"',
					'														class="x-field-label-cell"><label',
					'														for=""',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 170px; margin-right: 5px;" unselectable="on">&nbsp;</label></td>',
					'													<td role="presentation" class="x-form-item-body  "',
					'														colspan="2"',
					'														style="width: 100%;"><label',
					'														for=""',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 170px; margin-right: 5px;" unselectable="on">&nbsp;</label></td>',
					'												</tr>',
					'											</tbody>',
					'										</table>',
					'										<table',
					'											class="x-field x-table-plain x-form-item x-form-type-text x-field-default x-anchor-form-item x-form-dirty"',
					'											cellpadding="0" ',
					'											style="table-layout: fixed; width: 369px;">',
					'											<tbody>',
					'												<tr role="presentation" ',
					'													class="x-form-item-input-row">',
					'													<td role="presentation" ',
					'														style="" valign="top" halign="left" width="175"',
					'														class="x-field-label-cell"><label',
					'														for=""',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 170px; margin-right: 5px;" unselectable="on">สัญชาติ</label></td>',
					'													<td role="presentation" class="x-form-item-body  "',
					'														colspan="2"',
					'														style="width: 100%;"><label',
					'														for=""',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 170px; margin-right: 5px;" unselectable="on">{committeeNationality}</label></td>',
					'												</tr>',
					'											</tbody>',
					'										</table>',
					'										<table',
					'											class="x-field x-table-plain x-form-item x-form-type-text x-field-default x-anchor-form-item x-form-dirty"',
					'											cellpadding="0" ',
					'											style="table-layout: fixed; width: 369px;">',
					'											<tbody>',
					'												<tr role="presentation" ',
					'													class="x-form-item-input-row">',
					'													<td role="presentation" ',
					'														style="" valign="top" halign="left" width="175"',
					'														class="x-field-label-cell"><label',
					'														for=""',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 170px; margin-right: 5px;" unselectable="on">&nbsp;</label></td>',
					'													<td role="presentation" class="x-form-item-body  "',
					'														colspan="2"',
					'														style="width: 100%;"><label',
					'														for=""',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 170px; margin-right: 5px;" unselectable="on">&nbsp;</label></td>',
					'												</tr>',
					'											</tbody>',
					'										</table>',
					'										<table',
					'											class="x-field x-table-plain x-form-item x-form-type-text x-field-default x-anchor-form-item x-form-dirty"',
					'											cellpadding="0" ',
					'											style="table-layout: fixed; width: 369px;">',
					'											<tbody>',
					'												<tr role="presentation" ',
					'													class="x-form-item-input-row">',
					'													<td role="presentation" ',
					'														style="" valign="top" halign="left" width="175"',
					'														class="x-field-label-cell"><label',
					'														for=""',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 170px; margin-right: 5px;" unselectable="on">เขตพื้นที่</label></td>',
					'													<td role="presentation" class="x-form-item-body  "',
					'														colspan="2"',
					'														style="width: 100%;"><label',
					'														for=""',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 170px; margin-right: 5px;" unselectable="on">{amphurName}</label></td>',
					'												</tr>',
					'											</tbody>',
					'										</table>',
					'										',
					'										<div',
					'											class="x-splitter x-splitter-default x-splitter-horizontal x-unselectable"',
					'											style="width: 369px; height: 5px;" ></div>',
					'									</div></span>',
					'							</div>',
					'						</fieldset>',
					'					</div>',
					'				</div>',
					'			</div>',
					'		</div></span>',
					'</div>',
				'</tpl>',
				'<tpl if="this.isCommitteeTypeCorporate(committeePersonType)">',
					'<div class="x-container x-container-default"',
					'	style="width: 778px;">',
					'	<span  style="display: table; width: 100%; table-layout: fixed;">',
					'		<div style="display: table-cell; height: 100%; vertical-align: top;" class="">',
					'			<div',
					'				class="x-splitter x-splitter-default x-splitter-horizontal x-unselectable"',
					'				style="width: 778px; height: 5px;" ></div>',
					'			<div class="x-container x-container-default x-box-layout-ct"',
					'				 style="width: 778px;">',
					'				<div  class="x-box-inner "',
					'					role="presentation" style="width: 778px; height: 180px;">',
					'					<div  class="x-box-target"',
					'						style="width: 778px;">',
					'						<fieldset class="x-fieldset x-box-item x-fieldset-default"',
					'							style="border-width: 0px; right: auto; left: 0px; margin: 0px; width: 389px; top: 0px;"',
					'							>',
					'							<div  class="x-fieldset-body "',
					'								style="width: 369px;">',
					'								<span ',
					'									style="display: table; width: 100%; table-layout: fixed;"><div',
					'										',
					'										style="display: table-cell; height: 100%; vertical-align: top;"',
					'										class="">',
					'										<table',
					'											class="x-field x-table-plain x-form-item x-form-type-text x-field-default x-anchor-form-item x-form-dirty"',
					'											cellpadding="0" ',
					'											style="table-layout: fixed; width: 369px;">',
					'											<tbody>',
					'												<tr role="presentation" ',
					'													class="x-form-item-input-row">',
					'													<td role="presentation" ',
					'														style="" valign="top" halign="left" width="155"',
					'														class="x-field-label-cell"><label',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 150px; margin-right: 5px;" unselectable="on">ชื่อนิติบุคคล</label></td>',
					'													<td role="presentation" class="x-form-item-body  "',
					'														colspan="2"',
					'														style="width: 100%;"><label',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 150px; margin-right: 5px;" unselectable="on">{prefixName}&nbsp;{committeeName}&nbsp;{postfixName}</label></td>',
					'												</tr>',
					'											</tbody>',
					'										</table>',
					'										<table',
					'											class="x-container x-form-fieldcontainer x-table-plain x-form-item x-container-default x-anchor-form-item"',
					'											cellpadding="0"  data-errorqtip=""',
					'											style="table-layout: fixed; width: 369px;">',
					'											<tbody>',
					'												<tr role="presentation" ',
					'													class="x-form-item-input-row">',
					'													<td role="presentation" ',
					'														style="" valign="top" halign="left" width="155"',
					'														class="x-field-label-cell"><label',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 150px; margin-right: 5px;" unselectable="on">ทะเบียนนิติบุคคลเลขที่</label></td>',
					'													<td role="presentation" class="x-form-item-body  "',
					'														colspan="2"',
					'														style="width: 100%;"><label',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 150px; margin-right: 5px;" unselectable="on">{committeeIdentityNo}</label></td>',
					'												</tr>',
					'											</tbody>',
					'										</table>',
					'										<table',
					'											class="x-container x-form-fieldcontainer x-table-plain x-form-item x-container-default x-anchor-form-item"',
					'											cellpadding="0"  data-errorqtip=""',
					'											style="table-layout: fixed; width: 369px;">',
					'											<tbody>',
					'												<tr role="presentation" ',
					'													class="x-form-item-input-row">',
					'													<td role="presentation" ',
					'														style="" valign="top" halign="left" width="155"',
					'														class="x-field-label-cell"><label',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 150px; margin-right: 5px;" unselectable="on">โดย</label></td>',
					'													<td role="presentation" class="x-form-item-body  "',
					'														colspan="2"',
					'														style="width: 100%;"><label',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 150px; margin-right: 5px;" unselectable="on">{corporateSign}</label></td>',
					'												</tr>',
					'											</tbody>',
					'										</table>',
					'										<table',
					'											class="x-container x-form-fieldcontainer x-table-plain x-form-item x-container-default x-anchor-form-item"',
					'											cellpadding="0"  data-errorqtip=""',
					'											style="table-layout: fixed; width: 369px;">',
					'											<tbody>',
					'												<tr role="presentation" ',
					'													class="x-form-item-input-row">',
					'													<td role="presentation" ',
					'														style="" valign="top" halign="left" width="155"',
					'														class="x-field-label-cell"><label',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 180px; margin-right: 5px;" unselectable="on">ออกให้ ณ สำนักงานทะเบียน</label></td>',
					'													<td role="presentation" class="x-form-item-body  "',
					'														colspan="2"',
					'														style="width: 100%;"><label',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 150px; margin-right: 5px;" unselectable="on">{provinceName}</label></td>',
					'												</tr>',
					'											</tbody>',
					'										</table>',
					'										<table',
					'											class="x-container x-form-fieldcontainer x-table-plain x-form-item x-container-default x-anchor-form-item"',
					'											cellpadding="0"  data-errorqtip=""',
					'											style="table-layout: fixed; width: 369px;">',
					'											<tbody>',
					'												<tr role="presentation" ',
					'													class="x-form-item-input-row">',
					'													<td role="presentation" ',
					'														style="" valign="top" halign="left" width="155"',
					'														class="x-field-label-cell"><label',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 150px; margin-right: 5px;" unselectable="on">เลขประจำตัวผู้เสียภาษีอากร</label></td>',
					'													<td role="presentation" class="x-form-item-body  "',
					'														colspan="2"',
					'														style="width: 100%;"><label',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 150px; margin-right: 5px;" unselectable="on">{taxIdentityNo}</label></td>',
					'												</tr>',
					'											</tbody>',
					'										</table>',
					'										<table',
					'											class="x-container x-form-fieldcontainer x-table-plain x-form-item x-container-default x-anchor-form-item"',
					'											cellpadding="0"  data-errorqtip=""',
					'											style="table-layout: fixed; width: 369px;">',
					'											<tbody>',
					'												<tr role="presentation" ',
					'													class="x-form-item-input-row">',
					'													<td role="presentation" ',
					'														style="" valign="top" halign="left" width="155"',
					'														class="x-field-label-cell"><label',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 150px; margin-right: 5px;" unselectable="on">ออกให้ ณ จังหวัด</label></td>',
					'													<td role="presentation" class="x-form-item-body  "',
					'														colspan="2"',
					'														style="width: 100%;"><label',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 150px; margin-right: 5px;" unselectable="on">{taxProvinceName}</label></td>',
					'												</tr>',
					'											</tbody>',
					'										</table>							',
					'									</div></span>',
					'							</div>',
					'						</fieldset>',
					'						<fieldset class="x-fieldset x-box-item x-fieldset-default"',
					'							style="border-width: 0px; right: auto; left: 389px; margin: 0px; width: 389px; top: 0px;">',
					'							<div  class="x-fieldset-body "',
					'								style="width: 369px;">',
					'								<span ',
					'									style="display: table; width: 100%; table-layout: fixed;"><div',
					'										',
					'										style="display: table-cell; height: 100%; vertical-align: top;"',
					'										class="">',
					'										<table',
					'											class="x-field x-table-plain x-form-item x-form-type-text x-field-default x-anchor-form-item x-form-dirty"',
					'											cellpadding="0" ',
					'											style="table-layout: fixed; width: 369px;">',
					'											<tbody>',
					'												<tr role="presentation" ',
					'													class="x-form-item-input-row">',
					'													<td role="presentation" ',
					'														style="" valign="top" halign="left" width="175"',
					'														class="x-field-label-cell"><label',
					'														for=""',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 170px; margin-right: 5px;" unselectable="on">&nbsp;</label></td>',
					'													<td role="presentation" class="x-form-item-body  "',
					'														colspan="2"',
					'														style="width: 100%;"><label',
					'														for=""',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 170px; margin-right: 5px;" unselectable="on">&nbsp;</label></td>',
					'												</tr>',
					'											</tbody>',
					'										</table>							',
					'										<table',
					'											class="x-field x-table-plain x-form-item x-form-type-text x-field-default x-anchor-form-item x-form-dirty"',
					'											cellpadding="0" ',
					'											style="table-layout: fixed; width: 369px;">',
					'											<tbody>',
					'												<tr role="presentation" ',
					'													class="x-form-item-input-row">',
					'													<td role="presentation" ',
					'														style="" valign="top" halign="left" width="175"',
					'														class="x-field-label-cell"><label',
					'														for=""',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 170px; margin-right: 5px;" unselectable="on">ประเภทนิติบุคคล</label></td>',
					'													<td role="presentation" class="x-form-item-body  "',
					'														colspan="2"',
					'														style="width: 100%;"><label',
					'														for=""',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 170px; margin-right: 5px;" unselectable="on">{corporateType:this.isCorporateType}</label></td>',
					'												</tr>',
					'											</tbody>',
					'										</table>',
					'										<table',
					'											class="x-field x-table-plain x-form-item x-form-type-text x-field-default x-anchor-form-item x-form-dirty"',
					'											cellpadding="0" ',
					'											style="table-layout: fixed; width: 369px;">',
					'											<tbody>',
					'												<tr role="presentation" ',
					'													class="x-form-item-input-row">',
					'													<td role="presentation" ',
					'														style="" valign="top" halign="left" width="175"',
					'														class="x-field-label-cell"><label',
					'														for=""',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 170px; margin-right: 5px;" unselectable="on">&nbsp;</label></td>',
					'													<td role="presentation" class="x-form-item-body  "',
					'														colspan="2"',
					'														style="width: 100%;"><label',
					'														for=""',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 170px; margin-right: 5px;" unselectable="on">&nbsp;</label></td>',
					'												</tr>',
					'											</tbody>',
					'										</table>',
					'										<table',
					'											class="x-field x-table-plain x-form-item x-form-type-text x-field-default x-anchor-form-item x-form-dirty"',
					'											cellpadding="0" ',
					'											style="table-layout: fixed; width: 369px;">',
					'											<tbody>',
					'												<tr role="presentation" ',
					'													class="x-form-item-input-row">',
					'													<td role="presentation" ',
					'														style="" valign="top" halign="left" width="175"',
					'														class="x-field-label-cell"><label',
					'														for=""',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 170px; margin-right: 5px;" unselectable="on">เมื่อวันที่</label></td>',
					'													<td role="presentation" class="x-form-item-body  "',
					'														colspan="2"',
					'														style="width: 100%;"><label',
					'														for=""',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 170px; margin-right: 5px;" unselectable="on">{committeeIdentityDate}</label></td>',
					'												</tr>',
					'											</tbody>',
					'										</table>',
					'										<table',
					'											class="x-field x-table-plain x-form-item x-form-type-text x-field-default x-anchor-form-item x-form-dirty"',
					'											cellpadding="0" ',
					'											style="table-layout: fixed; width: 369px;">',
					'											<tbody>',
					'												<tr role="presentation" ',
					'													class="x-form-item-input-row">',
					'													<td role="presentation" ',
					'														style="" valign="top" halign="left" width="175"',
					'														class="x-field-label-cell"><label',
					'														for=""',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 170px; margin-right: 5px;" unselectable="on">&nbsp;</label></td>',
					'													<td role="presentation" class="x-form-item-body  "',
					'														colspan="2"',
					'														style="width: 100%;"><label',
					'														for=""',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 170px; margin-right: 5px;" unselectable="on">&nbsp;</label></td>',
					'												</tr>',
					'											</tbody>',
					'										</table>',
					'										<table',
					'											class="x-field x-table-plain x-form-item x-form-type-text x-field-default x-anchor-form-item x-form-dirty"',
					'											cellpadding="0" ',
					'											style="table-layout: fixed; width: 369px;">',
					'											<tbody>',
					'												<tr role="presentation" ',
					'													class="x-form-item-input-row">',
					'													<td role="presentation" ',
					'														style="" valign="top" halign="left" width="175"',
					'														class="x-field-label-cell"><label',
					'														for=""',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 170px; margin-right: 5px;" unselectable="on">เขตพื้นที่</label></td>',
					'													<td role="presentation" class="x-form-item-body  "',
					'														colspan="2"',
					'														style="width: 100%;"><label',
					'														for=""',
					'														class="x-form-item-label x-unselectable x-form-item-label-left"',
					'														style="width: 170px; margin-right: 5px;" unselectable="on">{taxAmphurName}</label></td>',
					'												</tr>',
					'											</tbody>',
					'										</table>',
					'										',
					'										<div',
					'											class="x-splitter x-splitter-default x-splitter-horizontal x-unselectable"',
					'											style="width: 369px; height: 5px;" ></div>',
					'									</div></span>',
					'							</div>',
					'						</fieldset>',
					'					</div>',
					'				</div>',
					'			</div>',
					'		</div></span>',
					'</div>',
				'</tpl>',
				{
					
	                isMale: function(v){
	                   

	                    if(v == 'M')
	                    {
	                    	return 'ชาย';
	                    }
	                    else
	                    {
	                    	return 'หญิง';
	                    }
	                    
	                },
	                isCommitteeTypeIndividual: function(v)
	                {
	                	return v == 'I';
	                },
	                isCommitteeTypeCorporate: function(v)
	                {
	                	return v == 'C';
	                },
	                isCorporateType: function(v)
	                {
	                	/*
	                	     {"corporateType":"1", "corporateTypeName":"บริษัทจำกัด"},
        					{"corporateType":"2", "corporateTypeName":"ห้างหุ้นส่วนจำกัด"}*/

	                	if(v=='1')
	                	{
	                		return 'บริษัทจำกัด';
	                	}
	                	if(v=='2')
	                	{
	                		return 'ห้างหุ้นส่วนจำกัด';
	                	}
	                	else
	                	{
	                		return 'ไม่ระบุ';
	                	}
	                }
	            
				}
            )
        }]
});