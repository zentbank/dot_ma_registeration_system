Ext.define('tourism.view.information.educationAndLanguage.EducationInfoGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.information-education-info-grid',
		
		stripeRows: true,
		requires: [
			'Ext.grid.Column',
			'Ext.grid.column.Template',
			'Ext.grid.column.Action',
			'Ext.grid.plugin.RowExpander'
		],
		initComponent: function(){

			// create every new form 
			var store = Ext.create('tourism.store.grid.EducationStore',{
				  storeId: 'information-education-info-store'
			  });
			
			var storeUniversity = Ext.create('tourism.store.combo.MasUniversityStore',{
				  storeId: 'window-registration-university-store'
			});
			
			var storeEducationLevel = Ext.create('tourism.store.combo.MasEducationLevelStore',{
				  storeId: 'window-registration-educationlevel-store'
			});
			storeEducationLevel.load();

			Ext.apply(this, {
				store: store,
				columns: [
					{
						header: 'การศึกษา',
						dataIndex: 'masEducationLevelId',
						flex: 1
						,renderer: function(value, metaData, gridmodel, rowIndex,colIndex,gridstore,gridview){
							var educationLevelName = value;
//							storeEducationLevel.load({
//								params: {masUniversityId: value}
//								,callback: function(records, operation, success){
//									//console.log(records);
//									Ext.Array.forEach(records, function(item){
//										if(item.data.masEducationLevelId == value)
//										{
//											educationLevelName = item.data.educationLevelName;
//										}
//	                            	});
//								}
//								,scope: this
//							});
							
							storeEducationLevel.each(function(edul){
								if(edul.get('masEducationLevelId') == value)
								{
									educationLevelName = edul.get('educationLevelName');
								}
							});
							
							return educationLevelName;
                        }
					}
				]
			});

			this.callParent();
		},
		plugins: [{
            ptype: 'rowexpander',
            rowBodyTpl : new Ext.XTemplate(
            		'<div class="x-container x-container-default"',
    				'	style="width: 778px;">',
    				'	<span  style="display: table; width: 100%; table-layout: fixed;">',
    				'		<div style="display: table-cell; height: 100%; vertical-align: top;" class="">',
    				'			<div',
    				'				class="x-splitter x-splitter-default x-splitter-horizontal x-unselectable"',
    				'				style="width: 778px; height: 5px;" ></div>',
    				'			<div class="x-container x-container-default x-box-layout-ct"',
    				'				 style="width: 778px;">',
    				'				<div  class="x-box-inner "',
    				'					role="presentation" style="width: 778px; height: 120px;">',
    				'					<div  class="x-box-target"',
    				'						style="width: 778px;">',
    				'						<fieldset class="x-fieldset x-box-item x-fieldset-default"',
    				'							style="border-width: 0px; right: auto; left: 0px; margin: 0px; width: 389px; top: 0px;"',
    				'							>',
    				'							<div  class="x-fieldset-body "',
    				'								style="width: 369px;">',
    				'								<span ',
    				'									style="display: table; width: 100%; table-layout: fixed;"><div',
    				'										style="display: table-cell; height: 100%; vertical-align: top;"',
    				'										class="">',
    				'										<table',
    				'											class="x-field x-table-plain x-form-item x-form-type-text x-field-default x-anchor-form-item x-form-dirty"',
    				'											cellpadding="0" ',
    				'											style="table-layout: fixed; width: 369px;">',
    				'											<tbody>',
    				'												<tr role="presentation" ',
    				'													class="x-form-item-input-row">',
    				'													<td role="presentation" ',
    				'														style="" valign="top" halign="left" width="155"',
    				'														class="x-field-label-cell"><label',
    				'														class="x-form-item-label x-unselectable x-form-item-label-left"',
    				'														style="width: 150px; margin-right: 5px;" unselectable="on">ระดับการศึกษา:</label></td>',
    				'													<td role="presentation" class="x-form-item-body  "',
    				'														colspan="2"',
    				'														style="width: 100%;"><label',
    				'														class="x-form-item-label x-unselectable x-form-item-label-left"',
    				'														style="width: 150px; margin-right: 5px;" unselectable="on">{educationLevelName}</label></td>',
    				'												</tr>',
    				
    				'												<tr role="presentation" ',
    				'													class="x-form-item-input-row">',
    				'													<td role="presentation" ',
    				'														style="" valign="top" halign="left" width="155"',
    				'														class="x-field-label-cell"><label',
    				'														class="x-form-item-label x-unselectable x-form-item-label-left"',
    				'														style="width: 150px; margin-right: 5px;" unselectable="on">สถาบัน:</label></td>',
    				'													<td role="presentation" class="x-form-item-body  "',
    				'														colspan="2"',
    				'														style="width: 100%;"><label',
    				'														class="x-form-item-label x-unselectable x-form-item-label-left"',
    				'														style="width: 150px; margin-right: 5px;" unselectable="on">{universityName}</label></td>',
    				'												</tr>',
    				
    				'												<tr role="presentation" ',
    				'													class="x-form-item-input-row">',
    				'													<td role="presentation" ',
    				'														style="" valign="top" halign="left" width="155"',
    				'														class="x-field-label-cell"><label',
    				'														class="x-form-item-label x-unselectable x-form-item-label-left"',
    				'														style="width: 150px; margin-right: 5px;" unselectable="on">วุฒิการศึกษา:</label></td>',
    				'													<td role="presentation" class="x-form-item-body  "',
    				'														colspan="2"',
    				'														style="width: 100%;"><label',
    				'														class="x-form-item-label x-unselectable x-form-item-label-left"',
    				'														style="width: 150px; margin-right: 5px;" unselectable="on">{graduationCourse}</label></td>',
    				'												</tr>',
    				
    				'												<tr role="presentation" ',
    				'													class="x-form-item-input-row">',
    				'													<td role="presentation" ',
    				'														style="" valign="top" halign="left" width="155"',
    				'														class="x-field-label-cell"><label',
    				'														class="x-form-item-label x-unselectable x-form-item-label-left"',
    				'														style="width: 150px; margin-right: 5px;" unselectable="on">หลักสูตร:</label></td>',
    				'													<td role="presentation" class="x-form-item-body  "',
    				'														colspan="2"',
    				'														style="width: 100%;"><label',
    				'														class="x-form-item-label x-unselectable x-form-item-label-left"',
    				'														style="width: 150px; margin-right: 5px;" unselectable="on">{educationMajor}</label></td>',
    				'												</tr>',
    				
    				'												<tr role="presentation" ',
    				'													class="x-form-item-input-row">',
    				'													<td role="presentation" ',
    				'														style="" valign="top" halign="left" width="155"',
    				'														class="x-field-label-cell"><label',
    				'														class="x-form-item-label x-unselectable x-form-item-label-left"',
    				'														style="width: 150px; margin-right: 5px;" unselectable="on">ปีที่สำเร็จการศึกษา:</label></td>',
    				'													<td role="presentation" class="x-form-item-body  "',
    				'														colspan="2"',
    				'														style="width: 100%;"><label',
    				'														class="x-form-item-label x-unselectable x-form-item-label-left"',
    				'														style="width: 150px; margin-right: 5px;" unselectable="on">{graduationYear}</label></td>',
    				'												</tr>',
    				
    				'											</tbody>',
    				'										</table>',
    				'									</div></span>',
    				'							</div>',
    				'						</fieldset>',
    				'					</div>',
    				'				</div>',
    				'			</div>',
    				'		</div></span>',
    				'</div>'

            )
        }]
	});