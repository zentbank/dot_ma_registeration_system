Ext.define('tourism.view.information.educationAndLanguage.LanguageInfoGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.information-language-info-grid',
		store: 'tourism.store.grid.ForeignLanguageStore',
		stripeRows: true,
		requires: [
			'Ext.grid.Column',
			'Ext.grid.column.Template',
			'Ext.grid.column.Action',
			'Ext.grid.plugin.RowExpander'
		],
		initComponent: function(){

			Ext.apply(this, {
				columns: [
					{
						xtype: 'rownumberer'
					},
					{
						header: 'ภาษา',
						dataIndex: 'countryName',
						flex: 1
					}
				]
			});

			this.callParent();
		}

	});