Ext.define('tourism.view.information.guide.InfoGuideWindow',{
	extend: 'Ext.window.Window',
	alias: 'widget.information_guide_window',
	requires: [
	           'tourism.view.information.guide.InfoGuideForm'
	],
	title: 'รายละเอียด',
	layout: 'fit',
	width: 750,
	height: 500,
	//True to make the window modal and mask everything behind it when displayed
    modal: true,
    
    initComponent: function(){
    	
    	this.items =[
    	    {
    	    	xtype: 'information-guide-formpanel',
    	    	id: 'information-guide-formpanel',
                frame: false,
                border: false,
                registrationModel: this.registrationModel
                ,roleAction: this.roleAction
                ,registrationType: this.registrationType
    	    }
    	];
    	
    	this.callParent(arguments);
    },
    loadFormRecord: function(model)
    {
    	
    	 var info = this.down('form');
    	 info.loadRecord(model);

         //Image
        var imageFieldSet = this.down('image');
        // console.log(model);
        imageFieldSet.setSrc("data:image/png;base64,"+model.get("imageFile"));
 
    	//ที่อยู่
         var traderaddressgrid =  info.down('information-address-taderaddress-grid');
         traderaddressgrid.getStore().load({
             params: {traderId: model.get('traderId'), recordStatus: model.get('traderRecordStatus')},
             callback: function(records, operation, success) {
                 // //console.log(records);
             },
             scope: this
         }); 
         
         //การศึกษา
         var educationinfogrid = info.down('information-education-info-grid');
         educationinfogrid.getStore().load({
         	params: {personId: model.get('personId'), educationType: 'E', recordStatus: model.get('traderRecordStatus')},
         	callback: function(records, operation, success){
         		//console.log(records);
         	},
         	scope: this
         });
         
         //ภาษา
         var languageinfogrid = info.down('information-language-info-grid');
         languageinfogrid.getStore().load({
         	params: {personId: model.get('personId'), recordStatus: model.get('traderRecordStatus')},
         	callback: function(records, operation, success){
         		//console.log(records);
         	},
         	scope: this
         });
         
         //การอบรม
         var traininfogrid = info.down('information-train-info-grid');
         traininfogrid.getStore().load({
        	params: {personId: model.get('personId'), educationType: 'T', recordStatus: model.get('traderRecordStatus')},
          	callback: function(records, operation, success){
          		//console.log(records);
          	},
         });
         
         //ประเภทมัคคุเทศก์
         var infotypeguidepanel = info.down('information-typeguide-info-panel');
         var traderCategoryTypeName = "";
         //console.log(model.get('traderCategory'));
         //console.log(model);
         if(model.get('traderCategory') == '100' || model.get('traderCategory') == '101')
 	     {
        	 traderCategoryTypeName = "ทั่วไป"
 	     }else
 	     {
 	    	traderCategoryTypeName = "เฉพาะพื้นที่"
 	     }
         
         infotypeguidepanel.on('afterrender',function(cmp){
         	cmp.loadRecord({

         		traderCategory: model.get('traderCategoryGuideTypeName')
         		,traderCategoryName: model.get('traderCategoryName')
         		,traderProvinceName: model.get('provinceGuideServiceName')
         		,traderArea: model.get('traderArea')
         	});
         });
         
         //เอกสารประกอบ
         var grid = info.down('document-guide-popup-grid');
         grid.getStore().load({
       	  params: {
       		  traderId: model.get('traderId')
       		  ,recordStatus: 'N'
       		  ,licenseType: 'G'
       		  ,registrationType: 'N'
       	  },
             callback: function(records, operation, success) {
//                  //console.log(records);
             },
             scope: this
         });
         
       //ประวัติการจดทะเบียน
       if(model.get('licenseNo') != "")
	   {
		   var historyinfogrid = info.down('information-history-info-grid');
	       historyinfogrid.getStore().load({
	       	params: {traderType: model.get('traderType'), licenseNo: model.get('licenseNo')},
	       	callback: function(records, operation, success){
	     
	       	},
	       	scope: this
	       });
	   }
       

      if(model.get("registrationType") == 'N' && this.roleAction != 'info')
      {
        //เรื่องร้องเรียน
    	  var cmp = info.down('tabpanel').getComponent(4);
    	  info.down('tabpanel').remove(cmp, true);
    	  
    	  // var cmp2 = info.down('tabpanel').getComponent(5);
    	  // info.down('tabpanel').remove(cmp2, true);
      }
      else
 	  {
 	  
	    	//เรื่องร้องเรียน
	          var dataParams = {};
	  	    dataParams.questionType = 'C';
	  	    dataParams.complaintProgress = 'INFO';
	  	    dataParams.licenseNo = model.get('licenseNo');
	  	    dataParams.traderType = model.get('traderType');

	  	    var grid = info.down('information-suspension-complaint-info-grid');
	  	    var store = grid.getStore();
	  	   
	  	     dataParams.page = '1';
	  	     dataParams.start = '0';
	  	     dataParams.limit = store.pageSize;

//	  	    //console.log(dataParams);

	  	      store.load({
	  	          params: dataParams,
	  	          callback: function(records, operation, success) {
	  	              
	  	          },
	  	          scope: this
	  	      }); 
	    	  
	      }
         
         
    }
	
});















