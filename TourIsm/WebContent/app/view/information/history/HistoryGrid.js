Ext.define('tourism.view.information.history.HistoryGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.information-history-info-grid',
//		store: 'tourism.store.grid.ForeignLanguageStore',
		stripeRows: true,
		requires: [
			'Ext.grid.Column',
			'Ext.grid.column.Template',
			'Ext.grid.column.Action',
			'Ext.grid.plugin.RowExpander'
		],
		initComponent: function(){

			var historyinfoStore = Ext.create('tourism.store.registration.RegistrationStore',{
//				storeId: this.storeId,
//				pageSize : this.storepageSize,
				groupField: 'headerInfo',
				proxy: {
					type: 'ajax',
					actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
					api: {
					    read: 'business/info/read'
					},
					reader: {
					    type: 'json',
					    root: 'list',
					    totalProperty: 'totalCount',
					    successProperty: 'success',
					    messageProperty: 'message'
					},
					listeners: {
					    exception: function(proxy, response, operation){

					        Ext.MessageBox.show({
					            title: 'REMOTE EXCEPTION',
					            msg: operation.getError(),
					            icon: Ext.MessageBox.ERROR,
					            buttons: Ext.Msg.OK
					        });
					    }
					}
				}
			});
			
			Ext.apply(this, {
				store: historyinfoStore
				,columns: [
//					{
//						xtype: 'rownumberer'
//					},
					{
						header: 'regId',
						dataIndex: 'regId',
						hidden: true
					},
					{
						header: 'licenseNo',
						dataIndex: 'licenseNo',
						hidden: true					
					},
					{
						header: 'ประวัติการจดทะเบียน',
						dataIndex: 'registrationTypeName',
						flex: 1
					}
					,{
						header: 'วันที่ออกใบอนุญาต',
						dataIndex: 'effectiveDate',
						flex: 1
					}
					,{
						header: 'วันครบกำหนดชำระค่าธรรมเนียม',
						dataIndex: 'expireDate',
						flex: 1
					}
					,{
						header: 'สถานะใบอนุญาต',
						dataIndex: 'licenseStatusName',
						flex: 1
					}
				]
			});

			this.callParent();
		}

	});