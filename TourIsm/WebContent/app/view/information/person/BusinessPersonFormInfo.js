Ext.define('tourism.view.information.person.BusinessPersonFormInfo', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.form.FieldSet',
        'Ext.form.FieldContainer',
        'Ext.form.field.VTypes',
        'tourism.view.information.person.IndividualPersonInfo',
        'tourism.view.information.person.CorporatePersonInfo'
        
    ],
    xtype: 'information-person-forminfo',
   initComponent: function(){

        Ext.apply(this, {
            // width: 800,
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },
            defaults: {
                    anchor: '100%'
            },
            items: [
                {
                    xtype:'information-person-individual-personinfo'
                   
                },
                {
                    xtype: 'information-person-corporate-personinfo'
                }
            ]

        
        });
        
        this.callParent();
    }
});