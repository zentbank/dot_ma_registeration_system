Ext.define('tourism.view.information.person.CorporatePersonInfo', {
    extend: 'Ext.form.FieldSet',
    requires:[
        'Ext.form.FieldContainer',
        'Ext.form.field.VTypes',
        'Ext.form.*'
    ],
    
    xtype: 'information-person-corporate-personinfo',

     initComponent: function(){

 Ext.apply(this, {
            checkboxToggle:true,
            checkboxName : 'personTypeCorporate',
            title: 'นิติบุคคล',
            defaultType: 'textfield',
            collapsed: true,
            layout: 'anchor',
            defaults: {
                anchor: '100%'
                
            },
            items :
            [
                {
                    // left hand side
                    xtype: 'container',
                    layout: 'anchor',
                    flex: 1,
                    defaults: {
                        anchor: '100%'
                    },
                    items:[

                        {
                            // ข้าพเจ้า
                            xtype: 'fieldcontainer',
                            
                            fieldLabel: 'ชื่อนิติบุคคล',
                            labelWidth: 200,
                            // labelWidth: 100,
                            layout: 'hbox',
                            items: [
                                {
                                    xtype: 'displayfield',
                                    name: 'traderOwnerNameCrop',
                                    renderer: function(value, dispField){
                                        return Ext.String.format(
                                            '<a href="http://datawarehouse.dbd.go.th/bdw/search/search1.html" target="_blank">{0}</a>',
                                            value
                                        );                 
                                    }
                                   
                                }
                               
                            ]
                        }// end ข้าพเจ้า
                    ]
                }// end left hand side
                ,{
                    xtype: 'container',
                    layout: 'hbox',
                    layoutCongig: {
                         pack:'center',
                         align:'middle'
                    },
                    items:
                    [
                        {
                            // ทะเบียนนิติบุคคลเลขที่
                            xtype: 'displayfield',
                            labelWidth: 200,
                            flex: 1,
                            fieldLabel: 'ทะเบียนนิติบุคคลเลขที่',
                            name: 'identityNoCorp'
                           
                        }// end // ทะเบียนนิติบุคคลเลขที่,
                        ,{
                            xtype: 'splitter'
                        }
                        ,{
                            xtype: 'displayfield',
                            labelWidth: 200,
                            flex: 1,
                            fieldLabel: 'ประเภทนิติบุคคล',
                            name: 'corporateType',
                             renderer: function(v, metaData, model){
                                if(v=='1')
                                {
                                    return 'บริษัทจำกัด';
                                }
                                if(v=='2')
                                {
                                    return 'ห้างหุ้นส่วนจำกัด';
                                }
                                else
                                {
                                    return 'ไม่ระบุ';
                                }
                             }
                        }
                    ]
                }
                ,{
                    // โดย
                    xtype: 'displayfield',
                    fieldLabel: 'โดย',
                    labelWidth: 200,
                    tabIndex: 20,
                    name: 'committeeNameSign'
                    // ,
                    // allowBlank: false,
                    // afterLabelTextTpl: required

                }// end โดย
                ,{
                    xtype: 'container',
                    layout: 'hbox',
                    layoutCongig: {
                         pack:'center',
                         align:'middle'
                    },
                    items:
                    [                       
                        {
                            xtype: 'displayfield',
                            labelWidth: 200,
                            flex: 1,
                            fieldLabel: 'ออกให้ ณ สำนักงานทะเบียน',
                            name: 'provinceNameCorp'
                           
                            
                        }// end ออกให้โดยสรรพากรจังหวัด
                        , {
                            xtype: 'splitter'
                        }
                        ,{
                            // เมื่อวันที่
                            xtype: 'displayfield',
                            labelWidth: 200,
                            flex: 1,
                            fieldLabel: 'เมื่อวันที่',
                            name: 'identityDateCorp'

                        }// end เมื่อวันที่
                        
                    ]
                }
                ,{
                    // เลขประจำตัวผู้เสียภาษีอากร
                    xtype: 'displayfield',
                    labelWidth: 200,
                    fieldLabel: 'เลขประจำตัวผู้เสียภาษีอากร',
                    name: 'taxIdentityNo'
                }// end ใบทะเบียนภาษีมูลค่าเพิ่ม (ถ้ามี) หรือเลขประจำตัวผู้เสียภาษีอากร
                ,{
                    // ออกให้ ณ จังหวัดหรือเขตพื้นที่
                    xtype: 'container',
                    layout: 'hbox',
                    layoutCongig: {
                         pack:'center',
                         align:'middle'
                    },
                    items:
                    [
                        {
                            // left hand side
                            xtype: 'container',
                            layout: 'anchor',
                            flex: 1,
                            defaults: {
                                anchor: '100%'
                            },
                            items:[
                                {
                                    xtype: 'displayfield',
                                    labelWidth: 200,
                                    fieldLabel: 'ออกให้ ณ จังหวัด',
                                    name: 'taxProvinceName'
                                   
                                    
                                }// end ออกให้โดยสรรพากรจังหวัด
                            ]
                        }// end left hand side
                        ,
                        {
                            xtype: 'splitter'
                        }
                        ,{
                            // right hand site
                            xtype: 'container',
                            layout: 'anchor',
                            flex: 1,
                            defaults: {
                                anchor: '100%'
                            },
                            items:[
                                {
                                    xtype: 'displayfield',
                                    labelWidth: 200,
                                    fieldLabel: 'เขตพื้นที่',
                                    name: 'taxAmphurName'
                                   
                                }// end เขตพื้นที่ 
                            ]
                        }// right hand site
                    ]
                }// end // ออกให้ ณ จังหวัดหรือเขตพื้นที่
            
            ]
         });

        this.callParent(arguments);
     }

});