Ext.define('tourism.view.information.person.IndividualPersonInfo', {
    extend: 'Ext.form.FieldSet',
    requires:[
        'Ext.form.FieldContainer',
        'Ext.form.field.VTypes',
        'Ext.form.*'
    ],
    
    xtype: 'information-person-individual-personinfo',

     initComponent: function(){

      

         Ext.apply(this, {
            checkboxToggle:true,
            checkboxName : 'personTypeIndividual',
            title: 'บุคคลธรรมดา',
            defaultType: 'textfield',
            collapsed: true,
            layout: 'anchor',
            defaults: {
                anchor: '100%'
            },
            items :
            [

                {
                    // หมายเลขบัตรประชาชนเลขที่
                    xtype: 'displayfield',
                    labelWidth: 200,
                    name: 'identityNo',
                    fieldLabel: 'หมายเลขบัตรประชาชน'
                    
                   
                },
                {
                    xtype:'container',
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%'
                    },
                    items:
                    [
                        {
                            
                            xtype: 'container',
                            layout: 'hbox',
                            layoutCongig: {
                                 pack:'center',
                                 align:'middle'
                            },
                            items: 
                            [
                                {
                                    // left hand side
                                    xtype: 'container',
                                    layout: 'anchor',
                                    flex: 1,
                                    defaults: {
                                        anchor: '100%'
                                    },
                                    items:
                                    [
                                        {
                                            // ชื่อ
                                            xtype: 'fieldcontainer',
                                            labelWidth: 200,
                                            fieldLabel: 'ชื่อ(ไทย)',
                                            layout: 'hbox',
                                            items: [
                                                {
                                                    xtype: 'displayfield',
                                                    name: 'traderOwnerName',
                                                    flex: 1,
                                                    renderer: function(value, dispField){
                                                        return Ext.String.format(
                                                            '<a href="http://www.ratchakitcha.soc.go.th/RKJ/index/index/announce/search.jsp" target="_blank">{0}</a>',
                                                            value
                                                        );                 
                                                    }
                                                   
                                                }
                                                // {
                                                //     xtype: 'displayfield',
                                                //     name: 'prefixName',
                                                //     flex: 1,
                                                //     renderer: function(value, dispField){
                                                //         return Ext.String.format(
                                                //             '<a href="http://www.ratchakitcha.soc.go.th/RKJ/index/index/announce/search.jsp" target="_blank">{0}</a>',
                                                //             value
                                                //         );                 
                                                //     }
                                                   
                                                // }, {
                                                //     xtype: 'displayfield',
                                                //     name: 'firstName',
                                                //     flex: 2,
                                                //     renderer: function(value, dispField){
                                                //         return Ext.String.format(
                                                //             '<a href="http://www.ratchakitcha.soc.go.th/RKJ/index/index/announce/search.jsp" target="_blank">{0}</a>',
                                                //             value
                                                //         );                 
                                                //     }
                                                   
                                                // }
                                            ]
                                        },
                                        {
                                            // ชื่อ
                                            xtype: 'fieldcontainer',
                                            labelWidth: 200,
                                            fieldLabel: 'ชื่อ(อังกฤษ)',
                                            // labelWidth: 100,
                                            layout: 'hbox',
                                            items: [
                                                {
                                                    xtype: 'displayfield',
                                                    name: 'prefixNameEn',
                                                    flex: 1
                                                }, {
                                                    xtype: 'displayfield',
                                                    name: 'firstNameEn',
                                                    flex: 2
                                                }
                                            ]
                                        },
                                        {
                                            // เกิดวันที่
                                            xtype: 'displayfield',
                                            fieldLabel: 'เกิดวันที่',
                                            labelWidth: 200,
                                            name: 'birthDate'
                                        }// end เกิดวันที่
                                        
                                    ]
                                }//end left hand side
                                , {
                                    xtype: 'splitter'
                                }
                                ,{
                                    // right hand side
                                    xtype: 'container',
                                    layout: 'anchor',
                                    flex: 1,
                                    defaults: {
                                        anchor: '100%'
                                    },
                                    items:
                                    [
                                        {
                                            // นามสกุล
                                            xtype: 'displayfield',
                                            name: 'lastName',
                                            fieldLabel: 'นามสกุล(ไทย)',
                                            labelWidth: 200
                                        }// end  นามสกุล
                                        ,{
                                            // นามสกุล
                                            xtype: 'displayfield',
                                            name: 'lastNameEn',
                                            fieldLabel: 'นามสกุล(อังกฤษ)',
                                            labelWidth: 200
                                        }
                                        ,{
                                            // สัญชาติ
                                            xtype: 'displayfield',
                                            name: 'personNationality',
                                            fieldLabel: 'สัญชาติ',
                                            labelWidth: 200
                                        }// end  สัญชาติ
                                                                           
                                    ]
                                } // end right hand side
                            ]
                        }
                        ,{
                            xtype: 'displayfield',
                            fieldLabel: 'เพศ',
                            labelWidth: 200,
                            name: 'gender',
                            renderer: function(v){
                                if(v=='M')
                                {
                                    return 'ชาย';
                                }
                                if(v=='W')
                                {
                                    return 'หญิง';
                                }
                            }
                        }
                        ,{
                            
                            xtype: 'container',
                            layout: 'hbox',
                            layoutCongig: {
                                 pack:'center',
                                 align:'middle'
                            },
                            items: 
                            [
                                {
                                    // left hand side
                                    xtype: 'container',
                                    layout: 'anchor',
                                    flex: 1,
                                    defaults: {
                                        anchor: '100%'
                                    },
                                    items:
                                    [
                                        {
                                            xtype: 'displayfield',
                                            fieldLabel: 'ออกให้ ณ จังหวัด',
                                            labelWidth: 200,
                                            name:'provinceName'
                                         
                                            
                                        }// end ออกให้โดยสรรพากรจังหวัด
                                        ,{
                                            // เมื่อวันที่
                                            xtype: 'displayfield',
                                            fieldLabel: 'เมื่อวันที่',
                                            labelWidth: 200,
                                            name: 'identityDate'
                                            // ,
                                            // allowBlank: false,
                                            // afterLabelTextTpl: required

                                        }// end เมื่อวันที่
                                    ]
                                }//end left hand side
                                , {
                                    xtype: 'splitter'
                                }
                                ,{
                                    // right hand side
                                    xtype: 'container',
                                    layout: 'anchor',
                                    flex: 1,
                                    defaults: {
                                        anchor: '100%'
                                    },
                                    items:
                                    [
                                       {
                                            xtype: 'displayfield',
                                            fieldLabel: 'เขตพื้นที่',
                                            labelWidth: 200,
                                            name: 'amphurName'
                                           
                                        }// end เขตพื้นที่ 
                                        ,{
                                            xtype: 'fieldset',
                                            itemId: 'imageWrapper',
                                            layout: 'anchor',
                                            items: [{
                                                xtype: 'image',
                                                itemId: 'imageFile',
                                                margin: '0 90px 0',
                                                listeners:{
                                                    afterrender:function(img, a, obj){
                                                        img.getEl().dom.style.width = '130px'; 
                                                        img.getEl().dom.style.height = '150px'; 
                                                    }
                                                }
                                            }]
                                        }
                                    ]
                                } // end right hand side

                            ]
                        }
                    ]
                        
                }
            ]
         });

        this.callParent(arguments);
     }

});