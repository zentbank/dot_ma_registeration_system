Ext.define('tourism.view.information.suspension.SuspensionComplaintGrid',{
	extend: 'Ext.grid.GridPanel',
	alias: 'widget.information-suspension-complaint-info-grid',
	stripeRows: true,
	requires: [
		'Ext.grid.Column',
		'Ext.toolbar.Paging',
		'Ext.grid.plugin.RowEditing',
		'Ext.grid.column.Template',
		'Ext.grid.column.Action',
		'Ext.grid.plugin.RowExpander',
		'Ext.grid.feature.Grouping'
		,'tourism.store.grid.ComplaintLicenseStore'
//		,'tourism.view.complaint.viewcomplaint.ComplaintInfoWindow'
//		,'tourism.view.complaint.addcomplaint.ComplaintAddEditWindow'
	]
	,initComponent: function(){
		
		var complaintLicenseStore = Ext.create('tourism.store.grid.ComplaintLicenseStore',{
			storeId: this.storeId
		});
		
		Ext.apply(this, {
			store: complaintLicenseStore
			,columns: [
			{
				header: 'regId',
				dataIndex: 'regId',
				hidden: true
			},
			{
				header: 'เลขที่ใบอนุญาต',
				dataIndex: 'licenseNo',
				flex: 1
			},
			{
				header: 'ชื่อผู้ถูกร้องเรียน',
				dataIndex: 'traderName',
				flex: 2
			},
			{
				header: 'ชื่อผู้ร้องเรียน',
				dataIndex: 'complaintName',
				flex: 2
			},
			{
				header: 'วันที่ร้องเรียน',
				dataIndex: 'complaintDate',
				flex: 1
			},
			{
				header: 'สถานะ',
				dataIndex: 'complaintStatusName',
				flex: 1
			}
			, {
	        	header: '',
	            xtype: 'actioncolumn',
	           	width:50,
	           	align : 'center',
	            items: [{
	                iconCls: 'icon-notes-xsmall',
	                tooltip: 'รายละเอียด',
		            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
//		            	this.fireEvent('itemclick', this, 'viewcomplaintreceive', grid, rowIndex, colIndex, record, node);
		            	
		            	var gridf = grid.up('grid');
			        	
			        	var win = Ext.create('tourism.view.complaint.viewcomplaint.ComplaintInfoWindow',{
		                    animateTarget: this.el,
		                    actionMode: 'view'
//		                    ,complaintProgress: gridf.complaintProgress
		                }).show();
			        	
			        	//console.log(record);
			        	
			        	win.loadFormRecord(record);
		            	
		            }
	            }]
	        }
			]
		
			,dockedItems:[
			{
				xtype: 'pagingtoolbar',
				store: complaintLicenseStore,
				dock: 'bottom',
				displayInfo: true
			}
			]
			
		});
		
		this.callParent(arguments);
		
	}
	
});











