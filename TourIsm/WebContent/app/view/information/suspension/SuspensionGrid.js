Ext.define('tourism.view.information.suspension.SuspensionGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.information-suspension-info-grid',
		stripeRows: true,
		requires: [
			'Ext.grid.Column',
			'Ext.grid.column.Template',
			'Ext.grid.column.Action',
			'Ext.grid.plugin.RowExpander'
		],
		initComponent: function(){

			var historyinfoStore = '';

			
			Ext.apply(this, {
				store: historyinfoStore
				,columns: [
					{
						header: 'regId',
						dataIndex: 'regId',
						hidden: true
					},
					{
						header: 'licenseNo',
						dataIndex: 'licenseNo',
						hidden: true					
					},
					{
						header: 'เรื่องร้องเรียน',
						dataIndex: 'registrationTypeName',
						flex: 1
					}
					,{
						header: 'วันที่ร้องเรียน',
						dataIndex: 'effectiveDate',
						flex: 1
					}
					,{
						header: 'สถานะ',
						dataIndex: 'licenseStatusName',
						flex: 1
					}
				]
			});

			this.callParent();
		}

	});