Ext.define('tourism.view.information.tourleader.InfoTourleaderForm', {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.data.*',
        'Ext.form.*',
        'Ext.tab.Tab',
        'tourism.view.information.tourleader.TraderTourleaderPersonInfo',
        'tourism.view.information.address.TraderAddressGrid',
        'tourism.view.information.educationAndLanguage.EducationInfoGrid'
        ,'tourism.view.information.educationAndLanguage.LanguageInfoGrid'
        ,'tourism.view.information.type.TypeInfoPanel'
        
        ,'tourism.view.document.registration.tourleader.DocumentTourleaderGrid'
        ,'tourism.view.information.history.HistoryGrid'
//        ,'tourism.view.information.suspension.SuspensionGrid'
        
        ,'tourism.view.information.suspension.SuspensionComplaintGrid'
    ],
    xtype: 'information-tourleader-formpanel',
    // title: 'ธุรกิจนำเที่ยว',
    bodyPadding: 5,
    autoScroll: true,
    initComponent: function(){

        
        Ext.apply(this, {
            width: '100%',
            height: '100%',
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },

            items: [
                {
                    xtype: 'information-tourleader-traderperson-container'
                    ,registrationType: this.registrationType
                },{
                    xtype: 'tabpanel',
                    // width: 400,
                    // height: 200,
                    plain: true,
                    frame: false,
                    border: false,
                   items: [
                        {
                            title: 'ที่อยู่',
                            layout: 'fit',
                            frame: false,
                            border: false,
                           
                            items:[
                                {
                                    xtype: 'information-address-taderaddress-grid',
                                    border: false
                                 
                                }
                            ]
                        }
                        ,{
                       	 	title: 'ประเภทการจดทะเบียน',
	                       	layout: 'anchor',
	                     	frame: false,
	                     	border: false,
                            items: [
                            {
                            	xtype: 'information-type-info-panel'
                            }
                            
                            ]
                        }
                        ,{
                        	title: 'การศึกษาและภาษา',
                        	layout: 'anchor',
                        	frame: false,
                        	border: false,
                        	items: [
                        	{
                        		xtype: 'information-education-info-grid'
                        	}
                        	,
                        	{xtype: 'splitter'},{xtype: 'splitter'},
                        	{
                        		xtype: 'information-language-info-grid'
                            }
                        	]
                        }
                        ,{
                            title: 'เรื่องร้องเรียน',
                            layout: 'anchor',
                        	frame: false,
                        	border: false,
                        	items: [
                        	{
//                        		xtype: 'information-suspension-info-grid'
                        		xtype: 'information-suspension-complaint-info-grid'
                        	}
                        	
                        	]
                        }
                        ,{
                            title: 'ประวัติการจดทะเบียน',
                            layout: 'anchor',
                        	frame: false,
                        	border: false,
                        	items: [
                        	{
                        		xtype: 'information-history-info-grid'
                        	}
                        	]
                        }
                        ,{
                        	title: 'เอกสารประกอบ',
  	                      layout: 'fit',
  	                      frame: false,
  	                      border: false,
  	                      items:[
  	                          {
  	                        	  xtype: 'document-tourleader-popup-grid',
  	                              border: false
  	                              
  	                            ,roleAction: this.roleAction
  	                          }
  	                      ]
                        }
                    ]
                }
        ]   
        });

        this.callParent(arguments);
    }
  
});