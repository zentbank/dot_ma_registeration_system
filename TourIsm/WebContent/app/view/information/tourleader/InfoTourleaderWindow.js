Ext.define('tourism.view.information.tourleader.InfoTourleaderWindow',{
	extend: 'Ext.window.Window',
	alias: 'widget.information_tourleader_window',
	requires: [
	           'tourism.view.information.tourleader.InfoTourleaderForm'
	],
	title: 'รายละเอียด',
	layout: 'fit',
	width: 750,
	height: 550,
	//True to make the window modal and mask everything behind it when displayed
    modal: true,
    
    initComponent: function(){
    	
    	this.items =[
    	    {
    	    	xtype: 'information-tourleader-formpanel',
    	    	id: 'information-tourleader-formpanel',
                frame: false,
                border: false,
                registrationModel: this.registrationModel
                ,roleAction: this.roleAction
                ,registrationType: this.registrationType
    	    }
    	];
    	
    	this.callParent(arguments);
    }

	,loadFormRecord: function(model){
		
		var info = this.down('form');
        info.loadRecord(model);

        //Image
        var imageFieldSet = this.down('image');
        // console.log(model);
        imageFieldSet.setSrc("data:image/png;base64,"+model.get("imageFile"));
      	
        //ที่อยู่
        var traderaddressgrid =  info.down('information-address-taderaddress-grid');
        traderaddressgrid.getStore().load({
            params: {traderId: model.get('traderId'), recordStatus: model.get('traderRecordStatus')},
            callback: function(records, operation, success) {
                // //console.log(records);
            },
            scope: this
        }); 
        
        //การศึกษา
        var educationinfogrid = info.down('information-education-info-grid');
        educationinfogrid.getStore().load({
        	params: {personId: model.get('personId'), educationType: 'E', recordStatus: model.get('traderRecordStatus')},
        	callback: function(records, operation, success){
        		//console.log(records);
        	},
        	scope: this
        });
        
        //ภาษา
        var languageinfogrid = info.down('information-language-info-grid');
        languageinfogrid.getStore().load({
        	params: {personId: model.get('personId'), recordStatus: model.get('traderRecordStatus')},
        	callback: function(records, operation, success){
        		//console.log(records);
        	},
        	scope: this
        });
        
        //ประเภทการจดทะเบียน
        
        if(model.get('traderCategory') == '300')
    	{
        	var infodetailpanel = info.down('information-detail-type-info-panel');
            
            infodetailpanel.on('afterrender',function(cmp){
            	cmp.loadRecord({
            		graduationCourse: model.get('graduationCourse')
            		,generationGraduate: model.get('generationGraduate')
            		,universityName: model.get('universityName')
            		,studyDate: model.get('studyDate')
            		,guidehidden: 'hidden=true'
            	});
            });
    	}
        if(model.get('traderCategory') == '200')
    	{
        	var infodetailpanel = info.down('information-detail-type-info-panel');
       
            
            infodetailpanel.on('afterrender',function(cmp){
            	cmp.loadRecord({
            		licenseGuideNo: model.get('licenseGuideNo')
            		,personFullName: model.get('personFullName')
            		,genderName: model.get('genderName')
            		,traderCategoryName: model.get('traderCategoryName')
            		,traderCategoryGuideTypeName: model.get('traderCategoryGuideTypeName')
            		,trainhidden: 'hidden=true'
            	});
            });
    	}
        
        //เอกสารประกอบ
        var grid = info.down('document-tourleader-popup-grid');
        grid.getStore().load({
      	  params: {
      		  traderId: model.get('traderId')
      		  ,recordStatus: 'N'
      		  ,licenseType: 'L'
      		  ,registrationType: 'N'
      	  },
            callback: function(records, operation, success) {
//                 //console.log(records);
            },
            scope: this
        });  
        
        //ประวัติการจดทะเบียน
        if(model.get('licenseNo') != "")
 	    {
	        var historyinfogrid = info.down('information-history-info-grid');
	        historyinfogrid.getStore().load({
	        	params: {traderType: model.get('traderType'), licenseNo: model.get('licenseNo')},
	        	callback: function(records, operation, success){
	        		//console.log(records);
	        	},
	        	scope: this
	        });
 	    }
	      
//	      //console.log(model);
	      if(model.get("registrationType") == 'N' && this.roleAction != 'info')
	      {
            //เรื่องร้องเรียน
	    	  var cmp = info.down('tabpanel').getComponent(3);
	    	  info.down('tabpanel').remove(cmp, true);
	    	  
	    	  // var cmp2 = info.down('tabpanel').getComponent(4);
	    	  // info.down('tabpanel').remove(cmp2, true);
	      }
	      else
    	  {
    	  
	    	//เรื่องร้องเรียน
	          var dataParams = {};
	  	    dataParams.questionType = 'C';
	  	    dataParams.complaintProgress = 'INFO';
	  	    dataParams.licenseNo = model.get('licenseNo');
	  	    dataParams.traderType = model.get('traderType');

	  	    var grid = info.down('information-suspension-complaint-info-grid');
	  	    var store = grid.getStore();
	  	   
	  	     dataParams.page = '1';
	  	     dataParams.start = '0';
	  	     dataParams.limit = store.pageSize;

//	  	    //console.log(dataParams);

	  	      store.load({
	  	          params: dataParams,
	  	          callback: function(records, operation, success) {
	  	               //console.log(records);
	  	          },
	  	          scope: this
	  	      }); 
	    	  
	      }
        
	}
	
});















