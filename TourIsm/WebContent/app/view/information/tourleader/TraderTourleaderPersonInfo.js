Ext.define('tourism.view.information.tourleader.TraderTourleaderPersonInfo', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.form.FieldSet',
        'Ext.form.FieldContainer',
        'Ext.form.field.VTypes',
        'Ext.data.*',
        'Ext.form.*'
    ],
    xtype: 'information-tourleader-traderperson-container',
   initComponent: function(){
  
        Ext.apply(this, {
            // width: 800,
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },
            defaults: {
                    anchor: '100%'
            },
            items: [{
                xtype: 'container',
                // title: 'Payment',
                layout: 'anchor',
                defaults: {
                    anchor: '100%'
                },
                items: [{
                    xtype: 'container',
                    layout: 'hbox',
                    /*
                    top margin is 25px
                    right margin is 50px
                    bottom margin is 75px
                    left margin is 100px
                    */
                    margin: '0 0 5 0',
                    items: [
                        {
                            xtype: 'hiddenfield',
                            name: 'regId'
                        },
                        {
                            xtype: 'hiddenfield',
                            name: 'registrationType'
                        },
                        {
                            xtype: 'displayfield',
                            name: 'registrationNo',
                            // labelAlign: 'left',
                            fieldLabel: 'เลขที่รับเรื่อง',
                            flex: 1
                            ,hidden: (this.roleAction != 'info' && this.registrationType =='N')?false:true
                        },{
                            xtype: 'displayfield',
                            fieldLabel: 'วันที่รับเรื่อง',
                            name: 'registrationDate',
                            flex: 1
                            ,hidden: (this.roleAction != 'info' && this.registrationType =='N')?false:true
                            // ,
                            // margin: '0 0 0 5'
                            // ,value: Ext.Date.add(new Date(), Ext.Date.YEAR, 543), //2554 ณ ปัจจุบัน
                           
                        },{
                            xtype: 'splitter'
                        },{
                            xtype: 'displayfield',
                            name: 'authorityName',
                            fieldLabel: 'ผู้รับเรื่อง',
                            flex: 1,
                            margin: '0 0 0 5'
                            ,hidden: (this.roleAction != 'info' && this.registrationType =='N')?false:true
                        }
                    ]
                },
                {
	                xtype: 'container',
	                // title: 'Payment',
	                layout: 'anchor',
	                defaults: {
	                    anchor: '100%'
	                },
	                items: [{
	                    xtype: 'container',
	                    layout: 'hbox',
	                    margin: '0 0 5 0',
	                    items: [
	                        {
	                            xtype: 'displayfield',
	                            name: 'licenseNo',
	                            // labelAlign: 'left',
	                            fieldLabel: 'เลขที่ใบอนุญาต',
	                            flex: 1
	                            ,hidden: (this.roleAction == 'info' || this.registrationType !='N')?false:true
	                        },{
	                            xtype: 'displayfield',
	                            fieldLabel: 'สถานะใบอนุญาต',
	                            name: 'licenseStatusName',
	                            flex: 1
	                            ,hidden: (this.roleAction == 'info' || this.registrationType !='N')?false:true
	                        }
	                    ]
	                }]
                },
                {
                    xtype: 'hiddenfield',
                    name: 'traderId'
                },
                {
                    xtype: 'hiddenfield',
                    name: 'personId'
                },
                {
                    xtype: 'hiddenfield',
                    name: 'traderType'
                },
                {
                	xtype : 'displayfield',
                	name: 'identityNo',
                	labelWidth: 300,
                    fieldLabel: 'เลขที่',
                    labelAlign: 'left'
                }
                ,{
                    xtype: 'displayfield',
                    name: 'traderOwnerName',
                    labelWidth: 300,
                    fieldLabel: 'ชื่อ-นามสกุล (ภาษาไทย)',
                    labelAlign: 'left',
                    renderer: function(value, dispField){
                        return Ext.String.format(
                            '<a href="http://www.ratchakitcha.soc.go.th/RKJ/index/index/announce/search.jsp" target="_blank">{0}</a>',
                            value
                        );                 
                    }
                }
                ,{
                    xtype: 'displayfield',
                    name: 'traderOwnerNameEn',
                    labelWidth: 300,
                    fieldLabel: 'ชื่อ-นามสกุล (ภาษาต่างประเทศ)',
                    labelAlign: 'left'
                }
                ,{
                    xtype: 'container',
                    layout: 'hbox',
                    /*
                    top margin is 25px
                    right margin is 50px
                    bottom margin is 75px
                    left margin is 100px
                    */
                    margin: '0 0 5 0',
                    items: [
                        {
                            xtype: 'displayfield',
                            name: 'gender',
                            fieldLabel: 'เพศ',
                            flex: 1,
                            renderer: function(value, metaData, gridmodel, rowIndex,colIndex,gridstore,gridview){
                            	if(value == 'F')
                            		return 'หญิง';
                            	else if(value == 'M')
                            		return 'ชาย';
                            }
                        }
                        ,{
                            xtype: 'displayfield',
                            fieldLabel: 'สัญชาติ',
//                            name: 'personNationality',
                            flex: 1 ,
                            value: 'ไทย'
                        }
                    ]
                }
                ,{
                    xtype: 'container',
                    layout: 'hbox',
                    /*
                    top margin is 25px
                    right margin is 50px
                    bottom margin is 75px
                    left margin is 100px
                    */
                    margin: '0 0 5 0',
                    items: [
                        {
                            xtype: 'displayfield',
                            name: 'birthDate',
                            fieldLabel: 'เกิดวันที่',
                            flex: 1
                        },{
                            xtype: 'displayfield',
                            fieldLabel: 'อายุ',
                            name: 'ageYear',
                            flex: 1 
                        }
                    ]
                }
                ,{
                    xtype: 'container',
                    layout: 'hbox',
                    /*
                    top margin is 25px
                    right margin is 50px
                    bottom margin is 75px
                    left margin is 100px
                    */
                    margin: '0 0 5 0',
                    items: [
                        {
                            xtype: 'displayfield',
                            name: 'provinceName',
                            fieldLabel: 'ออกให้ ณ จังหวัด',
                            flex: 1
                        },{
                            xtype: 'displayfield',
                            fieldLabel: 'อำเภอ/เขต/สังกัด',
                            name: 'amphurName',
                            flex: 1 
                        }
                    ]
                },{
                     xtype: 'hiddenfield',
                     name: 'imageFile'
               },{
                    xtype: 'container',
                    layout: 'hbox',
                    /*
                    top margin is 25px
                    right margin is 50px
                    bottom margin is 75px
                    left margin is 100px
                    */
                    margin: '0 0 5 0',
                    items: [
                        {
                            xtype: 'displayfield',
                            name: 'identityNoExpire',
                            // labelWidth: 300,
                            fieldLabel: 'หมดอายุวันที่',
                            labelAlign: 'left'
                        },{
                            xtype: 'fieldset',
                            itemId: 'imageWrapper',
                            margin: '0 100px 0',
                            layout: 'anchor',
                            items: [{
                                xtype: 'image',
                                itemId: 'imageFile',
                                margin: '0 90px 0',
                                listeners:{
                                    afterrender:function(img, a, obj){
                                        img.getEl().dom.style.width = '130px'; 
                                        img.getEl().dom.style.height = '150px'; 
                                    }
                                }
                            }]
                        }
                    ]
                }]
            }]

        
        });
        
        this.callParent(arguments);
    }
});