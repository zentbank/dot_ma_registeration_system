Ext.define('tourism.view.information.tradercategory.BusinessCategoryInfoPanel', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.form.FieldSet',
        'Ext.form.FieldContainer',
        'Ext.form.field.VTypes',
        'tourism.view.information.tradercategory.BusinessPlantripGridInfo'
    ],
    xtype: 'information-business-category-panel',
    initComponent: function(){

        Ext.apply(this, {
            // width: 800,
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },
            defaults: {
                    anchor: '100%'
            },
            items: [
                {
                    xtype: 'displayfield',
                    fieldLabel: 'ประเภทการทำธุรกิจนำเที่ยว',
                    name: 'traderCategory',
                    labelWidth: 200,
                    anchor: '100%',
                    renderer: function(value, metaData, model){

                        if(value == '100')
                        {
                            return 'ทั่วไป';
                        }
                        if(value == '200')
                        {
                            return 'นำเที่ยวจากต่างประเทศ';
                        }
                        if(value == '300')
                        {
                            return 'ในประเทศ';
                        }
                        if(value == '400')
                        {
                            return 'เฉพาะพื้นที่';
                        } 
                    }
                },
               
                 {
                    xtype: 'displayfield',
                    fieldLabel: 'จัดนำเที่ยวจำนวน (ครั้ง/ปี)',
                    name: 'plantripPerYear',
                    labelWidth: 200,
                    anchor: '100%'
                }
                ,{

                    // left hand side
                    xtype: 'container',
                    title: 'แผนการนำเที่ยว',
                    layout: 'anchor',
                    frame: false,
                    border: false,
                    items:[
                        {
                            xtype:'information-tradercategory-businessplantrip-gridinfo',
                            // title: 'นำเที่ยวประเทศ',
                            frame: false,
                            border: false,
                            anchor: '100% 100%'
                        }
                    ]
                }
               
            ]

        
        });
        
        this.callParent(arguments);
    }
});