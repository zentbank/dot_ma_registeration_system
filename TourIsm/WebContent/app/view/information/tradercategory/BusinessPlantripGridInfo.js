Ext.define('tourism.view.information.tradercategory.BusinessPlantripGridInfo',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.information-tradercategory-businessplantrip-gridinfo',
		
		stripeRows: true,
		requires: [
			'Ext.grid.Column',
			'Ext.grid.column.Template',
			'Ext.grid.feature.Summary'
		],
		initComponent: function(){

			// create every new form 
			var countryStrore = Ext.create('tourism.store.combo.CountryStrore',{
				storeId: 'information-tradercategory-businessplantrip-mascountrystrore'
			});
			countryStrore.load();

			var plantripStore = Ext.create('tourism.store.business.registration.tradercategory.PlantripStore',{
				storeId: 'information-tradercategory-businessplantrip-plantripStore'
			});

			Ext.apply(this, {
				store: plantripStore,
				height: 180,
				autoScroll: true,
				collapsed : false,
				collapseMode: 'mini',
				collapseDirection : 'bottom',
				hideHeaders: true,
				features: [{
		            ftype: 'summary',
		            dock: 'bottom'
				}],
				
				columns: [
					{xtype: 'rownumberer'},
					{
			                header: 'ประเทศ',
			            
			                dataIndex: 'countryId',
			                flex: 1,
							summaryType: 'count',

							summaryRenderer: function(value, summaryData, dataIndex) {
			                	return 'นำเที่ยวรวม ' + value + ' ประเทศ';
			            	},
		                	renderer: function(value, metaData, gridmodel, rowIndex,colIndex,gridstore,gridview){
		                		var countryName = value;
		                		countryStrore.each(function(model){
		                			if(model.get('countryId') == value)
		                			{
		                				countryName = model.get('countryName');
		                			}
		                		});
		                		
						        return countryName;
						    }
		            }
				]
			});

			this.callParent(arguments);
		}
	});