Ext.define('tourism.view.information.train.TrainInfoGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.information-train-info-grid',
		
		stripeRows: true,
		requires: [
			'Ext.grid.Column',
			'Ext.grid.column.Template',
			'Ext.grid.column.Action',
			'Ext.grid.plugin.RowExpander'
		],
		initComponent: function(){

			// create every new form 
			var store = Ext.create('tourism.store.grid.EducationStore',{
				  storeId: 'information-education-info-store'
			  });
			
			Ext.apply(this, {
				store: store,
				// selType: 'checkboxmodel',
				columns: [
					{
						header: 'eduId',
						dataIndex: 'eduId',
						hidden: true
					}
					,{
						header: 'การอบรม',
						dataIndex: 'graduationCourse',
						flex: 1
						
					}
					
				]
			});

			this.callParent();
		}
		,plugins: [{
            ptype: 'rowexpander',
            rowBodyTpl : new Ext.XTemplate(
            	'<div class="x-container x-container-default"',
				'	style="width: 778px;">',
				'	<span  style="display: table; width: 100%; table-layout: fixed;">',
				'		<div style="display: table-cell; height: 100%; vertical-align: top;" class="">',
				'			<div',
				'				class="x-splitter x-splitter-default x-splitter-horizontal x-unselectable"',
				'				style="width: 778px; height: 5px;" ></div>',
				'			<div class="x-container x-container-default x-box-layout-ct"',
				'				 style="width: 778px;">',
				'				<div  class="x-box-inner "',
				'					role="presentation" style="width: 778px; height: 120px;">',
				'					<div  class="x-box-target"',
				'						style="width: 778px;">',
				'						<fieldset class="x-fieldset x-box-item x-fieldset-default"',
				'							style="border-width: 0px; right: auto; left: 0px; margin: 0px; width: 389px; top: 0px;"',
				'							>',
				'							<div  class="x-fieldset-body "',
				'								style="width: 369px;">',
				'								<span ',
				'									style="display: table; width: 100%; table-layout: fixed;"><div',
				'										style="display: table-cell; height: 100%; vertical-align: top;"',
				'										class="">',
				'										<table',
				'											class="x-field x-table-plain x-form-item x-form-type-text x-field-default x-anchor-form-item x-form-dirty"',
				'											cellpadding="0" ',
				'											style="table-layout: fixed; width: 369px;">',
				'											<tbody>',
				'												<tr role="presentation" ',
				'													class="x-form-item-input-row">',
				'													<td role="presentation" ',
				'														style="" valign="top" halign="left" width="155"',
				'														class="x-field-label-cell"><label',
				'														class="x-form-item-label x-unselectable x-form-item-label-left"',
				'														style="width: 150px; margin-right: 5px;" unselectable="on">ชื่อหลักสูตร</label></td>',
				'													<td role="presentation" class="x-form-item-body  "',
				'														colspan="2"',
				'														style="width: 100%;"><label',
				'														class="x-form-item-label x-unselectable x-form-item-label-left"',
				'														style="width: 150px; margin-right: 5px;" unselectable="on">{graduationCourse}</label></td>',
				'												</tr>',
				'											</tbody>',
				'										</table>',
				
				'										<table',
				'											class="x-field x-table-plain x-form-item x-form-type-text x-field-default x-anchor-form-item x-form-dirty"',
				'											cellpadding="0" ',
				'											style="table-layout: fixed; width: 369px;">',
				'											<tbody>',
				'												<tr role="presentation" ',
				'													class="x-form-item-input-row">',
				'													<td role="presentation" ',
				'														style="" valign="top" halign="left" width="155"',
				'														class="x-field-label-cell"><label',
				'														class="x-form-item-label x-unselectable x-form-item-label-left"',
				'														style="width: 150px; margin-right: 5px;" unselectable="on">รุ่นที่</label></td>',
				'													<td role="presentation" class="x-form-item-body  "',
				'														colspan="2"',
				'														style="width: 100%;"><label',
				'														class="x-form-item-label x-unselectable x-form-item-label-left"',
				'														style="width: 150px; margin-right: 5px;" unselectable="on">{generationGraduate}</label></td>',
				'												</tr>',
				'											</tbody>',
				'										</table>',
				
				'										<table',
				'											class="x-field x-table-plain x-form-item x-form-type-text x-field-default x-anchor-form-item x-form-dirty"',
				'											cellpadding="0" ',
				'											style="table-layout: fixed; width: 369px;">',
				'											<tbody>',
				'												<tr role="presentation" ',
				'													class="x-form-item-input-row">',
				'													<td role="presentation" ',
				'														style="" valign="top" halign="left" width="155"',
				'														class="x-field-label-cell"><label',
				'														class="x-form-item-label x-unselectable x-form-item-label-left"',
				'														style="width: 150px; margin-right: 5px;" unselectable="on">สถาบัน</label></td>',
				'													<td role="presentation" class="x-form-item-body  "',
				'														colspan="2"',
				'														style="width: 100%;"><label',
				'														class="x-form-item-label x-unselectable x-form-item-label-left"',
				'														style="width: 150px; margin-right: 5px;" unselectable="on">{universityName}</label></td>',
				'												</tr>',
				'											</tbody>',
				'										</table>',
				
				'										<table',
				'											class="x-field x-table-plain x-form-item x-form-type-text x-field-default x-anchor-form-item x-form-dirty"',
				'											cellpadding="0" ',
				'											style="table-layout: fixed; width: 369px;">',
				'											<tbody>',
				'												<tr role="presentation" ',
				'													class="x-form-item-input-row">',
				'													<td role="presentation" ',
				'														style="" valign="top" halign="left" width="155"',
				'														class="x-field-label-cell"><label',
				'														class="x-form-item-label x-unselectable x-form-item-label-left"',
				'														style="width: 150px; margin-right: 5px;" unselectable="on">วันที่อบรม</label></td>',
				'													<td role="presentation" class="x-form-item-body  "',
				'														colspan="2"',
				'														style="width: 100%;"><label',
				'														class="x-form-item-label x-unselectable x-form-item-label-left"',
				'														style="width: 150px; margin-right: 5px;" unselectable="on">{studyDate}</label></td>',
				'												</tr>',
				'											</tbody>',
				'										</table>',
				
				'									</div></span>',
				'							</div>',
				'						</fieldset>',
				'					</div>',
				'				</div>',
				'			</div>',
				'		</div></span>',
				'</div>',
	        {
	        
		        isBusiness: function(traderType){
		           return traderType == 'B';
		        }
	    	}

            )
        }]
	});











