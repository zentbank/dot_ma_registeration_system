Ext.define('tourism.view.information.type.TypeInfoPanel', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.form.FieldSet',
        'Ext.form.FieldContainer',
        'Ext.form.field.VTypes'
        ,'tourism.view.information.type.DetailTypeInfoPanel'
    ],
    xtype: 'information-type-info-panel',
    initComponent: function(){

        Ext.apply(this, {
            // width: 800,
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },
            defaults: {
                    anchor: '100%'
            },
            items: [
                {
                    xtype: 'displayfield',
                    fieldLabel: '',
                    name: 'traderCategory',
                    labelWidth: 200,
                    anchor: '100%'
                    ,renderer: function(value, metaData, model){
                    	
                        if(value == '100')
                        {                      	
                            return 'เป็นผู้นำเที่ยวตามประกาศกฎกระทรวงฯ';
                        }
                        if(value == '200')
                        {
                            return 'เป็นมัคคุเทศก์และมาขอจดทะเบียนผู้นำเที่ยว';
                        }
                        if(value == '300')
                        {
                            return 'ผ่านการอบรมจากสถาบันที่สำนักทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ กำหนด';
                        }
                    }
                }
                ,{
                	xtype: 'information-detail-type-info-panel'
                }

            ]

        });
        
        this.callParent(arguments);
    }
});











