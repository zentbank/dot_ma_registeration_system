Ext.define('tourism.view.joborder.JobOrderFormSearch' ,{
    extend: 'Ext.form.FormPanel',
    alias : 'widget.joborder-JobOrderFormSearch',
    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.Time',
        'Ext.form.CheckboxGroup',
        'Ext.layout.container.HBox',
        'Ext.form.FieldSet',
        'Ext.util.Positionable'
    ],
    bodyPadding: 10,
    
    items: [{
            xtype: 'container',
            layout: 'anchor',
            defaults: {
                anchor: '100%',
                labelWidth: 150
            },
            items: [{
                xtype: 'textfield',
                name: 'licenseNo',
                fieldLabel: 'เลขที่ใบอนุญาต',
                allowBlank: false,
                tabIndex: 1
                //afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>'
            }, {
                xtype: 'textfield',
                name: 'traderName',
                fieldLabel: 'ชื่อใบอนุญาต',
                allowBlank: false,
                tabIndex: 2
            }, {
                xtype: 'container',
                layout: 'hbox',
                layoutCongig: {
                     pack:'center',
                     align:'middle'
                },
                items: [{
                    xtype: 'container',
                    // title: 'Payment',
                    flex: 1,
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%',
                        labelWidth: 150
                    },
                    items: [{
                        xtype: 'datefield',
                        fieldLabel: 'วันที่บันทึก',
                        name: 'itineraryFrom',
                        format: 'd/m/B'
                    }]
                }, {
                    xtype: 'splitter'
                }, {
                    xtype: 'container',
                    flex: 1,
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%',
                        labelWidth: 150
                    },
                    items: [{
                        xtype: 'datefield',
                        fieldLabel: 'ถึงวันที่',
                        name: 'itineraryTo',
                        format: 'd/m/B'
                    }]
                }]
            }]      
        },
        {
            xtype: 'toolbar',
            border: false,
            padding: '6px 0 6px 0px',
            items: [
                {
                	xtype: 'button',
                    text : 'ค้นหา',
                    action: 'searchJobOrder'
                }
            ]
        }

    ]
	
});    
