Ext.define('tourism.view.joborder.JobOrderGrid',{
	extend: 'Ext.grid.Panel',
	alias: 'widget.joborder-JobOrderGrid-grid',
	stripeRows: true,
	requires: [
       'Ext.grid.*',
       'Ext.data.*',
       'Ext.util.*',
       'Ext.state.*',
       'Ext.form.*',
       'Ext.grid.feature.Summary',
       'tourism.model.joborder.JobOrderModel'
	],
	
	initComponent: function(){
		var store = new Ext.data.Store({
	        model: 'tourism.model.joborder.JobOrderModel',
	        pageSize: 20,
			proxy: {
                type: 'ajax',
                actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
                api: {
                    read: 'business/joborder/read'
                },
                reader: {
                    type: 'json',
			        root: 'list',
			        totalProperty: 'totalCount',
			        successProperty: 'success',
			        messageProperty: 'message'
                },
                writer: {
                    type: 'json',
                    writeAllFields: true, ////just send changed fields
                    allowSingle: false, //always wrap in an array
                    batch: false
                },
                listeners: {
                    exception: function(proxy, response, operation){

                        Ext.MessageBox.show({
                            title: 'REMOTE EXCEPTION',
                            msg: operation.getError(),
                            icon: Ext.MessageBox.ERROR,
                            buttons: Ext.Msg.OK
                        });
                    },
                    beforeload: function( store, operation, eOpts )
                    {
                       	var formId = '#joborder-JobOrderFormSearch-formsearch';				    
			        	var formSearch = Ext.ComponentQuery.query(formId)[0];
			        	var values = formSearch.getValues();
							
			        	 for (field in values) 
					      {
					        if (Ext.isEmpty(values[field])) 
					        {
					          delete values[field];
					        }
					        
					      }
					    store.proxy.extraParams = values;
                    }
                }
            }

	    }); 
		
		Ext.apply(this,{
			store: store,
			dockedItems:[{
				xtype: 'pagingtoolbar',
				store: store,
				dock: 'bottom',
				displayInfo: true

			}],
			columns:[{
	             header: 'เลขที่ใบอนุญาต',
	             dataIndex: 'licenseNo',
	             flex: 1
	         },
	         {
				header: 'ชื่อ',
				dataIndex: 'traderName',
				flex: 2
			},
			{
				header: 'เลขที่ใบสั่งงาน',
				dataIndex: 'jobNo',
				flex: 1
			},
			{
				header: 'วันที่',
				dataIndex: 'jobDate',
				flex: 1
			},
			{
				header: 'วันที่บันทึก',
				dataIndex: 'writeDate',
				flex: 1
			}, {
	        	header: '',
	            xtype: 'actioncolumn',
	           	width:50,
	           	align : 'center',
	            items: [{
	              
					getClass: function(v, meta, rec) {          
	                    return 'icon-notes-xsmall';
	                },			        
		            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {

		            	this.fireEvent('itemclick', this, 'printJobOrder', grid, rowIndex, colIndex, record, node);
		            }
	            }]
	        }]
			 
		});
		
		
		this.callParent(arguments);
	}

});







