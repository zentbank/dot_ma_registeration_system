Ext.define('tourism.view.joborder.JobOrderPanel', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
        'tourism.view.joborder.JobOrderFormSearch',
        'tourism.view.joborder.JobOrderGrid'
    ],
    
    xtype: 'joborder-JobOrderPanel',
    initComponent: function(){

    	Ext.apply(this, {
    		layout: {
		        type: 'border'
		    },
		    items: [{
		        region: 'north',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items: [
		                {
		                	xtype: 'joborder-JobOrderFormSearch',
		                	title: 'ค้นหา',
		                	collapsible: true,  
		    	            // padding: '5px 5px 0px 5px',
		    	            id: 'joborder-JobOrderFormSearch-formsearch',
		    	            frame: false,
		    	            border: false
		                }
		        ]
		    },{
		        region: 'center',
		        xtype: 'panel',
		        layout: 'fit',
		        frame: false,
		        border: false,
		        items:[{
		        	xtype: 'joborder-JobOrderGrid-grid',
		        	id: 'joborder-JobOrder-grid',
		        	title: 'รายการ',
		            // padding: '5px 5px 5px 5px',
		            autoScroll: true,
		            border: false
		        }]
		    }]
    	});

    	this.callParent(arguments);
    }
});