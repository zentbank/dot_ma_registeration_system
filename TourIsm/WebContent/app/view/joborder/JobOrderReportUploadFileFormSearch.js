Ext.define('tourism.view.joborder.JobOrderReportUploadFileFormSearch' ,{
    extend: 'Ext.form.FormPanel',
    alias : 'widget.joborder-JobOrderReportUploadFileFormSearch',
    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.Time',
        'Ext.form.CheckboxGroup',
        'Ext.layout.container.HBox',
        'Ext.form.FieldSet',
        'Ext.util.Positionable'
    ],
    bodyPadding: 10,
    
    items: [{
            xtype: 'container',
            layout: 'anchor',
            defaults: {
                anchor: '100%',
                labelWidth: 150
            },
            items: [{
                xtype: 'textfield',
                name: 'businessLicenseNo',
                fieldLabel: 'เลขที่ใบอนุญาต',
                allowBlank: false,
                tabIndex: 1
                //afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>'
            }, {
                xtype: 'textfield',
                name: 'businessTraderName',
                fieldLabel: 'ชื่อผู้ประกอบธุรกิจ',
                allowBlank: false,
                tabIndex: 2
            }, {
                xtype: 'container',
                layout: 'hbox',
                layoutCongig: {
                     pack:'center',
                     align:'middle'
                }
            },{
                xtype: 'combo',
                fieldLabel: 'ภูมิภาค',
                store: 'tourism.store.combo.OrganizationStore',
                queryMode: 'remote',
                displayField: 'orgName',
                valueField: 'orgId',
                hiddenName: 'orgId',
                name :'orgId'
            }]      
        },
        {
            xtype: 'toolbar',
            border: false,
            padding: '6px 0 6px 0px',
            items: [
                {
                	xtype: 'button',
                	text : 'พิมพ์รายงาน',
                    action: 'printReportUploadFile'
                }
            ]
        }

    ]
	
});    
