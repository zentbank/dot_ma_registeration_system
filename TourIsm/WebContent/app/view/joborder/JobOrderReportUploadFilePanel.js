Ext.define('tourism.view.joborder.JobOrderReportUploadFilePanel', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
        'tourism.view.joborder.JobOrderReportUploadFileFormSearch'
    ],
    
    xtype: 'joborder-JobOrderReportUploadFilePanel',
    initComponent: function(){

    	Ext.apply(this, {
    		layout: {
		        type: 'border'
		    },
		    items: [{
		        region: 'north',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items: [
		                {
		                	xtype: 'joborder-JobOrderReportUploadFileFormSearch',
		                	title: 'ค้นหารายงานใบสั่งงานมัคคุเทศก์',
		                	collapsible: true,  
		    	            // padding: '5px 5px 0px 5px',
		    	            id: 'joborder-JobOrderReportUploadFileFormSearch-formsearch',
		    	            frame: false,
		    	            border: false
		                }
		        ]
		    },{
		        region: 'center',
		        xtype: 'panel',
		        layout: 'fit',
		        frame: false,
		        border: false
		    }]
    	});

    	this.callParent(arguments);
    }
});