Ext.define('tourism.view.ktbbank.KTBPaymentFormSearch' ,{
    extend: 'Ext.form.FormPanel',
    alias : 'widget.ktbbank-KTBPaymentFormSearch',
    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.Time',
        'Ext.form.CheckboxGroup',
        'Ext.layout.container.HBox',
        'Ext.form.FieldSet',
        'Ext.util.Positionable',
        'tourism.view.report.PaymentUploadPanel'
    ],
    bodyPadding: 10,
    
    items: [{
        xtype: 'container',
        layout: 'anchor',
        items: [{
            xtype: 'container',
            layout: 'hbox',
            layoutCongig: {
                 pack:'center',
                 align:'middle'
            },
            items: [
                {
                    xtype: 'container',
                    // title: 'Payment',
                    flex: 1,
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%',
                        labelWidth: 150
                    },
                    items: [{
                       xtype: 'datefield',
                        fieldLabel: 'วันที่ชำระเงิน',
                        name: 'DTranDateFrom',
                        allowBlank: false,
                        format: 'd/m/B'
                    }]
                },{
                    xtype: 'splitter'
                },
                {
                    xtype: 'container',
                    flex: 1,
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%',
                        labelWidth: 150
                    },
                    items: [{
                        xtype: 'datefield',
                        fieldLabel: 'ถึงวันที่',
                        name: 'DTranDateTo',
                        allowBlank: false,
                        format: 'd/m/B'
                    } ]
                }
            ]
        },{
            xtype: 'radiogroup',
            fieldLabel: 'สถานะการรับชำระเงิน',
            allowBlank: false,
            labelWidth: 150,
            // cls: 'x-check-group-alt',
            items: [
                {boxLabel: 'ยังไม่บันทึกข้อมูลการรับชำระเงิน', name: 'recordStatus', inputValue: 'T' , checked: true},
                {boxLabel: 'บันทึกข้อมูลการรับชำระเงิน', name: 'recordStatus', inputValue: 'N'}
                
            ]
        }
        // ,{
        //     xtype      : 'fieldcontainer',
        //     fieldLabel : 'สถานะการรับชำระเงิน',
        //     defaultType: 'radiofield',
        //     defaults: {
        //         flex: 1,
        //         labelWidth: 150
        //     },
        //     layout: 'hbox',
        //     items: [
        //         {
        //             boxLabel  : 'ยังไม่รับชำระเงินแล้ว',
        //             name      : 'recordStatus',
        //             inputValue: 'T',
        //             checked: true
        //         }, {
        //             boxLabel  : 'รับชำระเงินแล้ว',
        //             name      : 'recordStatus',
        //             inputValue: 'N'
        //         }
        //     ]
        // }
        ]      
    },
    {
        xtype: 'toolbar',
        border: false,
        padding: '6px 0 6px 0px',
        items: [
            {
            	xtype: 'button',
                text : 'ค้นหา',
                action: 'searchPayment'
            }
        ]
    }]
	
});    
