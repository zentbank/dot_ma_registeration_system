Ext.define('tourism.view.ktbbank.KTBPaymentGrid', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.ktbbank-KTBPaymentGrid-grid',
	stripeRows : true,
	requires : [ 'Ext.grid.*', 'Ext.data.*', 'Ext.util.*', 'Ext.state.*',
			'Ext.form.*', 'Ext.grid.feature.Summary',

			'tourism.store.ktbbank.KTBPaymentStore',
			'tourism.model.ktbbank.KTBPaymentModel',
			'tourism.view.ktbbank.KTBPreparePrintReceiptWindow',
			'tourism.view.ktbbank.ReceivePaymentWindow',
			'Ext.selection.CheckboxModel'],

	initComponent : function() {

		var grid = this;
		var showSummary = true;
		var store = Ext.create('tourism.store.ktbbank.KTBPaymentStore', {
			storeId : 'ktbbank-KTBPaymentStore-store',
			groupField : 'orgName',
			 proxy: {
		    	    type: 'ajax',
		    	    actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
		    	    api: {
		    	        read: 'tourleader/ktb/payment/readPaymentDetail',
		    	        update: 'tourleader/ktb/payment/savePayments',
		    	       
		    	    },
		    	    reader: {
		    	        type: 'json',
		    	        // metaProperty: '',
		    	        root: 'list',
		    	        // idProperty: 'emailId',
		    	        totalProperty: 'totalCount',
		    	        successProperty: 'success',
		    	        messageProperty: 'message'
		    	    },
		    	    writer: {
		    	        type: 'json',
		    	        // encode: true,
		    	        writeAllFields: true, ////just send changed fields
		    	        // root: 'data',
		    	        allowSingle: false, //always wrap in an array
		    	        batch: false
		    	    },
		    	    listeners: {
		    	        exception: function(proxy, response, operation){

		    	            Ext.MessageBox.show({
		    	                title: 'REMOTE EXCEPTION',
		    	                msg: operation.getError(),
		    	                icon: Ext.MessageBox.ERROR,
		    	                buttons: Ext.Msg.OK
		    	            });
		    	        }
		    	    }
		    	}
		});

		var selModel = Ext.create('Ext.selection.CheckboxModel', {
	        listeners: {
	            selectionchange: function(sm, selections) {
	                grid.down('#updatePayment').setDisabled(selections.length === 0);
	            },
	            beforeselect : function (checkbox, model, index, eOpts) {
	            
			        if (model.get('recordStatus') == 'N')
			          return false;
			    }
	        }
    	});

		Ext.apply(this, {
			store : store,
			columnLines: true,
        	selModel: selModel,
        	features: [{
	            ftype: 'summary',
	            dock: 'bottom'
			}
			,{
				ftype : 'grouping',
				groupHeaderTpl : '{name} ({rows.length} รายการ)',
				hideGroupedHeader : true,
				startCollapsed : false
			
			}
			],
			viewConfig: {
				stripeRows:false ,
		        getRowClass: function(record) {
		        	if (record.get('recordStatus') == 'N')
			          return "ux-disabled-record";
		        }
		    },
        	dockedItems: [
        	// {
	        //     xtype: 'toolbar',
	        //     dock: 'bottom',
	        //     ui: 'footer',
	        //     layout: {
	        //         pack: 'center'
	        //     },
	        //     items: [{
	        //         minWidth: 80,
	        //         text: 'Save'
	        //     },{
	        //         minWidth: 80,
	        //         text: 'Cancel'
	        //     }]
	        // }, 
	        {
	            xtype: 'toolbar',
	            items: [
	            {
	                text:'นำเข้าข้อมูลการรับชำระเงิน',
	                tooltip:'นำเข้าข้อมูลการรับชำระเงิน',
	                // iconCls:'add',
	                action: 'uploadFile'
	            }, '-',
	            // '-', {
	            //     text:'Options',
	            //     tooltip:'Set options',
	            //     iconCls:'option'
	            // },'-',

	            {
	                itemId: 'updatePayment',
	                text:'บันทึกข้อมูลการชำระเงิน',
	                tooltip:'บันทึกข้อมูลการชำระเงิน',
	                // iconCls:'ux-tourism-icon-add',
	                disabled: true,
	                action: 'savepayments'
	            }]
	        }],

			columns : [
					// {
					// 	header : 'วันทีทำรายการ',
					// 	dataIndex : 'progressDate',
					// 	flex : 2,
					// //	         	summaryType: 'count',

					// //	            summaryRenderer: function(value, summaryData, dataIndex) {
					// //	                return ((value === 0 || value > 1) ? '' + value + ' รายการ' : '(1 รายการ)');
					// //	            }
					// },
					// {
					// 	header : 'จำนวนเงิน',
					// 	dataIndex : 'DTranAmt',
					// 	flex : 1,
					// 	align : 'right',
					// 	renderer : function(value, metaData, record, rowIdx,
					// 			colIdx, store, view) {
					// 		return Ext.util.Format.number(value, '0,000.00');
					// 	},
					// 	summaryType : 'sum',
					// 	summaryRenderer : function(value, summaryData,
					// 			dataIndex) {
					// 		return Ext.util.Format.number(value, '0,000.00'); //+ ' บาท';
					// 	},
					// 	field : {
					// 		xtype : 'numberfield'

					// 	}
					// },

					// // {
					// // 	header : 'ผู้ทำรายการ',
					// // 	dataIndex : 'createUser',
					// // 	flex : 2
					// // }, 
					// {
					// 	header : 'สถานะ',
					// 	dataIndex : 'recordStatusText',
					// 	flex : 1
					// },
					// {
					// 	header : '',
					// 	xtype : 'actioncolumn',
					// 	width : 50,
					// 	align : 'center',
					// 	items : [ {
					// 		iconCls : 'icon-feepayment-xsmall',
					// 		tooltip : 'รับชำระเงิน',
					// 		handler : function(grid, rowIndex,
					// 				colIndex, node, e, record,
					// 				rowNode) {

					// 			this.fireEvent('itemclick', this,
					// 					'receivePayment',
					// 					grid, rowIndex, colIndex,
					// 					record, node);

					// 		}
					// 	} ]
					// }
				{
					header : 'เลขประจำตัวผู้เสียภาษี',
					dataIndex : 'DReference1',
					flex : 1
					,summaryType: 'count'
			        ,summaryRenderer: function(value){
			            return ' รวม';
			        }
				},
				{
					header : 'เลขที่ใบแจ้งชำระเงิน',
					dataIndex : 'DReference2',
					flex : 1,

				},
				{
					header : 'ชื่อ',
					dataIndex : 'personName',
					flex : 2
				},
				
				{
					header : 'หน่วยงาน',
					dataIndex : 'orgName',
					flex : 1,

				},

				{
					header : 'รายการ',
					dataIndex : 'feeRegistrationType',
					flex : 2
				},
				
				{
					header : 'จำนวนเงิน',
					dataIndex : 'DTranAmt',
					flex : 1,
					align : 'right',
					renderer : function(value, metaData, record, rowIdx,
							colIdx, store, view) {
						return Ext.util.Format.number(value, '0,000.00');
					},
					summaryType : 'sum',
					summaryRenderer : function(value, summaryData,
							dataIndex) {
						return Ext.util.Format.number(value, '0,000.00'); //+ ' บาท';
					},
					field : {
						xtype : 'numberfield'

					}
				}
			]

		});

		this.callParent(arguments);
	}

});
