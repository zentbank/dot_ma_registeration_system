Ext.define('tourism.view.ktbbank.KTBPaymentPanel', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
        'tourism.view.ktbbank.KTBPaymentFormSearch',
        'tourism.view.ktbbank.KTBPaymentGrid'
    ],
    
    xtype: 'ktbbank-KTBPaymentPanel',
    initComponent: function(){

    	Ext.apply(this, {
    		layout: {
		        type: 'border'
		    },
		    items: [{
		        region: 'north',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items: [
		                {
		                	xtype: 'ktbbank-KTBPaymentFormSearch',
		                	title: 'ค้นหา',
		                	collapsible: true,  
		    	            // padding: '5px 5px 0px 5px',
		    	            id: 'ktbbank-KTBPaymentFormSearch-formsearch',
		    	            frame: false,
		    	            border: false
		                }
		        ]
		    },{
		        region: 'center',
		        xtype: 'panel',
		        layout: 'fit',
		        frame: false,
		        border: false,
		        items:[{
		        	xtype: 'ktbbank-KTBPaymentGrid-grid',
		        	id: 'ktbbank-KTBPayment-grid',
		        	title: 'รายการ',
		            // padding: '5px 5px 5px 5px',
		            autoScroll: true,
		            border: false
		        }]
		    }]
    	});

    	this.callParent(arguments);
    }
});