Ext.define('tourism.view.ktbbank.KTBPreparePrintReceiptForm',{
	extend: 'Ext.form.Panel'
	,alias: 'widget.ktbbank-prepareprintreceipt-form'
	,requires: [
		'Ext.data.*',
		'Ext.form.*'
		
    ],
    bodyPadding: 5,
    autoScroll: true,

    initComponent: function(){

        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

        var receiveOfficerNameStore = Ext.create('tourism.store.combo.OfficerStore',{
            id:'ReceiptForm-receiveOfficerName-officeStore'
            ,listeners: {
                beforeload: function( store, operation, eOpts )
                    {
         
                        store.proxy.extraParams = {groupRole: 'OFFACC'};
                    }
            }
        });

        var authorityStore = Ext.create('tourism.store.combo.OfficerStore',{
            id:'ReceiptForm-authority-officeStore'
            ,listeners: {
                beforeload: function( store, operation, eOpts )
                    {
         
                        store.proxy.extraParams = {groupRole: 'OFFACC'};
                    }
            }
        });
        
        Ext.apply(this, {
            width: '100%',
            height: '100%',
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },

            items: 
            [
                {
                    xtype: 'combo',
                    fieldLabel: 'สำนักงาน',
                    store: 'tourism.store.combo.OrganizationStore',
                    queryMode: 'remote',
                    displayField: 'orgName',
                    valueField: 'orgId',
                    hiddenName: 'orgId',
                    name :'orgId'
                } 
                ,{
                   xtype: 'datefield',
                   name: 'receiveOfficerDate',
                   fieldLabel: 'วันที่รับเงิน',
                   format: 'd/m/B',
                   allowBlank: false,
                   afterLabelTextTpl: required
                },
                {
                    xtype: 'combo',
                    fieldLabel: 'ผู้รับเงิน',
                    name: 'receiveOfficerName',
                    store: receiveOfficerNameStore,
                    queryMode: 'remote',
                    displayField: 'officerName',
                    valueField: 'officerId',
                    hiddenName: 'officerId',
                    triggerAction: 'all',
                    allowBlank: false,
                    afterLabelTextTpl: required
                }
              ,{
                    xtype: 'combo',
                    fieldLabel: 'ผู้ตรวจสอบ',
                    name: 'authority',
                    store: authorityStore,
                    queryMode: 'remote',
                    displayField: 'officerName',
                    valueField: 'officerId',
                    hiddenName: 'officerId',
                    triggerAction: 'all',
                    allowBlank: false,
                    afterLabelTextTpl: required
                }
               
            ],

            buttons: [{
                text: 'พิมพ์ใบเสร็จ',
                scope: this,
                action: 'printReceiptBtn'

            }, {
                text: 'ยกเลิก',
                // width: 150,
                scope: this,
                handler: this.onResetClick
            }]    
        });
        this.callParent(arguments);
    }
    ,onResetClick: function(){
        this.up('window').close();
    }

  		

		
});