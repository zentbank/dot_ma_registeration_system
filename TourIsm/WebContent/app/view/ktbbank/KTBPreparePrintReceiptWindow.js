Ext.define('tourism.view.ktbbank.KTBPreparePrintReceiptWindow', {
    extend: 'Ext.window.Window',
    alias : 'ktbbank-prepareprintreceipt-addeditwindow',

    requires: [
        'tourism.view.ktbbank.KTBPreparePrintReceiptForm'
    ],

   initComponent: function() {
            

        Ext.apply(this, {
            title:'พิมพ์ใบเสร็จรับเงิน',
           
            autoShow: true,
            modal: true,
            items :[
                {
                    xtype: 'ktbbank-prepareprintreceipt-form',
                    id: 'ktbbank-prepareprintreceipt-form-001',
                    frame: false,
                    border: false
                }
            ]
        });

        this.callParent(arguments);
    }




});
