Ext.define('tourism.view.ktbbank.ReceivePaymentForm',{
    extend: 'Ext.form.Panel'
    ,alias: 'widget.ktb-receipt-payment-form'
    ,requires: [
        'Ext.data.*'
        ,'Ext.form.*'
        ,'tourism.view.ktbbank.ReceivePaymentGrid'
    ]
    ,bodyPadding: 5
    ,autoScroll: true
    ,initComponent: function(){
    	
    	
	    Ext.apply(this,{	
		    bodyPadding: 10,
		    width: '100%',
            height: '100%',
		    items: [{
	        	xtype: 'container',
	        	// title: 'Payment',
	        	layout: 'anchor',
//	        	defaults: {
//	        		anchor: '100%',
//	        		labelWidth: 150
//	        	},
	            items: [{
                	xtype: 'hiddenfield',
                	name: 'progressDate'
                },
                {
                    xtype: 'hiddenfield',
                    name: 'paymentNo'
                    
                }
	            ,{
                    
                    xtype: 'ktbbank-receive-payment-grid'
                    ,id: 'ktbbank-receive-payment-grid-001'
                    ,border: false
                  ,frame: false   
                }
                ]
	        }]  
		    ,buttons: [{
	            text: 'บันทึกข้อมูลการชำระเงิน',
                id: 'ktb-receipt-payment-form-receivepayment-btn',
	            action: 'receivepayment'
	        }] 
	    });

        this.callParent(arguments);
    }
        
});



