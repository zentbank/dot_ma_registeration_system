Ext.define('tourism.view.ktbbank.ReceivePaymentGrid', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.ktbbank-receive-payment-grid',
	stripeRows : true,
	requires : [ 'Ext.grid.*', 'Ext.data.*', 'Ext.util.*', 'Ext.state.*',
			'Ext.form.*', 'Ext.grid.feature.Summary',
			'Ext.grid.feature.Grouping',
			'tourism.store.ktbbank.KTBPaymentStore',
			'tourism.model.ktbbank.KTBPaymentModel' ],

	initComponent : function() {

		var store = Ext.create('tourism.store.ktbbank.KTBPaymentStore', {
			storeId : 'ktbbank-readPaymentDetail-store',
			groupField : 'orgName',
			proxy : {
				type : 'ajax',
				actionMethods : {
					create : 'POST',
					read : 'POST',
					update : 'POST',
					destroy : 'POST'
				},
				api : {
					read : 'tourleader/ktb/payment/readPaymentDetail'

				},
				reader : {
					type : 'json',
					// metaProperty: '',
					root : 'list',
					// idProperty: 'emailId',
					totalProperty : 'totalCount',
					successProperty : 'success',
					messageProperty : 'message'
				},
				writer : {
					type : 'json',
					// encode: true,
					writeAllFields : true, ////just send changed fields
					// root: 'data',
					allowSingle : false, //always wrap in an array
					batch : false
				},
				listeners : {
					exception : function(proxy, response, operation) {

						Ext.MessageBox.show({
							title : 'REMOTE EXCEPTION',
							msg : operation.getError(),
							icon : Ext.MessageBox.ERROR,
							buttons : Ext.Msg.OK
						});
					}
				}
			}
		});

		Ext.apply(this, {
			store : store,
			features: [{
	            ftype: 'summary',
	            dock: 'bottom'
			},{
				ftype : 'grouping',
				groupHeaderTpl : '{name} ({rows.length} รายการ)',
				hideGroupedHeader : true,
				startCollapsed : false
			
			}],
			
			columns : [
			{
				header : 'เลขประจำตัวผู้เสียภาษี',
				dataIndex : 'DReference1',
				flex : 1
				,summaryType: 'count'
		        ,summaryRenderer: function(value){
		            return ' รวม';
		        }
			},
			// {
			// 	header : 'เลขที่ใบแจ้งชำระเงิน',
			// 	dataIndex : 'DReference2',
			// 	flex : 1,

			// },
			{
				header : 'ชื่อ',
				dataIndex : 'personName',
				flex : 2
			},
			
			{
				header : 'หน่วยงาน',
				dataIndex : 'orgName',
				flex : 1,

			},

			{
				header : 'รายการ',
				dataIndex : 'feeRegistrationType',
				flex : 2
			},
			
			{
				header : 'จำนวนเงิน',
				dataIndex : 'DTranAmt',
				flex : 1,
				align : 'right',
				renderer : function(value, metaData, record, rowIdx,
						colIdx, store, view) {
					return Ext.util.Format.number(value, '0,000.00');
				},
				summaryType : 'sum',
				summaryRenderer : function(value, summaryData,
						dataIndex) {
					return Ext.util.Format.number(value, '0,000.00'); //+ ' บาท';
				},
				field : {
					xtype : 'numberfield'

				}
			}]

		});

		this.callParent(arguments);
	}

});
