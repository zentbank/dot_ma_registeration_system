Ext.define('tourism.view.ktbbank.ReceivePaymentWindow', {
    extend: 'Ext.window.Window',
    alias : 'ktbbank-receive-payment-window',

    requires: [
        'tourism.view.ktbbank.ReceivePaymentGrid',
        'tourism.view.ktbbank.ReceivePaymentForm'
    ],

   initComponent: function() {
            

        Ext.apply(this, {
            title:'รับชำระเงิน',
            width : 800,
			height : 550,
			layout: 'fit',
            autoShow: true,
            modal: true,
            items :[
                {
                    xtype: 'ktb-receipt-payment-form',
                    id: 'ktb-receipt-payment-form-001',
                    frame: false,
                    border: false
                }
            ]
        });

        this.callParent(arguments);
    }




});
