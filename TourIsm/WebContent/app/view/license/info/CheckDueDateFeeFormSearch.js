Ext.define('tourism.view.license.info.CheckDueDateFeeFormSearch' ,{
	extend: 'Ext.form.FormPanel',
    alias : 'widget.license-info-duedate-fee-formsearch',
    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.Time',
        'Ext.form.CheckboxGroup',
        'Ext.layout.container.HBox',
        'Ext.form.FieldSet'
    ],
    bodyPadding: 10,
    items: [
        {
        	xtype: 'container',
            // title: 'Payment',
            layout: 'anchor',
            defaults: {
                anchor: '100%',
                labelWidth: 150
            },
            items: [
                {
                    xtype: 'hidden',
                    name: 'traderType',
                    value: 'B'
                },
                {
                    xtype: 'container',
                    layout: 'hbox',
                    layoutCongig: {
                         pack:'center',
                         align:'middle'
                    },
                    items: [
                        {
                            xtype: 'container',
                            // title: 'Payment',
                            flex: 1,
                            layout: 'anchor',
                            defaults: {
                                anchor: '100%',
                                labelWidth: 100
                            },
                            items: [
                                 {
                                    xtype: 'combo',
                                    fieldLabel: 'สถานะ',
                                    store: 'tourism.store.combo.ExpireDateStore',
                                    queryMode: 'remote',
                                    displayField: 'expireDueDateFeeStatusName',
                                    valueField: 'expireDueDateFeeStatus',
                                    hiddenName: 'expireDueDateFeeStatus',
                                    name :'expireDueDateFeeStatus'
                                }
                                ,{
         						    xtype: 'datefield',
         						    fieldLabel: 'วันที่',
         						    name: 'expireDateFrom',
         						    format: 'd/m/B'
         						} 
                               
                            ]
                        },{
                            xtype: 'splitter'
                        },
                        {
                            xtype: 'container',
                            flex: 1,
                            layout: 'anchor',
                            defaults: {
                                anchor: '100%',
                                labelWidth: 100
                            },
                            items: [
                                 ,{
                                    xtype: 'combo',
                                    fieldLabel: 'ภูมิภาค',
                                    store: 'tourism.store.combo.OrganizationStore',
                                    queryMode: 'remote',
                                    displayField: 'orgName',
                                    valueField: 'orgId',
                                    hiddenName: 'orgId',
                                    name :'orgId'
                                }
                                ,{
        						    xtype: 'datefield',
        						    fieldLabel: 'ถึงวันที่',
        						    name: 'expireDateTo',
        						    format: 'd/m/B'
        						}

                            ]
                        }
                    ]
                }
                
            ]
	    },
	    {
	        xtype: 'toolbar',
	        border: false,
	        padding: '6px 0 6px 0px',
	        items: [{
	            xtype: 'button',
	            text : 'ค้นหา',
	            action: 'searchBusinessInfoExpireDate'
	        },
            {
            	xtype: 'button',
                text : 'พิมพ์รายงาน',
                action: 'printReportBusinessInfoExpireDate'
            }]
	    }
    
    ]
            
}); 





