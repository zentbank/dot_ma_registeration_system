Ext.define('tourism.view.license.info.CheckDueDateFeeGrid',{
	extend: 'Ext.grid.GridPanel',
	alias : 'widget.license-info-duedate-fee-grid',
	stripeRows: true,
	requires: [
		'Ext.grid.Column',
		'Ext.toolbar.Paging',
		'Ext.grid.plugin.RowEditing',
		'Ext.grid.column.Template',
		'Ext.grid.column.Action',
		'Ext.grid.plugin.RowExpander',
		'Ext.grid.feature.Grouping',
		'tourism.store.registration.RegistrationStore',
		'tourism.view.license.info.DeactivateForm',
		'tourism.view.license.info.RefundGuaranteeForm'
	]
	,initComponent: function(){
		
		var infoRegistrationStore = Ext.create('tourism.store.registration.RegistrationStore',{

			storeId: this.storeId,
			pageSize : this.storepageSize,
			groupField: 'headerInfo',
			roleAction: this.roleAction,
            traderType: this.traderType,
            roleGroup: this.roleGroup,
			proxy: {
				type: 'ajax',
				actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
				api: {
				    read: this.storeUrl
				},
				reader: {
				    type: 'json',
				    root: 'list',
				    totalProperty: 'totalCount',
				    successProperty: 'success',
				    messageProperty: 'message'
				},
				listeners: {
				    exception: function(proxy, response, operation){

				        Ext.MessageBox.show({
				            title: 'REMOTE EXCEPTION',
				            msg: operation.getError(),
				            icon: Ext.MessageBox.ERROR,
				            buttons: Ext.Msg.OK
				        });
				    }
				}
			}	
			,listeners: {
				scope: this,
				// single: true,
				beforeload: function( store, operation, eOpts )
			        {				        	
//			        	var formId = '#license-info-formsearch-' + store.traderType+'-'+ store.roleAction;
						var formId = '#license-info-duedate-formsearch-' + store.traderType+'-'+ store.roleAction;
			        
			        	var formSearch = Ext.ComponentQuery.query(formId)[0];
			        	var values = formSearch.getValues();
							
			        	 for (field in values) 
					      {
					        if (Ext.isEmpty(values[field])) 
					        {
					          delete values[field];
					        }
					        
					      }
			        	 console.log(values);
					    store.proxy.extraParams = values;
			        }
			}
			
		});
		
		Ext.apply(this, {
			store: infoRegistrationStore
			,columns: [
			{
				header: 'regId',
				dataIndex: 'regId',
				hidden: true
			},
			{
				header: 'ชื่อ',
				dataIndex: 'traderOwnerName',
				flex: 2
			},
			{
				header: 'ชื่อธุรกิจนำเที่ยว(TH)',
				dataIndex: 'traderName',
				flex: 2
			},
			{
				header: 'ชื่อธุรกิจนำเที่ยว(EN)',
				dataIndex: 'traderNameEn',
				flex: 2
			},
			{
				header: 'ประเภท',
				dataIndex: 'traderCategoryName',
				flex: 2
			},
			{
				header: 'ประวัติการจดทะเบียน',
				dataIndex: 'registrationTypeName',
				flex: 2
			},
			{
				header: 'วันที่ทำรายการ',
				dataIndex: 'registrationDate',
				flex: 2
			},
			{
				header: 'วันที่ออกใบอนุญาต',
				dataIndex: 'approveDate',
				flex: 1
			},
			{
				header: 'วันครบกำหนด',
				dataIndex: 'expireDate',
				flex: 1
			},
			{
				header: 'สถานะใบอนุญาต',
				dataIndex: 'licenseStatus',
				// xtype: 'actioncolumn',
				flex: 1,
				renderer: function(value, metaData, model){
					if(value == "S")
					{
						return '<span class="tour-fg-color-redLight">พักใช้ใบอนุญาต</span>';
					}
					if(value == "R")
					{
						return '<span class="tour-fg-color-redLight">เพิกถอนใบอนุญาต</span>';
					}
					if(value == "C")
					{
						return '<span class="tour-fg-color-redLight">ยกเลิก</span>';
					}
					if(value == "N")
					{
						return '<span class="tour-fg-color-green">ปกติ</span>';
					}
					if(value == "D")
					{
						return '<span class="tour-fg-color-lighten">ไม่ผ่านการตรวจสอบ/ยกเลิกการจดทะเบียน</span>';
					}
					if(value == "H")
					{
						return '<span class="tour-fg-color-lighten">ประวัติการจดทะเบียน</span>';
					}
					//Oat Add 17/09/57
					if(value == "T")
					{
						return '<span class="tour-fg-color-green">อยู่ระหว่างจดทะเบียน</span>';
					}
					//End Oat Add 17/09/57
					
				}

			}
//			, {
//	        	header: '',
//	            xtype: 'actioncolumn',
//	           	width:50,
//	           	align : 'center',
//	            items: [{
////	                iconCls: 'icon-edit-xsmall',
//	            	iconCls: 'icon-notes-xsmall',
//	                tooltip: 'รายละเอียด',
//		            handler : function(grid, rowIndex, colIndex, node, e, model, rowNode) {
//
//		            	if(model.get('roleAction') != 'C')
//		            	{
//		            		this.fireEvent('itemclick', this, 'editlicense', grid, rowIndex, colIndex, model, node);
//		            	}
//		            	else
//		            	{
//		            		this.fireEvent('itemclick', this, 'cancelLicense', grid, rowIndex, colIndex, model, node);
//		            	}
//		            	
//		            }
//	            }]
//	        }
//			//Oat Add 24/09/57
//			, {
//	        	header: '',
//	            xtype: 'actioncolumn',
//	           	width:50,
//	           	align : 'center',
//	            items: [{
////	            	iconCls: 'icon-notes-xsmall',
////	                tooltip: 'พิมพ์รายละเอียด',
//	            	getClass: function(v, meta, rec) {          
//	                    if (rec.get('licenseStatus') != 'H') {
//	                        this.items[0].tooltip = 'พิมพ์รายละเอียด';
//	                        return 'icon-checked-xsmall';
//	                    } else {
//	                        this.items[0].tooltip = '';
//	                        return '';
//	                    }
//	                },
//		            handler : function(grid, rowIndex, colIndex, node, e, model, rowNode) {
//
//		            	this.fireEvent('itemclick', this, 'printLicenseInfo', grid, rowIndex, colIndex, model, node);
//		            	
//		            }
//	            }]
//	        }
			//End Oat Add 24/09/57
			]
		
			,dockedItems:[
			{
				xtype: 'pagingtoolbar',
				store: infoRegistrationStore,
				dock: 'bottom',
				displayInfo: true

			}
			]
			
		});
		
		this.callParent(arguments);
		
	}	
//    ,features: [{
//        ftype: 'grouping',
//        groupHeaderTpl: '{name}',
//        hideGroupedHeader: true,
//        startCollapsed: false
//    }]
	
});











