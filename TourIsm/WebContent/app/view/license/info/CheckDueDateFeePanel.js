Ext.define('tourism.view.license.info.CheckDueDateFeePanel', {
	extend: 'Ext.container.Container',
	requires:[
          'Ext.tab.Panel',
          'Ext.layout.container.Border',
          'tourism.view.license.info.CheckDueDateFeeFormSearch'
          ,'tourism.view.license.info.CheckDueDateFeeGrid'
	],        
	
	xtype: 'license-info-duedate-fee-panel',
	initComponent: function(){
		
		var formSearch = this.getBusinessFormSearch(this.traderType ,this.roleAction);
		
		Ext.apply(this, {
    		layout: {
		        type: 'border'
		    }
		    ,items: [
		    {
		        region: 'north',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items:[formSearch]
		    }
		    ,{
		        region: 'center',
		        xtype: 'panel',
		        layout: 'fit',
		        frame: false,
		        border: false,
		        items:[{
		            xtype: 'license-info-duedate-fee-grid',
		            id: this.roleAction+'-'+this.traderType+'-license-info-duedate-fee-grid',
		            autoScroll: true,
		            border: false,
		            storeId: this.roleAction+'-'+this.traderType+'-license-info-duedate-fee-store',
		            storepageSize: 20,
		            storeUrl: this.storeUrl,
		            roleAction: this.roleAction,
		            traderType: this.traderType,
		            roleGroup: this.roleGroup
		            
		        }]
		    }
		    ]
    	});
		
		this.callParent(arguments);
		
	}
	,getBusinessFormSearch: function(traderType, roleAction)
	{
		return {
			
	            title:'เช็ควันชำระค่าธรรมเนียม',
	            collapsible: true,   // make collapsible
	            xtype: 'license-info-duedate-fee-formsearch',
	            id: 'license-info-duedate-formsearch-' + traderType+'-'+ roleAction,
	            frame: false,
	            border: false
		};
	}

});










