Ext.define('tourism.view.license.info.DeactivateForm', {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.data.*',
        'Ext.form.*',
        'Ext.tab.Tab',
        'tourism.view.deactivate.DeactivateTypeGrid'
    ],
    alias: 'widget.license-info-deactivate-form',
    // title: 'ธุรกิจนำเที่ยว',
    bodyPadding: 5,
    autoScroll: true,

    initComponent: function(){

        var licenseDetail;

        if(this.traderType == 'B')
        {
            licenseDetail = this.businessDetail();
        }
        else
        {
            licenseDetail = this.personDetail();
        }
        
        Ext.apply(this, {
            width: '100%',
            height: '100%',
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },

            items: 
            [ 
                {
                    xtype: 'container',
                    // title: 'Payment',
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%',
                        labelWidth: 250
                    },
                    items: 
                    [ 
                        {
                            xtype: 'container',
                            layout: 'hbox',
                            margin: '0 0 5 0',
                            items: [
                                {
                                    xtype: 'hiddenfield',
                                    name: 'deactiveId'
                                },
                                {
                                    xtype: 'hiddenfield',
                                    name: 'traderId'
                                },
                                {
                                    xtype: 'displayfield',
                                    fieldLabel: 'เลขที่รับเรื่อง',
                                    name: 'deactivateNo'
                                },{
                                    xtype: 'splitter'
                                },
                                {
                                    xtype: 'displayfield',
                                    fieldLabel: 'วันที่รับเรื่อง',
                                    name: 'deactivateDate'
                                },{
                                    xtype: 'splitter'
                                },
                                {
                                    xtype: 'displayfield',
                                    fieldLabel: 'ผู้รับเรื่อง',
                                    name: 'officerName'
                                }
                            ]
                        }, 
                        // รายละเอียด license
                        licenseDetail,
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'สาเหตุการยกเลิก',
                            name: 'deactivateTypeName'
                        },
                        
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'หมายเหตุ',
                            name: 'deactivateResonRemark'
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'วันที่อนุมัติ',
                            name: 'bookDate'
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'เลขที่หนังสือ',
                            name: 'deactivateBookNo'
                        }
                        //  ,{
                        //     xtype: 'radiogroup',
                        //     columns: 1,
                        //     vertical: true,
                        //     items: [
                        //         { boxLabel: 'ประสงค์ยื่นคำขอรับใบอนุญาตฯใหม่ต่อไป', name: 'renewLicenseStatus', inputValue: '1' },
                        //         { boxLabel: 'ไม่ประสงค์ยื่นคำขอรับใบอนุญาตฯใหม่', name: 'renewLicenseStatus', inputValue: '2', checked: true }
                               
                        //     ]
                        // }
                    ]
                }
            ]  
        });
        this.callParent(arguments);
    },
    businessDetail: function()
    {
        return {
                    xtype: 'container',
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%',
                        labelWidth: 250
                    },
                    margin: '0 0 5 0',
                    items: [
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'เลขที่ใบอนุญาต',
                            name: 'licenseNo'
                           
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'ชื่อประกอบธุรกิจนำเที่ยว',
                            name: 'traderName'
                           
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'ชื่อประกอบธุรกิจนำเที่ยว(ภาษาต่างประเทศ)',
                            name: 'traderNameEn'
                           
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'ผู้ขอจดทะเบียน',
                            name: 'traderOwnerName'
                           
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'เลขที่นิติบุคคล',
                            name: 'identityNo'
                           
                        },
                        
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'ประเภทธุรกิจนำเที่ยว',
                            name: 'traderCategoryName'
                           
                        }
                    ]
                };
    }
    ,personDetail: function()
    {
        return {
                    xtype: 'container',
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%',
                        labelWidth: 250
                    },
                    margin: '0 0 5 0',
                    items: [
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'เลขที่ใบอนุญาต',
                            name: 'licenseNo'
                           
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'ผู้ขอจดทะเบียน',
                            name: 'traderOwnerName'
                           
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'เลขที่นิติบุคคล',
                            name: 'identityNo'
                           
                        },
                        
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'ประเภทการจดทะเบียน',
                            name: 'traderCategoryName'
                           
                        }
                    ]
                };
    },onResetClick: function(){
        this.up('window').close();
    }
    
});