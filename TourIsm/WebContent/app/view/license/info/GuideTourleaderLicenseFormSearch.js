Ext.define('tourism.view.license.info.GuideTourleaderLicenseFormSearch' ,{
    extend: 'Ext.form.FormPanel',
    alias : 'widget.license-info-guidetourleader-formsearch',
    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.Time',
        'Ext.form.CheckboxGroup',
        'Ext.layout.container.HBox',
        'Ext.form.FieldSet'
    ],
    bodyPadding: 10,
    initComponent: function(){
        Ext.apply(this, {
            items: 
            [
                {
                    xtype: 'container',
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%',
                        labelWidth: 150
                    },
                    items: [
                        {
                            xtype: 'hidden',
                            name: 'traderType',
                            value: 'G'
                        },
                        {
                            xtype: 'container',
                            layout: 'hbox',
                            layoutCongig: {
                                 pack:'center',
                                 align:'middle'
                            },
                            items: [
                                {
                                    xtype: 'container',
                                    // title: 'Payment',
                                    flex: 1,
                                    layout: 'anchor',
                                    defaults: {
                                        anchor: '100%',
                                        labelWidth: 150
                                    },
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            fieldLabel: 'เลขที่ใบอนุญาต',
                                            name: 'licenseNo',
                                            allowBlank: true
                                        },
                                        {
                                           xtype: 'textfield',
                                            fieldLabel: 'ชื่อ(ภาษาไทย)',
                                           name: 'firstName',
                                           allowBlank: true
                                       },
                                       {
                                           xtype: 'textfield',
                                            fieldLabel: 'ชื่อ(ภาษาอังกฤษ)',
                                           name: 'firstNameEn',
                                           allowBlank: true
                                       }
                                        ,{
                                              xtype: 'combo',
                                              fieldLabel: 'สถานะใบอนุญาต',
                                              store: 'tourism.store.combo.LicenseStatusStore',
                                              queryMode: 'local',
                                              displayField: 'licenseStatusName',
                                              valueField: 'licenseStatus',
                                              hiddenName: 'licenseStatus',
                                              name :'licenseStatus'
                                        }
                                    ]
                                },{
                                    xtype: 'splitter'
                                },
                                {
                                    xtype: 'container',
                                    flex: 1,
                                    layout: 'anchor',
                                    defaults: {
                                        anchor: '100%',
                                        labelWidth: 200
                                    },
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            fieldLabel: 'หมายเลขบัตรประชาชน',
                                            name: 'identityNo'
                                        },
                                         {
                                           // นามสกุล
                                           xtype: 'textfield',
                                           fieldLabel: 'นามสกุล(ภาษาไทย)',
                                           name: 'lastName',
                                           labelAlign: 'left'
                                        },
                                        {
                                            // นามสกุล
                                            xtype: 'textfield',
                                            fieldLabel: 'นามสกุล(ภาษาอังกฤษ)',
                                            name: 'lastNameEn',
                                            labelAlign: 'left'
                                        },
                                         ,{
                                            xtype: 'combo',
                                            fieldLabel: 'ภูมิภาค',
                                            store: 'tourism.store.combo.OrganizationStore',
                                            queryMode: 'remote',
                                            displayField: 'orgName',
                                            valueField: 'orgId',
                                            hiddenName: 'orgId',
                                            name :'orgId'
                                        }
                                    ]
                                }
                            ]
                        }
                        
                    ]
                },
                {
                    xtype: 'toolbar',
                    border: false,
                    padding: '6px 0 6px 0px',
                    items: [{
                        xtype: 'button',
                        text : 'ค้นหา',
                        action: 'searchGuideTourleaderLicense'
                    }]
                }

            ]
        });

        this.callParent();
    }
    ,afterRender: function()
    {
        this.callParent(arguments);
        //add registration
        if(!Ext.isEmpty(this.traderType) )
        {
            if(this.traderType == 'guide')
            {
                this.getForm.findfield('traderType').setValue('G');
            }
            else
            {
                this.getForm.findfield('traderType').setValue('L');
            }
            
        }
    }
});    
