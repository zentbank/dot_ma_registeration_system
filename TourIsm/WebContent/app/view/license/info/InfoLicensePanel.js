Ext.define('tourism.view.license.info.InfoLicensePanel', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
        'tourism.view.license.info.TourisLicenseTourisFormSearch',
        'tourism.view.license.info.GuideTourleaderLicenseFormSearch',
        'tourism.view.license.info.InfoLicenseGrid'


    ],
    
    xtype: 'license-info-tour-guide-tourleader-panel',
    initComponent: function(){

    	var formSearch = this.getBusinessFormSearch(this.traderType ,this.roleAction);

    	if(this.traderType == 'guide')
    	{
    		formSearch = this.getGuideFormSearch(this.traderType, this.roleAction);
    	}
    	
    	if(this.traderType == 'tourleader')
    	{
    		formSearch = this.getTourleaderFormSearch(this.traderType ,this.roleAction);
    	}
    	Ext.apply(this, {
    		layout: {
		        type: 'border'
		    }
		    ,items: [
		    {
		        region: 'north',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items:[formSearch]
		    }
		    ,{
		        region: 'center',
		        xtype: 'panel',
		        layout: 'fit',
		        frame: false,
		        border: false,
		        items:[{
		            xtype: 'license-info-touris-guide-tourleader-grid',
		            id: this.roleAction+'-'+this.traderType+'-license-info-touris-guide-tourleader-grid',
		            autoScroll: true,
		            border: false,
		            storeId: this.roleAction+'-'+this.traderType+'-license-info-touris-guide-tourleader-store',
		            storepageSize: 20,
		            storeUrl: this.storeUrl,
		            roleAction: this.roleAction,
		            traderType: this.traderType,
		            roleGroup: this.roleGroup
		            
		        }]
		    }
		    ]
    	});

    	this.callParent(arguments);
    }
    ,getBusinessFormSearch: function(traderType, roleAction)
    {
    	return {
    		
	            title:'ค้นหาทะเบียนใบอนุญาตธุรกิจนำเที่ยว',
	            collapsible: true,   // make collapsible
	            xtype: 'license-info-touris-formsearch',
	            id: 'license-info-formsearch-' + traderType+'-'+ roleAction,
	            frame: false,
	            border: false
    	};
    }
    ,getGuideFormSearch: function(traderType, roleAction)
    {
    	return {
    		
	            title:'ค้นหาทะเบียนใบอนุญาตมัคคุเทศก์',
	            collapsible: true,   // make collapsible
	            xtype: 'license-info-guidetourleader-formsearch',
	            id: 'license-info-formsearch-' + traderType+'-'+ roleAction,
	            frame: false,
	            border: false
    	};
    }
    ,getTourleaderFormSearch: function(traderType, roleAction){
    	return {
    		
            title:'ค้นหาทะเบียนใบอนุญาตผู้นำเที่ยว',
            collapsible: true,   // make collapsible
            xtype: 'license-info-guidetourleader-formsearch',
            id: 'license-info-formsearch-' + traderType+'-'+ roleAction,
            frame: false,
            border: false
    	};
    }
});