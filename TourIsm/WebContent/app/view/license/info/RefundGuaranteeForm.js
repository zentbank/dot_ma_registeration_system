Ext.define('tourism.view.license.info.RefundGuaranteeForm', {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.data.*',
        'Ext.form.*',
        'Ext.tab.Tab',
        'tourism.view.refundguarantee.BankGuarantee',
        'tourism.view.refundguarantee.CashGuarantee',
        'tourism.view.refundguarantee.GovermentBound'
    ],
    alias: 'widget.license-info-refundguarantee-form',
    // title: 'ธุรกิจนำเที่ยว',
    bodyPadding: 5,
    // autoScroll: true,

    initComponent: function(){

        var licenseDetail = this.businessDetail();
          
        Ext.apply(this, {
            width: '100%',
            height: '100%',
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },

            items: 
            [ 
                {
                    xtype: 'container',
                    // title: 'Payment',
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%',
                        labelWidth: 200
                    },
                    items: 
                    [ 
                        {
                            xtype: 'hiddenfield',
                            name: 'guaranteeId'
                        },
                        // รายละเอียด license
                        licenseDetail,
                        {
                            xtype: 'fieldset',
                            layout: 'anchor',
                            // title: 'บันทึกคืนหลักประกัน',
                            defaults: {
                                anchor: '100%',
                                labelWidth: 200
                            },
                            // margin: '0 0 5 0',
                            items: [
                                
                                {
                                    xtype: 'displayfield',
                                    fieldLabel: 'ชื่อผู้รับหลักประกันคืน',
                                    name: 'refundName'
                                   
                                },
                                {
                                    xtype: 'radiogroup',
                                    fieldLabel: 'สถานะผู้รับคืน',
                                    columns: 1,
                                    vertical: true,
                                    items: [
                                        { boxLabel: 'กรรการบริษัท', name: 'refundType', inputValue: 'C' , checked: true },
                                        { boxLabel: 'ผู้รับมอบอำนาจ', name: 'refundType', inputValue: 'T'}
                                       
                                    ]
                                },
                                {
                                    xtype: 'displayfield',
                                    fieldLabel: 'จำนวนเงินคืน',
                                    name: 'refundMny'
                                   
                                },
                                
                                {
                                    xtype: 'displayfield',
                                    fieldLabel: 'ดอกผล',
                                    name: 'refundInterest'
                                   
                                },
                                
                                {
                                    xtype: 'displayfield',
                                    fieldLabel: 'วันที่รับหลักประกันคืน',
                                    name: 'refundDate'
                                   
                                },
                                {
                                    xtype: 'radiogroup',
                                    fieldLabel: 'สถานะหลักประกัน',
                                    columns: 1,
                                    vertical: true,
                                    items: [
                                        { boxLabel: 'ยังไม่มารับหลักประกันคืน', name: 'guaranteeStatus', inputValue: 'N' , checked: true },
                                        { boxLabel: 'คืนหลักประกัน', name: 'guaranteeStatus', inputValue: 'R'}
                                       
                                    ]
                                },
                                {
                                    xtype: 'displayfield',
                                    name: 'refundRemark',
                                    fieldLabel: 'หมายเหตุ'
                                }
                            ]
                        }
                        
                                              
                    ]
                }
            ]  
        });
        this.callParent(arguments);
    },
    businessDetail: function()
    {
        return {
                    xtype: 'fieldset',
                    layout: 'anchor',
                    title: 'รายละเอียด',
                    defaults: {
                        anchor: '100%',
                        labelWidth: 200
                    },
                    margin: '0 0 5 0',
                    items: [
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'เลขที่ใบอนุญาต',
                            name: 'licenseNo'
                           
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'ชื่อประกอบธุรกิจนำเที่ยว',
                            name: 'traderName'
                           
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'ชื่อประกอบธุรกิจนำเที่ยว</br>(ภาษาต่างประเทศ)',
                            name: 'traderNameEn'
                           
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'ผู้ขอจดทะเบียน',
                            name: 'traderOwnerName'
                           
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'เลขที่นิติบุคคล',
                            name: 'identityNo'
                           
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'ประเภทธุรกิจนำเที่ยว',
                            name: 'traderCategoryName'
                           
                        }
                        ,{
                            xtype: 'refundguarantee-cashguarantee-fieldser'
                           
                        }
                        ,{
                            xtype: 'refundguarantee-bankGuarantee-fieldser'
                           
                        }
                        ,{
                            xtype: 'refundguarantee-governmentbound-fieldser'
                           
                        }
                         ,
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'จำนวนเงินหลักประกันรวม',
                            name: 'guaranteeMny'
                           
                        }
                         ,
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'ดอกเบี้ย',
                            name: 'guaranteeInterest'
                           
                        }
                    ]
                };
    }
    
    
});