Ext.define('tourism.view.license.info.TourisLicenseTourisFormSearch' ,{
    extend: 'Ext.form.FormPanel',
    alias : 'widget.license-info-touris-formsearch',
    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.Time',
        'Ext.form.CheckboxGroup',
        'Ext.layout.container.HBox',
        'Ext.form.FieldSet'
    ],
    bodyPadding: 10,
    items: [
        {
                xtype: 'container',
                // title: 'Payment',
                layout: 'anchor',
                defaults: {
                    anchor: '100%',
                    labelWidth: 150
                },
                items: [
                    {
                        xtype: 'hidden',
                        name: 'traderType',
                        value: 'B'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'เลขที่ใบอนุญาต',
                        name: 'licenseNo',
                        anchor: '50%'
                    },
                    
                    {
                        xtype: 'container',
                        layout: 'hbox',
                        layoutCongig: {
                             pack:'center',
                             align:'middle'
                        },
                        items: [
                            {
                                xtype: 'container',
                                // title: 'Payment',
                                flex: 1,
                                layout: 'anchor',
                                defaults: {
                                    anchor: '100%',
                                    labelWidth: 150
                                },
                                items: [
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'ชื่อธุรกิจนำเที่ยว(TH)',
                                        // labelWidth: 150,
                                        name: 'traderName'
                                    }
                                    ,
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'ชื่อผู้ขอใบอนุญาต',
                                        // labelWidth: 150,
                                        name: 'firstName'
                                    }
                                    ,{
                                          xtype: 'combo',
                                          fieldLabel: 'สถานะใบอนุญาต',
                                          store: 'tourism.store.combo.LicenseStatusStore',
                                          queryMode: 'local',
                                          displayField: 'licenseStatusName',
                                          valueField: 'licenseStatus',
                                          hiddenName: 'licenseStatus',
                                          name :'licenseStatus'
                                    }
                                ]
                            },{
                                xtype: 'splitter'
                            },
                            {
                                xtype: 'container',
                                flex: 1,
                                layout: 'anchor',
                                defaults: {
                                    anchor: '100%',
                                    labelWidth: 200
                                },
                                items: [
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'ชื่อธุรกิจนำเที่ยว(EN)',
                                        // labelWidth: 150,
                                        name: 'traderNameEn'
                                    },
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'เลขนิติบุคคล/เลขที่บัตรประชาชน',
                                        // labelWidth: 150,
                                        name: 'identityNo'
                                    }
                                     ,{
                                        xtype: 'combo',
                                        fieldLabel: 'ภูมิภาค',
                                        store: 'tourism.store.combo.OrganizationStore',
                                        queryMode: 'remote',
                                        displayField: 'orgName',
                                        valueField: 'orgId',
                                        hiddenName: 'orgId',
                                        name :'orgId'
                                    }
                                ]
                            }
                        ]
                    }
                    
                ]
        },
        {
            xtype: 'toolbar',
            border: false,
            padding: '6px 0 6px 0px',
            items: [{
                xtype: 'button',
                text : 'ค้นหา',
                action: 'searchBusinessInfo'
            }]
        }

    ]
});    
