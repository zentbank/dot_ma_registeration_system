Ext.define('tourism.view.menu.TreeMenu' ,{
    extend: 'Ext.tree.Panel',
    alias : 'widget.treemenu',
    requires: [
        'Ext.data.TreeStore',
        'Ext.selection.TreeModel',
        'Ext.tree.Column',
        'Ext.tree.View',
        'tourism.store.menu.MenuStore'
    ],
    title: 'Simple Tree',
    padding: '5px 5px 0px 5px',
    // width: 200,
    // height: 150,
    store: 'tourism.store.menu.MenuStore',
    rootVisible: true,
    root: 'test',
    listeners: {
        load: {
            // element: 'el', //bind to the underlying el property on the panel
            fn: function( treeStore, node, records, successful, eOpts )
            {
            
                // for(var index in records)
                // {
                //     //console.log(records[index]);
                // }

                // var root = treeStore.getRootNode(),
                // tree = Ext.ComponentQuery.query('treemenu')[0];
                // selModel  = tree.getSelectionModel();

                // var index = root.indexOfId(35);
                // var infoNode = root.getChildAt(index);

                // // var nodefound = root.findChild('menuId', 'KTB_PAYMENT');

                // //console.log(infoNode);

                // selModel.select(infoNode);
                // tree.fireEvent('itemselect', infoNode);

                // selModel.select(root.firstChild);

                // tree.fireEvent('itemselect', root.firstChild);
            }
        }
    }

});    
