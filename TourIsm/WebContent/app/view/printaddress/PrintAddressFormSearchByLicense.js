Ext.define('tourism.view.printaddress.PrintAddressFormSearchByLicense',{
	extend: 'Ext.form.FormPanel',
	requires:[
				'Ext.form.field.Date',
				'Ext.form.field.Time',
				'Ext.form.CheckboxGroup',
				'Ext.layout.container.HBox',
				'Ext.form.FieldSet'
	          ],
	
	alias: 'widget.printaddress-formsearchbylicense',
	
	bodyPadding: 10,
	items: [
            {
        xtype: 'container',
        // title: 'Payment',
        layout: 'anchor',
        defaults: {
            anchor: '100%',
            labelWidth: 150
        },
        items: [
                {
                    xtype: 'container',
                    // title: 'Payment',
                    // flex: 1,
                    border: false,
                    frame: false,
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%',
                        labelWidth: 150
                    },
                    items: [
                        {
                            // html: 'กดปุ่มเพิ่มเพื่อกรอกข้อมูลเลขที่ใบอนุญาต'
                        }
                    ]
                }
        ]
    },
    {
        xtype: 'toolbar',
        align:'center',
        border: false,
        padding: '10px 0 10px 0px',
        items: [{
        	xtype: 'button',
            text : 'เพิ่ม',
            // action: 'searchPrintAddressBetweenRegistration'
            handler: function(){
                var form1 = this.up('form');

                var textLicense = Ext.create( 'Ext.form.field.Text',
                { 
                    fieldLabel: 'เลขที่ใบอนุญาต',
                    name: 'licenseNo'

                });

                form1.add(textLicense);


            }
        },
        {
            xtype: 'button',
            text : 'พิมพ์ที่อยู่',
            action: 'printAddressFormSearchByLicense'
        }]
    }

]
});