Ext.define('tourism.view.printaddress.PrintAddressPanel',{
	extend: 'Ext.container.Container',
	xtype: 'printaddress-panel',
	requires:[
	          'Ext.tab.Panel',
	          'Ext.layout.container.Border',
	          'tourism.view.printaddress.PrintAddressFormSearch',
	          'tourism.view.printaddress.PrintAddressFormSearchByLicense'
	],
	initComponent: function()
	{
		var formSearch = this.getGuideFormSearch(this.roleAction);
				
		Ext.apply(this,{
			layout: {
		        type: 'border'
		    },

		    items: [{
		        region: 'center',
		        xtype: 'panel',
		        frame: false,
		        // 
		        border: false,
		        items:[
		        {
			        // Fieldset in Column 1 - collapsible via toggle button
			        xtype:'fieldset',
			        title: 'พิมพ์ที่อยู่แบบช่วงเลขที่',
			        collapsible: true,
			        defaultType: 'textfield',
			        padding: '5px 5px 0px 5px',
			        defaults: {anchor: '100%'},
			        layout: 'anchor',
			        items :[formSearch]
			    },
			    {
			        // Fieldset in Column 1 - collapsible via toggle button
			        xtype:'fieldset',
			        title: 'พิมพ์ที่อยู่แบบระบุเอง',
			        collapsible: true,
			        defaultType: 'textfield',
			       	padding: '5px 5px 0px 5px',
			        defaults: {anchor: '100%'},
			        layout: 'anchor',
			        items :[{
    		
				            xtype: 'printaddress-formsearchbylicense',
				            id: this.roleAction+'-formsearchbylicense',
				            padding: '5px 5px 0px 5px',
				            frame: false,
				            border: false
			    	}]
			    }]
		    }]
		});
		this.callParent(arguments);
	},
	getGuideFormSearch: function(roleAction)
    {
    	return {
    		
	            // title:'ค้นหาการทำรายการพิมพ์ที่อยู่',
	            // collapsible: true,   // make collapsible
	            // collapsed : true,
	            xtype: 'printaddress-formsearch',
	            id: roleAction+'-formsearch',
	            padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false
    	};
    }
});