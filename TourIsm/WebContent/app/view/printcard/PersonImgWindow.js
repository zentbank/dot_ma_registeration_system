Ext.define('tourism.view.printcard.PersonImgWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.printcard-person-img-window',

    requires: [
    'tourism.view.printcard.ViewCardForm'
    ],
    
    initComponent: function() {
     

        Ext.apply(this, {
            title: 'ใบอนุญาต',
            // layout: 'fit',
            layout: 'card',
            activeItem: 0,
            autoShow: true,
            width: 800,
            height: 600,
            modal: true,
            items :[
            {
                // title: 'เลือกใบอนุญาต',
                xtype: 'printcard-viewcardform',
                id: 'printcard-'+this.roleAction+'-'+this.traderType+'-viewcardform',
                border: false,
                frame: false
            }, {
                html: 'card2'
            }]
        });

        this.callParent(arguments);
    },

});
