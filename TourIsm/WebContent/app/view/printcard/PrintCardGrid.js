Ext.define('tourism.view.printcard.PrintCardGrid',{
	extend: 'Ext.grid.GridPanel',
	alias: 'widget.printcard-grid',
	requires: [
				'Ext.grid.Column',
				'tourism.store.registration.RegistrationStore',
				'Ext.toolbar.Paging',
				'Ext.grid.plugin.RowEditing',
				'Ext.grid.column.Template',
				'Ext.grid.column.Action',
				'Ext.grid.plugin.RowExpander',
				'Ext.grid.feature.Grouping',
				'tourism.view.printcard.PersonImgWindow'
			],
	 
	initComponent: function() {
		var printCardStore = Ext.create('tourism.store.printcard.PrintCardGuideStore',{
			storeId: this.storeId,
			pageSize : this.storepageSize,
			groupField: 'headerInfo',
			roleAction: this.roleAction,
		    traderType: this.traderType ,
			proxy: {
				type: 'ajax',
				actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
				api: {
				    read: this.storeUrl
				},
				reader: {
				    type: 'json',
				    root: 'list',
				    totalProperty: 'totalCount',
				    successProperty: 'success',
				    messageProperty: 'message'
				},
				listeners: {
				    exception: function(proxy, response, operation){

				        Ext.MessageBox.show({
				            title: 'REMOTE EXCEPTION',
				            msg: operation.getError(),
				            icon: Ext.MessageBox.ERROR,
				            buttons: Ext.Msg.OK
				        });
				    }
				}
			}
			,listeners: {
				scope: this,
				// single: true,
				beforeload: function( store, operation, eOpts )
			        {
			        	var formId = '#' + store.roleAction+'-'+store.traderType+'-printcard-formsearch';
			        
			        	var formSearch = Ext.ComponentQuery.query(formId)[0];
			        	var values = formSearch.getValues();
							
			        	 for (field in values) 
					      {
					        if (Ext.isEmpty(values[field])) 
					        {
					          delete values[field];
					        }
					        
					      }
					    store.proxy.extraParams = values;
			        }
			}
		});
		
    	columns = this.getGuideColumn();
    	//store = printCardGuideStore;
    	if(this.traderType == 'tourleader')
    	{
    		columns = this.getTourLeaderColumn();
    		//store = printCardTourLeaderStore;
    	}
		
		Ext.apply(this,{
			//store: registrationStore,
			store : printCardStore,
			dockedItems:[
				{
					xtype: 'pagingtoolbar',
					store: printCardStore,
					//store : store,
					dock: 'bottom',
					displayInfo: true

				}
			],
			viewConfig: {
				stripeRows:true ,
				getRowClass: function(record, index) {                            
					if (record.get('roleColor') == 'true') {
					    return "online-block";
					}
				}
			},
			columns: columns
		});

		this.callParent(arguments);
	},
    getGuideColumn: function(){
    	return [{
			header: 'regId',
			dataIndex: 'regId',
			hidden: true
		},{
            text     : 'รูป',
            flex     : 1,
            sortable : true,
            dataIndex: 'imgData',
            renderer: function(value, metaData, gridmodel, rowIndex,colIndex,gridstore,gridview){
            	var dated = new Date();
        		return '<img src="'+value+'?dc_='+dated.getTime()+'" height="100" width="100" />';
		    }
        },
		{
			header: 'เลขที่ใบอนุญาต',
			dataIndex: 'licenseNo',
			flex: 1
		},
		{
			header: 'ชื่อ-นามสกุล',
			dataIndex: 'traderOwnerName',
			flex: 1
		},
		
		{
			header: 'ประเภทใบอนุญาต',
			dataIndex: 'traderCategoryName',
			flex: 1
		},
		{
			header: 'ประเภทการจดทะเบียน',
			dataIndex: 'registrationTypeName',
			flex: 1.25
		},
		{
			header: 'วันที่อนุมัติ',
			dataIndex: 'approveDate',
			flex: 0.75
		},{
        	header: 'รายละเอียด',
            xtype: 'actioncolumn',
           	//width:50,
           	align : 'center',
            items: [{
                iconCls: 'icon-edit-xsmall',
                tooltip: 'รายละเอียด',
	            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {

	            		this.fireEvent('itemclick', this, 'viewPrintCardguide', grid, rowIndex, colIndex, record, node);

	            }
            }]
        }, {
        	header: 'ถ่ายรูปมัคคุเทศก์',
        	xtype: 'actioncolumn',
        	width:30,
            align : 'center',
            items: [{
            	 getClass: function(v, meta, rec) {          
                      return 'icon-adduser-xsmall';
                  },
                iconCls: 'icon-checked-xsmall',
                tooltip: 'ดูรูปภาพ',
	            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {

	            	this.fireEvent('itemclick', this, 'takepicview', grid, rowIndex, colIndex, record, node);
	            	// if (record.get('imgAction') == 'P')
	            	// {
	            	// 	this.fireEvent('itemclick', this, 'viewfile', grid, rowIndex, colIndex, record, node);
	            	// }
	        //     	var win = Ext.create('tourism.view.printcard.PersonImgWindow',{
			      //     	id: 'printcard-'+grid.roleAction+'-'+grid.traderType+'-PersonImgWindow',
			      //     	roleAction: grid.roleAction,
		    			// traderType: grid.traderType
			      //   });

      				// win.show();
	            	
	            }
            }]
        }];
    },
    getTourLeaderColumn: function(){
    	return [{
			header: 'regId',
			dataIndex: 'regId',
			hidden: true
		},{
            text     : 'รูป',
            flex     : 1,
            sortable : true,
            dataIndex: 'imgData',
            renderer: function(value, metaData, gridmodel, rowIndex,colIndex,gridstore,gridview){
            	var dated = new Date();
        		return '<img src="'+value+'?dc_='+dated.getTime()+'" height="100" width="100" />';
		    }
        },
		{
			header: 'ชื่อ-นามสกุล',
			dataIndex: 'traderOwnerName',
			flex: 1
		},
		
		{
			header: 'ประเภทใบอนุญาต',
			dataIndex: 'traderCategory',
			flex: 2,
			renderer: function(value, metaData, model){
				if(value == '100')
                {                      	
                    return 'เป็นผู้นำเที่ยวตามประกาศกฎกระทรวงฯ';
                }
                if(value == '200')
                {
                    return 'เป็นมัคคุเทศก์และมาขอจดทะเบียนผู้นำเที่ยว';
                }
                if(value == '300')
                {
                    return 'ผ่านการอบรมจากสถาบันที่สำนักทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ กำหนด';
                }
				
			}
		},
		{
			header: 'ประเภทการจดทะเบียน',
			dataIndex: 'registrationTypeName',
			flex: 1.25
		},
		{
			header: 'วันที่อนุมัติ',
			dataIndex: 'approveDate',
			flex: 0.75
		},
        {
        	header: 'รายละเอียด',
            xtype: 'actioncolumn',
           	width:50,
           	align : 'center',
            items: [{
                iconCls: 'icon-edit-xsmall',
                tooltip: 'รายละเอียด',
	            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {

	            		this.fireEvent('itemclick', this, 'viewPrintCardTourLeader', grid, rowIndex, colIndex, record, node);
	            }
            }]
        }, {
        	header: 'ถ่ายรูป',
        	xtype: 'actioncolumn',
        	width:30,
            align : 'center',
            items: [{
            	 getClass: function(v, meta, rec) {          
                      return 'icon-adduser-xsmall';
                  },
                iconCls: 'icon-checked-xsmall',
                tooltip: 'ดูรูปภาพ',
	            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
	            	this.fireEvent('itemclick', this, 'takepicview', grid, rowIndex, colIndex, record, node);
	            	 
	            }
            }]
        }];
    }
   
});