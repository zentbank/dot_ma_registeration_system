Ext.define('tourism.view.printcard.PrintCardGuideFormSearch',{
	extend: 'Ext.form.FormPanel',
	requires:[
				'Ext.form.field.Date',
				'Ext.form.field.Time',
				'Ext.form.CheckboxGroup',
				'Ext.layout.container.HBox',
				'Ext.form.FieldSet'
	          ],
	
	alias: 'widget.printcard-guide-formsearch',
	
	bodyPadding: 10,
	items: [
            {
        xtype: 'container',
        // title: 'Payment',
        layout: 'anchor',
        defaults: {
            anchor: '100%',
            labelWidth: 150
        },
        items: [
                   {
                    xtype: 'container',
                    layout: 'hbox',
                    layoutCongig: {
                         pack:'center',
                         align:'middle'
                    },
                    items: [
                        {
                            xtype: 'container',
                            // title: 'Payment',
                            flex: 1,
                            layout: 'anchor',
                            defaults: {
                                anchor: '100%',
                                labelWidth: 150
                            },
	                        items: [
								{
								    xtype: 'textfield',
								    fieldLabel: 'เลขที่ใบอนุญาต',
								    name: 'licenseNoFrom'
								},
                                {
                                    xtype: 'datefield',
                                    fieldLabel: 'วันที่อนุมัติ จากวันที่',
                                    // labelWidth: 150,
                                    name: 'approveDateFrom',
                                    format: 'd/m/B'
                                },
                                {
                                    xtype: 'textfield',
                                     fieldLabel: 'ชื่อ',
                                    name: 'firstName',
                                    allowBlank: true
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'บัตรประจำตัวเลขที่',
                                    name: 'identityNo'
                                },
                                {
                                    xtype: 'combo',
                                    fieldLabel: 'ประเภทการทำรายการ',
                                    store: 'tourism.store.combo.GuideRegistrationTypeStore',
                                    queryMode: 'local',
                                    displayField: 'registrationTypeName',
                                    valueField: 'registrationType',
                                    hiddenName: 'registrationType',
                                    name: 'registrationType'
                                }
                                ,{
                                        xtype: 'combo',
                                        fieldLabel: 'สำนักงาน',
                                        store: 'tourism.store.combo.OrganizationStore',
                                        queryMode: 'remote',
                                        displayField: 'orgName',
                                        valueField: 'orgId',
                                        hiddenName: 'orgId',
                                        name :'orgId'
                                }
                            ]
                        },{
                            xtype: 'splitter'
                        },
                        {
                            xtype: 'container',
                            flex: 1,
                            layout: 'anchor',
                            defaults: {
                                anchor: '100%',
                                labelWidth: 150
                            },
                            items: [
								{
								    xtype: 'textfield',
								    fieldLabel: 'ถึงเลขที่ใบอนุญาต',
								    name: 'licenseNoTo'
								},
                                {   
                                    xtype: 'datefield',
                                    fieldLabel: 'ถึงวันที่',
                                    name: 'approveDateTo',
                                    flex: 1,
                                    format: 'd/m/B'
                                    // ,value: Ext.Date.add(new Date(), Ext.Date.YEAR, 543), //2554 ณ ปัจจุบัน
                                   
                                },
                                 {
                                    // นามสกุล
                                    xtype: 'textfield',
                                    fieldLabel: 'นามสกุล',
                                    name: 'lastName',
                                    labelAlign: 'left'
                                },
                                 {
                                    xtype: 'combo',
                                    fieldLabel: 'ประเภทมัคคุเทศก์',
                                    store: 'tourism.store.combo.GuideCategoryStore',
                                    queryMode: 'local',
                                    displayField: 'traderCategoryName',
                                    valueField: 'traderCategory',
                                    hiddenName: 'traderCategory',
                                    name :'traderCategory'
                                },
                                {
                                	xtype: 'combo',
                                    fieldLabel: 'สถานะเรื่อง',
                                    store: 'tourism.store.combo.PrintCardStatusStore',
                                    queryMode: 'local',
                                    displayField: 'printCardStatusName',
                                    valueField: 'printCardStatus',
                                    hiddenName: 'printCardStatus',
                                    name: 'printCardStatus',
                                    //value: 'W',
        	                        tabIndex: 9
                                }
                            ]
                        }
                    ]
                }
        ]
    },
    {
        xtype: 'toolbar',
        border: false,
        padding: '6px 0 6px 0px',
        items: [
                {
                	xtype: 'button',
                	text : 'ค้นหา',
                	action: 'searchCardBetweenRegistration'
                },
                {
                	xtype: 'button',
                	text : 'Export Excel',
                	action: 'exportExcelCardBetweenRegistration'
                },
                {
                	xtype: 'button',
                	text : 'ถ่ายรูปมัคคุเทศก์',
                	handler: function (){
                	var url = 'printcard/showTakePic_addNew.jsp?traderType=G';
 		        	if(!window.showTakePicWin || window.showTakePicWin.closed)
 		        	    {
 		        		  window.showTakePicWin = window.open(url,"ถ่ายรูปมัคคุเทศก์","width=970,height=650");
 		        	    }
 		        	else
 		        	   {
 		        		  window.showTakePicWin.document.location = url;
 		        	   	  window.showTakePicWin.focus();
 		        	   }
                	}
                }
                
        ]
    }

]
});