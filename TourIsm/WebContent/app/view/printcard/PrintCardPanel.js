Ext.define('tourism.view.printcard.PrintCardPanel',{
	extend: 'Ext.container.Container',
	xtype: 'printcard-panel',
	requires:[
	          'Ext.tab.Panel',
	          'Ext.layout.container.Border',
	          'tourism.view.printcard.PrintCardGrid',
	          'tourism.view.printcard.PrintCardGuideFormSearch',
	          'tourism.view.printcard.PrintCardTourLeaderFormSearch',
	          'tourism.view.printcard.PrintCardUploadPicturePanel'
	],
	initComponent: function()
	{
		var formSearch = this.getGuideFormSearch(this.roleAction);
		
		if(this.traderType == 'tourleader')
    	{
    		formSearch = this.getTourleaederFormSearch(this.roleAction);
    	}
		
		Ext.apply(this,{
			layout: {
		        type: 'border'
		    },
		    items: [{
		        region: 'north',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items:[formSearch]
		    },{
		        region: 'center',
		        xtype: 'panel',
		        layout: 'fit',
		        frame: false,
		        border: false,
		        items:[{
		            // title: 'การทำรายการพิมพ์ใบอนุญาตธุรกิจนำเที่ยว,มัคคุเทศน์,ผู้นำเที่ยว',
		            xtype: 'printcard-grid',
		            id: this.roleAction+'-'+this.traderType+'-printcard-grid',
		            // padding: '5px 5px 5px 5px',
		            autoScroll: true,
		            // frame: false 
		            border: false,
		            storeId: this.roleAction+'-'+this.traderType+'-printcard-store',
		            storepageSize: 20,
		            storeUrl: 'business/'+this.roleAction+'/registration/read/'+this.traderType,
		            roleAction: this.roleAction,
		            traderType: this.traderType 
		        }]
		    }]
		});
		this.callParent(arguments);
	},
	getGuideFormSearch: function(roleAction)
    {
    	return {
    		
	            title:'ค้นหาการทำรายการพิมพ์บัตรมัคคุเทศก์',
	            collapsible: true,   // make collapsible
	            // collapsed : true,
	            xtype: 'printcard-guide-formsearch',
	            id: roleAction+'-guide'+'-printcard-formsearch',
	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false
    	};
    },
    getTourleaederFormSearch: function(roleAction)
    {
    	return {
    		
	            title:'ค้นหาการทำรายการพิมพ์บัตรผู้นำเที่ยว',
	            collapsible: true,   // make collapsible
	            // collapsed : true,
	            xtype: 'printcard-tourleader-formsearch',
	            id: roleAction+'-tourleader'+'-printcard-formsearch',
	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false
    	};
    }
});