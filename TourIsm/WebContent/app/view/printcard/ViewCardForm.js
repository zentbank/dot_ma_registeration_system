Ext.define('tourism.view.printcard.ViewCardForm', {
    extend: 'Ext.container.Container',
    requires: [
        'Ext.data.*',
        'Ext.form.*',
        'Ext.tab.Tab',
        'Ext.tab.Panel',
        'Ext.layout.container.Border'
    ],
    alias: 'widget.printcard-viewcardform',
    initComponent: function(){
    
        Ext.apply(this, {
            width: '100%',
            height: '100%',
            layout: {
                type: 'border'
            },

            items: [/*{
                region: 'east',
                xtype: 'form',
                // title: 'รายละเอียด',
                // padding: '5px 5px 5px 0px',
                // collapsible: true,   // make collapsible
                width: 350,
                frame: false,
                border: false,
                split: false,
                splitterResize: false,
                fieldDefaults: {
                    labelAlign: 'left',
                    msgTarget: 'qtip'
                },
                frame: false,
                border: false,
                items: [{
                    xtype: 'container',
                    padding: '5px 5px 5px 5px',
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%'
                    },
                    items:[{
                        xtype: 'hiddenfield',
                        name: 'reqPassId'
                    }, {   
                        xtype: 'datefield',
                        fieldLabel: 'วันที่รับเรื่อง',
                        name: 'reqDate',
                        format: 'd/m/B',
                        value: new Date()
                    }]
                }]
            },*/{
                region: 'center',
                xtype: 'form',
                plain: true,
                // padding: '5px 5px 5px 5px',
                frame: false,
                border: false,
                items:[{html:'#'}]

            }]
   
        });
        this.callParent(arguments);
    }
   
    
});