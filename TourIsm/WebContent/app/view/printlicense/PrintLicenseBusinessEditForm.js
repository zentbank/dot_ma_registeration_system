Ext.define('tourism.view.printlicense.PrintLicenseBusinessEditForm', {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.data.*',
        'Ext.form.*',
        'Ext.tab.Tab',
    ],
    xtype: 'printlicense-business-edit-form',
    // title: 'มัคคุเทศน์',
    bodyPadding: 5,
    autoScroll: true,
    initComponent: function(){

        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
        
        var provinceStore =  Ext.create('tourism.store.combo.MasProvinceStore',{
            storeId:'printlicense-registration-masprovince-combo'
        });

        var amphurStore =   Ext.create('tourism.store.combo.MasAmphurStore',{
            storeId:'printlicense-registration-masamphur-store'
        });
        
        var tambolStore =   Ext.create('tourism.store.combo.MasTambolStore',{
            storeId:'printlicense-registration-mastambol-store'
        });
        
        Ext.apply(this, {
            //width: '100%',
            //height: '100%',   	
            fieldDefaults: {
                labelAlign: 'left',
                anchor: '100%',
        		labelWidth: 170,
                // labelWidth: 50,
                msgTarget: 'qtip'
            },

            items: [ 
{
    xtype: 'splitter'
},
                    {
                    	xtype: 'container',
                        layout: 'anchor',
                        defaults: {
                            anchor: '100%'
                        },
                        items: [         
                                	{
                                		xtype: 'container',
                                		layout: 'hbox',
                                		layoutConfig: {
                                			pack: 'center'
                                		},
                                		items:[
{
    xtype: 'splitter'
},{ xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},
{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},
{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},
{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},
{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},
{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},
{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},
{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},
{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},
										{
                                		    html: '<h3>ใบอนุญาตประกอบธุรกิจนำเที่ยว</h3>',
                                            border: false
                                		}
                                		 ],
                                	}
	
                    	],
                    },
                    {
                    	xtype: 'splitter'
                    },
                    {
                      	 xtype:'hiddenfield',
                      	 name: 'personType'
                       },
                    {
                   	 xtype:'textfield',
                   	 fieldLabel: 'ใบอนุญาตเลขที่',
                   	 name: 'licenseNo'
                    },
                    {
                    	xtype: 'splitter'
                    },
                    {
                		xtype: 'container',
                		layout: 'hbox',
                		layoutConfig: {
                			pack: 'center'
                		},
                		items:[
{
xtype: 'splitter'
},{ xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},
{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},
{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},
{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},
{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},{xtype: 'splitter'},						
						{
                		    html: 'นายทะเบียนธุรกิจนำเที่ยวและมัคคุเทศน์กลาง',
                            border: false
                		}
                		 ],
                	},
                	{
                		xtype: 'splitter'
                	},                    
                    {
                    	xtype: 'container',
                    	layout: 'hbox',
                    	items:[{
                    	   xtype: 'fieldcontainer',
      			    	   labelAlign: 'left',
      			    	   fieldLabel: 'ชื่อ(ภาษาไทย)',
      			    	   layout: 'hbox',
      			    	   		items: [{
      			    	   			xtype: 'combo',
      			    	   			store: 'tourism.store.combo.MasPrefixStore',
      			    	   			queryMode: 'remote',
      			    	   		    displayField: 'prefixName',
      			    	   		    valueField: 'prefixId',
      			    	   		    hiddenName: 'prefixId',
      			    	   		    name: 'prefixId',
      			    	   		    triggerAction: 'all',
      			    	   			flex:1,
      			    	   			allowBlank: false,
      			    	   			tabIndex : 6
      			    	   	},
      			    	   	{
      			    	   		xtype: 'splitter'
      			    	   	},
      			    	   	{
      			    	   		xtype: 'textfield',
      			    	   		name: 'firstName',
                                flex: 2,
                                allowBlank: false,
                                tabIndex : 7
      			    	   	},
      			    	   	{
      			    	   		xtype: 'textfield',
  			    	   			name: 'postfixName',
  			    	   			flex: 2,
  			    	   			allowBlank: false,
  			    	   			tabIndex : 7
      			    	   	}
      			    	   	]
                    	}]
                    },
                    {
                    	xtype: 'splitter'
                    },
                    {
                    	xtype: 'textfield',
                    	fieldLabel: 'ทะเบียนนิติบุคคลเลขที่',
                    	name: 'identityNo'
                    },
                    {
                    	xtype: 'splitter'
                    },
                    {
                    	//xtype: 'displayfield',
                    	html: 'ประกอบธุรกิจนำเที่ยวตามมาตรา 15 แห่งพระราชบัญญัติธุรกิจนำเที่ยวและมัคคุเทศน์ พ.ศ. 2551',
                    	border: false
                    },
                    {
                    	xtype: 'splitter'
                    },
                    {
                    	xtype: 'textfield',
                    	fieldLabel: 'ประเภท',
                    	name: 'traderCategoryName'
                    },
                    {
                    	xtype: 'textfield',
                    	fieldLabel: 'โดยใช้ชื่อภาษาไทยว่า',
                    	name: 'traderName'
                    },
                    {
                    	xtype: 'textfield',
                    	fieldLabel: 'หรือใช้ชื่อภาษาต่างประเทศว่า',
                    	name: 'traderNameEn'
                    },
                    {
                    	xtype: 'textfield',
                    	fieldLabel: 'ซึ่งอ่านเป็นภาษาไทยว่า',
                    	name: 'pronunciationName'
                    },
                    {
                    	xtype: 'splitter'
                    },
                    
                    {
                    	xtype: 'textfield',
                    	fieldLabel: 'สำนักงานตั้งอยู่เลขที่',
                        name: 'addressNo'
                    },
                    {
                    	xtype: 'container',
                    	layout: 'hbox',
                    		items:[
                    		       {
                    		    	   xtype: 'fieldset',
                    		    	   //flex: 1,
                           			   border: false,
                           			   defaults: {
                           				       anchor: '100%',
                           				       //labelWidth: 170
                           			},
                           			items:[
                    		       {
                    		       		xtype: 'textfield',
                    		       		fieldLabel: 'ตรอก/ซอย',
                    		       		name: 'soi'
                    		       },
                    		       {
                    		    	  xtype: 'mastambolcombo',
      	                              id : 'printlicense-registration-mastambol-combo',
      	                              fieldLabel: 'ตำบล',
      	                              store: tambolStore,
      	                              displayField: 'tambolName',
      	                              valueField: 'tambolId',
      	                              hiddenName: 'tambolId',
      	                              name: 'tambolId',
      	                           //   allowBlank: false,
      	                              afterLabelTextTpl: required,
      	                           //   masprovinceComboId: 'printlicense-registration-masprovince-combo',
      	                              masamphurComboId:'printlicense-registration-masamphur-combo',
      	                              queryParam: 'tambolName'
      	                              //,tabIndex : 16
                    		       },
                    		       {
                    		    	   xtype: 'masprovincecombo',
     	                              id : 'printlicense-registration-masprovince-combo',
     	                              fieldLabel: 'จังหวัด',
     	                              store: provinceStore,
     	                              displayField: 'provinceName',
     	                              valueField: 'provinceId',
     	                              hiddenName: 'provinceId',
     	                              name: 'provinceId',
     	                           //   allowBlank: false,
     	                              afterLabelTextTpl: required,
     	                              masamphurComboId:'printlicense-registration-masamphur-combo',
     	                           //  mastambolComboId:'printlicense-registration-mastambol-combo',
     	                              queryParam: 'provinceName'
     	                              //,tabIndex : 16
                    		       },
                      ]
                },
                    		       {
                    		    	   xtype: 'fieldset',
                    		    	   //flex: 1,
                           			   border: false,
                           			   defaults: {
                           				       anchor: '100%',
                           				       //labelWidth: 170
                           			},
                           			items:[
                    		       {
                    		       		xtype: 'textfield',
                    		       		fieldLabel: 'ถนน',
                    		       		name: 'roadName'
                    		       },
                    		       {
                    		    	  xtype: 'masamphurcombo',
     	                        	  fieldLabel: 'อำเภอ/เขต',
     	                              id : 'printlicense-registration-masamphur-combo',
     	                              store: amphurStore,
     	                              displayField: 'amphurName',
     	                              valueField: 'amphurId',
     	                              hiddenName: 'amphurId',
     	                              name: 'amphurId',
     	                          //    allowBlank: false,
     	                              afterLabelTextTpl: required,
     	                              queryParam: 'amphurName',
     	                              masprovinceComboId: 'printlicense-registration-masprovince-combo',
     	                              mastambolComboId:'printlicense-registration-mastambol-combo',	  
     	                             //,tabIndex : 17	  
                    		       },
                    		       {
                    		    	   xtype: 'textfield',
                    		    	   fieldLabel: 'รหัสไปรษณีย์',
                    		    	   name: 'postCode'
                    		       }
                    		
                    		       
                    ]
                 }
            ]
        },
        {
        	xtype: 'datefield',
        	fieldLabel: 'ออกให้ ณ วันที่',
        	name: 'today',
        	format: 'd/m/B'
        }
],
            buttons: [{
                text: 'พิมพ์ใบอนุญาต',
                scope: this,
                action: 'printlicensePDF',
                tabIndex : 24
            }]    
        });
        this.callParent();
    }
    ,onResetClick: function(){
        this.getForm().reset();
    }
    ,onCompleteClick: function(){
        var form = this.getForm();
        if (form.isValid()) {
            Ext.MessageBox.alert('Submitted Values', form.getValues(true));
        }
    }
  
    
    
});