Ext.define('tourism.view.printlicense.PrintLicenseBusinessFormSearch',{
	extend: 'Ext.form.FormPanel',
	requires:[
				'Ext.form.field.Date',
				'Ext.form.field.Time',
				'Ext.form.CheckboxGroup',
				'Ext.layout.container.HBox',
				'Ext.form.FieldSet'
	          ],
	
	alias: 'widget.printlicense-business-formsearch',
	
	bodyPadding: 10,
	
	items: [
	        {
	                xtype: 'container',
	                layout: 'anchor',
	                defaults: {
	                    anchor: '100%',
	                    labelWidth: 150
	                },
	                items: [
	                	{
			                xtype: 'hidden',
			                name: 'traderType',
			                value: 'B'
			            },
	                    {
	                        xtype: 'textfield',
	                        fieldLabel: 'เลขที่รับเรื่อง',
	                        name: 'registrationNo',
	                        anchor: '49.7%',
	                        tabIndex: 1
	                    },
	                    
	                    {
	                        xtype: 'textfield',
	                        fieldLabel: 'เลขที่ใบอนุญาต',
	                        name: 'licenseNo',
	                        anchor: '49.7%',
	                    },
	                    
	                    {
	                        xtype: 'container',
	                        layout: 'hbox',
	                        layoutCongig: {
	                             pack:'center',
	                             align:'middle'
	                        },
	                        items: [
	                            {
	                                xtype: 'container',
	                                // title: 'Payment',
	                                flex: 1,
	                                layout: 'anchor',
	                                defaults: {
	                                    anchor: '100%',
	                                    labelWidth: 150
	                                },
	                                items: [
	                                    {
	                                        xtype: 'datefield',
	                                        fieldLabel: 'วันที่อนุมัติ จากวันที่',
	                                        // labelWidth: 150,
	                                        name: 'approveDateFrom',
	                                        format: 'd/m/B',
	            	                        tabIndex: 2
	                                    },
	                                    {
	                                        xtype: 'textfield',
	                                        fieldLabel: 'ชื่อธุรกิจนำเที่ยว(TH)',
	                                        // labelWidth: 150,
	                                        name: 'traderName',
	            	                        tabIndex: 4
	                                    },
	                                    {
	                                        xtype: 'textfield',
	                                        fieldLabel: 'ชื่อผู้ขอใบอนุญาต',
	                                        // labelWidth: 150,
	                                        name: 'firstName',
	            	                        tabIndex: 6
	                                    },
	                                    {
	                                        xtype: 'combo',
	                                        fieldLabel: 'ประเภทการทำรายการ',
	                                        store: 'tourism.store.combo.BusinessRegistrationTypeStore',
	                                        queryMode: 'local',
	                                        displayField: 'registrationTypeName',
	                                        valueField: 'registrationType',
	                                        hiddenName: 'registrationType',
	                                        name: 'registrationType',
	            	                        tabIndex: 8
	                                    }
	                                ]
	                            },{
	                                xtype: 'splitter'
	                            },
	                            {
	                                xtype: 'container',
	                                flex: 1,
	                                layout: 'anchor',
	                                defaults: {
	                                    anchor: '100%',
	                                    labelWidth: 150
	                                },
	                                items: [
	                                    {   
	                                        xtype: 'datefield',
	                                        fieldLabel: 'ถึงวันที่',
	                                        
	                                        name: 'approveDateTo',
	                                        flex: 1,
	                                        format: 'd/m/B',
	            	                        tabIndex: 3
	                                        // ,value: Ext.Date.add(new Date(), Ext.Date.YEAR, 543), //2554 ณ ปัจจุบัน
	                                       
	                                    },
	                                    {
	                                        xtype: 'textfield',
	                                        fieldLabel: 'ชื่อธุรกิจนำเที่ยว(EN)',
	                                        // labelWidth: 150,
	                                        name: 'traderNameEn',
	            	                        tabIndex: 5
	                                    },
	                                     {
	                                        xtype: 'combo',
	                                        fieldLabel: 'ประเภทธุรกิจนำเที่ยว',
	                                        store: 'tourism.store.combo.BusinessCategoryStore',
	                                        queryMode: 'local',
	                                        displayField: 'traderCategoryName',
	                                        valueField: 'traderCategory',
	                                        hiddenName: 'traderCategory',
	                                        name :'traderCategory',
	            	                        tabIndex: 7
	                                    },
	                                     {
	                                        xtype: 'combo',
	                                        fieldLabel: 'สถานะเรื่อง',
	                                        store: 'tourism.store.combo.PrintStatusStore',
	                                        queryMode: 'local',
	                                        displayField: 'printLicenseStatusName',
	                                        valueField: 'printLicenseStatus',
	                                        hiddenName: 'printLicenseStatus',
	                                        name: 'printLicenseStatus',
//	                                        value: 'W',
	            	                        tabIndex: 9
	                                    }
	                                ]
	                            }
	                        ]
	                    }
	                ]
	        },
	        {
	            xtype: 'toolbar',
	            border: false,
	            padding: '6px 0 6px 0px',
	            items: [{
	                xtype: 'button',
	                text : 'ค้นหา',
	                action: 'searchLicenseBetweenRegistration',
                    tabIndex: 11
	            },
	            {
	            	xtype: 'button',
	                text : 'พิมพ์รายงาน',
	                action: 'printReportPrintLicense'
	            }
	            ]
	        }//,
//	        {
//	            xtype: 'toolbar',
//	            align:'center',
//	            border: false,
//	            padding: '10px 0 10px 0px',
//	            items: [{
//	            	xtype: 'button',
//	                text : 'พิมพ์รายงาน',
//	                action: 'printReportPrintLicense'
//	            }]
//	        }

	    ]
});