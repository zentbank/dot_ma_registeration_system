Ext.define('tourism.view.printlicense.PrintLicenseBusinessWindow',{
	extend: 'Ext.window.Window',
	alias: 'widget.printlicense_business_window',
	requires:[
	       'tourism.view.printlicense.PrintLicenseBusinessEditForm'
	 ],
	          title: 'พิมพ์ใบอนุญาต',
	          layout: 'fit',
	          width: 800,
	          //height: 600,
	          //True to make the window modal and mask everything behind it when displayed
	          modal: true,
	
	          initComponent: function(){
		 
		 this.items = [
		            {
		            	xtype: 'printlicense-business-edit-form',
		                frame: false,
		                border: false,
		                registrationModel: this.registrationModel
		            }  
		 ];
		 this.callParent(arguments);
	 },
	    loadFormRecord: function(model)
	    {
	        var info = this.down('form');

	        info.loadRecord(model);

	        
	        if(!Ext.isEmpty(model.get('provinceId')))
            {
              var provinceCombo = info.getForm().findField('provinceId');
              provinceCombo.getStore().on('load',function(store, records, successful, eOpts){
                provinceCombo.setValue(model.get('provinceId'));
              },this,{single:true});
              provinceCombo.getStore().load();
            }
            else
            {
              var provinceCombo = info.getForm().findField('provinceId');
              provinceCombo.clearValue();
            }
	        
	        if(!Ext.isEmpty(model.get('amphurId')))
            {
              var amphurCombo = info.getForm().findField('amphurId');
              amphurCombo.getStore().on('load',function(store, records, successful, eOpts){
                amphurCombo.setValue(model.get('amphurId'));
              },this,{single:true});
              amphurCombo.getStore().load();
            }
            else
            {
               var amphurCombo = info.getForm().findField('amphurId');
               amphurCombo.clearValue();
            }
	        
	        if(!Ext.isEmpty(model.get('tambolId')))
            {
              var tambolCombo = info.getForm().findField('tambolId');
              tambolCombo.getStore().on('load',function(store, records, successful, eOpts){
                tambolCombo.setValue(model.get('tambolId'));
              },this,{single:true});
              tambolCombo.getStore().load();
            }
            else
            {
               var amphurCombo = info.getForm().findField('amphurId');
               amphurCombo.clearValue();
            }
	        
	        if(!Ext.isEmpty(model.get('prefixId')))
            {
              var prefixIdCombo = info.getForm().findField('prefixId');
              prefixIdCombo.getStore().on('load',function(store, records, successful, eOpts){
                prefixIdCombo.setValue(model.get('prefixId'));
              },this,{single:true});
              prefixIdCombo.getStore().load();
            }
            else
            {
              var prefixIdCombo = info.getForm().findField('prefixId');
              prefixIdCombo.clearValue();
            }
	        
	        if(!Ext.isEmpty(model.get('personType')))
	        {
	        	if(model.get('personType') == 'I')
	        	{
	        		info.getForm().findField('postfixName').setValue(model.get('lastName'));
	        		//console.log('lastName');
	        	}else
	        	{
	        		info.getForm().findField('postfixName').setValue(model.get('postfixName'));
	        		//console.log('postfixName');
	        	}
	        }
	         /* if(!Ext.isEmpty(model.get('provinceName')))
	          {
	            info.getForm().findField('provinceNameCorp').setValue(model.get('provinceName'));
	          }

	          if(!Ext.isEmpty(model.get('prefixName')))
	          {
	            info.getForm().findField('prefixNameCorp').setValue(model.get('prefixName'));
	          }
	          if(!Ext.isEmpty(model.get('firstName')))
	          {
	            
	             info.getForm().findField('firstNameCorp').setValue(model.get('firstName'));
	          }
	          if(!Ext.isEmpty(model.get('identityNo')))
	          {
	           
	             info.getForm().findField('identityNoCorp').setValue(model.get('identityNo'));
	          }
	          if(!Ext.isEmpty(model.get('identityDate')))
	          {
	             info.getForm().findField('identityDateCorp').setValue(model.get('identityDate'));
	          }*/    
	       // }

	        /*var traderaddressgrid =  info.down('information-address-taderaddress-grid');
	           
	        traderaddressgrid.getStore().load({
	            params: {traderId: model.get('traderId'), recordStatus: model.get('traderRecordStatus')},
	            callback: function(records, operation, success) {
	                // //console.log(success);
	            },
	            scope: this
	        }); 

	        var committeeInfoGrid =  info.down('information-committee-committeeinfogrid');
	           
	        committeeInfoGrid.getStore().load({
	            params: {personId: model.get('personId'), recordStatus: model.get('traderRecordStatus')},
	            callback: function(records, operation, success) {
	                // //console.log(success);
	            },
	            scope: this
	        });   
	        var branchInfoGrid =  info.down('information-branch-infogrid');
	           
	        branchInfoGrid.getStore().load({
	            params: {branchParentId: model.get('traderId'), recordStatus: model.get('traderRecordStatus')},
	            callback: function(records, operation, success) {
	                // //console.log(success);
	            },
	            scope: this
	        });  

	        this.showConutry(model);
	        
	        //เอกสารประกอบ
	        var grid = info.down('document-business-popup-grid');
	        grid.getStore().load({
	      	  params: {
	      		  traderId: model.get('traderId')
	      		  ,recordStatus: 'N'
	      		  ,licenseType: 'B'
	      		  ,registrationType: 'N'
	      	  },
	            callback: function(records, operation, success) {
//	                 //console.log(records);
	            },
	            scope: this
	        }); 
	        
	      //ประวัติการจดทะเบียน
	      if(model.get('licenseNo') != "")
	 	  {
	        var historyinfogrid = info.down('information-history-info-grid');
	        historyinfogrid.getStore().load({
	        	params: {traderType: model.get('traderType'), licenseNo: model.get('licenseNo')},
	        	callback: function(records, operation, success){
	        		//console.log(records);
	        	},
	        	scope: this
	        });
	 	  }*/

	        // var plantripgrid =  info.down('information-tradercategory-businessplantrip-gridinfo');

	        // plantripgrid.getStore().load({
	          
	        //     params: {traderId: model.get('traderId'), recordStatus: model.get('traderRecordStatus')},
	        //     callback: function(records, operation, success) {
	        //         // //console.log(success);
	        //     },
	        //     scope: this
	        // });  
	    }

});