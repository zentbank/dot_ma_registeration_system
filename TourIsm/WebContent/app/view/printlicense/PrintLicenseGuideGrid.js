Ext.define('tourism.view.printlicense.PrintLicenseGuideGrid',{
	extend: 'Ext.grid.GridPanel',
	alias: 'widget.printlicense-guide-grid',
	requires: [
				'Ext.grid.Column',
				'tourism.store.registration.RegistrationStore',
				'Ext.toolbar.Paging',
				'Ext.grid.plugin.RowEditing',
				'Ext.grid.column.Template',
				'Ext.grid.column.Action',
				'Ext.grid.plugin.RowExpander',
				'Ext.grid.feature.Grouping'
			],
	 
	initComponent: function() {
		var registrationStore = Ext.create('tourism.store.registration.RegistrationStore',{
			storeId: this.storeId,
			pageSize : this.storepageSize,
			groupField: 'headerInfo',
			roleAction: this.roleAction,
		    traderType: this.traderType,
			proxy: {
				type: 'ajax',
				actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
				api: {
				    read: this.storeUrl
				},
				reader: {
				    type: 'json',
				    root: 'list',
				    totalProperty: 'totalCount',
				    successProperty: 'success',
				    messageProperty: 'message'
				},
				listeners: {
				    exception: function(proxy, response, operation){

				        Ext.MessageBox.show({
				            title: 'REMOTE EXCEPTION',
				            msg: operation.getError(),
				            icon: Ext.MessageBox.ERROR,
				            buttons: Ext.Msg.OK
				        });
				    }
				}
			}
			,listeners: {
				scope: this,
				// single: true,
				beforeload: function( store, operation, eOpts )
			        {

			        	var formId = '#' + store.roleAction+'-'+store.traderType+'-printlicense-formsearch';
			        
			        	var formSearch = Ext.ComponentQuery.query(formId)[0];
			        	var values = formSearch.getValues();
							
			        	 for (field in values) 
					      {
					        if (Ext.isEmpty(values[field])) 
					        {
					          delete values[field];
					        }
					        
					      }
					    store.proxy.extraParams = values;
			        }
			}
		});
		
		var columns = this.getGuideColumn();

		Ext.apply(this,{
			store: registrationStore,
			dockedItems:[
				{
					xtype: 'pagingtoolbar',
					store: registrationStore,
					dock: 'bottom',
					displayInfo: true

				}
			],
			viewConfig: {
				stripeRows:true ,
				getRowClass: function(record, index) {                            
					if (record.get('roleColor') == 'true') {
					    return "online-block";
					}
				}
			},
			columns: columns
		});

		this.callParent(arguments);
	},
//	features:[{
//		ftype: 'grouping',
//		groupHeaderTpl: '{name}',
//		// groupHeaderTpl: '{name} {rows.length} สาขา/คน',
//        hideGroupedHeader: false,
//        startCollapsed: false
//	}],
    getGuideColumn: function(){
    	return [{
			header: 'regId',
			dataIndex: 'regId',
			hidden: true
		},
		{
			header: 'เลขที่ใบอนุญาต',
			dataIndex: 'licenseNo',
			flex: 1
		},
		{
			header: 'ชื่อผู้ขอใบอนุญาต',
			dataIndex: 'traderOwnerName',
			flex: 2
		},
		{
			header: 'ประเภทมัคคุเทศก์',
			dataIndex: 'traderCategoryName',
			flex: 2
		},
		{
			header: 'ประเภทการจดทะเบียน',
			dataIndex: 'registrationTypeName',
			flex: 2
		},
		// {
		// 	header: 'เลขที่ใบอนุญาต',
		// 	dataIndex: 'licenseNo',
		// 	flex: 1
		// },
		{
			header: 'วันที่อนุมัติ',
			dataIndex: 'approveDate',
			flex: 0.75
		},
		
        {
        	header: 'พิมพ์ใบอนุญาต',
            xtype: 'actioncolumn',
           	//width:100,
           	align : 'center',
            items: [{
           
                 getClass: function(v, meta, rec) {          
                   if (rec.get('roleAction') == '1') {
                        this.items[0].tooltip = 'ยังไม่พิมพ์ใบอนุญาต';
                        return 'icon-notes-xsmall';
                    }else if (rec.get('roleAction') == 'P') {
						this.items[0].tooltip = 'พิมพ์ใบอนุญาตแล้ว';
                        return 'icon-checked-xsmall';
                    }  else {
                        this.items[0].tooltip = 'ยังไม่ส่งเรื่อง';
                        return '';
                    }
                },
	            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
	            	if (record.get('roleAction') == '1')
	            	{

	                	this.fireEvent('itemclick', this, 'priceLicenseGuide', grid, rowIndex, colIndex, record, node);
	            	}
	            	else
	            	{
	                	this.fireEvent('itemclick', this, 'priceLicenseGuide', grid, rowIndex, colIndex, record, node);
	            	}
				}
            }]
        },
        {
        	header: 'รายละเอียด',
            xtype: 'actioncolumn',
           	//width:50,
           	align : 'center',
            items: [{
                iconCls: 'icon-edit-xsmall',
                tooltip: 'รายละเอียด',
	            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {

	            		this.fireEvent('itemclick', this, 'viewPrintlicenseguide', grid, rowIndex, colIndex, record, node);

	            }
            }]
        },        
        {
        	header: '',
            xtype: 'actioncolumn',
           	width:50,
           	align : 'center',
            items: [{
           
                 getClass: function(v, meta, rec) {          
                    if (rec.get('progressBackStatus') != 'B') {
                        this.items[0].tooltip = 'รับเรื่อง';
                        return 'icon-comment-xsmall';
                    } else {
                        this.items[0].tooltip = 'เรื่องตีกลับ';
                        return 'icon-comment-xsmall-red';
                    }
                },
	            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {

	                this.fireEvent('itemclick', this, 'approvalprogress', grid, rowIndex, colIndex, record, node);
	            }
            }]
        }
];
    }
   
});