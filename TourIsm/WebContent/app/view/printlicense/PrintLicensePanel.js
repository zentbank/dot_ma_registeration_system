Ext.define('tourism.view.printlicense.PrintLicensePanel',{
	extend: 'Ext.container.Container',
	xtype: 'printlicense-panel',
	requires:[
	          'Ext.tab.Panel',
	          'Ext.layout.container.Border',
	          'tourism.view.printlicense.PrintLicenseBusinessFormSearch',
	          'tourism.view.printlicense.PrintLicenseGuideFormSearch',
	          'tourism.view.printlicense.PrintLicenseTourLeaderFormSearch',
	          'tourism.view.printlicense.PrintLicenseGrid',
	          'tourism.view.printlicense.PrintLicenseGuideGrid'
	          ],
	
	initComponent: function()
	{
		var formSearch = this.getBusinessFormSearch(this.roleAction);
		var grid = this.getBusinessGrid(this.roleAction);
		if(this.traderType == 'guide')
    	{
			formSearch = this.getGuideFormSearch(this.roleAction);
			grid = this.getGuideGrid(this.roleAction);
    	}else if(this.traderType == 'tourleader')
    	{
    		formSearch = this.getTourleaederFormSearch(this.roleAction);
    	}
		Ext.apply(this,{
			layout: {
		        type: 'border'
		    },
		    items: [{
		        region: 'north',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items:[formSearch]
		    },{
		        region: 'center',
		        xtype: 'panel',
		        layout: 'fit',
		        frame: false,
		        border: false,
		        items:[grid]
		    }]
    	});

    	this.callParent(arguments);
    },
    getBusinessFormSearch: function(roleAction)
    {
    	return {
    		
	            title:'ค้นหาการทำรายการพิมพ์ใบอนุญาตธุรกิจนำเที่ยว',
	            collapsible: true,   // make collapsible
	            // collapsed : true,
	            xtype: 'printlicense-business-formsearch',
	            id: roleAction+'-business'+'-printlicense-formsearch',
	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false
    	};
    },
    getGuideFormSearch: function(roleAction)
    {
    	return {
    		
	            title:'ค้นหาการทำรายการพิมพ์ใบอนุญาตมัคคุเทศก์',
	            collapsible: true,   // make collapsible
	            // collapsed : true,
	            xtype: 'printlicense-guide-formsearch',
	            id: roleAction+'-guide'+'-printlicense-formsearch',
	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false
    	};
    },
    getTourleaederFormSearch: function(roleAction)
    {
    	return {
    		
	            title:'ค้นหาการทำรายการพิมพ์ใบอนุญาตผู้นำเที่ยว',
	            collapsible: true,   // make collapsible
	            // collapsed : true,
	            xtype: 'printlicense-tourleader-formsearch',
	            id: roleAction+'-tourleader'+'-printlicense-formsearch',
	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false
    	};
    },
    getBusinessGrid: function(roleAction)
    {
    	return {
    		 xtype: 'printlicense-grid',
	            id: this.roleAction+'-'+this.traderType+'-printlicense-grid',
	            // padding: '5px 5px 5px 5px',
	            autoScroll: true,
	            // frame: false 
	            border: false,
	            storeId: this.roleAction+'-'+this.traderType+'-printlicense-store',
	            storepageSize: 20,
	            storeUrl: 'business/'+this.roleAction+'/registration/read',
	            roleAction: this.roleAction,
	            traderType: this.traderType
    	};
    },
    getGuideGrid: function(roleAction)
    {
    	return {
    		 xtype: 'printlicense-guide-grid',
	            id: this.roleAction+'-'+this.traderType+'-printlicense-grid',
	            // padding: '5px 5px 5px 5px',
	            autoScroll: true,
	            // frame: false 
	            border: false,
	            storeId: this.roleAction+'-'+this.traderType+'-printlicense-store',
	            storepageSize: 20,
	            storeUrl: 'business/'+this.roleAction+'/registration/read',
	            roleAction: this.roleAction,
	            traderType: this.traderType
    	};
    }
		
});