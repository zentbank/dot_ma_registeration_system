Ext.define('tourism.view.printlicense.PrintLicenseTourLeaderFormSearch',{
	extend: 'Ext.form.FormPanel',
	requires:[
				'Ext.form.field.Date',
				'Ext.form.field.Time',
				'Ext.form.CheckboxGroup',
				'Ext.layout.container.HBox',
				'Ext.form.FieldSet'
	          ],
	
	alias: 'widget.printlicense-tourleader-formsearch',
	
	bodyPadding: 10,
	items: [
            {
        xtype: 'container',
        // title: 'Payment',
        layout: 'anchor',
        defaults: {
            anchor: '100%',
            labelWidth: 150
        },
        items: [
            {
                xtype: 'hidden',
                name: 'traderType',
                value: 'L'
            },
            {
                xtype: 'textfield',
                fieldLabel: 'เลขที่รับเรื่อง',
                name: 'registrationNo',
                anchor: '49.7%',
            },
                                {
                    xtype: 'container',
                    layout: 'hbox',
                    layoutCongig: {
                         pack:'center',
                         align:'middle'
                    },
                    items: [
                        {
                            xtype: 'container',
                            // title: 'Payment',
                            flex: 1,
                            layout: 'anchor',
                            defaults: {
                                anchor: '100%',
                                labelWidth: 150
                            },
                            items: [
                                {
                                    xtype: 'datefield',
                                    fieldLabel: 'วันที่อนุมัติ จากวันที่',
                                    // labelWidth: 150,
                                    name: 'approveDateFrom',
                                    format: 'd/m/B'
                                },
                                {
                                    xtype: 'textfield',
                                     fieldLabel: 'ชื่อ',
                                    name: 'firstName',
                                    allowBlank: true
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'บัตรประจำตัวเลขที่',
                                    name: 'identityNo'
                                },
                                {
                                    xtype: 'combo',
                                    fieldLabel: 'ประเภทการทำรายการ',
                                    store: 'tourism.store.combo.TourleaderRegistrationTypeStore',
                                    queryMode: 'local',
                                    displayField: 'registrationTypeName',
                                    valueField: 'registrationType',
                                    hiddenName: 'registrationType',
                                    name: 'registrationType'
                                }
                            ]
                        },{
                            xtype: 'splitter'
                        },
                        {
                            xtype: 'container',
                            flex: 1,
                            layout: 'anchor',
                            defaults: {
                                anchor: '100%',
                                labelWidth: 150
                            },
                            items: [
                                {   
                                    xtype: 'datefield',
                                    fieldLabel: 'ถึงวันที่',
                                    
                                    name: 'approveDateTo',
                                    flex: 1,
                                    format: 'd/m/B'
                                    // ,value: Ext.Date.add(new Date(), Ext.Date.YEAR, 543), //2554 ณ ปัจจุบัน
                                   
                                },
                                 {
                                    // นามสกุล
                                    xtype: 'textfield',
                                    fieldLabel: 'นามสกุล',
                                    name: 'lastName',
                                    labelAlign: 'left'
                                },
                                 {
                                    xtype: 'combo',
                                    fieldLabel: 'ประเภทผู้นำเที่ยว',
                                    store: 'tourism.store.combo.TourleaderCategoryStore',
                                    queryMode: 'local',
                                    displayField: 'traderCategoryName',
                                    valueField: 'traderCategory',
                                    hiddenName: 'traderCategory',
                                    name :'traderCategory'
                                },
                                {
                                    xtype: 'combo',
                                    fieldLabel: 'สถานะเรื่อง',
                                    store: 'tourism.store.combo.PrintStatusStore',
                                    queryMode: 'local',
                                    displayField: 'printLicenseStatusName',
                                    valueField: 'printLicenseStatus',
                                    hiddenName: 'printLicenseStatus',
                                    name: 'printLicenseStatus',
                                    //value: 'W',
        	                        tabIndex: 9
                                }
                            ]
                        }
                    ]
                }
        ]
    },
    {
        xtype: 'toolbar',
        border: false,
        padding: '6px 0 6px 0px',
        items: [{
        	xtype: 'button',
            text : 'ค้นหา',
            action: 'searchLicenseBetweenRegistration'
        }]
    }

]
});