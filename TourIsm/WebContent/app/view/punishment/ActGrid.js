Ext.define('tourism.view.punishment.ActGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.punishment-act-grid',
		
		// stripeRows: true,
		requires: [
			'Ext.grid.Column',
			'Ext.grid.column.Template',
			'Ext.grid.column.Action',
			'tourism.store.punishment.ActStore'


		],
		initComponent: function(){

			// create every new form 
			var store = Ext.create('tourism.store.punishment.ActStore',{
					storeId: this.storeId,
				    proxy: {
					    type: 'ajax',
					    actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
					    api: {
					        read: this.storeUrl
					    },
					    reader: {
					        type: 'json',
					        // metaProperty: '',
					        root: 'list',
					        // idProperty: 'emailId',
					        totalProperty: 'totalCount',
					        successProperty: 'success',
					        messageProperty: 'messgae'
					    },
					    writer: {
					        type: 'json',
					        // encode: true,
					        writeAllFields: true, ////just send changed fields
					        // root: 'data',
					        allowSingle: false, //always wrap in an array
					        batch: false
					    },
					    listeners: {
					        exception: function(proxy, response, operation){

					            Ext.MessageBox.show({
					                title: 'REMOTE EXCEPTION',
					                msg: operation.getError(),
					                icon: Ext.MessageBox.ERROR,
					                buttons: Ext.Msg.OK
					            });
					        }
					    }
					}
			});

			Ext.apply(this, {
				store: store,
				hideHeaders: true,
				hideMode: 'display' ,
				rowLines: false,
				// frameHeader : false,
				// selType: 'checkboxmodel',
				// mode: 'MULTI',
				// checkSelector : '',
				columns: [
					{
						header: 'suspendId',
						dataIndex: 'suspendId',
						hidden: true
					},
					{
						header: 'revokeId',
						dataIndex: 'revokeId',
						hidden: true
					},
					{
			            xtype: 'checkcolumn',
			            header: '',
			            dataIndex: 'active',
			            width: 30,
			            stopSelection: false
			        },
			        
					{
						header: '',
						dataIndex: 'actDesc',
						flex: 1
					}			
				]
				
			});

			this.callParent(arguments);
		}
	});