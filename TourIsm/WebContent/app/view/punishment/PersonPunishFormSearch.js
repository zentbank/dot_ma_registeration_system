Ext.define('tourism.view.punishment.PersonPunishFormSearch' ,{
    // extend: 'Ext.form.FieldSet',
    extend: 'Ext.form.FormPanel',
    alias : 'widget.person-punishment-formsearch',
    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.Time',
        'Ext.form.CheckboxGroup',
        'Ext.layout.container.HBox',
        'Ext.form.FieldSet'
    ],
    bodyPadding: 10,
    initComponent: function(){

        var status;

        if(this.roleAction == 'revoke')
        {
            status =  {
                            xtype: 'combo',
                            fieldLabel: 'สถานะใบอนุญาต',
                            store: 'tourism.store.combo.ReovkeStatus',
                            queryMode: 'local',
                            displayField: 'revokeStatusName',
                            valueField: 'revokeStatus',
                            hiddenName: 'revokeStatus',
                            name :'revokeStatus'
                        };
        }
        else
        {
            status = {
                            xtype: 'combo',
                            fieldLabel: 'สถานะใบอนุญาต',
                            store: 'tourism.store.combo.SuspendStatus',
                            queryMode: 'local',
                            displayField: 'suspendStatusName',
                            valueField: 'suspendStatus',
                            hiddenName: 'suspendStatus',
                            name :'suspendStatus'
                        };
        }

       var officerStore = Ext.create('tourism.store.combo.AdmUserStore',{
            id: this.roleAction+'-'+this.traderTypeName+'-officer-store'
            ,proxy: {
                type: 'ajax',
                actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
                api: {
                    read: 'tourleader/combo/officergroup'
                },
                reader: {
                    type: 'json',
                    // metaProperty: '',
                    root: 'list',
                    // idProperty: 'emailId',
                    totalProperty: 'totalCount',
                    successProperty: 'success',
                    messageProperty: 'msg'
                },
                writer: {
                    type: 'json',
                    // encode: true,
                    writeAllFields: true, ////just send changed fields
                    // root: 'data',
                    allowSingle: false, //always wrap in an array
                    batch: false
                },
                listeners: {
                    exception: function(proxy, response, operation){

                        Ext.MessageBox.show({
                            title: 'REMOTE EXCEPTION',
                            msg: operation.getError(),
                            icon: Ext.MessageBox.ERROR,
                            buttons: Ext.Msg.OK
                        });
                    }
                }
            }
            ,listeners: {
                beforeload: function( store, operation, eOpts )
                    {
         
                        store.proxy.extraParams = {groupRole: 'OFFLAW'};
                    }
            }
        });

        Ext.apply(this, {
            items: [
                {
                        xtype: 'container',
                        // title: 'Payment',
                        layout: 'anchor',
                        defaults: {
                            anchor: '100%',
                            labelWidth: 250
                        },
                        items: [
                            {
                                xtype: 'container',
                                layout: 'hbox',
                                layoutCongig: {
                                     pack:'center',
                                     align:'middle'
                                },
                                items: [
                                    {
                                        xtype: 'container',
                                        // title: 'Payment',
                                        flex: 1,
                                        layout: 'anchor',
                                        defaults: {
                                            anchor: '100%',
                                            labelWidth: 250
                                        },
                                        items: [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'เลขที่ใบอนุญาต',
                                                name: 'licenseNo'
                                            }
                                        ]
                                    },{
                                        xtype: 'splitter'
                                    },
                                    {
                                        xtype: 'container',
                                        flex: 1,
                                        layout: 'anchor',
                                        defaults: {
                                            anchor: '100%',
                                            labelWidth: 250
                                        },
                                        items: [
                                            {
		                                        xtype: 'textfield',
		                                        fieldLabel: 'เลขบัตรประชาชน',
		                                        name: 'identityNo'
		                                    }
                                        ]
                                    }
                                ]
                            },
                            {
                                xtype: 'container',
                                layout: 'hbox',
                                layoutCongig: {
                                     pack:'center',
                                     align:'middle'
                                },
                                items: [
                                    {
                                        xtype: 'container',
                                        // title: 'Payment',
                                        flex: 1,
                                        layout: 'anchor',
                                        defaults: {
                                            anchor: '100%',
                                            labelWidth: 250
                                        },
                                        items: [
                                             {
                                                xtype: 'textfield',
                                                fieldLabel: 'ชื่อผู้ขออนุญาต',
                                                name: 'firstName',
                                                anchor: '100%'
                                            },
                                              {
                                                xtype: 'combo',
                                                fieldLabel: 'นิติกรผู้รับผิดชอบ',
                                                name: 'officerId',
                                                store: officerStore,
                                                queryMode: 'remote',
                                                displayField: 'userFullName',
                                                valueField: 'userId',
                                                hiddenName: 'officerId',
                                                triggerAction: 'all',
                                                // flex: 1,
                                                // allowBlank: false
                                            }
                                        ]
                                    },{
                                        xtype: 'splitter'
                                    },
                                    {
                                        xtype: 'container',
                                        flex: 1,
                                        layout: 'anchor',
                                        defaults: {
                                            anchor: '100%',
                                            labelWidth: 250
                                        },
                                        items: [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'นามสกุล',
                                                name: 'lastName'
                                            },
                                             status
                                            
                                        ]
                                    }
                                ]
                            }
                            
                        ]
                },
                {
                    xtype: 'toolbar',
                    border: false,
                    padding: '6px 0 6px 0px',
                    items: [{
                        xtype: 'button',
                        text : 'ค้นหา',
                        action: 'searchSuspend'
                    }]
                }

            ]            
        });

        this.callParent(arguments);
    }
    // width: '100%',

});    
