Ext.define('tourism.view.punishment.PunishmentGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.punishment-grid',
		stripeRows: true,
		requires: [
			'Ext.grid.Column',
			'tourism.store.punishment.SuspensionStore',
			'Ext.toolbar.Paging',
			'Ext.grid.plugin.RowEditing',
			'Ext.grid.column.Template',
			'Ext.grid.column.Action',
			'Ext.grid.plugin.RowExpander',
			'Ext.grid.feature.Grouping',
			'tourism.view.punishment.suspension.AddSuspensionWindow',
			'tourism.view.punishment.alert.AlertWindow',
			'tourism.model.punishment.SuspensionModel',
			'tourism.model.punishment.RevokeModel'
		],
		initComponent: function(){

			var punishmentModel = '';
			var columnModel = '';
			if(this.roleAction == 'suspension')
	    	{
	    		punishmentModel = 'tourism.model.punishment.SuspensionModel';
	    		columnModel = this.getSuspensionColumn();
	    	}
	    	else
	    	{
	    		punishmentModel = 'tourism.model.punishment.RevokeModel';
	    		columnModel = this.getRevokeColumn();
	    	}


			var suspensionStore = Ext.create('tourism.store.punishment.SuspensionStore',{
					storeId: this.storeId,
					model : punishmentModel,
					roleAction: this.roleAction,
					traderTypeName: this.traderTypeName,
					pageSize: this.storepageSize,
				    proxy: {
					    type: 'ajax',
					    actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
					    api: {
					        read: this.storeUrl
					    },
					    reader: {
					        type: 'json',
					        // metaProperty: '',
					        root: 'list',
					        // idProperty: 'emailId',
					        totalProperty: 'totalCount',
					        successProperty: 'success',
					        messageProperty: 'message'
					    },
					    writer: {
					        type: 'json',
					        // encode: true,
					        writeAllFields: true, ////just send changed fields
					        // root: 'data',
					        allowSingle: false, //always wrap in an array
					        batch: false
					    },
					    listeners: {
					        exception: function(proxy, response, operation){

					            Ext.MessageBox.show({
					                title: 'REMOTE EXCEPTION',
					                msg: operation.getError(),
					                icon: Ext.MessageBox.ERROR,
					                buttons: Ext.Msg.OK
					            });
					        }
					    }
					}
					,listeners: {
						beforeload: function( store, operation, eOpts )
					        {
					        	// //console.log(store.baseParam);
					        	// //console.log(store);
					        	// store.proxy.extraParams = {test: 123};

					        	var formId = '#punishment-'+store.roleAction+'-'+store.traderTypeName+'-formsearch';

					        	var formSearch = Ext.ComponentQuery.query(formId)[0];
					        	var values = formSearch.getValues();
   								
					        	 for (field in values) 
							      {
							        if (Ext.isEmpty(values[field])) 
							        {
							          delete values[field];
							        }
							        
							      }
							    store.proxy.extraParams = values;
					        }
					}
			});

			var btnText = '';

			if(this.roleAction == 'suspension')
	    	{
	    		btnText = 'เพิ่มข้อมูลการพักใช้ใบอนุญาต';
	    	}
	    	else
	    	{
	    		btnText = 'เพิ่มข้อมูลการเพิกถอนใบอนุญาต';
	    	}

	   

			Ext.apply(this, {
				store: suspensionStore,
				dockedItems:[
					{
						xtype: 'pagingtoolbar',
						store: suspensionStore,
						dock: 'bottom',
						displayInfo: true,
						items: [
							{

									text: btnText,
									// iconCls: 'icon-add',
									// itemId: 'add-business-registrationgrid-btn',
									action: 'addpunishment'
									// ,
									// handler: function()
									// {
									
									// 	var win = Ext.create('tourism.view.punishment.suspension.AddSuspensionWindow');
									// 	win.show();
									// }
								
			        		}
						]

					}
				],
				columns: columnModel
			});

			this.callParent(arguments);

		},
		 plugins: [{
            ptype: 'rowexpander',
            // pluginId: 'rowexpanderTourleader',
            rowBodyTpl : new Ext.XTemplate(
            	'<div>',
            	'<tpl switch="traderType">',
		            '<tpl case="B">',
		                '<p><b>ออกใบอนุญาตให้: {traderOwnerName}</b></p>',
	            		'<p><b>ทะเบียนนิติบุคคลเลขที่: {identityNo}</b></p>',
	            		'<p><b>ประเภทธุรกิจนำเที่ยว: {traderCategoryName}</b></p>',
		            	'<p><b>ชื่อธุรกิจนำเที่ยว (ภาษาไทย): {traderName}</b></p>',
		                '<p><b>ชื่อธุรกิจนำเที่ยว (ภาษาต่างประเทศ): {traderNameEn}</b> </p>',
		                '<p><b>เลขที่รับเรื่อง: {suspensionNo}</b> </p>',
		                '<p><b>วันที่รับเรื่อง : {suspensionDate}</b> </p>',
		                // '<p><font size="2">{traderAddress}</font></p>',
		            '<tpl case="G">',
		            	'<p><b>ออกใบอนุญาตให้: {traderOwnerName}</b></p>',
	            		'<p><b>หมายเลขบัตรประชาชน: {identityNo}</b></p>',
	            		'<p><b>ประเภทมัคคุเทศก์: {traderCategoryName}</b></p>',
	            		'<p><b>เลขที่รับเรื่อง: {suspensionNo}</b> </p>',
		                '<p><b>วันที่รับเรื่อง : {suspensionDate}</b> </p>',
		                // '<p><font size="2">{traderAddress}</font></p>',
		            '<tpl default>',
		            	'<p><b>ออกใบอนุญาตให้: {traderOwnerName}</b></p>',
	            		'<p><b>หมายเลขบัตรประชาชน: {identityNo}</b></p>',
	            		'<p><b>ประเภทมัคคุเทศก์: {traderCategoryName}</b></p>',
	            		'<p><b>เลขที่รับเรื่อง: {suspensionNo}</b> </p>',
		                '<p><b>วันที่รับเรื่อง : {suspensionDate}</b> </p>',
		                // '<p><font size="2">{traderAddress}</font></p>',
		        '</tpl>',
            		
                '<div>'
            )
        }],
        getSuspensionColumn: function()
        {
        	return [
					{
						header: 'suspendId',
						dataIndex: 'suspendId',
						hidden: true
					},
					{
						header: 'ใบอนุญาต',
						dataIndex: 'traderTypeName',
						hidden: true
					},
					{
						header: 'เลขที่ใบอนุญาต',
						dataIndex: 'licenseNo',
						flex: 1
					},
					{
						header: 'ชื่อ',
						dataIndex: 'traderName',
						flex: 2
					},
					{
						header: 'วันที่พักใช้',
						dataIndex: 'suspendFrom',
						flex: 1
					},
					{
						header: 'ถึงวันที่',
						dataIndex: 'suspendTo',
						flex: 1
					},
					{
						header: 'สถานะ',
						dataIndex: 'suspendStatus',
						flex: 2,
						renderer: function(value, metaData, model){
							
							        /*
                                    {"suspendStatus":"S", "suspendStatusName":"พักใช้ใบอนุญาต"},
        {"suspendStatus":"C", "suspendStatusName":"ยกเลิกการพักใช้"},
         {"suspendStatus":"N", "suspendStatusName":"ครบกำหนดการพักใช้"},
        {"suspendStatus":"W", "suspendStatusName":"อยู่ระหว่างดำเนินการพักใช้"}
                            */
                            
							if(value == "S")
							{
								return '<span class="tour-fg-color-redLight">พักใช้ใบอนุญาต</span>';
							}
							if(value == "C")
							{
								return '<span class="tour-fg-color-teal">ยกเลิกการพักใช้</span>';
							}
							if(value == "N")
							{
								return '<span class="tour-fg-color-green">ครบกำหนดการพักใช้</span>';
							}
							if(value == "W")
							{
								return '<span class="tour-fg-color-yellow">อยู่ระหว่างดำเนินการพักใช้</span>';
							}
							if(value == "A")
							{
								return '<span class="tour-fg-color-yellow">อนุมัติให้พักใช้</span>';
							}
							
						}
					},
					{
						header: 'ผู้รับผิดชอบเรื่อง',
						dataIndex: 'officerName',
						flex: 2
					},
			        {
			        	header: 'รายละเอียด',
			            xtype: 'actioncolumn',
			           	width:80,
			           	align : 'center',
			            items: [{
			                iconCls: 'icon-edit-xsmall',
			                tooltip: 'รายละเอียด',
				            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
				            	
				            	if (record.get('officerAsOwner') == '1') {
				            		this.fireEvent('itemclick', this, 'punishmentdetail', grid, rowIndex, colIndex, record, node);

				            	}
				            	else
				            	{
				            		this.fireEvent('itemclick', this, 'viewpunishmentdetail', grid, rowIndex, colIndex, record, node);

				            	}
				            		
				            }
			            }]
			        },
			        {
			        	header: 'การแจ้งเตือน',
			            xtype: 'actioncolumn',
			           	width:100,
			           	align : 'center',
			            items: [{
			                iconCls: 'icon-edit-xsmall',
			                tooltip: 'การแจ้งเตือน',
				            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {

				            	if (record.get('officerAsOwner') == '1')
				            	{
				            		this.fireEvent('itemclick', this, 'suspendAlert', grid, rowIndex, colIndex, record, node);
				            	}
				            	else
				            	{
				            		this.fireEvent('itemclick', this, 'viewsuspendAlert', grid, rowIndex, colIndex, record, node);
				            	}

				            		
							}
			            }]
			        },
			        {
			        	header: 'ยกเลิก',
			            xtype: 'actioncolumn',
			           	width:60,
			           	align : 'center',
			            items: [{
			                // iconCls: 'icon-edit-xsmall',
			                // tooltip: 'รายละเอียด',
			               	getClass: function(v, meta, record) {          
			               		if (record.get('officerAsOwner') == '1')
			               		{
				                    if (record.get('suspendStatus') == 'S') {
				                        this.items[0].tooltip = 'ยกเลิกการพักใช้';
				                        return 'icon-edit-xsmall';
				                    } 
				                    
			               		}
			               		else
			               		{
			               			return '';
			               		}

			                },
				            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {

				            	if (record.get('officerAsOwner') == '1')
				            	{
				            		if (record.get('suspendStatus') == 'S') {
				            			this.fireEvent('itemclick', this, 'cancelsuspension', grid, rowIndex, colIndex, record, node);
			                        
				                    } 
				            	}
				            					            		
							}
			            }]
			        }
				];
        },
        getRevokeColumn: function()
        {
        	return [
					{
						header: 'revokeId',
						dataIndex: 'revokeId',
						hidden: true
					},
					{
						header: 'ใบอนุญาต',
						dataIndex: 'traderTypeName',
						hidden: true
					},
					{
						header: 'เลขที่ใบอนุญาต',
						dataIndex: 'licenseNo',
						flex: 1
					},
					{
						header: 'ชื่อ',
						dataIndex: 'traderName',
						flex: 2
					},
					{
						header: 'วันที่เพิกถอน',
						dataIndex: 'revokeDate',
						flex: 1
					},
					{
						header: 'สถานะ',
						dataIndex: 'revokeStatus',
						flex: 2,
						renderer: function(value, metaData, model){
							
							        /*
                                    {"suspendStatus":"S", "suspendStatusName":"พักใช้ใบอนุญาต"},
        {"suspendStatus":"C", "suspendStatusName":"ยกเลิกการพักใช้"},
         {"suspendStatus":"N", "suspendStatusName":"ครบกำหนดการพักใช้"},
        {"suspendStatus":"W", "suspendStatusName":"อยู่ระหว่างดำเนินการพักใช้"}
                            */
                            
							if(value == "R")
							{
								return '<span class="tour-fg-color-redLight">เพิกถอนใบอนุญาต</span>';
							}
							if(value == "C")
							{
								return '<span class="tour-fg-color-teal">ยกเลิกการเพิกถอน</span>';
							}
							if(value == "N")
							{
								return '<span class="tour-fg-color-green">ครบกำหนดการเพิกถอน</span>';
							}
							if(value == "W")
							{
								return '<span class="tour-fg-color-yellow">อยู่ระหว่างดำเนินการเพิกถอน</span>';
							}
							if(value == "A")
							{
								return '<span class="tour-fg-color-yellow">อนุมัติให้เพิกถอน</span>';
							}
							
						}
					},
					{
						header: 'ผู้รับผิดชอบเรื่อง',
						dataIndex: 'officerName',
						flex: 2
					},
			        {
			        	header: 'รายละเอียด',
			            xtype: 'actioncolumn',
			           	width:80,
			           	align : 'center',
			            items: [{
			                iconCls: 'icon-edit-xsmall',
			                tooltip: 'รายละเอียด',
				            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {

				            	if (record.get('officerAsOwner') == '1') {
				            		this.fireEvent('itemclick', this, 'punishmentdetail', grid, rowIndex, colIndex, record, node);

				            	}
				            	else
				            	{
				            		this.fireEvent('itemclick', this, 'viewpunishmentdetail', grid, rowIndex, colIndex, record, node);

				            	}
				            		
				            }
				            
			            }]
			        },
			        {
			        	header: 'การแจ้งเตือน',
			            xtype: 'actioncolumn',
			           	width:100,
			           	align : 'center',
			            items: [{
			                iconCls: 'icon-edit-xsmall',
			                tooltip: 'การแจ้งเตือน',
				            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {

				            	if (record.get('officerAsOwner') == '1')
				            	{
				            		this.fireEvent('itemclick', this, 'suspendAlert', grid, rowIndex, colIndex, record, node);
				            	}
				            	else
				            	{
				            		this.fireEvent('itemclick', this, 'viewsuspendAlert', grid, rowIndex, colIndex, record, node);
				            	}
							}
			            }]
			        },
			        {
			        	header: 'ยกเลิก',
			            xtype: 'actioncolumn',
			           	width:60,
			           	align : 'center',
			            items: [{
   
			               	getClass: function(v, meta, record) {          
			                    
			                    if (record.get('officerAsOwner') == '1')
			               		{
				                    if (record.get('revokeStatus') == 'R') {
				                        this.items[0].tooltip = 'ยกเลิกการเพิกถอน';
				                        return 'icon-edit-xsmall';
				                    } 
				                    
			               		}
			               		else
			               		{
			               			return '';
			               		}
			                },
				            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
			                
			                    if (record.get('officerAsOwner') == '1')
				            	{
				            		if (record.get('revokeStatus') == 'R') {
				            			this.fireEvent('itemclick', this, 'cancelsuspension', grid, rowIndex, colIndex, record, node);
			                        
				                    } 
				            	}
				            	
				            		
							}
			            }]
			        }
				];
        }
	});