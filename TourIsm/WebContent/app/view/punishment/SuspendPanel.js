Ext.define('tourism.view.punishment.SuspendPanel', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
        'tourism.view.punishment.BusinessPunishFromSearch',
        'tourism.view.punishment.PersonPunishFormSearch',
        'tourism.view.punishment.PunishmentGrid'
        
    ],
    alias : 'widget.punishment-suspension-panel',
    initComponent: function(){
    	var title = 'ค้นรายการเพิกถอนใบอนุญาต'
    	if(this.roleAction == 'suspension')
    	{
    		title = 'ค้นรายการพักใช้ใบอนุญาต'
    	}

	    if(this.traderType == 'B')
		{
			title = title + 'ธุรกิจนำเที่ยว';
		}
            //id: 'punishment-suspension-business-formsearch',
            //id: 'punishment-revoke-business-formsearch',

	    var formSearch = {
            title:title,
            collapsible: true,   // make collapsible
            collapsed : false,
            xtype: 'business-punishment-formsearch',
            id: 'punishment-'+this.roleAction+'-'+this.traderTypeName+'-formsearch',

            // padding: '5px 5px 0px 5px',
            frame: false,
            border: false,
            roleAction: this.roleAction,
            traderType: this.traderType,
            traderTypeName: this.traderTypeName
		};

    	if(this.traderType != 'B')
    	{
    		if(this.traderType == 'G')
    		{
    			title = title + 'มัคคุเทศก์';
    		}
    		 if(this.traderType == 'L')
    		{
    			title = title + 'ผู้นำเที่ยว';
    		}

			formSearch = {
	            title:title,
	            collapsible: true,   // make collapsible
	            collapsed : false,
	            xtype: 'person-punishment-formsearch',
	            id: 'punishment-'+this.roleAction+'-'+this.traderTypeName+'-formsearch',

	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false,
	            roleAction: this.roleAction,
	            traderType: this.traderType,
	            traderTypeName: this.traderTypeName
			};
    	}



    	Ext.apply(this, {
    		layout: {
		        type: 'border'
		    },
		    items: [{
		        region: 'north',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items:[formSearch]
		    },{
		        region: 'center',
		        xtype: 'panel',
		        layout: 'fit',
		        frame: false,
		        border: false,
		        items:[
		        {
		            xtype: 'punishment-grid',
		            id: 'punishment-'+this.roleAction+'-'+this.traderTypeName+'-grid',
		            // padding: '5px 5px 5px 5px',
		            autoScroll: true,
		            // frame: false 
		            border: false,
		            storeId: 'punishment-'+this.roleAction+'-'+this.traderTypeName+'-store',
		            storepageSize: 20,
		            storeUrl: 'punishment/'+this.roleAction+'/'+this.traderTypeName+'/read',
		            roleAction: this.roleAction,
		            traderType: this.traderType,
		            traderTypeName: this.traderTypeName
		            
		        }
		        ]
		    }]
    	});

    	this.callParent(arguments);
    }
});