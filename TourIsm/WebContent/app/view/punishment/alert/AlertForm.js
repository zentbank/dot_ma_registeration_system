Ext.define('tourism.view.punishment.alert.AlertForm', {
    extend: 'Ext.form.Panel',
    alias : 'widget.punishment-alert-alertform',
    requires: [
        'Ext.data.*',
        'Ext.form.*',
        'Ext.tab.Tab',
        'tourism.view.punishment.alert.PostFieldSet',
        'tourism.view.punishment.alert.EmailFieldSet',
        'tourism.model.punishment.SuspensionAlertModel'
    ],
   
    bodyPadding: 5,
    autoScroll: true,

    initComponent: function(){

    
        
        Ext.apply(this, {
            width: '100%',
            height: '100%',
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },

            items: 
            [
                {
                    xtype: 'punishment-alert-emailfieldset'
                }
                ,{
                    xtype: 'punishment-alert-postfieldset'
                }
                // {
                //     html:'test'
                // }
               
            ],

            buttons: [{
                text: 'บันทึก',
                scope: this,
                action: 'savePostAlert',
                 id: 'punishment-'+this.roleAction+'-'+this.traderTypeName+'-savePostAlert'
            }, {
                text: 'ยกเลิก',
                // width: 150,
                scope: this,
                handler: this.onResetClick
            }]    
        });
        this.callParent(arguments);
    }
    ,onResetClick: function(){
        this.up('window').close();
    }
});