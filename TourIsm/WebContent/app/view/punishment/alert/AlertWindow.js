Ext.define('tourism.view.punishment.alert.AlertWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.punishment-alert-alertWindow',

    requires: [
        'tourism.view.punishment.alert.AlertForm'
    ],
    
    initComponent: function() {


        Ext.apply(this, {
            title:'การแจ้งเตือน',
            // layout: 'fit',
            // layout: 'fit',
            activeItem: 0,
            autoShow: true,
            // width: 600,
            // height: 400,
            autoShow: true,
            modal: true,
            items :[
                {
                   xtype: 'punishment-alert-alertform',
                   id: 'punishment-'+this.roleAction+'-'+this.traderTypeName+'-alertform',
                   border: false,
                   frame: false,
                    roleAction: this.roleAction,
                    traderType: this.traderType,
                    traderTypeName: this.traderTypeName
                }
            ]
        });
        this.callParent(arguments);
    },

});
