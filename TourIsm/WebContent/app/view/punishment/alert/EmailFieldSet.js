Ext.define('tourism.view.punishment.alert.EmailFieldSet', {
    extend: 'Ext.form.FieldSet',
    alias : 'widget.punishment-alert-emailfieldset',

    requires:[
        'Ext.form.FieldContainer',
        'Ext.form.field.VTypes'
    ],

     initComponent: function(){


         Ext.apply(this, {
            checkboxToggle:true,
            checkboxName : 'emailAlertType',
            title: 'การแจ้งเตือนทางอีเมล',
            // defaultType: 'textfield',
            collapsed: false,
            layout: 'anchor',
            defaults: {
                anchor: '100%'
            },
            items :
            [ 
                {
                    xtype: 'displayfield',
                    fieldLabel: 'วันที่แจ้งเตือน',
                    name: 'emailAlertDate'
                           
                },
                {
                    xtype: 'displayfield',
                    fieldLabel: 'วันที่ตอบรับ',
                    name: 'emailResponseDate'
                   
                },
                {
                    xtype: 'displayfield',
                    fieldLabel: 'วันที่ตอบรับ',
                    name: 'emailAlertStatus'
                   
                }
            ]
         });

        this.callParent(arguments);
     }

});