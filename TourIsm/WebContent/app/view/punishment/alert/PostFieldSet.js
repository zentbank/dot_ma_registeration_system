Ext.define('tourism.view.punishment.alert.PostFieldSet', {
    extend: 'Ext.form.FieldSet',
    alias : 'widget.punishment-alert-postfieldset',

    requires:[
        'Ext.data.*',
        'Ext.form.*',
        'Ext.form.FieldContainer',
        'Ext.form.field.VTypes'
    ],

     initComponent: function(){

        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

         Ext.apply(this, {
            checkboxToggle:true,
            checkboxName : 'postalertType',
            title: 'การแจ้งเตือนทางจดหมายลงทะเบียน',
            // defaultType: 'textfield',
            collapsed: false,
            layout: 'anchor',
            defaults: {
                anchor: '100%'
            },
            items :
            [
                {
                    xtype: 'hiddenfield',
                    name: 'suspendId'
                },

                {
                    xtype: 'hiddenfield',
                    name: 'revokeId'
                },
                 {
                    xtype: 'hiddenfield',
                    name: 'revokeAlertId'
                },
                {
                    xtype: 'hiddenfield',
                    name: 'suspendAlertId'
                },               
                {
                    xtype: 'datefield',
                    fieldLabel: 'วันที่แจ้ง',
                    name: 'alertDate',
                    format: 'd/m/B',
                    allowBlank: false,
                    afterLabelTextTpl: required
                   
                },
                {
                    xtype: 'datefield',
                    fieldLabel: 'วันที่ตอบรับ',
                    name: 'responseDate',
                    format: 'd/m/B',
                    allowBlank: false,
                    afterLabelTextTpl: required
                   
                },
                
                {
                    xtype: 'combo',
                    fieldLabel: 'การตอบรับ',
                    store: 'tourism.store.combo.AlertStatusStore',
                    queryMode: 'local',
                    displayField: 'alertStatusName',
                    valueField: 'alertStatus',
                    hiddenName: 'alertStatus',
                    name: 'alertStatus',
                    allowBlank: false,
                    afterLabelTextTpl: required
                }
            ]
         });

        this.callParent(arguments);
     }

});