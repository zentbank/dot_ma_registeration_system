Ext.define('tourism.view.punishment.evidence.EvidenceForm', {
    extend: 'Ext.form.Panel',
    alias : 'widget.punishment-evidence-form',
    requires: [
        'Ext.data.*',
        'Ext.form.*',
        'Ext.tab.Tab'
        
    ],
   
    bodyPadding: 5,
    autoScroll: true,

    initComponent: function(){

     var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
        
        Ext.apply(this, {
            width: '100%',
            height: '100%',
            // layout: 'anchor',
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },
            items: 
            [
                {
                    xtype: 'hiddenfield',
                    name: 'suspendId'
                },

                {
                    xtype: 'hiddenfield',
                    name: 'revokeId'
                },
                {
                  xtype: 'hiddenfield',
                  name: 'traderType'
                }
                ,
                {
                  xtype: 'hiddenfield',
                  name: 'punishmentType'
                }
                ,       
                {
                    xtype: 'hiddenfield',
                    name: 'licenseNo'
                }
                 ,{
                    xtype: 'textfield',
                    fieldLabel: 'ชื่อเอกสารคำสั่ง',
                    name: 'punishmentDocName',
                    anchor: '100%',
                    allowBlank: false,
                    afterLabelTextTpl: required
                }
                ,{
                    xtype: 'filefield',
                    // id: 'form-file',
                    emptyText: 'เลือกเอกสารที่ต้องการ',
                    fieldLabel: 'เอกสารคำสั่ง',
                    name: 'evidence',
                    allowBlank: false,
                    afterLabelTextTpl: required,
                    anchor: '100%',
                    buttonText: '',
                    buttonConfig: {
                        iconCls: 'upload-icon'
                    }
                }
            ],

            buttons: [{
                text: 'บันทึก',
                scope: this,
                action: 'saveEvidence',
                id: 'punishment-'+this.roleAction+'-'+this.traderTypeName+'-saveEvidence'
            }, {
                text: 'ยกเลิก',
                // width: 150,
                scope: this,
                handler: this.onResetClick
            }]    
        });
        this.callParent(arguments);
    }
    ,onResetClick: function(){
        this.up('window').close();
    }
});