Ext.define('tourism.view.punishment.evidence.EvidenceGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.punishment-evidence-grid',
		stripeRows: true,
		requires: [
			'Ext.grid.Column',
			'tourism.store.punishment.EvidenceStore',
			'Ext.toolbar.Paging',
			'Ext.grid.plugin.RowEditing',
			'Ext.grid.column.Template',
			'Ext.grid.column.Action',
			'Ext.grid.plugin.RowExpander',
			'Ext.grid.feature.Grouping',
			'Ext.grid.column.RowNumberer'
			// ,
			// 'tourism.view.punishment.evidence.EvidenceWindow'
		],
		initComponent: function(){

			
			var columnModel = '';
			if(this.roleAction == 'suspension')
	    	{
	    		
	    		columnModel = this.getSuspensionColumn();
	    	}
	    	else
	    	{
	    		
	    		columnModel = this.getRevokeColumn();
	    	}


			var evidenceStore = Ext.create('tourism.store.punishment.EvidenceStore',{
					storeId: this.storeId,
				    proxy: {
					    type: 'ajax',
					    actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
					    api: {
					        read: 'punishment/suspension/evidence/read',
					        destroy: 'punishment/suspension/evidence/delete'
					    },
					    reader: {
					        type: 'json',
					        // metaProperty: '',
					        root: 'list',
					        // idProperty: 'emailId',
					        totalProperty: 'totalCount',
					        successProperty: 'success',
					        messageProperty: 'message'
					    },
					    writer: {
					        type: 'json',
					        // encode: true,
					        writeAllFields: true, ////just send changed fields
					        // root: 'data',
					        allowSingle: false, //always wrap in an array
					        batch: false
					    },
					    listeners: {
					        exception: function(proxy, response, operation){

					            Ext.MessageBox.show({
					                title: 'REMOTE EXCEPTION',
					                msg: operation.getError(),
					                icon: Ext.MessageBox.ERROR,
					                buttons: Ext.Msg.OK
					            });
					        }
					    }
					}
			});

		
			Ext.apply(this, {
				store: evidenceStore,
				dockedItems: [{
				    xtype: 'toolbar',
				    dock: 'top',
				    // ui: 'footer',
				    // defaults: {minWidth: minButtonWidth},
				    items: [
				        { xtype: 'component', flex: 1 },
				        { 
				        	xtype: 'button',
				        	text: 'เพิ่มเอกสาร',
				        	action: 'addevidence'
				        }
				    ]
				}],
				columns: columnModel
			});

			this.callParent(arguments);

		},
        getSuspensionColumn: function()
        {
        	return [

					{
						header: 'punishmentId',
						dataIndex: 'punishmentId',
						hidden: true
					},
					{
						header: 'suspendId',
						dataIndex: 'suspendId',
						hidden: true
					},
					{xtype: 'rownumberer'},
					{
						header: 'ชื่อเอกสาร',
						dataIndex: 'punishmentDocName',
						flex: 5
					},
					
					
					 {
			        	header: 'รายละเอียด',
			            xtype: 'actioncolumn',
			           	flex: 1,
			           	align : 'center',
			            items: [{
			                iconCls: 'icon-edit-xsmall',
			                tooltip: 'รายละเอียด',
				            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
			                   this.fireEvent('itemclick', this, 'viewevidence', grid, rowIndex, colIndex, record, node);
				            		
							}
			            }]
			        },
					
			        {
			        	header: 'ลบ',
			            xtype: 'actioncolumn',
			           	flex: 1,
			           	align : 'center',
			            items: [{
			                iconCls: 'icon-delete-xsmall',
			                tooltip: 'ลบเอกสาร',
				            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
			                   this.fireEvent('itemclick', this, 'deleteevidence', grid, rowIndex, colIndex, record, node);
				            		
							}
			            }]
			        }
				];
        },
        getRevokeColumn: function()
        {
        	return [
        			{
						header: 'punishmentId',
						dataIndex: 'punishmentId',
						hidden: true
					},
					{
						header: 'revokeId',
						dataIndex: 'revokeId',
						hidden: true
					},
					{xtype: 'rownumberer'},
					{
						header: 'ชื่อเอกสาร',
						dataIndex: 'punishmentDocName',
						flex: 5
						
					},
					
					 {
			        	header: 'รายละเอียด',
			            xtype: 'actioncolumn',
			           	flex: 1,
			           	align : 'center',
			            items: [{
			                iconCls: 'icon-edit-xsmall',
			                tooltip: 'รายละเอียด',
				            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
			                   this.fireEvent('itemclick', this, 'viewevidence', grid, rowIndex, colIndex, record, node);
				            		
							}
			            }]
			        },
					
			        {
			        	header: 'ลบ',
			            xtype: 'actioncolumn',
			           	flex: 1,
			           	align : 'center',
			            items: [{
			                iconCls: 'icon-edit-xsmall',
			                tooltip: 'ลบเอกสาร',
				            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
			                   this.fireEvent('itemclick', this, 'deleteevidence', grid, rowIndex, colIndex, record, node);
				            		
							}
			            }]
			        }
				];
        }
	});