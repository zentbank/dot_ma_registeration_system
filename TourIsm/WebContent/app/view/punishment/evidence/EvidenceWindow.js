Ext.define('tourism.view.punishment.evidence.EvidenceWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.punishment-evidence-add-window',

    requires: [
        
        'tourism.view.punishment.evidence.EvidenceForm'
    ],
    
    initComponent: function() {
            

        Ext.apply(this, {
            title:'เอกสารคำสั่ง',
            
            // layout: 'card',
            activeItem: 0,
            autoShow: true,
            width: 400,
            // height: 600,
            autoShow: true,
            modal: true,
            items :[
                {
                    // title: 'บันทึกข้อมูลการพักใช้',
                    xtype: 'punishment-evidence-form',
                    id: 'punishment-'+this.roleAction+'-'+this.traderTypeName+'-evidence-form',
                    border: false,
                    frame: false,
                    roleAction: this.roleAction,
                    traderType: this.traderType,
                    traderTypeName: this.traderTypeName
                }
            ]
        });

        this.callParent(arguments);
    }

});
