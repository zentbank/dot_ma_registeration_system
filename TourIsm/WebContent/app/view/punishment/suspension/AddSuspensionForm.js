Ext.define('tourism.view.punishment.suspension.AddSuspensionForm', {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.data.*',
        'Ext.form.*',
        'Ext.tab.Tab',
        'tourism.view.punishment.ActGrid'
    ],
    alias: 'widget.punishment-suspension-addeditform',
    // title: 'ธุรกิจนำเที่ยว',
    bodyPadding: 5,
    autoScroll: true,

    initComponent: function(){

        var licenseDetail;

        if(this.traderType == 'B')
        {
            licenseDetail = this.businessDetail();
        }
        else
        {
            licenseDetail = this.personDetail();
        }


        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
        
        Ext.apply(this, {
            width: '100%',
            height: '100%',
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },

            items: 
            [ 
                {
                    xtype: 'container',
                    // title: 'Payment',
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%',
                        labelWidth: 250
                    },
                    items: 
                    [ 
                        {
                            xtype: 'container',
                            layout: 'hbox',
                            margin: '0 0 5 0',
                            items: [
                                {
                                    xtype: 'hiddenfield',
                                    name: 'suspendId'
                                },
                                {
                                    xtype: 'hiddenfield',
                                    name: 'traderId'
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'suspensionNo',
                                    // labelAlign: 'left',
                                    fieldLabel: 'เลขที่รับเรื่อง',
                                    flex: 1,
                                    allowBlank: false,
                                    afterLabelTextTpl: required
                                },{
                                    xtype: 'splitter'
                                },{
                                    xtype: 'datefield',
                                    fieldLabel: 'วันที่รับเรื่อง',
                                    name: 'suspensionDate',
                                    flex: 1,
                                    format: 'd/m/B',
                                    allowBlank: false,
                                    afterLabelTextTpl: required
                                   
                                },{
                                    xtype: 'splitter'
                                },{
                                    xtype: 'textfield',
                                    name: 'officerName',
                                    fieldLabel: 'ผู้รับเรื่อง',
                                    flex: 1,
                                    allowBlank: false,
                                    margin: '0 0 0 5',
                                    allowBlank: false,
                                    afterLabelTextTpl: required
                                }
                            ]
                        }, 
                        // รายละเอียด license
                        licenseDetail,
                        {
                            xtype: 'tabpanel',
                            plain: true,
                            frame: false,
                            border: false,
                            items: [
                                {
                                    xtype: 'container',
                                    layout: 'anchor',
                                    margin: '5 0 0 0',
                                    defaults: {
                                        anchor: '100%',
                                        labelWidth: 250
                                    },
                                    title: 'รายละเอียดการพักใช้',
                                    items: [
                                        {
                                           html: '<B>สาเหตุการพักใช้ </B>:',
                                           // flex: 1,
                                           frame: false, 
                                            border: false
                                        },{
                                            xtype: 'splitter'
                                        }, {
                                            xtype: 'punishment-act-grid',
                                            autoScroll: true,
                                            frame: false, 
                                            border: false,
                                            roleAction: this.roleAction,
                                            traderType: this.traderType,
                                            traderTypeName: this.traderTypeName,
                                            storeId: 'punishment-'+this.roleAction+'-'+this.traderTypeName+'-store',
                                            storeUrl: 'punishment/'+this.roleAction+'/'+this.traderTypeName+'/masact/read'
                                        }
                                        ,{
                                            xtype: 'splitter'
                                        }, {
                                            xtype: 'textareafield',
                                            name: 'suspendDetail',
                                            fieldLabel: 'รายละเอียดการพักใช้',
                                            allowBlank: false,
                                            afterLabelTextTpl: required
                                        },
                                        {
                                      
                                            xtype: 'fieldcontainer',
                                            layout: 'hbox',
                                            items:[
                                                {
                                      
                                                    xtype: 'fieldcontainer',
                                                    layout: 'hbox',
                                                    flex: 2,
                                                    items:[
                                                        {
                                                            xtype: 'numberfield',
                                                            name: 'suspendPeriod',
                                                            fieldLabel: 'ระยะเวลาที่ถูกพักใช้',
                                                            value: 6,
                                                            labelWidth: 250,
                                                            flex: 5,
                                                            minValue: 1,
                                                            maxValue: 6,
                                                            allowExponential: false,
                                                            allowBlank: false,
                                                            afterLabelTextTpl: required
                                                        },{xtype: 'splitter'}
                                                        ,{
                                                             xtype: 'label',
                                                             text: 'เดือน',
                                                             flex: 1,
                                                             labelAlign: 'left'
                                                        }
                                                    ]
                                                }
                                                ,{
                                      
                                                    xtype: 'fieldcontainer',
                                                    layout: 'hbox',
                                                    flex: 1,
                                                    items:[
                                                        {
                                                            xtype: 'numberfield',
                                                            name: 'suspendPeriodDay',
                                                            // fieldLabel: 'ระยะเวลาที่ถูกพักใช้',
                                                            value: 0,
                                                            flex: 5,
                                                            minValue: 0,
                                                            // maxValue: 6,
                                                            allowExponential: false,
                                                            allowBlank: false,
                                                            afterLabelTextTpl: required
                                                        },{xtype: 'splitter'}
                                                        ,{
                                                             xtype: 'label',
                                                             text: 'วัน',
                                                             value: 0,
                                                             maxValue: 30,
                                                             flex: 1,
                                                             labelAlign: 'left'
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                        , {
                                            xtype: 'displayfield',
                                            fieldLabel: 'เริ่มพักใช้ตั้งแต่วันที่',
                                            name: 'suspendFrom'
                                           
                                        },
                                         {
                                            xtype: 'displayfield',
                                            fieldLabel: 'พักใช้ถึงวันที่',
                                            name: 'suspendTo'
                                           
                                        },
                                        {
                                            xtype: 'radiogroup',
                                            id: 'punishment-'+this.roleAction+'-'+this.traderTypeName+'-suspendStatus-checkboxgroup',
                                            fieldLabel: 'สถานะการอนุมัติ',
                                            allowBlank: false,
                                            afterLabelTextTpl: required,
                                            columns: 1,
                                            vertical: true,
                                            items: [
                                                { boxLabel: 'รับเรื่องรออนุมัติ', name: 'suspendStatus', inputValue: 'W' , checked: true},
                                                { boxLabel: 'อนุมัติการพักใช้', name: 'suspendStatus', inputValue: 'A' }
                                               
                                            ],
                                            listeners: {
                                                
                                                change : {
                                                    fn: function(radiogroup, newValue, oldValue, eOpts){ 
                                                        
                                                        if(newValue.suspendStatus == 'W')
                                                        {

                                                            var formSuspend = radiogroup.up('punishment-suspension-addeditform'); 

                                                            formSuspend.down('#punishment-suspension-'+formSuspend.traderTypeName+'-approveSuspension').setVisible(false);
                                                            formSuspend.down('#punishment-suspension-'+formSuspend.traderTypeName+'-addSuspension').setVisible(true);
                                                            formSuspend.down('#punishment-suspension-'+formSuspend.traderTypeName+'-printSuspension').setVisible(true);
                                                            formSuspend.down('#punishment-suspension-'+formSuspend.traderTypeName+'-printSuspensionTime').setVisible(true);                                                                                                     

                                                        }
                                                        else if(newValue.suspendStatus == 'A')
                                                        {
                                                            var formSuspend = radiogroup.up('punishment-suspension-addeditform'); 

                                                            formSuspend.down('#punishment-suspension-'+formSuspend.traderTypeName+'-approveSuspension').setVisible(true);
                                                            formSuspend.down('#punishment-suspension-'+formSuspend.traderTypeName+'-addSuspension').setVisible(false);
                                                            formSuspend.down('#punishment-suspension-'+formSuspend.traderTypeName+'-printSuspension').setVisible(false);
                                                            formSuspend.down('#punishment-suspension-'+formSuspend.traderTypeName+'-printSuspensionTime').setVisible(false); 
                                                        }
                                                    }
                                                }
                                               
                                            }
                                            
                                        }
                                    ]
                                },
                                {
                                    xtype: 'punishment-evidence-grid',
                                    id: 'punishment-'+this.roleAction+'-'+this.traderTypeName+'-evidence-grid',
                                    title: 'เอกสารคำสั่ง',
                                    layout: 'fit',
                                    autoScroll: true,
                                    frame: false, 
                                    border: false,
                                    roleAction: this.roleAction,
                                    traderType: this.traderType,
                                    traderTypeName: this.traderTypeName,
                                    storeId: 'punishment-'+this.roleAction+'-'+this.traderTypeName+'-store',
                                    storeUrl: 'punishment/'+this.roleAction+'/evidence'
                                }
                            ]
                        }
                        
                    ]
                }
            ],

            buttons: [{
                text: 'พิมพ์เอกสารคำสั่ง',
                action: 'printSuspension',
                id: 'punishment-'+this.roleAction+'-'+this.traderTypeName+'-printSuspension'
            },{
                text: 'พิมพ์เอกสารคำสั่งกำหนดระยะเวลา',
                action: 'printSuspensionTime',
                id: 'punishment-'+this.roleAction+'-'+this.traderTypeName+'-printSuspensionTime'
            },'->',{
                text: 'บันทึกข้อมูล',
                action: 'addSuspension',
                id: 'punishment-'+this.roleAction+'-'+this.traderTypeName+'-addSuspension'
            },{
                text: 'อนุมัติ',
                action: 'approveSuspension',
                id: 'punishment-'+this.roleAction+'-'+this.traderTypeName+'-approveSuspension'
            }, {
                text: 'ยกเลิก',
                // width: 150,
                scope: this,
                handler: this.onResetClick
            }]    
        });
        this.callParent(arguments);
    },
    businessDetail: function()
    {
        return {
                    xtype: 'container',
                    layout: 'anchor',
                    // defaults: {
                    //     anchor: '100%',
                    //     labelWidth: 250
                    // },
                    margin: '0 0 5 0',
                    items: [
                        {
                            xtype: 'container',
                            layout: 'hbox',
                            
                            items: [
                                {
                                    xtype: 'container',
                                    layout: 'anchor',
                                    flex: 1,
                                    defaults: {
                                        anchor: '100%',
                                        labelWidth: 250
                                    },
                                     // margin: '0 0 5 0',
                                    items: [
                                        {
                                            xtype: 'displayfield',
                                            fieldLabel: 'เลขที่ใบอนุญาต',
                                            name: 'licenseNo'
                                           
                                        },
                                        {
                                            xtype: 'displayfield',
                                            fieldLabel: 'ชื่อประกอบธุรกิจนำเที่ยว',
                                            name: 'traderName'
                                           
                                        },
                                        {
                                            xtype: 'displayfield',
                                            fieldLabel: 'ผู้ขอจดทะเบียน',
                                            name: 'traderOwnerName'
                                           
                                        }
                                        
                                    ]
                                },{
                                    xtype: 'splitter'
                                },
                                {
                                    xtype: 'container',
                                    layout: 'anchor',
                                    flex: 1,
                                    defaults: {
                                        anchor: '100%',
                                        labelWidth: 250
                                    },
                                     // margin: '0 0 5 0',
                                    items: [
                                        {
                                            xtype: 'displayfield',
                                            fieldLabel: 'ประเภทธุรกิจนำเที่ยว',
                                            name: 'traderCategoryName'
                                           
                                        },
                                        {
                                            xtype: 'displayfield',
                                            fieldLabel: 'ชื่อประกอบธุรกิจนำเที่ยว(ภาษาต่างประเทศ)',
                                            name: 'traderNameEn'
                                           
                                        },
                                        {
                                            xtype: 'displayfield',
                                            fieldLabel: 'เลขที่นิติบุคคล',
                                            name: 'identityNo'
                                           
                                        }
                                        
                                    ]
                                }
                            ]
                        
                        }
                        
                    ]
                };
    }
    ,personDetail: function()
    {
        return {
                    xtype: 'container',
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%',
                        labelWidth: 250
                    },
                    margin: '0 0 5 0',
                    items: [
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'เลขที่ใบอนุญาต',
                            name: 'licenseNo'
                           
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'ผู้ขอจดทะเบียน',
                            name: 'traderOwnerName'
                           
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'เลขบัตรประชาชน',
                            name: 'identityNo'
                           
                        },
                        
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'ประเภทการจดทะเบียน',
                            name: 'traderCategoryName'
                           
                        }
                    ]
                };
    },onResetClick: function(){
        this.up('window').close();
    }
    
});