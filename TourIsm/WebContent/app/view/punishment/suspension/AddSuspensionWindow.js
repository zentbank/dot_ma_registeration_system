Ext.define('tourism.view.punishment.suspension.AddSuspensionWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.punishment-suspension-add-window',

    requires: [
        // 'tourism.view.punishment.suspension.SelectorBusinessPanel',
        'tourism.view.info.SearchLicensePanel',
        'tourism.view.punishment.suspension.AddSuspensionForm',
        'tourism.view.punishment.revoke.AddRevokeForm',
        'tourism.view.punishment.evidence.EvidenceGrid',
        'tourism.view.punishment.evidence.EvidenceWindow'
    ],
    
    initComponent: function() {
            var formModel = '';
            var title_a = 'พักใช้ใบอนุญาต';
            if(this.roleAction == 'suspension')
            {
                
                formModel = {
                    // title: 'บันทึกข้อมูลการพักใช้',
                    xtype: 'punishment-suspension-addeditform',
                    id: 'punishment-'+this.roleAction+'-'+this.traderTypeName+'-formaddedit',
                    border: false,
                    frame: false,
                    roleAction: this.roleAction,
                    traderType: this.traderType,
                    traderTypeName: this.traderTypeName
                };
            }
            else
            {
                
                formModel = {
                    // title: 'บันทึกข้อมูลการพักใช้',
                    xtype: 'punishment-revoke-addeditform',
                    id: 'punishment-'+this.roleAction+'-'+this.traderTypeName+'-formaddedit',
                    border: false,
                    frame: false,
                    roleAction: this.roleAction,
                    traderType: this.traderType,
                    traderTypeName: this.traderTypeName
                };

                title_a = 'เพิกถอนใบอนุญาต';
            }

        Ext.apply(this, {
            title: title_a,
            // layout: 'fit',
            layout: 'card',
            activeItem: 0,
            autoShow: true,
            width: 800,
            height: 600,
            autoShow: true,
            modal: true,
            items :[
             
                {
                    // title: 'เลือกใบอนุญาต',
                    xtype: 'info-license-searchlicensepanel',
                    id: 'punishment-'+this.roleAction+'-'+this.traderTypeName+'-searchlicensepanel',
                    border: false,
                    frame: false
                },formModel
              
            ]
        });

        this.callParent(arguments);
    },

});
