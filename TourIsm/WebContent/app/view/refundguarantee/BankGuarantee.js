Ext.define('tourism.view.refundguarantee.BankGuarantee', {
    extend: 'Ext.form.FieldSet',
    requires:[
        'Ext.form.FieldContainer',
        'Ext.form.field.VTypes',
        'Ext.form.*'
    ],
    
    xtype: 'refundguarantee-bankGuarantee-fieldser',

     initComponent: function(){

        
         Ext.apply(this, {
            // checkboxToggle:true,
            // checkboxName : 'guaranteeTypeBankGuarantee',
            title: 'หนังสือค้ำประกันธนาคาร',
            defaultType: 'displayfield',
            // collapsed: true,
            layout: 'anchor',
            defaults: {
                anchor: '100%'
            },
            items :
            [
                {
                    xtype: 'displayfield',
                    name: 'bankGuaranteeId',
                    fieldLabel: 'เลขที่หนังสือค้ำประกัน'
                   
                },
                {
                    xtype: 'displayfield',
                    name: 'bankGuaranteeName',
                    fieldLabel: 'ชื่อบัญชี'
                  
                }
                ,{
                	xtype: 'displayfield',
                    name: 'masBankByBankGuaranteeBankName',
                    fieldLabel: 'ธนาคาร'
                	
            	  
               
                },
                {
                    xtype: 'displayfield',
                    name: 'bankGuaranteeMny',
                    fieldLabel: 'จำนวนเงิน'
                }
            ]
         });

        this.callParent(arguments);
     }

});