Ext.define('tourism.view.refundguarantee.CashGuarantee', {
    extend: 'Ext.form.FieldSet',
    requires:[
        'Ext.form.FieldContainer',
        'Ext.form.field.VTypes',
        'Ext.form.*',
        'tourism.store.combo.MasBankStore',
        'tourism.model.combo.MasBankModel'
        
    ],
    
    xtype: 'refundguarantee-cashguarantee-fieldser',

     initComponent: function(){

        
         Ext.apply(this, {
           
            title: 'เงินสด, Cashier Check',
            defaultType: 'displayfield',
            layout: 'anchor',
            defaults: {
                anchor: '100%'
            },
            items :
            [
                {
                    xtype: 'displayfield',
                    name: 'cashAccountId',
                    fieldLabel: 'เลขที่บัญชี'
                },
                {
                    xtype: 'displayfield',
                    name: 'cashAccountName',
                    // id: 'cash-accountName',
                    fieldLabel: 'ชื่อ่บัญชี'
                }
                ,{
            	    
                    xtype: 'displayfield',
                    name: 'masBankByCashAccountBankName',
                    fieldLabel: 'ธนาคาร'
                }
                ,{
                    xtype: 'displayfield',
                    name: 'cashAccountBankBranchName',
                    fieldLabel: 'สาขา'
                   
                },
                {
                    xtype: 'displayfield',
                    name: 'cashAccountMny',
                    fieldLabel: 'จำนวนเงิน'
                }
            ]
         });

        this.callParent(arguments);
     }

});