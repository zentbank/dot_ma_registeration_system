Ext.define('tourism.view.refundguarantee.GovermentBound', {
    extend: 'Ext.form.FieldSet',
    requires:[
        'Ext.form.FieldContainer',
        'Ext.form.field.VTypes',
        'Ext.form.*'
    ],
    
    xtype: 'refundguarantee-governmentbound-fieldser',

     initComponent: function(){

         Ext.apply(this, {

            title: 'พันธบัตรรัฐบาล',
            defaultType: 'displayfield',
            layout: 'anchor',
            defaults: {
                anchor: '100%'
            },
            items :
            [

                {
                    xtype: 'displayfield',
                    name: 'governmentBondId',
                    fieldLabel: 'เลขที่พันธบัตร'
                },
                {
                    xtype: 'displayfield',
                    name: 'governmentBondExpireDate',
                    // id: 'cash-accountName',
                    fieldLabel: 'วันหมดอายุ'
                },
                {
                    xtype: 'displayfield',
                    name: 'governmentBondMny',
                    fieldLabel: 'จำนวนเงิน'
                }
                
            ]
         });

        this.callParent(arguments);
     }

});