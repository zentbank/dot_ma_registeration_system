Ext.define('tourism.view.refundguarantee.RefundGuaranteeAddEditForm', {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.data.*',
        'Ext.form.*',
        'Ext.tab.Tab',
        'tourism.view.refundguarantee.BankGuarantee',
        'tourism.view.refundguarantee.CashGuarantee',
        'tourism.view.refundguarantee.GovermentBound'
    ],
    alias: 'widget.refundguarantee-addeditform',
    // title: 'ธุรกิจนำเที่ยว',
    bodyPadding: 5,
    // autoScroll: true,

    initComponent: function(){

        var licenseDetail = this.businessDetail();

       

        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
        
        Ext.apply(this, {
            width: '100%',
            height: '100%',
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },

            items: 
            [ 
                {
                    xtype: 'container',
                    // title: 'Payment',
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%',
                        labelWidth: 200
                    },
                    items: 
                    [ 
                        {
                            xtype: 'hiddenfield',
                            name: 'guaranteeId'
                        },
                        // รายละเอียด license
                        licenseDetail,
                        {
                            xtype: 'fieldset',
                            layout: 'anchor',
                            title: 'บันทึกคืนหลักประกัน',
                            defaults: {
                                anchor: '100%',
                                labelWidth: 200
                            },
                            // margin: '0 0 5 0',
                            items: [
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'ชื่อผู้รับหลักประกันคืน',
                                    name: 'refundName',
                                    allowBlank: false,
                                    afterLabelTextTpl: required
                                },
                                {
                                    xtype: 'radiogroup',
                                    fieldLabel: 'สถานะผู้รับคืน',
                                    allowBlank: false,
                                    afterLabelTextTpl: required,
                                    columns: 1,
                                    vertical: true,
                                    items: [
                                        { boxLabel: 'กรรการบริษัท', name: 'refundType', inputValue: 'C' , checked: true },
                                        { boxLabel: 'ผู้รับมอบอำนาจ', name: 'refundType', inputValue: 'T'}
                                       
                                    ]
                                },
                                {
                                    xtype: 'numberfield',
                                    fieldLabel: 'จำนวนเงินคืน',
                                    name: 'refundMny',
                                    //maxLength: 13,
                                    hideTrigger: true,
                                    keyNavEnabled: false,
                                    mouseWheelEnabled: false,
                                    allowBlank: false,
                                    afterLabelTextTpl: required
                                },
                                {
                                    xtype: 'numberfield',
                                    fieldLabel: 'ดอกผล',
                                    name: 'refundInterest',
                                    //maxLength: 13,
                                    hideTrigger: true,
                                    keyNavEnabled: false,
                                    mouseWheelEnabled: false,
                                    allowBlank: false,
                                    afterLabelTextTpl: required
                                }
                                ,{
                                    xtype: 'datefield',
                                    fieldLabel: 'วันที่รับหลักประกันคืน',
                                    name: 'refundDate',
                                    format: 'd/m/B',
                                    allowBlank: false,
                                    afterLabelTextTpl: required
                                   
                                },
                                 {
                                    xtype: 'radiogroup',
                                    fieldLabel: 'สถานะหลักประกัน',
                                    allowBlank: false,
                                    afterLabelTextTpl: required,
                                    columns: 1,
                                    vertical: true,
                                    items: [
                                        { boxLabel: 'ปกติ', name: 'guaranteeStatus', inputValue: 'N' , checked: true },
                                        { boxLabel: 'คืนหลักประกัน', name: 'guaranteeStatus', inputValue: 'R'}
                                       
                                    ]
                                },
                                {
                                    xtype: 'textareafield',
                                    name: 'refundRemark',
                                    fieldLabel: 'หมายเหตุ'
                                }
                            ]
                        }
                        
                                              
                    ]
                }
            ],

            buttons: [{
                text: 'บันทึกข้อมูล',
                action: 'saveRefundGuarantee',
                id: 'refundguarantee-formaddedit-saveRefundGuarantee'
            }, {
                text: 'ยกเลิก',
                // width: 150,
                scope: this,
                handler: this.onResetClick
            }]    
        });
        this.callParent(arguments);
    },
    businessDetail: function()
    {
        return {
                    xtype: 'fieldset',
                    layout: 'anchor',
                    title: 'รายละเอียด',
                    defaults: {
                        anchor: '100%',
                        labelWidth: 200
                    },
                    margin: '0 0 5 0',
                    items: [
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'เลขที่ใบอนุญาต',
                            name: 'licenseNo'
                           
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'ชื่อประกอบธุรกิจนำเที่ยว',
                            name: 'traderName'
                           
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'ชื่อประกอบธุรกิจนำเที่ยว</br>(ภาษาต่างประเทศ)',
                            name: 'traderNameEn'
                           
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'ผู้ขอจดทะเบียน',
                            name: 'traderOwnerName'
                           
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'เลขที่นิติบุคคล',
                            name: 'identityNo'
                           
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'ประเภทธุรกิจนำเที่ยว',
                            name: 'traderCategoryName'
                           
                        }
                        ,{
                            xtype: 'refundguarantee-cashguarantee-fieldser'
                           
                        }
                        ,{
                            xtype: 'refundguarantee-bankGuarantee-fieldser'
                           
                        }
                        ,{
                            xtype: 'refundguarantee-governmentbound-fieldser'
                           
                        }
                         ,
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'จำนวนเงินหลักประกันรวม',
                            name: 'guaranteeMny'
                           
                        }
                         ,
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'ดอกเบี้ย',
                            name: 'guaranteeInterest'
                           
                        }
                    ]
                };
    }
    
    ,onResetClick: function(){
        this.up('window').close();
    }
    
});