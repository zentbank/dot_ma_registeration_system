Ext.define('tourism.view.refundguarantee.RefundGuaranteeFormSearch' ,{
    // extend: 'Ext.form.FieldSet',
    extend: 'Ext.form.FormPanel',
    alias : 'widget.refundguarantee-formsearch',
    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.Time',
        'Ext.form.CheckboxGroup',
        'Ext.layout.container.HBox',
        'Ext.form.FieldSet'
    ],
    bodyPadding: 10,
    initComponent: function(){

        

        Ext.apply(this, {
            items: [
                {
                        xtype: 'container',
                        // title: 'Payment',
                        layout: 'anchor',
                        defaults: {
                            anchor: '100%',
                            labelWidth: 250
                        },
                        items: [
                            {
                                xtype: 'container',
                                layout: 'hbox',
                                layoutCongig: {
                                     pack:'center',
                                     align:'middle'
                                },
                                items: [
                                    {
                                        xtype: 'container',
                                        // title: 'Payment',
                                        flex: 1,
                                        layout: 'anchor',
                                        defaults: {
                                            anchor: '100%',
                                            labelWidth: 250
                                        },
                                        items: [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'เลขที่ใบอนุญาต',
                                                name: 'licenseNo'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'เลขที่นิติบุคคล',
                                                name: 'identityNo'
                                            },
                                            {
                                                xtype: 'combo',
                                                fieldLabel: 'สถานะการคืนหลักประกัน',
                                                store: 'tourism.store.combo.RefundGuaranteeStatusStore',
                                                queryMode: 'local',
                                                displayField: 'guaranteeStatusName',
                                                valueField: 'guaranteeStatus',
                                                hiddenName: 'guaranteeStatus',
                                                name :'guaranteeStatus'
                                            }
                                        ]
                                    },{
                                        xtype: 'splitter'
                                    },
                                    {
                                        xtype: 'container',
                                        flex: 1,
                                        layout: 'anchor',
                                        defaults: {
                                            anchor: '100%',
                                            labelWidth: 250
                                        },
                                        items: [
                                             {
                                                xtype: 'textfield',
                                                fieldLabel: 'ชื่อธุรกิจนำเที่ยว',
                                                name: 'traderName',
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'ชื่อผู้ขออนุญาต',
                                                name: 'firstName'
                                            }
                                        ]
                                    }
                                ]
                            }
                            
                            
                        ]
                },
                {
                    xtype: 'toolbar',
                    border: false,
                    padding: '6px 0 6px 0px',
                    items: [{
                        xtype: 'button',
                        text : 'ค้นหา',
                        action: 'searchRefundGuarantee'
                    }]
                }

            ]            
        });

        this.callParent(arguments);
    }
    // width: '100%',

});    
