Ext.define('tourism.view.refundguarantee.RefundGuaranteeGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.refundguarantee-grid',
		stripeRows: true,
		requires: [
			'Ext.grid.Column',
			'tourism.store.refundguarantee.RefundGuaranteeStore',
			'Ext.toolbar.Paging',
			'Ext.grid.plugin.RowEditing',
			'Ext.grid.column.Template',
			'Ext.grid.column.Action',
			'Ext.grid.plugin.RowExpander',
			'Ext.grid.feature.Grouping',
			'Ext.grid.column.RowNumberer',
			'tourism.view.refundguarantee.RefundGuaranteeWindow'
		],
		initComponent: function(){

			var refundGuaranteeStore = Ext.create('tourism.store.refundguarantee.RefundGuaranteeStore',{
					storeId: this.storeId,
				    proxy: {
					    type: 'ajax',
					    actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
					    api: {
					        read: 'business/refundguarantee/read'
					    },
					    reader: {
					        type: 'json',
					        // metaProperty: '',
					        root: 'list',
					        // idProperty: 'emailId',
					        totalProperty: 'totalCount',
					        successProperty: 'success',
					        messageProperty: 'message'
					    },
					    writer: {
					        type: 'json',
					        // encode: true,
					        writeAllFields: true, ////just send changed fields
					        // root: 'data',
					        allowSingle: false, //always wrap in an array
					        batch: false
					    },
					    listeners: {
					        exception: function(proxy, response, operation){

					            Ext.MessageBox.show({
					                title: 'REMOTE EXCEPTION',
					                msg: operation.getError(),
					                icon: Ext.MessageBox.ERROR,
					                buttons: Ext.Msg.OK
					            });
					        }
					    }

					}
					,listeners: {
						beforeload: function( store, operation, eOpts )
					        {
					        	
					        	var formId = '#refundguarantee-business-formsearch';

					        	var formSearch = Ext.ComponentQuery.query(formId)[0];
					        	var values = formSearch.getValues();
   								
					        	 for (field in values) 
							      {
							        if (Ext.isEmpty(values[field])) 
							        {
							          delete values[field];
							        }
							        
							      }
							    store.proxy.extraParams = values;
					        }
					}
			});

		
			Ext.apply(this, {
				store: refundGuaranteeStore,
				dockedItems:[
					{
						xtype: 'pagingtoolbar',
						store: refundGuaranteeStore,
						dock: 'bottom',
						displayInfo: true

					}
				],
				columns: [

					{
						header: 'guaranteeId',
						dataIndex: 'guaranteeId',
						hidden: true
					},
					{
						header: 'ประเภท',
						dataIndex: 'licenseStatusName',
						flex: 1
					},
					{
						header: 'เลขที่ใบอนุญาต',
						dataIndex: 'licenseNo',
						flex: 1
					},					
					{
						header: 'ชื่อธุรกิจนำเที่ยว',
						dataIndex: 'traderName',
						flex: 2
					},
					{
						header: 'ประเภทธุรกิจนำเที่ยว',
						dataIndex: 'traderCategoryName',
						flex: 1
					},
					{
						header: 'สถานะการคืนหลักประกัน',
						dataIndex: 'guaranteeStatusName',
						flex: 1
					},
					
					 {
			        	header: 'คืนหลักประกัน',
			            xtype: 'actioncolumn',
			           	flex: 1,
			           	align : 'center',
			            items: [{
			                iconCls: 'icon-edit-xsmall',
			                tooltip: 'คืนหลักประกัน',
				            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
			                   this.fireEvent('itemclick', this, 'refundguarantee', grid, rowIndex, colIndex, record, node);
				            		
							}
			            }]
			        }
				]
			});

			this.callParent(arguments);

		}
	});