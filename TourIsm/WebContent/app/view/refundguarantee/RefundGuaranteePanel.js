Ext.define('tourism.view.refundguarantee.RefundGuaranteePanel', {
    extend: 'Ext.container.Container',
    alias: 'widget.refundguarantee-panel',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
        'tourism.view.refundguarantee.RefundGuaranteeFormSearch',
        'tourism.view.refundguarantee.RefundGuaranteeGrid'
       
    ],
    initComponent: function(){

    	
    	Ext.apply(this, {
    		layout: {
		        type: 'border'
		    },
		    items: [{
		        region: 'north',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items:[{
                    title:'ค้นหาข้อมูลคืนหลักประกัน',
                    collapsible: true,   // make collapsible
                    collapsed : false,
                    xtype: 'refundguarantee-formsearch',
                    id: 'refundguarantee-business-formsearch',
                    // padding: '5px 5px 0px 5px',
                    frame: false,
                    border: false
                }]
		    }
		    ,{
		        region: 'center',
		        xtype: 'panel',
		        layout: 'fit',
		        frame: false,
		        border: false,
		        items:[{
		            // title: 'การทำรายการใบอนุญาตธุรกิจนำเที่ยว',
		            xtype: 'refundguarantee-grid',
		            id: 'refundguarantee-business-grid',
		            // padding: '5px 5px 5px 5px',
		            autoScroll: true,
		            // frame: false 
		            border: false,
		            storeId: 'refundguarantee-business-store',
		            storepageSize: 20
		        }]
		    }
		    ]
    	});

    	this.callParent(arguments);
    }
});