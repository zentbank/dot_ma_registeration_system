Ext.define('tourism.view.refundguarantee.RefundGuaranteeWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.refundguarantee-window',

    requires: [
        'tourism.view.refundguarantee.RefundGuaranteeAddEditForm'
    ],
    
    initComponent: function() {


        Ext.apply(this, {
            title:'บันทึกคืนหลักประกัน',
            // layout: 'fit',
            // layout: 'fit',
            // activeItem: 0,
            // autoShow: true,
            width: 500,
            height: 600,
            autoShow: true,
            autoScroll: true,
            modal: true,
            items :[
                {
                   xtype: 'refundguarantee-addeditform',
                   border: false,
                   frame: false
                   // ,
                   // autoScroll: true
                }
            ]
        });
        this.callParent(arguments);
    },

});
