Ext.define('tourism.view.refundguarantee.account.AccountCanclePanel', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
        'tourism.view.refundguarantee.account.AccountCancleFormSearch',
        'tourism.view.refundguarantee.account.AccountCancleGrid'//,
//        'tourism.view.account.AccountGuideFormSearch'
    ],
    
    xtype: 'account-cancle-panel',
    initComponent: function(){
    	
    	var formSearch = this.getBusinessFormSearch(this.roleAction, this.traderType);
    	
    	Ext.apply(this, {
    		layout: {
		        type: 'border'
		    },
		    items: [{
		        region: 'north',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items:[formSearch]
		    },{
		        region: 'center',
		        xtype: 'panel',
		        layout: 'fit',
		        frame: false,
		        border: false,
		        items:[{
		            // title: 'การทำรายการใบอนุญาตธุรกิจนำเที่ยว',
		            xtype: 'account-cancle-grid',
		            id: this.roleAction+'-'+this.traderType+'-account-grid',
		            // padding: '5px 5px 5px 5px',
		            autoScroll: true,
		            // frame: false 
		            border: false,
		            storeId: this.roleAction+'-'+this.traderType+'-account-store',
		            storepageSize: 20,
		            storeUrl: 'business/info/readAccountWillCancle',
		            roleAction: this.roleAction,
		            traderType: this.traderType
		            
		        }]
		    }
		    ]
    	});

    	this.callParent(arguments);
    },
    getBusinessFormSearch: function(roleAction, traderType)
    {
    	return {
    		
	            title:'ค้นหาการทำรายการใบอนุญาตธุรกิจนำเที่ยว',
	            collapsible: true,   // make collapsible
	            xtype: 'account-cancle-formsearch',
	            id: roleAction+'-'+traderType+'-account-formsearch',
	            frame: false,
	            border: false
    	};
    }
    
});