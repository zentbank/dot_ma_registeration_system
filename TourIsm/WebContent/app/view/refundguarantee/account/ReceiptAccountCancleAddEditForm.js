Ext.define('tourism.view.refundguarantee.account.ReceiptAccountCancleAddEditForm',{
    extend: 'Ext.form.Panel'
    ,alias: 'widget.receipt-account-cancle-addedit-form'
    ,requires: [
        'Ext.data.*'
        ,'Ext.form.*'
        ,'tourism.view.account.receipt.ReceiptForm'
        ,'tourism.view.account.guarantee.GuaramteeAccountForm'
        
    ]
    ,bodyPadding: 5
    ,autoScroll: true
    ,initComponent: function(){
        Ext.apply(this, {
            width: '100%',
            height: '100%',
            items: [
            {
                xtype: 'tabpanel',
                plain: true,
                frame: false,
                border: false,
                items: [
                    {
                        title:'บันทึกข้อมูลใบเสร็จรับเงิน',
                        xtype: 'receipt-addedit-form',
                        frame: false,
                        border: false
                    },
                    {
                        title: 'บันทึกข้อมูลหลักประกัน',
                        xtype: 'guarantee-account-form',
                        frame: false,
                        border: false
                    }
                    
                    ]
            }]
            ,buttons: [{
                text: 'บันทึกข้อมูลและพิมพ์ใบเสร็จ',
                action: 'saveReceiptCancleBtn'
            }]  
        });

        this.callParent(arguments);
    }
        
});