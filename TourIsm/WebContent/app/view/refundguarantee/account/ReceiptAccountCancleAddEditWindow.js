Ext.define('tourism.view.refundguarantee.account.ReceiptAccountCancleAddEditWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.receipt-account-cancle-addeditwindow',

    requires: [
        'tourism.view.refundguarantee.account.ReceiptAccountCancleAddEditForm'
    ],

    title : 'บันทึกข้อมูลหลักประกัน',
    layout: 'fit',
    width: 500,
    height: 500,
    //True to make the window modal and mask everything behind it when displayed
    modal: true,
    // iconCls: 'icon-form',

    initComponent: function() {

        // Ext.apply(this, {

        // });
        this.items = [
                {
                    xtype: 'receipt-account-cancle-addedit-form',
                    // id: 'receipt-account-addedit-form',
                    frame: false,
                    border: false
                }
        ];

        this.callParent(arguments);
    }

});
