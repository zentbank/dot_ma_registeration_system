Ext.define('tourism.view.registrationprogress.RegistrationProgressGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.registrationprogress-grid',
		stripeRows: true,
		requires: [
			'Ext.grid.Column',
			'Ext.toolbar.Paging',
			'Ext.grid.plugin.RowEditing',
			'Ext.grid.column.Template',
			'Ext.grid.column.Action',
			'Ext.grid.plugin.RowExpander',
			'tourism.store.registration.RegistrationProgressStore'
		],
		initComponent: function(){
			var registrationProgressStore = Ext.create('tourism.store.registration.RegistrationProgressStore',{
				
			});

			Ext.apply(this, {
				store: registrationProgressStore,
				
			});

			this.callParent(arguments);


		},
		columns: [
			{
				header: ' ',
				dataIndex: 'progress',
				flex: 1
			}
			,{
	        	header: '',
	            xtype: 'actioncolumn',
	           	width:50,
	           	align : 'center',
	            items: [{
	                // iconCls: 'icon-send-xsmall',
	                // tooltip: 'การผ่านเรื่อง',
	                getClass: function(v, meta, rec) {          
	                    if (rec.get('progressStatus') == 'A') {
	                        
	                        return 'icon-checked-xsmall';
	                    }if (rec.get('progressStatus') == 'R') {
	                        
	                        return 'icon-page_warning-xsmall';
	                    } if (rec.get('progressStatus') == 'W') {
	                        
	                        return 'icon-wait-xsmall';
	                    } 
	                }
	            }]
	        }
		],
		 plugins: [{
            ptype: 'rowexpander',
            rowBodyTpl : new Ext.XTemplate(
            	'<div>',
            		'<p><b>{progressStatusName}</b></p>',
            		'<p><b>{progressDesc}</b></p>',
            		'<p><b>วันที่&nbsp;{progressDate}</b></p>',
	            	'<p><b>โดย&nbsp;{authorityName}</b></p>',
                '<div>'
            )
        }]
	});