Ext.define('tourism.view.registrationprogress.RegistrationProgressWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.registrationprogress-window',

    requires: [
        'tourism.view.registrationprogress.RegistrationProgressGrid'
    ],

    title : 'สถานะการจดทะเบียนใบอนุญาต',
    layout: 'fit',
    // autoShow: true,
    width: 400,
    height: 500,
    //True to make the window modal and mask everything behind it when displayed
    modal: true,
    // iconCls: 'icon-form',

    initComponent: function() {

        this.items = [
                {
                    xtype: 'registrationprogress-grid',
                    frame: false,
                    border: false
                }
        ];

        this.callParent(arguments);
    }
});
