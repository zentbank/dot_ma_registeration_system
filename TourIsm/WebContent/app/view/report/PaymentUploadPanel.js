Ext.define('tourism.view.report.PaymentUploadPanel',{
	extend: 'Ext.window.Window',
	alias: 'widget.payment_upload_window',
	requires: [
//	           'tourism.view.information.tourleader.InfoTourleaderForm'
	],
	title: 'File Upload Form',
	layout: 'fit',
	width: 500,
//	height: 600,
	//True to make the window modal and mask everything behind it when displayed
    modal: true,
    
    initComponent: function(){
    	
    	this.items =[
    	    {
    	    		xtype: 'form',
    	            bodyPadding: '10 10 0',

    	            defaults: {
    	                anchor: '100%',
    	                allowBlank: false,
    	                msgTarget: 'side',
    	                labelWidth: 150
    	            },
    	            
    	    		frame: false,
                    border: false,
                    // id: this.roleAction+'-'+this.actionMode+'-'+this.traderType+'-panel',
                    
    	            items: [
					
    	            {
    	                xtype: 'filefield',
    	                id: 'form-file',
    	                emptyText: 'Select an file',
    	                fieldLabel: 'ไฟล์สำหรับนำเข้า',
    	                name: 'file',
    	                buttonText: '',
    	                buttonConfig: {
    	                    iconCls: 'upload-icon'
    	                }
    	            }
    	    		]

    	            ,buttons: [{
    	                text: 'Save',
    	                handler: function(btn){
    	                    var form = this.up('window').down('form').getForm();
    	                    if(form.isValid()){
    	                        form.submit({
    	                            url: 'business/ktb/payment/upload',
    	                            method: 'POST',
    	                            waitMsg: 'Uploading your file...',
    	                            success: function(form, action) {
    	                            	
    	                            var dataObj = action.result.data;
    	                            
    	                            var grid = Ext.ComponentQuery.query('#ktbbank-KTBPayment-grid')[0];
    	                            var store = grid.getStore();

    	                             store.load({
    	                                 params: dataObj,
    	                                 callback: function(records, operation, success) {
    	                                     // //console.log(success);
    	                                 },
    	                                 scope: this
    	                             }); 
    	                            
    	               	             var noti = Ext.create('widget.uxNotification', {
    	               	               // title: 'Notification',
    	               	               position: 'tr',
    	               	               manager: 'instructions',
    	               	               // cls: 'ux-notification-light',
    	               	               // iconCls: 'ux-notification-icon-information',
    	               	               html: '<b>บันทึกข้อมูลเรียบร้อยแล้ว</b>',
    	               	               closable: false,
    	               	               autoCloseDelay: 4000,
    	               	               width: 300,
    	               	               slideBackDuration: 500,
    	               	               slideInAnimation: 'bounceOut',
    	               	               slideBackAnimation: 'easeIn'
    	               	             });
    	               	             
       	               	            noti.on('show',function(){
       	               	            	
       	               	            	
		  	                            this.up('window').close();
  	               	                },btn);
    	               	             
    	               	             noti.show();
    	               	            
    	                            	
    	                            }
    	                        	,failure: function(form, action)
    	                        	{
    	                        		switch (action.failureType) {
    	                                case Ext.form.action.Action.CLIENT_INVALID:
    	                                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
    	                                    break;
    	                                case Ext.form.action.Action.CONNECT_FAILURE:
    	                                    Ext.Msg.alert('Failure', 'Ajax communication failed');
    	                                    break;
    	                                case Ext.form.action.Action.SERVER_INVALID:
    	                                   Ext.Msg.alert('Failure', "ไม่สามารถบันทึกไฟล์ข้อมูลเอกสารได้");//action.result.msg);
    	                        		}
    	                        	}
    	                        });
    	                    }
    	                }
    	            } 

    	    	]

    	    }
    	];
    	
    	this.callParent(arguments);
    }

	
});















