Ext.define('tourism.view.report.ReportBusinessRegistrationTypePanel',{
	extend: 'Ext.container.Container',
	xtype: 'reportbusinessregistrationtype-panel',
	requires:[
	          'Ext.tab.Panel',
	          'Ext.layout.container.Border',
	          'tourism.view.report.ReportBusinessRegistrationTypeFormSearch'
	],
	initComponent: function()
	{
		var formSearch = this.getBusinessRegistrationTypeSearch(this.roleAction);
				
		Ext.apply(this,{
			layout: {
		        type: 'border'
		    },
		    items: [{
		        region: 'center',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items:[formSearch]
		    }]
		});
		this.callParent(arguments);
	},
	getBusinessRegistrationTypeSearch: function(roleAction)
    {
    	return {
    		
	            title:'ตารางแสดงสถิติจำนวนการออกใบอนุญาตประกอบธุรกิจนำเที่ยว กรณีมีการเปลี่ยนแปลงและเพิ่มสาขา',
	            collapsible: true,   // make collapsible
	            // collapsed : true,
	            xtype: 'reportbusinessregistrationtype-formsearch',
	            id: roleAction+'-formsearch',
	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false
    	};
    }
});