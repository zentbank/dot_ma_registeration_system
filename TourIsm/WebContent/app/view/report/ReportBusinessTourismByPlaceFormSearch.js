Ext.define('tourism.view.report.ReportBusinessTourismByPlaceFormSearch',{
	extend: 'Ext.form.FormPanel',
	requires:[
				'Ext.form.field.Date',
				'Ext.form.field.Time',
				'Ext.form.CheckboxGroup',
				'Ext.layout.container.HBox',
				'Ext.form.FieldSet',
				'Ext.form.field.Number'
	          ],
	
	alias: 'widget.reportBusinessTourismByPlace-formsearch',
	
	bodyPadding: 10,
	items: [
            {
        xtype: 'container',
        // title: 'Payment',
        layout: 'anchor',
        defaults: {
            anchor: '100%',
            labelWidth: 150
        },
        items: [
                   {
                    xtype: 'container',
                    layout: 'hbox',
                    layoutCongig: {
                         pack:'center',
                         align:'middle'
                    },
                    items: [
                        {
                            xtype: 'container',
                            // title: 'Payment',
                            flex: 1,
                            layout: 'anchor',
                            defaults: {
                                anchor: '100%',
                                labelWidth: 100
                            },
	                        items: [
								{
	                              // จังหวัด (TH)
	                              xtype: 'masprovincecombo',
	                              fieldLabel: 'จังหวัด',
	                              id : 'masProvince-combo',
	                              store: 'tourism.store.combo.MasProvinceStore',
	                              displayField: 'provinceName',
	                              valueField: 'provinceId',
	                              hiddenName: 'provinceId',
	                              name: 'provinceId',
	                              masamphurComboId:'masAmphur-combo',
	                              queryParam: 'provinceName'
	                          },{
	                               // อำเภอ/เขต
	                              xtype: 'masamphurcombo',
	                              fieldLabel: 'อำเภอ/เขต',
	                              id : 'masAmphur-combo',
	                              store: 'tourism.store.combo.MasAmphurStore',
	                              displayField: 'amphurName',
	                              valueField: 'amphurId',
	                              hiddenName: 'amphurId',
	                              name: 'amphurId',
	                              queryParam: 'amphurName',
	                              masprovinceComboId: 'masProvince-combo',
	                              mastambolComboId: 'masTambol-combo'
	                             
	                          },{
	                               // ตำบล/แขวง
	                              xtype: 'mastambolcombo',
	                              fieldLabel: 'ตำบล/แขวง',
	                              id : 'masTambol-combo',
	                              store: 'tourism.store.combo.MasTambolStore',
	                              displayField: 'tambolName',
	                              valueField: 'tambolId',
	                              hiddenName: 'tambolId',
	                              name: 'tambolId',
	                              masamphurComboId: 'masAmphur-combo',
	                              queryParam: 'tambolName'
	                             
	                          }
                            ]
                        }
                    ]
                }
        ]
    },
    {
        xtype: 'toolbar',
        align:'center',
        border: false,
        padding: '10px 0 10px 0px',
        items: [{
        	xtype: 'button',
            text : 'พิมพ์รายงาน',
            action: 'printReportBusinessTourism'
        }]
    }

]
});