Ext.define('tourism.view.report.ReportBusinessTourismByPlacePanel',{
	extend: 'Ext.container.Container',
	xtype: 'reportBusinessTourismByPlace-panel',
	requires:[
	          'Ext.tab.Panel',
	          'Ext.layout.container.Border',
	          'tourism.view.report.ReportBusinessTourismByPlaceFormSearch'
	],
	initComponent: function()
	{
		Ext.apply(this,{
			layout: {
		        type: 'border'
		    },
		    items: [{
		        region: 'center',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items:[{
		        	title:'รายงานจำนวนสถานประกอบธุรกิจนำเที่ยวจำแนกตามที่ตั้ง',
		            collapsible: true,   // make collapsible
		            // collapsed : true,
		            xtype: 'reportBusinessTourismByPlace-formsearch',
		            id: 'reportBusinessTourismByPlace-formsearch',
		            padding: '5px 5px 0px 5px',
		            frame: false,
		            border: false
		        }]
		    }]
		});
		this.callParent(arguments);
	}
});