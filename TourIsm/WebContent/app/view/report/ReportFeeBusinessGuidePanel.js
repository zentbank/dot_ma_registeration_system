Ext.define('tourism.view.report.ReportFeeBusinessGuidePanel',{
	extend: 'Ext.container.Container',
	xtype: 'reportfeebusinessguide-panel',
	requires:[
	          'Ext.tab.Panel',
	          'Ext.layout.container.Border',
	          'tourism.view.report.ReportFeeBusinessGuideFormSearch'
	],
	initComponent: function()
	{
		var formSearch = this.getGuideFormSearch(this.roleAction);
				
		Ext.apply(this,{
			layout: {
		        type: 'border'
		    },
		    items: [{
		        region: 'center',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items:[formSearch]
		    }]
		});
		this.callParent(arguments);
	},
	getGuideFormSearch: function(roleAction)
    {
    	return {
    		
	            title:'รายงานทะเบียนคุมการจัดเก็บรายได้ค่าธรรมเนียม',
	            collapsible: true,   // make collapsible
	            // collapsed : true,
	            xtype: 'reportfeebusinessguide-formsearch',
	            id: roleAction+'-formsearch',
	            padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false
    	};
    }
});