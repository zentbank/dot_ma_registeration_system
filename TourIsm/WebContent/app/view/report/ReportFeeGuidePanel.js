Ext.define('tourism.view.report.ReportFeeGuidePanel',{
	extend: 'Ext.container.Container',
	xtype: 'reportfeeguide-panel',
	requires:[
	          'Ext.tab.Panel',
	          'Ext.layout.container.Border',
	          'tourism.view.report.ReportFeeGuideFormSearch'
	],
	initComponent: function()
	{
		var formSearch = this.getGuideFormSearch(this.roleAction);
				
		Ext.apply(this,{
			layout: {
		        type: 'border'
		    },
		    items: [{
		        region: 'center',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items:[formSearch]
		    }]
		});
		this.callParent(arguments);
	},
	getGuideFormSearch: function(roleAction)
    {
    	return {
    		
	            title:'รายงานการรับค่าธรรมเนียมมัคคุเทศก์',
	            collapsible: true,   // make collapsible
	            // collapsed : true,
	            xtype: 'reportfeeguide-formsearch',
	            id: roleAction+'-formsearch',
	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false
    	};
    }
});