Ext.define('tourism.view.report.ReportFeeInExTotalFormSearch',{
	extend: 'Ext.form.FormPanel',
	requires:[
				'Ext.form.field.Date',
				'Ext.form.field.Time',
				'Ext.form.CheckboxGroup',
				'Ext.layout.container.HBox',
				'Ext.form.FieldSet',
				'Ext.form.field.Number'
	          ],
	
	alias: 'widget.reportfeeinextotal-formsearch',
	
	bodyPadding: 10,
	items: [
            {
        xtype: 'container',
        // title: 'Payment',
        layout: 'anchor',
        defaults: {
            anchor: '100%',
            labelWidth: 150
        },
        items: [
                   {
                    xtype: 'container',
                    layout: 'hbox',
                    layoutCongig: {
                         pack:'center',
                         align:'middle'
                    },
                    items: [
                        {
                            xtype: 'container',
                            // title: 'Payment',
                            flex: 1,
                            layout: 'anchor',
                            defaults: {
                                anchor: '100%',
                                labelWidth: 100
                            },
	                        items: [
								{
									xtype: 'combo',
									fieldLabel: 'ประจำเดือน',
    			    	   			store: 'tourism.store.month.MonthStore',
    			    	   		    queryMode: 'local',
    			    	   		    displayField: 'monthName',
    			    	   		    valueField: 'monthId',
    			    	   		    hiddenName: 'monthName',
    			    	   		    triggerAction: 'all',
    			    	   		    name: 'monthName',
    			    	   			flex:1,
    			    	   			allowBlank: false,
								    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>'
								},
                            ]
                        },{
                            xtype: 'splitter'
                        },
                        {
                            xtype: 'container',
                            flex: 1,
                            layout: 'anchor',
                            defaults: {
                                anchor: '100%',
                                labelWidth: 50
                            },
                            items: [
									{
									    xtype: 'numberfield',
									    fieldLabel: 'พ.ศ.',
									    name: 'year',
									    value: '2557',
									    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>'
									    //allowBlank: false
									},
                            ]
                        }
                    ]
                }
        ]
    },
    {
        xtype: 'toolbar',
        align:'center',
        border: false,
        padding: '10px 0 10px 0px',
        items: [{
        	xtype: 'button',
            text : 'พิมพ์รายงาน',
            action: 'printReportFeeInExTotal'
        }]
    }

]
});