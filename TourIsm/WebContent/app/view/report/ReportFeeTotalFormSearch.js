Ext.define('tourism.view.report.ReportFeeTotalFormSearch',{
	extend: 'Ext.form.FormPanel',
	requires:[
				'Ext.form.field.Date',
				'Ext.form.field.Time',
				'Ext.form.CheckboxGroup',
				'Ext.layout.container.HBox',
				'Ext.form.FieldSet'
	          ],
	
	alias: 'widget.reportfeetotal-formsearch',
	
	bodyPadding: 10,
	items: [
            {
        xtype: 'container',
        // title: 'Payment',
        layout: 'anchor',
        defaults: {
            anchor: '100%',
            labelWidth: 150
        },
        items: [
                   {
                    xtype: 'container',
                    layout: 'anchor',
                    layoutCongig: {
                         pack:'center',
                         align:'middle'
                    },
                    items: [
                        {
                         
								    xtype: 'datefield',
								    fieldLabel: 'วันที่',
								    // labelWidth: 150,
								    anchor: '50%',
								    name: 'receiptDateFrom',
								    format: 'd/m/B',
								    allowBlank: false,
								    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>'
							
                        },{
                            xtype: 'combo',
                            fieldLabel: 'สำนักงาน',
                            store: 'tourism.store.combo.OrganizationStore',
                            queryMode: 'remote',
                            displayField: 'orgName',
                            anchor: '50%',
                            valueField: 'orgId',
                            hiddenName: 'orgId',
                            allowBlank: false,
                            name :'orgId'
                        }
                    ]
                }
        ]
    },
    {
        xtype: 'toolbar',
        align:'center',
        border: false,
        padding: '10px 0 10px 0px',
        items: [{
        	xtype: 'button',
            text : 'พิมพ์รายงาน',
            action: 'printReportFeeTotal'
        }]
    }

]
});