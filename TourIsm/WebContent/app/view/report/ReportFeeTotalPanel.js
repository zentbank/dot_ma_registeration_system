Ext.define('tourism.view.report.ReportFeeTotalPanel',{
	extend: 'Ext.container.Container',
	xtype: 'reportfeetotal-panel',
	requires:[
	          'Ext.tab.Panel',
	          'Ext.layout.container.Border',
	          'tourism.view.report.ReportFeeTotalFormSearch'
	],
	initComponent: function()
	{
		var formSearch = this.getFormSearch(this.roleAction);
				
		Ext.apply(this,{
			layout: {
		        type: 'border'
		    },
		    items: [{
		        region: 'center',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items:[formSearch]
		    }]
		});
		this.callParent(arguments);
	},
	getFormSearch: function(roleAction)
    {
    	return {
    		
	            title:'รายละเอียดการส่งเงินค่าธรรมเนียมประจำวัน',
	            collapsible: true,   // make collapsible
	            // collapsed : true,
	            xtype: 'reportfeetotal-formsearch',
	            id: roleAction+'-formsearch',
	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false
    	};
    }
});