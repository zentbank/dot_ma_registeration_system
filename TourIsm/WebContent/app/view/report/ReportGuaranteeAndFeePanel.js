Ext.define('tourism.view.report.ReportGuaranteeAndFeePanel',{
	extend: 'Ext.container.Container',
	xtype: 'reportguaranteeandfee-panel',
	requires:[
	          'Ext.tab.Panel',
	          'Ext.layout.container.Border',
	          'tourism.view.report.ReportGuaranteeAndFeeFormSearch'
	],
	initComponent: function()
	{
		var formSearch = this.getFormSearch(this.roleAction);
				
		Ext.apply(this,{
			layout: {
		        type: 'border'
		    },
		    items: [{
		        region: 'center',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items:[formSearch]
		    }]
		});
		this.callParent(arguments);
	},
	getFormSearch: function(roleAction)
    {
    	return {
    		
	            title:'สรุปรายงานการรับหลักประกันและค่าธรรมเนียม',
	            collapsible: true,   // make collapsible
	            // collapsed : true,
	            xtype: 'reportguaranteeandfee-formsearch',
	            id: roleAction+'-formsearch',
	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false
    	};
    }
});