Ext.define('tourism.view.report.ReportGuaranteeAndFeeRegistrationTypePanel',{
	extend: 'Ext.container.Container',
	xtype: 'reportguaranteeandfeeregistrationtype-panel',
	requires:[
	          'Ext.tab.Panel',
	          'Ext.layout.container.Border',
	          'tourism.view.report.ReportGuaranteeAndFeeRegistrationTypeFormSearch'
	],
	initComponent: function()
	{
		var formSearch = this.getFormSearch(this.roleAction);
				
		Ext.apply(this,{
			layout: {
		        type: 'border'
		    },
		    items: [{
		        region: 'center',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items:[formSearch]
		    }]
		});
		this.callParent(arguments);
	},
	getFormSearch: function(roleAction)
    {
    	return {
    		
	            title:'รายงานการรับหลักประกันและค่าธรรมเนียมชนิดต่างๆ',
	            collapsible: true,   // make collapsible
	            // collapsed : true,
	            xtype: 'reportguaranteeandfeeregistrationtype-formsearch',
	            id: roleAction+'-formsearch',
	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false
    	};
    }
});