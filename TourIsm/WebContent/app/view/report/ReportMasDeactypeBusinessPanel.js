Ext.define('tourism.view.report.ReportMasDeactypeBusinessPanel',{
	extend: 'Ext.container.Container',
	xtype: 'reportmasdeactypebusiness-panel',
	requires:[
	          'Ext.tab.Panel',
	          'Ext.layout.container.Border',
	          'tourism.view.report.ReportMasDeactypeBusinessFormSearch'
	],
	initComponent: function()
	{
		var formSearch = this.getBusinessFormSearch(this.roleAction);
				
		Ext.apply(this,{
			layout: {
		        type: 'border'
		    },
		    items: [{
		        region: 'center',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items:[formSearch]
		    }]
		});
		this.callParent(arguments);
	},
	getBusinessFormSearch: function(roleAction)
    {
    	return {
    		
	            title:'รายชื่อบริษัทที่ยกเลิกธุรกิจนำเที่ยว และขอรับคืนหลักประกัน',
	            collapsible: true,   // make collapsible
	            // collapsed : true,
	            xtype: 'reportmasdeactypebusiness-formsearch',
	            id: roleAction+'-formsearch',
	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false
    	};
    }
});