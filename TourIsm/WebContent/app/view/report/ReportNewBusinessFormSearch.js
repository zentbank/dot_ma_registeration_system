Ext.define('tourism.view.report.ReportNewBusinessFormSearch' ,{
    extend: 'Ext.form.FormPanel',
    alias : 'widget.reportnewbusiness-formsearch',
    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.Time',
        'Ext.form.CheckboxGroup',
        'Ext.layout.container.HBox',
        'Ext.form.FieldSet'
    ],
    bodyPadding: 10,
    // width: '100%',
    items: [
        {
                xtype: 'container',
                // title: 'Payment',
                layout: 'anchor',
                defaults: {
                    anchor: '100%',
                    labelWidth: 150
                },
                items: [      
                    {
                        xtype: 'container',
                        layout: 'hbox',
                        layoutCongig: {
                             pack:'center',
                             align:'middle'
                        },
                        items: [
                            {
                                xtype: 'container',
                                // title: 'Payment',
                                flex: 1,
                                layout: 'anchor',
                                defaults: {
                                    anchor: '100%',
                                    labelWidth: 150
                                },
                                items: [
                                    {
                                    	xtype: 'datefield',
                                        fieldLabel: 'ตั้งแต่วันที่',
                                        // labelWidth: 150,
                                        name: 'receiptDateFrom',
                                        format: 'd/m/B',
                                        allowBlank: false,
        								afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>'
                                    },
                                    {
                                        xtype: 'combo',
                                        fieldLabel: 'สำนักงาน',
                                        store: 'tourism.store.combo.OrganizationStore',
                                        queryMode: 'remote',
                                        displayField: 'orgName',
                                        valueField: 'orgId',
                                        hiddenName: 'orgId',
                                        name :'orgId'
                                    }
//                                    ,{
//                                    	xtype: 'combo',
//                                        fieldLabel: 'ประเภทใบอนุญาต',
//                                        store: 'tourism.store.combo.TraderTypeStore',
//                                        queryMode: 'local',
//                                        displayField: 'traderTypeName',
//                                        valueField: 'traderType',
//                                        hiddenName: 'traderType',
//                                        name :'traderType'
//                                    }
                                ]
                            },{
                                xtype: 'splitter'
                            },
                            {
                                xtype: 'container',
                                flex: 1,
                                layout: 'anchor',
                                defaults: {
                                    anchor: '100%',
                                    labelWidth: 200
                                },
                                items: [
                                    {
                                    	xtype: 'datefield',
                                        fieldLabel: 'ถึงวันที่',
                                        name: 'receiptDateTo',
                                        format: 'd/m/B'
                                    }
                                     
                                ]
                            }
                        ]
                    }
                    
                ]
        },
        {
            xtype: 'toolbar',
            border: false,
            padding: '6px 0 6px 0px',
            items: [{
                xtype: 'button',
                text : 'พิมพ์รายงาน',
                action: 'printReportNewBusiness'
            }]
        }

    ]
});    





