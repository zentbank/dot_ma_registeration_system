Ext.define('tourism.view.report.ReportNewBusinessPanel',{
	extend: 'Ext.container.Container',
	xtype: 'reportnewbusiness-panel',
	requires:[
	          'Ext.tab.Panel',
	          'Ext.layout.container.Border',
	          'tourism.view.report.ReportNewBusinessFormSearch'
	],
	initComponent: function()
	{
		var formSearch = this.getBusinessFormSearch(this.roleAction);
				
		Ext.apply(this,{
			layout: {
		        type: 'border'
		    },
		    items: [{
		        region: 'center',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items:[formSearch]
		    }]
		});
		this.callParent(arguments);
	},
	getBusinessFormSearch: function(roleAction)
    {
    	return {
    		
	            title:'ตารางแสดงรายละเอียดธุรกิจนำเที่ยว จดใหม่',
	            collapsible: true,   // make collapsible
	            // collapsed : true,
	            xtype: 'reportnewbusiness-formsearch',
	            id: roleAction+'-formsearch',
	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false
    	};
    }
});