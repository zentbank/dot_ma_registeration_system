Ext.define('tourism.view.report.ReportProvinceBusinessFormSearch',{
	extend: 'Ext.form.FormPanel',
	requires:[
				'Ext.form.field.Date',
				'Ext.form.field.Time',
				'Ext.form.CheckboxGroup',
				'Ext.layout.container.HBox',
				'Ext.form.FieldSet'
	          ],
	
	alias: 'widget.reportprovincebusiness-formsearch',
	
	bodyPadding: 10,
	items: [
            {
        xtype: 'container',
        // title: 'Payment',
        layout: 'anchor',
        defaults: {
            anchor: '100%',
            labelWidth: 150
        },
        items: [
                   {
                    xtype: 'container',
                    layout: 'hbox',
                    layoutCongig: {
                         pack:'center',
                         align:'middle'
                    },
                    items: [
                        {
                            xtype: 'container',
                            // title: 'Payment',
                            flex: 1,
                            layout: 'anchor',
                            defaults: {
                                anchor: '100%',
                                labelWidth: 150
                            },
	                        items: [
								{
								    xtype: 'datefield',
								    fieldLabel: 'ตั้งแต่วันที่',
								    // labelWidth: 150,
								    name: 'receiptDateFrom',
								    format: 'd/m/B',
								    allowBlank: false,
								    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>'
								}
								//Sek Add 19/09/59
								,{
                                    xtype: 'combo',
                                    fieldLabel: 'จังหวัด',
                                    store: 'tourism.store.combo.MasProvinceStore',
                                    queryMode: 'remote',
                                    displayField: 'provinceName',
                                    valueField: 'provinceId',
                                    hiddenName: 'provinceId',
                                    name :'provinceId'
                                }
								//
                            ]
                        },{
                            xtype: 'splitter'
                        },
                        {
                            xtype: 'container',
                            flex: 1,
                            layout: 'anchor',
                            defaults: {
                                anchor: '100%',
                                labelWidth: 150
                            },
                            items: [
									{
									    xtype: 'datefield',
									    fieldLabel: 'ถึงวันที่',
									    // labelWidth: 150,
									    name: 'receiptDateTo',
									    format: 'd/m/B'//,
									    //allowBlank: false
									},
                            ]
                        }
                    ]
                }
        ]
    },
    {
        xtype: 'toolbar',
        align:'center',
        border: false,
        padding: '10px 0 10px 0px',
        items: [{
        	xtype: 'button',
            text : 'พิมพ์รายงาน',
            action: 'printReportProvinceBusiness'
        }]
    }

]
});