Ext.define('tourism.view.report.ReportProvinceBusinessPanel',{
	extend: 'Ext.container.Container',
	xtype: 'reportprovincebusiness-panel',
	requires:[
	          'Ext.tab.Panel',
	          'Ext.layout.container.Border',
	          'tourism.view.report.ReportProvinceBusinessFormSearch'
	],
	initComponent: function()
	{
		var formSearch = this.getBusinessFormSearch(this.roleAction);
				
		Ext.apply(this,{
			layout: {
		        type: 'border'
		    },
		    items: [{
		        region: 'center',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items:[formSearch]
		    }]
		});
		this.callParent(arguments);
	},
	getBusinessFormSearch: function(roleAction)
    {
    	return {
    		
	            title:'ตารางแสดงสถิติจำนวนธุรกิจนำเที่ยว จำแนกตามจังหวัด',
	            collapsible: true,   // make collapsible
	            // collapsed : true,
	            xtype: 'reportprovincebusiness-formsearch',
	            id: roleAction+'-formsearch',
	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false
    	};
    }
});