Ext.define('tourism.view.report.ReportStaticTourLeaderPanel',{
	extend: 'Ext.container.Container',
	xtype: 'reportstatictourleader-panel',
	requires:[
	          'Ext.tab.Panel',
	          'Ext.layout.container.Border',
	          'tourism.view.report.ReportStaticTourLeaderFormSearch'
	],
	initComponent: function()
	{
		var formSearch = this.getTourLeaderFormSearch(this.roleAction);
				
		Ext.apply(this,{
			layout: {
		        type: 'border'
		    },
		    items: [{
		        region: 'center',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items:[formSearch]
		    }]
		});
		this.callParent(arguments);
	},
	getTourLeaderFormSearch: function(roleAction)
    {
    	return {
    		
	            title:'ตารางแสดงสถิติจำนวนผู้ขอขึ้นทะเบียนเป็นผู้นำเที่ยว',
	            collapsible: true,   // make collapsible
	            // collapsed : true,
	            xtype: 'reportstatictourleader-formsearch',
	            id: roleAction+'-formsearch',
	            // padding: '5px 5px 0px 5px',
	            frame: false,
	            border: false
    	};
    }
});