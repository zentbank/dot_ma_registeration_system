Ext.define('tourism.view.report.SuspendOrderPanel',{
	extend: 'Ext.form.FormPanel',
	requires:[
				'Ext.form.field.Date',
				'Ext.form.field.Time',
				'Ext.form.CheckboxGroup',
				'Ext.layout.container.HBox',
				'Ext.form.FieldSet'
	          ],
	
	alias: 'widget.report-suspend-order-form',
	
	bodyPadding: 10,
	items: [
            {
        xtype: 'container',
        // title: 'Payment',
        layout: 'anchor',
        defaults: {
            anchor: '100%',
            labelWidth: 150
        },
        items: [
                   {
                            xtype: 'container',
                            flex: 1,
                            layout: 'anchor',
                            defaults: {
                                anchor: '100%',
                                labelWidth: 150
                            },
                            items: [
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'เลขที่ใบอนุญาต',
                                        name: 'licenseNo',
                                        id: 'report-suspend-order-98-licenseNo'
                                    },
                            ]
                        }
        ]
    },
    {
        xtype: 'toolbar',
        align:'center',
        border: false,
        padding: '10px 0 10px 0px',
        items: [{
        	xtype: 'button',
            text : 'พิมพ์คำสั่ง',
            handler: function(btn){
                // var form = btn.up('form');
                var form =  Ext.ComponentQuery.query('#report-suspend-order-form-087')[0];
                var licenseNo = Ext.getCmp('report-suspend-order-98-licenseNo').getValue();

                var reqUrl = "business/punishment/docx/supendOrderDocx?licenseNo="+licenseNo;
              window.open(reqUrl,'_blank'); 
            }
        }]
    }

]
});