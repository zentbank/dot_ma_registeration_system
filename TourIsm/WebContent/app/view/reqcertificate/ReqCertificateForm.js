Ext.define('tourism.view.reqcertificate.ReqCertificateForm', {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.data.*',
        'Ext.form.*',
        'Ext.tab.Tab',
        'tourism.view.reqcertificate.ReqDocGrid'
    ],
    alias: 'widget.reqcertificate-addeditform',
    // title: 'ธุรกิจนำเที่ยว',
    bodyPadding: 5,
    autoScroll: true,

    initComponent: function(){

        var licenseDetail = this.businessDetail();

  
        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
        
        Ext.apply(this, {
            width: '100%',
            height: '100%',
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },

            items: [{
                xtype: 'container',
                // title: 'Payment',
                layout: 'anchor',
                defaults: {
                    anchor: '100%',
                    labelWidth: 350
                },
                items: [{
                    xtype: 'container',
                    layout: 'hbox',
                    // margin: '0 0 5 0',
                    items: [{
                            xtype: 'hiddenfield',
                            name: 'cerId'
                        }]
                }, 
                licenseDetail, 
                {
                    xtype: 'tabpanel',
                    plain: true,
                    frame: false,
                    border: false,
                    bodyPadding: 5,
                    items: [{
                        xtype: 'container',
                        layout: 'anchor',
                        defaults: {
                            anchor: '100%'
                            // ,
                            // labelWidth: 250
                        },
                        title: 'รายละเอียดการขอหนังสือรับรองใบอนุญาต',
                        items: [{
                            xtype:'fieldset',
                            title: 'หนังสือแจ้งความจำนงจากบริษัท',
                            collapsible: true,
                            defaults: {
                                anchor: '100%',
                                labelWidth: 200
                            },
                            layout: 'anchor',
                            items :[{   
                                xtype: 'datefield',
                                fieldLabel: 'วันที่ออกหนังสือ',
                                name: 'ttAaDate',
                                format: 'd/m/B',
                                value: new Date()
                            }]
                        }, {
                            xtype:'fieldset',
                            title: 'บันทึกข้อความถึงผู้อำนวยการสำนักทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์',
                            collapsible: true,
                            defaults: {
                                anchor: '100%',
                                labelWidth: 200
                            },
                            layout: 'anchor',
                            items :[{
                                xtype: 'textfield',
                                fieldLabel: 'เลขที่หนังสือ',
                                name: 'bookDocToManagerNo'
                            }, {   
                                xtype: 'datefield',
                                fieldLabel: 'วันที่ออกหนังสือ',
                                name: 'bookDocManagerDate',
                                format: 'd/m/B',
                                value: new Date()
                            }]
                        }, {
                            xtype:'fieldset',
                            title: 'หนังสือรับรองใบอนุญาตภาษาอังกฤษ',
                            collapsible: true,
                            defaults: {
                                anchor: '100%',
                                labelWidth: 200
                            },
                            layout: 'anchor',
                            items :[{
                                xtype: 'textfield',
                                fieldLabel: 'เลขที่หนังสือ',
                                name: 'bookNo'
                            }, {   
                                xtype: 'datefield',
                                fieldLabel: 'วันที่ออกหนังสือ',
                                name: 'bookDate',
                                format: 'd/m/B',
                                value: new Date()
                            }]
                        }, {
                            xtype:'fieldset',
                            title: 'รายละเอียดการอนุมัติ',
                            collapsible: true,
                            defaults: {
                                anchor: '100%',
                                labelWidth: 200
                            },
                                layout: 'anchor',
                                items :[{
                                xtype: 'datefield',
                                fieldLabel: 'วันที่ขอ',
                                name: 'reqDate',
                                format: 'd/m/B'
                            }, {
                                xtype: 'textareafield',
                                name: 'approveRemark',
                                fieldLabel: 'รายละเอียด/เหตุผล',
                                allowBlank: true
                            }, {
                            xtype: 'radiogroup',
                            id: 'reqcertificate-'+this.roleAction+'-'+this.traderType+'-approveStatus-checkboxgroup',
                            fieldLabel: 'สถานะการอนุมัติ',
                            // allowBlank: false,
                            // afterLabelTextTpl: required,
                            columns: 2,
                            vertical: true,
                            items: [
                                { boxLabel: 'อนุมัติ', name: 'approveStatus', inputValue: 'A'},
                                { boxLabel: 'ไม่อนุมัติ', name: 'approveStatus', inputValue: 'R' }
                            ]
                                
                            }, {
                                xtype: 'datefield',
                                fieldLabel: 'วันที่อนุมัติ',
                                id: 'reqcertificate-'+this.roleAction+'-'+this.traderType+'-approveDate',
                                name: 'approveDate',
                                format: 'd/m/B'
                            }]
                        }]
                    }, {
                        xtype: 'reqcertificate-docreq-grid',
                        id: 'reqcertificate-'+this.roleAction+'-'+this.traderType+'-document-grid',
                        title: 'เอกสาร',
                        layout: 'fit',
                        autoScroll: true,
                        frame: false, 
                        border: false
                        
                    }
                    ]
                }]
            }],

            buttons: [{
                text: 'พิมพ์เอกสาร',
                action: 'printCertificate',
                id: 'reqcertificate-'+this.roleAction+'-'+this.traderType+'-printCertificate'
            },'->',{
                text: 'บันทึกข้อมูล',
                action: 'addCertificate',
                id: 'reqcertificate-'+this.roleAction+'-'+this.traderType+'-addCertificate'
            }, {
                text: 'ยกเลิก',
                // width: 150,
                scope: this,
                handler: this.onResetClick
            }]    
        });
        this.callParent(arguments);
    },
    businessDetail: function()
    {
        return {
                    xtype: 'container',
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%',
                        labelWidth: 250
                    },
                    items: [{
                        xtype: 'hiddenfield',
                        name: 'traderId'
                    }, {
                        xtype: 'displayfield',
                        fieldLabel: 'เลขที่ใบอนุญาต',
                        name: 'licenseNo'
                       
                    }, {
                        xtype: 'displayfield',
                        fieldLabel: 'ชื่อประกอบธุรกิจนำเที่ยว',
                        name: 'traderName'
                       
                    }, {
                        xtype: 'displayfield',
                        fieldLabel: 'ชื่อประกอบธุรกิจนำเที่ยว</br>(ภาษาต่างประเทศ)',
                        name: 'traderNameEn'
                       
                    }, {
                        xtype: 'displayfield',
                        fieldLabel: 'ประเภทธุรกิจนำเที่ยว',
                        name: 'traderCategoryName'
                       
                    }, {
                        xtype: 'displayfield',
                        fieldLabel: 'ผู้ขอจดทะเบียน',
                        name: 'traderOwnerName'
                       
                    }, {
                        xtype: 'displayfield',
                        fieldLabel: 'เลขที่นิติบุคคล',
                        name: 'identityNo'
                       
                    }]
                    
                };
    },
    onResetClick: function(){
        this.up('window').close();
    }
    
});