Ext.define('tourism.view.reqcertificate.ReqCertificateGrid',{
// roleAction: 'reqdoc',
// traderType: 'business'
	extend: 'Ext.grid.Panel',
	alias: 'widget.reqcertificate-grid',
	stripeRows: true,
	requires: [
       'Ext.grid.*',
       'Ext.data.*',
       'Ext.util.*',
       'Ext.state.*',
       'Ext.form.*',
       'tourism.model.reqcertificate.ReqCertificateModel',
       'tourism.view.reqcertificate.ReqCertificateWindow'

	],
	
	initComponent: function(){
		var showSummary = true;

	    var store = new Ext.data.Store({
	        model: 'tourism.model.reqcertificate.ReqCertificateModel',
	        pageSize: 20,
			proxy: {
                type: 'ajax',
                actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
                api: {
                    read: 'business/req/certificate/read',
                    destroy: 'business/mas/customer/delete'
                },
                reader: {
                    type: 'json',
			        root: 'list',
			        totalProperty: 'totalCount',
			        successProperty: 'success',
			        messageProperty: 'message'
                },
                writer: {
                    type: 'json',
                    writeAllFields: true, ////just send changed fields
                    allowSingle: false, //always wrap in an array
                    batch: false
                },
                listeners: {
                    exception: function(proxy, response, operation){

                        Ext.MessageBox.show({
                            title: 'REMOTE EXCEPTION',
                            msg: operation.getError(),
                            icon: Ext.MessageBox.ERROR,
                            buttons: Ext.Msg.OK
                        });
                    },
                    beforeload: function( store, operation, eOpts )
                    {
                       	var formId = '#' + store.roleAction+'-'+store.traderType+'-reqcertificate-formsearch';				    
			        	var formSearch = Ext.ComponentQuery.query(formId)[0];
			        	var values = formSearch.getValues();
							
			        	 for (field in values) 
					      {
					        if (Ext.isEmpty(values[field])) 
					        {
					          delete values[field];
					        }
					        
					      }
					    store.proxy.extraParams = values;
                    }
                }
            }

	    }); 
		
		Ext.apply(this,{
			store: store,
			tools: [{
                itemId: 'showReqcertificateWindow',
                type: 'plus',
                tooltip: 'เพิ่มการขอหนังสือรับรองใบอนุญาต',
                callback: function(grid){
                
                	var win = Ext.create('tourism.view.reqcertificate.ReqCertificateWindow',{
                	id: 'reqcertificate-window-id001',
				      traderType: 'business',
				      roleAction: 'reqdoc',
				      mode: 'adddata'
				    });
				    win.show();

				    var form = win.down('info-license-formsearch');

				    form.getForm().findField('traderType').setValue('B');
                }
            }],
			dockedItems:[{
				xtype: 'pagingtoolbar',
				store: store,
				dock: 'bottom',
				displayInfo: true

			}],
			 columns:[{
	             header: 'เลขที่ใบอนุญาต',
	             dataIndex: 'licenseNo',
	             flex: 2
	         	
	         }, {
				header: 'ชื่อ',
				dataIndex: 'traderName',
				flex: 2
			}, {
				header: 'วันที่ขอ',
				dataIndex: 'reqDate',
				flex: 1
			}, {
				header: 'สถานะ',
				dataIndex: 'approveStatus',
				flex: 2
			}, {
	        	header: '',
	            xtype: 'actioncolumn',
	           	width:50,
	           	align : 'center',
	            items: [{
	              
					getClass: function(v, meta, rec) {          
	                    return 'icon-edit-xsmall';
	                },			        
		            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {

		            	this.fireEvent('itemclick', this, 'editreqcer', grid, rowIndex, colIndex, record, node);
		            }
	            }]
	        }]
	 
		});
		
		
		this.callParent(arguments);
	}

});







