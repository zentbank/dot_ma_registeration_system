Ext.define('tourism.view.reqcertificate.ReqCertificatePanel', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
        'tourism.view.reqcertificate.ReqCertificateFormSearch',
        'tourism.view.reqcertificate.ReqCertificateGrid'
    ],
    
    xtype: 'reqcertificate-panel',
    initComponent: function(){

    	Ext.apply(this, {
    		layout: {
		        type: 'border'
		    },
		    items: [{
		        region: 'north',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items: [
		                {
		                	xtype: 'reqcertificate-formsearch',
		                	title: 'ค้นหา',
		                	collapsible: true,  
		    	            id: this.roleAction+'-'+this.traderType+'-reqcertificate-formsearch',
		    	            frame: false,
		    	            border: false,
		    	            roleAction: this.roleAction,
		            		traderType: this.traderType 
		                }
		        ]
		    },{
		        region: 'center',
		        xtype: 'panel',
		        layout: 'fit',
		        frame: false,
		        border: false,
		        items:[{
		        	xtype: 'reqcertificate-grid',
		        	id: this.roleAction+'-'+this.traderType+'-reqcertificate-grid',
		        	title: 'รายการ',
		            autoScroll: true,
		            border: false,
		            roleAction: this.roleAction,
		            traderType: this.traderType 
		        }]
		    }]
    	});

    	this.callParent(arguments);
    }
});