Ext.define('tourism.view.reqcertificate.ReqCertificateWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.reqcertificate-window',

    requires: [
        'tourism.view.info.SearchLicensePanel',
        'tourism.view.reqcertificate.ReqCertificateForm'
    ],
    
    initComponent: function() {
     

        Ext.apply(this, {
            title: 'หนังสือรับรองใบอนุญาต',
            // layout: 'fit',
            layout: 'card',
            activeItem: 0,
            autoShow: true,
            width: 800,
            height: 600,
            modal: true,
            items :[
            {
                // title: 'เลือกใบอนุญาต',
                xtype: 'info-license-searchlicensepanel',
                id: 'reqcertificate-'+this.roleAction+'-'+this.traderType+'-searchlicensepanel',
                border: false,
                frame: false
            }, {
                // title: 'บันทึกข้อมูลการพักใช้',
                xtype: 'reqcertificate-addeditform',
                id: 'reqcertificate-'+this.roleAction+'-'+this.traderType+'-formaddedit',
                border: false,
                frame: false,
                roleAction: this.roleAction,
                traderType: this.traderType
            }]
        });

        this.callParent(arguments);
    },

});
