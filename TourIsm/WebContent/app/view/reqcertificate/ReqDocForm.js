Ext.define('tourism.view.reqcertificate.ReqDocForm', {
    extend: 'Ext.form.Panel',
    alias : 'widget.reqcertificate-docreq-form',
    requires: [
        'Ext.data.*',
        'Ext.form.*',
        'Ext.tab.Tab'
        
    ],
   
    bodyPadding: 5,
    autoScroll: true,

    initComponent: function(){

     var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
        
        Ext.apply(this, {
            width: '100%',
            height: '100%',
            // layout: 'anchor',
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },
            items: 
            [
                {
                    xtype: 'hiddenfield',
                    name: 'reqDocId'
                }, {
                    xtype: 'textfield',
                    fieldLabel: 'ชื่อเอกสารคำสั่ง',
                    name: 'documentName',
                    anchor: '100%',
                    allowBlank: false,
                    afterLabelTextTpl: required
                }, {
                    xtype: 'filefield',
                    // id: 'form-file',
                    emptyText: 'เลือกเอกสารที่ต้องการ',
                    fieldLabel: 'เอกสาร',
                    name: 'documentFile',
                    allowBlank: false,
                    afterLabelTextTpl: required,
                    anchor: '100%',
                    buttonText: '',
                    buttonConfig: {
                        iconCls: 'upload-icon'
                    }
                }
            ],

            buttons: [{
                text: 'บันทึก',
                scope: this,
                action: 'saveReqDocCertificate',
                id: 'reqcertificate-'+this.roleAction+'-'+this.traderType+'-saveReqDocCertificate'
            }, {
                text: 'ยกเลิก',
                // width: 150,
                scope: this,
                handler: this.onResetClick
            }]    
        });
        this.callParent(arguments);
    }
    ,onResetClick: function(){
        this.up('window').close();
    }
});