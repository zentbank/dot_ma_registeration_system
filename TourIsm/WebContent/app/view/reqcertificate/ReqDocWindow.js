Ext.define('tourism.view.reqcertificate.ReqDocWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.reqcertificate-docreq-window',

    requires: [
        
        'tourism.view.reqcertificate.ReqDocForm'
    ],
    
    initComponent: function() {
            

        Ext.apply(this, {
            title:'เอกสาร',
            autoShow: true,
            modal: true,
            items :[
                {
                    // title: 'บันทึกข้อมูลการพักใช้',
                    xtype: 'reqcertificate-docreq-form',
                    id: 'reqcertificate-'+this.roleAction+'-'+this.traderType+'-docreq-form',
                    border: false,
                    frame: false,
                    roleAction: this.roleAction,
                    traderType: this.traderType
                   
                }
            ]
        });

        this.callParent(arguments);
    }

});
