Ext.define('tourism.view.reqpassport.ReqPassportForm', {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.data.*',
        'Ext.form.*',
        'Ext.tab.Tab',
        'tourism.view.reqcertificate.ReqDocGrid'
    ],
    alias: 'widget.reqpassport-addeditform',
    // title: 'ธุรกิจนำเที่ยว',
    bodyPadding: 5,
    autoScroll: true,
    
    

    initComponent: function(){
  
        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
        
        Ext.apply(this, {
            width: '100%',
            height: '100%',
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },

            items: [{
                xtype: 'tabpanel',
                plain: true,
                frame: false,
                border: false,
                items: [{
                    xtype: 'form',
                    title: 'รายละเอียด',
                    
                    frame: false,
                    border: false,
                    items: [{
                        xtype: 'container',
                        layout: 'anchor',
                        defaults: {
                            anchor: '100%'
                        },
                        items:[{
                            xtype: 'hiddenfield',
                            name: 'reqPassId'
                        }, {   
                            xtype: 'datefield',
                            fieldLabel: 'วันที่รับเรื่อง',
                            name: 'reqDate',
                            format: 'd/m/B',
                            value: new Date()
                        }, {
                            xtype:'fieldset',
                            title: 'รายละเอียด',
                            collapsible: true,
                            defaults: {
                                anchor: '100%',
                                labelWidth: 200
                            },
                            layout: 'anchor',
                            items :[{
                                xtype: 'hiddenfield',
                                name: 'traderGuideId'
                            }, {
                                xtype: 'hiddenfield',
                                name: 'traderLeaderId'
                            },{
                                xtype: 'displayfield',
                                fieldLabel: 'ชื่อ-นามสกุล',
                                name: 'personName'
                               
                            }, {
                                xtype: 'displayfield',
                                fieldLabel: 'หมายเลขบัตรประชาชน',
                                name: 'reqIdCard'
                               
                            }, {
                                xtype: 'displayfield',
                                fieldLabel: 'หนังสือเดินทางเลขที่',
                                name: 'passportNo'
                               
                            },  {
                                xtype: 'textfield',
                                fieldLabel: 'เลขที่ใบอนุญาตผู้นำเที่ยว',
                                name: 'leaderLicenseNo',
                                allowBlank: false,
                                afterLabelTextTpl: required
                            }, {
                                xtype: 'container',
                                title: 'รายละเอียด',
                                layout: 'anchor',
                                defaults: {
                                    anchor: '100%',
                                    labelWidth: 200
                                },
                                items: [{
                                    xtype: 'displayfield',
                                    fieldLabel: 'เลขที่ใบอนุญาตมัคคุเทศก์',
                                    flex: 1,
                                    name: 'guideLicenseNo'
                                }, {
                                    xtype: 'displayfield',
                                    fieldLabel: 'วันหมดอายุ',
                                    flex: 1,
                                    name: 'expireDate'
                                }]
                            }]
                        }, {
                            xtype:'fieldset',
                            title: 'เป็นพนักงานของบริษัท', // title or checkboxToggle creates fieldset header
                            checkboxToggle: true,
                            itemId: 'reqpassport-addeditform-business-fieldset',
                            // check ให้เลือกธุรกิจนำเที่ยว
                            listeners: {
                                expand: function( f, eOpts ) {
                                   var form = f.up('form');
                                   var licenseNo = form.getForm().findField('licenseNo').getValue();

                                   if(Ext.isEmpty(licenseNo)){
                                        var win = Ext.create('tourism.view.reqpassport.ReqPassportWindow',{
                                          id: 'reqpassport-business-window-id002',
                                          traderType: 'business',
                                          roleAction: 'subpassport',
                                          mode: 'adddata'
                                        });
                                        win.show();

                                        var form = win.down('info-license-formsearch');

                                        form.getForm().findField('traderType').setValue('B');
                                   }
                                }, 
                                collapse: function( f, eOpts ){
                                   var form = f.up('form');
                                   form.getForm().findField('traderId').reset();
                                   form.getForm().findField('licenseNo').reset();
                                   form.getForm().findField('traderName').reset();
                                   form.getForm().findField('traderNameEn').reset();
                                   form.getForm().findField('traderCategoryName').reset();
                                   form.getForm().findField('traderOwnerName').reset();
                                   form.getForm().findField('identityNo').reset();
                                }
                            },
                            collapsed: true, // fieldset initially collapsed
                            layout:'anchor',
                            defaults: {
                                anchor: '100%',
                                labelWidth: 200
                            },
                            items :[{
                                xtype: 'hiddenfield',
                                name: 'traderId'
                            }, {
                                xtype: 'displayfield',
                                fieldLabel: 'เลขที่ใบอนุญาต',
                                name: 'licenseNo'
                               
                            }, {
                                xtype: 'displayfield',
                                fieldLabel: 'ชื่อประกอบธุรกิจนำเที่ยว',
                                name: 'traderName'
                               
                            }, {
                                xtype: 'displayfield',
                                fieldLabel: 'ชื่อประกอบธุรกิจนำเที่ยว</br>(ภาษาต่างประเทศ)',
                                name: 'traderNameEn'
                               
                            }, {
                                xtype: 'displayfield',
                                fieldLabel: 'ประเภทธุรกิจนำเที่ยว',
                                name: 'traderCategoryName'
                               
                            }, {
                                xtype: 'displayfield',
                                fieldLabel: 'ผู้ขอจดทะเบียน',
                                name: 'traderOwnerName'
                               
                            }, {
                                xtype: 'displayfield',
                                fieldLabel: 'เลขที่นิติบุคคล',
                                name: 'identityNo'
                               
                            }]
                        }, {
                            xtype:'fieldset',
                            title: 'หนังสือรับรองจากสมาคม',
                            collapsible: true,
                            defaults: {
                                anchor: '100%',
                                labelWidth: 200
                            },
                            layout: 'anchor',
                            items :[{
                                xtype: 'textfield',
                                fieldLabel: 'ชื่อสมาคม',
                                name: 'agentsAssociation'
                            } ,{
                                xtype: 'textfield',
                                fieldLabel: 'เลขที่หนังสือ',
                                name: 'ttAaNo'
                            }, {   
                                xtype: 'datefield',
                                fieldLabel: 'ลงวันที่',
                                name: 'ttAaDate',
                                format: 'd/m/B',
                                value: new Date()
                            }]
                        }, {
                            xtype:'fieldset',
                            title: 'บันทึกข้อความถึงผู้อำนวยการสำนักทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์',
                            collapsible: true,
                            defaults: {
                                anchor: '100%',
                                labelWidth: 200
                            },
                            layout: 'anchor',
                            items :[{
                                xtype: 'textfield',
                                fieldLabel: 'เลขที่หนังสือ',
                                name: 'bookDocToManagerNo'
                            }, {   
                                xtype: 'datefield',
                                fieldLabel: 'วันที่ออกหนังสือ',
                                name: 'bookDocManagerDate',
                                format: 'd/m/B',
                                value: new Date()
                            }]
                        }, {
                            xtype:'fieldset',
                            title: 'บันทึกข้อความถึงอธิบดีกรมท่องเที่ยว',
                            collapsible: true,
                            defaults: {
                                anchor: '100%',
                                labelWidth: 200
                            },
                            layout: 'anchor',
                            items :[{
                                xtype: 'textfield',
                                fieldLabel: 'เลขที่หนังสือ',
                                name: 'bookDocToDirectorNo'
                            }, {   
                                xtype: 'datefield',
                                fieldLabel: 'วันที่ออกหนังสือ',
                                name: 'bookDocToDirectorDate',
                                format: 'd/m/B',
                                value: new Date()
                            }]
                        }, {
                            xtype:'fieldset',
                            title: 'หนังสือรับรองสถานะภาพผู้นำเที่ยวเพื่อขอหนังสือเดินทางเล่ม 2',
                            collapsible: true,
                            defaults: {
                                anchor: '100%',
                                labelWidth: 200
                            },
                            layout: 'anchor',
                            items :[{
                                xtype: 'textfield',
                                fieldLabel: 'เลขที่หนังสือ',
                                name: 'bookNo'
                            }, {   
                                xtype: 'datefield',
                                fieldLabel: 'วันที่ออกหนังสือ',
                                name: 'bookDate',
                                format: 'd/m/B',
                                value: new Date()
                            }]
                        }]
                    }]
                }, {
                    xtype: 'reqcertificate-docreq-grid',
                    id: 'reqpassport-'+this.roleAction+'-'+this.traderType+'-document-grid',
                    title: 'เอกสาร',
                    layout: 'fit',
                    autoScroll: true,
                    frame: false, 
                    border: false
                
                }]
            }],

            buttons: [{
                text: 'พิมพ์เอกสาร',
                action: 'printPassport',
                id: 'reqpassport-'+this.roleAction+'-'+this.traderType+'-printPassport'
            },'->',{
                text: 'บันทึกข้อมูล',
                action: 'saveReqPassport',
                id: 'reqpassport-'+this.roleAction+'-'+this.traderType+'-saveReqPassport'
            }, {
                text: 'ยกเลิก',
                // width: 150,
                scope: this,
                handler: this.onResetClick
            }]    
        });
        this.callParent(arguments);
    },
    onResetClick: function(){
        this.up('window').close();
    }
    
});