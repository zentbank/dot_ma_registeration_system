Ext.define('tourism.view.reqpassport.ReqPassportFormSearch' ,{
    // extend: 'Ext.form.FieldSet',
    extend: 'Ext.form.FormPanel',
    alias : 'widget.reqpassport-formsearch',
    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.Time',
        'Ext.form.CheckboxGroup',
        'Ext.layout.container.HBox',
        'Ext.form.FieldSet'
    ],
    bodyPadding: 10,
    // width: '100%',
    items: [{
        xtype: 'container',
        layout: 'hbox',
        layoutCongig: {
             pack:'center',
             align:'middle'
        },
        items: [
            {
                xtype: 'container',
                // title: 'Payment',
                flex: 1,
                layout: 'anchor',
                defaults: {
                    anchor: '100%',
                    labelWidth: 150
                },
                items: [{
                    xtype: 'textfield',
                    fieldLabel: 'ทะเบียนผู้นำเที่ยวเลขที่',
                    name: 'leaderLicenseNo'
                }, {
                    xtype: 'textfield',
                    fieldLabel: 'หมายเลขบัตรประชาชน',
                    name: 'reqIdCard'
                }, {
                    xtype: 'datefield',
                    fieldLabel: 'วันที่รับเรื่อง',
                    name: 'reqDateFrom',
                    format: 'd/m/B'
                } , {   
                    xtype: 'datefield',
                    fieldLabel: 'ถึงวันที่',
                    name: 'reqDateTo',
                    flex: 1,
                    format: 'd/m/B'
                }]
            },{
                xtype: 'splitter'
            },
            {
                xtype: 'container',
                flex: 1,
                layout: 'anchor',
                defaults: {
                    anchor: '100%',
                    labelWidth: 150
                },
                items: [{
                    xtype: 'textfield',
                    fieldLabel: 'ใบอนุญาตมัคคุเทศกืเลขที่',
                    name: 'guideLicenseNo'
                }, {
                    xtype: 'textfield',
                    fieldLabel: 'หมายเลขหนังสือเดินทาง',
                    name: 'passportNo'
                }, {
                    xtype: 'textfield',
                    fieldLabel: 'ชื่อ-นามสกุล',
                    name: 'personName'
                }]
            }
        ]
    },
    {
        xtype: 'toolbar',
        border: false,
        padding: '6px 0 6px 0px',
        items: [{
            xtype: 'button',
            text : 'ค้นหา',
            action: 'searchReqPassport'
        }, {
            xtype: 'button',
            text : 'พิมพ์รายงาน',
            action: 'printReport'
        }]
    }]
});    
