Ext.define('tourism.view.reqpassport.ReqPassportGrid',{
   // roleAction: 'subpassport',
   //        traderType: 'tourleader'
	extend: 'Ext.grid.Panel',
	alias: 'widget.reqpassport-grid',
	stripeRows: true,
	requires: [
       'Ext.grid.*',
       'Ext.data.*',
       'Ext.util.*',
       'Ext.state.*',
       'Ext.form.*',
       'tourism.model.reqpassport.ReqPassportModel',
       'tourism.view.reqpassport.ReqPassportWindow'

	],
	
	initComponent: function(){
		var showSummary = true;

	    var store = new Ext.data.Store({
	        model: 'tourism.model.reqpassport.ReqPassportModel',
	        pageSize: 20,
	        groupField: 'traderName',
			proxy: {
                type: 'ajax',
                actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
                api: {
                    read: 'business/req/passport/read'
                },
                reader: {
                    type: 'json',
			        root: 'list',
			        totalProperty: 'totalCount',
			        successProperty: 'success',
			        messageProperty: 'message'
                },
                writer: {
                    type: 'json',
                    writeAllFields: true, ////just send changed fields
                    allowSingle: false, //always wrap in an array
                    batch: false
                },
                listeners: {
                    exception: function(proxy, response, operation){

                        Ext.MessageBox.show({
                            title: 'REMOTE EXCEPTION',
                            msg: operation.getError(),
                            icon: Ext.MessageBox.ERROR,
                            buttons: Ext.Msg.OK
                        });
                    },
                    beforeload: function( store, operation, eOpts )
                    {
                       	var formId = '#' + store.roleAction+'-'+store.traderType+'-reqpassport-formsearch';				    
			        	var formSearch = Ext.ComponentQuery.query(formId)[0];
			        	var values = formSearch.getValues();
							
			        	 for (field in values) 
					      {
					        if (Ext.isEmpty(values[field])) 
					        {
					          delete values[field];
					        }
					        
					      }
					    store.proxy.extraParams = values;
                    }
                }
            }

	    }); 
		
		Ext.apply(this,{
			store: store,
			tools: [{
                itemId: 'showReqPassportWindow',
                type: 'plus',
                tooltip: 'เพิ่มการขอหนังสือรเดินทางเล่ม 2',
                callback: function(grid){
                
                	var win = Ext.create('tourism.view.reqpassport.ReqPassportWindow',{
                	  id: 'reqpassport-tour-window-id001',
				      traderType: 'tourleader',
				      roleAction: 'subpassport',
				      mode: 'adddata'
				    });
				    win.show();

				    var form = win.down('info-license-formsearch');

				    form.getForm().findField('traderType').setValue('L');
                }
            }],
            features: [{
		        ftype: 'grouping',
		        groupHeaderTpl: '{columnName}: {name}',
		        hideGroupedHeader: true,
		        startCollapsed: false
		    }],
			dockedItems:[{
				xtype: 'pagingtoolbar',
				store: store,
				dock: 'bottom',
				displayInfo: true

			}],
			 columns:[ {
				header: 'ชื่อ-นามสกุล',
				dataIndex: 'personName',
				flex: 2,
                renderer: function(value, metaData, model, rowIndex,colIndex,gridstore,gridview){
            		return Ext.String.format('<p>{0}</p><p><small>ใบอนุญาตมัคคุเทศก์เลขที่: {1}</small></p><p><small>ทะเบียนผู้นำเที่ยวเลขที่:  {2}</small></p>',
            			value,
            			model.get('guideLicenseNo'),
            			model.get('leaderLicenseNo'));
			    }
			}, {
	             header: 'ใบอนุญาตธุรกิจนำเที่ยว',
	             dataIndex: 'traderName',
	             flex: 1
	         	
	         }, {
	             header: 'เลขที่หนังสือ',
	             dataIndex: 'bookNo',
	             flex: 1
	         	
	         }, {
				header: 'วันที่หนังสือ',
				dataIndex: 'bookDate',
				flex: 1
			}, {
	        	header: '',
	            xtype: 'actioncolumn',
	           	width:50,
	           	align : 'center',
	            items: [{
	              
					getClass: function(v, meta, rec) {          
	                    return 'icon-edit-xsmall';
	                },			        
		            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {

		            	this.fireEvent('itemclick', this, 'editreqcer', grid, rowIndex, colIndex, record, node);
		            }
	            }]
	        }]
	 
		});
		
		
		this.callParent(arguments);
	}

});







