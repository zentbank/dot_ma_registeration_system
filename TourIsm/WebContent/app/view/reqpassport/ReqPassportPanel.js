Ext.define('tourism.view.reqpassport.ReqPassportPanel', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
        'tourism.view.reqpassport.ReqPassportFormSearch',
        'tourism.view.reqpassport.ReqPassportGrid'
    ],
    
    xtype: 'reqpassport-panel',
    initComponent: function(){

    	Ext.apply(this, {
    		layout: {
		        type: 'border'
		    },
		    items: [{
		        region: 'north',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items: [
		                {
		                	xtype: 'reqpassport-formsearch',
		                	title: 'ค้นหา',
		                	collapsible: true,  
		    	            id: this.roleAction+'-'+this.traderType+'-reqpassport-formsearch',
		    	            frame: false,
		    	            border: false,
		    	            roleAction: this.roleAction,
		            		traderType: this.traderType 
		                }
		        ]
		    },{
		        region: 'center',
		        xtype: 'panel',
		        layout: 'fit',
		        frame: false,
		        border: false,
		        items:[{
		        	xtype: 'reqpassport-grid',
		        	id: this.roleAction+'-'+this.traderType+'-reqpassport-grid',
		        	title: 'รายการ',
		            autoScroll: true,
		            border: false,
		            roleAction: this.roleAction,
		            traderType: this.traderType 
		        }]
		    }]
    	});

    	this.callParent(arguments);
    }
});