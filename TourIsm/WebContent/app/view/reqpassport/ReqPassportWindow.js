Ext.define('tourism.view.reqpassport.ReqPassportWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.reqpassport-window',

    requires: [
        'tourism.view.info.SearchLicensePanel',
        'tourism.view.reqpassport.ReqPassportForm'
    ],
    
    initComponent: function() {
     

        Ext.apply(this, {
            title: 'หนังสือรับรองใบอนุญาต',
            // layout: 'fit',
            layout: 'card',
            activeItem: 0,
            autoShow: true,
            width: 800,
            height: 600,
            modal: true,
            items :[
            {
                // title: 'เลือกใบอนุญาต',
                xtype: 'info-license-searchlicensepanel',
                id: 'reqpassport-'+this.roleAction+'-'+this.traderType+'-searchlicensepanel',
                border: false,
                frame: false
            }, {
                // title: 'บันทึกข้อมูลการพักใช้',
                xtype: 'reqpassport-addeditform',
                id: 'reqpassport-'+this.roleAction+'-'+this.traderType+'-formaddedit',
                border: false,
                frame: false,
                roleAction: this.roleAction,
                traderType: this.traderType
            }]
        });

        this.callParent(arguments);
    },

});
