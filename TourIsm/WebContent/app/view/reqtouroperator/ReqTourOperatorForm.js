Ext.define('tourism.view.reqtouroperator.ReqTourOperatorForm', {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.data.*',
        'Ext.form.*',
        'Ext.tab.Tab',
        'tourism.view.reqtouroperator.ReqTourOptDocGrid'
    ],
    alias: 'widget.reqtouroperator-addeditform',
    // title: 'ธุรกิจนำเที่ยว',
    bodyPadding: 5,
    autoScroll: true,



    initComponent: function(){

        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

        var masprefixTourOperatorstore =  Ext.create('tourism.store.combo.MasPrefixStore',{
            storeId:'reqtouroperator-touroperator-masprefixstore-store'
        });

        var masprefixPersontore =  Ext.create('tourism.store.combo.MasPrefixStore',{
            storeId:'reqtouroperator-person-masprefixstore-store'
        });

        var provinceStore =  Ext.create('tourism.store.combo.MasProvinceStore',{
            storeId:'reqtouroperator-address-masprovince-store'
        });

        var  tambolStore  = Ext.create('tourism.store.combo.MasTambolStore',{
            storeId:'business-reqtouroperator-address-tambol-store'
        });
        var amphurStore =   Ext.create('tourism.store.combo.MasAmphurStore',{
            storeId:'reqtouroperator-address-masamphurEN-store'
        });

        Ext.apply(this, {
            width: '100%',
            height: '100%',
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },

            items: [{
                xtype: 'tabpanel',
                plain: true,
                frame: false,
                border: false,
                items: [{
                    xtype: 'form',
                    title: 'บันทึกข้อมูล',

                    frame: false,
                    border: false,
                    items: [{
                        xtype: 'container',
                        layout: 'anchor',
                        defaults: {
                            anchor: '100%'
                        },
                        items:[{
                            // ชื่อ
                            xtype: 'fieldcontainer',


                            afterLabelTextTpl: required,
                            // labelWidth: 100,
                            layout: 'hbox',
                            items: [
                                {
                                    xtype: 'masprefixcombo',
                                    fieldLabel: 'คำนำหน้า',
                                    labelAlign: 'top',
                                    store: masprefixTourOperatorstore,
                                    afterLabelTextTpl: required,
                                    queryMode: 'remote',
                                    displayField: 'prefixName',
                                    valueField: 'prefixId',
                                    hiddenName: 'prefixId',
                                    name: 'prefixId',
                                    tabIndex: 2,
                                    flex: 1,
                                    triggerAction: 'all',
                                    allowBlank: false,
                                    prefixType: 'C'
                                }, {
                                    xtype: 'splitter'
                                }, {
                                    xtype: 'textfield',
                                    labelAlign: 'top',
                                    afterLabelTextTpl: required,
                                    fieldLabel: 'ชื่อ',
                                    name: 'companyName',
                                    tabIndex: 2,
                                    flex: 2,
                                    allowBlank: false
                                },{
                                    xtype: 'splitter'
                                },{
                                    xtype: 'combo',
                                    fieldLabel: 'คำลงท้าย',
                                    labelAlign: 'top',
                                    store: 'tourism.store.combo.MasPostfixStore',
                                    queryMode: 'remote',
                                    displayField: 'postfixName',
                                    valueField: 'postfixId',
                                    hiddenName: 'postfixId',
                                    name: 'postfixId',
                                    tabIndex: 3,
                                    flex: 1,
                                    triggerAction: 'all'
                                    // ,
                                    // allowBlank: false
                                }
                            ]
                        },{
                            xtype: 'textfield',
                            labelAlign: 'top',
                            afterLabelTextTpl: required,
                            fieldLabel: 'เลขที่นิติบุคคล',
                            name: 'taxId',
                            tabIndex: 4,
                            flex: 2,
                            allowBlank: false
                        }, {
                            xtype:'fieldset',
                            title: 'ผู้ให้ข้อมูล',
                            collapsible: true,
                            defaults: {
                                anchor: '100%',
                                labelWidth: 200
                            },
                            layout: 'anchor',
                            items :[{
                                // ชื่อ
                                xtype: 'fieldcontainer',


                                afterLabelTextTpl: required,
                                // labelWidth: 100,
                                layout: 'hbox',
                                items: [
                                    {
                                        xtype: 'masprefixcombo',
                                         fieldLabel: 'คำนำหน้าชื่อ',
                                        labelAlign: 'top',
                                        store: masprefixPersontore,
                                        queryMode: 'remote',
                                        displayField: 'prefixName',
                                        valueField: 'prefixId',
                                        hiddenName: 'reqPrefix',
                                        name: 'reqPrefix',
                                        tabIndex: 5,
                                        flex: 1,
                                        triggerAction: 'all',
                                        allowBlank: false,
                                        afterLabelTextTpl: required,
                                        prefixType: 'I'
                                    }, {
                                        xtype: 'splitter'
                                    }, {
                                        xtype: 'textfield',
                                        labelAlign: 'top',
                                        fieldLabel: 'ชื่อ',
                                        name: 'reqName',
                                        tabIndex: 6,
                                        afterLabelTextTpl: required,
                                        flex: 2,
                                        allowBlank: false
                                    },{
                                        xtype: 'splitter'
                                    },{
                                        xtype: 'textfield',
                                        labelAlign: 'top',
                                        fieldLabel: 'นามสกุล',
                                        name: 'reqLastName',
                                        tabIndex: 7,
                                        flex: 2,
                                        afterLabelTextTpl: required,
                                        allowBlank: false
                                    }
                                ]
                            },{
                                // ชื่อ
                                xtype: 'fieldcontainer',
                                labelAlign: 'top',
//                                fieldLabel: 'ชื่อ-นามสกุล',

                                // labelWidth: 100,
                                layout: 'hbox',
                                items: [
                                     {
                                        xtype: 'textfield',
                                        name: 'reqCardId',
                                        labelAlign: 'top',
                                        afterLabelTextTpl: required,
                                        fieldLabel: 'หมายเลขบัตรประชาชน',
                                        tabIndex: 8,
                                        flex: 2,
                                        allowBlank: false
                                     }, {
                                         xtype: 'splitter'
                                     },{
                                        xtype: 'textfield',
                                        afterLabelTextTpl: required,
                                         name: 'reqNation',
                                        labelAlign: 'top',
                                        fieldLabel: 'สัญชาติ',
                                        tabIndex: 9,
                                        flex: 1,
                                        allowBlank: false
                                    }
                                ]
                            },{
                                xtype: 'textareafield',
                                name: 'reqRelationship',
                                labelAlign: 'top',
                                fieldLabel: 'เกี่ยวข้องกับนิติบุคคลโดยเป็น',
                                allowBlank: true
                            },{

                                xtype: 'fieldcontainer',
                                layout: 'hbox',
                                items: [
                                    {
                                        xtype: 'textfield',
                                        labelAlign: 'top',
//                                        afterLabelTextTpl: required,
                                        fieldLabel: 'อยู่บ้านเลขที่',
                                        name: 'homeNo',
                                        tabIndex: 10,
                                        flex: 1,
                                        allowBlank: true
                                    }, {
                                        xtype: 'splitter'
                                    }, {
                                        xtype: 'textfield',
                                        labelAlign: 'top',
//                                        afterLabelTextTpl: required,
                                        fieldLabel: 'ซอย',
                                        name: 'soi',
                                        tabIndex: 11,
                                        flex: 2,
                                        allowBlank: true
                                    },{
                                        xtype: 'splitter'
                                    },{
                                        xtype: 'textfield',
                                        labelAlign: 'top',
//                                        afterLabelTextTpl: required,
                                        fieldLabel: 'ถนน',
                                        name: 'roadName',
                                        tabIndex: 12,
                                        flex: 2,
                                        allowBlank: true
                                    }
                                ]
                            },{

                                xtype: 'fieldcontainer',
                                layout: 'hbox',
                                items: [
                                    {
                                        // จังหวัด (TH)
                                        // xtype: 'combo',
                                        xtype: 'masprovincecombo',
                                        labelAlign: 'top',
                                        fieldLabel: 'จังหวัด',
                                        id : 'reqtouroperator-address-masprovince-combo',
                                        store: provinceStore,
                                        displayField: 'provinceName',
                                        valueField: 'provinceId',
                                        hiddenName: 'provinceId',
                                        name: 'provinceId',
                                        allowBlank: true,
                                        flex: 2,
                                        //                                        afterLabelTextTpl: required,
                                        masamphurComboId:'reqtouroperator-address-masamphur-combo',
                                        queryParam: 'provinceName',
                                        tabIndex : 13


                                    }// end จังหวัด (TH)
                                    ,{
                                        xtype: 'splitter'
                                    }
                                    ,{
                                        // อำเภอ/เขต
                                        // xtype: 'combo',
                                        xtype: 'masamphurcombo',
                                        labelAlign: 'top',
                                        fieldLabel: 'อำเภอ/เขต',
                                        id : 'reqtouroperator-address-masamphur-combo',
                                        store: amphurStore,
                                        // queryMode: 'remote',
                                        displayField: 'amphurName',
                                        valueField: 'amphurId',
                                        hiddenName: 'amphurId',
                                        name: 'amphurId',
                                        flex: 2,
                                        // triggerAction: 'all',
                                        allowBlank: true,
//                                        afterLabelTextTpl: required,
                                        queryParam: 'amphurName',
                                        masprovinceComboId: 'reqtouroperator-address-masprovince-combo',
                                        mastambolComboId: 'reqtouroperator-address-tambol-combo',
                                        tabIndex : 14

                                    }// end อำเภอ/เขต
                                    , {
                                        xtype: 'splitter'
                                    } ,{
                                        // ตำบล่
                                        // xtype: 'combo',
                                        xtype: 'mastambolcombo',
                                        fieldLabel: 'ตำบล',
                                        labelAlign: 'top',
                                        id : 'reqtouroperator-address-tambol-combo',
                                        store: tambolStore,
                                        // queryMode: 'remote',
                                        displayField: 'tambolName',
                                        valueField: 'tambolId',
                                        hiddenName: 'tambolId',
                                        name: 'tambolId',
                                        flex: 2,
                                        // triggerAction: 'all',
                                        allowBlank: true,
//                                        afterLabelTextTpl: required,
                                        queryParam: 'tambolName',
                                        masamphurComboId: 'reqtouroperator-address-masamphur-combo',
                                        tabIndex : 15

                                    }// end ตำบล่
                                ]
                            },{

                                xtype: 'fieldcontainer',
                                layout: 'hbox',
                                items: [
                                    {
                                        xtype: 'textfield',
                                        labelAlign: 'top',
//                                        afterLabelTextTpl: required,
                                        fieldLabel: 'รหัสไปรษณีย์',
                                        name: 'postCode',
                                        tabIndex: 16,
                                        flex: 1,
                                        allowBlank: true
                                    }, {
                                        xtype: 'splitter'
                                    }, {
                                        xtype: 'textfield',
                                        labelAlign: 'top',
//                                        afterLabelTextTpl: required,
                                        fieldLabel: 'โทรศัพท์',
                                        name: 'telephone',
                                        tabIndex: 17,
                                        flex: 1,
                                        allowBlank: true
                                    }
                                ]
                            }]
                        }, {
                            xtype:'fieldset',
                            collapsed: true,
                            title: 'บทสัมภาษณ์',
                            collapsible: true,
                            defaults: {
                                anchor: '100%',
                                labelWidth: 200
                            },
                            layout: 'anchor',
                            items :[{
                                xtype: 'textareafield',
                                name: 'reqDesc1',
                                labelAlign: 'top',
                                fieldLabel: 'ข้อ 1.',
                                allowBlank: true
                            },{
                                xtype: 'textareafield',
                                name: 'reqDesc2',
                                labelAlign: 'top',
                                fieldLabel: 'ข้อ 2.',
                                allowBlank: true
                            },{
                                xtype: 'textareafield',
                                name: 'reqDesc3',
                                labelAlign: 'top',
                                fieldLabel: 'ข้อ 3.',
                                allowBlank: true
                            },{
                                xtype: 'textareafield',
                                name: 'reqDesc4',
                                labelAlign: 'top',
                                fieldLabel: 'ข้อ 4.',
                                allowBlank: true
                            },{
                                xtype: 'textareafield',
                                name: 'reqDesc5',
                                labelAlign: 'top',
                                fieldLabel: 'ข้อ 5.',
                                allowBlank: true
                            },{
                                xtype: 'textareafield',
                                name: 'reqDesc6',
                                labelAlign: 'top',
                                fieldLabel: 'ข้อ 6.',
                                allowBlank: true
                            },{
                                xtype: 'textareafield',
                                name: 'reqDesc7',
                                labelAlign: 'top',
                                fieldLabel: 'ข้อ 7.',
                                allowBlank: true
                            },{
                                xtype: 'textareafield',
                                name: 'reqDesc8',
                                labelAlign: 'top',
                                fieldLabel: 'ข้อ 8.',
                                allowBlank: true
                            },{
                                xtype: 'textareafield',
                                name: 'reqDesc9',
                                labelAlign: 'top',
                                fieldLabel: 'ข้อ 9.',
                                allowBlank: true
                            },{
                                xtype: 'textareafield',
                                name: 'reqDesc10',
                                labelAlign: 'top',
                                fieldLabel: 'ข้อ 10.',
                                allowBlank: true
                            },{
                                xtype: 'textareafield',
                                name: 'reqDesc11',
                                labelAlign: 'top',
                                fieldLabel: 'ข้อ 11.',
                                allowBlank: true
                            },{
                                xtype: 'textfield',
                                labelAlign: 'top',
                                fieldLabel: 'ผู้สัมภาษณ์',
                                name: 'interviewer',
                                tabIndex: 2,
                                allowBlank: true
                            },{
                                xtype: 'textfield',
                                labelAlign: 'top',
                                fieldLabel: 'พยาน',
                                name: 'witness',
                                tabIndex: 2,
                                allowBlank: true
                            }]
                        }]
                    }]
                }
                , {
                    xtype: 'reqtouroperator-docreq-grid',
                    id: 'reqtouroperator-'+this.roleAction+'-'+this.traderType+'-document-grid',
                    title: 'เอกสาร',
                    layout: 'fit',
                    autoScroll: true,
                    frame: false,
                    border: false

                }
                ]
            }],

            buttons: [{
                text: 'พิมพ์เอกสาร',
                action: 'printTourOperator',
                id: 'reqtouroperator-'+this.roleAction+'-'+this.traderType+'-printReqTourOpt'
            },'->',{
                text: 'บันทึกข้อมูล',
                action: 'saveReqTourOpt',
                id: 'reqtouroperator-'+this.roleAction+'-'+this.traderType+'-saveReqTourOpt'
            }, {
                text: 'ยกเลิก',
                // width: 150,
                scope: this,
                handler: this.onResetClick
            }]
        });
        this.callParent(arguments);
    },
    onResetClick: function(){
        this.up('window').close();
    }

});
