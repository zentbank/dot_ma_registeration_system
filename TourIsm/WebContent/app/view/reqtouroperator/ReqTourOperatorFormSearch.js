Ext.define('tourism.view.reqtouroperator.ReqTourOperatorFormSearch' ,{
    // extend: 'Ext.form.FieldSet',
    extend: 'Ext.form.FormPanel',
    alias : 'widget.reqtouroperator-formsearch',
    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.Time',
        'Ext.form.CheckboxGroup',
        'Ext.layout.container.HBox',
        'Ext.form.FieldSet'
    ],
    bodyPadding: 10,
    // width: '100%',
    items: [{
        xtype: 'container',
        layout: 'hbox',
        layoutCongig: {
            pack:'center',
            align:'middle'
        },
        items: [
            {
                xtype: 'container',
                // title: 'Payment',
                flex: 1,
                layout: 'anchor',
                defaults: {
                    anchor: '100%',
                    labelWidth: 150
                },
                items: [ {
                    xtype: 'textfield',
                    fieldLabel: 'เลขที่นิติบุคคล',
                    name: 'taxId'
                }]
            },{
                xtype: 'splitter'
            },
            {
                xtype: 'container',
                flex: 1,
                layout: 'anchor',
                defaults: {
                    anchor: '100%',
                    labelWidth: 180
                },
                items: [{
                    xtype: 'textfield',
                    fieldLabel: 'ชื่อบริษัทประกอบธุรกิจนำเที่ยว',
                    name: 'companyName'
                }]
            }
        ]
    },
            {
                xtype: 'toolbar',
                border: false,
                padding: '6px 0 6px 0px',
                items: [{
                    xtype: 'button',
                    text : 'ค้นหา',
                    action: 'searchReqTourOperator'
                }]
            }]
});
