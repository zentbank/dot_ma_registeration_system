Ext.define('tourism.view.reqtouroperator.ReqTourOperatorGrid',{
    // roleAction: 'subpassport',
    //        traderType: 'tourleader'
    extend: 'Ext.grid.Panel',
    alias: 'widget.reqtouroperator-grid',
    stripeRows: true,
    requires: [
        'Ext.grid.*',
        'Ext.data.*',
        'Ext.util.*',
        'Ext.state.*',
        'Ext.form.*',
        'tourism.model.reqtouroperator.ReqTourOperatorModel',
        'tourism.view.reqtouroperator.ReqTourOperatorWindow'

    ],

    initComponent: function(){
        var showSummary = true;

        var store = new Ext.data.Store({
            model: 'tourism.model.reqtouroperator.ReqTourOperatorModel',
            pageSize: 20,
//            groupField: 'traderName',
            proxy: {
                type: 'ajax',
                actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
                api: {
                    read: 'business/reqtouroperator/read'
                },
                reader: {
                    type: 'json',
                    root: 'list',
                    totalProperty: 'totalCount',
                    successProperty: 'success',
                    messageProperty: 'message'
                },
                writer: {
                    type: 'json',
                    writeAllFields: true, ////just send changed fields
                    allowSingle: false, //always wrap in an array
                    batch: false
                },
                listeners: {
                    exception: function(proxy, response, operation){

                        Ext.MessageBox.show({
                            title: 'REMOTE EXCEPTION',
                            msg: operation.getError(),
                            icon: Ext.MessageBox.ERROR,
                            buttons: Ext.Msg.OK
                        });
                    },
                    beforeload: function( store, operation, eOpts )
                    {
                        var formId = '#' + store.roleAction+'-'+store.traderType+'-reqtouroperator-formsearch';
                        var formSearch = Ext.ComponentQuery.query(formId)[0];
                        var values = formSearch.getValues();

                        for (field in values)
                        {
                            if (Ext.isEmpty(values[field]))
                            {
                                delete values[field];
                            }

                        }
                        store.proxy.extraParams = values;
                    }
                }
            }

        });

        Ext.apply(this,{
            store: store,
            tools: [{
                itemId: 'showReqtouroeratorWindow',
                type: 'plus',
                tooltip: 'บันทึกแบบคำขอ',
                callback: function(grid){

                    var win = Ext.create('tourism.view.reqtouroperator.ReqTourOperatorWindow',{
                        id: 'reqtouroperator-tour-window-id001',
                        traderType: 'officer',
                        roleAction: 'reqtouroperator',
                        mode: 'adddata'
                    });
                    win.show();

                    var formreqtouroperator = win.down('#reqtouroperator-reqtouroperator-officer-formaddedit');

                    formreqtouroperator.down('#reqtouroperator-reqtouroperator-officer-printReqTourOpt').setVisible(false);
                    var tabPanel = formreqtouroperator.down('tabpanel');
                    tabPanel.child('#reqtouroperator-reqtouroperator-officer-document-grid').tab.hide();
                }
            }],
//            features: [{
//                ftype: 'grouping',
//                groupHeaderTpl: '{columnName}: {name}',
//                hideGroupedHeader: true,
//                startCollapsed: false
//            }],
            dockedItems:[{
                xtype: 'pagingtoolbar',
                store: store,
                dock: 'bottom',
                displayInfo: true

            }],
            columns:[ {
                header: 'ชื่อบริษัท',
                dataIndex: 'companyName',
                flex: 2

            }, {
                header: 'เลขที่นิติบุคคล',
                dataIndex: 'taxId',
                flex: 1

            }, {
                header: 'ชื่อผู้ขอ',
                dataIndex: 'reqName',
                flex: 2
            }, {
                header: '',
                xtype: 'actioncolumn',
                width:50,
                align : 'center',
                items: [{

                    getClass: function(v, meta, rec) {
                        return 'icon-edit-xsmall';
                    },
                    handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {

                        this.fireEvent('itemclick', this, 'editreqopt', grid, rowIndex, colIndex, record, node);
                    }
                }]
            }]

        });


        this.callParent(arguments);
    }

});







