Ext.define('tourism.view.reqtouroperator.ReqTourOperatorPanel', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
        'tourism.view.reqtouroperator.ReqTourOperatorFormSearch',
        'tourism.view.reqtouroperator.ReqTourOperatorGrid'
    ],

    xtype: 'reqtouroperator-panel',
    initComponent: function(){

        Ext.apply(this, {
            layout: {
                type: 'border'
            },
            items: [{
                region: 'north',
                xtype: 'panel',
                frame: false,
                border: false,
                items: [
                    {
                        xtype: 'reqtouroperator-formsearch',
                        title: 'ค้นหา',
                        collapsible: true,
                        id: this.roleAction+'-'+this.traderType+'-reqtouroperator-formsearch',
                        frame: false,
                        border: false,
                        roleAction: this.roleAction,
                        traderType: this.traderType
                    }
                ]
            },{
                region: 'center',
                xtype: 'panel',
                layout: 'fit',
                frame: false,
                border: false,
                items:[{
                    xtype: 'reqtouroperator-grid',
                    id: this.roleAction+'-'+this.traderType+'-reqtouroperator-grid',
                    title: 'รายการ',
                    autoScroll: true,
                    border: false,
                    roleAction: this.roleAction,
                    traderType: this.traderType
                }]
            }]
        });

        this.callParent(arguments);
    }
});
