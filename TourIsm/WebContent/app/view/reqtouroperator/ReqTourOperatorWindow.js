Ext.define('tourism.view.reqtouroperator.ReqTourOperatorWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.reqtouroperator-window',

    requires: [
        'tourism.view.reqtouroperator.ReqTourOperatorForm'
    ],

    initComponent: function() {


        Ext.apply(this, {
            title: 'หนังสือรับรองประกอบธุรกิจนำเที่ยว',
             layout: 'fit',
//            layout: 'card',
            activeItem: 0,
            autoShow: true,
            width: 600,
            height: 600,
            modal: true,
            items :[
                 {
                    // title: 'บันทึกข้อมูลการพักใช้',
                     xtype: 'reqtouroperator-addeditform',
                     id: 'reqtouroperator-'+this.roleAction+'-'+this.traderType+'-formaddedit',
                    border: false,
                    frame: false,
                    roleAction: this.roleAction,
                    traderType: this.traderType
                }]
        });

        this.callParent(arguments);
    },

});
