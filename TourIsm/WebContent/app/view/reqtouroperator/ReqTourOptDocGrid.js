Ext.define('tourism.view.reqtouroperator.ReqTourOptDocGrid',{
    extend: 'Ext.grid.Panel',
    alias: 'widget.reqtouroperator-docreq-grid',
    stripeRows: true,
    requires: [
        'Ext.grid.*',
        'Ext.data.*',
        'Ext.util.*',
        'Ext.state.*',
        'Ext.form.*',
        'tourism.model.reqcertificate.ReqCertificateModel',
        'tourism.view.reqtouroperator.ReqTourOptDocWindow'
//        ,
//        'tourism.view.reqtouroperator.ReqDocWindow'

    ],

    initComponent: function(){

        var store = new Ext.data.Store({
            model: 'tourism.model.reqcertificate.ReqCertificateModel',
            proxy: {
                type: 'ajax',
                actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
                api: {
                    read: 'business/reqtouroperator/doc/readDocument',
                    destroy: 'business/reqtouroperator/doc/delete'
                },
                reader: {
                    type: 'json',
                    root: 'list',
                    totalProperty: 'totalCount',
                    successProperty: 'success',
                    messageProperty: 'message'
                },
                writer: {
                    type: 'json',
                    writeAllFields: true, ////just send changed fields
                    allowSingle: false, //always wrap in an array
                    batch: false
                },
                listeners: {
                    exception: function(proxy, response, operation){

                        Ext.MessageBox.show({
                            title: 'REMOTE EXCEPTION',
                            msg: operation.getError(),
                            icon: Ext.MessageBox.ERROR,
                            buttons: Ext.Msg.OK
                        });
                    }
                }
            }

        });

        Ext.apply(this,{
            store: store,
            columns:[{
                dataIndex: 'reqDocId',
                hidden: true
            }, {
                header: 'เอกสาร',
                dataIndex: 'documentName',
                flex: 2
            }, {
                header: '',
                xtype: 'actioncolumn',
                width: 50,
                align : 'center',
                items: [{
                    getClass: function(v, meta, rec) {
                        return 'icon-edit-xsmall';
                    },
                    handler: function(grid, rowIndex, colIndex, node, e, record, rowNode) {

                        this.fireEvent('itemclick', this, 'addReqDoc', grid, rowIndex, colIndex, record, node);
                    }
                }]
            }, {
                header: '',
                xtype: 'actioncolumn',
                width: 50,
                align : 'center',
                items: [{
                    getClass: function(v, meta, rec) {
                        if(rec.get('viewfile') == 'view') {
                            return 'icon-checked-xsmall';
                        }
                        else{
                            return '';
                        }

                    },
                    handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {

                        this.fireEvent('itemclick', this, 'viewReqDoc', grid, rowIndex, colIndex, record, node);
                    }
                }]
            }]

        });


        this.callParent(arguments);
    }

});







