Ext.define('tourism.view.reqtouroperator.ReqTourOptDocWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.reqtouroperator-docreq-window',

    requires: [

        'tourism.view.reqtouroperator.ReqTourOptDocForm'
    ],

    initComponent: function() {


        Ext.apply(this, {
            title:'เอกสาร',
            autoShow: true,
            modal: true,
            items :[
                {
                    // title: 'บันทึกข้อมูลการพักใช้',
                    xtype: 'reqtouroperator-docreq-form',
                    id: 'reqtouroperator-'+this.roleAction+'-'+this.traderType+'-docreq-form',
                    border: false,
                    frame: false,
                    roleAction: this.roleAction,
                    traderType: this.traderType

                }
            ]
        });

        this.callParent(arguments);
    }

});
