Ext.define('tourism.view.tourleader.registration.TourleaderPanel', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border'//,
        //'tourism.view.tourleader.registration.TourleaderRegistrationFromSearch'
        //'tourism.view.business.registration.RegistrationGrid'
    ],
    
    xtype: 'tourleader-registrationPanel',
    layout: {
        type: 'border'
    },
    items: [{
        region: 'north',
        xtype: 'panel',
        frame: false,
        border: false,
        items:[{
            title:'ค้นหาข้อมูลการทำรายการผู้นำเที่ยว',
            collapsible: true,   // make collapsible
            xtype: 'tourleader_registrationformsearch',
            id: 'tourleader-registration-formsearch-key',
            // padding: '5px 5px 0px 5px',
            frame: false,
            border: false
        }]
    },{
        region: 'center',
        xtype: 'panel',
        layout: 'fit',
        frame: false,
        border: false,
        items:[{
            title: 'การทำรายการใบอนุญาตผู้นำเที่ยว',
            xtype: 'tourleader_registrationgrid',
            // padding: '0px 5px 5px 5px',
            autoScroll: true,
            border: false
        }]
    }]
});