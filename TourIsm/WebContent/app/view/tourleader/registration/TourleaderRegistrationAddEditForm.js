Ext.define('tourism.view.tourleader.registration.TourleaderRegistrationAddEditForm',{
	extend: 'Ext.form.Panel',
    requires: [
       'Ext.data.*',
       'Ext.form.*',
       'Ext.tab.Tab',
       'tourism.view.tourleader.registration.registrationTab.address.TraderAddressGrid',
       'tourism.view.tourleader.registration.registrationTab.tourleaderType.TourleaderTypeAddEditForm',
       'tourism.view.window.registration.registrationTab.educationAndLanguage.LanguageGrid',
       'tourism.view.window.registration.registrationTab.educationAndLanguage.EducationGrid'
       
               
    ],
    xtype: 'form-tourleader-registration-addedit',
    bodyPadding: 5,
    autoScroll: true,
    initComponent: function(){
        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
        
        Ext.apply(this,{
        	width: '100%',
            height: '100%',
            fieldDefaults: {
                labelAlign: 'left',
                msgTarget: 'qtip'
            },
            
            items:[
            {
            	xtype: 'container',
            	layout: 'anchor',
            	defaults: 
            	{
                    anchor: '100%'
                },
                
                items:[
                {
            	   xtype: 'container',
                   layout: 'hbox',
                   items:[
                   {
                	   xtype: 'hiddenfield',
                	   name: 'regId'
                   },{
                	   xtype: 'hiddenfield',
                	   name: 'registrationType'
                   },
                   {
                       xtype: 'hiddenfield',
                       name: 'traderId'
                   },
                   {
                       xtype: 'hiddenfield',
                       name: 'traderType'
                   },{
                	   xtype: 'hiddenfield',
                	   name: 'personId'
                   },{
                       xtype: 'hiddenfield',
                       name: 'imageFile'
                   },{
                       xtype: 'hiddenfield',
                       name: 'fullAddress'
                   },{
                	   xtype: 'textfield',
                       name: 'registrationNo',
                       fieldLabel: 'เลขที่รับเรื่อง',
//                       labelAlign: 'right',
                       flex: 1,
                       allowBlank: false
                   },{
                            xtype: 'splitter'
                        },{
                	   xtype: 'datefield',
                	   name: 'registrationDate',
                	   fieldLabel: 'วันที่รับเรื่อง',
//                	   labelAlign: 'right',
                	   flex: 1,
//                       format: 'd/m/Y',
                	   format: 'd/m/B',
                       allowBlank: false
                   },{
                            xtype: 'splitter'
                        },{
                       xtype: 'textfield',
                       name: 'authorityName',
                       fieldLabel: 'ผู้รับเรื่อง',
//                       labelAlign: 'right',
                       flex: 1,
                       allowBlank: false
                   }
                   ]
                
                },{
                            xtype: 'splitter'
                },{
                	xtype: 'container',
                    layout: 'hbox',
                    items:[
					{
						xtype: 'fieldset',
						flex:1,
					    border: false,
					    defaults: {
					     anchor: '100%',
					     labelWidth: 150
					    },
					    items: [
							{
								xtype: 'fieldcontainer',
         			    	   labelAlign: 'left',
         			    	  fieldLabel: 'บัตรประจำตัวเลขที่',
         			    	   layout: 'hbox',
         			    	   		items: [{

        							    xtype: 'textfield',
        							    name: 'identityNo',
        							    maxLength: 13,
                                       minLength: 13,
                                        enforceMaxLength: true,
                                        maskRe: /\d/,
        			                    allowBlank: false
        			                    ,tabIndex : 1
        			                    
        			                    //Oat Add 02/02/58
        			                    ,listeners: {
        			                        blur: function(value){
        			                     	   var form = Ext.create('Ext.form.Panel');
        			                     	  
        			                     	  var fthis = this.up('form'); 
        			                     	   var identityNo = this.getValue();
        			                     	   
        			                     	   console.log(identityNo);
        			 
        			                     	   form.load({
        			                                url: 'business/registration/checkidentityNoTourleader/verification',
        			                                method : 'POST',
        			                                model: 'tourism.model.registration.RegistrationModel',
        			                                params: {identityNo:identityNo},
        			                                success: function(form, action) {
        			                             	   
        			                                 var dataObj = action.result.data;
        			                                 var title = fthis.getForm().findField('statusCheckIdentityNo');
//        			                                 var bshow = Ext.getCmp('button-show');
        			                              			                                 
        			                                 if(dataObj.check == 'true')
        			                                 {
        			                                	 title.setValue('<span style="color:red;font-weight:bold">เลขบัตรประจำตัวซำ้</span>');
        				                                 this.setValue("");
        			                                 }else{
        			                                	title.setValue('<span style="color:green;font-weight:bold">ผ่านการตรวจสอบ</span>');  
        			                               }
        			                               
        			                                },
        			                                failure: function(form, action) {
        			                                  Ext.Msg.alert('เกิดข้อผิดพลาด', action.result ? action.result.message : 'No response');
        			                                },
        			                                scope: this 
        			                            }); 	   
        			                        }
        			                     }
        			                    //End Oat Add
             			    	   	},
             			    	   	{
             			    	   		xtype: 'splitter'
             			    	   	},
             			    	   	{
             			    	   		xtype: 'button',
         			    	   			iconCls: 'icon-search-user-xsmall',
         			                    flex: 1,
         			                    action: 'searchUserFromIDCardOnline'
             			    	   	}
         			    	   	]
         			    	 
			                    
							}
							//Oat Add 02/02/58
							,{
			                	xtype: 'displayfield',
			                    name: 'statusCheckIdentityNo',
			                    labelWidth: 170,
			                  
			                }
							//End Oat Add
							,{
								// ชื่อ
	                            xtype: 'fieldcontainer',
	                            labelAlign: 'left',
	                            fieldLabel: 'ชื่อ(ภาษาไทย)',
	                            layout: 'hbox',
	                            items: [
	                                {
	                                    xtype: 'masprefixcombo',
	                                    prefixType: 'I',
	                                    name: 'prefixId',
	                                    store: 'tourism.store.combo.MasPrefixStore',
	                                    queryMode: 'remote',
	                                    displayField: 'prefixName',
	                                    valueField: 'prefixId',
	                                    hiddenName: 'prefixId',
	                                    triggerAction: 'all',
	                                    flex: 1,
	    			                    allowBlank: false
	    			                    ,tabIndex : 3
	                                }, {
	                                    xtype: 'splitter'
	                                }, {
	                                    xtype: 'textfield',
	                                    name: 'firstName',
	                                    flex: 2,
	                                    allowBlank: false
	                                    ,tabIndex : 4
	                                }
	                            ]
							},{
								// ชื่อภาษาอังกฤษ
	                            xtype: 'fieldcontainer',
	                            labelAlign: 'left',
	                            fieldLabel: 'ชื่อ(ภาษาอังกฤษ)',
	                            layout: 'hbox',
	                            items: [
	                                {
	                                    xtype: 'masprefixcombo',
	                                    prefixType: 'I',
	                                    name: 'prefixIdEn',
	                                    store: 'tourism.store.combo.MasPrefixStore',
	                                    queryMode: 'remote',
	                                    displayField: 'prefixNameEn',
	                                    valueField: 'prefixId',
	                                    hiddenName: 'prefixId',
	                                    triggerAction: 'all',
	                                    flex: 1,
	    			                    allowBlank: false
	    			                    ,tabIndex : 6
	                                }, {
	                                    xtype: 'splitter'
	                                }, {
	                                    xtype: 'textfield',
	                                    name: 'firstNameEn',
	                                    flex: 2,
	                                    allowBlank: false
	                                    ,tabIndex : 7
	                                }
	                            ]
							},{
	                            xtype: 'radiogroup',
	                            name: 'gendergroup',
	                            fieldLabel: 'เพศ',
	                            // Arrange checkboxes into two columns, distributed vertically
	                            columns: 2,
	                            vertical: true,
	                            items: [
	                                { boxLabel: 'ชาย',  name: 'gender', inputValue: 'M', checked: true ,tabIndex : 9},
	                                { boxLabel: 'หญิง',  name: 'gender', inputValue: 'F' ,tabIndex : 10} 
	                            ]
	                        },{
	                        	 xtype: 'datefield',
	                             fieldLabel: 'เกิดวันที่',
	                             name: 'birthDate',
//	                             format: 'd/m/Y',
	                             format: 'd/m/B',
				                 allowBlank: false
				                 ,tabIndex : 11
	                        }
	                        ,{
                                xtype: 'combo',
                                name: 'provinceId',
                                store: 'tourism.store.combo.MasProvinceStore',
                                fieldLabel: 'ออกให้ ณ จังหวัด',
                                queryMode: 'remote',
                                queryParam: 'provinceName',
                                displayField: 'provinceName',
                                valueField: 'provinceId',
                                hiddenName: 'provinceId',
                                triggerAction: 'all',
                                flex: 1,
			                    allowBlank: false
			                    ,tabIndex : 13
			                    ,listeners:{
			                        scope: this,
			                        'select': function(combo, records){
			                        	
			                        	var masamphurCombo = this.getForm().findField('amphurId');

			                            if(!Ext.isEmpty(masamphurCombo))
			                            {
			                                masamphurCombo.clearValue();
			                                masamphurCombo.setRawValue("");
			                                masamphurCombo.getStore().load({
			                                    params: {provinceId: combo.getValue()},
			                                    callback: function(records, operation, success) {
			                                        // //console.log(success);
			                                    },
			                                    scope: this
			                                }); 
			                            }
			                        	
//			                        	this.getForm().findField('amphurId').clearValue();
//			                        	this.getForm().findField('amphurId').setRawValue("");
			                        }
			                    }
                            }
	                        ,{
	                        	 xtype: 'datefield',
	                             fieldLabel: 'หมดอายุวันที่',
	                             name: 'identityNoExpire',
//	                             format: 'd/m/Y',
	                             format: 'd/m/B',
				                 allowBlank: false
				                 ,tabIndex : 15
	                        }
					    ]
					},{
						xtype: 'fieldset',
					    flex:1,
					    border: false,
					    defaults: {
					     anchor: '100%',
					     labelWidth: 170
					    },
					    items: [
							{
							    xtype: 'textfield',
							    fieldLabel: 'หมายเลขหนังสือเดินทางเลขที่',
							    name: 'passportNo',
							    maxLength: 10,
                               minLength: 7,
                                enforceMaxLength: true,
                                // maskRe: /\d/,
			                    allowBlank: false
			                    ,tabIndex : 2
							}
							//Oat Add 02/02/58
							,{
			                	xtype: 'displayfield',
			                    name: 'xxx',
			                    labelWidth: 170,
			                  
			                }
							//End Oat Add
	                        ,{
	                            // นามสกุล(ภาษาไทย)
	                            xtype: 'textfield',
	                            fieldLabel: 'นามสกุล(ภาษาไทย)',
	                            name: 'lastName',
	                            labelAlign: 'left',
			                    allowBlank: false
			                    ,tabIndex : 5
	                        },
	                        {
	                            // นามสกุล(ภาษาอังกฤษ)
	                            xtype: 'textfield',
	                            fieldLabel: 'นามสกุล(ภาษาอังกฤษ)',
	                            name: 'lastNameEn',
	                            labelAlign: 'left',
			                    allowBlank: false
			                    ,tabIndex : 8
	                        },
	                        {
	                            // นามสกุล(ภาษาอังกฤษ)
	                            xtype: 'textfield',
	                            fieldLabel: 'สัญชาติ',
	                            name: 'personNationality',
	                            labelAlign: 'left',
//	                            value: 'ไทย',
			                    allowBlank: false
			                    // ,tabIndex : 9
	                        }
	                        ,{xtype: 'splitter'},
	                        {
	                            // อายุ
	                        	xtype: 'fieldcontainer',
	                            layout: 'hbox',
	                            items:[
	                            {
		                            xtype: 'textfield',
		                            labelWidth: 170,
		                            width: 350,
		                            fieldLabel: 'อายุ',
		                            name: 'ageYear',
		                            labelAlign: 'left',
				                    allowBlank: false
				                    ,tabIndex : 12
	                            },{xtype: 'splitter'},{xtype: 'splitter'},
                                {
	                            	 xtype: 'label',
			                         text: 'ปี',
			                         labelAlign: 'left'
	                            }
	                            ]
	                        },{
                                xtype: 'combo',
                                name: 'amphurId',
                                store: 'tourism.store.combo.MasAmphurStore',
                                fieldLabel: 'อำเภอ/เขต/สังกัด',
                                queryMode: 'remote',
                                queryParam: 'amphurName',
                                displayField: 'amphurName',
                                valueField: 'amphurId',
                                hiddenName: 'amphurId',
                                triggerAction: 'all',
                                flex: 1,
			                    allowBlank: false
			                    ,tabIndex : 14
			                    ,listeners: {
			                    	beforequery : {
			                    		scope: this
				                        ,fn: function( queryPlan, eOpts)
				                        {
					                        // queryPlan.provinceId = 10;
					                        var masprovinceCombo = this.getForm().findField('provinceId');//Ext.getCmp(this.masprovinceComboId);
					                        
					                        if(!Ext.isEmpty(masprovinceCombo.getValue()))
					                        { 
					                            queryPlan.combo.store.proxy.extraParams = {
					                              provinceId: masprovinceCombo.getValue()
					                            }; 
					                        }
				                                                                      
				                        }
				                    }
			                    }
	                        
                            },{
                                    
                                 xtype: 'fieldset',
                                 itemId: 'imageWrapper',
                                 margin: '0 0 0',
                                 layout: 'anchor',
                                 items: [{
                                    xtype: 'image',
                                    itemId: 'imageFile',
                                    margin: '0 90px 0',
                                    listeners:{
                                        afterrender:function(img, a, obj){
                                            img.getEl().dom.style.width = '130px'; 
                                            img.getEl().dom.style.height = '150px'; 
                                        }
                                    }
                                 }]
                            }
					    ]
					}
                    ]
                
                }
                ]
                
            },{
            	xtype: 'tabpanel',
            	plain: true,
                frame: false,
                border: false,
                items: [
                {
                	title: 'ที่อยู่',
                	layout: 'fit',
                    frame: false,
                    border: false,
                    items: [
                    {
                    	xtype: 'tourleader_registration_registrationTab_traderaddressgrid',
                    	frame: false,
                		border: false
                    }
                    ]
                },{
                	title: 'ประเภทการจดทะเบียน',
                	layout: 'fit',
                	frame: false,
                	border: false,
                	items: [
                	{
                		xtype: 'tourleader_registration_registrationTab_tourleadertypeaddedit_form',
                		frame: false,
                		border: false
                	}
                	]
                },{
                	title: 'การศึกษาและภาษา',
                	layout: 'anchor',
                	frame: false,
                	border: false,
                	items: [
                	{
                		xtype: 'window_registration_registrationTab_education_grid'
                	}
                	,
                	{xtype: 'splitter'},{xtype: 'splitter'},
                	{
                		xtype: 'window_registration_registrationTab_language_grid'
                    }
                	]
                }
                
                ]
            }
		    ],
            buttons: [{
                text: 'บันทึก',
                scope: this,
                action: 'saveTourleaderRegistration'
//                handler: this.onCompleteClick
            },
            {
                text: 'ค้นหาข้อมูลจากบัตรประชาชน',
                scope: this,
                action: 'loadDataFromIdCard'
            }]    
        
        });
        this.callParent();
    },
    
    onResetClick: function(){
        this.getForm().reset();
    },
    
    afterRender: function()
    {
	    this.callParent(arguments);
	    //add registration
        if(!Ext.isEmpty(this.registrationModel) )
        {
            this.loadRecord(this.registrationModel);
    	    console.log(this.registrationModel);  
    	    
    	    //Oat Add 22/09/57
    	    var model = this.registrationModel;
    	    if(Ext.isEmpty(model.get('personNationality')))
    	    {
//    	    	var freg = this.down('form');
    	    	var personNationalityTxt = this.getForm().findField('personNationality');
    	    	personNationalityTxt.setValue("ไทย");
    	    	
    	    }
    	    //
    	    
        }
        
        //Oat Add 13/10/57
        var form = this;
		
		 var preFixId = form.getForm().findField('prefixId');
		 preFixId.addListener("blur" ,function(textf, eOpts){
		 	var values  =  textf.up('form').getValues();
		 	var preFixIdEn = textf.up('form').getForm().findField('prefixIdEn');
			
			if((!Ext.isEmpty(preFixId)))
		 	{
		 		preFixIdEn.setValue(values.prefixId);
		 	}
		 },this);
		 //
        
        
//	    this.loadRecord(this.registrationModel); 
    }
    
});





