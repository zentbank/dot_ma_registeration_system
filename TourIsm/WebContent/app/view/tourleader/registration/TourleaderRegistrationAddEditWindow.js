Ext.define('tourism.view.tourleader.registration.TourleaderRegistrationAddEditWindow',{
	extend: 'Ext.window.Window',
	alias: 'widget.tourleader_registrationaddedit_window',
	requires: [
	        'tourism.view.tourleader.registration.TourleaderRegistrationAddEditForm'
	],	
	title: 'จดทะเบียนใบอนุญาตใหม่',
	layout: 'fit',
	width: 750,
	height: 750,
	//True to make the window modal and mask everything behind it when displayed
    modal: true,
    
    initComponent: function(){

        var formPanel = {
                    xtype: 'form-tourleader-registration-addedit',
                    id: 'key-addedit-tourleader-registration-form',
                    frame: false,
                    border: false,
                    registrationModel: this.registrationModel
        };

        if(this.roleAction != 'key')
        {
          formPanel = {
                    xtype: 'form-tourleader-registration-addedit',
                    frame: false,
                    id: this.roleAction+'-'+this.traderType+'-registration-addedit',
                    border: false
                };
        }
        
        this.items = [
                formPanel 
        ];
    	this.callParent(arguments);
    },
    loadDataModel: function(model)
    {
    	// if(model && (model instanceof tourism.model.business.registration.BusinessRegistrationModel)){
            
            var freg = this.down('form');
            freg.loadRecord(model);

            //Image
            var imageFieldSet = this.down('image');
            // console.log(model);
            imageFieldSet.setSrc("data:image/png;base64,"+model.get("imageFile"));

            //Province
            if(!Ext.isEmpty(model.get('provinceId')))
            {
              var provinceCombo = freg.getForm().findField('provinceId');
              provinceCombo.getStore().on('load',function(store, records, successful, eOpts){
                provinceCombo.setValue(model.get('provinceId'));
              },this,{single:true});
              provinceCombo.getStore().load();
            }
            else
            {
              var provinceCombo = freg.getForm().findField('provinceId');
              provinceCombo.clearValue();
            }
            
            //Amphur
            if(!Ext.isEmpty(model.get('amphurId')))
        	{
        	  var amphurCombo = freg.getForm().findField('amphurId'); 
        	  amphurCombo.getStore().on('load',function(){
        		  
        	  },this,{single:true});
        	  amphurCombo.getStore().load();
            }
            else
        	{
            	var amphurCombo = freg.getForm().findField('amphurId'); 
            	amphurCombo.clearValue();
        	}

            //Prefix
            if(!Ext.isEmpty(model.get('prefixId')))
            {
              var prefixIdCombo = freg.getForm().findField('prefixId');
              var prefixIdEnCombo = freg.getForm().findField('prefixIdEn');
              prefixIdCombo.getStore().on('load',function(store, records, successful, eOpts){
                prefixIdCombo.setValue(model.get('prefixId'));
                prefixIdEnCombo.setValue(model.get('prefixId'));
              },this,{single:true});
              prefixIdCombo.getStore().load();
            }
            else
            {
              var prefixIdCombo = freg.getForm().findField('prefixId');
              var prefixIdEnCombo = freg.getForm().findField('prefixIdEn');
              prefixIdCombo.clearValue();
              prefixIdEnCombo.clearValue();
            }
            
            //ที่อยู่
            var traderaddressgrid = Ext.ComponentQuery.query('tourleader_registration_registrationTab_traderaddressgrid')[0];
            
            traderaddressgrid.getStore().load({
                params: {traderId: model.get('traderId'), recordStatus: model.get('traderRecordStatus')},
                callback: function(records, operation, success) {
                	//console.log(records);
                
                },
                scope: this
            }); 
            
            //ประเภทการจดทะเบียน
        	if(model.get('traderCategory') == '200')
    		{
    		  var traderGuideDetail = model.get('traderGuideDetail');
    		  var label200 = Ext.getCmp('tourleader_registration_tourleaderType_200_html');
  			  label200.setText(traderGuideDetail);
//                		  freg.getForm().findField('label200').setText(traderGuideDetail);
    		}
        	if(model.get('traderCategory') == '300')
    		{
    		  var traderTrainedDetail = model.get('traderTrainedDetail');
    		  var label300 = Ext.getCmp('tourleader_registration_tourleaderType_300_html');
  			  label300.setText(traderTrainedDetail);
    		}
        	
        	//การศึกษา
        	var educationgrid = Ext.ComponentQuery.query('window_registration_registrationTab_education_grid')[0];
        	educationgrid.getStore().load({
                params: {personId: model.get('personId'), educationType: 'E', recordStatus: model.get('traderRecordStatus')},
                callback: function(records, operation, success) 
                {
                	//console.log(records);                            
                },
                scope: this
            });
        	
        	//ภาษา
        	var languagegrid = Ext.ComponentQuery.query('window_registration_registrationTab_language_grid')[0];
        	languagegrid.getStore().load({
                params: {personId: model.get('personId'), recordStatus: model.get('traderRecordStatus')},
                callback: function(records, operation, success) {

                	
                	//console.log(records);                            
                },
                scope: this
            });
        	
        	
    	// }    
    }
});
