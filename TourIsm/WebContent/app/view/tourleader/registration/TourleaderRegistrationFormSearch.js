Ext.define('tourism.view.tourleader.registration.TourleaderRegistrationFormSearch' ,{
    extend: 'Ext.form.FormPanel',
    alias : 'widget.tourleader_registrationformsearch',
    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.Time',
        'Ext.form.CheckboxGroup',
        'Ext.layout.container.HBox',
        'Ext.form.FieldSet',
        'Ext.form.FieldContainer'
    ],
    bodyPadding: 10,
    // width: '100%',
    items: 
    [
        {
            xtype: 'container',
            // title: 'Payment',
            layout: 'anchor',
            defaults: {
                anchor: '100%',
                labelWidth: 150
            },
            items: [
                {
                    xtype: 'hidden',
                    name: 'traderType',
                    value: 'L'
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'เลขที่รับเรื่อง',
                    name: 'registrationNo',
                    anchor: '50%'
                },
                                    {
                        xtype: 'container',
                        layout: 'hbox',
                        layoutCongig: {
                             pack:'center',
                             align:'middle'
                        },
                        items: [
                            {
                                xtype: 'container',
                                // title: 'Payment',
                                flex: 1,
                                layout: 'anchor',
                                defaults: {
                                    anchor: '100%',
                                    labelWidth: 150
                                },
                                items: [
                                    {
                                        xtype: 'datefield',
                                        fieldLabel: 'วันที่รับเรื่อง',
                                        // labelWidth: 150,
                                        name: 'registrationDateFrom',
                                        format: 'd/m/B'
                                    },
                                    {
                                        xtype: 'textfield',
                                         fieldLabel: 'ชื่อ',
                                        name: 'firstName',
                                        allowBlank: true
                                    },
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'บัตรประจำตัวเลขที่',
                                        name: 'identityNo'
                                    },
                                    {
                                        xtype: 'combo',
                                        fieldLabel: 'ประเภทการทำรายการ',
                                        store: 'tourism.store.combo.TourleaderRegistrationTypeStore',
                                        queryMode: 'local',
                                        displayField: 'registrationTypeName',
                                        valueField: 'registrationType',
                                        hiddenName: 'registrationType',
                                        name: 'registrationType'
                                    }
                                ]
                            },{
                                xtype: 'splitter'
                            },
                            {
                                xtype: 'container',
                                flex: 1,
                                layout: 'anchor',
                                defaults: {
                                    anchor: '100%',
                                    labelWidth: 150
                                },
                                items: [
                                    {   
                                        xtype: 'datefield',
                                        fieldLabel: 'ถึงวันที่',
                                        
                                        name: 'registrationDateTo',
                                        flex: 1,
                                        format: 'd/m/B'
                                        // ,value: Ext.Date.add(new Date(), Ext.Date.YEAR, 543), //2554 ณ ปัจจุบัน
                                       
                                    },
                                     {
                                        // นามสกุล
                                        xtype: 'textfield',
                                        fieldLabel: 'นามสกุล',
                                        name: 'lastName',
                                        labelAlign: 'left'
                                    },
                                     {
                                        xtype: 'combo',
                                        fieldLabel: 'ประเภทผู้นำเที่ยว',
                                        store: 'tourism.store.combo.TourleaderCategoryStore',
                                        queryMode: 'local',
                                        displayField: 'traderCategoryName',
                                        valueField: 'traderCategory',
                                        hiddenName: 'traderCategory',
                                        name :'traderCategory'
                                    },
                                     {
                                        xtype: 'combo',
                                        fieldLabel: 'สถานะเรื่อง',
                                        store: 'tourism.store.combo.RegKeyProgressStatusStore',
                                        queryMode: 'local',
                                        displayField: 'regProgressStatusName',
                                        valueField: 'regProgressStatus',
                                        hiddenName: 'regProgressStatus',
                                        name: 'regProgressStatus'
                                    }
                                ]
                            }
                        ]
                    }
            ]
        },
        {
            xtype: 'toolbar',
            border: false,
            padding: '6px 0 6px 0px',
            items: [{
                xtype: 'button',
                text : 'ค้นหา',
                action: 'searchTraderBetweenRegistration'
                // iconCls: 'icon-search',
//                itemId: 'search-business-registrationfromsearch-btn',
//                handler: function(btn, evt){
//                }  
            }]
        }

    ]

});    




