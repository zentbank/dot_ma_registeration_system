Ext.define('tourism.view.tourleader.registration.registrationTab.address.TraderAddressAddEditWindow',{
	extend: 'Ext.window.Window',
	alias: 'widget.tourleader_registration_address_addedit_window',
	requires: [
	  'tourism.view.tourleader.registration.registrationTab.address.TraderAddressAddEditForm'
	],
	title: 'เพิ่มแก้ไขข้อมุลที่อยู่',
	layout: 'fit',
    autoShow: true,
    width: 600,
    height: 500,
    //True to make the window modal and mask everything behind it when displayed
    modal: true,
    
    initComponent: function(){
    	this.items = [
    	{
    		xtype: 'tourleader_registration_address_traderaddressaddedit_form',
    		frame: false,
            border: false
    	}
    	];
    	
    	this.callParent(arguments);
    }

	
});









