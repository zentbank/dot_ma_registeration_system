Ext.define('tourism.view.tourleader.registration.registrationTab.tourleaderType.TourleaderTypeAddEditForm',{
	extend: 'Ext.form.Panel',
	alias: 'widget.tourleader_registration_registrationTab_tourleadertypeaddedit_form',
	
	initComponent: function(){
		Ext.apply(this,{
			width: '100%',
	        height: '100%',
	        fieldDefaults: {
	            labelAlign: 'left',
	            // labelWidth: 50,
	            msgTarget: 'qtip'
	        },
	
	        items:[
	        {
	        	xtype: 'container',
	        	border: false,
	        	layout: 'anchor',
	        	defaults: {
	        		anchor: '100%'
	        	},
	        	items: [
	        	{
	        		
	        		xtype: 'fieldset',
	                border: false,
	                items: [
					{
						xtype: 'hiddenfield',
	                	name: 'eduId'
					}, 
					{
						xtype: 'hiddenfield',
	                	name: 'licenseGuideNo'
					},
					{
						xtype: 'hiddenfield',
	                	name: 'traderGuideId'
					},
					{
						xtype: 'hiddenfield',
	                	name: 'personGuideId'
					},
	               /* {
	                	 xtype: 'radio',
	                	 boxLabel  : 'เป็นผู้นำเที่ยวตามประกาศกฎกระทรวงฯ',
//	                     name      : 'tourleaderType',
	                	 name: 'traderCategory',
	                     inputValue: '100',
	                     action: 'addTourleaderType',
	                     checked: true
//	 	        		 listeners: {
//			        			change: this.onSelectTourleaderType	
//			        	 }
	                },
	                {
	                	 xtype: 'radio',
	                	 boxLabel  : 'เป็นมัคคุเทศก์และมาขอจดทะเบียนผู้นำเที่ยว',
//	                     name      : 'tourleaderType',
	                	 name: 'traderCategory',
	                     inputValue: '200',
	                     action	   : 'addTourleaderType'
//	 	        		 listeners: {
//			        			change: this.onSelectTourleaderType	
//			        	 }
	                },
		        	{
		        		xtype: 'label',
		        		id: 'tourleader_registration_tourleaderType_200_html',
		        		name: 'label200',
		        		text: ''
		        	},*/
		        	{
	                	 xtype: 'radio',
	                	 boxLabel  : 'ผ่านการอบรมจากสถาบันที่สำนักทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ กำหนด',
//	                     name      : 'tourleaderType',
	                	 name: 'traderCategory',
	                     inputValue: '300',
	                     action: 'addTourleaderType'
//	 	        		 listeners: {
//			        			change: this.onSelectTourleaderType	
//			        	 }
	                },
	                {
		        		xtype: 'label',
		        		id: 'tourleader_registration_tourleaderType_300_html',
		        		name: 'label300',
		        		text: ''
                    },
		        	{
	                	 xtype: 'radio',
	                	 boxLabel  : 'จบการศึกษาด้านการท่องเที่ยว',
//	                     name      : 'tourleaderType',
	                	 name: 'traderCategory',
	                     inputValue: '400',
	                     action: 'addTourleaderType'
//	 	        		 listeners: {
//			        			change: this.onSelectTourleaderType	
//			        	 }
	                },
	                {
		        		xtype: 'label',
		        		id: 'tourleader_registration_tourleaderType_400_html',
		        		name: 'label400',
		        		text: ''
                   }
	                ]
	        	}
	        	]
	        }
	        ]
		});
		this.callParent(arguments);
	}//	,
	

	
});










