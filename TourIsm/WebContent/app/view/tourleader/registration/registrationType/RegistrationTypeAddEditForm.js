Ext.define('tourism.view.tourleader.registration.registrationType.RegistrationTypeAddEditForm', {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.data.*',
        'Ext.form.*',
        'Ext.tab.Tab'
    ],
    alias: 'widget.tourleader-registration-registrationtype-addeditform',
    bodyPadding: 5,
    autoScroll: true,

    initComponent: function(){

        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
        
        Ext.apply(this, {
            width: '100%',
            height: '100%',
            fieldDefaults: {
                labelAlign: 'left',
                msgTarget: 'qtip'
            },

            items: 
            [ 
                {
                    xtype: 'container',
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%',
                        labelWidth: 150
                    },
                    items: 
                    [ 
                        {
                            xtype: 'radiogroup',
                            fieldLabel: 'ประเภทการทำรายการ',
                            allowBlank: false,
                            afterLabelTextTpl: required,
                            // Arrange checkboxes into two columns, distributed vertically
                            columns: 1,
                            vertical: true,
                            items: [
                                { boxLabel: 'จดทะเบียนใบอนุญาตผู้นำเที่ยวใหม่',  name: 'registrationType', inputValue: 'N' ,checked: true},
                                { boxLabel: 'ออกใบแทนใบอนุญาตผู้นำเที่ยว',  name: 'registrationType', inputValue: 'T' },
                                { boxLabel: 'เปลี่ยนแปลงรายการใบอนุญาตผู้นำเที่ยว',  name: 'registrationType', inputValue: 'C' }
                            ]
                        }
                    ]
                }
            ],

            buttons: [{
                text: 'ดำเนินการต่อ',
                action: 'selectRegType'
            }]    
        });
        this.callParent();
    }
    ,
    
    onResetClick: function(){
        this.getForm().reset();
    }//,
    
//    onCompleteClick: function(btn){
//        var form = this.getForm();
//        if (form.isValid()) {
//            var win    = btn.up('window');
//            var layout = win.getLayout();
//            layout.setActiveItem(1);
//        }
//    }
});