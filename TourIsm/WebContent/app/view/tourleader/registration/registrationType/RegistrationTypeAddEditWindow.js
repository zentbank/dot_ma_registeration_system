Ext.define('tourism.view.tourleader.registration.registrationType.RegistrationTypeAddEditWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.tourleader-registration-registrationtype-addeditwindow',

    requires: [
        'tourism.view.tourleader.registration.registrationType.RegistrationTypeAddEditForm',
        'tourism.view.tourleader.registration.registrationType.RegistrationTypeSearchBusinessForm'
    ],
    
    initComponent: function() {


        Ext.apply(this, {
            title : 'เลือกประเภทการทำรายการ',
            // layout: 'fit',
            layout: 'card',
            activeItem: 0,
            autoShow: true,
            width: 450,
            height: 400,
            autoShow: true,
            modal: true,
            items :[
                {
                   xtype: 'tourleader-registration-registrationtype-addeditform'
                },
                // {
                //    xtype: 'tourleader-registration-registrationtype-searchbusinessform'
                // }

                {
                   xtype: 'business-registration-registrationtype-searchbusinessform',
                   id: 'tourleader-registration-registrationtype-searchtourleaderform',
                   registerType: 'guide',
                   border: false,
                   frame: false
                }
            ]
        });

        this.callParent(arguments);
    },

});
