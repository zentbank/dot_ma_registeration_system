Ext.define('tourism.view.window.registration.registrationTab.educationAndLanguage.EducationGrid',{
	extend: 'Ext.grid.Panel',
	alias: 'widget.window_registration_registrationTab_education_grid',
//	store: 'tourism.store.grid.EducationStore',
	stripeRows: true,
	requires: ['Ext.grid.*',
	           'Ext.data.*',
	           'Ext.util.*',
	           'Ext.state.*',
	           'Ext.form.*',
	           
	           'tourism.store.combo.MasEducationLevelStore',
	           'tourism.model.combo.MasEducationLevelModel',
	           
	           'tourism.model.combo.MasUniversityModel',
	           'tourism.store.combo.MasUniversityStore',
	           
	           'tourism.store.grid.EducationStore',
	           'tourism.model.grid.EducationModel'
	],
	
	initComponent: function(){
		var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
//			ptype: 'rowediting',
//			pluginId: 'rowediting-grid-packagegrid',
	        clicksToMoveEditor: 1,
	        autoCancel: false,
			listeners: {
				edit: function(editor, context, eOpts) {
					context.grid.getStore().sync();
				}
			}
	    });
		
		var store = Ext.create('tourism.store.grid.EducationStore',{
			  storeId: 'window-registration-education-store'
		  });
		
		var storeEducationLevel = Ext.create('tourism.store.combo.MasEducationLevelStore',{
			  storeId: 'window-registration-educationlevel-store'
		});
		storeEducationLevel.load();
		
		var storeUniversity = Ext.create('tourism.store.combo.MasUniversityStore',{
			  storeId: 'window-registration-university-store'
		});
		storeUniversity.load();
		
		
		Ext.apply(this,{
			store: store,
//			selType : 'rowmodel',
			height: 150,
			 columns:[

			 {
	                header: 'ระดับการศึกษา',
	                dataIndex: 'masEducationLevelId',
	                flex: 2,
	                editor: 
	                {
	                	xtype: 'combo',
	                	id: 'education_grid_maseducationlevel_combo',
	                    typeAhead: true,
	                    triggerAction: 'all',
	                    valueField: 'masEducationLevelId',
	                    displayField: 'educationLevelName',
	                    store: storeEducationLevel,
	                    allowBlank: false
                	},
                	renderer: function(value, metaData, gridmodel, rowIndex,colIndex,gridstore,gridview){
                		var educationLevel = value;
                		storeEducationLevel.each(function(edu){
                			if(edu.get('masEducationLevelId') == value)
                			{
                				educationLevel = edu.get('educationLevelName');
                			}
                		});
				        return educationLevel;
				    }
	         },
	         {
	        	 header: 'สถาบัน',
	             dataIndex: 'masUniversityId',
	             flex: 3,
	             editor: 
	             {
                	xtype: 'combo',
                	id: 'education_grid_masUniversity_combo',
                    typeAhead: true,
                    triggerAction: 'all',
                    valueField: 'masUniversityId',
                    displayField: 'universityName',
                    store: storeUniversity,
                    allowBlank: false
             	},
            	renderer: function(value, metaData, gridmodel, rowIndex,colIndex,gridstore,gridview){
	        		var university = value;
	        		storeUniversity.each(function(uni){
	        			if(uni.get('masUniversityId') == value)
	        			{
	        				university = uni.get('universityName');
	        			}
	        		});
			        return university;
            	} 
	         },
	         {
	             header: 'วุฒิการศึกษา',
	             dataIndex: 'graduationCourse',
	             flex: 2,
	             editor: {
	                 // defaults to textfield if no xtype is supplied
	                 allowBlank: false
	             }
	         },
	         {
	             header: 'สาขาวิชา',
	             dataIndex: 'educationMajor',
	             flex: 2,
	             editor: {
	                 // defaults to textfield if no xtype is supplied
	                 allowBlank: false
	             }
	         },
	         {
	             header: 'ปีที่สำเร็จ',
	             dataIndex: 'graduationYear',
	             flex: 1,
	             editor: {
	                 // defaults to textfield if no xtype is supplied
	                 allowBlank: false
	             }
	         },
			 {
				header: 'ลบ',
                xtype: 'actioncolumn',
                sortable: false,
                menuDisabled: true,
                flex: 1,
                items: [{
//	                    icon: 'resources/images/icons/fam/delete.gif',
                	iconCls: 'icon-delete-xsmall',
                    tooltip: 'ลบข้อมูลระดับการศึกษา',
                    scope: this,
                    handler: this.onRemoveClick
                }]
	          }
			
			 ],
			 
             tbar: [{
                text: 'เพิ่มข้อมูลระดับการศึกษา',
                scope: this,
                handler: function(btn){
            	  // var form = Ext.ComponentQuery.query('form-tourleader-registration-addedit')[0];
            	  var form = btn.up('form');
		    	  var personId = form.getForm().findField('personId').getValue();
                	
                  rowEditing.cancelEdit();
            	// Create a model instance
                  var rec = Ext.create('tourism.model.grid.EducationModel', {
                	personId: personId
                	,masEducationLevelId: ' '
                	,masUniversityId: ' '
                	,graduationCourse: 'วุฒิการศึกษา'
                	,educationMajor: 'สาขาวิชา'
                	,graduationYear: '2556'
                	,educationType: 'E'
                  });
                  
                  store.insert(0, rec);
                  rowEditing.startEdit(0, 0);
                  
                  var universityCombo = Ext.getCmp('education_grid_masUniversity_combo');
                  universityCombo.getStore().on('load',function(store, records, successful, eOpts){
                	  universityCombo.setValue(1);
                    },this,{single:true});
                  universityCombo.getStore().load();
                  

                  var educationLevelCombo = Ext.getCmp('education_grid_maseducationlevel_combo');
                  educationLevelCombo.getStore().on('load',function(){
                	  educationLevelCombo.setValue(1);
                  },this,{single:true});
                  educationLevelCombo.getStore().load();

            	

                }
             }],
             plugins: [rowEditing]
		});
		
		
		this.callParent(arguments);
	},

    onRemoveClick: function(grid, rowIndex){
        this.getStore().removeAt(rowIndex);
        this.getStore().sync();
    }

});







