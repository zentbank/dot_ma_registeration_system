Ext.define('tourism.view.window.registration.registrationTab.educationAndLanguage.LanguageGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.window_registration_registrationTab_language_grid',
//		store: 'tourism.store.grid.ForeignLanguageStore',
		requires: [
			'Ext.grid.Column',
			'tourism.store.grid.ForeignLanguageStore',
			'Ext.toolbar.Paging',
			'Ext.grid.plugin.RowEditing',
			'Ext.form.field.Number',
			'Ext.form.field.Date'
		],
		
		
		initComponent: function(){

			var store = Ext.create('tourism.store.grid.ForeignLanguageStore',{
				  storeId: 'window-registration-foreignlanguage-store',
				  id: 'window-registration-foreignlanguage-store-id'
			  });	

			// store.load();
			
		  var storeLanguage = Ext.create('tourism.store.combo.LanguageStore',{
			  storeId: 'window-registration-language-store'
		  });
		  storeLanguage.load();

			Ext.apply(this,{
				store: store,
//				selType : 'rowmodel',
				height: 200,
				plugins:[
				{
					ptype: 'rowediting',
					pluginId: 'rowediting-grid-packagegrid',
					clicksToEdit: 1,
					autoCancel: false,
					listeners: {
						edit: function(editor, context, eOpts) {
							context.grid.getStore().sync();
						}
					}
				}
				], 
				columns: [	
				
				{
					xtype: 'rownumberer'
				},
				{
	                header: 'ภาษา',
	                dataIndex: 'countryId',
	                flex: 3,
	                editor: 
	                {
	                	xtype: 'combo',
	                	id: 'language_grid_country_combo',
	                    typeAhead: true,
	                    triggerAction: 'all',
	                    valueField: 'countryId',
	                    displayField: 'language',
	                    store: storeLanguage,
	                    allowBlank: false
                	}
                	,
                	renderer: function(value, metaData, gridmodel, rowIndex,colIndex,gridstore,gridview){
                		var language = value;
                		storeLanguage.each(function(lang){
                			if(lang.get('countryId') == value)
                			{
                				language = lang.get('language');
                			}
                		});
				        return language;
				    }
	            }
				, {
	                xtype: 'actioncolumn',
	                header: 'ลบ',
	                sortable: false,
	                menuDisabled: true,
	                flex: 1,
	                items: [{
//	                    icon: 'resources/images/icons/fam/delete.gif',
	                	iconCls: 'icon-delete-xsmall',
	                    tooltip: 'Language-Remove',
	                    scope: this,
	                    handler: this.onRemoveClick
	                }]
	            }
				],
				tbar: [{
					text: 'เพิ่มความสามารถใช้ภาษาต่างประเทศ',
                	scope: this,
                	handler: function(btn){
				        // var form = Ext.ComponentQuery.query('form-tourleader-registration-addedit')[0];
				        var form = btn.up('form');
				    	var personId = form.getForm().findField('personId').getValue();
				    	var traderId = form.getForm().findField('traderId').getValue();
				    	
						grid = btn.up('grid');
				        var  rowEditing = grid.getPlugin('rowediting-grid-packagegrid');
				        rowEditing.cancelEdit();
				        
//				        var rec = Ext.create("tourism.model.grid.ForeignLanguageModel");
				        var rec = Ext.create("tourism.model.grid.ForeignLanguageModel",{
				        	personId: personId
				        	,traderId: traderId
				        	,countryId: ' '
				        });
				        
				        grid.getStore().insert(0, rec);
				        rowEditing.startEdit(0,0);
				        
				        var countryCombo = Ext.getCmp('language_grid_country_combo');
				        countryCombo.getStore().on('load',function(store, records, successful, eOpts){
				        	countryCombo.setValue(41);
		                    },this,{single:true});
				        countryCombo.getStore().load();
				        
	                }
				}
				]
				
			});

			this.callParent(arguments);
		},
		
	    onRemoveClick: function(grid, rowIndex){
	        this.getStore().removeAt(rowIndex);
	        this.getStore().sync();
	    }
		
	});














