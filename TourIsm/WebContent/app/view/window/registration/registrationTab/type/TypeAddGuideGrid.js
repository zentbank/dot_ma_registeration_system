Ext.define('tourism.view.window.registration.registrationTab.type.TypeAddGuideGrid',{
	extend: 'Ext.grid.Panel',
	alias: 'widget.window_registration_typeaddguidesearch_grid',
	stripeRows: true,
	requires: [
	           'Ext.grid.Column',   
	           'Ext.toolbar.Paging',
	     	   'Ext.grid.plugin.RowEditing',
	     	   'Ext.grid.column.Template',
	     	   'Ext.grid.column.Action',
	     	   'Ext.grid.plugin.RowExpander',
	     	   'tourism.store.grid.TraderStore',
	],
	
	initComponent: function(){
		
		var store = Ext.create('tourism.store.grid.TraderStore',{
			  storeId: 'window-registration-typeaddguide-store'
		  });
		
		Ext.apply(this,{
			store: store,
			selType: 'checkboxmodel',
			 columns:[
//			 {
//				text: '',
//				dataIndex: '',
//				hidden: true
//			 },
			 {
				 text: 'เลขที่ใบอนุญาต',
				 dataIndex: 'licenseNo',
				 flex: 1
			 },{
				 text: 'ชื่อ-นามสกุล',
				 dataIndex: 'personFullName',
				 flex: 1
			 },{
				text: 'หมายเลขบัตร',
				dataIndex: 'identityNo',
				flex: 1
			 },{
				 text: 'ประเภทมัคคุเทศก์',
				 dataIndex: 'traderCategoryName',
				 flex: 1
			 }
			 ]
		});
		this.callParent(arguments);
	}
	
	
});














