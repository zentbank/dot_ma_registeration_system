Ext.define('tourism.view.window.registration.registrationTab.type.TypeAddGuideSearchForm',{
	extend: 'Ext.form.Panel',
    requires: [
       'Ext.data.*',
       'Ext.form.*',
       'tourism.view.window.registration.registrationTab.type.TypeAddGuideGrid'
    ],
    xtype: 'window_registration_typeaddguidesearch_form',
    bodyPadding: 5,
    autoScroll: true,
	
	initComponent: function(){
		var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
		
		Ext.apply(this,{
			width: '100%',
	        height: '100%',
	        fieldDefaults: {
	            labelAlign: 'left',
	            msgTarget: 'qtip'
	        },
	
	        items:[
	        {
	        	xtype: 'container',
	        	layout: 'anchor',
	        	defaults: {
	        		anchor: '50%'
	        	},
	        	items: [
	        	{
                  // เลขที่ใบอนุญาต
                  xtype: 'textfield',
                  name: 'licenseNo',
                  fieldLabel: 'เลขที่ใบอนุญาต'
                  ,value: '11-12345'
                }
	        	]
	        },
	        {
	        	xtype: 'button',
	        	text: 'ค้นหา',
	        	action: 'searchTypeGuide'
	        },{xtype: 'splitter'},{xtype: 'splitter'},
	        {
	        	xtype: 'container',
	        	layout: 'anchor',
	        	defaults: {
	        		anchor: '100%'
	        	},
	        	items: [
	        	{
                  xtype: 'window_registration_typeaddguidesearch_grid'
                }
	        	]
	        }
	        ],
	        
	        buttons: [{
                text: 'บันทึก',
                scope: this,
                action: 'saveTypeGuide'
            }] 
		});
		this.callParent(arguments);
	}
	
});















