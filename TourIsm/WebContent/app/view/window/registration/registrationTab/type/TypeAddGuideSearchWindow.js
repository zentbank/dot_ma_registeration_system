Ext.define('tourism.view.window.registration.registrationTab.type.TypeAddGuideSearchWindow',{
	extend: 'Ext.window.Window',
	alias: 'widget.window_registration_typeaddguidesearch_window',
	requires: ['tourism.view.window.registration.registrationTab.type.TypeAddGuideSearchForm'],
	title: 'มัคคุเทศก์',
	layout: 'fit',
	width: 750,
	height: 550,
	modal: true,
	
	initComponent: function(){
		this.items = [
		{
			xtype: 'window_registration_typeaddguidesearch_form',
			frame: false,
            border: false
		}
		];
		
		this.callParent(arguments);
	}
	
});














