Ext.define('tourism.view.window.registration.registrationTab.type.TypeAddTrainGrid',{
	extend: 'Ext.grid.Panel',
	alias: 'widget.window_registration_typeaddtrainsearch_grid',
	stripeRows: true,
	requires: [
	           'Ext.grid.Column',   
	           'Ext.toolbar.Paging',
	     	   'Ext.grid.plugin.RowEditing',
	     	   'Ext.grid.column.Template',
	     	   'Ext.grid.column.Action',
	     	   'Ext.grid.plugin.RowExpander',
	     	   'tourism.store.grid.PersonTrainedStore',
	],
	
	initComponent: function(){
		
		var store = Ext.create('tourism.store.grid.PersonTrainedStore',{
			  storeId: 'window-registration-typeaddtrain-store'
		  });
		
		Ext.apply(this,{
			store: store,
			selType: 'checkboxmodel',
			mode: 'SINGLE',
			 columns:[
//			 {
//                xtype: 'checkcolumn',
//                header: '',
//                dataIndex: 'eduId',
//                width: 40,
//                stopSelection: false
//             },
			 {
				header: '',
				dataIndex: 'eduId',
				hidden: true
			 },
             {
				 text: 'ชื่อ-นามสกุล',
			     dataIndex: 'firstName',
			     flex: 2,
				 renderer: function(value, metaData, model)
				 {
                    
                    return model.get('prefixName')+model.get('firstName')+' '+model.get('lastName');
				 }
			 },{
				 text: 'ชื่อหลักสูตร',
				 dataIndex: 'graduationCourse',
				 flex: 2
			 },{
				 text: 'รุ่นที่',
				 dataIndex: 'generationGraduate',
				 width: 40,
			 },{
				text: 'สถาบัน',
				dataIndex: 'universityName',
				flex: 2
			 },{
				 text: 'วันที่อบรม',
				 dataIndex: 'studyDate',
				 flex: 1
			 },{
				 text: 'ถึงวันที่',
				 dataIndex: 'graduationDate',
				 flex: 1
			 }
			 ]
		});
		this.callParent(arguments);
	}
	
	
});














