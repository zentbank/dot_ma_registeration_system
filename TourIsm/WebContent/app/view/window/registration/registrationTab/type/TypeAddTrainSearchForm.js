Ext.define('tourism.view.window.registration.registrationTab.type.TypeAddTrainSearchForm',{
	extend: 'Ext.form.Panel',
    requires: [
       'Ext.data.*',
       'Ext.form.*',
       'tourism.view.window.registration.registrationTab.type.TypeAddTrainGrid'
    ],
    xtype: 'window_registration_typeaddtrainsearch_form',
    bodyPadding: 5,
    autoScroll: true,
	
	initComponent: function(){
		var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
		
		Ext.apply(this,{
			width: '100%',
	        height: '100%',
	        fieldDefaults: {
	            labelAlign: 'left',
	            msgTarget: 'qtip'
	        },
	
	        items:[
	        {
	        	xtype: 'container',
	        	layout: 'anchor',
	        	defaults: {
	        		anchor: '50%'
	        	},
	        	items: [
	        	{
                  // หมายเลขบัตรประชาชน
                  xtype: 'textfield',
                  name: 'identityNo',
                  fieldLabel: 'หมายเลขบัตรประชาชน',
                  labelWidth: 150
                },
                {
                    xtype: 'combo',
                    id : 'window-registration-type-masuniversity-combo',
                    name: 'masUniversityId',
                    fieldLabel: 'สถาบัน',
                    labelWidth: 150,
                    store: 'tourism.store.combo.MasUniversityStore',
                    queryMode: 'remote',
                    displayField: 'universityName',
                    valueField: 'masUniversityId',
                    hiddenName: 'masUniversityId',
                    triggerAction: 'all',
                    flex: 1,
                    // afterLabelTextTpl: required,
                    allowBlank: true
                },{
             	   xtype: 'datefield',
            	   name: 'studyDate',
            	   fieldLabel: 'วันที่อบรม',
            	   labelWidth: 150,
            	   flex: 1,
                   format: 'd/m/B'
//                   afterLabelTextTpl: required,
//                   allowBlank: false
               },{
             	   xtype: 'datefield',
            	   name: 'graduationDate',
            	   fieldLabel: 'ถึงวันที่',
            	   labelWidth: 150,
            	   flex: 1,
                   format: 'd/m/B'
//                   afterLabelTextTpl: required,
//                   allowBlank: false
               }
               ]
	        },
	        {
	        	xtype: 'button',
	        	text: 'ค้นหา',
	        	action: 'searchTypeTrain'
	        },{xtype: 'splitter'},{xtype: 'splitter'},
	        {
	        	xtype: 'container',
	        	layout: 'anchor',
	        	defaults: {
	        		anchor: '100%'
	        	},
	        	items: [
	        	{
                  xtype: 'window_registration_typeaddtrainsearch_grid'
                }
	        	]
	        }
	        ],
	        
	        buttons: [{
                text: 'บันทึก',
                scope: this,
                action: 'saveTypeTrain'
            }] 
		});
		this.callParent(arguments);
	}
	
});















