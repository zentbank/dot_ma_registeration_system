Ext.define('tourism.view.window.registration.registrationTab.type.TypeAddTrainSearchWindow',{
	extend: 'Ext.window.Window',
	alias: 'widget.window_registration_typeaddtrainsearch_window',
	requires: ['tourism.view.window.registration.registrationTab.type.TypeAddTrainSearchForm'],
	title: 'การฝึกอบรม',
	layout: 'fit',
	width: 750,
	height: 550,
	modal: true,
	
	initComponent: function(){
		this.items = [
		{
			xtype: 'window_registration_typeaddtrainsearch_form',
			frame: false,
            border: false
		}
		];
		
		this.callParent(arguments);
	}
	
});