Ext.define('tourism.view.window.verification.CheckDocumentForm', {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.data.*',
        'Ext.form.*',
        'Ext.tab.Tab'
    ],
    xtype: 'verification-reg-document-check-doc-form',
    bodyPadding: 5,
    autoScroll: true,

    initComponent: function(){
        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

        Ext.apply(this, {
            width: '100%',
            height: '100%',
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },
            items: 
            [
                {
                    xtype: 'hiddenfield',
                    name: 'traderId'
                },  {
                    xtype: 'hiddenfield',
                    name: 'masDocId'
                }, 
                {
                    xtype: 'hiddenfield',
                    name: 'regDocId'
                }, 
                {
                    xtype: 'displayfield',
                    name: 'documentName'
                },
                {
                    xtype: 'radiogroup',
                    fieldLabel: 'ผลการตรวจสอบ',
                    allowBlank: false,
                    afterLabelTextTpl: required,
                    columns: 1,
                    vertical: false,
                    items: [
                        { boxLabel: 'เอกสารถูกต้อง',  name: 'docStatus', inputValue: '1' , checked: true},
                        { boxLabel: 'เอกสารไม่ถูกต้อง',  name: 'docStatus', inputValue: '0' }
                    ]
                 },
                 {
                    xtype     : 'textareafield',
                    grow      : true,
                    name      : 'docRemark',
                    fieldLabel: 'ข้อคิดเห็น',
                    anchor    : '100%'
                }
            ],

            buttons: [{
                text: 'บันทึก',
                scope: this,
                action: 'saveCheckDocs'
            }, {
                text: 'ยกเลิก',
                // width: 150,
                scope: this,
                handler: this.onResetClick
            }]    
        });
        this.callParent(arguments);
    }
    ,onResetClick: function(){
        this.up('window').close();
    }
});