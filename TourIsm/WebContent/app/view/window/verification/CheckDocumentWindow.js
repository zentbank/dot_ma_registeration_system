Ext.define('tourism.view.window.verification.CheckDocumentWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.verification-reg-document-check-doc-window',

    requires: [
        'tourism.view.window.verification.CheckDocumentForm'
    ],

    title : 'ผลการตรวจสอบ',
    layout: 'fit',
    autoShow: true,
    width: 600,
    height: 400,
    //True to make the window modal and mask everything behind it when displayed
    modal: true,
    
    // iconCls: 'icon-form',

    initComponent: function() {

        this.items = [
            {
                xtype: 'verification-reg-document-check-doc-form',
                frame: false,
                border: false
            }
            
        ];

        this.callParent(arguments);
    },

});
