Ext.define('tourism.view.window.verification.RegDocumentGrid',{
	extend: 'Ext.grid.Panel',
	alias: 'widget.verification-reg-document-grid',
	stripeRows: true,
	requires: [
      'Ext.grid.Column',   
      'Ext.toolbar.Paging',
	  'Ext.grid.plugin.RowEditing',
	  'Ext.grid.column.Template',
	  'Ext.grid.column.Action',
	  'Ext.grid.plugin.RowExpander',
	  'tourism.store.grid.DocumentRegistrationStore',
	  'Ext.grid.feature.Grouping',
	  'tourism.view.window.verification.CheckDocumentWindow'
	],
	initComponent: function(){
		
//		var columnUpload = 
//			{
//	        	header: '',
//	            xtype: 'actioncolumn',
//	           	width:50,
//	           	align : 'center',
//	            items: [{
//	                iconCls: 'icon-upload-xsmall',
//	                tooltip: 'แนบเอกสาร',
//		            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
//		            	this.fireEvent('itemclick', this, 'documentguideUpload', grid, rowIndex, colIndex, record, node);
//		            }
//	            }]
//				,hidden: this.roleAction == 'document'?false:true
//	        };
  
		var columnView = 
		{
        	header: 'ดูเอกสาร',
            xtype: 'actioncolumn',
           	width:100,
           	align : 'center',
            items: [{
            	getClass: function(v, meta, rec) {          
                   if (rec.get('havefile')) {
                        this.items[0].tooltip = 'ดูเอกสาร';
                        return 'icon-checked-xsmall';
                    }
                },
	                
	            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
	            	
	            	if(record.get("havefile"))
            		{
//	            		this.fireEvent('itemclick', this, 'viewfile', grid, rowIndex, colIndex, record, node);
	            		window.open('business/document/registration/viewfile?docPath='+record.get('docPath'),"Pic","width=600,height=600");
            		}
	            }
                
            }]
	    };	
		
	  var store = Ext.create('tourism.store.grid.DocumentRegistrationStore',{
		  storeId: 'verification-reg-document-store'
	  });
	  
	  Ext.apply(this,{
		 store: store,
		 columns:[
		 {
			text: 'masDocId',
			dataIndex: 'masDocId',
			hidden: true
		 }
		 ,{
			 text: 'traderId'
			 ,dataIndex: 'traderId'
		     ,hidden: true
		 }
		 ,{
			 text: 'เอกสาร',
			 dataIndex: 'documentName',
			 flex: 1,
			 tdCls:'wrap-text'
		 }
		 
//		 ,columnUpload
		 ,columnView
		 ,{
	        	header: 'บันทึกผล</br>การตรวจสอบ',
	            xtype: 'actioncolumn',
	           	width:100,
	           	align : 'center',
	            items: [{
	            	getClass: function(v, meta, rec) {        
	            	   var cssCls = 'icon-notes-xsmall';
	            	   
	                   if (rec.get('docStatus')=='1') {
	                	   
	                        cssCls = 'icon-correct-xsmall';
	                        
	                    }
	                   if (rec.get('docStatus')=='0') {
	                        cssCls = 'icon-delete-xsmall';
	                    }
	                   
	                   return cssCls;
	                },
		                
		            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {
		            	if(record.get("havefile"))
	            		{
		            		this.fireEvent('itemclick', this, 'considerDocs', grid, rowIndex, colIndex, record, node);
		            		
	            		}
		            }
	                
	            }]
		    }
		 
		 ]
	  
	  });
	  
	  this.callParent();
	}

	,features: [{
	    ftype: 'grouping',
	    groupHeaderTpl: '{name} ({rows.length} รายการ)',
	    hideGroupedHeader: true,
	    startCollapsed: false
	}]
	
});