Ext.define('tourism.view.window.verification.VerificationForm', {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.data.*',
        'Ext.form.*',
        'Ext.tab.Tab',
        'tourism.view.window.verification.RegDocumentGrid'
    ],
    xtype: 'registration-verification-form',
    // title: 'ธุรกิจนำเที่ยว',
//    bodyPadding: 5,
    autoScroll: true,

    initComponent: function(){
        var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

        var approveStatus = 'ผ่าน';
        var rejectStatus = 'ไม่ผ่าน';

        if(this.roleAction == 'KEY')
        {
            approveStatus = 'รับเรื่อง';
            rejectStatus = 'ไม่รับเรื่อง';
        }

        if(this.roleAction == 'director')
        {
            approveStatus = 'อนุมัติ';
            rejectStatus = 'ไม่อนุมัติ';
        }
        
    	Ext.apply(this, {
    		layout: {
		        type: 'border',
		        frame: false,
		        border: false
		    },
		    
		    items: [{
		        region: 'center',
		        xtype: 'panel',
		        layout: 'fit',
		        frame: false,
		        border: false,
		        items:[{
		        	xtype: 'verification-reg-document-grid',
		        	id: this.roleAction+'-verification-reg-document-grid',
//		        	title: 'รายการเอกสารประกอบ',
		            autoScroll: true,
		            border: false
		        }]
		    },{
		        region: 'south',
		        xtype: 'panel',
		        layout: 'anchor',
		        frame: false,
		        bodyPadding: 5,
		        border: false,
		        items: [{
                    xtype: 'container',
                    layout: 'anchor',
                    
                    defaults: {
                        anchor: '100%',
                        labelWidth: 200
                    },
                    items: [{
    				    xtype: 'hiddenfield',
    				    name: 'regId'
    				},                
    				{
    				    xtype: 'radiogroup',
    				    fieldLabel: 'ผลการตรวจสอบ',
    				    allowBlank: false,
    				    afterLabelTextTpl: required,
    				    columns: 1,
    				    vertical: false,
    				    items: [
    				        { boxLabel: approveStatus,  name: 'progressStatus', inputValue: 'A' , checked: true},
    				        { boxLabel: rejectStatus,  name: 'progressStatus', inputValue: 'R' },
    				        { boxLabel: 'ขอเอกสารเพิ่มเติม',  name: 'progressStatus', inputValue: '0' }
    				    ]
    				 },
    				 {
    				    xtype     : 'textareafield',
    				    grow      : true,
    				    name      : 'progressDesc',
    				    fieldLabel: 'ข้อคิดเห็น',
    				    anchor    : '100%'
    				},
    				{
    				    xtype: 'fieldcontainer',
    				    fieldLabel: 'หัวหน้ากลุ่มทะเบียน',
    				    id: 'registration-verification-form-progressStatusManager',
    				    defaultType: 'checkboxfield',
    				    anchor    : '100%',
    				    items: [
    				        {
    				            boxLabel  : 'ผ่านการตรวจสอบ',
    				            name      : 'progressStatusManager',
    				            inputValue: 'A'
    				        }
    				    ]
    				}]
                }]
		    }],
		    buttons: [{
                text: 'บันทึก',
                scope: this,
                action: 'saveVerification'
                // handler: this.onCompleteClick
            }, {
                text: 'ยกเลิก',
                // width: 150,
                scope: this,
                handler: this.onResetClick
            }]  
    	});

        
//        Ext.apply(this, {
//            width: '100%',
//            height: '100%',
//            fieldDefaults: {
//                labelAlign: 'left',
//                // labelWidth: 50,
//                msgTarget: 'qtip'
//            },
//            items: 
//            [
//                {
//                    xtype: 'hiddenfield',
//                    name: 'regId'
//                },                
//                {
//                    xtype: 'radiogroup',
//                    fieldLabel: 'ผลการตรวจสอบ',
//                    allowBlank: false,
//                    afterLabelTextTpl: required,
//                    columns: 1,
//                    vertical: false,
//                    items: [
//                        { boxLabel: approveStatus,  name: 'progressStatus', inputValue: 'A' },
//                        { boxLabel: rejectStatus,  name: 'progressStatus', inputValue: 'R' }
//                    ]
//                 },
//                 {
//                    xtype     : 'textareafield',
//                    grow      : true,
//                    name      : 'progressDesc',
//                    fieldLabel: 'ข้อคิดเห็น',
//                    anchor    : '100%'
//                },
//                {
//                    xtype: 'fieldcontainer',
//                    fieldLabel: 'หัวหน้ากลุ่มทะเบียน',
//                    id: 'registration-verification-form-progressStatusManager',
//                    defaultType: 'checkboxfield',
//                    anchor    : '100%',
//                    items: [
//                        {
//                            boxLabel  : 'ผ่านการตรวจสอบ',
//                            name      : 'progressStatusManager',
//                            inputValue: 'A'
//                        }
//                    ]
//                }
//            ],
//
//            buttons: [{
//                text: 'บันทึก',
//                scope: this,
//                action: 'saveVerification'
//                // handler: this.onCompleteClick
//            }, {
//                text: 'ยกเลิก',
//                // width: 150,
//                scope: this,
//                handler: this.onResetClick
//            }]    
//        });
        this.callParent(arguments);
    }
    ,afterRender: function()
    {
        this.callParent(arguments);

        var chkbox = this.down('#registration-verification-form-progressStatusManager');

        chkbox.setVisible(false);

        if(this.roleAction == 'supervisor')
        {
           if(this.multipleJobs == 'MAN')
           {
                if(!chkbox.isVisible())
                {
                    chkbox.setVisible(true);
                }
           }
        }

        if(this.roleAction == 'recheck')
        {
           if(this.multipleJobs == 'MAN')
           {
                if(!chkbox.isVisible())
                {
                    chkbox.setVisible(true);
                }
           }
        }

   
        
    }
    ,onResetClick: function(){
        this.up('window').close();
    }
});