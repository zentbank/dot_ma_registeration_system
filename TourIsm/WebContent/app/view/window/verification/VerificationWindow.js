Ext.define('tourism.view.window.verification.VerificationWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.registration-verification-window',

    requires: [
        'tourism.view.window.verification.VerificationForm'
    ],

    title : 'ผลการตรวจสอบ',
    layout: 'fit',
    autoShow: true,
    width: 800,
    height: 600,
    //True to make the window modal and mask everything behind it when displayed
    modal: true,
    
    // iconCls: 'icon-form',

    initComponent: function() {

        this.items = [
            {
                xtype: 'registration-verification-form',
                frame: false,
                border: false,
                actionMode: this.actionMode,
                roleAction:  this.roleAction,
                verificationGrid: this.verificationGrid,
                multipleJobs: this.multipleJobs
            }
            
        ];

        this.callParent(arguments);
    },

});
