<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="target-densitydpi=device-dpi, width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta name="description" content="Metro UI CSS">
    <meta name="author" content="Sergey Pimenov">
    <meta name="keywords" content="windows 8, modern style, Metro UI, style, modern, css, framework">

    <link href="${pageContext.request.contextPath}/page/css/modern.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/modern-responsive.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/site.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/page/js/google-code-prettify/prettify.css" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment_langs.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/dropdown.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/accordion.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/buttonset.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/carousel.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/input-control.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/pagecontrol.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/rating.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-drag.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/calendar.js"></script>
    
    <!--<script src="http://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/sha256.js"></script>
    <script src="http://crypto-js.googlecode.com/svn/tags/3.1.2/build/components/enc-base64-min.js"></script>-->

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/rollups/sha256.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/components/enc-base64-min.js"></script>

    <title>สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์</title>
<script type="text/javascript">

function validateForm(formData, jqForm, options) { 
    // formData is an array of objects representing the name and value of each field 
    // that will be sent to the server;  it takes the following form: 
    // 
    // [ 
    //     { name:  username, value: valueOfUsernameInput }, 
    //     { name:  password, value: valueOfPasswordInput } 
    // ] 
    // 
    // To validate, we can examine the contents of this array to see if the 
    // username and password fields have values.  If either value evaluates 
    // to false then we return false from this method. 
 


    for (var i=0; i < formData.length; i++) { 
        if (!formData[i].value) { 
            $("#simple-msg").html('<pre><code class="fg-color-red">กรุณากรอกชื่อผู้ใช้</code></pre>');
            return false; 
        } 
    } 
    alert('Both fields contain values.'); 
}

$(document).ready(function()
{    
    $("#simple-post").click(function()
    {

            var form = document.loginForm;

            var hash = CryptoJS.SHA256(form.password.value);
          form.password.value = hash.toString(CryptoJS.enc.Hex);
          form.submit();
           
    });

    //Submit on Enter
    $(".submittable input").bind('keypress', function (e) {
        if(e.keyCode==13){
           var form = document.loginForm;

            var hash = CryptoJS.SHA256(form.password.value);
          form.password.value = hash.toString(CryptoJS.enc.Hex);
          form.submit();
        }

       
    });


});

</script>
</head>
<body class="metrouicss" >



<div class="page secondary">
    <div class="page-header">
        <div class="page">
            <div class="page-header">
                <div class="page-header-content">
                    <h3>ระบบฐานข้อมูลผู้ประกอบธุรกิจนำเที่ยวและมัคคุเทศก์ กรมการท่องเที่ยว</h3>
                   
                </div>
               
            </div>
            <!-- <div class="nav-bar">
                 <div class="padding10">
                    <span class="element">
                      
                    </span>
                </div> 
            </div> -->
        </div>
       
    </div> 

    <br />

    <div class="page-region">
        <div class="page-region-content">
           

                         <form name="loginForm" id="loginForm" action="${pageContext.request.contextPath}/welcome" method="post">
                            <div class="grid">

                                <div class="row">
                                     <div class="span3">
                                        <label class="input-control text">
                                            ชื่อผู้ใช้งาน
                                        </label>
                                    </div>
                                    <div class="span5">
                                        <div class="input-control text">
                                            <input type="text" name="userName" placeholder="ชื่อผู้ใช้งาน"  required="required"/>
                                            <button class="btn-clear"></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                     <div class="span3">
                                        <label class="input-control text">
                                            รหัสผ่าน
                                        </label>
                                    </div>
                                    <div class="span5">
                                        <div class="input-control text submittable">
                                            <input id="userPwd" type="password" name="password" placeholder="รหัสผ่าน"  required="required" />
                                            <button class="btn-clear"></button>
                                        </div>
                                    </div>
                                </div>   

                                      

                                 <div class="row">
                                     <div class="span3">
                                        <label class="input-control text">
                                            &nbsp;
                                        </label>
                                    </div>
                                    <div class="span5">
                                        <button id="simple-post" type="button" class="big bg-color-blue fg-color-white"> 
                                            ลงชื่อเข้าใช้ 
                                        </button>
                                    </div>
                                </div>                    
                         
                         
                                

                                <div id="simple-msg"></div>
                             
                            </div>
                        </form>
         
    </div>
</div>

    </body>
</html>
