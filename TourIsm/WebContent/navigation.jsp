<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="page">
    <div class="nav-bar">
        <div class="nav-bar-inner padding10">
            <span class="pull-menu"></span>

            <a href="${pageContext.request.contextPath}/page/webmobile.jsp"><span class="element brand">
                <img class="place-left" src="${pageContext.request.contextPath}/page/images/logo32.png" style="height: 20px"/> สำนักทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์
            </span></a>

            <div class="divider"></div>

            <ul class="menu">
                <li><a href="${pageContext.request.contextPath}/page/webmobile.jsp">หน้าหลัก</a></li>
                <li data-role="dropdown">
                    <a href="#">ข้อมูลใบอนุญาต</a>
                    <ul class="dropdown-menu">
                    <li><a href="${pageContext.request.contextPath}/info/license/tour">ใบอนุญาตธุรกิจนำเที่ยว</a></li>
                    <li><a href="${pageContext.request.contextPath}/info/license/guide">ใบอนุญาตมัคคุเทศก์</a></li>
                    <li><a href="${pageContext.request.contextPath}/info/license/leader">ใบอนุญาตผู้นำเที่ยว</a></li>
                        
                    </ul>
                </li>
                <li><a href="${pageContext.request.contextPath}/info/license/complaint/menu">ร้องเรียนใบอนุญาต</a></li>
              <!--  <li><a href="${pageContext.request.contextPath}/page/MemberLogin.jsp">สมาขิก</a></li> -->
                <li><a href="${pageContext.request.contextPath}/page/MemberBlank.jsp">สมาขิก</a></li>
            </ul>

        </div>
    </div>
</div>