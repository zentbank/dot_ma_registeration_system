<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="target-densitydpi=device-dpi, width=device-width, initial-scale=1.0, maximum-scale=1">

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery-1.9.0.min.js"></script>

    <title>สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์</title>
<script type="text/javascript">
$(document).ready(function(){    
	
	var fileName = ('${param.traderType}'=='L')?'qrtemplate_leader.html':'qrtemplate.html'
	
	$('body').load('${pageContext.request.contextPath}/printcard/'+fileName +'?rd='+ new Date().getTime(), function(){
		
		//get data Director
		
		
		//var qrCodeUrl = document.location.href.replace('cardbackview','qrcode');
		//var signatureUrl = document.location.href.replace('cardbackview','signatureDirector');
		// $('body').html(h).show();
	
		document.getElementById('cardQrCode_bg').src = '${pageContext.request.contextPath}/resources/cardtemplate/bgwhite.jpg';
       // document.getElementById('cardQrCode').src = qrCodeUrl;
       // console.log(document.getElementById('signature'));
       // document.getElementById('signature').src = signatureUrl;
       
		if('${param.traderType}'!='L'){
			console.log('${param.traderType}');
			var signatureUrl = document.location.href.replace('cardbackview','signatureDirector');
			document.getElementById('signature').src = signatureUrl;
		}
		
		var jsonUrl = document.location.href.replace('cardbackview','showTakePicJson');
		$.ajax({
			  url: jsonUrl,
			  data: {},
			  success: setCardDivData,
			  dataType: 'json'
			});
		
	});
});

function setCardDivData(json, textStatus, jqXHR)
{
  console.log('setCardDivData');
  if(json.success)
    {
	  var m = json.data;
		console.log(m.l_director);
		
	  
	  var h = $('body').html();
	  h = h.replace(  new RegExp("\\{l_director\\}","g")  , m.l_director );
	  
	  $('body').html(h).show();
    }
  else
	{
	  alert('Unnable to load CardDivData!!!');
	}
  
}

</script>
</head>
<body>
</body>