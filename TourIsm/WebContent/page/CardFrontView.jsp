<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery-1.9.0.min.js"></script>

    <title>สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์</title>
<script type="text/javascript">
$(document).ready(function(){    
	
		 loadCardDiv();
		});

/** Load HTML card template , the HTML code will be created by IT-dept */
function loadCardDiv()
{
	var fileName = ('${param.traderType}'=='L')?'template2.html':'template1.html'
	$('body').hide().load('${pageContext.request.contextPath}/printcard/'+fileName +'?rd='+ new Date().getTime(), function(){
		var jsonUrl = document.location.href.replace('cardfrontview','showTakePicJson');
		$.ajax({
			  url: jsonUrl,
			  data: {},
			  success: setCardDivData,
			  dataType: 'json'
			});
		

		if('${param.traderType}'=='L'){
			
			var signatureUrl = document.location.href.replace('cardfrontview','signatureDirector');
			document.getElementById('signature').src = signatureUrl;
		}

        
	});
}

/** put JSON card data into HTML template */
function setCardDivData(json, textStatus, jqXHR)
{
  
  if(json.success)
    {
	  var m = json.data;
	  m.imgPath = '${pageContext.request.contextPath}/business/printcard/registration/img/template/';
	  
	  do_print_param_texts(m);
	  
	  var h = $('body').html();
	  for(var p in m)
		  { 
		    var val = m[p]+'';
		    val = val.replace(/\.\.\./g, '<br/>'); //replace ... with <br/>
  	        val = val.replace(/\+/g, '<br/>'); //replace + with <br/>
		   // h = h.replace(  new RegExp("\\{"+p+"\\}","g")  , val );
  	      if(p != "l_licenseNo"){
  	    	h = h.replace(  new RegExp("\\{"+p+"\\}","g")  , val );
  	    }else{
  	    	if(m.traderType == 'L'){
  	    		h = h.replace(  new RegExp("\\{"+p+"\\}","g")  , m.licenseLeaderNo );
  	    	}else{
	    		h = h.replace(  new RegExp("\\{"+p+"\\}","g")  , val );
	    	}
  	    }
		  }
	   
	  $('body').html(h).show();
      var imgData = document.getElementById('myphoto').src = m.imgData;
      do_print_param_positions();
    }
  else
	{
	  alert('Unnable to load CardDivData!!!');
	}
  
}

/**Override json data with text stored in printKey*/
function do_print_param_texts(data)
{
  var printParams = <%=sss.dot.tourism.controller.admuser.LuaController.globalMap.get(request.getParameter("printKey"))%>;
  if(printParams==null) return;
  
  if(printParams.texts)
	{
	   for(var p in printParams.texts)
		   {
		     data[p] = printParams.texts[p];
		   }
	}
  
}

/**Override item positions with new position stored in printKey*/
function do_print_param_positions()
{
  var printParams = <%=sss.dot.tourism.controller.admuser.LuaController.globalMap.get(request.getParameter("printKey"))%>;
  if(printParams==null) return;
  
  if(printParams.movedItems)
	{ 
	   
	   for(var p in printParams.movedItems)
	   {
	     var pos =  printParams.movedItems[p];
	     var ele = document.getElementById(p);
	     ele.style.top = pos.top;
	     ele.style.left = pos.left;
	   }
	}
  
}

</script>
<style type="text/css">
body{
	margin:0;
	padding:0
}
@font-face {
    font-family: font_freedb;
    src: url(${pageContext.request.contextPath}/printcard/fonts/freedb.ttf);
}

@font-face {
    font-family: PSL-Kittithada;
    src: local('PSL-Kittithada')
}


</style>
</head>
<body >
</body>