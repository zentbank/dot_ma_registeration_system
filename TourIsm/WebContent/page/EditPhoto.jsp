<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
   <!--  <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery-1.9.0.min.js"></script>-->

   <script type="text/javascript" src="http://code.jquery.com/jquery-1.6.2.js"></script>
    
    <!-- jQuery-Ui -->
    <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.7/themes/base/jquery-ui.css" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.7/jquery-ui.js"></script>
    
    <!-- SHJS - Syntax Highlighting for JavaScript -->
    <script type="text/javascript" src="http://shjs.sourceforge.net/sh_main.min.js"></script>
    <script type="text/javascript" src="http://shjs.sourceforge.net/lang/sh_javascript.min.js"></script>
    <link type="text/css" rel="stylesheet" href="http://shjs.sourceforge.net/sh_style.css" />
    <script type="text/javascript">$(document).ready(function(){sh_highlightDocument();});</script>
    
    <!-- jrac - jQuery Resize And Crop -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/page/css/style.jrac.css" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/jrac/jquery.jrac.js"></script>
    <script src="${pageContext.request.contextPath}/printcard/websocket.js"></script>

    <!-- This page business -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/page/css/style.test.jrac.css" />



    <title>สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์</title>

<style type="text/css">

body{
    margin:0;
    padding:0
}
@font-face {
    font-family: font_freedb;
    src: url(${pageContext.request.contextPath}/printcard/fonts/freedb.ttf);
}

@font-face {
    font-family: PSL-Kittithada;
    src: local('PSL-Kittithada')
}
.charms-shaii {
  position: fixed;
  right: 0;
  top: 400px;
  bottom: 0;
  height: 100px;
  min-width: 0;
  width: 100%;
  z-index:-1;
}
.charms.place-left-shaii {
  left: 0;
  right: auto;
}
.bg-color-blueLight {
  background-color: #eff4ff !important;
}


</style>


<script type="text/javascript">

<!--//--><![CDATA[//><!--
      var jracviewport = null;
      $(document).ready(function(){
    	holo_initWebSocket(); 
    	
    	
        $(document).ajaxStart(function(){
          $("#wait").css("display","block");
        });
        $(document).ajaxComplete(function(){
          $("#wait").css("display","none");
        });

        
        loadCardDiv();
          
       

        //edit photo
        document.getElementById("framePhoto").src = "${model.imgData}";
        //"${param.imageData}";
        // Apply jrac on some image.
        jracviewport = $('.pane img').jrac({
          'crop_width': 263,
          'crop_height': 294,
          'crop_x': 72,
          'crop_y': 32,
          'image_width': 400,
          'viewport_onload': function() {
            var $viewport = this;
            var inputs = $viewport.$container.parent('.pane').find('.coords input:text');
            var events = ['jrac_crop_x','jrac_crop_y','jrac_crop_width','jrac_crop_height','jrac_image_width','jrac_image_height'];
            for (var i = 0; i < events.length; i++) {
              var event_name = events[i];
              // Register an event with an element.
              $viewport.observator.register(event_name, inputs.eq(i));
              // Attach a handler to that event for the element.
              inputs.eq(i).bind(event_name, function(event, $viewport, value) {
                $(this).val(value);
              })
              // Attach a handler for the built-in jQuery change event, handler
              // which read user input and apply it to relevent viewport object.
              .change(event_name, function(event) {
                var event_name = event.data;
                $viewport.$image.scale_proportion_locked = $viewport.$container.parent('.pane').find('.coords input:checkbox').is(':checked');
                $viewport.observator.set_property(event_name,$(this).val());
              });
            }
            $viewport.$container.append('<div>ขนาดรูป: '
              +$viewport.$image.originalWidth+' x '
              +$viewport.$image.originalHeight+'</div>')
          }
        })
        // React on all viewport events.
        .bind('jrac_events', function(event, $viewport) {
          var inputs = $(this).parents('.pane').find('.coords input');
          inputs.css('background-color',($viewport.observator.crop_consistent())?'chartreuse':'salmon');
        });

         
       

        

      });
      //--><!]]>
      
     
      /** Load HTML card template , the HTML code will be created by IT-dept */
      function loadCardDiv()
      {
      	loadHTML( ('${param.traderType}'=='L')?'template2.html':'template1.html');
      }
       
      /*load HTML code into textarea , so user can edit it*/
      function loadHTML(filename)
      {      //alert(filename)
             $.ajax({
                   url: '${pageContext.request.contextPath}/printcard/'+filename+'?rd=' + new Date().getTime(),
                   data: {},
                   success: function (data, txtStatus, jqXHR){
                  	 window.theCode = data;
                  	 //alert(window.theCode);
                       loadJsonData();
                  	 //preview();
                   },
                   dataType: 'html'
                 });
      }
      
      function loadJsonData()
      {
    	  var jsonUrl = '${pageContext.request.contextPath}/business/printcard/registration/showTakePicJson/${model.printCardId}/${model.licenseNo}';
          if(${model.printCardId} > 0){
            jsonUrl = '${pageContext.request.contextPath}/business/printcard/registration/showTakePicJson/${model.printCardId}/${model.licenseNo}';
          }else{
              jsonUrl = '${pageContext.request.contextPath}/business/printcard/registration/getTakePicJson/${model.identityNo}/${model.traderType}/${model.traderCategory}';
          }
          $.ajax({
              url: jsonUrl,
              data: {},
              success: setCardDivData,
              dataType: 'json'
            });
      }
      
     /** put JSON card data into HTML template */
    function setCardDivData(json, textStatus, jqXHR){
      
      if(json.success)
        {
        var m = json.data;
        m.imgPath = '${pageContext.request.contextPath}/business/printcard/registration/img/template/';
        
        window.thePhoto = m.imgData; //alert(window.thePhoto);
		window.theDataJsonStr = JSON.stringify(m);
		preview();
        }
		 
      else
      {
        alert('Unnable to load CardDivData!!!')
      }
    }
      
      function preview()
      {
          document.getElementById('previewFrame').src = '${pageContext.request.contextPath}/printcard/tp_admin_preview.jsp?rd='+ new Date().getTime();
      }  

    window.base64Image = null;  /** store the cropped photo in base 64 format */
    function cropPhotoImage(){
          var jrac_crop_x = document.getElementById('jrac_crop_x').value;
          var jrac_crop_y = document.getElementById('jrac_crop_y').value;
          var jrac_crop_width = document.getElementById('jrac_crop_width').value;
          var jrac_crop_height = document.getElementById('jrac_crop_height').value;
          var jrac_image_width = document.getElementById('jrac_image_width').value;
          var jrac_image_height = document.getElementById('jrac_image_height').value;

          //get image to canvas
          var oColorImg = document.getElementById('framePhoto');
          var oCanvas = document.createElement('CANVAS');
          var oCtx = oCanvas.getContext('2d');
          oCanvas.width = jrac_image_width;//oColorImg.naturalWidth *0.1;
          oCanvas.height = jrac_image_height;//oColorImg.naturalHeight *0.1;

          oCtx.drawImage(oColorImg, 0, 0, jrac_image_width, jrac_image_height);


          var imgData=oCtx.getImageData(jrac_crop_x ,jrac_crop_y,jrac_crop_width, jrac_crop_height); 
          var oCanvasDesc = document.createElement('CANVAS');
          var oCtxDesc = oCanvasDesc.getContext('2d');
          oCanvasDesc.width = jrac_crop_width;//oColorImg.naturalWidth *0.1;
          oCanvasDesc.height = jrac_crop_height;//oColorImg.naturalHeight *0.1;
          oCtxDesc.putImageData(imgData,0,0); 

           

          window.base64Image = oCanvasDesc.toDataURL("image/jpeg", 1.0);
          updateIFramePhoto(window.base64Image);
          
      }
    
      function onBase64PhotoAvailable(b64)
      {
    	  window.base64Image = b64;
      }
      
      /**Send cropped base64 data to iframe */
      function updateIFramePhoto()
      {
    	  document.getElementById('previewFrame').contentWindow.updatePhoto(window.base64Image);
      }
      
      function testPhotoImage(){
        document.getElementById("framePhoto").src =  "/TourIsm/page/images/loulou.jpg";
        var imgData = document.getElementById('framePhoto').src;

        var dataBase64 = imgData.match(/base64/g);

        if(!dataBase64){
            var oColorImg = document.getElementById('framePhoto');
            var oCanvas = document.createElement('CANVAS');
            var oCtx = oCanvas.getContext('2d');
            oCanvas.width = oColorImg.naturalWidth;
            oCanvas.height = oColorImg.naturalHeight;

            oCtx.drawImage(oColorImg, 0, 0);

            var imgBase64 = oCanvas.toDataURL("image/jpeg", 1.0);
            imgData = imgBase64;
        }
        document.form_save.imageData.value = imgData;
        document.form_save.submit();
      }
      function savePhotoImage()
      {           
        var imgData = window.base64Image;//document.getElementById('myphoto').src;
           //alert(imgData); // <--- this is image data in based 64 encoding

          var dataBase64 = imgData.match(/base64/g);

          if(!dataBase64){
              var oColorImg = document.getElementById('myphoto');
              var oCanvas = document.createElement('CANVAS');
              var oCtx = oCanvas.getContext('2d');
              oCanvas.width = oColorImg.naturalWidth;
              oCanvas.height = oColorImg.naturalHeight;

              oCtx.drawImage(oColorImg, 0, 0);

              var imgBase64 = oCanvas.toDataURL("image/jpeg", 1.0);
              imgData = imgBase64;
          }
          
          var urlEditMode = '${pageContext.request.contextPath}/business/printcard/registration/savecardphoto';
          var urlAddMode  = '${pageContext.request.contextPath}/lua?f=save_printcard_data_addmode';
          
          $.ajax({
          //   url: ('${param.printCardId}'=='0')?urlAddMode:urlEditMode,
            url: urlEditMode,
            data: {'printCardId':'${param.printCardId}', 'licenseNo':'${param.licenseNo}', 'identityNo':'${param.identityNo}','traderCategory': '${param.traderCategory}', 'traderType':'${param.traderType}', 'imgData':imgData},
            success: function(){ 
             
              var url = '${pageContext.request.contextPath}/business/printcard/registration/showTakePic'
                        +'?printCardId='+"${param.printCardId}"
                        +'&identityNo='+ "${param.identityNo}"   
                        +'&licenseNo='+ "${param.licenseNo}"
                        +'&traderCategory='+"${param.traderCategory}"
                        +'&traderType='+"${param.traderType}";
              window.location = url;
            },
            dataType: 'json',
            type : 'POST'
          });
             
        
      }
      
      function saveCroppedPhoto()
      {
    	var b64 = window.base64Image;//document.getElementById('myphoto').src;
    	var f = document.getElementById('form_download');
    	//alert(b64);return;
    	f.b64.value = b64;
    	f.submit();
    	
      }
      
      function backToTakePicture(){

        try{
          closeCanon();  
        }catch(e){

        }
    	
    	
    	  
        var url = '${pageContext.request.contextPath}/business/printcard/registration/backToTakePicture'
                        +'?printCardId='+"${model.printCardId}"
                        +'&identityNo='+ "${model.identityNo}"   
                        +'&licenseNo='+ "${model.licenseNo}"
                        +'&traderCategory='+"${model.traderCategory}"
                        +'&traderType='+"${model.traderType}";

        window.location = url;
      }
      
      function closeCanon()
      {
    	  var jsonCmd = "closeCanon:{}";	
    	  holo_send(jsonCmd);
    	  
      }
      
      function holo_receive(evt)
      {
         var data = evt.data;
         if(data)
      	   data = $.parseJSON(evt.data);
      }
      
</script>
</head>
<body>
    <div class="pane clearfix" style='position:absolute;right:10px'>
      <img id="framePhoto" src="" alt="รูปมัคคุเทศก์ผู้นำเที่ยยว" />
      <table class="coords" style='display:none'>
        <tr><td>crop x</td><td><input id="jrac_crop_x" type="text" /></td></tr>
        <tr><td>crop y</td><td><input id="jrac_crop_y" type="text" /></td></tr>
        <tr><td>crop width</td><td><input id="jrac_crop_width" type="text" /></td></tr>
        <tr><td>crop height</td><td><input id="jrac_crop_height" type="text" /></td></tr>
        <tr><td>image width</td><td><input id="jrac_image_width" type="text" /></td></tr>
        <tr><td>image height</td><td><input id="jrac_image_height" type="text" /></td></tr>
        <tr><td>lock proportion</td><td><input id="jrac_crop_width" type="checkbox" checked="checked" /></td></tr>
      </table>
    </div>
    <div class="charms-shaii bg-color-blueLight">
          <div style='position:absolute;left:300px;top:15px;'>
              <!-- <button id="take-picture" type="button" >             
                <img src='${pageContext.request.contextPath}/resources/images/camera-icon.png' style='height: 64px;width:64px'/>
              </button>
              <button id="crop-image" type="button" > 
                <img src='${pageContext.request.contextPath}/resources/images/crop-icon.png' style='height: 64px;width:64px'/>
              </button>
              <button id="save-image" type="button" >             
                <img src='${pageContext.request.contextPath}/resources/images/circle-save-icon.png' style='height: 64px;width:64px'/>
              </button> -->
              <div style='position:absolute;left:15px;top:5px;'>
                   <a href="javascript:backToTakePicture();"><img src='${pageContext.request.contextPath}/resources/images/camera-icon.png' style='height: 64px;width:64px;position: absolute;top:0px;z-index:1'/></a>
              </div>
              <div style='position:absolute;left:115px;'>
                   <a href="javascript:cropPhotoImage();"><img src='${pageContext.request.contextPath}/resources/images/crop-icon.png' style='height: 74px;width:74px;position: absolute;top:0px;z-index:1'/></a>
              </div>
              <div style='position:absolute;left:215px;top:5px;'>
                   <a href="javascript:savePhotoImage();"><img src='${pageContext.request.contextPath}/resources/images/circle-save-icon.png' style='height: 64px;width:64px;position: absolute;top:0px;z-index:1'/></a>
              </div>
              <div style='position:absolute;left:315px;top:5px;'>
                   <a href="javascript:saveCroppedPhoto();"><img src='${pageContext.request.contextPath}/resources/images/download-icons.png' style='height: 64px;width:64px;position: absolute;top:0px;z-index:1'/></a>
              </div>
              
      </div>
      
    </div>
    <div>
      <iframe id='previewFrame' frameBorder='0' style='width:500px;background-color:white;position:absolute;top:0px;left:0px;width:450px;height:300px;z-index:9' src=''></iframe>
    </div>
   
    <form name="form_save" action="" method="POST">
     <input name="printCardId" value="${model.printCardId}" type="hidden" />
     <input name="identityNo" value="${model.identityNo}" type="hidden" />
     <input name="licenseNo" value="${model.licenseNo}" type="hidden" />
     <input name="traderCategory" value="${model.traderCategory}" type="hidden" />
     <input name="traderType" value="${model.traderType}" type="hidden" />
     <input name="imageData" value="${model.imgData}" type="hidden" />
    </form>
    <div id="wait" style="display:none;width:150px;height:140px;border:0px solid black;background-color: #ffffff;position:absolute;top:25%;left:50%;padding:2px;"><img src='${pageContext.request.contextPath}/page/images/active.gif' width="120" height="120" />
    
    <div id='downloadFormDiv' style='display:none'> <form id='form_download' target="_blank" action='${pageContext.request.contextPath}/DownloadFileServlet' method='post'><input name='b64' value=''><input name='fileName' value='guide_photo.jpg'></form> </div>  
    
</body>