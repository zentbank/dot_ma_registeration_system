<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="target-densitydpi=device-dpi, width=device-width, initial-scale=1.0, maximum-scale=1">

   <!--  <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery-1.9.0.min.js"></script>-->

   <script type="text/javascript" src="http://code.jquery.com/jquery-1.6.2.js"></script>
    
    <!-- jQuery-Ui -->
    <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.7/themes/base/jquery-ui.css" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.7/jquery-ui.js"></script>
    
    <!-- SHJS - Syntax Highlighting for JavaScript -->
    <script type="text/javascript" src="http://shjs.sourceforge.net/sh_main.min.js"></script>
    <script type="text/javascript" src="http://shjs.sourceforge.net/lang/sh_javascript.min.js"></script>
    <link type="text/css" rel="stylesheet" href="http://shjs.sourceforge.net/sh_style.css" />
    <script type="text/javascript">$(document).ready(function(){sh_highlightDocument();});</script>
    
    <!-- jrac - jQuery Resize And Crop -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/page/css/style.jrac.css" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/jrac/jquery.jrac.js"></script>

    <!-- This page business -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/page/css/style.test.jrac.css" />

    <title>สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์</title>
<script type="text/javascript">

<!--//--><![CDATA[//><!--
      var jracviewport = null;
      $(document).ready(function(){

      	document.getElementById("myphoto").src = "/TourIsm/page/images/loulou.jpg";
        // Apply jrac on some image.
        jracviewport = $('.pane img').jrac({
          'crop_width': 250,
          'crop_height': 170,
          'crop_x': 100,
          'crop_y': 100,
          'image_width': 400,
          'viewport_onload': function() {
            var $viewport = this;
            var inputs = $viewport.$container.parent('.pane').find('.coords input:text');
            var events = ['jrac_crop_x','jrac_crop_y','jrac_crop_width','jrac_crop_height','jrac_image_width','jrac_image_height'];
            for (var i = 0; i < events.length; i++) {
              var event_name = events[i];
              // Register an event with an element.
              $viewport.observator.register(event_name, inputs.eq(i));
              // Attach a handler to that event for the element.
              inputs.eq(i).bind(event_name, function(event, $viewport, value) {
                $(this).val(value);
              })
              // Attach a handler for the built-in jQuery change event, handler
              // which read user input and apply it to relevent viewport object.
              .change(event_name, function(event) {
                var event_name = event.data;
                $viewport.$image.scale_proportion_locked = $viewport.$container.parent('.pane').find('.coords input:checkbox').is(':checked');
                $viewport.observator.set_property(event_name,$(this).val());
              });
            }
            $viewport.$container.append('<div>Image natual size: '
              +$viewport.$image.originalWidth+' x '
              +$viewport.$image.originalHeight+'</div>')
          }
        })
        // React on all viewport events.
        .bind('jrac_events', function(event, $viewport) {
          var inputs = $(this).parents('.pane').find('.coords input');
          inputs.css('background-color',($viewport.observator.crop_consistent())?'chartreuse':'salmon');
        });

         $("#crop-image").click(function(){
            // alert(jracviewport);
            var jrac_crop_x = document.getElementById('jrac_crop_x').value;
            var jrac_crop_y = document.getElementById('jrac_crop_y').value;
            var jrac_crop_width = document.getElementById('jrac_crop_width').value;
            var jrac_crop_height = document.getElementById('jrac_crop_height').value;
            var jrac_image_width = document.getElementById('jrac_image_width').value;
            var jrac_image_height = document.getElementById('jrac_image_height').value;

            //get image to canvas
            var oColorImg = document.getElementById('myphoto');
            var oCanvas = document.createElement('CANVAS');
            var oCtx = oCanvas.getContext('2d');
            oCanvas.width = jrac_image_width;//oColorImg.naturalWidth *0.1;
            oCanvas.height = jrac_image_height;//oColorImg.naturalHeight *0.1;

            oCtx.drawImage(oColorImg, 0, 0, jrac_image_width, jrac_image_height);

            //scale


            // //crop image
            // var imgData=oCtx.getImageData(jrac_crop_x ,jrac_crop_y,jrac_crop_width, jrac_crop_height); 
            // oCtx.putImageData(imgData,0,0, 0,0,jrac_crop_width, jrac_crop_height);  
            // document.getElementById('myphoto2').src = oCanvas.toDataURL("image/jpeg", 1.0);

            // var imgData=oCtx.getImageData(jrac_crop_x ,jrac_crop_y,jrac_crop_width, jrac_crop_height); 

            var imgData=oCtx.getImageData(jrac_crop_x ,jrac_crop_y,jrac_crop_width, jrac_crop_height); 
            var oCanvasDesc = document.createElement('CANVAS');
            var oCtxDesc = oCanvasDesc.getContext('2d');
            oCanvasDesc.width = jrac_crop_width;//oColorImg.naturalWidth *0.1;
            oCanvasDesc.height = jrac_crop_height;//oColorImg.naturalHeight *0.1;
            oCtxDesc.putImageData(imgData,0,0); 

            // oCtxDesc.putImageData(imgData, 0, 0);

            document.getElementById('myphoto2').src = oCanvasDesc.toDataURL("image/jpeg", 1.0);

            

         }); 

      });
      //--><!]]>
</script>
</head>
<body>

    <h1>jQuery Resize and Crop (jrac)</h1>
    <p>jQuery Resize And Crop (jrac) is a jQuery plugin that build a viewport around a
given image permitting to visually resize an image and place a crop. Then it is
possible to use the coordinates data to be used for any purpose.</p>
    <h2>Demo</h2>
    <p>Move the image or the crop with the pointer, resize the crop with the pointer, use the zoom bar to scale the image or set your values into the inputs.</p>
    <div class="pane clearfix">
      <img id="myphoto" src="" alt="Loulou form Sos Chats Geneva" />
      <table class="coords">
        <tr><td>crop x</td><td><input id="jrac_crop_x" type="text" /></td></tr>
        <tr><td>crop y</td><td><input id="jrac_crop_y" type="text" /></td></tr>
        <tr><td>crop width</td><td><input id="jrac_crop_width" type="text" /></td></tr>
        <tr><td>crop height</td><td><input id="jrac_crop_height" type="text" /></td></tr>
        <tr><td>image width</td><td><input id="jrac_image_width" type="text" /></td></tr>
        <tr><td>image height</td><td><input id="jrac_image_height" type="text" /></td></tr>
        <tr><td>lock proportion</td><td><input id="jrac_crop_width" type="checkbox" checked="checked" /></td></tr>
      </table>

      <button id="crop-image" type="button"> 
          crop
      </button>

    </div>
    <canvas id='canvasx' style='display:none'></canvas>
    <img id="myphoto2" src="" alt="Loulou form Sos Chats Geneva" />
</body>