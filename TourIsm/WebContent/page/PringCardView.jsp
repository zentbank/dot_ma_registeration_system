<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="target-densitydpi=device-dpi, width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta name="description" content="Metro UI CSS">
    <meta name="author" content="Sergey Pimenov">
    <meta name="keywords" content="windows 8, modern style, Metro UI, style, modern, css, framework">

    <link href="${pageContext.request.contextPath}/page/css/modern.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/modern-responsive.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/site.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/page/js/google-code-prettify/prettify.css" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment_langs.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/dropdown.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/accordion.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/buttonset.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/carousel.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/input-control.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/pagecontrol.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/rating.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-drag.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/calendar.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/sha256.js"></script>

    <script src="http://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/sha256.js"></script>
    <script src="http://crypto-js.googlecode.com/svn/tags/3.1.2/build/components/enc-base64-min.js"></script>
    <script src="${pageContext.request.contextPath}/printcard/printcard.js"></script>
    <script src="${pageContext.request.contextPath}/printcard/websocket.js"></script>
    <title>สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์</title>
<script type="text/javascript"> 

var IS_DOWNLOADING = false;

$(document).ready(function(){    
	     holo_initWebSocket();
		 loadCardDiv();
		 pageInit();
});

function showShaiiDiv()
{
   $('#rightPanel_eark').hide();
   $('#rightPanel_shaii').show();
}

function showEarkDiv()
{
	$('#rightPanel_shaii').hide();
	$('#rightPanel_eark').show();
}


/** Load HTML card template , the HTML code will be created by IT-dept */
function loadCardDiv()
{
	loadHTML( ('${param.traderType}'=='L')?'template2.html':'template1.html');
}
 
/*load HTML code into textarea , so user can edit it*/
function loadHTML(filename)
{      //alert(filename)
       $.ajax({
             url: '${pageContext.request.contextPath}/printcard/'+filename+'?rd=' + new Date().getTime(),
             data: {},
             success: function (data, txtStatus, jqXHR){
            	 window.theCode = data;
            	 //alert(window.theCode);
                 loadJsonData();
            	 //preview();
             },
             dataType: 'html'
           });
}

function loadJsonData()
{
	 var jsonUrl = '';
     if(${model.printCardId} > 0){
         jsonUrl = '${pageContext.request.contextPath}/business/printcard/registration/showTakePicJson/${model.printCardId}/${model.licenseNo}';
     }else{
         jsonUrl = '${pageContext.request.contextPath}/business/printcard/registration/getTakePicJson/${model.identityNo}/${model.traderType}/${model.traderCategory}';
     }
     
     $.ajax({
		  url: jsonUrl,
		  data: {},
		  success: function (jsonData){ 
			  var data = jsonData.data;
			  data.imgPath = '${pageContext.request.contextPath}/business/printcard/registration/img/template/';
			  
			  window.thePhoto = data.imgData; //alert(window.thePhoto);
			  window.theDataJsonStr = JSON.stringify(data);
			  preview(); 
			  },
		  dataType: 'json'
		});
}

function preview()
{
    //window.theDataJsonStr = "{}";
    document.getElementById('previewFrame').src = '${pageContext.request.contextPath}/printcard/tp_admin_preview.jsp?rd='+ new Date().getTime();
}

var theOverrideTexts = {};
function setText(fieldName, newTxt)
{
	 var data = jQuery.parseJSON(theDataJsonStr);
	 data[fieldName] = newTxt;
	 theOverrideTexts[fieldName] = newTxt; //<--- store override text in to global so we can construct jsonString later
	 window.theDataJsonStr = JSON.stringify(data);
	 preview();
}

 

var movedItems = {};
var printKey = '<%=System.currentTimeMillis()%>';
var base64Photo = null;

function onItemMoved(id , top , left)
{ 
   movedItems[id] = {top : top , left : left}
}

function onBase64PhotoAvailable(b64)
{   //alert('onBase64PhotoAvailable>>>'+b64);
	window.base64Photo = b64;
}

/** store position on the server then print */
function saveThenPrintCard(download)
{    
	var data = JSON.stringify({movedItems:movedItems, texts:theOverrideTexts});
	
	$.ajax({
	      url: '${pageContext.request.contextPath}/lua?f=save_print_positions&rd=' + new Date().getTime(),
	      data: 'key='+printKey+'&data='+data,
	      success: function(){ 
	    	printCard(download);
	      },
	      dataType: 'json',
	      type : 'POST'
	    });
	
	
} 
function printCard(download)
{      
	IS_DOWNLOADING = download
	
	updatePrintingProgress(1,4);
	 
    
    $.ajax({
      url: '${pageContext.request.contextPath}/business/printcard/registration/savecardphoto',
      data: {'imgData':window.base64Photo , 'printCardId':'${model.printCardId}', 'licenseNo':'${model.licenseNo}'},
      success: function(){ 
        updatePrintingProgress(2,4);
        genCards();
      },
      dataType: 'json',
      type : 'POST'
    });
    	 
	
}

/** Generate card image files (eg. front_card.png and back_card.png) , to be fed to the printer later */
function genCards()
{   
	var prefix = document.location.href.substring(0,document.location.href.replace("//","").indexOf('/')+2);
	
	var licenseNo  = '${model.licenseNo}';
	var licenseNo2 = licenseNo.replace(/\\./g); 
	var licenseNo2 =  licenseNo2.replace(/-/g,""); 
	 
	
	var subDir = ('${param.traderType}'=='L')?'tourleader':'guide';
	
	var url1 = prefix+"${pageContext.request.contextPath}/business/printcard/registration/cardfrontview/${model.printCardId}/${model.licenseNo}?printKey="+printKey+'&traderType=${param.traderType}'; //Front Card URL
    var url2 = prefix+"${pageContext.request.contextPath}/business/printcard/registration/cardbackview/${model.printCardId}/${model.licenseNo}?printKey="+printKey+'&traderType=${param.traderType}'; //Back Card URL

    var jsonCmd = "genImage:{'url1':'"+ url1 +"', 'url2':'"+ url2 +"', 'subDir': '"+ subDir +"', 'licenseNo' : '"+  licenseNo2 +"'}";	
    //alert(jsonCmd);
    holo_send(jsonCmd);
}

function holo_receive(evt)
{
   var data = evt.data;
   if(data)
	   data = $.parseJSON(evt.data);
   
   
   if(data && data.cmd=='genImage')
      {
	     if(data.success)
	       { updatePrintingProgress(3,4);
	         
	         /**Execute printing if we're not downloading */
	         if(!IS_DOWNLOADING)
	            sendPrintCmd(data.img1, data.img2);
	       }
      }
   
   if(data && data.cmd=='printCard')
   {
	     if(data.success)
	       {  updatePrintingProgress(4,4); }
   }
}

/** Send image to the printer */
function sendPrintCmd(img1, img2)
{
	holo_send("printCard:{'img1':'"+ img1 +"', 'img2':'"+ img2 +"'}");
}

 

/** put JSON card data into HTML template */
function setCardDivData(json, textStatus, jqXHR)
{
  
  if(json.success)
    {
	  var m = json.data;
	  m.imgPath = '${pageContext.request.contextPath}/business/printcard/registration/img/template/';
	  var h = $('#cardDiv').html();
	  for(var p in m)
		  { 
		    h = h.replace(  new RegExp("\\{"+p+"\\}","g")  , m[p] );
		  }
	   
	  $('#cardDiv').html(h).show();

      var imgData = document.getElementById('myphoto').src = m.imgData;
    }
  else
	{
	  alert('Unnable to load CardDivData!!!')
	}
  
  
}

function updatePrintingProgress( currentStep , maxStep )
{
	//alert('Step '+currentStep+' of '+maxStep);
	
	//step1 Save image from webpage to server (0% complete)
	
	//step2 generate font and back cards from URLs (20% complete)
	
	//step3 printing (40% complete)
	
	//step4 done (100% complete)
}
 
function takePhoto()
{
   var url = '${pageContext.request.contextPath}/page/takePhoto.jsp?printCardId=${param.printCardId}&identityNo=${param.identityNo}&licenseNo=${param.identityNo}'
            +'&traderCategory=${param.traderCategory}&traderType=${param.traderType}';
   document.location.href = url+'&rd='+new Date().getTime();
   
}
</script>
</head>
<!-- <body class="metrouicss" > -->
<body  >

<!-- <div id="cardDiv"></div> -->
<iframe id='previewFrame' frameBorder='0' style='width:500px;background-color:white;position:absolute;top:0px;left:0px;width:450px;height:300px;z-index:9' src=''></iframe>

<div class="metrouicss" >
<div class="page" id="page-index" style='width:100%;margin:5px;'>


    <div class="page-region">
        <div class="page-region-content">
        	<div class="grid">
        		<div class="row">
                    <div class="span1">
                       
                        <div class="span1" style="height: 500px;">                                
                                <div id="buttonDiv" style='position:absolute;top:300px;'>
                                     <div style='position:absolute;left:15px;'>
                                         <!-- STEP1 ถ่ายรูป :  --><a href="javascript:takePhoto();"><!-- 1.1 Webcam --><img src='${pageContext.request.contextPath}/resources/images/webcam-icom-circle.png' style='height: 64px;width:64px;position: absolute;top:0px;z-index:1'/></a>
                                    </div> 
                                    
                                     <div style='position:absolute;left:215px;'>
                                         <!-- STEP2-Aพิมบัตร : --> <a href="javascript:saveThenPrintCard(false);"><!-- พิมพ์ --><img src='${pageContext.request.contextPath}/resources/images/print-128.png' style='height: 64px;width:64px;position: absolute;top:0px;z-index:1'/></a>
 
                                    </div>
                                    
                                    <div style='position:absolute;left:315px;'>
                                         <!-- STEP2-BDownload : --> <a href="javascript:saveThenPrintCard(true);"><!-- download --><img src='${pageContext.request.contextPath}/resources/images/download-icons.png' style='height: 64px;width:64px;position: absolute;top:0px;z-index:1'/></a>
 
                                    </div>
                                     
                                   
                                   
                                </div>
                                
                                
                                <div id="rightPanel_shaii" style='position:absolute;left:450px' class="charms-shaii bg-color-blueLight fg-color-blueDark">
                                    <div>
                                	<p style="padding: 0px 5px 0 5px;">รายละเอียด</p>
                                	<input type="hidden" name="printCardId" value="${model.printCardId}" />
                                    <small style="padding: 0px 5px 0 5px;">เลขที่ใบอนุญาต</small>
                                    <div class="input-control text" style="padding: 0px 5px 0 5px;">
                                    	<input type="text" name="licenseNo" value="${model.traderType == 'L'? model.licenseLeaderNo : model.licenseNo}" />
                                    	<button class="btn-clear" />
                                	</div>
                                	<small style="padding: 0px 5px 0 5px;">ชื่อ-นามสกุล</small>
                                	<div class="input-control text" style="padding: 0px 5px 0 5px;">
                                    	<input type="text" name="traderOwnerName" value="${model.l_traderOwnerName}" onchange="setText('l_traderOwnerName',this.value);"/>
                                    	<button class="btn-clear" />
                                	</div>
                                	<small style="padding: 0px 5px 0 5px;">Name Last name</small>
                                	<div class="input-control text" style="padding: 0px 5px 0 5px;">
                                    	<input type="text" name="traderOwnerNameEn" value="${model.l_traderOwnerNameEn}" onchange="setText('l_traderOwnerNameEn',this.value);"/>
                                    	<button class="btn-clear" />
                                	</div>
                                	<small style="padding: 0px 5px 0 5px;">ประเภท</small>
                                	<div class="input-control text" style="padding: 0px 5px 0 5px;">
                                    	<input type="text" name="traderCategoryName" value="${model.traderCategoryName}" />
                                    	<button class="btn-clear" />
                                	</div>
                                	<small style="padding: 0px 5px 0 5px;">พื้นที่ให้บริการ</small>
                                	<div class="input-control text" style="padding: 0px 5px 0 5px;">
                                    	<input type="text" name="traderArea" value="${model.l_traderArea}" />
                                    	<button class="btn-clear" />
                                	</div>
                                	<small style="padding: 0px 5px 0 5px;">เลขบัตรประจำตัวประชาชน</small>
                                	<div class="input-control text" style="padding: 0px 5px 0 5px;">
                                    	<input type="text" name="identityNo" value="${model.identityNo}" />
                                    	<button class="btn-clear" />
                                	</div>
                                	<small style="padding: 0px 5px 0 5px;">วันออกบัตร</small>
                                	<div class="input-control text" style="padding: 0px 5px 0 5px;">
                                    	<input type="text" name="effectiveDate" value="${model.effectiveDate}" />
                                    	<button class="btn-clear" />
                                	</div>
                                	<small style="padding: 0px 5px 0 5px;">บัตรหมดอายุ</small>
                                	<div class="input-control text" style="padding: 0px 5px 0 5px;">
                                    	<input type="text" name="expireDate" value="${model.expireDate}" />
                                    	<button class="btn-clear" />
                                	</div>
                                	</div>
                                </div> 
                         </div> 
                         
                </div>
        	</div>
       	</div>
    </div>
</div>


<div id="rightPanel_eark" style="position:absolute; top:0;right:0px;z-index:9; background-color:red;display:none"> 
							  	 		<div id='photo_webcam_div'> 
							    				<script src='${pageContext.request.contextPath}/printcard/cam.js'></script>    
							    				<video id='video' muted></video><br/>  
							            <input type='button' value='ถ่ายรูป' onclick='takePicture()'>
							            <select id='videoSource'></select><select id='audioSource' style='display:none'></select>  
							  	 		 </div>
							 
							  	 		 <div id='photo_upload_div' style="display:none">
											 <h3>Upload Existing Photo</h3><input type='file' id='file'>	
							  	 		 </div>
							
							 
							  	 		 <div id='photo_canon_div' style="display:none">
							  	 		 	 Canon Div Render here	
							  	 		 </div>
							
							         <div id='corp_div' style="display:none">
							           <canvas id='photo_canvas'></canvas>Photo to canvas<div id='webcamTxt' style='display:none'></div>
							           <canvas id='canvas3' style='display:none'></canvas>
							           
							         </div>
</div>
</body>
</html>
