<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="target-densitydpi=device-dpi, width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta name="description" content="Metro UI CSS">
    <meta name="author" content="Sergey Pimenov">
    <meta name="keywords" content="windows 8, modern style, Metro UI, style, modern, css, framework">

    <link href="${pageContext.request.contextPath}/page/css/modern.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/modern-responsive.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/site.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/page/js/google-code-prettify/prettify.css" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment_langs.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/dropdown.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/accordion.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/buttonset.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/carousel.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/input-control.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/pagecontrol.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/rating.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-drag.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/calendar.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/sha256.js"></script>

    <script src="http://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/sha256.js"></script>
    <script src="http://crypto-js.googlecode.com/svn/tags/3.1.2/build/components/enc-base64-min.js"></script>

    <title>สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์</title>
<script type="text/javascript">
$(document).ready(function()
		{    
	
		    $("#provinceId").change(function()
		    {

		            var form = document.loginForm;

		            ajaxAmphur(form);

		    });

		    $("#amphurId").change(function()
				    {

				            var form = document.loginForm;

				            ajaxTambol(form);

				    });

		    function ajaxAmphur(a)
		    {
				var provinceId = a.provinceId.value;
				 $.post("../readamphur", { provinceId: provinceId},
			            function(data) {
			            
			              var options = '';
						    for (var i = 0; i < data.length; i++)
						    {
						        options += '<option value="' + data[i].amphurId + '">' + data[i].amphurName  + '</option>';
						    };
						
							$("#amphurId").html(options);
							a.amphurId.value= data[0].amphurId;
							
							ajaxTambol(a);
			            },
			            "json" // กำหนดให้มันเป็น json เพื่อที่ jquery จะแปลงข้อมูล json ให้เป็น object มาให้เลย
			         ); 
			};

			 function ajaxTambol(a)
			    {
					var amphurId = a.amphurId.value;
					
					 $.post("../readtambol", { amphurId: amphurId},
				            function(data) {
				             
				              var options = '';
							    for (var i = 0; i < data.length; i++)
							    {
							        options += '<option value="' + data[i].tambolId + '">' + data[i].tambolName  + '</option>';
							    };
							   
								$("#tambolId").html(options);
				            },
				            "json" // กำหนดให้มันเป็น json เพื่อที่ jquery จะแปลงข้อมูล json ให้เป็น object มาให้เลย
				         ); 
				};
		});


</script>
</head>
<body class="metrouicss" >



<div class="page secondary">
    <div class="page-header">
        <div class="page">
            <div class="page-header">
                <div class="page-header-content">
                    <h3>ใบอนุญาตธุรกิจนำเที่ยว</h3>
                   
                </div>
               
            </div>
    
        </div>
       
    </div> 

    <br />

    <div class="page-region">
        <div class="page-region-content">
           

	     <form name="loginForm" id="loginForm" action="../printLicensePdf" method="post">

	     		<input type="hidden" name="printLicenseId" value="${model.printLicenseId}" />
				<input type="hidden" name="day" value="${model.day}" />
				<input type="hidden" name="month" value="${model.month}" />
				<input type="hidden" name="year" value="${model.year}" />
				<input type="hidden" name="registrationType" value="${model.registrationType}" />
				<input type="hidden" name="position" value="${model.position}" />
				<input type="hidden" name="position1" value="${model.position1}" />
				<input type="hidden" name="position2" value="${model.position2}" />
				<input type="hidden" name="orgId" value="${model.orgId}" />
				<input type="hidden" name="addressId" value="${model.addressId}" />
				
	        <div class="grid">

	            <div class="row">
	                 <div class="span3">
	                    <label class="input-control text">
	                        ใบอนุญาตเลขที่
	                    </label>
	                </div>
	                <div class="span5">
	                    <div class="input-control text">
	                        <input type="text" name=licenseNo value="${model.licenseNo}"/>
	                        <button class="btn-clear"></button>
	                    </div>
	                </div>
	            </div>

	            <div class="row">
	                 <div class="span3">
	                    <label class="input-control text">
	                        ออกใบอนุญาตให้
	                    </label>
	                </div>
	                
	                <div class="span5">
	                    <div class="input-control text">
	                        <input type="text" name="traderOwnerName" value="${model.traderOwnerName}"/>
	                        <button class="btn-clear"></button>
	                    </div>
	                </div>
	                <%-- <div class="span2">
	                    <div class="input-control select">
	                        <input type="text" name="traderOwnerName" value="${model.traderOwnerName}"/>
	                        <button class="btn-clear"></button>
	                        <select name="prefixId" id="prefixId"> <!-- onchange="javascript:load_SearchMasAmphur(this)" -->
							    <option value="${model.prefixId}" selected>${model.prefixName}</option>
							    <c:forEach items="${prefix}" var="prefix"> 
							          	<option value="${prefix.prefixId}">${prefix.prefixName}</option>		             
							    </c:forEach>
							</select>   
	                    </div>
	                </div>  
	                <div class="span3">
	                    <div class="input-control text">
	                        <input type="text" name="firstName" value="${model.firstName}"/>
	                        <button class="btn-clear"></button>
	                    </div>
	                </div>
	                 <div class="span2">
	                    <div class="input-control text">
	                        <input type="text" name="lastName" value="${model.lastName}"/>
	                        <button class="btn-clear"></button>
	                        <select name="postfixId" id="postfixId"> <!-- onchange="javascript:load_SearchMasAmphur(this)" -->
							    <option value="${model.postfixId}" selected>${model.postfixName}</option>
							    <c:forEach items="${postfix}" var="postfix"> 
							          	<option value="${postfix.postfixId}">${postfix.postfixName}</option>		             
							    </c:forEach>
							</select>   
	                    </div>
	                </div>  --%>
	            </div>   

	                  

	            <div class="row">
	                 <div class="span3">
	                    <label class="input-control text">
	                        ทะเบียนนิติบุคคลเลขที่
	                    </label>
	                </div>
	                <div class="span5">
	                    <div class="input-control text">
	                        <input type="text" name="identityNo" value="${model.identityNo}"/>
	                        <button class="btn-clear"></button>
	                    </div>
	                </div>
	            </div>       

	            <div class="row">
	                 <div class="span3">
	                    <label class="input-control text">
	                        ประเภท
	                    </label>
	                </div>
	                <div class="span5">
	                    <div class="input-control text">
	                        <input type="text" name="traderCategoryName" value="${model.traderCategoryName}"/>
	                        <button class="btn-clear"></button>
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                 <div class="span3">
	                    <label class="input-control text">
	                        โดยใช้ชื่อภาษาไทยว่า
	                    </label>
	                </div>
	                <div class="span5">
	                    <div class="input-control text">
	                        <input type="text" name="traderName" value="${model.traderName}"/>
	                        <button class="btn-clear"></button>
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                 <div class="span3">
	                    <label class="input-control text">
	                        หรือใช้ชื่อภาษาต่างประเทศ(ถ้ามี)ว่า
	                    </label>
	                </div>
	                <div class="span5">
	                    <div class="input-control text">
	                        <input type="text" name="traderNameEn" value="${model.traderNameEn}"/>
	                        <button class="btn-clear"></button>
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                 <div class="span3">
	                    <label class="input-control text">
	                        ซึ่งอ่านเป็นภาษาไทยว่า
	                    </label>
	                </div>
	                <div class="span5">
	                    <div class="input-control text">
	                        <input type="text" name="pronunciationName" value="${model.pronunciationName}"/>
	                        <button class="btn-clear"></button>
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                 <div class="span3">
	                    <label class="input-control text">
	                        สำนักงานตั้งอยู่เลขที่
	                    </label>
	                </div>
	                <div class="span5">
	                    <div class="input-control text">
	                        <input type="text" name="addressNo" value="${model.addressNo}"/>
	                        <button class="btn-clear"></button>
	                    </div>
	                </div>
	            </div>
	            
	            <%--  <div class="row">
	                 <div class="span3">
	                    <label class="input-control text">
	                        อาคาร
	                    </label>
	                </div>
	                <div class="span5">
	                    <div class="input-control text">
	                        <input type="text" name="buildingName" value="${model.buildingName}"/>
	                        <button class="btn-clear"></button>
	                    </div>
	                </div>
	            </div>
	            
	            <div class="row">
	                 <div class="span3">
	                    <label class="input-control text">
	                        ชั้น
	                    </label>
	                </div>
	                <div class="span5">
	                    <div class="input-control text">
	                        <input type="text" name="floor" value="${model.floor}"/>
	                        <button class="btn-clear"></button>
	                    </div>
	                </div>
	            </div>
	            
	            <div class="row">
	                 <div class="span3">
	                    <label class="input-control text">
	                        ห้อง
	                    </label>
	                </div>
	                <div class="span5">
	                    <div class="input-control text">
	                        <input type="text" name="roomNo" value="${model.roomNo}"/>
	                        <button class="btn-clear"></button>
	                    </div>
	                </div>
	            </div>
	            
	            <div class="row">
	                 <div class="span3">
	                    <label class="input-control text">
	                        หมู่บ้าน
	                    </label>
	                </div>
	                <div class="span5">
	                    <div class="input-control text">
	                        <input type="text" name="villageName" value="${model.villageName}"/>
	                        <button class="btn-clear"></button>
	                    </div>
	                </div>
	            </div>
	            
	            <div class="row">
	                 <div class="span3">
	                    <label class="input-control text">
	                        หมู่ที่
	                    </label>
	                </div>
	                <div class="span5">
	                    <div class="input-control text">
	                        <input type="text" name="moo" value="${model.moo}"/>
	                        <button class="btn-clear"></button>
	                    </div>
	                </div>
	            </div> --%>
	            
	            <div class="row">
	                 <div class="span3">
	                    <label class="input-control text">
	                        ตรอก/ซอย
	                    </label>
	                </div>
	                <div class="span5">
	                    <div class="input-control text">
	                        <input type="text" name="soi" value="${model.soi}"/>
	                        <button class="btn-clear"></button>
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                 <div class="span3">
	                    <label class="input-control text">
	                        ถนน
	                    </label>
	                </div>
	                <div class="span5">
	                    <div class="input-control text">
	                        <input type="text" name="roadName" value="${model.roadName}"/>
	                        <button class="btn-clear"></button>
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                 <div class="span3">
	                    <label class="input-control text">
	                        ตำบล/แขวง
	                    </label>
	                </div>
	                <div class="span5">
	                    <div class="input-control text">
	                         <input type="text" name="tambolName" value="${model.tambolName}"/>
	                        <button class="btn-clear"></button> 
	                       <%--  <select name='tambolId' id="tambolId"> <!-- onchange="javascript:load_SearchMasAmphur(this)" -->
							    <option value="${model.tambolId}" selected>${model.tambolName}</option>
							    <c:forEach items="${tambol}" var="tambol"> 
							          	<option value="${tambol.tambolId}">${tambol.tambolName}</option>		             
							    </c:forEach>
							</select>   --%>
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                 <div class="span3">
	                    <label class="input-control text">
	                        อำเภอ/เขต
	                    </label>
	                </div>
	                <div class="span5">
	                    <div class="input-control text">
	                         <input type="text" name="amphurName" value="${model.amphurName}"/>
	                        <button class="btn-clear"></button> 
	                         <%-- <select name='amphurId' id="amphurId"> <!-- onchange="javascript:load_SearchMasAmphur(this)" -->
							    <option value="${model.amphurId}" selected>${model.amphurName}</option>
							    <c:forEach items="${amphur}" var="amphur"> 
							          	<option value="${amphur.amphurId}">${amphur.amphurName}</option>		             
							    </c:forEach>
							</select> --%>
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                 <div class="span3">
	                    <label class="input-control text">
	                        จังหวัด
	                    </label>
	                </div>
	                <div class="span5">
	                    <div class="input-control text">
	                        <input type="text" name="provinceName" value="${model.provinceName}"/>
	                        <button class="btn-clear"></button>
	                        <%-- <select name='provinceId' id="provinceId">
							    <option value="${model.provinceId}" selected>${model.provinceName}</option>
							    <c:forEach items="${province}" var="province" >					      
							            <option value="${province.provinceId}">${province.provinceName}</option>
							    </c:forEach>
							    
							</select> --%>
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                 <div class="span3">
	                    <label class="input-control text">
	                        รหัสไปรษณีย์
	                    </label>
	                </div>
	                <div class="span5">
	                    <div class="input-control text">
	                        <input type="text" name="postCode" value="${model.postCode}"/>
	                        <button class="btn-clear"></button>
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                 <div class="span3">
	                    <label class="input-control text">
	                        ออกให้ ณ วันที่
	                    </label>
	                </div>
	                <div class="span5">
	                    <div class="input-control text">
	                        <input type="text" name="today" value="${model.today}"/>
	                        <button class="btn-clear"></button>
	                    </div>
	                </div>
	            </div>
			  <div class="row">
	                 <div class="span3">
	                    <label class="input-control text">
	                        ชื่อนายทะเบียน
	                    </label>
	                </div>
	                <div class="span5">
	                    <div class="input-control text">
	                        <input type="text" name="managerBG" value="${model.managerBG}"/>
	                        <button class="btn-clear"></button>
	                    </div>
	                </div>
	           </div>
			<c:if test="${!empty branch}">
	
	           <div class="row">
	                 <div class="span3">
	                    <label class="input-control text">
	                       <input type="hidden" name="branchType" value="${model.branchType}" />
	                    </label>
	                </div>
	                <div class="span5">
	                    <div class="input-control text">
	                       สาขา
	                    </div>
	                </div>
	            </div>

				<div class="row">
	                 <div class="span3">
	                    <label class="input-control text">
	                        สาขาที่
	                    </label>
	                </div>
	                <div class="span5">
	                    <div class="input-control text">
	                        <input type="text" name="branchNo" value="${branch.branchNo}"/>
	                        <button class="btn-clear"></button>
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                 <div class="span3">
	                    <label class="input-control text">
	                        เลขที่
	                    </label>
	                </div>
	                <div class="span5">
	                    <div class="input-control text">
	                        <input type="text" name="branchAddressNo" value="${branch.addressNo}"/>
	                        <button class="btn-clear"></button>
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                 <div class="span3">
	                    <label class="input-control text">
	                        ซอย
	                    </label>
	                </div>
	                <div class="span5">
	                    <div class="input-control text">
	                        <input type="text" name="branchSoi" value="${branch.soi}"/>
	                        <button class="btn-clear"></button>
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                 <div class="span3">
	                    <label class="input-control text">
	                        ถนน
	                    </label>
	                </div>
	                <div class="span5">
	                    <div class="input-control text">
	                        <input type="text" name="branchRoadName" value="${branch.roadName}"/>
	                        <button class="btn-clear"></button>
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                 <div class="span3">
	                    <label class="input-control text">
	                        ตำบล/แขวง
	                    </label>
	                </div>
	                <div class="span5">
	                    <div class="input-control text">
	                        <input type="text" name="branchTambolName" value="${branch.tambolName}"/>
	                        <button class="btn-clear"></button>
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                 <div class="span3">
	                    <label class="input-control text">
	                        อำเภอ/เขต
	                    </label>
	                </div>
	                <div class="span5">
	                    <div class="input-control text">
	                        <input type="text" name="branchAmphurName" value="${branch.amphurName}"/>
	                        <button class="btn-clear"></button>
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                 <div class="span3">
	                    <label class="input-control text">
	                        จังหวัด
	                    </label>
	                </div>
	                <div class="span5">
	                    <div class="input-control text">
	                        <input type="text" name="branchProvinceName" value="${branch.provinceName}"/>
	                        <button class="btn-clear"></button>
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                 <div class="span3">
	                    <label class="input-control text">
	                        รหัสไปรษณีย์
	                    </label>
	                </div>
	                <div class="span5">
	                    <div class="input-control text">
	                        <input type="text" name="branchPostCode" value="${branch.postCode}"/>
	                        <button class="btn-clear"></button>
	                    </div>
	                </div>
	            </div>
		</c:if>
	            
	                      

	      	    <div class="row">
	                 <div class="span3">
	                    <label class="input-control text">
	                        &nbsp;
	                    </label>
	                </div>
	                <div class="span5">
	                    <input type="submit" value="พิมพ์" class="big bg-color-blue fg-color-white" />
	                </div>
	            </div>
	     
	     		
	            

	            <div id="simple-msg"></div>
	         
	        </div>
	    </form>
         
    </div>
</div>

    </body>
</html>
