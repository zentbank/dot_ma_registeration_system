<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/html">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="target-densitydpi=device-dpi, width=device-width, initial-scale=1.0, maximum-scale=1">
<meta name="description" content="Metro UI CSS">
<meta name="author" content="Sergey Pimenov">
<meta name="keywords"
	content="windows 8, modern style, Metro UI, style, modern, css, framework">

<link href="${pageContext.request.contextPath}/page/css/modern.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/page/css/modern-responsive.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath}/page/css/site.css"
	rel="stylesheet" type="text/css">
<link
	href="${pageContext.request.contextPath}/page/js/google-code-prettify/prettify.css"
	rel="stylesheet" type="text/css">

<script type="text/javascript"
	src="${pageContext.request.contextPath}/page/js/assets/jquery-1.9.0.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/page/js/assets/jquery.mousewheel.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/page/js/assets/moment.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/page/js/assets/moment_langs.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/page/js/modern/dropdown.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/page/js/modern/accordion.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/page/js/modern/buttonset.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/page/js/modern/carousel.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/page/js/modern/input-control.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/page/js/modern/pagecontrol.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/page/js/modern/rating.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/page/js/modern/slider.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/page/js/modern/tile-slider.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/page/js/modern/tile-drag.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/page/js/modern/calendar.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/page/js/sha256.js"></script>

<script
	src="http://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/sha256.js"></script>
<script
	src="http://crypto-js.googlecode.com/svn/tags/3.1.2/build/components/enc-base64-min.js"></script>
<script src="${pageContext.request.contextPath}/printcard/printcard.js"></script>
<script src="${pageContext.request.contextPath}/printcard/websocket.js"></script>
<script src="${pageContext.request.contextPath}/printcard/purl.js"></script>
<script src="${pageContext.request.contextPath}/printcard/ajaxfileupload.js"></script>
<script>
var rootPath = '${pageContext.request.contextPath}';

function holo_receive(evt)
{
   var data = evt.data;
   if(data)
	   data = $.parseJSON(evt.data);
   
   if(data && data.cmd=='canon')
      {
	     if(data.success)
	       {  
	         //alert("receive canon photo");
	         //alert(data.b64);
	         var str = "data:image/jpg;base64,"+data.b64;
	         submitBase64Photo(rootPath, str);
	       }
      }
   
 
}

$(document).ready(function(){    
	 pageInit();
	 holo_initWebSocket();
});
</script>
<title>สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์</title>

<body>
	<div id="buttonDiv" style='position: absolute;'>
		<div style='position: absolute; left: 15px;'>
			<!-- STEP1 ถ่ายรูป :  -->
			<a href="javascript:$('#rightPanel_eark').show();showPhotoDiv('photo_webcam_div');"> <!-- 1.1 Webcam --> <img
				src='${pageContext.request.contextPath}/resources/images/webcam-icom-circle.png'
				style='height: 64px; width: 64px; position: absolute; top: 0px; z-index: 1' id='icon1'/>
			</a>
		</div>
		<div style='position: absolute; left: 115px;'>
			<a href="javascript:$('#rightPanel_eark').show();showPhotoDiv('photo_upload_div');"> <!-- 1.2 Upload --> <img
				src='${pageContext.request.contextPath}/resources/images/upload-circle-icon.png'
				style='height: 64px; width: 64px; position: absolute; top: 0px; z-index: 1' />
			</a>
		</div>
		<div style='position: absolute; left: 215px;'>
			<a href="javascript:$('#rightPanel_eark').show();showPhotoDiv('photo_canon_div');startCanon();"> <!-- 1.3 Canon --> <img
				src='${pageContext.request.contextPath}/resources/images/camera-icon.png'
				style='height: 64px; width: 64px; position: absolute; top: 0px; z-index: 1' />
			</a>
		</div>
	</div>


	<div id="rightPanel_eark"
		style="position: absolute; top: 100px; background-color: red; display:none">
		<div id='photo_webcam_div'>
			<script src='${pageContext.request.contextPath}/printcard/cam.js'></script>
			<video id='video' muted></video>
			<br /> <input type='button' value='ถ่ายรูป' onclick="takePicture();submitPhoto('${pageContext.request.contextPath}');">
			<select id='videoSource'></select><select id='audioSource'
				style='display: none'></select>
				 
		</div>

		<div id='photo_upload_div' style="display: none">
			<h3>Upload Existing Photo</h3>
			<input type='file' id='imgData' name="imgData" onchange="uploadPhoto('${pageContext.request.contextPath}');">
		</div>


		<div id='photo_canon_div' style="display: none">Canon Div Render
			here</div>

		<div id='corp_div' style="display: none">
			<canvas id='photo_canvas'></canvas>
			<div id='webcamTxt' style='display: none'></div>
			<canvas id='canvas3' style='display: none'></canvas>
			
		 
			
			<!--  submit to jrac -->
			<form id='f1' method='post' style='display:none'>
			<input name='imageData' type='hidden'>
			</form>
			
			<hr>
			This is image 2
			<img id='image2' src=''>
			
			
		</div>
	</div>
</body>
</html>
