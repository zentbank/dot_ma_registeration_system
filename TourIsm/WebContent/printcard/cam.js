var videoElement = null
var audioSelect  = null;
var videoSelect  = null;
//var startButton  = null;
var canvas = null;
var ctx = null;
var localMediaStream = null;

 


var photoWidth, photoHeight;
var getVideoSize = function() {
    photoWidth = videoElement.videoWidth;
    photoHeight = videoElement.videoHeight;
    videoElement.removeEventListener('playing', getVideoSize, false);
    document.getElementById('webcamTxt').innerHTML = 'Webcam Dim = '+photoWidth+'x'+photoHeight;
};
 
function initCam()
{
videoElement = document.getElementById("video"); 
audioSelect = document.getElementById("audioSource"); 
videoSelect = document.getElementById("videoSource");  




audioSelect.onchange = startVideo;
videoSelect.onchange = startVideo;

navigator.getUserMedia = navigator.getUserMedia ||
  navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

if (typeof MediaStreamTrack === 'undefined'){
  alert('This browser does not support MediaStreamTrack.\n\nTry Chrome Canary.');
} else {
  MediaStreamTrack.getSources(gotSources);
}

//document.getElementById('div1').style.display = 'none';
//document.getElementById('div2').style.display = 'inline';

}//end initCam

function gotSources(sourceInfos) {

  //exit if we've already setup VDOsource dropdown
  if(videoSelect.length>0)return;

  for (var i = 0; i != sourceInfos.length; ++i) {
    var sourceInfo = sourceInfos[i];
    var option = document.createElement("option");
    option.value = sourceInfo.id;
    if (sourceInfo.kind === 'audio') {
      	    
      option.text = sourceInfo.label || 'microphone ' + (audioSelect.length + 1);
      audioSelect.appendChild(option);
    } else if (sourceInfo.kind === 'video') {
      option.text = sourceInfo.label || 'camera ' + (videoSelect.length + 1);
      videoSelect.appendChild(option);
    } else {
      console.log('Some other kind of source: ', sourceInfo);
    }
  }
}


function successCallback(stream) {
  //alert('starting VDO');	
  window.stream = stream; // make stream available to console
  videoElement.src = window.URL.createObjectURL(stream);
  videoElement.addEventListener('playing', getVideoSize, false);
  videoElement.play();
   
  localMediaStream = stream;
  //alert('set localMediaStream to '+localMediaStream)
}

function errorCallback(error){
  console.log("navigator.getUserMedia error: ", error);

  localMediaStream = null;
}

function startVideo(){
  if (!!window.stream) {
    videoElement.src = null;
    window.stream.stop();
  }
  

  var audioSource = audioSelect.value;
  var videoSource = videoSelect.value;
  var constraints = {
    audio: {
      optional: [{sourceId: audioSource}]
    },
    video: {
      optional: [{sourceId: videoSource}]
    }
  };
  //alert(audioSource+','+videoSource)
  navigator.getUserMedia(constraints, successCallback, errorCallback);
}

 
  function takePicture() {
	 // alert('takepicture');
    if (localMediaStream) {  
     var v = videoElement;
     var c = canvas;
      
    
      c.setAttribute('width', photoWidth);
      c.setAttribute('height', photoHeight); 
      ctx.fillRect(0, 0, photoWidth, photoHeight);
      ctx.drawImage(v, 0, 0, photoWidth, photoHeight);

      hidePhotoDiv();
      beginPreview(); 
      
    }else{alert('no localMediaStream'+localMediaStream);}
  }

