	var corpStepGap = 10;
    var corpStepH   = 0;
    var corpStepW   = 0;

    function param(key)
    {
      if(this.p_url == null)
        this.p_url = $.url(document.location);
        
      var ret = this.p_url.param(key);
      if(!ret)
    	  return '';
      else
    	  return ret;
      
    };
    
    function printcardParams()
    {
    	var s = 'printCardId='+param('printCardId')+'&identityNo='+param('identityNo')+'&licenseNo='+param('licenseNo')+'&traderCategory='+param('traderCategory')+'&traderType='+param('traderType');
       
        return s;
    }
  

    function pageInit()
    { 
       var canvasId = 'photo_canvas'
       canvas = document.getElementById(canvasId);
       ctx = canvas.getContext('2d'); 

    }

    function showPhotoDiv(id)
    {
    	hidePhotoDiv();
    	$('#'+id).show();
      
      if(id=='photo_webcam_div')
         { initCam(); startVideo(); }  
    }

    function hidePhotoDiv()
    {
      $("[id ^= 'photo_'][id $= '_div']").hide(); //hide all photo_XXXXX_div
      $('#corp_div').hide();
    }
    
   

    //---------------------- Canvas method ----------------------------------------
    var myphoto  = null;
    var layer1 = null;
     var container = null;
     var cropW = 300;
     var cropH = 300;

    /*  
    function beginPreview_old()
    { //alert('beginPreview');
      corpStepH = corpStepW = 0; 
      var w = canvas.getAttribute('width');
      var h = canvas.getAttribute('height');
      myphoto=ctx.getImageData(0,0,w,h); //store original photo before starting preview
      ctx.fillStyle="#FFFFFF";
      ctx.fillRect(0,0,w,h); 
      document.addEventListener('keydown', moveCanvas, false);
       

      $('#corp_div').show();
      drawCanvas();
    }
    */
     function beginPreview()
     {   
       $('#corp_div').show(); 
     }
     
     function submitPhoto(path)
     {
    	 var url = path+'/business/printcard/registration/jracphoto?'+printcardParams();
    	 //alert('submit to '+url);
    	 var f = document.getElementById('f1');
    	 f.action = url;
    	 f.imageData.value = document.getElementById('photo_canvas').toDataURL('image/jpeg',  1.0);
    	 f.submit(); 
     }
     
     function submitBase64Photo(path, base64Data)
     {
    	 var url = path+'/business/printcard/registration/jracphoto?'+printcardParams();
    	 //alert('submit to '+url);
    	 var f = document.getElementById('f1');
    	 f.action = url;
    	 f.imageData.value = base64Data;
    	 f.submit(); 
     }
    

    /* Draw the corping canvas */ 
    function drawCanvas()
    {
      var w = photoWidth;
      var h = photoHeight;
      ctx.putImageData(myphoto,0,0);   
      ctx.strokeStyle='#000000';
      ctx.lineWidth=3;
      ctx.strokeRect( corpStepW*corpStepGap ,corpStepH*corpStepGap,cropW, cropH);
  
      copyPhoto();
    }

    var canvas3 = null;
    /**Copy the photo canvas to card image*/
    function copyPhoto()
    {
       //var imgData=ctx.getImageData(corpStepW*corpStepGap,corpStepH*corpStepGap, cropW, cropH); 
       //ctx2.putImageData(imgData,0,0, 0,0,640,480);  
      if(canvas3==null)
        {canvas3 = document.getElementById('canvas3');
         canvas3.setAttribute('width',cropW);
         canvas3.setAttribute('height',cropH);
         ctx3 = canvas3.getContext('2d');

        }
      var imgData=ctx.getImageData(corpStepW*corpStepGap ,corpStepH*corpStepGap,cropW, cropH); 
      ctx3.putImageData(imgData,0,0, 0,0,cropW, cropH);  

      document.getElementById('myphoto').src = canvas3.toDataURL();
      //document.getElementById('myphoto').width  = 100;
      //document.getElementById('myphoto').height = 400;
    }

    function moveCanvas(e)
    {
        //console.log(e.keyCode);

        switch (e.keyCode) {
        case 37:
            corpStepW = corpStepW - 1;//alert('left');
            break;
        case 38:
            corpStepH = corpStepH - 1;//alert('up');
            break;
        case 39:
            corpStepW = corpStepW + 1; //alert('right');
            break;
        case 40:
            corpStepH = corpStepH + 1; //alert('down');
            break;
        }
      drawCanvas();

    }
    
 //----------- UPLOAD PHOTO Method ------------ //
 
function endsWith(str, suffix) {
        return str.indexOf(suffix, str.length - suffix.length) !== -1;
}
    
var rootPath = null;
function uploadPhoto(path)
{     
	 rootPath = path;
     var id = 'imgData';
     var url = path+'/business/printcard/registration/uploadImgData';

     var fileName = document.getElementById('imgData').value.toLowerCase();
     var fileValid = false;
     
     if( endsWith(fileName,".jpg") ||  endsWith(fileName,".jpeg"))
       { 
    	  fileValid = true;   
       }
     else
       {
    	 alert("Only jpeg or jpg are allowed");
    	 document.getElementById('imgData').value = '';
   	     return;   
       }
     
     $.ajaxFileUpload
       (
           {
               url:url, 
               secureuri:false,
               fileElementId:id,
               dataType: 'json',
               success: function (data, status)
               {
                   if(typeof(data.error) != 'undefined')
                   {   
                	   alert('Error#1'); 
                   }else
                   {
                      //alert('Upload success');
                      //alert('data.data = ' + data.data);
                       
                      
                      var tmpImage = new Image();
                      tmpImage.onload = function(){
                    	  
                    	  var w = this.width;
                    	  var h = this.height;
                    	  
                    	  
                    	  var canvas = document.getElementById('photo_canvas');
                    	  var ctx = canvas.getContext('2d');
                    	  
                    	  
                    	  canvas.setAttribute('width', w);
                    	  canvas.setAttribute('height', h); 
                          ctx.fillRect(0, 0, w, h);
                          ctx.drawImage(tmpImage, 0, 0, w, h); 
                          //alert('Done drawing image.');
                          beginPreview();
                          submitPhoto(rootPath);
                      }
                     
                      
                      tmpImage.src = rootPath+data.data; 
                      
                   }
               },
               error: function (data, status, e)
               {
                  alert('Error#2');

               }
           }
       );
   return false;
}

//--------------- CANON Method

function startCanon()
{
	var jsonCmd = "canon:{}";	  
	holo_send(jsonCmd);
}
    