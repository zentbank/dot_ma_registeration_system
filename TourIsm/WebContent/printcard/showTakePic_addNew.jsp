<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="UTF-8">
    <meta charset="utf-8">
    <meta name="viewport" content="target-densitydpi=device-dpi, width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta name="description" content="Metro UI CSS">
    <meta name="author" content="Sergey Pimenov">
    <meta name="keywords" content="windows 8, modern style, Metro UI, style, modern, css, framework">

    <link href="${pageContext.request.contextPath}/page/css/modern.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/modern-responsive.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/site.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/page/js/google-code-prettify/prettify.css" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment_langs.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/dropdown.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/accordion.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/buttonset.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/carousel.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/input-control.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/pagecontrol.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/rating.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-drag.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/calendar.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/sha256.js"></script>

    <script src="http://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/sha256.js"></script>
    <script src="http://crypto-js.googlecode.com/svn/tags/3.1.2/build/components/enc-base64-min.js"></script>
    <script src="${pageContext.request.contextPath}/printcard/printcard.js"></script>
    <script src="${pageContext.request.contextPath}/printcard/websocket.js"></script>
    <title>สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์</title>
<script>
function nextStep()
{
  var url = '../business/printcard/registration/showTakePic'
                         
                        +'?printCardId=0&licenseNo=0&traderType='+$('#traderType').val() 
                        +'&identityNo='+ $('#identityNo').val() 
                        +'&traderCategory='+$('#traderCategory').val();
  document.location.href = url;
}
</script>
</head>
<body class="metrouicss">

<div class="page secondary">

  <div class="page-header">
        <div class="page">
            <div class="page-header">
                <div class="page-header-content">
                    <c:if test="${param.traderType=='L'}">
                       <h3>ผู้นำเที่ยว</h3>
                    </c:if>
                    <c:if test="${param.traderType=='G'}">
                      <h3>มัคคุเทศก์</h3>
                    </c:if>
                </div>
            </div>
        </div>
  </div> 
      <div class="page-region">
        <div class="page-region-content">
          <form onsubmit="return false;">
           <div class="grid">
              <div class="row">
                   <div class="span3">
                      <label class="input-control text">
                          เลขที่บัตรประชาชน
                      </label>
                  </div>
                  
                  <div class="span6">
                      <div class="input-control text">
                       <input id="identityNo" type="text" value="">
                      </div>
                      
                  </div>

              </div>
              <div class="row">
                   <div class="span3">
                      <label class="input-control text">
                         <c:if test="${param.traderType=='L'}">
                           ประเภทผู้นำเที่ยว
                        </c:if>
                        <c:if test="${param.traderType=='G'}">
                          ประเภทมัคคุเทศก์
                        </c:if>
                      </label>
                  </div>
                  
                  <div class="span6">
                      <div class="input-control select">
                        <c:if test="${param.traderType=='L'}">
                           <select id="traderCategory">
                                   <option value="100">ตามกฎกระทรวงฯ</option>
                                   <option value="200">เป็นมัคคุเทศก์</option>
                                   <option value="300">ผ่านการอบรม</option>
                         </select> 
                        </c:if>
                        <c:if test="${param.traderType=='G'}">
                          <select id="traderCategory">
                             <option value="100">100-มัคคุเทศก์ทั่วไป (ต่างประเทศ)</option>
                             <option value="101">100-มัคคุเทศก์ทั่วไป (ไทย)</option>
                             <option value="200">200-มัคคุเทศก์เฉพาะ (ต่างประเทศ-เฉพาะพื้นที่)</option>
                             <option value="201">201-มัคคุเทศก์เฉพาะ (ไทย-เฉพาะพื้นที่)</option>
                             <option value="202">202-มัคคุเทศก์เฉพาะ (เดินป่า)</option>
                             <option value="203">203-มัคคุเทศก์เฉพาะ (ศิลปะวัฒนธรรม)</option>
                             <option value="204">204-มัคคุเทศก์เฉพาะ (ทางทะเล)</option>
                             <option value="205">205-มัคคุเทศก์เฉพาะ (ทะเลชายฝั่ง)</option>
                             <option value="206">206-มัคคุเทศก์เฉพาะ (แหล่งท่องเที่ยวธรรมชาติ)</option>
                             <option value="207">207-มัคคุเทศก์เฉพาะ (วัฒนธรรมท้องถิ่น)</option> 
                          </select> 
                        </c:if>
                      </div>
                      
                  </div>

              </div>
              <div class="row">
                   <div class="span3">
                     <input id="traderType" type="hidden" value="${param.traderType}">
                    <button onclick='nextStep();'>ดำเนินการต่อ</button>
                  </div>
                  
                  <div class="span6">
                      <div class="input-control text">
                       &nbsp;
                      </div>
                      
                  </div>

              </div>
            </div>
            </form>
            </div>
        </div>
      </div>
</div>

<!-- <form onsubmit="return false;"> -->

<!-- 
<table border="1"  style='width:50%'>
  <tr>
    <td>เลขที่บัตรประชาชน</td> 
    <td> <input id="identityNo" type="text" value=""> </td>
  </tr>
  
  <c:if test="${param.traderType=='L'}">
  <tr>
    <td>ประเภทผู้นำเที่ยว</td> 
    <td> <select id="traderCategory">
                   <option value="100">ตามกฎกระทรวงฯ</option>
                   <option value="200">เป็นมัคคุเทศก์</option>
                   <option value="300">ผ่านการอบรม</option>
         </select>  
    </td>
  </tr> 
  </c:if>
  
  <c:if test="${param.traderType=='G'}">
    <tr>
    <td>ประเภทมัคคุเทศก์</td> 
    <td> <select id="traderCategory">
                   <option value="100">100-มัคคุเทศก์ทั่วไป (ต่างประเทศ)</option>
                   <option value="101">100-มัคคุเทศก์ทั่วไป (ไทย)</option>
                   <option value="200">200-มัคคุเทศก์เฉพาะ (ต่างประเทศ-เฉพาะพื้นที่)</option>
                   <option value="201">201-มัคคุเทศก์เฉพาะ (ไทย-เฉพาะพื้นที่)</option>
                   <option value="202">202-มัคคุเทศก์เฉพาะ (เดินป่า)</option>
                   <option value="203">203-มัคคุเทศก์เฉพาะ (ศิลปะวัฒนธรรม)</option>
                   <option value="204">204-มัคคุเทศก์เฉพาะ (ทางทะเล)</option>
                   <option value="205">205-มัคคุเทศก์เฉพาะ (ทะเลชายฝั่ง)</option>
                   <option value="206">206-มัคคุเทศก์เฉพาะ (แหล่งท่องเที่ยวธรรมชาติ)</option>
                   <option value="207">207-มัคคุเทศก์เฉพาะ (วัฒนธรรมท้องถิ่น)</option> 
                </select> 
    </td>
  </tr>
  </c:if>
   
</table> -->

<!-- <input id="traderType" type="hidden" value="${param.traderType}">
<button onclick='nextStep();'>ดำเนินการต่อ</button> -->
<!-- </form> -->


</body>
</html>