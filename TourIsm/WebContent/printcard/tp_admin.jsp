<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="target-densitydpi=device-dpi, width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta name="description" content="Metro UI CSS">
    <meta name="author" content="Sergey Pimenov">
    <meta name="keywords" content="windows 8, modern style, Metro UI, style, modern, css, framework">

    <link href="${pageContext.request.contextPath}/page/css/modern.css" rel="stylesheet">

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment_langs.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/dropdown.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/accordion.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/buttonset.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/carousel.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/input-control.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/pagecontrol.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/rating.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-drag.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/calendar.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/sha256.js"></script>
<title>ออกแบบบัตร</title>
<style type="text/css">
.colName{
    color:blue;
    font-size:10pt;
}

body{
    margin:0;
    padding:0
}

</style>

 <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery-1.9.0.min.js"></script>
 <script>
 var previewWindow = null;
 var theCode = ""; //HTML code to preview
 var theDataJsonStr = "";

 $(document).ready(function(){
        loadHTML($('input[name="fileName"]:checked').val());
        $('[name=fileName]').on('change', function(){loadHTML(this.value)});
        $('[json=true]').on('change', function(){preview();});

    });

 /*load HTML code into textarea , so user can edit it*/
 function loadHTML(filename)
 {
        $.ajax({
              url: '${pageContext.request.contextPath}/printcard/'+filename+'?rd=' + new Date().getTime(),
              data: {},
              success: function (data, txtStatus, jqXHR){
                  $('#code').val(data);
                  preview();
              },
              dataType: 'html'
            });
 }

 function preview()
 {
     window.theCode = $('#code').val();
     window.theDataJsonStr = buildJSON();

     document.getElementById('previewFrame').src = '${pageContext.request.contextPath}/printcard/tp_admin_preview.jsp?rd='+ new Date().getTime();
 }



 function buildJSON()
 {
    var obj = {imgPath:'${pageContext.request.contextPath}/business/printcard/registration/img/template/'};

    $('[json=true]').each(function(x,y){
        obj[this.id] =  $(this).val() ;
    });

    return JSON.stringify(obj);

 }

 function save()
 {
     $.ajax({
          url: '${pageContext.request.contextPath}/lua?f=save_printcard_template',
          data: 'fileName='+$('input[name="fileName"]:checked').val()+'&html='+$('#code').val(),
          success: function (data, txtStatus, jqXHR){
             alert('success'+data);

            // test loading saved content
             $('#code').val('');
             loadHTML($('input[name="fileName"]:checked').val());
          },
          type:'POST',
          dataType: 'json'
        });
 }
 </script>
</head>
<body style="background-color:#f7f7f9;">
<!-- <h4 style="margin-left:5px;">ออกแบบบัตร</h4> -->

<table border='0' width='100%' style="margin-left:5px;margin-top:5px;">
<tr>
  <td id='editDiv' width='40%' >
    <div>
    <input type='radio' name='fileName' value='template1.html' checked>บัตรมัคคุเทศน์
    <input type='radio' name='fileName' value='template2.html'>บัตรผู้นำเที่ยว
     <button id='previewBtn' onclick='preview();'>แสดงตัวอย่าง</button>
     <button id='saveBtn' onclick="save();">บันทึก</button>
    </div>
    <div>&nbsp;</div>
    <textarea id='code' cols="80" rows="20"></textarea>
    <div>&nbsp;</div>

  </td>
  <td id='previewDiv' bgcolor='#f7f7f9' align='center' valign='middle'>
    <iframe id='previewFrame' frameBorder='0' style='width:100%;height:100%;margin:10px'></iframe></td>
</tr>

<!-- Dummy data form -->
<tr>
   <td colspan="2" bgcolor='#d5e7ec'>
      <!-- <h4>ข้อมูลทดสอบแสดงบนบัตร</h4> -->
      <table width='100%' border="0" >

        <tr id='userType'>
           <td>ประเภท(สี)</td>
           <td colspan="5">
               <select id="traderCategory" json="true">
                   <option value="100">100-มัคคุเทศก์ทั่วไป (ต่างประเทศ)</option>
                   <option value="101">100-มัคคุเทศก์ทั่วไป (ไทย)</option>
                   <option value="200">200-มัคคุเทศก์เฉพาะ (ต่างประเทศ-เฉพาะพื้นที่)</option>
                   <option value="201">201-มัคคุเทศก์เฉพาะ (ไทย-เฉพาะพื้นที่)</option>
                   <option value="202">202-มัคคุเทศก์เฉพาะ (เดินป่า)</option>
                   <option value="203">203-มัคคุเทศก์เฉพาะ (ศิลปะวัฒนธรรม)</option>
                   <option value="204">204-มัคคุเทศก์เฉพาะ (ทางทะเล)</option>
                   <option value="205">205-มัคคุเทศก์เฉพาะ (ทะเลชายฝั่ง)</option>
                   <option value="206">206-มัคคุเทศก์เฉพาะ (แหล่งท่องเที่ยวธรรมชาติ)</option>
                   <option value="207">207-มัคคุเทศก์เฉพาะ (วัฒนธรรมท้องถิ่น)</option>
                </select>
            </td>
        </tr>

        <tr>
            <td width='10%'>1.ชื่อ<br><span class='colName'>l_traderOwnerName</span></td><td><input type="text" json="true" id="l_traderOwnerName" value="สมชาย กสิกรไทย" size='25'></td>
            <td width='10%'>2.NAME<br><span class='colName'>l_traderOwnerNameEn</span></td><td><input type="text" json="true" id="l_traderOwnerNameEn" value="SOMCHAI KASIKORNTHAI" size='25'></td>
            <td width='10%'>3.ID NO.<br><span class='colName'>l_identityNo</span></td><td><input type="text" json="true" id="l_identityNo" value="3245644564567" size='25'></td>
        </tr>

        <tr>
            <td width='10%'>4.License NO.<br><span class='colName'>l_licenseNo</span></td><td><input type="text" json="true" id="l_licenseNo" value="11-00227" size='25'></td>
            <td width='10%'>5.วันหมดอายุ<br><span class='colName'>l_expireDate</span></td><td><input type="text" json="true" id="l_expireDate" value='27/11/2019' size="25"></td>
            <td width='10%'>6.จังหวัด<br><span class='colName'>l_traderArea</span></td><td><input type="text" json="true" id="l_traderArea" value='กรุงเทพมหานคร' size="25"></td>
        </tr>


      </table>
   </td>
</tr>
</table>

</body>
</html>