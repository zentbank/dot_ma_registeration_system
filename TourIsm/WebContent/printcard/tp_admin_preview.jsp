<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="target-densitydpi=device-dpi, width=device-width, initial-scale=1.0, maximum-scale=1">

    <script type="text/javascript" src="../page/js/assets/jquery-1.9.0.min.js"></script>

    <title>สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์</title>
<script type="text/javascript">
var all_items = [];
var activeItem = null;
var movedItems = {};

$(document).ready(function(){
    loadCardDiv(window.parent.theCode);
    $(document).on('keyup',moveItem);
        });

/** Load HTML card template , the HTML code will be created by IT-dept */
function loadCardDiv(html)
{
    $('body').html(html).hide();
    var userData = jQuery.parseJSON(window.parent.theDataJsonStr);
    setCardDivData(userData, null , null); 
}

/** put JSON card data into HTML template */
function setCardDivData(json, textStatus, jqXHR)
{
      var h = $('body').html();
      //json.l_traderOwnerName = 'โอวาท...ยาวยาวยาว+ยาวอีก'
      for(var p in json)
          {
    	    var val = ''+json[p];
    	    val = val.replace(/\.\.\./g, '<br/>'); //replace ... with <br/>
    	    val = val.replace(/\+/g, '<br/>'); //replace + with <br/>
    	    if(p != "l_licenseNo"){
    	    	h = h.replace(  new RegExp("\\{"+p+"\\}","g")  , val );
    	    }else{
    	    	if(json.traderType == 'L'){
    	    		h = h.replace(  new RegExp("\\{"+p+"\\}","g")  , json.licenseLeaderNo );
    	    	}else{
    	    		h = h.replace(  new RegExp("\\{"+p+"\\}","g")  , val );
    	    	}
    	    }
            
          }
      
      console.log(json);
      
 
      $('body').html(h).show();
      
	  // not available in edit template
	  
	  var photo = document.getElementById('myphoto');
	  if(window.parent.onBase64PhotoAvailable)
		{
		  //photo = document.getElementById('myphoto');
    	  photo.onload = function(){
    		    var imgData = document.getElementById('myphoto').src;
    		  
    		    var dataBase64 = imgData.match(/base64/g);

    		    if(!dataBase64)
    		    {
    		        var oColorImg = document.getElementById('myphoto');
    		        var oCanvas = document.createElement('CANVAS');
    		        var oCtx = oCanvas.getContext('2d');
    		        oCanvas.width = oColorImg.naturalWidth;
    		        oCanvas.height = oColorImg.naturalHeight;

    		        oCtx.drawImage(oColorImg, 0, 0);

    		        var imgBase64 = oCanvas.toDataURL("image/jpeg", 1.0);
    		        imgData = imgBase64; 
    	 		 }
    		    window.parent.onBase64PhotoAvailable(imgData);    
    	  }//end function
		}
	  
      if(window.parent.thePhoto)
    	{  
    	  photo.src = window.parent.thePhoto; //alert(window.parent.thePhoto)
    	}

	  
	  //attach onclick event
	  $('[item=true]').each(function (index, ele){
		  all_items[all_items.length] = ele;
		 $(this).on('click', function(){ 
			setActiveItem(this.id);
		 })
	  });
}

function setActiveItem(id)
{ 
	for(var i=0;i<all_items.length;i++)
	{
		var item = $(all_items[i]);
		$(item).css('border-style','none');
		$(item).css('border-width','0px'); 
	}
	
	var item = $(document.getElementById(id));
	$(item).css('border-style','solid');
	$(item).css('border-width','1px');
	
	activeItem = item
}

function moveItem(evt)
{
	if(!activeItem){return;}
	
	var item = activeItem;
	//console.log('move ' + item +' >>> '+ evt.which);
	
	//38,40,37,39 บน ล่าง ซ้าย ขวา
	var n = parseInt(evt.which)
	console.log('move to ' + item +' >>> '+ n);
	var x = 0;
	var y =0;
	var step = 5;
	
	if(n==38) y = -step;
	if(n==40) y = step;
	
	if(n==37) x = -step;
	if(n==39) x = step;
	
	applyMove(item, x , y)
}

function applyMove(item, x , y)
{
  var moved = false;
  if(x!=0)
	{
	    var oldX = parseInt(item.css('left').replace('px',''))
	    var newX = oldX + x;
	    item.css('left', newX+'px');
	    console.log('X update '+oldX+'  >>>> '+ newX);
	    moved = true;
	}
  
  if(y!=0)
	{
	    var oldY = parseInt(item.css('top').replace('px',''))
	    var newY = oldY + y;
	    item.css('top', newY+'px');
	    console.log('Y update '+oldY+'  >>>> '+ newY);
	    moved = true;
	}
  
  if(moved)
	{
	  if(window.parent.onItemMoved)
		{ window.parent.onItemMoved(item.attr('id'), item.css('top') , item.css('left'));  }
	  
	}
}
/**the photo data was receive from jRac (EditPhoto.jsp) */ 
function updatePhoto(croppedBase64)
{
	document.getElementById('myphoto').src = croppedBase64;
}

 

</script>
<style type="text/css">
body{
    margin:0;
    padding:0
}
@font-face {
    font-family: font_freedb;
    src: url(${pageContext.request.contextPath}/printcard/fonts/freedb.ttf);
}

@font-face {
    font-family: PSL-Kittithada;
    src: local('PSL-Kittithada')
}


</style>

</head>
<body >  
</body>