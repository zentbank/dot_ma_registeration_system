var holo_server = "ws://localhost:8887";
var holo_ws = null;
var DeviceError = false;

  
function holo_send(cmd)
{
	if(!holo_ws || holo_ws==null)
       holo_initWebSocket();

	holo_ws.send(cmd);
}


/******************* WebSocket functions *****************/

function holo_initWebSocket()
{
	  if (!window.WebSocket)
          alert("FATAL: WebSocket not natively supported. This remote controller will not work!");
	  
	  
	  var ws = new WebSocket(holo_server);
      ws.onopen = function() {
    	   console.log("[WebSocket#onopen]\n"); 
      };
      ws.onmessage = function(e) {
    	   console.log("[WebSocket#onmessage] Message: '" + e.data + "'\n");
    	  
    	  if(holo_receive)
			    { holo_receive(e); }
      };
      ws.onclose = function() {
    	  // console.log("[WebSocket#onclose]\n");
          holo_ws = null; 
          DeviceError = true;
      };
          
      holo_ws = ws;    
}