local lu = {}


lu.addMetaMap = function (m)
   if( type(m) == 'userdata' and getmetatable(m)==nil and type(m.put)=="function" ) then 
    local meta = {};
    meta['__index'] = function (obj , key)
              return obj:get(key);
            end;

    meta['__newindex'] = function (obj , key, val)
               return obj:put(key, val);
              end;
    setmetatable(m, meta);
  end
  
  if( type(m) == 'userdata' and getmetatable(m)==nil and type(m.getProperty)=="function" ) then 
    local meta = {};
    meta['__index'] = function (obj , key)
              return obj:getProperty(key);
            end;

    meta['__newindex'] = function (obj , key, val)
               return obj:setProperty(key, val);
              end;
    setmetatable(m, meta);
  end

  if( type(m) == 'userdata' and getmetatable(m)==nil and type(m.getParameter)=="function" ) then 
    local meta = {};
    meta['__index'] = function (obj , key)
    					return obj:getParameter(key);
					  end; 
    setmetatable(m, meta);
  end
  
  return m;
end

lu.checkPermission = function (screenId , action)
local perm = sess:getAttribute('user'):getPermission(screenId); 
if(perm==nil or not perm:isAuthorized(action))
  then
      util:Ex("You're not authorized to perform ".. action .." action on screen#".. screenId .."!\nplease contact admin")
  end
return true
end

return lu 

