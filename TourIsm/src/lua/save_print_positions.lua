function init()
print('save save_print_positions.init() is called')
req = lu.addMetaMap(req)

end

function run()
print('save save_print_positions.run() is called')
print('key = ', req.key , 'data = ', req.data)
controller:putValue(req.key , req.data)
print('done')
end
