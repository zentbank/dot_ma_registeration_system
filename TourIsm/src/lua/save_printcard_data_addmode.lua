function init()
print('save printcard data addmode.init() is called')
req = lu.addMetaMap(req)

end

function run()
print('save printcard data addmode.run() is called')
print('printcard_json_dir', printcard_json_dir , 'identityNo', req.identityNo , 'traderType' , req.traderType , 'traderCategory', req.traderCategory )


JSON = _G.loadfile("/lua/JSON.lua")() 
local t = {identityNo = req.identityNo , traderType = req.traderType , traderCategory = req.traderCategory , imgData = req.imgData}
 
local theFile = printcard_json_dir .. req.identityNo .. '_' .. req.traderType .. '_' .. req.traderCategory .. '.json'
io.output(theFile) 
io.write( JSON:encode_pretty(t) )
io.close()


print('---------- Saved printcard data to', theFile)
end
