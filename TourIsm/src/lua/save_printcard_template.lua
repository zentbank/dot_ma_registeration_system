function init()
print('save printcard template.init() is called')
req = lu.addMetaMap(req)

end

function run()
print('save printcard template.run() is called')
--lu.checkPermission('save_printcard_template','save')

local theFile = printcard_template_dir .. req.fileName

io.output(theFile) 
io.write(req.html)
io.close()


print('---------- Saved printcard template to', theFile)
end
