 package luaj;
 
import java.io.File;

//import main.EntryPoint;

import java.util.HashMap;
import java.util.Map;

import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;
import org.luaj.vm2.lib.jse.JsePlatform;

/** Represents a lua script  */
public class LuaScriptCLI {
    String lua_dev_dir = null;//"C://Users//sysnajar//Dropbox//Projects//AtmWeb//src//lua//{scriptFileName}.lua";
     
    /** The chunk. */
    private LuaValue chunk;
     
     /** The globals. */
    private Globals globals = JsePlatform.standardGlobals();
     
    /* stores .lua script in /src/lua/  */
    private static final String luaDir = "/lua/{scriptFileName}.lua";
     
    public LuaScriptCLI(String scriptFileName){
        init(scriptFileName);
    }
      
    
     
    private void init(String scriptFileName)
    { 
        //globals.set("env", CoerceJavaToLua.coerce(this));
        if(lua_dev_dir!=null)
            chunk = globals.loadfile(lua_dev_dir.replaceAll("\\{scriptFileName\\}", scriptFileName)); 
        else
            chunk = globals.loadfile(luaDir.replaceAll("\\{scriptFileName\\}", scriptFileName)); 
         
        chunk.call();
    }
     
    public static Varargs run(String scriptFileName, Map<String,Object> mapParams, String[] params)
    { 
        LuaScriptCLI lua = new LuaScriptCLI(scriptFileName); 
         if(params==null)
             params = new String[0];
         
        lua.globals.set("params", CoerceJavaToLua.coerce(params)); 
        
        for(String k : mapParams.keySet())
        {
        	lua.globals.set(k, CoerceJavaToLua.coerce(mapParams.get(k))); 	
        }
            
        LuaValue commonFn = lua.globals.loadfile(luaDir.replaceAll("\\{scriptFileName\\}", "common")).call();  
        lua.globals.set("lu", commonFn );
        
        lua.chunk = lua.globals.loadfile(luaDir.replaceAll("\\{scriptFileName\\}", scriptFileName)); 
         
        final LuaValue[] emptyArgs = new LuaValue[0];
        lua.globals.get("init").invoke(emptyArgs);
        return lua.globals.get("run").invoke(emptyArgs);  
    }
     
    
     
     
     
      
 
}
