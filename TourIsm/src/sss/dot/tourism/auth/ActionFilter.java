package sss.dot.tourism.auth;

import java.io.IOException;
import java.nio.charset.Charset;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sss.aut.service.User;
import com.sss.aut.service.UserPermission;

public class ActionFilter implements Filter {

	public void destroy() {
		// TODO Auto-generated method stub
		
	}

//	public void doFilter(ServletRequest arg0, ServletResponse arg1,
//			FilterChain arg2) throws IOException, ServletException {
//		// TODO Auto-generated method stub
//		
//	}
	
	 public synchronized void  doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
	  {
	    try
	    {
	      if (request instanceof HttpServletRequest)
	      {

	        HttpServletRequest req = (HttpServletRequest) request;

	        HttpServletResponse res = (HttpServletResponse) response;
	        
	        
	        String url = req.getRequestURI();
	        
	        
	
	        if ( (url.indexOf("page/") != -1) || (url.indexOf("login.jsp") != -1)|| (url.indexOf("pageError.jsp") != -1) || (url.indexOf("doLogin.htm") != -1) || (url.indexOf(".") == -1))
	        {
	          chain.doFilter(request, response);
	          
	        }
	        else
	        {
	          User user = (User) req.getSession(true).getAttribute("user");
	          if (user == null)
	          {
	            res.sendRedirect("login.jsp");
	          }
	          else
	          {	            
	            chain.doFilter(request, response);

	          }

	        }
	      }
	      
	      
	    }
	    catch (Exception e)
	    {
	    	e.printStackTrace();
	    	 HttpServletResponse res = (HttpServletResponse) response;
	    	res.sendRedirect("login.jsp");
	    }
	   

	  }

	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}
