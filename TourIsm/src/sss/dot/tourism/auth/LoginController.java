package sss.dot.tourism.auth;


 
import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import sss.dot.tourism.dto.mas.AdmUserDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;

import com.sss.aut.service.User;
 
@Controller
@RequestMapping("auth")
public class LoginController {
	
	@Autowired
	private sss.dot.tourism.service.admin.IUserAccountService userAccountService;
	
	@Autowired
	private HttpServletRequest httpreq;
	
	
	Logger logger = Logger.getLogger(this.getClass());
 
//	@RequestMapping(value = "/user/logout" , method = RequestMethod.GET )
//	public ModelAndView userLogout(HttpServletRequest req, HttpServletResponse resp)  throws Exception {
//		
//		req.getSession().removeAttribute("user"); 
//		return new ModelAndView ( "/login.jsp" );
//	}
	
	@RequestMapping(value = "/user/logout", method = RequestMethod.POST)
	public @ResponseBody String logout() {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			
			httpreq.getSession().removeAttribute("user"); 
			return jsonmsg.writeMessage();

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
			// return null;
		}
	}
	
	@RequestMapping(value = "/user/dologin", method = RequestMethod.POST)
	public @ResponseBody String doLogin(@ModelAttribute AdmUserDTO param) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			
			User user = (User)userAccountService.getUser(param.getUserLogin(), param.getLoginPassword());
			if(user != null)
			{
	
				httpreq.getSession(true).setAttribute("user", user);  
				return jsonmsg.writeMessage();
			}
			else
			{
				return jsonmsg.writeErrorMessage("กกรุณาตรวจสอบข้อมูล");
			}
			

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
			// return null;
		}
	}
	
}