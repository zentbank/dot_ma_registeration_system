package sss.dot.tourism.auth;

import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.http.IWriteMessagePerform;
import sss.dot.tourism.http.JSONPerform;
import sss.dot.tourism.http.JsonHttpMessage;

// Implements Filter class
public class UserAuthFilter implements Filter {

	private ArrayList urlList;

	public void destroy() {
	}

	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		String url = request.getServletPath();
		boolean allowedRequest = false;
		String strURL = "";

		// To check if the url can be excluded or not
		for (int i = 0; i < urlList.size(); i++) {
			strURL = urlList.get(i).toString();
			if (url.startsWith(strURL)) {
				allowedRequest = true;
			}
		}

		if (!allowedRequest) {
			
			HttpSession session = request.getSession(true);
			if (session == null
					|| session.getAttribute("user") == null) {
//				if(url.startsWith("welcome"))
//				{
//					// Forward the control to login.jsp if authentication fails
//					
//				}
//				else
//				{
//					System.out.println("IWriteMessagePerform");
//					
////					HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
////					
////					
////					IWriteMessagePerform perform = new JSONPerform();
////					perform.writeMessage(response, jsonmsg.writeErrorMessage("login_f"));
//				}
				
				request.getRequestDispatcher("/login.jsp").forward(request,
						response);
				
			}
		}
		chain.doFilter(req, res);
	}

	public void init(FilterConfig config) throws ServletException {
		// Read the URLs to be avoided for authentication check (From web.xml)
		String urls = config.getInitParameter("avoid-urls");
		StringTokenizer token = new StringTokenizer(urls, ",");
		urlList = new ArrayList();
		while (token.hasMoreTokens()) {
			urlList.add(token.nextToken());
		}
	}
}