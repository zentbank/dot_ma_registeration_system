package sss.dot.tourism.auth;

import java.io.UnsupportedEncodingException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import sss.dot.tourism.dto.deactivate.DeactivateLicenseDTO;
import sss.dot.tourism.dto.mas.AdmGroupDTO;
import sss.dot.tourism.dto.mas.AdmUserDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;

import com.sss.aut.service.User;
@Controller
@RequestMapping("welcome")
public class UserAuthenController {
		@Autowired
		private sss.dot.tourism.service.admin.IUserAccountService userAccountService;
		
		@Autowired
		private HttpServletRequest httpreq;
		
		
		Logger logger = Logger.getLogger(this.getClass());
		
		@RequestMapping(method = RequestMethod.POST)
		public ModelAndView viewEvents(String userName, String password, HttpServletRequest req, Model model) {

			try{
				
				User user = (User)userAccountService.getUser(userName, password);
				if(user != null)
				{
		
					req.getSession(true).setAttribute("user", user);  
					
					return new ModelAndView ( "/index.html" );
				}
				else
				{
					model.addAttribute("errMsg", "ไม่สามารถเข้าสู่ระบบได้กรุณาตรวจสอบข้อมูล");
					return new ModelAndView ( "/login.jsp" );
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
				return new ModelAndView ( "/login.jsp" );
			}
		}
		
		

}
