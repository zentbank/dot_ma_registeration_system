package sss.dot.tourism.controller.admuser;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import sss.dot.tourism.dto.deactivate.DeactivateLicenseDTO;
import sss.dot.tourism.dto.deactivate.MasDeactivateTypeDTO;
import sss.dot.tourism.dto.mas.AdmGroupDTO;
import sss.dot.tourism.dto.mas.AdmUserDTO;
import sss.dot.tourism.dto.punishment.PunishmentEvidenceDTO;
import sss.dot.tourism.dto.registration.TraderAddressDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.service.admin.IUserAccountService;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.User;

@Controller 
@RequestMapping("/admuser")
public class AdmUserController {
	
	@Autowired
	IUserAccountService userAccountService;
	
	@Autowired
	private HttpServletRequest req;

	@RequestMapping(value = "/admmenu/read", method = RequestMethod.POST)
	public @ResponseBody
	String readMenu(@ModelAttribute AdmGroupDTO param) {
	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			
			/*
			 * เจ้าหน้าที่รับผิดชอบเรื่อง
				1=กลุ่มเจ้าหน้าที่รับผิดชอบเรื่อง
				0=เมนูผู้ใช้งาน*/
			param.setOfficerGroup(0);
			List list = userAccountService.getAdmGroup(param, user);
			
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/officergroup/read", method = RequestMethod.POST)
	public @ResponseBody
	String readOfficerGroup(@ModelAttribute AdmGroupDTO param) {
	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");

			/*
			 * เจ้าหน้าที่รับผิดชอบเรื่อง
				1=กลุ่มเจ้าหน้าที่รับผิดชอบเรื่อง
				0=เมนูผู้ใช้งาน*/
			param.setOfficerGroup(1);
			List list = userAccountService.getAdmGroup(param, user);
			
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	
							  
	@RequestMapping(value = "/saveadmuser", method = RequestMethod.POST)
	public @ResponseBody
	String saveAdmuser(@ModelAttribute AdmUserDTO param) {
	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			
			if((param.getUserMenu() != null) && (!param.getUserMenu().isEmpty()))
			{
				Object[] object = (Object[]) jsonmsg.toArrayObject(
						AdmGroupDTO.class, param.getUserMenu());
				
				param.setListUserMenu(object);
			}
			
			if((param.getUserGroup() != null) && (!param.getUserGroup().isEmpty()))
			{
				Object[] object = (Object[]) jsonmsg.toArrayObject(
						AdmGroupDTO.class, param.getUserGroup());
				
				param.setListUserGroup(object);
			}

			userAccountService.saveAdmUser(param ,user);	
			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/read", method = RequestMethod.POST)
	public @ResponseBody String readAdmUser(@ModelAttribute AdmUserDTO param ) 
	{

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");
		
			
			List list = this.userAccountService.getAdmUserAll(
					param, 
					user,
					param.getStart(),
					param.getLimit()
					);
			
			Long numAll = new Long(userAccountService.countAdmUserAll(
					param, 
					user,
					0,
					Integer.MAX_VALUE
					).size());
			
			return jsonmsg.writePagingMessage(list, numAll);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody
	String deleteTraderAddress(@RequestBody String json) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			Object[] object = (Object[]) jsonmsg.toArrayObject(
					AdmUserDTO.class, json);

			this.userAccountService.deleteAdmUser(object ,user);

			return jsonmsg.writeMessage();
		} catch (Exception e) {

			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
			// return null;
		}

	}
	
	
	@RequestMapping(value = "/signature/save", method = RequestMethod.POST)
	public @ResponseBody String saveEvidence(@RequestParam("signatureFile") MultipartFile signature 
			,@RequestParam("userId") String userId
			) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			
			AdmUserDTO param = new AdmUserDTO();
			param.setSignatureFile(signature);
			param.setUserId(Long.parseLong(userId));

			this.userAccountService.saveSignature(param, user);
			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
}
