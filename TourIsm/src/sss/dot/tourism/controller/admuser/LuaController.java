package sss.dot.tourism.controller.admuser;
 
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import luaj.LuaScriptCLI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import sss.dot.tourism.dto.mas.AdmGroupDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.util.ConstantUtil;

@Controller  
public class LuaController {
	
	 
	@Autowired
	private HttpServletRequest req;
	
	public static Map globalMap = new HashMap();

	@RequestMapping(value = "/lua")
	public @ResponseBody
	String doLua(HttpServletRequest req, HttpServletResponse res,HttpSession sess) {
	    System.out.println("LuaController is called");
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			String luaFile = req.getParameter("f");
			String printcard_template_dir = sess.getServletContext().getRealPath("/printcard")+File.separator;
			
			Map<String, Object> m = new HashMap<String, Object>();
			m.put("req", req);
			m.put("res", res);
			m.put("sess", sess);
			m.put("printcard_template_dir", printcard_template_dir);
			m.put("printcard_json_dir", ConstantUtil.TEMPLATE_GUIDE_FILE_LOCATION);
			m.put("controller", this);
			
			
			LuaScriptCLI.run((luaFile==null)?"lua1":luaFile, m, new String[]{});
			
			List list = new ArrayList(); 
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	public void putValue(String key, Object value){
		globalMap.put(key,  value);
	}
	
	public Object getValue(String key){
		return globalMap.get(key);
	}
	 
}

