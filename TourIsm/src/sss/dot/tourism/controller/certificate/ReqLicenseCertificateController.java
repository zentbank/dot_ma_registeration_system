package sss.dot.tourism.controller.certificate;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import sss.dot.tourism.controller.report.ReportExcelBuilder;
import sss.dot.tourism.dto.certificate.ReqLicenseCertificateDTO;
import sss.dot.tourism.dto.deactivate.MasDeactivateTypeDTO;
import sss.dot.tourism.dto.punishment.PunishmentEvidenceDTO;
import sss.dot.tourism.dto.punishment.SuspensionAlertDTO;
import sss.dot.tourism.dto.registration.ReceiptDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.passport.dto.ReqPassportDTO;
import sss.dot.tourism.service.certificate.IReqLicenseCertificateService;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.User;


@Controller
@RequestMapping("/req/certificate")
public class ReqLicenseCertificateController {

	@Autowired
	private IReqLicenseCertificateService reqLicenseCertificateService;
	
	
	@Autowired
	private HttpServletRequest req;
	
	@RequestMapping(value = "/read", method = RequestMethod.POST)
	public @ResponseBody String read(@ModelAttribute ReqLicenseCertificateDTO param ) 
	{
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			
			User user = (User) req.getSession(true).getAttribute("user");

			param.setOrgId(user.getUserData().getOrgId());
			
			Long numAll = new Long(reqLicenseCertificateService.countAll(param, user, param.getStart(), param.getLimit()));
			
			List list = this.reqLicenseCertificateService.getByPage(param, user, param.getStart(), param.getLimit());
			
			return jsonmsg.writePagingMessage(list, numAll);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public @ResponseBody
	String save(@ModelAttribute ReqLicenseCertificateDTO param ) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			User user = (User) req.getSession(true).getAttribute("user");
			this.reqLicenseCertificateService.saveAll(param ,user);

			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
			// return null;
		}
	}
	
	@RequestMapping(value = "/readDocument", method = RequestMethod.POST)
	public @ResponseBody
	String readDocument(@ModelAttribute ReqLicenseCertificateDTO param) {

	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			List list = reqLicenseCertificateService.getDocuments(param, user);
			
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/readById", method = RequestMethod.POST)
	public @ResponseBody
	String readById(@ModelAttribute ReqLicenseCertificateDTO param) {

	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			Object obj = reqLicenseCertificateService.getById(param, user);
			
			return jsonmsg.writeMessage(obj);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	
	
	@RequestMapping(value = "/saveDocument", method = RequestMethod.POST)
	public @ResponseBody String saveDocument(@RequestParam("documentFile") MultipartFile documentFile 
			,@RequestParam("reqDocId") long reqDocId) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			
			User user = (User) req.getSession(true).getAttribute("user");
			this.reqLicenseCertificateService.saveDocument(reqDocId, documentFile, user);
			
			ReqLicenseCertificateDTO param = new ReqLicenseCertificateDTO();
			
			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/viewReqDoc", method = RequestMethod.GET)
	public void viewfile(HttpServletRequest req, HttpServletResponse resp,@RequestParam("reqDocId") long reqDocId){
		
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			ReqLicenseCertificateDTO param = new ReqLicenseCertificateDTO();
			param.setReqDocId(reqDocId);
			ReqLicenseCertificateDTO dto = (ReqLicenseCertificateDTO)this.reqLicenseCertificateService.getDocumentsById(param, user);
		
			
			String fileType = dto.getReqDocPath().substring(dto.getReqDocPath().lastIndexOf(".") + 1, dto.getReqDocPath().length());
			
			if(!fileType.equals("pdf"))
			{
				File f = new File(dto.getReqDocPath());
			
				resp.setContentType("image/" + fileType);
				BufferedImage bi = ImageIO.read(f);
				OutputStream out = resp.getOutputStream();
				ImageIO.write(bi, fileType, out);
				out.close();
			}
			else
			{
				File pdfFile = new File(dto.getReqDocPath());
				
				String fileName = dto.getReqDocPath().substring(dto.getReqDocPath().lastIndexOf("/") + 1, dto.getReqDocPath().length());

				resp.setContentType("application/pdf");
				resp.addHeader("Content-Disposition", "attachment; filename=" + fileName);
				resp.setContentLength((int) pdfFile.length());

				FileInputStream fileInputStream = new FileInputStream(pdfFile);
				OutputStream responseOutputStream = resp.getOutputStream();
				int bytes;
				while ((bytes = fileInputStream.read()) != -1) {
					responseOutputStream.write(bytes);
				
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
	
		}
	}
	
    @RequestMapping(value = "/docx/reqcertificateDocx" , method = RequestMethod.GET)
    public String suspensionDocx(@RequestParam("cerId") long cerId, ModelMap model )
    {
      	try {
	    	User user = (User) req.getSession(true).getAttribute("user");
	    	
	    	ReqLicenseCertificateDTO param = new ReqLicenseCertificateDTO();
	    	param.setCerId(cerId);
	    	
	    	Object dto = this.reqLicenseCertificateService.getReqCertificateDocx(param, user);
	    	model.addAttribute("reqcertificate", dto);
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		
        return "reqcertificateDocx";
    }
    
    
	@RequestMapping(value = "/readexcel", method = RequestMethod.GET)
	public  ModelAndView reportFeeTotalExcel (HttpServletRequest request, HttpServletResponse resp,
			@RequestParam("licenseNo") String licenseNo,
			@RequestParam("reqDateFrom") String reqDateFrom,
			@RequestParam("traderName") String traderName,
			@RequestParam("reqDateTo")String reqDateTo) throws Exception
	{
		User user = (User) request.getSession(true).getAttribute("user");
		
		ReqLicenseCertificateDTO param = new ReqLicenseCertificateDTO();
		param.setLicenseNo(licenseNo);
		param.setReqDateFrom(reqDateFrom);
		param.setReqDateTo(reqDateTo);
		param.setTraderName(traderName);

		resp.setContentType("application/vnd.ms-excel");
		resp.setHeader("Content-Disposition",
				"attachment; filename=รายงานการขอใบรับรองใบอนุญาต.xls");

		String templateFileName = "reqcertificate.xls";
		ReportExcelBuilder builder = new ReportExcelBuilder(
				templateFileName);
		builder.build(resp,this.reqLicenseCertificateService.getReqCertificateExcel(param, user));

		return null;
	}
}
