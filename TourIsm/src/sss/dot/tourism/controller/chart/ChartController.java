package sss.dot.tourism.controller.chart;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.service.chart.IChartService;

import com.sss.aut.service.User;

@Controller
@RequestMapping("/chart")

public class ChartController {
	@Autowired 
	IChartService chartService;
	@Autowired
	private HttpServletRequest httpreq;
	

	
	@RequestMapping(value = "/readChart", method = RequestMethod.POST)
	public @ResponseBody String readChart(@ModelAttribute RegistrationDTO param ) 
	{
		System.out.println("###Controller ChartController method readChartBusinessByTraderCategory");
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			
			User user = (User) httpreq.getSession(true).getAttribute("user");
			
			List list = this.chartService.getChartBusiness(param, user);
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/excel/print", method = RequestMethod.GET)
	  public void printDetail(@ModelAttribute RegistrationDTO dto ,HttpServletRequest req, HttpServletResponse resp)
	  {
		  System.out.println("###Controller ReportExcelController method printDetail");
	      try
	      {
	    	  
	    	  User user = (User) req.getSession(true).getAttribute("user");
	    	  
	    	String templateFileName = dto.getTemplateFileName(); //"chartdetail.xls";
	    	  
	        resp.setContentType("application/vnd.ms-excel");
	        resp.setHeader("Content-Disposition", "attachment; filename="+templateFileName);
//	        resp.setHeader("Content-Disposition", "attachment; filename=chartdetail.xls");

	        ReportExcelBuilder builder = new ReportExcelBuilder(templateFileName);
	        
//	        String approveDateFrom = req.getParameter("approveDateFrom");
//	        String approveDateTo = req.getParameter("approveDateTo");
	        
	        System.out.println("######ApproveDate = "+dto.getApproveDateFrom()+" "+dto.getApproveDateTo());
	        
	        builder.build(resp, this.chartService.getDetail(dto , dto.getStart(), dto.getLimit()));
	      }
	      catch(Exception e)
	      {
	        e.printStackTrace();
	      }
	   
	  }
	
	
	@RequestMapping(value = "/excel/licenseDetail", method = RequestMethod.GET)
	  public void printDetail(HttpServletResponse resp
			  ,@RequestParam("traderType") String traderType
			  ,@RequestParam("orgId") String orgId
			  ,@RequestParam("provinceId") String provinceId)
	  {
		  System.out.println("###Controller ChartController method printDetail");
	      try
	      {
	    	  
	    	  User user = (User) httpreq.getSession(true).getAttribute("user");
	    	  
	    	String templateFileName = "TourGuideLeaderDetailAll.xls"; 
	    	  
	    	resp.setContentType("application/vnd.ms-excel");
	    	resp.setHeader("Content-Disposition", "attachment; filename="+templateFileName);

	        ReportExcelBuilder builder = new ReportExcelBuilder(templateFileName);
	        
	        RegistrationDTO dto = new RegistrationDTO();
	        dto.setTraderType(traderType);
	        if((null != orgId) && (!orgId.isEmpty()))
	        {
	        	dto.setOrgId(Long.valueOf(orgId));
	        }
	        if((null != provinceId) && (!provinceId.isEmpty()))
	        {
	        	dto.setProvinceId(Long.valueOf(provinceId));
	        }
	        
	        builder.build(resp, this.chartService.getLicenseDetail(dto));
	      }
	      catch(Exception e)
	      {
	        e.printStackTrace();
	      }
	   
	  }
	
}









