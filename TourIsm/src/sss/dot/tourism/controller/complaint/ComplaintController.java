package sss.dot.tourism.controller.complaint;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import sss.dot.tourism.dto.complaint.ComplaintDocDTO;
import sss.dot.tourism.dto.complaint.ComplaintLicenseDTO;
import sss.dot.tourism.dto.complaint.ComplaintProgressDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.service.complaint.IComplaintService;
import sss.dot.tourism.service.sendmail.ISendMailService;

import com.sss.aut.service.User;


@Controller
@RequestMapping("/complaint")

public class ComplaintController {
	
	@Autowired 
	IComplaintService complaintService;
//	@Autowired
//	ISendMailService sendMailService;
	
	@RequestMapping(value = "/deleteDocument", method = RequestMethod.POST)
	public @ResponseBody
	String deleteDocument(@ModelAttribute ComplaintDocDTO param ,HttpServletRequest req) {
		System.out.println("###Controller ComplaintController Method deleteDocument");
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			User user = (User) req.getSession(true).getAttribute("user");
			complaintService.deleteDocument(param, user);
			
			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/createcomplaint", method = RequestMethod.POST)
	public @ResponseBody
	String createComplaint(@ModelAttribute ComplaintLicenseDTO param ,HttpServletRequest req) {
		System.out.println("###Controller ComplaintController Method createComplaint");
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			User user = (User) req.getSession(true).getAttribute("user");
			complaintService.createComplaint(param, user);
			
			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/fileupload", method = RequestMethod.POST)
	public @ResponseBody String upload(HttpServletRequest request, HttpServletResponse response, @RequestParam("file") MultipartFile f) 
	{

	    HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
	    try{
	    	User user = (User) request.getSession(true).getAttribute("user");
	    	
	    	ComplaintDocDTO param = new ComplaintDocDTO();
	    	if(!request.getParameter("complaintDocId").equals(""))
	    		param.setComplaintDocId(new Long(request.getParameter("complaintDocId")));
	    	else
	    		param.setComplaintDocId(new Long(0));
	    	param.setComplaintDocName(request.getParameter("complaintDocName"));
//	    	param.setComplaintDocPath(request.getParameter("complaintDocPath"));
	    	param.setComplaintDocType(request.getParameter("complaintDocType"));
	    	param.setComplaintLicenseId(new Long(request.getParameter("complaintLicenseId")));
	    	
	    	param.setTraderType(request.getParameter("traderType"));
	    	param.setLicenseNo(request.getParameter("licenseNo"));
	    	param.setQuestionType(request.getParameter("questionType"));

	    	this.complaintService.upload(param, f, user);

	    	return jsonmsg.writeMessage();
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    	return jsonmsg.writeErrorMessage(e.getMessage());
	    }
	
	}
	
	@RequestMapping(value = "/readDocument", method = RequestMethod.POST)
	public @ResponseBody String readDocument(@ModelAttribute ComplaintDocDTO param ,HttpServletRequest req) 
	{
		System.out.println("###Controller ComplaintController method readDocument");
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			List list = this.complaintService.getDocument(param, user);
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value="/viewfile", method=RequestMethod.GET )
	public void viewfile(HttpServletRequest req, HttpServletResponse resp){
		System.out.println("###viewfile");
		try {
			
			User user = (User) req.getSession(true).getAttribute("user");
			
			String separator=  java.io.File.separator;
			
			String complaintDocPath = req.getParameter("complaintDocPath");
			
			String fileType = complaintDocPath.substring(complaintDocPath.lastIndexOf(".") + 1, complaintDocPath.length());

			
			if(!fileType.equals("pdf"))
			{
				File f = new File(complaintDocPath);
			
				resp.setContentType("image/" + fileType);
				BufferedImage bi = ImageIO.read(f);
				OutputStream out = resp.getOutputStream();
				ImageIO.write(bi, fileType, out);
				out.close();
			}
			else
			{
				File pdfFile = new File(complaintDocPath);
				
				String fileName = complaintDocPath.substring(complaintDocPath.lastIndexOf("/") + 1, complaintDocPath.length());

				resp.setContentType("application/pdf");
				resp.addHeader("Content-Disposition", "attachment; filename=" + fileName);
				resp.setContentLength((int) pdfFile.length());

				FileInputStream fileInputStream = new FileInputStream(pdfFile);
				OutputStream responseOutputStream = resp.getOutputStream();
				int bytes;
				while ((bytes = fileInputStream.read()) != -1) {
					responseOutputStream.write(bytes);
				
				}
			}
		
			

			

			
		} catch (Exception e) {
			e.printStackTrace();
			
	
		}
	}
	
	//saveChangeQuestionType
	@RequestMapping(value = "/saveChangeQuestionType", method = RequestMethod.POST)
	public @ResponseBody String saveChangeQuestionType(@ModelAttribute ComplaintLicenseDTO param ,HttpServletRequest req) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			
			User user = (User) req.getSession(true).getAttribute("user");
			param.setOrgId(user.getUserData().getOrgId());

			this.complaintService.saveChangeQuestionType(param, user);

			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	//saveComplaintReceive
	@RequestMapping(value = "/saveComplaintReceive", method = RequestMethod.POST)
	public @ResponseBody String saveComplaintReceive(@ModelAttribute ComplaintLicenseDTO param ,HttpServletRequest req) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			
			User user = (User) req.getSession(true).getAttribute("user");
			param.setOrgId(user.getUserData().getOrgId());

			this.complaintService.saveComplaintReceive(param, user);
			
			//sek edit
//			System.out.println(param.getComplaintProgress());
//			System.out.println(param.getComplaintStatus());
//			if(param.getComplaintProgress().equals("RL") && param.getComplaintStatus().equals("G"))
//			{
//				System.out.println(param.getTraderId());
//				sendMailService.SendMailComplaint(param);
//			}
			//ens sek
			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	
	//readComplaint
	@RequestMapping(value = "/readComplaint", method = RequestMethod.POST)
	public @ResponseBody String readComplaint(@ModelAttribute ComplaintLicenseDTO param ,HttpServletRequest req) 
	{

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			User user = (User) req.getSession(true).getAttribute("user");
			param.setOrgId(user.getUserData().getOrgId());
			
			Long numAll = new Long(complaintService.getComplaintPaging(
					param, 
					user,
					0,
					Integer.MAX_VALUE).size());
			
			List list = this.complaintService.getComplaintPaging(
					param, 
					user, 
					param.getStart(), 
					param.getLimit());
			
			return jsonmsg.writePagingMessage(list, numAll);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/readComplaintProgressByConsider", method = RequestMethod.POST)
	public @ResponseBody String readComplaintProgressByConsider(@ModelAttribute ComplaintProgressDTO param ,HttpServletRequest req) 
	{
		System.out.println("###Controller ComplaintController method readComplaintProgressByConsider");
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			
			User user = (User) req.getSession(true).getAttribute("user");
			List list = this.complaintService.getComplaintProgressByConsider(param, user);
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
//	saveComplaintProgressConsider
	@RequestMapping(value = "/saveComplaintProgressConsider", method = RequestMethod.POST)
	public @ResponseBody String saveComplaintProgressConsider(@ModelAttribute ComplaintProgressDTO param ,HttpServletRequest req) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			User user = (User) req.getSession(true).getAttribute("user");
			this.complaintService.saveComplaintProgressConsider(param, user);
			
			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/deleteComplaintProgressByConsider", method = RequestMethod.POST)
	public @ResponseBody String deleteComplaintProgressByConsider(@RequestBody String json ,HttpServletRequest req) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			User user = (User) req.getSession(true).getAttribute("user");
			Object[] object = (Object[]) jsonmsg.toArrayObject(ComplaintProgressDTO.class, json);

			this.complaintService.delete(object ,user);

			return jsonmsg.writeMessage();
		} catch (Exception e) {

			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
			// return null;
		}

	}
	
	//saveQuestion
		@RequestMapping(value = "/saveQuestion", method = RequestMethod.POST)
		public @ResponseBody String saveQuestion(@ModelAttribute ComplaintLicenseDTO param ,HttpServletRequest req) {

			HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
			try {
				
				User user = (User) req.getSession(true).getAttribute("user");
				param.setOrgId(user.getUserData().getOrgId());

				this.complaintService.saveQuestion(param, user);
				
				return jsonmsg.writeMessage(param);

			} catch (Exception e) {
				e.printStackTrace();
				return jsonmsg.writeErrorMessage(e.getMessage());
			}
		}
		
		
		@RequestMapping(value = "/readComplaintProgressAll", method = RequestMethod.POST)
		public @ResponseBody String readComplaintProgressAll(@ModelAttribute ComplaintProgressDTO param ,HttpServletRequest req) 
		{
			System.out.println("###Controller ComplaintController method readComplaintProgressAll");
			HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
			try {
				User user = (User) req.getSession(true).getAttribute("user");
				List list = this.complaintService.readComplaintProgressAll(param, user);
				return jsonmsg.writeMessage(list);

			} catch (Exception e) {
				e.printStackTrace();
				return jsonmsg.writeErrorMessage(e.getMessage());
		
			}
		}
		
		
		public static void main(String arg[])
		{
			String path = "test/folder/wer.jpg";
			String path1 = "test/folder/wer.pdf";
			
			String fileType = path1.substring(path1.lastIndexOf(".") + 1, path1.length());
			System.out.println(fileType);
		}
	
}











