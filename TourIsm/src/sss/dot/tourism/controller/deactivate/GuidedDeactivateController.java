package sss.dot.tourism.controller.deactivate;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import sss.dot.tourism.dto.deactivate.DeactivateLicenseDTO;
import sss.dot.tourism.dto.deactivate.MasDeactivateTypeDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.service.deactivate.IDeactivateLicenseService;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.User;

@Controller 
@RequestMapping("/deactivate/guide")
public class GuidedDeactivateController {
	@Autowired
	IDeactivateLicenseService deactivateLicenseService;
	
	@Autowired
	private HttpServletRequest req;

	@RequestMapping(value = "/deactivatetype/read", method = RequestMethod.POST)
	public @ResponseBody
	String readAct(@ModelAttribute MasDeactivateTypeDTO param) {
//	String readAct() {
	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			User user = (User) req.getSession(true).getAttribute("user");
			param.setLicenseType(TraderType.GUIDE.getStatus());
			List list = deactivateLicenseService.getDeactivateTypeAll(param, user);
			
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/prepare", method = RequestMethod.POST)
	public @ResponseBody
	String readTraderPrepareRevoke(@ModelAttribute DeactivateLicenseDTO param) {
	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");

			DeactivateLicenseDTO dto = (DeactivateLicenseDTO)deactivateLicenseService.getTraderPrepareDeactivate(param, user);	
			return jsonmsg.writeMessage(dto);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/adddeactivate", method = RequestMethod.POST)
	public @ResponseBody
	String saveRevoke(@ModelAttribute DeactivateLicenseDTO param) {
	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			
			Object[] object = (Object[]) jsonmsg.toArrayObject(
					MasDeactivateTypeDTO.class, param.getMasAct());

			deactivateLicenseService.saveDeactivation(param, object ,user);	
			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/read", method = RequestMethod.POST)
	public @ResponseBody String readSuspension(@ModelAttribute DeactivateLicenseDTO param ) 
	{

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			
			param.setTraderType(TraderType.GUIDE.getStatus());
			

			
			List list = this.deactivateLicenseService.getAll(
					param, 
					user
					);
			
			param.setStart(0);
			param.setLimit(Integer.MAX_VALUE);
			Long numAll = new Long(deactivateLicenseService.countAll(
					param, 
					user
					).size());
			
			return jsonmsg.writePagingMessage(list, numAll);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/activate", method = RequestMethod.POST)
	public @ResponseBody
	String cancelsuspend(@ModelAttribute DeactivateLicenseDTO param) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");


			this.deactivateLicenseService.activate(param ,user);

			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
			// return null;
		}
	}
	
	@RequestMapping(value = "/deactivate", method = RequestMethod.POST)
	public @ResponseBody
	String revoke(@ModelAttribute DeactivateLicenseDTO param) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");

			Object[] object = (Object[]) jsonmsg.toArrayObject(
					MasDeactivateTypeDTO.class, param.getMasAct());
			this.deactivateLicenseService.deActivate(param ,object,user);

			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
			// return null;
		}
	}
	

}
