package sss.dot.tourism.controller.guarantee;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import sss.dot.tourism.dto.registration.GuaranteeDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.service.guarantee.IGuaranteeService;

import com.sss.aut.service.User;


@Controller 
@RequestMapping("/refundguarantee")
public class GuaranteeController {
	
	@Autowired
	IGuaranteeService guaranteeService;
	
	@Autowired
	private HttpServletRequest req;
	
	private boolean refundGuarantee = true;
	private boolean changeGuarantee = false;
	
	@RequestMapping(value = "/read", method = RequestMethod.POST)
	public @ResponseBody String readGuarantee(@ModelAttribute GuaranteeDTO param ) 
	{

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			

			
			List list = this.guaranteeService.getAll(
					param, 
					user,
					refundGuarantee
					);
			
			param.setStart(0);
			param.setLimit(Integer.MAX_VALUE);
			Long numAll = new Long(guaranteeService.countAll(
					param, 
					user,
					refundGuarantee
					).size());
			
			return jsonmsg.writePagingMessage(list, numAll);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public @ResponseBody
	String save(@ModelAttribute GuaranteeDTO param) {
	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");

			guaranteeService.save(param ,user);	
			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/change/read", method = RequestMethod.POST)
	public @ResponseBody String readGuaranteeForChange(@ModelAttribute GuaranteeDTO param ) 
	{

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			

			
			List list = this.guaranteeService.getAll(
					param, 
					user,
					changeGuarantee
					);
			
			param.setStart(0);
			param.setLimit(Integer.MAX_VALUE);
			Long numAll = new Long(guaranteeService.countAll(
					param, 
					user,
					changeGuarantee
					).size());
			
			return jsonmsg.writePagingMessage(list, numAll);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/change/save", method = RequestMethod.POST)
	public @ResponseBody
	String saveChangeGuarantee(@ModelAttribute GuaranteeDTO param) {
	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");

			guaranteeService.saveChangeGuarantee(param ,user);	
			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
}
