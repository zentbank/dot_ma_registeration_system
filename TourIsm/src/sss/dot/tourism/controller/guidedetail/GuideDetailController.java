package sss.dot.tourism.controller.guidedetail;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import sss.dot.tourism.dto.guidedetail.GuideDetailDTO;
import sss.dot.tourism.dto.guidedetail.GuideForeignLanguageDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.service.guidedetail.IGuideDetailService;

import com.sss.aut.service.User;


@Controller
@RequestMapping("/guidedetail")

//guide/guidedetail/createguidedetail
public class GuideDetailController {
	
	@Autowired
	IGuideDetailService guidedetailService;
	
	@Autowired
	private HttpServletRequest httpreq;
	
	@RequestMapping(value = "/searchlicenseguidedetail", method = RequestMethod.POST)
	public @ResponseBody String searchLicenseGuideDetail(@ModelAttribute GuideDetailDTO param ,HttpServletRequest req) 
	{
		System.out.println("class GuideDetailController method searchLicenseGuideDetail");
		
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			guidedetailService.searchLicenseGuideDetail(param, (User) req.getSession(true).getAttribute("user"));
			
			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/removeguidedetail", method = RequestMethod.POST)
	public @ResponseBody String deleteGuideDetail(@ModelAttribute GuideDetailDTO param ,HttpServletRequest req) 
	{
		System.out.println("class GuideDetailController method deleteGuideDetail");
		
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			guidedetailService.deleteGuideDetail(param, (User) req.getSession(true).getAttribute("user"));
			
			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/key/read", method = RequestMethod.POST)
	public @ResponseBody String readGuideDetail(@ModelAttribute GuideDetailDTO param ) 
	{

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

		    User user = (User) httpreq.getSession(true).getAttribute("user");			

			Long numAll = new Long(guidedetailService.countGuideDetail(
					param, 
					user,
					0,
					Integer.MAX_VALUE).size());
			
			List list = this.guidedetailService.getGuideDetail(
					param, 
					user, 
					param.getStart(), 
					param.getLimit());
			
			return jsonmsg.writePagingMessage(list, numAll);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	
	@RequestMapping(value = "/createguidedetail", method = RequestMethod.POST)
	public @ResponseBody String createGuideDetail(@ModelAttribute GuideDetailDTO param ,HttpServletRequest req) 
	{
		System.out.println("class GuideDetailController method createGuideDetail");
		
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			guidedetailService.createGuideDetail(param, (User) req.getSession(true).getAttribute("user"));
			
			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/saveAllGuideDetail", method = RequestMethod.POST)
	public @ResponseBody String saveAllGuideDetail(@ModelAttribute GuideDetailDTO param ,HttpServletRequest req) 
	{
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			guidedetailService.saveAllGuideDetail(param, (User) req.getSession(true).getAttribute("user"));
			
			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/foreignlanguage/read", method = RequestMethod.POST)
	public @ResponseBody String readGuideForeignLanguage(@ModelAttribute GuideForeignLanguageDTO param) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
			List list = this.guidedetailService.getAll(param, user);
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
//	guide/guidedetail/foreignlanguage/create
	@RequestMapping(value = "/foreignlanguage/create", method = RequestMethod.POST)
	public @ResponseBody
	String createGuideForeignLanguage(@RequestBody String json) {

		System.out.println("#####Controller createGuideForeignLanguage");
		
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
			Object[] object = (Object[]) jsonmsg.toArrayObject(GuideForeignLanguageDTO.class, json);

			System.out.println("#####json = "+json);
			
			List list = this.guidedetailService.create(object ,user);
			
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/foreignlanguage/update", method = RequestMethod.POST)
	public @ResponseBody String updateGuideForeignLanguage(@RequestBody String json) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
			Object[] object = (Object[]) jsonmsg.toArrayObject(GuideForeignLanguageDTO.class, json);

			this.guidedetailService.update(object ,user);

			return jsonmsg.writeMessage();
		} catch (Exception e) {

			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}

	}
	
	@RequestMapping(value = "/foreignlanguage/delete", method = RequestMethod.POST)
	public @ResponseBody
	String deleteGuideForeignLanguage(@RequestBody String json) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
			Object[] object = (Object[]) jsonmsg.toArrayObject(GuideForeignLanguageDTO.class, json);

			this.guidedetailService.delete(object ,user);

			return jsonmsg.writeMessage();
		} catch (Exception e) {

			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
			// return null;
		}

	}

}
















