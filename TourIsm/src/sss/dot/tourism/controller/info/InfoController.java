package sss.dot.tourism.controller.info;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import sss.dot.tourism.controller.report.PDFReport;
import sss.dot.tourism.controller.report.ReportExcelBuilder;
import sss.dot.tourism.dao.registration.RegistrationDAO;
import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.domain.Registration;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.service.info.IInfoService;
import sss.dot.tourism.util.LicenseStatus;

import com.sss.aut.service.User;

@Controller
@RequestMapping("/info")
public class InfoController {
	
	public final String MIME_PDF = "application/pdf";
	@Autowired 
	ServletContext context;
	
	@Autowired
	IInfoService infoService;
	
	//Oat Add 26/09/57
	@Autowired
	TraderDAO traderDAO;
	
	@Autowired
	RegistrationDAO registrationDAO;
	//
	
	@Autowired
	private HttpServletRequest req;
	
	@RequestMapping(value = "/read", method = RequestMethod.POST)
	public @ResponseBody String readInfoPaging(@ModelAttribute RegistrationDTO param ) 
	{
		System.out.println("###Controller InfoController method readInfo");
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			
			Long numAll = new Long(infoService.countInfoPaging(
					param, 
					user,
					0,
					Integer.MAX_VALUE).size());

			List list = this.infoService.getInfoPaging(
					param, 
					user, 
					param.getStart(), 
					param.getLimit());
			
			return jsonmsg.writePagingMessage(list, numAll);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	//Oat Add 13/11/57
	@RequestMapping(value = "/readCheckDueDateFee", method = RequestMethod.POST)
	public @ResponseBody String readInfoCheckDueDateFeeByPaging(@ModelAttribute RegistrationDTO param ) 
	{
		System.out.println("###Controller InfoController method readInfoCheckDueDateFeeByPaging");
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			
			Long numAll = new Long(infoService.infoCheckDueDateFeeByPaging(
					param, 
					user,
					0,
					Integer.MAX_VALUE).size());

			List list = this.infoService.getInfoCheckDueDateFeeByPaging(
					param, 
					user, 
					param.getStart(), 
					param.getLimit());
			
			return jsonmsg.writePagingMessage(list, numAll);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	//
	
	//Oat Add 17/11/57
	@RequestMapping(value = "/printCheckDueDateFeeExcel", method = RequestMethod.GET)
	public @ResponseBody String printInfoCheckDueDateFeeExcel
			(
			@RequestParam("expireDueDateFeeStatus") String expireDueDateFeeStatus
			,@RequestParam("orgId") int orgId
			,@RequestParam("traderType") String traderType
			,@RequestParam("expireDateFrom") String expireDateFrom
			,@RequestParam("expireDateTo") String expireDateTo
			,HttpServletRequest request, HttpServletResponse resp) throws Exception
	{
		System.out.println("###Controller InfoController method printInfoCheckDueDateFeeExcel");
		User user = (User) req.getSession(true).getAttribute("user");
		RegistrationDTO param = new RegistrationDTO();
		param.setExpireDueDateFeeStatus(expireDueDateFeeStatus);
		param.setOrgId(orgId);
		param.setTraderType(traderType);
		param.setExpireDateFrom(expireDateFrom);
		param.setExpireDateTo(expireDateTo);
		
		resp.setContentType("application/vnd.ms-excel");
		resp.setHeader("Content-Disposition", "attachment; filename=reportprintInfoCheckDueDateFee.xls");
		
		String templateFileName = "reportprintInfoCheckDueDateFee.xls";
		Map model = new HashMap();

		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		list = this.infoService.getInfoCheckDueDateFeeByPaging(param, user, 0, Integer.MAX_VALUE);
		System.out.println("###report list.size() = "+list.size());
		model.put("registrationType", "ธุรกิจนำเที่ยว");
		model.put("regPrintLicenseBusiness", list);
		
		ReportExcelBuilder builder = new ReportExcelBuilder(templateFileName);
//		builder.build(resp,this.printReportFeeBusinessGuideService.getPrintReportFeeBusienessGuide(dto, user));
		builder.build(resp, model);

		return null;
	}
	//
	
	@RequestMapping(value = "/reorganize/read", method = RequestMethod.POST)
	public @ResponseBody String readReorganizePaging(@ModelAttribute RegistrationDTO param ) 
	{
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			
			Long numAll = new Long(infoService.countReoeganizePaging(
					param, 
					user,
					0,
					Integer.MAX_VALUE).size());

			List list = this.infoService.getReoeganizePaging(
					param, 
					user, 
					param.getStart(), 
					param.getLimit());
			
			return jsonmsg.writePagingMessage(list, numAll);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	
	@RequestMapping(value = "/reorganize/save", method = RequestMethod.POST)
	public @ResponseBody
	String saveAllRegistration(@ModelAttribute RegistrationDTO param ,HttpServletRequest req) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			infoService.saveReorganizeLicense(param, (User) req.getSession(true).getAttribute("user"));
			
			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage("ไม่สามารถบันทึกข้อมูลได้");
		}
	}
	
	//Oat Add 24/09/57
	@RequestMapping(value = "/reportPrintLicenseInfoDocx", method = RequestMethod.GET)
	public ModelAndView printLicenseInfoDocx(@ModelAttribute RegistrationDTO param , HttpServletRequest request, HttpServletResponse resp) throws Exception
	{
		System.out.println("#####Controller InfoController Method printLicenseInfoDocx");
		System.out.println("###traderId = "+param.getTraderId());
		System.out.println("###regId = "+param.getRegId());
		System.out.println("###personId = "+param.getPersonId());
		
		User user = (User) req.getSession(true).getAttribute("user");
		
		//Model
		ModelMap model = new ModelMap();

		String  reportInfoDocxStr = this.infoService.printLicenseBusinessInfoDocx(model, param, user);
		
//		List list = new ArrayList();
//		RegistrationDTO dto2 = new RegistrationDTO();
//		dto2.setAddressNo("555");
//		list.add(dto2);
//		list.add(dto2);
//		model.addAttribute("listRegistration",list);
//		model.addAttribute("listReg",list);

		return new ModelAndView(reportInfoDocxStr, model);
	
//		MemoDataReportDTO dto = new MemoDataReportDTO();
//		try{
//			IReportDocxService service = (IReportDocxService) this.getApplicationContext().getBean("printDocxService");
//			
//			String payCentralAddStr = req.getParameter("payCentralAddStr").toString();
//			String memoIdStr = req.getParameter("memoId").toString();
//			String memType = req.getParameter("memType").toString();
//			BigDecimal memoId = new BigDecimal(memoIdStr);
//			
//			System.out.println("payCentralAddStr = "+payCentralAddStr);
//			System.out.println("memoId = "+memoId);
//			System.out.println("memType = "+memType);
//			
//			MemoDataReportDTO param = new MemoDataReportDTO();
//			param.setPayCentralAddStr(payCentralAddStr);
//			param.setMemoId(memoId);
//			param.setMemType(memType);
//			
//			//service
//			dto = service.printMemberEraseMemoDocx(model, param);
//		
//		}catch(Exception ex){
//			ex.printStackTrace();
//		}
//		
//		return new ModelAndView(dto.getReportDocxStr(),model);
		
	}
	
//	//Oat Add 24/09/57 ไม่ได้ใช้แล้ว ยังไม่สมบูรณ์
//	@RequestMapping(value = "/reportPrintLicenseInfoPdf", method = RequestMethod.GET)
//	public ModelAndView printLicenseInfoPDF(@ModelAttribute RegistrationDTO param , HttpServletRequest request, HttpServletResponse resp) throws Exception
//	{
//		User user = (User) req.getSession(true).getAttribute("user");
//		
//		System.out.println("traderId = "+param.getTraderId());
//		System.out.println("regId = "+param.getRegId());
//		
//		Trader trader = (Trader)this.traderDAO.findByPrimaryKey(param.getTraderId());
//		Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(param.getTraderId());
//		
////			this.printLicenseService.updatePrintLicneseById(param, null);
//		
//		Map<String, Object> model = new HashMap<String, Object>();
//		model.put("licenseNo", trader.getLicenseNo());
//		model.put("licenseStatusName", LicenseStatus.getMeaning(trader.getLicenseStatus()));
//		model.put("traderName", trader.getTraderName());
//		model.put("traderNameEn", trader.getTraderNameEn());
//		model.put("pronunciationName", trader.getPronunciationName());
//		
//		List list = new ArrayList();
//		RegistrationDTO dto = new RegistrationDTO();
//		dto.setAddressNo("555");
//		list.add(dto);
//		list.add(dto);
////		model.put("dataSource", list);
//		//-----
//		try
//		{
//			List render = new ArrayList();
//			Map map = new HashMap();
//			render.add(model);
//	
//			String sep = java.io.File.separator; /* sep === "/" */
//
//			//directory file
//			String jasperFile = null;
//			
//			jasperFile = sep + "WEB-INF" + sep + "reports" + sep+ "printLicenseBusinessInfo.jasper";
//			System.out.println("##########jasperFile = "+jasperFile);
//			
//			String jasperName = getReportPath(jasperFile);
//			System.out.println("##########jasperName = "+jasperName);
//			//
//	
//			PDFReport report1 = new PDFReport(jasperName, map);
//			 
//			List jasperPrintList = new ArrayList();
//			jasperPrintList.add(report1.getJasperPrint(new JRMapCollectionDataSource(render)));
//			
//			//resp.setContentType("application/msword");
//			resp.setContentType(this.MIME_PDF);
//
//			JRPdfExporter exporter = new JRPdfExporter();
//			exporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST, jasperPrintList);
//			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, resp.getOutputStream());
//			exporter.setParameter(JRPdfExporterParameter.IS_CREATING_BATCH_MODE_BOOKMARKS, Boolean.TRUE);
//			exporter.exportReport();
//	
//		} catch (Exception e)
//		{
//			System.out.println("display stack trace in the browser ");
//			StringWriter stringWriter = new StringWriter();
//			PrintWriter printWriter = new PrintWriter(stringWriter);
//			e.printStackTrace(printWriter);
//			resp.setContentType("text/plain");
//			resp.getOutputStream().print("");
//			e.printStackTrace();
//		}
//		return null;
//	}
//	//End Oat Add 24/09/57
	
	public String getReportPath(String jasperFile)
	{
		String realPath = this.context.getRealPath(jasperFile);

		System.out.println("******realPath = " + realPath);

		File realFile = new File(realPath);

		String absolutePath = realFile.getAbsolutePath();

		System.out.println("****absolutePath = " + absolutePath);

		return absolutePath;
	}
	
	//Oat Add 16/02/58
		@RequestMapping(value = "/readAccountWillCancle", method = RequestMethod.POST)
		public @ResponseBody String readInfoAccountWillCancleByPaging(@ModelAttribute RegistrationDTO param ) 
		{
			System.out.println("###Controller InfoController method readInfoAccountWillCancleByPaging");
			HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
			try {
				User user = (User) req.getSession(true).getAttribute("user");
				
				Long numAll = new Long(infoService.getInfoAccountWillCancleByPaging(
						param, 
						user,
						0,
						Integer.MAX_VALUE).size());

				List list = this.infoService.getInfoAccountWillCancleByPaging(
						param, 
						user, 
						param.getStart(), 
						param.getLimit());
				
				return jsonmsg.writePagingMessage(list, numAll);

			} catch (Exception e) {
				e.printStackTrace();
				return jsonmsg.writeErrorMessage(e.getMessage());
		
			}
		}
		//

}
