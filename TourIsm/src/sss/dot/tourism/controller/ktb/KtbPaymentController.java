package sss.dot.tourism.controller.ktb;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import sss.dot.tourism.controller.report.PDFReport;
import sss.dot.tourism.dto.ktb.KtbPaymentDTO;
import sss.dot.tourism.dto.mas.MasDocumentDTO;
import sss.dot.tourism.dto.registration.BranchDTO;
import sss.dot.tourism.dto.registration.ReceiptDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.service.ktb.IKtbPaymentService;
import sss.dot.tourism.service.printreceiptslip.IPrintReceiptSlipService;

import com.sss.aut.service.User;

@Controller
@RequestMapping("/ktb/payment")
public class KtbPaymentController {
	
	public final String MIME_PDF = "application/pdf";
	
	@Autowired
	private IKtbPaymentService ktbPaymentService;
	@Autowired
	private HttpServletRequest httpreq;
	
	
	
	@Autowired
	ServletContext context;
	

	
//	@SuppressWarnings("unchecked")
//	@RequestMapping(value = "/print", method = RequestMethod.GET)
//	public ModelAndView printReceiptSlipPDF(HttpServletRequest request, HttpServletResponse resp,
//			@RequestParam("orgId") String orgId,
//			@RequestParam("receiveOfficerDate") String receiveOfficerDate,
//			@RequestParam("receiveOfficerName") String receiveOfficerName,
//			@RequestParam("authority")String authority) throws Exception
//	{
//		
//		ReceiptDTO params = new ReceiptDTO();
//		params.setOrgId(Long.valueOf(orgId));
//		params.setReceiveOfficerDate(receiveOfficerDate);
//		params.setReceiveOfficerName(receiveOfficerName);
//		params.setAuthority(authority);
//		
//		User user = (User) request.getSession(true).getAttribute("user");
//		
//		FileInputStream fis = null;
//		try
//		{
//			List<Map<String, ?>> receiptSlips = this.ktbPaymentService.getPrintPayment(params, user);
//
//			List render = new ArrayList();
//			Map map = new HashMap();
//			
//			if(!receiptSlips.isEmpty())
//			{
//				for(Map model: receiptSlips)
//				{
//					if((null != model.get("signature")) && (!model.get("signature").toString().isEmpty()))
//					{
//						File file = new File(model.get("signature").toString());
//						
//						fis = new FileInputStream(file);
//						model.put("signature", fis);
//						
//					}
//					
//					render.add(model);
//				}
//			}
//			
//			String sep = java.io.File.separator; /* sep === "/" */
//			
//			List jasperPrintList = new ArrayList();
//			
//			String jasperFile = null;
//			String jasperName = null;
//			String jasperFileEx = null;
//			String jasperNameEx = null;
//
//			if(params.getOrgId() == 1)
//			{
//				jasperFile = sep + "WEB-INF" + sep + "reports" + sep
//						+ "printReceiptSlipSignature.jasper";
//				
//				 
//			}else{
//				jasperFile = sep + "WEB-INF" + sep + "reports" + sep
//						+ "printReceiptSlipEx.jasper";
//				jasperFileEx = sep + "WEB-INF" + sep + "reports" + sep
//						+ "printReceiptSlipEx1.jasper";
//				jasperNameEx = getReportPath(jasperFileEx);
//			}
//			
//			
//
//			
//			jasperName = getReportPath(jasperFile);
//			
//			PDFReport report1 = new PDFReport(jasperName, map);
//			jasperPrintList.add(report1
//					.getJasperPrint(new JRMapCollectionDataSource(render)));
//			
//			if(jasperNameEx!=null)
//			{
//				PDFReport report2 = new PDFReport(jasperNameEx, map);
//				for(int i=1;i<=2;i++)
//				{
//				jasperPrintList.add(report2
//						.getJasperPrint(new JRMapCollectionDataSource(render)));
//				}
//			}
//			
//			resp.setContentType(this.MIME_PDF);
//
//			JRPdfExporter exporter = new JRPdfExporter();
//			exporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST,
//					jasperPrintList);
//			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
//					resp.getOutputStream());
//			exporter.setParameter(
//					JRPdfExporterParameter.IS_CREATING_BATCH_MODE_BOOKMARKS,
//					Boolean.TRUE);
//			exporter.exportReport();
//
//		} catch (Exception e)
//		{
//			System.out.println("display stack trace in the browser ");
//			StringWriter stringWriter = new StringWriter();
//			PrintWriter printWriter = new PrintWriter(stringWriter);
//			e.printStackTrace(printWriter);
//			resp.setContentType("text/plain");
//			resp.getOutputStream().print("");
//			e.printStackTrace();
//		}
//		finally {
//			try {
//				if (fis != null)
//					fis.close();
//			} catch (IOException ex) {
//				ex.printStackTrace();
//			}
//		}
//		return null;
//	}
	
	public String getReportPath(String jasperFile)
	{
		String realPath = this.context.getRealPath(jasperFile);

		System.out.println("******realPath = " + realPath);

		File realFile = new File(realPath);

		String absolutePath = realFile.getAbsolutePath();

		System.out.println("****absolutePath = " + absolutePath);

		return absolutePath;
	}
	
//	@RequestMapping(value = "/read", method = RequestMethod.POST)
//	public @ResponseBody String readDocumentRegistration(@ModelAttribute KtbPaymentDTO param ) 
//	{
//		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
//		try {
//			User user = (User) httpreq.getSession(true).getAttribute("user");
//			
//			List list = ktbPaymentService.getAll(param, user);
//			return jsonmsg.writeMessage(list);
//
//		} catch (Exception e) {
//			e.printStackTrace();
//			return jsonmsg.writeErrorMessage(e.getMessage());
//		}
//	}

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public @ResponseBody String upload(HttpServletRequest request, HttpServletResponse response, @RequestParam("file") MultipartFile f) 
	{

	    HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
	    try{
	    	User user = (User) request.getSession(true).getAttribute("user");
	    	KtbPaymentDTO param = new KtbPaymentDTO();
	    	
	    	
	    	this.ktbPaymentService.upload(param, f, user);

	    	return jsonmsg.writeMessage(param);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    	return jsonmsg.writeErrorMessage(e.getMessage());
	    }
	
	}
	
	@RequestMapping(value = "/readPayment", method = RequestMethod.POST)
	public @ResponseBody String readPayment(@RequestParam("progressDate") String progressDate) 
	{
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
			
			List list = ktbPaymentService.findAttachPaymentFiles(progressDate);
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	@RequestMapping(value = "/readPaymentDetail", method = RequestMethod.POST)
	public @ResponseBody String readPaymentDetail(@ModelAttribute KtbPaymentDTO param) 
	{
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
			
//			List list = ktbPaymentService.findAttachPaymentFilesDetail(progressDate, paymentNo);
			List list = ktbPaymentService.findAttachPaymentFilesDetail(param);
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
//	@RequestMapping(value = "/savePaymentDetail", method = RequestMethod.POST)
//	public @ResponseBody String savePaymentDetail(@ModelAttribute KtbPaymentDTO param) 
//	{
//
//	    HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
//	    try{
//	    	User user = (User) httpreq.getSession(true).getAttribute("user");
////	    	KtbPaymentDTO param = new KtbPaymentDTO();
//	    	
//	    	
//	    	this.ktbPaymentService.savePaymentDetail(param);
//
//	    	return jsonmsg.writeMessage(param);
//	    }
//	    catch(Exception e)
//	    {
//	    	e.printStackTrace();
//	    	return jsonmsg.writeErrorMessage(e.getMessage());
//	    }
//	
//	}
	
	@RequestMapping(value = "/savePayments", method = RequestMethod.POST)
	public @ResponseBody String savePayments(@RequestParam("data") String json) 
	{

	    HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
	    try{
	    	User user = (User) httpreq.getSession(true).getAttribute("user");
	    	KtbPaymentDTO param = new KtbPaymentDTO();
	    	
	    	KtbPaymentDTO[] object = (KtbPaymentDTO[]) jsonmsg.toArrayObject(
					KtbPaymentDTO.class, json);
	    	
	    	System.out.println("json = " + json);
//	    	Thread.sleep(3000);
	    	
	    	this.ktbPaymentService.savePaymentDetail(object);

	    	return jsonmsg.writeMessage(param);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    	return jsonmsg.writeErrorMessage(e.getMessage());
	    }
	
	}
}
