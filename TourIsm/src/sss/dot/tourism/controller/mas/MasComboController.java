package sss.dot.tourism.controller.mas;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import sss.dot.tourism.dto.mas.AdmUserDTO;
import sss.dot.tourism.dto.mas.CountryDTO;
import sss.dot.tourism.dto.mas.ForeignLanguageDTO;
import sss.dot.tourism.dto.mas.MasAmphurDTO;
import sss.dot.tourism.dto.mas.MasBankDTO;
import sss.dot.tourism.dto.mas.MasComplaintTypeDTO;
import sss.dot.tourism.dto.mas.MasEducationLevelDTO;
import sss.dot.tourism.dto.mas.MasPositionDTO;
import sss.dot.tourism.dto.mas.MasPrefixDTO;
import sss.dot.tourism.dto.mas.MasProvinceDTO;
import sss.dot.tourism.dto.mas.MasTambolDTO;
import sss.dot.tourism.dto.mas.MasUniversityDTO;
import sss.dot.tourism.dto.mas.OrganizationDTO;
import sss.dot.tourism.dto.training.EducationDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.service.mas.IComboService;
import sss.dot.tourism.service.mas.IOrganizationService;

import com.sss.aut.service.User;



@Controller
@RequestMapping("/combo")
public class MasComboController {
	
	@Autowired
	IOrganizationService organizationService;
	@Autowired
	IComboService comboService;
	
	@Autowired
	private HttpServletRequest req;

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public @ResponseBody String createUser() {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

//			Object[] object = (Object[]) jsonmsg.toArrayObject(
//					PersonalDTO.class, json);
			OrganizationDTO dto = new OrganizationDTO();
			dto.setOrgName("สำนักงานทะเบียน");
			
			OrganizationDTO[] object = {dto};

			List list = this.organizationService.create((Object[])object);

			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/prefix", method = RequestMethod.POST)
	public @ResponseBody String readPrefix(@ModelAttribute MasPrefixDTO param) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			List list = this.comboService.getPrefix(param);
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	@RequestMapping(value = "/position", method = RequestMethod.POST)
	public @ResponseBody String readPosition(@ModelAttribute MasPositionDTO param) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			List list = this.comboService.getPosition(param);
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/postfix", method = RequestMethod.POST)
	public @ResponseBody String readPostfix() {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			List list = this.comboService.getPostfix();
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/province", method = RequestMethod.POST)
	public @ResponseBody String readProvince(@ModelAttribute MasProvinceDTO param) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			List list = this.comboService.getProvince(param);
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/amphur", method = RequestMethod.POST)
	public @ResponseBody String readAmphur(@ModelAttribute MasAmphurDTO param) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			List list = this.comboService.getAmphur(param);
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/tambol", method = RequestMethod.POST)
	public @ResponseBody String readTambol(@ModelAttribute MasTambolDTO param) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			List list = this.comboService.getTambol(param);
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/university", method = RequestMethod.POST)
	public @ResponseBody String readUniversity(@ModelAttribute MasUniversityDTO param) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			List list = this.comboService.getUniversity(param);
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/country", method = RequestMethod.POST)
	public @ResponseBody String readConuntry(@ModelAttribute CountryDTO param) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			List list = this.comboService.getCountry(param);
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/language", method = RequestMethod.POST)
	public @ResponseBody String readLanguage(@ModelAttribute ForeignLanguageDTO param) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			List list = this.comboService.getForeignLanguage(param);
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/educationlevel", method = RequestMethod.POST)
	public @ResponseBody String readEducationLevel(@ModelAttribute MasEducationLevelDTO param) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			List list = this.comboService.getEducationLevel(param);
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/bank", method = RequestMethod.POST)
	public @ResponseBody String readBank(@ModelAttribute MasBankDTO param) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			List list = this.comboService.getBank(param);
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/course", method = RequestMethod.POST)
	public @ResponseBody String readCourse(@ModelAttribute EducationDTO param) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			System.out.println("find course with param: "+param);
			List list = this.comboService.getCourse(param);
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	
	@RequestMapping(value = "/organization", method = RequestMethod.POST)
	public @ResponseBody String readOrganization(@ModelAttribute OrganizationDTO param) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			List list = this.comboService.getOrganization(param);
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	
	@RequestMapping(value = "/complainttype", method = RequestMethod.POST)
	public @ResponseBody String readComplaintType(@ModelAttribute MasComplaintTypeDTO param) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			List list = this.comboService.getComplaintType(param);
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	
	@RequestMapping(value = "/admuser", method = RequestMethod.POST)
	public @ResponseBody String readAdmUser(@ModelAttribute AdmUserDTO param) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			List list = this.comboService.getAdmUser(param);
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/officergroup", method = RequestMethod.POST)
	public @ResponseBody String readOfficergroup(@ModelAttribute AdmUserDTO param) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			
			User user = (User) req.getSession(true).getAttribute("user");

			List list = this.comboService.getOfficerGroup(param, user);
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
}
