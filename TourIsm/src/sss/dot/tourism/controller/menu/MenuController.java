package sss.dot.tourism.controller.menu;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.service.menu.IMenuService;

import com.sss.aut.service.User;


@Controller 
@RequestMapping("/menu")
public class MenuController {
	
	@Autowired
	IMenuService menuService;
	
	@RequestMapping(value = "/read", method = RequestMethod.POST)
	public @ResponseBody String read(HttpServletRequest req) {
	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			User user = (User) req.getSession(true).getAttribute("user");
			List list = menuService.getMenu(user);
			
			return jsonmsg.writeTree(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}

}
