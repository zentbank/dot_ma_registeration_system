package sss.dot.tourism.controller.printaddress;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sss.aut.service.User;

import sss.dot.tourism.controller.report.PDFReport;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.dto.registration.TraderAddressDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.service.printaddress.IPrintAddressService;


@Controller
@RequestMapping("/printaddress/registration")
public class PrintAddressController {
	public final String MIME_PDF = "application/pdf";
	protected static Logger logger = Logger.getLogger("controller");
	@Autowired
	ServletContext context;
	@Autowired
	IPrintAddressService printAddressService;
	
	@RequestMapping(value = "/readform", method = RequestMethod.GET)
	public  String readPrintAddressForm (@RequestParam("licenseNoFrom") String licenseNoFrom,ModelMap model)
	{
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try{
			
			System.out.println("licenseNoFrom = " + licenseNoFrom);
			HttpServletRequest request = null;
			HttpServletResponse resp = null;
			RegistrationDTO param = new RegistrationDTO();
			param.setLicenseNoFrom(licenseNoFrom);
			List<RegistrationDTO> list = (ArrayList<RegistrationDTO>)this.printAddressService.getPrintAddressByLicenseNo(param, null);

			RegistrationDTO dto = new RegistrationDTO();
			if(!list.isEmpty())
			{
				dto  = list.get(0);
				System.out.println("ss="+dto.getTraderOwnerName());
				//this.printLicenseTestPDF(dto, request, resp);
			}
			//this.printAddressService
			
			//model.addAttribute("model", dto);
			
			/*if(dto.getTraderType().equals("B"))
			{
				return "/page/printLicenseForm.jsp";
			}else
			{
				return "/page/printLicenseGuideForm.jsp";
			}*/
			return null;
		}catch (Exception e){
			e.printStackTrace();
			return "/page/pageerror.jsp";
		}
	}
	
	@RequestMapping(value = "/readform/{licenseNoFrom}/{licenseNoTo}", method = RequestMethod.GET)
	public  String readPrintAddressFormTo (@PathVariable long printLicenseId, ModelMap model)
	{
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try{
			
			System.out.println("printLicenseId = " + printLicenseId);
			RegistrationDTO param = new RegistrationDTO();
			param.setPrintLicenseId(printLicenseId);
			List<RegistrationDTO> list = (ArrayList<RegistrationDTO>)this.printAddressService.getPrintAddressByLicenseNo(param, null);
			
			RegistrationDTO dto = new RegistrationDTO();
			if(!list.isEmpty())
			{
				dto  = list.get(0);
			}
			
			model.addAttribute("model", dto);
			
			if(dto.getTraderType().equals("B"))
			{
				return "/page/printLicenseForm.jsp";
			}else
			{
				return "/page/printLicenseGuideForm.jsp";
			}
			
		}catch (Exception e){
			e.printStackTrace();
			return "/page/pageerror.jsp";
		}
	}

	@RequestMapping(value = "/printAddressPdf", method = RequestMethod.GET)
	public ModelAndView printLicenseTestPDF(@RequestParam("licenseNoFrom") String licenseNoFrom,@RequestParam("licenseNoTo") String licenseNoTo,HttpServletRequest request, HttpServletResponse resp) throws Exception
	{
		RegistrationDTO dto = new RegistrationDTO();
		dto.setLicenseNoFrom(licenseNoFrom);
		if(licenseNoTo!=null)
		{
			dto.setLicenseNoTo(licenseNoTo);
		}
		licenseNoFrom = licenseNoFrom.substring(0, 3);
		System.out.println(licenseNoFrom);
		List<RegistrationDTO> list = (ArrayList<RegistrationDTO>)this.printAddressService.getPrintAddressByLicenseNo(dto, null);
		try
		{
			List render = new ArrayList();

		if(!list.isEmpty())
		{
			for(RegistrationDTO param : list)
			{
			Map<String, Object> model = new HashMap<String, Object>();
		
			String address = param.getTraderOwnerName()+" ("+param.getLicenseNo()+") "
							+"\n"+param.getTraderAddress();
			model.put("licenseNo", param.getLicenseNo());
			model.put("traderOwnerName", param.getTraderOwnerName());
			model.put("firstNameEn", param.getFirstNameEn());
			model.put("identityNo", param.getIdentityNo());
			model.put("traderCategoryName", param.getTraderCategoryName());
			model.put("traderName", param.getTraderName());
			model.put("traderNameEn", param.getTraderNameEn());
			model.put("pronunciationName", param.getPronunciationName());
			model.put("addressNo", param.getAddressNo());
			model.put("soi", param.getSoi());
			model.put("roadName", param.getRoadName());
			model.put("postCode", param.getPostCode());
			model.put("tambolName", param.getTambolName());
			model.put("amphurName", param.getAmphurName());
			model.put("provinceName", param.getProvinceName());
			model.put("day", param.getDay());
			model.put("month", param.getMonth());
			model.put("year", param.getYear());
			model.put("dayF", param.getDayF());
			model.put("monthF", param.getMonthF());
			model.put("yearF", param.getYearF());
			model.put("traderAddress", address);
			render.add(model);
		}
	}




			
			Map map = new HashMap();
			

			String sep = java.io.File.separator; /* sep === "/" */
			List jasperPrintList = new ArrayList();
			
			String jasperFile = null;

		    jasperFile = sep + "WEB-INF" + sep + "reports" + sep
						+ "printAddress.jasper";

			String jasperName = getReportPath(jasperFile);

			PDFReport report1 = new PDFReport(jasperName, map);
			jasperPrintList.add(report1
					.getJasperPrint(new JRMapCollectionDataSource(render)));

			

			//resp.setContentType("application/msword");
			resp.setContentType(this.MIME_PDF);
			
			
			
			JRPdfExporter exporter = new JRPdfExporter();
			exporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST,
					jasperPrintList);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
					resp.getOutputStream());
			exporter.setParameter(
					JRPdfExporterParameter.IS_CREATING_BATCH_MODE_BOOKMARKS,
					Boolean.TRUE);
			exporter.exportReport();

		} catch (Exception e)
		{
			System.out.println("display stack trace in the browser ");
			StringWriter stringWriter = new StringWriter();
			PrintWriter printWriter = new PrintWriter(stringWriter);
			e.printStackTrace(printWriter);
			resp.setContentType("text/plain");
			resp.getOutputStream().print("");
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	@RequestMapping(value = "/printAddressByLicensePdf", method = RequestMethod.GET)
	public @ResponseBody
	String printAddressByLicensePdf(@RequestParam("licenseNo") String licenseNo,HttpServletRequest request, HttpServletResponse resp) throws Exception {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
	
			User user = (User) request.getSession(true).getAttribute("user");
			Object[] object = (Object[]) jsonmsg.toArrayObject(
					RegistrationDTO.class, licenseNo);

			
			try
			{
				List<RegistrationDTO> list = (ArrayList<RegistrationDTO>)this.printAddressService.getPrintAddressByLicenseNo(object);
				List render = new ArrayList();

			if(!list.isEmpty())
			{
				for(RegistrationDTO param : list)
				{
				Map<String, Object> model = new HashMap<String, Object>();
				
				System.out.println(param.getTraderOwnerName() + param.getTraderOwnerName() + param.getTraderOwnerName());
			
				String address = param.getTraderOwnerName()+" ("+param.getLicenseNo()+") "
								+"\n"+param.getTraderAddress();
				model.put("licenseNo", param.getLicenseNo());
				model.put("traderOwnerName", param.getTraderOwnerName());
				model.put("firstNameEn", param.getFirstNameEn());
				model.put("identityNo", param.getIdentityNo());
				model.put("traderCategoryName", param.getTraderCategoryName());
				model.put("traderName", param.getTraderName());
				model.put("traderNameEn", param.getTraderNameEn());
				model.put("pronunciationName", param.getPronunciationName());
				model.put("addressNo", param.getAddressNo());
				model.put("soi", param.getSoi());
				model.put("roadName", param.getRoadName());
				model.put("postCode", param.getPostCode());
				model.put("tambolName", param.getTambolName());
				model.put("amphurName", param.getAmphurName());
				model.put("provinceName", param.getProvinceName());
				model.put("day", param.getDay());
				model.put("month", param.getMonth());
				model.put("year", param.getYear());
				model.put("dayF", param.getDayF());
				model.put("monthF", param.getMonthF());
				model.put("yearF", param.getYearF());
				model.put("traderAddress", address);
				render.add(model);
			}
		}




				
				Map map = new HashMap();
				

				String sep = java.io.File.separator; /* sep === "/" */
				List jasperPrintList = new ArrayList();
				
				String jasperFile = null;

			    jasperFile = sep + "WEB-INF" + sep + "reports" + sep
							+ "printAddress.jasper";

				String jasperName = getReportPath(jasperFile);

				PDFReport report1 = new PDFReport(jasperName, map);
				jasperPrintList.add(report1
						.getJasperPrint(new JRMapCollectionDataSource(render)));

				

				//resp.setContentType("application/msword");
				resp.setContentType(this.MIME_PDF);
				
				
				
				JRPdfExporter exporter = new JRPdfExporter();
				exporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST,
						jasperPrintList);
				exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
						resp.getOutputStream());
				exporter.setParameter(
						JRPdfExporterParameter.IS_CREATING_BATCH_MODE_BOOKMARKS,
						Boolean.TRUE);
				exporter.exportReport();

			} catch (Exception e)
			{
				System.out.println("display stack trace in the browser ");
				StringWriter stringWriter = new StringWriter();
				PrintWriter printWriter = new PrintWriter(stringWriter);
				e.printStackTrace(printWriter);
				resp.setContentType("text/plain");
				resp.getOutputStream().print("");
				e.printStackTrace();
			}
			return null;
	}

	public String getReportPath(String jasperFile)
	{
		String realPath = this.context.getRealPath(jasperFile);

		System.out.println("******realPath = " + realPath);

		File realFile = new File(realPath);

		String absolutePath = realFile.getAbsolutePath();

		System.out.println("****absolutePath = " + absolutePath);

		return absolutePath;
	}
}
