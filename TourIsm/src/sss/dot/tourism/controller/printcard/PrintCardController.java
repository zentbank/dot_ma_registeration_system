package sss.dot.tourism.controller.printcard;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import sss.dot.tourism.controller.report.ReportExcelBuilder;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.dto.uploadpicture.UpLoadPictureDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.service.file.FileManage;
import sss.dot.tourism.service.printcard.IPrintCardService;
import sss.dot.tourism.util.ConstantUtil;
import sss.dot.tourism.util.ImageUtils;
import sss.dot.tourism.util.TraderCategory;
import sss.dot.tourism.util.TraderType;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.sss.aut.service.User;

@Controller
@RequestMapping("/printcard/registration")
public class PrintCardController {
	public final String MIME_PDF = "application/pdf";
	protected static Logger logger = Logger.getLogger("controller");
	@Autowired
	ServletContext context;
	
	@Autowired
	IPrintCardService printCardService;
	
	@Autowired
	private HttpServletRequest httpreq;
	
	
	@RequestMapping(value = "/read/guide", method = RequestMethod.POST)
	public @ResponseBody 
	String readPrintCardGuide(@ModelAttribute RegistrationDTO param)
	{
		System.out.println("GUIDE");
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try{
			User user = (User) httpreq.getSession(true).getAttribute("user");
//			param.setOrgId(user.getUserData().getOrgId());
			param.setTraderType("G");
			param.setApplicationContextPath(httpreq.getContextPath());
			Long numAll = new Long(printCardService.countPrintCard(
					param, 
					user,
					0,
					Integer.MAX_VALUE).size());
			
			List list = this.printCardService.getPrintCard(
					param, 
					user, 
					param.getStart(), 
					param.getLimit());
			
			return jsonmsg.writePagingMessage(list, numAll);
			
		}catch (Exception e){
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/read/tourleader", method = RequestMethod.POST)
	public @ResponseBody 
	String readPrintCardTourLeader(@ModelAttribute RegistrationDTO param)
	{
		System.out.println("TOURLEADER");
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try{
			User user = (User) httpreq.getSession(true).getAttribute("user");
//			param.setOrgId(user.getUserData().getOrgId());
			param.setTraderType("L");
			param.setApplicationContextPath(httpreq.getContextPath());
			Long numAll = new Long(printCardService.getPrintCard(
					param, 
					user,
					0,
					Integer.MAX_VALUE).size());
			
			List list = this.printCardService.getPrintCard(
					param, 
					user, 
					param.getStart(), 
					param.getLimit());
			
			return jsonmsg.writePagingMessage(list, numAll);
			
		}catch (Exception e){
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	@RequestMapping(value = "/uploadPicture", method = RequestMethod.POST)
	public @ResponseBody String upload(HttpServletRequest request, HttpServletResponse response, @RequestParam("file") MultipartFile f) 
	{

	    HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
	    try{
	    	User user = (User) request.getSession(true).getAttribute("user");
	    	UpLoadPictureDTO param = new UpLoadPictureDTO();
	    	param.setTraderId(new Long(request.getParameter("traderId")));
	    	
	    	param.setPersonImgId(new Long(request.getParameter("personImgId")));
	    	
	    	
	    	this.printCardService.upload(param, f, user);

	    	return jsonmsg.writeMessage();
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    	return jsonmsg.writeErrorMessage(e.getMessage());
	    }
	
	}
	
	@RequestMapping(value="/viewfile", method=RequestMethod.GET )
	public void viewfile(HttpServletRequest req, HttpServletResponse resp){
		System.out.println("###viewfile");
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			resp.setContentType("image/jpeg");
			String separator=  java.io.File.separator;
			
			String imgPath = req.getParameter("imgPath");

			File f = new File(imgPath);
			
			BufferedImage bi = ImageIO.read(f);
			OutputStream out = resp.getOutputStream();
			ImageIO.write(bi, "jpg", out);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
			
	
		}
	}
	
	@RequestMapping(value="/printCardExcel",method=RequestMethod.GET)
	public ModelAndView printCardExcel(@RequestParam("licenseNoFrom") String licenseNoFrom,@RequestParam("licenseNoTo") String licenseNoTo
			,@RequestParam("approveDateFrom") String approveDateFrom,@RequestParam("approveDateTo") String approveDateTo
			,@RequestParam("firstName") String firstName,@RequestParam("lastName") String lastName
			,@RequestParam("identityNo") String identityNo,@RequestParam("registrationType") String registrationType
			,@RequestParam("traderCategory") String traderCategory,@RequestParam("printCardStatus") String printCardStatus
			,@RequestParam("traderType") String traderType,@RequestParam("orgId") String orgId
			,HttpServletRequest request, HttpServletResponse resp) throws Exception
	{
		RegistrationDTO params = new RegistrationDTO();
		User user = (User) request.getSession(true).getAttribute("user");
//		params.setOrgId(user.getUserData().getOrgId());
		
		if((licenseNoFrom!=null) && (!licenseNoFrom.equals("")))
		{
			params.setLicenseNoFrom(licenseNoFrom);
		}
		
		if((licenseNoTo!=null) && (!licenseNoTo.equals("")))
		{
			params.setLicenseNoTo(licenseNoTo);
		}
		
		if((approveDateFrom!=null) && (!approveDateFrom.equals("")))
		{
			params.setApproveDateFrom(approveDateFrom);
		}
		
		if((approveDateTo!=null) && (!approveDateTo.equals("")))
		{
			params.setApproveDateTo(approveDateTo);
		}
		
		if((firstName!=null) && (!firstName.equals("")))
		{
//			System.out.println("firstName re-encode: "+new String(firstName.getBytes(),"UTF-8"));
			params.setFirstName(firstName);
		}
		
		if((lastName != null) && (!lastName.equals("")))
		{
			params.setLastName(lastName);
		}
		
		if((identityNo != null) && (!identityNo.equals("")))
		{
			params.setIdentityNo(identityNo);
		}
		
		if((registrationType!=null) && (!registrationType.equals("")))
		{
			params.setRegistrationType(registrationType);
		}
		
		if((traderCategory!=null) && (!traderCategory.equals("")))
		{
			params.setTraderCategory(traderCategory);
		}
			
		if((printCardStatus!=null) && (!printCardStatus.equals("")))
		{
			params.setPrintCardStatus(printCardStatus);
		}
		
		if((traderType!=null) && (!traderType.equals("")))
		{
			params.setTraderType(traderType);
		}
		
		if((orgId != null) && (!orgId.equals("")))
		{
			params.setOrgId(Long.valueOf(orgId));
		}
		
		System.out.println("params.getFirstName(): "+params.getFirstName());
		Map<String, List<RegistrationDTO>> model = new HashMap<String, List<RegistrationDTO>>(); 
//		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		List<RegistrationDTO> listprintCardGuide = this.printCardService.getPrintCardExcel(params,user);
		
	     model.put("regPrintCard",listprintCardGuide);
		resp.setContentType("application/vnd.ms-excel");
		resp.setHeader("Content-Disposition",
				"attachment; filename=printcardguide.xls");

		String templateFileName = "printcardguide.xls";

		ReportExcelBuilder builder = new ReportExcelBuilder(templateFileName);
		builder.build(resp,model);
		
		this.printCardService.updateASPrint(listprintCardGuide, user);
		
		return null;
	}
	
	@RequestMapping(value = "/showTakePic", method = {RequestMethod.GET, RequestMethod.POST})
//	public String shawItinerary(@PathVariable("printCardId") String printCardId ,@PathVariable("licenseNo") String licenseNo, Model model, HttpServletRequest req) {
	public String showTakePic(@RequestParam("printCardId") String printCardId 
			,@RequestParam("identityNo") String identityNo
			,@RequestParam("licenseNo") String licenseNo
			,@RequestParam("traderCategory") String traderCategory
			,@RequestParam("traderType") String traderType
			, Model model, HttpServletRequest req) {
	
		
		RegistrationDTO params = new RegistrationDTO();
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			 
			System.out.println("printCardId = "+ printCardId);
			System.out.println("identityNo = "+ identityNo);
			System.out.println("licenseNo = "+ licenseNo);
			System.out.println("traderCategory = "+ traderCategory);
			System.out.println("traderType = "+ traderType);
//			String traderType = req.getParameter("traderType");
			
			String forward = "/page/PringCardView.jsp";
			String docPath = null;
			
			
			if(StringUtils.isNotEmpty(printCardId)){
				params.setPrintCardId(Long.valueOf(printCardId));
			}else{
				params.setPrintCardId(Long.valueOf("0"));
			}
			if(params.getPrintCardId() > 0){
				
				params.setLicenseNo(licenseNo);
				System.out.println("req.getContextPath() = " + req.getContextPath());
				params.setApplicationContextPath(req.getContextPath());
				RegistrationDTO dto  = (RegistrationDTO) this.printCardService.getPerson(
						params, 
						user);
				
				
				model.addAttribute("model", dto);
				
				docPath = this.printCardService.getPhotoLocation(params, user);
				
				if(StringUtils.isEmpty(docPath)){
					
					JSONParser parser = new JSONParser();
					//get image data
					String imageData = "";
					String jsonPicFileName = identityNo + "_" + traderType + "_" + traderCategory+".json";
					File fimg = new File(ConstantUtil.TEMPLATE_GUIDE_FILE_LOCATION + "picjson" + java.io.File.separator + jsonPicFileName);
					if(fimg.exists()){
						Object obj = parser.parse(new FileReader(ConstantUtil.TEMPLATE_GUIDE_FILE_LOCATION + "picjson" + java.io.File.separator + jsonPicFileName));
						 
						JSONObject jsonObject = (JSONObject) obj;
				 
						imageData = (String) jsonObject.get("imageData");
						
						System.out.println("imageData = " + imageData);
						
						params.setImgData(imageData);
						
						String replace = "data:image/jpeg;base64,";
						if(params.getImgData().matches("(?i).*image/png.*")){
							replace = "data:image/png;base64,";
							params.setExtension("png");
						}
						if(params.getImgData().matches("(?i).*image/jpg.*")){
							replace = "data:image/jpg;base64,";
							params.setExtension("jpg");
						}
						if(params.getImgData().matches("(?i).*image/jpeg.*")){
							replace = "data:image/jpeg;base64,";
							params.setExtension("jpeg");
						}
									
						params.setImgData(params.getImgData().replace(replace, ""));
						
						this.printCardService.saveCardPhoto(params, user);
						
						docPath = "image";
						
					}
					
				}
				
			}else{
				//get file location
				String jsonPicFileName = identityNo + "_" + traderType + "_" + traderCategory+".json";
				System.out.println("jsonPicFileName = " + jsonPicFileName);
				File file = new File(ConstantUtil.TEMPLATE_GUIDE_FILE_LOCATION + "picjson" + java.io.File.separator + jsonPicFileName);
				
				if(file.exists()){
					docPath = jsonPicFileName;
				}
				
				RegistrationDTO dto = new RegistrationDTO();
				dto.setTraderType(traderType);
				if(StringUtils.isNotEmpty(traderType)){
					dto.setTraderTypeName(TraderType.getMeaning(dto.getTraderType()));
				}
				dto.setTraderCategory(traderCategory);
				dto.setIdentityNo(identityNo);			
				model.addAttribute("model", dto);
			}			
			if(StringUtils.isNotEmpty(docPath)){
				forward = "/page/PringCardView.jsp";
			}else{
				forward = "/page/takePhoto.jsp";
			}
			
			return forward;

		} catch (Exception e) {
			e.printStackTrace();
			return "/page/joborder/pageerror.jsp";

		}
	}
	@RequestMapping(value = "/backToTakePicture", method = {RequestMethod.GET, RequestMethod.POST})
	public String backToTakePicture(@RequestParam("printCardId") String printCardId 
			,@RequestParam("identityNo") String identityNo
			,@RequestParam("licenseNo") String licenseNo
			,@RequestParam("traderCategory") String traderCategory
			,@RequestParam("traderType") String traderType
			, Model model, HttpServletRequest req) {
	
		
		RegistrationDTO params = new RegistrationDTO();
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			 
			System.out.println("printCardId = "+ printCardId);
			System.out.println("identityNo = "+ identityNo);
			System.out.println("licenseNo = "+ licenseNo);
			System.out.println("traderCategory = "+ traderCategory);
			System.out.println("traderType = "+ traderType);			
			String forward = "/page/takePhoto.jsp";
			String docPath = null;
			
			
			if(StringUtils.isNotEmpty(printCardId)){
				params.setPrintCardId(Long.valueOf(printCardId));
			}else{
				params.setPrintCardId(Long.valueOf("0"));
			}
			if(params.getPrintCardId() > 0){
				
				params.setLicenseNo(licenseNo);
				params.setApplicationContextPath(req.getContextPath());
				RegistrationDTO dto  = (RegistrationDTO) this.printCardService.getPerson(
						params, 
						user);
				
				model.addAttribute("model", dto);
				
				docPath = this.printCardService.getPhotoLocation(params, user);
				
			}else{
				//get file location
				String jsonPicFileName = identityNo + "_" + traderType + "_" + traderCategory+".json";
				File file = new File(ConstantUtil.TEMPLATE_GUIDE_FILE_LOCATION + "picjson" + java.io.File.separator + jsonPicFileName);
				
				if(file.exists()){
					docPath = jsonPicFileName;
				}
				
				RegistrationDTO dto = new RegistrationDTO();
				dto.setTraderType(traderType);
				if(StringUtils.isNotEmpty(traderType)){
					dto.setTraderTypeName(TraderType.getMeaning(dto.getTraderType()));
				}
				dto.setTraderCategory(traderCategory);
				dto.setIdentityNo(identityNo);			
				model.addAttribute("model", dto);
			}			
			
			return forward;

		} catch (Exception e) {
			e.printStackTrace();
			return "/page/joborder/pageerror.jsp";

		}
	}
	
	@RequestMapping(value = "/showTakePicJson/{printCardId}/{licenseNo}", method = {RequestMethod.GET, RequestMethod.POST})
	public @ResponseBody String loadTemplateData(@PathVariable("printCardId") String printCardId ,@PathVariable("licenseNo") String licenseNo, Model model, HttpServletRequest req) {
		RegistrationDTO params = new RegistrationDTO();
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			 
			if(StringUtils.isNotEmpty(printCardId)){
				params.setPrintCardId(Long.valueOf(printCardId));
			}
			
			params.setLicenseNo(licenseNo);
			params.setApplicationContextPath(req.getContextPath());
			
			RegistrationDTO dto  = (RegistrationDTO) this.printCardService.getPerson(
					params, 
					user);
			
			
			return HttpMessageFactory.create(HttpMessageType.JSON).writeMessage(dto);

		} catch (Exception e) {
			e.printStackTrace(); 
			return "/page/joborder/pageerror.jsp";

		}
	}
	
	@RequestMapping(value = "/getTakePicJson/{identityNo}/{traderType}/{traderCategory}", method = {RequestMethod.GET, RequestMethod.POST})
	public @ResponseBody String getTakePicJson(@PathVariable("identityNo") String identityNo ,@PathVariable("traderType") String traderType, @PathVariable("traderCategory") String traderCategory, Model model, HttpServletRequest req) {

		try {
			String jsonPicFileName = identityNo + "_" + traderType + "_" + traderCategory+".json";
			
			RegistrationDTO dto  = new RegistrationDTO();
			JSONParser parser = new JSONParser();
			//get image data
			String imageData = "";
			File fimg = new File(ConstantUtil.TEMPLATE_GUIDE_FILE_LOCATION + "picjson" + java.io.File.separator + jsonPicFileName);
			if(fimg.exists()){
				Object obj = parser.parse(new FileReader(ConstantUtil.TEMPLATE_GUIDE_FILE_LOCATION + "picjson" + java.io.File.separator + jsonPicFileName));
				 
				JSONObject jsonObject = (JSONObject) obj;
		 
				imageData = (String) jsonObject.get("imageData");
				
				System.out.println("imageData = " + imageData);
				
				dto.setImgData(imageData);
				
			}
			dto.setIdentityNo(identityNo);
			dto.setTraderType(traderType);
			dto.setTraderCategory(traderCategory);

			return HttpMessageFactory.create(HttpMessageType.JSON).writeMessage(dto);

		} catch (Exception e) {
			e.printStackTrace(); 
			return "/page/joborder/pageerror.jsp";

		}
	}
	
	
	@RequestMapping(value = "/img/template/{traderCategory}", method = RequestMethod.GET)
	public void viewfile(@PathVariable("traderCategory") String category,
			HttpServletRequest req, HttpServletResponse resp,HttpSession sess){
		
		if(ConstantUtil.TEMPLATE_GUIDE_FILE_LOCATION==null)
		  {
			ConstantUtil.TEMPLATE_GUIDE_FILE_LOCATION = sess.getServletContext().getRealPath("/resources")+File.separator+"cardtemplate"+File.separator;
		  }
		
		System.out.println("ConstantUtil.TEMPLATE_GUIDE_FILE_LOCATION"+ConstantUtil.TEMPLATE_GUIDE_FILE_LOCATION);
		
		try {		
			String extension = "jpg";
			String fileName = "blank";
			String path = ConstantUtil.TEMPLATE_GUIDE_FILE_LOCATION;
			System.out.println("path"+path);
			
			if(TraderCategory.PLATINUM_GUIDE.getStatus().equals(category)){
				fileName = "PLATINUM_GUIDE";
			}
			else if(TraderCategory.GOLD_GUIDE.getStatus().equals(category)){
				fileName = "GOLD_GUIDE";
			}
			else if(TraderCategory.PINK_GUIDE.getStatus().equals(category)){
				fileName = "PINK_GUIDE";
			}
			else if(TraderCategory.BLUE_GUIDE.getStatus().equals(category)){
				fileName = "BLUE_GUIDE";
			}
			else if(TraderCategory.GREEN_GUIDE.getStatus().equals(category)){
				fileName = "GREEN_GUIDE";
			}
			else if(TraderCategory.RED_GUIDE.getStatus().equals(category)){
				fileName = "RED_GUIDE";
			}
			else if(TraderCategory.ORANGE_GUIDE.getStatus().equals(category)){
				fileName = "ORANGE_GUIDE";
			}
			else if(TraderCategory.YELLOW_GUIDE.getStatus().equals(category)){
				fileName = "YELLOW_GUIDE";
			}
			else if(TraderCategory.VIOLET_GUIDE.getStatus().equals(category)){
				fileName = "VIOLET_GUIDE";
			}
			else if(TraderCategory.BROWN_GUIDE.getStatus().equals(category)){
				fileName = "BROWN_GUIDE";
			}
			else if("L".equals(category)){
				fileName = "leader";
			}
			else if("SIG".equals(category)){
				fileName = "SIG";
				extension = "png";
			}
		    
			fileName = path+ fileName+"."+extension;
			
			System.out.println("getcard background : " + path+ fileName+"."+extension);
			
			File fileF = new File(fileName);
			System.out.println("fileName"+fileName);
			System.out.println("fileF"+fileF.getAbsolutePath());
			
			resp.setContentType("image/" + extension);
			System.out.println("fileF "+fileF);
			BufferedImage bi = ImageIO.read(fileF);
			OutputStream out = resp.getOutputStream();
			ImageIO.write(bi, extension, out);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
			
	
		}
	}
	
	@RequestMapping(value = "/cardfrontview/{printCardId}/{licenseNo}", method = {RequestMethod.GET, RequestMethod.POST})
	public String cardfrontview(@PathVariable("printCardId") String printCardId ,@PathVariable("licenseNo") String licenseNo, Model model, HttpServletRequest req) {
		RegistrationDTO params = new RegistrationDTO();
		try {
//			User user = (User) req.getSession(true).getAttribute("user");
//			 
//			if(StringUtils.isNotEmpty(printCardId)){
//				params.setPrintCardId(Long.valueOf(printCardId));
//			}
//			
//			params.setLicenseNo(licenseNo);
//			
//			RegistrationDTO dto  = (RegistrationDTO) this.printCardService.getPerson(
//					params, 
//					user);
//			
//			model.addAttribute("model", dto);

			return "/page/CardFrontView.jsp";

		} catch (Exception e) {
			e.printStackTrace();
//			param.setErrMsg(e.getMessage());
//			model.addAttribute("model", param);
			return "/page/joborder/pageerror.jsp";

		}
	}
	
	@RequestMapping(value = "/savecardphoto", method = RequestMethod.POST)
	public @ResponseBody String savecardphoto(@ModelAttribute RegistrationDTO params)
	{
			try{
			User user = (User) httpreq.getSession(true).getAttribute("user");
			
			if(params.getPrintCardId() > 0){
				String replace = "data:image/jpeg;base64,";
				if(params.getImgData().matches("(?i).*image/png.*")){
					replace = "data:image/png;base64,";
					params.setExtension("png");
				}
				if(params.getImgData().matches("(?i).*image/jpg.*")){
					replace = "data:image/jpg;base64,";
					params.setExtension("jpg");
				}
				if(params.getImgData().matches("(?i).*image/jpeg.*")){
					replace = "data:image/jpeg;base64,";
					params.setExtension("jpeg");
				}
							
				params.setImgData(params.getImgData().replace(replace, ""));
				
				this.printCardService.saveCardPhoto(params, user);
			}else{
				//save json photo
	            JSONObject obj = new JSONObject();
	            obj.put("imageData", params.getImgData());
	            obj.put("identityNo", params.getIdentityNo());
	            obj.put("traderType", params.getTraderType());
	            obj.put("traderCategory", params.getTraderCategory());
			 
				try {
					String jsonPicFileName = params.getIdentityNo() + "_" + params.getTraderType() + "_" + params.getTraderCategory()+".json";				
					System.out.println("jsonPicFileName" + jsonPicFileName);
					FileWriter file = new FileWriter(ConstantUtil.TEMPLATE_GUIDE_FILE_LOCATION + "picjson" + java.io.File.separator + jsonPicFileName);
					file.write(obj.toJSONString());
					file.flush();
					file.close();
			 
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			return HttpMessageFactory.create(HttpMessageType.JSON).writeMessage(params);
			
		}catch (Exception e){
			e.printStackTrace();
			return HttpMessageFactory.create(HttpMessageType.JSON).writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/photobase64format/{printCardId}/{licenseNo}", method = {RequestMethod.GET, RequestMethod.POST})
	public @ResponseBody String getPhotobase64format(@PathVariable("printCardId") String printCardId ,@PathVariable("licenseNo") String licenseNo
			, HttpServletResponse response) {
		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
			RegistrationDTO params = new RegistrationDTO();
			
			if(StringUtils.isNotEmpty(printCardId)){
				params.setPrintCardId(Long.valueOf(printCardId));
			}
			params.setLicenseNo(licenseNo);
			
			String photo = this.printCardService.getPhotoBase64Format(params,
					user);
			ServletOutputStream out = response.getOutputStream();

			response.setContentType("image/jpeg;charset=utf-8");
			out.write(photo.getBytes());
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();

		}

		return null;
	}
	
	@RequestMapping(value = "/photo/{printCardId}/{licenseNo}", method = {RequestMethod.GET, RequestMethod.POST})
	public @ResponseBody String getphoto(@PathVariable("printCardId") String printCardId ,@PathVariable("licenseNo") String licenseNo
			, HttpServletResponse response, HttpSession sss) {
		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
			RegistrationDTO params = new RegistrationDTO();
			
			if(StringUtils.isNotEmpty(printCardId)){
				params.setPrintCardId(Long.valueOf(printCardId));
			}
			params.setLicenseNo(licenseNo);
			
			String docPath = this.printCardService.getPhotoLocation(params, user);
//			String docPath = "/Users/SHAII_MACBOOK/Documents/workingfolder/TourismDepartment/DOT_PRINT_CARD/pic/Registration/Guide/1100307/R/101.png"; // this.getPhotoLocation(dto, user);
			
 
			System.out.println("getphoto = " + docPath);
 
			
			String fileType = docPath.substring(docPath.lastIndexOf(".") + 1, docPath.length());
			
			File f = new File(docPath);
			if (!f.exists()) {  
				docPath = sss.getServletContext().getRealPath("/resources")+File.separator+"cardtemplate"+File.separator+"blank.jpg";
			}
			f = new File(docPath);
			System.out.println("f "+f.getAbsolutePath());
			if (f.exists()) { 
				response.setContentType("image/" + fileType);
				response.setHeader("Content-disposition", "attachment; filename="+ printCardId);
				BufferedImage bi = ImageIO.read(f);
				OutputStream out = response.getOutputStream();
				ImageIO.write(bi, fileType, out);
				out.close();
			}
			

		} catch (Exception e) {
			e.printStackTrace();

		}

		return null;
	}
	
	@RequestMapping(value = "/qrcode/{printCardId}/{licenseNo}", method = {RequestMethod.GET, RequestMethod.POST})
	public @ResponseBody String getQRCode(@PathVariable("printCardId") String printCardId ,@PathVariable("licenseNo") String licenseNo
			, HttpServletResponse response) {
		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
			RegistrationDTO params = new RegistrationDTO();
			
			if(StringUtils.isNotEmpty(printCardId)){
				params.setPrintCardId(Long.valueOf(printCardId));
			}
			params.setLicenseNo(licenseNo);
			
			String qrData = this.printCardService.getPrepareQRCode(params, user);
			
			int size = 150;
	        String fileType = "png";
			
		       // Create the ByteMatrix for the QR-Code that encodes the given String
		       Hashtable hintMap = new Hashtable();
		       hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
		       QRCodeWriter qrCodeWriter = new QRCodeWriter();
		       BitMatrix byteMatrix = qrCodeWriter.encode(qrData,
		               BarcodeFormat.QR_CODE, size, size, hintMap);
		       // Make the BufferedImage that are to hold the QRCode
		       int matrixWidth = byteMatrix.getWidth();
		       BufferedImage image = new BufferedImage(matrixWidth, matrixWidth,
		               BufferedImage.TYPE_INT_RGB);
		       image.createGraphics();

		       Graphics2D graphics = (Graphics2D) image.getGraphics();
		       graphics.setColor(Color.WHITE);
		       graphics.fillRect(0, 0, matrixWidth, matrixWidth);
		       // Paint and save the image using the ByteMatrix
		       graphics.setColor(Color.BLACK);

		       for (int i = 0; i < matrixWidth; i++) {
		           for (int j = 0; j < matrixWidth; j++) {
		               if (byteMatrix.get(i, j)) {
		                   graphics.fillRect(i, j, 1, 1);
		               }
		           }
		       }
			
			response.setContentType("image/" + fileType);
			OutputStream out = response.getOutputStream();
			ImageIO.write(image, fileType, out);
			out.close();

		} catch (Exception e) {
			e.printStackTrace();

		}

		return null;
	}
	
	public static void main(String arg[]){
		String path = "/Users/Shaii/Documents/IOI Technologies/Kanith_signature.png";
		
		System.out.println(path.substring(path.lastIndexOf("/") + 1, path.length()));
	}
	@RequestMapping(value = "/signatureDirector/{printCardId}/{licenseNo}", method = {RequestMethod.GET, RequestMethod.POST})
	public @ResponseBody String signatureDirector(@PathVariable("printCardId") String printCardId ,@PathVariable("licenseNo") String licenseNo
			, HttpServletRequest request, HttpServletResponse response,HttpSession sess) {
		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
			RegistrationDTO params = new RegistrationDTO();
			
			if(StringUtils.isNotEmpty(printCardId)){
				params.setPrintCardId(Long.valueOf(printCardId));
			}
			params.setLicenseNo(licenseNo);
			
			String imgPath = this.printCardService.getDirectorSignature(params, user);
			
			System.out.println("imgPath :" + imgPath);
			
			String fileName = imgPath.substring(imgPath.lastIndexOf(java.io.File.separator) + 1, imgPath.length());
			
			System.out.println("fileName :" + fileName);
			
			File fileF = new File(imgPath);
			
			if (!fileF.exists()) {  
				imgPath = sess.getServletContext().getRealPath("/resources")+File.separator+"cardtemplate"+File.separator;
				fileName = "blank.jpg";
				System.out.println("can't find >>> "+fileF.getAbsolutePath());
				
			}
			System.out.println("fileName :" + fileName);
			fileF = new File(imgPath);
			
			String originalFilename = fileName;
			int dot = originalFilename.lastIndexOf('.');
			String extension = "";
			if(dot != -1)
			{
				extension = originalFilename.substring(dot+1);
			}
			
			response.setContentType("image/" + extension);
			BufferedImage bi = ImageIO.read(fileF);
			OutputStream out = response.getOutputStream();
			ImageIO.write(bi, extension, out);
			out.close();

		} catch (Exception e) {
			e.printStackTrace();

		}

		return null;
	}
	
	@RequestMapping(value = "/cardbackview/{printCardId}/{licenseNo}", method = {RequestMethod.GET, RequestMethod.POST})
	public String cardbackview(@PathVariable("printCardId") String printCardId ,@PathVariable("licenseNo") String licenseNo, Model model, HttpServletRequest req) {
		RegistrationDTO params = new RegistrationDTO();
		try {

			return "/page/CardBackView.jsp";

		} catch (Exception e) {
			e.printStackTrace();
//			param.setErrMsg(e.getMessage());
//			model.addAttribute("model", param);
			return "/page/joborder/pageerror.jsp";

		}
	}
	
	@RequestMapping(value = "/testjrac/{printCardId}/{licenseNo}", method = {RequestMethod.GET, RequestMethod.POST})
	public String testjrac(@PathVariable("printCardId") String printCardId ,@PathVariable("licenseNo") String licenseNo, Model model, HttpServletRequest req) {
		RegistrationDTO params = new RegistrationDTO();
		try {

			return "/page/Jrac.jsp";

		} catch (Exception e) {
			e.printStackTrace();
//			param.setErrMsg(e.getMessage());
//			model.addAttribute("model", param);
			return "/page/joborder/pageerror.jsp";

		}
	}

	
	@RequestMapping(value = "/jracphoto", method = {RequestMethod.GET, RequestMethod.POST})
	public String editphoto(@RequestParam("printCardId") String printCardId 
			,@RequestParam("identityNo") String identityNo
			,@RequestParam("licenseNo") String licenseNo
			,@RequestParam("traderCategory") String traderCategory
			,@RequestParam("imageData") String imageData
			,@RequestParam("traderType") String traderType
			, Model model, HttpServletRequest req) {
		RegistrationDTO params = new RegistrationDTO();
		try {
			if(StringUtils.isNotEmpty(printCardId)){
				params.setPrintCardId(Long.valueOf(printCardId));
			}else{
				params.setPrintCardId(Long.valueOf(0));
			}
			if(StringUtils.isNotEmpty(identityNo)){
				params.setIdentityNo(identityNo);
			}
			if(StringUtils.isNotEmpty(licenseNo)){
				params.setLicenseNo(licenseNo);
			}
			if(StringUtils.isNotEmpty(traderCategory)){
				params.setTraderCategory(traderCategory);
			}
			if(StringUtils.isNotEmpty(imageData)){
				params.setImgData(imageData);
			}
			if(StringUtils.isNotEmpty(traderType)){
				params.setTraderType(traderType);
			}
			
			model.addAttribute("model", params);
			return "/page/EditPhoto.jsp";

		} catch (Exception e) {
			e.printStackTrace();
//			param.setErrMsg(e.getMessage());
//			model.addAttribute("model", param);
			return "/page/joborder/pageerror.jsp";

		}
		

	}
	/*@Deprecated
	 * 
	 * */
	@RequestMapping(value = "/tobase64format", method = RequestMethod.POST)
	public @ResponseBody String tobase64format(HttpServletRequest request, HttpServletResponse response,HttpSession sess, @RequestParam("imgData") MultipartFile imgData) 
	{

	    HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
	    try{
	    	
	    	String guidePhoto = sess.getServletContext().getRealPath("/resources")+File.separator+"cardtemplate"+File.separator+"temp"+File.separator;
	    	
	    	String originalFilename = imgData.getOriginalFilename();
			int dot = originalFilename.lastIndexOf('.');
			String extension = "";
			if(dot != -1)
			{
				extension = originalFilename.substring(dot+1);
			}
			String returnPath = "/business/printcard/registration/read/imgData/" ;
			String fileName = System.currentTimeMillis()+"";
			returnPath = returnPath + fileName+"_"+extension;
			
			FileManage filem = new FileManage();
			filem.uploadFile(imgData.getInputStream(), guidePhoto, fileName+"."+extension);
			
			 File file = new File(guidePhoto+fileName+"."+extension);
			 
			 // Reading a Image file from file system
            FileInputStream imageInFile = new FileInputStream(file);
            byte imageData[] = new byte[(int) file.length()];
            imageInFile.read(imageData);
 
            // Converting Image byte array into Base64 String
            String base64str = ImageUtils.encodeImage(imageData);
	    	
	    	String header = "data:image/jpg;base64,";
	    	if("jpg".equalsIgnoreCase(extension)){
	    		header = "data:image/jpg;base64,";
	    	}
	    	if("jpeg".equalsIgnoreCase(extension)){
	    		header = "data:image/jpeg;base64,";
	    	}
	    	
	    	System.out.println("tobase64format-->"+header+base64str);
	    	
	    	return jsonmsg.writeMessage(header+base64str);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    	return jsonmsg.writeErrorMessage(e.getMessage());
	    }
	
	}
 
	@RequestMapping(value = "/uploadImgData", method = {RequestMethod.POST,RequestMethod.GET})
	public synchronized @ResponseBody String updloadImageData(HttpServletRequest request, HttpServletResponse response, HttpSession sess,@RequestParam("imgData") MultipartFile imgData) 
	{
		 HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		    try{
		    	
		    	String guidePhoto = sess.getServletContext().getRealPath("/resources")+File.separator+"cardtemplate"+File.separator+"temp"+File.separator;
		    	
		    	String originalFilename = imgData.getOriginalFilename();
				int dot = originalFilename.lastIndexOf('.');
				String extension = "";
				if(dot != -1)
				{
					extension = originalFilename.substring(dot+1);
				}
				String returnPath = "/business/printcard/registration/read/imgData/" ;
				String fileName = System.currentTimeMillis()+"";
				returnPath = returnPath + fileName+"_"+extension;
				
				FileManage filem = new FileManage();
				filem.uploadFile(imgData.getInputStream(), guidePhoto, fileName+"."+extension);
		    	
		    	return jsonmsg.writeMessage(returnPath);
		    }
		    catch(Exception e)
		    {
		    	e.printStackTrace();
		    	return jsonmsg.writeErrorMessage(e.getMessage());
		    }
	}
	
	@RequestMapping(value = "/read/imgData/{token}", method = {RequestMethod.POST,RequestMethod.GET})
	public void updloadImgData(HttpServletRequest request, HttpServletResponse response,HttpSession sess, @PathVariable("token") String fileName) 
	{
		 HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		    try{
		    	fileName = fileName.replace("_", ".");
		    	String imgPath = sess.getServletContext().getRealPath("/resources")+File.separator+"cardtemplate"+File.separator+"temp"+File.separator;				
				File fileF = new File(imgPath+fileName);
				
				System.out.println("imgPath"+imgPath+fileName);
				
				 System.out.println("fileName = "+fileName);
				
				if (!fileF.exists()) {  
					imgPath = sess.getServletContext().getRealPath("/resources")+File.separator+"cardtemplate"+File.separator;
					fileName = "blank.jpg";
					System.out.println("can't find >>> "+fileF.getAbsolutePath());
					
				}
				fileF = new File(imgPath+fileName);
				
				String originalFilename = fileName;
				int dot = originalFilename.lastIndexOf('.');
				String extension = "";
				if(dot != -1)
				{
					extension = originalFilename.substring(dot+1);
				}
				
				response.setContentType("image/" + extension);
				BufferedImage bi = ImageIO.read(fileF);
				OutputStream out = response.getOutputStream();
				ImageIO.write(bi, extension, out);
				out.close();
		    	
		    }
		    catch(Exception e)
		    {
		    	e.printStackTrace();
		    }
	}

    @RequestMapping(value = "/tp_admin", method = {RequestMethod.GET, RequestMethod.POST})
	public String tp_admin(HttpServletRequest req) {
		RegistrationDTO params = new RegistrationDTO();
		try {

			return "/printcard/tp_admin.jsp";

		} catch (Exception e) {
			e.printStackTrace();
//			
			return "/page/joborder/pageerror.jsp";

		}
 
	}
	@RequestMapping(value = "/tp_admin_preview", method = {RequestMethod.GET, RequestMethod.POST})
	public String tp_admin_preview( HttpServletRequest req) {
		RegistrationDTO params = new RegistrationDTO();
		try {

			return "/printcard/tp_admin_preview.jsp";

		} catch (Exception e) {
			e.printStackTrace();
//			param.setErrMsg(e.getMessage());
//			model.addAttribute("model", param);
			return "/page/joborder/pageerror.jsp";

		}
	}
}
