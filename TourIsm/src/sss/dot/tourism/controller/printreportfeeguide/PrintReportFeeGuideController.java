package sss.dot.tourism.controller.printreportfeeguide;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import sss.dot.tourism.controller.report.ReportExcelBuilder;
import sss.dot.tourism.dto.registration.ReceiptDTO;
import sss.dot.tourism.service.printreportfeeguide.IPrintReportFeeGuideService;

import com.sss.aut.service.User;

@Controller
@RequestMapping("/reportfeeguide/registration")
public class PrintReportFeeGuideController {
	
	@Autowired
	IPrintReportFeeGuideService printReportFeeGuideService;
	
	@RequestMapping(value = "/reportfeeguideExcel", method = RequestMethod.GET)
	public  ModelAndView reportfeeguideExcel (@RequestParam("receiptDateFrom") String dateFrom,@RequestParam("receiptDateTo") String dateTo,@RequestParam("orgId") Long orgId,HttpServletRequest request, HttpServletResponse resp) throws Exception
	{
		User user = (User) request.getSession(true).getAttribute("user");
		ReceiptDTO dto = new ReceiptDTO();
        dto.setReceiptDateFrom(dateFrom);
		if(!dateTo.equals("0"))
		{
			dto.setReceiptDateTo(dateTo);	
		}
		
		//Oat Add 07/10/57
		dto.setOrgId(orgId);
		//

		resp.setContentType("application/vnd.ms-excel");
		resp.setHeader("Content-Disposition",
				"attachment; filename=reportfeeguide.xls");

		String templateFileName = "reportfeeguide.xls";

		ReportExcelBuilder builder = new ReportExcelBuilder(
				templateFileName);
		builder.build(resp,this.printReportFeeGuideService.getPrintReportFeeGuide(dto, user));
		
		return null;
	}
}
