package sss.dot.tourism.controller.punishment;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import sss.dot.tourism.dto.punishment.MasActDTO;
import sss.dot.tourism.dto.punishment.RevokeLicenseDTO;
import sss.dot.tourism.dto.punishment.SuspensionAlertDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.service.punishment.IRevokeService;
import sss.dot.tourism.service.sendmail.ISendMailService;
import sss.dot.tourism.util.RevokeStatus;
import sss.dot.tourism.util.SuspendStatus;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.User;

@Controller 
@RequestMapping("/revoke/business")
public class BusinessRevokeController {
	@Autowired
	IRevokeService revokeService;
	@Autowired
	ISendMailService sendMailService;

	@RequestMapping(value = "/masact/read", method = RequestMethod.POST)
	public @ResponseBody
	String readAct(@ModelAttribute MasActDTO param ,HttpServletRequest req) {
//	String readAct() {
	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			User user = (User) req.getSession(true).getAttribute("user");
			param.setTraderType(TraderType.TOUR_COMPANIES.getStatus());
			List list = revokeService.getActAll(param, user);
			
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/prepare", method = RequestMethod.POST)
	public @ResponseBody
	String readTraderPrepareRevoke(@ModelAttribute RevokeLicenseDTO param ,HttpServletRequest req) {
	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			RevokeLicenseDTO dto = (RevokeLicenseDTO)revokeService.getTraderPrepareRevoke(param, user);	
			return jsonmsg.writeMessage(dto);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/addrevoke", method = RequestMethod.POST)
	public @ResponseBody
	String saveRevoke(@ModelAttribute RevokeLicenseDTO param ,HttpServletRequest req) {
	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			
			User user = (User) req.getSession(true).getAttribute("user");
			Object[] object = (Object[]) jsonmsg.toArrayObject(
					MasActDTO.class, param.getMasAct());

			revokeService.saveRevoke(param, object ,user);	
			
			/*
			 * --------- Comment out because user mistake proof
			if(RevokeStatus.APPROVE.getStatus().equals(param.getRevokeStatus()))
			{
				sendMailService.SendMail(param.getRevokeId(),"R",param.getTraderId());
			}
			*/
			
			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/read", method = RequestMethod.POST)
	public @ResponseBody String readSuspension(@ModelAttribute RevokeLicenseDTO param ,HttpServletRequest req) 
	{

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			
			param.setTraderType(TraderType.TOUR_COMPANIES.getStatus());
			
			User user = (User) req.getSession(true).getAttribute("user");
			
			List list = this.revokeService.getAll(
					param, 
					user
					);
			
			param.setStart(0);
			param.setLimit(Integer.MAX_VALUE);
			Long numAll = new Long(revokeService.countAll(
					param, 
					user
					).size());
			
			return jsonmsg.writePagingMessage(list, numAll);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/cancelrevoke", method = RequestMethod.POST)
	public @ResponseBody
	String cancelsuspend(@ModelAttribute RevokeLicenseDTO param ,HttpServletRequest req) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			
			User user = (User) req.getSession(true).getAttribute("user");

			this.revokeService.cancelRevoke(param ,user);

			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
			// return null;
		}
	}
	
	@RequestMapping(value = "/revoke", method = RequestMethod.POST)
	public @ResponseBody
	String revoke(@ModelAttribute SuspensionAlertDTO param ,HttpServletRequest req) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			User user = (User) req.getSession(true).getAttribute("user");
			Object object = this.revokeService.saveAlert(param ,user);

			return jsonmsg.writeMessage(object);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
			// return null;
		}
	}
	
	@RequestMapping(value = "/alert/read", method = RequestMethod.POST)
	public @ResponseBody String readAlert(@ModelAttribute SuspensionAlertDTO param ,HttpServletRequest req) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			User user = (User) req.getSession(true).getAttribute("user");
			Object obj = this.revokeService.getAlertAll(param, user);
			return jsonmsg.writeMessage(obj);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
}
