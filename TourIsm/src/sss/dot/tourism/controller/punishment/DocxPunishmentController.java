package sss.dot.tourism.controller.punishment;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sss.aut.service.User;

import sss.dot.tourism.domain.MasAct;
import sss.dot.tourism.dto.punishment.RevokeLicenseDTO;
import sss.dot.tourism.dto.punishment.SuspensionLicenseDTO;
import sss.dot.tourism.service.punishment.IRevokeService;
import sss.dot.tourism.service.punishment.ISuspensionService;

@Controller 
@RequestMapping("/print/docx")
public class DocxPunishmentController {
	
	@Autowired
	ISuspensionService suspensionService;
	
	@Autowired
	IRevokeService revokeService;
	
	@Autowired
	private HttpServletRequest req;

    @RequestMapping(value = "/suspension" , method = RequestMethod.GET)
    public String suspensionDocx(@RequestParam("suspendId") String suspendId, ModelMap model )
    {
    	String docx = "suspensionDocx";
    	Long SUSPEND_FEE = new Long(3);
      	try {
	    	User user = (User) req.getSession(true).getAttribute("user");
	    	
	    	SuspensionLicenseDTO param = new SuspensionLicenseDTO();
	    	param.setSuspendId(Long.valueOf(suspendId));
	    	
	    	MasAct act = this.suspensionService.getActBySupendId(param, user);
	    	if((null != act) && (SUSPEND_FEE.equals(act.getMasActId()))){
	    		Object dto = this.suspensionService.getSuspensionOrderDocx(param, user);
		    	model.addAttribute("docx", dto);
		    	docx = "suspendOrderDocx";
	    	}else{
	    		Object dto = this.suspensionService.getSuspensionDocx(param, user);
		    	model.addAttribute("license", dto);
	    	}
	    	
	    	
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		
        return docx;
    }
    
    @RequestMapping(value = "/suspensionTime" , method = RequestMethod.GET)
    public String suspensionDocxTime(@RequestParam("suspendId") String suspendId, ModelMap model )
    {
    	String docx = "suspensionDocx";
    	Long SUSPEND_FEE = new Long(3);
      	try {
	    	User user = (User) req.getSession(true).getAttribute("user");
	    	
	    	SuspensionLicenseDTO param = new SuspensionLicenseDTO();
	    	param.setSuspendId(Long.valueOf(suspendId));
	    	
	    	MasAct act = this.suspensionService.getActBySupendId(param, user);
	    	if((null != act) && (SUSPEND_FEE.equals(act.getMasActId()))){
	    		Object dto = this.suspensionService.getSuspensionOrderDocx(param, user);
		    	model.addAttribute("docx", dto);
		    	docx = "suspendOrderDocxTime";
	    	}else{
	    		Object dto = this.suspensionService.getSuspensionDocx(param, user);
		    	model.addAttribute("license", dto);
	    	}
	    	
	    	
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		
        return docx;
    }
    
    @RequestMapping(value = "/revoke" , method = RequestMethod.GET)
    public String revokeDocx(@RequestParam("revokeId") String revokeId, ModelMap model )
    {
      	try {
	    	User user = (User) req.getSession(true).getAttribute("user");
	    	
	    	RevokeLicenseDTO param = new RevokeLicenseDTO();
	    	param.setRevokeId(Long.valueOf(revokeId));
	    	
	    	Object dto = this.revokeService.getRevokeDocx(param, user);
	    	model.addAttribute("license", dto);
		} catch (Exception e) {
			e.printStackTrace();
			
		}
        return "revokeDocx";
    }
    
    @RequestMapping(value = "/revokeNew" , method = RequestMethod.GET)
    public String revokeNewDocx(@RequestParam("revokeId") String revokeId, ModelMap model )
    {
      	try {
	    	User user = (User) req.getSession(true).getAttribute("user");
	    	
	    	RevokeLicenseDTO param = new RevokeLicenseDTO();
	    	param.setRevokeId(Long.valueOf(revokeId));
	    	
	    	Object dto = this.revokeService.getRevokeDocx(param, user);
	    	model.addAttribute("license", dto);
		} catch (Exception e) {
			e.printStackTrace();
			
		}
        return "revokeNewDocx";
    }
    

}







