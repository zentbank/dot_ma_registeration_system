package sss.dot.tourism.controller.punishment;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import sss.dot.tourism.dto.punishment.PunishmentEvidenceDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.service.punishment.IPunishmentEvidenceService;

import com.sss.aut.service.User;


@Controller 
@RequestMapping("/suspension/evidence")
public class EvidenceSuspensionController {
	@Autowired
	IPunishmentEvidenceService punishmentEvidenceService;
	
	@Autowired
	private HttpServletRequest httpreq;
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public @ResponseBody String saveEvidence(@RequestParam("evidence") MultipartFile evidence 
			,@RequestParam("traderType") String traderType
			,@RequestParam("licenseNo") String licenseNo
			,@RequestParam("punishmentDocName") String punishmentDocName
			,@RequestParam("suspendId") long suspendId
			,@RequestParam("revokeId") long revokeId
			,@RequestParam("punishmentType") String punishmentType) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
			PunishmentEvidenceDTO param = new PunishmentEvidenceDTO();
			
			param.setEvidence(evidence);
			param.setTraderType(traderType);
			param.setLicenseNo(licenseNo);
			param.setPunishmentDocName(punishmentDocName);
			param.setSuspendId(suspendId);
			param.setPunishmentType(punishmentType);
			param.setRevokeId(revokeId);

			this.punishmentEvidenceService.save(param, user);
			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/read", method = RequestMethod.POST)
	public @ResponseBody String readEvidence(@ModelAttribute PunishmentEvidenceDTO param) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
			List list = this.punishmentEvidenceService.getAll(param, user);
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody String deleteEvidence(@RequestBody String json) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
			Object[] object = (Object[]) jsonmsg.toArrayObject(
					PunishmentEvidenceDTO.class, json);

			this.punishmentEvidenceService.delete(object, user);
			return jsonmsg.writeMessage(object);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/viewfile", method = RequestMethod.GET)
	public void viewfile(HttpServletRequest req, HttpServletResponse resp,@RequestParam("punishmentId") long punishmentId){
		
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			PunishmentEvidenceDTO param = new PunishmentEvidenceDTO();
			param.setPunishmentId(punishmentId);
			PunishmentEvidenceDTO dto = (PunishmentEvidenceDTO)this.punishmentEvidenceService.getById(param, user);
		
			System.out.println(dto.getPunishmentDocPath());
			
			String fileType = dto.getPunishmentDocPath().substring(dto.getPunishmentDocPath().lastIndexOf(".") + 1, dto.getPunishmentDocPath().length());
			
			if(!fileType.equals("pdf"))
			{
				File f = new File(dto.getPunishmentDocPath());
			
				resp.setContentType("image/" + fileType);
				BufferedImage bi = ImageIO.read(f);
				OutputStream out = resp.getOutputStream();
				ImageIO.write(bi, fileType, out);
				out.close();
			}
			else
			{
				File pdfFile = new File(dto.getPunishmentDocPath());
				
				String fileName = dto.getPunishmentDocPath().substring(dto.getPunishmentDocPath().lastIndexOf("/") + 1, dto.getPunishmentDocPath().length());

				resp.setContentType("application/pdf");
				resp.addHeader("Content-Disposition", "attachment; filename=" + fileName);
				resp.setContentLength((int) pdfFile.length());

				FileInputStream fileInputStream = new FileInputStream(pdfFile);
				OutputStream responseOutputStream = resp.getOutputStream();
				int bytes;
				while ((bytes = fileInputStream.read()) != -1) {
					responseOutputStream.write(bytes);
				
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
	
		}
	}
}
