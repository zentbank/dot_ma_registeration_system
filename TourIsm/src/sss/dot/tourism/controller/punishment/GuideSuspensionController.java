package sss.dot.tourism.controller.punishment;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import sss.dot.tourism.dto.punishment.MasActDTO;
import sss.dot.tourism.dto.punishment.SuspensionAlertDTO;
import sss.dot.tourism.dto.punishment.SuspensionLicenseDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.service.punishment.ISuspensionAlertService;
import sss.dot.tourism.service.punishment.ISuspensionService;
import sss.dot.tourism.service.sendmail.ISendMailService;
import sss.dot.tourism.util.SuspendStatus;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.User;


@Controller 
@RequestMapping("/suspension/guide")
public class GuideSuspensionController {
	@Autowired
	ISuspensionService suspensionService;
	
	@Autowired
	ISendMailService sendMailService;
	
	@Autowired
	ISuspensionAlertService suspensionAlertService;

	@RequestMapping(value = "/masact/read", method = RequestMethod.POST)
	public @ResponseBody
	String readAct(@ModelAttribute MasActDTO param ,HttpServletRequest req) {
	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			User user = (User) req.getSession(true).getAttribute("user");
			param.setTraderType(TraderType.GUIDE.getStatus());
			List list = suspensionService.getActAll(param, user);
			
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/prepare", method = RequestMethod.POST)
	public @ResponseBody
	String readTraderPrepareSuspension(@ModelAttribute SuspensionLicenseDTO param ,HttpServletRequest req) {
	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			User user = (User) req.getSession(true).getAttribute("user");
			SuspensionLicenseDTO dto = (SuspensionLicenseDTO)suspensionService.getTraderPrepareSuspension(param, user);	
			return jsonmsg.writeMessage(dto);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/addsuspension", method = RequestMethod.POST)
	public @ResponseBody
	String addsuspension(@ModelAttribute SuspensionLicenseDTO param ,HttpServletRequest req) {
	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			
			User user = (User) req.getSession(true).getAttribute("user");
			Object[] object = (Object[]) jsonmsg.toArrayObject(
					MasActDTO.class, param.getMasAct());

			suspensionService.saveSuspension(param, object ,user);
			/*
			 * --------- Comment out because user mistake proof
			if(SuspendStatus.APPROVE.getStatus().equals(param.getSuspendStatus()))
			{
				sendMailService.SendMail(param.getSuspendId(),"S",param.getTraderId());
			}
			*/
			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/read", method = RequestMethod.POST)
	public @ResponseBody String readSuspension(@ModelAttribute SuspensionLicenseDTO param ,HttpServletRequest req) 
	{

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			
			User user = (User) req.getSession(true).getAttribute("user");
			param.setTraderType(TraderType.GUIDE.getStatus());
			

			
			List list = this.suspensionService.getAll(
					param, 
					user
					);
			
			param.setStart(0);
			param.setLimit(Integer.MAX_VALUE);
			
			Long numAll = new Long(suspensionService.countAll(
					param, 
					user
					).size());
			
			return jsonmsg.writePagingMessage(list, numAll);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/cancelsuspend", method = RequestMethod.POST)
	public @ResponseBody
	String cancelsuspend(@ModelAttribute SuspensionLicenseDTO param ,HttpServletRequest req) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			User user = (User) req.getSession(true).getAttribute("user");
			this.suspensionService.cancelSuspend(param ,user);

			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
			// return null;
		}
	}
	
	@RequestMapping(value = "/suspension", method = RequestMethod.POST)
	public @ResponseBody
	String createTraderAddress(@ModelAttribute SuspensionAlertDTO param ,HttpServletRequest req) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			User user = (User) req.getSession(true).getAttribute("user");
			Object object = this.suspensionAlertService.save(param ,user);

			return jsonmsg.writeMessage(object);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
			// return null;
		}
	}
	
	@RequestMapping(value = "/alert/read", method = RequestMethod.POST)
	public @ResponseBody String readBranch(@ModelAttribute SuspensionAlertDTO param ,HttpServletRequest req) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			Object obj = this.suspensionAlertService.getAlertAll(param, user);
			return jsonmsg.writeMessage(obj);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
}
