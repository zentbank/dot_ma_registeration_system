package sss.dot.tourism.controller.punishment;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sss.aut.service.User;

import sss.dot.tourism.dto.punishment.SuspensionAlertDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.service.punishment.ISuspensionAlertService;
import sss.dot.tourism.service.punishment.ISuspensionService;


@Controller
@RequestMapping("/suspension/alert")
public class SuspensionAlertController {

	@Autowired
	ISuspensionAlertService suspensionAlertService;
	
	@Autowired
	ISuspensionService suspensionService;
	
	@Autowired
	private HttpServletRequest httpreq;

	@RequestMapping(value = "/read", method = RequestMethod.POST)
	public @ResponseBody String readBranch(@ModelAttribute SuspensionAlertDTO param) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			User user = (User) httpreq.getSession(true).getAttribute("user");
			Object obj = this.suspensionAlertService.getAlertAll(param, user);
			return jsonmsg.writeMessage(obj);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
//	@RequestMapping(value = "/save", method = RequestMethod.POST)
//	public @ResponseBody
//	String createTraderAddress(@ModelAttribute SuspensionAlertDTO param) {
//
//		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
//		try {
//
//
//			Object object = this.suspensionAlertService.save(param ,DummyUser.getInstance().getUser());
//
//			return jsonmsg.writeMessage(object);
//
//		} catch (Exception e) {
//			e.printStackTrace();
//			return jsonmsg.writeErrorMessage(e.getMessage());
//			// return null;
//		}
//	}



}
