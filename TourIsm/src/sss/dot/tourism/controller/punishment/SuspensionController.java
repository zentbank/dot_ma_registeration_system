package sss.dot.tourism.controller.punishment;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import sss.dot.tourism.domain.TraderAddress;
import sss.dot.tourism.dto.certificate.ReqLicenseCertificateDTO;
import sss.dot.tourism.dto.punishment.MasActDTO;
import sss.dot.tourism.dto.punishment.SuspensionLicenseDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.service.punishment.ISuspensionService;
import sss.dot.tourism.service.sendmail.ISendMailService;

import com.sss.aut.service.User;

@Controller 
@RequestMapping("/punishment")
public class SuspensionController {
	
	@Autowired
	ISuspensionService suspensionService;
	
	@Autowired
	private HttpServletRequest httpreq;

	@Autowired
	ISendMailService sendMailService;
	
	@RequestMapping(value = "/masact/read", method = RequestMethod.POST)
	public @ResponseBody
	String readAct(@ModelAttribute MasActDTO param) {
//	String readAct() {
	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
			List list = suspensionService.getActAll(param, user);
			
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/suspension/prepare", method = RequestMethod.POST)
	public @ResponseBody
	String readTraderPrepareSuspension(@ModelAttribute SuspensionLicenseDTO param) {
	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
			SuspensionLicenseDTO dto = (SuspensionLicenseDTO)suspensionService.getTraderPrepareSuspension(param, user);	
			return jsonmsg.writeMessage(dto);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/suspension/addsuspension", method = RequestMethod.POST)
	public @ResponseBody
	String addsuspension(@ModelAttribute SuspensionLicenseDTO param) {
	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
			Object[] object = (Object[]) jsonmsg.toArrayObject(
					MasActDTO.class, param.getMasAct());

			suspensionService.saveSuspension(param, object ,user);	
			
           // sendMailService.SendMail(param.getEmail(),param.getSuspendId(),"S",param.getTraderId());
            
			
			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/suspension/readsuspension", method = RequestMethod.POST)
	public @ResponseBody String readSuspension(@ModelAttribute SuspensionLicenseDTO param ) 
	{

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
//			param.setOrgId(user.getUserData().getOrgId());
			

			
			List list = this.suspensionService.getAll(
					param, 
					user
					);
			
			param.setStart(0);
			param.setLimit(Integer.MAX_VALUE);
			
			Long numAll = new Long(suspensionService.countAll(
					param, 
					user
					).size());
			
			return jsonmsg.writePagingMessage(list, numAll);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/suspension/cancelsuspend", method = RequestMethod.POST)
	public @ResponseBody
	String cancelsuspend(@ModelAttribute SuspensionLicenseDTO param) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			User user = (User) httpreq.getSession(true).getAttribute("user");
			this.suspensionService.cancelSuspend(param ,user);

			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
			// return null;
		}
	}
	
	  @RequestMapping(value = "/docx/supendOrderDocx" , method = RequestMethod.GET)
	    public String supendOrderDocx(@RequestParam("licenseNo") String licenseNo, ModelMap model )
	    {
	      	try {
		    	
		    	Object dto = this.suspensionService.getSuspensionOrderDocx(licenseNo);
		    	model.addAttribute("docx", dto);
			} catch (Exception e) {
				e.printStackTrace();
				
			}
			
	        return "suspendOrderDocx";
	    }
}
