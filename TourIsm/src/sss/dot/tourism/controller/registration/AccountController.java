package sss.dot.tourism.controller.registration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import sss.dot.tourism.controller.report.ReportExcelBuilder;
import sss.dot.tourism.dto.mas.OfficerDTO;
import sss.dot.tourism.dto.registration.ReceiptDTO;
import sss.dot.tourism.dto.registration.ReceiptDetailDTO;
import sss.dot.tourism.dto.registration.RegisterProgressDTO;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.service.registration.IAccountService;
import sss.dot.tourism.util.RecordStatus;

import com.sss.aut.service.User;


@Controller
@RequestMapping("/account/registration")
public class AccountController {

	@Autowired
	IAccountService accountService;
	
	@Autowired
	private HttpServletRequest req;
	

	
	
	@RequestMapping(value = "/read", method = RequestMethod.POST)
	public @ResponseBody String readTraderBetweenRegistration(@ModelAttribute RegistrationDTO param ) 
	{
		System.out.println("##########Class AccountController method readTraderBetweenRegistration");
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			
			User user = (User) req.getSession(true).getAttribute("user");
			
            param.setTraderRecordStatus(RecordStatus.TEMP.getStatus());
		    param.setRegRecordStatus(RecordStatus.TEMP.getStatus());
		    param.setPersonRecordStatus(RecordStatus.TEMP.getStatus());


			param.setOrgId(user.getUserData().getOrgId());
			
			Long numAll = new Long(accountService.countTraderBetweenRegistrationPaging(
					param, 
					user,
					0,
					Integer.MAX_VALUE).size());
			
			List list = this.accountService.getTraderBetweenRegistration(
					param, 
					user, 
					param.getStart(), 
					param.getLimit());
			
			return jsonmsg.writePagingMessage(list, numAll);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/verification", method = RequestMethod.POST)
	public @ResponseBody
	String verificationRegistration(@ModelAttribute RegisterProgressDTO param) {
	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		
		try {

			User user = (User) req.getSession(true).getAttribute("user");
			accountService.verification(param, user);
			
			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
//	@RequestMapping(value = "/payers", method = RequestMethod.POST)
//	public @ResponseBody
//	String readPayers(@ModelAttribute ReceiptDTO param) {
//	
//		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
//		try {
//
//			Object obj = accountService.getPayers(param, user);
//			
//			return jsonmsg.writeMessage(obj);
//
//		} catch (Exception e) {
//			e.printStackTrace();
//			return jsonmsg.writeErrorMessage(e.getMessage());
//		}
//	}
	
	@RequestMapping(value = "/officer", method = RequestMethod.POST)
	public @ResponseBody
	String readOfficer(@ModelAttribute OfficerDTO param) {
	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		
		try {

			User user = (User) req.getSession(true).getAttribute("user");
			Object obj = accountService.getOfficer(param, user);
			
			return jsonmsg.writeMessage(obj);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	
	@RequestMapping(value = "/receiptdetailfee", method = RequestMethod.POST)
	public @ResponseBody
	String readReceiptdetailFee(@ModelAttribute ReceiptDetailDTO param) {
	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			
			List list = accountService.getReceiptDetailFee(param, user);
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	
	
	@RequestMapping(value = "/readreceipt", method = RequestMethod.POST)
	public @ResponseBody
	String readReceipt(@ModelAttribute ReceiptDTO param) {
	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			String authorityName = (String) req.getSession(true).getAttribute("authority0121");
			
			param.setAuthority(authorityName==null?"":authorityName);
			
			Object obj = accountService.getReceipt(param, user);
			
			return jsonmsg.writeMessage(obj);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	//Oat Add 25/02/58
	@RequestMapping(value = "/readreceiptcancle", method = RequestMethod.POST)
	public @ResponseBody
	String readReceiptCancle(@ModelAttribute ReceiptDTO param) {
	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			String authorityName = (String) req.getSession(true).getAttribute("authority0121");
			
			param.setAuthority(authorityName==null?"":authorityName);
			
			Object obj = accountService.getReceiptCancle(param, user);
			
			return jsonmsg.writeMessage(obj);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	//End Oat Add
	
	@RequestMapping(value = "/savereceiptaccount", method = RequestMethod.POST)
	public @ResponseBody
	String saveReceiptAccount(@ModelAttribute ReceiptDTO param) {
		
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			
			User user = (User) req.getSession(true).getAttribute("user");
			
			req.getSession(true).setAttribute("authority0121", param.getAuthority());
			
			Object[] object = (Object[]) jsonmsg.toArrayObject(
					ReceiptDetailDTO.class, param.getReceiptDetail());

			this.accountService.saveReceiptAccount(param ,object, user);

			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	//Oat Add 26/02/58
	@RequestMapping(value = "/savereceiptaccountcancle", method = RequestMethod.POST)
	public @ResponseBody
	String saveReceiptAccountCancle(@ModelAttribute ReceiptDTO param) {
		
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			
			User user = (User) req.getSession(true).getAttribute("user");
			
			req.getSession(true).setAttribute("authority0121", param.getAuthority());
			
			Object[] object = (Object[]) jsonmsg.toArrayObject(
					ReceiptDetailDTO.class, param.getReceiptDetail());

			this.accountService.saveReceiptAccountCancle(param ,object, user);

			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	//End

	
	@RequestMapping(value = "/receipt/reportsummaryfeegurantee", method = RequestMethod.GET)
	public  void reportFeeInExTotalExcel (@RequestParam("receiveOfficerDateFrom") String receiveOfficerDateFrom
			,@RequestParam("receiveOfficerDateTo") String receiveOfficerDateTo
			,@RequestParam("traderType") String traderType
			,@RequestParam("registrationType") String registrationType
			,@RequestParam("orgId") Long orgId
			,HttpServletResponse resp) throws Exception
	{
		try
	    {

			
			User user = (User) req.getSession(true).getAttribute("user");
			ReceiptDTO dto = new ReceiptDTO();
			
			dto.setReceiveOfficerDateFrom(receiveOfficerDateFrom==null?"":receiveOfficerDateFrom);
			dto.setReceiveOfficerDateTo(receiveOfficerDateTo==null?"":receiveOfficerDateTo);
			dto.setTraderType(traderType==null?"":traderType);
			dto.setRegistrationType(registrationType==null?"":registrationType);
			
			//Oat Edit 07/10/57
			dto.setOrgId(orgId);
//			dto.setOrgId(user.getUserData().getOrgId());
			//
			
	
			resp.setContentType("application/vnd.ms-excel");
			resp.setHeader("Content-Disposition",
					"attachment; filename=reportsummaryfeeguarantee.xls");
	
			String templateFileName = "reportsummaryfeeguarantee.xls";
			Map model = new HashMap();

			ReportExcelBuilder builder = new ReportExcelBuilder(templateFileName);
			
			model = this.accountService.getSummaryReceiptAndFee(dto, user);
			
			builder.build(resp, model);

	    }
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}
	
	//Oat Add 30/10/57
	@RequestMapping(value = "/receipt/reportguaranteeandfee", method = RequestMethod.GET)
	public  void reportGuaranteeAndFee(@RequestParam("receiveOfficerDateFrom") String receiveOfficerDateFrom
			,@RequestParam("receiveOfficerDateTo") String receiveOfficerDateTo
//			,@RequestParam("traderType") String traderType
//			,@RequestParam("registrationType") String registrationType
			,@RequestParam("orgId") Long orgId
			,HttpServletResponse resp) throws Exception
	{
		try
	    {			
			User user = (User) req.getSession(true).getAttribute("user");
			ReceiptDTO dto = new ReceiptDTO();
			
			dto.setReceiveOfficerDateFrom(receiveOfficerDateFrom==null?"":receiveOfficerDateFrom);
			dto.setReceiveOfficerDateTo(receiveOfficerDateTo==null?"":receiveOfficerDateTo);
			
			//Oat Edit 07/10/57
			dto.setOrgId(orgId);
//			dto.setOrgId(user.getUserData().getOrgId());
			//
			
	
			resp.setContentType("application/vnd.ms-excel");
			resp.setHeader("Content-Disposition",
					"attachment; filename=reportguaranteeandfee.xls");
	
			String templateFileName = "reportguaranteeandfee.xls";
			Map model = new HashMap();

			ReportExcelBuilder builder = new ReportExcelBuilder(templateFileName);
			
			model = this.accountService.getSummaryGuaranteeReceiptAndFee(dto, user);
			
			builder.build(resp, model);

	    }
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}
	
	@RequestMapping(value = "/updatefeepaid", method = RequestMethod.POST)
	public @ResponseBody
	String updateFeePaid(@ModelAttribute RegistrationDTO param) {
		
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			
			User user = (User) req.getSession(true).getAttribute("user");
			

			this.accountService.updateFeePaid(param , user);

			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	// --------------------------------------------------------------------------------
	// payment online
	// --------------------------------------------------------------------------------
	@RequestMapping(value = "/readPaymentOnlineForPrintReceipt", method = RequestMethod.POST)
	public @ResponseBody
	String readPaymentOnlineForPrintReceipt(@ModelAttribute ReceiptDTO param) {
	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			
			List list = accountService.getPaymentOnlineForPrintReceipt(param, user);
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	
	
	@RequestMapping(value = "/readPaymentOnlineDetail", method = RequestMethod.POST)
	public @ResponseBody
	String getPaymentOnlineForPrintReceipt(@ModelAttribute ReceiptDTO param) {
	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			String authorityName = (String) req.getSession(true).getAttribute("authority0121");
			
			param.setAuthority(authorityName==null?"":authorityName);
			
			Object obj = accountService.getPaymentOnlineDetail(param, user);
			
			return jsonmsg.writeMessage(obj);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	
	
	@RequestMapping(value = "/paymentdetailfee", method = RequestMethod.POST)
	public @ResponseBody
	String readPaymentDetailfee(@ModelAttribute ReceiptDetailDTO param) {
	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			
			List list = accountService.getPaymentDetailFee(param, user);
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	
	@RequestMapping(value = "/savePayment", method = RequestMethod.POST)
	public @ResponseBody
	String savePayment(@ModelAttribute ReceiptDTO param) {
		
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			
			User user = (User) req.getSession(true).getAttribute("user");
			
			req.getSession(true).setAttribute("authority0121", param.getAuthority());
			
			Object[] object = (Object[]) jsonmsg.toArrayObject(
					ReceiptDetailDTO.class, param.getReceiptDetail());

			this.accountService.savePayment(param ,object, user);

			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	// --------------------------------------------------------------------------------
	// end payment online
	// --------------------------------------------------------------------------------
}








