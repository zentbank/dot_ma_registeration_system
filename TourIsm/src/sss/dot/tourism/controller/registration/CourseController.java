package sss.dot.tourism.controller.registration;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import sss.dot.tourism.dto.training.EducationDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.service.registration.ICourseService;

import com.sss.aut.service.User;


@Controller
@RequestMapping("/course")
public class CourseController  {
	
	@Autowired
	private ICourseService courseService;
	
	@Autowired
	private HttpServletRequest httpreq;
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody
	String createCourse(@ModelAttribute EducationDTO param) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
			//Object[] object = (Object[]) jsonmsg.toArrayObject(
				//	EducationDTO.class, json);

			//List list = this.courseService.create(param ,user);
			courseService.create(param ,user);
			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
			// return null;
		}
	}
	
	@RequestMapping(value = "/read", method = RequestMethod.POST)
	public @ResponseBody
	String readCourse(@ModelAttribute EducationDTO param){
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
	try{
		
		User user = (User) httpreq.getSession(true).getAttribute("user");
		param.setRecordStatus("E");
		
		Long numAll = new Long(courseService.countCourseAll(
				param, 
				user,
				0,
				Integer.MAX_VALUE).size());
		
		List list = this.courseService.getAll(param, user,
				param.getStart(), 
				param.getLimit());
		
		return jsonmsg.writePagingMessage(list,numAll);	
	}catch (Exception e) {
		e.printStackTrace();
		return jsonmsg.writeErrorMessage(e.getMessage());
	}

	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody
	String deleteCourse(@RequestBody String json) {
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
			Object[] object = (Object[]) jsonmsg.toArrayObject(
					EducationDTO.class, json);

			this.courseService.delete(object ,user);

			return jsonmsg.writeMessage();
		} catch (Exception e) {

			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
			// return null;
		}
	}
	
	@RequestMapping(value = "/update" , method = RequestMethod.POST)
	public @ResponseBody
	String updateCourse(@ModelAttribute EducationDTO param)
	{
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
			courseService.update(param ,user);
			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
			// return null;
		}
	}
	
	@RequestMapping(value = "/addperson", method = RequestMethod.POST)
	public @ResponseBody
	String createPersonTrainedCourse(@ModelAttribute EducationDTO param) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
			courseService.createPersonTrained(param ,user);
			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
			// return null;
		}
	}
	
	@RequestMapping(value = "/readperson", method = RequestMethod.POST)
	public @ResponseBody
	String readPersonTrained(@ModelAttribute EducationDTO param){
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
	try{
		User user = (User) httpreq.getSession(true).getAttribute("user");
		
		param.setRecordStatus("N");
		
		Long numAll = new Long(courseService.countPersonAll(
				param, 
				user,
				0,
				Integer.MAX_VALUE).size());
		
		List list = this.courseService.getPersonAll(param, user,
				param.getStart(), 
				param.getLimit());
		
		return jsonmsg.writePagingMessage(list,numAll);	
	}catch (Exception e) {
		e.printStackTrace();
		return jsonmsg.writeErrorMessage(e.getMessage());
	}

	}
	
	@RequestMapping(value = "/updateperson" , method = RequestMethod.POST)
	public @ResponseBody
	String updatePersonCourse(@ModelAttribute EducationDTO param)
	{
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
			courseService.updatePersonTrained(param ,user);
			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/deleteperson" , method = RequestMethod.POST)
	public @ResponseBody
	String deletePersonCourse(@RequestBody String json) throws Exception
	{
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
			Object[] object = (Object[]) jsonmsg.toArrayObject(
					EducationDTO.class, json);
			courseService.deletePersonTrained(object, user);
			return jsonmsg.writeMessage(object);
		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}

}
