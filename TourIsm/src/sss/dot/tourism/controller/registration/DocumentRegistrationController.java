package sss.dot.tourism.controller.registration;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import sss.dot.tourism.dto.certificate.ReqLicenseCertificateDTO;
import sss.dot.tourism.dto.mas.MasDocumentDTO;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.service.registration.IDocumentRegistrationService;
import sss.dot.tourism.util.PropertyLoader;
import sss.dot.tourism.util.RecordStatus;

import com.sss.aut.service.User;

@Controller
@RequestMapping("/document/registration")
public class DocumentRegistrationController {

	@Autowired
	IDocumentRegistrationService documentRegistrationService;
	
	@Autowired
	private HttpServletRequest httpreq;
	

	
	@RequestMapping(value = "/read", method = RequestMethod.POST)
	public @ResponseBody String readDocumentRegistration(@ModelAttribute RegistrationDTO param ) 
	{
		System.out.println("###Controller DocumentRegistrationController method readDocumentRegistration");
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
            param.setTraderRecordStatus(RecordStatus.TEMP.getStatus());
            param.setRegRecordStatus(RecordStatus.TEMP.getStatus());

			param.setOrgId(user.getUserData().getOrgId());
			
			Long numAll = new Long(documentRegistrationService.getDocumentRegistrationPaging(
					param, 
					user,
					0,
					Integer.MAX_VALUE).size());

			List list = this.documentRegistrationService.getDocumentRegistrationPaging(
					param, 
					user, 
					param.getStart(), 
					param.getLimit()); 
			
			return jsonmsg.writePagingMessage(list, numAll);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/readDocument", method = RequestMethod.POST)
	public @ResponseBody String readDocument(@ModelAttribute MasDocumentDTO param ) 
	{
		System.out.println("###Controller DocumentRegistrationController method readDocument");
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
			List list = this.documentRegistrationService.getDocument(param, user);
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/fileupload", method = RequestMethod.POST)
	public @ResponseBody String upload(HttpServletRequest request, HttpServletResponse response, @RequestParam("file") MultipartFile f) 
	{

	    HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
	    try{
	    	User user = (User) request.getSession(true).getAttribute("user");
	    	MasDocumentDTO param = new MasDocumentDTO();
	    	param.setTraderId(new Long(request.getParameter("traderId")));
	    	param.setMasDocId(new Long(request.getParameter("masDocId")));
	    	param.setRegDocId(new Long(request.getParameter("regDocId")));
	    	
	    	this.documentRegistrationService.upload(param, f, user);

	    	return jsonmsg.writeMessage();
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    	return jsonmsg.writeErrorMessage(e.getMessage());
	    }
	
	}
	
	@RequestMapping(value="/viewfile", method=RequestMethod.GET )
	public void viewfile(HttpServletRequest req, HttpServletResponse resp){
		System.out.println("###viewfile");
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			resp.setContentType("image/jpeg");
			String separator=  java.io.File.separator;
			
			String docPath = req.getParameter("docPath");
			
			System.out.println("#####docPath = "+docPath);
			
			String fileType = docPath.substring(docPath.lastIndexOf(".") + 1, docPath.length());

			System.out.println("#####fileType = "+fileType);
			
			if(!fileType.equals("pdf"))
			{
				File f = new File(docPath);
				System.out.println("#####f = "+f.exists());
				System.out.println("#####f = "+f.length());
				resp.setContentType("image/" + fileType);
				BufferedImage bi = ImageIO.read(f);
				OutputStream out = resp.getOutputStream();
				ImageIO.write(bi, fileType, out);
				out.close();
			}
			else
			{
				File pdfFile = new File(docPath);
				
				String fileName = docPath.substring(docPath.lastIndexOf("/") + 1, docPath.length());

				resp.setContentType("application/pdf");
				resp.addHeader("Content-Disposition", "attachment; filename=" + fileName);
				resp.setContentLength((int) pdfFile.length());

				FileInputStream fileInputStream = new FileInputStream(pdfFile);
				OutputStream responseOutputStream = resp.getOutputStream();
				int bytes;
				while ((bytes = fileInputStream.read()) != -1) {
					responseOutputStream.write(bytes);
				
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			
	
		}
	}
	@RequestMapping(value = "/updateCheckdocs", method = RequestMethod.POST)
	public @ResponseBody
	String updateCheckdocs(@ModelAttribute MasDocumentDTO param ) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			User user = (User) httpreq.getSession(true).getAttribute("user");
			this.documentRegistrationService.updateCheckdocs(param ,user);

			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
			// return null;
		}
	}
	
}
