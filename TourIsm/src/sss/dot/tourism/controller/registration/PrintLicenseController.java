package sss.dot.tourism.controller.registration;

import java.io.File;
import java.io.FileInputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import sss.dot.tourism.controller.report.PDFReport;
import sss.dot.tourism.controller.report.ReportExcelBuilder;
import sss.dot.tourism.domain.MasAmphur;
import sss.dot.tourism.domain.MasPrefix;
import sss.dot.tourism.dto.mas.MasAmphurDTO;
import sss.dot.tourism.dto.mas.MasPosfixDTO;
import sss.dot.tourism.dto.mas.MasPrefixDTO;
import sss.dot.tourism.dto.mas.MasProvinceDTO;
import sss.dot.tourism.dto.mas.MasTambolDTO;
import sss.dot.tourism.dto.registration.BranchDTO;
import sss.dot.tourism.dto.registration.ReceiptDTO;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.http.JSONPerform;
import sss.dot.tourism.service.mas.IComboService;
import sss.dot.tourism.service.registration.IPrintLicenseService;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.RegistrationType;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.User;
import com.thoughtworks.xstream.XStream;


@Controller
@RequestMapping("/printlicense/registration")
public class PrintLicenseController {
	public final String MIME_PDF = "application/pdf";
	protected static Logger logger = Logger.getLogger("controller");
	@Autowired
	IPrintLicenseService printLicenseService;
	
	@Autowired
	IComboService comboService;
	
	@Autowired
	ServletContext context;
	
	@Autowired
	private HttpServletRequest httpreq;
	
	//Oat Add
	@RequestMapping(value = "/reportprintlicenseExcel", method = RequestMethod.GET)
	public  ModelAndView reportPrintLicenseExcel 
	(@RequestParam("registrationNo") String registrationNo
	  ,@RequestParam("licenseNo") String licenseNo
	  ,@RequestParam("approveDateFrom") String approveDateFrom
	  ,@RequestParam("traderName") String traderName
	  ,@RequestParam("firstName") String firstName
	  ,@RequestParam("registrationType") String registrationType
	  ,@RequestParam("approveDateTo") String approveDateTo
	  ,@RequestParam("traderNameEn") String traderNameEn
	  ,@RequestParam("traderCategory") String traderCategory
	  ,@RequestParam("printLicenseStatus") String printLicenseStatus
	  ,@RequestParam("traderType") String traderType
	  ,HttpServletRequest request, HttpServletResponse resp) throws Exception
	{
		
		System.out.println("##########Class PrintLicenseController Method reportPrintLicenseExcel");
		User user = (User) request.getSession(true).getAttribute("user");
		
		RegistrationDTO param = new RegistrationDTO();
		param.setRegistrationNo(registrationNo);
		param.setLicenseNo(licenseNo);
		param.setApproveDateFrom(approveDateFrom);
		param.setTraderName(traderName);
		param.setFirstName(firstName);
		param.setRegistrationType(registrationType);
		param.setApproveDateTo(approveDateTo);
		param.setTraderNameEn(traderNameEn);
		param.setTraderCategory(traderCategory);
		param.setPrintLicenseStatus(printLicenseStatus);
		param.setTraderType(traderType);
		
		resp.setContentType("application/vnd.ms-excel");
		resp.setHeader("Content-Disposition", "attachment; filename=reportprintlicensebusiness.xls");
		
		String templateFileName = "reportprintlicensebusiness.xls";
		Map model = new HashMap();

		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		list = this.printLicenseService.getPrintLicenseExcel(model, param, user, 0, Integer.MAX_VALUE);
		System.out.println("###report list.size() = "+list.size());
		model.put("registrationType", "ธุรกิจนำเที่ยว");
		model.put("regPrintLicenseBusiness", list);
		
		ReportExcelBuilder builder = new ReportExcelBuilder(templateFileName);
//		builder.build(resp,this.printReportFeeBusinessGuideService.getPrintReportFeeBusienessGuide(dto, user));
		builder.build(resp, model);

		return null;
	}
	
	@RequestMapping(value = "/reportprintlicenseGuideExcel", method = RequestMethod.GET)
	public  ModelAndView reportPrintLicenseGuideExcel 
	(@RequestParam("registrationNo") String registrationNo
	  ,@RequestParam("licenseNo") String licenseNo
	  ,@RequestParam("approveDateFrom") String approveDateFrom
	  ,@RequestParam("firstName") String firstName
	  ,@RequestParam("identityNo") String identityNo
	  ,@RequestParam("registrationType") String registrationType
	  ,@RequestParam("approveDateTo") String approveDateTo
	  ,@RequestParam("lastName") String lastName
	  ,@RequestParam("traderCategory") String traderCategory
	  ,@RequestParam("printLicenseStatus") String printLicenseStatus
	  ,@RequestParam("traderType") String traderType
	  ,HttpServletRequest request, HttpServletResponse resp) throws Exception
	{
		User user = (User) request.getSession(true).getAttribute("user");
		
		RegistrationDTO param = new RegistrationDTO();
		param.setRegistrationNo(registrationNo);
		param.setLicenseNo(licenseNo);
		param.setApproveDateFrom(approveDateFrom);
		param.setFirstName(firstName);
		param.setIdentityNo(identityNo);
		param.setRegistrationType(registrationType);
		param.setApproveDateTo(approveDateTo);
		param.setLastName(lastName);
		param.setTraderCategory(traderCategory);
		param.setPrintLicenseStatus(printLicenseStatus);
		param.setTraderType(traderType);
		
		resp.setContentType("application/vnd.ms-excel");
		resp.setHeader("Content-Disposition", "attachment; filename=reportprintlicenseguide.xls");
		
		String templateFileName = "reportprintlicenseguide.xls";
		Map model = new HashMap();
		
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		list = this.printLicenseService.getPrintLicenseExcel(model, param, user, 0, Integer.MAX_VALUE);
		model.put("registrationType", "มัคคุเทศก์");
		model.put("regPrintLicenseGuide", list);
		
		ReportExcelBuilder builder = new ReportExcelBuilder(templateFileName);
//		builder.build(resp,this.printReportFeeBusinessGuideService.getPrintReportFeeBusienessGuide(dto, user));
		builder.build(resp, model);

		return null;
	}
	//End Oat Add
	
	@RequestMapping(value = "/read", method = RequestMethod.POST)
	public @ResponseBody String readPrintLicense (@ModelAttribute RegistrationDTO param)
	{
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try{
			User user = (User) httpreq.getSession(true).getAttribute("user");

			param.setOrgId(user.getUserData().getOrgId());
			
			Long numAll = new Long(printLicenseService.countPrintLicense(
					param, 
					user,
					0,
					Integer.MAX_VALUE).size());
			
			List list = this.printLicenseService.getPrintLicense(
					param, 
					user, 
					param.getStart(), 
					param.getLimit());
			
			return jsonmsg.writePagingMessage(list, numAll);
			
		}catch (Exception e){
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	

//	@RequestMapping(value = "/readform/{printLicenseId}", method = RequestMethod.GET)
//	public  String readPrintLicenseForm (@PathVariable long printLicenseId, ModelMap model)
//	{
//		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
//		try{
//			
//			User user = (User) httpreq.getSession(true).getAttribute("user");
//			RegistrationDTO param = new RegistrationDTO();
//			param.setPrintLicenseId(printLicenseId);
//			List<RegistrationDTO> list = (ArrayList<RegistrationDTO>)this.printLicenseService.getPrintLicneseById(param, null);
//			
//			RegistrationDTO dto = new RegistrationDTO();
//			if(!list.isEmpty())
//			{
//				dto  = list.get(0);
//			}
//			
//			model.addAttribute("model", dto);
//			
//			if(dto.getTraderType().equals("B"))
//			{
//				return "/page/printLicenseForm.jsp";
//			}else
//			{
//				return "/page/printLicenseGuideForm.jsp";
//			}
//			
//		}catch (Exception e){
//			e.printStackTrace();
//			return "/page/pageerror.jsp";
//		}
//	}
	
	@RequestMapping(value = "/readform/{printLicenseId}", method = RequestMethod.GET)
	public  String readPrintLicenseForm (@PathVariable int printLicenseId, ModelMap model,@ModelAttribute RegistrationDTO param)
	{
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try{
			
			User user = (User) httpreq.getSession(true).getAttribute("user");
			
			List list = (List)this.printLicenseService.getPrintLicneseById(printLicenseId, user);
			
			RegistrationDTO dto = (RegistrationDTO)list.get(0);
			
			BranchDTO branchDTO = new BranchDTO();
			if(list.size() > 1)
			{
				branchDTO = (BranchDTO)list.get(1);
			}
			
			MasProvinceDTO provinceGuide = new MasProvinceDTO();
			List<MasProvinceDTO> provinceGuideList = (List<MasProvinceDTO>)this.comboService.getProvince(provinceGuide);
			
			MasProvinceDTO province = new MasProvinceDTO();
			List<MasProvinceDTO> provinceList = (List<MasProvinceDTO>)this.comboService.getProvince(province);
			
			MasAmphurDTO amphur = new MasAmphurDTO();
			System.out.println(param.getProvinceId());
			amphur.setProvinceId(dto.getProvinceId());
			List<MasAmphurDTO> amphurList = (List<MasAmphurDTO>)this.comboService.getAmphur(amphur);
			
			MasTambolDTO tambol = new MasTambolDTO();
			tambol.setAmphurId(dto.getAmphurId());
			List<MasTambolDTO> tambolList = (List<MasTambolDTO>)this.comboService.getTambol(tambol);
			
			
			MasPrefixDTO prefix = new MasPrefixDTO();
			List<MasPrefixDTO> prefixList = (List<MasPrefixDTO>)this.comboService.getPrefix(prefix);
			
			MasPosfixDTO postfix = new MasPosfixDTO();
			List<MasPosfixDTO> postfixList = (List<MasPosfixDTO>)this.comboService.getPostfix();
			
			model.addAttribute("model", dto);
			model.addAttribute("provinceGuide", provinceGuideList);
			model.addAttribute("province", provinceList);
			model.addAttribute("amphur", amphurList);
			model.addAttribute("tambol", tambolList);
			model.addAttribute("prefix", prefixList);
			model.addAttribute("postfix", postfixList);
			
			if(dto.getTraderType().equals("B"))
			{
				if ((dto.getBranchType() != null) && (dto.getBranchType().equals("1"))) 
				{
					model.addAttribute("branch", branchDTO);
				}
				return "/page/printLicenseForm.jsp";
			}else
			{
				return "/page/printLicenseGuideForm.jsp";
			}
			
		}catch (Exception e){
			e.printStackTrace();
			return "/page/pageerror.jsp";
		}
	}

	@RequestMapping(value = "/printLicensePdf", method = RequestMethod.POST)
	public ModelAndView printLicensePDF(@ModelAttribute RegistrationDTO param , HttpServletRequest request, HttpServletResponse resp) throws Exception
	{
		System.out.println(" Controller PrintLicenseController method printLicensePdf ");
		
		User user = (User) httpreq.getSession(true).getAttribute("user");
		param.getPrintLicenseId();
		param.getPrintLicenseStatus();
		
		System.out.println("guideProvinceId: "+param.getProvinceGuideServiceId());
		System.out.println("getPrintLicenseId: "+param.getPrintLicenseId());
		System.out.println("getTraderId: "+param.getTraderId());
		
		this.printLicenseService.updatePrintLicneseById(param, null);
		this.printLicenseService.getLicenseForPrint(param,user);
		
		FileInputStream qrCode = this.printLicenseService.getQRCode(param, null);
		
//		System.out.println("qrCode is null: "+(null==qrCode));
		System.out.println("position"+param.getPosition());
		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("licenseNo", param.getLicenseNo());
		//model.put("traderOwnerName", param.getTraderOwnerName());
		model.put("firstNameEn", param.getFirstNameEn());
		model.put("identityNo", param.getIdentityNo());
		
		model.put("qrCode", qrCode);
		//model.put("traderCategoryName", param.getTraderCategoryName());
		
		if(param.getTraderName()!=null && !param.getTraderName().isEmpty())
		{
			model.put("traderCategoryName", param.getTraderCategoryName());
			if(param.getTraderName().length() > 65)
			{
				model.put("traderNameLong", param.getTraderName());
			}else
			{
				model.put("traderName", param.getTraderName());
			}
//			System.out.println("param.getTraderNameEn(): "+param.getTraderNameEn());
//			System.out.println("param.getTraderNameEn().length(): "+param.getTraderNameEn().length());
			if(param.getTraderNameEn().length() > 40)
			{
				model.put("traderNameEnLong", param.getTraderNameEn());
			}else
			{
				model.put("traderNameEn", param.getTraderNameEn());
			}
		
			if(param.getPronunciationName().length() > 65)
			{
				model.put("pronunciationNameLong", param.getPronunciationName());
			}else
			{
				model.put("pronunciationName", param.getPronunciationName());
			}
			if(param.getTraderOwnerName().length() > 65)
			{
				model.put("traderOwnerNameLong", param.getTraderOwnerName());
			}else
			{
				model.put("traderOwnerName", param.getTraderOwnerName());
			}
		}else
		{
			if(param.getTraderOwnerName().length() > 65)
			{
				model.put("traderOwnerNameLong", param.getTraderOwnerName());
			}else
			{
				model.put("traderOwnerName", param.getTraderOwnerName());
			}
			
			if(param.getTraderCategoryName().length() >= 80 && param.getTraderCategoryName().length() <= 100 && param.getTraderCategoryNameOth1().equals(""))
			{
				model.put("traderCategoryNameLong", param.getTraderCategoryName());
				model.put("traderCategoryNameOth2", param.getTraderCategoryNameOth2());
			}else if(param.getTraderCategoryName().length() > 100 && param.getTraderCategoryNameOth1().equals(""))
			{
				System.out.println("IN");
				model.put("traderCategoryNameRiver", param.getTraderCategoryName());
				model.put("traderCategoryNameOth2", param.getTraderCategoryNameOth2());
			}else if(param.getTraderCategoryNameOth1().equals(""))
			{
				model.put("traderCategoryName", param.getTraderCategoryName());
				model.put("traderCategoryNameOth2", param.getTraderCategoryNameOth2());
			}else
			{
				System.out.println("OUT");
				model.put("traderCategoryNameOth1", param.getTraderCategoryNameOth1());
				model.put("traderCategoryNameOth2", param.getTraderCategoryNameOth2());
			}
		}
		
		if(param.getAddressNo().length() > 61)
		{
			model.put("addressNoLong", param.getAddressNo());
		}else
		{
			model.put("addressNo", param.getAddressNo());
		}
		
		model.put("soi", param.getSoi());
		model.put("roadName", param.getRoadName());
		model.put("postCode", param.getPostCode());
		model.put("tambolName", param.getTambolName());
		model.put("amphurName", param.getAmphurName());
		System.out.println("sa " +param.getProvinceName());
		//model.put("provinceName", param.getProvinceId());
		model.put("provinceName", param.getProvinceName());
		model.put("day", param.getDay());
		model.put("month", param.getMonth());
		model.put("year", param.getYear());
		
		String[] expireDate = param.getExpireDate().split("/");
		model.put("expireDay", Integer.parseInt(expireDate[0]));
		model.put("expireMonthName", DateUtils.getMonthName(expireDate[1]));
		model.put("expireYear", expireDate[2]);
		
		String[] effitiveDate = param.getEffectiveDate().split("/");
		
		model.put("licenseStartDay", Integer.parseInt(effitiveDate[0]));
		model.put("licenseStartDayMonth",DateUtils.getMonthName(effitiveDate[1]));
		model.put("licenseStartYear", effitiveDate[2]);
		
		model.put("dayF", param.getDayF());
		System.out.println(param.getDayF());
		model.put("monthF", param.getMonthF());
		model.put("yearF", param.getYearF());
		model.put("managerBG", param.getManagerBG());
		
		if(param.getPosition().length() > 80)
		{
			model.put("positionLong", param.getPosition());
		}else
		{
			model.put("position", param.getPosition());
		}
		
		model.put("position1", param.getPosition1());
		model.put("position2", param.getPosition2());
		
		//branch
		model.put("branchNo", param.getBranchNo());
		model.put("branchAddressNo", param.getBranchAddressNo());
		model.put("branchSoi", param.getBranchSoi());
		model.put("branchRoadName", param.getBranchRoadName());
		model.put("branchTambolName", param.getBranchTambolName());
		model.put("branchAmphurName", param.getBranchAmphurName());
		model.put("branchProvinceName", param.getBranchProvinceName());
		model.put("branchPostCode", param.getBranchPostCode());
		try
		{



			List render = new ArrayList();
			Map map = new HashMap();
			render.add(model);

			String sep = java.io.File.separator; /* sep === "/" */
			List jasperPrintList = new ArrayList();
			
			String jasperFile = null;
			if(param.getTraderName()!=null && !param.getTraderName().isEmpty())
			{
				System.out.println("getTraderId = "+param.getTraderId());
				System.out.println("getTraderName = "+param.getTraderName());
				//สาขา
				if((param.getBranchType() != null) && (param.getBranchType().equals("1")))
				{
					//Oat Edit 22/12/57
//					if(user.getUserData().getOrgId() == 1)
//					{
//						jasperFile = sep + "WEB-INF" + sep + "reports" + sep
//								+ "printLicenseBranch.jasper";
//					}
//					else
//					{
//						jasperFile = sep + "WEB-INF" + sep + "reports" + sep
//								+ "printLicenseBusiness_North_Branch.jasper";
//					}
					
//					if(user.getUserData().getOrgId() == 1 || user.getUserData().getOrgId() == 6 
//							|| user.getUserData().getOrgId() == 7 || user.getUserData().getOrgId() == 8 
//							|| user.getUserData().getOrgId() == 9 || user.getUserData().getOrgId() == 10 
//							|| user.getUserData().getOrgId() == 11)
//					{
//						jasperFile = sep + "WEB-INF" + sep + "reports" + sep
//								+ "printLicenseBranch.jasper";
//					}
//					else
//					{
//						jasperFile = sep + "WEB-INF" + sep + "reports" + sep
//								+ "printLicenseBusiness_North_Branch.jasper";
//					}
					jasperFile = sep + "WEB-INF" + sep + "reports" + sep
							+ "printLicenseBranch.jasper";
					
					//
					
					
				}
				else
				{
					System.out.println("sss :"+param.getOrgId());
					if(param.getOrgId() == 1)
					{
						jasperFile = sep + "WEB-INF" + sep + "reports" + sep
							+ "printLicenseBusiness.jasper";
					}else if(param.getOrgId() == 2) //2
					{
						jasperFile = sep + "WEB-INF" + sep + "reports" + sep
							+ "printLicenseBusiness_North.jasper";
					}else if(param.getOrgId() == 3) //3
					{
						jasperFile = sep + "WEB-INF" + sep + "reports" + sep
							+ "printLicenseBusiness_South2.jasper";
					}else if(param.getOrgId() == 4) //4
					{
						jasperFile = sep + "WEB-INF" + sep + "reports" + sep
							+ "printLicenseBusiness_South1.jasper";
					}
					else if(param.getOrgId() == 5) //5
					{
						jasperFile = sep + "WEB-INF" + sep + "reports" + sep
							+ "printLicenseBusiness_East.jasper";
					}
					
					//Oat Add 22/12/57
					//เพิ่มสาขาใหม่
					else if(param.getOrgId() == 6 || param.getOrgId() == 7 || param.getOrgId() == 8 || param.getOrgId() == 9 
							|| param.getOrgId() == 10 || param.getOrgId() == 11 || param.getOrgId() == 12 || 
							param.getOrgId() == 13 || param.getOrgId() == 14 || param.getOrgId() == 15 || 
							param.getOrgId() == 16 || param.getOrgId() == 17 || param.getOrgId() == 18 || 
							param.getOrgId() == 19 || param.getOrgId() == 20 || param.getOrgId() == 21)
					{
						jasperFile = sep + "WEB-INF" + sep + "reports" + sep
								+ "printLicenseBusiness_All.jasper";
					}
					//
						
				}
				 
			}
			else
			{
//				 jasperFile = sep + "WEB-INF" + sep + "reports" + sep
//						+ "printLicenseGuide.jasper";
				 System.out.println("sss :"+param.getOrgId());
					if(param.getOrgId() == 1)
					{
						jasperFile = sep + "WEB-INF" + sep + "reports" + sep
							+ "printLicenseGuide.jasper";
					}else if(param.getOrgId() == 2) //2
					{
						jasperFile = sep + "WEB-INF" + sep + "reports" + sep
							+ "printLicenseGuide_North.jasper";
					}else if(param.getOrgId() == 3) //3
					{
						jasperFile = sep + "WEB-INF" + sep + "reports" + sep
							+ "printLicenseGuide_South2.jasper";
					}else if(param.getOrgId() == 4) //4
					{
						jasperFile = sep + "WEB-INF" + sep + "reports" + sep
							+ "printLicenseGuide_South1.jasper";
					}
					else if(param.getOrgId() == 5) //5
					{
						jasperFile = sep + "WEB-INF" + sep + "reports" + sep
							+ "printLicenseGuide_East.jasper";
					}
					
					//Oat Add 22/12/57
					//เพิ่มสาขาใหม่
					else if(param.getOrgId() == 6 || param.getOrgId() == 7 || param.getOrgId() == 8 || param.getOrgId() == 9 
							|| param.getOrgId() == 10 || param.getOrgId() == 11 || param.getOrgId() == 12 || 
							param.getOrgId() == 13 || param.getOrgId() == 14 || param.getOrgId() == 15 || 
							param.getOrgId() == 16 || param.getOrgId() == 17 || param.getOrgId() == 18 || 
							param.getOrgId() == 19 || param.getOrgId() == 20 || param.getOrgId() == 21)
					{
						jasperFile = sep + "WEB-INF" + sep + "reports" + sep
								+ "printLicenseGuide_All.jasper";
					}
					//
			}
			System.out.println(param.getRegistrationType()); 
			if(param.getRegistrationType()!=null && param.getRegistrationType().equals("T"))
			{
				jasperFile = sep + "WEB-INF" + sep + "reports" + sep
						+ "printLicenseBusinessTemporary.jasper";
				if((param.getBranchType() != null) && (param.getBranchType().equals("1")))
				{
					jasperFile = sep + "WEB-INF" + sep + "reports" + sep
							+ "printLicenseBranch.jasper";
				}
			}
			
			System.out.println("##########jasperFile = "+jasperFile);
			String jasperName = getReportPath(jasperFile);

			PDFReport report1 = new PDFReport(jasperName, map);
			jasperPrintList.add(report1.getJasperPrint(new JRMapCollectionDataSource(render)));			

			//resp.setContentType("application/msword");
			resp.setContentType(this.MIME_PDF);
						
			JRPdfExporter exporter = new JRPdfExporter();
			exporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST, jasperPrintList);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, resp.getOutputStream());
			exporter.setParameter(JRPdfExporterParameter.IS_CREATING_BATCH_MODE_BOOKMARKS, Boolean.TRUE);
			exporter.exportReport();

		} catch (Exception e)
		{
			System.out.println("display stack trace in the browser ");
			StringWriter stringWriter = new StringWriter();
			PrintWriter printWriter = new PrintWriter(stringWriter);
			e.printStackTrace(printWriter);
			resp.setContentType("text/plain");
			resp.getOutputStream().print("");
			e.printStackTrace();
		}
		return null;
	}
	
//	//Oat Add 24/09/57
//	@RequestMapping(value = "/reportPrintLicenseInfoPdf", method = RequestMethod.POST)
//	public ModelAndView printLicenseInfoPDF(@ModelAttribute RegistrationDTO param , HttpServletRequest request, HttpServletResponse resp) throws Exception
//	{
//		User user = (User) httpreq.getSession(true).getAttribute("user");
//		
//		
//		System.out.println("licenseNo = "+param.getLicenseNo());
//		System.out.println("traderId = "+param.getTraderId());
//		System.out.println("regId = "+param.getRegId());
//		
////		this.printLicenseService.updatePrintLicneseById(param, null);
//		
//		Map<String, Object> model = new HashMap<String, Object>();
//		model.put("licenseNo", param.getLicenseNo());
//		model.put("licenseStatusName", param.getLicenseStatusName());
//		model.put("registrationNo", param.getRegistrationNo());
//		
//		List list = new ArrayList();
//		RegistrationDTO dto = new RegistrationDTO();
//		dto.setAddressNo("555");
//		list.add(dto);
//		list.add(dto);
//		model.put("listTemp", list);
//		//-----
//		try
//		{
//			List render = new ArrayList();
//			Map map = new HashMap();
//			render.add(model);
//	
//			String sep = java.io.File.separator; /* sep === "/" */
//
//			//directory file
//			String jasperFile = null;
//			
//			jasperFile = sep + "WEB-INF" + sep + "reports" + sep+ "printLicenseBusinessInfo.jasper";
//			System.out.println("##########jasperFile = "+jasperFile);
//			
//			String jasperName = getReportPath(jasperFile);
//			System.out.println("##########jasperName = "+jasperName);
//			//
//	
//			PDFReport report1 = new PDFReport(jasperName, map);
//			
//			List jasperPrintList = new ArrayList();
//			jasperPrintList.add(report1.getJasperPrint(new JRMapCollectionDataSource(render)));
//	
//			//resp.setContentType("application/msword");
//			resp.setContentType(this.MIME_PDF);
//
//			JRPdfExporter exporter = new JRPdfExporter();
//			exporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST, jasperPrintList);
//			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, resp.getOutputStream());
//			exporter.setParameter(JRPdfExporterParameter.IS_CREATING_BATCH_MODE_BOOKMARKS, Boolean.TRUE);
//			exporter.exportReport();
//	
//		} catch (Exception e)
//		{
//			System.out.println("display stack trace in the browser ");
//			StringWriter stringWriter = new StringWriter();
//			PrintWriter printWriter = new PrintWriter(stringWriter);
//			e.printStackTrace(printWriter);
//			resp.setContentType("text/plain");
//			resp.getOutputStream().print("");
//			e.printStackTrace();
//		}
//		return null;
//	}
//	//End Oat Add 24/09/57

	public String getReportPath(String jasperFile)
	{
		String realPath = this.context.getRealPath(jasperFile);

		System.out.println("******realPath = " + realPath);

		File realFile = new File(realPath);

		String absolutePath = realFile.getAbsolutePath();

		System.out.println("****absolutePath = " + absolutePath);

		return absolutePath;
	}

	@RequestMapping(value = "/readamphur", method = RequestMethod.POST)
	public@ResponseBody String readAmphur (HttpServletRequest req, HttpServletResponse res,@RequestParam(value="provinceId", required=true) String provinceId) throws Exception
	{
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);

		List<MasAmphurDTO> amphur = new ArrayList();
			
			User user = (User) httpreq.getSession(true).getAttribute("user");
			
			MasAmphurDTO amphurDTO = new MasAmphurDTO();
			
			System.out.println(provinceId);
			
			long id = Long.valueOf(provinceId);
			
			amphurDTO.setProvinceId(id);
			List<MasAmphurDTO> amphurList = (List<MasAmphurDTO>)this.comboService.getAmphur(amphurDTO);
			
			if(!amphurList.isEmpty() && amphurList!=null)
			{
				for(MasAmphurDTO param : amphurList)
				{
					MasAmphurDTO dto = new MasAmphurDTO();
					dto.setAmphurId(param.getAmphurId());
					System.out.println(dto.getAmphurId());
					dto.setAmphurName(param.getAmphurName());
					System.out.println(dto.getAmphurName());
					amphur.add(dto);
				}
			}
			
			return jsonmsg.serialize(amphur);
	}
	
	@RequestMapping(value = "/readtambol", method = RequestMethod.POST)
	public@ResponseBody String readtambol (HttpServletRequest req, HttpServletResponse res,@RequestParam(value="amphurId", required=true) String amphurId) throws Exception
	{
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);

		List<MasTambolDTO> tambol = new ArrayList();
			
			User user = (User) httpreq.getSession(true).getAttribute("user");
			
			MasTambolDTO tambolDTO = new MasTambolDTO();
			
			System.out.println(amphurId);
			
			long id = Long.valueOf(amphurId);
			
			tambolDTO.setAmphurId(id);
			List<MasTambolDTO> tambolList = (List<MasTambolDTO>)this.comboService.getTambol(tambolDTO);
			
			if(!tambolList.isEmpty() && tambolList!=null)
			{
				for(MasTambolDTO param : tambolList)
				{
					MasTambolDTO dto = new MasTambolDTO();
					dto.setTambolId(param.getTambolId());
					System.out.println(dto.getAmphurId());
					dto.setTambolName(param.getTambolName());
					System.out.println(dto.getTambolName());
					tambol.add(dto);
				}
			}
			
			return jsonmsg.serialize(tambol);
	}
	
}

