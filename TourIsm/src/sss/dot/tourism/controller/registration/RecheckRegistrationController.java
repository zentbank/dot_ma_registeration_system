package sss.dot.tourism.controller.registration;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import sss.dot.tourism.dto.registration.RegisterProgressDTO;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.service.registration.IApprovalProcessService;
import sss.dot.tourism.util.RecordStatus;

import com.sss.aut.service.User;
@Controller
@RequestMapping("/recheck/registration")
public class RecheckRegistrationController {
	@Autowired
	IApprovalProcessService recheckRegistrationService;
	
	@Autowired
	private HttpServletRequest httpreq;
	@RequestMapping(value = "/read", method = RequestMethod.POST)
	public @ResponseBody String readTraderBetweenRegistration(@ModelAttribute RegistrationDTO param ) 
	{

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
            param.setTraderRecordStatus(RecordStatus.TEMP.getStatus());
		    param.setRegRecordStatus(RecordStatus.TEMP.getStatus());
		    param.setPersonRecordStatus(RecordStatus.TEMP.getStatus());


			param.setOrgId(user.getUserData().getOrgId());
			
			Long numAll = new Long(recheckRegistrationService.countTraderBetweenRegistration(
					param, 
					user,
					0,
					Integer.MAX_VALUE).size());
			
			List list = this.recheckRegistrationService.getTraderBetweenRegistration(
					param, 
					user, 
					param.getStart(), 
					param.getLimit());
			
			return jsonmsg.writePagingMessage(list, numAll);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/verification", method = RequestMethod.POST)
	public @ResponseBody
	String verificationRegistration(@ModelAttribute RegisterProgressDTO param) {
	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
			recheckRegistrationService.verification(param, user);
			
			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
}
