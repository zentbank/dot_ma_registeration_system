package sss.dot.tourism.controller.registration;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import sss.dot.tourism.dto.registration.CommitteeDTO;
import sss.dot.tourism.dto.registration.RegisterProgressDTO;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.service.registration.IRegisterProgressCommitteeService;
import sss.dot.tourism.service.registration.IRegisterProgressService;
import sss.dot.tourism.service.registration.IRegistrationService;
import sss.dot.tourism.util.RecordStatus;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.User;


@Controller
@RequestMapping("/registration")

//business/registration/trader/readbylicenseno
public class RegistrationController {
	
	@Autowired
	IRegistrationService registrationService;
	@Autowired
	IRegisterProgressService registerProgressService;
	@Autowired
	IRegisterProgressCommitteeService registerProgressCommitteeService;
	
	@Autowired
	private HttpServletRequest context;
	
	@RequestMapping(value = "/createregistration", method = RequestMethod.POST)
	public @ResponseBody
	String createRegistration(@ModelAttribute RegistrationDTO param ,HttpServletRequest req) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			registrationService.createRegistration(param, (User) req.getSession(true).getAttribute("user"));
			
			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/addnewregistration", method = RequestMethod.POST)
	public @ResponseBody
	String addNewRegistration(@ModelAttribute RegistrationDTO param ,HttpServletRequest req) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			Object object =  registrationService.addNewRegistration(param, (User) req.getSession(true).getAttribute("user"));
			
			
			
			return jsonmsg.writeMessage(object);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	//tourleader registration type guide 
	@RequestMapping(value = "/toruleader/loadguide", method = RequestMethod.POST)
	public @ResponseBody String readGuide(@ModelAttribute RegistrationDTO param ,HttpServletRequest req) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			param.setTraderRecordStatus("N");
			param.setPersonRecordStatus("N");
			
			User user = (User) req.getSession(true).getAttribute("user");
			param.setOrgId(user.getUserData().getOrgId());
			List list = this.registrationService.getGuideLicense(param , (User) req.getSession(true).getAttribute("user"));
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}

	@RequestMapping(value = "/saveAllRegistration", method = RequestMethod.POST)
	public @ResponseBody
	String saveAllRegistration(@ModelAttribute RegistrationDTO param ,HttpServletRequest req) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			System.out.println("Save all registration");
			registrationService.saveAllRegistration(param, (User) req.getSession(true).getAttribute("user"));
			
			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	
	@RequestMapping(value = "/readtraderbetweenregistration", method = RequestMethod.POST)
	public @ResponseBody String readTraderBetweenRegistration(@ModelAttribute RegistrationDTO param ,HttpServletRequest req) 
	{

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
            param.setTraderRecordStatus(RecordStatus.TEMP.getStatus());
            param.setRegRecordStatus(RecordStatus.TEMP.getStatus());
            

            User user = (User) context.getSession(true).getAttribute("user");
            
			param.setOrgId(user.getUserData().getOrgId());
			
			Long numAll = new Long(registrationService.countTraderBetweenRegistrationPaging(
					param, 
					(User) req.getSession(true).getAttribute("user"),
					0,
					Integer.MAX_VALUE).size());
			
			List list = this.registrationService.getTraderBetweenRegistrationPaging(
					param, 
					(User) req.getSession(true).getAttribute("user"), 
					param.getStart(), 
					param.getLimit());
			
			return jsonmsg.writePagingMessage(list, numAll);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/trader/readbylicenseno", method = RequestMethod.POST)
	public @ResponseBody String readTraderByLicenseNo(@ModelAttribute RegistrationDTO param ,HttpServletRequest req) 
	{

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
//			List list = this.registrationService.getTraderByLicenseNo(param, (User) req.getSession(true).getAttribute("user"));
			List list = this.registrationService.getLicenseByLicenseNo(param, (User) req.getSession(true).getAttribute("user"));
			
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	

	

	@RequestMapping(value = "/progress/status", method = RequestMethod.POST)
	public @ResponseBody String progress(@ModelAttribute RegisterProgressDTO param ,HttpServletRequest req) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			List list = this.registerProgressService.getRegistrationProgressStatus(param , (User) req.getSession(true).getAttribute("user"));
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	
	@RequestMapping(value = "/checkname/verification", method = RequestMethod.POST)
	public  @ResponseBody String checkName(@ModelAttribute RegistrationDTO params,HttpServletRequest req) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			
			params.setTraderType(TraderType.TOUR_COMPANIES.getStatus());
			
			Object obj = this.registrationService.verificationName(params , (User) req.getSession(true).getAttribute("user"));
			
			return jsonmsg.writeMessage(obj);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}

	
	@RequestMapping(value = "/checkname/verificationen", method = RequestMethod.POST)
	public @ResponseBody String checkNameEn(@ModelAttribute RegistrationDTO param,HttpServletRequest req) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			param.setTraderType(TraderType.TOUR_COMPANIES.getStatus());
			
			System.out.println(param.getTraderNameEn());
			Object obj = this.registrationService.verificationName(param , (User) req.getSession(true).getAttribute("user"));
			
			return jsonmsg.writeMessage(obj);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/checkidentityNo/verification", method = RequestMethod.POST)
	public @ResponseBody String checkIdentityNo(@ModelAttribute RegistrationDTO param,HttpServletRequest req) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			param.setTraderType(TraderType.TOUR_COMPANIES.getStatus());
			
			System.out.println(param.getIdentityNo());
			Object obj = this.registrationService.verificationIdentityNo(param ,(User) req.getSession(true).getAttribute("user"));
			
			return jsonmsg.writeMessage(obj);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	//Oat Add 02/02/58
	@RequestMapping(value = "/checkidentityNoTourleader/verification", method = RequestMethod.POST)
	public @ResponseBody String checkIdentityNoTourleader(@ModelAttribute RegistrationDTO param,HttpServletRequest req) 
	{
		System.out.println("Controller RegistrationController method checkIdentityNoTourleader");
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			param.setTraderType(TraderType.TOUR_COMPANIES.getStatus());
			
			System.out.println(param.getIdentityNo());
			Object obj = this.registrationService.verificationIdentityNoTourleader(param ,(User) req.getSession(true).getAttribute("user"));
			
			return jsonmsg.writeMessage(obj);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	//End Oat Add
	
	@RequestMapping(value = "/progress/statuscommittee", method = RequestMethod.POST)
	public @ResponseBody String progressCommittee(@ModelAttribute CommitteeDTO param,HttpServletRequest req) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			System.out.println(param.getCommitteeIdentityNo());
			List list = this.registerProgressCommitteeService.getRegistrationProgressCommitteeStatus(param , (User) req.getSession(true).getAttribute("user"));
			return jsonmsg.writeMessage(list);
		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/requireDocs", method = RequestMethod.POST)
	public @ResponseBody
	String verificationRegistration(@ModelAttribute RegisterProgressDTO param) {
	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) context.getSession(true).getAttribute("user");
			this.registrationService.requestAdditionalDoc(param, user);
			System.out.println("requireDocs");
			
			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
}
