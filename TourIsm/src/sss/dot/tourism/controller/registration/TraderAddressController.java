package sss.dot.tourism.controller.registration;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import sss.dot.tourism.dto.registration.TraderAddressDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.service.registration.ITraderAddressService;

import com.sss.aut.service.User;

@Controller
@RequestMapping("/traderaddress")
public class TraderAddressController {
	
	@Autowired
	private ITraderAddressService traderAddressService;
	@Autowired
	private HttpServletRequest httpreq;
	
	@RequestMapping(value = "/read", method = RequestMethod.POST)
	public @ResponseBody String readEducationTempAll(@ModelAttribute TraderAddressDTO param) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
			List list = this.traderAddressService.getAll(param, user);
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody
	String createTraderAddress(@RequestBody String json) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
			Object[] object = (Object[]) jsonmsg.toArrayObject(
					TraderAddressDTO.class, json);

			List list = this.traderAddressService.create(object ,user);

			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
			// return null;
		}
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String updateTraderAddress(@RequestBody String json) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
			Object[] object = (Object[]) jsonmsg.toArrayObject(
					TraderAddressDTO.class, json);

			this.traderAddressService.update(object ,user);

			return jsonmsg.writeMessage();
		} catch (Exception e) {

			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
			// return null;
		}

	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody
	String deleteTraderAddress(@RequestBody String json) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
			Object[] object = (Object[]) jsonmsg.toArrayObject(
					TraderAddressDTO.class, json);

			this.traderAddressService.delete(object ,user);

			return jsonmsg.writeMessage();
		} catch (Exception e) {

			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
			// return null;
		}

	}
}
