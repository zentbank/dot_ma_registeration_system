package sss.dot.tourism.controller.report;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import net.sf.jasperreports.engine.JRBand;
import net.sf.jasperreports.engine.JRElement;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRGroup;
import net.sf.jasperreports.engine.JRImage;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

/**
 * @author bsutton
 * 
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public abstract class AbstractJasperReport
{
  static public final int FORMAT_HTML = 1;
  static public final int FORMAT_PDF = 2;

  // private File m_filReport = null;
  // private File m_filImagePath = null;
  // private String m_strTitle = null;
  protected JasperReport m_report = null;
  protected Map m_mapParameters = new HashMap();

  @SuppressWarnings("unused")
protected AbstractJasperReport(String filReport, Map mapParameters) throws JRException, IOException
  {

//    m_mapParameters = mapParameters;

//    Class c = this.getClass();
    // filReport = "..//jasperResource//jasperSource//REGR009.jasper";
    InputStream is = new FileInputStream(filReport);
    if (is == null)
    {
      System.out.println("InputStream is null");
      System.out.println(filReport);
      File f = new File(filReport);
      System.out.println("is File == " + f.isFile() + " < " + f.getAbsolutePath() + " > ");
    }
    else
    {
      System.out.println("InputStream is not null");
    }
    m_report = (JasperReport) JRLoader.loadObject(is);

  }

  @SuppressWarnings("unchecked")
void setParameter(String strKey, Object oValue)
  {
    m_mapParameters.put(strKey, oValue);
  }

  @SuppressWarnings("unchecked")
void setParameters(Map mapParameters)
  {
    m_mapParameters.putAll(mapParameters);
  }

  void validate() throws JRException
  {
    // Check that all of the reports required parameters have been set.
    HashMap mapParameters = new HashMap(m_mapParameters);

    JRParameter[] aParams = m_report.getParameters();
    for (int i = 0; i < aParams.length; i++)
    {
      String strName = aParams[i].getName();
      if (!m_mapParameters.containsKey(strName))
        throw new JRException("Required Jasper Parameter " + strName + " not passed to report.");
      mapParameters.remove(strName);
    }

    // Check that there is a JRParameter for every parameter passed in
    if (mapParameters.size() != 0)
      throw new JRException("The following parameters were not found in the Report: " + mapParameters.toString());
  }

  /**
   * @return
   */
  public Iterator getImageFiles()
  {
    JRBand[] aBands = getBands();

    Vector vImages = new Vector();
    for (int i = 0; i < aBands.length; i++)
    {
      JRElement[] elements = aBands[i].getElements();
      if (elements != null)
      {
        for (int j = 0; j < elements.length; j++)
        {
          if (elements[j] instanceof JRImage)
          {
            JRImage image = (JRImage) elements[j];
            String strExpression = image.getExpression().getText();
            File filImage = new File(evaluate(strExpression));
            vImages.add(filImage);
          }
        }
      }
    }

    return vImages.iterator();
  }

  /**
   * @param strExpression
   *           enhance evaluate to emulate the Jasper expression evaulation
   *          as per dori.jasper.engine.fill.evaluateImage().
   */
  private String evaluate(String strExpression)
  {
    if (strExpression.charAt(0) == '"' && strExpression.charAt(strExpression.length() - 1) == '"')
      strExpression = strExpression.substring(1, strExpression.length() - 1);

    // Replace a double backslash with a single backslash
    int nLen = 0;
    while (nLen != strExpression.length())
    {
      nLen = strExpression.length();
      // strExpression = strExpression.replaceAll("\\\\\\\\", "\\\\");
    }

    return strExpression;
  }

  @SuppressWarnings({ "unchecked", "rawtypes", "deprecation" })
public JRBand[] getBands()
  {
    Vector vBands = new Vector();
    vBands.add(m_report.getBackground());
    vBands.add(m_report.getColumnFooter());
    vBands.add(m_report.getColumnHeader());
    vBands.add(m_report.getDetail());
    vBands.add(m_report.getPageFooter());
    vBands.add(m_report.getPageHeader());
    vBands.add(m_report.getSummary());
    vBands.add(m_report.getTitle());

    JRGroup[] groups = m_report.getGroups();
    if (groups != null)
    {
      for (int i = 0; i < groups.length; i++)
      {
        vBands.add(groups[i].getGroupFooter());
        vBands.add(groups[i].getGroupHeader());
      }
    }

    JRBand[] aBand = new JRBand[1];
    aBand = (JRBand[]) vBands.toArray(aBand);
    return aBand;
  }
}

