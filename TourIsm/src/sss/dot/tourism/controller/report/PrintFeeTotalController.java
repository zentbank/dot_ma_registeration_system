package sss.dot.tourism.controller.report;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import sss.dot.tourism.dto.registration.ReceiptDTO;
import sss.dot.tourism.service.printreportfeetotal.IPrintReportFeeTotalService;

import com.sss.aut.service.User;

@Controller
@RequestMapping("/reportfeetotal/print")
public class PrintFeeTotalController {
	@Autowired
	IPrintReportFeeTotalService printReportFeeTotalService;
	
	@RequestMapping(value = "/reportfeetotalExcel", method = RequestMethod.GET)
	public  ModelAndView reportFeeTotalExcel (@RequestParam("receiptDateFrom") String dateFrom, @RequestParam("orgId") String orgId,HttpServletRequest request, HttpServletResponse resp) throws Exception
	{
		User user = (User) request.getSession(true).getAttribute("user");
		ReceiptDTO dto = new ReceiptDTO();
       
		dto.setReceiptDateFrom(dateFrom);
		
		if(StringUtils.isNotEmpty(orgId)){
			dto.setOrgId(Long.valueOf(orgId));
		}

		resp.setContentType("application/vnd.ms-excel");
		resp.setHeader("Content-Disposition",
				"attachment; filename=reportfeetotal.xls");

		String templateFileName = "reportfeetotal.xls";
		//Map model = new HashMap();
		//List list  = new ArrayList();
		//model.put("regDate", dto.getReceiptDateFrom());
		ReportExcelBuilder builder = new ReportExcelBuilder(
				templateFileName);
		builder.build(resp,this.printReportFeeTotalService.getPrintReportFeeTotal(dto, user));

		return null;
	}
}
