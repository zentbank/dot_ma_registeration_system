package sss.dot.tourism.controller.report;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import sss.dot.tourism.dto.registration.ReceiptDTO;
import sss.dot.tourism.service.printreceiptslip.IPrintReceiptSlipService;

import com.sss.aut.service.User;

@Controller
@RequestMapping("/printReceipt/slip")
public class PrintReceiptSlipController {

	public final String MIME_PDF = "application/pdf";

	@Autowired
	ServletContext context;

	@Autowired
	private HttpServletRequest httpreq;

	@Autowired
	IPrintReceiptSlipService printReceiptService;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/read", method = RequestMethod.GET)
	public ModelAndView printReceiptSlipPDF(@RequestParam long receiptId,
			HttpServletRequest request, HttpServletResponse resp)
			throws Exception {

		User user = (User) request.getSession(true).getAttribute("user");

		ReceiptDTO params = new ReceiptDTO();
		params.setReceiptId(receiptId);

		Map model = this.printReceiptService.getPrintReceiptSlip(params, user);

		Map modelFromBase = new HashMap();
		modelFromBase.putAll(model);

		FileInputStream fis = null;
		String signaturePath = "";
		String sep = java.io.File.separator; /* sep === "/" */
		try {

			List render = new ArrayList();
			Map map = new HashMap();

			System.out.println("signature = " + model.get("signature"));
			if ((null != model.get("signature"))
					&& (!model.get("signature").toString().isEmpty())) {
				try {
					signaturePath = model.get("signature").toString();
					File file = new File(model.get("signature").toString());
					
//					signaturePath = "/Users/vorarats/Desktop/File Share/janyarak.jpg";
//					File file = new File("/Users/vorarats/Desktop/File Share/janyarak.jpg");
						
					fis = new FileInputStream(file);
					model.put("signature", fis);
				} catch (FileNotFoundException fx) {
					String blankFile = sep + "WEB-INF" + sep + "reports" + sep
							+ "blank.png";
					signaturePath = this.context.getRealPath(blankFile);

					File file = new File(signaturePath);

					fis = new FileInputStream(file);
					model.put("signature", fis);

					fx.printStackTrace();
				}

			}

			render.add(model);

			List jasperPrintList = new ArrayList();

			String jasperFile = null;
			String jasperName = null;
			String jasperFileEx = null;
			String jasperNameEx = null;

			// Oat Edit 24/12/2557
			// if(params.getOrgId() == 1)
			if ((params.getOrgId() == 1) || (params.getOrgId() == 7)) {
				jasperFile = sep + "WEB-INF" + sep + "reports" + sep
						+ "printReceiptSlipSignature.jasper";
			} else {
				System.out.println("สาขา = " + params.getOrgId());

				jasperFile = sep + "WEB-INF" + sep + "reports" + sep
						+ "printReceiptSlipEx.jasper";
				jasperFileEx = sep + "WEB-INF" + sep + "reports" + sep
						+ "printReceiptSlipEx1.jasper";
				jasperNameEx = getReportPath(jasperFileEx);
			}
			//

			jasperName = getReportPath(jasperFile);

			PDFReport report1 = new PDFReport(jasperName, map);
			jasperPrintList.add(report1
					.getJasperPrint(new JRMapCollectionDataSource(render)));

			System.out.println("jasperFile" + jasperFile);
			System.out.println("jasperFileEx" + jasperFileEx);

			if (jasperNameEx != null) {
				System.out.println("jasperNameEx != null");
				PDFReport report2 = new PDFReport(jasperNameEx, map);

				for (int i = 1; i <= 2; i++) {
					List renderEx = new ArrayList();
					if ((null != signaturePath) && (!signaturePath.isEmpty())) {
						try {
							File fileExzx = new File(signaturePath);

							FileInputStream fisEx = new FileInputStream(
									fileExzx);

							Map modelEx = new HashMap();

							modelEx.putAll(modelFromBase);

							modelEx.put("signatureEx", fisEx);

							renderEx.add(modelEx);
						} catch (FileNotFoundException fx) {
							fx.printStackTrace();
						}

					}

					jasperPrintList.add(report2
							.getJasperPrint(new JRMapCollectionDataSource(
									renderEx)));
				}

			}

			resp.setContentType(this.MIME_PDF);

			JRPdfExporter exporter = new JRPdfExporter();
			exporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST,
					jasperPrintList);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
					resp.getOutputStream());
			exporter.setParameter(
					JRPdfExporterParameter.IS_CREATING_BATCH_MODE_BOOKMARKS,
					Boolean.TRUE);
			exporter.exportReport();

		} catch (Exception e) {
			System.out.println("display stack trace in the browser ");
			StringWriter stringWriter = new StringWriter();
			PrintWriter printWriter = new PrintWriter(stringWriter);
			e.printStackTrace(printWriter);
			resp.setContentType("text/plain");
			resp.getOutputStream().print("");
			e.printStackTrace();
		} finally {
			try {
				if (fis != null)
					fis.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String getReportPath(String jasperFile) {
		String realPath = this.context.getRealPath(jasperFile);

		System.out.println("******realPath = " + realPath);

		File realFile = new File(realPath);

		String absolutePath = realFile.getAbsolutePath();

		System.out.println("****absolutePath = " + absolutePath);

		return absolutePath;
	}

	public static void main(String arg[]) {
		String officer1 = "นางนวพร นารถบุญ";
		String officer2 = "นางสาวเพ็ญศิริรัตน์ อาจทวี";
		String officer3 = "นางสาวสมใจ ทองก้อน";

		if (officer1.indexOf("นวพร") > 0) {
			System.out.println("นวพร");
		} else if (officer3.indexOf("สมใจ") > 0) {
			System.out.println("สมใจ");

		} else if (officer2.indexOf("เพ็ญ") > 0) {
			System.out.println("เพ็ญ");
		} else {
			System.out.println("1");
		}
	}

}
