package sss.dot.tourism.controller.report;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import sss.dot.tourism.service.printreportbusinesstourismbyplace.IPrintReportBusinessTourismByPlace;

@Controller
@RequestMapping("/reportBusinessTourismByPlace")
public class PrintReportBusinessTourismByPlaceController {
	
	@Autowired
	IPrintReportBusinessTourismByPlace printReportBusinessTourismByPlace;
	
	@RequestMapping(value = "/printReport", method = RequestMethod.GET)
	public  ModelAndView reportFeeBusinessGuideExcel (@RequestParam("provinceId") String paramProvinceId,
													  @RequestParam("amphurId") String paramAmphurId,
													  @RequestParam("tambolId") String paramTambolId ,
													  HttpServletRequest request, HttpServletResponse resp) throws Exception
	{
		
		resp.setContentType("application/vnd.ms-excel");
		resp.setHeader("Content-Disposition","attachment; filename=BusinessTourismByPlace.xls");

		String templateFileName = "BusinessTourismByPlace.xls";
		//Map model = new HashMap();
		//List list  = new ArrayList();
		//model.put("regPrintFeeBusinessGuide", list);
		System.out.println("paramProvinceId: "+paramProvinceId);
		System.out.println("paramAmphurId: "+paramAmphurId);
		System.out.println("paramTambolId: "+paramTambolId);
		ReportExcelBuilder builder = new ReportExcelBuilder(templateFileName);
		Map model = new HashMap();
		Long provinceId = StringUtils.isNotBlank(paramProvinceId)?new Long(paramProvinceId):null;
		Long amphurId = StringUtils.isNotBlank(paramAmphurId)?new Long(paramAmphurId):null;
		Long tambolId = StringUtils.isNotBlank(paramTambolId)?new Long(paramTambolId):null;
		model.put("businessTourisms", this.printReportBusinessTourismByPlace.printReportBusinessTourismByPlace(provinceId, amphurId, tambolId));
		builder.build(resp,model);

		return null;
	}
}
