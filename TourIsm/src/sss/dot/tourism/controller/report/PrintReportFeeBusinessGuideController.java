package sss.dot.tourism.controller.report;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import sss.dot.tourism.dto.registration.ReceiptDTO;
import sss.dot.tourism.service.printreportfeebusinessguide.IPrintReportFeeBusinessGuideService;

import com.sss.aut.service.User;

@Controller
@RequestMapping("/reportfeebusinessguide/registration")
public class PrintReportFeeBusinessGuideController {
	
	@Autowired
	IPrintReportFeeBusinessGuideService printReportFeeBusinessGuideService;
	
	@RequestMapping(value = "/reportfeebusinessguideExcel", method = RequestMethod.GET)
	public  ModelAndView reportFeeBusinessGuideExcel (@RequestParam("month") String month,@RequestParam("year") String year,HttpServletRequest request, HttpServletResponse resp) throws Exception
	{
		User user = (User) request.getSession(true).getAttribute("user");
		ReceiptDTO dto = new ReceiptDTO();
       
		dto.setMonth(month);
		dto.setYear(year);

		resp.setContentType("application/vnd.ms-excel");
		resp.setHeader("Content-Disposition",
				"attachment; filename=reportfeebusinessguide.xls");

		String templateFileName = "reportfeebusinessguide.xls";
		//Map model = new HashMap();
		//List list  = new ArrayList();
		//model.put("regPrintFeeBusinessGuide", list);
		ReportExcelBuilder builder = new ReportExcelBuilder(
				templateFileName);
		builder.build(resp,this.printReportFeeBusinessGuideService.getPrintReportFeeBusienessGuide(dto, user));

		return null;
	}
}
