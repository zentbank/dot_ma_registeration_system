package sss.dot.tourism.controller.report;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import sss.dot.tourism.dto.registration.ReceiptDTO;
import sss.dot.tourism.service.printreportfeebusinessguide.IPrintReportFeeBusinessGuideService;
import sss.dot.tourism.service.printreportfeeinextotal.IPrintReportFeeInExTotalService;

import com.sss.aut.service.User;

@Controller
@RequestMapping("/reportfeeinextotal/print")
public class PrintReportFeeInExTotalController {

	@Autowired
	IPrintReportFeeInExTotalService printReportFeeInExTotalService;
	
	@RequestMapping(value = "/reportfeeinextotalExcel", method = RequestMethod.GET)
	public  void reportFeeInExTotalExcel (@RequestParam("month") String month,@RequestParam("year") String year,HttpServletRequest request, HttpServletResponse resp) throws Exception
	{
		try
	    {
			User user = (User) request.getSession(true).getAttribute("user");
			ReceiptDTO dto = new ReceiptDTO();
			
			//Oat Add 23/09/57
			dto.setOrgId(user.getUserData().getOrgId());
			//
	       
			dto.setMonth(month);
			dto.setYear(year);
	
			System.out.println(month);
			System.out.println(year);
			resp.setContentType("application/vnd.ms-excel");
			resp.setHeader("Content-Disposition",
					"attachment; filename=reportfeeinextotal.xls");
	
			String templateFileName = "reportfeeinextotal.xls";
			Map model = new HashMap();
	//		List list  = new ArrayList();
	//		model.put("regFee1", list);
			ReportExcelBuilder builder = new ReportExcelBuilder(templateFileName);
			
			model = this.printReportFeeInExTotalService.getPrintReportFeeInExTotal(dto, user);
			
			builder.build(resp, model);

	    }
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}
}







