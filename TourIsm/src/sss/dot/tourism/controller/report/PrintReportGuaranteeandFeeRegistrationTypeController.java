package sss.dot.tourism.controller.report;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import sss.dot.tourism.dto.registration.ReceiptDTO;
import sss.dot.tourism.service.printreportguaranteeandfeeregistrationtype.IPrintReportGuaranteeAndFeeRegistrationTypeService;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.User;


@Controller
@RequestMapping("/report/print")
public class PrintReportGuaranteeandFeeRegistrationTypeController {
	@Autowired
	IPrintReportGuaranteeAndFeeRegistrationTypeService printReportGuaranteeAndFeeRegistrationTypeService;
	
	@RequestMapping(value = "/reportguaranteeandfeeregistrationtypeExcel", method = RequestMethod.GET)
	public ModelAndView reportGuaranteeAndFeeRegistrationtypeExcel(@RequestParam("receiptDateFrom") String dateFrom,@RequestParam("receiptDateTo") String dateTo,
			@RequestParam("traderType") String traderType,@RequestParam("registrationType") String registrationType,HttpServletRequest request, HttpServletResponse resp) throws Exception
	{
		User user = (User) request.getSession(true).getAttribute("user");
		ReceiptDTO dto = new ReceiptDTO();
		
		dto.setReceiptDateFrom(dateFrom);
		if(!dateTo.equals("0"))
		{
			dto.setReceiptDateTo(dateTo);	
		}
		
		dto.setTraderType(traderType);
		dto.setRegistrationType(registrationType);
		
		
		if(TraderType.TOUR_COMPANIES.equals(traderType))
		{
			resp.setContentType("application/vnd.ms-excel");
			resp.setHeader("Content-Disposition",
					"attachment; filename=reportguaranteefeeguide.xls");

			String templateFileName = "reportguaranteeandfeeregistrationtype.xls";

			ReportExcelBuilder builder = new ReportExcelBuilder(
				templateFileName);
			builder.build(resp,this.printReportGuaranteeAndFeeRegistrationTypeService.getPrintReportGuaranteeAndFeeRegistrationType(dto, user));
		}else
		{
			resp.setContentType("application/vnd.ms-excel");
			resp.setHeader("Content-Disposition",
					"attachment; filename=reportguaranteefeeguide.xls");

			String templateFileName = "reportguaranteeandfeeregistrationtypeguide.xls";

			ReportExcelBuilder builder = new ReportExcelBuilder(
				templateFileName);
			builder.build(resp,this.printReportGuaranteeAndFeeRegistrationTypeService.getPrintReportGuaranteeAndFeeRegistrationType(dto, user));
		}
		return null;
		
	}
}
