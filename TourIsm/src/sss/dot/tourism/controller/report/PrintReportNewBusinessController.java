package sss.dot.tourism.controller.report;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sss.aut.service.User;

import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.service.reportnewbusiness.IPrintReportNewBusinessService;
import sss.dot.tourism.service.reportprovincebusiness.IPrintReportProvinceBusinessService;

@Controller
@RequestMapping("/reportnewbusiness/print")
public class PrintReportNewBusinessController {
	
	@Autowired
	IPrintReportNewBusinessService printReportNewBusinessService;
	
	@RequestMapping(value = "/printExcel", method = RequestMethod.GET)
	public void reportProvinceBusinessToExcel(@ModelAttribute RegistrationDTO dto ,HttpServletRequest request, HttpServletResponse resp)throws Exception{
			System.out.println("######REPORT NEW BUSINESS");
			
			try
		    {
				User user = (User) request.getSession(true).getAttribute("user");
				//RegistrationDTO dto = new RegistrationDTO();
				
				//Oat Add 23/09/57
				//dto.setOrgId(user.getUserData().getOrgId());
				//
				System.out.println(dto.getOrgId());
				System.out.println(dto.getDateFrom());
				System.out.println(dto.getDateTo());
//				System.out.println(dto.getProvinceId());
				
				resp.setContentType("application/vnd.ms-excel");
				resp.setHeader("Content-Disposition",
						"attachment; filename=reportnewbusiness.xls");
		
				String templateFileName = "reportnewbusiness.xls";
				Map model = new HashMap();
				List list  = new ArrayList();
				
				ReportExcelBuilder builder = new ReportExcelBuilder(templateFileName);
				
				model = this.printReportNewBusinessService.getPrintReportNewBusiness(dto, user);
				
				builder.build(resp, model);

		    }
			catch(Exception e)
			{
				e.printStackTrace();
			}
	}
}
