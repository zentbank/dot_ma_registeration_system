package sss.dot.tourism.controller.report;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sss.aut.service.User;

import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.service.reportprovincebusiness.IPrintReportProvinceBusinessService;
import sss.dot.tourism.service.reportstaticleader.IPrintReportStaticTourLeaderService;

@Controller
@RequestMapping("/reportstatictourleader/print")
public class PrintReportStaticTourleaderController {
	@Autowired
	IPrintReportStaticTourLeaderService printReportStaticTourLeaderService;
	
	@RequestMapping(value = "/printExcel", method = RequestMethod.GET)
	public void reportStaticTourLeaderToExcel(@ModelAttribute RegistrationDTO dto ,HttpServletRequest request, HttpServletResponse resp)throws Exception{
			System.out.println("######REPORT STATIC TOURLEADER");
			
			try
		    {
				User user = (User) request.getSession(true).getAttribute("user");
				//RegistrationDTO dto = new RegistrationDTO();
				
				//sek Add 24/09/59
				dto.setOrgId(user.getUserData().getOrgId());
				//
				
				System.out.println(dto.getDateFrom());
				System.out.println(dto.getDateTo());
				
				resp.setContentType("application/vnd.ms-excel");
				resp.setHeader("Content-Disposition",
						"attachment; filename=reportstatictourleader.xls");
		
				String templateFileName = "reportstatictourleader.xls";
				Map model = new HashMap();
				List list  = new ArrayList();
				
				ReportExcelBuilder builder = new ReportExcelBuilder(templateFileName);
				
				model = this.printReportStaticTourLeaderService.getPrintReportStaticTourLeader(dto, user);
				
				builder.build(resp, model);

		    }
			catch(Exception e)
			{
				e.printStackTrace();
			}
	}
}
