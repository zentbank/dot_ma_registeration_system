package sss.dot.tourism.controller.report;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;


import net.sf.jxls.exception.ParsePropertyException;
import net.sf.jxls.transformer.XLSTransformer;
public class ReportExcelBuilder {
	private XLSTransformer transformer;
	  private String templateFileName;
	  private Map beans;
	  
	  public ReportExcelBuilder() {
	    this.transformer = new XLSTransformer();
	    this.beans = new HashMap();
	  }
	  public ReportExcelBuilder(String templateFileName) {
	    this.transformer = new XLSTransformer();
	    this.templateFileName = templateFileName;
	    this.beans = new HashMap();
	  }

	  public Map getBeans() {
	    return beans;
	  }
	  public void setBeans(Map beans) {
	    this.beans = beans;
	  }
	  public String getTemplateFileName() {
	    return templateFileName;
	  }
	  public void setTemplateFileName(String templateFileName) {
	    this.templateFileName = templateFileName;
	  }
	  public XLSTransformer getTransformer() {
	    return transformer;
	  }
	  public void setTransformer(XLSTransformer transformer) {
	    this.transformer = transformer;
	  }
	  
	  public void build(HttpServletResponse resp) throws Exception {
	    
	    if(this.validateBuilder()) {
	      HSSFWorkbook wb = null;
	          OutputStream out = null;
	          try {
	              out = resp.getOutputStream();
	              Class c = this.getClass();
	              InputStream is = c.getResourceAsStream(this.templateFileName);
	              wb = this.transformer.transformXLS(is, this.beans);

	          } catch (ParsePropertyException e) {
	                  throw new ServletException(e);
	          } catch (IOException e) {
	                  throw new ServletException(e);
	          }

	          wb.write(out);
	          out.close();
	    }
	  }
	  
	  public void build(HttpServletResponse resp, Map beans) throws Exception {
	    this.beans = beans;
	    this.build(resp);
	  }
	  
	  public void addParemeter(String name, Object object) {
	    this.beans.put(name, object);
	  }
	  
	  public void delParameter(String name) {
	    this.beans.remove(name);
	  }
	  
	  public boolean validateBuilder() throws Exception {
	    
	    if(this.transformer == null) {
	      throw new Exception("transformer is null");
	    } else if(this.templateFileName == null || this.templateFileName.length() <= 0) {
	      throw new Exception("templateFileName is null or empty");
	    }
	    return true;
	  }
}
