package sss.dot.tourism.controller.trader;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.service.trader.ITraderService;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.User;


@Controller
@RequestMapping("/license")
public class TraderController {
	
	@Autowired
	ITraderService traderService;
	@Autowired
	private HttpServletRequest httpreq;
	
	@RequestMapping(value = "/trader/readAllLicense", method = RequestMethod.POST)
	public @ResponseBody String readTraderByLicenseNo(@ModelAttribute RegistrationDTO param ) 
	{

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
			List list = this.traderService.getTraderAll(param, user);
			
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/trader/readName", method = RequestMethod.POST)
	public @ResponseBody String readTraderByName(@ModelAttribute RegistrationDTO param ) 
	{
		System.out.println("IN");
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		

		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
			param.setTraderType(TraderType.TOUR_COMPANIES.getStatus());
			
			List list = this.traderService.getTraderAllName(param, user);
			
			param.setStart(0);
			param.setLimit(Integer.MAX_VALUE);
			Long numAll = new Long(traderService.getTraderAllName(
					param, 
					user
					).size());
			
			return jsonmsg.writePagingMessage(list, numAll);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	// guide/license/trader/updateguide
	@RequestMapping(value = "/trader/updateguide", method = RequestMethod.GET)
	public @ResponseBody
	String saveAllRegistration() {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			traderService.updateGuide();
			
			return jsonmsg.writeMessage();

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	// guide/license/trader/updatetour
	@RequestMapping(value = "/trader/updatetour", method = RequestMethod.GET)
	public @ResponseBody
	String updatetour() {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			traderService.updateTour();
			
			return jsonmsg.writeMessage();

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
}
