package sss.dot.tourism.controller.training;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import sss.dot.tourism.dto.training.EducationDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.service.training.IPersonTrainedService;

import com.sss.aut.service.User;


@Controller
@RequestMapping("/personTrained")
public class PersonTrainedController {
	@Autowired
	private IPersonTrainedService personTrainedService;
	
	@Autowired
	private HttpServletRequest httpreq;

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/read", method = RequestMethod.POST)
//	@RequestMapping(value = "/read", method = RequestMethod.GET)
	public @ResponseBody String readpersonTrained(@ModelAttribute EducationDTO param) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) httpreq.getSession(true).getAttribute("user");
			List list = this.personTrainedService.getAll(param, user);
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
}
