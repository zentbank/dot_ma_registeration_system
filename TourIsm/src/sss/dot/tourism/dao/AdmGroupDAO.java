package sss.dot.tourism.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.domain.AdmGroup;
import sss.dot.tourism.domain.AdmGroup;
import sss.dot.tourism.util.RecordStatus;

@Repository("admGroupDAO")
public class AdmGroupDAO extends BaseDAO{
	
	public AdmGroupDAO()
	{
		this.domainObj = AdmGroup.class;
	}
	public List<AdmGroup> findGroup(int officerGroup) throws Exception
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  AdmGroup as dep ");
		hql.append(" where dep.recordStatus = ? ");
		params.add(RecordStatus.NORMAL.getStatus());
		
		if(officerGroup > 0)
		{
			hql.append(" and dep.officerGroup = ? ");
			params.add(officerGroup);
		}
		

		
				
		return (List<AdmGroup>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
}
