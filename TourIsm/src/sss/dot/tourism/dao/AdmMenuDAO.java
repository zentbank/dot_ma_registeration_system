package sss.dot.tourism.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.domain.AdmMenu;
@Repository("admMenuDAO")
public class AdmMenuDAO extends BaseDAO {

	public AdmMenuDAO() {
		this.domainObj = AdmMenu.class;
	}

	public List<Object[]> findRootMenu(long userId) {
		Map<String, Object> parames = new HashMap<String, Object>();

		StringBuilder hql = new StringBuilder();

		hql.append(" SELECT ME.SCREEN_ID  ");
		hql.append(" , ME.GROUP_ID ");
		hql.append(" FROM ADM_USER_GROUP UG ");
		hql.append(" INNER JOIN ADM_MENU ME ");
		hql.append(" ON(ME.GROUP_ID = UG.GROUP_ID)  ");
		hql.append(" WHERE UG.USER_ID = :USER_ID ");
		parames.put("USER_ID", userId);

		SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());

		sqlQuery.addScalar("SCREEN_ID", Hibernate.LONG);// 0
		sqlQuery.addScalar("GROUP_ID", Hibernate.LONG);// 1

		sqlQuery.setProperties(parames);
		List<Object[]> result = sqlQuery.list();

		return result;
	}

}
