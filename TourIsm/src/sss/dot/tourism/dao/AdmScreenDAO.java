package sss.dot.tourism.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.domain.AdmScreen;
import sss.dot.tourism.domain.DeactivateDetail;

@Repository("admScreenDAO")
public class AdmScreenDAO extends BaseDAO {
	
	public AdmScreenDAO()
	{
		this.domainObj = AdmScreen.class;
	}
	
	public List<AdmScreen> findChildren(long screenParentId) throws Exception
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  AdmScreen as dep ");
		hql.append(" where dep.recordStatus = 'N' ");
		
		hql.append(" and dep.admScreen.screenId = ? ");
		params.add(screenParentId);
	
		
		return (List<AdmScreen>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}

}
