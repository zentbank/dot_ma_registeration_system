package sss.dot.tourism.dao;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.cglib.reflect.FastClass;
import net.sf.cglib.reflect.FastMethod;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.domain.AdmGroup;
import sss.dot.tourism.domain.AdmUser;
import sss.dot.tourism.domain.AdmUserGroup;
import sss.dot.tourism.dto.mas.AdmGroupDTO;
import sss.dot.tourism.dto.mas.AdmUserDTO;
import sss.dot.tourism.util.RecordStatus;

@Repository("admUserGroupDAO")
public class AdmUserGroupDAO extends BaseDAO{

	public AdmUserGroupDAO()
	{
		this.domainObj = AdmUserGroup.class;
	}
	
	@SuppressWarnings("unchecked")
	public List<AdmGroup> findRole(long userLogin) throws Exception
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" select dep.admGroup from  AdmUserGroup as dep ");
		hql.append(" where dep.admUser.userId = ? ");
		params.add(userLogin);
		
				
		return (List<AdmGroup>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
	
	@SuppressWarnings("unchecked")
	public List<AdmUser> findUser(AdmUserDTO dto, int firstRow ,int pageSize) throws Exception
	{
		@SuppressWarnings("rawtypes")
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  AdmUser as dep ");
		hql.append(" where dep.recordStatus = ? ");
		params.add(RecordStatus.NORMAL.getStatus());
		
		if((dto.getUserName() != null) && (!dto.getUserName().isEmpty()))
		{
			hql.append(" and dep.userName like ? ");
			params.add("%"+dto.getUserName()+"%");
		}
		
		if((dto.getUserLastname() != null) && (!dto.getUserLastname().isEmpty()))
		{
			hql.append(" and dep.userLastname like ? ");
			params.add("%"+dto.getUserLastname()+"%");
		}
		
		if(dto.getOrgId() > 0)
		{
			hql.append(" and dep.organization.orgId = ? ");
			params.add(dto.getOrgId());
		}
		
				
		return (List<AdmUser>)this.findByAddPaging(hql.toString(), firstRow, pageSize, params.toArray());
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<AdmGroup> findRole(long userLogin, String groupRole) throws Exception
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" select dep.admGroup from  AdmUserGroup as dep ");
		hql.append(" where dep.admUser.userId = ? ");
		params.add(userLogin);
		
		hql.append(" and dep.admGroup.groupRole = ? ");
		params.add(groupRole);
		
				
		return (List<AdmGroup>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
	
	@SuppressWarnings("unchecked")
	public List<AdmUserGroup> findMapUserGroup(long userId) throws Exception
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  AdmUserGroup as dep ");
		hql.append(" where dep.admUser.userId = ? ");
		params.add(userId);
		
				
		return (List<AdmUserGroup>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
	
	@SuppressWarnings("unchecked")
	public List<AdmUserGroup> findMapUserGroup(long userId , int officerGroup) throws Exception
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  AdmUserGroup as dep ");
		hql.append(" where dep.admUser.userId = ? ");
		params.add(userId);
		
		hql.append(" and dep.admGroup.officerGroup = ? ");
		params.add(officerGroup);
		
				
		return (List<AdmUserGroup>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
	
	public List<AdmGroupDTO> findbyUserId(long userId, int officerGroup)
	{
		Map<String,Object> parames = new HashMap<String,Object>();
		    
	    StringBuilder hql = new StringBuilder();
		hql.append(" SELECT ");
		hql.append(" GP.GROUP_ID ");
		hql.append(" ,GP.GROUP_NAME ");
		hql.append(" ,GP.GROUP_ROLE ");
		hql.append(" ,GP.OFFICER_GROUP ");
		hql.append(" ,SEL.USER_ID ");
		hql.append(" FROM ADM_GROUP GP ");
		hql.append(" LEFT JOIN  ");
		hql.append(" ( SELECT  ");
		hql.append(" G.GROUP_ID ");
		hql.append(" ,G.GROUP_NAME ");
		hql.append(" ,G.GROUP_ROLE ");
		hql.append(" ,G.OFFICER_GROUP ");
		hql.append(" ,UG.USER_ID ");
		hql.append(" FROM ADM_USER_GROUP UG ");
		hql.append(" RIGHT JOIN ADM_GROUP G ");
		hql.append(" ON(G.GROUP_ID = UG.GROUP_ID) ");
		hql.append(" WHERE G.RECORD_STATUS = 'N' ");
		hql.append(" AND UG.USER_ID = :USER_ID ) SEL ");
		hql.append(" ON(SEL.GROUP_ID = GP.GROUP_ID) ");
		hql.append(" WHERE GP.RECORD_STATUS = 'N' ");
		
		hql.append(" AND GP.OFFICER_GROUP = :OFFICER_GROUP ");
		
		parames.put("USER_ID", userId);
		parames.put("OFFICER_GROUP", officerGroup);
		
	
	
		SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
    
	
	    sqlQuery.addScalar("GROUP_ID", Hibernate.LONG); //0
	    sqlQuery.addScalar("GROUP_NAME", Hibernate.STRING); //1
	    sqlQuery.addScalar("GROUP_ROLE", Hibernate.STRING); //2
	    sqlQuery.addScalar("OFFICER_GROUP", Hibernate.STRING); //3
	    sqlQuery.addScalar("USER_ID", Hibernate.LONG);//4
    
	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
	    
	    List<AdmGroupDTO> list = new ArrayList<AdmGroupDTO>();
	    
	    if(!result.isEmpty())
	    {
	    	for(Object[] obj: result)
	    	{
	    		AdmGroupDTO dto = new AdmGroupDTO();
	    		
	    		dto.setGroupId(Long.valueOf(obj[0].toString()));
	    		dto.setGroupName(obj[1]==null?"":obj[1].toString());
	    		dto.setGroupRole(obj[2]==null?"":obj[2].toString());
	    		dto.setOfficerGroup(obj[3]==null?0:Integer.valueOf(obj[3].toString()));
	    		
	    		
	    		if(obj[4] !=null)
	    		{
	    			dto.setUserId(Long.valueOf(obj[4].toString()));
	    			dto.setActive(true);
	    		}
	    		
	    		
	    		list.add(dto);
	    	}
	    }

    return list;
}
	
	 public Object insert(Object persistence)
	  {

	    return this.getHibernateTemplate().save(persistence);
	  }
	  /**
	   * 
	   * @see com.sss.dao.IBaseDAO#update(java.lang.Object)
	   */
	  public void update(Object persistence)
	  {

	   
	    this.getHibernateTemplate().update(persistence);
	  }
}
