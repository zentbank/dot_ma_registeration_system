package sss.dot.tourism.dao;

import org.springframework.stereotype.Repository;


@Repository("commonDAO")
public class CommonDAO extends BaseDAO {

	public <T> T findByPrimarykey(Class<T> clazz, Long id)
			    throws Exception
	{
	  return getHibernateTemplate().get(clazz, id);
	}
	public <T> T findByPrimarykey(Class<T> clazz, String id)
			    throws Exception
	{
		return getHibernateTemplate().get(clazz, id);
	}
}
