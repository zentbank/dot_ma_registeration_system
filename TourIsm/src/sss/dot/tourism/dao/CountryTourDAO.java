package sss.dot.tourism.dao;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.domain.CountryTour;

@Repository("countryTourDAO")
public class CountryTourDAO extends BaseDAO{
	public CountryTourDAO()
	{
		this.domainObj = CountryTour.class;
	}
}
