package sss.dot.tourism.dao;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.domain.DotLicOwnerCommittee;

@Repository("dotLicOwnerCommitteeDAO")
public class DotLicOwnerCommitteeDAO extends BaseDAO{
	public DotLicOwnerCommitteeDAO()
	{
		this.domainObj = DotLicOwnerCommittee.class;
	}
}
