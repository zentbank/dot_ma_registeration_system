package sss.dot.tourism.dao;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.domain.DotLicOwnerEduBackground;


@Repository("dotLicOwnerEduBackgroundDAO")
public class DotLicOwnerEduBackgroundDAO extends BaseDAO{
	public DotLicOwnerEduBackgroundDAO()
	{
		this.domainObj = DotLicOwnerEduBackground.class;
	}
}
