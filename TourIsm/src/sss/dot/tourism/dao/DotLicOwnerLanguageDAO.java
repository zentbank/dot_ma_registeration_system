package sss.dot.tourism.dao;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.domain.DotLicOwnerLanguage;

@Repository("dotLicOwnerLanguageDAO")
public class DotLicOwnerLanguageDAO extends BaseDAO{
	public DotLicOwnerLanguageDAO()
	{
		this.domainObj = DotLicOwnerLanguage.class;
	}
}
