package sss.dot.tourism.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.domain.DotLicense;
import sss.dot.tourism.domain.Trader;


@Repository("dotLicenseDAO")
public class DotLicenseDAO extends BaseDAO{

	public DotLicenseDAO()
	{
		this.domainObj = DotLicense.class;
	}
	
	public DotLicense findByLicenseNo(String licenseNo)
	{
		ArrayList params = new ArrayList();
		
		StringBuilder hql = new StringBuilder();
		hql.append(" from  DotLicense as tr ");
		hql.append(" where tr.recordStatus = 'N' ");

		if (StringUtils.isNotEmpty(licenseNo)) {
			hql.append(" and tr.licenseNo = ? ");
			params.add(licenseNo);
		}
		
		
		List<DotLicense> list = (List<DotLicense>) this.getHibernateTemplate().find(hql.toString(), params.toArray());

		return CollectionUtils.isNotEmpty(list)?list.get(0):null;
	}
}
