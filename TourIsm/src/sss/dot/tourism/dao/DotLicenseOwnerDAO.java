package sss.dot.tourism.dao;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.domain.DotLicenseOwner;

@Repository("dotLicenseOwnerDAO")
public class DotLicenseOwnerDAO extends BaseDAO{
	
	public DotLicenseOwnerDAO()
	{
		this.domainObj = DotLicenseOwner.class;
	}
}
