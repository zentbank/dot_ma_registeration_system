package sss.dot.tourism.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.domain.Country;
import sss.dot.tourism.domain.DotMsLanguage;


@Repository("dotMsLanguageDAO")
public class DotMsLanguageDAO extends BaseDAO{
	public DotMsLanguageDAO()
	{
		this.domainObj = DotMsLanguage.class;
	}
	
	public List<DotMsLanguage> listLanguageByName(String langName)
	  {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  DotMsLanguage as dep ");
		hql.append(" where dep.langName = ? ");
		params.add(langName);

		
		return (List<DotMsLanguage>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	  }
}
