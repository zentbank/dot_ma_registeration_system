package sss.dot.tourism.dao;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.domain.DotSubLicense;


@Repository("dotSubLicenseDAO")
public class DotSubLicenseDAO extends BaseDAO{
	public DotSubLicenseDAO()
	{
		this.domainObj = DotSubLicense.class;
	}
}
