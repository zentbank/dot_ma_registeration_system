package sss.dot.tourism.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.domain.DotTrBillDetail;
import sss.dot.tourism.domain.DotTrBillPayment;


@Repository("dotTrBillDetailDAO")
public class DotTrBillDetailDAO extends BaseDAO{
	public DotTrBillDetailDAO()
	{
		this.domainObj = DotTrBillDetail.class;
	}
	
	public List<DotTrBillDetail> findBillDetailByPayId(long billPaymentId) throws Exception
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  DotTrBillDetail as dep ");
		hql.append(" where dep.recordStatus = 'N' ");
		hql.append(" and dep.dotTrBillPayment.billPaymentId = ? ");
		params.add(billPaymentId);
		
		List<DotTrBillDetail> list = (List<DotTrBillDetail>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	
		
		return CollectionUtils.isNotEmpty(list)?list:null;
	}
}
