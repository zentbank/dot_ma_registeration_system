package sss.dot.tourism.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.domain.DotTrBillPayment;
import sss.dot.tourism.domain.DotTrFeeLicense;

@Repository("dotTrBillPaymentDAO")
public class DotTrBillPaymentDAO extends BaseDAO{
	public DotTrBillPaymentDAO()
	{
		this.domainObj = DotTrBillPayment.class;
	}
//	public DotTrBillPayment findBillPaymentByTrFeeLicense(long licenseFeeId) throws Exception
//	{
//		ArrayList params = new ArrayList();
//		StringBuilder hql = new StringBuilder();
//		hql.append(" from  DotTrBillPayment as dep ");
//		hql.append(" where dep.recordStatus = 'N' ");
//		hql.append(" and dep.dotTrFeeLicense.licenseFeeId = ? ");
//		params.add(licenseFeeId);
//		
//		List<DotTrBillPayment> list = (List<DotTrBillPayment>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
//	
//		
//		return CollectionUtils.isNotEmpty(list)?list.get(0):null;
//	}
	public DotTrBillPayment findBillPaymentByBillNo(String taxId, String billPaymentNo) throws Exception
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  DotTrBillPayment as dep ");
		hql.append(" where dep.recordStatus = 'N' ");
		hql.append(" and dep.dotTrFeeLicense.dotLicenseOwner.taxId = ? ");
		hql.append(" and dep.billPaymentNo = ? ");
		params.add(taxId);
		params.add(billPaymentNo);
		
		List<DotTrBillPayment> list = (List<DotTrBillPayment>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	
		
		return CollectionUtils.isNotEmpty(list)?list.get(0):null;
	}
}
