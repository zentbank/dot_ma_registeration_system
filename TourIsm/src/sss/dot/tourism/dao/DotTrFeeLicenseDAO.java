package sss.dot.tourism.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.domain.DotTrFeeLicense;

@Repository("dotTrFeeLicenseDAO")
public class DotTrFeeLicenseDAO  extends BaseDAO{
	public DotTrFeeLicenseDAO()
	{
		this.domainObj = DotTrFeeLicense.class;
	}
	
	public List<DotTrFeeLicense> findTrFeeLicenseByRegId(long regId) throws Exception
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  DotTrFeeLicense as dep ");
		hql.append(" where dep.recordStatus = 'N' ");
		
		hql.append(" and dep.registration.regId = ? ");
		params.add(regId);
	
		
		return (List<DotTrFeeLicense>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
}
