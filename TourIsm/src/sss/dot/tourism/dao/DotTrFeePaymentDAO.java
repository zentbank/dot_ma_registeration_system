package sss.dot.tourism.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.domain.DotTrFeeLicense;
import sss.dot.tourism.domain.DotTrFeePayment;

@Repository("dotTrFeePaymentDAO")
public class DotTrFeePaymentDAO extends BaseDAO{
	
	public DotTrFeePaymentDAO() {
		this.domainObj = DotTrFeePayment.class;
	}
	
	public DotTrFeePayment findTrFeePaymentByBill(long billPaymentId) throws Exception
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  DotTrFeePayment as dep ");
		hql.append(" where dep.recordStatus = 'N' ");
		
		hql.append(" and dep.dotTrBillPayment.billPaymentId = ? ");
		params.add(billPaymentId);
		
		List<DotTrFeePayment> list = (List<DotTrFeePayment>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	
		
		return CollectionUtils.isNotEmpty(list)?list.get(0):null;
	}
	
}
