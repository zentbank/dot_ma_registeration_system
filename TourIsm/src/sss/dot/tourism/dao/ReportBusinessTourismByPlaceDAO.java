package sss.dot.tourism.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.dto.report.ReportBusinessTourismByPlaceDTO;

@Repository("reportBusinessTourimByPlaceDAO")
public class ReportBusinessTourismByPlaceDAO extends BaseDAO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public List<ReportBusinessTourismByPlaceDTO> findReportBuisnessTourismDTO(Long provinceId, Long amphurId, Long tambolId) throws Exception{
		Map<String,Object> params = new HashMap<String,Object>();
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT PROV.PROVINCE_NAME, AMP.AMPHUR_NAME, TMB.TAMBOL_NAME, COUNT(TR.LICENSE_NO) AS BUSINESS_TOURISM_AMT ");
		sql.append(" FROM TRADER TR  ");
		sql.append(" 	INNER JOIN TRADER_ADDRESS TA ON TR.TRADER_ID = TA.TRADER_ID ");
		sql.append(" 	INNER JOIN MAS_PROVINCE PROV ON TA.PROVINCE_ID = PROV.PROVINCE_ID ");
		sql.append(" 	INNER JOIN MAS_AMPHUR AMP ON TA.AMPHUR_ID = AMP.AMPHUR_ID ");
		sql.append(" 	INNER JOIN MAS_TAMBOL TMB ON TA.TAMBOL_ID = TMB.TAMBOL_ID ");
		sql.append(" WHERE TR.RECORD_STATUS = 'N' ");
		sql.append(" AND TR.LICENSE_STATUS = 'N' ");
		sql.append(" AND TR.BRANCH_PARENT_ID IS NULL ");
		sql.append(" AND TR.TRADER_TYPE = 'B' ");
		sql.append(" AND TA.ADDRESS_TYPE = 'O' ");
		if(null != provinceId){
			sql.append(" AND TA.PROVINCE_ID = :provinceId ");
			params.put("provinceId", provinceId);
		}
		if(null != amphurId){
			sql.append(" AND TA.AMPHUR_ID = :amphurId ");
			params.put("amphurId", amphurId);
		}
		if(null != tambolId){
			sql.append(" AMD TA.TAMBOL_ID = :tambolId ");
			params.put("tambolId", tambolId);
		}
		sql.append(" GROUP BY PROV.PROVINCE_NAME, AMP.AMPHUR_NAME, TMB.TAMBOL_NAME ");
		sql.append(" ORDER BY PROV.PROVINCE_NAME, AMP.AMPHUR_NAME, TMB.TAMBOL_NAME ");
		
		SQLQuery query = getSession().createSQLQuery(sql.toString());
		query.addScalar("PROVINCE_NAME", Hibernate.STRING); //-- 0
		query.addScalar("AMPHUR_NAME", Hibernate.STRING); //-- 1
		query.addScalar("TAMBOL_NAME", Hibernate.STRING); //-- 2
		query.addScalar("BUSINESS_TOURISM_AMT", Hibernate.INTEGER); //-- 3
		
		
		query.setProperties(params);
		
		List<ReportBusinessTourismByPlaceDTO> reportBusinessTourismByPlaces = new ArrayList<ReportBusinessTourismByPlaceDTO>();
		List<Object[]> objs = query.list(); 
		for(Object[] obj : objs){
			ReportBusinessTourismByPlaceDTO reportBusinessTourismByPlace = new ReportBusinessTourismByPlaceDTO();
			reportBusinessTourismByPlace.setProvinceName((String)obj[0]);
			reportBusinessTourismByPlace.setAmphurName((String)obj[1]);
			reportBusinessTourismByPlace.setTambolName((String)obj[2]);
			reportBusinessTourismByPlace.setBusinessTourismAmt((Integer)obj[3]);
			
			
			reportBusinessTourismByPlaces.add(reportBusinessTourismByPlace);
		}
		return reportBusinessTourismByPlaces;
	}
}
