package sss.dot.tourism.dao.certificate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.ReqLicenseCertificate;
import sss.dot.tourism.dto.certificate.ReqLicenseCertificateDTO;
import sss.dot.tourism.util.DateUtils;

@Repository("reqLicenseCertificateDAO")
public class ReqLicenseCertificateDAO extends BaseDAO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8234521051207491499L;

	public ReqLicenseCertificateDAO() {
		this.domainObj = ReqLicenseCertificate.class;
	}

	public List<Object[]> findByPaging(ReqLicenseCertificateDTO param,
			int start, int limit, long orgId) throws Exception {
		Map<String, Object> parames = new HashMap<String, Object>();

		StringBuilder hql = new StringBuilder();

		hql.append(" SELECT  ");
		hql.append(" TR.LICENSE_NO ");
		hql.append(" ,TR.TRADER_NAME ");
		hql.append(" ,TR.TRADER_NAME_EN ");
		hql.append(" ,TR.TRADER_CATEGORY ");
		hql.append(" ,CER.REQ_DATE ");
		hql.append(" ,CER.APPROVE_DATE ");
		hql.append(" ,CER.APPROVE_STATUS ");
		hql.append(" ,CER.PRINT_DATE ");
		hql.append(" ,CER.CER_ID ");
		// hql.append(" ,CER.REQ_NAME ");
		// hql.append(" ,CER.CONTACT_NO ");
		hql.append("  FROM TRADER TR ");
		hql.append(" INNER JOIN REQ_LICENSE_CERTIFICATE CER ");
		hql.append(" ON(CER.LICENSE_NO = TR.LICENSE_NO) ");
		hql.append(" WHERE CER.RECORD_STATUS = 'N' ");
		hql.append(" AND TR.RECORD_STATUS = 'N' ");
		hql.append(" AND TR.BRANCH_TYPE IS NULL ");

		if ((param.getTraderType() != null)
				&& (!param.getTraderType().isEmpty())) {
			hql.append("  AND TR.LICENSE_NO = :LICENSE_NO ");
			parames.put("LICENSE_NO", param.getTraderType());
		}

		if ((param.getTraderName() != null)
				&& (!param.getTraderName().isEmpty())) {
			hql.append(" AND TR.TRADER_NAME LIKE :TRADER_NAME'  ");
			parames.put("TRADER_NAME", "%" + param.getTraderName() + "%");
		}

		if ((null != param.getReqDateFrom())
				&& (!param.getReqDateFrom().equals(""))) {
			if ((null != param.getReqDateTo())
					&& (!param.getReqDateTo().equals(""))) {
				hql.append("  AND ( CER.REQ_DATE >=  convert(datetime, :REQ_DATE_START_DATE ,20) ");
				hql.append(" AND CER.REQ_DATE <  convert(datetime, :REQ_DATE__END_DATE ,20) ) ");

				String sendDtmFrom = DateUtils.getDateMSSQLFormat(
						param.getReqDateFrom(), "00:00");
				String sendDtmTo = DateUtils.getDateMSSQLFormat(
						param.getReqDateTo(), "23:59");

				parames.put("REQ_DATE_START_DATE", sendDtmFrom);
				parames.put("REQ_DATE__END_DATE", sendDtmTo);
			}
		}

		if (orgId > 0) {
			hql.append("  AND TR.ORG_ID = :ORG_ID ");
			parames.put("ORG_ID", orgId);
		}
		

		hql.append(" ORDER BY CER.REQ_DATE, CER.LICENSE_NO ");

		SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
		sqlQuery.setFirstResult(start);
		sqlQuery.setMaxResults(limit);

		sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING); // 0
		sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING); // 1
		sqlQuery.addScalar("TRADER_NAME_EN", Hibernate.STRING); // 2
		sqlQuery.addScalar("TRADER_CATEGORY", Hibernate.STRING);// 3
		sqlQuery.addScalar("REQ_DATE", Hibernate.DATE);// 4
		sqlQuery.addScalar("APPROVE_DATE", Hibernate.DATE);// 5
		sqlQuery.addScalar("APPROVE_STATUS", Hibernate.STRING);// 6
		sqlQuery.addScalar("PRINT_DATE", Hibernate.DATE);// 7
		sqlQuery.addScalar("CER_ID", Hibernate.LONG);// 8

		sqlQuery.setProperties(parames);
		List<Object[]> result = sqlQuery.list();

		return result;
	}
}
