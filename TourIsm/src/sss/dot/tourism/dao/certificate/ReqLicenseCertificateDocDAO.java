package sss.dot.tourism.dao.certificate;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.MasDocument;
import sss.dot.tourism.domain.ReqLicenseCertificateDoc;
import sss.dot.tourism.dto.certificate.ReqLicenseCertificateDTO;
import sss.dot.tourism.util.RegistrationType;
import sss.dot.tourism.util.TraderType;

@Repository("reqLicenseCertificateDocDAO")
public class ReqLicenseCertificateDocDAO extends BaseDAO{


	/**
	 * 
	 */
	private static final long serialVersionUID = -8164994321582320850L;

	public ReqLicenseCertificateDocDAO()
	{
		this.domainObj = ReqLicenseCertificateDoc.class;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<ReqLicenseCertificateDoc> findAll(ReqLicenseCertificateDTO dto)
	  {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  ReqLicenseCertificateDoc as dep ");
		hql.append(" where dep.recordStatus = 'N' ");
		
		if(dto.isCerIdNotEmpty()){
			hql.append(" and dep.reqLicenseCertificate.cerId = ? ");
			params.add(dto.getCerId());
		}

		hql.append(" order by dep.masDocument.docIndex ");
		
		return (List<ReqLicenseCertificateDoc>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	  }
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<MasDocument> findMasDoc()
	  {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  MasDocument as dep ");
		hql.append(" where dep.recordStatus = 'N' ");

		hql.append(" and dep.licenseType = ? ");
		params.add(TraderType.TOUR_COMPANIES.getStatus());
		hql.append(" and dep.registrationType = ? ");
		params.add(RegistrationType.TOUR_COMPANIES_CERTIFICATE.getStatus());

		hql.append(" order by dep.docIndex ");
		
		return (List<MasDocument>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	  }

}
