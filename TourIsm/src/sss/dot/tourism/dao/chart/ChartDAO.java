package sss.dot.tourism.dao.chart;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.User;

@Repository("chartDAO")
public class ChartDAO extends BaseDAO{
	private static final long serialVersionUID = -6455075565457388105L;

	public ChartDAO()
	{
		this.domainObj = Trader.class;
	}
	
	//GUIDE
	
	//หาจำนวนใบอนุญาตของ guide ที่ถูกยกเลิกจาก licenseNo และ traderCategory และ orgId
	//TRADER
	public List<Object[]> findGuideByTraderCategoryAndOrgId(RegistrationDTO dto)throws Exception
	{
		Map<String,Object> parames = new HashMap<String,Object>();
	    
	    StringBuilder hql = new StringBuilder();
	    
		hql.append(" SELECT COUNT(*) AS C FROM TRADER TR ");   

//Oat Add 08/05/58
		 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
		 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Add 08/05/58
		
		hql.append(" WHERE TR.TRADER_TYPE = 'G' AND TR.RECORD_STATUS = 'N' ");            
		hql.append(" AND TR.LICENSE_NO = :TRADER_LICENSE_NO");
		parames.put("TRADER_LICENSE_NO", dto.getLicenseNo());
		
		//TRADER_CATEGORY
		hql.append(" AND TR.TRADER_CATEGORY = :TRADER_CATEGORY");
		parames.put("TRADER_CATEGORY", dto.getTraderCategory());
		
//Oat Edit 08/05/58
		 hql.append(" AND TA.ADDRESS_TYPE = 'S' AND TA.RECORD_STATUS = 'N' ");
		 
		 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
		 parames.put("TRADER_ORG_ID", dto.getOrgId());
		 
//			//ORG_ID
//			hql.append(" AND TR.ORG_ID = :ORG_ID ");
//			parames.put("ORG_ID", dto.getOrgId());
	 
//End Oat Edit 08/05/58


		SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());

		//TRADER_CATEGORY
	    sqlQuery.addScalar("C", Hibernate.LONG); 
	    
	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
  
	    return result;
	}
	
	//ตารางมัคคุเทศก์ ต่ออายุ
	public List<Object[]> findExcelGuideRenew(RegistrationDTO dto)throws Exception
	{
		Map<String,Object> parames = new HashMap<String,Object>();
	    
	    StringBuilder hql = new StringBuilder();
	    
	    DateFormat ft =  DateUtils.getProcessDateFormatThai();

	    hql.append(" SELECT COUNT(*) AS ORG FROM TRADER TR 	INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID) ");
	    
//Oat Edit 24/04/58
		 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
		 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Edit 24/04/58
	    
	    hql.append(" WHERE TR.TRADER_TYPE = 'G' AND TR.RECORD_STATUS IN ('N','H') ");
		hql.append(" AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS = 'N' ");
		hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL ");
//		hql.append(" AND RE.APPROVE_DATE BETWEEN CONVERT(DATETIME, '01/12/2013', 103) AND CONVERT(DATETIME, '30/12/2013', 103) ");
		//ApproveDateFrom
		 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
		 {
			 
			 hql.append("  AND ( RE.APPROVE_DATE >=  convert(datetime, :APPROVE_START_DATE ,20) ");
				hql.append(" AND RE.APPROVE_DATE <=  convert(datetime, :APPROVE_END_DATE ,20) ) ");
				
				String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
				String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
					
				parames.put("APPROVE_START_DATE", sendDtmFrom);
				parames.put("APPROVE_END_DATE", sendDtmTo);
			 
//		 	hql.append(" AND RE.APPROVE_DATE BETWEEN :APPROVE_DATE_FORM ");
//		    parames.put("APPROVE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
//		    
//		    //ApproveDateTo
//		    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
//		    {
//		    	hql.append(" AND :APPROVE_DATE_TO ");
//			    parames.put("APPROVE_DATE_TO", ft.parse(dto.getApproveDateTo()));
//		    }
//		    else
//		    {
//		    	hql.append(" AND :APPROVE_DATE_TO ");
//			    parames.put("APPROVE_DATE_TO", new Date());
//		    }
		 }
		 
		hql.append(" AND TR.TRADER_CATEGORY = :TRADER_CATEGORY ");
		parames.put("TRADER_CATEGORY", dto.getTraderCategory());
		
		//orgId		
//Oat Edit 24/04/58
//		hql.append(" AND TR.ORG_ID = :ORG_ID ");
//		parames.put("ORG_ID", dto.getOrgId());
		 
		 hql.append(" AND TA.ADDRESS_TYPE = 'S' AND TA.RECORD_STATUS = 'N' ");
		 
		 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
		 parames.put("TRADER_ORG_ID", dto.getOrgId());
//End Oat Edit 24/04/58
		
		//R, T
		hql.append(" AND RE.REGISTRATION_TYPE = :REGISTRATION_TYPE ");
		parames.put("REGISTRATION_TYPE", dto.getRegistrationType());
	    
	    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());

	    sqlQuery.addScalar("ORG", Hibernate.LONG); 
	    
	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
  
	    return result;
	}
	
//	//ตารางสรุปจำนวนผู้ขอรับใบอนุญาตเป็นมัคคุเทศก์(ยื่นใหม่)
//	public List<Object[]> findExcelGuideNew(RegistrationDTO dto)throws Exception
//	{
//		Map<String,Object> parames = new HashMap<String,Object>();
//	    
//	    StringBuilder hql = new StringBuilder();
//	    
//	    DateFormat ft =  DateUtils.getProcessDateFormatThai();
//
//	    hql.append(" SELECT COUNT(*) AS ORG FROM TRADER TR 	INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID) ");
//	    hql.append(" WHERE TR.TRADER_TYPE = 'G' AND TR.RECORD_STATUS IN ('N','H') ");
//		hql.append(" AND RE.REGISTRATION_TYPE = 'N' AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS = 'N' ");
//		hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL ");
////		hql.append(" AND RE.APPROVE_DATE BETWEEN CONVERT(DATETIME, '01/12/2013', 103) AND CONVERT(DATETIME, '30/12/2013', 103) ");
//		//ApproveDateFrom
//		 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
//		 {
////		 	hql.append(" AND RE.APPROVE_DATE BETWEEN :APPROVE_DATE_FORM ");
////		    parames.put("APPROVE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
////		    
////		    //ApproveDateTo
////		    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
////		    {
////		    	hql.append(" AND :APPROVE_DATE_TO ");
////			    parames.put("APPROVE_DATE_TO", ft.parse(dto.getApproveDateTo()));
////		    }
////		    else
////		    {
////		    	hql.append(" AND :APPROVE_DATE_TO ");
////			    parames.put("APPROVE_DATE_TO", new Date());
////		    }
//			 
//			 hql.append("  AND ( RE.APPROVE_DATE >=  convert(datetime, :APPROVE_START_DATE ,20) ");
//				hql.append(" AND RE.APPROVE_DATE <=  convert(datetime, :APPROVE_END_DATE ,20) ) ");
//				
//				String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
//				String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
//					
//				parames.put("APPROVE_START_DATE", sendDtmFrom);
//				parames.put("APPROVE_END_DATE", sendDtmTo);
//		 }
//		 
//		hql.append(" AND TR.TRADER_CATEGORY = :TRADER_CATEGORY ");
//		parames.put("TRADER_CATEGORY", dto.getTraderCategory());
//		
//		hql.append(" AND TR.ORG_ID = :ORG_ID ");
//		parames.put("ORG_ID", dto.getOrgId());
//	    
//		 
//	    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
//
//	    sqlQuery.addScalar("ORG", Hibernate.LONG); 
//	    
//	    sqlQuery.setProperties(parames);
//	    List<Object[]>  result = sqlQuery.list();
//  
//	    return result;
//	}
	
	
	//หา traderCategory ของ guide ที่ถูกยกเลิกจาก licenseNo
	//TRADER
	public List<Object[]> findGuideTraderCategory(RegistrationDTO dto)throws Exception
	{
		Map<String,Object> parames = new HashMap<String,Object>();
	    
	    StringBuilder hql = new StringBuilder();
	    
		hql.append(" SELECT TR.TRADER_CATEGORY FROM TRADER TR ");            
		hql.append(" WHERE TR.TRADER_TYPE = 'G' AND TR.RECORD_STATUS = 'N' ");            
		hql.append(" AND TR.LICENSE_NO = :TRADER_LICENSE_NO");
		parames.put("TRADER_LICENSE_NO", dto.getLicenseNo());

		SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());

		//TRADER_CATEGORY
	    sqlQuery.addScalar("TRADER_CATEGORY", Hibernate.STRING); 
	    
	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
  
	    return result;
	}
	
	//หา licenseNo ของ guide ที่ถูกยกเลิกในช่วงวัน
	//DEACTIVATE_LICENSE
	public List<Object[]> findGuideDeactivateLicense(RegistrationDTO dto)throws Exception
	{
		Map<String,Object> parames = new HashMap<String,Object>();
	    
	    StringBuilder hql = new StringBuilder();
	    
	    DateFormat ft =  DateUtils.getProcessDateFormatThai();
	    
		hql.append(" SELECT DL.LICENSE_NO FROM DEACTIVATE_LICENSE DL ");   
		
//Oat Add 08/05/58
		 hql.append(" INNER JOIN TRADER TR ON (DL.TRADER_TYPE = TR.TRADER_TYPE AND DL.LICENSE_NO = TR.LICENSE_NO) ");
//		 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
//		 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Add 08/05/58
		
		hql.append(" WHERE DL.TRADER_TYPE = 'G' AND DL.APPROVE_STATUS IN ('A','C') AND DL.RECORD_STATUS = 'N'  ");            
		//BookDateFrom
		 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
		 {
		 	hql.append(" AND DL.BOOK_DATE BETWEEN :BOOK_DATE_FORM ");
			    parames.put("BOOK_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
			    
			    //BookDateTo
			    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
			    {
			    	hql.append(" AND :BOOK_DATE_TO ");
				    parames.put("BOOK_DATE_TO", ft.parse(dto.getApproveDateTo()));
			    }
			    else
			    {
			    	hql.append(" AND :BOOK_DATE_TO ");
				    parames.put("BOOK_DATE_TO", new Date());
			    }
		 }
		 
//Oat Edit 08/05/58
		 hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL  ");
		 hql.append(" AND TR.TRADER_TYPE = 'G' AND TR.LICENSE_STATUS = 'C' AND TR.RECORD_STATUS = 'N' ");
//		 hql.append(" AND TA.ADDRESS_TYPE = 'S' AND TA.RECORD_STATUS = 'N' ");
		 
//		 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
//		 parames.put("TRADER_ORG_ID", dto.getOrgId());
		 
//		 //เพิ่มเข้าไปทีหลังนะ เพื่อให้ใช้ร่วมกันได้
		 if(dto.getOrgId() > 0)
		 {
			 hql.append(" AND DL.ORG_ID = :ORG_ID ");
			parames.put("ORG_ID", dto.getOrgId());
		 }
	 
		 hql.append("Group by DL.LICENSE_NO ");
//End Oat Edit 08/05/58

		SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());

		//LICENSE_NO
	    sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING); 
	    
	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
  
	    return result;
	}
	
	//ตารางแสดงจำนวนผู้ที่ได้รับใบอนุญาตเป็นมัคคุเทศก์ ในภาพรวม
	public List<Object[]> findExcelGuideAll(RegistrationDTO dto)throws Exception
	{
		Map<String,Object> parames = new HashMap<String,Object>();
	    
	    StringBuilder hql = new StringBuilder();
	    
	    DateFormat ft =  DateUtils.getProcessDateFormatThai();

	    hql.append(" SELECT COUNT(*) AS ORG");
	    
	    hql.append(" ,TR.ORG_ID AS ORG_ID");
	    hql.append(" FROM TRADER TR ");
	    hql.append(" INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID) ");

//Oat Add 12/05/58
//		 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
//		 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Add 12/05/58	    
	    
	    hql.append(" WHERE TR.TRADER_TYPE = 'G' AND TR.RECORD_STATUS IN ('N') ");
		hql.append(" AND TR.LICENSE_STATUS = 'N' AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS = 'N' ");
		hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL ");
		
		//OAT ADD 03/11/58
		//OAT EDIT 05/01/59
		hql.append(" AND RE.REGISTRATION_TYPE = 'N'  ");
		
//		hql.append(" AND RE.APPROVE_DATE BETWEEN CONVERT(DATETIME, '01/12/2013', 103) AND CONVERT(DATETIME, '30/12/2013', 103) ");
		//ApproveDateFrom
		 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
		 {
			 hql.append("  and ( RE.APPROVE_DATE >=  convert(datetime, :APPROVE_START_DATE ,20) ");
				hql.append(" and RE.APPROVE_DATE <=  convert(datetime, :APPROVE_END_DATE ,20) ) ");
				
				String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
				String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
					
				parames.put("APPROVE_START_DATE", sendDtmFrom);
				parames.put("APPROVE_END_DATE", sendDtmTo);
			 
//		 	hql.append(" AND RE.APPROVE_DATE BETWEEN :APPROVE_DATE_FORM ");
//		    parames.put("APPROVE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
//		    
//		    //ApproveDateTo
//		    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
//		    {
//		    	hql.append(" AND :APPROVE_DATE_TO ");
//			    parames.put("APPROVE_DATE_TO", ft.parse(dto.getApproveDateTo()));
//		    }
//		    else
//		    {
//		    	hql.append(" AND :APPROVE_DATE_TO ");
//			    parames.put("APPROVE_DATE_TO", new Date());
//		    }
		 }

		hql.append(" AND TR.TRADER_CATEGORY = :TRADER_CATEGORY ");
		parames.put("TRADER_CATEGORY", dto.getTraderCategory());
		

//Oat Edit 12/05/58
		hql.append(" AND TR.ORG_ID = :ORG_ID ");
		parames.put("ORG_ID", dto.getOrgId());
		
		hql.append(" GROUP BY TR.ORG_ID ");
			 
//			 hql.append(" AND TA.ADDRESS_TYPE = 'S' AND TA.RECORD_STATUS = 'N' ");
			 
//			 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
//			 parames.put("TRADER_ORG_ID", dto.getOrgId());
//End Oat Edit 12/05/58
		 
	    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());

	    sqlQuery.addScalar("ORG", Hibernate.INTEGER); 
	    sqlQuery.addScalar("ORG_ID", Hibernate.INTEGER); 
	    
	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
	    
//	    System.out.println(result.size());
  
	    return result;
	}
	
	//กราฟแสดงจำนวนผู้ที่ได้รับใบอนุญาตเป็นมัคคุเทศก์ ในภาพรวม
	public List<Object[]> findChartPieGuideAll(RegistrationDTO dto)throws Exception
		{
			Map<String,Object> parames = new HashMap<String,Object>();
		    
		    StringBuilder hql = new StringBuilder();
		    
		    DateFormat ft =  DateUtils.getProcessDateFormatThai();

		    hql.append(" SELECT COUNT(*) AS ORG FROM TRADER TR 	INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID) ");
		    hql.append(" WHERE TR.TRADER_TYPE = 'G' AND TR.RECORD_STATUS IN ('N') ");
			hql.append(" AND TR.LICENSE_STATUS = 'N' AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS = 'N' ");
			hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL ");
//			hql.append(" AND RE.APPROVE_DATE BETWEEN CONVERT(DATETIME, '01/12/2013', 103) AND CONVERT(DATETIME, '30/12/2013', 103) ");
			//ApproveDateFrom
			 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
			 {
//			 	hql.append(" AND RE.APPROVE_DATE BETWEEN :APPROVE_DATE_FORM ");
//			    parames.put("APPROVE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
//			    
//			    //ApproveDateTo
//			    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
//			    {
//			    	hql.append(" AND :APPROVE_DATE_TO ");
//				    parames.put("APPROVE_DATE_TO", ft.parse(dto.getApproveDateTo()));
//			    }
//			    else
//			    {
//			    	hql.append(" AND :APPROVE_DATE_TO ");
//				    parames.put("APPROVE_DATE_TO", new Date());
//			    }
				 
				 hql.append("  AND ( RE.APPROVE_DATE >=  convert(datetime, :APPROVE_START_DATE ,20) ");
					hql.append(" AND RE.APPROVE_DATE <=  convert(datetime, :APPROVE_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
						
					parames.put("APPROVE_START_DATE", sendDtmFrom);
					parames.put("APPROVE_END_DATE", sendDtmTo);
			 }
			 
			hql.append(" AND TR.TRADER_CATEGORY = :TRADER_CATEGORY ");
			parames.put("TRADER_CATEGORY", dto.getTraderCategory());
		    
			 
		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());

		    sqlQuery.addScalar("ORG", Hibernate.LONG); 
		    
		    sqlQuery.setProperties(parames);
		    List<Object[]>  result = sqlQuery.list();
	  
		    return result;
		}
	
	//รายงานการจดทะเบียนมัคคุเทศก์จำแนกเป็นรายภาค (ค้นหาทีละสำนักงานตาม ordId) ยอดรวม ยื่นใหม่
		public List<Object[]> findExcelGuideOrg(RegistrationDTO dto)throws Exception
		{
		 	Map<String,Object> parames = new HashMap<String,Object>();
		    
		    StringBuilder hql = new StringBuilder();
		    
		    DateFormat ft =  DateUtils.getProcessDateFormatThai();

		    hql.append(" SELECT A1.OLD, A2.NEWG, A3.NEWA FROM ");
		    
		    hql.append(" (SELECT COUNT(*) AS OLD ");
		    hql.append(" FROM TRADER TR 	INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID) ");
		    
//Oat Add 11/05/58
//			 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
//			 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Add 11/05/58
		    
		    hql.append(" WHERE TR.TRADER_TYPE = 'G' AND TR.RECORD_STATUS IN ('N')  ");
		    hql.append(" AND TR.LICENSE_STATUS = 'N' AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS = 'N'  ");
		    hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL ");
//			    hql.append(" AND RE.APPROVE_DATE < CONVERT(DATETIME, '01/12/2014', 103)  ");
		    //ApproveDateFrom
			 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
			 {
//			 	hql.append(" AND RE.APPROVE_DATE < :APPROVE_DATE_FORM ");
//				parames.put("APPROVE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
				
				hql.append("  AND ( RE.APPROVE_DATE <  convert(datetime, :APPROVE_START_DATE ,20) ) ");
				String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");					
				parames.put("APPROVE_START_DATE", sendDtmFrom);
			
			 }
			 
//Oat Edit 11/05/58
			 if(dto.getOrgId()> 0)
			 {
				 hql.append(" AND TR.ORG_ID = :ORG_ID ");
				parames.put("ORG_ID", dto.getOrgId());
			 }
				 
//				 hql.append(" AND TA.ADDRESS_TYPE = 'S' AND TA.RECORD_STATUS = 'N' ");
				 
//				 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
//			     parames.put("TRADER_ORG_ID", dto.getOrgId());
//End Oat Edit 11/05/58
			 
		    hql.append(" ) AS A1  ");
		    	
		    hql.append(" ,	");
		    hql.append(" (SELECT COUNT(*) AS NEWG ");
		    hql.append(" FROM TRADER TR 	INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID) ");
		    
//Oat Add 11/05/58
//			 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
//			 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Add 11/05/58
			 
		    hql.append(" WHERE TR.TRADER_TYPE = 'G' AND TR.RECORD_STATUS IN ('N','H')  ");
		    hql.append(" AND RE.REGISTRATION_TYPE = 'N' AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS = 'N' ");
		    hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL  ");
//			    hql.append(" AND RE.APPROVE_DATE BETWEEN CONVERT(DATETIME, '01/12/2013', 103) AND CONVERT(DATETIME, '30/12/2013', 103) ");
		    //ApproveDateFrom
			 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
			 {
//			 	hql.append(" AND RE.APPROVE_DATE BETWEEN :APPROVE_DATE_FORM ");
//			    parames.put("APPROVE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
//			    
//			    //ApproveDateTo
//			    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
//			    {
//			    	hql.append(" AND :APPROVE_DATE_TO ");
//				    parames.put("APPROVE_DATE_TO", ft.parse(dto.getApproveDateTo()));
//			    }
//			    else
//			    {
//			    	hql.append(" AND :APPROVE_DATE_TO ");
//				    parames.put("APPROVE_DATE_TO", new Date());
//			    }
				 
				    hql.append("  AND ( RE.APPROVE_DATE >=  convert(datetime, :APPROVE_START_DATE ,20) ");
					hql.append(" AND RE.APPROVE_DATE <=  convert(datetime, :APPROVE_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
						
					parames.put("APPROVE_START_DATE", sendDtmFrom);
					parames.put("APPROVE_END_DATE", sendDtmTo);
			 }
			 
//Oat Edit 11/05/58
			 if(dto.getOrgId()> 0)
			 {
				 hql.append(" AND TR.ORG_ID = :ORG_ID ");
				parames.put("ORG_ID", dto.getOrgId());
			 }
				 
//				 hql.append(" AND TA.ADDRESS_TYPE = 'S' AND TA.RECORD_STATUS = 'N' ");
				 
//				 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
//				 parames.put("TRADER_ORG_ID", dto.getOrgId());
//End Oat Edit 11/05/58
			 
		    hql.append(" AND TR.TRADER_CATEGORY IN ('100','101') ) AS A2 ");
		    
		    //A3
		    hql.append(" ,	");
		    hql.append(" (SELECT COUNT(*) AS NEWA ");
		    hql.append(" FROM TRADER TR 	INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID) ");
		    
//Oat Add 11/05/58
//			 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
//			 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Add 11/05/58
			 
		    hql.append(" WHERE TR.TRADER_TYPE = 'G' AND TR.RECORD_STATUS IN ('N','H')  ");
		    hql.append(" AND RE.REGISTRATION_TYPE = 'N' AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS = 'N'  ");
		    hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL  ");
//			    hql.append(" AND RE.APPROVE_DATE BETWEEN CONVERT(DATETIME, '01/12/2013', 103) AND CONVERT(DATETIME, '30/12/2013', 103) ");
		    //ApproveDateFrom
			 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
			 {
//			 	hql.append(" AND RE.APPROVE_DATE BETWEEN :APPROVE_DATE_FORM ");
//			    parames.put("APPROVE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
//			    
//			    //ApproveDateTo
//			    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
//			    {
//			    	hql.append(" AND :APPROVE_DATE_TO ");
//				    parames.put("APPROVE_DATE_TO", ft.parse(dto.getApproveDateTo()));
//			    }
//			    else
//			    {
//			    	hql.append(" AND :APPROVE_DATE_TO ");
//				    parames.put("APPROVE_DATE_TO", new Date());
//			    }
				 
				 hql.append("  AND ( RE.APPROVE_DATE >=  convert(datetime, :APPROVE_START_DATE ,20) ");
					hql.append(" AND RE.APPROVE_DATE <=  convert(datetime, :APPROVE_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
						
					parames.put("APPROVE_START_DATE", sendDtmFrom);
					parames.put("APPROVE_END_DATE", sendDtmTo);
			 }
			 
//Oat Edit 11/05/58
			 if(dto.getOrgId()> 0)
			 {
				 hql.append(" AND TR.ORG_ID = :ORG_ID ");
				parames.put("ORG_ID", dto.getOrgId());
			 }
				 
//				 hql.append(" AND TA.ADDRESS_TYPE = 'S' AND TA.RECORD_STATUS = 'N' ");
				 
//				 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
//				 parames.put("TRADER_ORG_ID", dto.getOrgId());
//End Oat Edit 11/05/58
			 
		    hql.append(" AND TR.TRADER_CATEGORY IN ('200','201','202','203','204','205','206','207') ) AS A3 ");
		    
			SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());

		    sqlQuery.addScalar("OLD", Hibernate.LONG); 
		    sqlQuery.addScalar("NEWG", Hibernate.LONG); 
		    sqlQuery.addScalar("NEWA", Hibernate.LONG); 
		    
		    sqlQuery.setProperties(parames);
		    List<Object[]>  result = sqlQuery.list();
	  
		    return result;
		}
	
	//ตาราง ภาพรวม มัคคุเทศก์ ทั้งส่วนกลาง และสาขา จำแนกตามประเภท (ค้นหาทีละประเภทมัคคุเทศก์ตาม traderCategory)
	public List<Object[]> findExcelGuideType(RegistrationDTO dto)throws Exception
	{
	 	Map<String,Object> parames = new HashMap<String,Object>();
	    
	    StringBuilder hql = new StringBuilder();
	    
	    DateFormat ft =  DateUtils.getProcessDateFormatThai();

	    hql.append(" SELECT A1.OLD, A2.NEW FROM ");
	    
	    hql.append(" (SELECT COUNT(*) AS OLD ");
	    hql.append(" FROM TRADER TR 	INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID) ");
	  //Sek Add 09/02/60
//		 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
//		 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//Sek Oat Add 09/02/60
	    
	    hql.append(" WHERE TR.TRADER_TYPE = 'G' AND TR.RECORD_STATUS IN ('N')  ");
	    hql.append(" AND TR.LICENSE_STATUS = 'N' AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS = 'N' ");
	    hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL ");
//		    hql.append(" AND RE.APPROVE_DATE < CONVERT(DATETIME, '01/12/2014', 103)  ");
	    //ApproveDateFrom
		 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
		 {
//		 	hql.append(" AND RE.APPROVE_DATE < :APPROVE_DATE_FORM ");
//			parames.put("APPROVE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
			 hql.append("  AND ( RE.APPROVE_DATE <  convert(datetime, :APPROVE_START_DATE ,20) ) ");
			 String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
					
			parames.put("APPROVE_START_DATE", sendDtmFrom);
		
		 }
//		 hql.append(" AND TA.ADDRESS_TYPE = 'S' AND TA.RECORD_STATUS = 'N' ");
	    hql.append(" AND TR.TRADER_CATEGORY = :TRADER_CATEGORY) AS A1  ");
	    parames.put("TRADER_CATEGORY", dto.getTraderCategory());
	    	
	    hql.append(" ,	");
	    hql.append(" (SELECT COUNT(*) AS NEW ");
	    hql.append(" FROM TRADER TR 	INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID) ");
	  //Sek Add 09/02/60
//		 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
//		 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//Sek Oat Add 09/02/60
	    hql.append(" WHERE TR.TRADER_TYPE = 'G' AND TR.RECORD_STATUS IN ('N','H')  ");
	    
	    //OAT EDIT 05/01/59
//	    hql.append(" AND RE.REGISTRATION_TYPE = 'N' AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS = 'N'  ");
	    hql.append(" AND RE.REGISTRATION_TYPE = 'N' AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS IN ('N','H') ");
	    
	    hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL  ");
//		    hql.append(" AND RE.APPROVE_DATE BETWEEN CONVERT(DATETIME, '01/12/2013', 103) AND CONVERT(DATETIME, '30/12/2013', 103) ");
	    //ApproveDateFrom
		 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
		 {
//		 	hql.append(" AND RE.APPROVE_DATE BETWEEN :APPROVE_DATE_FORM ");
//		    parames.put("APPROVE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
//		    
//		    //ApproveDateTo
//		    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
//		    {
//		    	hql.append(" AND :APPROVE_DATE_TO ");
//			    parames.put("APPROVE_DATE_TO", ft.parse(dto.getApproveDateTo()));
//		    }
//		    else
//		    {
//		    	hql.append(" AND :APPROVE_DATE_TO ");
//			    parames.put("APPROVE_DATE_TO", new Date());
//		    }
			 
			 	hql.append("  AND ( RE.APPROVE_DATE >=  convert(datetime, :APPROVE_START_DATE ,20) ");
				hql.append(" AND RE.APPROVE_DATE <=  convert(datetime, :APPROVE_END_DATE ,20) ) ");
				
				String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
				String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
					
				parames.put("APPROVE_START_DATE", sendDtmFrom);
				parames.put("APPROVE_END_DATE", sendDtmTo);
		 }
//		 hql.append(" AND TA.ADDRESS_TYPE = 'S' AND TA.RECORD_STATUS = 'N' ");
		 
	     hql.append(" AND TR.TRADER_CATEGORY = :TRADER_CATEGORY) AS A2 ");
	    parames.put("TRADER_CATEGORY", dto.getTraderCategory()); 
	    
		SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());

	    sqlQuery.addScalar("OLD", Hibernate.LONG); 
	    sqlQuery.addScalar("NEW", Hibernate.LONG); 
	    
	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
  
	    return result;
	}
	
	//BUSINESS
//	//ค้นหาตารางแสดงจำนวนผู้มายื่นเรื่องขอรับใบอนุญาตประกอบธุรกิจนำเที่ยว(รายใหม่) (ค้นหาทีละสำนักงานตาม ordId)
//	public List<Object[]> findExcelBusinessNew(RegistrationDTO dto)throws Exception

//ค้นหาตารางแสดง ยกเลิกและรับหลักประกันคืนและขอรับหลักประกันคืน(กรณีถูกเพิกถอน)*****
	//ขอรับหลักประกันคืน(กรณีถูกเพิกถอน)
	public List<Object[]> findBusinessRevokeGuarantee(RegistrationDTO dto)throws Exception
	{
		Map<String,Object> parames = new HashMap<String,Object>();
	    
	    StringBuilder hql = new StringBuilder();
	    
	    DateFormat ft =  DateUtils.getProcessDateFormatThai();

	    hql.append(" SELECT A1.OUTBOUND, A2.INBOUND, A3.COUNTRY ,A4.AREA FROM ");
//OUTBOUND
	    hql.append(" (SELECT COUNT(*) AS OUTBOUND FROM REVOKE_LICENSE RL  ");
		hql.append(" INNER JOIN REVOKE_MAP_TRADER RM  ");
		hql.append(" ON (RL.REVOKE_ID = RM.REVOKE_ID) ");
		hql.append(" INNER JOIN TRADER TR ");
		hql.append(" ON(RM.TRADER_TYPE = TR.TRADER_TYPE AND RM.LICENSE_NO = TR.LICENSE_NO) ");
		hql.append(" INNER JOIN GUARANTEE G  ");
		hql.append(" ON(TR.TRADER_TYPE = G.TRADER_TYPE AND TR.LICENSE_NO = G.LICENSE_NO) ");
		
//Oat Edit 22/04/58
		 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
		 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Edit 22/04/58
		
		hql.append(" WHERE  ");
		//TRADER
		hql.append(" TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS = 'N' AND TR.BRANCH_TYPE IS NULL "); 
		
//Oat Edit 22/04/58
//		hql.append(" AND TR.TRADER_CATEGORY = '100' AND TR.ORG_ID = :TRADER_ORG_ID ");
//		parames.put("TRADER_ORG_ID", dto.getOrgId());
		 
		 hql.append(" AND TR.TRADER_CATEGORY = '100' AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS = 'N' ");
		 
		 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
		 parames.put("TRADER_ORG_ID", dto.getOrgId());
//End Oat Edit 22/04/58
		
		//REVOKE
		hql.append(" AND RL.REVOKE_STATUS IN ('R','C') AND RL.RECORD_STATUS = 'N' ");
//		hql.append(" AND RL.REVOKE_DATE BETWEEN CONVERT(DATETIME, '01/10/2013', 103)  ");
//		hql.append(" AND CONVERT(DATETIME, '30/12/2014', 103) ");
		//RevokeDateFrom
		 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
		 {
		 	hql.append(" AND RL.REVOKE_DATE BETWEEN :REVOKE_DATE_FORM ");
			    parames.put("REVOKE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
			    
			    //RevokeDateTo
			    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
			    {
			    	hql.append(" AND :REVOKE_DATE_TO ");
				    parames.put("REVOKE_DATE_TO", ft.parse(dto.getApproveDateTo()));
			    }
			    else
			    {
			    	hql.append(" AND :REVOKE_DATE_TO ");
				    parames.put("REVOKE_DATE_TO", new Date());
			    }
		 }
		//GUARANTEE
		hql.append(" AND G.GUARANTEE_STATUS = 'R' AND G.RECORD_STATUS = 'N') AS A1 ");

//INBOUND
		hql.append(" ,(SELECT COUNT(*) AS INBOUND FROM REVOKE_LICENSE RL  ");
		hql.append(" INNER JOIN REVOKE_MAP_TRADER RM  ");
		hql.append(" ON (RL.REVOKE_ID = RM.REVOKE_ID) ");
		hql.append(" INNER JOIN TRADER TR ");
		hql.append(" ON(RM.TRADER_TYPE = TR.TRADER_TYPE AND RM.LICENSE_NO = TR.LICENSE_NO) ");
		hql.append(" INNER JOIN GUARANTEE G  ");
		hql.append(" ON(TR.TRADER_TYPE = G.TRADER_TYPE AND TR.LICENSE_NO = G.LICENSE_NO) ");
		
//Oat Edit 22/04/58
		 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
		 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Edit 22/04/58
		 
		hql.append(" WHERE  ");
		//TRADER
		hql.append(" TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS = 'N' AND TR.BRANCH_TYPE IS NULL "); 
		
//Oat Edit 22/04/58
//		hql.append(" AND TR.TRADER_CATEGORY = '200' AND TR.ORG_ID = :TRADER_ORG_ID ");
//		parames.put("TRADER_ORG_ID", dto.getOrgId());
		 
		 hql.append(" AND TR.TRADER_CATEGORY = '200' AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS = 'N' ");
		 
		 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
		 parames.put("TRADER_ORG_ID", dto.getOrgId());
//End Oat Edit 22/04/58
		 
		//REVOKE
		hql.append(" AND RL.REVOKE_STATUS IN ('R','C') AND RL.RECORD_STATUS = 'N' ");
		//RevokeDateFrom
		 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
		 {
		 	hql.append(" AND RL.REVOKE_DATE BETWEEN :REVOKE_DATE_FORM ");
			    parames.put("REVOKE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
			    
			    //RevokeDateTo
			    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
			    {
			    	hql.append(" AND :REVOKE_DATE_TO ");
				    parames.put("REVOKE_DATE_TO", ft.parse(dto.getApproveDateTo()));
			    }
			    else
			    {
			    	hql.append(" AND :REVOKE_DATE_TO ");
				    parames.put("REVOKE_DATE_TO", new Date());
			    }
		 }
		//GUARANTEE
		hql.append(" AND G.GUARANTEE_STATUS = 'R' AND G.RECORD_STATUS = 'N') AS A2 ");

//COUNTRY   
		hql.append(" ,(SELECT COUNT(*) AS COUNTRY FROM REVOKE_LICENSE RL  ");
		hql.append(" INNER JOIN REVOKE_MAP_TRADER RM  ");
		hql.append(" ON (RL.REVOKE_ID = RM.REVOKE_ID) ");
		hql.append(" INNER JOIN TRADER TR ");
		hql.append(" ON(RM.TRADER_TYPE = TR.TRADER_TYPE AND RM.LICENSE_NO = TR.LICENSE_NO) ");
		hql.append(" INNER JOIN GUARANTEE G  ");
		hql.append(" ON(TR.TRADER_TYPE = G.TRADER_TYPE AND TR.LICENSE_NO = G.LICENSE_NO) ");
		
//Oat Edit 22/04/58
		 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
		 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Edit 22/04/58
		 
		hql.append(" WHERE  ");
		//TRADER
		hql.append(" TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS = 'N' AND TR.BRANCH_TYPE IS NULL "); 
		
//Oat Edit 22/04/58
//		hql.append(" AND TR.TRADER_CATEGORY = '300' AND TR.ORG_ID = :TRADER_ORG_ID ");
//		parames.put("TRADER_ORG_ID", dto.getOrgId());
		 
		 hql.append(" AND TR.TRADER_CATEGORY = '300' AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS = 'N' ");
		 
		 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
		 parames.put("TRADER_ORG_ID", dto.getOrgId());
//End Oat Edit 22/04/58
		 
		//REVOKE
		hql.append(" AND RL.REVOKE_STATUS IN ('R','C') AND RL.RECORD_STATUS = 'N' ");
		//RevokeDateFrom
		 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
		 {
		 	hql.append(" AND RL.REVOKE_DATE BETWEEN :REVOKE_DATE_FORM ");
			    parames.put("REVOKE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
			    
			    //RevokeDateTo
			    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
			    {
			    	hql.append(" AND :REVOKE_DATE_TO ");
				    parames.put("REVOKE_DATE_TO", ft.parse(dto.getApproveDateTo()));
			    }
			    else
			    {
			    	hql.append(" AND :REVOKE_DATE_TO ");
				    parames.put("REVOKE_DATE_TO", new Date());
			    }
		 }
		//GUARANTEE
		hql.append(" AND G.GUARANTEE_STATUS = 'R' AND G.RECORD_STATUS = 'N') AS A3 ");
//AREA
		hql.append(" ,(SELECT COUNT(*) AS AREA FROM REVOKE_LICENSE RL  ");
		hql.append(" INNER JOIN REVOKE_MAP_TRADER RM  ");
		hql.append(" ON (RL.REVOKE_ID = RM.REVOKE_ID) ");
		hql.append(" INNER JOIN TRADER TR ");
		hql.append(" ON(RM.TRADER_TYPE = TR.TRADER_TYPE AND RM.LICENSE_NO = TR.LICENSE_NO) ");
		hql.append(" INNER JOIN GUARANTEE G  ");
		hql.append(" ON(TR.TRADER_TYPE = G.TRADER_TYPE AND TR.LICENSE_NO = G.LICENSE_NO) ");
		
//Oat Edit 22/04/58
		 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
		 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Edit 22/04/58
		 
		hql.append(" WHERE  ");
		//TRADER
		hql.append(" TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS = 'N' AND TR.BRANCH_TYPE IS NULL "); 
		
//Oat Edit 22/04/58
//		hql.append(" AND TR.TRADER_CATEGORY = '400' AND TR.ORG_ID = :TRADER_ORG_ID ");
//		parames.put("TRADER_ORG_ID", dto.getOrgId());
		 
		 hql.append(" AND TR.TRADER_CATEGORY = '400' AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS = 'N' ");
		 
		 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
		 parames.put("TRADER_ORG_ID", dto.getOrgId());
//End Oat Edit 22/04/58
		 
		//REVOKE
		hql.append(" AND RL.REVOKE_STATUS IN ('R','C') AND RL.RECORD_STATUS = 'N' ");
		//RevokeDateFrom
		 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
		 {
		 	hql.append(" AND RL.REVOKE_DATE BETWEEN :REVOKE_DATE_FORM ");
			    parames.put("REVOKE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
			    
			    //RevokeDateTo
			    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
			    {
			    	hql.append(" AND :REVOKE_DATE_TO ");
				    parames.put("REVOKE_DATE_TO", ft.parse(dto.getApproveDateTo()));
			    }
			    else
			    {
			    	hql.append(" AND :REVOKE_DATE_TO ");
				    parames.put("REVOKE_DATE_TO", new Date());
			    }
		 }
		//GUARANTEE
		hql.append(" AND G.GUARANTEE_STATUS = 'R' AND G.RECORD_STATUS = 'N') AS A4 ");
		
	    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
	    sqlQuery.addScalar("OUTBOUND", Hibernate.LONG); 
	    sqlQuery.addScalar("INBOUND", Hibernate.LONG); 
	    sqlQuery.addScalar("COUNTRY", Hibernate.LONG); 
	    sqlQuery.addScalar("AREA", Hibernate.LONG); 
	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
  
	    return result;

	}
	
	
	//ยกเลิกและรับหลักประกันคืน
	public List<Object[]> findBusinessCancleGuarantee(RegistrationDTO dto)throws Exception
	{
		Map<String,Object> parames = new HashMap<String,Object>();
	    
	    StringBuilder hql = new StringBuilder();
	    
	    DateFormat ft =  DateUtils.getProcessDateFormatThai();

	    hql.append(" SELECT A1.OUTBOUND, A2.INBOUND, A3.COUNTRY ,A4.AREA FROM ");
//OUTBOUND
	    hql.append(" (SELECT COUNT(*) AS OUTBOUND FROM DEACTIVATE_LICENSE DL "); 
		hql.append(" INNER JOIN TRADER TR ");
		hql.append(" ON(DL.TRADER_TYPE = TR.TRADER_TYPE AND DL.LICENSE_NO = TR.LICENSE_NO) ");
		hql.append(" INNER JOIN GUARANTEE G  ");
		hql.append(" ON(TR.TRADER_TYPE = G.TRADER_TYPE AND TR.LICENSE_NO = G.LICENSE_NO) ");
		
//Oat Edit 22/04/58
		 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
		 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Edit 22/04/58
		
		hql.append(" WHERE  ");
		//TRADER
		hql.append(" TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS = 'N' AND TR.BRANCH_TYPE IS NULL  ");
		 
//Oat Edit 22/04/58
//		 hql.append(" AND TR.TRADER_CATEGORY = '100' AND TR.ORG_ID = :TRADER_ORG_ID ");
//		 parames.put("TRADER_ORG_ID", dto.getOrgId());
		 
		 hql.append(" AND TR.TRADER_CATEGORY = '100' AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS = 'N' ");
		 
		 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
		 parames.put("TRADER_ORG_ID", dto.getOrgId());
//End Oat Edit 22/04/58
		 
		//DEACTIVATE
		hql.append(" AND DL.APPROVE_STATUS IN ('A','C') AND DL.RECORD_STATUS = 'N'  ");
//		hql.append(" AND DL.BOOK_DATE BETWEEN CONVERT(DATETIME, '01/10/2013', 103)  ");
//		hql.append(" AND CONVERT(DATETIME, '30/12/2014', 103) ");
		//BookDateFrom
		 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
		 {
		 	hql.append(" AND DL.BOOK_DATE BETWEEN :BOOK_DATE_FORM ");
			    parames.put("BOOK_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
			    //BookDateTo
			    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
			    {
			    	hql.append(" AND :BOOK_DATE_TO ");
				    parames.put("BOOK_DATE_TO", ft.parse(dto.getApproveDateTo()));
			    }
			    else
			    {
			    	hql.append(" AND :BOOK_DATE_TO ");
				    parames.put("BOOK_DATE_TO", new Date());
			    }
		 }
		//GUARANTEE
		hql.append(" AND G.GUARANTEE_STATUS = 'R' AND G.RECORD_STATUS = 'N') AS A1 ");

//INBOUND
	    hql.append(" ,(SELECT COUNT(*) AS INBOUND FROM DEACTIVATE_LICENSE DL "); 
		hql.append(" INNER JOIN TRADER TR ");
		hql.append(" ON(DL.TRADER_TYPE = TR.TRADER_TYPE AND DL.LICENSE_NO = TR.LICENSE_NO) ");
		hql.append(" INNER JOIN GUARANTEE G  ");
		hql.append(" ON(TR.TRADER_TYPE = G.TRADER_TYPE AND TR.LICENSE_NO = G.LICENSE_NO) ");
		
//Oat Edit 22/04/58
		 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
		 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Edit 22/04/58
		 
		hql.append(" WHERE  ");
		//TRADER
		hql.append(" TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS = 'N' AND TR.BRANCH_TYPE IS NULL  ");
		 
//Oat Edit 22/04/58
//		hql.append(" AND TR.TRADER_CATEGORY = '200' AND TR.ORG_ID = :TRADER_ORG_ID ");
//		 parames.put("TRADER_ORG_ID", dto.getOrgId());
		 
		 hql.append(" AND TR.TRADER_CATEGORY = '200' AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS = 'N' ");
		 
		 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
		 parames.put("TRADER_ORG_ID", dto.getOrgId());
//End Oat Edit 22/04/58
		 
		//DEACTIVATE
		hql.append(" AND DL.APPROVE_STATUS IN ('A','C') AND DL.RECORD_STATUS = 'N'  ");
		//BookDateFrom
		 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
		 {
		 	hql.append(" AND DL.BOOK_DATE BETWEEN :BOOK_DATE_FORM ");
			    parames.put("BOOK_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
			    //BookDateTo
			    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
			    {
			    	hql.append(" AND :BOOK_DATE_TO ");
				    parames.put("BOOK_DATE_TO", ft.parse(dto.getApproveDateTo()));
			    }
			    else
			    {
			    	hql.append(" AND :BOOK_DATE_TO ");
				    parames.put("BOOK_DATE_TO", new Date());
			    }
		 }
		//GUARANTEE
		hql.append(" AND G.GUARANTEE_STATUS = 'R' AND G.RECORD_STATUS = 'N') AS A2 ");
		
//COUNTRY
	    hql.append(" ,(SELECT COUNT(*) AS COUNTRY FROM DEACTIVATE_LICENSE DL "); 
		hql.append(" INNER JOIN TRADER TR ");
		hql.append(" ON(DL.TRADER_TYPE = TR.TRADER_TYPE AND DL.LICENSE_NO = TR.LICENSE_NO) ");
		hql.append(" INNER JOIN GUARANTEE G  ");
		hql.append(" ON(TR.TRADER_TYPE = G.TRADER_TYPE AND TR.LICENSE_NO = G.LICENSE_NO) ");
		
//Oat Edit 22/04/58
		 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
		 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Edit 22/04/58
		 
		hql.append(" WHERE  ");
		//TRADER
		hql.append(" TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS = 'N' AND TR.BRANCH_TYPE IS NULL  ");

//Oat Edit 22/04/58
//		hql.append(" AND TR.TRADER_CATEGORY = '300' AND TR.ORG_ID = :TRADER_ORG_ID ");
//		 parames.put("TRADER_ORG_ID", dto.getOrgId());
		 
		 hql.append(" AND TR.TRADER_CATEGORY = '300' AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS = 'N' ");
		 
		 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
		 parames.put("TRADER_ORG_ID", dto.getOrgId());
//End Oat Edit 22/04/58
		 
		//DEACTIVATE
		hql.append(" AND DL.APPROVE_STATUS IN ('A','C') AND DL.RECORD_STATUS = 'N'  ");
		//BookDateFrom
		 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
		 {
		 	hql.append(" AND DL.BOOK_DATE BETWEEN :BOOK_DATE_FORM ");
			    parames.put("BOOK_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
			    
			    //BookDateTo
			    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
			    {
			    	hql.append(" AND :BOOK_DATE_TO ");
				    parames.put("BOOK_DATE_TO", ft.parse(dto.getApproveDateTo()));
			    }
			    else
			    {
			    	hql.append(" AND :BOOK_DATE_TO ");
				    parames.put("BOOK_DATE_TO", new Date());
			    }
		 }
		//GUARANTEE
		hql.append(" AND G.GUARANTEE_STATUS = 'R' AND G.RECORD_STATUS = 'N') AS A3 ");

//AREA
	    hql.append(" ,(SELECT COUNT(*) AS AREA FROM DEACTIVATE_LICENSE DL "); 
		hql.append(" INNER JOIN TRADER TR ");
		hql.append(" ON(DL.TRADER_TYPE = TR.TRADER_TYPE AND DL.LICENSE_NO = TR.LICENSE_NO) ");
		hql.append(" INNER JOIN GUARANTEE G  ");
		hql.append(" ON(TR.TRADER_TYPE = G.TRADER_TYPE AND TR.LICENSE_NO = G.LICENSE_NO) ");
		
//Oat Edit 22/04/58
		 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
		 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Edit 22/04/58
		 
		hql.append(" WHERE  ");
		//TRADER
		hql.append(" TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS = 'N' AND TR.BRANCH_TYPE IS NULL  ");
		 
//Oat Edit 22/04/58
//		hql.append(" AND TR.TRADER_CATEGORY = '400' AND TR.ORG_ID = :TRADER_ORG_ID ");
//		 parames.put("TRADER_ORG_ID", dto.getOrgId());
		 
		 hql.append(" AND TR.TRADER_CATEGORY = '400' AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS = 'N' ");
		 
		 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
		 parames.put("TRADER_ORG_ID", dto.getOrgId());
//End Oat Edit 22/04/58
			 
		//DEACTIVATE
		hql.append(" AND DL.APPROVE_STATUS IN ('A','C') AND DL.RECORD_STATUS = 'N'  ");
		//BookDateFrom
		 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
		 {
		 	hql.append(" AND DL.BOOK_DATE BETWEEN :BOOK_DATE_FORM ");
			    parames.put("BOOK_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
			    //BookDateTo
			    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
			    {
			    	hql.append(" AND :BOOK_DATE_TO ");
				    parames.put("BOOK_DATE_TO", ft.parse(dto.getApproveDateTo()));
			    }
			    else
			    {
			    	hql.append(" AND :BOOK_DATE_TO ");
				    parames.put("BOOK_DATE_TO", new Date());
			    }
		 }
		//GUARANTEE
		hql.append(" AND G.GUARANTEE_STATUS = 'R' AND G.RECORD_STATUS = 'N') AS A4 ");	    

	    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
	    sqlQuery.addScalar("OUTBOUND", Hibernate.LONG); 
	    sqlQuery.addScalar("INBOUND", Hibernate.LONG); 
	    sqlQuery.addScalar("COUNTRY", Hibernate.LONG); 
	    sqlQuery.addScalar("AREA", Hibernate.LONG); 
	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
  
	    return result;
	}

	//หายอดเดิม + จดใหม่
	public List<Object[]> findBusinessOldNew(RegistrationDTO dto)throws Exception
	{
		Map<String,Object> parames = new HashMap<String,Object>();
	    
	    StringBuilder hql = new StringBuilder();
	    
	    DateFormat ft =  DateUtils.getProcessDateFormatThai();

	    hql.append(" SELECT A.OLD, B.NEW FROM ");
	    //ยอดเดิม
	     hql.append(" (SELECT COUNT(*) AS OLD ");
		 hql.append(" FROM TRADER TR 	INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID) ");    
		 
//Oat Edit 21/04/58
		 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
		 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Edit 21/04/58
		 
		 hql.append(" WHERE TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS IN ('N')  ");
		 hql.append(" AND TR.LICENSE_STATUS = 'N' AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS = 'N'  ");
		 hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL  ");
//		 hql.append(" AND RE.APPROVE_DATE < CONVERT(DATETIME, '25/11/2013', 103) ");
		//ApproveDateFrom
		 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
		 {
//		 	hql.append(" AND RE.APPROVE_DATE < :APPROVE_DATE_FORM ");
//			    parames.put("APPROVE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
//			    
			    hql.append("  AND ( RE.APPROVE_DATE <  convert(datetime, :APPROVE_START_DATE ,20) ) ");
//				hql.append(" AND RE.APPROVE_DATE <=  convert(datetime, :APPROVE_END_DATE ,20) ) ");
				
				String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
//				String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
					
				parames.put("APPROVE_START_DATE", sendDtmFrom);
//				parames.put("APPROVE_END_DATE", sendDtmTo);
		 }
		 
//Oat Edit 21/04/58
//		 hql.append(" AND TR.ORG_ID = :TRADER_ORG_ID ");
//		 parames.put("TRADER_ORG_ID", dto.getOrgId());
		 
		 hql.append(" AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS = 'N' ");
		 
		 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
		 parames.put("TRADER_ORG_ID", dto.getOrgId());
//End Oat Edit 21/04/58
		 
		 hql.append(" ) AS A ");
		 
		 //รายใหม่
		 hql.append(" , ");
		 hql.append(" (SELECT COUNT(*) AS NEW ");
		 hql.append(" FROM TRADER TR 	INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID)");
		 
//Oat Edit 21/04/58
		 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
		 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Edit 21/04/58
		 
		 hql.append(" WHERE TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS IN ('N','H') ");
		 hql.append(" AND RE.REGISTRATION_TYPE = 'N' AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS = 'N' ");
		 hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL ");
//		 hql.append(" AND RE.APPROVE_DATE BETWEEN CONVERT(DATETIME, '25/11/2013', 103) AND CONVERT(DATETIME, '30/12/2013', 103) "); 
		//ApproveDateFrom
		 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
		 {
//		 	hql.append(" AND RE.APPROVE_DATE BETWEEN :APPROVE_DATE_FORM ");
//			    parames.put("APPROVE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
//			    
//			    //ApproveDateTo
//			    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
//			    {
//			    	hql.append(" AND :APPROVE_DATE_TO ");
//				    parames.put("APPROVE_DATE_TO", ft.parse(dto.getApproveDateTo()));
//			    }
//			    else
//			    {
//			    	hql.append(" AND :APPROVE_DATE_TO ");
//				    parames.put("APPROVE_DATE_TO", new Date());
//			    }
			 
			    hql.append("  AND ( RE.APPROVE_DATE >=  convert(datetime, :APPROVE_START_DATE ,20) ");
				hql.append(" AND RE.APPROVE_DATE <=  convert(datetime, :APPROVE_END_DATE ,20) ) ");
				
				String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
				String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
					
				parames.put("APPROVE_START_DATE", sendDtmFrom);
				parames.put("APPROVE_END_DATE", sendDtmTo);
		 }
		 
//		 hql.append(" AND TR.ORG_ID = :TRADER_ORG_ID ");
//		 parames.put("TRADER_ORG_ID", dto.getOrgId());
		 
//Oat Edit 21/04/58
//		 hql.append(" AND TR.ORG_ID = :TRADER_ORG_ID ");
//		 parames.put("TRADER_ORG_ID", dto.getOrgId());
		 
		 hql.append(" AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS = 'N' ");
		 
		 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
		 parames.put("TRADER_ORG_ID", dto.getOrgId());
//End Oat Edit 21/04/58
		 
		 hql.append(" ) AS B ");
		 
	    
	    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());

	    sqlQuery.addScalar("OLD", Hibernate.LONG); 
	    sqlQuery.addScalar("NEW", Hibernate.LONG); 
	    
	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
  
	    return result;
	}
//จบค้นหาตารางแสดง ยกเลิกและรับหลักประกันคืนและขอรับหลักประกันคืน(กรณีถูกเพิกถอน)*****

//ตารางแสดงจำนวนพักใช้และถูกเพิกถอนใบอนุญาตประกอบธุรกิจนำเที่ยว (ค้นหาทีละสำนักงานตาม ordId)
	public List<Object[]> findExcelBusinessSuspensionAndRevoke(RegistrationDTO dto)throws Exception
	{
	    Map<String,Object> parames = new HashMap<String,Object>();
	    
	    StringBuilder hql = new StringBuilder();
	    
	    DateFormat ft =  DateUtils.getProcessDateFormatThai();
	    
	    hql.append(" SELECT A1.OUTBOUND , A2.INBOUND, A3.COUNTRY ,A4.AREA  ,A5.OUTBOUNDR , A6.INBOUNDR, A7.COUNTRYR ,A8.AREAR FROM ");                 
	
	 //พักใช้
//OUTBOUND
	    hql.append(" (SELECT COUNT(*) AS OUTBOUND FROM SUSPENSION_LICENSE SL   ");
	    hql.append(" INNER JOIN SUSPENSION_MAP_TRADER SM   ");
	    hql.append(" ON (SL.SUSPEND_ID = SM.SUSPEND_ID)  ");
	    hql.append(" INNER JOIN TRADER TR  ");
	    hql.append(" ON (SM.TRADER_TYPE = TR.TRADER_TYPE AND SM.LICENSE_NO = TR.LICENSE_NO)  ");
	    
//Oat Edit 24/04/58
		 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
		 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Edit 24/04/58
	    
	    hql.append(" WHERE   ");
	    //TRADER
	    hql.append(" TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS = 'N' AND TR.BRANCH_TYPE IS NULL  ");
	    
//Oat Edit 22/04/58
//	    hql.append(" AND TR.TRADER_CATEGORY = '100' AND TR.ORG_ID = :TRADER_ORG_ID  ");
//	    parames.put("TRADER_ORG_ID", dto.getOrgId());
		 
		 hql.append(" AND TR.TRADER_CATEGORY = '100' AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS = 'N' ");
		 
		 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
		 parames.put("TRADER_ORG_ID", dto.getOrgId());
//End Oat Edit 22/04/58
		 
	    //SUSPENSION
	    hql.append(" AND SL.SUSPEND_STATUS IN ('S','C') AND SL.RECORD_STATUS = 'N'  ");
//	    hql.append(" AND SL.SUSPEND_FROM BETWEEN CONVERT(DATETIME, '01/10/2013', 103)   ");
//	    hql.append(" AND CONVERT(DATETIME, '30/12/2014', 103) ) AS A1  ");
	  //SuspendDateFrom
		 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
		 {
		 	hql.append(" AND SL.SUSPEND_FROM BETWEEN :SUSPEND_DATE_FORM ");
			    parames.put("SUSPEND_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
			    
			    //SuspendDateTo
			    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
			    {
			    	hql.append(" AND :SUSPEND_DATE_TO ");
				    parames.put("SUSPEND_DATE_TO", ft.parse(dto.getApproveDateTo()));
			    }
			    else
			    {
			    	hql.append(" AND :SUSPEND_DATE_TO ");
				    parames.put("SUSPEND_DATE_TO", new Date());
			    }
		 }
		hql.append(" ) AS A1 ");
//INBOUND
		hql.append(" ,(SELECT COUNT(*) AS INBOUND FROM SUSPENSION_LICENSE SL   ");
	    hql.append(" INNER JOIN SUSPENSION_MAP_TRADER SM   ");
	    hql.append(" ON (SL.SUSPEND_ID = SM.SUSPEND_ID)  ");
	    hql.append(" INNER JOIN TRADER TR  ");
	    hql.append(" ON (SM.TRADER_TYPE = TR.TRADER_TYPE AND SM.LICENSE_NO = TR.LICENSE_NO)  ");
	    
//Oat Edit 24/04/58
		 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
		 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Edit 24/04/58
		 
	    hql.append(" WHERE   ");
	    //TRADER
	    hql.append(" TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS = 'N' AND TR.BRANCH_TYPE IS NULL  ");
	    
//Oat Edit 22/04/58
//	    hql.append(" AND TR.TRADER_CATEGORY = '200' AND TR.ORG_ID = :TRADER_ORG_ID  ");
//	    parames.put("TRADER_ORG_ID", dto.getOrgId());
		 
		 hql.append(" AND TR.TRADER_CATEGORY = '200' AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS = 'N' ");
		 
		 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
		 parames.put("TRADER_ORG_ID", dto.getOrgId());
//End Oat Edit 22/04/58
		 
	    //SUSPENSION
	    hql.append(" AND SL.SUSPEND_STATUS IN ('S','C') AND SL.RECORD_STATUS = 'N'  ");
	    //SuspendDateFrom
		 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
		 {
		 	hql.append(" AND SL.SUSPEND_FROM BETWEEN :SUSPEND_DATE_FORM ");
			    parames.put("SUSPEND_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
			    
			    //SuspendDateTo
			    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
			    {
			    	hql.append(" AND :SUSPEND_DATE_TO ");
				    parames.put("SUSPEND_DATE_TO", ft.parse(dto.getApproveDateTo()));
			    }
			    else
			    {
			    	hql.append(" AND :SUSPEND_DATE_TO ");
				    parames.put("SUSPEND_DATE_TO", new Date());
			    }
		 }
		hql.append(" ) AS A2 ");
//COUNTRY
		hql.append(" ,(SELECT COUNT(*) AS COUNTRY FROM SUSPENSION_LICENSE SL   ");
	    hql.append(" INNER JOIN SUSPENSION_MAP_TRADER SM   ");
	    hql.append(" ON (SL.SUSPEND_ID = SM.SUSPEND_ID)  ");
	    hql.append(" INNER JOIN TRADER TR  ");
	    hql.append(" ON (SM.TRADER_TYPE = TR.TRADER_TYPE AND SM.LICENSE_NO = TR.LICENSE_NO)  ");
	    
//Oat Edit 24/04/58
		 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
		 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Edit 24/04/58
		 
	    hql.append(" WHERE   ");
	    //TRADER
	    hql.append(" TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS = 'N' AND TR.BRANCH_TYPE IS NULL  ");
	    
//Oat Edit 22/04/58
//	    hql.append(" AND TR.TRADER_CATEGORY = '300' AND TR.ORG_ID = :TRADER_ORG_ID  ");
//	    parames.put("TRADER_ORG_ID", dto.getOrgId());
		 
		 hql.append(" AND TR.TRADER_CATEGORY = '300' AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS = 'N' ");
		 
		 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
		 parames.put("TRADER_ORG_ID", dto.getOrgId());
//End Oat Edit 22/04/58
		 
	    //SUSPENSION
	    hql.append(" AND SL.SUSPEND_STATUS IN ('S','C') AND SL.RECORD_STATUS = 'N'  ");
	    //SuspendDateFrom
		 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
		 {
		 	hql.append(" AND SL.SUSPEND_FROM BETWEEN :SUSPEND_DATE_FORM ");
			    parames.put("SUSPEND_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
			    
			    //SuspendDateTo
			    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
			    {
			    	hql.append(" AND :SUSPEND_DATE_TO ");
				    parames.put("SUSPEND_DATE_TO", ft.parse(dto.getApproveDateTo()));
			    }
			    else
			    {
			    	hql.append(" AND :SUSPEND_DATE_TO ");
				    parames.put("SUSPEND_DATE_TO", new Date());
			    }
		 }
		hql.append(" ) AS A3 ");
//AREA
		hql.append(" ,(SELECT COUNT(*) AS AREA FROM SUSPENSION_LICENSE SL   ");
	    hql.append(" INNER JOIN SUSPENSION_MAP_TRADER SM   ");
	    hql.append(" ON (SL.SUSPEND_ID = SM.SUSPEND_ID)  ");
	    hql.append(" INNER JOIN TRADER TR  ");
	    hql.append(" ON (SM.TRADER_TYPE = TR.TRADER_TYPE AND SM.LICENSE_NO = TR.LICENSE_NO)  ");
	    
//Oat Edit 24/04/58
		 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
		 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Edit 24/04/58
		 
	    hql.append(" WHERE   ");
	    //TRADER
	    hql.append(" TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS = 'N' AND TR.BRANCH_TYPE IS NULL  ");
	    
//Oat Edit 22/04/58
//	    hql.append(" AND TR.TRADER_CATEGORY = '400' AND TR.ORG_ID = :TRADER_ORG_ID  ");
//	    parames.put("TRADER_ORG_ID", dto.getOrgId());
		 
		 hql.append(" AND TR.TRADER_CATEGORY = '400' AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS = 'N' ");
		 
		 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
		 parames.put("TRADER_ORG_ID", dto.getOrgId());
//End Oat Edit 22/04/58
		 
	    //SUSPENSION
	    hql.append(" AND SL.SUSPEND_STATUS IN ('S','C') AND SL.RECORD_STATUS = 'N'  ");
	    //SuspendDateFrom
		 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
		 {
		 	hql.append(" AND SL.SUSPEND_FROM BETWEEN :SUSPEND_DATE_FORM ");
			    parames.put("SUSPEND_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
			    
			    //SuspendDateTo
			    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
			    {
			    	hql.append(" AND :SUSPEND_DATE_TO ");
				    parames.put("SUSPEND_DATE_TO", ft.parse(dto.getApproveDateTo()));
			    }
			    else
			    {
			    	hql.append(" AND :SUSPEND_DATE_TO ");
				    parames.put("SUSPEND_DATE_TO", new Date());
			    }
		 }
		hql.append(" ) AS A4 ");
    
    //ถูกเพิกถอน----------------------------------------------------------
//OUTBOUNDR
	    hql.append(", (SELECT COUNT(*) AS OUTBOUNDR FROM REVOKE_LICENSE RL  ");
		hql.append(" INNER JOIN REVOKE_MAP_TRADER RM  ");
		hql.append(" ON (RL.REVOKE_ID = RM.REVOKE_ID) ");
		hql.append(" INNER JOIN TRADER TR ");
		hql.append(" ON(RM.TRADER_TYPE = TR.TRADER_TYPE AND RM.LICENSE_NO = TR.LICENSE_NO) ");
		
//Oat Edit 24/04/58
		 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
		 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Edit 24/04/58
		 
		hql.append(" WHERE  ");
		//TRADER
		hql.append(" TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS = 'N' AND TR.BRANCH_TYPE IS NULL "); 
		
//Oat Edit 22/04/58
//		hql.append(" AND TR.TRADER_CATEGORY = '100' AND TR.ORG_ID = :TRADER_ORG_ID ");
//		parames.put("TRADER_ORG_ID", dto.getOrgId());
		 
		 hql.append(" AND TR.TRADER_CATEGORY = '100' AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS = 'N' ");
		 
		 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
		 parames.put("TRADER_ORG_ID", dto.getOrgId());
//End Oat Edit 22/04/58
		 
		//REVOKE
		hql.append(" AND RL.REVOKE_STATUS IN ('R','C') AND RL.RECORD_STATUS = 'N' ");
		//RevokeDateFrom
		 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
		 {
		 	hql.append(" AND RL.REVOKE_DATE BETWEEN :REVOKE_DATE_FORM ");
			    parames.put("REVOKE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
			    
			    //RevokeDateTo
			    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
			    {
			    	hql.append(" AND :REVOKE_DATE_TO ");
				    parames.put("REVOKE_DATE_TO", ft.parse(dto.getApproveDateTo()));
			    }
			    else
			    {
			    	hql.append(" AND :REVOKE_DATE_TO ");
				    parames.put("REVOKE_DATE_TO", new Date());
			    }
		 }
		hql.append(" ) AS A5 ");

//INBOUNDR
	    hql.append(", (SELECT COUNT(*) AS INBOUNDR FROM REVOKE_LICENSE RL  ");
		hql.append(" INNER JOIN REVOKE_MAP_TRADER RM  ");
		hql.append(" ON (RL.REVOKE_ID = RM.REVOKE_ID) ");
		hql.append(" INNER JOIN TRADER TR ");
		hql.append(" ON(RM.TRADER_TYPE = TR.TRADER_TYPE AND RM.LICENSE_NO = TR.LICENSE_NO) ");
		
//Oat Edit 24/04/58
		 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
		 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Edit 24/04/58
		 
		hql.append(" WHERE  ");
		//TRADER
		hql.append(" TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS = 'N' AND TR.BRANCH_TYPE IS NULL "); 
		
//Oat Edit 22/04/58
//		hql.append(" AND TR.TRADER_CATEGORY = '200' AND TR.ORG_ID = :TRADER_ORG_ID ");
//		parames.put("TRADER_ORG_ID", dto.getOrgId());
		 
		 hql.append(" AND TR.TRADER_CATEGORY = '200' AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS = 'N' ");
		 
		 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
		 parames.put("TRADER_ORG_ID", dto.getOrgId());
//End Oat Edit 22/04/58
		 
		//REVOKE
		hql.append(" AND RL.REVOKE_STATUS IN ('R','C') AND RL.RECORD_STATUS = 'N' ");
		//RevokeDateFrom
		 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
		 {
		 	hql.append(" AND RL.REVOKE_DATE BETWEEN :REVOKE_DATE_FORM ");
			    parames.put("REVOKE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
			    
			    //RevokeDateTo
			    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
			    {
			    	hql.append(" AND :REVOKE_DATE_TO ");
				    parames.put("REVOKE_DATE_TO", ft.parse(dto.getApproveDateTo()));
			    }
			    else
			    {
			    	hql.append(" AND :REVOKE_DATE_TO ");
				    parames.put("REVOKE_DATE_TO", new Date());
			    }
		 }
		hql.append(" ) AS A6 ");
//COUNTRYR
		hql.append(", (SELECT COUNT(*) AS COUNTRYR FROM REVOKE_LICENSE RL  ");
		hql.append(" INNER JOIN REVOKE_MAP_TRADER RM  ");
		hql.append(" ON (RL.REVOKE_ID = RM.REVOKE_ID) ");
		hql.append(" INNER JOIN TRADER TR ");
		hql.append(" ON(RM.TRADER_TYPE = TR.TRADER_TYPE AND RM.LICENSE_NO = TR.LICENSE_NO) ");
		
//Oat Edit 24/04/58
		 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
		 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Edit 24/04/58
		 
		hql.append(" WHERE  ");
		//TRADER
		hql.append(" TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS = 'N' AND TR.BRANCH_TYPE IS NULL "); 
		
//Oat Edit 22/04/58
//		hql.append(" AND TR.TRADER_CATEGORY = '300' AND TR.ORG_ID = :TRADER_ORG_ID ");
//		parames.put("TRADER_ORG_ID", dto.getOrgId());
		 
		 hql.append(" AND TR.TRADER_CATEGORY = '300' AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS = 'N' ");
		 
		 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
		 parames.put("TRADER_ORG_ID", dto.getOrgId());
//End Oat Edit 22/04/58
		 
		//REVOKE
		hql.append(" AND RL.REVOKE_STATUS IN ('R','C') AND RL.RECORD_STATUS = 'N' ");
		//RevokeDateFrom
		 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
		 {
		 	hql.append(" AND RL.REVOKE_DATE BETWEEN :REVOKE_DATE_FORM ");
			    parames.put("REVOKE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
			    
			    //RevokeDateTo
			    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
			    {
			    	hql.append(" AND :REVOKE_DATE_TO ");
				    parames.put("REVOKE_DATE_TO", ft.parse(dto.getApproveDateTo()));
			    }
			    else
			    {
			    	hql.append(" AND :REVOKE_DATE_TO ");
				    parames.put("REVOKE_DATE_TO", new Date());
			    }
		 }
		hql.append(" ) AS A7 ");
//AREAR
		hql.append(", (SELECT COUNT(*) AS AREAR FROM REVOKE_LICENSE RL  ");
		hql.append(" INNER JOIN REVOKE_MAP_TRADER RM  ");
		hql.append(" ON (RL.REVOKE_ID = RM.REVOKE_ID) ");
		hql.append(" INNER JOIN TRADER TR ");
		hql.append(" ON(RM.TRADER_TYPE = TR.TRADER_TYPE AND RM.LICENSE_NO = TR.LICENSE_NO) ");
		
//Oat Edit 24/04/58
		 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
		 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Edit 24/04/58
		 
		hql.append(" WHERE  ");
		//TRADER
		hql.append(" TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS = 'N' AND TR.BRANCH_TYPE IS NULL "); 
		
//Oat Edit 22/04/58
//		hql.append(" AND TR.TRADER_CATEGORY = '400' AND TR.ORG_ID = :TRADER_ORG_ID ");
//		parames.put("TRADER_ORG_ID", dto.getOrgId());
		 
		 hql.append(" AND TR.TRADER_CATEGORY = '400' AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS = 'N' ");
		 
		 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
		 parames.put("TRADER_ORG_ID", dto.getOrgId());
//End Oat Edit 22/04/58
		 
		//REVOKE
		hql.append(" AND RL.REVOKE_STATUS IN ('R','C') AND RL.RECORD_STATUS = 'N' ");
		//RevokeDateFrom
		 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
		 {
		 	hql.append(" AND RL.REVOKE_DATE BETWEEN :REVOKE_DATE_FORM ");
			    parames.put("REVOKE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
			    
			    //RevokeDateTo
			    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
			    {
			    	hql.append(" AND :REVOKE_DATE_TO ");
				    parames.put("REVOKE_DATE_TO", ft.parse(dto.getApproveDateTo()));
			    }
			    else
			    {
			    	hql.append(" AND :REVOKE_DATE_TO ");
				    parames.put("REVOKE_DATE_TO", new Date());
			    }
		 }
		hql.append(" ) AS A8 "); 
	    
	    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());

	    sqlQuery.addScalar("OUTBOUND", Hibernate.LONG); 
	    sqlQuery.addScalar("INBOUND", Hibernate.LONG); 
	    sqlQuery.addScalar("COUNTRY", Hibernate.LONG); 
	    sqlQuery.addScalar("AREA", Hibernate.LONG); 
	    
	    sqlQuery.addScalar("OUTBOUNDR", Hibernate.LONG); 
	    sqlQuery.addScalar("INBOUNDR", Hibernate.LONG); 
	    sqlQuery.addScalar("COUNTRYR", Hibernate.LONG); 
	    sqlQuery.addScalar("AREAR", Hibernate.LONG); 
	    
	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
  
	    return result;
	}
	
	//ตารางแสดงจำนวนผู้มายื่นขอชำระค่าธรรมเนียมประกอบธุรกิจนำเที่ยว ราย 2 ปี (ค้นหาทีละสำนักงานตาม ordId)
	public List<Object[]> findExcelBusinessRenew(RegistrationDTO dto)throws Exception
		{
			   Map<String,Object> parames = new HashMap<String,Object>();
			    
			    StringBuilder hql = new StringBuilder();
			    
			    DateFormat ft =  DateUtils.getProcessDateFormatThai();
			    
				 hql.append(" SELECT A1.OUTBOUND , A2.INBOUND, A3.COUNTRY ,A4.AREA  FROM ");
				
				 hql.append(" (SELECT COUNT(*)  AS OUTBOUND ");
				 hql.append(" FROM TRADER TR 	INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID) ");
				 
//Oat Edit 21/04/58
				 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
				 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Edit 21/04/58
				 
				 hql.append(" WHERE TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS IN ('N','H')  ");
				 hql.append(" AND RE.REGISTRATION_TYPE = 'R' AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS = 'N' ");
				
				 // hql.append(" AND RE.APPROVE_DATE BETWEEN CONVERT(DATETIME, '01/10/2013', 103) AND CONVERT(DATETIME, '30/12/2013', 103) ");
				 //ApproveDateFrom
				 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
				 {
//				 	hql.append(" AND RE.APPROVE_DATE BETWEEN :APPROVE_DATE_FORM ");
//					    parames.put("APPROVE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
//					    
//					    //ApproveDateTo
//					    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
//					    {
//					    	hql.append(" AND :APPROVE_DATE_TO ");
//						    parames.put("APPROVE_DATE_TO", ft.parse(dto.getApproveDateTo()));
//					    }
//					    else
//					    {
//					    	hql.append(" AND :APPROVE_DATE_TO ");
//						    parames.put("APPROVE_DATE_TO", new Date());
//					    }
					 
					    hql.append("  AND ( RE.APPROVE_DATE >=  convert(datetime, :APPROVE_START_DATE ,20) ");
						hql.append(" AND RE.APPROVE_DATE <=  convert(datetime, :APPROVE_END_DATE ,20) ) ");
						
						String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
						String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
							
						parames.put("APPROVE_START_DATE", sendDtmFrom);
						parames.put("APPROVE_END_DATE", sendDtmTo);
				 }
				 
				 hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL  ");
				 hql.append(" AND TR.TRADER_CATEGORY = '100' ");
				 
//Oat Edit 21/04/58
//				 hql.append(" AND TR.ORG_ID = :TRADER_ORG_ID ");
//				 parames.put("TRADER_ORG_ID", dto.getOrgId());
				 
				 hql.append(" AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS = 'N' ");
				 
				 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
				 parames.put("TRADER_ORG_ID", dto.getOrgId());
//End Oat Edit 21/04/58
				 
				 hql.append(" ) AS A1 ");
				 hql.append("  , ");
				 hql.append(" (SELECT COUNT(*) AS INBOUND ");
				 hql.append(" FROM TRADER TR 	INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID) ");
				 
//Oat Edit 21/04/58
				 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
				 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Edit 21/04/58
				 
				 hql.append(" WHERE TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS IN ('N','H')  ");
				 hql.append(" AND RE.REGISTRATION_TYPE = 'R' AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS = 'N'  ");
				 
				// hql.append(" AND RE.APPROVE_DATE BETWEEN CONVERT(DATETIME, '01/10/2013', 103) AND CONVERT(DATETIME, '30/12/2013', 103) ");
				//ApproveDateFrom
				 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
				 {
//				 	hql.append(" AND RE.APPROVE_DATE BETWEEN :APPROVE_DATE_FORM ");
//					    parames.put("APPROVE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
//					    
//					    //ApproveDateTo
//					    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
//					    {
//					    	hql.append(" AND :APPROVE_DATE_TO ");
//						    parames.put("APPROVE_DATE_TO", ft.parse(dto.getApproveDateTo()));
//					    }
//					    else
//					    {
//					    	hql.append(" AND :APPROVE_DATE_TO ");
//						    parames.put("APPROVE_DATE_TO", new Date());
//					    }
					 
					    hql.append("  AND ( RE.APPROVE_DATE >=  convert(datetime, :APPROVE_START_DATE ,20) ");
						hql.append(" AND RE.APPROVE_DATE <=  convert(datetime, :APPROVE_END_DATE ,20) ) ");
						
						String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
						String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
							
						parames.put("APPROVE_START_DATE", sendDtmFrom);
						parames.put("APPROVE_END_DATE", sendDtmTo);
				 }
				 
				 hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL  ");
				 hql.append(" AND TR.TRADER_CATEGORY = '200' ");
				 
//Oat Edit 21/04/58
//				 hql.append(" AND TR.ORG_ID = :TRADER_ORG_ID ");
//				 parames.put("TRADER_ORG_ID", dto.getOrgId());
				 
				 hql.append(" AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS = 'N' ");
				 
				 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
				 parames.put("TRADER_ORG_ID", dto.getOrgId());
//End Oat Edit 21/04/58
				 
				 hql.append(" ) AS A2 ");
				
				 hql.append(" , ");
				 hql.append("  (SELECT COUNT(*) AS COUNTRY ");
				 hql.append("  FROM TRADER TR 	INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID) ");
				 
//Oat Edit 21/04/58
				 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
				 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Edit 21/04/58
				 
				 hql.append(" WHERE TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS IN ('N','H')  ");
				 hql.append(" AND RE.REGISTRATION_TYPE = 'R' AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS = 'N'  ");
				 
				// hql.append(" AND RE.APPROVE_DATE BETWEEN CONVERT(DATETIME, '01/10/2013', 103) AND CONVERT(DATETIME, '30/12/2013', 103) ");
				//ApproveDateFrom
				 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
				 {
//				 	hql.append(" AND RE.APPROVE_DATE BETWEEN :APPROVE_DATE_FORM ");
//					    parames.put("APPROVE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
//					    
//					    //ApproveDateTo
//					    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
//					    {
//					    	hql.append(" AND :APPROVE_DATE_TO ");
//						    parames.put("APPROVE_DATE_TO", ft.parse(dto.getApproveDateTo()));
//					    }
//					    else
//					    {
//					    	hql.append(" AND :APPROVE_DATE_TO ");
//						    parames.put("APPROVE_DATE_TO", new Date());
//					    }
					 
					    hql.append("  AND ( RE.APPROVE_DATE >=  convert(datetime, :APPROVE_START_DATE ,20) ");
						hql.append(" AND RE.APPROVE_DATE <=  convert(datetime, :APPROVE_END_DATE ,20) ) ");
						
						String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
						String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
							
						parames.put("APPROVE_START_DATE", sendDtmFrom);
						parames.put("APPROVE_END_DATE", sendDtmTo);
				 }
				 
				 hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL  ");
				 hql.append(" AND TR.TRADER_CATEGORY = '300' ");
				 
//Oat Edit 21/04/58
//				 hql.append(" AND TR.ORG_ID = :TRADER_ORG_ID ");
//				 parames.put("TRADER_ORG_ID", dto.getOrgId());
				 
				 hql.append(" AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS = 'N' ");
				 
				 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
				 parames.put("TRADER_ORG_ID", dto.getOrgId());
//End Oat Edit 21/04/58
				 
				 hql.append(" ) AS A3 ");
				
				 hql.append("  , ");
				 hql.append(" (SELECT COUNT(*) AS AREA ");
				 hql.append(" FROM TRADER TR 	INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID) ");
				 
//Oat Edit 21/04/58
				 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
				 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Edit 21/04/58
				 
				 hql.append(" WHERE TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS IN ('N','H')  ");
				 hql.append(" AND RE.REGISTRATION_TYPE = 'R' AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS = 'N'  ");
				 
				// hql.append(" AND RE.APPROVE_DATE BETWEEN CONVERT(DATETIME, '01/12/2013', 103) AND CONVERT(DATETIME, '30/12/2013', 103) ");
				//ApproveDateFrom
				 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
				 {
//				 	hql.append(" AND RE.APPROVE_DATE BETWEEN :APPROVE_DATE_FORM ");
//					    parames.put("APPROVE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
//					    
//					    //ApproveDateTo
//					    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
//					    {
//					    	hql.append(" AND :APPROVE_DATE_TO ");
//						    parames.put("APPROVE_DATE_TO", ft.parse(dto.getApproveDateTo()));
//					    }
//					    else
//					    {
//					    	hql.append(" AND :APPROVE_DATE_TO ");
//						    parames.put("APPROVE_DATE_TO", new Date());
//					    }
					 
					    hql.append("  AND ( RE.APPROVE_DATE >=  convert(datetime, :APPROVE_START_DATE ,20) ");
						hql.append(" AND RE.APPROVE_DATE <=  convert(datetime, :APPROVE_END_DATE ,20) ) ");
						
						String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
						String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
							
						parames.put("APPROVE_START_DATE", sendDtmFrom);
						parames.put("APPROVE_END_DATE", sendDtmTo);
				 }
				 
				 hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL  ");
				 hql.append(" AND TR.TRADER_CATEGORY = '400' ");
				 
//Oat Edit 21/04/58
//				 hql.append(" AND TR.ORG_ID = :TRADER_ORG_ID ");
//				 parames.put("TRADER_ORG_ID", dto.getOrgId());
				 
				 hql.append(" AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS = 'N' ");
				 
				 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
				 parames.put("TRADER_ORG_ID", dto.getOrgId());
//End Oat Edit 21/04/58
				 
				 hql.append(" ) AS A4 ");
			    
			    
			    
			    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());

			    sqlQuery.addScalar("OUTBOUND", Hibernate.LONG); 
			    sqlQuery.addScalar("INBOUND", Hibernate.LONG); 
			    sqlQuery.addScalar("COUNTRY", Hibernate.LONG); 
			    sqlQuery.addScalar("AREA", Hibernate.LONG); 
			    
			    sqlQuery.setProperties(parames);
			    List<Object[]>  result = sqlQuery.list();
		  
			    return result;
		}
	
	//ตารางแสดงจำนวนผู้มาดำเนินการเกี่ยวกับธุรกิจนำเที่ยวในภาพรวม (ค้นหาทีละสำนักงานตาม ordId)
	public List<Object[]> findExcelBusinessAll(RegistrationDTO dto)throws Exception
	{
		   Map<String,Object> parames = new HashMap<String,Object>();
		    
		    StringBuilder hql = new StringBuilder();
		    
		    DateFormat ft =  DateUtils.getProcessDateFormatThai();

		    hql.append(" SELECT A.OLD, B.NEW, C.CANCLE, D.REVOKEXX FROM ");
		    //ยอดเดิม
		     hql.append(" (SELECT COUNT(*) AS OLD ");
			 hql.append(" FROM TRADER TR 	INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID) ");   
			 
//Oat Edit 20/04/58
			 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
			 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Edit 20/04/58
			 
			 hql.append(" WHERE TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS IN ('N')  ");
			 hql.append(" AND TR.LICENSE_STATUS = 'N' AND RE.APPROVE_STATUS = 'A'  ");
			 //Oat Add 05/01/59
			 hql.append(" AND RE.RECORD_STATUS = 'N' ");
			 
			 hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL  ");
//			 hql.append(" AND RE.APPROVE_DATE < CONVERT(DATETIME, '25/11/2013', 103) ");
			//ApproveDateFrom
			 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
			 {
//			 	hql.append(" AND RE.APPROVE_DATE < :APPROVE_DATE_FORM ");
//				    parames.put("APPROVE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
				    
				    hql.append("  AND ( RE.APPROVE_DATE <  convert(datetime, :APPROVE_START_DATE ,20) ) ");
//					hql.append(" AND RE.APPROVE_DATE <=  convert(datetime, :APPROVE_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
//					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
						
					parames.put("APPROVE_START_DATE", sendDtmFrom);
//					parames.put("APPROVE_END_DATE", sendDtmTo);
			 }
			 		 
//Oat Edit 20/04/58
//			 hql.append(" AND TR.ORG_ID = :TRADER_ORG_ID ");
//			 parames.put("TRADER_ORG_ID", dto.getOrgId());
					 
			 hql.append(" AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS = 'N' ");
			 
			 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
			 parames.put("TRADER_ORG_ID", dto.getOrgId());
			 
//End Oat Edit 20/04/58
			 
			 hql.append(" ) AS A ");
			 
			 //รายใหม่
			 hql.append(" , ");
			 hql.append(" (SELECT COUNT(*) AS NEW ");
			 hql.append(" FROM TRADER TR 	INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID)");
			 
//Oat Edit 20/04/58
			 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
			 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Edit 20/04/58
			 
			 hql.append(" WHERE TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS IN ('N','H') ");
			 
			//Oat Edit 05/01/59
//			 hql.append(" AND RE.REGISTRATION_TYPE = 'N' AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS = 'N' ");
			 hql.append(" AND RE.REGISTRATION_TYPE = 'N' AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS IN ('N','H') ");
			 
			 
			 hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL ");
//			 hql.append(" AND RE.APPROVE_DATE BETWEEN CONVERT(DATETIME, '25/11/2013', 103) AND CONVERT(DATETIME, '30/12/2013', 103) "); 
			//ApproveDateFrom
			 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
			 {
//			 	hql.append(" AND RE.APPROVE_DATE BETWEEN :APPROVE_DATE_FORM ");
//				    parames.put("APPROVE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
//				    
//				    //ApproveDateTo
//				    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
//				    {
//				    	hql.append(" AND :APPROVE_DATE_TO ");
//					    parames.put("APPROVE_DATE_TO", ft.parse(dto.getApproveDateTo()));
//				    }
//				    else
//				    {
//				    	hql.append(" AND :APPROVE_DATE_TO ");
//					    parames.put("APPROVE_DATE_TO", new Date());
//				    }
				 
				    hql.append("  AND ( RE.APPROVE_DATE >=  convert(datetime, :APPROVE_START_DATE ,20) ");
					hql.append(" AND RE.APPROVE_DATE <=  convert(datetime, :APPROVE_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
						
					parames.put("APPROVE_START_DATE", sendDtmFrom);
					parames.put("APPROVE_END_DATE", sendDtmTo);
			 }
			 
//Oat Edit 20/04/58
//			 hql.append(" AND TR.ORG_ID = :TRADER_ORG_ID ");
//			 parames.put("TRADER_ORG_ID", dto.getOrgId());
					 
			 //Oat Edit 05/01/59
//			 hql.append(" AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS = 'N' ");
			 hql.append(" AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS IN ('N','H') ");
			 
			 
			 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
			 parames.put("TRADER_ORG_ID", dto.getOrgId());
			 
//End Oat Edit 20/04/58
			 
			 hql.append(" ) AS B ");
		    
			 //ยกเลิก
			 hql.append(" , ");
			 hql.append(" (SELECT COUNT(*) AS CANCLE  ");
			 hql.append(" FROM DEACTIVATE_LICENSE DL ");

//Oat Edit 20/04/58
			 hql.append(" INNER JOIN TRADER TR ON (DL.TRADER_TYPE = TR.TRADER_TYPE AND DL.LICENSE_NO = TR.LICENSE_NO) ");
			 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
			 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Edit 20/04/58
			 
			 hql.append(" WHERE DL.TRADER_TYPE = 'B' AND DL.APPROVE_STATUS IN ('A','C') AND DL.RECORD_STATUS = 'N'  ");
//			 AND DL.BOOK_DATE BETWEEN CONVERT(DATETIME, '01/10/2013', 103) AND CONVERT(DATETIME, '30/12/2014', 103);

			//BookDateFrom
			 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
			 {
			 	hql.append(" AND DL.BOOK_DATE BETWEEN :BOOK_DATE_FORM ");
				    parames.put("BOOK_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
				    
				    //BookDateTo
				    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
				    {
				    	hql.append(" AND :BOOK_DATE_TO ");
					    parames.put("BOOK_DATE_TO", ft.parse(dto.getApproveDateTo()));
				    }
				    else
				    {
				    	hql.append(" AND :BOOK_DATE_TO ");
					    parames.put("BOOK_DATE_TO", new Date());
				    }
			 }
			 
//Oat Edit 20/04/58
			 hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL  ");
			 hql.append(" AND TR.TRADER_TYPE = 'B' AND TR.LICENSE_STATUS = 'C' AND TR.RECORD_STATUS = 'N' ");
			 hql.append(" AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS = 'N' ");
			 
			 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
			 parames.put("TRADER_ORG_ID", dto.getOrgId());
			 
			 //parames.put("DEACTIVATE_LICENSE_ORG_ID", dto.getOrgId());
			 //hql.append(" AND DL.ORG_ID = :DEACTIVATE_LICENSE_ORG_ID ");
		 
//End Oat Edit 20/04/58			 
			 
			 hql.append(" ) AS C ");
			 
			 //เพิกถอน
			 hql.append(" , ");
			 hql.append(" (SELECT  COUNT(*) AS REVOKEXX  ");
			 hql.append(" FROM REVOKE_LICENSE RL INNER JOIN REVOKE_MAP_TRADER RM ON(RL.REVOKE_ID = RM.REVOKE_ID) ");
			 
//Oat Edit 20/04/58
			 hql.append(" INNER JOIN TRADER TR ON (RM.LICENSE_NO = TR.LICENSE_NO) ");
			 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
			 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID)  ");
//End Oat Edit 20/04/58
			 
			 hql.append(" WHERE RM.TRADER_TYPE = 'B' AND RL.REVOKE_STATUS IN ('R','C') AND RL.RECORD_STATUS = 'N' ");
			 
//			 hql.append("AND RE.APPROVE_DATE BETWEEN CONVERT(DATETIME, '25/11/2013', 103) AND CONVERT(DATETIME, '30/12/2013', 103)  ");
			//ApproveDateFrom
			 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
			 {
			 	hql.append(" AND RL.REVOKE_DATE BETWEEN :REVOKE_DATE_FORM ");
				    parames.put("REVOKE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
				    
				    //ApproveDateTo
				    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
				    {
				    	hql.append(" AND :REVOKE_DATE_TO ");
					    parames.put("REVOKE_DATE_TO", ft.parse(dto.getApproveDateTo()));
				    }
				    else
				    {
				    	hql.append(" AND :REVOKE_DATE_TO ");
					    parames.put("REVOKE_DATE_TO", new Date());
				    }
			 }
			 
			//Oat Edit 20/04/58
			 hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL  ");
			 hql.append(" AND TR.TRADER_TYPE = 'B' AND TR.LICENSE_STATUS = 'R' AND TR.RECORD_STATUS = 'N' ");
			 hql.append(" AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS = 'N' ");
			 
			 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
			 parames.put("TRADER_ORG_ID", dto.getOrgId());
			 
//			 hql.append(" AND RL.ORG_ID = :REVOKE_LICENSE_ORG_ID ");
//			 parames.put("REVOKE_LICENSE_ORG_ID", dto.getOrgId());
		 
//End Oat Edit 20/04/58	
			 
			 hql.append(" ) AS D ");
			 
		    
		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());

		    sqlQuery.addScalar("OLD", Hibernate.LONG); 
		    sqlQuery.addScalar("NEW", Hibernate.LONG); 
		    sqlQuery.addScalar("CANCLE", Hibernate.LONG); 
		    sqlQuery.addScalar("REVOKEXX", Hibernate.LONG); 
		    
		    sqlQuery.setProperties(parames);
		    List<Object[]>  result = sqlQuery.list();
	  
		    return result;
	}
	
	//ตารางแสดงจำนวนของธุรกิจนำเที่ยวจำแนกตามประเภท (ค้นหาทีละสำนักงานตาม orgId)
	public List<Object[]> findExcelBusinessByTraderCategory(RegistrationDTO dto)throws Exception
	{
		   Map<String,Object> parames = new HashMap<String,Object>();
		    
		    StringBuilder hql = new StringBuilder();
		    
		    DateFormat ft =  DateUtils.getProcessDateFormatThai();
		    
			 hql.append(" SELECT A1.OUTBOUND , A2.INBOUND, A3.COUNTRY ,A4.AREA  FROM ");
			
			 hql.append(" (SELECT COUNT(*)  AS OUTBOUND ");
			 hql.append(" FROM TRADER TR 	INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID) ");
			 
//Oat Edit 2/04/58
			 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
			 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Edit 2/04/58
			 
			 hql.append(" WHERE TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS IN ('N','H')  ");
			 hql.append(" AND RE.REGISTRATION_TYPE = 'N' AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS IN ('N','H')  ");
			
			 // hql.append(" AND RE.APPROVE_DATE BETWEEN CONVERT(DATETIME, '01/10/2013', 103) AND CONVERT(DATETIME, '30/12/2013', 103) ");
			 //ApproveDateFrom
			 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
			 {
//			 	hql.append(" AND RE.APPROVE_DATE BETWEEN :APPROVE_DATE_FORM ");
//				    parames.put("APPROVE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
//				    
//				    //ApproveDateTo
//				    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
//				    {
//				    	hql.append(" AND :APPROVE_DATE_TO ");
//					    parames.put("APPROVE_DATE_TO", ft.parse(dto.getApproveDateTo()));
//				    }
//				    else
//				    {
//				    	hql.append(" AND :APPROVE_DATE_TO ");
//					    parames.put("APPROVE_DATE_TO", new Date());
//				    }
				 
				    hql.append("  AND ( RE.APPROVE_DATE >=  convert(datetime, :APPROVE_START_DATE ,20) ");
					hql.append(" AND RE.APPROVE_DATE <=  convert(datetime, :APPROVE_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
						
					parames.put("APPROVE_START_DATE", sendDtmFrom);
					parames.put("APPROVE_END_DATE", sendDtmTo);
			 }
			 
			 hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL  ");
			 hql.append(" AND TR.TRADER_CATEGORY = '100' ");
			 
//Oat Edit 2/04/58
//			 hql.append(" AND TR.ORG_ID = :TRADER_ORG_ID ");
//			 parames.put("TRADER_ORG_ID", dto.getOrgId());
			 
			 hql.append(" AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS IN ('N','H') ");
			 
			 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
			 parames.put("TRADER_ORG_ID", dto.getOrgId());
			 
//End Oat Edit 2/04/58
			 
			 hql.append(" ) AS A1 ");
			 hql.append("  , ");
			 hql.append(" (SELECT COUNT(*) AS INBOUND ");
			 hql.append(" FROM TRADER TR 	INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID) ");
//Oat Edit 2/04/58
			 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
			 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Edit 2/04/58
			 hql.append(" WHERE TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS IN ('N','H')  ");
			 hql.append(" AND RE.REGISTRATION_TYPE = 'N' AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS IN ('N','H')  ");
			 
			// hql.append(" AND RE.APPROVE_DATE BETWEEN CONVERT(DATETIME, '01/10/2013', 103) AND CONVERT(DATETIME, '30/12/2013', 103) ");
			//ApproveDateFrom
			 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
			 {
//			 	hql.append(" AND RE.APPROVE_DATE BETWEEN :APPROVE_DATE_FORM ");
//				    parames.put("APPROVE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
//				    
//				    //ApproveDateTo
//				    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
//				    {
//				    	hql.append(" AND :APPROVE_DATE_TO ");
//					    parames.put("APPROVE_DATE_TO", ft.parse(dto.getApproveDateTo()));
//				    }
//				    else
//				    {
//				    	hql.append(" AND :APPROVE_DATE_TO ");
//					    parames.put("APPROVE_DATE_TO", new Date());
//				    }
				 
				    hql.append("  AND ( RE.APPROVE_DATE >=  convert(datetime, :APPROVE_START_DATE ,20) ");
					hql.append(" AND RE.APPROVE_DATE <=  convert(datetime, :APPROVE_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
						
					parames.put("APPROVE_START_DATE", sendDtmFrom);
					parames.put("APPROVE_END_DATE", sendDtmTo);
			 }
			 
			 hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL  ");
			 hql.append(" AND TR.TRADER_CATEGORY = '200' ");
//Oat Edit 2/04/58
//			 hql.append(" AND TR.ORG_ID = :TRADER_ORG_ID ");
//			 parames.put("TRADER_ORG_ID", dto.getOrgId());
			 
			 hql.append(" AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS IN ('N','H') ");
			 
			 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
			 parames.put("TRADER_ORG_ID", dto.getOrgId());
			 
//End Oat Edit 2/04/58
			 hql.append(" ) AS A2 ");
			
			 hql.append(" , ");
			 hql.append("  (SELECT COUNT(*) AS COUNTRY ");
			 hql.append("  FROM TRADER TR 	INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID) ");
//Oat Edit 2/04/58
			 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
			 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Edit 2/04/58
			 hql.append(" WHERE TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS IN ('N','H')  ");
			 hql.append(" AND RE.REGISTRATION_TYPE = 'N' AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS IN ('N','H')  ");
			 
			// hql.append(" AND RE.APPROVE_DATE BETWEEN CONVERT(DATETIME, '01/10/2013', 103) AND CONVERT(DATETIME, '30/12/2013', 103) ");
			//ApproveDateFrom
			 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
			 {
//			 	hql.append(" AND RE.APPROVE_DATE BETWEEN :APPROVE_DATE_FORM ");
//				    parames.put("APPROVE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
//				    
//				    //ApproveDateTo
//				    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
//				    {
//				    	hql.append(" AND :APPROVE_DATE_TO ");
//					    parames.put("APPROVE_DATE_TO", ft.parse(dto.getApproveDateTo()));
//				    }
//				    else
//				    {
//				    	hql.append(" AND :APPROVE_DATE_TO ");
//					    parames.put("APPROVE_DATE_TO", new Date());
//				    }
				 
				 hql.append("  AND ( RE.APPROVE_DATE >=  convert(datetime, :APPROVE_START_DATE ,20) ");
					hql.append(" AND RE.APPROVE_DATE <=  convert(datetime, :APPROVE_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
						
					parames.put("APPROVE_START_DATE", sendDtmFrom);
					parames.put("APPROVE_END_DATE", sendDtmTo);
			 }
			 
			 hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL  ");
			 hql.append(" AND TR.TRADER_CATEGORY = '300' ");
//Oat Edit 2/04/58
//			 hql.append(" AND TR.ORG_ID = :TRADER_ORG_ID ");
//			 parames.put("TRADER_ORG_ID", dto.getOrgId());
			 
			 hql.append(" AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS IN ('N','H') ");
			 
			 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
			 parames.put("TRADER_ORG_ID", dto.getOrgId());
			 
//End Oat Edit 2/04/58
			 hql.append(" ) AS A3 ");
			
			 hql.append(" 		, ");
			 hql.append(" (SELECT COUNT(*) AS AREA ");
			 hql.append(" FROM TRADER TR 	INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID) ");
//Oat Edit 2/04/58
			 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
			 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Edit 2/04/58
			 hql.append(" WHERE TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS IN ('N','H')  ");
			 hql.append(" AND RE.REGISTRATION_TYPE = 'N' AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS IN ('N','H')  ");
			 
			// hql.append(" AND RE.APPROVE_DATE BETWEEN CONVERT(DATETIME, '01/12/2013', 103) AND CONVERT(DATETIME, '30/12/2013', 103) ");
			//ApproveDateFrom
			 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
			 {
//			 	hql.append(" AND RE.APPROVE_DATE BETWEEN :APPROVE_DATE_FORM ");
//				    parames.put("APPROVE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
//				    
//				    //ApproveDateTo
//				    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
//				    {
//				    	hql.append(" AND :APPROVE_DATE_TO ");
//					    parames.put("APPROVE_DATE_TO", ft.parse(dto.getApproveDateTo()));
//				    }
//				    else
//				    {
//				    	hql.append(" AND :APPROVE_DATE_TO ");
//					    parames.put("APPROVE_DATE_TO", new Date());
//				    }
				 
				 hql.append("  AND ( RE.APPROVE_DATE >=  convert(datetime, :APPROVE_START_DATE ,20) ");
					hql.append(" AND RE.APPROVE_DATE <=  convert(datetime, :APPROVE_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
						
					parames.put("APPROVE_START_DATE", sendDtmFrom);
					parames.put("APPROVE_END_DATE", sendDtmTo);
			 }
			 
			 hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL  ");
			 hql.append(" AND TR.TRADER_CATEGORY = '400' ");
			//Oat Edit 2/04/58
//			 hql.append(" AND TR.ORG_ID = :TRADER_ORG_ID ");
//			 parames.put("TRADER_ORG_ID", dto.getOrgId());
			 
			 hql.append(" AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS IN ('N','H') ");
			 
			 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
			 parames.put("TRADER_ORG_ID", dto.getOrgId());
			 
//End Oat Edit 2/04/58
			 hql.append(" ) AS A4 ");
		    
		    
		    
		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());

		    sqlQuery.addScalar("OUTBOUND", Hibernate.LONG); 
		    sqlQuery.addScalar("INBOUND", Hibernate.LONG); 
		    sqlQuery.addScalar("COUNTRY", Hibernate.LONG); 
		    sqlQuery.addScalar("AREA", Hibernate.LONG); 
		    
		    sqlQuery.setProperties(parames);
		    List<Object[]>  result = sqlQuery.list();
	  
		    return result;
	}
	
	//กราฟแสดงจำนวนของธุรกิจนำเที่ยวจำแนกตามประเภทการจดทะเบียน
	public List<Object[]> findChartPieBusinessByTraderCategory(RegistrationDTO dto, User user)throws Exception
	{
		   Map<String,Object> parames = new HashMap<String,Object>();
		    
		    StringBuilder hql = new StringBuilder();
		    
		    DateFormat ft =  DateUtils.getProcessDateFormatThai();
		    
		    //100
		    hql.append(" SELECT TR.TRADER_CATEGORY, COUNT(*) AS COUNT ");
		    hql.append(" FROM TRADER TR 	INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID) ");
		    hql.append(" WHERE TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS IN ('N','H') ");
		    hql.append(" AND RE.REGISTRATION_TYPE = 'N' AND RE.APPROVE_STATUS = 'A' AND RE.APPROVE_STATUS = 'N' ");
		    
		    //ApproveDateFrom
		    if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
		    {
//		    	hql.append(" AND RE.APPROVE_DATE BETWEEN :APPROVE_DATE_FORM ");
//			    parames.put("APPROVE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
//			    
//			    //ApproveDateTo
//			    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
//			    {
//			    	hql.append(" AND :APPROVE_DATE_TO ");
//				    parames.put("APPROVE_DATE_TO", ft.parse(dto.getApproveDateTo()));
//			    }
//			    else
//			    {
//			    	hql.append(" AND :APPROVE_DATE_TO ");
//				    parames.put("APPROVE_DATE_TO", new Date());
//			    }
		    	
		    	hql.append("  AND ( RE.APPROVE_DATE >=  convert(datetime, :APPROVE_START_DATE ,20) ");
				hql.append(" AND RE.APPROVE_DATE <=  convert(datetime, :APPROVE_END_DATE ,20) ) ");
				
				String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
				String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
					
				parames.put("APPROVE_START_DATE", sendDtmFrom);
				parames.put("APPROVE_END_DATE", sendDtmTo);
		    }
//		    hql.append(" AND RE.APPROVE_DATE BETWEEN CONVERT(DATETIME, '01/10/2013', 103) AND CONVERT(DATETIME, '30/12/2013', 103) ");
		    
		    hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL ");
		    hql.append(" AND TR.TRADER_CATEGORY = '100' ");
		    hql.append(" GROUP BY TR.TRADER_CATEGORY ");
		    hql.append(" UNION ");
		    
		    //200
		    hql.append(" SELECT TR.TRADER_CATEGORY, COUNT(*) AS COUNT ");
		    hql.append(" FROM TRADER TR 	INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID) ");
		    hql.append(" WHERE TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS IN ('N','H') ");
		    hql.append(" AND RE.REGISTRATION_TYPE = 'N' AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS = 'N' ");
		    
		  //ApproveDateFrom
		    if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
		    {
//		    	hql.append(" AND RE.APPROVE_DATE BETWEEN :APPROVE_DATE_FORM ");
//			    parames.put("APPROVE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
//			    
//			    //ApproveDateTo
//			    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
//			    {
//			    	hql.append(" AND :APPROVE_DATE_TO ");
//				    parames.put("APPROVE_DATE_TO", ft.parse(dto.getApproveDateTo()));
//			    }
//			    else
//			    {
//			    	hql.append(" AND :APPROVE_DATE_TO ");
//				    parames.put("APPROVE_DATE_TO", new Date());
//			    }
		    	
		    	hql.append("  AND ( RE.APPROVE_DATE >=  convert(datetime, :APPROVE_START_DATE ,20) ");
				hql.append(" AND RE.APPROVE_DATE <=  convert(datetime, :APPROVE_END_DATE ,20) ) ");
				
				String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
				String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
					
				parames.put("APPROVE_START_DATE", sendDtmFrom);
				parames.put("APPROVE_END_DATE", sendDtmTo);
		    }
		    
		    hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL ");
		    hql.append(" AND TR.TRADER_CATEGORY = '200' ");
		    hql.append(" GROUP BY TR.TRADER_CATEGORY ");
		    hql.append(" UNION ");
		    
		  //300
		    hql.append(" SELECT TR.TRADER_CATEGORY, COUNT(*) AS COUNT ");
		    hql.append(" FROM TRADER TR 	INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID) ");
		    hql.append(" WHERE TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS IN ('N','H') ");
		    hql.append(" AND RE.REGISTRATION_TYPE = 'N' AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS = 'N' ");
		    
		  //ApproveDateFrom
		    if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
		    {
//		    	hql.append(" AND RE.APPROVE_DATE BETWEEN :APPROVE_DATE_FORM ");
//			    parames.put("APPROVE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
//			    
//			    //ApproveDateTo
//			    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
//			    {
//			    	hql.append(" AND :APPROVE_DATE_TO ");
//				    parames.put("APPROVE_DATE_TO", ft.parse(dto.getApproveDateTo()));
//			    }
//			    else
//			    {
//			    	hql.append(" AND :APPROVE_DATE_TO ");
//				    parames.put("APPROVE_DATE_TO", new Date());
//			    }
		    	
		    	hql.append("  AND ( RE.APPROVE_DATE >=  convert(datetime, :APPROVE_START_DATE ,20) ");
				hql.append(" AND RE.APPROVE_DATE <=  convert(datetime, :APPROVE_END_DATE ,20) ) ");
				
				String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
				String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
					
				parames.put("APPROVE_START_DATE", sendDtmFrom);
				parames.put("APPROVE_END_DATE", sendDtmTo);
		    }
		    
		    hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL ");
		    hql.append(" AND TR.TRADER_CATEGORY = '300' ");
		    hql.append(" GROUP BY TR.TRADER_CATEGORY ");
		    hql.append(" UNION ");
		    
		  //400
		    hql.append(" SELECT TR.TRADER_CATEGORY, COUNT(*) AS COUNT ");
		    hql.append(" FROM TRADER TR 	INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID) ");
		    hql.append(" WHERE TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS IN ('N','H') ");
		    hql.append(" AND RE.REGISTRATION_TYPE = 'N' AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS = 'N' ");
		    
		  //ApproveDateFrom
		    if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
		    {
//		    	hql.append(" AND RE.APPROVE_DATE BETWEEN :APPROVE_DATE_FORM ");
//			    parames.put("APPROVE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
//			    
//			    //ApproveDateTo
//			    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
//			    {
//			    	hql.append(" AND :APPROVE_DATE_TO ");
//				    parames.put("APPROVE_DATE_TO", ft.parse(dto.getApproveDateTo()));
//			    }
//			    else
//			    {
//			    	hql.append(" AND :APPROVE_DATE_TO ");
//				    parames.put("APPROVE_DATE_TO", new Date());
//			    }
		    	

		    	hql.append("  AND ( RE.APPROVE_DATE >=  convert(datetime, :APPROVE_START_DATE ,20) ");
				hql.append(" AND RE.APPROVE_DATE <=  convert(datetime, :APPROVE_END_DATE ,20) ) ");
				
				String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
				String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
					
				parames.put("APPROVE_START_DATE", sendDtmFrom);
				parames.put("APPROVE_END_DATE", sendDtmTo);
		    }
		    
		    hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL ");
		    hql.append(" AND TR.TRADER_CATEGORY = '400' ");
		    hql.append(" GROUP BY TR.TRADER_CATEGORY ");

		    
		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
			
		    sqlQuery.addScalar("TRADER_CATEGORY", Hibernate.STRING); 
		    sqlQuery.addScalar("COUNT", Hibernate.LONG); 
		    
		    sqlQuery.setProperties(parames);
		    List<Object[]>  result = sqlQuery.list();
	  
		    return result;
	}
	
	//กราฟแสดงจำนวนของธุรกิจนำเที่ยวจำแนกตามสาขา
	public List<Object[]> findChartPieBusinessByOrgId(RegistrationDTO dto, User user)throws Exception
	{
		   Map<String,Object> parames = new HashMap<String,Object>();
		    
		    StringBuilder hql = new StringBuilder();
		    
		    DateFormat ft =  DateUtils.getProcessDateFormatThai();
		    
		    //OrgId=1
		    hql.append(" SELECT TR.ORG_ID, COUNT(*) AS COUNT ");
		    hql.append(" FROM TRADER TR 	INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID) ");
		    hql.append(" WHERE TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS IN ('N','H') ");
		    hql.append(" AND RE.REGISTRATION_TYPE = 'N' AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS = 'N' ");
		    
		    //ApproveDateFrom
		    if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
		    {
//		    	hql.append(" AND RE.APPROVE_DATE BETWEEN :APPROVE_DATE_FORM ");
//			    parames.put("APPROVE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
//			    
//			    //ApproveDateTo
//			    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
//			    {
//			    	hql.append(" AND :APPROVE_DATE_TO ");
//				    parames.put("APPROVE_DATE_TO", ft.parse(dto.getApproveDateTo()));
//			    }
//			    else
//			    {
//			    	hql.append(" AND :APPROVE_DATE_TO ");
//				    parames.put("APPROVE_DATE_TO", new Date());
//			    }
		    	
		    	hql.append("  AND ( RE.APPROVE_DATE >=  convert(datetime, :APPROVE_START_DATE ,20) ");
				hql.append(" AND RE.APPROVE_DATE <=  convert(datetime, :APPROVE_END_DATE ,20) ) ");
				
				String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
				String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
					
				parames.put("APPROVE_START_DATE", sendDtmFrom);
				parames.put("APPROVE_END_DATE", sendDtmTo);
		    }
//		    hql.append(" AND RE.APPROVE_DATE BETWEEN CONVERT(DATETIME, '01/10/2013', 103) AND CONVERT(DATETIME, '30/12/2013', 103) ");
		    
		    hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL ");
		    hql.append(" AND TR.ORG_ID = '1' ");
		    hql.append(" GROUP BY TR.ORG_ID ");
		    hql.append(" UNION ");
		    
		    //ORG_ID=2
		    hql.append(" SELECT TR.ORG_ID, COUNT(*) AS COUNT ");
		    hql.append(" FROM TRADER TR 	INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID) ");
		    hql.append(" WHERE TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS IN ('N','H') ");
		    hql.append(" AND RE.REGISTRATION_TYPE = 'N' AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS = 'N' ");
		    
		  //ApproveDateFrom
		    if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
		    {
//		    	hql.append(" AND RE.APPROVE_DATE BETWEEN :APPROVE_DATE_FORM ");
//			    parames.put("APPROVE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
//			    
//			    //ApproveDateTo
//			    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
//			    {
//			    	hql.append(" AND :APPROVE_DATE_TO ");
//				    parames.put("APPROVE_DATE_TO", ft.parse(dto.getApproveDateTo()));
//			    }
//			    else
//			    {
//			    	hql.append(" AND :APPROVE_DATE_TO ");
//				    parames.put("APPROVE_DATE_TO", new Date());
//			    }
		    	
		    	hql.append("  AND ( RE.APPROVE_DATE >=  convert(datetime, :APPROVE_START_DATE ,20) ");
				hql.append(" AND RE.APPROVE_DATE <=  convert(datetime, :APPROVE_END_DATE ,20) ) ");
				
				String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
				String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
					
				parames.put("APPROVE_START_DATE", sendDtmFrom);
				parames.put("APPROVE_END_DATE", sendDtmTo);
		    }
		    
		    hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL ");
		    hql.append(" AND TR.ORG_ID = '2' ");
		    hql.append(" GROUP BY TR.ORG_ID ");
		    hql.append(" UNION ");
		    
		  //ORG_ID=3
		    hql.append(" SELECT TR.ORG_ID, COUNT(*) AS COUNT ");
		    hql.append(" FROM TRADER TR 	INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID) ");
		    hql.append(" WHERE TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS IN ('N','H') ");
		    hql.append(" AND RE.REGISTRATION_TYPE = 'N' AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS = 'N' ");
		    
		  //ApproveDateFrom
		    if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
		    {
//		    	hql.append(" AND RE.APPROVE_DATE BETWEEN :APPROVE_DATE_FORM ");
//			    parames.put("APPROVE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
//			    
//			    //ApproveDateTo
//			    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
//			    {
//			    	hql.append(" AND :APPROVE_DATE_TO ");
//				    parames.put("APPROVE_DATE_TO", ft.parse(dto.getApproveDateTo()));
//			    }
//			    else
//			    {
//			    	hql.append(" AND :APPROVE_DATE_TO ");
//				    parames.put("APPROVE_DATE_TO", new Date());
//			    }
		    	hql.append("  AND ( RE.APPROVE_DATE >=  convert(datetime, :APPROVE_START_DATE ,20) ");
				hql.append(" AND RE.APPROVE_DATE <=  convert(datetime, :APPROVE_END_DATE ,20) ) ");
				
				String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
				String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
					
				parames.put("APPROVE_START_DATE", sendDtmFrom);
				parames.put("APPROVE_END_DATE", sendDtmTo);
		    }
		    
		    hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL ");
		    hql.append(" AND TR.ORG_ID = '3' ");
		    hql.append(" GROUP BY TR.ORG_ID ");
		    hql.append(" UNION ");
		    
		  //ORG_ID=4
		    hql.append(" SELECT TR.ORG_ID, COUNT(*) AS COUNT ");
		    hql.append(" FROM TRADER TR 	INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID) ");
		    hql.append(" WHERE TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS IN ('N','H') ");
		    hql.append(" AND RE.REGISTRATION_TYPE = 'N' AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS = 'N' ");
		    
		  //ApproveDateFrom
		    if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
		    {
//		    	hql.append(" AND RE.APPROVE_DATE BETWEEN :APPROVE_DATE_FORM ");
//			    parames.put("APPROVE_DATE_FORM", ft.parse(dto.getApproveDateFrom()));
//			    
//			    //ApproveDateTo
//			    if(dto.getApproveDateTo()!=null && !dto.getApproveDateTo().equals(""))
//			    {
//			    	hql.append(" AND :APPROVE_DATE_TO ");
//				    parames.put("APPROVE_DATE_TO", ft.parse(dto.getApproveDateTo()));
//			    }
//			    else
//			    {
//			    	hql.append(" AND :APPROVE_DATE_TO ");
//				    parames.put("APPROVE_DATE_TO", new Date());
//			    }
		    	
		    	hql.append("  AND ( RE.APPROVE_DATE >=  convert(datetime, :APPROVE_START_DATE ,20) ");
				hql.append(" AND RE.APPROVE_DATE <=  convert(datetime, :APPROVE_END_DATE ,20) ) ");
				
				String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
				String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
					
				parames.put("APPROVE_START_DATE", sendDtmFrom);
				parames.put("APPROVE_END_DATE", sendDtmTo);
		    }
		    
		    hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL ");
		    hql.append(" AND TR.ORG_ID = '4' ");
		    hql.append(" GROUP BY TR.ORG_ID ");

		    
		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
			
		    sqlQuery.addScalar("ORG_ID", Hibernate.LONG); 
		    sqlQuery.addScalar("COUNT", Hibernate.LONG); 
		    
		    sqlQuery.setProperties(parames);
		    List<Object[]>  result = sqlQuery.list();
	  
		    return result;
	}
	
	public List<Object[]> findLicenseDetail(RegistrationDTO dto) throws Exception
	{
		 	Map<String,Object> parames = new HashMap<String,Object>();
		    
		    StringBuilder hql = new StringBuilder();
		    
		   if(TraderType.TOUR_COMPANIES.getStatus().equals(dto.getTraderType()))
		   {
			   hql = this.findBusinessLicense(dto);
		   }
		   if(TraderType.GUIDE.getStatus().equals(dto.getTraderType()))
		   {
			   hql = this.findGuideLicense(dto);
		   }
		   
		   if(dto.getOrgId() > 0)
			{
		    	parames.put("orgId", dto.getOrgId());
			}
			
			
			if(dto.getProvinceId() > 0)
			{
				parames.put("provinceId", dto.getProvinceId());
			}

		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
		    
		    
			
		    sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING); 
		    sqlQuery.addScalar("TRADER_CATEGORY", Hibernate.STRING); 
		    sqlQuery.addScalar("EFFECTIVE_DATE", Hibernate.DATE); 
		    sqlQuery.addScalar("EXPIRE_DATE", Hibernate.DATE); 
		    sqlQuery.addScalar("ADDRESS", Hibernate.STRING); 
		    sqlQuery.addScalar("MOBILE_NO", Hibernate.STRING); 
		    sqlQuery.addScalar("TELEPHONE", Hibernate.STRING); 
		    
		    sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING); 
		    sqlQuery.addScalar("TRADER_NAME_EN", Hibernate.STRING); 
		    
		    sqlQuery.addScalar("EMAIL", Hibernate.STRING); 
		    sqlQuery.addScalar("IDENTITY_NO", Hibernate.STRING);
		    
		    sqlQuery.setProperties(parames);
		    List<Object[]>  result = sqlQuery.list();
	  
		    return result;
	}
	
	public StringBuilder findBusinessLicense(RegistrationDTO param) throws Exception
	{
		StringBuilder hql = new StringBuilder();
		
		hql.append(" SELECT  ");
		hql.append(" TR.LICENSE_NO AS LICENSE_NO ,TR.TRADER_NAME AS TRADER_NAME,TR.TRADER_NAME_EN AS TRADER_NAME_EN ");
		hql.append(" , CASE TR.TRADER_CATEGORY	 ");
		hql.append(" 	WHEN  '100' THEN 'OUTBOUND' ");
		hql.append(" 	WHEN  '200' THEN 'INBOUND' ");
		hql.append(" 	WHEN  '300' THEN 'ในประเทศ' ");
		hql.append(" 	WHEN  '400' THEN 'เฉพาะพื้นที่' ");
		hql.append("  END AS TRADER_CATEGORY ");
		hql.append(" ,TR.EFFECTIVE_DATE AS EFFECTIVE_DATE ");
		hql.append(" ,TR.EXPIRE_DATE AS EXPIRE_DATE ");
		hql.append(" ,'เลขที่ ' + ISNULL ( TA.ADDRESS_NO , '' ) +  ISNULL ( ' อาคาร ' + TA.BUILDING_NAME , '' )+ ISNULL ( ' ชั้น ' + TA.FLOOR , '' ) + ISNULL (' หมู่ที่ ' + TA.MOO , '' ) ");
		hql.append(" + ISNULL (' ซอย ' + TA.SOI , '' )+ ISNULL (' ถนน ' + TA.ROAD_NAME , '' ) + ISNULL ( ' ตำบล/แขวง ' + ML.TAMBOL_NAME , '' ) ");
		hql.append(" + ISNULL (' อำเภอ/เขต ' + AM.AMPHUR_NAME, '' ) + ISNULL (' จังหวัด ' + MP.PROVINCE_NAME, '' )+ ISNULL (' รหัสไปรษณีย์ ' + TA.POST_CODE, '' ) AS ADDRESS ");
		hql.append(" ,TA.MOBILE_NO AS MOBILE_NO, TA.TELEPHONE AS TELEPHONE, TA.EMAIL AS EMAIL ");
		hql.append(" ,P.IDENTITY_NO ");
		hql.append(" FROM TRADER TR ");
		hql.append(" LEFT JOIN TRADER_ADDRESS TA ");
		hql.append(" ON(TA.TRADER_ID = TR.TRADER_ID)  ");
		hql.append(" LEFT JOIN MAS_AMPHUR AM ");
		hql.append(" ON(AM.AMPHUR_ID = TA.AMPHUR_ID) ");
		hql.append(" LEFT JOIN MAS_PROVINCE MP ");
		hql.append(" ON(TA.PROVINCE_ID = MP.PROVINCE_ID) ");
		hql.append(" LEFT JOIN MAS_TAMBOL ML ");
		hql.append(" ON(ML.TAMBOL_ID = TA.TAMBOL_ID) ");
		hql.append(" LEFT JOIN PERSON P ");
		hql.append(" ON(TR.PERSON_ID = P.PERSON_ID) ");
		hql.append(" WHERE TR.BRANCH_TYPE IS NULL ");
		hql.append(" AND TR.RECORD_STATUS = 'N' ");
		hql.append(" AND TR.TRADER_TYPE = 'B' ");
	
//Oat Edit 19/05/58		
//		if(param.getOrgId() > 0)
//		{
//			hql.append(" AND TR.ORG_ID = :orgId ");
//		}
		if(param.getOrgId() > 0)
		{
			hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :orgId ) ");
		}
//Oat Edit 19/05/58
		
		hql.append(" AND TR.LICENSE_STATUS = 'N' ");
		
		if(param.getProvinceId() > 0)
		{
			hql.append(" AND TA.PROVINCE_ID = :provinceId ");
		}
		
//Oat Edit 19/05/58
		hql.append(" AND TA.ADDRESS_TYPE = 'O' ");
//		hql.append(" AND TA.ADDRESS_TYPE = 'S' ");
		
//Oat Edit 01/07/58
		hql.append(" ORDER BY TA.PROVINCE_ID, TA.AMPHUR_ID, TA.TAMBOL_ID; ");
//		hql.append(" ORDER BY TR.LICENSE_NO; ");
		
		return hql;
	}
	
	public StringBuilder findGuideLicense(RegistrationDTO param) throws Exception
	{
		StringBuilder hql = new StringBuilder();

		hql.append(" SELECT  ");
		hql.append(" TR.LICENSE_NO AS LICENSE_NO ,ISNULL (PF.PREFIX_NAME , '' ) + PE.FIRST_NAME +' '+ PE.LAST_NAME AS TRADER_NAME ,TR.TRADER_NAME_EN AS TRADER_NAME_EN ");
		
		hql.append(" , CASE TR.TRADER_CATEGORY	 ");
		hql.append(" 	WHEN  '100' THEN 'ประเภท ทั่วไป (ให้บริการนำเที่ยวแก่นักท่องเที่ยวไทยและต่างประเทศ)' ");
		hql.append(" 	WHEN  '101' THEN 'ประเภท ทั่วไป (ให้บริการนำเที่ยวเฉพาะนักท่องเที่ยวไทย)' ");
		hql.append(" 	WHEN  '200' THEN 'ประเภท เฉพาะพื้นที่ (ให้บริการนำเที่ยวแก่นักท่องเที่ยวไทยและต่างประเทศ)' ");
		hql.append(" 	WHEN  '201' THEN 'ประเภท เฉพาะพื้นที่ (ให้บริการนำเที่ยวเฉพาะนักท่องเที่ยวไทย)' ");
		hql.append(" 	WHEN  '202' THEN 'ประเภท เดินป่า (ให้บริการนำเที่ยวแก่นักท่องเที่ยวไทยและต่างประเทศในเขตป่า)' ");
		hql.append(" 	WHEN  '203' THEN 'ประเภท ศิลปวัฒนธรรม (ให้บริการนำเที่ยวแก่นักท่องเที่ยวไทยและต่างประเทศด้านศิลปวัฒนธรรม)' ");
		hql.append(" 	WHEN  '204' THEN 'ประเภท ทางทะเล (ให้บริการนำเที่ยวแก่นักท่องเที่ยวไทยและต่างประเทศทางทะเล)' ");
		hql.append(" 	WHEN  '205' THEN 'ประเภท ทางทะเลชายฝั่ง (ให้บริการนำเที่ยวแก่นักท่องเที่ยวไทยและต่างประเทศทางทะเลชายฝั่ง ไม่เกิน 40 ไมล์ทะเล)' ");
		hql.append(" 	WHEN  '206' THEN 'ประเภทเฉพาะ แหล่งท่องเที่ยวธรรมชาติ (ให้บริการนำเที่ยวแก่นักท่องเที่ยวไทยและต่างประเทศในแหล่งท่องเที่ยวธรรมชาติ)' ");
		hql.append(" 	WHEN  '207' THEN 'ประเภทเฉพาะ วัฒนธรรมท้องถิ่น (ให้บริการนำเที่ยวแก่นักท่องเที่ยวไทยและต่างประเทศด้านวัฒนธรรมท้องถิ่น)' ");
		hql.append("  END AS TRADER_CATEGORY ");
		
		hql.append(" ,TR.EFFECTIVE_DATE AS EFFECTIVE_DATE ");
		hql.append(" ,TR.EXPIRE_DATE AS EXPIRE_DATE ");
		hql.append(" ,'เลขที่ ' + ISNULL ( TA.ADDRESS_NO , '' ) +  ISNULL ( ' อาคาร ' + TA.BUILDING_NAME , '' )+ ISNULL ( ' ชั้น ' + TA.FLOOR , '' ) + ISNULL (' หมู่ที่ ' + TA.MOO , '' ) ");
		hql.append(" + ISNULL (' ซอย ' + TA.SOI , '' )+ ISNULL (' ถนน ' + TA.ROAD_NAME , '' ) + ISNULL ( ' ตำบล/แขวง ' + TA.ROAD_NAME , '' ) ");
		hql.append(" + ISNULL (' อำเภอ/เขต ' + AM.AMPHUR_NAME, '' ) + ISNULL (' จังหวัด ' + MP.PROVINCE_NAME, '' )+ ISNULL (' รหัสไปรษณีย์ ' + TA.POST_CODE, '' ) AS ADDRESS ");
		hql.append(" ,TA.MOBILE_NO AS MOBILE_NO, TA.TELEPHONE AS TELEPHONE , TA.EMAIL AS EMAIL ");
		hql.append(" ,PE.IDENTITY_NO ");
		hql.append(" FROM TRADER TR  ");
		
		hql.append(" INNER JOIN  ");
		hql.append(" (SELECT TX.LICENSE_NO AS LICENSE_NO, MAX(TX.EXPIRE_DATE) AS EXPIRE_DATE FROM TRADER TX ");
		hql.append(" WHERE TX.BRANCH_TYPE IS NULL ");
		hql.append(" AND TX.TRADER_TYPE = 'G' ");
		hql.append(" GROUP BY TX.LICENSE_NO) T1 ");
		hql.append(" ON ((T1.LICENSE_NO = TR.LICENSE_NO) AND (TR.EXPIRE_DATE = T1.EXPIRE_DATE)) ");
		
		hql.append(" INNER JOIN PERSON PE ");
		hql.append(" ON(PE.PERSON_ID = TR.PERSON_ID) ");
		hql.append(" LEFT JOIN MAS_PREFIX PF ");
		hql.append(" ON(PF.PREFIX_ID = PE.PREFIX_ID) ");
		hql.append(" LEFT JOIN TRADER_ADDRESS TA ");
		hql.append(" ON(TA.TRADER_ID = TR.TRADER_ID)  ");
		hql.append(" LEFT JOIN MAS_AMPHUR AM ");
		hql.append(" ON(AM.AMPHUR_ID = TA.AMPHUR_ID) ");
		hql.append(" LEFT JOIN MAS_PROVINCE MP ");
		hql.append(" ON(TA.PROVINCE_ID = MP.PROVINCE_ID) ");
		
		hql.append(" WHERE TR.BRANCH_TYPE IS NULL ");
		hql.append(" AND TR.RECORD_STATUS = 'N' ");
		hql.append(" AND TR.TRADER_TYPE = 'G' ");

//Oat Edit 19/05/58
//		if(param.getOrgId() > 0)
//		{
//			hql.append(" AND TR.ORG_ID = :orgId ");
//		}
		if(param.getOrgId() > 0)
		{
			hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :orgId ) ");
		}
		
//End Oat Edit 19/05/58
		
		hql.append(" AND TR.LICENSE_STATUS = 'N' ");
		
		if(param.getProvinceId() > 0)
		{
			hql.append(" AND TA.PROVINCE_ID = :provinceId ");
		}
		
//Oat Edit 19/05/58
//		hql.append(" AND TA.ADDRESS_TYPE = 'O' ");
		hql.append(" AND TA.ADDRESS_TYPE = 'S' ");
//End Oat Edit 19/05/58
		
		hql.append(" AND TR.EXPIRE_DATE > CONVERT (date, GETDATE()) ");
		hql.append(" ORDER BY TR.LICENSE_NO; ");
		
		return hql;
	}
	
	//OAT ADD 04/01/59
	//ตารางสรุปจำนวนผู้ขอรับใบอนุญาตเป็นมัคคุเทศก์(ยื่นใหม่)
		public List<Object[]> findExcelGuideNew(RegistrationDTO dto)throws Exception
		{
			Map<String,Object> parames = new HashMap<String,Object>();
		    
		    StringBuilder hql = new StringBuilder();
		    
		    DateFormat ft =  DateUtils.getProcessDateFormatThai();

		    hql.append(" SELECT COUNT(*) AS ORG, ");
		    hql.append(" TR.ORG_ID AS ORG_ID ");
		    hql.append(" FROM TRADER TR ");
		    hql.append(" INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID)");
//			hql.append(" LEFT JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
//			hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");   
		    hql.append(" WHERE TR.TRADER_TYPE = 'G' AND TR.RECORD_STATUS IN ('N','H') ");
			hql.append(" AND TR.LICENSE_STATUS = 'N' AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS IN ('N','H') ");
			hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL ");
			hql.append(" AND RE.REGISTRATION_TYPE = 'N'  ");
			
			//ApproveDateFrom
			 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
			 {
				 hql.append("  and ( RE.APPROVE_DATE >=  convert(datetime, :APPROVE_START_DATE ,20) ");
					hql.append(" and RE.APPROVE_DATE <=  convert(datetime, :APPROVE_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
						
					parames.put("APPROVE_START_DATE", sendDtmFrom);
					parames.put("APPROVE_END_DATE", sendDtmTo);
			 }
			 
			hql.append(" AND TR.TRADER_CATEGORY = :TRADER_CATEGORY ");
			parames.put("TRADER_CATEGORY", dto.getTraderCategory());
			
			hql.append(" AND TR.ORG_ID = :TRADER_ORG_ID");
			parames.put("TRADER_ORG_ID", dto.getOrgId());
			hql.append(" GROUP BY TR.ORG_ID");
			
//			hql.append(" AND TA.ADDRESS_TYPE = 'S' AND TA.RECORD_STATUS IN ('N','H') ");
//			hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
//			parames.put("TRADER_ORG_ID", dto.getOrgId());
 
		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());

		    sqlQuery.addScalar("ORG", Hibernate.INTEGER); 
		    sqlQuery.addScalar("ORG_ID", Hibernate.INTEGER); 
		    
		    sqlQuery.setProperties(parames);
		    List<Object[]>  result = sqlQuery.list();
		    
		    System.out.println(result.size());
	  
		    return result;
		}
		
		
	//Oat Add 3/02/59
	//ตารางแสดงจำนวนของธุรกิจนำเที่ยวจำแนกตามประเภท (ค้นหาทีละสำนักงานตาม orgId)
	public List<Object[]> findExcelBusinessByTraderCategory2(RegistrationDTO dto)throws Exception
	{
		   Map<String,Object> parames = new HashMap<String,Object>();
		    
		    StringBuilder hql = new StringBuilder();
		    
		    DateFormat ft =  DateUtils.getProcessDateFormatThai();
		    
			 hql.append(" SELECT A1.OUTBOUND , A2.INBOUND, A3.COUNTRY ,A4.AREA  FROM ");

			 hql.append(" (SELECT COUNT(*)  AS OUTBOUND ");
			 hql.append(" FROM TRADER TR 	INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID) ");
			 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
			 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
			 hql.append(" WHERE TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS IN ('N')  ");
			 hql.append(" AND TR.LICENSE_STATUS = 'N' AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS IN ('N')  ");
			 //ApproveDateFrom
			 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
			 {
				    hql.append("  AND ( RE.APPROVE_DATE >=  convert(datetime, :APPROVE_START_DATE ,20) ");
					hql.append(" AND RE.APPROVE_DATE <=  convert(datetime, :APPROVE_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
						
					parames.put("APPROVE_START_DATE", sendDtmFrom);
					parames.put("APPROVE_END_DATE", sendDtmTo);
			 }
			 hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL  ");
			 hql.append(" AND TR.TRADER_CATEGORY = '100' ");		 
			 hql.append(" AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS IN ('N') ");
			 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
			 parames.put("TRADER_ORG_ID", dto.getOrgId());
			 hql.append(" ) AS A1 ");
			 hql.append("  , ");
			 
			 hql.append(" (SELECT COUNT(*) AS INBOUND ");
			 hql.append(" FROM TRADER TR 	INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID) ");
			 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
			 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
			 hql.append(" WHERE TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS IN ('N')  ");
			 hql.append(" AND TR.LICENSE_STATUS = 'N' AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS IN ('N')  ");
			//ApproveDateFrom
			 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
			 {
				    hql.append("  AND ( RE.APPROVE_DATE >=  convert(datetime, :APPROVE_START_DATE ,20) ");
					hql.append(" AND RE.APPROVE_DATE <=  convert(datetime, :APPROVE_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
						
					parames.put("APPROVE_START_DATE", sendDtmFrom);
					parames.put("APPROVE_END_DATE", sendDtmTo);
			 }
			 
			 hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL  ");
			 hql.append(" AND TR.TRADER_CATEGORY = '200' ");
			 hql.append(" AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS IN ('N') ");
			 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
			 parames.put("TRADER_ORG_ID", dto.getOrgId());
			 hql.append(" ) AS A2 ");
			 hql.append(" , ");
			 
			 hql.append("  (SELECT COUNT(*) AS COUNTRY ");
			 hql.append("  FROM TRADER TR 	INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID) ");
			 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
			 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
			 hql.append(" WHERE TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS IN ('N')  ");
			 hql.append(" AND TR.LICENSE_STATUS = 'N' AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS IN ('N')  ");
			//ApproveDateFrom
			 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
			 { 
				 hql.append("  AND ( RE.APPROVE_DATE >=  convert(datetime, :APPROVE_START_DATE ,20) ");
					hql.append(" AND RE.APPROVE_DATE <=  convert(datetime, :APPROVE_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
						
					parames.put("APPROVE_START_DATE", sendDtmFrom);
					parames.put("APPROVE_END_DATE", sendDtmTo);
			 }
			 
			 hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL  ");
			 hql.append(" AND TR.TRADER_CATEGORY = '300' ");		 
			 hql.append(" AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS IN ('N') ");			 
			 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
			 parames.put("TRADER_ORG_ID", dto.getOrgId());
			 hql.append(" ) AS A3 ");
			
			 hql.append(" 		, ");
			 hql.append(" (SELECT COUNT(*) AS AREA ");
			 hql.append(" FROM TRADER TR 	INNER JOIN REGISTRATION RE ON(TR.TRADER_ID = RE.TRADER_ID) ");
			 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
			 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
			 hql.append(" WHERE TR.TRADER_TYPE = 'B' AND TR.RECORD_STATUS IN ('N')  ");
			 hql.append(" AND TR.LICENSE_STATUS = 'N' AND RE.APPROVE_STATUS = 'A' AND RE.RECORD_STATUS IN ('N')  ");
			//ApproveDateFrom
			 if(dto.getApproveDateFrom()!=null && !dto.getApproveDateFrom().equals(""))
			 {	 
				 hql.append("  AND ( RE.APPROVE_DATE >=  convert(datetime, :APPROVE_START_DATE ,20) ");
					hql.append(" AND RE.APPROVE_DATE <=  convert(datetime, :APPROVE_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
						
					parames.put("APPROVE_START_DATE", sendDtmFrom);
					parames.put("APPROVE_END_DATE", sendDtmTo);
			 }
			 hql.append(" AND TR.BRANCH_TYPE IS NULL AND TR.TRADER_CATEGORY IS NOT NULL  ");
			 hql.append(" AND TR.TRADER_CATEGORY = '400' ");			 
			 hql.append(" AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS IN ('N') ");			 
			 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
			 parames.put("TRADER_ORG_ID", dto.getOrgId());
			 hql.append(" ) AS A4 ");
		    		 		    
		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());

		    sqlQuery.addScalar("OUTBOUND", Hibernate.LONG); 
		    sqlQuery.addScalar("INBOUND", Hibernate.LONG); 
		    sqlQuery.addScalar("COUNTRY", Hibernate.LONG); 
		    sqlQuery.addScalar("AREA", Hibernate.LONG); 
		    
		    sqlQuery.setProperties(parames);
		    List<Object[]>  result = sqlQuery.list();
	  
		    return result;
	}
	
}






