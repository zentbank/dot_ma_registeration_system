package sss.dot.tourism.dao.complaint;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.ComplaintDoc;
import sss.dot.tourism.dto.complaint.ComplaintDocDTO;
import sss.dot.tourism.util.RecordStatus;

import com.sss.aut.service.User;

@Repository("complaintDocDAO")
public class ComplaintDocDAO extends BaseDAO{
	
	private static final long serialVersionUID = -6455075565457388105L;
	
	public ComplaintDocDAO()
	{
		this.domainObj = ComplaintDoc.class;
	}
	
	public List<ComplaintDoc> findComplaintDoc(ComplaintDocDTO dto, User user)throws Exception
	{
		ArrayList params = new ArrayList();
	    StringBuilder hql = new StringBuilder();
	
	    hql.append(" from  ComplaintDoc as doc ");
		hql.append(" where doc.recordStatus = ? ");
		params.add(RecordStatus.NORMAL.getStatus());
		
		hql.append(" and doc.complaintLicense.complaintId = ? ");
		params.add(dto.getComplaintLicenseId());
		
		
		hql.append(" and doc.complaintDocType = ? ");
		params.add(dto.getComplaintDocType());
	    
	    List<ComplaintDoc> list = (List<ComplaintDoc>) this.getHibernateTemplate().find(hql.toString(), params.toArray());

		return list;

	}

}
