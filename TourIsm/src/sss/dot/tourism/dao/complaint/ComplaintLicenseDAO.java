package sss.dot.tourism.dao.complaint;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.ComplaintLicense;
import sss.dot.tourism.dto.complaint.ComplaintLicenseDTO;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.RecordStatus;

import com.sss.aut.service.User;

@Repository("complaintLicenseDAO")
public class ComplaintLicenseDAO extends BaseDAO{
	
	private static final long serialVersionUID = -6455075565457388105L;
	
	public ComplaintLicenseDAO()
	{
		this.domainObj = ComplaintLicense.class;
	}
	
	public List<Object[]> findComplaintPaging(ComplaintLicenseDTO dto, User user ,int start, int limit)throws Exception
	{
		Map<String,Object> parames = new HashMap<String,Object>();
	    
		DateFormat ft =  DateUtils.getProcessDateFormatThai();
		
	    StringBuilder hql = new StringBuilder();
	    
	    hql.append(" SELECT COL.COMPLAINT_ID AS COMPLAINT_ID, ");
	    hql.append(" COL.QUESTION_TYPE AS QUESTION_TYPE, ");    
	    hql.append(" COL.COMPLAINT_DESC AS COMPLAINT_DESC, ");
	    hql.append(" COL.COMPLAINT_CARD_ID AS COMPLAINT_CARD_ID, ");
	    hql.append(" COL.COMPLAINT_NAME AS COMPLAINT_NAME, ");
	    hql.append(" COL.COMPLAINT_TEL AS COMPLAINT_TEL, ");
	    hql.append(" COL.COMPLAINT_EMAIL AS COMPLAINT_EMAIL, ");
	    hql.append(" COL.COMPLAINT_NO AS COMPLAINT_NO, ");
	    hql.append(" COL.COMPLAINT_DATE AS COMPLAINT_DATE, ");
	    hql.append(" COL.MAS_COMPLAINT_TYPE AS MAS_COMPLAINT_TYPE, ");
	    hql.append(" COL.COMPLAINT_STATUS AS COMPLAINT_STATUS, ");
	    hql.append(" COL.AUTHORITY_COMMENT AS AUTHORITY_COMMENT, ");
	    hql.append(" COL.COMPLAINT_PROGRESS AS COMPLAINT_PROGRESS, ");
	    hql.append(" COL.AUTHORITY AS AUTHORITY, ");
	    
	    hql.append(" COMT.LICENSE_NO AS LICENSE_NO, ");
	    hql.append(" COMT.TRADER_TYPE AS TRADER_TYPE, ");
	    
	    hql.append(" COL.AUTHORITY_DATE AS AUTHORITY_DATE ");
	    
	    //FROM
	    hql.append(" FROM COMPLAINT_LICENSE COL ");
		hql.append(" INNER JOIN COMPLAINT_MAP_TRADER COMT ");
		hql.append(" ON(COL.COMPLAINT_ID = COMT.COMPLAINT_ID) ");
		
		//WHERE
		//RecordStatus
		hql.append(" WHERE COL.RECORD_STATUS = :COMPLAINT_RECORD_STATUS ");
	    parames.put("COMPLAINT_RECORD_STATUS", RecordStatus.NORMAL.getStatus());
	    
	    //QuestionType
	    hql.append(" AND COL.QUESTION_TYPE = :COMPLAINT_QUESTION_TYPE ");
	    parames.put("COMPLAINT_QUESTION_TYPE", dto.getQuestionType());
	    
	    //LICENSE_NO เลขที่ใบอนุญาต
	    if(dto.getLicenseNo() !=null && !dto.getLicenseNo().equals(""))
	    {
	    	hql.append(" AND COMT.LICENSE_NO = :COMPLAINT_MAP_TRADER_LICENSE_NO ");
		    parames.put("COMPLAINT_MAP_TRADER_LICENSE_NO", dto.getLicenseNo());
	    }
	    
	    //TRADER_TYPE ประเภทใบอนุญาต
	    if(dto.getTraderType() !=null && !dto.getTraderType().equals(""))
	    {
	    	hql.append(" AND COMT.TRADER_TYPE = :COMPLAINT_MAP_TRADER_TRADER_TYPE ");
		    parames.put("COMPLAINT_MAP_TRADER_TRADER_TYPE", dto.getTraderType());
	    }
	    
	    //TraderName ชื่อธุรกิจนำเที่ยว/มัคคุเทศก์/ผู้นำเที่ยว
	    if(dto.getTraderName()!=null && !dto.getTraderName().equals(""))
	    {
	    	hql.append(" AND COMT.TRADER_NAME LIKE :TRADER_TRADER_NAME ");
		    parames.put("TRADER_TRADER_NAME", "%"+dto.getTraderName()+"%");
	    }
	    
	    //ComplaintName ชื่อผู้ร้องเรียน
	    if(dto.getComplaintName()!=null && !dto.getComplaintName().equals(""))
	    {
	    	hql.append(" AND COL.COMPLAINT_NAME LIKE :COMPLAINT_COMPLAINT_NAME ");
		    parames.put("COMPLAINT_COMPLAINT_NAME", "%"+dto.getComplaintName()+"%");
	    }
	    
	    //MasComplaintTypeId ประเภทเรื่องร้องเรียน
	    if(dto.getMasComplaintTypeId() > 0)
	    {
	    	hql.append(" AND COL.MAS_COMPLAINT_TYPE = :COMPLAINT_MAS_COMPLAINT_TYPE ");
		    parames.put("COMPLAINT_MAS_COMPLAINT_TYPE", dto.getMasComplaintTypeId());
	    }
	    
	    //ComplaintStatus สถานะ
	    if(dto.getComplaintStatus()!=null && !dto.getComplaintStatus().equals(""))
	    {
	    	hql.append(" AND COL.COMPLAINT_STATUS = :COMPLAINT_COMPLAINT_STATUS ");
		    parames.put("COMPLAINT_COMPLAINT_STATUS", dto.getComplaintStatus());
	    }
	    
	    //Question
	    //complaintDesc รายละเอียดเรื่อง
	    if(dto.getComplaintDesc()!=null && !dto.getComplaintDesc().equals(""))
	    {
	    	hql.append(" AND COL.COMPLAINT_DESC LIKE :COMPLAINT_COMPLAINT_DESC ");
		    parames.put("COMPLAINT_COMPLAINT_DESC", "%"+dto.getComplaintDesc()+"%");
	    }
	    
	    //ComplaintDateFrom
	    if(dto.getComplaintDateFrom()!=null && !dto.getComplaintDateFrom().equals(""))
		 {
		 	hql.append(" AND COL.COMPLAINT_DATE BETWEEN :COMPLAINT_DATE_FORM ");
		    parames.put("COMPLAINT_DATE_FORM", ft.parse(dto.getComplaintDateFrom()));

		    //ComplaintDateTo
		    if(dto.getComplaintDateTo()!=null && !dto.getComplaintDateTo().equals(""))
		    {
		    	Calendar cal = Calendar.getInstance();
				cal.setTime(ft.parse(dto.getComplaintDateTo()));
				cal.add(Calendar.DATE, 1);
				
		    	hql.append(" AND :COMPLAINT_DATE_TO ");
		    	parames.put("COMPLAINT_DATE_TO", cal.getTime());
//			    parames.put("COMPLAINT_DATE_TO", ft.parse(dto.getComplaintDateTo()));
		    }
		    else
		    {
		    	Calendar cal = Calendar.getInstance();
				cal.setTime(new Date());
				cal.add(Calendar.DATE, 1);
		    	
		    	hql.append(" AND :COMPLAINT_DATE_TO ");
		    	parames.put("COMPLAINT_DATE_TO", cal.getTime());
//			    parames.put("COMPLAINT_DATE_TO", new Date());
		    }
		 }
	    
	    
	    System.out.println("##########ComplaintLicenseDAO##########");
		System.out.println("#####hql = "+hql.toString());
		SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
	    sqlQuery.setFirstResult(start);
	    sqlQuery.setMaxResults(limit);
	    
	    sqlQuery.addScalar("COMPLAINT_ID", Hibernate.LONG); //0
		sqlQuery.addScalar("QUESTION_TYPE", Hibernate.STRING); //1
		sqlQuery.addScalar("COMPLAINT_DESC", Hibernate.STRING); //2
		sqlQuery.addScalar("COMPLAINT_CARD_ID", Hibernate.STRING); //3
		sqlQuery.addScalar("COMPLAINT_NAME", Hibernate.STRING); //4
		sqlQuery.addScalar("COMPLAINT_TEL", Hibernate.STRING); //5
		sqlQuery.addScalar("COMPLAINT_EMAIL", Hibernate.STRING); //6
		sqlQuery.addScalar("COMPLAINT_NO", Hibernate.STRING); //7
		sqlQuery.addScalar("COMPLAINT_DATE", Hibernate.DATE);//8
		sqlQuery.addScalar("MAS_COMPLAINT_TYPE", Hibernate.LONG);//9 
		sqlQuery.addScalar("COMPLAINT_STATUS", Hibernate.STRING); //10
		sqlQuery.addScalar("AUTHORITY_COMMENT", Hibernate.STRING); //11
		sqlQuery.addScalar("COMPLAINT_PROGRESS", Hibernate.STRING); //12
		sqlQuery.addScalar("AUTHORITY", Hibernate.LONG);     //13
		sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING); //14
		sqlQuery.addScalar("TRADER_TYPE", Hibernate.STRING); //15
		
		sqlQuery.addScalar("AUTHORITY_DATE", Hibernate.DATE); //16
	    
	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
  
	    return result;
	}
	
	public List<Object[]> findComplaintDetail(ComplaintLicenseDTO dto)throws Exception
	{
		Map<String,Object> parames = new HashMap<String,Object>();
	    
		DateFormat ft =  DateUtils.getProcessDateFormatThai();
		
	    StringBuilder hql = new StringBuilder();
	    hql.append(" SELECT  ");
	    hql.append(" 	PR.PREFIX_NAME AS PREFIX_NAME, ");
	    hql.append(" 	PO.POSTFIX_NAME AS POSTFIX_NAME, ");
	    hql.append(" 	PE.FIRST_NAME AS FIRST_NAME, ");
	    hql.append(" 	PE.LAST_NAME AS LAST_NAME, ");
	    hql.append(" 	TR.TRADER_CATEGORY AS TRADER_CATEGORY, ");
	    hql.append(" 	PE.PERSON_TYPE AS PERSON_TYPE, ");
	    hql.append("    CL.COMPLAINT_NO AS COMPLAINT_NO ");
	    hql.append(" FROM COMPLAINT_LICENSE CL ");
	    hql.append(" INNER JOIN COMPLAINT_MAP_TRADER CMT ON CMT.COMPLAINT_ID = CL.COMPLAINT_ID ");
	    hql.append(" INNER JOIN TRADER TR ON TR.TRADER_TYPE = CMT.TRADER_TYPE AND CMT.LICENSE_NO = TR.LICENSE_NO ");
	    hql.append(" INNER JOIN PERSON PE ON PE.PERSON_ID = TR.PERSON_ID ");
	    hql.append(" LEFT JOIN MAS_PREFIX PR ON PR.PREFIX_ID = PE.PREFIX_ID ");
	    hql.append(" LEFT JOIN MAS_POSFIX PO ON PO.POSTFIX_ID = PE.POSTFIX_ID ");
	    hql.append(" WHERE  CL.COMPLAINT_PROGRESS = 'ML' AND ");
	    hql.append(" 	   CL.COMPLAINT_STATUS = 'G' AND ");
	    hql.append(" 	   CL.RECORD_STATUS = 'N' AND ");
	    hql.append(" 	   TR.RECORD_STATUS = 'N' AND ");
	    
	    if(dto.getComplaintLicenseId() > 0)
	    {
	    	hql.append(" CL.COMPLAINT_ID = :COMPLAINT_ID ");
	    	parames.put("COMPLAINT_ID", dto.getComplaintLicenseId());
	    }
	    
	    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
	    sqlQuery.addScalar("PREFIX_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("POSTFIX_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("FIRST_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("LAST_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("TRADER_CATEGORY", Hibernate.STRING);
	    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);
	    sqlQuery.addScalar("COMPLAINT_NO", Hibernate.STRING);
	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
	    return result;
	}
	
}











