package sss.dot.tourism.dao.complaint;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.sf.cglib.reflect.FastClass;
import net.sf.cglib.reflect.FastMethod;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.ComplaintLicense;
import sss.dot.tourism.domain.ComplaintMapTrader;
import sss.dot.tourism.domain.Trader;

@Repository("complaintMapTraderDAO")
public class ComplaintMapTraderDAO extends BaseDAO{
	
	private static final long serialVersionUID = -6455075565457388105L;
	
	public ComplaintMapTraderDAO()
	{
		this.domainObj = ComplaintMapTrader.class;
	}
	
	public Object insert(Object persistence)
	  {

	    return this.getHibernateTemplate().save(persistence);
	  }
	
	public void update(Object persistence)
	  {
	    this.getHibernateTemplate().update(persistence);
	  }
	
	public List<ComplaintMapTrader> findComplaintMapTraderByComplaintLicense(ComplaintLicense complaintLicense)
	{
		ArrayList params = new ArrayList();
		
		StringBuilder hql = new StringBuilder();
		hql.append(" select cmtr from  ComplaintMapTrader as cmtr ");
		hql.append(" where cmtr.complaintLicense.complaintId = ? ");
		params.add(complaintLicense.getComplaintId());
		
		List<ComplaintMapTrader> list = (List<ComplaintMapTrader>) this.getHibernateTemplate().find(hql.toString(), params.toArray());

		return list;
	}
}
