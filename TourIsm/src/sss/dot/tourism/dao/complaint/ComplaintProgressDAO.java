package sss.dot.tourism.dao.complaint;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.ComplaintProgress;
import sss.dot.tourism.dto.complaint.ComplaintLicenseDTO;
import sss.dot.tourism.dto.complaint.ComplaintProgressDTO;
import sss.dot.tourism.util.RecordStatus;

import com.sss.aut.service.User;

@Repository("complaintProgressDAO")
public class ComplaintProgressDAO extends BaseDAO{
	private static final long serialVersionUID = -6455075565457388105L;
	
	public ComplaintProgressDAO()
	{
		this.domainObj = ComplaintProgress.class;
	}
	
	public List<ComplaintProgress> findComplaintProgressByComplaintLicenseAndProgressStatus(ComplaintLicenseDTO dto, User user)throws Exception
	{
		ArrayList params = new ArrayList();
	    StringBuilder hql = new StringBuilder();
	
	    hql.append(" from  ComplaintProgress as cp ");
		hql.append(" where cp.recordStatus = ? ");
		params.add(RecordStatus.NORMAL.getStatus());
		
		hql.append(" and cp.complaintLicense.complaintId = ? ");
		params.add(dto.getComplaintLicenseId());
		
		if( dto.getComplaintStatus() != null && !dto.getComplaintStatus().equals("") )
		{
			hql.append(" and cp.progressStatus = ? ");
			params.add(dto.getComplaintStatus());
		}
		
		if( dto.getComplaintProgress() != null && !dto.getComplaintProgress().equals("") )
		{
			hql.append(" and cp.progressRole = ? ");
			params.add(dto.getComplaintProgress());
		}
		
	    
	    List<ComplaintProgress> list = (List<ComplaintProgress>) this.getHibernateTemplate().find(hql.toString(), params.toArray());

		return list;

	}
	
	public List<ComplaintProgress> findComplaintProgressByConsider(ComplaintProgressDTO dto, User user)throws Exception
	{
		ArrayList params = new ArrayList();
	    StringBuilder hql = new StringBuilder();
	
	    hql.append(" from  ComplaintProgress as cp ");
		hql.append(" where cp.recordStatus = ? ");
		params.add(RecordStatus.NORMAL.getStatus());
		
		hql.append(" and cp.complaintLicense.complaintId = ? ");
		params.add(dto.getComplaintLicenseId());
		
//		hql.append(" and cp.progressStatus = ? ");
//		params.add(dto.getComplaintStatus());
		
		if( dto.getProgressRole() != null && !dto.getProgressRole().equals("") )
		{
			hql.append(" and cp.progressRole = ? ");
			params.add(dto.getProgressRole());
		}
		
//		hql.append(" order by cp.progressDate, cp.comProgressId ");
		hql.append(" order by cp.createDtm ");
	    
	    List<ComplaintProgress> list = (List<ComplaintProgress>) this.getHibernateTemplate().find(hql.toString(), params.toArray());

		return list;

	}
	
}















