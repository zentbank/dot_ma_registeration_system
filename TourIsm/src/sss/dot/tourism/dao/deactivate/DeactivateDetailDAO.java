package sss.dot.tourism.dao.deactivate;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.DeactivateDetail;
import sss.dot.tourism.domain.SuspensionDetail;


@Repository("deactivateDetailDAO")
public class DeactivateDetailDAO  extends BaseDAO{
	
	public DeactivateDetailDAO()
	{
		this.domainObj = DeactivateDetail.class;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<DeactivateDetail> findByDeactivate(long deactiveId)
	  {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  DeactivateDetail as dep ");
		hql.append(" where dep.recordStatus = 'N' ");
		
		hql.append(" and dep.deactivateLicense.deactiveId = ? ");
		params.add(deactiveId);
	
		
		return (List<DeactivateDetail>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	  }
}
