package sss.dot.tourism.dao.deactivate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.DeactivateLicense;
import sss.dot.tourism.dto.deactivate.DeactivateLicenseDTO;

@Repository("deactivateLicenseDAO")
public class DeactivateLicenseDAO extends BaseDAO{

	public DeactivateLicenseDAO()
	{
		this.domainObj = DeactivateLicense.class;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Object[]> findDeactivateLicensePaging(DeactivateLicenseDTO param,int start, int limit)
	  {
		   Map<String,Object> parames = new HashMap<String,Object>();
		    
		    StringBuilder hql = new StringBuilder();
		    
		    hql.append(" SELECT ");
		    hql.append("  TR.TRADER_ID ");
		    hql.append(" ,TR.LICENSE_NO ");
		    hql.append(" ,TR.TRADER_NAME ");
		    hql.append(" ,TR.TRADER_TYPE  ");
		    hql.append(" ,PE.PERSON_ID ");
		    hql.append(" ,PX.PREFIX_NAME ");
		    hql.append(" ,PE.FIRST_NAME ");
		    hql.append(" ,PE.LAST_NAME ");
		    hql.append(" ,PO.POSTFIX_NAME ");
		    hql.append(" ,PE.PERSON_TYPE ");
		    hql.append(" ,DL.DEACTIVE_ID ");
		    hql.append(" ,DL.DEACTIVATE_DATE ");
		    hql.append(" ,DL.DEACTIVATE_NO ");
		    hql.append(" ,DL.DEACTIVATE_DATE ");
		    hql.append(" ,DL.BOOK_DATE ");
		    hql.append(" ,DL.BOOK_NO ");
		    hql.append(" ,DL.APPROVE_STATUS ");
		    hql.append(" ,PE.IDENTITY_NO ");
		    hql.append(" ,TR.TRADER_CATEGORY ");
		    hql.append(" ,TR.TRADER_NAME_EN ");
		    hql.append(" ,DL.RENEW_LICENSE_STATUS ");
		    hql.append(" ,DL.DEACTIVATE_RESON_REMARK ");
		    hql.append(" ,DL.OFFICER_NAME ");
		    
		    hql.append(" FROM DEACTIVATE_LICENSE DL ");
		    hql.append(" INNER JOIN TRADER TR ");
		    hql.append(" ON((TR.LICENSE_NO = DL.LICENSE_NO) AND (DL.TRADER_TYPE = TR.TRADER_TYPE)) ");
		    hql.append(" INNER JOIN PERSON PE ");
		    hql.append(" ON(PE.PERSON_ID = TR.PERSON_ID) ");
		    
//Oat Add 29/05/58
//			 hql.append(" LEFT JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
//			 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Add 29/05/58
		    
		    hql.append(" LEFT JOIN MAS_PREFIX PX ");
		    hql.append(" ON(PE.PREFIX_ID = PX.PREFIX_ID) ");
		    hql.append(" LEFT JOIN MAS_POSFIX PO ");
		    hql.append(" ON(PE.POSTFIX_ID = PO.POSTFIX_ID ) ");
		    hql.append(" WHERE TR.RECORD_STATUS = 'N' ");
		    hql.append(" AND TR.BRANCH_TYPE IS NULL ");
		    
		    if((param.getTraderType() != null) && (!param.getTraderType().isEmpty()))
		    {
		    	hql.append("  AND TR.TRADER_TYPE = :TRADER_TYPE ");
		    	parames.put("TRADER_TYPE", param.getTraderType());
		    }
		    
		    if((param.getTraderName() != null) && (!param.getTraderName().isEmpty()))
		    {
		    	hql.append("  AND TR.TRADER_NAME LIKE :TRADER_NAME ");
		    	parames.put("TRADER_NAME", "%"+param.getTraderName()+"%");
		    }
		    
		    if((param.getIdentityNo() != null) && (!param.getIdentityNo().isEmpty()))
		    {
		    	hql.append("  AND PE.IDENTITY_NO = :IDENTITY_NO ");
		    	parames.put("IDENTITY_NO", param.getIdentityNo());
		    }
		    if((param.getFirstName() != null) && (!param.getFirstName().isEmpty()))
		    {
		    	hql.append("  AND PE.FIRST_NAME LIKE :FIRST_NAME ");
		    	parames.put("FIRST_NAME", "%"+param.getFirstName()+"%");
		    }
		    if((param.getLastName() != null) && (!param.getLastName().isEmpty()))
		    {
		    	hql.append("  AND PE.LAST_NAME LIKE :LAST_NAME ");
		    	parames.put("LAST_NAME","%"+param.getLastName()+"%");
		    }		    
		    if((param.getApproveStatus() != null) && (!param.getApproveStatus().isEmpty()))
		    {
		    	hql.append("  AND DL.APPROVE_STATUS = :APPROVE_STATUS ");
		    	parames.put("APPROVE_STATUS", param.getApproveStatus());
		    }		
		  
		    if((param.getLicenseNo() != null) && (!param.getLicenseNo().isEmpty()))
		    {
		    	hql.append("  AND TR.LICENSE_NO = :LICENSE_NO ");
		    	parames.put("LICENSE_NO", param.getLicenseNo());
		    }	    
		    

//Oat Edit 29/05/58
//			 hql.append(" AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS = 'N' ");
			 
//			 if(param.getOrgId() > 0)
//			 {
//				 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
//				 parames.put("TRADER_ORG_ID", param.getOrgId());
//			 }
			 
			    if(param.getOrgId() > 0)
			    {
			    	hql.append("  AND DL.ORG_ID = :ORG_ID");
			    	parames.put("ORG_ID", param.getOrgId());
			    }
		 
//End Oat Edit 29/05/58
		    
		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
		    sqlQuery.setFirstResult(start);
		    sqlQuery.setMaxResults(limit);
		    
			
		    sqlQuery.addScalar("TRADER_ID", Hibernate.LONG); //0
		    sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING); //1
		    sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING); //2
		    sqlQuery.addScalar("TRADER_TYPE", Hibernate.STRING);//3
		    sqlQuery.addScalar("PERSON_ID", Hibernate.LONG);//4
		    sqlQuery.addScalar("PREFIX_NAME", Hibernate.STRING);//5
		    sqlQuery.addScalar("FIRST_NAME", Hibernate.STRING);//6
		    sqlQuery.addScalar("LAST_NAME", Hibernate.STRING);//7
		    sqlQuery.addScalar("POSTFIX_NAME", Hibernate.STRING);//8
		    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);//9
		    sqlQuery.addScalar("DEACTIVE_ID", Hibernate.LONG);//10
		    sqlQuery.addScalar("DEACTIVATE_DATE", Hibernate.DATE);//11
		    sqlQuery.addScalar("DEACTIVATE_NO", Hibernate.STRING);//12
		    sqlQuery.addScalar("DEACTIVATE_DATE", Hibernate.DATE);//13
		    sqlQuery.addScalar("BOOK_DATE", Hibernate.DATE);//14
		    sqlQuery.addScalar("BOOK_NO", Hibernate.STRING);//15
		    sqlQuery.addScalar("APPROVE_STATUS", Hibernate.STRING);//16
		    sqlQuery.addScalar("IDENTITY_NO", Hibernate.STRING);//17
		    sqlQuery.addScalar("TRADER_CATEGORY", Hibernate.STRING);//18
		    sqlQuery.addScalar("TRADER_NAME_EN", Hibernate.STRING);//19
		    sqlQuery.addScalar("RENEW_LICENSE_STATUS", Hibernate.INTEGER);//20	
		    sqlQuery.addScalar("DEACTIVATE_RESON_REMARK", Hibernate.STRING);//21
		    sqlQuery.addScalar("OFFICER_NAME", Hibernate.LONG);//22
		   

		    
		    sqlQuery.setProperties(parames);
		    List<Object[]>  result = sqlQuery.list();
	  
		    return result;
	  }
}
