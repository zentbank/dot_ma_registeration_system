package sss.dot.tourism.dao.deactivate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.MasDeactivateType;
import sss.dot.tourism.dto.deactivate.MasDeactivateTypeDTO;
import sss.dot.tourism.dto.punishment.MasActDTO;


@Repository("masDeactivateTypeDAO")
public class MasDeactivateTypeDAO extends BaseDAO {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5027558463318944906L;

	public MasDeactivateTypeDAO()
	{
		this.domainObj = MasDeactivateType.class;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<MasDeactivateType> findByTraderType(String traderType)
	  {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  MasDeactivateType as dep ");
		hql.append(" where dep.recordStatus = 'N' ");
		
		if((traderType != null) && (!(traderType.isEmpty())))
		{
			hql.append(" and dep.licenseType = ? ");
			params.add(traderType);
		}
		
		return (List<MasDeactivateType>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	  }
	
	public List<MasDeactivateTypeDTO> findbyDeactiveIdAndTraderType(long deactiveId,String traderType)
	{
		Map<String,Object> parames = new HashMap<String,Object>();
		    
	    StringBuilder hql = new StringBuilder();
	    
	    hql.append(" SELECT ");
	    hql.append(" MAC.MAS_DEACTIVATE_TYPE_ID ");
//	    hql.append(" ,SEL.DEACTIVATE_NAME ");
	    hql.append(" ,ISNULL(SEL.DEACTIVATE_NAME, MAC.DEACTIVATE_NAME) AS DEACTIVATE_NAME ");
	    
	    hql.append(" ,MAC.LICENSE_TYPE ");
	    hql.append(" ,SEL.DEACTIVE_ID ");
	    hql.append(" FROM ");
	    hql.append(" MAS_DEACTIVATE_TYPE MAC ");
	    hql.append(" LEFT JOIN ( ");
	    hql.append(" SELECT ");
	    hql.append(" MC.MAS_DEACTIVATE_TYPE_ID ");
//	    hql.append(" ,MC.DEACTIVATE_NAME ");
	    hql.append(" ,ISNULL(SD.DEACTIVATE_DESC, MC.DEACTIVATE_NAME) AS DEACTIVATE_NAME ");
	    
	    hql.append(" ,MC.LICENSE_TYPE ");
	    hql.append(" ,SD.DEACTIVE_ID ");
	    hql.append(" FROM DEACTIVATE_DETAIL SD ");
	    hql.append(" RIGHT  JOIN MAS_DEACTIVATE_TYPE MC ");
	    hql.append(" ON(SD.MAS_DEACTIVATE_TYPE_ID = MC.MAS_DEACTIVATE_TYPE_ID) ");
	    hql.append(" WHERE MC.RECORD_STATUS = 'N' ");
	    
	    hql.append(" AND SD.DEACTIVE_ID = :DEACTIVE_ID ");
	    parames.put("DEACTIVE_ID", deactiveId);
	    
	    hql.append(" ) SEL ");
	    hql.append(" ON(SEL.MAS_DEACTIVATE_TYPE_ID = MAC.MAS_DEACTIVATE_TYPE_ID) ");
	    hql.append(" WHERE MAC.RECORD_STATUS = 'N' ");
	    
	    hql.append(" AND MAC.LICENSE_TYPE = :TRADER_TYPE ");
	    parames.put("TRADER_TYPE", traderType);
	    
	    
	    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
    
		
	    sqlQuery.addScalar("MAS_DEACTIVATE_TYPE_ID", Hibernate.LONG); //0
	    sqlQuery.addScalar("DEACTIVATE_NAME", Hibernate.STRING); //1
	    sqlQuery.addScalar("LICENSE_TYPE", Hibernate.STRING); //2
	    sqlQuery.addScalar("DEACTIVE_ID", Hibernate.LONG);//3
	    
	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
	    
	    List<MasDeactivateTypeDTO> list = new ArrayList<MasDeactivateTypeDTO>();
	    
	    if(!result.isEmpty())
	    {
	    	for(Object[] obj: result)
	    	{
	    		MasDeactivateTypeDTO dto = new MasDeactivateTypeDTO();
	    		
	    		dto.setMasDeactivateTypeId(Long.valueOf(obj[0].toString()));
	    		dto.setDeactivateName(obj[1]==null?"":obj[1].toString());
	    		dto.setLicenseType(obj[2]==null?"":obj[2].toString());
	    		
	    		
	    		if(obj[3] !=null)
	    		{
	    			dto.setDeactiveId(Long.valueOf(obj[3].toString()));
	    			dto.setActive(true);
	    		}
	    		
	    		
	    		list.add(dto);
	    	}
	    }
  
	    return list;
	}
}
