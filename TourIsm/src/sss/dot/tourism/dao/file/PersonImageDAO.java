package sss.dot.tourism.dao.file;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.PersonImg;


@Repository("personImageDAO")
public class PersonImageDAO extends BaseDAO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PersonImageDAO() {
		this.domainObj = PersonImg.class; 
	}
	public List<PersonImg> findAllByTraderAndPersonImg(long traderId, long personImgId)
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  PersonImg as regd ");
		hql.append(" where regd.trader.traderId = ? ");
		hql.append(" and regd.personImgId = ? ");

		params.add(traderId);
		params.add(personImgId);

		return (List<PersonImg>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
	public List<PersonImg> findAllByTrader(long traderId)
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  PersonImg as regd ");
		hql.append(" where regd.trader.traderId = ? ");

		params.add(traderId);

		return (List<PersonImg>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
}
