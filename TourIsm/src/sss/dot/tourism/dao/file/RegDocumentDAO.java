package sss.dot.tourism.dao.file;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.RegDocument;
import sss.dot.tourism.domain.Registration;

@Repository("regDocumentDAO")
public class RegDocumentDAO extends BaseDAO{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1610360359920656534L;
	
	public RegDocumentDAO()
	{
		this.domainObj = RegDocument.class;
	}

	public List<RegDocument> findAllByTraderAndMasDoc(long traderId, long masDocId)
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  RegDocument as regd ");
		hql.append(" where regd.trader.traderId = ? ");
		hql.append(" and regd.masDocument.masDocId = ? ");
		hql.append(" order by regd.createDtm desc ");

		params.add(traderId);
		params.add(masDocId);

		return (List<RegDocument>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
	
}














