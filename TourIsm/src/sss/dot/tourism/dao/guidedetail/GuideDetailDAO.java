package sss.dot.tourism.dao.guidedetail;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.GuideDetail;
import sss.dot.tourism.dto.guidedetail.GuideDetailDTO;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.RecordStatus;

import com.sss.aut.service.User;

@Repository("guidedetailDAO")
public class GuideDetailDAO extends BaseDAO{
	private static final long serialVersionUID = -1610360359920656534L;
	
	public GuideDetailDAO()
	{
		this.domainObj = GuideDetail.class;
	}
	
	public List<Object[]> findGuideDetailPaging(GuideDetailDTO dto, User user ,int start, int limit)throws Exception
	{
		Map<String,Object> parames = new HashMap<String,Object>();
	    
	    StringBuilder hql = new StringBuilder();
	    
	    hql.append(" SELECT G.GUIDE_ID AS GUIDE_ID ");
	    hql.append(" ,G.PREFIX_ID AS PREFIX_ID ");
	    hql.append(" ,P.PREFIX_NAME AS PREFIX_NAME ");
	    hql.append(" ,G.FIRST_NAME AS FIRST_NAME ");
	    hql.append(" ,G.LAST_NAME AS LAST_NAME ");
	    hql.append(" ,G.LICENSE_NO AS LICENSE_NO ");
	    
	    hql.append(" ,G.FIRST_NAME_EN AS FIRST_NAME_EN ");
	    hql.append(" ,G.LAST_NAME_EN AS LAST_NAME_EN ");
	    hql.append(" ,G.BIRTH_DATE AS BIRTH_DATE ");
	    hql.append(" ,G.AGE_YEAR AS AGE_YEAR ");
	    hql.append(" ,G.EFFECTIVE_DATE AS EFFECTIVE_DATE ");
	    hql.append(" ,G.EXPIRE_DATE AS EXPIRE_DATE ");
	    
	    hql.append(" ,G.ADDRESS_NO AS ADDRESS_NO ");
	    hql.append(" ,G.VILLAGE_NAME AS VILLAGE_NAME ");
	    hql.append(" ,G.MOO AS MOO ");
	    hql.append(" ,G.SOI AS SOI ");
	    hql.append(" ,G.ROAD_NAME AS ROAD_NAME ");
	    hql.append(" ,G.TAMBOL_ID AS TAMBOL_ID ");
	    hql.append(" ,G.AMPHUR_ID AS AMPHUR_ID ");
	    hql.append(" ,G.PROVINCE_ID AS PROVINCE_ID ");
	    hql.append(" ,G.POST_CODE AS POST_CODE ");
	    hql.append(" ,G.TELEPHONE AS TELEPHONE ");
	    hql.append(" ,G.MOBILE_NO AS MOBILE_NO ");
	    hql.append(" ,G.FAX AS FAX ");
	    hql.append(" ,G.EMAIL AS EMAIL ");
	    
	    hql.append(" ,G.EDUCATION_LEVEL_NAME AS EDUCATION_LEVEL_NAME ");
	    hql.append(" ,G.AMATEUR_LEVEL AS AMATEUR_LEVEL ");
	    hql.append(" ,G.AMATEUR_LEVEL_NAME AS AMATEUR_LEVEL_NAME ");
	    hql.append(" ,G.EXPERIENCE_YEAR AS EXPERIENCE_YEAR ");
	    hql.append(" ,G.WORK_TYPE AS WORK_TYPE ");
	    hql.append(" ,G.WORK_TYPE_NAME AS WORK_TYPE_NAME ");
	    hql.append(" ,G.SALARY_NAME AS SALARY_NAME ");
	    hql.append(" ,G.CHARACTER_TYPE_NAME AS CHARACTER_TYPE_NAME ");
	    hql.append(" ,G.CLUB_NAME AS CLUB_NAME ");
	    hql.append(" ,G.REGISTER_DTM AS REGISTER_DTM ");
	    
	    hql.append(" FROM GUIDE_DETAIL G ");
	    hql.append(" LEFT JOIN GUIDE_FOREIGN_LANGUAGE GL ");
	    hql.append(" ON(G.GUIDE_ID = GL.GUIDE_DETAILGUIDE_ID) ");
	    hql.append(" LEFT JOIN MAS_PREFIX P ");
	    hql.append(" ON(G.PREFIX_ID = P.PREFIX_ID) ");
		
	    hql.append(" WHERE G.RECORD_STATUS = :GUIDE_RECORD_STATUS ");
	    parames.put("GUIDE_RECORD_STATUS", RecordStatus.NORMAL.getStatus());
	    
	    //LICENSE_NO
	    if((null != dto.getLicenseNo()) && (!"".equals(dto.getLicenseNo())))
	    {
		    hql.append(" AND G.LICENSE_NO LIKE :GUIDE_LICENSE_NO ");
		    parames.put("GUIDE_LICENSE_NO", "%"+dto.getLicenseNo()+"%");
	    }
	    //EXPIRE_DATE
	    if((null != dto.getExpireDate()) && (!dto.getExpireDate().equals("")))
	    {
	    	hql.append("  AND ( G.EXPIRE_DATE =  convert(datetime, :GUIDE_EXPIRE_DATE ,20) ) ");
	    	String guideExpireDate = DateUtils.getDateMSSQLFormat(dto.getExpireDate(), "00:00");
	    	parames.put("GUIDE_EXPIRE_DATE", guideExpireDate);
	    }
	    //FIRST_NAME
	    if((null != dto.getFirstName()) && (!"".equals(dto.getFirstName())))
	    {
		    hql.append(" AND G.FIRST_NAME LIKE :GUIDE_FIRST_NAME ");
		    parames.put("GUIDE_FIRST_NAME", "%"+dto.getFirstName()+"%");
	    }
	    //LAST_NAME
	    if((null != dto.getLastName()) && (!"".equals(dto.getLastName())))
	    {
		    hql.append(" AND G.LAST_NAME LIKE :GUIDE_LAST_NAME ");
		    parames.put("GUIDE_LAST_NAME", "%"+dto.getLastName()+"%");
	    }
	    //FOREIGN_LANG
	    if(dto.getCountryId() > 0)
	    {
	    	hql.append(" AND GL.FOREIGN_LANG = :GUIDE_FOREIGN_LANG ");
		    parames.put("GUIDE_FOREIGN_LANG", dto.getCountryId());
	    }
	    //LANGUAGE_LEVEL
	    if((null != dto.getLanguageLevel()) && (!"".equals(dto.getLanguageLevel())))
	    {
		    hql.append(" AND GL.LANGUAGE_LEVEL = :GUIDE_LANGUAGE_LEVEL ");
		    parames.put("GUIDE_LANGUAGE_LEVEL", dto.getLanguageLevel());
	    }
	    //AMATEUR_LEVEL
	    if((null != dto.getAmateurLevel()) && (!"".equals(dto.getAmateurLevel())))
	    {
		    hql.append(" AND G.AMATEUR_LEVEL = :GUIDE_AMATEUR_LEVEL ");
		    parames.put("GUIDE_AMATEUR_LEVEL", dto.getAmateurLevel());
	    }
	    //WORK_TYPE
	    if((null != dto.getWorkType()) && (!"".equals(dto.getWorkType())))
	    {
		    hql.append(" AND G.WORK_TYPE = :GUIDE_WORK_TYPE ");
		    parames.put("GUIDE_WORK_TYPE", dto.getWorkType());
	    }
	    
	    hql.append(" GROUP BY G.GUIDE_ID, G.PREFIX_ID, P.PREFIX_NAME , G.FIRST_NAME, G.LAST_NAME, G.LICENSE_NO ");
	    hql.append(" ,G.FIRST_NAME_EN, G.LAST_NAME_EN, G.BIRTH_DATE, G.AGE_YEAR, G.EFFECTIVE_DATE, G.EXPIRE_DATE ");
	    hql.append(" ,G.ADDRESS_NO, G.VILLAGE_NAME, G.MOO, G.SOI, G.ROAD_NAME, G.TAMBOL_ID, G.AMPHUR_ID, G.PROVINCE_ID ");
	    hql.append(" ,G.POST_CODE, G.TELEPHONE, G.MOBILE_NO, G.FAX, G.EMAIL, G.EDUCATION_LEVEL_NAME, G.AMATEUR_LEVEL ");
	    hql.append(" ,G.AMATEUR_LEVEL_NAME, G.EXPERIENCE_YEAR, G.WORK_TYPE, G.WORK_TYPE_NAME, G.SALARY_NAME, G.CHARACTER_TYPE_NAME, G.CLUB_NAME, G.REGISTER_DTM ");
	    
	    System.out.println("#####hql.toString() = "+hql.toString());
		
	    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
	    sqlQuery.setFirstResult(start);
	    sqlQuery.setMaxResults(limit);
	    
		sqlQuery.addScalar("GUIDE_ID", Hibernate.LONG); //0
		sqlQuery.addScalar("PREFIX_ID", Hibernate.LONG);//1
	    sqlQuery.addScalar("PREFIX_NAME", Hibernate.STRING);//2
	    sqlQuery.addScalar("FIRST_NAME", Hibernate.STRING);//3
	    sqlQuery.addScalar("LAST_NAME", Hibernate.STRING);//4
	    sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING);//5
	    
	    sqlQuery.addScalar("FIRST_NAME_EN", Hibernate.STRING);//6
	    sqlQuery.addScalar("LAST_NAME_EN", Hibernate.STRING);//7
	    sqlQuery.addScalar("BIRTH_DATE", Hibernate.DATE);//8
	    sqlQuery.addScalar("AGE_YEAR", Hibernate.LONG);//9
	    sqlQuery.addScalar("EFFECTIVE_DATE", Hibernate.DATE);//10
	    sqlQuery.addScalar("EXPIRE_DATE", Hibernate.DATE);//11

	    sqlQuery.addScalar("ADDRESS_NO", Hibernate.STRING);//12
	    sqlQuery.addScalar("VILLAGE_NAME", Hibernate.STRING);//13
	    sqlQuery.addScalar("MOO", Hibernate.STRING);//14
	    sqlQuery.addScalar("SOI", Hibernate.STRING);//15
	    sqlQuery.addScalar("ROAD_NAME", Hibernate.STRING);//16
	    sqlQuery.addScalar("TAMBOL_ID", Hibernate.LONG);//17
	    sqlQuery.addScalar("AMPHUR_ID", Hibernate.LONG);//18
	    sqlQuery.addScalar("PROVINCE_ID", Hibernate.LONG);//19
	    sqlQuery.addScalar("POST_CODE", Hibernate.STRING);//20
	    sqlQuery.addScalar("TELEPHONE", Hibernate.STRING);//21
	    sqlQuery.addScalar("MOBILE_NO", Hibernate.STRING);//22
	    sqlQuery.addScalar("FAX", Hibernate.STRING);//23
	    sqlQuery.addScalar("EMAIL", Hibernate.STRING);//24
	    
	    sqlQuery.addScalar("EDUCATION_LEVEL_NAME", Hibernate.STRING);//25
	    sqlQuery.addScalar("AMATEUR_LEVEL", Hibernate.STRING);//26
	    sqlQuery.addScalar("AMATEUR_LEVEL_NAME", Hibernate.STRING);//27
	    sqlQuery.addScalar("EXPERIENCE_YEAR", Hibernate.STRING);//28
	    sqlQuery.addScalar("WORK_TYPE", Hibernate.STRING);//29
	    sqlQuery.addScalar("WORK_TYPE_NAME", Hibernate.STRING);//30
	    sqlQuery.addScalar("SALARY_NAME", Hibernate.STRING);//31
	    sqlQuery.addScalar("CHARACTER_TYPE_NAME", Hibernate.STRING);//32
	    sqlQuery.addScalar("CLUB_NAME", Hibernate.STRING);//33
	    sqlQuery.addScalar("REGISTER_DTM", Hibernate.DATE);//34
	    
	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
  
	    return result;
	}
	
	
	
}










