package sss.dot.tourism.dao.guidedetail;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.ForeignLanguage;
import sss.dot.tourism.domain.GuideForeignLanguage;
import sss.dot.tourism.dto.guidedetail.GuideForeignLanguageDTO;
import sss.dot.tourism.util.RecordStatus;

@Repository("guideForeignLanguageDAO")
public class GuideForeignLanguageDAO extends BaseDAO{
	private static final long serialVersionUID = -1610360359920656534L;
	
	public GuideForeignLanguageDAO()
	{
		this.domainObj = GuideForeignLanguage.class;
	}
	
	@SuppressWarnings("unchecked")
	public List<GuideForeignLanguage> findGuideForeignLanguageAll(GuideForeignLanguageDTO dto)throws Exception 
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  GuideForeignLanguage as gf ");
		hql.append(" where gf.recordStatus = ? ");
		params.add(RecordStatus.NORMAL.getStatus());
		
		if (dto.getGuideId() > 0) {
			hql.append(" and gf.guideDetail.guideId = ? ");
			params.add(dto.getGuideId());
		}

		List<GuideForeignLanguage> list = (List<GuideForeignLanguage>) this.getHibernateTemplate().find(hql.toString(), params.toArray());

		return list;
	}
	
	
}














