package sss.dot.tourism.dao.info;

import java.util.List;

import com.sss.aut.service.User;

public interface IInfoService {
	public List getInfoPaging(Object object, User user ,int start, int limit) throws Exception;
	public List countInfoPaging(Object object, User user ,int start, int limit) throws Exception;
	
	public List getReoeganizePaging(Object object, User user ,int start, int limit) throws Exception;
	public List countReoeganizePaging(Object object, User user ,int start, int limit) throws Exception;
	
	
	public void saveReorganizeLicense(Object object, User user ) throws Exception;
}
