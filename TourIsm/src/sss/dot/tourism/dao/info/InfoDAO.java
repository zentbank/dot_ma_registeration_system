package sss.dot.tourism.dao.info;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.Registration;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.util.DateUtils;

import com.sss.aut.service.User;

@Repository("infoDAO")
public class InfoDAO extends BaseDAO{
	private static final long serialVersionUID = -6455075565457388105L;
	
	public InfoDAO()
	{
		this.domainObj = Registration.class;
	}
	
	public List<Object[]> findInfoPaging(RegistrationDTO dto, User user ,int start, int limit, boolean reorganize)throws Exception
	{
		Map<String,Object> parames = new HashMap<String,Object>();
	    
	    StringBuilder hql = new StringBuilder();
	
	    hql.append(" SELECT RE.REG_ID AS REG_ID, ");
	    hql.append(" RE.REGISTRATION_NO AS REGISTRATION_NO, ");
	    hql.append(" RE.REGISTRATION_DATE AS REGISTRATION_DATE, ");
	    hql.append(" RE.REGISTRATION_PROGRESS AS REGISTRATION_PROGRESS, ");
	    hql.append(" RE.REGISTRATION_TYPE AS REGISTRATION_TYPE, ");
	    hql.append(" RE.ORG_ID AS ORG_ID, ");
	    hql.append(" RE.RECORD_STATUS AS REG_RECORD_STATUS, ");
	    hql.append(" TR.TRADER_ID AS TRADER_ID, ");
	    hql.append(" TR.TRADER_TYPE AS TRADER_TYPE, ");
	    hql.append(" TR.TRADER_NAME AS TRADER_NAME, ");
	    hql.append(" TR.TRADER_NAME_EN AS TRADER_NAME_EN, ");
	    hql.append(" TR.TRADER_CATEGORY AS TRADER_CATEGORY, ");
	    hql.append(" PE.PERSON_ID AS PERSON_ID, ");
	    hql.append(" PE.PERSON_TYPE AS PERSON_TYPE, ");
	    hql.append(" MP.PREFIX_NAME AS PREFIX_NAME, ");
	    hql.append(" PE.FIRST_NAME AS FIRST_NAME, ");
	    hql.append(" PE.LAST_NAME AS LAST_NAME, ");
	    hql.append(" POSF.POSTFIX_NAME AS POSTFIX_NAME, ");
	    hql.append(" MP.PREFIX_ID AS PREFIX_ID, ");
	    hql.append(" POSF.POSTFIX_ID AS POSTFIX_ID, ");
	    hql.append(" PEAMP.AMPHUR_ID AS PERSON_AMPHUR_ID, ");
	    hql.append(" TAXPEAMP.AMPHUR_ID AS PERSON_TAXAMPHUR_ID, ");
	    hql.append(" PEPROV.PROVINCE_ID AS PERSON_PROVINCE_ID, ");
	    hql.append(" TAXPEPROV.PROVINCE_ID AS PERSON_TAXPROVINCE_ID, ");
	    
		
	    hql.append(" TR.TRADER_GUIDE_ID AS TRADER_GUIDE_ID, ");
	    hql.append(" TR.PRONUNCIATION_NAME AS PRONUNCIATION_NAME, ");
	    hql.append(" TR.LICENSE_NO AS LICENSE_NO, ");
	    hql.append(" PE.PERSON_TYPE AS PERSON_TYPE, ");
	    hql.append(" PE.IDENTITY_NO AS IDENTITY_NO, ");
	    hql.append(" PE.COMMITTEE_NAME1 AS COMMITTEE_NAME1, ");
	    hql.append(" PE.COMMITTEE_NAME2 AS COMMITTEE_NAME2, ");
	    hql.append(" PE.COMMITTEE_NAME_SIGN AS COMMITTEE_NAME_SIGN, ");
	    hql.append(" PE.PASSPORT_NO AS PASSPORT_NO, ");
	    hql.append(" PE.FIRST_NAME_EN AS FIRST_NAME_EN, ");
	    hql.append(" PE.LAST_NAME_EN AS LAST_NAME_EN, ");
	    hql.append(" PE.GENDER AS GENDER, ");
	    hql.append(" PE.PERSON_NATIONALITY AS PERSON_NATIONALITY, ");
	    hql.append(" PE.BIRTH_DATE AS BIRTH_DATE, ");
	    hql.append(" PE.AGE_YEAR AS AGE_YEAR, ");
	    hql.append(" PE.IDENTITY_NO_EXPIRE AS IDENTITY_NO_EXPIRE, ");
	    hql.append(" PE.CORPORATE_TYPE AS CORPORATE_TYPE, ");
	    hql.append(" PE.TAX_IDENTITY_NO AS TAX_IDENTITY_NO, ");
	    hql.append(" PE.IDENTITY_DATE AS IDENTITY_DATE, ");
	    hql.append(" TR.RECORD_STATUS AS TRADER_RECORD_STATUS, ");
	    
	    hql.append(" PEAMP.AMPHUR_NAME AS PERSON_AMPHUR_NAME, ");
	    hql.append(" TAXPEAMP.AMPHUR_NAME AS PERSON_TAXAMPHUR_NAME, ");
	    hql.append(" PEPROV.PROVINCE_NAME AS PERSON_PROVINCE_NAME, ");
	    hql.append(" TAXPEPROV.PROVINCE_NAME AS PERSON_TAXPROVINCE_NAME, ");
	    hql.append(" MP.PREFIX_NAME_EN AS PREFIX_NAME_EN ");
	    
	    //APPROVE_DATE
	    hql.append(" ,RE.APPROVE_DATE AS APPROVE_DATE ");
	    
	    //EFFECTIVE_DATE
	    hql.append(" ,TR.EFFECTIVE_DATE AS TRADER_EFFECTIVE_DATE ");
	    
	    //EXPIRE_DATE
	    hql.append(" ,TR.EXPIRE_DATE AS TRADER_EXPIRE_DATE ");
	    
	    //LICENSE_STATUS
	    hql.append(" ,TR.LICENSE_STATUS AS TRADER_LICENSE_STATUS ");
	    
	    hql.append(" ,PE.IMAGE_FILE AS IMAGE_FILE ");
	    hql.append(" FROM REGISTRATION RE ");
	    hql.append(" INNER JOIN TRADER TR ");
	    hql.append(" ON(RE.TRADER_ID = TR.TRADER_ID) ");
	 
	    hql.append(" INNER JOIN PERSON PE ");
	    hql.append(" ON(PE.PERSON_ID = TR.PERSON_ID) ");

	    hql.append(" LEFT JOIN MAS_PREFIX MP ");
	    hql.append(" ON(PE.PREFIX_ID = MP.PREFIX_ID) ");

	    hql.append(" LEFT JOIN MAS_POSFIX POSF ");
	    hql.append(" ON(POSF.POSTFIX_ID = PE.POSTFIX_ID) ");
	    hql.append(" LEFT JOIN MAS_AMPHUR PEAMP ");
	    hql.append(" ON(PEAMP.AMPHUR_ID = PE.AMPHUR_ID) ");

	    hql.append(" LEFT JOIN MAS_AMPHUR TAXPEAMP ");
	    hql.append(" ON(TAXPEAMP.AMPHUR_ID = PE.TAX_AMPHUR_ID) ");

	    hql.append(" LEFT JOIN MAS_PROVINCE PEPROV ");
	    hql.append(" ON(PEPROV.PROVINCE_ID = PE.PROVINCE_ID) ");

	    hql.append(" LEFT JOIN MAS_PROVINCE TAXPEPROV ");
	    hql.append(" ON(TAXPEPROV.PROVINCE_ID = PE.TAX_PROVINCE_ID) ");
	    
	    //Where
	    hql.append(" WHERE  TR.BRANCH_TYPE IS NULL ");
	    
	    if(!reorganize)
	    {
	    	//Oat Edit 17/09/57
	    	hql.append(" AND TR.RECORD_STATUS IN ('N','H','T') ");
		    hql.append(" AND RE.RECORD_STATUS IN ('N','H','T') ");
//	    	hql.append(" AND TR.RECORD_STATUS IN ('N','H') ");
//		    hql.append(" AND RE.RECORD_STATUS IN ('N','H') ");
		    //End Oat Edit 17/09/57
	    }
	    else
	    {
	    	//Oat Edit 17/06/58
	    	hql.append(" AND TR.RECORD_STATUS IN ('N','H') ");
		    hql.append(" AND RE.RECORD_STATUS IN ('N','H') ");
//	    	hql.append(" AND TR.RECORD_STATUS = 'N' ");
//		    hql.append(" AND RE.RECORD_STATUS = 'N' ");
		    //End Oat Edit 17/09/57

	    }
	    
	    
	    
	    //PERSON_TYPE
	    hql.append(" AND PE.PERSON_TYPE IS NOT NULL ");

	    //TraderType B G L
	    if((null != dto.getTraderType()) && (!"".equals(dto.getTraderType())))
	    {
	    	 hql.append(" AND TR.TRADER_TYPE = :TRADER_TRADER_TYPE ");
			    
			 parames.put("TRADER_TRADER_TYPE", dto.getTraderType());
	    }
	    
	  //licenseNo เลขที่ใบอนุญาต
	    if((null != dto.getLicenseNo()) && (!"".equals(dto.getLicenseNo())))
	    {
	    	 hql.append(" AND TR.LICENSE_NO LIKE :TRADER_LICENSE_NO ");
			    
			 parames.put("TRADER_LICENSE_NO", "%"+dto.getLicenseNo()+"%");
	    }
	    
	    //TRADER_NAME
	    if((null != dto.getTraderName()) && (!"".equals(dto.getTraderName())))
	    {
		    hql.append(" AND TR.TRADER_NAME LIKE :TRADER_TRADER_NAME ");
		    
		    parames.put("TRADER_TRADER_NAME", "%"+dto.getTraderName()+"%");
	    }
	    
	    //TRADER_NAME_EN
	    if((null != dto.getTraderNameEn()) && (!"".equals(dto.getTraderNameEn())))
	    {
		    hql.append(" AND TR.TRADER_NAME_EN LIKE :TRADER_TRADER_NAME_EN ");
		    
		    parames.put("TRADER_TRADER_NAME_EN", "%"+dto.getTraderNameEn()+"%");
	    }
	    
	    //FIRST_NAME
	    if((null != dto.getFirstName()) && (!"".equals(dto.getFirstName())))
	    {
		    hql.append(" AND PE.FIRST_NAME LIKE :PERSON_FIRST_NAME ");
		    
		    parames.put("PERSON_FIRST_NAME", "%"+dto.getFirstName()+"%");
	    }
	    
	    //IDENTITY_NO
	    if(dto.getIdentityNo() != null && !dto.getIdentityNo().equals(""))
	    {
		    hql.append(" AND PE.IDENTITY_NO = :PERSON_IDENTITY_NO ");
		    parames.put("PERSON_IDENTITY_NO", dto.getIdentityNo());
	    }
	    
	    //LICENSE_STATUS สถานะใบอนุญาต
	    if((null != dto.getLicenseStatus()) && (!"".equals(dto.getLicenseStatus())))
	    {
		    hql.append(" AND TR.LICENSE_STATUS = :TRADER_LICENSE_STATUS ");
		    
		    parames.put("TRADER_LICENSE_STATUS", dto.getLicenseStatus());
	    }
	    
	    //OrgId ภูมิภาค
	    if(dto.getOrgId()>0)
	    {
	    	hql.append(" AND RE.ORG_ID = :REGISTRATION_ORG_ID ");
	    	parames.put("REGISTRATION_ORG_ID", dto.getOrgId());
	    }
	    
//BUSINESS--
	    
	    //TRADER_CATEGORY
	    if((null != dto.getTraderCategory()) && (!"".equals(dto.getTraderCategory())))
	    {
	    	 hql.append(" AND TR.TRADER_CATEGORY = :TRADER_TRADER_CATEGORY ");
			    
			 parames.put("TRADER_TRADER_CATEGORY", dto.getTraderCategory());
	    }
	    
	    //GUIDE--
//	    //REGISTRATION_NO
//	    if((null != dto.getRegistrationNo()) && (!"".equals(dto.getRegistrationNo())))
//	    {
//		    hql.append(" AND RE.REGISTRATION_NO = :REGISTRATION_REGISTRATION_NO ");
//		    
//		    parames.put("REGISTRATION_REGISTRATION_NO", dto.getRegistrationNo());
//	    }
	    
	    //LASTNAME
	    if((null != dto.getLastName()) && (!"".equals(dto.getLastName())))
	    {
		    hql.append(" AND PE.LAST_NAME LIKE :PERSON_LAST_NAME ");
		    
		    parames.put("PERSON_LAST_NAME", "%"+dto.getLastName()+"%");
	    }
	    
	    //FIRST_NAME_EN
	    if((null != dto.getFirstNameEn()) && (!"".equals(dto.getFirstNameEn())))
	    {
		    hql.append(" AND PE.FIRST_NAME_EN LIKE :PERSON_FIRST_NAME_EN ");
		    
		    parames.put("PERSON_FIRST_NAME_EN", "%"+dto.getFirstNameEn()+"%");
	    }
	    
	    //LASTNAME_EN
	    if((null != dto.getLastNameEn()) && (!"".equals(dto.getLastNameEn())))
	    {
		    hql.append(" AND PE.LAST_NAME_EN LIKE :PERSON_LAST_NAME_EN ");
		    
		    parames.put("PERSON_LAST_NAME_EN", "%"+dto.getLastNameEn()+"%");
	    }

	    //registrationType
	    if((null != dto.getRegistrationType()) && (!"".equals(dto.getRegistrationType())))
	    {
	    	hql.append(" AND RE.REGISTRATION_TYPE = :REGISTRATION_REGISTRATION_TYPE ");
		    
		    parames.put("REGISTRATION_REGISTRATION_TYPE", dto.getRegistrationType());
	    }
	    
	    hql.append(" ORDER BY TR.LICENSE_NO ,TR.EFFECTIVE_DATE");
	    
	    //End Where
	    
	    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
	    sqlQuery.setFirstResult(start);
	    sqlQuery.setMaxResults(limit);
	    
		sqlQuery.addScalar("REG_ID", Hibernate.LONG); //0
		sqlQuery.addScalar("REGISTRATION_NO", Hibernate.STRING); //1
	    sqlQuery.addScalar("REGISTRATION_DATE", Hibernate.DATE); //2
	    sqlQuery.addScalar("REGISTRATION_PROGRESS", Hibernate.STRING); //3
	    sqlQuery.addScalar("REGISTRATION_TYPE", Hibernate.STRING);//4 
	    sqlQuery.addScalar("ORG_ID", Hibernate.LONG);//5
	    sqlQuery.addScalar("REG_RECORD_STATUS", Hibernate.STRING);//6
	    sqlQuery.addScalar("TRADER_ID", Hibernate.LONG); //7
	    sqlQuery.addScalar("TRADER_TYPE", Hibernate.STRING);//8
	    sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING);//9
	    sqlQuery.addScalar("TRADER_NAME_EN", Hibernate.STRING);//10
	    sqlQuery.addScalar("TRADER_CATEGORY", Hibernate.STRING);//11
	    sqlQuery.addScalar("PERSON_ID", Hibernate.LONG);//12
	    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);//13
	    sqlQuery.addScalar("PREFIX_NAME", Hibernate.STRING);//14
	    sqlQuery.addScalar("FIRST_NAME", Hibernate.STRING);//15
	    sqlQuery.addScalar("LAST_NAME", Hibernate.STRING);//16
	    sqlQuery.addScalar("POSTFIX_NAME", Hibernate.STRING);//17
//	   
	    sqlQuery.addScalar("PREFIX_ID", Hibernate.LONG);//18
	    sqlQuery.addScalar("POSTFIX_ID", Hibernate.LONG);//19
	    sqlQuery.addScalar("PERSON_AMPHUR_ID", Hibernate.LONG);//20
	    sqlQuery.addScalar("PERSON_TAXAMPHUR_ID", Hibernate.LONG);//21
	    sqlQuery.addScalar("PERSON_PROVINCE_ID", Hibernate.LONG);//22
	    sqlQuery.addScalar("PERSON_TAXPROVINCE_ID", Hibernate.LONG);//23
	    
	    sqlQuery.addScalar("TRADER_GUIDE_ID", Hibernate.LONG);//24
	    sqlQuery.addScalar("PRONUNCIATION_NAME", Hibernate.STRING);//25
	    sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING);//26
	    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);//27
	    sqlQuery.addScalar("IDENTITY_NO", Hibernate.STRING);//28
	    sqlQuery.addScalar("COMMITTEE_NAME1", Hibernate.STRING);//29
	    sqlQuery.addScalar("COMMITTEE_NAME2", Hibernate.STRING);//30
	    sqlQuery.addScalar("COMMITTEE_NAME_SIGN", Hibernate.STRING);//31
	    sqlQuery.addScalar("PASSPORT_NO", Hibernate.STRING);//32
	    sqlQuery.addScalar("FIRST_NAME_EN", Hibernate.STRING);//33
	    sqlQuery.addScalar("LAST_NAME_EN", Hibernate.STRING);//34
	    sqlQuery.addScalar("GENDER", Hibernate.STRING);//35
	    sqlQuery.addScalar("PERSON_NATIONALITY", Hibernate.STRING);//36
	    sqlQuery.addScalar("BIRTH_DATE", Hibernate.DATE);//37
	    sqlQuery.addScalar("AGE_YEAR", Hibernate.STRING);//38
	    sqlQuery.addScalar("IDENTITY_NO_EXPIRE", Hibernate.DATE);//39
	    sqlQuery.addScalar("CORPORATE_TYPE", Hibernate.STRING);//40
	    sqlQuery.addScalar("TAX_IDENTITY_NO", Hibernate.STRING);//41
	    sqlQuery.addScalar("IDENTITY_DATE", Hibernate.DATE);//42
	    sqlQuery.addScalar("TRADER_RECORD_STATUS", Hibernate.STRING);//43
	    
	    sqlQuery.addScalar("PERSON_AMPHUR_NAME", Hibernate.STRING);//44
	    sqlQuery.addScalar("PERSON_TAXAMPHUR_NAME", Hibernate.STRING);//45
	    sqlQuery.addScalar("PERSON_PROVINCE_NAME", Hibernate.STRING);//46
	    sqlQuery.addScalar("PERSON_TAXPROVINCE_NAME", Hibernate.STRING);//47
	    sqlQuery.addScalar("PREFIX_NAME_EN", Hibernate.STRING);//48
	    sqlQuery.addScalar("APPROVE_DATE", Hibernate.DATE);//49
	    
	    sqlQuery.addScalar("TRADER_EFFECTIVE_DATE", Hibernate.DATE);//50
	    sqlQuery.addScalar("TRADER_EXPIRE_DATE", Hibernate.DATE);//51
	    sqlQuery.addScalar("TRADER_LICENSE_STATUS", Hibernate.STRING);//52
	    sqlQuery.addScalar("IMAGE_FILE", Hibernate.STRING);//53
	    

	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
  
	    return result;
	}
	
	//Oat Add 13/11/57
	public List<Object[]> findInfoCheckDueDateFeeByPaging(RegistrationDTO dto, User user ,int start, int limit)throws Exception
	{
		Map<String,Object> parames = new HashMap<String,Object>();
	    
	    StringBuilder hql = new StringBuilder();
	
	    hql.append(" SELECT RE.REG_ID AS REG_ID, ");
	    hql.append(" RE.REGISTRATION_NO AS REGISTRATION_NO, ");
	    hql.append(" RE.REGISTRATION_DATE AS REGISTRATION_DATE, ");
	    hql.append(" RE.REGISTRATION_PROGRESS AS REGISTRATION_PROGRESS, ");
	    hql.append(" RE.REGISTRATION_TYPE AS REGISTRATION_TYPE, ");
	    hql.append(" RE.ORG_ID AS ORG_ID, ");
	    hql.append(" RE.RECORD_STATUS AS REG_RECORD_STATUS, ");
	    hql.append(" TR.TRADER_ID AS TRADER_ID, ");
	    hql.append(" TR.TRADER_TYPE AS TRADER_TYPE, ");
	    hql.append(" TR.TRADER_NAME AS TRADER_NAME, ");
	    hql.append(" TR.TRADER_NAME_EN AS TRADER_NAME_EN, ");
	    hql.append(" TR.TRADER_CATEGORY AS TRADER_CATEGORY, ");
	    hql.append(" PE.PERSON_ID AS PERSON_ID, ");
	    hql.append(" PE.PERSON_TYPE AS PERSON_TYPE, ");
	    hql.append(" MP.PREFIX_NAME AS PREFIX_NAME, ");
	    hql.append(" PE.FIRST_NAME AS FIRST_NAME, ");
	    hql.append(" PE.LAST_NAME AS LAST_NAME, ");
	    hql.append(" POSF.POSTFIX_NAME AS POSTFIX_NAME, ");
	    hql.append(" MP.PREFIX_ID AS PREFIX_ID, ");
	    hql.append(" POSF.POSTFIX_ID AS POSTFIX_ID, ");
	    hql.append(" PEAMP.AMPHUR_ID AS PERSON_AMPHUR_ID, ");
	    hql.append(" TAXPEAMP.AMPHUR_ID AS PERSON_TAXAMPHUR_ID, ");
	    hql.append(" PEPROV.PROVINCE_ID AS PERSON_PROVINCE_ID, ");
	    hql.append(" TAXPEPROV.PROVINCE_ID AS PERSON_TAXPROVINCE_ID, ");
	    
		
	    hql.append(" TR.TRADER_GUIDE_ID AS TRADER_GUIDE_ID, ");
	    hql.append(" TR.PRONUNCIATION_NAME AS PRONUNCIATION_NAME, ");
	    hql.append(" TR.LICENSE_NO AS LICENSE_NO, ");
	    hql.append(" PE.PERSON_TYPE AS PERSON_TYPE, ");
	    hql.append(" PE.IDENTITY_NO AS IDENTITY_NO, ");
	    hql.append(" PE.COMMITTEE_NAME1 AS COMMITTEE_NAME1, ");
	    hql.append(" PE.COMMITTEE_NAME2 AS COMMITTEE_NAME2, ");
	    hql.append(" PE.COMMITTEE_NAME_SIGN AS COMMITTEE_NAME_SIGN, ");
	    hql.append(" PE.PASSPORT_NO AS PASSPORT_NO, ");
	    hql.append(" PE.FIRST_NAME_EN AS FIRST_NAME_EN, ");
	    hql.append(" PE.LAST_NAME_EN AS LAST_NAME_EN, ");
	    hql.append(" PE.GENDER AS GENDER, ");
	    hql.append(" PE.PERSON_NATIONALITY AS PERSON_NATIONALITY, ");
	    hql.append(" PE.BIRTH_DATE AS BIRTH_DATE, ");
	    hql.append(" PE.AGE_YEAR AS AGE_YEAR, ");
	    hql.append(" PE.IDENTITY_NO_EXPIRE AS IDENTITY_NO_EXPIRE, ");
	    hql.append(" PE.CORPORATE_TYPE AS CORPORATE_TYPE, ");
	    hql.append(" PE.TAX_IDENTITY_NO AS TAX_IDENTITY_NO, ");
	    hql.append(" PE.IDENTITY_DATE AS IDENTITY_DATE, ");
	    hql.append(" TR.RECORD_STATUS AS TRADER_RECORD_STATUS, ");
	    
	    hql.append(" PEAMP.AMPHUR_NAME AS PERSON_AMPHUR_NAME, ");
	    hql.append(" TAXPEAMP.AMPHUR_NAME AS PERSON_TAXAMPHUR_NAME, ");
	    hql.append(" PEPROV.PROVINCE_NAME AS PERSON_PROVINCE_NAME, ");
	    hql.append(" TAXPEPROV.PROVINCE_NAME AS PERSON_TAXPROVINCE_NAME, ");
	    hql.append(" MP.PREFIX_NAME_EN AS PREFIX_NAME_EN ");
	    
	    //APPROVE_DATE
	    hql.append(" ,RE.APPROVE_DATE AS APPROVE_DATE ");
	    
	    //EFFECTIVE_DATE
	    hql.append(" ,TR.EFFECTIVE_DATE AS TRADER_EFFECTIVE_DATE ");
	    
	    //EXPIRE_DATE
	    hql.append(" ,TR.EXPIRE_DATE AS TRADER_EXPIRE_DATE ");
	    
	    //LICENSE_STATUS
	    hql.append(" ,TR.LICENSE_STATUS AS TRADER_LICENSE_STATUS ");
	    
	    //Oat Add 25/06/58
	    hql.append(" ,'เลขที่ ' + ISNULL ( TA.ADDRESS_NO , '' ) +  ISNULL ( ' อาคาร ' + TA.BUILDING_NAME , '' )+ ISNULL ( ' ชั้น ' + TA.FLOOR , '' ) + ISNULL (' หมู่ที่ ' + TA.MOO , '' ) ");
		hql.append(" + ISNULL (' ซอย ' + TA.SOI , '' )+ ISNULL (' ถนน ' + TA.ROAD_NAME , '' ) + ISNULL ( ' ตำบล/แขวง ' + MT.TAMBOL_NAME , '' ) ");
		hql.append(" + ISNULL (' อำเภอ/เขต ' + MA.AMPHUR_NAME, '' ) + ISNULL (' จังหวัด ' + P.PROVINCE_NAME, '' )+ ISNULL (' รหัสไปรษณีย์ ' + TA.POST_CODE, '' ) AS ADDRESS ");
	    //
		
	    hql.append(" FROM REGISTRATION RE ");
	    hql.append(" INNER JOIN TRADER TR ");
	    hql.append(" ON(RE.TRADER_ID = TR.TRADER_ID) ");
	 
	    hql.append(" INNER JOIN PERSON PE ");
	    hql.append(" ON(PE.PERSON_ID = TR.PERSON_ID) ");
	    
//Oat Add 29/05/58
		 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
		 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
		 
		 	//25/06/58
		    hql.append(" LEFT JOIN MAS_AMPHUR MA ");
			hql.append(" ON(MA.AMPHUR_ID = TA.AMPHUR_ID) ");
			hql.append(" LEFT JOIN MAS_TAMBOL MT ");
			hql.append(" ON(MT.TAMBOL_ID = TA.TAMBOL_ID) ");
//End Oat Add 29/05/58

	    hql.append(" LEFT JOIN MAS_PREFIX MP ");
	    hql.append(" ON(PE.PREFIX_ID = MP.PREFIX_ID) ");

	    hql.append(" LEFT JOIN MAS_POSFIX POSF ");
	    hql.append(" ON(POSF.POSTFIX_ID = PE.POSTFIX_ID) ");
	    hql.append(" LEFT JOIN MAS_AMPHUR PEAMP ");
	    hql.append(" ON(PEAMP.AMPHUR_ID = PE.AMPHUR_ID) ");

	    hql.append(" LEFT JOIN MAS_AMPHUR TAXPEAMP ");
	    hql.append(" ON(TAXPEAMP.AMPHUR_ID = PE.TAX_AMPHUR_ID) ");

	    hql.append(" LEFT JOIN MAS_PROVINCE PEPROV ");
	    hql.append(" ON(PEPROV.PROVINCE_ID = PE.PROVINCE_ID) ");

	    hql.append(" LEFT JOIN MAS_PROVINCE TAXPEPROV ");
	    hql.append(" ON(TAXPEPROV.PROVINCE_ID = PE.TAX_PROVINCE_ID) ");
	    
	    //WHERE
	    hql.append(" WHERE  TR.BRANCH_TYPE IS NULL ");
	    
	    //RECORD_STATUS
	    hql.append(" AND TR.RECORD_STATUS = 'N' ");
		hql.append(" AND RE.RECORD_STATUS = 'N' ");
	    	    
	    //PERSON_TYPE
	    hql.append(" AND PE.PERSON_TYPE IS NOT NULL ");
	    
	    //TraderType B G L
	    if((null != dto.getTraderType()) && (!"".equals(dto.getTraderType())))
	    {
	    	 hql.append(" AND TR.TRADER_TYPE = :TRADER_TRADER_TYPE ");
			    
			 parames.put("TRADER_TRADER_TYPE", dto.getTraderType());
	    }
	    
//	  //licenseNo เลขที่ใบอนุญาต
//	    if((null != dto.getLicenseNo()) && (!"".equals(dto.getLicenseNo())))
//	    {
//	    	 hql.append(" AND TR.LICENSE_NO LIKE :TRADER_LICENSE_NO ");
//			    
//			 parames.put("TRADER_LICENSE_NO", "%"+dto.getLicenseNo()+"%");
//	    }
	    
	    //EXPIRE_DATE
	    if((null != dto.getExpireDateFrom()) && (!dto.getExpireDateFrom().equals("")))
	    {
	    	if((null != dto.getExpireDateTo()) && (!dto.getExpireDateTo().equals("")))
	    	{
				hql.append("  and ( TR.EXPIRE_DATE >=  convert(datetime, :TR_START_DATE ,20) ");
				hql.append(" and TR.EXPIRE_DATE <=  convert(datetime, :TR_END_DATE ,20) ) ");
				
				String expireDtmFrom = DateUtils.getDateMSSQLFormat(dto.getExpireDateFrom(), "00:00");
				String expireDtmTo = DateUtils.getDateMSSQLFormat(dto.getExpireDateTo(), "23:00");
					
				parames.put("TR_START_DATE", expireDtmFrom);
				parames.put("TR_END_DATE", expireDtmTo);
				
				System.out.println("#####expireDtmFrom = "+expireDtmFrom);
				System.out.println("#####expireDtmTo = "+expireDtmTo);
				
	    	}else
	    	{
	    		hql.append("  and ( TR.EXPIRE_DATE >=  convert(datetime, :TR_START_DATE ,20) )");
				String expireDtmFrom = DateUtils.getDateMSSQLFormat(dto.getExpireDateFrom(), "00:00");
				parames.put("TR_START_DATE", expireDtmFrom);
				
//	    		hql.append("  and ( TR.EXPIRE_DATE >=  convert(datetime, :TR_START_DATE ,20)");
//				hql.append(" and TR.EXPIRE_DATE <=  convert(datetime, :TR_END_DATE ,20) ) ");
//				
//				String expireDtmFrom = DateUtils.getDateMSSQLFormat(dto.getExpireDateFrom(), "00:00");
//				String expireDtmTo = DateUtils.getDateMSSQLFormat(dto.getExpireDateFrom(), "23:00");
//					
//				parames.put("TR_START_DATE", expireDtmFrom);
//				parames.put("TR_END_DATE", expireDtmTo);
	    	}
	    }
	    
	    //LICENSE_STATUS สถานะใบอนุญาต
		hql.append(" AND TR.LICENSE_STATUS = 'N' ");
        
//Oat Edit 08/05/58
		 		 
		 if(dto.getOrgId() != 0)
		 {
			 hql.append(" AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS = 'N' ");
			 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");

			 parames.put("TRADER_ORG_ID", dto.getOrgId());
		 }
		 else
		 {
			 hql.append(" AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS = 'N' ");
//			 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
//
//			 parames.put("TRADER_ORG_ID", user.getUserData().getOrgId());
		 }
		 
//	        if(dto.getOrgId() != 0)
//	        {
//	          hql.append(" AND RE.ORG_ID = :REGISTRATION_ORG_ID ");
//	          parames.put("REGISTRATION_ORG_ID", dto.getOrgId());
//	        }
//	        else
//	        {
//	          hql.append(" AND RE.ORG_ID = :REGISTRATION_ORG_ID ");
//	          parames.put("REGISTRATION_ORG_ID", user.getUserData().getOrgId());
//	        }
	 
//End Oat Edit 08/05/58
        
        //ORDER BY
//	    hql.append(" ORDER BY TR.LICENSE_NO ,TR.EFFECTIVE_DATE");
        hql.append(" ORDER BY TR.EXPIRE_DATE, TR.LICENSE_NO");
	    //End Where
	    
	    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
	    sqlQuery.setFirstResult(start);
	    sqlQuery.setMaxResults(limit);
	    
		sqlQuery.addScalar("REG_ID", Hibernate.LONG); //0
		sqlQuery.addScalar("REGISTRATION_NO", Hibernate.STRING); //1
	    sqlQuery.addScalar("REGISTRATION_DATE", Hibernate.DATE); //2
	    sqlQuery.addScalar("REGISTRATION_PROGRESS", Hibernate.STRING); //3
	    sqlQuery.addScalar("REGISTRATION_TYPE", Hibernate.STRING);//4 
	    sqlQuery.addScalar("ORG_ID", Hibernate.LONG);//5
	    sqlQuery.addScalar("REG_RECORD_STATUS", Hibernate.STRING);//6
	    sqlQuery.addScalar("TRADER_ID", Hibernate.LONG); //7
	    sqlQuery.addScalar("TRADER_TYPE", Hibernate.STRING);//8
	    sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING);//9
	    sqlQuery.addScalar("TRADER_NAME_EN", Hibernate.STRING);//10
	    sqlQuery.addScalar("TRADER_CATEGORY", Hibernate.STRING);//11
	    sqlQuery.addScalar("PERSON_ID", Hibernate.LONG);//12
	    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);//13
	    sqlQuery.addScalar("PREFIX_NAME", Hibernate.STRING);//14
	    sqlQuery.addScalar("FIRST_NAME", Hibernate.STRING);//15
	    sqlQuery.addScalar("LAST_NAME", Hibernate.STRING);//16
	    sqlQuery.addScalar("POSTFIX_NAME", Hibernate.STRING);//17
//	   
	    sqlQuery.addScalar("PREFIX_ID", Hibernate.LONG);//18
	    sqlQuery.addScalar("POSTFIX_ID", Hibernate.LONG);//19
	    sqlQuery.addScalar("PERSON_AMPHUR_ID", Hibernate.LONG);//20
	    sqlQuery.addScalar("PERSON_TAXAMPHUR_ID", Hibernate.LONG);//21
	    sqlQuery.addScalar("PERSON_PROVINCE_ID", Hibernate.LONG);//22
	    sqlQuery.addScalar("PERSON_TAXPROVINCE_ID", Hibernate.LONG);//23
	    
	    sqlQuery.addScalar("TRADER_GUIDE_ID", Hibernate.LONG);//24
	    sqlQuery.addScalar("PRONUNCIATION_NAME", Hibernate.STRING);//25
	    sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING);//26
	    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);//27
	    sqlQuery.addScalar("IDENTITY_NO", Hibernate.STRING);//28
	    sqlQuery.addScalar("COMMITTEE_NAME1", Hibernate.STRING);//29
	    sqlQuery.addScalar("COMMITTEE_NAME2", Hibernate.STRING);//30
	    sqlQuery.addScalar("COMMITTEE_NAME_SIGN", Hibernate.STRING);//31
	    sqlQuery.addScalar("PASSPORT_NO", Hibernate.STRING);//32
	    sqlQuery.addScalar("FIRST_NAME_EN", Hibernate.STRING);//33
	    sqlQuery.addScalar("LAST_NAME_EN", Hibernate.STRING);//34
	    sqlQuery.addScalar("GENDER", Hibernate.STRING);//35
	    sqlQuery.addScalar("PERSON_NATIONALITY", Hibernate.STRING);//36
	    sqlQuery.addScalar("BIRTH_DATE", Hibernate.DATE);//37
	    sqlQuery.addScalar("AGE_YEAR", Hibernate.STRING);//38
	    sqlQuery.addScalar("IDENTITY_NO_EXPIRE", Hibernate.DATE);//39
	    sqlQuery.addScalar("CORPORATE_TYPE", Hibernate.STRING);//40
	    sqlQuery.addScalar("TAX_IDENTITY_NO", Hibernate.STRING);//41
	    sqlQuery.addScalar("IDENTITY_DATE", Hibernate.DATE);//42
	    sqlQuery.addScalar("TRADER_RECORD_STATUS", Hibernate.STRING);//43
	    
	    sqlQuery.addScalar("PERSON_AMPHUR_NAME", Hibernate.STRING);//44
	    sqlQuery.addScalar("PERSON_TAXAMPHUR_NAME", Hibernate.STRING);//45
	    sqlQuery.addScalar("PERSON_PROVINCE_NAME", Hibernate.STRING);//46
	    sqlQuery.addScalar("PERSON_TAXPROVINCE_NAME", Hibernate.STRING);//47
	    sqlQuery.addScalar("PREFIX_NAME_EN", Hibernate.STRING);//48
	    sqlQuery.addScalar("APPROVE_DATE", Hibernate.DATE);//49
	    
	    sqlQuery.addScalar("TRADER_EFFECTIVE_DATE", Hibernate.DATE);//50
	    sqlQuery.addScalar("TRADER_EXPIRE_DATE", Hibernate.DATE);//51
	    sqlQuery.addScalar("TRADER_LICENSE_STATUS", Hibernate.STRING);//52
	    
	    sqlQuery.addScalar("ADDRESS", Hibernate.STRING);//53

	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
  
	    return result;
	}
	//End Oat Add 13/11/57

	//Oat Add 20/02/58
//		public List<Object[]> findInfoAccountWillCancleByPaging(RegistrationDTO dto, User user ,int start, int limit)throws Exception
//		{
//			Map<String,Object> parames = new HashMap<String,Object>();
//		    
//		    StringBuilder hql = new StringBuilder();
//		
//		    hql.append(" SELECT RE.REG_ID AS REG_ID, ");
//		    hql.append(" RE.REGISTRATION_NO AS REGISTRATION_NO, ");
//		    hql.append(" RE.REGISTRATION_DATE AS REGISTRATION_DATE, ");
//		    hql.append(" RE.REGISTRATION_PROGRESS AS REGISTRATION_PROGRESS, ");
//		    hql.append(" RE.REGISTRATION_TYPE AS REGISTRATION_TYPE, ");
//		    hql.append(" RE.ORG_ID AS ORG_ID, ");
//		    hql.append(" RE.RECORD_STATUS AS REG_RECORD_STATUS, ");
//		    hql.append(" TR.TRADER_ID AS TRADER_ID, ");
//		    hql.append(" TR.TRADER_TYPE AS TRADER_TYPE, ");
//		    hql.append(" TR.TRADER_NAME AS TRADER_NAME, ");
//		    hql.append(" TR.TRADER_NAME_EN AS TRADER_NAME_EN, ");
//		    hql.append(" TR.TRADER_CATEGORY AS TRADER_CATEGORY, ");
//		    hql.append(" PE.PERSON_ID AS PERSON_ID, ");
//		    hql.append(" PE.PERSON_TYPE AS PERSON_TYPE, ");
//		    hql.append(" MP.PREFIX_NAME AS PREFIX_NAME, ");
//		    hql.append(" PE.FIRST_NAME AS FIRST_NAME, ");
//		    hql.append(" PE.LAST_NAME AS LAST_NAME, ");
//		    hql.append(" POSF.POSTFIX_NAME AS POSTFIX_NAME, ");
//		    hql.append(" MP.PREFIX_ID AS PREFIX_ID, ");
//		    hql.append(" POSF.POSTFIX_ID AS POSTFIX_ID, ");
//		    hql.append(" PEAMP.AMPHUR_ID AS PERSON_AMPHUR_ID, ");
//		    hql.append(" TAXPEAMP.AMPHUR_ID AS PERSON_TAXAMPHUR_ID, ");
//		    hql.append(" PEPROV.PROVINCE_ID AS PERSON_PROVINCE_ID, ");
//		    hql.append(" TAXPEPROV.PROVINCE_ID AS PERSON_TAXPROVINCE_ID, ");
//		    
//			
//		    hql.append(" TR.TRADER_GUIDE_ID AS TRADER_GUIDE_ID, ");
//		    hql.append(" TR.PRONUNCIATION_NAME AS PRONUNCIATION_NAME, ");
//		    hql.append(" TR.LICENSE_NO AS LICENSE_NO, ");
//		    hql.append(" PE.PERSON_TYPE AS PERSON_TYPE, ");
//		    hql.append(" PE.IDENTITY_NO AS IDENTITY_NO, ");
//		    hql.append(" PE.COMMITTEE_NAME1 AS COMMITTEE_NAME1, ");
//		    hql.append(" PE.COMMITTEE_NAME2 AS COMMITTEE_NAME2, ");
//		    hql.append(" PE.COMMITTEE_NAME_SIGN AS COMMITTEE_NAME_SIGN, ");
//		    hql.append(" PE.PASSPORT_NO AS PASSPORT_NO, ");
//		    hql.append(" PE.FIRST_NAME_EN AS FIRST_NAME_EN, ");
//		    hql.append(" PE.LAST_NAME_EN AS LAST_NAME_EN, ");
//		    hql.append(" PE.GENDER AS GENDER, ");
//		    hql.append(" PE.PERSON_NATIONALITY AS PERSON_NATIONALITY, ");
//		    hql.append(" PE.BIRTH_DATE AS BIRTH_DATE, ");
//		    hql.append(" PE.AGE_YEAR AS AGE_YEAR, ");
//		    hql.append(" PE.IDENTITY_NO_EXPIRE AS IDENTITY_NO_EXPIRE, ");
//		    hql.append(" PE.CORPORATE_TYPE AS CORPORATE_TYPE, ");
//		    hql.append(" PE.TAX_IDENTITY_NO AS TAX_IDENTITY_NO, ");
//		    hql.append(" PE.IDENTITY_DATE AS IDENTITY_DATE, ");
//		    hql.append(" TR.RECORD_STATUS AS TRADER_RECORD_STATUS, ");
//		    
//		    hql.append(" PEAMP.AMPHUR_NAME AS PERSON_AMPHUR_NAME, ");
//		    hql.append(" TAXPEAMP.AMPHUR_NAME AS PERSON_TAXAMPHUR_NAME, ");
//		    hql.append(" PEPROV.PROVINCE_NAME AS PERSON_PROVINCE_NAME, ");
//		    hql.append(" TAXPEPROV.PROVINCE_NAME AS PERSON_TAXPROVINCE_NAME, ");
//		    hql.append(" MP.PREFIX_NAME_EN AS PREFIX_NAME_EN ");
//		    
//		    //APPROVE_DATE
//		    hql.append(" ,RE.APPROVE_DATE AS APPROVE_DATE ");
//		    
//		    //EFFECTIVE_DATE
//		    hql.append(" ,TR.EFFECTIVE_DATE AS TRADER_EFFECTIVE_DATE ");
//		    
//		    //EXPIRE_DATE
//		    hql.append(" ,TR.EXPIRE_DATE AS TRADER_EXPIRE_DATE ");
//		    
//		    //LICENSE_STATUS
//		    hql.append(" ,TR.LICENSE_STATUS AS TRADER_LICENSE_STATUS ");
//		    
//		    hql.append(" FROM REGISTRATION RE ");
//		    hql.append(" INNER JOIN TRADER TR ");
//		    hql.append(" ON(RE.TRADER_ID = TR.TRADER_ID) ");
//		 
//		    hql.append(" INNER JOIN PERSON PE ");
//		    hql.append(" ON(PE.PERSON_ID = TR.PERSON_ID) ");
//
//		    hql.append(" LEFT JOIN MAS_PREFIX MP ");
//		    hql.append(" ON(PE.PREFIX_ID = MP.PREFIX_ID) ");
//
//		    hql.append(" LEFT JOIN MAS_POSFIX POSF ");
//		    hql.append(" ON(POSF.POSTFIX_ID = PE.POSTFIX_ID) ");
//		    hql.append(" LEFT JOIN MAS_AMPHUR PEAMP ");
//		    hql.append(" ON(PEAMP.AMPHUR_ID = PE.AMPHUR_ID) ");
//
//		    hql.append(" LEFT JOIN MAS_AMPHUR TAXPEAMP ");
//		    hql.append(" ON(TAXPEAMP.AMPHUR_ID = PE.TAX_AMPHUR_ID) ");
//
//		    hql.append(" LEFT JOIN MAS_PROVINCE PEPROV ");
//		    hql.append(" ON(PEPROV.PROVINCE_ID = PE.PROVINCE_ID) ");
//
//		    hql.append(" LEFT JOIN MAS_PROVINCE TAXPEPROV ");
//		    hql.append(" ON(TAXPEPROV.PROVINCE_ID = PE.TAX_PROVINCE_ID) ");
//		    
//		    //WHERE
//		    hql.append(" WHERE  TR.BRANCH_TYPE IS NULL ");
//		    
//		    //RECORD_STATUS
//		    hql.append(" AND TR.RECORD_STATUS = 'N' ");
//			hql.append(" AND RE.RECORD_STATUS = 'N' ");
//		    	    
//		    //PERSON_TYPE
//		    hql.append(" AND PE.PERSON_TYPE IS NOT NULL ");
//		    
//		    //TraderType B G L
//		    if((null != dto.getTraderType()) && (!"".equals(dto.getTraderType())))
//		    {
//		    	 hql.append(" AND TR.TRADER_TYPE = :TRADER_TRADER_TYPE ");
//				    
//				 parames.put("TRADER_TRADER_TYPE", dto.getTraderType());
//		    }
//		    
//		  //licenseNo เลขที่ใบอนุญาต
//		    if((null != dto.getLicenseNo()) && (!"".equals(dto.getLicenseNo())))
//		    {
//		    	 hql.append(" AND TR.LICENSE_NO LIKE :TRADER_LICENSE_NO ");
//				    
//				 parames.put("TRADER_LICENSE_NO", "%"+dto.getLicenseNo()+"%");
//		    }
//		    
////		    //EXPIRE_DATE
////		    if((null != dto.getExpireDateFrom()) && (!dto.getExpireDateFrom().equals("")))
////		    {
////		    	if((null != dto.getExpireDateTo()) && (!dto.getExpireDateTo().equals("")))
////		    	{
////					hql.append("  and ( TR.EXPIRE_DATE >=  convert(datetime, :TR_START_DATE ,20) ");
////					hql.append(" and TR.EXPIRE_DATE <=  convert(datetime, :TR_END_DATE ,20) ) ");
////					
////					String expireDtmFrom = DateUtils.getDateMSSQLFormat(dto.getExpireDateFrom(), "00:00");
////					String expireDtmTo = DateUtils.getDateMSSQLFormat(dto.getExpireDateTo(), "23:00");
////						
////					parames.put("TR_START_DATE", expireDtmFrom);
////					parames.put("TR_END_DATE", expireDtmTo);
////					
////					System.out.println("#####expireDtmFrom = "+expireDtmFrom);
////					System.out.println("#####expireDtmTo = "+expireDtmTo);
////					
////		    	}else
////		    	{
////		    		hql.append("  and ( TR.EXPIRE_DATE >=  convert(datetime, :TR_START_DATE ,20) ");
////					hql.append(" and TR.EXPIRE_DATE <=  convert(datetime, :TR_END_DATE ,20) ) ");
////					
////					String expireDtmFrom = DateUtils.getDateMSSQLFormat(dto.getExpireDateFrom(), "00:00");
////					String expireDtmTo = DateUtils.getDateMSSQLFormat(dto.getExpireDateFrom(), "23:00");
////						
////					parames.put("TR_START_DATE", expireDtmFrom);
////					parames.put("TR_END_DATE", expireDtmTo);
////		    	}
////		    }
//		    
//		    //LICENSE_STATUS สถานะใบอนุญาต
//			hql.append(" AND TR.LICENSE_STATUS = 'N' ");
//		    
//		    //OrgId ภูมิภาค
//	        if(dto.getOrgId() != 0)
//	        {
//	          hql.append(" AND RE.ORG_ID = :REGISTRATION_ORG_ID ");
//	          parames.put("REGISTRATION_ORG_ID", dto.getOrgId());
//	        }
//	        else
//	        {
//	          hql.append(" AND RE.ORG_ID = :REGISTRATION_ORG_ID ");
//	          parames.put("REGISTRATION_ORG_ID", user.getUserData().getOrgId());
//	        }
//	        //ORDER BY
////		    hql.append(" ORDER BY TR.LICENSE_NO ,TR.EFFECTIVE_DATE");
//	        hql.append(" ORDER BY TR.EXPIRE_DATE, TR.LICENSE_NO");
//		    //End Where
//		    
//		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
//		    sqlQuery.setFirstResult(start);
//		    sqlQuery.setMaxResults(limit);
//		    
//			sqlQuery.addScalar("REG_ID", Hibernate.LONG); //0
//			sqlQuery.addScalar("REGISTRATION_NO", Hibernate.STRING); //1
//		    sqlQuery.addScalar("REGISTRATION_DATE", Hibernate.DATE); //2
//		    sqlQuery.addScalar("REGISTRATION_PROGRESS", Hibernate.STRING); //3
//		    sqlQuery.addScalar("REGISTRATION_TYPE", Hibernate.STRING);//4 
//		    sqlQuery.addScalar("ORG_ID", Hibernate.LONG);//5
//		    sqlQuery.addScalar("REG_RECORD_STATUS", Hibernate.STRING);//6
//		    sqlQuery.addScalar("TRADER_ID", Hibernate.LONG); //7
//		    sqlQuery.addScalar("TRADER_TYPE", Hibernate.STRING);//8
//		    sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING);//9
//		    sqlQuery.addScalar("TRADER_NAME_EN", Hibernate.STRING);//10
//		    sqlQuery.addScalar("TRADER_CATEGORY", Hibernate.STRING);//11
//		    sqlQuery.addScalar("PERSON_ID", Hibernate.LONG);//12
//		    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);//13
//		    sqlQuery.addScalar("PREFIX_NAME", Hibernate.STRING);//14
//		    sqlQuery.addScalar("FIRST_NAME", Hibernate.STRING);//15
//		    sqlQuery.addScalar("LAST_NAME", Hibernate.STRING);//16
//		    sqlQuery.addScalar("POSTFIX_NAME", Hibernate.STRING);//17
////		   
//		    sqlQuery.addScalar("PREFIX_ID", Hibernate.LONG);//18
//		    sqlQuery.addScalar("POSTFIX_ID", Hibernate.LONG);//19
//		    sqlQuery.addScalar("PERSON_AMPHUR_ID", Hibernate.LONG);//20
//		    sqlQuery.addScalar("PERSON_TAXAMPHUR_ID", Hibernate.LONG);//21
//		    sqlQuery.addScalar("PERSON_PROVINCE_ID", Hibernate.LONG);//22
//		    sqlQuery.addScalar("PERSON_TAXPROVINCE_ID", Hibernate.LONG);//23
//		    
//		    sqlQuery.addScalar("TRADER_GUIDE_ID", Hibernate.LONG);//24
//		    sqlQuery.addScalar("PRONUNCIATION_NAME", Hibernate.STRING);//25
//		    sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING);//26
//		    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);//27
//		    sqlQuery.addScalar("IDENTITY_NO", Hibernate.STRING);//28
//		    sqlQuery.addScalar("COMMITTEE_NAME1", Hibernate.STRING);//29
//		    sqlQuery.addScalar("COMMITTEE_NAME2", Hibernate.STRING);//30
//		    sqlQuery.addScalar("COMMITTEE_NAME_SIGN", Hibernate.STRING);//31
//		    sqlQuery.addScalar("PASSPORT_NO", Hibernate.STRING);//32
//		    sqlQuery.addScalar("FIRST_NAME_EN", Hibernate.STRING);//33
//		    sqlQuery.addScalar("LAST_NAME_EN", Hibernate.STRING);//34
//		    sqlQuery.addScalar("GENDER", Hibernate.STRING);//35
//		    sqlQuery.addScalar("PERSON_NATIONALITY", Hibernate.STRING);//36
//		    sqlQuery.addScalar("BIRTH_DATE", Hibernate.DATE);//37
//		    sqlQuery.addScalar("AGE_YEAR", Hibernate.STRING);//38
//		    sqlQuery.addScalar("IDENTITY_NO_EXPIRE", Hibernate.DATE);//39
//		    sqlQuery.addScalar("CORPORATE_TYPE", Hibernate.STRING);//40
//		    sqlQuery.addScalar("TAX_IDENTITY_NO", Hibernate.STRING);//41
//		    sqlQuery.addScalar("IDENTITY_DATE", Hibernate.DATE);//42
//		    sqlQuery.addScalar("TRADER_RECORD_STATUS", Hibernate.STRING);//43
//		    
//		    sqlQuery.addScalar("PERSON_AMPHUR_NAME", Hibernate.STRING);//44
//		    sqlQuery.addScalar("PERSON_TAXAMPHUR_NAME", Hibernate.STRING);//45
//		    sqlQuery.addScalar("PERSON_PROVINCE_NAME", Hibernate.STRING);//46
//		    sqlQuery.addScalar("PERSON_TAXPROVINCE_NAME", Hibernate.STRING);//47
//		    sqlQuery.addScalar("PREFIX_NAME_EN", Hibernate.STRING);//48
//		    sqlQuery.addScalar("APPROVE_DATE", Hibernate.DATE);//49
//		    
//		    sqlQuery.addScalar("TRADER_EFFECTIVE_DATE", Hibernate.DATE);//50
//		    sqlQuery.addScalar("TRADER_EXPIRE_DATE", Hibernate.DATE);//51
//		    sqlQuery.addScalar("TRADER_LICENSE_STATUS", Hibernate.STRING);//52
//		    
//
//		    sqlQuery.setProperties(parames);
//		    List<Object[]>  result = sqlQuery.list();
//	  
//		    return result;
//		}

		
		public List<Object[]> findInfoReceiptWillCanclePaging(RegistrationDTO dto, User user ,int start, int limit)throws Exception
		{
			Map<String,Object> parames = new HashMap<String,Object>();
		    
		    StringBuilder hql = new StringBuilder();
		    
		    hql.append(" 	    SELECT RE.REG_ID AS REG_ID, ");
		    hql.append(" 	    RE.REGISTRATION_NO AS REGISTRATION_NO, ");
		    hql.append(" 	    RE.REGISTRATION_DATE AS REGISTRATION_DATE, ");
		    hql.append(" 	    RE.REGISTRATION_PROGRESS AS REGISTRATION_PROGRESS, ");
		    hql.append(" 	    RE.REGISTRATION_TYPE AS REGISTRATION_TYPE, ");
		    hql.append(" 	    RE.ORG_ID AS ORG_ID, ");
		    hql.append(" 	    RE.RECORD_STATUS AS RECORD_STATUS, ");
		    hql.append(" 	    TR.TRADER_ID AS TRADER_ID, ");
		    hql.append(" 	    TR.TRADER_TYPE AS TRADER_TYPE, ");
		    hql.append(" 	    TR.TRADER_NAME AS TRADER_NAME, ");
		    hql.append(" 	    TR.TRADER_NAME_EN AS TRADER_NAME_EN, ");
		    hql.append(" 	    TR.TRADER_CATEGORY AS TRADER_CATEGORY, ");
		    hql.append(" 	    PE.PERSON_ID AS PERSON_ID, ");
		    hql.append(" 	    PE.PERSON_TYPE AS PERSON_TYPE, ");
		    hql.append(" 	    MP.PREFIX_NAME AS PREFIX_NAME, ");
		    hql.append(" 	    PE.FIRST_NAME AS FIRST_NAME, ");
		    hql.append(" 	    PE.LAST_NAME AS LAST_NAME, ");
		    hql.append(" 	    POSF.POSTFIX_NAME AS POSTFIX_NAME, ");
		    hql.append(" 	    MP.PREFIX_ID AS PREFIX_ID, ");
		    hql.append(" 	    POSF.POSTFIX_ID AS POSTFIX_ID, ");
		    hql.append(" 	    PEAMP.AMPHUR_ID AS PERSON_AMPHUR_ID, ");
		    hql.append(" 	    TAXPEAMP.AMPHUR_ID AS PERSON_TAXAMPHUR_ID, ");
		    hql.append(" 	    PEPROV.PROVINCE_ID AS PERSON_PROVINCE_ID, ");
		    hql.append(" 	    TAXPEPROV.PROVINCE_ID AS PERSON_TAXPROVINCE_ID, ");
		    hql.append(" 	    TR.TRADER_GUIDE_ID AS TRADER_GUIDE_ID, ");
		    hql.append(" 	    TR.PRONUNCIATION_NAME AS PRONUNCIATION_NAME, ");
		    hql.append(" 	    TR.LICENSE_GUIDE_NO AS LICENSE_GUIDE_NO, ");
		    hql.append(" 	    PE.PERSON_TYPE AS PERSON_TYPE, ");
		    hql.append(" 	    PE.IDENTITY_NO AS IDENTITY_NO, ");
		    hql.append(" 	    PE.COMMITTEE_NAME1 AS COMMITTEE_NAME1, ");
		    hql.append(" 	    PE.COMMITTEE_NAME2 AS COMMITTEE_NAME2, ");
		    hql.append(" 	    PE.COMMITTEE_NAME_SIGN AS COMMITTEE_NAME_SIGN, ");
		    hql.append(" 	    PE.PASSPORT_NO AS PASSPORT_NO, ");
		    hql.append(" 	    PE.FIRST_NAME_EN AS FIRST_NAME_EN, ");
		    hql.append(" 	    PE.LAST_NAME_EN AS LAST_NAME_EN, ");
		    hql.append(" 	    PE.GENDER AS GENDER, ");
		    hql.append(" 	    PE.PERSON_NATIONALITY AS PERSON_NATIONALITY, ");
		    hql.append(" 	    PE.BIRTH_DATE AS BIRTH_DATE, ");
		    hql.append(" 	    PE.AGE_YEAR AS AGE_YEAR, ");
		    hql.append(" 	    PE.IDENTITY_NO_EXPIRE AS IDENTITY_NO_EXPIRE, ");
		    hql.append(" 	    PE.CORPORATE_TYPE AS CORPORATE_TYPE, ");
		    hql.append(" 	    PE.TAX_IDENTITY_NO AS TAX_IDENTITY_NO, ");
		    hql.append(" 	    PE.IDENTITY_DATE AS IDENTITY_DATE, ");
		    hql.append(" 	    TR.RECORD_STATUS AS RECORD_STATUS, ");
		    hql.append(" 	    PEAMP.AMPHUR_NAME AS PERSON_AMPHUR_NAME, ");
		    hql.append(" 	    TAXPEAMP.AMPHUR_NAME AS PERSON_TAXAMPHUR_NAME, ");
		    hql.append(" 	    PEPROV.PROVINCE_NAME AS PERSON_PROVINCE_NAME, ");
		    hql.append(" 	    TAXPEPROV.PROVINCE_NAME AS PERSON_TAXPROVINCE_NAME, ");
		    hql.append(" 	    MP.PREFIX_NAME_EN AS PREFIX_NAME_EN, ");
		    hql.append(" 	    TR.LICENSE_NO AS LICENSE_NO, ");
		    hql.append(" 	    REPT.RECEIPT_ID AS RECEIPT_ID, ");
		    hql.append(" 	    REPT.BOOK_NO AS BOOK_NO, ");
		    hql.append(" 	    REPT.RECEIPT_NO AS RECEIPT_NO, ");
		    hql.append(" 	    REPT.RECEIPT_STATUS AS RECEIPT_STATUS ");
		    
		    //APPROVE_DATE
		    hql.append(" ,RE.APPROVE_DATE AS APPROVE_DATE ");
		    //EFFECTIVE_DATE
		    hql.append(" ,TR.EFFECTIVE_DATE AS TRADER_EFFECTIVE_DATE ");
		    //EXPIRE_DATE
		    hql.append(" ,TR.EXPIRE_DATE AS TRADER_EXPIRE_DATE ");
		
		    hql.append(" 	    FROM REGISTRATION RE ");

		    hql.append(" 	    LEFT JOIN RECEIPT_MAP_REGISTRATION REREG ");
		    hql.append(" 	    ON(REREG.REG_ID = RE.REG_ID) ");

		    hql.append(" 	    LEFT JOIN RECEIPT REPT ");
		    hql.append(" 	    ON(REPT.RECEIPT_ID = REREG.RECEIPT_ID) ");

		    hql.append(" 	    INNER JOIN TRADER TR ");
		    hql.append(" 	    ON(RE.TRADER_ID = TR.TRADER_ID) ");

		    hql.append(" 	    INNER JOIN PERSON PE ");
		    hql.append(" 	    ON(PE.PERSON_ID = TR.PERSON_ID) ");

		    hql.append(" 	    LEFT JOIN MAS_PREFIX MP ");
		    hql.append(" 	    ON(PE.PREFIX_ID = MP.PREFIX_ID) ");

		    hql.append(" 	    LEFT JOIN MAS_POSFIX POSF ");
		    hql.append(" 	    ON(POSF.POSTFIX_ID = PE.POSTFIX_ID) ");

		    hql.append(" 	    LEFT JOIN MAS_AMPHUR PEAMP ");
		    hql.append(" 	    ON(PEAMP.AMPHUR_ID = PE.AMPHUR_ID) ");

		    hql.append(" 	    LEFT JOIN MAS_AMPHUR TAXPEAMP ");
		    hql.append(" 	    ON(TAXPEAMP.AMPHUR_ID = PE.TAX_AMPHUR_ID) ");

		    hql.append(" 	    LEFT JOIN MAS_PROVINCE PEPROV ");
		    hql.append(" 	    ON(PEPROV.PROVINCE_ID = PE.PROVINCE_ID) ");

		    hql.append(" 	    LEFT JOIN MAS_PROVINCE TAXPEPROV ");
		    hql.append(" 	    ON(TAXPEPROV.PROVINCE_ID = PE.TAX_PROVINCE_ID) ");
			
		    hql.append(" WHERE RE.ORG_ID = :REGISTRATION_ORG_ID ");
		    
		    parames.put("REGISTRATION_ORG_ID", user.getUserData().getOrgId());
		    
		    hql.append(" AND TR.BRANCH_TYPE IS NULL ");
		    hql.append(" AND TR.RECORD_STATUS = 'N' ");
		    hql.append(" AND RE.RECORD_STATUS = 'N' ");
		    hql.append(" AND TR.LICENSE_STATUS = 'N' ");
		    
		    if((null != dto.getLicenseNo()) && (!"".equals(dto.getLicenseNo())))
		    {
		    	 hql.append(" AND TR.LICENSE_NO LIKE :TRADER_LICENSE_NO ");
				    
				 parames.put("TRADER_LICENSE_NO", "%"+dto.getLicenseNo()+"%");
		    }
		    
		    if((null != dto.getTraderType()) && (!"".equals(dto.getTraderType())))
		    {
		    	 hql.append(" AND TR.TRADER_TYPE = :TRADER_TRADER_TYPE ");
				    
				 parames.put("TRADER_TRADER_TYPE", dto.getTraderType());
		    }
			
		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
		    sqlQuery.setFirstResult(start);
		    sqlQuery.setMaxResults(limit);
		    
			sqlQuery.addScalar("REG_ID", Hibernate.LONG); //0
			sqlQuery.addScalar("REGISTRATION_NO", Hibernate.STRING); //1
		    sqlQuery.addScalar("REGISTRATION_DATE", Hibernate.DATE); //2
		    sqlQuery.addScalar("REGISTRATION_PROGRESS", Hibernate.STRING); //3
		    sqlQuery.addScalar("REGISTRATION_TYPE", Hibernate.STRING);//4
		    sqlQuery.addScalar("ORG_ID", Hibernate.LONG);//5
		    sqlQuery.addScalar("RECORD_STATUS", Hibernate.STRING);//6
		    sqlQuery.addScalar("TRADER_ID", Hibernate.LONG); //7
		    sqlQuery.addScalar("TRADER_TYPE", Hibernate.STRING);//8
		    sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING);//9
		    sqlQuery.addScalar("TRADER_NAME_EN", Hibernate.STRING);//10
		    sqlQuery.addScalar("TRADER_CATEGORY", Hibernate.STRING);//11
		    sqlQuery.addScalar("PERSON_ID", Hibernate.LONG);//12
		    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);//13
		    sqlQuery.addScalar("PREFIX_NAME", Hibernate.STRING);//14
		    sqlQuery.addScalar("FIRST_NAME", Hibernate.STRING);//15
		    sqlQuery.addScalar("LAST_NAME", Hibernate.STRING);//16
		    sqlQuery.addScalar("POSTFIX_NAME", Hibernate.STRING);//17

		    sqlQuery.addScalar("PREFIX_ID", Hibernate.LONG);//18
		    sqlQuery.addScalar("POSTFIX_ID", Hibernate.LONG);//19
		    sqlQuery.addScalar("PERSON_AMPHUR_ID", Hibernate.LONG);//20
		    sqlQuery.addScalar("PERSON_TAXAMPHUR_ID", Hibernate.LONG);//21
		    sqlQuery.addScalar("PERSON_PROVINCE_ID", Hibernate.LONG);//22
		    sqlQuery.addScalar("PERSON_TAXPROVINCE_ID", Hibernate.LONG);//23
		    
		    sqlQuery.addScalar("TRADER_GUIDE_ID", Hibernate.LONG);//24
		    sqlQuery.addScalar("PRONUNCIATION_NAME", Hibernate.STRING);//25
		    sqlQuery.addScalar("LICENSE_GUIDE_NO", Hibernate.STRING);//26
		    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);//27
		    sqlQuery.addScalar("IDENTITY_NO", Hibernate.STRING);//28
		    sqlQuery.addScalar("COMMITTEE_NAME1", Hibernate.STRING);//29
		    sqlQuery.addScalar("COMMITTEE_NAME2", Hibernate.STRING);//30
		    sqlQuery.addScalar("COMMITTEE_NAME_SIGN", Hibernate.STRING);//31
		    sqlQuery.addScalar("PASSPORT_NO", Hibernate.STRING);//32
		    sqlQuery.addScalar("FIRST_NAME_EN", Hibernate.STRING);//33
		    sqlQuery.addScalar("LAST_NAME_EN", Hibernate.STRING);//34
		    sqlQuery.addScalar("GENDER", Hibernate.STRING);//35
		    sqlQuery.addScalar("PERSON_NATIONALITY", Hibernate.STRING);//36
		    sqlQuery.addScalar("BIRTH_DATE", Hibernate.DATE);//37
		    sqlQuery.addScalar("AGE_YEAR", Hibernate.STRING);//38
		    sqlQuery.addScalar("IDENTITY_NO_EXPIRE", Hibernate.DATE);//39
		    sqlQuery.addScalar("CORPORATE_TYPE", Hibernate.STRING);//40
		    sqlQuery.addScalar("TAX_IDENTITY_NO", Hibernate.STRING);//41
		    sqlQuery.addScalar("IDENTITY_DATE", Hibernate.DATE);//42
		    sqlQuery.addScalar("RECORD_STATUS", Hibernate.STRING);//43
		    
		    sqlQuery.addScalar("PERSON_AMPHUR_NAME", Hibernate.STRING);//44
		    sqlQuery.addScalar("PERSON_TAXAMPHUR_NAME", Hibernate.STRING);//45
		    sqlQuery.addScalar("PERSON_PROVINCE_NAME", Hibernate.STRING);//46
		    sqlQuery.addScalar("PERSON_TAXPROVINCE_NAME", Hibernate.STRING);//47
		    sqlQuery.addScalar("PREFIX_NAME_EN", Hibernate.STRING);//48
		    sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING);//49
		    sqlQuery.addScalar("RECEIPT_ID", Hibernate.LONG);//50
		    sqlQuery.addScalar("BOOK_NO", Hibernate.STRING);//51
		    sqlQuery.addScalar("RECEIPT_NO", Hibernate.STRING);//52
		    sqlQuery.addScalar("RECEIPT_STATUS", Hibernate.STRING);//53
		    
		    sqlQuery.addScalar("APPROVE_DATE", Hibernate.DATE);//54
		    sqlQuery.addScalar("TRADER_EFFECTIVE_DATE", Hibernate.DATE);//55
		    sqlQuery.addScalar("TRADER_EXPIRE_DATE", Hibernate.DATE);//56

		    sqlQuery.setProperties(parames);
		    List<Object[]>  result = sqlQuery.list();
	  
		    return result;
		}
		//End Oat Add 
	
}
