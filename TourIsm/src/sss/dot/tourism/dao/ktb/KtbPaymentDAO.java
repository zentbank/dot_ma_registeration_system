package sss.dot.tourism.dao.ktb;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.KtbPayment;
import sss.dot.tourism.domain.Registration;
import sss.dot.tourism.dto.certificate.ReqLicenseCertificateDTO;
import sss.dot.tourism.dto.ktb.KtbPaymentDTO;
import sss.dot.tourism.util.DateUtils;

@Repository("ktbPaymentDAO")
public class KtbPaymentDAO extends BaseDAO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2918099022414391301L;

	public KtbPaymentDAO()
	{
		this.domainObj = KtbPayment.class;
	}
	
	public List<KtbPayment> findByRef(String ref1, String ref2) throws Exception
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  KtbPayment as dep ");
		hql.append(" where dep.DReference1 = ? ");
		hql.append(" and dep.DReference2 = ? ");
		params.add(ref1);
		params.add(ref2);
		
		return (List<KtbPayment>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
	
	public KtbPayment findByRef(String ref1, String ref2, String DReference3) throws Exception
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  KtbPayment as dep ");
		hql.append(" where dep.DReference1 = ? ");
		hql.append(" and dep.DReference2 = ? ");
		hql.append(" and dep.DReference3 = ? ");
		hql.append(" and dep.recordStatus = 'T' ");
		params.add(ref1);
		params.add(ref2);
		params.add(DReference3);
		
		List<KtbPayment>  list = (List<KtbPayment>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
		return CollectionUtils.isNotEmpty(list)?list.get(0):null;
	}
	
	public List<KtbPayment> findSumaryPayment(String progressDate, String status) throws Exception
	{

		
		
		
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  KtbPayment as dep ");
		
		if((null == status) || (status.isEmpty()))
		{
			hql.append(" where dep.recordStatus in ('T', 'W', 'P') ");
		}
		else
		{
			hql.append(" where dep.recordStatus = ? ");
			params.add(status);
		}
	    
    	if(null != progressDate)
    	{
			hql.append("  and ( dep.DTranDate >=  convert(datetime, ? ,20) ");
			hql.append(" and dep.DTranDate <=  convert(datetime, ? ,20) ) ");
			
			String sendDtmFrom = DateUtils.getDateMSSQLFormat(progressDate, "00:00");
			String sendDtmTo = DateUtils.getDateMSSQLFormat(progressDate, "23:59");

			
			params.add(sendDtmFrom);
			params.add(sendDtmTo);
    	}
    	
    	hql.append(" order by dep.licenseNo ");
	    
		return (List<KtbPayment>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
	
	public List<KtbPayment> findPayment() throws Exception
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  KtbPayment as dep ");
		hql.append(" where dep.recordStatus = 'T' ");
		
		
		return (List<KtbPayment>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
	
	
	public List<Registration> findRegistrationByLicenseNo(String licenseNo, String traderType, String registrationType,String regRecordStatus, String traderRecordStatus, long orgId)
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" select reg from  Registration as reg ");
		hql.append(" where reg.recordStatus = ? ");
		hql.append(" and reg.registrationType = ? ");
		hql.append(" and reg.trader.branchType is null ");
		hql.append(" and reg.trader.traderType = ? ");
		hql.append(" and reg.trader.recordStatus = ? ");
		hql.append(" and reg.trader.licenseNo = ? ");

		params.add(regRecordStatus);
		params.add(registrationType);
		params.add(traderType);
		params.add(traderRecordStatus);
		params.add(licenseNo);
		
		if(orgId > 0)
		{
			hql.append(" and reg.trader.organization.orgId = ? ");
			params.add(orgId);
		}

		return (List<Registration>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
	
	public List<Object[]> findAttachPaymentFiles(String progressDate) throws Exception {
		Map<String, Object> parames = new HashMap<String, Object>();

		StringBuilder hql = new StringBuilder();
		


		 hql.append(" SELECT  ");
		 hql.append(" 	 CONVERT(VARCHAR,P.PROGRESS_DATE,103) AS PROGRESS_DATE ");
		 hql.append(" 	, P.RECORD_STATUS  AS RECORD_STATUS ");
		 hql.append(" 	, SUM(P.D_TRAN_AMT) AS D_TRAN_AMT  ");
		 hql.append(" 	,P.PAYMENT_NO AS PAYMENT_NO ");
		 
		 
		 hql.append(" FROM KTB_PAYMENT P ");
		
		

		 if(StringUtils.isNotEmpty(progressDate)){
			 hql.append(" WHERE P.D_TRAN_DATE >= convert(datetime, :REQ_DATE_START_DATE ,20) ");
			 hql.append(" AND P.D_TRAN_DATE < convert(datetime, :REQ_DATE__END_DATE ,20) ");
			 
			 String sendDtmFrom = DateUtils.getDateMSSQLFormat(
						progressDate, "00:00");
				String sendDtmTo = DateUtils.getDateMSSQLFormat(
						progressDate, "23:59");
				
				parames.put("REQ_DATE_START_DATE", sendDtmFrom);
				parames.put("REQ_DATE__END_DATE", sendDtmTo);
		 }
		 
		 hql.append(" GROUP BY  ");
		 hql.append(" 	CONVERT(VARCHAR,P.PROGRESS_DATE,103) ");
		 hql.append(" 	,P.RECORD_STATUS ");
		 hql.append(" 	,P.PAYMENT_NO ");
	

		SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());

		sqlQuery.addScalar("PROGRESS_DATE", Hibernate.STRING); // 0
		sqlQuery.addScalar("RECORD_STATUS", Hibernate.STRING); // 1
		sqlQuery.addScalar("D_TRAN_AMT", Hibernate.BIG_DECIMAL); // 2
		sqlQuery.addScalar("PAYMENT_NO", Hibernate.STRING);// 3
		
		

		sqlQuery.setProperties(parames);
		List<Object[]> result = sqlQuery.list();

		return CollectionUtils.isNotEmpty(result)?result:null;
	}
	
//	public List<Object[]> findAttachPaymentFilesDetail(String progressDate, String paymentNo) throws Exception {
	public List<Object[]> findAttachPaymentFilesDetail(KtbPaymentDTO param) throws Exception {
		Map<String, Object> parames = new HashMap<String, Object>();

		StringBuilder hql = new StringBuilder();
		
		

		 hql.append("  SELECT  ");
		 hql.append("  	P.D_REFERENCE_1 AS D_REFERENCE_1 ");
		 hql.append("  	,P.D_REFERENCE_2 AS D_REFERENCE_2 ");
		 hql.append("  	,CONVERT(VARCHAR,P.PROGRESS_DATE,103) AS PROGRESS_DATE ");
		 hql.append("  	, P.RECORD_STATUS  AS RECORD_STATUS ");
		 hql.append("  	, P.D_TRAN_AMT AS D_TRAN_AMT  ");
		 hql.append("  	,TRFEE.FEE_REGISTRATION_TYPE AS FEE_REGISTRATION_TYPE ");
		 hql.append("  	,CASE WHEN PE.OWNER_TYPE = 'I' THEN MF.PREFIX_NAME + PE.OWNER_NAME +' '+ PE.OWNER_LAST_NAME   ");
		 hql.append("  		ELSE CASE WHEN PE.POSTFIX_ID IS NOT NULL THEN MF.PREFIX_NAME + PE.OWNER_NAME +' '+ MP.POSTFIX_NAME  ");
		 hql.append("  				ELSE MF.PREFIX_NAME + PE.OWNER_NAME ");
		 hql.append("  			END ");
		 hql.append("  	END AS PERSON_NAME ");
		 hql.append("  	,LIC.LICENSE_TYPE AS LICENSE_TYPE ");
		 hql.append("  	,P.KTB_PAY_ID AS KTB_PAY_ID ");
		 hql.append("  	,REG.ORG_ID AS ORG_ID ");
		 
		 
		 
		 hql.append("  FROM KTB_PAYMENT P ");
		 hql.append("  INNER JOIN DOT_TR_BILL_PAYMENT BILL ");
		 hql.append("  ON(BILL.BILL_PAYMENT_NO = P.D_REFERENCE_2) ");
		 hql.append("  INNER JOIN DOT_TR_FEE_LICENSE TRFEE ");
		 hql.append("  ON(TRFEE.LICENSE_FEE_ID = BILL.LICENSE_FEE_ID) ");
		 hql.append("  INNER JOIN DOT_LICENSE LIC ");
		 hql.append("  ON(LIC.LICENSE_ID = TRFEE.LICENSE_ID) ");
		 hql.append("  INNER JOIN DOT_LICENSE_OWNER PE ");
		 hql.append("  ON(PE.TAX_ID = P.D_REFERENCE_1) ");
		 hql.append("  INNER JOIN MAS_PREFIX MF ");
		 hql.append("  ON(MF.PREFIX_ID = PE.PREFIX_ID) ");
		 hql.append("  LEFT JOIN MAS_POSFIX MP ");
		 hql.append("  ON(MP.POSTFIX_ID = PE.POSTFIX_ID) ");
		 hql.append("  INNER JOIN REGISTRATION REG ");
		 hql.append("  ON(REG.REG_ID = TRFEE.REG_ID) ");
		 
		 
		 
		 hql.append(" WHERE PE.RECORD_STATUS = 'N'  ");
		 
		 if(StringUtils.isNotEmpty(param.getRecordStatus())){
			 hql.append("  AND P.RECORD_STATUS = :STATUS ");
			 parames.put("STATUS", param.getRecordStatus());
		 }
		 
		 if((StringUtils.isNotEmpty(param.getDTranDateFrom())) 
				 && (StringUtils.isNotEmpty(param.getDTranDateTo()))){
			 
			String sendDtmFrom = DateUtils.getDateMSSQLFormat(
					param.getDTranDateFrom(), "00:00");
			String sendDtmTo = DateUtils.getDateMSSQLFormat(
					param.getDTranDateTo(), "23:59");
			
			 hql.append(" AND P.PROGRESS_DATE >= convert(datetime, :REQ_DATE_START_DATE ,20) ");
			 hql.append(" AND P.PROGRESS_DATE < convert(datetime, :REQ_DATE__END_DATE ,20) ");
			
			parames.put("REQ_DATE_START_DATE", sendDtmFrom);
			parames.put("REQ_DATE__END_DATE", sendDtmTo);
			 
		 }
			
//		hql.append(" AND P.PAYMENT_NO = :PAYMENT_NO ");
//		parames.put("PAYMENT_NO", paymentNo);
		 
		 
		 hql.append("  GROUP BY D_REFERENCE_1, D_REFERENCE_2, CONVERT(VARCHAR,P.PROGRESS_DATE,103) ");
		 hql.append("  , P.RECORD_STATUS, D_TRAN_AMT, FEE_REGISTRATION_TYPE, CASE WHEN PE.OWNER_TYPE = 'I' THEN MF.PREFIX_NAME + PE.OWNER_NAME +' '+ PE.OWNER_LAST_NAME   ");
		 hql.append("  		ELSE CASE WHEN PE.POSTFIX_ID IS NOT NULL THEN MF.PREFIX_NAME + PE.OWNER_NAME +' '+ MP.POSTFIX_NAME  ");
		 hql.append("  				ELSE MF.PREFIX_NAME + PE.OWNER_NAME ");
		 hql.append("  			END ");
		 hql.append("  	END ");
		 hql.append("  	, LIC.LICENSE_TYPE, P.KTB_PAY_ID  ");
		 hql.append("  	, REG.ORG_ID   ");
		
		
		
	

		SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());

		sqlQuery.addScalar("D_REFERENCE_1", Hibernate.STRING); // 0
		sqlQuery.addScalar("D_REFERENCE_2", Hibernate.STRING); // 1
		sqlQuery.addScalar("PROGRESS_DATE", Hibernate.STRING); // 2
		sqlQuery.addScalar("RECORD_STATUS", Hibernate.STRING);// 3
		sqlQuery.addScalar("D_TRAN_AMT", Hibernate.BIG_DECIMAL);// 4
		sqlQuery.addScalar("FEE_REGISTRATION_TYPE", Hibernate.STRING);// 5
		sqlQuery.addScalar("PERSON_NAME", Hibernate.STRING);// 6
		sqlQuery.addScalar("LICENSE_TYPE", Hibernate.STRING);// 7
		sqlQuery.addScalar("KTB_PAY_ID", Hibernate.LONG);//8
		sqlQuery.addScalar("ORG_ID", Hibernate.LONG);//9

		sqlQuery.setProperties(parames);
		List<Object[]> result = sqlQuery.list();

		return CollectionUtils.isNotEmpty(result)?result:null;
	}
	
	
}
