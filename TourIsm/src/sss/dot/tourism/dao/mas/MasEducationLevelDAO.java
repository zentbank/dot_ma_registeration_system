package sss.dot.tourism.dao.mas;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.MasEducationLevel;


@Repository("masEducationLevelDAO")
public class MasEducationLevelDAO extends BaseDAO{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4143228682303596112L;

	public MasEducationLevelDAO()
	{
		this.domainObj = MasEducationLevel.class;
	}
}
