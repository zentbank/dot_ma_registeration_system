package sss.dot.tourism.dao.mas;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.MasAmphur;
import sss.dot.tourism.domain.MasFee;

@Repository("masFeeDAO")
public class MasFeeDAO extends BaseDAO {
	
	public MasFeeDAO()
	{
		this.domainObj = MasFee.class;
	}
	
	public List<MasFee> findFee(String registerType, String traderType)
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  MasFee as dep ");
		hql.append(" where dep.recordStatus = 'N' ");
		
		if((null != registerType) && (!registerType.isEmpty()))
		{
			hql.append(" and dep.registrationType = ? ");
			params.add(registerType);
		}
		
		if((null != traderType) && (!traderType.isEmpty()))
		{
			hql.append(" and dep.traderType = ? ");
			params.add(traderType);
		}
		
		hql.append(" order by dep.feeId ");
		
		return (List<MasFee>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
}
