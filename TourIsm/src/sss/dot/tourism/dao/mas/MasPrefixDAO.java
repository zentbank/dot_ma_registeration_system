package sss.dot.tourism.dao.mas;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.MasPrefix;
import sss.dot.tourism.dto.mas.MasPrefixDTO;

@Repository("masPrefixDAO")
public class MasPrefixDAO extends BaseDAO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8286402677175694697L;

	public MasPrefixDAO()
	{
		this.domainObj = MasPrefix.class;
	}
	
	  @SuppressWarnings({ "unchecked", "rawtypes" })
	public List<MasPrefix> findAll(MasPrefixDTO dto)
	  {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  MasPrefix as dep ");
		hql.append(" where dep.recordStatus = 'N' ");
		

		if((dto.getPrefixName() != null) && (!dto.getPrefixName().isEmpty()))
		{
			hql.append(" and dep.prefixName like ? ");
			params.add("%"+dto.getPrefixName()+"%");
		}
		
		if((dto.getPrefixType() != null) && (!dto.getPrefixType().isEmpty()))
		{
			hql.append(" and dep.prefixType = ? ");
			params.add(dto.getPrefixType());
		}
		
		hql.append(" order by dep.prefixName ");
		
		return (List<MasPrefix>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	  }
}
