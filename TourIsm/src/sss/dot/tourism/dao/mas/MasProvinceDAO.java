package sss.dot.tourism.dao.mas;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.MasProvince;
import sss.dot.tourism.dto.mas.MasProvinceDTO;

@Repository("masProvinceDAO")
public class MasProvinceDAO extends BaseDAO{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3371427625753658645L;

	public MasProvinceDAO()
	{
		this.domainObj = MasProvince.class;
	}
	
	  @SuppressWarnings({ "unchecked", "rawtypes" })
	public List<MasProvince> findAll(MasProvinceDTO dto)
	  {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  MasProvince as dep ");
		hql.append(" where dep.recordStatus = 'N' ");
		

		if((dto.getProvinceName() != null) && (!dto.getProvinceName().isEmpty()))
		{
			hql.append(" and dep.provinceName like ? ");
			params.add("%"+dto.getProvinceName()+"%");
		}
		
		if((dto.getProvinceNameEn() != null) && (!dto.getProvinceNameEn().isEmpty()))
		{
			hql.append(" and dep.provinceNameEn like ? ");
			params.add("%"+dto.getProvinceNameEn()+"%");
		}
		
		hql.append(" order by dep.provinceName ");
		
		return (List<MasProvince>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	  }
}
