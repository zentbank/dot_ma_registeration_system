package sss.dot.tourism.dao.mas;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.Education;
import sss.dot.tourism.domain.MasUniversity;
@Repository("masUniversityDAO")
public class MasUniversityDAO extends BaseDAO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2513399553249359335L;

	public MasUniversityDAO()
	{
		this.domainObj = MasUniversity.class;
	}
	
	@SuppressWarnings("unchecked")
	public List<MasUniversity> findUniversityName(long masUniversityId) throws Exception
	{
		@SuppressWarnings("rawtypes")
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  MasUniversity as tr ");
		hql.append(" where tr.masUniversityId = ? ");
		hql.append(" order by tr.universityName ");
		params.add(masUniversityId);
		
		List<MasUniversity> list = (List<MasUniversity>) this
				.getHibernateTemplate().find(hql.toString(), params.toArray());
		
		return list;
	}
	
	public List<MasUniversity> findAll(){
		StringBuilder hql = new StringBuilder();
		hql.append(" from  MasUniversity as tr ");
		hql.append(" order by tr.universityName ");
		
		List<MasUniversity> list = (List<MasUniversity>) this.getHibernateTemplate().find(hql.toString());
		return list;
	}
}
