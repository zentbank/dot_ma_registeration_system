package sss.dot.tourism.dao.mas;

import java.util.List;

import org.hibernate.ObjectNotFoundException;
import org.springframework.orm.hibernate3.HibernateObjectRetrievalFailureException;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.Organization;

@Repository("organizationDAO")
public class OrganizationDAO extends BaseDAO{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1134083508458780677L;

	public OrganizationDAO()
	{
		this.domainObj = Organization.class;
	}
	
	public List<?> findAll(int a)
	  {
	    List<?> result = null;
	    try
	    {
	      String primaryKey = this.getHibernateTemplate().getSessionFactory().getClassMetadata(domainObj).getIdentifierPropertyName();

	      String hql = "from " + domainObj.getSimpleName() + " where orgId not in ('1','2','3','4','5','6')  order by " + primaryKey;

	      result = this.getHibernateTemplate().find(hql);

	    }
	    catch (ObjectNotFoundException e)
	    {
	      this.error(e.getMessage(), e);
	    }
	    catch (HibernateObjectRetrievalFailureException e)
	    {
	      this.error(e.getMessage(), e);
	    }
	    return result;
	  }

}
