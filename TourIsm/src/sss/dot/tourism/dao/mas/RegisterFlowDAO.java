package sss.dot.tourism.dao.mas;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.RegisterFlow;

@Repository("registerFlowDAO")
public class RegisterFlowDAO extends BaseDAO{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1727102814221049253L;

	public RegisterFlowDAO()
	{
		this.domainObj = RegisterFlow.class;
	}
	
	  @SuppressWarnings({ "unchecked", "rawtypes" })
	public RegisterFlow findNextRole(String role)
	  {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  RegisterFlow as dep ");
		hql.append(" where dep.registerRole = ? ");
		params.add(role);
		
		return (RegisterFlow)this.getHibernateTemplate().find(hql.toString(), params.toArray()).get(0);
	  }

}
