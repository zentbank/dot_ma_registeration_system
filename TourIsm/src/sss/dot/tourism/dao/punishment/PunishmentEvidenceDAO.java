package sss.dot.tourism.dao.punishment;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.PunishmentEvidence;

@Repository("punishmentEvidenceDAO")
public class PunishmentEvidenceDAO extends BaseDAO {
	public PunishmentEvidenceDAO()
	{
		this.domainObj = PunishmentEvidence.class;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<PunishmentEvidence> findByRevokeLicense(long revokeId)
	  {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  PunishmentEvidence as dep ");
		hql.append(" where dep.recordStatus = 'N' ");
		hql.append(" and dep.punishmentType = 'R' ");
		hql.append(" and dep.revokeLicense.revokeId = ? ");
		params.add(revokeId);
	
		
		return (List<PunishmentEvidence>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	  }
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<PunishmentEvidence> findBySuspensionLicense(long suspendId)
	  {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  PunishmentEvidence as dep ");
		hql.append(" where dep.recordStatus = 'N' ");
		hql.append(" and dep.punishmentType = 'S' ");
		hql.append(" and dep.suspensionLicense.suspendId = ? ");
		params.add(suspendId);
	
		
		return (List<PunishmentEvidence>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	  }
}
