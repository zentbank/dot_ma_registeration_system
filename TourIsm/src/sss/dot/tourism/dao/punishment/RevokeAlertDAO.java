package sss.dot.tourism.dao.punishment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.RevokeAlert;
import sss.dot.tourism.domain.SuspensionAlert;
import sss.dot.tourism.dto.punishment.SuspensionAlertDTO;

@Repository("revokeAlertDAO")
public class RevokeAlertDAO extends BaseDAO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1182601176899038911L;

	public RevokeAlertDAO()
	{
		this.domainObj = RevokeAlert.class;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<RevokeAlert> findByRevokeLicense(long revokeId)
	  {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  RevokeAlert as dep ");
		hql.append(" where dep.recordStatus = 'N' ");
		
		hql.append(" and dep.revokeLicense.revokeId = ? ");
		params.add(revokeId);
	
		
		return (List<RevokeAlert>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	  }
	
	public List<Object[]> findRevokeDetail(SuspensionAlertDTO param) throws Exception
	{
		Map<String,Object> parames = new HashMap<String,Object>();
	    
	    StringBuilder hql = new StringBuilder();
	    
	    hql.append(" SELECT ");
	    hql.append(" 	TR.TRADER_NAME AS TRADER_NAME, ");
	    hql.append(" 	TR.LICENSE_NO AS LICENSE_NO, ");
	    hql.append(" 	TR.TRADER_TYPE AS TRADER_TYPE, ");
	    hql.append(" 	PE.PERSON_TYPE AS PERSON_TYPE, ");
	    hql.append(" 	PRE.PREFIX_NAME AS PREFIX_NAME, ");
	    hql.append(" 	PO.POSTFIX_NAME AS POSTFIX_NAME, ");
	    hql.append(" 	PE.FIRST_NAME AS FIRST_NAME, ");
	    hql.append(" 	PE.LAST_NAME AS LAST_NAME, ");
	    hql.append(" 	MA.ACT_DESC AS ACT_DESC, ");
	    hql.append(" 	RL.REVOKE_DETAIL AS REVOKE_DETAIL, ");
	    hql.append(" 	RL.RECEIVE_NO AS RECEIVE_NO, ");
	    hql.append(" 	TR.TRADER_CATEGORY AS TRADER_CATEGORY ");
	    hql.append(" FROM ");
	    hql.append(" 	REVOKE_LICENSE RL ");
	    hql.append(" 		INNER JOIN ");
	    hql.append(" 		REVOKE_DETAIL RD ");
	    hql.append(" 		ON ");
	    hql.append(" 		RD.REVOKE_ID = RL.REVOKE_ID ");
	    hql.append(" 		INNER JOIN ");
	    hql.append(" 		REVOKE_MAP_TRADER RMT ");
	    hql.append(" 		ON ");
	    hql.append(" 		RMT.REVOKE_ID = RD.REVOKE_ID ");
	    hql.append(" 		INNER JOIN ");
	    hql.append(" 		TRADER TR ");
	    hql.append(" 		ON ");
	    hql.append(" 		TR.LICENSE_NO = RMT.LICENSE_NO AND ");
	    hql.append(" 		TR.TRADER_TYPE = RMT.TRADER_TYPE ");
	    hql.append(" 		INNER JOIN ");
	    hql.append(" 		MAS_ACT MA ");
	    hql.append(" 		ON ");
	    hql.append(" 		MA.MAS_ACT_ID = RD.ACT ");
	    hql.append(" 		INNER JOIN ");
	    hql.append(" 		PERSON PE ");
	    hql.append(" 		ON ");
	    hql.append(" 		PE.PERSON_ID = TR.PERSON_ID ");
	    hql.append(" 		LEFT JOIN ");
	    hql.append(" 		MAS_PREFIX PRE ");
	    hql.append(" 		ON ");
	    hql.append(" 		PRE.PREFIX_ID = PE.PREFIX_ID ");
	    hql.append(" 		LEFT JOIN ");
	    hql.append(" 		MAS_POSFIX PO ");
	    hql.append(" 		ON ");
	    hql.append(" 		PO.POSTFIX_ID = PE.POSTFIX_ID ");
	    hql.append(" WHERE ");
	    hql.append(" 	RL.REVOKE_STATUS = 'A' AND ");
	    hql.append(" 	RL.RECORD_STATUS = 'N' AND ");
	    hql.append(" 	TR.RECORD_STATUS = 'N' AND ");
	    
	    if(param.getRevokeId() > 0)
	    {
	    	hql.append(" RL.REVOKE_ID = :REVOKE_ID  ");
	    	parames.put("REVOKE_ID", param.getRevokeId());
	    }
	    
	    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
	    
	    sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING);
	    sqlQuery.addScalar("TRADER_TYPE", Hibernate.STRING);
	    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);
	    sqlQuery.addScalar("PREFIX_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("POSTFIX_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("FIRST_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("LAST_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("ACT_DESC", Hibernate.STRING);
	    sqlQuery.addScalar("REVOKE_DETAIL", Hibernate.STRING);
	    sqlQuery.addScalar("RECEIVE_NO", Hibernate.STRING);
	    sqlQuery.addScalar("TRADER_CATEGORY", Hibernate.STRING);
	  
	    
	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
  
	    return result;
	}
}
