package sss.dot.tourism.dao.punishment;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.MasAct;
import sss.dot.tourism.domain.MasProvince;
import sss.dot.tourism.domain.SuspensionDetail;
import sss.dot.tourism.dto.mas.MasProvinceDTO;

@Repository("suspensionDetailDAO")
public class SuspensionDetailDAO extends BaseDAO{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9124638530122990411L;

	public SuspensionDetailDAO()
	{
		this.domainObj = SuspensionDetail.class;
	}

	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<SuspensionDetail> findBySuspendLicense(long suspendId)
	  {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  SuspensionDetail as dep ");
		hql.append(" where dep.recordStatus = 'N' ");
		
		hql.append(" and dep.suspensionLicense.suspendId = ? ");
		params.add(suspendId);
	
		
		return (List<SuspensionDetail>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	  }
}
