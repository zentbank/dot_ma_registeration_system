package sss.dot.tourism.dao.punishment;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.sf.cglib.reflect.FastClass;
import net.sf.cglib.reflect.FastMethod;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.SuspensionDetail;
import sss.dot.tourism.domain.SuspensionMapTrader;

@Repository("suspensionMapTraderDAO")
public class SuspensionMapTraderDAO extends BaseDAO {

	public SuspensionMapTraderDAO() {
		this.domainObj = SuspensionMapTrader.class;
	}

	public Object insert(Object persistence) {

		return this.getHibernateTemplate().save(persistence);
	}

	/**
	 * 
	 * @see com.sss.dao.IBaseDAO#update(java.lang.Object)
	 */
	public void update(Object persistence) {
		this.getHibernateTemplate().update(persistence);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<SuspensionMapTrader> findBySuspendLicense(long suspendId) {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  SuspensionMapTrader as dep ");
		hql.append(" where dep.suspensionLicense.suspendId = ? ");

		params.add(suspendId);

		return (List<SuspensionMapTrader>) this.getHibernateTemplate().find(
				hql.toString(), params.toArray());
	}
}
