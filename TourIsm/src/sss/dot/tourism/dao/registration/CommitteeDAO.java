package sss.dot.tourism.dao.registration;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.Committee;
import sss.dot.tourism.domain.RegisterProgress;
import sss.dot.tourism.domain.TraderAddress;

@Repository("committeeDAO")
public class CommitteeDAO extends BaseDAO{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1827693195045408716L;

	public CommitteeDAO()
	{
		this.domainObj = Committee.class;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Committee> findAllByPerson(long personId,String committeeType, String recordStatus)
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  Committee as dep ");
		hql.append(" where  dep.person.personId = ? ");
		
		params.add(personId);
		
		if((recordStatus != null) && !(recordStatus.equals("")))
		{
			hql.append(" and dep.recordStatus = ? ");
			params.add(recordStatus);
		}
		
		if((committeeType != null) && !(committeeType.equals("")))
		{
			hql.append(" and dep.committeeType = ? ");
			params.add(committeeType);
		}
		
		return (List<Committee>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
	@SuppressWarnings("unchecked")
	public List<Committee> findRegisterProgressCommitteebyRegistration(String committeeIdentityNo,String committeePersonType)
			throws Exception {
		
		@SuppressWarnings("rawtypes")
		ArrayList params = new ArrayList();
		
		StringBuilder hql = new StringBuilder();
		hql.append(" select dep from  Committee as dep ");
		hql.append(" inner join dep.person pe ");
		
		
		hql.append(" where dep.committeeIdentityNo = ? ");
		params.add(committeeIdentityNo);
		
		if((null != committeePersonType ) && (!committeePersonType.isEmpty()))
		{
			hql.append(" and dep.committeePersonType = ? ");
			params.add(committeePersonType);
		}
		
		
		hql.append(" order by dep.committeeId asc ");

		List<Committee> list = (List<Committee>) this
				.getHibernateTemplate().find(hql.toString(), params.toArray());

		return list;
	}
}
