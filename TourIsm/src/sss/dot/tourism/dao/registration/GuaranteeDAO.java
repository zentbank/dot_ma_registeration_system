package sss.dot.tourism.dao.registration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import com.sss.aut.service.User;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.Guarantee;
import sss.dot.tourism.dto.registration.GuaranteeDTO;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.RecordStatus;

@Repository("guaranteeDAO")
public class GuaranteeDAO extends BaseDAO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GuaranteeDAO()
	{
		this.domainObj = Guarantee.class;
	}
	
	
	@SuppressWarnings("unchecked")
	public List findByTrader(String licenseNo, String traderType, long traderId) throws Exception
	{
		String recordStatus = "N";
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  Guarantee as ga ");
		hql.append(" where ga.recordStatus = ? ");
		params.add(RecordStatus.NORMAL.getStatus());
		hql.append(" and ga.guaranteeStatus = ? ");
		params.add(RecordStatus.NORMAL.getStatus());
		
		if((licenseNo != null) && (!(licenseNo.isEmpty())))
		{
			hql.append(" and ga.licenseNo = ? ");
			params.add(licenseNo);
		}
		
		if((traderType != null) && (!(traderType.isEmpty())))
		{
			hql.append(" and ga.traderType = ? ");
			params.add(traderType);
		}		
		
		if(traderId > 0)
		{
			hql.append(" and ga.trader.traderId = ? ");
			params.add(traderId);
		}
		

		return (List<Guarantee>)this.getHibernateTemplate().find(hql.toString(), params.toArray());	
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List findByTrader( long traderId) throws Exception
	{
		String recordStatus = "N";
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  Guarantee as ga ");
		hql.append(" where ga.recordStatus = ? ");
		params.add(RecordStatus.NORMAL.getStatus());
		hql.append(" and ga.guaranteeStatus = ? ");
		params.add(RecordStatus.NORMAL.getStatus());
		
			
		
		if(traderId > 0)
		{
			hql.append(" and ga.trader.traderId = ? ");
			params.add(traderId);
		}
		

		return (List<Guarantee>)this.getHibernateTemplate().find(hql.toString(), params.toArray());	
	}
	
	@SuppressWarnings("unchecked")
	public List findByTrader(String licenseNo, String traderType) throws Exception
	{
		
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  Guarantee as ga ");
		hql.append(" where ga.recordStatus = ? ");
		params.add(RecordStatus.NORMAL.getStatus());
		
		if((licenseNo != null) && (!(licenseNo.isEmpty())))  
		{
			hql.append(" and ga.licenseNo = ? ");
			params.add(licenseNo);
		}
		
		if((traderType != null) && (!(traderType.isEmpty())))
		{
			hql.append(" and ga.traderType = ? ");
			params.add(traderType);
		}	

		return (List<Guarantee>)this.getHibernateTemplate().find(hql.toString(), params.toArray());	
	}  
	
	@SuppressWarnings({ "unchecked", "rawtypes" }) 
	public List<Object[]> findGuaranteePaging(GuaranteeDTO param,int start, int limit,boolean isRefund)
	  { 
		   Map<String,Object> parames = new HashMap<String,Object>();
		    
		    StringBuilder hql = new StringBuilder();
	    
		    hql.append(" SELECT  ");
		    hql.append(" TR.TRADER_ID ");
		    hql.append(" ,TR.TRADER_NAME ");
		    hql.append(" ,TR.LICENSE_NO ");
		    hql.append(" ,TR.LICENSE_STATUS ");
		    hql.append(" ,TR.TRADER_CATEGORY ");
		    hql.append(" ,GU.GUARANTEE_ID ");
		    hql.append(" ,GU.GUARANTEE_STATUS ");
		    hql.append(" ,GU.GUARANTEE_TYPE ");
		    hql.append(" ,GU.ACCOUNT_ID ");
		    hql.append(" ,GU.ACCOUNT_NAME ");
		    hql.append(" ,GU.BANK_ID ");
		    hql.append(" ,BK.BANK_NAME ");
		    hql.append(" ,GU.BANK_BRANCH_NAME ");
		    hql.append(" ,GU.ACCOUNT_EXPIRE_DATE ");
		    hql.append(" ,GU.GUARANTEE_MNY ");
		    hql.append(" ,GU.GUARANTEE_INTEREST ");
		    hql.append(" ,GU.REFUND_NAME ");
		    hql.append(" ,GU.REFUND_TYPE ");
		    hql.append(" ,GU.REFUND_MNY ");
		    hql.append(" ,GU.REFUND_INTEREST ");
		    hql.append(" ,GU.REFUND_DATE ");
		    hql.append(" ,GU.REFUND_REMARK ");
		    
		    
		    hql.append(" ,TR.TRADER_TYPE ");
		    
		    hql.append(" ,PE.PERSON_ID ");
		    hql.append(" ,PX.PREFIX_NAME ");
		    hql.append(" ,PE.FIRST_NAME ");
		    hql.append(" ,PE.LAST_NAME ");
		    hql.append(" ,PO.POSTFIX_NAME ");
		    hql.append(" ,PE.PERSON_TYPE ");
		    hql.append(" ,PE.IDENTITY_NO ");
		    
		    hql.append(" ,TR.TRADER_NAME_EN ");
		    
		    hql.append(" ,GU.CASH_ACCOUNT_ID ");
		    hql.append(" ,GU.CASH_ACCOUNT_NAME ");
		    hql.append(" ,GU.CASH_ACCOUNT_BANK_ID ");
		    hql.append(" ,GU.CASH_ACCOUNT_BANK_BRANCH_NAME ");
		    hql.append(" ,GU.CASH_ACCOUNT_MNY ");
		    
		    hql.append(" ,GU.BANK_GUARANTEE_ID ");
		    hql.append(" ,GU.BANK_GUARANTEE_NAME ");
		    hql.append(" ,GU.BANK_GUARANTEE_BANK_ID ");
		    hql.append(" ,GU.BANK_GUARANTEE_MNY ");
		    
		    hql.append(" ,GU.GOVERNMENT_BOND_ID ");   
		    hql.append(" ,GU.GOVERNMENT_BOND_EXPIRE_DATE ");
		    hql.append(" ,GU.GOVERNMENT_BOND_MNY ");
		    hql.append(" ,GU.RECEIVE_DATE ");
		    hql.append(" ,GU.RECEIVE_NAME ");

		    
		    hql.append(" FROM TRADER TR ");
		    hql.append(" INNER JOIN GUARANTEE GU ");
		    hql.append(" ON((GU.LICENSE_NO = TR.LICENSE_NO) AND (TR.TRADER_TYPE = GU.TRADER_TYPE)) ");
		    hql.append(" LEFT JOIN MAS_BANK BK ");
		    hql.append(" ON (BK.MAS_BANK_ID = GU.BANK_ID) ");
		    
		    hql.append(" INNER JOIN PERSON PE ");
		    hql.append(" ON (PE.PERSON_ID = TR.PERSON_ID) ");
		    
		    hql.append(" LEFT JOIN MAS_PREFIX PX ");
		    hql.append(" ON(PE.PREFIX_ID = PX.PREFIX_ID) ");
		    hql.append(" LEFT JOIN MAS_POSFIX PO ");
		    hql.append(" ON(PE.POSTFIX_ID = PO.POSTFIX_ID ) ");
		    
//Oat Add 09/06/58
			 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
			 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Add 09/06/58
		    
		    hql.append(" WHERE TR.BRANCH_TYPE IS NULL ");
		    
		    if(isRefund)
		    {
		    	hql.append(" AND TR.LICENSE_STATUS IN ('R', 'C') ");
		    }
		    else
		    {
		    	hql.append(" AND TR.LICENSE_STATUS = 'N' ");
		    	hql.append(" AND GU.GUARANTEE_STATUS = 'N' ");
		    }
		    
		    
		    hql.append(" AND TR.RECORD_STATUS = 'N' ");
		    hql.append(" AND GU.RECORD_STATUS = 'N' ");
		   
		    
		    
		    if((param.getTraderType() != null) && (!param.getTraderType().isEmpty()))
		    {
		    	hql.append("  AND TR.TRADER_TYPE = :TRADER_TYPE ");
		    	parames.put("TRADER_TYPE", param.getTraderType());
		    }
		    
		    if((param.getTraderName() != null) && (!param.getTraderName().isEmpty()))
		    {
		    	hql.append("  AND TR.TRADER_NAME LIKE :TRADER_NAME ");
		    	parames.put("TRADER_NAME", "%"+param.getTraderName()+"%");
		    }
		    
		    if((param.getIdentityNo() != null) && (!param.getIdentityNo().isEmpty()))
		    {
		    	hql.append("  AND PE.IDENTITY_NO = :IDENTITY_NO ");
		    	parames.put("IDENTITY_NO", param.getIdentityNo());
		    }
		    if((param.getFirstName() != null) && (!param.getFirstName().isEmpty()))
		    {
		    	hql.append("  AND PE.FIRST_NAME LIKE :FIRST_NAME ");
		    	parames.put("FIRST_NAME", "%"+param.getFirstName()+"%");
		    }
		    if((param.getLastName() != null) && (!param.getLastName().isEmpty()))
		    {
		    	hql.append("  AND PE.LAST_NAME LIKE :LAST_NAME ");
		    	parames.put("LAST_NAME","%"+param.getLastName()+"%");
		    }		    
		
		    
		    if((param.getLicenseNo() != null) && (!param.getLicenseNo().isEmpty()))
		    {
		    	hql.append("  AND TR.LICENSE_NO = :LICENSE_NO ");
		    	parames.put("LICENSE_NO", param.getLicenseNo());
		    }	
		    
//Oat Edit 08/05/58
			 hql.append(" AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS = 'N' ");
			 
			 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
			 parames.put("TRADER_ORG_ID", param.getOrgId());
			 
//			    if(param.getOrgId() > 0)
//			    {
//			    	hql.append(" AND TR.ORG_ID = :ORG_ID ");
//			    	parames.put("ORG_ID", param.getOrgId());
//			    }
		 
//End Oat Edit 08/05/58
		    
		    if((param.getGuaranteeStatus() != null) && (!(param.getGuaranteeStatus().isEmpty())))
		    {
		    	hql.append(" AND GU.GUARANTEE_STATUS = :GUARANTEE_STATUS ");
		    	parames.put("GUARANTEE_STATUS", param.getGuaranteeStatus());		    	
		    }
		    
		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
		    sqlQuery.setFirstResult(start);
		    sqlQuery.setMaxResults(limit);
		    
			
		    sqlQuery.addScalar("TRADER_ID", Hibernate.LONG); //0
		    sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING); //1
		    sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING); //2
		    sqlQuery.addScalar("LICENSE_STATUS", Hibernate.STRING);//3
		    sqlQuery.addScalar("TRADER_CATEGORY", Hibernate.STRING);//4
		    
		    sqlQuery.addScalar("GUARANTEE_ID", Hibernate.LONG);//5
		    sqlQuery.addScalar("GUARANTEE_STATUS", Hibernate.STRING);//6
		    sqlQuery.addScalar("GUARANTEE_TYPE", Hibernate.STRING);//7
		    sqlQuery.addScalar("ACCOUNT_ID", Hibernate.STRING);//8
		    sqlQuery.addScalar("ACCOUNT_NAME", Hibernate.STRING);//9
		    sqlQuery.addScalar("BANK_ID", Hibernate.LONG);//10
		    sqlQuery.addScalar("BANK_NAME", Hibernate.STRING);//11
		    sqlQuery.addScalar("BANK_BRANCH_NAME", Hibernate.STRING);//12
		    sqlQuery.addScalar("ACCOUNT_EXPIRE_DATE", Hibernate.DATE);//13
		    sqlQuery.addScalar("GUARANTEE_MNY", Hibernate.BIG_DECIMAL);//14
		    sqlQuery.addScalar("GUARANTEE_INTEREST", Hibernate.BIG_DECIMAL);//15
		    
		    sqlQuery.addScalar("REFUND_NAME", Hibernate.STRING);//16
		    sqlQuery.addScalar("REFUND_TYPE", Hibernate.STRING);//17
		    sqlQuery.addScalar("REFUND_MNY", Hibernate.BIG_DECIMAL);//18
		    sqlQuery.addScalar("REFUND_INTEREST", Hibernate.BIG_DECIMAL);//19
		    sqlQuery.addScalar("REFUND_DATE", Hibernate.DATE);//20
		    sqlQuery.addScalar("REFUND_REMARK", Hibernate.STRING);//21
		    
		    sqlQuery.addScalar("TRADER_TYPE", Hibernate.STRING);//22
		    
		    sqlQuery.addScalar("PERSON_ID", Hibernate.LONG);//23
		    sqlQuery.addScalar("PREFIX_NAME", Hibernate.STRING);//24
		    sqlQuery.addScalar("FIRST_NAME", Hibernate.STRING);//25
		    sqlQuery.addScalar("LAST_NAME", Hibernate.STRING);//26
		    sqlQuery.addScalar("POSTFIX_NAME", Hibernate.STRING);//27
		    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);//28
		    sqlQuery.addScalar("IDENTITY_NO", Hibernate.STRING);//29
		    
		    sqlQuery.addScalar("TRADER_NAME_EN", Hibernate.STRING);//30
		    
		    sqlQuery.addScalar("CASH_ACCOUNT_ID", Hibernate.STRING);//31
		    sqlQuery.addScalar("CASH_ACCOUNT_NAME", Hibernate.STRING);//32
		    sqlQuery.addScalar("CASH_ACCOUNT_BANK_ID", Hibernate.LONG);//33
		    sqlQuery.addScalar("CASH_ACCOUNT_BANK_BRANCH_NAME", Hibernate.STRING);//34
		    sqlQuery.addScalar("CASH_ACCOUNT_MNY", Hibernate.BIG_DECIMAL);//35
		    
		    sqlQuery.addScalar("BANK_GUARANTEE_ID", Hibernate.STRING);//36
		    sqlQuery.addScalar("BANK_GUARANTEE_NAME", Hibernate.STRING);//37
		    sqlQuery.addScalar("BANK_GUARANTEE_BANK_ID", Hibernate.LONG);//38
		    sqlQuery.addScalar("BANK_GUARANTEE_MNY", Hibernate.BIG_DECIMAL);//39
		    
		    sqlQuery.addScalar("GOVERNMENT_BOND_ID", Hibernate.STRING);//40
		    sqlQuery.addScalar("GOVERNMENT_BOND_EXPIRE_DATE", Hibernate.DATE);//41
		    sqlQuery.addScalar("GOVERNMENT_BOND_MNY", Hibernate.BIG_DECIMAL);//42
		    
		    sqlQuery.addScalar("RECEIVE_DATE", Hibernate.DATE);//43
		    sqlQuery.addScalar("RECEIVE_NAME", Hibernate.STRING);//44
		    
		    
		    sqlQuery.setProperties(parames);
		    List<Object[]>  result = sqlQuery.list();
	  
		    return result;
	  }
	
	public List<Object[]> findGuaranteeRefund(GuaranteeDTO dto, User user) throws Exception
	{
		 Map<String,Object> parames = new HashMap<String,Object>();
		    
		    StringBuilder hql = new StringBuilder();
		    hql.append(" select TR.PUNISHMENT_DATE AS PUNISHMENT_DATE, ");
		    hql.append(" GA.GUARANTEE_MNY AS GUARANTEE_MNY, ");
		    hql.append(" GA.GUARANTEE_ID AS GUARANTEE_ID ");
		    hql.append(" from TRADER TR ");
		    hql.append(" inner join GUARANTEE GA on GA.TRADER_TYPE = TR.TRADER_TYPE and GA.LICENSE_NO = TR.LICENSE_NO ");
		    
//Oat Add 03/06/58
			 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
			 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Add 03/06/58
		    
		    hql.append(" where TR.LICENSE_STATUS in ('C','R') ");
		    hql.append(" and GA.RECORD_STATUS = 'N' ");
		    hql.append(" and GA.GUARANTEE_STATUS = 'N' ");
		    hql.append(" and TR.RECORD_STATUS = 'N' ");
		    
		    if((null != dto.getDate()) && (!dto.getDate().equals("")))
		    {
		    	//Oat Edit
//		  		hql.append("  and (convert(datetime, :RE_START_DATE ,20)) - TR.PUNISHMENT_DATE = 731 ");
		   		  		
		    	hql.append("  and ISNULL(TR.PUNISHMENT_DATE, :RE_START_DATE)  = :RE_END_DATE ");
		  		
				String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getDate(), "00:00");
				parames.put("RE_START_DATE", sendDtmFrom);
				
				//วันที่ย้อนหลังไปสองปี
				String d = dto.getDate();
				
				String[] arr = d.split("/");
				
				String yearStr = arr[2];
				
				int year = Integer.parseInt(yearStr)-2;
				
				String dateTo = arr[0]+"/"+arr[1]+"/"+year;
				
				System.out.println("#####dateTo = "+dateTo);
				//
				
				String sendDtmTo = DateUtils.getDateMSSQLFormat(dateTo, "00:00");
				parames.put("RE_END_DATE", sendDtmTo);
				
				
//Oat Add 03/06/58
				 hql.append(" AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS = 'N' ");
				 
				 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
				 parames.put("TRADER_ORG_ID", user.getUserData().getOrgId());
				 			 
//End Oat Add 03/06/58
				
			    System.out.println("#####sendDtmFrom = "+sendDtmFrom);
			    System.out.println("#####sendDtmTo = "+sendDtmTo);
			    
		    	//End Oat Edit
		    }
		
		    System.out.println("#####hql findGuaranteeRefund = "+hql);
		    
//		    System.out.println("#####hql.toString() = "+hql.toString());
		  		    
		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
		    sqlQuery.addScalar("PUNISHMENT_DATE", Hibernate.DATE);//0
		    sqlQuery.addScalar("GUARANTEE_MNY", Hibernate.BIG_DECIMAL);//1
		    sqlQuery.addScalar("GUARANTEE_ID", Hibernate.LONG);//2
		    sqlQuery.setProperties(parames);
		    List<Object[]>  result = sqlQuery.list();
		    
		    System.out.println("#####result.size() = "+result.size());
		    
		return result;	
	}
	public List<Object[]> findGuaranteeRefund(GuaranteeDTO dto,long orgId) throws Exception
	{
		 Map<String,Object> parames = new HashMap<String,Object>();
		    
		    StringBuilder hql = new StringBuilder();
		    hql.append(" select TR.PUNISHMENT_DATE AS PUNISHMENT_DATE, ");
		    hql.append(" GA.GUARANTEE_MNY AS GUARANTEE_MNY, ");
		    hql.append(" GA.GUARANTEE_ID AS GUARANTEE_ID ");
		    hql.append(" from TRADER TR ");
		    hql.append(" inner join GUARANTEE GA on GA.TRADER_TYPE = TR.TRADER_TYPE and GA.LICENSE_NO = TR.LICENSE_NO ");
		    
//Oat Add 03/06/58
			 hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
			 hql.append(" LEFT JOIN MAS_PROVINCE P ON (TA.PROVINCE_ID = P.PROVINCE_ID) ");
//End Oat Add 03/06/58
		    
		    hql.append(" where TR.LICENSE_STATUS in ('C','R') ");
		    hql.append(" and GA.RECORD_STATUS = 'N' ");
		    hql.append(" and GA.GUARANTEE_STATUS = 'N' ");
		    hql.append(" and TR.RECORD_STATUS = 'N' ");
		    
		    if((null != dto.getDate()) && (!dto.getDate().equals("")))
		    {
		    	//Oat Edit
//		  		hql.append("  and (convert(datetime, :RE_START_DATE ,20)) - TR.PUNISHMENT_DATE = 731 ");
		   		  		
		    	hql.append("  and ISNULL(TR.PUNISHMENT_DATE, :RE_START_DATE)  = :RE_END_DATE ");
		  		
				String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getDate(), "00:00");
				parames.put("RE_START_DATE", sendDtmFrom);
				
				//วันที่ย้อนหลังไปสองปี
				String d = dto.getDate();
				
				String[] arr = d.split("/");
				
				String yearStr = arr[2];
				
				int year = Integer.parseInt(yearStr)-2;
				
				String dateTo = arr[0]+"/"+arr[1]+"/"+year;
				
				System.out.println("#####dateTo = "+dateTo);
				//
				
				String sendDtmTo = DateUtils.getDateMSSQLFormat(dateTo, "00:00");
				parames.put("RE_END_DATE", sendDtmTo);
				
				
//Oat Add 03/06/58
				 hql.append(" AND TA.ADDRESS_TYPE = 'O' AND TA.RECORD_STATUS = 'N' ");
				 
				 hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :TRADER_ORG_ID ) ");
				 parames.put("TRADER_ORG_ID", orgId);
				 			 
//End Oat Add 03/06/58
				
			    System.out.println("#####sendDtmFrom = "+sendDtmFrom);
			    System.out.println("#####sendDtmTo = "+sendDtmTo);
			    
		    	//End Oat Edit
		    }
		
		    System.out.println("#####hql findGuaranteeRefund = "+hql);
		    
//		    System.out.println("#####hql.toString() = "+hql.toString());
		  		    
		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
		    sqlQuery.addScalar("PUNISHMENT_DATE", Hibernate.DATE);//0
		    sqlQuery.addScalar("GUARANTEE_MNY", Hibernate.BIG_DECIMAL);//1
		    sqlQuery.addScalar("GUARANTEE_ID", Hibernate.LONG);//2
		    sqlQuery.setProperties(parames);
		    List<Object[]>  result = sqlQuery.list();
		    
		    System.out.println("#####result.size() = "+result.size());
		    
		return result;	
	}
	
}
