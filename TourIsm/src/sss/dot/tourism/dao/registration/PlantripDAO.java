package sss.dot.tourism.dao.registration;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.Plantrip;
import sss.dot.tourism.domain.Trader;

@Repository("plantripDAO")
public class PlantripDAO extends BaseDAO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3747241781541418698L;

	public PlantripDAO()
	{
		this.domainObj = Plantrip.class;
	}
	
	@SuppressWarnings("unchecked")
	public List<Plantrip> findPlanTripbyTrader(long traderId,String recordStatus)
			throws Exception {
		
		@SuppressWarnings("rawtypes")
		ArrayList params = new ArrayList();
		
		StringBuilder hql = new StringBuilder();
		hql.append(" from  Plantrip as tr ");
		
		hql.append(" where tr.trader.traderId = ? ");
		params.add(traderId);
		
		if((null != recordStatus) && (!recordStatus.isEmpty()))
		{
			hql.append(" and tr.recordStatus = ? ");
			params.add(recordStatus);
		}
		
		
		
		

		List<Plantrip> list = (List<Plantrip>) this
				.getHibernateTemplate().find(hql.toString(), params.toArray());

		return list;
	}
}
