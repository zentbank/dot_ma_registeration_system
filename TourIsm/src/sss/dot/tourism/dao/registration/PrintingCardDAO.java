package sss.dot.tourism.dao.registration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import com.sss.aut.service.User;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.PrintingCard;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.util.DateUtils;

@Repository("printingCardDAO")
public class PrintingCardDAO extends BaseDAO{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6892707192687537766L;
	public PrintingCardDAO()
	{
		this.domainObj = PrintingCard.class;
	}
	 public Object insert(Object persistence)
	  {
	    
	    return this.getHibernateTemplate().save(persistence);
	  }
	  /**
	   * 
	   * @see com.sss.dao.IBaseDAO#update(java.lang.Object)
	   */
	  public void update(Object persistence)
	  {
	    this.getHibernateTemplate().update(persistence);
	  }
	  
	  public List<Object[]> findPrintCardAll(RegistrationDTO dto, User user ,int start, int limit)throws Exception
	  {
		  Map<String,Object> parames = new HashMap<String,Object>();
		    
		  StringBuilder hql = new StringBuilder();
		  
		  hql.append(" SELECT  ");
		  hql.append(" PC.PRINT_CARD_ID  ");
		  hql.append(" ,PC.PRINT_CARD_DATE ");
		  hql.append(" ,PC.PRINT_CARD_STATUS ");
		  hql.append(" ,REG.REG_ID ");
		  hql.append(" ,REG.REGISTRATION_TYPE ");
		  hql.append(" ,REG.APPROVE_DATE ");
		  hql.append(" ,P.PERSON_ID ");
		  hql.append(" ,P.FIRST_NAME ");
		  hql.append(" ,P.LAST_NAME ");
		  hql.append(" ,P.FIRST_NAME_EN ");
		  hql.append(" ,P.LAST_NAME_EN ");
		  hql.append(" ,P.IDENTITY_NO ");
		  hql.append(" ,CASE WHEN MSP.PREFIX_NAME = 'นางสาว' THEN 'น.ส.' ELSE MSP.PREFIX_NAME END AS PREFIX_NAME ");
		  hql.append(" ,MSP.PREFIX_NAME_EN ");
		  hql.append(" ,TR.TRADER_ID ");
		  hql.append(" ,TR.LICENSE_NO ");
		  hql.append(" ,TR.TRADER_CATEGORY ");
		  hql.append(" ,TR.TRADER_TYPE ");
		  hql.append(" ,TR.EFFECTIVE_DATE ");
		  hql.append(" ,TR.EXPIRE_DATE ");
		  hql.append(" ,TR.TRADER_AREA ");
		  hql.append(" ,PRO.PROVINCE_NAME ");
		  hql.append(" ,TR.ORG_ID ");
		  
		  hql.append(" FROM PRINTING_CARD PC ");
		  hql.append(" INNER JOIN REGISTRATION REG ");
		  hql.append(" ON(REG.REG_ID = PC.REG_ID) ");
		  hql.append(" INNER JOIN TRADER TR ");
		  hql.append(" ON(TR.TRADER_ID = REG.TRADER_ID) ");
		  hql.append(" INNER JOIN PERSON P ");
		  hql.append(" ON(P.PERSON_ID = TR.PERSON_ID) ");
		  hql.append(" LEFT JOIN MAS_PREFIX MSP ");
		  hql.append(" ON(P.PREFIX_ID = MSP.PREFIX_ID) ");
		  hql.append(" LEFT JOIN MAS_PROVINCE PRO ");
		  hql.append(" ON (PRO.PROVINCE_ID = TR.PROVINCE_ID) ");
		  hql.append(" WHERE REG.APPROVE_STATUS = 'A' ");
//		  hql.append(" AND (REG.APPROVE_DATE >= '04/01/2014' AND REG.APPROVE_DATE < '04/02/2014') ");
		  
		  System.out.println("traderType: "+dto.getTraderType());
		  System.out.println("orgId: "+dto.getOrgId());
		  System.out.println("-------------------------------------------");
		  System.out.println("firstName: "+dto.getFirstName());
		  System.out.println("-------------------------------------------");
		  if(dto.getOrgId() > 0)
		  {	  
			  hql.append("  AND REG.ORG_ID = :ORG_ID ");
			  parames.put("ORG_ID", dto.getOrgId());
		  }
		  
		  //Oat Add 22/12/58
		  hql.append(" AND TR.RECORD_STATUS IN("+"'N','H'"+") ");
		  
	    if((null != dto.getFirstName()) && (!"".equals(dto.getFirstName())))
	    {
		    hql.append(" AND P.FIRST_NAME LIKE :FIRST_NAME ");
		    
		    parames.put("FIRST_NAME", "%"+dto.getFirstName()+"%");
	    }
	    if((null != dto.getLastName()) && (!"".equals(dto.getLastName())))
	    {
		    hql.append(" AND P.LAST_NAME LIKE :LAST_NAME ");
		    
		    parames.put("LAST_NAME", "%"+dto.getLastName()+"%");
	    }
	    
	    if((null != dto.getIdentityNo()) && (!"".equals(dto.getIdentityNo())))
	    {
		    hql.append(" AND P.IDENTITY_NO LIKE :IDENTITY_NO ");
		    
		    parames.put("IDENTITY_NO", "%"+dto.getIdentityNo()+"%");
	    }
	    
	    if((null != dto.getPrintCardStatus()) && (!"".equals(dto.getPrintCardStatus())))
	    {
		    hql.append("AND PC.PRINT_CARD_STATUS = :PRINT_CARD_STATUS ");
		    
		    parames.put("PRINT_CARD_STATUS", dto.getPrintCardStatus());
	    }
	    
		  
		  if((null != dto.getLicenseNoFrom() &&(!dto.getLicenseNoFrom().equals(""))))
		    {
		    	if((null != dto.getLicenseNoTo() &&(!dto.getLicenseNoTo().equals(""))))
		    	{
		    		hql.append(" AND (  TR.LICENSE_NO BETWEEN :LICENSE_NO ");
		    		parames.put("LICENSE_NO",dto.getLicenseNoFrom());
		    		hql.append(" AND :LICENSE_NO1 ) ");
		    		parames.put("LICENSE_NO1",dto.getLicenseNoTo());
		    	}else
		    	{
		    		hql.append(" AND TR.LICENSE_NO = :LICENSE_NO");
		    		parames.put("LICENSE_NO",dto.getLicenseNoFrom());
		    	}
		    	
		    }else if((null != dto.getLicenseNoTo() &&(!dto.getLicenseNoTo().equals(""))))
		    {
		    	if((null != dto.getLicenseNoFrom() &&(!dto.getLicenseNoFrom().equals(""))))
		    	{
		    		hql.append(" AND (  TR.LICENSE_NO BETWEEN :LICENSE_NO ");
		    		parames.put("LICENSE_NO",dto.getLicenseNoFrom());
		    		hql.append(" AND :LICENSE_NO1 ) ");
		    		parames.put("LICENSE_NO1",dto.getLicenseNoTo());
		    	}else
		    	{
		    		hql.append(" AND TR.LICENSE_NO = :LICENSE_NO");
		    		parames.put("LICENSE_NO",dto.getLicenseNoTo());
		    	}
		    }
		  
		    if((null != dto.getApproveDateFrom()) && (!dto.getApproveDateFrom().equals("")))
		    {
		    	if((null != dto.getApproveDateTo()) && (!dto.getApproveDateTo().equals("")))
		    	{
					hql.append("  AND ( REG.APPROVE_DATE >=  convert(datetime, :APPROVE_START_DATE ,20) ");
					hql.append(" AND REG.APPROVE_DATE <  convert(datetime, :APPROVE_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
						
					parames.put("APPROVE_START_DATE", sendDtmFrom);
					parames.put("APPROVE_END_DATE", sendDtmTo);
		    	}
		    }
		    
		    if((null != dto.getTraderType()) && (!"".equals(dto.getTraderType())))
		    {
		    	 hql.append(" AND TR.TRADER_TYPE = :TRADER_TRADER_TYPE ");
				    
				 parames.put("TRADER_TRADER_TYPE", dto.getTraderType());
		    }
		    if((null != dto.getTraderCategory()) && (!"".equals(dto.getTraderCategory())))
		    {
		    	 hql.append(" AND TR.TRADER_CATEGORY = :TRADER_TRADER_CATEGORY ");
				    
				 parames.put("TRADER_TRADER_CATEGORY", dto.getTraderCategory());
		    }
			
		    if((null != dto.getRegistrationType()) && (!"".equals(dto.getRegistrationType())))
		    {
			    hql.append(" AND REG.REGISTRATION_TYPE = :REGISTRATION_REGISTRATION_TYPE ");
			    
			    parames.put("REGISTRATION_REGISTRATION_TYPE", dto.getRegistrationType());
		    }
		    
		    if(dto.getPrintCardId() > 0)
			  {	  
				  hql.append("  AND PC.PRINT_CARD_ID = :PRINT_CARD_ID ");
				  parames.put("PRINT_CARD_ID", dto.getPrintCardId());
			  }
		    
		    hql.append("  ORDER BY TR.LICENSE_NO ASC ");			
		    System.out.println(hql.toString());
		    
		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
		    sqlQuery.setFirstResult(start);
		    sqlQuery.setMaxResults(limit);
		    
			  
		    
			sqlQuery.addScalar("PRINT_CARD_ID", Hibernate.INTEGER); //0
			sqlQuery.addScalar("PRINT_CARD_DATE", Hibernate.DATE); //1
		    sqlQuery.addScalar("PRINT_CARD_STATUS", Hibernate.STRING); //2
		    sqlQuery.addScalar("REG_ID", Hibernate.LONG); //3
		    sqlQuery.addScalar("REGISTRATION_TYPE", Hibernate.STRING);//4
		    sqlQuery.addScalar("APPROVE_DATE", Hibernate.DATE);//5
		    sqlQuery.addScalar("PERSON_ID", Hibernate.LONG);//6
		    sqlQuery.addScalar("FIRST_NAME", Hibernate.STRING); //7
		    sqlQuery.addScalar("LAST_NAME", Hibernate.STRING);//8
		    
		    sqlQuery.addScalar("FIRST_NAME_EN", Hibernate.STRING);//9
		    sqlQuery.addScalar("LAST_NAME_EN", Hibernate.STRING);//10
		    sqlQuery.addScalar("IDENTITY_NO", Hibernate.STRING);//11
		    sqlQuery.addScalar("PREFIX_NAME", Hibernate.STRING);//12
		    sqlQuery.addScalar("PREFIX_NAME_EN", Hibernate.STRING);//13
		    
		    sqlQuery.addScalar("TRADER_ID", Hibernate.LONG);//14
		    sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING);//15
		    sqlQuery.addScalar("TRADER_CATEGORY", Hibernate.STRING);//16
		    sqlQuery.addScalar("TRADER_TYPE", Hibernate.STRING);//17
		    sqlQuery.addScalar("EFFECTIVE_DATE", Hibernate.DATE);//18
		    sqlQuery.addScalar("EXPIRE_DATE", Hibernate.DATE);//19
		    sqlQuery.addScalar("TRADER_AREA", Hibernate.STRING);//20
		    sqlQuery.addScalar("PROVINCE_NAME", Hibernate.STRING);//21
		    sqlQuery.addScalar("ORG_ID", Hibernate.LONG);//22

		    sqlQuery.setProperties(parames);
		    List<Object[]>  result = sqlQuery.list();
		  
		  return result;
	  }
	  
	  public List<Object[]> findPrintCardPaging(RegistrationDTO dto, User user ,int start, int limit)throws Exception
		{
			Map<String,Object> parames = new HashMap<String,Object>();
		    
		    StringBuilder hql = new StringBuilder();
		    
		    hql.append(" 	    SELECT PIC.REG_ID AS REG_ID, ");
		    hql.append(" 	    RE.REGISTRATION_NO AS REGISTRATION_NO, ");
		    hql.append(" 	    RE.REGISTRATION_DATE AS REGISTRATION_DATE, ");
		    hql.append(" 	    RE.REGISTRATION_PROGRESS AS REGISTRATION_PROGRESS, ");
		    hql.append(" 	    RE.REGISTRATION_TYPE AS REGISTRATION_TYPE, ");
		    hql.append(" 	    RE.ORG_ID AS ORG_ID, ");
		    hql.append(" 	    RE.RECORD_STATUS AS RECORD_STATUS, ");
		    hql.append(" 	    TR.TRADER_ID AS TRADER_ID, ");
		    hql.append(" 	    TR.TRADER_TYPE AS TRADER_TYPE, ");
		    hql.append(" 	    TR.TRADER_NAME AS TRADER_NAME, ");
		    hql.append(" 	    TR.TRADER_NAME_EN AS TRADER_NAME_EN, ");
		    hql.append(" 	    TR.TRADER_CATEGORY AS TRADER_CATEGORY, ");
		    hql.append(" 	    PE.PERSON_ID AS PERSON_ID, ");
		    hql.append(" 	    PE.PERSON_TYPE AS PERSON_TYPE, ");
		    hql.append(" 	    MP.PREFIX_NAME AS PREFIX_NAME, ");
		    hql.append(" 	    PE.FIRST_NAME AS FIRST_NAME, ");
		    hql.append(" 	    PE.LAST_NAME AS LAST_NAME, ");
		    hql.append(" 	    POSF.POSTFIX_NAME AS POSTFIX_NAME, ");
		    hql.append(" 	    MP.PREFIX_ID AS PREFIX_ID, ");
		    hql.append(" 	    POSF.POSTFIX_ID AS POSTFIX_ID, ");
		    hql.append(" 	    PEAMP.AMPHUR_ID AS PERSON_AMPHUR_ID, ");
		    hql.append(" 	    TAXPEAMP.AMPHUR_ID AS PERSON_TAXAMPHUR_ID, ");
		    hql.append(" 	    PEPROV.PROVINCE_ID AS PERSON_PROVINCE_ID, ");
		    hql.append(" 	    TAXPEPROV.PROVINCE_ID AS PERSON_TAXPROVINCE_ID, ");
		    hql.append(" 	    TR.TRADER_GUIDE_ID AS TRADER_GUIDE_ID, ");
		    hql.append(" 	    TR.PRONUNCIATION_NAME AS PRONUNCIATION_NAME, ");
		    hql.append(" 	    TR.LICENSE_GUIDE_NO AS LICENSE_GUIDE_NO, ");
		    hql.append(" 	    PE.PERSON_TYPE AS PERSON_TYPE, ");
		    hql.append(" 	    PE.IDENTITY_NO AS IDENTITY_NO, ");
		    hql.append(" 	    PE.COMMITTEE_NAME1 AS COMMITTEE_NAME1, ");
		    hql.append(" 	    PE.COMMITTEE_NAME2 AS COMMITTEE_NAME2, ");
		    hql.append(" 	    PE.COMMITTEE_NAME_SIGN AS COMMITTEE_NAME_SIGN, ");
		    hql.append(" 	    PE.PASSPORT_NO AS PASSPORT_NO, ");
		    hql.append(" 	    PE.FIRST_NAME_EN AS FIRST_NAME_EN, ");
		    hql.append(" 	    PE.LAST_NAME_EN AS LAST_NAME_EN, ");
		    hql.append(" 	    PE.GENDER AS GENDER, ");
		    hql.append(" 	    PE.PERSON_NATIONALITY AS PERSON_NATIONALITY, ");
		    hql.append(" 	    PE.BIRTH_DATE AS BIRTH_DATE, ");
		    hql.append(" 	    PE.AGE_YEAR AS AGE_YEAR, ");
		    hql.append(" 	    PE.IDENTITY_NO_EXPIRE AS IDENTITY_NO_EXPIRE, ");
		    hql.append(" 	    PE.CORPORATE_TYPE AS CORPORATE_TYPE, ");
		    hql.append(" 	    PE.TAX_IDENTITY_NO AS TAX_IDENTITY_NO, ");
		    hql.append(" 	    PE.IDENTITY_DATE AS IDENTITY_DATE, ");
		    hql.append(" 	    TR.RECORD_STATUS AS RECORD_STATUS, ");
		    hql.append(" 	    PEAMP.AMPHUR_NAME AS PERSON_AMPHUR_NAME, ");
		    hql.append(" 	    TAXPEAMP.AMPHUR_NAME AS PERSON_TAXAMPHUR_NAME, ");
		    hql.append(" 	    PEPROV.PROVINCE_NAME AS PERSON_PROVINCE_NAME, ");
		    hql.append(" 	    TAXPEPROV.PROVINCE_NAME AS PERSON_TAXPROVINCE_NAME, ");
		    hql.append(" 	    MP.PREFIX_NAME_EN AS PREFIX_NAME_EN, ");
		    hql.append(" 	    TR.LICENSE_NO AS LICENSE_NO, ");
		    hql.append("		PIC.PRINT_CARD_ID AS PRINT_CARD_ID, ");
		    hql.append("		PIC.PRINT_CARD_DATE AS PRINT_CARD_DATE, ");
		    hql.append("		PIC.PRINT_CARD_STATUS AS PRINT_CARD_STATUS, ");
		    hql.append("		TR.TRADER_BRANCH_NO AS TRADER_BRANCH_NO, ");
		    hql.append("		RE.APPROVE_DATE AS APPROVE_DATE, ");
		    hql.append("		TR.EXPIRE_DATE AS EXPIRE_DATE ");
		   
		    
		    //hql.append("		TR.BRANCH_TYPE AS BRANCH_TYPE ");

		    hql.append("    FROM TRADER TR ");
		    hql.append("    INNER JOIN ( ");
		    hql.append("    SELECT TRA.LICENSE_NO , TRA.TRADER_TYPE ");
		    hql.append("	FROM PRINTING_CARD PC ");
		    hql.append("	INNER JOIN TRADER TRA ");
		    hql.append("	ON(TRA.TRADER_ID = PC.TRADER_ID) ");
		    hql.append("	GROUP BY TRA.LICENSE_NO, ");
		    hql.append("	TRA.TRADER_TYPE ");
		    hql.append("    ) PLC ");
		    hql.append("    ON(PLC.LICENSE_NO = TR.LICENSE_NO AND PLC.TRADER_TYPE = TR.TRADER_TYPE) ");
		    //hql.append("    LEFT JOIN REGISTRATION RE ");
		   // hql.append("    ON(RE.TRADER_ID = TR.TRADER_ID) "); 
		  	hql.append("    INNER JOIN PRINTING_CARD PIC ");
		    hql.append("    ON(PIC.TRADER_ID = TR.TRADER_ID) ");
		    hql.append("    INNER JOIN REGISTRATION RE ");
		    hql.append("    ON(RE.REG_ID = PIC.REG_ID) ");
		    hql.append("    INNER JOIN PERSON PE ");
		    hql.append("    ON(PE.PERSON_ID = TR.PERSON_ID) ");
		    /*hql.append("    INNER JOIN TRADER_ADDRESS TRD ");
		    hql.append("    ON(TRD.TRADER_ID = PRI.TRADER_ID) ");*/
		    //WHERE TR.RECORD_STATUS IN ('N','H')
		   // AND PRI.PRINT_LICENSE_STATUS IN ('W','A')
		   // ORDER BY TR.TRADER_ID;
		    
		   /* hql.append("    INNER JOIN REGISTRATION RE ");
		    hql.append("    ON(RE.REG_ID = PRI.REG_ID) "); 
		    INNER JOIN
			REGISTRATION RE
			ON
			(RE.REG_ID = PRI.REG_ID)*/
		    
		    
		    
		
		    /*hql.append(" 	    FROM PRINTING_LICENSE PRI ");
		 
		    hql.append("		INNER JOIN REGISTRATION RE ");
		    hql.append("		ON(RE.REG_ID = PRI.REG_ID) ");

		    hql.append(" 	    INNER JOIN TRADER TR ");
		    hql.append(" 	    ON(PRI.TRADER_ID = TR.TRADER_ID) ");

		    hql.append(" 	    INNER JOIN PERSON PE ");
		    hql.append(" 	    ON(PE.PERSON_ID = TR.PERSON_ID) ");*/

		    hql.append(" 	    INNER JOIN MAS_PREFIX MP ");
		    hql.append(" 	    ON(PE.PREFIX_ID = MP.PREFIX_ID) ");

		    hql.append(" 	    LEFT JOIN MAS_POSFIX POSF ");
		    hql.append(" 	    ON(POSF.POSTFIX_ID = PE.POSTFIX_ID) ");

		    hql.append(" 	    LEFT JOIN MAS_AMPHUR PEAMP ");
		    hql.append(" 	    ON(PEAMP.AMPHUR_ID = PE.AMPHUR_ID) ");

		    hql.append(" 	    LEFT JOIN MAS_AMPHUR TAXPEAMP ");
		    hql.append(" 	    ON(TAXPEAMP.AMPHUR_ID = PE.TAX_AMPHUR_ID) ");

		    hql.append(" 	    LEFT JOIN MAS_PROVINCE PEPROV ");
		    hql.append(" 	    ON(PEPROV.PROVINCE_ID = PE.PROVINCE_ID) ");

		    hql.append(" 	    LEFT JOIN MAS_PROVINCE TAXPEPROV ");
		    hql.append(" 	    ON(TAXPEPROV.PROVINCE_ID = PE.TAX_PROVINCE_ID) ");
			
		    hql.append(" WHERE TR.ORG_ID = :REGISTRATION_ORG_ID ");
		    
		    parames.put("REGISTRATION_ORG_ID", user.getUserData().getOrgId());
		   
		    hql.append(" AND TR.RECORD_STATUS IN("+"'N','H'"+") ");
		    
//		    hql.append(" AND TRD.ADDRESS_TYPE = 'S' ");
//		    hql.append(" AND TR.BRANCH_TYPE IS NULL ");

		    if((null != dto.getTraderName()) && (!"".equals(dto.getTraderName())))
		    {
			    hql.append(" AND TR.TRADER_NAME LIKE :TRADER_TRADER_NAME ");
			    
			    parames.put("TRADER_TRADER_NAME", "%"+dto.getTraderName()+"%");
		    }
		    
		    if((null != dto.getTraderNameEn()) && (!"".equals(dto.getTraderNameEn())))
		    {
			    hql.append(" AND TR.TRADER_NAME_EN LIKE :TRADER_TRADER_NAME_EN ");
			    
			    parames.put("TRADER_TRADER_NAME_EN", "%"+dto.getTraderNameEn()+"%");
		    }
		    
		    if((null != dto.getFirstName()) && (!"".equals(dto.getFirstName())))
		    {
			    hql.append(" AND PE.FIRST_NAME LIKE :TRADER_FIRST_NAME ");
			    
			    parames.put("TRADER_FIRST_NAME", "%"+dto.getFirstName()+"%");
		    }
		    
		    if(dto.getTraderId() > 0)
		    {
		    	 hql.append(" AND TR.TRADER_ID = :TRADER_TRADER_ID ");
				    
				 parames.put("TRADER_TRADER_ID", dto.getTraderId());
		    }
		    if((null != dto.getTraderType()) && (!"".equals(dto.getTraderType())))
		    {
		    	 hql.append(" AND TR.TRADER_TYPE = :TRADER_TRADER_TYPE ");
				    
				 parames.put("TRADER_TRADER_TYPE", dto.getTraderType());
		    }
		    if((null != dto.getTraderCategory()) && (!"".equals(dto.getTraderCategory())))
		    {
		    	 hql.append(" AND TR.TRADER_CATEGORY = :TRADER_TRADER_CATEGORY ");
				    
				 parames.put("TRADER_TRADER_CATEGORY", dto.getTraderCategory());
		    }
			
		    if((null != dto.getRegistrationType()) && (!"".equals(dto.getRegistrationType())))
		    {
			    hql.append(" AND RE.REGISTRATION_TYPE = :REGISTRATION_REGISTRATION_TYPE ");
			    
			    parames.put("REGISTRATION_REGISTRATION_TYPE", dto.getRegistrationType());
		    }
		    
		    if((null != dto.getRegistrationNo()) && (!"".equals(dto.getRegistrationNo())))
		    {
			    hql.append(" AND RE.REGISTRATION_NO = :REGISTRATION_REGISTRATION_NO ");
			    
			    parames.put("REGISTRATION_REGISTRATION_NO", dto.getRegistrationNo());
		    }

		    if((null != dto.getApproveDateFrom()) && (!dto.getApproveDateFrom().equals("")))
		    {
		    	if((null != dto.getApproveDateTo()) && (!dto.getApproveDateTo().equals("")))
		    	{
					hql.append("  and ( RE.APPROVE_DATE >=  convert(datetime, :APPROVE_START_DATE ,20) ");
					hql.append(" and RE.APPROVE_DATE <  convert(datetime, :APPROVE_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
						
					parames.put("APPROVE_START_DATE", sendDtmFrom);
					parames.put("APPROVE_END_DATE", sendDtmTo);
		    	}
		    }
		    
		    if((null != dto.getPrintCardStatus()) && (!dto.getPrintCardStatus().equals("")))
		    {
		    	hql.append(" and PIC.PRINT_CARD_STATUS =:PRINT_CARD_STATUS ");
		    	parames.put("PRINT_CARD_STATUS", dto.getPrintCardStatus());
		    }else
		    {
		    	hql.append(" and PIC.PRINT_CARD_STATUS IN("+"'W','A'"+") ");
		    	//parames.put("PRINT_LICENSE_STATUS", "('W','A')");
		    }
		    
		    if((null != dto.getLicenseNoFrom() &&(!dto.getLicenseNoFrom().equals(""))))
		    {
		    	if((null != dto.getLicenseNoTo() &&(!dto.getLicenseNoTo().equals(""))))
		    	{
		    		hql.append(" and TR.LICENSE_NO BETWEEN :LICENSE_NO ");
		    		parames.put("LICENSE_NO",dto.getLicenseNoFrom());
		    		hql.append(" and :LICENSE_NO1 ");
		    		parames.put("LICENSE_NO1",dto.getLicenseNoTo());
		    	}else
		    	{
		    		hql.append(" and TR.LICENSE_NO = :LICENSE_NO");
		    		parames.put("LICENSE_NO",dto.getLicenseNoFrom());
		    	}
		    	
		    }else if((null != dto.getLicenseNoTo() &&(!dto.getLicenseNoTo().equals(""))))
		    {
		    	if((null != dto.getLicenseNoFrom() &&(!dto.getLicenseNoFrom().equals(""))))
		    	{
		    		hql.append(" and TR.LICENSE_NO BETWEEN :LICENSE_NO ");
		    		parames.put("LICENSE_NO",dto.getLicenseNoFrom());
		    		hql.append(" and :LICENSE_NO1 ");
		    		parames.put("LICENSE_NO1",dto.getLicenseNoTo());
		    	}else
		    	{
		    		hql.append(" and TR.LICENSE_NO = :LICENSE_NO");
		    		parames.put("LICENSE_NO",dto.getLicenseNoTo());
		    	}
		    }
		    
		    	hql.append("  ORDER BY PIC.REG_ID,TR.TRADER_ID, RE.APPROVE_DATE ASC ");			
		    System.out.println(hql.toString());
		    
		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
		    sqlQuery.setFirstResult(start);
		    sqlQuery.setMaxResults(limit);
		    
			sqlQuery.addScalar("REG_ID", Hibernate.LONG); //1
			sqlQuery.addScalar("REGISTRATION_NO", Hibernate.STRING); //2
		    sqlQuery.addScalar("REGISTRATION_DATE", Hibernate.DATE); //3
		    sqlQuery.addScalar("REGISTRATION_PROGRESS", Hibernate.STRING); //4
		    sqlQuery.addScalar("REGISTRATION_TYPE", Hibernate.STRING);//5 
		    sqlQuery.addScalar("ORG_ID", Hibernate.LONG);//6
		    sqlQuery.addScalar("RECORD_STATUS", Hibernate.STRING);//7
		    sqlQuery.addScalar("TRADER_ID", Hibernate.LONG); //8
		    sqlQuery.addScalar("TRADER_TYPE", Hibernate.STRING);//9
		    sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING);//10
		    sqlQuery.addScalar("TRADER_NAME_EN", Hibernate.STRING);//11
		    sqlQuery.addScalar("TRADER_CATEGORY", Hibernate.STRING);//12
		    sqlQuery.addScalar("PERSON_ID", Hibernate.LONG);//13
		    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);//14
		    sqlQuery.addScalar("PREFIX_NAME", Hibernate.STRING);//15
		    sqlQuery.addScalar("FIRST_NAME", Hibernate.STRING);//16
		    sqlQuery.addScalar("LAST_NAME", Hibernate.STRING);//17
		    sqlQuery.addScalar("POSTFIX_NAME", Hibernate.STRING);//18

		    sqlQuery.addScalar("PREFIX_ID", Hibernate.LONG);//26
		    sqlQuery.addScalar("POSTFIX_ID", Hibernate.LONG);//27
		    sqlQuery.addScalar("PERSON_AMPHUR_ID", Hibernate.LONG);//28
		    sqlQuery.addScalar("PERSON_TAXAMPHUR_ID", Hibernate.LONG);//29
		    sqlQuery.addScalar("PERSON_PROVINCE_ID", Hibernate.LONG);//30
		    sqlQuery.addScalar("PERSON_TAXPROVINCE_ID", Hibernate.LONG);//31
		    
		    sqlQuery.addScalar("TRADER_GUIDE_ID", Hibernate.LONG);
		    sqlQuery.addScalar("PRONUNCIATION_NAME", Hibernate.STRING);
		    sqlQuery.addScalar("LICENSE_GUIDE_NO", Hibernate.STRING);
		    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);
		    sqlQuery.addScalar("IDENTITY_NO", Hibernate.STRING);
		    sqlQuery.addScalar("COMMITTEE_NAME1", Hibernate.STRING);
		    sqlQuery.addScalar("COMMITTEE_NAME2", Hibernate.STRING);
		    sqlQuery.addScalar("COMMITTEE_NAME_SIGN", Hibernate.STRING);
		    sqlQuery.addScalar("PASSPORT_NO", Hibernate.STRING);
		    sqlQuery.addScalar("FIRST_NAME_EN", Hibernate.STRING);
		    sqlQuery.addScalar("LAST_NAME_EN", Hibernate.STRING);
		    sqlQuery.addScalar("GENDER", Hibernate.STRING);
		    sqlQuery.addScalar("PERSON_NATIONALITY", Hibernate.STRING);
		    sqlQuery.addScalar("BIRTH_DATE", Hibernate.DATE);
		    sqlQuery.addScalar("AGE_YEAR", Hibernate.STRING);
		    sqlQuery.addScalar("IDENTITY_NO_EXPIRE", Hibernate.DATE);
		    sqlQuery.addScalar("CORPORATE_TYPE", Hibernate.STRING);
		    sqlQuery.addScalar("TAX_IDENTITY_NO", Hibernate.STRING);
		    sqlQuery.addScalar("IDENTITY_DATE", Hibernate.DATE);
		    sqlQuery.addScalar("RECORD_STATUS", Hibernate.STRING);
		    
		    sqlQuery.addScalar("PERSON_AMPHUR_NAME", Hibernate.STRING);
		    sqlQuery.addScalar("PERSON_TAXAMPHUR_NAME", Hibernate.STRING);
		    sqlQuery.addScalar("PERSON_PROVINCE_NAME", Hibernate.STRING);
		    sqlQuery.addScalar("PERSON_TAXPROVINCE_NAME", Hibernate.STRING);
		    sqlQuery.addScalar("PREFIX_NAME_EN", Hibernate.STRING);
		    sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING);
		    sqlQuery.addScalar("PRINT_CARD_ID", Hibernate.LONG);
		    sqlQuery.addScalar("PRINT_CARD_DATE", Hibernate.DATE);
		    sqlQuery.addScalar("PRINT_CARD_STATUS", Hibernate.STRING);
		    sqlQuery.addScalar("TRADER_BRANCH_NO", Hibernate.INTEGER);
		    sqlQuery.addScalar("APPROVE_DATE", Hibernate.DATE);
		    sqlQuery.addScalar("EXPIRE_DATE", Hibernate.DATE);
		    //sqlQuery.addScalar("BRANCH_TYPE", Hibernate.STRING);
		    //sqlQuery.addScalar("BOOK_NO", Hibernate.STRING);
		    //sqlQuery.addScalar("RECEIPT_NO", Hibernate.STRING);
		    //sqlQuery.addScalar("RECEIPT_STATUS", Hibernate.STRING);

		    sqlQuery.setProperties(parames);
		    List<Object[]>  result = sqlQuery.list();
	  
		    return result;
		}
	  public List<Object[]> findPrintCard(RegistrationDTO dto, User user ,int start, int limit)throws Exception
		{
			Map<String,Object> parames = new HashMap<String,Object>();
		    
		    StringBuilder hql = new StringBuilder();
		    
		    hql.append(" 	    SELECT PIC.REG_ID AS REG_ID, ");
		    hql.append(" 	    RE.REGISTRATION_NO AS REGISTRATION_NO, ");
		    hql.append(" 	    RE.REGISTRATION_DATE AS REGISTRATION_DATE, ");
		    hql.append(" 	    RE.REGISTRATION_PROGRESS AS REGISTRATION_PROGRESS, ");
		    hql.append(" 	    RE.REGISTRATION_TYPE AS REGISTRATION_TYPE, ");
		    hql.append(" 	    RE.ORG_ID AS ORG_ID, ");
		    hql.append(" 	    RE.RECORD_STATUS AS RECORD_STATUS, ");
		    hql.append(" 	    TR.TRADER_ID AS TRADER_ID, ");
		    hql.append(" 	    TR.TRADER_TYPE AS TRADER_TYPE, ");
		    hql.append(" 	    TR.TRADER_NAME AS TRADER_NAME, ");
		    hql.append(" 	    TR.TRADER_NAME_EN AS TRADER_NAME_EN, ");
		    hql.append(" 	    TR.TRADER_CATEGORY AS TRADER_CATEGORY, ");
		    hql.append(" 	    PE.PERSON_ID AS PERSON_ID, ");
		    hql.append(" 	    PE.PERSON_TYPE AS PERSON_TYPE, ");
		    hql.append(" 	    MP.PREFIX_NAME AS PREFIX_NAME, ");
		    hql.append(" 	    PE.FIRST_NAME AS FIRST_NAME, ");
		    hql.append(" 	    PE.LAST_NAME AS LAST_NAME, ");
		    hql.append(" 	    POSF.POSTFIX_NAME AS POSTFIX_NAME, ");
		    hql.append(" 	    MP.PREFIX_ID AS PREFIX_ID, ");
		    hql.append(" 	    POSF.POSTFIX_ID AS POSTFIX_ID, ");
		    hql.append(" 	    PEAMP.AMPHUR_ID AS PERSON_AMPHUR_ID, ");
		    hql.append(" 	    TAXPEAMP.AMPHUR_ID AS PERSON_TAXAMPHUR_ID, ");
		    hql.append(" 	    PEPROV.PROVINCE_ID AS PERSON_PROVINCE_ID, ");
		    hql.append(" 	    TAXPEPROV.PROVINCE_ID AS PERSON_TAXPROVINCE_ID, ");
		    hql.append(" 	    TR.TRADER_GUIDE_ID AS TRADER_GUIDE_ID, ");
		    hql.append(" 	    TR.PRONUNCIATION_NAME AS PRONUNCIATION_NAME, ");
		    hql.append(" 	    TR.LICENSE_GUIDE_NO AS LICENSE_GUIDE_NO, ");
		    hql.append(" 	    PE.PERSON_TYPE AS PERSON_TYPE, ");
		    hql.append(" 	    PE.IDENTITY_NO AS IDENTITY_NO, ");
		    hql.append(" 	    PE.COMMITTEE_NAME1 AS COMMITTEE_NAME1, ");
		    hql.append(" 	    PE.COMMITTEE_NAME2 AS COMMITTEE_NAME2, ");
		    hql.append(" 	    PE.COMMITTEE_NAME_SIGN AS COMMITTEE_NAME_SIGN, ");
		    hql.append(" 	    PE.PASSPORT_NO AS PASSPORT_NO, ");
		    hql.append(" 	    PE.FIRST_NAME_EN AS FIRST_NAME_EN, ");
		    hql.append(" 	    PE.LAST_NAME_EN AS LAST_NAME_EN, ");
		    hql.append(" 	    PE.GENDER AS GENDER, ");
		    hql.append(" 	    PE.PERSON_NATIONALITY AS PERSON_NATIONALITY, ");
		    hql.append(" 	    PE.BIRTH_DATE AS BIRTH_DATE, ");
		    hql.append(" 	    PE.AGE_YEAR AS AGE_YEAR, ");
		    hql.append(" 	    PE.IDENTITY_NO_EXPIRE AS IDENTITY_NO_EXPIRE, ");
		    hql.append(" 	    PE.CORPORATE_TYPE AS CORPORATE_TYPE, ");
		    hql.append(" 	    PE.TAX_IDENTITY_NO AS TAX_IDENTITY_NO, ");
		    hql.append(" 	    PE.IDENTITY_DATE AS IDENTITY_DATE, ");
		    hql.append(" 	    TR.RECORD_STATUS AS RECORD_STATUS, ");
		    hql.append(" 	    PEAMP.AMPHUR_NAME AS PERSON_AMPHUR_NAME, ");
		    hql.append(" 	    TAXPEAMP.AMPHUR_NAME AS PERSON_TAXAMPHUR_NAME, ");
		    hql.append(" 	    PEPROV.PROVINCE_NAME AS PERSON_PROVINCE_NAME, ");
		    hql.append(" 	    TAXPEPROV.PROVINCE_NAME AS PERSON_TAXPROVINCE_NAME, ");
		    hql.append(" 	    MP.PREFIX_NAME_EN AS PREFIX_NAME_EN, ");
		    hql.append(" 	    TR.LICENSE_NO AS LICENSE_NO, ");
		    hql.append("		PIC.PRINT_CARD_ID AS PRINT_CARD_ID, ");
		    hql.append("		PIC.PRINT_CARD_DATE AS PRINT_CARD_DATE, ");
		    hql.append("		PIC.PRINT_CARD_STATUS AS PRINT_CARD_STATUS, ");
		    hql.append("		TR.TRADER_BRANCH_NO AS TRADER_BRANCH_NO, ");
		    hql.append("		RE.APPROVE_DATE AS APPROVE_DATE, ");
		    hql.append("		TR.EXPIRE_DATE AS EXPIRE_DATE, ");
		    hql.append("		TR.EFFECTIVE_DATE AS EFFECTIVE_DATE ");
		    

		    hql.append("    FROM TRADER TR ");
		    hql.append("    INNER JOIN ( ");
		    hql.append("    SELECT TRA.LICENSE_NO , TRA.TRADER_TYPE ");
		    hql.append("	FROM PRINTING_CARD PC ");
		    hql.append("	INNER JOIN TRADER TRA ");
		    hql.append("	ON(TRA.TRADER_ID = PC.TRADER_ID) ");
		    hql.append("	GROUP BY TRA.LICENSE_NO, ");
		    hql.append("	TRA.TRADER_TYPE ");
		    hql.append("    ) PLC ");
		    hql.append("    ON(PLC.LICENSE_NO = TR.LICENSE_NO AND PLC.TRADER_TYPE = TR.TRADER_TYPE) ");
		    //hql.append("    LEFT JOIN REGISTRATION RE ");
		   // hql.append("    ON(RE.TRADER_ID = TR.TRADER_ID) "); 
		  	hql.append("    INNER JOIN PRINTING_CARD PIC ");
		    hql.append("    ON(PIC.TRADER_ID = TR.TRADER_ID) ");
		    hql.append("    INNER JOIN REGISTRATION RE ");
		    hql.append("    ON(RE.REG_ID = PIC.REG_ID) ");
		    hql.append("    INNER JOIN PERSON PE ");
		    hql.append("    ON(PE.PERSON_ID = TR.PERSON_ID) ");
		    /*hql.append("    INNER JOIN TRADER_ADDRESS TRD ");
		    hql.append("    ON(TRD.TRADER_ID = PRI.TRADER_ID) ");*/
		    //WHERE TR.RECORD_STATUS IN ('N','H')
		   // AND PRI.PRINT_LICENSE_STATUS IN ('W','A')
		   // ORDER BY TR.TRADER_ID;
		    
		   /* hql.append("    INNER JOIN REGISTRATION RE ");
		    hql.append("    ON(RE.REG_ID = PRI.REG_ID) "); 
		    INNER JOIN
			REGISTRATION RE
			ON
			(RE.REG_ID = PRI.REG_ID)*/
		    
		    
		    
		
		    /*hql.append(" 	    FROM PRINTING_LICENSE PRI ");
		 
		    hql.append("		INNER JOIN REGISTRATION RE ");
		    hql.append("		ON(RE.REG_ID = PRI.REG_ID) ");

		    hql.append(" 	    INNER JOIN TRADER TR ");
		    hql.append(" 	    ON(PRI.TRADER_ID = TR.TRADER_ID) ");

		    hql.append(" 	    INNER JOIN PERSON PE ");
		    hql.append(" 	    ON(PE.PERSON_ID = TR.PERSON_ID) ");*/

		    hql.append(" 	    INNER JOIN MAS_PREFIX MP ");
		    hql.append(" 	    ON(PE.PREFIX_ID = MP.PREFIX_ID) ");

		    hql.append(" 	    LEFT JOIN MAS_POSFIX POSF ");
		    hql.append(" 	    ON(POSF.POSTFIX_ID = PE.POSTFIX_ID) ");

		    hql.append(" 	    LEFT JOIN MAS_AMPHUR PEAMP ");
		    hql.append(" 	    ON(PEAMP.AMPHUR_ID = PE.AMPHUR_ID) ");

		    hql.append(" 	    LEFT JOIN MAS_AMPHUR TAXPEAMP ");
		    hql.append(" 	    ON(TAXPEAMP.AMPHUR_ID = PE.TAX_AMPHUR_ID) ");

		    hql.append(" 	    LEFT JOIN MAS_PROVINCE PEPROV ");
		    hql.append(" 	    ON(PEPROV.PROVINCE_ID = PE.PROVINCE_ID) ");

		    hql.append(" 	    LEFT JOIN MAS_PROVINCE TAXPEPROV ");
		    hql.append(" 	    ON(TAXPEPROV.PROVINCE_ID = PE.TAX_PROVINCE_ID) ");
			
		    hql.append(" WHERE TR.ORG_ID = :REGISTRATION_ORG_ID ");
		    
		    parames.put("REGISTRATION_ORG_ID", user.getUserData().getOrgId());
		   
		    hql.append(" AND TR.RECORD_STATUS IN("+"'N','H'"+") ");
		    
//		    hql.append(" AND TRD.ADDRESS_TYPE = 'S' ");
//		    hql.append(" AND TR.BRANCH_TYPE IS NULL ");

		    if((null != dto.getTraderName()) && (!"".equals(dto.getTraderName())))
		    {
			    hql.append(" AND TR.TRADER_NAME LIKE :TRADER_TRADER_NAME ");
			    
			    parames.put("TRADER_TRADER_NAME", "%"+dto.getTraderName()+"%");
		    }
		    
		    if((null != dto.getTraderNameEn()) && (!"".equals(dto.getTraderNameEn())))
		    {
			    hql.append(" AND TR.TRADER_NAME_EN LIKE :TRADER_TRADER_NAME_EN ");
			    
			    parames.put("TRADER_TRADER_NAME_EN", "%"+dto.getTraderNameEn()+"%");
		    }
		    
		    if((null != dto.getFirstName()) && (!"".equals(dto.getFirstName())))
		    {
			    hql.append(" AND PE.FIRST_NAME LIKE :TRADER_FIRST_NAME ");
			    
			    parames.put("TRADER_FIRST_NAME", "%"+dto.getFirstName()+"%");
		    }
		    
		    if(dto.getTraderId() > 0)
		    {
		    	 hql.append(" AND TR.TRADER_ID = :TRADER_TRADER_ID ");
				    
				 parames.put("TRADER_TRADER_ID", dto.getTraderId());
		    }
		    if((null != dto.getTraderType()) && (!"".equals(dto.getTraderType())))
		    {
		    	 hql.append(" AND TR.TRADER_TYPE = :TRADER_TRADER_TYPE ");
				    
				 parames.put("TRADER_TRADER_TYPE", dto.getTraderType());
		    }
		    if((null != dto.getTraderCategory()) && (!"".equals(dto.getTraderCategory())))
		    {
		    	 hql.append(" AND TR.TRADER_CATEGORY = :TRADER_TRADER_CATEGORY ");
				    
				 parames.put("TRADER_TRADER_CATEGORY", dto.getTraderCategory());
		    }
			
		    if((null != dto.getRegistrationType()) && (!"".equals(dto.getRegistrationType())))
		    {
			    hql.append(" AND RE.REGISTRATION_TYPE = :REGISTRATION_REGISTRATION_TYPE ");
			    
			    parames.put("REGISTRATION_REGISTRATION_TYPE", dto.getRegistrationType());
		    }
		    
		    if((null != dto.getRegistrationNo()) && (!"".equals(dto.getRegistrationNo())))
		    {
			    hql.append(" AND RE.REGISTRATION_NO = :REGISTRATION_REGISTRATION_NO ");
			    
			    parames.put("REGISTRATION_REGISTRATION_NO", dto.getRegistrationNo());
		    }

		    if((null != dto.getApproveDateFrom()) && (!dto.getApproveDateFrom().equals("")))
		    {
		    	if((null != dto.getApproveDateTo()) && (!dto.getApproveDateTo().equals("")))
		    	{
					hql.append("  and ( RE.APPROVE_DATE >=  convert(datetime, :APPROVE_START_DATE ,20) ");
					hql.append(" and RE.APPROVE_DATE <=  convert(datetime, :APPROVE_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getApproveDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getApproveDateTo(), "23:00");
						
					parames.put("APPROVE_START_DATE", sendDtmFrom);
					parames.put("APPROVE_END_DATE", sendDtmTo);
		    	}
		    }
		    hql.append(" and PIC.PRINT_CARD_STATUS IN("+"'W','A'"+") ");
		    
		 //   if((null != dto.getPrintCardStatus()) && (!dto.getPrintCardStatus().equals("")))
		  //  {
		  //  	hql.append(" and PIC.PRINT_CARD_STATUS =:PRINT_CARD_STATUS ");
		  //  	parames.put("PRINT_CARD_STATUS", dto.getPrintCardStatus());
		  //  }else
		  //  {
		  //  	hql.append(" and PIC.PRINT_CARD_STATUS IN("+"'W','A'"+") ");
		    	//parames.put("PRINT_LICENSE_STATUS", "('W','A')");
		  //  }
		    
		    if((null != dto.getLicenseNoFrom() &&(!dto.getLicenseNoFrom().equals(""))))
		    {
		    	if((null != dto.getLicenseNoTo() &&(!dto.getLicenseNoTo().equals(""))))
		    	{
		    		hql.append(" and TR.LICENSE_NO BETWEEN :LICENSE_NO ");
		    		parames.put("LICENSE_NO",dto.getLicenseNoFrom());
		    		hql.append(" and :LICENSE_NO1 ");
		    		parames.put("LICENSE_NO1",dto.getLicenseNoTo());
		    	}else
		    	{
		    		hql.append(" and TR.LICENSE_NO = :LICENSE_NO");
		    		parames.put("LICENSE_NO",dto.getLicenseNoFrom());
		    	}
		    	
		    }else if((null != dto.getLicenseNoTo() &&(!dto.getLicenseNoTo().equals(""))))
		    {
		    	if((null != dto.getLicenseNoFrom() &&(!dto.getLicenseNoFrom().equals(""))))
		    	{
		    		hql.append(" and TR.LICENSE_NO BETWEEN :LICENSE_NO ");
		    		parames.put("LICENSE_NO",dto.getLicenseNoFrom());
		    		hql.append(" and :LICENSE_NO1 ");
		    		parames.put("LICENSE_NO1",dto.getLicenseNoTo());
		    	}else
		    	{
		    		hql.append(" and TR.LICENSE_NO = :LICENSE_NO");
		    		parames.put("LICENSE_NO",dto.getLicenseNoTo());
		    	}
		    }
		    
		    	hql.append("  ORDER BY PIC.REG_ID,TR.TRADER_ID, RE.APPROVE_DATE ASC ");			
		    System.out.println(hql.toString());
		    
		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
		   // sqlQuery.setFirstResult(start);
		   // sqlQuery.setMaxResults(limit);
		    
			sqlQuery.addScalar("REG_ID", Hibernate.LONG); //1
			sqlQuery.addScalar("REGISTRATION_NO", Hibernate.STRING); //2
		    sqlQuery.addScalar("REGISTRATION_DATE", Hibernate.DATE); //3
		    sqlQuery.addScalar("REGISTRATION_PROGRESS", Hibernate.STRING); //4
		    sqlQuery.addScalar("REGISTRATION_TYPE", Hibernate.STRING);//5 
		    sqlQuery.addScalar("ORG_ID", Hibernate.LONG);//6
		    sqlQuery.addScalar("RECORD_STATUS", Hibernate.STRING);//7
		    sqlQuery.addScalar("TRADER_ID", Hibernate.LONG); //8
		    sqlQuery.addScalar("TRADER_TYPE", Hibernate.STRING);//9
		    sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING);//10
		    sqlQuery.addScalar("TRADER_NAME_EN", Hibernate.STRING);//11
		    sqlQuery.addScalar("TRADER_CATEGORY", Hibernate.STRING);//12
		    sqlQuery.addScalar("PERSON_ID", Hibernate.LONG);//13
		    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);//14
		    sqlQuery.addScalar("PREFIX_NAME", Hibernate.STRING);//15
		    sqlQuery.addScalar("FIRST_NAME", Hibernate.STRING);//16
		    sqlQuery.addScalar("LAST_NAME", Hibernate.STRING);//17
		    sqlQuery.addScalar("POSTFIX_NAME", Hibernate.STRING);//18

		    sqlQuery.addScalar("PREFIX_ID", Hibernate.LONG);//26
		    sqlQuery.addScalar("POSTFIX_ID", Hibernate.LONG);//27
		    sqlQuery.addScalar("PERSON_AMPHUR_ID", Hibernate.LONG);//28
		    sqlQuery.addScalar("PERSON_TAXAMPHUR_ID", Hibernate.LONG);//29
		    sqlQuery.addScalar("PERSON_PROVINCE_ID", Hibernate.LONG);//30
		    sqlQuery.addScalar("PERSON_TAXPROVINCE_ID", Hibernate.LONG);//31
		    
		    sqlQuery.addScalar("TRADER_GUIDE_ID", Hibernate.LONG);
		    sqlQuery.addScalar("PRONUNCIATION_NAME", Hibernate.STRING);
		    sqlQuery.addScalar("LICENSE_GUIDE_NO", Hibernate.STRING);
		    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);
		    sqlQuery.addScalar("IDENTITY_NO", Hibernate.STRING);
		    sqlQuery.addScalar("COMMITTEE_NAME1", Hibernate.STRING);
		    sqlQuery.addScalar("COMMITTEE_NAME2", Hibernate.STRING);
		    sqlQuery.addScalar("COMMITTEE_NAME_SIGN", Hibernate.STRING);
		    sqlQuery.addScalar("PASSPORT_NO", Hibernate.STRING);
		    sqlQuery.addScalar("FIRST_NAME_EN", Hibernate.STRING);
		    sqlQuery.addScalar("LAST_NAME_EN", Hibernate.STRING);
		    sqlQuery.addScalar("GENDER", Hibernate.STRING);
		    sqlQuery.addScalar("PERSON_NATIONALITY", Hibernate.STRING);
		    sqlQuery.addScalar("BIRTH_DATE", Hibernate.DATE);
		    sqlQuery.addScalar("AGE_YEAR", Hibernate.STRING);
		    sqlQuery.addScalar("IDENTITY_NO_EXPIRE", Hibernate.DATE);
		    sqlQuery.addScalar("CORPORATE_TYPE", Hibernate.STRING);
		    sqlQuery.addScalar("TAX_IDENTITY_NO", Hibernate.STRING);
		    sqlQuery.addScalar("IDENTITY_DATE", Hibernate.DATE);
		    sqlQuery.addScalar("RECORD_STATUS", Hibernate.STRING);
		    
		    sqlQuery.addScalar("PERSON_AMPHUR_NAME", Hibernate.STRING);
		    sqlQuery.addScalar("PERSON_TAXAMPHUR_NAME", Hibernate.STRING);
		    sqlQuery.addScalar("PERSON_PROVINCE_NAME", Hibernate.STRING);
		    sqlQuery.addScalar("PERSON_TAXPROVINCE_NAME", Hibernate.STRING);
		    sqlQuery.addScalar("PREFIX_NAME_EN", Hibernate.STRING);
		    sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING);
		    sqlQuery.addScalar("PRINT_CARD_ID", Hibernate.LONG);
		    sqlQuery.addScalar("PRINT_CARD_DATE", Hibernate.DATE);
		    sqlQuery.addScalar("PRINT_CARD_STATUS", Hibernate.STRING);
		    sqlQuery.addScalar("TRADER_BRANCH_NO", Hibernate.INTEGER);
		    sqlQuery.addScalar("APPROVE_DATE", Hibernate.DATE);
		    sqlQuery.addScalar("EXPIRE_DATE", Hibernate.DATE);
		    sqlQuery.addScalar("EFFECTIVE_DATE", Hibernate.DATE);
		    //sqlQuery.addScalar("BRANCH_TYPE", Hibernate.STRING);
		    //sqlQuery.addScalar("BOOK_NO", Hibernate.STRING);
		    //sqlQuery.addScalar("RECEIPT_NO", Hibernate.STRING);
		    //sqlQuery.addScalar("RECEIPT_STATUS", Hibernate.STRING);

		    sqlQuery.setProperties(parames);
		    List<Object[]>  result = sqlQuery.list();
	  
		    return result;
		}
}
