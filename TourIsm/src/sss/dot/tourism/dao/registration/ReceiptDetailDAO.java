package sss.dot.tourism.dao.registration;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.ReceiptDetail;
import sss.dot.tourism.dto.deactivate.MasDeactivateTypeDTO;
import sss.dot.tourism.dto.registration.ReceiptDetailDTO;

@Repository("receiptDetailDAO")
public class ReceiptDetailDAO extends BaseDAO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1698164175449953283L;

	public ReceiptDetailDAO()
	{
		this.domainObj = ReceiptDetail.class;
	}

	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<ReceiptDetail> findbyReceipt(long receiptId)
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  ReceiptDetail as dep ");
		hql.append(" where dep.recordStatus = 'N' ");
		hql.append(" and dep.receipt.receiptId = ? ");
	
		params.add(receiptId);
		
		return (List<ReceiptDetail>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
	
	public List<ReceiptDetailDTO> findReceiptDetailFee(long receiptId,String traderType, String registrationType)
	{
		Map<String,Object> parames = new HashMap<String,Object>();
		    
	    StringBuilder hql = new StringBuilder();
	    

//		hql.append(" SELECT ");
//		hql.append(" 	MAC.FEE_ID , ");
//		hql.append(" 	ISNULL(SEL.FEE_NAME, ");
//		hql.append(" 	MAC.FEE_NAME) AS FEE_NAME , ");
//		hql.append(" 	ISNULL(SEL.FEE_MNY, ");
//		hql.append(" 	MAC.FEE_MNY) AS FEE_MNY , ");
//		hql.append(" 	MAC.TRADER_TYPE , ");
//		hql.append(" 	MAC.REGISTRATION_TYPE, ");
//		hql.append(" 	SEL.RECEIPT_ID, ");
//		hql.append(" 	SEL.RECEIPT_DETAIL_ID ");
//		hql.append(" FROM ");
//		hql.append(" 	MAS_FEE MAC ");
//		hql.append(" 		LEFT JOIN ");
//		hql.append(" 		( ");
		hql.append(" SELECT ");
		hql.append(" 	MF.FEE_ID , ");
		hql.append(" 	ISNULL(RD.FEE_NAME, ");
		hql.append(" 	MF.FEE_NAME) AS FEE_NAME , ");
		hql.append(" 	ISNULL(RD.FEE_MNY, ");
		hql.append(" 	MF.FEE_MNY) AS FEE_MNY , ");
		hql.append(" 	MF.TRADER_TYPE , ");
		hql.append(" 	MF.REGISTRATION_TYPE, ");
		hql.append(" 	RD.RECEIPT_ID, ");
		hql.append(" 	RD.RECEIPT_DETAIL_ID ");
		hql.append(" FROM ");
		hql.append(" 	RECEIPT_DETAIL RD ");
		hql.append(" 		INNER JOIN ");
		hql.append(" 		MAS_FEE MF ");
		hql.append(" 		ON ");
		hql.append(" 		(RD.FEE = MF.FEE_ID) ");
		hql.append(" WHERE ");
		//hql.append(" 	MF.RECORD_STATUS = 'N' AND ");
		
		hql.append(" 	MF.TRADER_TYPE = :TRADER_TYPE AND  ");
		parames.put("TRADER_TYPE", traderType);
		
		hql.append(" 	MF.REGISTRATION_TYPE = :REGISTRATION_TYPE ");
		parames.put("REGISTRATION_TYPE", registrationType);
		hql.append("  	AND ");
		
		hql.append(" 	RD.RECEIPT_ID = :RECEIPT_ID   ");
		parames.put("RECEIPT_ID", receiptId);
		
//		hql.append(" 	 ) SEL ");
//		hql.append(" 		ON ");
//		hql.append(" 		(SEL.FEE_ID = MAC.FEE_ID) ");
//		hql.append(" WHERE ");
//		hql.append(" 	MAC.RECORD_STATUS = 'N' AND ");
//		
//		hql.append(" 	MAC.TRADER_TYPE = :MAC_TRADER_TYPE AND  ");
//		parames.put("MAC_TRADER_TYPE", traderType);
//		
//		hql.append(" 	MAC.REGISTRATION_TYPE = :MAC_REGISTRATION_TYPE ");
//		parames.put("MAC_REGISTRATION_TYPE", registrationType);
		
	    System.out.println("hql findReceiptDetailFee: "+hql.toString());
	    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
    
		
	    sqlQuery.addScalar("FEE_ID", Hibernate.LONG); //0
	    sqlQuery.addScalar("FEE_NAME", Hibernate.STRING); //1
	    sqlQuery.addScalar("FEE_MNY", Hibernate.BIG_DECIMAL); //2
	    sqlQuery.addScalar("TRADER_TYPE", Hibernate.STRING);//3
	    sqlQuery.addScalar("REGISTRATION_TYPE", Hibernate.STRING);//4
	    sqlQuery.addScalar("RECEIPT_ID", Hibernate.LONG);//5
	    sqlQuery.addScalar("RECEIPT_DETAIL_ID", Hibernate.LONG);//6
	    
	    
	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
	    
	    List<ReceiptDetailDTO> list = new ArrayList<ReceiptDetailDTO>();
	    
	    if(!result.isEmpty())
	    {
	    	for(Object[] obj: result)
	    	{
	    		ReceiptDetailDTO dto = new ReceiptDetailDTO();
	    		
	    		dto.setFeeId(Long.valueOf(obj[0].toString()));
	    		dto.setFeeName(obj[1]==null?"":obj[1].toString());
	    		dto.setFeeMny(obj[2]==null?new BigDecimal(0):new BigDecimal(obj[2].toString()));
	    		dto.setTraderType(obj[3]==null?"":obj[3].toString());
	    		dto.setRegistrationType(obj[4]==null?"":obj[4].toString());
	    		
	    		if(obj[5] != null)
	    		{
	    			dto.setReceiptId(Long.valueOf(obj[5].toString()));
	    		}
	    		if(obj[6] != null)
	    		{
	    			dto.setReceiptDetailId(Long.valueOf(obj[6].toString()));
	    		}
	    		
	    		
	    		
	    		list.add(dto);
	    	}
	    }
  
	    return list;
	}
}
