package sss.dot.tourism.dao.registration;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.cglib.reflect.FastClass;
import net.sf.cglib.reflect.FastMethod;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import com.sss.aut.service.User;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.ReceiptMapRegistration;
import sss.dot.tourism.dto.registration.ReceiptDTO;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.util.DateUtils;

@Repository("receiptMapRegistrationDAO")
public class ReceiptMapRegistrationDAO extends BaseDAO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6707289308682219670L;

	public ReceiptMapRegistrationDAO()
	{
		this.domainObj = ReceiptMapRegistration.class;
	}
	
	/**
	   * 
	   * @see com.sss.dao.IBaseDAO#insert(java.lang.Object)
	   */
	  public Object insert(Object persistence)
	  {
//	    try
//	    {
//	      FastClass clazz = FastClass.create(persistence.getClass());
//
////	      FastMethod method = clazz.getMethod("setRecordStatus", new Class[] { String.class });
//	//
////	      method.invoke(persistence, new Object[] { "N" });
//
//	      FastMethod method2 = clazz.getMethod("setCreateDtm", new Class[] { Date.class });
//
//	      method2.invoke(persistence, new Object[] { new Date() });
//
//	    }
//	    catch (InvocationTargetException e)
//	    {
//
//	      this.error(e.getMessage(), e);
//
//	    }
	    return this.getHibernateTemplate().save(persistence);
	  }
	  /**
	   * 
	   * @see com.sss.dao.IBaseDAO#update(java.lang.Object)
	   */
	  public void update(Object persistence)
	  {

//	    try
//	    {
//	      FastClass clazz = FastClass.create(persistence.getClass());
//
//	      FastMethod method = clazz.getMethod("setLastUpdDtm", new Class[] { Date.class });
//
//	      method.invoke(persistence, new Object[] { new Date() });
//
//	    }
//	    catch (InvocationTargetException e)
//	    {
//
//	      this.error(e.getMessage(), e);
//
//	    }
	    this.getHibernateTemplate().update(persistence);
	  }
	  public List<Object[]> findReceiptRegistrationType(ReceiptDTO dto, User user)throws Exception
	  {
		  Map<String,Object> parames = new HashMap<String,Object>();
		    
		  StringBuilder hql = new StringBuilder(); 
		  hql.append("	select reg.REGISTRATION_TYPE AS REGISTRATION_TYPE, ");
		  hql.append(" fee.FEE_MNY  AS FEE_MNY ");
		  hql.append(" from RECEIPT_MAP_REGISTRATION RMG ");
          hql.append(" inner join REGISTRATION reg on reg.REG_ID = RMG.REG_ID ");
		  hql.append(" inner join TRADER tr on tr.TRADER_ID = reg.TRADER_ID "); 
		  hql.append(" inner join MAS_FEE fee on fee.REGISTRATION_TYPE = reg.REGISTRATION_TYPE  ");
          hql.append(" where fee.REGISTRATION_TYPE IN ('N','C','T','R') ");
          hql.append(" and tr.TRADER_TYPE = 'G' ");
		  hql.append(" and fee.TRADER_TYPE = 'G' ");
		  hql.append(" and fee.FEE_ID in ('5','6','12') ");

		  if((null != dto.getReceiptDateFrom()) && (!dto.getReceiptDateFrom().equals("")))
		    {
		    	if((null != dto.getReceiptDateTo()) && (!dto.getReceiptDateTo().equals("")))
		    	{
					hql.append("  and ( RMG.RECEIPT_MAP_DATE >=  convert(datetime, :RMG_START_DATE ,20) ");
					hql.append(" and RMG.RECEIPT_MAP_DATE <=  convert(datetime, :RMG_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getReceiptDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getReceiptDateTo(), "23:00");
						
					parames.put("RMG_START_DATE", sendDtmFrom);
					parames.put("RMG_END_DATE", sendDtmTo);
		    	}else
		    	{
		    		hql.append("  and ( RMG.RECEIPT_MAP_DATE >=  convert(datetime, :RMG_START_DATE ,20) ");
					hql.append(" and RMG.RECEIPT_MAP_DATE <=  convert(datetime, :RMG_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getReceiptDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getReceiptDateFrom(), "23:00");
						
					parames.put("RMG_START_DATE", sendDtmFrom);
					parames.put("RMG_END_DATE", sendDtmFrom);
		    	}
		    }
		  
		  hql.append("group by reg.REGISTRATION_TYPE,fee.FEE_MNY ");
		  
		  SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
			
		  sqlQuery.addScalar("REGISTRATION_TYPE", Hibernate.STRING); 
		  sqlQuery.addScalar("FEE_MNY", Hibernate.BIG_DECIMAL); 
		 
		  
		  sqlQuery.setProperties(parames);
		  List<Object[]>  result = sqlQuery.list();
	  
		    return result;
	  }
	  public List<Object[]> findReceiptSumMnyNewRegistrationGeneral(ReceiptDTO dto, User user)throws Exception
	  {
		  Map<String,Object> parames = new HashMap<String,Object>();
		    
		  StringBuilder hql = new StringBuilder(); 
		  hql.append("	select sum(REC.FEE_MNY) AS SUM_FEE_MNY, ");
		  hql.append(" count(reg.REG_ID) AS TOTAL, ");
		  hql.append(" REC.FEE_MNY AS FEE_MNY ");
		  hql.append(" from RECEIPT_MAP_REGISTRATION RMG ");
          hql.append(" inner join REGISTRATION reg on reg.REG_ID = RMG.REG_ID ");
		  hql.append(" inner join TRADER tr on tr.TRADER_ID = reg.TRADER_ID "); 
		  
		  //OAT ADD 03/11/58
		  hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = tr.TRADER_ID) "); 
		  
		  //hql.append(" inner join MAS_FEE fee on fee.REGISTRATION_TYPE = reg.REGISTRATION_TYPE  ");
		  hql.append(" inner join RECEIPT_DETAIL REC ON RMG.RECEIPT_ID = REC.RECEIPT_ID ");
		  
		  //Oat Edit
		  
		  //OAT EDIT 03/11/58
//		  hql.append(" where reg.ORG_ID = :REGISTRATION_ORG_ID and reg.REGISTRATION_TYPE = 'N' ");
		  hql.append(" where reg.REGISTRATION_TYPE = 'N' ");
          
          //End Oat Edit
          
          hql.append(" and tr.TRADER_TYPE = 'G' ");
		  //hql.append(" and fee.TRADER_TYPE = 'G' ");
		  hql.append(" and tr.TRADER_CATEGORY between '100' AND '101' ");
		  hql.append(" and REC.FEE = '5' ");

		  if((null != dto.getReceiptDateFrom()) && (!dto.getReceiptDateFrom().equals("")))
		    {
		    	if((null != dto.getReceiptDateTo()) && (!dto.getReceiptDateTo().equals("")))
		    	{
					hql.append("  and ( RMG.RECEIPT_MAP_DATE >=  convert(datetime, :RMG_START_DATE ,20) ");
					hql.append(" and RMG.RECEIPT_MAP_DATE <=  convert(datetime, :RMG_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getReceiptDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getReceiptDateTo(), "23:00");
						
					parames.put("RMG_START_DATE", sendDtmFrom);
					parames.put("RMG_END_DATE", sendDtmTo);
		    	}else
		    	{
		    		hql.append("  and ( RMG.RECEIPT_MAP_DATE >=  convert(datetime, :RMG_START_DATE ,20) ");
					hql.append(" and RMG.RECEIPT_MAP_DATE <=  convert(datetime, :RMG_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getReceiptDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getReceiptDateFrom(), "23:00");
						
					parames.put("RMG_START_DATE", sendDtmFrom);
					parames.put("RMG_END_DATE", sendDtmFrom);
		    	}
		    }
		  
		  //OAT ADD 03/11/58
		  hql.append(" and TA.ADDRESS_TYPE = 'S' AND TA.RECORD_STATUS = 'N' AND TA.PROVINCE_ID IN (SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :REGISTRATION_ORG_ID ) ");     
		  System.out.println("dto.getOrgId() = "+dto.getOrgId());
		  if(dto.getOrgId() != 0)
		  {
			  System.out.println("Not equal 0");
			  parames.put("REGISTRATION_ORG_ID", dto.getOrgId());
		  }
		  else
		  {
			  System.out.println("Equal 0");
			  parames.put("REGISTRATION_ORG_ID", user.getUserData().getOrgId());
		  }
		  //
		  
		  hql.append(" group by REC.FEE_MNY ");
		  SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
			
		  sqlQuery.addScalar("SUM_FEE_MNY", Hibernate.BIG_DECIMAL); 
		  sqlQuery.addScalar("TOTAL", Hibernate.STRING);  
		  sqlQuery.addScalar("FEE_MNY", Hibernate.BIG_DECIMAL);  
		  sqlQuery.setProperties(parames);
		  List<Object[]>  result = sqlQuery.list();
	  
		    return result;
	  }
	  public List<Object[]> findReceiptSumMnyNewRegistrationPrivate(ReceiptDTO dto, User user)throws Exception
	  {
		  Map<String,Object> parames = new HashMap<String,Object>();
		    
		  StringBuilder hql = new StringBuilder(); 
		  hql.append("	select sum(REC.FEE_MNY) AS SUM_FEE_MNY, ");
		  hql.append(" count(reg.REG_ID) AS TOTAL, ");
		  hql.append(" REC.FEE_MNY AS FEE_MNY ");
		  hql.append(" from RECEIPT_MAP_REGISTRATION RMG ");
          hql.append(" inner join REGISTRATION reg on reg.REG_ID = RMG.REG_ID ");
		  hql.append(" inner join TRADER tr on tr.TRADER_ID = reg.TRADER_ID "); 
		  
		  //OAT ADD 03/11/58
		  hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = tr.TRADER_ID) "); 
		  
		  //hql.append(" inner join MAS_FEE fee on fee.REGISTRATION_TYPE = reg.REGISTRATION_TYPE  ");
		  hql.append(" INNER JOIN RECEIPT_DETAIL REC ON RMG.RECEIPT_ID = REC.RECEIPT_ID ");
		  
		  //Oat Edit
		  
		  hql.append(" where reg.REGISTRATION_TYPE = 'N' ");
		  
//		  hql.append(" where reg.ORG_ID = :REGISTRATION_ORG_ID and reg.REGISTRATION_TYPE = 'N' ");
		  
//		  if(dto.getOrgId() != 0)
//		  {
//			  parames.put("REGISTRATION_ORG_ID", dto.getOrgId());
//		  }
//		  else
//		  {
//			  parames.put("REGISTRATION_ORG_ID", user.getUserData().getOrgId());
//		  }
//		  parames.put("REGISTRATION_ORG_ID", user.getUserData().getOrgId());
//          hql.append(" where reg.REGISTRATION_TYPE = 'N' ");
          
          //End Oat Edit
		  
          hql.append(" and tr.TRADER_TYPE = 'G' ");
		  //hql.append(" and fee.TRADER_TYPE = 'G' ");
		  hql.append(" and tr.TRADER_CATEGORY between '200' AND '207' ");
		  hql.append(" and REC.FEE = '5' ");

		  if((null != dto.getReceiptDateFrom()) && (!dto.getReceiptDateFrom().equals("")))
		    {
		    	if((null != dto.getReceiptDateTo()) && (!dto.getReceiptDateTo().equals("")))
		    	{
					hql.append("  and ( RMG.RECEIPT_MAP_DATE >=  convert(datetime, :RMG_START_DATE ,20) ");
					hql.append(" and RMG.RECEIPT_MAP_DATE <=  convert(datetime, :RMG_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getReceiptDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getReceiptDateTo(), "23:00");
						
					parames.put("RMG_START_DATE", sendDtmFrom);
					parames.put("RMG_END_DATE", sendDtmTo);
		    	}else
		    	{
		    		hql.append("  and ( RMG.RECEIPT_MAP_DATE >=  convert(datetime, :RMG_START_DATE ,20) ");
					hql.append(" and RMG.RECEIPT_MAP_DATE <=  convert(datetime, :RMG_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getReceiptDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getReceiptDateFrom(), "23:00");
						
					parames.put("RMG_START_DATE", sendDtmFrom);
					parames.put("RMG_END_DATE", sendDtmFrom);
		    	}
		    }
		  
		//OAT ADD 03/11/58
		  hql.append(" and TA.ADDRESS_TYPE = 'S' AND TA.RECORD_STATUS = 'N' AND TA.PROVINCE_ID IN (SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :REGISTRATION_ORG_ID ) ");     
		  System.out.println("dto.getOrgId() = "+dto.getOrgId());
		  if(dto.getOrgId() != 0)
		  {
			  System.out.println("Not equal 0");
			  parames.put("REGISTRATION_ORG_ID", dto.getOrgId());
		  }
		  else
		  {
			  System.out.println("Equal 0");
			  parames.put("REGISTRATION_ORG_ID", user.getUserData().getOrgId());
		  }
		  //
		  
		  hql.append(" group by REC.FEE_MNY ");
		  SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
			
		  sqlQuery.addScalar("SUM_FEE_MNY", Hibernate.BIG_DECIMAL); 
		  sqlQuery.addScalar("TOTAL", Hibernate.STRING);  
		  sqlQuery.addScalar("FEE_MNY", Hibernate.BIG_DECIMAL);  
		  sqlQuery.setProperties(parames);
		  List<Object[]>  result = sqlQuery.list();
	  
		    return result;
	  }
	  public List<Object[]> findReceiptSumMnyReNewRegistrationGeneral(ReceiptDTO dto, User user)throws Exception
	  {
		  Map<String,Object> parames = new HashMap<String,Object>();
		    
		  StringBuilder hql = new StringBuilder(); 
		  hql.append("	select sum(REC.FEE_MNY) AS SUM_FEE_MNY, ");
		  hql.append(" count(reg.REG_ID) AS TOTAL, ");
		  hql.append(" REC.FEE_MNY AS FEE_MNY ");
		  hql.append(" from RECEIPT_MAP_REGISTRATION RMG ");
          hql.append(" inner join REGISTRATION reg on reg.REG_ID = RMG.REG_ID ");
		  hql.append(" inner join TRADER tr on tr.TRADER_ID = reg.TRADER_ID "); 
		  
		//OAT ADD 03/11/58
		  hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = tr.TRADER_ID) "); 
		  
		  //hql.append(" inner join MAS_FEE fee on fee.REGISTRATION_TYPE = reg.REGISTRATION_TYPE  ");
		  hql.append(" INNER JOIN RECEIPT_DETAIL REC ON RMG.RECEIPT_ID = REC.RECEIPT_ID ");
		  
		  //Oat Edit 03/11/58
		  
		  hql.append(" where reg.REGISTRATION_TYPE = 'R' ");
		  
//		  hql.append(" where reg.ORG_ID = :REGISTRATION_ORG_ID and reg.REGISTRATION_TYPE = 'R' ");
//		  
//		  if(dto.getOrgId() != 0)
//		  {
//			  parames.put("REGISTRATION_ORG_ID", dto.getOrgId());
//		  }
//		  else
//		  {
//			  parames.put("REGISTRATION_ORG_ID", user.getUserData().getOrgId());
//		  }
		  
//		  parames.put("REGISTRATION_ORG_ID", user.getUserData().getOrgId());
//         hql.append(" where reg.REGISTRATION_TYPE = 'R' ");
          
          //End Oat Edit
		  
          hql.append(" and tr.TRADER_TYPE = 'G' ");
		  //hql.append(" and fee.TRADER_TYPE = 'G' ");
		  hql.append(" and tr.TRADER_CATEGORY between '100' AND '101' ");
		  hql.append(" and REC.FEE = '6' ");

		  if((null != dto.getReceiptDateFrom()) && (!dto.getReceiptDateFrom().equals("")))
		    {
		    	if((null != dto.getReceiptDateTo()) && (!dto.getReceiptDateTo().equals("")))
		    	{
					hql.append("  and ( RMG.RECEIPT_MAP_DATE >=  convert(datetime, :RMG_START_DATE ,20) ");
					hql.append(" and RMG.RECEIPT_MAP_DATE <=  convert(datetime, :RMG_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getReceiptDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getReceiptDateTo(), "23:00");
						
					parames.put("RMG_START_DATE", sendDtmFrom);
					parames.put("RMG_END_DATE", sendDtmTo);
		    	}else
		    	{
		    		hql.append("  and ( RMG.RECEIPT_MAP_DATE >=  convert(datetime, :RMG_START_DATE ,20) ");
					hql.append(" and RMG.RECEIPT_MAP_DATE <=  convert(datetime, :RMG_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getReceiptDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getReceiptDateFrom(), "23:00");
						
					parames.put("RMG_START_DATE", sendDtmFrom);
					parames.put("RMG_END_DATE", sendDtmFrom);
		    	}
		    }
		  
		//OAT ADD 03/11/58
		  hql.append(" and TA.ADDRESS_TYPE = 'S' AND TA.RECORD_STATUS = 'N' AND TA.PROVINCE_ID IN (SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :REGISTRATION_ORG_ID ) ");     
		  System.out.println("dto.getOrgId() = "+dto.getOrgId());
		  if(dto.getOrgId() != 0)
		  {
			  System.out.println("Not equal 0");
			  parames.put("REGISTRATION_ORG_ID", dto.getOrgId());
		  }
		  else
		  {
			  System.out.println("Equal 0");
			  parames.put("REGISTRATION_ORG_ID", user.getUserData().getOrgId());
		  }
		  //
		  
		  hql.append(" group by REC.FEE_MNY ");
		  
		  SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
			
		  sqlQuery.addScalar("SUM_FEE_MNY", Hibernate.BIG_DECIMAL); 
		  sqlQuery.addScalar("TOTAL", Hibernate.STRING);  
		  sqlQuery.addScalar("FEE_MNY", Hibernate.BIG_DECIMAL);  
		  sqlQuery.setProperties(parames);
		  List<Object[]>  result = sqlQuery.list();
	  
		    return result;
	  }
	  public List<Object[]> findReceiptSumMnyReNewRegistrationPrivate(ReceiptDTO dto, User user)throws Exception
	  {
		  Map<String,Object> parames = new HashMap<String,Object>();
		    
		  StringBuilder hql = new StringBuilder(); 
		  hql.append("	select sum(REC.FEE_MNY) AS SUM_FEE_MNY, ");
		  hql.append(" count(reg.REG_ID) AS TOTAL, ");
		  hql.append(" REC.FEE_MNY AS FEE_MNY ");
		  hql.append(" from RECEIPT_MAP_REGISTRATION RMG ");
          hql.append(" inner join REGISTRATION reg on reg.REG_ID = RMG.REG_ID ");
		  hql.append(" inner join TRADER tr on tr.TRADER_ID = reg.TRADER_ID "); 
		  
		//OAT ADD 03/11/58
		  hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = tr.TRADER_ID) "); 
		  
		  //hql.append(" inner join MAS_FEE fee on fee.REGISTRATION_TYPE = reg.REGISTRATION_TYPE  ");
		  hql.append(" INNER JOIN RECEIPT_DETAIL REC ON RMG.RECEIPT_ID = REC.RECEIPT_ID ");
		  
		  //Oat Edit
		  
		  hql.append(" where reg.REGISTRATION_TYPE = 'R' ");
		  
//		  hql.append(" where reg.ORG_ID = :REGISTRATION_ORG_ID and reg.REGISTRATION_TYPE = 'R' ");
//		  
//		  if(dto.getOrgId() != 0)
//		  {
//			  parames.put("REGISTRATION_ORG_ID", dto.getOrgId());
//		  }
//		  else
//		  {
//			  parames.put("REGISTRATION_ORG_ID", user.getUserData().getOrgId());
//		  }
		  
//		  parames.put("REGISTRATION_ORG_ID", user.getUserData().getOrgId());
//         hql.append(" where reg.REGISTRATION_TYPE = 'R' ");
          
          //End Oat Edit
		  

          hql.append(" and tr.TRADER_TYPE = 'G' ");
		  //hql.append(" and fee.TRADER_TYPE = 'G' ");
		  hql.append(" and tr.TRADER_CATEGORY between '200' AND '207' ");
		  hql.append(" and REC.FEE = '6' ");

		  if((null != dto.getReceiptDateFrom()) && (!dto.getReceiptDateFrom().equals("")))
		    {
		    	if((null != dto.getReceiptDateTo()) && (!dto.getReceiptDateTo().equals("")))
		    	{
					hql.append("  and ( RMG.RECEIPT_MAP_DATE >=  convert(datetime, :RMG_START_DATE ,20) ");
					hql.append(" and RMG.RECEIPT_MAP_DATE <=  convert(datetime, :RMG_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getReceiptDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getReceiptDateTo(), "23:00");
						
					parames.put("RMG_START_DATE", sendDtmFrom);
					parames.put("RMG_END_DATE", sendDtmTo);
		    	}else
		    	{
		    		hql.append("  and ( RMG.RECEIPT_MAP_DATE >=  convert(datetime, :RMG_START_DATE ,20) ");
					hql.append(" and RMG.RECEIPT_MAP_DATE <=  convert(datetime, :RMG_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getReceiptDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getReceiptDateFrom(), "23:00");
						
					parames.put("RMG_START_DATE", sendDtmFrom);
					parames.put("RMG_END_DATE", sendDtmFrom);
		    	}
		    }
		  
		//OAT ADD 03/11/58
		  hql.append(" and TA.ADDRESS_TYPE = 'S' AND TA.RECORD_STATUS = 'N' AND TA.PROVINCE_ID IN (SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :REGISTRATION_ORG_ID ) ");     
		  System.out.println("dto.getOrgId() = "+dto.getOrgId());
		  if(dto.getOrgId() != 0)
		  {
			  System.out.println("Not equal 0");
			  parames.put("REGISTRATION_ORG_ID", dto.getOrgId());
		  }
		  else
		  {
			  System.out.println("Equal 0");
			  parames.put("REGISTRATION_ORG_ID", user.getUserData().getOrgId());
		  }
		  //
		  
		  hql.append(" group by REC.FEE_MNY ");
		  SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
			
		  sqlQuery.addScalar("SUM_FEE_MNY", Hibernate.BIG_DECIMAL); 
		  sqlQuery.addScalar("TOTAL", Hibernate.STRING);  
		  sqlQuery.addScalar("FEE_MNY", Hibernate.BIG_DECIMAL);
		  sqlQuery.setProperties(parames);
		  List<Object[]>  result = sqlQuery.list();
	  
		    return result;
	  }
	  public List<Object[]> findReceiptSumMnyTemporaryRegistration(ReceiptDTO dto, User user)throws Exception
	  {
		  Map<String,Object> parames = new HashMap<String,Object>();
		    
		  StringBuilder hql = new StringBuilder(); 
		  hql.append("	select sum(REC.FEE_MNY) AS SUM_FEE_MNY, ");
		  hql.append(" count(reg.REG_ID) AS TOTAL, ");
		  hql.append(" REC.FEE_MNY AS FEE_MNY ");
		  hql.append(" from RECEIPT_MAP_REGISTRATION RMG ");
          hql.append(" inner join REGISTRATION reg on reg.REG_ID = RMG.REG_ID ");
		  hql.append(" inner join TRADER tr on tr.TRADER_ID = reg.TRADER_ID "); 
		  //hql.append(" inner join MAS_FEE fee on fee.REGISTRATION_TYPE = reg.REGISTRATION_TYPE  ");
		  hql.append(" INNER JOIN RECEIPT_DETAIL REC ON RMG.RECEIPT_ID = REC.RECEIPT_ID ");
          hql.append(" where reg.REGISTRATION_TYPE = 'T' ");
          hql.append(" and tr.TRADER_TYPE = 'G' ");
		  //hql.append(" and fee.TRADER_TYPE = 'G' ");
		  //hql.append(" and tr.TRADER_CATEGORY between '200' AND '207' ");
		  hql.append(" and REC.FEE = '12' ");

		  if((null != dto.getReceiptDateFrom()) && (!dto.getReceiptDateFrom().equals("")))
		    {
		    	if((null != dto.getReceiptDateTo()) && (!dto.getReceiptDateTo().equals("")))
		    	{
					hql.append("  and ( RMG.RECEIPT_MAP_DATE >=  convert(datetime, :RMG_START_DATE ,20) ");
					hql.append(" and RMG.RECEIPT_MAP_DATE <=  convert(datetime, :RMG_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getReceiptDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getReceiptDateTo(), "23:00");
						
					parames.put("RMG_START_DATE", sendDtmFrom);
					parames.put("RMG_END_DATE", sendDtmTo);
		    	}else
		    	{
		    		hql.append("  and ( RMG.RECEIPT_MAP_DATE >=  convert(datetime, :RMG_START_DATE ,20) ");
					hql.append(" and RMG.RECEIPT_MAP_DATE <=  convert(datetime, :RMG_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getReceiptDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getReceiptDateFrom(), "23:00");
						
					parames.put("RMG_START_DATE", sendDtmFrom);
					parames.put("RMG_END_DATE", sendDtmFrom);
		    	}
		    }
		  
		  hql.append(" group by REC.FEE_MNY ");
		  SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
			
		  sqlQuery.addScalar("SUM_FEE_MNY", Hibernate.BIG_DECIMAL); 
		  sqlQuery.addScalar("TOTAL", Hibernate.STRING);  
		  sqlQuery.addScalar("FEE_MNY", Hibernate.BIG_DECIMAL);
		  sqlQuery.setProperties(parames);
		  List<Object[]>  result = sqlQuery.list();
	  
		    return result;
	  }
}
