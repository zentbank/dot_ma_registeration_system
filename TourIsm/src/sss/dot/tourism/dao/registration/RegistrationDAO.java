package sss.dot.tourism.dao.registration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.Registration;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.domain.TraderAddress;
import sss.dot.tourism.dto.registration.CommitteeDTO;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.ProgressStatus;
import sss.dot.tourism.util.RecordStatus;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.User;


@Repository("registrationDAO")
public class RegistrationDAO extends BaseDAO{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6455075565457388105L;

	public RegistrationDAO()
	{
		this.domainObj = Registration.class;
	}
	
		
	
	public List<Object[]> findTraderBetweenRegistration(RegistrationDTO dto, User user ,int start, int limit)throws Exception
	{
		   Map<String,Object> parames = new HashMap<String,Object>();
		    
		    StringBuilder hql = new StringBuilder();
		    
		  
		    hql.append(" SELECT RE.REG_ID AS REG_ID, ");
		    hql.append(" TR.TRADER_ID AS TRADER_ID, ");
		    hql.append(" PE.PERSON_ID AS PERSON_ID ");
		    hql.append(" FROM REGISTRATION RE ");
		    hql.append(" INNER JOIN TRADER TR ");
		    hql.append(" ON(RE.TRADER_ID = TR.TRADER_ID) ");
		    hql.append(" INNER JOIN PERSON PE ");
		    hql.append(" ON(PE.PERSON_ID = TR.PERSON_ID) ");
		    
		    hql.append(" WHERE RE.RECORD_STATUS = :REGISTRATION_RECORD_STATUS ");
		    
		    parames.put("REGISTRATION_RECORD_STATUS", dto.getRegRecordStatus());
		    
		    hql.append(" AND TR.RECORD_STATUS = :TRADER_RECORD_STATUS ");
		    
		    parames.put("TRADER_RECORD_STATUS", dto.getTraderRecordStatus());
		    
		    hql.append(" AND RE.ORG_ID = :REGISTRATION_ORG_ID ");
		    
		    parames.put("REGISTRATION_ORG_ID", user.getUserData().getOrgId());
		    
		    hql.append(" AND TR.BRANCH_TYPE IS NULL ");
			    
		    if((null != dto.getTraderName()) && (!"".equals(dto.getTraderName())))
		    {
			    hql.append(" AND TR.TRADER_NAME LIKE :TRADER_TRADER_NAME ");
			    
			    parames.put("TRADER_TRADER_NAME", "%"+dto.getTraderName()+"%");
		    }
		    
		    if((null != dto.getTraderNameEn()) && (!"".equals(dto.getTraderNameEn())))
		    {
			    hql.append(" AND TR.TRADER_NAME_EN LIKE :TRADER_TRADER_NAME_EN ");
			    
			    parames.put("TRADER_TRADER_NAME_EN", "%"+dto.getTraderNameEn()+"%");
		    }
		    
		    if((null != dto.getFirstName()) && (!"".equals(dto.getFirstName())))
		    {
			    hql.append(" AND PE.FIRST_NAME LIKE :TRADER_FIRST_NAME ");
			    
			    parames.put("TRADER_FIRST_NAME", "%"+dto.getFirstName()+"%");
		    }
		    
		    if(dto.getTraderId() > 0)
		    {
		    	 hql.append(" AND TR.TRADER_ID = :TRADER_TRADER_ID ");
				    
				 parames.put("TRADER_TRADER_ID", dto.getTraderId());
		    }
		    if((null != dto.getTraderType()) && (!"".equals(dto.getTraderType())))
		    {
		    	 hql.append(" AND TR.TRADER_TYPE = :TRADER_TRADER_TYPE ");
				    
				 parames.put("TRADER_TRADER_TYPE", dto.getTraderType());
		    }
		    if((null != dto.getTraderCategory()) && (!"".equals(dto.getTraderCategory())))
		    {
		    	 hql.append(" AND TR.TRADER_CATEGORY = :TRADER_TRADER_CATEGORY ");
				    
				 parames.put("TRADER_TRADER_CATEGORY", dto.getTraderCategory());
		    }
		    
		    /*
		    AND RE.REGISTRATION_TYPE = ''
		    AND RE.REGISTRATION_NO = ''
		    AND RE.REGISTRATION_DATE BETWEEN X AND B */
//		    -- ยังไม่ส่งเรื่อง
//		    AND (RE.REGISTRATION_PROGRESS = 'KEY'
//		    -- ส่งเรื่องแล้ว
//		    OR RE.REGISTRATION_PROGRESS <> 'KEY')
//		    -- ตรวจสอบจาก register_progress
//		    ;
		    
		    if((null != dto.getRegistrationType()) && (!"".equals(dto.getRegistrationType())))
		    {
			    hql.append(" AND RE.REGISTRATION_TYPE = :REGISTRATION_REGISTRATION_TYPE ");
			    
			    parames.put("REGISTRATION_REGISTRATION_TYPE", dto.getRegistrationType());
		    }
		    
		    if((null != dto.getRegistrationNo()) && (!"".equals(dto.getRegistrationNo())))
		    {
			    hql.append(" AND RE.REGISTRATION_NO = :REGISTRATION_REGISTRATION_NO ");
			    
			    parames.put("REGISTRATION_REGISTRATION_NO", dto.getRegistrationNo());
		    }
		    

		    
		    
		    
		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
		    sqlQuery.setFirstResult(start);
		    sqlQuery.setMaxResults(limit);
			
		    sqlQuery.addScalar("REG_ID", Hibernate.LONG); 
		    sqlQuery.addScalar("TRADER_ID", Hibernate.LONG); 
		    sqlQuery.addScalar("PERSON_ID", Hibernate.LONG); 
		    
		    sqlQuery.setProperties(parames);
		    List<Object[]>  result = sqlQuery.list();
	  
		    return result;
	}
	
	public List<Object[]> findBetweenRegistrationPaging(RegistrationDTO dto, User user ,int start, int limit)throws Exception
	{
		Map<String,Object> parames = new HashMap<String,Object>();
	    
	    StringBuilder hql = new StringBuilder();
		

	    
	    hql.append(" SELECT RE.REG_ID AS REG_ID, ");
	    hql.append(" RE.REGISTRATION_NO AS REGISTRATION_NO, ");
	    hql.append(" RE.REGISTRATION_DATE AS REGISTRATION_DATE, ");
	    hql.append(" RE.REGISTRATION_PROGRESS AS REGISTRATION_PROGRESS, ");
	    hql.append(" RE.REGISTRATION_TYPE AS REGISTRATION_TYPE, ");
	    hql.append(" RE.ORG_ID AS ORG_ID, ");
	    hql.append(" RE.RECORD_STATUS AS RECORD_STATUS, ");
	    hql.append(" TR.TRADER_ID AS TRADER_ID, ");
	    hql.append(" TR.TRADER_TYPE AS TRADER_TYPE, ");
	    hql.append(" TR.TRADER_NAME AS TRADER_NAME, ");
	    hql.append(" TR.TRADER_NAME_EN AS TRADER_NAME_EN, ");
	    hql.append(" TR.TRADER_CATEGORY AS TRADER_CATEGORY, ");
	    hql.append(" PE.PERSON_ID AS PERSON_ID, ");
	    hql.append(" PE.PERSON_TYPE AS PERSON_TYPE, ");
	    hql.append(" MP.PREFIX_NAME AS PREFIX_NAME, ");
	    hql.append(" PE.FIRST_NAME AS FIRST_NAME, ");
	    hql.append(" PE.LAST_NAME AS LAST_NAME, ");
	    hql.append(" POSF.POSTFIX_NAME AS POSTFIX_NAME, ");
	    hql.append(" MP.PREFIX_ID AS PREFIX_ID, ");
	    hql.append(" POSF.POSTFIX_ID AS POSTFIX_ID, ");
	    hql.append(" PEAMP.AMPHUR_ID AS PERSON_AMPHUR_ID, ");
	    hql.append(" TAXPEAMP.AMPHUR_ID AS PERSON_TAXAMPHUR_ID, ");
	    hql.append(" PEPROV.PROVINCE_ID AS PERSON_PROVINCE_ID, ");
	    hql.append(" TAXPEPROV.PROVINCE_ID AS PERSON_TAXPROVINCE_ID, ");
	    
		
	    hql.append(" TR.TRADER_GUIDE_ID AS TRADER_GUIDE_ID, ");
	    hql.append(" TR.PRONUNCIATION_NAME AS PRONUNCIATION_NAME, ");
	    hql.append(" TR.LICENSE_GUIDE_NO AS LICENSE_GUIDE_NO, ");
	    hql.append(" PE.PERSON_TYPE AS PERSON_TYPE, ");
	    hql.append(" PE.IDENTITY_NO AS IDENTITY_NO, ");
	    hql.append(" PE.COMMITTEE_NAME1 AS COMMITTEE_NAME1, ");
	    hql.append(" PE.COMMITTEE_NAME2 AS COMMITTEE_NAME2, ");
	    hql.append(" PE.COMMITTEE_NAME_SIGN AS COMMITTEE_NAME_SIGN, ");
	    hql.append(" PE.PASSPORT_NO AS PASSPORT_NO, ");
	    hql.append(" PE.FIRST_NAME_EN AS FIRST_NAME_EN, ");
	    hql.append(" PE.LAST_NAME_EN AS LAST_NAME_EN, ");
	    hql.append(" PE.GENDER AS GENDER, ");
	    hql.append(" PE.PERSON_NATIONALITY AS PERSON_NATIONALITY, ");
	    hql.append(" PE.BIRTH_DATE AS BIRTH_DATE, ");
	    hql.append(" PE.AGE_YEAR AS AGE_YEAR, ");
	    hql.append(" PE.IDENTITY_NO_EXPIRE AS IDENTITY_NO_EXPIRE, ");
	    hql.append(" PE.CORPORATE_TYPE AS CORPORATE_TYPE, ");
	    hql.append(" PE.TAX_IDENTITY_NO AS TAX_IDENTITY_NO, ");
	    hql.append(" PE.IDENTITY_DATE AS IDENTITY_DATE, ");
	    hql.append(" TR.RECORD_STATUS AS RECORD_STATUS, ");
	    
	    hql.append(" PEAMP.AMPHUR_NAME AS PERSON_AMPHUR_NAME, ");
	    hql.append(" TAXPEAMP.AMPHUR_NAME AS PERSON_TAXAMPHUR_NAME, ");
	    hql.append(" PEPROV.PROVINCE_NAME AS PERSON_PROVINCE_NAME, ");
	    hql.append(" TAXPEPROV.PROVINCE_NAME AS PERSON_TAXPROVINCE_NAME, ");
	    hql.append(" MP.PREFIX_NAME_EN AS PREFIX_NAME_EN, ");
	    hql.append(" TR.LICENSE_NO AS LICENSE_NO ");
	    

	    hql.append(" FROM REGISTRATION RE ");
	    hql.append(" INNER JOIN TRADER TR ");
	    hql.append(" ON(RE.TRADER_ID = TR.TRADER_ID) ");
	 
	    hql.append(" INNER JOIN PERSON PE ");
	    hql.append(" ON(PE.PERSON_ID = TR.PERSON_ID) ");

	    hql.append(" LEFT JOIN MAS_PREFIX MP ");
	    hql.append(" ON(PE.PREFIX_ID = MP.PREFIX_ID) ");

	    hql.append(" LEFT JOIN MAS_POSFIX POSF ");
	    hql.append(" ON(POSF.POSTFIX_ID = PE.POSTFIX_ID) ");


	    hql.append(" LEFT JOIN MAS_AMPHUR PEAMP ");
	    hql.append(" ON(PEAMP.AMPHUR_ID = PE.AMPHUR_ID) ");

	    hql.append(" LEFT JOIN MAS_AMPHUR TAXPEAMP ");
	    hql.append(" ON(TAXPEAMP.AMPHUR_ID = PE.TAX_AMPHUR_ID) ");

	    hql.append(" LEFT JOIN MAS_PROVINCE PEPROV ");
	    hql.append(" ON(PEPROV.PROVINCE_ID = PE.PROVINCE_ID) ");

	    hql.append(" LEFT JOIN MAS_PROVINCE TAXPEPROV ");
	    hql.append(" ON(TAXPEPROV.PROVINCE_ID = PE.TAX_PROVINCE_ID) ");
		
	    hql.append(" WHERE RE.RECORD_STATUS = :REGISTRATION_RECORD_STATUS ");
	    parames.put("REGISTRATION_RECORD_STATUS", dto.getRegRecordStatus());
	    System.out.println("RE.RECORD_STATUS = "+dto.getRegRecordStatus());
	    
	    hql.append(" AND TR.RECORD_STATUS = :TRADER_RECORD_STATUS ");
	    parames.put("TRADER_RECORD_STATUS", dto.getTraderRecordStatus());
	    System.out.println("TR.RECORD_STATUS = "+dto.getTraderRecordStatus());
	    
	    hql.append(" AND RE.ORG_ID = :REGISTRATION_ORG_ID "); 
	    parames.put("REGISTRATION_ORG_ID", user.getUserData().getOrgId());
	    System.out.println("RE.ORG_ID = "+user.getUserData().getOrgId());
	    
//	    hql.append(" AND TRD.ADDRESS_TYPE = 'S' ");
	    hql.append(" AND TR.BRANCH_TYPE IS NULL ");
	    
	    hql.append(" AND TR.LICENSE_STATUS <> 'T' ");

	    if((null != dto.getTraderName()) && (!"".equals(dto.getTraderName())))
	    {
		    hql.append(" AND TR.TRADER_NAME LIKE :TRADER_TRADER_NAME ");
		    
		    parames.put("TRADER_TRADER_NAME", "%"+dto.getTraderName()+"%");
	    }
	    
	    if((null != dto.getTraderNameEn()) && (!"".equals(dto.getTraderNameEn())))
	    {
		    hql.append(" AND TR.TRADER_NAME_EN LIKE :TRADER_TRADER_NAME_EN ");
		    
		    parames.put("TRADER_TRADER_NAME_EN", "%"+dto.getTraderNameEn()+"%");
	    }
	    
	    if((null != dto.getFirstName()) && (!"".equals(dto.getFirstName())))
	    {
		    hql.append(" AND PE.FIRST_NAME LIKE :TRADER_FIRST_NAME ");
		    
		    parames.put("TRADER_FIRST_NAME", "%"+dto.getFirstName()+"%");
	    }
	    
	    if(dto.getTraderId() > 0)
	    {
	    	 hql.append(" AND TR.TRADER_ID = :TRADER_TRADER_ID ");
			    
			 parames.put("TRADER_TRADER_ID", dto.getTraderId());
	    }
	    if((null != dto.getTraderType()) && (!"".equals(dto.getTraderType())))
	    {
	    	 hql.append(" AND TR.TRADER_TYPE = :TRADER_TRADER_TYPE ");
			    
			 parames.put("TRADER_TRADER_TYPE", dto.getTraderType());
			 System.out.println("TR.TRADER_TYPE = "+dto.getTraderType());
	    }
	    if((null != dto.getTraderCategory()) && (!"".equals(dto.getTraderCategory())))
	    {
	    	 hql.append(" AND TR.TRADER_CATEGORY = :TRADER_TRADER_CATEGORY ");
			    
			 parames.put("TRADER_TRADER_CATEGORY", dto.getTraderCategory());
	    }
		
	    if((null != dto.getRegistrationType()) && (!"".equals(dto.getRegistrationType())))
	    {
		    hql.append(" AND RE.REGISTRATION_TYPE = :REGISTRATION_REGISTRATION_TYPE ");
		    
		    parames.put("REGISTRATION_REGISTRATION_TYPE", dto.getRegistrationType());
	    }
	    
	    if((null != dto.getRegistrationNo()) && (!"".equals(dto.getRegistrationNo())))
	    {
		    hql.append(" AND RE.REGISTRATION_NO = :REGISTRATION_REGISTRATION_NO ");
		    
		    parames.put("REGISTRATION_REGISTRATION_NO", dto.getRegistrationNo());
	    }
	    // Role Lawyer Rechech other
//	    if((null != dto.getRegistrationProgress()) && (!"".equals(dto.getRegistrationProgress())))
//	    {
//		    hql.append(" AND RE.REGISTRATION_PROGRESS = :REGISTRATION_REGISTRATION_PROGRESS ");
//		    
//		    parames.put("REGISTRATION_REGISTRATION_PROGRESS", dto.getRegistrationProgress());
//	    }
	    
	    if((null != dto.getRegProgressStatus()) && (!"".equals(dto.getRegProgressStatus())))
	    {
	    	// w ยังไม่ส่งเรื่อง
	    	// w ยังไม่พิมพ์ใบเสร็จ
	    	if(dto.getRegProgressStatus().equals(ProgressStatus.WAITING.getStatus()))
	    	{
			    hql.append(" AND RE.REGISTRATION_PROGRESS = :REGISTRATION_REGISTRATION_PROGRESS ");
			    parames.put("REGISTRATION_REGISTRATION_PROGRESS", dto.getRegistrationProgress());
			    
			    System.out.println("#####RE.REGISTRATION_PROGRESS ="+dto.getRegistrationProgress());
	    	}
	    	else
	    	{
	    		// A ส่งเรื่องแล้ว G  รับเรื่องแล้ว
	    		// A พิมพ์ใบเสร็จแล้ว G  เรื่องที่ยังไม่ส่งมา
			    hql.append(" AND RE.REGISTRATION_PROGRESS in ("+dto.getRegGroupProgress()+") ");
			    
			    System.out.println("#####RE.REGISTRATION_PROGRESS in ("+dto.getRegGroupProgress()+")");
	    	}
	    	
	    	 
	    }
	    
	    if((null != dto.getRegistrationDateFrom()) && (!dto.getRegistrationDateFrom().equals("")))
	    {
	    	if((null != dto.getRegistrationDateTo()) && (!dto.getRegistrationDateTo().equals("")))
	    	{
				hql.append("  and ( RE.REGISTRATION_DATE >=  convert(datetime, :REG_START_DATE ,20) ");
				hql.append(" and RE.REGISTRATION_DATE <=  convert(datetime, :REG_END_DATE ,20) ) ");
				
				String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getRegistrationDateFrom(), "00:00");
				String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getRegistrationDateTo(), "23:00");
					
				parames.put("REG_START_DATE", sendDtmFrom);
				parames.put("REG_END_DATE", sendDtmTo);
	    	}
	    }
	    
	    System.out.println("#####hql.toString() = "+hql.toString());
		
	    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
	    sqlQuery.setFirstResult(start);
	    sqlQuery.setMaxResults(limit);
	    
		sqlQuery.addScalar("REG_ID", Hibernate.LONG); //1
		sqlQuery.addScalar("REGISTRATION_NO", Hibernate.STRING); //2
	    sqlQuery.addScalar("REGISTRATION_DATE", Hibernate.DATE); //3
	    sqlQuery.addScalar("REGISTRATION_PROGRESS", Hibernate.STRING); //4
	    sqlQuery.addScalar("REGISTRATION_TYPE", Hibernate.STRING);//5 
	    sqlQuery.addScalar("ORG_ID", Hibernate.LONG);//6
	    sqlQuery.addScalar("RECORD_STATUS", Hibernate.STRING);//7
	    sqlQuery.addScalar("TRADER_ID", Hibernate.LONG); //8
	    sqlQuery.addScalar("TRADER_TYPE", Hibernate.STRING);//9
	    sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING);//10
	    sqlQuery.addScalar("TRADER_NAME_EN", Hibernate.STRING);//11
	    sqlQuery.addScalar("TRADER_CATEGORY", Hibernate.STRING);//12
	    sqlQuery.addScalar("PERSON_ID", Hibernate.LONG);//13
	    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);//14
	    sqlQuery.addScalar("PREFIX_NAME", Hibernate.STRING);//15
	    sqlQuery.addScalar("FIRST_NAME", Hibernate.STRING);//16
	    sqlQuery.addScalar("LAST_NAME", Hibernate.STRING);//17
	    sqlQuery.addScalar("POSTFIX_NAME", Hibernate.STRING);//18
	    sqlQuery.addScalar("PREFIX_ID", Hibernate.LONG);//26
	    sqlQuery.addScalar("POSTFIX_ID", Hibernate.LONG);//27
	    sqlQuery.addScalar("PERSON_AMPHUR_ID", Hibernate.LONG);//28
	    sqlQuery.addScalar("PERSON_TAXAMPHUR_ID", Hibernate.LONG);//29
	    sqlQuery.addScalar("PERSON_PROVINCE_ID", Hibernate.LONG);//30
	    sqlQuery.addScalar("PERSON_TAXPROVINCE_ID", Hibernate.LONG);//31
	    
	    sqlQuery.addScalar("TRADER_GUIDE_ID", Hibernate.LONG);
	    sqlQuery.addScalar("PRONUNCIATION_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("LICENSE_GUIDE_NO", Hibernate.STRING);
	    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);
	    sqlQuery.addScalar("IDENTITY_NO", Hibernate.STRING);
	    sqlQuery.addScalar("COMMITTEE_NAME1", Hibernate.STRING);
	    sqlQuery.addScalar("COMMITTEE_NAME2", Hibernate.STRING);
	    sqlQuery.addScalar("COMMITTEE_NAME_SIGN", Hibernate.STRING);
	    sqlQuery.addScalar("PASSPORT_NO", Hibernate.STRING);
	    sqlQuery.addScalar("FIRST_NAME_EN", Hibernate.STRING);
	    sqlQuery.addScalar("LAST_NAME_EN", Hibernate.STRING);
	    sqlQuery.addScalar("GENDER", Hibernate.STRING);
	    sqlQuery.addScalar("PERSON_NATIONALITY", Hibernate.STRING);
	    sqlQuery.addScalar("BIRTH_DATE", Hibernate.DATE);
	    sqlQuery.addScalar("AGE_YEAR", Hibernate.STRING);
	    sqlQuery.addScalar("IDENTITY_NO_EXPIRE", Hibernate.DATE);
	    sqlQuery.addScalar("CORPORATE_TYPE", Hibernate.STRING);
	    sqlQuery.addScalar("TAX_IDENTITY_NO", Hibernate.STRING);
	    sqlQuery.addScalar("IDENTITY_DATE", Hibernate.DATE);
	    sqlQuery.addScalar("RECORD_STATUS", Hibernate.STRING);
	    
	    sqlQuery.addScalar("PERSON_AMPHUR_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("PERSON_TAXAMPHUR_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("PERSON_PROVINCE_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("PERSON_TAXPROVINCE_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("PREFIX_NAME_EN", Hibernate.STRING);
	    sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING);

	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
  
	    return result;
	}
	
	public List<Registration> findAllByTrader(long traderId)
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  Registration as reg ");
		hql.append(" where reg.trader.traderId = ? ");

		params.add(traderId);

		return (List<Registration>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
	
	public List<Object[]> listDotTrLicense(long regId)throws Exception
	{
		    StringBuilder hql = new StringBuilder();
		    hql.append(" SELECT LICENSE_FEE_ID, TAX_ID, FEE_REGISTRATION_TYPE FROM DOT_TR_FEE_LICENSE WHERE REG_ID = " + regId);
		    
		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
		    sqlQuery.addScalar("LICENSE_FEE_ID", Hibernate.LONG);//1
		    sqlQuery.addScalar("TAX_ID", Hibernate.STRING);//2
		    sqlQuery.addScalar("FEE_REGISTRATION_TYPE", Hibernate.STRING);//3
		    
		    List<Object[]>  result = sqlQuery.list();
		    
		    return result;
	}
	
	public List<Object[]> findCheckIdentityNoRegistrationPersonSuspend(RegistrationDTO dto, User user)throws Exception
	{
		   Map<String,Object> parames = new HashMap<String,Object>();
		    
		    StringBuilder hql = new StringBuilder();
		    
		    
		    hql.append(" select P.PERSON_ID AS PERSON_ID, ");
		    hql.append(" P.PERSON_TYPE AS PERSON_TYPE, ");
		    hql.append(" PRE.PREFIX_NAME AS PREFIX_NAME, ");
		    hql.append(" PREF.PREFIX_NAME AS POSTFIX_NAME, ");
		    hql.append(" P.FIRST_NAME AS FIRST_NAME, ");
		    hql.append(" P.LAST_NAME AS LAST_NAME, ");
		    hql.append(" CM.COMMITTEE_NAME AS COMMITTEE_NAME,");
		    hql.append(" CM.COMMITTEE_LASTNAME AS COMMITTEE_LASTNAME, "); 
		    hql.append(" CM.COMMITTEE_TYPE AS COMMITTEE_TYPE, "); 
		    hql.append(" SL.SUSPEND_PERIOD AS SUSPEND_PERIOD, ");
		    hql.append(" SL.SUSPEND_FROM AS SUSPEND_FROM, ");
		    hql.append(" SL.SUSPEND_TO AS SUSPEND_TO, ");
		    hql.append(" TR.TRADER_NAME AS TRADER_NAME ");
		   // hql.append(" ");
		    hql.append(" from PERSON P ");
		    hql.append(" left join COMMITTEE CM ");
		    hql.append(" on CM.COMMITTEE_IDENTITY_NO = P.IDENTITY_NO ");
		    hql.append(" inner join TRADER TR ");
		    hql.append(" on TR.PERSON_ID = P.PERSON_ID ");
		    hql.append(" inner join SUSPENSION_MAP_TRADER SMT ");
		    hql.append(" on SMT.LICENSE_NO = TR.LICENSE_NO ");
		    hql.append(" inner join SUSPENSION_LICENSE SL ");
		    hql.append(" on SL.SUSPEND_ID = SMT.SUSPEND_ID ");
		    hql.append(" inner join MAS_PREFIX PRE ");
		    hql.append(" on PRE.PREFIX_ID = P.PREFIX_ID ");
		    hql.append(" left join MAS_PREFIX PREF ");
		    hql.append(" on PREF.PREFIX_ID = P.POSTFIX_ID ");
		    hql.append(" where  P.RECORD_STATUS NOT IN ('C','D') ");
		    hql.append(" and TR.RECORD_STATUS NOT IN ('C','D') ");
		    hql.append(" and CM.RECORD_STATUS NOT IN ('C','D') ");
		    hql.append(" and TR.TRADER_TYPE = 'B' ");
		    hql.append("  and SL.RECORD_STATUS = 'N' ");
		    hql.append("  and SL.SUSPEND_STATUS = 'S' ");
			    
		    if((null != dto.getIdentityNo()) && (!"".equals(dto.getIdentityNo())))
		    {
			    hql.append(" AND P.IDENTITY_NO = :PERSON_IDENTITY_NO ");
			    
			    parames.put("PERSON_IDENTITY_NO", dto.getIdentityNo());
		    }
		    
		    if((null != dto.getIdentityNo()) && (!"".equals(dto.getIdentityNo())))
		    {
			    hql.append(" AND CM.COMMITTEE_IDENTITY_NO = :PERSON_IDENTITY_NO ");
			    
			    parames.put("PERSON_IDENTITY_NO", dto.getIdentityNo());
		    }
		    
		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
		    sqlQuery.addScalar("PERSON_ID", Hibernate.LONG);//1
		    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);//2
		    sqlQuery.addScalar("PREFIX_NAME", Hibernate.STRING);//3
		    sqlQuery.addScalar("POSTFIX_NAME", Hibernate.STRING);//4
		    sqlQuery.addScalar("FIRST_NAME", Hibernate.STRING);//5
		    sqlQuery.addScalar("LAST_NAME", Hibernate.STRING);//6
		    sqlQuery.addScalar("COMMITTEE_NAME", Hibernate.STRING);//7
		    sqlQuery.addScalar("COMMITTEE_LASTNAME", Hibernate.STRING);//8
		    sqlQuery.addScalar("COMMITTEE_TYPE", Hibernate.STRING);//9
		    sqlQuery.addScalar("SUSPEND_PERIOD", Hibernate.STRING);//10
		    sqlQuery.addScalar("SUSPEND_FROM", Hibernate.DATE);//11
		    sqlQuery.addScalar("SUSPEND_TO", Hibernate.DATE);//12
		    sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING);//13
		    
		    sqlQuery.setProperties(parames);
		    List<Object[]>  result = sqlQuery.list();
		    
		    return result;
	}

	
	public List<Object[]> findCheckIdentityNoRegistrationRevokeSuspend(CommitteeDTO dto, User user)throws Exception
	{
		   Map<String,Object> parames = new HashMap<String,Object>();
		    
		    StringBuilder hql = new StringBuilder();
		    
		    
		    hql.append("SELECT ");
		    hql.append("	P.PERSON_ID AS PERSON_ID, ");
		    hql.append("	P.PERSON_TYPE AS PERSON_TYPE, ");
		    hql.append("	PREP.PREFIX_NAME AS PREFIX_NAME_P, ");
		    hql.append("	PREF.POSTFIX_NAME AS POSTFIX_NAME_P, ");
		    hql.append("	PREC.PREFIX_NAME AS PREFIX_NAME_CM, ");
		    hql.append("	PREM.POSTFIX_NAME AS POSTFIX_NAME_CM, ");
		    hql.append("	P.FIRST_NAME AS FIRST_NAME, ");
		    hql.append("	P.LAST_NAME AS LAST_NAME, ");
		    hql.append("	P.PERSON_STATUS AS PERSON_STATUS, ");
		    hql.append("	P.PUNISHMENT_DATE AS PUNISHMENT_DATE, ");
		    hql.append("	P.PUNISHMENT_EXPIRE_DATE AS PUNISHMENT_EXPIRE_DATE, ");
		    hql.append("	CM.COMMITTEE_STATUS AS COMMITTEE_STATUS, ");
		    hql.append("	CM.COMMITTEE_NAME AS COMMITTEE_NAME, ");
		    hql.append("	CM.COMMITTEE_LASTNAME AS COMMITTEE_LASTNAME, ");
		    hql.append("	CM.COMMITTEE_TYPE AS COMMITTEE_TYPE, ");
		    hql.append("	CM.PUNISHMENT_DATE AS PUNISHMENT_DATE_CM, ");
		    hql.append("	CM.PUNISHMENT_EXPIRE_DATE AS PUNISHMENT_EXPIRE_DATE_CM, ");
		    hql.append("	TR.TRADER_NAME AS TRADER_NAME ");
		    hql.append("	 ");
		    hql.append("FROM ");
		    hql.append("	COMMITTEE CM ");
		    hql.append("	INNER JOIN PERSON P ON P.PERSON_ID = CM.PERSON_ID ");
		    hql.append("	INNER JOIN TRADER TR ON P.PERSON_ID = TR.PERSON_ID ");
		    hql.append("		INNER JOIN ");
		    hql.append("		MAS_PREFIX PREP ");
		    hql.append("		ON ");
		    hql.append("		PREP.PREFIX_ID = P.PREFIX_ID ");
		    hql.append("		LEFT JOIN ");
		    hql.append("		MAS_POSFIX PREF ");
		    hql.append("		ON ");
		    hql.append("		PREF.POSTFIX_ID = P.POSTFIX_ID ");
		    hql.append("		INNER JOIN ");
		    hql.append("		MAS_PREFIX PREC ");
		    hql.append("		ON ");
		    hql.append("		PREC.PREFIX_ID = CM.PREFIX_ID ");
		    hql.append("		LEFT JOIN ");
		    hql.append("		MAS_POSFIX PREM ");
		    hql.append("		ON ");
		    hql.append("		PREM.POSTFIX_ID = CM.POSTFIX_ID ");
		    hql.append("WHERE ");
		    hql.append("	CM.COMMITTEE_STATUS in ('R','S') ");
		    hql.append("	AND TR.TRADER_BRANCH_NO IS NULL ");
		    
		   // if(!"".equals(dto.getTraderId()) && dto.getTraderId() > 0)
		    //{
		    //	hql.append(" AND TR.TRADER_ID = :TRADER_ID_TRADER ");
			//    parames.put("TRADER_ID_TRADER", dto.getTraderId());
		    //}
		    
		    if((null != dto.getCommitteePersonType()) && (!"".equals(dto.getCommitteePersonType())))
		    {
		    	hql.append(" AND CM.COMMITTEE_PERSON_TYPE = :COMMITTEE_PERSON_TYPE ");
			    parames.put("COMMITTEE_PERSON_TYPE", dto.getCommitteePersonType());
		    }
			    
		    if((null != dto.getCommitteeIdentityNo()) && (!"".equals(dto.getCommitteeIdentityNo())))
		    {
			    hql.append(" AND CM.COMMITTEE_IDENTITY_NO = :COMMITTEE_IDENTITY_NO ");
			    
			    parames.put("COMMITTEE_IDENTITY_NO", dto.getCommitteeIdentityNo());
		    }
		    
		    hql.append(" ORDER BY CM.COMMITTEE_ID ASC ");
		    
		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
		    sqlQuery.addScalar("PERSON_ID", Hibernate.LONG);//1
		    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);//2
		    sqlQuery.addScalar("PREFIX_NAME_P", Hibernate.STRING);//3
		    sqlQuery.addScalar("POSTFIX_NAME_P", Hibernate.STRING);//4
		    sqlQuery.addScalar("POSTFIX_NAME_CM", Hibernate.STRING);//4
		    sqlQuery.addScalar("POSTFIX_NAME_CM", Hibernate.STRING);//4
		    sqlQuery.addScalar("FIRST_NAME", Hibernate.STRING);//5
		    sqlQuery.addScalar("LAST_NAME", Hibernate.STRING);//6
		    sqlQuery.addScalar("PERSON_STATUS", Hibernate.STRING);//6
		    sqlQuery.addScalar("PUNISHMENT_DATE", Hibernate.DATE);//6
		    sqlQuery.addScalar("PUNISHMENT_EXPIRE_DATE", Hibernate.DATE);//6
		    sqlQuery.addScalar("COMMITTEE_STATUS", Hibernate.STRING);//6
		    sqlQuery.addScalar("COMMITTEE_NAME", Hibernate.STRING);//7
		    sqlQuery.addScalar("COMMITTEE_LASTNAME", Hibernate.STRING);//8
		    sqlQuery.addScalar("COMMITTEE_TYPE", Hibernate.STRING);//9
		    sqlQuery.addScalar("PUNISHMENT_DATE_CM", Hibernate.DATE);//10
		    sqlQuery.addScalar("PUNISHMENT_EXPIRE_DATE_CM", Hibernate.DATE);//11
		    sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING);//13
		    
		    sqlQuery.setProperties(parames);
		    List<Object[]>  result = sqlQuery.list();
		    
		    return result;
	}
	
	public List<Object[]> findCheckIdentityNoRegistrationPCMRevokeSuspend(RegistrationDTO dto, User user)throws Exception
	{
		   Map<String,Object> parames = new HashMap<String,Object>();
		    
		    StringBuilder hql = new StringBuilder();
		    
		    
		    hql.append("SELECT ");
		    hql.append("	P.PERSON_ID AS PERSON_ID, ");
		    hql.append("	P.PERSON_TYPE AS PERSON_TYPE, ");
		    hql.append("	PREP.PREFIX_NAME AS PREFIX_NAME_P, ");
		    hql.append("	PREF.POSTFIX_NAME AS POSTFIX_NAME_P, ");
		    hql.append("	PREC.PREFIX_NAME AS PREFIX_NAME_CM, ");
		    hql.append("	PREM.POSTFIX_NAME AS POSTFIX_NAME_CM, ");
		    hql.append("	P.FIRST_NAME AS FIRST_NAME, ");
		    hql.append("	P.LAST_NAME AS LAST_NAME, ");
		    hql.append("	P.PERSON_STATUS AS PERSON_STATUS, ");
		    hql.append("	P.PUNISHMENT_DATE AS PUNISHMENT_DATE, ");
		    hql.append("	P.PUNISHMENT_EXPIRE_DATE AS PUNISHMENT_EXPIRE_DATE, ");
		    hql.append("	CM.COMMITTEE_STATUS AS COMMITTEE_STATUS, ");
		    hql.append("	CM.COMMITTEE_NAME AS COMMITTEE_NAME, ");
		    hql.append("	CM.COMMITTEE_LASTNAME AS COMMITTEE_LASTNAME, ");
		    hql.append("	CM.COMMITTEE_TYPE AS COMMITTEE_TYPE, ");
		    hql.append("	CM.PUNISHMENT_DATE AS PUNISHMENT_DATE_CM, ");
		    hql.append("	CM.PUNISHMENT_EXPIRE_DATE AS PUNISHMENT_EXPIRE_DATE_CM, ");
		    hql.append("	TR.TRADER_NAME AS TRADER_NAME ");
		    hql.append("FROM ");
		    hql.append("	COMMITTEE CM ");
		    hql.append("	INNER JOIN PERSON P ON P.PERSON_ID = CM.PERSON_ID ");
		    hql.append("	INNER JOIN TRADER TR ON P.PERSON_ID = TR.PERSON_ID ");
		    hql.append("		INNER JOIN ");
		    hql.append("		MAS_PREFIX PREP ");
		    hql.append("		ON ");
		    hql.append("		PREP.PREFIX_ID = P.PREFIX_ID ");
		    hql.append("		LEFT JOIN ");
		    hql.append("		MAS_POSFIX PREF ");
		    hql.append("		ON ");
		    hql.append("		PREF.POSTFIX_ID = P.POSTFIX_ID ");
		    hql.append("		INNER JOIN ");
		    hql.append("		MAS_PREFIX PREC ");
		    hql.append("		ON ");
		    hql.append("		PREC.PREFIX_ID = CM.PREFIX_ID ");
		    hql.append("		LEFT JOIN ");
		    hql.append("		MAS_POSFIX PREM ");
		    hql.append("		ON ");
		    hql.append("		PREM.POSTFIX_ID = CM.POSTFIX_ID ");
		    hql.append("WHERE ");
		    hql.append("	CM.COMMITTEE_STATUS in ('R','S') ");
		    
		    if((null != dto.getCommitteePersonType()) && (!"".equals(dto.getCommitteePersonType())))
		    {
		    	hql.append(" AND CM.COMMITTEE_PERSON_TYPE = :COMMITTEE_PERSON_TYPE ");
			    parames.put("COMMITTEE_PERSON_TYPE", dto.getCommitteePersonType());
		    }
			    
		    if((null != dto.getIdentityNo()) && (!"".equals(dto.getIdentityNo())))
		    {
			    hql.append(" AND CM.COMMITTEE_IDENTITY_NO = :COMMITTEE_IDENTITY_NO ");
			    
			    parames.put("COMMITTEE_IDENTITY_NO", dto.getIdentityNo());
		    }
		    
		    hql.append(" ORDER BY CM.COMMITTEE_ID ASC ");
		    
		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
		    sqlQuery.addScalar("PERSON_ID", Hibernate.LONG);//1
		    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);//2
		    sqlQuery.addScalar("PREFIX_NAME_P", Hibernate.STRING);//3
		    sqlQuery.addScalar("POSTFIX_NAME_P", Hibernate.STRING);//4
		    sqlQuery.addScalar("POSTFIX_NAME_CM", Hibernate.STRING);//4
		    sqlQuery.addScalar("POSTFIX_NAME_CM", Hibernate.STRING);//4
		    sqlQuery.addScalar("FIRST_NAME", Hibernate.STRING);//5
		    sqlQuery.addScalar("LAST_NAME", Hibernate.STRING);//6
		    sqlQuery.addScalar("PERSON_STATUS", Hibernate.STRING);//6
		    sqlQuery.addScalar("PUNISHMENT_DATE", Hibernate.DATE);//6
		    sqlQuery.addScalar("PUNISHMENT_EXPIRE_DATE", Hibernate.DATE);//6
		    sqlQuery.addScalar("COMMITTEE_STATUS", Hibernate.STRING);//6
		    sqlQuery.addScalar("COMMITTEE_NAME", Hibernate.STRING);//7
		    sqlQuery.addScalar("COMMITTEE_LASTNAME", Hibernate.STRING);//8
		    sqlQuery.addScalar("COMMITTEE_TYPE", Hibernate.STRING);//9
		    sqlQuery.addScalar("PUNISHMENT_DATE_CM", Hibernate.DATE);//10
		    sqlQuery.addScalar("PUNISHMENT_EXPIRE_DATE_CM", Hibernate.DATE);//11
		    sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING);//13
		    
		    sqlQuery.setProperties(parames);
		    List<Object[]>  result = sqlQuery.list();
		    
		    return result;
	}
	
	public List<Object[]> findCheckIdentityNoRegistrationPersonRevokeSuspend(CommitteeDTO dto, User user)throws Exception
	{
		   Map<String,Object> parames = new HashMap<String,Object>();
		    
		    StringBuilder hql = new StringBuilder();
		    
		    
		    hql.append("SELECT ");
		    hql.append("	P.PERSON_ID AS PERSON_ID, ");
		    hql.append("	P.PERSON_TYPE AS PERSON_TYPE, ");
		    hql.append("	PREP.PREFIX_NAME AS PREFIX_NAME_P, ");
		    hql.append("	PREF.POSTFIX_NAME AS POSTFIX_NAME_P, ");
		    hql.append("	PREC.PREFIX_NAME AS PREFIX_NAME_CM, ");
		    hql.append("	PREM.POSTFIX_NAME AS POSTFIX_NAME_CM, ");
		    hql.append("	P.FIRST_NAME AS FIRST_NAME, ");
		    hql.append("	P.LAST_NAME AS LAST_NAME, ");
		    hql.append("	P.PERSON_STATUS AS PERSON_STATUS, ");
		    hql.append("	P.PUNISHMENT_DATE AS PUNISHMENT_DATE, ");
		    hql.append("	P.PUNISHMENT_EXPIRE_DATE AS PUNISHMENT_EXPIRE_DATE, ");
		    hql.append("	CM.COMMITTEE_STATUS AS COMMITTEE_STATUS, ");
		    hql.append("	CM.COMMITTEE_NAME AS COMMITTEE_NAME, ");
		    hql.append("	CM.COMMITTEE_LASTNAME AS COMMITTEE_LASTNAME, ");
		    hql.append("	CM.COMMITTEE_TYPE AS COMMITTEE_TYPE, ");
		    hql.append("	CM.PUNISHMENT_DATE AS PUNISHMENT_DATE_CM, ");
		    hql.append("	CM.PUNISHMENT_EXPIRE_DATE AS PUNISHMENT_EXPIRE_DATE_CM, ");
		    hql.append("	TR.TRADER_NAME AS TRADER_NAME ");
		    hql.append("	 ");
		    hql.append("FROM ");
		    hql.append("	PERSON P ");
		    hql.append("	LEFT JOIN COMMITTEE CM ON P.IDENTITY_NO = CM.COMMITTEE_IDENTITY_NO and P.PERSON_ID = CM.PERSON_ID ");
		    hql.append("	INNER JOIN TRADER TR ON P.PERSON_ID = TR.PERSON_ID ");
		    hql.append("		INNER JOIN ");
		    hql.append("		MAS_PREFIX PREP ");
		    hql.append("		ON ");
		    hql.append("		PREP.PREFIX_ID = P.PREFIX_ID ");
		    hql.append("		LEFT JOIN ");
		    hql.append("		MAS_POSFIX PREF ");
		    hql.append("		ON ");
		    hql.append("		PREF.POSTFIX_ID = P.POSTFIX_ID ");
		    hql.append("		LEFT JOIN ");
		    hql.append("		MAS_PREFIX PREC ");
		    hql.append("		ON ");
		    hql.append("		PREC.PREFIX_ID = CM.PREFIX_ID ");
		    hql.append("		LEFT JOIN ");
		    hql.append("		MAS_POSFIX PREM ");
		    hql.append("		ON ");
		    hql.append("		PREM.POSTFIX_ID = CM.POSTFIX_ID ");
		    hql.append("WHERE ");
		    hql.append("	P.PERSON_STATUS in ('R','S','N') ");
		    hql.append("	AND TR.TRADER_BRANCH_NO IS NULL ");
		    
		   // if(!"".equals(dto.getTraderId()) && dto.getTraderId() > 0)
		    //{
		    //	hql.append(" AND TR.TRADER_ID = :TRADER_ID_TRADER ");
			//    parames.put("TRADER_ID_TRADER", dto.getTraderId());
		    //}
		    
		    if((null != dto.getPersonType()) && (!"".equals(dto.getPersonType())))
		    {
		    	hql.append(" AND P.PERSON_TYPE = :PERSON_TYPE ");
			    parames.put("PERSON_TYPE", dto.getPersonType());
		    }
			    
		    if((null != dto.getPersonIdentityNo()) && (!"".equals(dto.getPersonIdentityNo())))
		    {
			    hql.append(" AND P.IDENTITY_NO = :PERSON_IDENTITY_NO ");
			    
			    parames.put("PERSON_IDENTITY_NO", dto.getPersonIdentityNo());
		    }
		    
		    hql.append(" ORDER BY P.PUNISHMENT_DATE ASC ");
		    
		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
		    sqlQuery.addScalar("PERSON_ID", Hibernate.LONG);//0
		    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);//1
		    sqlQuery.addScalar("PREFIX_NAME_P", Hibernate.STRING);//2
		    sqlQuery.addScalar("POSTFIX_NAME_P", Hibernate.STRING);//3
		    sqlQuery.addScalar("PREFIX_NAME_CM", Hibernate.STRING);//4
		    sqlQuery.addScalar("POSTFIX_NAME_CM", Hibernate.STRING);//5
		    sqlQuery.addScalar("FIRST_NAME", Hibernate.STRING);//6
		    sqlQuery.addScalar("LAST_NAME", Hibernate.STRING);//7
		    sqlQuery.addScalar("PERSON_STATUS", Hibernate.STRING);//8
		    sqlQuery.addScalar("PUNISHMENT_DATE", Hibernate.DATE);//9
		    sqlQuery.addScalar("PUNISHMENT_EXPIRE_DATE", Hibernate.DATE);//10
		    sqlQuery.addScalar("COMMITTEE_STATUS", Hibernate.STRING);//11
		    sqlQuery.addScalar("COMMITTEE_NAME", Hibernate.STRING);//12
		    sqlQuery.addScalar("COMMITTEE_LASTNAME", Hibernate.STRING);//13
		    sqlQuery.addScalar("COMMITTEE_TYPE", Hibernate.STRING);//14
		    sqlQuery.addScalar("PUNISHMENT_DATE_CM", Hibernate.DATE);//15
		    sqlQuery.addScalar("PUNISHMENT_EXPIRE_DATE_CM", Hibernate.DATE);//16
		    sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING);//17

		    
		    sqlQuery.setProperties(parames);
		    List<Object[]>  result = sqlQuery.list();
		    
		    return result;
	}
	
	public List<Registration> findRegistration(long traderId)
	{
		ArrayList params = new ArrayList();
		
		StringBuilder hql = new StringBuilder();
		hql.append(" from  Registration as reg ");
		hql.append(" where reg.trader.traderId = ? ");

		params.add(traderId);
	
		
		List<Registration> list = (List<Registration>) this.getHibernateTemplate().find(hql.toString(), params.toArray());

		return list;
	}
	
	//Oat Add 02/02/58
	public List<Object[]> findCheckIdentityNoTourleader(RegistrationDTO dto, User user)throws Exception
	{
		   Map<String,Object> parames = new HashMap<String,Object>();
		    
		    StringBuilder hql = new StringBuilder();
		    
		    hql.append("SELECT ");
		    hql.append(" P.PERSON_ID AS PERSON_ID, ");
		    hql.append(" P.IDENTITY_NO AS IDENTITY_NO ");
		    hql.append(" FROM PERSON P INNER JOIN TRADER TR ON P.PERSON_ID = TR.PERSON_ID ");
		    hql.append(" WHERE TR.TRADER_TYPE = :TRADER_TRADER_TYPE ");
		    parames.put("TRADER_TRADER_TYPE", TraderType.LEADER.getStatus());
			    
		    if((null != dto.getIdentityNo()) && (!"".equals(dto.getIdentityNo())))
		    {
			    hql.append(" AND P.IDENTITY_NO = :PERSON_IDENTITY_NO ");
			    
			    parames.put("PERSON_IDENTITY_NO", dto.getIdentityNo());
		    }
		    
		    hql.append(" AND P.RECORD_STATUS = :PERSON_RECORD_STATUS  ");
		    parames.put("PERSON_RECORD_STATUS", RecordStatus.NORMAL.getStatus());
		    
		    hql.append(" AND TR.RECORD_STATUS = :TRADER_RECORD_STATUS  ");
		    parames.put("TRADER_RECORD_STATUS", RecordStatus.NORMAL.getStatus());
		    
		    System.out.println("hql = "+hql.toString());
		    
		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
		    sqlQuery.addScalar("PERSON_ID", Hibernate.LONG);//1
		    sqlQuery.addScalar("IDENTITY_NO", Hibernate.STRING);//2
		    
		    sqlQuery.setProperties(parames);
		    List<Object[]>  result = sqlQuery.list();
		    
		    return result;
	}
	//End Oat Add
}
