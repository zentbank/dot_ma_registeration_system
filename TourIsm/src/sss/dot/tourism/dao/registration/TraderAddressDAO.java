package sss.dot.tourism.dao.registration;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.TraderAddress;

@Repository("traderAddressDAO")
public class TraderAddressDAO extends BaseDAO{
 /**
	 * 
	 */
	private static final long serialVersionUID = -100289054974147759L;

public TraderAddressDAO()
 {
	 this.domainObj = TraderAddress.class;
 }

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<TraderAddress> findAllByTrader(long traderId,String traderAddressType, String recordStatus)
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  TraderAddress as dep ");
		
		hql.append(" where dep.trader.traderId = ?  ");
		params.add(traderId);
		
//		if((null != recordStatus) && (!recordStatus.isEmpty()))
//		{
//			hql.append(" and dep.recordStatus = ? ");
//			params.add(recordStatus);
//		}
//		if(traderAddressType != null)
		System.out.println("traderAddressType : "+ traderAddressType);
		if((null != traderAddressType) && (!traderAddressType.isEmpty()))
		{
			hql.append(" and dep.addressType = ? ");
			params.add(traderAddressType);
		}
		
	
		
		return (List<TraderAddress>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<TraderAddress> findAllByTraderPl(long traderId,String traderAddressType, String recordStatus)
	{
		String recordStatusIn = "('N','H')";
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  TraderAddress as dep ");
		hql.append(" where dep.recordStatus in ('N','H') ");
		//params.add(" "+recordStatusIn);
		hql.append(" and dep.trader.traderId = ? ");
		params.add(traderId);
		
		if(traderAddressType != null)
		{
			hql.append(" and dep.addressType = ? ");
			params.add(traderAddressType);
		}
		
		System.out.println("#####traderId = "+traderId);
		System.out.println("#####hql = "+hql.toString());
		
		return (List<TraderAddress>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<TraderAddress> findAddressPrintLicense(long traderId,String traderAddressType, String recordStatus)
	{
		String recordStatusIn = "('N','H')";
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  TraderAddress as dep ");
		hql.append(" where dep.recordStatus in ('N','H') ");
		//params.add(" "+recordStatusIn);
		hql.append(" and dep.trader.traderId = ? ");
		params.add(traderId);
		
		if(traderAddressType != null)
		{
			hql.append(" and dep.addressType in ('B','S') ");
			params.add(traderAddressType);
		}
		
	
		
		return (List<TraderAddress>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<TraderAddress> findAllByTraderName(long traderId,String traderAddressType, String recordStatus)
	{
		System.out.println("OK");
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  TraderAddress as dep ");
		hql.append(" where dep.recordStatus in ('N','H','T') ");
		hql.append(" and dep.trader.traderId = ? ");
		params.add(traderId);
		
		if(traderAddressType != null)
		{
			hql.append(" and dep.addressType = ? ");
			params.add(traderAddressType);
		}
		
	
		
		return (List<TraderAddress>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<TraderAddress> findAllEmaiByTrader(long traderId, String recordStatus)
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  TraderAddress as dep ");
		hql.append(" where dep.recordStatus = ? ");
		hql.append(" and dep.trader.traderId = ? ");
		
		
		params.add(recordStatus);
		params.add(traderId);
		
//		if(traderAddressType != null)
//		{
//			hql.append(" and dep.addressType = ? ");
//			params.add(traderAddressType);
//		}
//		
	
		
		return (List<TraderAddress>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<String> findAllEmaiByTrader(long traderId)
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" select dep.email from  TraderAddress as dep ");
		hql.append(" where dep.recordStatus = 'N' ");
		hql.append(" and dep.trader.traderId = ? ");
		
		
		params.add(traderId);
		
		
		hql.append(" group by dep.email  ");
			
	
		
		return (List<String>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
}
