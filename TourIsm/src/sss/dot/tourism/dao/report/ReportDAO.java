package sss.dot.tourism.dao.report;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.TraderType;

@Repository("reportDAO")
public class ReportDAO extends BaseDAO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	  	
	public List<Object[]> findSumLanguageGuide(RegistrationDTO param) throws Exception
	{
		Map<String,Object> parames = new HashMap<String,Object>();
		
		StringBuilder hql = new StringBuilder();

		hql.append(" SELECT  ");
		hql.append(" C.COUNTRY_ID AS COUNTRY_ID ");
		hql.append(" ,COUNT(F.TRADER_ID) AS SUMGUIDE ");
		
		hql.append(" FROM COUNTRY C ");
		hql.append(" LEFT OUTER JOIN FOREIGN_LANGUAGE F ON C.COUNTRY_ID = F.FOREIGN_LANG  ");
		hql.append(" WHERE C.RECORD_STATUS = 'N' ");
		hql.append(" AND C.LANGUAGE <> '' ");
		hql.append(" AND F.RECORD_STATUS = 'N' ");
		
		if(param.getCountryId() != 0){
			hql.append(" AND C.COUNTRY_ID = :countryId ");
			parames.put("countryId", param.getCountryId());
		}
		
		hql.append(" GROUP BY C.COUNTRY_ID ,C.COUNTRY_NAME,C.LANGUAGE ");
		
		System.out.println("hql="+hql.toString());
		
		SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
	    
	    
		
	    sqlQuery.addScalar("COUNTRY_ID", Hibernate.BIG_DECIMAL); 
	    sqlQuery.addScalar("SUMGUIDE", Hibernate.BIG_DECIMAL); 
	    
	    
	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
	    
	    return result;
	}
	
	public List<Object[]> findSumProvinceBusiness(RegistrationDTO param) throws Exception{
		
		Map<String,Object> parames = new HashMap<String,Object>();
		StringBuilder hql = new StringBuilder();
		
		hql.append(" SELECT ");
		hql.append(" P.PROVINCE_NAME AS PROVINCE_NAME ");
		hql.append(" ,COUNT(T.LICENSE_NO) AS SUMBUSINESS ");
		hql.append(" FROM TRADER T ");
		hql.append(" INNER JOIN TRADER_ADDRESS A ON A.TRADER_ID = T.TRADER_ID ");
		hql.append(" INNER JOIN MAS_PROVINCE P ON A.PROVINCE_ID = P.PROVINCE_ID ");
		hql.append(" WHERE T.TRADER_TYPE = 'B' ");
		hql.append(" AND T.LICENSE_STATUS = 'N' ");
		hql.append(" AND T.RECORD_STATUS = 'N' ");
		hql.append(" AND A.RECORD_STATUS = 'N' ");
		hql.append(" AND A.ADDRESS_TYPE = 'O' ");
		hql.append(" AND P.RECORD_STATUS = 'N' ");
		hql.append(" AND T.BRANCH_TYPE IS NULL ");
		
		if(param.getProvinceId() !=0){
			hql.append(" AND P.PROVINCE_ID = :provinceId ");
			parames.put("provinceId", param.getProvinceId());
		}
		
		hql.append(" GROUP BY P.PROVINCE_NAME ");
		hql.append(" ORDER BY P.PROVINCE_NAME ");

		System.out.println("hql="+hql.toString());
		
		SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
	    
	    
		
	    sqlQuery.addScalar("PROVINCE_NAME", Hibernate.STRING); 
	    sqlQuery.addScalar("SUMBUSINESS", Hibernate.BIG_DECIMAL); 
	    
	    
	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
		
		return result;
		
	}
	
	public List<Object[]> findSumStaticTourLeader(RegistrationDTO dto) throws Exception{
		Map<String,Object> parames = new HashMap<String,Object>();
		StringBuilder hql = new StringBuilder();
		
		hql.append(" SELECT ");
		hql.append(" T.TRADER_TYPE AS TRADER_TYPE ");
		hql.append(" ,COUNT(T.LICENSE_NO) AS SUMSTATICTOURLEADER");
		hql.append(" FROM REGISTRATION R ");
		hql.append(" INNER JOIN TRADER T ON T.TRADER_ID = R.TRADER_ID ");
		hql.append(" WHERE T.TRADER_TYPE = 'L' ");
		hql.append(" AND T.LICENSE_STATUS = 'N' ");
		hql.append(" AND T.RECORD_STATUS = 'N' ");
		hql.append(" AND R.REGISTRATION_TYPE = 'N' ");
		hql.append(" AND R.RECORD_STATUS = 'N' ");
		if((null != dto.getDateFrom()) && (!dto.getDateTo().equals("")))
	    {
	    	if(!(dto.getDateTo().equals("0")))
	    	{
				hql.append(" AND R.REGISTRATION_DATE >=  convert(datetime, :RE_START_DATE ,20) ");
				hql.append(" AND R.REGISTRATION_DATE <=  convert(datetime, :RE_END_DATE ,20)  ");
				
				String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getDateFrom(), "00:00");
				String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getDateTo(), "23:00");

				parames.put("RE_START_DATE", sendDtmFrom);
				parames.put("RE_END_DATE", sendDtmTo);
	    	}else
	    	{
	    		hql.append("  and  R.REGISTRATION_DATE >=  convert(datetime, :RE_START_DATE ,20) ");
				//hql.append(" and R.REGISTRATION_DATE <=  convert(datetime, :RE_END_DATE ,20)  ");
				
				String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getDateFrom(), "00:00");
				//String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getDateTo(), "23:00");
					
				parames.put("RE_START_DATE", sendDtmFrom);
				//parames.put("RE_END_DATE", sendDtmTo);
	    	}
	    }
		
		hql.append(" GROUP BY T.TRADER_TYPE ");
		
		SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
		
		sqlQuery.addScalar("TRADER_TYPE", Hibernate.STRING);
		sqlQuery.addScalar("SUMSTATICTOURLEADER", Hibernate.BIG_DECIMAL); 
	    
	    
	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
		
		return result;

	}
	
	public List<Object[]> findSumBusinessRegistrationType(RegistrationDTO dto) throws Exception{
		Map<String,Object> parames = new HashMap<String,Object>();
		StringBuilder hql = new StringBuilder();
		
		hql.append(" SELECT ");
		hql.append(" COUNT(T.LICENSE_NO) AS SUMCHANGE   ");
		hql.append(" ,R.REGISTRATION_TYPE AS REGISTRATION_TYPE ");
		hql.append(" FROM REGISTRATION R   ");
		hql.append(" INNER JOIN TRADER T ON T.TRADER_ID = R.TRADER_ID ");
		hql.append(" WHERE T.TRADER_TYPE = 'B' ");
		hql.append(" AND T.LICENSE_STATUS = 'N' ");
		hql.append(" AND T.RECORD_STATUS = 'N' ");
		hql.append(" AND R.RECORD_STATUS = 'N' ");
		hql.append(" AND R.REGISTRATION_TYPE IN ('C','B') ");
		if((null != dto.getDateFrom()) && (!dto.getDateTo().equals("")))
	    {
	    	if(!(dto.getDateTo().equals("0")))
	    	{
				hql.append(" AND R.REGISTRATION_DATE >=  convert(datetime, :RE_START_DATE ,20) ");
				hql.append(" AND R.REGISTRATION_DATE <=  convert(datetime, :RE_END_DATE ,20)  ");
				
				String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getDateFrom(), "00:00");
				String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getDateTo(), "23:00");

				parames.put("RE_START_DATE", sendDtmFrom);
				parames.put("RE_END_DATE", sendDtmTo);
	    	}else
	    	{
	    		hql.append("  and  R.REGISTRATION_DATE >=  convert(datetime, :RE_START_DATE ,20) ");
				//hql.append(" and R.REGISTRATION_DATE <=  convert(datetime, :RE_END_DATE ,20)  ");
				
				String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getDateFrom(), "00:00");
				//String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getDateTo(), "23:00");
					
				parames.put("RE_START_DATE", sendDtmFrom);
				//parames.put("RE_END_DATE", sendDtmTo);
	    	}
	    }
		hql.append(" GROUP BY R.REGISTRATION_TYPE ");
		
		SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
		
		sqlQuery.addScalar("REGISTRATION_TYPE", Hibernate.STRING);
		sqlQuery.addScalar("SUMCHANGE", Hibernate.BIG_DECIMAL); 
	    
	    
	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
		
		return result;
	}
	
	public List<Object[]> findNewBusinessLicenseDetail(RegistrationDTO dto) throws Exception
	{
		 	Map<String,Object> parames = new HashMap<String,Object>();
		    
		    StringBuilder hql = new StringBuilder();
		    
			   hql = this.findNewBusinessLicense(dto);
		   
//		   if(TraderType.GUIDE.getStatus().equals(dto.getTraderType()))
//		   {
//			   hql = this.findGuideLicense(dto);
//		   }
//		   if(TraderType.LEADER.getStatus().equals(dto.getTraderType()))
//		   {
//			   //hql = this.findGuideLicense(dto);
//			   //hql = this.find
//		   }
		   
		   if(dto.getOrgId() > 0)
			{
		    	parames.put("orgId", dto.getOrgId());
			}
			
			if(dto.getDateFrom() !=null && !dto.getDateFrom().equals("")){
				String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getDateFrom(), "00:00");

				parames.put("RE_START_DATE", sendDtmFrom);
			}
			
			if(dto.getDateTo() !=null &&!dto.getDateTo().equals("")){
				String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getDateTo(), "23:00");
				
				parames.put("RE_END_DATE", sendDtmTo);
			}
//			if(dto.getProvinceId() > 0)
//			{
//				parames.put("provinceId", dto.getProvinceId());
//			}

		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
		    
		    
			
		    sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING); 
		    sqlQuery.addScalar("TRADER_CATEGORY", Hibernate.STRING); 
		    sqlQuery.addScalar("EFFECTIVE_DATE", Hibernate.DATE); 
		    sqlQuery.addScalar("EXPIRE_DATE", Hibernate.DATE); 
		    sqlQuery.addScalar("ADDRESS", Hibernate.STRING); 
		    sqlQuery.addScalar("MOBILE_NO", Hibernate.STRING); 
		    sqlQuery.addScalar("TELEPHONE", Hibernate.STRING); 
		    
		    sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING); 
		    sqlQuery.addScalar("TRADER_NAME_EN", Hibernate.STRING); 
		    
		    sqlQuery.addScalar("EMAIL", Hibernate.STRING); 
		    
		    sqlQuery.addScalar("BUILDING_NAME", Hibernate.STRING); 
		    sqlQuery.addScalar("FLOOR", Hibernate.STRING); 
		    sqlQuery.addScalar("MOO", Hibernate.STRING); 
		    sqlQuery.addScalar("SOI", Hibernate.STRING); 
		    sqlQuery.addScalar("ROAD_NAME", Hibernate.STRING); 
		    sqlQuery.addScalar("TAMBOL_NAME", Hibernate.STRING); 
		    sqlQuery.addScalar("AMPHUR_NAME", Hibernate.STRING); 
		    sqlQuery.addScalar("PROVINCE_NAME", Hibernate.STRING);
		    sqlQuery.addScalar("POST_CODE", Hibernate.STRING);
		    
		    sqlQuery.setProperties(parames);
		    List<Object[]>  result = sqlQuery.list();
	  
		    return result;
	}
	
	public StringBuilder findNewBusinessLicense(RegistrationDTO param) throws Exception
	{
		StringBuilder hql = new StringBuilder();
		
		hql.append(" SELECT  ");
		hql.append(" TR.LICENSE_NO AS LICENSE_NO ,TR.TRADER_NAME AS TRADER_NAME,TR.TRADER_NAME_EN AS TRADER_NAME_EN ");
		hql.append(" , CASE TR.TRADER_CATEGORY	 ");
		hql.append(" 	WHEN  '100' THEN 'OUTBOUND(ทั่วไป)' ");
		hql.append(" 	WHEN  '200' THEN 'INBOUND(นำเที่ยวจากต่างประเทศ)' ");
		hql.append(" 	WHEN  '300' THEN 'ในประเทศ' ");
		hql.append(" 	WHEN  '400' THEN 'เฉพาะพื้นที่' ");
		hql.append("  END AS TRADER_CATEGORY ");
		hql.append(" ,TR.EFFECTIVE_DATE AS EFFECTIVE_DATE ");
		hql.append(" ,TR.EXPIRE_DATE AS EXPIRE_DATE ");
		
		hql.append(" ,TA.ADDRESS_NO AS ADDRESS");
		hql.append(" ,TA.BUILDING_NAME AS BUILDING_NAME ");
		hql.append(" ,TA.FLOOR AS FLOOR ");
		hql.append(" ,TA.MOO AS MOO ");
		hql.append(" ,TA.SOI AS SOI");
		hql.append(" ,TA.ROAD_NAME AS ROAD_NAME ");
		hql.append(" ,ML.TAMBOL_NAME AS TAMBOL_NAME");
		hql.append(" ,AM.AMPHUR_NAME AS AMPHUR_NAME");
		hql.append(" ,MP.PROVINCE_NAME AS PROVINCE_NAME");
		hql.append(" ,TA.POST_CODE AS POST_CODE");
		
		//hql.append(" ,'เลขที่ ' + ISNULL ( TA.ADDRESS_NO , '' ) +  ISNULL ( ' อาคาร ' + TA.BUILDING_NAME , '' )+ ISNULL ( ' ชั้น ' + TA.FLOOR , '' ) + ISNULL (' หมู่ที่ ' + TA.MOO , '' ) ");
		//hql.append(" + ISNULL (' ซอย ' + TA.SOI , '' )+ ISNULL (' ถนน ' + TA.ROAD_NAME , '' ) + ISNULL ( ' ตำบล/แขวง ' + ML.TAMBOL_NAME , '' ) ");
		//hql.append(" + ISNULL (' อำเภอ/เขต ' + AM.AMPHUR_NAME, '' ) + ISNULL (' จังหวัด ' + MP.PROVINCE_NAME, '' )+ ISNULL (' รหัสไปรษณีย์ ' + TA.POST_CODE, '' ) AS ADDRESS ");
		hql.append(" ,TA.MOBILE_NO AS MOBILE_NO, TA.TELEPHONE AS TELEPHONE, TA.EMAIL AS EMAIL ");
		hql.append(" FROM TRADER TR ");
		hql.append(" INNER JOIN REGISTRATION RE ");
		hql.append(" ON(RE.TRADER_ID = TR.TRADER_ID) ");
		hql.append(" LEFT JOIN TRADER_ADDRESS TA ");
		hql.append(" ON(TA.TRADER_ID = TR.TRADER_ID)  ");
		hql.append(" LEFT JOIN MAS_AMPHUR AM ");
		hql.append(" ON(AM.AMPHUR_ID = TA.AMPHUR_ID) ");
		hql.append(" LEFT JOIN MAS_PROVINCE MP ");
		hql.append(" ON(TA.PROVINCE_ID = MP.PROVINCE_ID) ");
		hql.append(" LEFT JOIN MAS_TAMBOL ML ");
		hql.append(" ON(ML.TAMBOL_ID = TA.TAMBOL_ID) ");
		hql.append(" WHERE TR.BRANCH_TYPE IS NULL ");
		hql.append(" AND TR.RECORD_STATUS = 'N' ");
		hql.append(" AND TR.TRADER_TYPE = 'B' ");
	
//Oat Edit 19/05/58		
//		if(param.getOrgId() > 0)
//		{
//			hql.append(" AND TR.ORG_ID = :orgId ");
//		}
		if(param.getOrgId() > 0)
		{
			hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :orgId ) ");
		}
//Oat Edit 19/05/58
		
		hql.append(" AND TR.LICENSE_STATUS = 'N' ");
		
		if(param.getProvinceId() > 0)
		{
			hql.append(" AND TA.PROVINCE_ID = :provinceId ");
		}
		
		if((null != param.getDateFrom()) && (!param.getDateTo().equals("")))
	    {
	    	if(!(param.getDateTo().equals("0")))
	    	{
				hql.append(" AND RE.REGISTRATION_DATE >=  convert(datetime, :RE_START_DATE ,20) ");
				hql.append(" AND RE.REGISTRATION_DATE <=  convert(datetime, :RE_END_DATE ,20)  ");
				
	    	}else
	    	{
	    		hql.append("  and  RE.REGISTRATION_DATE >=  convert(datetime, :RE_START_DATE ,20) ");
	    	}
	    }
		
//Oat Edit 19/05/58
		hql.append(" AND TA.ADDRESS_TYPE = 'O' ");
//		hql.append(" AND TA.ADDRESS_TYPE = 'S' ");
		
//Oat Edit 01/07/58
		hql.append(" ORDER BY TA.PROVINCE_ID, TA.AMPHUR_ID, TA.TAMBOL_ID; ");
//		hql.append(" ORDER BY TR.LICENSE_NO; ");
		
		return hql;
	}
	
	public List<Object[]> findMasDeactypeBusinessLicenseDetail(RegistrationDTO dto) throws Exception
	{
		 	Map<String,Object> parames = new HashMap<String,Object>();
		    
		    StringBuilder hql = new StringBuilder();
		    DateFormat ft =  DateUtils.getProcessDateFormatThai();
			   hql = this.findMasDeactypeBusinessLicense(dto);
		   
//		   if(TraderType.GUIDE.getStatus().equals(dto.getTraderType()))
//		   {
//			   hql = this.findGuideLicense(dto);
//		   }
//		   if(TraderType.LEADER.getStatus().equals(dto.getTraderType()))
//		   {
//			   //hql = this.findGuideLicense(dto);
//			   //hql = this.find
//		   }
		   
		   if(dto.getOrgId() > 0)
			{
		    	parames.put("orgId", dto.getOrgId());
			}
			
			if(dto.getDateFrom() !=null && !dto.getDateFrom().equals("")){
				 parames.put("BOOK_DATE_FORM", ft.parse(dto.getDateFrom()));
			}
			
			if(dto.getDateTo() !=null &&!dto.getDateTo().equals("")){
				   parames.put("BOOK_DATE_TO", ft.parse(dto.getDateTo()));
			}else{
				parames.put("BOOK_DATE_TO", new Date());
			}


		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
		    
		    
		    sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING); 
		    sqlQuery.addScalar("TRADER_CATEGORY", Hibernate.STRING); 
		    sqlQuery.addScalar("EFFECTIVE_DATE", Hibernate.DATE); 
		    sqlQuery.addScalar("EXPIRE_DATE", Hibernate.DATE); 
		    sqlQuery.addScalar("ADDRESS", Hibernate.STRING); 
		    sqlQuery.addScalar("MOBILE_NO", Hibernate.STRING); 
		    sqlQuery.addScalar("TELEPHONE", Hibernate.STRING); 
		    
		    sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING); 
		    sqlQuery.addScalar("TRADER_NAME_EN", Hibernate.STRING); 
		    
		    sqlQuery.addScalar("EMAIL", Hibernate.STRING); 
		    sqlQuery.addScalar("DEACTIVATE_DATE", Hibernate.DATE);
		    
		    sqlQuery.addScalar("BUILDING_NAME", Hibernate.STRING); 
		    sqlQuery.addScalar("FLOOR", Hibernate.STRING); 
		    sqlQuery.addScalar("MOO", Hibernate.STRING); 
		    sqlQuery.addScalar("SOI", Hibernate.STRING); 
		    sqlQuery.addScalar("ROAD_NAME", Hibernate.STRING); 
		    sqlQuery.addScalar("TAMBOL_NAME", Hibernate.STRING); 
		    sqlQuery.addScalar("AMPHUR_NAME", Hibernate.STRING); 
		    sqlQuery.addScalar("PROVINCE_NAME", Hibernate.STRING);
		    sqlQuery.addScalar("POST_CODE", Hibernate.STRING);
		    sqlQuery.setProperties(parames);
		    List<Object[]>  result = sqlQuery.list();
	  
		    return result;
	}
	
	
	public StringBuilder findMasDeactypeBusinessLicense(RegistrationDTO param) throws Exception
	{
		DateFormat ft =  DateUtils.getProcessDateFormatThai();
		StringBuilder hql = new StringBuilder();
		
		hql.append(" SELECT  ");
		hql.append(" TR.LICENSE_NO AS LICENSE_NO ,TR.TRADER_NAME AS TRADER_NAME,TR.TRADER_NAME_EN AS TRADER_NAME_EN ");
		hql.append(" , CASE TR.TRADER_CATEGORY	 ");
		hql.append(" 	WHEN  '100' THEN 'OUTBOUND(ทั่วไป)' ");
		hql.append(" 	WHEN  '200' THEN 'INBOUND(นำเที่ยวจากต่างประเทศ)' ");
		hql.append(" 	WHEN  '300' THEN 'ในประเทศ' ");
		hql.append(" 	WHEN  '400' THEN 'เฉพาะพื้นที่' ");
		hql.append("  END AS TRADER_CATEGORY ");
		hql.append(" ,TR.EFFECTIVE_DATE AS EFFECTIVE_DATE ");
		hql.append(" ,TR.EXPIRE_DATE AS EXPIRE_DATE ");
		
		hql.append(" ,TA.ADDRESS_NO AS ADDRESS");
		hql.append(" ,TA.BUILDING_NAME AS BUILDING_NAME ");
		hql.append(" ,TA.FLOOR AS FLOOR ");
		hql.append(" ,TA.MOO AS MOO ");
		hql.append(" ,TA.SOI AS SOI");
		hql.append(" ,TA.ROAD_NAME AS ROAD_NAME ");
		hql.append(" ,ML.TAMBOL_NAME AS TAMBOL_NAME");
		hql.append(" ,AM.AMPHUR_NAME AS AMPHUR_NAME");
		hql.append(" ,MP.PROVINCE_NAME AS PROVINCE_NAME");
		hql.append(" ,TA.POST_CODE AS POST_CODE");
		
		//hql.append(" ,'เลขที่ ' + ISNULL ( TA.ADDRESS_NO , '' ) +  ISNULL ( ' อาคาร ' + TA.BUILDING_NAME , '' )+ ISNULL ( ' ชั้น ' + TA.FLOOR , '' ) + ISNULL (' หมู่ที่ ' + TA.MOO , '' ) ");
		//hql.append(" + ISNULL (' ซอย ' + TA.SOI , '' )+ ISNULL (' ถนน ' + TA.ROAD_NAME , '' ) + ISNULL ( ' ตำบล/แขวง ' + ML.TAMBOL_NAME , '' ) ");
		//hql.append(" + ISNULL (' อำเภอ/เขต ' + AM.AMPHUR_NAME, '' ) + ISNULL (' จังหวัด ' + MP.PROVINCE_NAME, '' )+ ISNULL (' รหัสไปรษณีย์ ' + TA.POST_CODE, '' ) AS ADDRESS ");
		hql.append(" ,TA.MOBILE_NO AS MOBILE_NO, TA.TELEPHONE AS TELEPHONE, TA.EMAIL AS EMAIL ");
		hql.append(" ,DL.DEACTIVATE_DATE AS DEACTIVATE_DATE ");
		hql.append(" FROM DEACTIVATE_LICENSE DL	 ");
		hql.append(" INNER JOIN TRADER TR ON(DL.TRADER_TYPE = TR.TRADER_TYPE  AND DL.LICENSE_NO = TR.LICENSE_NO) ");
		hql.append(" INNER JOIN GUARANTEE G ON(TR.TRADER_TYPE = G.TRADER_TYPE AND TR.LICENSE_NO = G.LICENSE_NO) ");
		hql.append(" INNER JOIN TRADER_ADDRESS TA ON (TA.TRADER_ID = TR.TRADER_ID) ");
		hql.append(" LEFT JOIN MAS_AMPHUR AM ");
		hql.append(" ON(AM.AMPHUR_ID = TA.AMPHUR_ID) ");
		hql.append(" LEFT JOIN MAS_PROVINCE MP ");
		hql.append(" ON(TA.PROVINCE_ID = MP.PROVINCE_ID) ");
		hql.append(" LEFT JOIN MAS_TAMBOL ML ");
		hql.append(" ON(ML.TAMBOL_ID = TA.TAMBOL_ID) ");
		hql.append(" WHERE TR.TRADER_TYPE   = 'B' ");
		hql.append(" AND TR.RECORD_STATUS   = 'N' ");
		hql.append(" AND TR.BRANCH_TYPE    IS NULL ");
		hql.append(" AND TA.ADDRESS_TYPE    = 'O' ");
		hql.append(" AND TA.RECORD_STATUS   = 'N' ");
		hql.append(" AND DL.APPROVE_STATUS IN ('A') ");
		hql.append(" AND DL.RECORD_STATUS   = 'N' ");
		hql.append(" AND G.GUARANTEE_STATUS = 'R' ");
		hql.append(" AND G.RECORD_STATUS    = 'N' ");
		hql.append(" AND TR.LICENSE_STATUS = 'C' ");
		 if(param.getDateFrom()!=null && !param.getDateFrom().equals(""))
		 {
		 	hql.append(" AND DL.BOOK_DATE BETWEEN :BOOK_DATE_FORM ");
			   
			    //BookDateTo
			    if(param.getApproveDateTo()!=null && !param.getApproveDateTo().equals(""))
			    {
			    	hql.append(" AND :BOOK_DATE_TO ");
				 
			    }
			    else
			    {
			    	hql.append(" AND :BOOK_DATE_TO ");
			    }
		 }
			
		if(param.getOrgId() > 0)
		{
			hql.append(" AND TA.PROVINCE_ID IN ( SELECT P.PROVINCE_ID FROM MAS_PROVINCE P WHERE P.ORG_ID = :orgId ) ");
		}
		
		hql.append(" GROUP BY TR.LICENSE_NO,TR.TRADER_NAME,TR.TRADER_NAME_EN,TR.TRADER_CATEGORY,TR.EFFECTIVE_DATE,TR.EXPIRE_DATE,TA.MOBILE_NO,TA.TELEPHONE,TA.EMAIL,DL.DEACTIVATE_DATE ");
		hql.append(" ,TA.ADDRESS_NO,TA.BUILDING_NAME,TA.FLOOR,TA.MOO,TA.SOI,TA.ROAD_NAME,ML.TAMBOL_NAME,AM.AMPHUR_NAME,MP.PROVINCE_NAME,TA.POST_CODE ");
		hql.append(" ORDER BY TR.LICENSE_NO,TR.TRADER_NAME ");	
		
		return hql;
	}
}
