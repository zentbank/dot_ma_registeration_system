package sss.dot.tourism.domain;

// Generated Dec 19, 2014 3:10:09 PM by Hibernate Tools 3.4.0.CR1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * AdmUserGroup generated by hbm2java
 */
@Entity
@Table(name = "ADM_USER_GROUP")
public class AdmUserGroup implements java.io.Serializable {

	private long userGroupId;
	private AdmUser admUser;
	private AdmGroup admGroup;

	public AdmUserGroup() {
	}

	public AdmUserGroup(long userGroupId) {
		this.userGroupId = userGroupId;
	}

	public AdmUserGroup(long userGroupId, AdmUser admUser, AdmGroup admGroup) {
		this.userGroupId = userGroupId;
		this.admUser = admUser;
		this.admGroup = admGroup;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "USER_GROUP_ID", unique = true, nullable = false)
	public long getUserGroupId() {
		return this.userGroupId;
	}

	public void setUserGroupId(long userGroupId) {
		this.userGroupId = userGroupId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID")
	public AdmUser getAdmUser() {
		return this.admUser;
	}

	public void setAdmUser(AdmUser admUser) {
		this.admUser = admUser;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "GROUP_ID")
	public AdmGroup getAdmGroup() {
		return this.admGroup;
	}

	public void setAdmGroup(AdmGroup admGroup) {
		this.admGroup = admGroup;
	}

}
