package sss.dot.tourism.domain;

// Generated Dec 19, 2014 3:10:09 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * DeactivateDetail generated by hbm2java
 */
@Entity
@Table(name = "DEACTIVATE_DETAIL")
public class DeactivateDetail implements java.io.Serializable {

	private long deactivateDtlId;
	private DeactivateLicense deactivateLicense;
	private MasDeactivateType masDeactivateType;
	private String recordStatus;
	private String createUser;
	private Date createDtm;
	private String lastUpdUser;
	private Date lastUpdDtm;
	private String deactivateDesc;

	public DeactivateDetail() {
	}

	public DeactivateDetail(long deactivateDtlId) {
		this.deactivateDtlId = deactivateDtlId;
	}

	public DeactivateDetail(long deactivateDtlId,
			DeactivateLicense deactivateLicense,
			MasDeactivateType masDeactivateType, String recordStatus,
			String createUser, Date createDtm, String lastUpdUser,
			Date lastUpdDtm, String deactivateDesc) {
		this.deactivateDtlId = deactivateDtlId;
		this.deactivateLicense = deactivateLicense;
		this.masDeactivateType = masDeactivateType;
		this.recordStatus = recordStatus;
		this.createUser = createUser;
		this.createDtm = createDtm;
		this.lastUpdUser = lastUpdUser;
		this.lastUpdDtm = lastUpdDtm;
		this.deactivateDesc = deactivateDesc;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DEACTIVATE_DTL_ID", unique = true, nullable = false)
	public long getDeactivateDtlId() {
		return this.deactivateDtlId;
	}

	public void setDeactivateDtlId(long deactivateDtlId) {
		this.deactivateDtlId = deactivateDtlId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DEACTIVE_ID")
	public DeactivateLicense getDeactivateLicense() {
		return this.deactivateLicense;
	}

	public void setDeactivateLicense(DeactivateLicense deactivateLicense) {
		this.deactivateLicense = deactivateLicense;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MAS_DEACTIVATE_TYPE_ID")
	public MasDeactivateType getMasDeactivateType() {
		return this.masDeactivateType;
	}

	public void setMasDeactivateType(MasDeactivateType masDeactivateType) {
		this.masDeactivateType = masDeactivateType;
	}

	@Column(name = "RECORD_STATUS")
	public String getRecordStatus() {
		return this.recordStatus;
	}

	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}

	@Column(name = "CREATE_USER")
	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATE_DTM", length = 23)
	public Date getCreateDtm() {
		return this.createDtm;
	}

	public void setCreateDtm(Date createDtm) {
		this.createDtm = createDtm;
	}

	@Column(name = "LAST_UPD_USER")
	public String getLastUpdUser() {
		return this.lastUpdUser;
	}

	public void setLastUpdUser(String lastUpdUser) {
		this.lastUpdUser = lastUpdUser;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPD_DTM", length = 23)
	public Date getLastUpdDtm() {
		return this.lastUpdDtm;
	}

	public void setLastUpdDtm(Date lastUpdDtm) {
		this.lastUpdDtm = lastUpdDtm;
	}

	@Column(name = "DEACTIVATE_DESC")
	public String getDeactivateDesc() {
		return this.deactivateDesc;
	}

	public void setDeactivateDesc(String deactivateDesc) {
		this.deactivateDesc = deactivateDesc;
	}

}
