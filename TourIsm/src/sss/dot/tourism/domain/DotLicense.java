package sss.dot.tourism.domain;


// Generated Apr 14, 2015 4:50:05 PM by Hibernate Tools 3.4.0.CR1

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * DotLicense generated by hbm2java
 */
@Entity
@Table(name = "DOT_LICENSE")
public class DotLicense implements java.io.Serializable {

	private long licenseId;
	private MasAmphur masAmphur;
	private MasProvince masProvince;
	private DotLicenseOwner dotLicenseOwner;
	private MasTambol masTambol;
	private String licenseNo;
	private String licenseGuideNo;
	private String licenseType;
	private String licenseCategory;
	private Date effectiveDate;
	private Date expireDate;
	private String licenseName;
	private String licenseNameEn;
	private String licenseStatus;
	private String pronunciationName;
	private String licenseServiceArea;
	private MasProvince licenseServiceProvince;
	private String licenseBuildingName;
	private String licenseBuildingNameEn;
	private String licenseRoomNo;
	private String licenseFloor;
	private String licenseVillageName;
	private String licenseVillageNameEn;
	private String licenseHomeNo;
	private String licenseMooNo;
	private String licenseSoi;
	private String licenseSoiEn;
	private String licenseRoadName;
	private String licenseRoadNameEn;
	private String licensePostCode;
	private String licenseEmail;
	private String licenseFax;
	private String licenseMobileNo;
	private String licenseTelephone;
	private String licenseWebsite;
	private String licenseMapLat;
	private String licenseMapLong;
	private Integer planTrips;
	private String recordStatus;
	private String createUser;
	private Date createDtm;
	private Date lastUpdDtm;
	private String lastUpdUser;
	
//	private List<CountryTour> countryTours;
//	private List<DotTrFeeLicense> dotTrFeeLicenses;
//	private List<DotSubLicense> dotSubLicenses;
//	private List<DotLicOwnerEduBackground> dotLicOwnerEduBackgrounds;
//	
//	private List<RegDocument> regDocuments;


//	private String licenseFullName;
//	private String licenesCategoryName;
//	private String paymentDate;
//	private String licenseStatusName;
	
	private MasBank masBankByBankGuaranteeBankId;
	private MasBank masBankByCashAccountBankId;
	
	private String cashAccountId;
	private String cashAccountName;
	private String cashAccountBankBranchName;
	private BigDecimal cashAccountMny;
	private String bankGuaranteeId;
	private String bankGuaranteeName;
	private BigDecimal bankGuaranteeMny;
	private String bankGuaranteeBankBranchName;
	private String governmentBondId;
	private Date governmentBondExpireDate;
	private BigDecimal governmentBondMny;
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "LICENSE_ID", unique = true, nullable = false)
	public long getLicenseId() {
		return this.licenseId;
	}

	public void setLicenseId(long licenseId) {
		this.licenseId = licenseId;
	}

	@ManyToOne(fetch = FetchType.EAGER , cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
	@JoinColumn(name = "LICENSE_AMPHUR_ID")
	public MasAmphur getMasAmphur() {
		return this.masAmphur;
	}

	public void setMasAmphur(MasAmphur masAmphur) {
		this.masAmphur = masAmphur;
	}

	@ManyToOne(fetch = FetchType.EAGER , cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
	@JoinColumn(name = "LICENSE_PROVINCE_ID")
	public MasProvince getMasProvince() {
		return this.masProvince;
	}

	public void setMasProvince(MasProvince masProvince) {
		this.masProvince = masProvince;
	}

	@ManyToOne(fetch = FetchType.EAGER )
	@JoinColumn(name = "TAX_ID", nullable = false)
	public DotLicenseOwner getDotLicenseOwner() {
		return this.dotLicenseOwner;
	}

	public void setDotLicenseOwner(DotLicenseOwner dotLicenseOwner) {
		this.dotLicenseOwner = dotLicenseOwner;
	}

	@ManyToOne(fetch = FetchType.EAGER , cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
	@JoinColumn(name = "LICENSE_TAMBOL_ID")
	public MasTambol getMasTambol() {
		return this.masTambol;
	}

	public void setMasTambol(MasTambol masTambol) {
		this.masTambol = masTambol;
	}

	@Column(name = "LICENSE_NO", unique = true, nullable = true)
	public String getLicenseNo() {
		return this.licenseNo;
	}

	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	
	@Column(name = "LICENSE_GUIDE_NO", unique = true, nullable = true)
	public String getLicenseGuideNo() {
		return this.licenseGuideNo;
	}

	public void setLicenseGuideNo(String licenseGuideNo) {
		this.licenseGuideNo = licenseGuideNo;
	}

	@Column(name = "LICENSE_TYPE", nullable = false)
	public String getLicenseType() {
		return this.licenseType;
	}

	public void setLicenseType(String licenseType) {
		this.licenseType = licenseType;
	}

	@Column(name = "LICENSE_CATEGORY", nullable = false)
	public String getLicenseCategory() {
		return this.licenseCategory;
	}

	public void setLicenseCategory(String licenseCategory) {
		this.licenseCategory = licenseCategory;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "EFFECTIVE_DATE", length = 23)
	public Date getEffectiveDate() {
		return this.effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "EXPIRE_DATE", length = 23)
	public Date getExpireDate() {
		return this.expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	@Column(name = "LICENSE_NAME")
	public String getLicenseName() {
		return this.licenseName;
	}

	public void setLicenseName(String licenseName) {
		this.licenseName = licenseName;
	}
	
	@Column(name = "LICENSE_STATUS")
	public String getLicenseStatus() {
		return licenseStatus;
	}

	public void setLicenseStatus(String licenseStatus) {
		this.licenseStatus = licenseStatus;
	}

	@Column(name = "LICENSE_NAME_EN")
	public String getLicenseNameEn() {
		return this.licenseNameEn;
	}

	public void setLicenseNameEn(String licenseNameEn) {
		this.licenseNameEn = licenseNameEn;
	}

	@Column(name = "PRONUNCIATION_NAME")
	public String getPronunciationName() {
		return this.pronunciationName;
	}

	public void setPronunciationName(String pronunciationName) {
		this.pronunciationName = pronunciationName;
	}
	@Column(name = "LICENSE_SERVICE_AREA")
	public String getLicenseServiceArea() {
		return licenseServiceArea;
	}

	public void setLicenseServiceArea(String licenseServiceArea) {
		this.licenseServiceArea = licenseServiceArea;
	}
	@ManyToOne(fetch = FetchType.EAGER , cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
	@JoinColumn(name = "LICENSE_SERVICE_PROVINCE")
	public MasProvince getLicenseServiceProvince() {
		return licenseServiceProvince;
	}

	public void setLicenseServiceProvince(MasProvince licenseServiceProvince) {
		this.licenseServiceProvince = licenseServiceProvince;
	}
	@Column(name = "LICENSE_BUILDING_NAME")
	public String getLicenseBuildingName() {
		return this.licenseBuildingName;
	}

	public void setLicenseBuildingName(String licenseBuildingName) {
		this.licenseBuildingName = licenseBuildingName;
	}

	@Column(name = "LICENSE_BUILDING_NAME_EN")
	public String getLicenseBuildingNameEn() {
		return this.licenseBuildingNameEn;
	}

	public void setLicenseBuildingNameEn(String licenseBuildingNameEn) {
		this.licenseBuildingNameEn = licenseBuildingNameEn;
	}

	@Column(name = "LICENSE_ROOM_NO")
	public String getLicenseRoomNo() {
		return this.licenseRoomNo;
	}

	public void setLicenseRoomNo(String licenseRoomNo) {
		this.licenseRoomNo = licenseRoomNo;
	}

	@Column(name = "LICENSE_FLOOR")
	public String getLicenseFloor() {
		return this.licenseFloor;
	}

	public void setLicenseFloor(String licenseFloor) {
		this.licenseFloor = licenseFloor;
	}

	@Column(name = "LICENSE_VILLAGE_NAME")
	public String getLicenseVillageName() {
		return this.licenseVillageName;
	}

	public void setLicenseVillageName(String licenseVillageName) {
		this.licenseVillageName = licenseVillageName;
	}

	@Column(name = "LICENSE_VILLAGE_NAME_EN")
	public String getLicenseVillageNameEn() {
		return this.licenseVillageNameEn;
	}

	public void setLicenseVillageNameEn(String licenseVillageNameEn) {
		this.licenseVillageNameEn = licenseVillageNameEn;
	}

	@Column(name = "LICENSE_HOME_NO")
	public String getLicenseHomeNo() {
		return this.licenseHomeNo;
	}

	public void setLicenseHomeNo(String licenseHomeNo) {
		this.licenseHomeNo = licenseHomeNo;
	}

	@Column(name = "LICENSE_MOO_NO")
	public String getLicenseMooNo() {
		return this.licenseMooNo;
	}

	public void setLicenseMooNo(String licenseMooNo) {
		this.licenseMooNo = licenseMooNo;
	}

	@Column(name = "LICENSE_SOI")
	public String getLicenseSoi() {
		return this.licenseSoi;
	}

	public void setLicenseSoi(String licenseSoi) {
		this.licenseSoi = licenseSoi;
	}

	@Column(name = "LICENSE_ROAD_NAME")
	public String getLicenseRoadName() {
		return this.licenseRoadName;
	}

	public void setLicenseRoadName(String licenseRoadName) {
		this.licenseRoadName = licenseRoadName;
	}

	@Column(name = "LICENSE_ROAD_NAME_EN")
	public String getLicenseRoadNameEn() {
		return this.licenseRoadNameEn;
	}

	public void setLicenseRoadNameEn(String licenseRoadNameEn) {
		this.licenseRoadNameEn = licenseRoadNameEn;
	}

	@Column(name = "LICENSE_POST_CODE")
	public String getLicensePostCode() {
		return this.licensePostCode;
	}

	public void setLicensePostCode(String licensePostCode) {
		this.licensePostCode = licensePostCode;
	}

	@Column(name = "LICENSE_EMAIL")
	public String getLicenseEmail() {
		return this.licenseEmail;
	}

	public void setLicenseEmail(String licenseEmail) {
		this.licenseEmail = licenseEmail;
	}

	@Column(name = "LICENSE_FAX")
	public String getLicenseFax() {
		return this.licenseFax;
	}

	public void setLicenseFax(String licenseFax) {
		this.licenseFax = licenseFax;
	}

	@Column(name = "LICENSE_MOBILE_NO")
	public String getLicenseMobileNo() {
		return this.licenseMobileNo;
	}

	public void setLicenseMobileNo(String licenseMobileNo) {
		this.licenseMobileNo = licenseMobileNo;
	}

	@Column(name = "LICENSE_TELEPHONE")
	public String getLicenseTelephone() {
		return this.licenseTelephone;
	}

	public void setLicenseTelephone(String licenseTelephone) {
		this.licenseTelephone = licenseTelephone;
	}

	@Column(name = "LICENSE_WEBSITE")
	public String getLicenseWebsite() {
		return this.licenseWebsite;
	}

	public void setLicenseWebsite(String licenseWebsite) {
		this.licenseWebsite = licenseWebsite;
	}

	@Column(name = "LICENSE_MAP_LAT")
	public String getLicenseMapLat() {
		return this.licenseMapLat;
	}

	public void setLicenseMapLat(String licenseMapLat) {
		this.licenseMapLat = licenseMapLat;
	}

	@Column(name = "LICENSE_MAP_LONG")
	public String getLicenseMapLong() {
		return this.licenseMapLong;
	}

	public void setLicenseMapLong(String licenseMapLong) {
		this.licenseMapLong = licenseMapLong;
	}

	@Column(name = "RECORD_STATUS")
	public String getRecordStatus() {
		return this.recordStatus;
	}

	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}

	@Column(name = "CREATE_USER")
	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATE_DTM", length = 23)
	public Date getCreateDtm() {
		return this.createDtm;
	}

	public void setCreateDtm(Date createDtm) {
		this.createDtm = createDtm;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPD_DTM", length = 23)
	public Date getLastUpdDtm() {
		return this.lastUpdDtm;
	}

	public void setLastUpdDtm(Date lastUpdDtm) {
		this.lastUpdDtm = lastUpdDtm;
	}

	@Column(name = "LAST_UPD_USER")
	public String getLastUpdUser() {
		return this.lastUpdUser;
	}

	public void setLastUpdUser(String lastUpdUser) {
		this.lastUpdUser = lastUpdUser;
	}

	@Column(name = "PLAN_TRIPS")
	public Integer getPlanTrips() {
		return this.planTrips;
	}

	public void setPlanTrips(Integer planTrips) {
		this.planTrips = planTrips;
	}

//	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "dotLicense")
//	public List<CountryTour> getCountryTours() {
//		return this.countryTours;
//	}
////
//	public void setCountryTours(List<CountryTour> countryTours) {
//		this.countryTours = countryTours;
//	}
//
//	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY, mappedBy = "dotLicense")
//	public List<DotTrFeeLicense> getDotTrFeeLicenses() {
//		return this.dotTrFeeLicenses;
//	}
//
//	public void setDotTrFeeLicenses(List<DotTrFeeLicense> dotTrFeeLicenses) {
//		this.dotTrFeeLicenses = dotTrFeeLicenses;
//	}
//
//	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER, mappedBy = "dotLicense")
//	public List<DotSubLicense> getDotSubLicenses() {
//		return this.dotSubLicenses;
//	}
//
//	public void setDotSubLicenses(List<DotSubLicense> dotSubLicenses) {
//		this.dotSubLicenses = dotSubLicenses;
//	}
//	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY, mappedBy = "dotLicense")
//	public List<RegDocument> getRegDocuments() {
//		return this.regDocuments;
//	}
//
//	public void setRegDocuments(List<RegDocument> regDocuments) {
//		this.regDocuments = regDocuments;
//	}
//	
//	@OneToMany(fetch = FetchType.LAZY, mappedBy = "dotLicense")
//	@Transient
//	public List<DotLicOwnerEduBackground> getDotLicOwnerEduBackgrounds() {
//		return dotLicOwnerEduBackgrounds;
//	}
//
//	public void setDotLicOwnerEduBackgrounds(
//			List<DotLicOwnerEduBackground> dotLicOwnerEduBackgrounds) {
//		this.dotLicOwnerEduBackgrounds = dotLicOwnerEduBackgrounds;
//	}

	@Column(name = "LICENSE_SOI_EN")
	public String getLicenseSoiEn() {
		return licenseSoiEn;
	}

	public void setLicenseSoiEn(String licenseSoiEn) {
		this.licenseSoiEn = licenseSoiEn;
	}
	@ManyToOne(fetch = FetchType.EAGER , cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
	@JoinColumn(name = "CASH_ACCOUNT_BANK_ID")
	public MasBank getMasBankByCashAccountBankId() {
		return this.masBankByCashAccountBankId;
	}

	public void setMasBankByCashAccountBankId(MasBank masBankByCashAccountBankId) {
		this.masBankByCashAccountBankId = masBankByCashAccountBankId;
	}
	
	@ManyToOne(fetch = FetchType.EAGER , cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
	@JoinColumn(name = "BANK_GUARANTEE_BANK_ID")
	public MasBank getMasBankByBankGuaranteeBankId() {
		return this.masBankByBankGuaranteeBankId;
	}

	public void setMasBankByBankGuaranteeBankId(
			MasBank masBankByBankGuaranteeBankId) {
		this.masBankByBankGuaranteeBankId = masBankByBankGuaranteeBankId;
	}
	
	@Column(name = "CASH_ACCOUNT_ID")
	public String getCashAccountId() {
		return this.cashAccountId;
	}

	public void setCashAccountId(String cashAccountId) {
		this.cashAccountId = cashAccountId;
	}

	@Column(name = "CASH_ACCOUNT_NAME")
	public String getCashAccountName() {
		return this.cashAccountName;
	}

	public void setCashAccountName(String cashAccountName) {
		this.cashAccountName = cashAccountName;
	}

	@Column(name = "CASH_ACCOUNT_BANK_BRANCH_NAME")
	public String getCashAccountBankBranchName() {
		return this.cashAccountBankBranchName;
	}

	public void setCashAccountBankBranchName(String cashAccountBankBranchName) {
		this.cashAccountBankBranchName = cashAccountBankBranchName;
	}

	@Column(name = "CASH_ACCOUNT_MNY")
	public BigDecimal getCashAccountMny() {
		return this.cashAccountMny;
	}

	public void setCashAccountMny(BigDecimal cashAccountMny) {
		this.cashAccountMny = cashAccountMny;
	}

	@Column(name = "BANK_GUARANTEE_ID")
	public String getBankGuaranteeId() {
		return this.bankGuaranteeId;
	}

	public void setBankGuaranteeId(String bankGuaranteeId) {
		this.bankGuaranteeId = bankGuaranteeId;
	}

	@Column(name = "BANK_GUARANTEE_NAME")
	public String getBankGuaranteeName() {
		return this.bankGuaranteeName;
	}

	public void setBankGuaranteeName(String bankGuaranteeName) {
		this.bankGuaranteeName = bankGuaranteeName;
	}

	@Column(name = "BANK_GUARANTEE_MNY")
	public BigDecimal getBankGuaranteeMny() {
		return this.bankGuaranteeMny;
	}

	public void setBankGuaranteeMny(BigDecimal bankGuaranteeMny) {
		this.bankGuaranteeMny = bankGuaranteeMny;
	}
	@Column(name = "BANK_GUARANTEE_BANK_BRANCH_NAME")
	public String getBankGuaranteeBankBranchName() {
		return bankGuaranteeBankBranchName;
	}

	public void setBankGuaranteeBankBranchName(String bankGuaranteeBankBranchName) {
		this.bankGuaranteeBankBranchName = bankGuaranteeBankBranchName;
	}
	@Column(name = "GOVERNMENT_BOND_ID")
	public String getGovernmentBondId() {
		return this.governmentBondId;
	}

	public void setGovernmentBondId(String governmentBondId) {
		this.governmentBondId = governmentBondId;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "GOVERNMENT_BOND_EXPIRE_DATE", length = 23)
	public Date getGovernmentBondExpireDate() {
		return this.governmentBondExpireDate;
	}

	public void setGovernmentBondExpireDate(Date governmentBondExpireDate) {
		this.governmentBondExpireDate = governmentBondExpireDate;
	}

	@Column(name = "GOVERNMENT_BOND_MNY")
	public BigDecimal getGovernmentBondMny() {
		return this.governmentBondMny;
	}

	public void setGovernmentBondMny(BigDecimal governmentBondMny) {
		this.governmentBondMny = governmentBondMny;
	}

}
