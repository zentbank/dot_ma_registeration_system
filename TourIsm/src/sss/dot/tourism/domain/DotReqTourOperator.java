package sss.dot.tourism.domain;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.NaturalId;

@Entity
@Table(name = "DOT_REQ_TOUR_OPERATOR")
public class DotReqTourOperator implements java.io.Serializable{

	private String taxId;
	private MasPrefix masPrefix;
	private MasPosfix masPosfix;
	private String companyName;
	private String reqCardId;
	private MasPrefix reqPrefix;
	private String reqName;
	private String reqLastName;
	private String reqRelationship;
	private String reqDesc1;
	private String reqDesc2;
	private String reqDesc3;
	private String reqDesc4;
	private String reqDesc5;
	private String reqDesc6;
	private String reqDesc7;
	private String reqDesc8;
	private String reqDesc9;
	private String reqDesc10;
	private String reqDesc11;
	private String reqDesc12;
	private String interviewer;
	private String witness;
	private String reqNation;
	
	
	
	
	private MasAmphur masAmphur;
	private MasProvince masProvince;
	private MasTambol masTambol;
	
	
	private String villageName;
	private String villageNameEn;
	private String buildingName;
	private String buildingNameEn;
	private String floor;
	private String roomNo;
	private String homeNo;
	private String mooNo;
	private String soi;
	private String soiEn;
	private String roadName;
	private String roadNameEn;
	private String postCode;
	private String email;
	private String fax;
	private String mobileNo;
	private String telephone;
	private String website;
	private String recordStatus;
	private Date createDtm;
	private String createUser;
	private Date lastUpdDtm;
	private String lastUpdUser;
	
	
	
	
	@Id
	@NaturalId
	@Column(name = "TAX_ID", unique = true, nullable = false)
	public String getTaxId() {
		return this.taxId;
	}

	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}
	
	@ManyToOne(fetch = FetchType.EAGER , cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
	@JoinColumn(name = "PREFIX_ID", nullable = false)
	public MasPrefix getMasPrefix() {
		return this.masPrefix;
	}

	public void setMasPrefix(MasPrefix masPrefix) {
		this.masPrefix = masPrefix;
	}

	@ManyToOne(fetch = FetchType.EAGER , cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
	@JoinColumn(name = "POSTFIX_ID")
	public MasPosfix getMasPosfix() {
		return this.masPosfix;
	}

	public void setMasPosfix(MasPosfix masPosfix) {
		this.masPosfix = masPosfix;
	}
	@Column(name = "COMPANY_NAME")
	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	@Column(name = "REQ_CARD_ID")
	public String getReqCardId() {
		return reqCardId;
	}

	public void setReqCardId(String reqCardId) {
		this.reqCardId = reqCardId;
	}
	@ManyToOne(fetch = FetchType.EAGER , cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
	@JoinColumn(name = "REQ_PREFIX")
	public MasPrefix getReqPrefix() {
		return reqPrefix;
	}

	public void setReqPrefix(MasPrefix reqPrefix) {
		this.reqPrefix = reqPrefix;
	}
	
	
	@Column(name = "REQ_NAME")
	public String getReqName() {
		return reqName;
	}

	public void setReqName(String reqName) {
		this.reqName = reqName;
	}
	@Column(name = "REQ_LASTNAME")
	public String getReqLastName() {
		return reqLastName;
	}

	public void setReqLastName(String reqLastName) {
		this.reqLastName = reqLastName;
	}
	@Column(name = "REQ_RELATIONSHIP")
	public String getReqRelationship() {
		return reqRelationship;
	}

	public void setReqRelationship(String reqRelationship) {
		this.reqRelationship = reqRelationship;
	}
	@Column(name = "REQ_DESC_1")
	public String getReqDesc1() {
		return reqDesc1;
	}

	public void setReqDesc1(String reqDesc1) {
		this.reqDesc1 = reqDesc1;
	}
	@Column(name = "REQ_DESC_2")
	public String getReqDesc2() {
		return reqDesc2;
	}

	public void setReqDesc2(String reqDesc2) {
		this.reqDesc2 = reqDesc2;
	}
	@Column(name = "REQ_DESC_3")
	public String getReqDesc3() {
		return reqDesc3;
	}

	public void setReqDesc3(String reqDesc3) {
		this.reqDesc3 = reqDesc3;
	}
	@Column(name = "REQ_DESC_4")
	public String getReqDesc4() {
		return reqDesc4;
	}

	public void setReqDesc4(String reqDesc4) {
		this.reqDesc4 = reqDesc4;
	}
	@Column(name = "REQ_DESC_5")
	public String getReqDesc5() {
		return reqDesc5;
	}

	public void setReqDesc5(String reqDesc5) {
		this.reqDesc5 = reqDesc5;
	}
	@Column(name = "REQ_DESC_6")
	public String getReqDesc6() {
		return reqDesc6;
	}

	public void setReqDesc6(String reqDesc6) {
		this.reqDesc6 = reqDesc6;
	}
	@Column(name = "REQ_DESC_7")
	public String getReqDesc7() {
		return reqDesc7;
	}

	public void setReqDesc7(String reqDesc7) {
		this.reqDesc7 = reqDesc7;
	}
	@Column(name = "REQ_DESC_8")
	public String getReqDesc8() {
		return reqDesc8;
	}

	public void setReqDesc8(String reqDesc8) {
		this.reqDesc8 = reqDesc8;
	}
	@Column(name = "REQ_DESC_9")
	public String getReqDesc9() {
		return reqDesc9;
	}

	public void setReqDesc9(String reqDesc9) {
		this.reqDesc9 = reqDesc9;
	}
	@Column(name = "REQ_DESC_10")
	public String getReqDesc10() {
		return reqDesc10;
	}

	public void setReqDesc10(String reqDesc10) {
		this.reqDesc10 = reqDesc10;
	}
	@Column(name = "REQ_DESC_11")
	public String getReqDesc11() {
		return reqDesc11;
	}

	public void setReqDesc11(String reqDesc11) {
		this.reqDesc11 = reqDesc11;
	}
	@Column(name = "REQ_DESC_12")
	public String getReqDesc12() {
		return reqDesc12;
	}

	public void setReqDesc12(String reqDesc12) {
		this.reqDesc12 = reqDesc12;
	}
	@Column(name = "INTERVIEWER")
	public String getInterviewer() {
		return interviewer;
	}

	public void setInterviewer(String interviewer) {
		this.interviewer = interviewer;
	}
	@Column(name = "WITNESS")
	public String getWitness() {
		return witness;
	}

	public void setWitness(String witness) {
		this.witness = witness;
	}
	
	@Column(name = "REQ_NATION")
	public String getReqNation() {
		return reqNation;
	}

	public void setReqNation(String reqNation) {
		this.reqNation = reqNation;
	}
	

	@ManyToOne(fetch = FetchType.EAGER , cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
	@JoinColumn(name = "AMPHUR_ID")
	public MasAmphur getMasAmphur() {
		return this.masAmphur;
	}
	

	public void setMasAmphur(MasAmphur masAmphur) {
		this.masAmphur = masAmphur;
	}

	@ManyToOne(fetch = FetchType.EAGER , cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
	@JoinColumn(name = "PROVINCE_ID")
	public MasProvince getMasProvince() {
		return this.masProvince;
	}

	public void setMasProvince(MasProvince masProvince) {
		this.masProvince = masProvince;
	}

	@ManyToOne(fetch = FetchType.EAGER , cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
	@JoinColumn(name = "TAMBOL_ID")
	public MasTambol getMasTambol() {
		return this.masTambol;
	}

	public void setMasTambol(MasTambol masTambol) {
		this.masTambol = masTambol;
	}

	@Column(name = "VILLAGE_NAME")
	public String getVillageName() {
		return this.villageName;
	}

	public void setVillageName(String villageName) {
		this.villageName = villageName;
	}

	@Column(name = "VILLAGE_NAME_EN")
	public String getVillageNameEn() {
		return this.villageNameEn;
	}

	public void setVillageNameEn(String villageNameEn) {
		this.villageNameEn = villageNameEn;
	}

	@Column(name = "BUILDING_NAME")
	public String getBuildingName() {
		return this.buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	@Column(name = "BUILDING_NAME_EN")
	public String getBuildingNameEn() {
		return this.buildingNameEn;
	}

	public void setBuildingNameEn(String buildingNameEn) {
		this.buildingNameEn = buildingNameEn;
	}

	@Column(name = "FLOOR")
	public String getFloor() {
		return this.floor;
	}

	public void setFloor(String floor) {
		this.floor = floor;
	}

	@Column(name = "ROOM_NO")
	public String getRoomNo() {
		return this.roomNo;
	}

	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}

	@Column(name = "HOME_NO")
	public String getHomeNo() {
		return this.homeNo;
	}

	public void setHomeNo(String homeNo) {
		this.homeNo = homeNo;
	}

	@Column(name = "MOO_NO")
	public String getMooNo() {
		return this.mooNo;
	}

	public void setMooNo(String mooNo) {
		this.mooNo = mooNo;
	}

	@Column(name = "SOI")
	public String getSoi() {
		return this.soi;
	}

	public void setSoi(String soi) {
		this.soi = soi;
	}

	@Column(name = "SOI_EN")
	public String getSoiEn() {
		return soiEn;
	}

	public void setSoiEn(String soiEn) {
		this.soiEn = soiEn;
	}

	@Column(name = "ROAD_NAME")
	public String getRoadName() {
		return this.roadName;
	}

	public void setRoadName(String roadName) {
		this.roadName = roadName;
	}

	@Column(name = "ROAD_NAME_EN")
	public String getRoadNameEn() {
		return this.roadNameEn;
	}

	public void setRoadNameEn(String roadNameEn) {
		this.roadNameEn = roadNameEn;
	}

	@Column(name = "POST_CODE")
	public String getPostCode() {
		return this.postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	@Column(name = "EMAIL")
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "FAX")
	public String getFax() {
		return this.fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	@Column(name = "MOBILE_NO")
	public String getMobileNo() {
		return this.mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	@Column(name = "TELEPHONE")
	public String getTelephone() {
		return this.telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	@Column(name = "WEBSITE")
	public String getWebsite() {
		return this.website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	@Column(name = "RECORD_STATUS")
	public String getRecordStatus() {
		return this.recordStatus;
	}

	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATE_DTM", length = 23)
	public Date getCreateDtm() {
		return this.createDtm;
	}

	public void setCreateDtm(Date createDtm) {
		this.createDtm = createDtm;
	}

	@Column(name = "CREATE_USER")
	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPD_DTM", length = 23)
	public Date getLastUpdDtm() {
		return this.lastUpdDtm;
	}

	public void setLastUpdDtm(Date lastUpdDtm) {
		this.lastUpdDtm = lastUpdDtm;
	}

	@Column(name = "LAST_UPD_USER")
	public String getLastUpdUser() {
		return this.lastUpdUser;
	}

	public void setLastUpdUser(String lastUpdUser) {
		this.lastUpdUser = lastUpdUser;
	}


}
