package sss.dot.tourism.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "DOT_REQ_TOUR_OPERATOR_DOC")
public class DotReqTourOperatorDoc implements java.io.Serializable{
	private long reqDocId;
	private DotReqTourOperator dotReqTourOperator;
	private MasDocument masDocument;
	private String reqDocPath;
	private String recordStatus;
	private String createUser;
	private Date createDtm;
	private Date lastUpdDtm;
	private String lastUpdUser;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "REQ_PASS_ID", unique = true, nullable = false)
	public long getReqDocId() {
		return this.reqDocId;
	}

	public void setReqDocId(long reqDocId) {
		this.reqDocId = reqDocId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TAX_ID", nullable = false)
	public DotReqTourOperator getDotReqTourOperator() {
		return dotReqTourOperator;
	}

	
	public void setDotReqTourOperator(DotReqTourOperator dotReqTourOperator) {
		this.dotReqTourOperator = dotReqTourOperator;
	}

	public void setMasDocument(MasDocument masDocument) {
		this.masDocument = masDocument;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MAS_DOC_ID", nullable = false)
	public MasDocument getMasDocument() {
		return this.masDocument;
	}

	
	

	@Column(name = "REQ_DOC_PATH", nullable = false)
	public String getReqDocPath() {
		return this.reqDocPath;
	}

	public void setReqDocPath(String reqDocPath) {
		this.reqDocPath = reqDocPath;
	}

	@Column(name = "RECORD_STATUS")
	public String getRecordStatus() {
		return this.recordStatus;
	}

	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}

	@Column(name = "CREATE_USER")
	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATE_DTM", length = 23)
	public Date getCreateDtm() {
		return this.createDtm;
	}

	public void setCreateDtm(Date createDtm) {
		this.createDtm = createDtm;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPD_DTM", length = 23)
	public Date getLastUpdDtm() {
		return this.lastUpdDtm;
	}

	public void setLastUpdDtm(Date lastUpdDtm) {
		this.lastUpdDtm = lastUpdDtm;
	}

	@Column(name = "LAST_UPD_USER")
	public String getLastUpdUser() {
		return this.lastUpdUser;
	}

	public void setLastUpdUser(String lastUpdUser) {
		this.lastUpdUser = lastUpdUser;
	}

}
