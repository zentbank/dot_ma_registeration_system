package sss.dot.tourism.domain;

// Generated Dec 19, 2014 3:10:09 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * JobOrderHotel generated by hbm2java
 */
@Entity
@Table(name = "JOB_ORDER_HOTEL")
public class JobOrderHotel implements java.io.Serializable {

	private long hotelId;
	private JobOrder jobOrder;
	private String tourisType;
	private Integer roomType1;
	private Integer roomType2;
	private Integer roomType3;
	private String remark;
	private String recordStatus;
	private String createUser;
	private Date createDtm;
	private String lastUpdUser;
	private Date lastUpdDtm;

	public JobOrderHotel() {
	}

	public JobOrderHotel(long hotelId, JobOrder jobOrder) {
		this.hotelId = hotelId;
		this.jobOrder = jobOrder;
	}

	public JobOrderHotel(long hotelId, JobOrder jobOrder, String tourisType,
			Integer roomType1, Integer roomType2, Integer roomType3,
			String remark, String recordStatus, String createUser,
			Date createDtm, String lastUpdUser, Date lastUpdDtm) {
		this.hotelId = hotelId;
		this.jobOrder = jobOrder;
		this.tourisType = tourisType;
		this.roomType1 = roomType1;
		this.roomType2 = roomType2;
		this.roomType3 = roomType3;
		this.remark = remark;
		this.recordStatus = recordStatus;
		this.createUser = createUser;
		this.createDtm = createDtm;
		this.lastUpdUser = lastUpdUser;
		this.lastUpdDtm = lastUpdDtm;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "HOTEL_ID", unique = true, nullable = false)
	public long getHotelId() {
		return this.hotelId;
	}

	public void setHotelId(long hotelId) {
		this.hotelId = hotelId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "JOB_ID", nullable = false)
	public JobOrder getJobOrder() {
		return this.jobOrder;
	}

	public void setJobOrder(JobOrder jobOrder) {
		this.jobOrder = jobOrder;
	}

	@Column(name = "TOURIS_TYPE")
	public String getTourisType() {
		return this.tourisType;
	}

	public void setTourisType(String tourisType) {
		this.tourisType = tourisType;
	}

	@Column(name = "ROOM_TYPE_1")
	public Integer getRoomType1() {
		return this.roomType1;
	}

	public void setRoomType1(Integer roomType1) {
		this.roomType1 = roomType1;
	}

	@Column(name = "ROOM_TYPE_2")
	public Integer getRoomType2() {
		return this.roomType2;
	}

	public void setRoomType2(Integer roomType2) {
		this.roomType2 = roomType2;
	}

	@Column(name = "ROOM_TYPE_3")
	public Integer getRoomType3() {
		return this.roomType3;
	}

	public void setRoomType3(Integer roomType3) {
		this.roomType3 = roomType3;
	}

	@Column(name = "REMARK")
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "RECORD_STATUS")
	public String getRecordStatus() {
		return this.recordStatus;
	}

	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}

	@Column(name = "CREATE_USER")
	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATE_DTM", length = 23)
	public Date getCreateDtm() {
		return this.createDtm;
	}

	public void setCreateDtm(Date createDtm) {
		this.createDtm = createDtm;
	}

	@Column(name = "LAST_UPD_USER")
	public String getLastUpdUser() {
		return this.lastUpdUser;
	}

	public void setLastUpdUser(String lastUpdUser) {
		this.lastUpdUser = lastUpdUser;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPD_DTM", length = 23)
	public Date getLastUpdDtm() {
		return this.lastUpdDtm;
	}

	public void setLastUpdDtm(Date lastUpdDtm) {
		this.lastUpdDtm = lastUpdDtm;
	}

}
