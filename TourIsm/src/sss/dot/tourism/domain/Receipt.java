package sss.dot.tourism.domain;

// Generated Dec 19, 2014 3:10:09 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Receipt generated by hbm2java
 */
@Entity
@Table(name = "RECEIPT")
public class Receipt implements java.io.Serializable {

	private long receiptId;
	private String bookNo;
	private String receiptNo;
	private String receiptName;
	private String receiveOfficerName;
	private Date receiveOfficerDate;
	private String authority;
	private String receiptStatus;
	private String receiptRemark;
	private String recordStatus;
	private String createUser;
	private Date createDtm;
	private String lastUpdUser;
	private Date lastUpdDtm;

	public Receipt() {
	}

	public Receipt(long receiptId) {
		this.receiptId = receiptId;
	}

	public Receipt(long receiptId, String bookNo, String receiptNo,
			String receiptName, String receiveOfficerName,
			Date receiveOfficerDate, String authority, String receiptStatus,
			String receiptRemark, String recordStatus, String createUser,
			Date createDtm, String lastUpdUser, Date lastUpdDtm) {
		this.receiptId = receiptId;
		this.bookNo = bookNo;
		this.receiptNo = receiptNo;
		this.receiptName = receiptName;
		this.receiveOfficerName = receiveOfficerName;
		this.receiveOfficerDate = receiveOfficerDate;
		this.authority = authority;
		this.receiptStatus = receiptStatus;
		this.receiptRemark = receiptRemark;
		this.recordStatus = recordStatus;
		this.createUser = createUser;
		this.createDtm = createDtm;
		this.lastUpdUser = lastUpdUser;
		this.lastUpdDtm = lastUpdDtm;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "RECEIPT_ID", unique = true, nullable = false)
	public long getReceiptId() {
		return this.receiptId;
	}

	public void setReceiptId(long receiptId) {
		this.receiptId = receiptId;
	}

	@Column(name = "BOOK_NO")
	public String getBookNo() {
		return this.bookNo;
	}

	public void setBookNo(String bookNo) {
		this.bookNo = bookNo;
	}

	@Column(name = "RECEIPT_NO")
	public String getReceiptNo() {
		return this.receiptNo;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	@Column(name = "RECEIPT_NAME")
	public String getReceiptName() {
		return this.receiptName;
	}

	public void setReceiptName(String receiptName) {
		this.receiptName = receiptName;
	}

	@Column(name = "RECEIVE_OFFICER_NAME")
	public String getReceiveOfficerName() {
		return this.receiveOfficerName;
	}

	public void setReceiveOfficerName(String receiveOfficerName) {
		this.receiveOfficerName = receiveOfficerName;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "RECEIVE_OFFICER_DATE", length = 23)
	public Date getReceiveOfficerDate() {
		return this.receiveOfficerDate;
	}

	public void setReceiveOfficerDate(Date receiveOfficerDate) {
		this.receiveOfficerDate = receiveOfficerDate;
	}

	@Column(name = "AUTHORITY")
	public String getAuthority() {
		return this.authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	@Column(name = "RECEIPT_STATUS")
	public String getReceiptStatus() {
		return this.receiptStatus;
	}

	public void setReceiptStatus(String receiptStatus) {
		this.receiptStatus = receiptStatus;
	}

	@Column(name = "RECEIPT_REMARK")
	public String getReceiptRemark() {
		return this.receiptRemark;
	}

	public void setReceiptRemark(String receiptRemark) {
		this.receiptRemark = receiptRemark;
	}

	@Column(name = "RECORD_STATUS")
	public String getRecordStatus() {
		return this.recordStatus;
	}

	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}

	@Column(name = "CREATE_USER")
	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATE_DTM", length = 23)
	public Date getCreateDtm() {
		return this.createDtm;
	}

	public void setCreateDtm(Date createDtm) {
		this.createDtm = createDtm;
	}

	@Column(name = "LAST_UPD_USER")
	public String getLastUpdUser() {
		return this.lastUpdUser;
	}

	public void setLastUpdUser(String lastUpdUser) {
		this.lastUpdUser = lastUpdUser;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPD_DTM", length = 23)
	public Date getLastUpdDtm() {
		return this.lastUpdDtm;
	}

	public void setLastUpdDtm(Date lastUpdDtm) {
		this.lastUpdDtm = lastUpdDtm;
	}

}
