package sss.dot.tourism.domain;

// Generated Dec 19, 2014 3:10:09 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * ReqLicenseCertificate generated by hbm2java
 */
@Entity
@Table(name = "REQ_LICENSE_CERTIFICATE")
public class ReqLicenseCertificate implements java.io.Serializable {

	private long cerId;
	private String licenseNo;
	private Date reqDate;
	private Date printDate;
	private String approveStatus;
	private Date approveDate;
	private String approveRemark;
	private String recordStatus;
	private String createUser;
	private Date createDtm;
	private String lastUpdUser;
	private Date lastUpdDtm;
	private String bookNo;
	private String bookDocToManagerNo;
	private Date bookDocManagerDate;
	private Date bookDate;
	private String ttAaNo;
	private Date ttAaDate;

	public ReqLicenseCertificate() {
	}

	public ReqLicenseCertificate(long cerId, String licenseNo, Date reqDate) {
		this.cerId = cerId;
		this.licenseNo = licenseNo;
		this.reqDate = reqDate;
	}

	public ReqLicenseCertificate(long cerId, String licenseNo, Date reqDate,
			Date printDate, String approveStatus, Date approveDate,
			String approveRemark, String recordStatus, String createUser,
			Date createDtm, String lastUpdUser, Date lastUpdDtm, String bookNo,
			String bookDocToManagerNo, Date bookDocManagerDate, Date bookDate,
			String ttAaNo, Date ttAaDate) {
		this.cerId = cerId;
		this.licenseNo = licenseNo;
		this.reqDate = reqDate;
		this.printDate = printDate;
		this.approveStatus = approveStatus;
		this.approveDate = approveDate;
		this.approveRemark = approveRemark;
		this.recordStatus = recordStatus;
		this.createUser = createUser;
		this.createDtm = createDtm;
		this.lastUpdUser = lastUpdUser;
		this.lastUpdDtm = lastUpdDtm;
		this.bookNo = bookNo;
		this.bookDocToManagerNo = bookDocToManagerNo;
		this.bookDocManagerDate = bookDocManagerDate;
		this.bookDate = bookDate;
		this.ttAaNo = ttAaNo;
		this.ttAaDate = ttAaDate;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CER_ID", unique = true, nullable = false)
	public long getCerId() {
		return this.cerId;
	}

	public void setCerId(long cerId) {
		this.cerId = cerId;
	}

	@Column(name = "LICENSE_NO", nullable = false)
	public String getLicenseNo() {
		return this.licenseNo;
	}

	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "REQ_DATE", nullable = false, length = 23)
	public Date getReqDate() {
		return this.reqDate;
	}

	public void setReqDate(Date reqDate) {
		this.reqDate = reqDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "PRINT_DATE", length = 23)
	public Date getPrintDate() {
		return this.printDate;
	}

	public void setPrintDate(Date printDate) {
		this.printDate = printDate;
	}

	@Column(name = "APPROVE_STATUS")
	public String getApproveStatus() {
		return this.approveStatus;
	}

	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "APPROVE_DATE", length = 23)
	public Date getApproveDate() {
		return this.approveDate;
	}

	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}

	@Column(name = "APPROVE_REMARK")
	public String getApproveRemark() {
		return this.approveRemark;
	}

	public void setApproveRemark(String approveRemark) {
		this.approveRemark = approveRemark;
	}

	@Column(name = "RECORD_STATUS")
	public String getRecordStatus() {
		return this.recordStatus;
	}

	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}

	@Column(name = "CREATE_USER")
	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATE_DTM", length = 23)
	public Date getCreateDtm() {
		return this.createDtm;
	}

	public void setCreateDtm(Date createDtm) {
		this.createDtm = createDtm;
	}

	@Column(name = "LAST_UPD_USER")
	public String getLastUpdUser() {
		return this.lastUpdUser;
	}

	public void setLastUpdUser(String lastUpdUser) {
		this.lastUpdUser = lastUpdUser;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPD_DTM", length = 23)
	public Date getLastUpdDtm() {
		return this.lastUpdDtm;
	}

	public void setLastUpdDtm(Date lastUpdDtm) {
		this.lastUpdDtm = lastUpdDtm;
	}

	@Column(name = "BOOK_NO")
	public String getBookNo() {
		return this.bookNo;
	}

	public void setBookNo(String bookNo) {
		this.bookNo = bookNo;
	}

	@Column(name = "BOOK_DOC_TO_MANAGER_NO")
	public String getBookDocToManagerNo() {
		return this.bookDocToManagerNo;
	}

	public void setBookDocToManagerNo(String bookDocToManagerNo) {
		this.bookDocToManagerNo = bookDocToManagerNo;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "BOOK_DOC_MANAGER_DATE", length = 23)
	public Date getBookDocManagerDate() {
		return this.bookDocManagerDate;
	}

	public void setBookDocManagerDate(Date bookDocManagerDate) {
		this.bookDocManagerDate = bookDocManagerDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "BOOK_DATE", length = 23)
	public Date getBookDate() {
		return this.bookDate;
	}

	public void setBookDate(Date bookDate) {
		this.bookDate = bookDate;
	}

	@Column(name = "TT_AA_NO")
	public String getTtAaNo() {
		return this.ttAaNo;
	}

	public void setTtAaNo(String ttAaNo) {
		this.ttAaNo = ttAaNo;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TT_AA_DATE", length = 23)
	public Date getTtAaDate() {
		return this.ttAaDate;
	}

	public void setTtAaDate(Date ttAaDate) {
		this.ttAaDate = ttAaDate;
	}

}
