package sss.dot.tourism.domain;

// Generated Dec 19, 2014 3:10:09 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * RunningNo generated by hbm2java
 */
@Entity
@Table(name = "RUNNING_NO")
public class RunningNo implements java.io.Serializable {

	private long runningId;
	private Organization organization;
	private String runningNo1;
	private String budgetYear;
	private String traderType;
	private String runningName;
	private String runningPrefix;
	private String runingSeparater;
	private Integer runningNo2;
	private String traderCategory;
	private String recordStatus;
	private String createUser;
	private Date createDtm;
	private String lastUpdUser;
	private Date lastUpdDtm;

	public RunningNo() {
	}

	public RunningNo(long runningId) {
		this.runningId = runningId;
	}

	public RunningNo(long runningId, Organization organization,
			String runningNo1, String budgetYear, String traderType,
			String runningName, String runningPrefix, String runingSeparater,
			Integer runningNo2, String traderCategory, String recordStatus,
			String createUser, Date createDtm, String lastUpdUser,
			Date lastUpdDtm) {
		this.runningId = runningId;
		this.organization = organization;
		this.runningNo1 = runningNo1;
		this.budgetYear = budgetYear;
		this.traderType = traderType;
		this.runningName = runningName;
		this.runningPrefix = runningPrefix;
		this.runingSeparater = runingSeparater;
		this.runningNo2 = runningNo2;
		this.traderCategory = traderCategory;
		this.recordStatus = recordStatus;
		this.createUser = createUser;
		this.createDtm = createDtm;
		this.lastUpdUser = lastUpdUser;
		this.lastUpdDtm = lastUpdDtm;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "RUNNING_ID", unique = true, nullable = false)
	public long getRunningId() {
		return this.runningId;
	}

	public void setRunningId(long runningId) {
		this.runningId = runningId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ORG_ID")
	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	@Column(name = "RUNNING_NO1")
	public String getRunningNo1() {
		return this.runningNo1;
	}

	public void setRunningNo1(String runningNo1) {
		this.runningNo1 = runningNo1;
	}

	@Column(name = "BUDGET_YEAR")
	public String getBudgetYear() {
		return this.budgetYear;
	}

	public void setBudgetYear(String budgetYear) {
		this.budgetYear = budgetYear;
	}

	@Column(name = "TRADER_TYPE")
	public String getTraderType() {
		return this.traderType;
	}

	public void setTraderType(String traderType) {
		this.traderType = traderType;
	}

	@Column(name = "RUNNING_NAME")
	public String getRunningName() {
		return this.runningName;
	}

	public void setRunningName(String runningName) {
		this.runningName = runningName;
	}

	@Column(name = "RUNNING_PREFIX")
	public String getRunningPrefix() {
		return this.runningPrefix;
	}

	public void setRunningPrefix(String runningPrefix) {
		this.runningPrefix = runningPrefix;
	}

	@Column(name = "RUNING_SEPARATER")
	public String getRuningSeparater() {
		return this.runingSeparater;
	}

	public void setRuningSeparater(String runingSeparater) {
		this.runingSeparater = runingSeparater;
	}

	@Column(name = "RUNNING_NO2")
	public Integer getRunningNo2() {
		return this.runningNo2;
	}

	public void setRunningNo2(Integer runningNo2) {
		this.runningNo2 = runningNo2;
	}

	@Column(name = "TRADER_CATEGORY")
	public String getTraderCategory() {
		return this.traderCategory;
	}

	public void setTraderCategory(String traderCategory) {
		this.traderCategory = traderCategory;
	}

	@Column(name = "RECORD_STATUS")
	public String getRecordStatus() {
		return this.recordStatus;
	}

	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}

	@Column(name = "CREATE_USER")
	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATE_DTM", length = 23)
	public Date getCreateDtm() {
		return this.createDtm;
	}

	public void setCreateDtm(Date createDtm) {
		this.createDtm = createDtm;
	}

	@Column(name = "LAST_UPD_USER")
	public String getLastUpdUser() {
		return this.lastUpdUser;
	}

	public void setLastUpdUser(String lastUpdUser) {
		this.lastUpdUser = lastUpdUser;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPD_DTM", length = 23)
	public Date getLastUpdDtm() {
		return this.lastUpdDtm;
	}

	public void setLastUpdDtm(Date lastUpdDtm) {
		this.lastUpdDtm = lastUpdDtm;
	}

}
