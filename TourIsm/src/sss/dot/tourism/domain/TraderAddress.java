package sss.dot.tourism.domain;

// Generated Dec 19, 2014 3:10:09 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TraderAddress generated by hbm2java
 */
@Entity
@Table(name = "TRADER_ADDRESS")
public class TraderAddress implements java.io.Serializable {

	private long addressId;
	private MasAmphur masAmphur;
	private MasProvince masProvince;
	private Trader trader;
	private MasTambol masTambol;
	private Person person;
	private String addressType;
	private String addressNo;
	private String buildingName;
	private String buildingNameEn;
	private String floor;
	private String roomNo;
	private String moo;
	private String villageName;
	private String villageNameEn;
	private String soi;
	private String soiEn;
	private String roadName;
	private String roadNameEn;
	private String postCode;
	private String telephone;
	private String mobileNo;
	private String fax;
	private String email;
	private String website;
	private String recordStatus;
	private String createUser;
	private Date createDtm;
	private String lastUpdUser;
	private Date lastUpdDtm;

	public TraderAddress() {
	}

	public TraderAddress(long addressId, Trader trader) {
		this.addressId = addressId;
		this.trader = trader;
	}

	public TraderAddress(long addressId, MasAmphur masAmphur,
			MasProvince masProvince, Trader trader, MasTambol masTambol,
			Person person, String addressType, String addressNo,
			String buildingName, String buildingNameEn, String floor,
			String roomNo, String moo, String villageName,
			String villageNameEn, String soi, String soiEn, String roadName,
			String roadNameEn, String postCode, String telephone,
			String mobileNo, String fax, String email, String website,
			String recordStatus, String createUser, Date createDtm,
			String lastUpdUser, Date lastUpdDtm) {
		this.addressId = addressId;
		this.masAmphur = masAmphur;
		this.masProvince = masProvince;
		this.trader = trader;
		this.masTambol = masTambol;
		this.person = person;
		this.addressType = addressType;
		this.addressNo = addressNo;
		this.buildingName = buildingName;
		this.buildingNameEn = buildingNameEn;
		this.floor = floor;
		this.roomNo = roomNo;
		this.moo = moo;
		this.villageName = villageName;
		this.villageNameEn = villageNameEn;
		this.soi = soi;
		this.soiEn = soiEn;
		this.roadName = roadName;
		this.roadNameEn = roadNameEn;
		this.postCode = postCode;
		this.telephone = telephone;
		this.mobileNo = mobileNo;
		this.fax = fax;
		this.email = email;
		this.website = website;
		this.recordStatus = recordStatus;
		this.createUser = createUser;
		this.createDtm = createDtm;
		this.lastUpdUser = lastUpdUser;
		this.lastUpdDtm = lastUpdDtm;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ADDRESS_ID", unique = true, nullable = false)
	public long getAddressId() {
		return this.addressId;
	}

	public void setAddressId(long addressId) {
		this.addressId = addressId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "AMPHUR_ID")
	public MasAmphur getMasAmphur() {
		return this.masAmphur;
	}

	public void setMasAmphur(MasAmphur masAmphur) {
		this.masAmphur = masAmphur;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PROVINCE_ID")
	public MasProvince getMasProvince() {
		return this.masProvince;
	}

	public void setMasProvince(MasProvince masProvince) {
		this.masProvince = masProvince;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TRADER_ID", nullable = false)
	public Trader getTrader() {
		return this.trader;
	}

	public void setTrader(Trader trader) {
		this.trader = trader;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TAMBOL_ID")
	public MasTambol getMasTambol() {
		return this.masTambol;
	}

	public void setMasTambol(MasTambol masTambol) {
		this.masTambol = masTambol;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PERSON_ID")
	public Person getPerson() {
		return this.person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@Column(name = "ADDRESS_TYPE")
	public String getAddressType() {
		return this.addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	@Column(name = "ADDRESS_NO")
	public String getAddressNo() {
		return this.addressNo;
	}

	public void setAddressNo(String addressNo) {
		this.addressNo = addressNo;
	}

	@Column(name = "BUILDING_NAME")
	public String getBuildingName() {
		return this.buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	@Column(name = "BUILDING_NAME_EN")
	public String getBuildingNameEn() {
		return this.buildingNameEn;
	}

	public void setBuildingNameEn(String buildingNameEn) {
		this.buildingNameEn = buildingNameEn;
	}

	@Column(name = "FLOOR")
	public String getFloor() {
		return this.floor;
	}

	public void setFloor(String floor) {
		this.floor = floor;
	}

	@Column(name = "ROOM_NO")
	public String getRoomNo() {
		return this.roomNo;
	}

	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}

	@Column(name = "MOO")
	public String getMoo() {
		return this.moo;
	}

	public void setMoo(String moo) {
		this.moo = moo;
	}

	@Column(name = "VILLAGE_NAME")
	public String getVillageName() {
		return this.villageName;
	}

	public void setVillageName(String villageName) {
		this.villageName = villageName;
	}

	@Column(name = "VILLAGE_NAME_EN")
	public String getVillageNameEn() {
		return this.villageNameEn;
	}

	public void setVillageNameEn(String villageNameEn) {
		this.villageNameEn = villageNameEn;
	}

	@Column(name = "SOI")
	public String getSoi() {
		return this.soi;
	}

	public void setSoi(String soi) {
		this.soi = soi;
	}
	
	@Column(name = "SOI_EN")
	public String getSoiEn() {
		return this.soiEn;
	}

	public void setSoiEn(String soiEn) {
		this.soiEn = soiEn;
	}

	@Column(name = "ROAD_NAME")
	public String getRoadName() {
		return this.roadName;
	}

	public void setRoadName(String roadName) {
		this.roadName = roadName;
	}

	@Column(name = "ROAD_NAME_EN")
	public String getRoadNameEn() {
		return this.roadNameEn;
	}

	public void setRoadNameEn(String roadNameEn) {
		this.roadNameEn = roadNameEn;
	}

	@Column(name = "POST_CODE")
	public String getPostCode() {
		return this.postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	@Column(name = "TELEPHONE")
	public String getTelephone() {
		return this.telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	@Column(name = "MOBILE_NO")
	public String getMobileNo() {
		return this.mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	@Column(name = "FAX")
	public String getFax() {
		return this.fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	@Column(name = "EMAIL")
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "WEBSITE")
	public String getWebsite() {
		return this.website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	@Column(name = "RECORD_STATUS")
	public String getRecordStatus() {
		return this.recordStatus;
	}

	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}

	@Column(name = "CREATE_USER")
	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATE_DTM", length = 23)
	public Date getCreateDtm() {
		return this.createDtm;
	}

	public void setCreateDtm(Date createDtm) {
		this.createDtm = createDtm;
	}

	@Column(name = "LAST_UPD_USER")
	public String getLastUpdUser() {
		return this.lastUpdUser;
	}

	public void setLastUpdUser(String lastUpdUser) {
		this.lastUpdUser = lastUpdUser;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPD_DTM", length = 23)
	public Date getLastUpdDtm() {
		return this.lastUpdDtm;
	}

	public void setLastUpdDtm(Date lastUpdDtm) {
		this.lastUpdDtm = lastUpdDtm;
	}

}
