package sss.dot.tourism.dto.complaint;


public class ComplaintDocDTO {
	
	private static final long serialVersionUID = 5537323899311753349L;
	private long complaintDocId;
	private long complaintLicenseId;
	private String complaintDocType;
	private String complaintDocName;
	private String complaintDocPath;
	
	private String havefile;
	
	private String traderType;
	private String licenseNo;
	private String questionType;
	
	public String getTraderType() {
		return traderType;
	}
	public void setTraderType(String traderType) {
		this.traderType = traderType;
	}
	public String getLicenseNo() {
		return licenseNo;
	}
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	public String getQuestionType() {
		return questionType;
	}
	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}
	public String getHavefile() {
		return havefile;
	}
	public void setHavefile(String havefile) {
		this.havefile = havefile;
	}
	public long getComplaintDocId() {
		return complaintDocId;
	}
	public void setComplaintDocId(long complaintDocId) {
		this.complaintDocId = complaintDocId;
	}
	public long getComplaintLicenseId() {
		return complaintLicenseId;
	}
	public void setComplaintLicenseId(long complaintLicenseId) {
		this.complaintLicenseId = complaintLicenseId;
	}
	public String getComplaintDocType() {
		return complaintDocType;
	}
	public void setComplaintDocType(String complaintDocType) {
		this.complaintDocType = complaintDocType;
	}
	public String getComplaintDocName() {
		return complaintDocName;
	}
	public void setComplaintDocName(String complaintDocName) {
		this.complaintDocName = complaintDocName;
	}
	public String getComplaintDocPath() {
		return complaintDocPath;
	}
	public void setComplaintDocPath(String complaintDocPath) {
		this.complaintDocPath = complaintDocPath;
	}
	
	
}
