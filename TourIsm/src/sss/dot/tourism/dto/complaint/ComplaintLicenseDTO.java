package sss.dot.tourism.dto.complaint;


public class ComplaintLicenseDTO 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5537323899311753349L;
//	private long complaintId;
	private long complaintLicenseId;
	
	private long masComplaintTypeId;
	private String questionType;
	private String complaintDesc;
	private String complaintCardId;
	private String complaintName;
	private String complaintTel;
	private String complaintEmail;
	private String complaintNo;
	private String complaintDate;
	private String complaintStatus;
	private String authorityComment;
	private String complaintProgress;
	private long userId;
	
	private String officerAsOwner;
	
	private String traderType;
	private String licenseNo;
	
	private String complaintStatusName;
	
	private long orgId;
	
	private String traderName;
	
	private boolean viewData;
	
	private String complaintTypeName;
	private String userFullName;
	private String traderTypeName;
	
	private String authorityDate;
	
	private String complaintDateFrom;
	private String complaintDateTo;
	
	private int page;
	private int start;
	private int limit;

	private long traderId;
	
	public String getOfficerAsOwner() {
		return officerAsOwner;
	}
	public void setOfficerAsOwner(String officerAsOwner) {
		this.officerAsOwner = officerAsOwner;
	}
	public String getComplaintDateFrom() {
		return complaintDateFrom;
	}
	public void setComplaintDateFrom(String complaintDateFrom) {
		this.complaintDateFrom = complaintDateFrom;
	}
	public String getComplaintDateTo() {
		return complaintDateTo;
	}
	public void setComplaintDateTo(String complaintDateTo) {
		this.complaintDateTo = complaintDateTo;
	}
	public boolean isViewData() {
		return viewData;
	}
	public void setViewData(boolean viewData) {
		this.viewData = viewData;
	}
	public String getAuthorityDate() {
		return authorityDate;
	}
	public void setAuthorityDate(String authorityDate) {
		this.authorityDate = authorityDate;
	}
	public String getTraderTypeName() {
		return traderTypeName;
	}
	public void setTraderTypeName(String traderTypeName) {
		this.traderTypeName = traderTypeName;
	}
	public String getUserFullName() {
		return userFullName;
	}
	public void setUserFullName(String userFullName) {
		this.userFullName = userFullName;
	}
	public String getComplaintTypeName() {
		return complaintTypeName;
	}
	public void setComplaintTypeName(String complaintTypeName) {
		this.complaintTypeName = complaintTypeName;
	}

	public String getTraderName() {
		return traderName;
	}
	public void setTraderName(String traderName) {
		this.traderName = traderName;
	}
	public long getOrgId() {
		return orgId;
	}
	public void setOrgId(long orgId) {
		this.orgId = orgId;
	}
	public String getComplaintStatusName() {
		return complaintStatusName;
	}
	public void setComplaintStatusName(String complaintStatusName) {
		this.complaintStatusName = complaintStatusName;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public String getLicenseNo() {
		return licenseNo;
	}
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	public String getTraderType() {
		return traderType;
	}
	public void setTraderType(String traderType) {
		this.traderType = traderType;
	}
	public long getComplaintLicenseId() {
		return complaintLicenseId;
	}
	public void setComplaintLicenseId(long complaintLicenseId) {
		this.complaintLicenseId = complaintLicenseId;
	}
	public long getMasComplaintTypeId() {
		return masComplaintTypeId;
	}
	public void setMasComplaintTypeId(long masComplaintTypeId) {
		this.masComplaintTypeId = masComplaintTypeId;
	}
	public String getQuestionType() {
		return questionType;
	}
	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}
	public String getComplaintDesc() {
		return complaintDesc;
	}
	public void setComplaintDesc(String complaintDesc) {
		this.complaintDesc = complaintDesc;
	}
	public String getComplaintCardId() {
		return complaintCardId;
	}
	public void setComplaintCardId(String complaintCardId) {
		this.complaintCardId = complaintCardId;
	}
	public String getComplaintName() {
		return complaintName;
	}
	public void setComplaintName(String complaintName) {
		this.complaintName = complaintName;
	}
	public String getComplaintTel() {
		return complaintTel;
	}
	public void setComplaintTel(String complaintTel) {
		this.complaintTel = complaintTel;
	}
	public String getComplaintEmail() {
		return complaintEmail;
	}
	public void setComplaintEmail(String complaintEmail) {
		this.complaintEmail = complaintEmail;
	}
	public String getComplaintNo() {
		return complaintNo;
	}
	public void setComplaintNo(String complaintNo) {
		this.complaintNo = complaintNo;
	}
	public String getComplaintDate() {
		return complaintDate;
	}
	public void setComplaintDate(String complaintDate) {
		this.complaintDate = complaintDate;
	}
	public String getComplaintStatus() {
		return complaintStatus;
	}
	public void setComplaintStatus(String complaintStatus) {
		this.complaintStatus = complaintStatus;
	}
	public String getAuthorityComment() {
		return authorityComment;
	}
	public void setAuthorityComment(String authorityComment) {
		this.authorityComment = authorityComment;
	}
	public String getComplaintProgress() {
		return complaintProgress;
	}
	public void setComplaintProgress(String complaintProgress) {
		this.complaintProgress = complaintProgress;
	}
	public long getTraderId() {
		return traderId;
	}
	public void setTraderId(long traderId) {
		this.traderId = traderId;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
