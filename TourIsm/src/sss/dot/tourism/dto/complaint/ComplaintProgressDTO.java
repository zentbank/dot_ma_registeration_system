package sss.dot.tourism.dto.complaint;


public class ComplaintProgressDTO {
	
	private long comProgressId;
	private long complaintLicenseId;
	private String progressRole;
	private String progressStatus;
	private String progressDesc;
	private String progressDate;
	private String authority;
	
	private String progressStatusName;
	
	private boolean edit;
	
	public boolean isEdit() {
		return edit;
	}
	public void setEdit(boolean edit) {
		this.edit = edit;
	}
	public String getProgressStatusName() {
		return progressStatusName;
	}
	public void setProgressStatusName(String progressStatusName) {
		this.progressStatusName = progressStatusName;
	}
	public long getComProgressId() {
		return comProgressId;
	}
	public void setComProgressId(long comProgressId) {
		this.comProgressId = comProgressId;
	}
	public long getComplaintLicenseId() {
		return complaintLicenseId;
	}
	public void setComplaintLicenseId(long complaintLicenseId) {
		this.complaintLicenseId = complaintLicenseId;
	}
	public String getProgressRole() {
		return progressRole;
	}
	public void setProgressRole(String progressRole) {
		this.progressRole = progressRole;
	}
	public String getProgressStatus() {
		return progressStatus;
	}
	public void setProgressStatus(String progressStatus) {
		this.progressStatus = progressStatus;
	}
	public String getProgressDesc() {
		return progressDesc;
	}
	public void setProgressDesc(String progressDesc) {
		this.progressDesc = progressDesc;
	}
	public String getProgressDate() {
		return progressDate;
	}
	public void setProgressDate(String progressDate) {
		this.progressDate = progressDate;
	}
	public String getAuthority() {
		return authority;
	}
	public void setAuthority(String authority) {
		this.authority = authority;
	}

	
	
}
