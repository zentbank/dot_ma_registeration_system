package sss.dot.tourism.dto.deactivate;

import java.io.Serializable;
import java.util.Date;

public class DeactivateLicenseDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2122732110712922175L;
	private long deactiveId;
	private String remark2;
	private String remark;
	private String deactivateResonRemark;
	private String approveStatus;
	private String approveStatusName;
	private String bookNo;
	private String bookDate;
	private String renewLicenseStatus;
	private String deactivateNo;
	private String deactivateDate;
	private String licenseNo;
	private String traderType;
	private String officerName;
	private long officerId;
	
	private String masAct;
	private long traderId;
	private String traderTypeName;
	private String identityNo;
	private String firstName;
	private String lastName;
	private String traderOwnerName;
	
	private String traderCategory;
	private String traderCategoryName;

	private String traderName;
	private String traderNameEn;
	
	
	private int page;
	private int start;
	private int limit;
	
	private long orgId;
	
	
	
	
	public long getOrgId() {
		return orgId;
	}
	public void setOrgId(long orgId) {
		this.orgId = orgId;
	}
	public String getOfficerName() {
		return officerName;
	}
	public void setOfficerName(String officerName) {
		this.officerName = officerName;
	}
	public long getOfficerId() {
		return officerId;
	}
	public void setOfficerId(long officerId) {
		this.officerId = officerId;
	}
	public String getApproveStatusName() {
		return approveStatusName;
	}
	public void setApproveStatusName(String approveStatusName) {
		this.approveStatusName = approveStatusName;
	}
	public long getDeactiveId() {
		return deactiveId;
	}
	public void setDeactiveId(long deactiveId) {
		this.deactiveId = deactiveId;
	}
	public String getRemark2() {
		return remark2;
	}
	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getDeactivateResonRemark() {
		return deactivateResonRemark;
	}
	public void setDeactivateResonRemark(String deactivateResonRemark) {
		this.deactivateResonRemark = deactivateResonRemark;
	}
	public String getApproveStatus() {
		return approveStatus;
	}
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	public String getBookNo() {
		return bookNo;
	}
	public void setBookNo(String bookNo) {
		this.bookNo = bookNo;
	}
	public String getBookDate() {
		return bookDate;
	}
	public void setBookDate(String bookDate) {
		this.bookDate = bookDate;
	}
	public String getRenewLicenseStatus() {
		return renewLicenseStatus;
	}
	public void setRenewLicenseStatus(String renewLicenseStatus) {
		this.renewLicenseStatus = renewLicenseStatus;
	}
	public String getDeactivateNo() {
		return deactivateNo;
	}
	public void setDeactivateNo(String deactivateNo) {
		this.deactivateNo = deactivateNo;
	}
	public String getDeactivateDate() {
		return deactivateDate;
	}
	public void setDeactivateDate(String deactivateDate) {
		this.deactivateDate = deactivateDate;
	}
	public String getLicenseNo() {
		return licenseNo;
	}
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	public String getTraderType() {
		return traderType;
	}
	public void setTraderType(String traderType) {
		this.traderType = traderType;
	}
	public String getMasAct() {
		return masAct;
	}
	public void setMasAct(String masAct) {
		this.masAct = masAct;
	}
	public long getTraderId() {
		return traderId;
	}
	public void setTraderId(long traderId) {
		this.traderId = traderId;
	}
	public String getTraderTypeName() {
		return traderTypeName;
	}
	public void setTraderTypeName(String traderTypeName) {
		this.traderTypeName = traderTypeName;
	}
	public String getIdentityNo() {
		return identityNo;
	}
	public void setIdentityNo(String identityNo) {
		this.identityNo = identityNo;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getTraderOwnerName() {
		return traderOwnerName;
	}
	public void setTraderOwnerName(String traderOwnerName) {
		this.traderOwnerName = traderOwnerName;
	}
	public String getTraderCategory() {
		return traderCategory;
	}
	public void setTraderCategory(String traderCategory) {
		this.traderCategory = traderCategory;
	}
	public String getTraderCategoryName() {
		return traderCategoryName;
	}
	public void setTraderCategoryName(String traderCategoryName) {
		this.traderCategoryName = traderCategoryName;
	}
	public String getTraderName() {
		return traderName;
	}
	public void setTraderName(String traderName) {
		this.traderName = traderName;
	}
	public String getTraderNameEn() {
		return traderNameEn;
	}
	public void setTraderNameEn(String traderNameEn) {
		this.traderNameEn = traderNameEn;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	
	
}
