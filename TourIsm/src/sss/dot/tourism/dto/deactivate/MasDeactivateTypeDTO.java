package sss.dot.tourism.dto.deactivate;

import java.io.Serializable;

public class MasDeactivateTypeDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3731060924502707350L;
	private long masDeactivateTypeId;
	private String licenseType;
	private String deactivateName;
	
	private long deactiveId;
	
	private long deactivateDtlId;
	
	private boolean active;
	
	
	
	

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public long getMasDeactivateTypeId() {
		return masDeactivateTypeId;
	}

	public void setMasDeactivateTypeId(long masDeactivateTypeId) {
		this.masDeactivateTypeId = masDeactivateTypeId;
	}

	public String getLicenseType() {
		return licenseType;
	}

	public void setLicenseType(String licenseType) {
		this.licenseType = licenseType;
	}

	public String getDeactivateName() {
		return deactivateName;
	}

	public void setDeactivateName(String deactivateName) {
		this.deactivateName = deactivateName;
	}

	public long getDeactiveId() {
		return deactiveId;
	}

	public void setDeactiveId(long deactiveId) {
		this.deactiveId = deactiveId;
	}

	public long getDeactivateDtlId() {
		return deactivateDtlId;
	}

	public void setDeactivateDtlId(long deactivateDtlId) {
		this.deactivateDtlId = deactivateDtlId;
	}
	
	
}
