package sss.dot.tourism.dto.guidedetail;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class GuideDetailDTO implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5537323899311753349L;
	
	private long guideId;
	private Integer prefixId;
	private String firstName;
	private String lastName;
	private String firstNameEn;
	private String lastNameEn;
	private String birthDate;
	private Integer ageYear;
	private String licenseNo;
	private String effectiveDate;
	private String expireDate;
	private String addressNo;
	private String villageName;
	private String moo;
	private String soi;
	private String roadName;
	private Long tambolId;
	private Long amphurId;
	private Long provinceId;
	private String postCode;
	private String telephone;
	private String mobileNo;
	private String fax;
	private String email;
	private String educationLevelName;
	private String educationLevelNameOth;
	private String amateurLevel;
	private String amateurLevelName;
	private String experienceYear;
	private String workType;
	private String workTypeName;
	private String salaryName;
	private String characterTypeName;
	private String characterTypeNameOth;
	private String clubName;
	private String clubNameTxt;
	private String recordStatus;
	private String registerDtm;
	
	private int page;
	private int start;
	private int limit;
	
	private long countryId;
	private String languageLevel;
	private String fullName;
	private String prefixName;
	
	
	
	public String getRegisterDtm() {
		return registerDtm;
	}
	public void setRegisterDtm(String registerDtm) {
		this.registerDtm = registerDtm;
	}
	public String getPrefixName() {
		return prefixName;
	}
	public void setPrefixName(String prefixName) {
		this.prefixName = prefixName;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getLanguageLevel() {
		return languageLevel;
	}
	public void setLanguageLevel(String languageLevel) {
		this.languageLevel = languageLevel;
	}
	public long getCountryId() {
		return countryId;
	}
	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getExpireDate() {
		return expireDate;
	}
	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public String getClubNameTxt() {
		return clubNameTxt;
	}
	public void setClubNameTxt(String clubNameTxt) {
		this.clubNameTxt = clubNameTxt;
	}
	public String getCharacterTypeNameOth() {
		return characterTypeNameOth;
	}
	public void setCharacterTypeNameOth(String characterTypeNameOth) {
		this.characterTypeNameOth = characterTypeNameOth;
	}
	public String getEducationLevelNameOth() {
		return educationLevelNameOth;
	}
	public void setEducationLevelNameOth(String educationLevelNameOth) {
		this.educationLevelNameOth = educationLevelNameOth;
	}
	public long getGuideId() {
		return guideId;
	}
	public void setGuideId(long guideId) {
		this.guideId = guideId;
	}
	public Integer getPrefixId() {
		return prefixId;
	}
	public void setPrefixId(Integer prefixId) {
		this.prefixId = prefixId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstNameEn() {
		return firstNameEn;
	}
	public void setFirstNameEn(String firstNameEn) {
		this.firstNameEn = firstNameEn;
	}
	public String getLastNameEn() {
		return lastNameEn;
	}
	public void setLastNameEn(String lastNameEn) {
		this.lastNameEn = lastNameEn;
	}
	public Integer getAgeYear() {
		return ageYear;
	}
	public void setAgeYear(Integer ageYear) {
		this.ageYear = ageYear;
	}
	public String getLicenseNo() {
		return licenseNo;
	}
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	public String getAddressNo() {
		return addressNo;
	}
	public void setAddressNo(String addressNo) {
		this.addressNo = addressNo;
	}
	public String getVillageName() {
		return villageName;
	}
	public void setVillageName(String villageName) {
		this.villageName = villageName;
	}
	public String getMoo() {
		return moo;
	}
	public void setMoo(String moo) {
		this.moo = moo;
	}
	public String getSoi() {
		return soi;
	}
	public void setSoi(String soi) {
		this.soi = soi;
	}
	public String getRoadName() {
		return roadName;
	}
	public void setRoadName(String roadName) {
		this.roadName = roadName;
	}
	public Long getTambolId() {
		return tambolId;
	}
	public void setTambolId(Long tambolId) {
		this.tambolId = tambolId;
	}
	public Long getAmphurId() {
		return amphurId;
	}
	public void setAmphurId(Long amphurId) {
		this.amphurId = amphurId;
	}
	public Long getProvinceId() {
		return provinceId;
	}
	public void setProvinceId(Long provinceId) {
		this.provinceId = provinceId;
	}
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEducationLevelName() {
		return educationLevelName;
	}
	public void setEducationLevelName(String educationLevelName) {
		this.educationLevelName = educationLevelName;
	}
	public String getAmateurLevel() {
		return amateurLevel;
	}
	public void setAmateurLevel(String amateurLevel) {
		this.amateurLevel = amateurLevel;
	}
	public String getAmateurLevelName() {
		return amateurLevelName;
	}
	public void setAmateurLevelName(String amateurLevelName) {
		this.amateurLevelName = amateurLevelName;
	}
	public String getExperienceYear() {
		return experienceYear;
	}
	public void setExperienceYear(String experienceYear) {
		this.experienceYear = experienceYear;
	}
	public String getWorkType() {
		return workType;
	}
	public void setWorkType(String workType) {
		this.workType = workType;
	}
	public String getWorkTypeName() {
		return workTypeName;
	}
	public void setWorkTypeName(String workTypeName) {
		this.workTypeName = workTypeName;
	}
	public String getSalaryName() {
		return salaryName;
	}
	public void setSalaryName(String salaryName) {
		this.salaryName = salaryName;
	}
	public String getCharacterTypeName() {
		return characterTypeName;
	}
	public void setCharacterTypeName(String characterTypeName) {
		this.characterTypeName = characterTypeName;
	}
	public String getClubName() {
		return clubName;
	}
	public void setClubName(String clubName) {
		this.clubName = clubName;
	}
	public String getRecordStatus() {
		return recordStatus;
	}
	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}

	
	

}
