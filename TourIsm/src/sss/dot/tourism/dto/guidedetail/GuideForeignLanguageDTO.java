package sss.dot.tourism.dto.guidedetail;

import java.io.Serializable;

import sss.dot.tourism.domain.Country;
import sss.dot.tourism.domain.GuideDetail;

public class GuideForeignLanguageDTO implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5537323899311753349L;
	
	private long foreignId;
	private long countryId;
	private long guideId;
	private String language;
	private String languageLevel;
	private String recordStatus;
	
	
	
	public long getCountryId() {
		return countryId;
	}
	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}
	public long getGuideId() {
		return guideId;
	}
	public void setGuideId(long guideId) {
		this.guideId = guideId;
	}
	public long getForeignId() {
		return foreignId;
	}
	public void setForeignId(long foreignId) {
		this.foreignId = foreignId;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getLanguageLevel() {
		return languageLevel;
	}
	public void setLanguageLevel(String languageLevel) {
		this.languageLevel = languageLevel;
	}
	public String getRecordStatus() {
		return recordStatus;
	}
	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}
	

	
}
