package sss.dot.tourism.dto.ktb;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class KtbPaymentDTO implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6104963394822082159L;
	private long ktbPayId;
	private String licenseNo;
	private String traderType;
	private String HSeqNo;
	private String HPostDate;
	private String DSeqNo;
	private String DTranDate;
	
	private String DTranDateFrom;
	private String DTranDateTo;
	
	private String DCustName;
	private String DReference1;
	private String DReference2;
	private BigDecimal DTranAmt;
	private String DReceiptNo;
	private String progressDate;
	private String recordStatus;
	private String recordStatusText;
	private String createUser;
	
	private String feeRegistrationType;
	private String personName;
	
	private String paymentNo;
	private String DReference3;
	
	private String orgName;
	
	
	
	public String getDTranDateFrom() {
		return DTranDateFrom;
	}
	public void setDTranDateFrom(String dTranDateFrom) {
		DTranDateFrom = dTranDateFrom;
	}
	public String getDTranDateTo() {
		return DTranDateTo;
	}
	public void setDTranDateTo(String dTranDateTo) {
		DTranDateTo = dTranDateTo;
	}
	public String getPaymentNo() {
		return paymentNo;
	}
	public void setPaymentNo(String paymentNo) {
		this.paymentNo = paymentNo;
	}
	public String getDReference3() {
		return DReference3;
	}
	public void setDReference3(String dReference3) {
		DReference3 = dReference3;
	}
	public String getRecordStatusText() {
		return recordStatusText;
	}
	public void setRecordStatusText(String recordStatusText) {
		this.recordStatusText = recordStatusText;
	}
	public String getFeeRegistrationType() {
		return feeRegistrationType;
	}
	public void setFeeRegistrationType(String feeRegistrationType) {
		this.feeRegistrationType = feeRegistrationType;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public String getRecordStatus() {
		return recordStatus;
	}
	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}
	public long getKtbPayId() {
		return ktbPayId;
	}
	public void setKtbPayId(long ktbPayId) {
		this.ktbPayId = ktbPayId;
	}
	public String getLicenseNo() {
		return licenseNo;
	}
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	public String getTraderType() {
		return traderType;
	}
	public void setTraderType(String traderType) {
		this.traderType = traderType;
	}
	public String getHSeqNo() {
		return HSeqNo;
	}
	public void setHSeqNo(String hSeqNo) {
		HSeqNo = hSeqNo;
	}
	public String getHPostDate() {
		return HPostDate;
	}
	public void setHPostDate(String hPostDate) {
		HPostDate = hPostDate;
	}
	public String getDSeqNo() {
		return DSeqNo;
	}
	public void setDSeqNo(String dSeqNo) {
		DSeqNo = dSeqNo;
	}
	public String getDTranDate() {
		return DTranDate;
	}
	public void setDTranDate(String dTranDate) {
		DTranDate = dTranDate;
	}
	public String getDCustName() {
		return DCustName;
	}
	public void setDCustName(String dCustName) {
		DCustName = dCustName;
	}
	public String getDReference1() {
		return DReference1;
	}
	public void setDReference1(String dReference1) {
		DReference1 = dReference1;
	}
	public String getDReference2() {
		return DReference2;
	}
	public void setDReference2(String dReference2) {
		DReference2 = dReference2;
	}
	public BigDecimal getDTranAmt() {
		return DTranAmt;
	}
	public void setDTranAmt(BigDecimal dTranAmt) {
		DTranAmt = dTranAmt;
	}
	public String getDReceiptNo() {
		return DReceiptNo;
	}
	public void setDReceiptNo(String dReceiptNo) {
		DReceiptNo = dReceiptNo;
	}
	public String getProgressDate() {
		return progressDate;
	}
	public void setProgressDate(String progressDate) {
		this.progressDate = progressDate;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	
	
	

}
