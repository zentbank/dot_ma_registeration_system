package sss.dot.tourism.dto.mas;

import java.io.Serializable;

public class ForeignLanguageDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2719597793080314370L;
	private long foreignId;
	private long countryId;
	private String countryName;
	private String countryNameEn;
	private String language;
	private String recordStatus;
	
	private long personId;
	private long traderId;
	
	
	
	public long getTraderId() {
		return traderId;
	}
	public void setTraderId(long traderId) {
		this.traderId = traderId;
	}
	public String getRecordStatus() {
		return recordStatus;
	}
	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}
	public long getPersonId() {
		return personId;
	}
	public void setPersonId(long personId) {
		this.personId = personId;
	}
	public long getCountryId() {
		return countryId;
	}
	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getCountryNameEn() {
		return countryNameEn;
	}
	public void setCountryNameEn(String countryNameEn) {
		this.countryNameEn = countryNameEn;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public long getForeignId() {
		return foreignId;
	}
	public void setForeignId(long foreignId) {
		this.foreignId = foreignId;
	}
	
	
}
