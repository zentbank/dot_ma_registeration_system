package sss.dot.tourism.dto.mas;

import java.io.Serializable;

public class MasAmphurDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -127764867828403702L;
	private long amphurId;
	private String amphurName;
	private String amphurNameEn;
	
	private long provinceId;
	private String provinceName;
	public long getAmphurId() {
		return amphurId;
	}
	public void setAmphurId(long amphurId) {
		this.amphurId = amphurId;
	}
	public String getAmphurName() {
		return amphurName;
	}
	public void setAmphurName(String amphurName) {
		this.amphurName = amphurName;
	}
	public long getProvinceId() {
		return provinceId;
	}
	public void setProvinceId(long provinceId) {
		this.provinceId = provinceId;
	}
	public String getProvinceName() {
		return provinceName;
	}
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}
	public String getAmphurNameEn() {
		return amphurNameEn;
	}
	public void setAmphurNameEn(String amphurNameEn) {
		this.amphurNameEn = amphurNameEn;
	}
	
	
}
