package sss.dot.tourism.dto.mas;

import java.io.Serializable;

public class MasBankDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long masBankId;
	private String bankName;
	
	public long getMasBankId() {
		return masBankId;
	}
	public void setMasBankId(long masBankId) {
		this.masBankId = masBankId;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

}
