package sss.dot.tourism.dto.mas;

import java.io.Serializable;

public class MasComplaintTypeDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8840241138721419328L;
	
	private long masComplaintTypeId;
	private String complaintTypeName;
	
	public long getMasComplaintTypeId() {
		return masComplaintTypeId;
	}
	public void setMasComplaintTypeId(long masComplaintTypeId) {
		this.masComplaintTypeId = masComplaintTypeId;
	}
	public String getComplaintTypeName() {
		return complaintTypeName;
	}
	public void setComplaintTypeName(String complaintTypeName) {
		this.complaintTypeName = complaintTypeName;
	}

}
