package sss.dot.tourism.dto.mas;

import java.io.Serializable;

public class MasDocumentDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8840241138721419328L;
	private long masDocId;
	private Integer docIndex;
	private String documentName;
	private String licenseType;
	private String registrationType;
	private String personType;
	private Integer branchType;
	private String recordStatus;
	
	private long traderId;
	private String branchTypeName;
	private long regDocId;
	private String docPath;
	
	private String docStatus;
	private String docRemark;
	
	public String getDocPath() {
		return docPath;
	}
	public void setDocPath(String docPath) {
		this.docPath = docPath;
	}
	public long getRegDocId() {
		return regDocId;
	}
	public void setRegDocId(long regDocId) {
		this.regDocId = regDocId;
	}
	private String havefile;
	
	public String getHavefile() {
		return havefile;
	}
	public void setHavefile(String havefile) {
		this.havefile = havefile;
	}
	public String getBranchTypeName() {
		return branchTypeName;
	}
	public void setBranchTypeName(String branchTypeName) {
		this.branchTypeName = branchTypeName;
	}
	public long getTraderId() {
		return traderId;
	}
	public void setTraderId(long traderId) {
		this.traderId = traderId;
	}
	public String getRecordStatus() {
		return recordStatus;
	}
	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}
	public long getMasDocId() {
		return masDocId;
	}
	public void setMasDocId(long masDocId) {
		this.masDocId = masDocId;
	}
	public Integer getDocIndex() {
		return docIndex;
	}
	public void setDocIndex(Integer docIndex) {
		this.docIndex = docIndex;
	}
	public String getDocumentName() {
		return documentName;
	}
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	public String getLicenseType() {
		return licenseType;
	}
	public void setLicenseType(String licenseType) {
		this.licenseType = licenseType;
	}
	public String getRegistrationType() {
		return registrationType;
	}
	public void setRegistrationType(String registrationType) {
		this.registrationType = registrationType;
	}
	public String getPersonType() {
		return personType;
	}
	public void setPersonType(String personType) {
		this.personType = personType;
	}
	public Integer getBranchType() {
		return branchType;
	}
	public void setBranchType(Integer branchType) {
		this.branchType = branchType;
	}
	public String getDocStatus() {
		return docStatus;
	}
	public void setDocStatus(String docStatus) {
		this.docStatus = docStatus;
	}
	public String getDocRemark() {
		return docRemark;
	}
	public void setDocRemark(String docRemark) {
		this.docRemark = docRemark;
	}
	
	
}
