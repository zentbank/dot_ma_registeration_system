package sss.dot.tourism.dto.mas;

import java.io.Serializable;

public class MasEducationLevelDTO implements Serializable{
	private static final long serialVersionUID = 8840241138721419328L;
	private long masEducationLevelId;
	private String educationLevelName;
	
	public long getMasEducationLevelId() {
		return masEducationLevelId;
	}
	public void setMasEducationLevelId(long masEducationLevelId) {
		this.masEducationLevelId = masEducationLevelId;
	}
	public String getEducationLevelName() {
		return educationLevelName;
	}
	public void setEducationLevelName(String educationLevelName) {
		this.educationLevelName = educationLevelName;
	}
}
