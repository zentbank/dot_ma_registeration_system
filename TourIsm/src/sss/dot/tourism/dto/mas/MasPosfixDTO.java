package sss.dot.tourism.dto.mas;

import java.io.Serializable;

public class MasPosfixDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8822752039052256395L;
	private long postfixId;
	private String postfixName;
	private String postfixNameEN;
	
	
	
	public String getPostfixNameEN() {
		return postfixNameEN;
	}
	public void setPostfixNameEN(String postfixNameEN) {
		this.postfixNameEN = postfixNameEN;
	}
	public long getPostfixId() {
		return postfixId;
	}
	public void setPostfixId(long postfixId) {
		this.postfixId = postfixId;
	}
	public String getPostfixName() {
		return postfixName;
	}
	public void setPostfixName(String postfixName) {
		this.postfixName = postfixName;
	}
	
	
}
