package sss.dot.tourism.dto.mas;

import java.io.Serializable;

public class MasPositionDTO implements Serializable{
	private long posId;
	private String posName;
	public long getPosId() {
		return posId;
	}
	public void setPosId(long posId) {
		this.posId = posId;
	}
	public String getPosName() {
		return posName;
	}
	public void setPosName(String posName) {
		this.posName = posName;
	}
	
	
}
