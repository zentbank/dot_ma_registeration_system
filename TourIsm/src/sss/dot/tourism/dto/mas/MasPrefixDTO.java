package sss.dot.tourism.dto.mas;

import java.io.Serializable;

public class MasPrefixDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4111376921466286722L;
	private long prefixId;
	private String prefixName;
	private String prefixNameEn;
	private String prefixType;
	
	
	public String getPrefixType() {
		return prefixType;
	}
	public void setPrefixType(String prefixType) {
		this.prefixType = prefixType;
	}
	public String getPrefixNameEn() {
		return prefixNameEn;
	}
	public void setPrefixNameEn(String prefixNameEn) {
		this.prefixNameEn = prefixNameEn;
	}
	public long getPrefixId() {
		return prefixId;
	}
	public void setPrefixId(long prefixId) {
		this.prefixId = prefixId;
	}
	public String getPrefixName() {
		return prefixName;
	}
	public void setPrefixName(String prefixName) {
		this.prefixName = prefixName;
	}
	
	
}
