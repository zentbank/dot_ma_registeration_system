package sss.dot.tourism.dto.mas;

import java.io.Serializable;

public class MasTambolDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4494178864404517925L;
	private long tambolId;
	private String tambolName;
	private String tambolNameEn;
	
	private long amphurId;
	private String amphurName;
	public long getTambolId() {
		return tambolId;
	}
	public void setTambolId(long tambolId) {
		this.tambolId = tambolId;
	}
	public String getTambolName() {
		return tambolName;
	}
	public void setTambolName(String tambolName) {
		this.tambolName = tambolName;
	}
	public long getAmphurId() {
		return amphurId;
	}
	public void setAmphurId(long amphurId) {
		this.amphurId = amphurId;
	}
	public String getAmphurName() {
		return amphurName;
	}
	public void setAmphurName(String amphurName) {
		this.amphurName = amphurName;
	}
	public String getTambolNameEn() {
		return tambolNameEn;
	}
	public void setTambolNameEn(String tambolNameEn) {
		this.tambolNameEn = tambolNameEn;
	}
	
	
}
