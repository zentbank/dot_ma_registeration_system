package sss.dot.tourism.dto.mas;

import java.io.Serializable;

public class MasUniversityDTO implements Serializable{
	private static final long serialVersionUID = -4111376921466286722L;
	private long masUniversityId;
	private String universityName;
	
	public long getMasUniversityId() {
		return masUniversityId;
	}
	public void setMasUniversityId(long masUniversityId) {
		this.masUniversityId = masUniversityId;
	}
	public String getUniversityName() {
		return universityName;
	}
	public void setUniversityName(String universityName) {
		this.universityName = universityName;
	}

}
