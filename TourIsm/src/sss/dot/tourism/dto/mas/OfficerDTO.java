package sss.dot.tourism.dto.mas;

import java.io.Serializable;
import java.util.Date;

public class OfficerDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1324950144391113868L;
	private long officerId;
	private String officerName;
	private String officerType;
	private String position;
	
	private String groupRole;
	private Integer officerGroup;
	
	
		
	public String getGroupRole() {
		return groupRole;
	}
	public void setGroupRole(String groupRole) {
		this.groupRole = groupRole;
	}
	public Integer getOfficerGroup() {
		return officerGroup;
	}
	public void setOfficerGroup(Integer officerGroup) {
		this.officerGroup = officerGroup;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getOfficerType() {
		return officerType;
	}
	public void setOfficerType(String officerType) {
		this.officerType = officerType;
	}
	public long getOfficerId() {
		return officerId;
	}
	public void setOfficerId(long officerId) {
		this.officerId = officerId;
	}
	public String getOfficerName() {
		return officerName;
	}
	public void setOfficerName(String officerName) {
		this.officerName = officerName;
	}
	
}
