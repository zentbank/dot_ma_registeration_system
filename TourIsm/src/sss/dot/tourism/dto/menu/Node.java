package sss.dot.tourism.dto.menu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Node implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4992043177222502459L;
	private long nodeId;
	private String text;

	private boolean expanded;
	private boolean leaf;
	private List<Node> children = new ArrayList<Node>();

	private String menuId;
	
	
	

	public Node() {
		this.leaf = false;
		this.expanded = false;
	}

	public long getNodeId() {
		return nodeId;
	}

	public void setNodeId(long nodeId) {
		this.nodeId = nodeId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean isExpanded() {
		return expanded;
	}

	public void setExpanded(boolean expanded) {
		this.expanded = expanded;
	}

	public boolean isLeaf() {
		return leaf;
	}

	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}

	public List<Node> getChildren() {
		return children;
	}

	public void setChildren(List<Node> children) {
		this.children = children;
	}

	public String getMenuId() {
		return menuId;
	}

	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}

	public void addChild(Node child) {
		this.getChildren().add(child);
	}

	public void addChildren(List<Node> children) {
		this.getChildren().addAll(children);
	}

	public void changeToLeaf() {
		this.leaf = true;
	}

	public boolean haveChildren() {
		return ((this.children != null) && (this.children.size() > 0));
	}
}
