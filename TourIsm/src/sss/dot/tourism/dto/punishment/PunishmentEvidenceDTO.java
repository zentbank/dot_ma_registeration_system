package sss.dot.tourism.dto.punishment;

import java.io.Serializable;

import org.springframework.web.multipart.MultipartFile;

import sss.dot.tourism.domain.RevokeLicense;
import sss.dot.tourism.domain.SuspensionLicense;

public class PunishmentEvidenceDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4357276786739601554L;

	private long punishmentId;

	private String punishmentDocName;
	private String punishmentDocPath;
	private String punishmentType;
	
	private long suspendId;
	private long revokeId;
	
	private String traderType;
	private String licenseNo;
	private String traderTypeName;
	
	MultipartFile evidence;
	
	
	
	
	
	public MultipartFile getEvidence() {
		return evidence;
	}
	public void setEvidence(MultipartFile evidence) {
		this.evidence = evidence;
	}
	public String getTraderType() {
		return traderType;
	}
	public void setTraderType(String traderType) {
		this.traderType = traderType;
	}
	public String getLicenseNo() {
		return licenseNo;
	}
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	public String getTraderTypeName() {
		return traderTypeName;
	}
	public void setTraderTypeName(String traderTypeName) {
		this.traderTypeName = traderTypeName;
	}
	public long getPunishmentId() {
		return punishmentId;
	}
	public void setPunishmentId(long punishmentId) {
		this.punishmentId = punishmentId;
	}
	public String getPunishmentDocName() {
		return punishmentDocName;
	}
	public void setPunishmentDocName(String punishmentDocName) {
		this.punishmentDocName = punishmentDocName;
	}
	public String getPunishmentDocPath() {
		return punishmentDocPath;
	}
	public void setPunishmentDocPath(String punishmentDocPath) {
		this.punishmentDocPath = punishmentDocPath;
	}
	public String getPunishmentType() {
		return punishmentType;
	}
	public void setPunishmentType(String punishmentType) {
		this.punishmentType = punishmentType;
	}
	public long getSuspendId() {
		return suspendId;
	}
	public void setSuspendId(long suspendId) {
		this.suspendId = suspendId;
	}
	public long getRevokeId() {
		return revokeId;
	}
	public void setRevokeId(long revokeId) {
		this.revokeId = revokeId;
	}
	
	
}
