package sss.dot.tourism.dto.punishment;

import java.io.Serializable;
import java.util.Date;

import sss.dot.tourism.domain.AdmUser;

public class RevokeLicenseDTO implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5776627389616401574L;
	private long revokeId;
	private String receiveNo;
	private String receiveDate;
	private String revokeStatus;
	private String revokeStatusName;
	private String revokeDate;
	private String cancelDate;
	private String cancelRemark;
	private String revokeDetail;
	private String officerName;
	private long officerId;
	private String masAct;
	private String officerAsOwner;
	
	private String suspensionNo;
	private String suspensionDate;
	
	
	private long traderId;
	private String traderType;
	private String licenseNo;
	private String traderTypeName;
	private String identityNo;
	private String firstName;
	private String lastName;
	private String traderOwnerName;
	
	private String traderCategory;
	private String traderCategoryName;

	private String traderName;
	private String traderNameEn;
	
	
	private int page;
	private int start;
	private int limit;
	
	private String personType;
	private String prefixName;
	private String postfixName;
	private String actDesc;
	private String email;
	
	
	public String getOfficerAsOwner() {
		return officerAsOwner;
	}



	public void setOfficerAsOwner(String officerAsOwner) {
		this.officerAsOwner = officerAsOwner;
	}



	public String getSuspensionNo() {
		return suspensionNo;
	}



	public void setSuspensionNo(String suspensionNo) {
		this.suspensionNo = suspensionNo;
	}



	public String getSuspensionDate() {
		return suspensionDate;
	}



	public void setSuspensionDate(String suspensionDate) {
		this.suspensionDate = suspensionDate;
	}



	public long getRevokeId() {
		return revokeId;
	}
	
	
	
	public String getRevokeStatusName() {
		return revokeStatusName;
	}



	public void setRevokeStatusName(String revokeStatusName) {
		this.revokeStatusName = revokeStatusName;
	}



	public void setRevokeId(long revokeId) {
		this.revokeId = revokeId;
	}
	public String getReceiveNo() {
		return receiveNo;
	}
	public void setReceiveNo(String receiveNo) {
		this.receiveNo = receiveNo;
	}
	public String getReceiveDate() {
		return receiveDate;
	}
	public void setReceiveDate(String receiveDate) {
		this.receiveDate = receiveDate;
	}
	public String getRevokeStatus() {
		return revokeStatus;
	}
	public void setRevokeStatus(String revokeStatus) {
		this.revokeStatus = revokeStatus;
	}
	public String getRevokeDate() {
		return revokeDate;
	}
	public void setRevokeDate(String revokeDate) {
		this.revokeDate = revokeDate;
	}
	public String getCancelDate() {
		return cancelDate;
	}
	public void setCancelDate(String cancelDate) {
		this.cancelDate = cancelDate;
	}
	public String getCancelRemark() {
		return cancelRemark;
	}
	public void setCancelRemark(String cancelRemark) {
		this.cancelRemark = cancelRemark;
	}
	public String getRevokeDetail() {
		return revokeDetail;
	}
	public void setRevokeDetail(String revokeDetail) {
		this.revokeDetail = revokeDetail;
	}
	public String getOfficerName() {
		return officerName;
	}
	public void setOfficerName(String officerName) {
		this.officerName = officerName;
	}
	public long getOfficerId() {
		return officerId;
	}
	public void setOfficerId(long officerId) {
		this.officerId = officerId;
	}
	public String getMasAct() {
		return masAct;
	}
	public void setMasAct(String masAct) {
		this.masAct = masAct;
	}
	public long getTraderId() {
		return traderId;
	}
	public void setTraderId(long traderId) {
		this.traderId = traderId;
	}
	public String getTraderType() {
		return traderType;
	}
	public void setTraderType(String traderType) {
		this.traderType = traderType;
	}
	public String getLicenseNo() {
		return licenseNo;
	}
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	public String getTraderTypeName() {
		return traderTypeName;
	}
	public void setTraderTypeName(String traderTypeName) {
		this.traderTypeName = traderTypeName;
	}
	public String getIdentityNo() {
		return identityNo;
	}
	public void setIdentityNo(String identityNo) {
		this.identityNo = identityNo;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getTraderOwnerName() {
		return traderOwnerName;
	}
	public void setTraderOwnerName(String traderOwnerName) {
		this.traderOwnerName = traderOwnerName;
	}
	public String getTraderCategory() {
		return traderCategory;
	}
	public void setTraderCategory(String traderCategory) {
		this.traderCategory = traderCategory;
	}
	public String getTraderCategoryName() {
		return traderCategoryName;
	}
	public void setTraderCategoryName(String traderCategoryName) {
		this.traderCategoryName = traderCategoryName;
	}
	public String getTraderName() {
		return traderName;
	}
	public void setTraderName(String traderName) {
		this.traderName = traderName;
	}
	public String getTraderNameEn() {
		return traderNameEn;
	}
	public void setTraderNameEn(String traderNameEn) {
		this.traderNameEn = traderNameEn;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}



	public String getPersonType() {
		return personType;
	}



	public void setPersonType(String personType) {
		this.personType = personType;
	}



	public String getPrefixName() {
		return prefixName;
	}



	public void setPrefixName(String prefixName) {
		this.prefixName = prefixName;
	}



	public String getPostfixName() {
		return postfixName;
	}



	public void setPostfixName(String postfixName) {
		this.postfixName = postfixName;
	}



	public String getActDesc() {
		return actDesc;
	}



	public void setActDesc(String actDesc) {
		this.actDesc = actDesc;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
