package sss.dot.tourism.dto.registration;

import java.io.Serializable;

public class BranchDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2253252364011724440L;

	//traderId
	private long branchId;
	
	private Integer traderBranchNo;
	private String branchType;
	
	/**
	 * sss.dot.tourism.domain.Trader
	 */
	//BRANCH_PARENT_ID
	private long branchParentId;
	

	
	private long addressId;


	private String addressType;
	private String branchNo;
	private String printLicense;
	private String addressNo;
	private String buildingName;
	private String buildingNameEn;
	private String villageName;
	private String villageNameEn;
	private String roadName;
	private String roadNameEn;
	private String floor;
	private String roomNo;
	private String moo;
	private String soi;
	private String postCode;
	private String telephone;
	private String mobileNo;
	private String fax;
	private String email;
	private String website;
	
	private String addressNoEn;
	private String buildingNoEn;
	private String floorEn;
	private String roomNoEn;
	private String mooEn;
	private String villageEn;
	private String soiEn;
	private String roadEn;
	private String postCodeEn;
	private String telephoneEn;
	private String mobileNoEn;
	private String faxEn;
	private String emailEn;
	private String websiteEn;
	private long amphurIdEn;
	private String amphurNameEn;
	private long provinceIdEn;
	private String provinceNameEn;
	private long tambolIdEn;
	private String tambolNameEn;
	
	private String traderAddress;
	
	/**
	 * sss.dot.tourism.domain.MasAmphur
	 */
	private long amphurId;
	private String amphurName;
	
	/**
	 * sss.dot.tourism.domain.MasProvince
	 */
	private long provinceId;
	private String provinceName;
	
	/**
	 * sss.dot.tourism.domain.MasTambol
	 */
	private long tambolId;
	private String tambolName;
	
	
	
	public String getTraderAddress() {
		return traderAddress;
	}
	public void setTraderAddress(String traderAddress) {
		this.traderAddress = traderAddress;
	}
	public long getBranchId() {
		return branchId;
	}
	public void setBranchId(long branchId) {
		this.branchId = branchId;
	}
	public Integer getTraderBranchNo() {
		return traderBranchNo;
	}
	public void setTraderBranchNo(Integer traderBranchNo) {
		this.traderBranchNo = traderBranchNo;
	}
	public String getBranchType() {
		return branchType;
	}
	public void setBranchType(String branchType) {
		this.branchType = branchType;
	}

	public long getBranchParentId() {
		return branchParentId;
	}
	public void setBranchParentId(long branchParentId) {
		this.branchParentId = branchParentId;
	}
	public long getAddressId() {
		return addressId;
	}
	public void setAddressId(long addressId) {
		this.addressId = addressId;
	}
	public String getAddressType() {
		return addressType;
	}
	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}
	public String getBranchNo() {
		return branchNo;
	}
	public void setBranchNo(String branchNo) {
		this.branchNo = branchNo;
	}
	public String getPrintLicense() {
		return printLicense;
	}
	public void setPrintLicense(String printLicense) {
		this.printLicense = printLicense;
	}
	public String getAddressNo() {
		return addressNo;
	}
	public void setAddressNo(String addressNo) {
		this.addressNo = addressNo;
	}
	public String getBuildingName() {
		return buildingName;
	}
	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}
	public String getBuildingNameEn() {
		return buildingNameEn;
	}
	public void setBuildingNameEn(String buildingNameEn) {
		this.buildingNameEn = buildingNameEn;
	}
	public String getVillageName() {
		return villageName;
	}
	public void setVillageName(String villageName) {
		this.villageName = villageName;
	}
	public String getVillageNameEn() {
		return villageNameEn;
	}
	public void setVillageNameEn(String villageNameEn) {
		this.villageNameEn = villageNameEn;
	}
	public String getRoadName() {
		return roadName;
	}
	public void setRoadName(String roadName) {
		this.roadName = roadName;
	}
	public String getRoadNameEn() {
		return roadNameEn;
	}
	public void setRoadNameEn(String roadNameEn) {
		this.roadNameEn = roadNameEn;
	}
	public String getFloor() {
		return floor;
	}
	public void setFloor(String floor) {
		this.floor = floor;
	}
	public String getRoomNo() {
		return roomNo;
	}
	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}
	public String getMoo() {
		return moo;
	}
	public void setMoo(String moo) {
		this.moo = moo;
	}
	public String getSoi() {
		return soi;
	}
	public void setSoi(String soi) {
		this.soi = soi;
	}
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getAddressNoEn() {
		return addressNoEn;
	}
	public void setAddressNoEn(String addressNoEn) {
		this.addressNoEn = addressNoEn;
	}
	public String getBuildingNoEn() {
		return buildingNoEn;
	}
	public void setBuildingNoEn(String buildingNoEn) {
		this.buildingNoEn = buildingNoEn;
	}
	public String getFloorEn() {
		return floorEn;
	}
	public void setFloorEn(String floorEn) {
		this.floorEn = floorEn;
	}
	public String getRoomNoEn() {
		return roomNoEn;
	}
	public void setRoomNoEn(String roomNoEn) {
		this.roomNoEn = roomNoEn;
	}
	public String getMooEn() {
		return mooEn;
	}
	public void setMooEn(String mooEn) {
		this.mooEn = mooEn;
	}
	public String getVillageEn() {
		return villageEn;
	}
	public void setVillageEn(String villageEn) {
		this.villageEn = villageEn;
	}
	public String getSoiEn() {
		return soiEn;
	}
	public void setSoiEn(String soiEn) {
		this.soiEn = soiEn;
	}
	public String getRoadEn() {
		return roadEn;
	}
	public void setRoadEn(String roadEn) {
		this.roadEn = roadEn;
	}
	public String getPostCodeEn() {
		return postCodeEn;
	}
	public void setPostCodeEn(String postCodeEn) {
		this.postCodeEn = postCodeEn;
	}
	public String getTelephoneEn() {
		return telephoneEn;
	}
	public void setTelephoneEn(String telephoneEn) {
		this.telephoneEn = telephoneEn;
	}
	public String getMobileNoEn() {
		return mobileNoEn;
	}
	public void setMobileNoEn(String mobileNoEn) {
		this.mobileNoEn = mobileNoEn;
	}
	public String getFaxEn() {
		return faxEn;
	}
	public void setFaxEn(String faxEn) {
		this.faxEn = faxEn;
	}
	public String getEmailEn() {
		return emailEn;
	}
	public void setEmailEn(String emailEn) {
		this.emailEn = emailEn;
	}
	public String getWebsiteEn() {
		return websiteEn;
	}
	public void setWebsiteEn(String websiteEn) {
		this.websiteEn = websiteEn;
	}
	public long getAmphurIdEn() {
		return amphurIdEn;
	}
	public void setAmphurIdEn(long amphurIdEn) {
		this.amphurIdEn = amphurIdEn;
	}
	public String getAmphurNameEn() {
		return amphurNameEn;
	}
	public void setAmphurNameEn(String amphurNameEn) {
		this.amphurNameEn = amphurNameEn;
	}
	public long getProvinceIdEn() {
		return provinceIdEn;
	}
	public void setProvinceIdEn(long provinceIdEn) {
		this.provinceIdEn = provinceIdEn;
	}
	public String getProvinceNameEn() {
		return provinceNameEn;
	}
	public void setProvinceNameEn(String provinceNameEn) {
		this.provinceNameEn = provinceNameEn;
	}
	public long getTambolIdEn() {
		return tambolIdEn;
	}
	public void setTambolIdEn(long tambolIdEn) {
		this.tambolIdEn = tambolIdEn;
	}
	public String getTambolNameEn() {
		return tambolNameEn;
	}
	public void setTambolNameEn(String tambolNameEn) {
		this.tambolNameEn = tambolNameEn;
	}
	public long getAmphurId() {
		return amphurId;
	}
	public void setAmphurId(long amphurId) {
		this.amphurId = amphurId;
	}
	public String getAmphurName() {
		return amphurName;
	}
	public void setAmphurName(String amphurName) {
		this.amphurName = amphurName;
	}
	public long getProvinceId() {
		return provinceId;
	}
	public void setProvinceId(long provinceId) {
		this.provinceId = provinceId;
	}
	public String getProvinceName() {
		return provinceName;
	}
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}
	public long getTambolId() {
		return tambolId;
	}
	public void setTambolId(long tambolId) {
		this.tambolId = tambolId;
	}
	public String getTambolName() {
		return tambolName;
	}
	public void setTambolName(String tambolName) {
		this.tambolName = tambolName;
	}
	
	
	
}
