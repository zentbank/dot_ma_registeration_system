package sss.dot.tourism.dto.registration;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import sss.dot.tourism.domain.MasBank;
import sss.dot.tourism.domain.Trader;

public class GuaranteeDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long guaranteeId;
	
	private String receiveDate;
	private String receiveName;
	private String refundName;
	private String refundType;
	private BigDecimal refundMny;
	private BigDecimal refundInterest;
	private String refundDate;
	private String refundRemark;
	private String guaranteeStatus;
	private String guaranteeStatusName;
	private String guaranteeRemark;
	private BigDecimal guaranteeMny;
	private BigDecimal guaranteeInterest;
	private String gauranteeRecordStatus;

	
	private long traderId;
	private String traderType;
	private String licenseNo;
	private String traderTypeName;
	private String identityNo;
	private String firstName;
	private String lastName;
	private String traderOwnerName;
	
	private String traderCategory;
	private String traderCategoryName;

	private String traderName;
	private String traderNameEn;
	private String licenseStatus;
	private String licenseStatusName;
	private String punishmentDate;
	private String punishmentExpireDate;
	
	private long orgId;
	
	
	private int page;
	private int start;
	private int limit;
	

	private long masBankByCashAccountBankId;
	private String masBankByCashAccountBankName;

	private long masBankByBankGuaranteeBankId;
	private String masBankByBankGuaranteeBankName;
	
	private long masBankByBankId;

	private String cashAccountId;
	private String cashAccountName;
	private String cashAccountBankBranchName;
	private BigDecimal cashAccountMny;
	private String bankGuaranteeId;
	private String bankGuaranteeName;
	private BigDecimal bankGuaranteeMny;
	private String governmentBondId;
	private String governmentBondExpireDate;
	private BigDecimal governmentBondMny;
	
	
	private String date;
	
	
	public String getMasBankByCashAccountBankName() {
		return masBankByCashAccountBankName;
	}


	public void setMasBankByCashAccountBankName(String masBankByCashAccountBankName) {
		this.masBankByCashAccountBankName = masBankByCashAccountBankName;
	}


	public String getMasBankByBankGuaranteeBankName() {
		return masBankByBankGuaranteeBankName;
	}


	public void setMasBankByBankGuaranteeBankName(
			String masBankByBankGuaranteeBankName) {
		this.masBankByBankGuaranteeBankName = masBankByBankGuaranteeBankName;
	}


	public long getMasBankByCashAccountBankId() {
		return masBankByCashAccountBankId;
	}


	public void setMasBankByCashAccountBankId(long masBankByCashAccountBankId) {
		this.masBankByCashAccountBankId = masBankByCashAccountBankId;
	}


	public long getMasBankByBankGuaranteeBankId() {
		return masBankByBankGuaranteeBankId;
	}


	public void setMasBankByBankGuaranteeBankId(long masBankByBankGuaranteeBankId) {
		this.masBankByBankGuaranteeBankId = masBankByBankGuaranteeBankId;
	}


	public long getMasBankByBankId() {
		return masBankByBankId;
	}


	public void setMasBankByBankId(long masBankByBankId) {
		this.masBankByBankId = masBankByBankId;
	}


	public String getCashAccountId() {
		return cashAccountId;
	}


	public void setCashAccountId(String cashAccountId) {
		this.cashAccountId = cashAccountId;
	}


	public String getCashAccountName() {
		return cashAccountName;
	}


	public void setCashAccountName(String cashAccountName) {
		this.cashAccountName = cashAccountName;
	}


	public String getCashAccountBankBranchName() {
		return cashAccountBankBranchName;
	}


	public void setCashAccountBankBranchName(String cashAccountBankBranchName) {
		this.cashAccountBankBranchName = cashAccountBankBranchName;
	}


	public BigDecimal getCashAccountMny() {
		return cashAccountMny;
	}


	public void setCashAccountMny(BigDecimal cashAccountMny) {
		this.cashAccountMny = cashAccountMny;
	}


	public String getBankGuaranteeId() {
		return bankGuaranteeId;
	}


	public void setBankGuaranteeId(String bankGuaranteeId) {
		this.bankGuaranteeId = bankGuaranteeId;
	}


	public String getBankGuaranteeName() {
		return bankGuaranteeName;
	}


	public void setBankGuaranteeName(String bankGuaranteeName) {
		this.bankGuaranteeName = bankGuaranteeName;
	}


	public BigDecimal getBankGuaranteeMny() {
		return bankGuaranteeMny;
	}


	public void setBankGuaranteeMny(BigDecimal bankGuaranteeMny) {
		this.bankGuaranteeMny = bankGuaranteeMny;
	}


	public String getGovernmentBondId() {
		return governmentBondId;
	}


	public void setGovernmentBondId(String governmentBondId) {
		this.governmentBondId = governmentBondId;
	}


	public String getGovernmentBondExpireDate() {
		return governmentBondExpireDate;
	}


	public void setGovernmentBondExpireDate(String governmentBondExpireDate) {
		this.governmentBondExpireDate = governmentBondExpireDate;
	}


	public BigDecimal getGovernmentBondMny() {
		return governmentBondMny;
	}


	public void setGovernmentBondMny(BigDecimal governmentBondMny) {
		this.governmentBondMny = governmentBondMny;
	}




	public String getGuaranteeStatusName() {
		return guaranteeStatusName;
	}


	public void setGuaranteeStatusName(String guaranteeStatusName) {
		this.guaranteeStatusName = guaranteeStatusName;
	}


	public long getOrgId() {
		return orgId;
	}


	public void setOrgId(long orgId) {
		this.orgId = orgId;
	}


	public String getLicenseStatus() {
		return licenseStatus;
	}


	public void setLicenseStatus(String licenseStatus) {
		this.licenseStatus = licenseStatus;
	}


	public String getLicenseStatusName() {
		return licenseStatusName;
	}


	public void setLicenseStatusName(String licenseStatusName) {
		this.licenseStatusName = licenseStatusName;
	}


	public String getTraderType() {
		return traderType;
	}


	public void setTraderType(String traderType) {
		this.traderType = traderType;
	}


	public String getLicenseNo() {
		return licenseNo;
	}


	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}


	public String getTraderTypeName() {
		return traderTypeName;
	}


	public void setTraderTypeName(String traderTypeName) {
		this.traderTypeName = traderTypeName;
	}


	public String getIdentityNo() {
		return identityNo;
	}


	public void setIdentityNo(String identityNo) {
		this.identityNo = identityNo;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getTraderOwnerName() {
		return traderOwnerName;
	}


	public void setTraderOwnerName(String traderOwnerName) {
		this.traderOwnerName = traderOwnerName;
	}


	public String getTraderCategory() {
		return traderCategory;
	}


	public void setTraderCategory(String traderCategory) {
		this.traderCategory = traderCategory;
	}


	public String getTraderCategoryName() {
		return traderCategoryName;
	}


	public void setTraderCategoryName(String traderCategoryName) {
		this.traderCategoryName = traderCategoryName;
	}


	public String getTraderName() {
		return traderName;
	}


	public void setTraderName(String traderName) {
		this.traderName = traderName;
	}


	public String getTraderNameEn() {
		return traderNameEn;
	}


	public void setTraderNameEn(String traderNameEn) {
		this.traderNameEn = traderNameEn;
	}


	public int getPage() {
		return page;
	}


	public void setPage(int page) {
		this.page = page;
	}


	public int getStart() {
		return start;
	}


	public void setStart(int start) {
		this.start = start;
	}


	public int getLimit() {
		return limit;
	}


	public void setLimit(int limit) {
		this.limit = limit;
	}


	
	public long getGuaranteeId() {
		return guaranteeId;
	}


	public void setGuaranteeId(long guaranteeId) {
		this.guaranteeId = guaranteeId;
	}




	public String getReceiveDate() {
		return receiveDate;
	}


	public void setReceiveDate(String receiveDate) {
		this.receiveDate = receiveDate;
	}


	public String getReceiveName() {
		return receiveName;
	}


	public void setReceiveName(String receiveName) {
		this.receiveName = receiveName;
	}


	public String getRefundName() {
		return refundName;
	}


	public void setRefundName(String refundName) {
		this.refundName = refundName;
	}


	public String getRefundType() {
		return refundType;
	}


	public void setRefundType(String refundType) {
		this.refundType = refundType;
	}


	public BigDecimal getRefundMny() {
		return refundMny;
	}


	public void setRefundMny(BigDecimal refundMny) {
		this.refundMny = refundMny;
	}


	public BigDecimal getRefundInterest() {
		return refundInterest;
	}


	public void setRefundInterest(BigDecimal refundInterest) {
		this.refundInterest = refundInterest;
	}


	public String getRefundDate() {
		return refundDate;
	}


	public void setRefundDate(String refundDate) {
		this.refundDate = refundDate;
	}


	public String getRefundRemark() {
		return refundRemark;
	}


	public void setRefundRemark(String refundRemark) {
		this.refundRemark = refundRemark;
	}


	public String getGuaranteeStatus() {
		return guaranteeStatus;
	}


	public void setGuaranteeStatus(String guaranteeStatus) {
		this.guaranteeStatus = guaranteeStatus;
	}


	public String getGuaranteeRemark() {
		return guaranteeRemark;
	}


	public void setGuaranteeRemark(String guaranteeRemark) {
		this.guaranteeRemark = guaranteeRemark;
	}


	public BigDecimal getGuaranteeMny() {
		return guaranteeMny;
	}


	public void setGuaranteeMny(BigDecimal guaranteeMny) {
		this.guaranteeMny = guaranteeMny;
	}


	public BigDecimal getGuaranteeInterest() {
		return guaranteeInterest;
	}


	public void setGuaranteeInterest(BigDecimal guaranteeInterest) {
		this.guaranteeInterest = guaranteeInterest;
	}





	public long getTraderId() {
		return traderId;
	}


	public void setTraderId(long traderId) {
		this.traderId = traderId;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
	public String getGauranteeRecordStatus() {
		return gauranteeRecordStatus;
	}


	public void setGauranteeRecordStatus(String gauranteeRecordStatus) {
		this.gauranteeRecordStatus = gauranteeRecordStatus;
	}


	public String getPunishmentDate() {
		return punishmentDate;
	}


	public void setPunishmentDate(String punishmentDate) {
		this.punishmentDate = punishmentDate;
	}


	public String getPunishmentExpireDate() {
		return punishmentExpireDate;
	}


	public void setPunishmentExpireDate(String punishmentExpireDate) {
		this.punishmentExpireDate = punishmentExpireDate;
	}


	public String getDate() {
		return date;
	}


	public void setDate(String date) {
		this.date = date;
	}

	
}
