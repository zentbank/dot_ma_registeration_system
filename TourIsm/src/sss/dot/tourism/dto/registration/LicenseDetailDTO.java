package sss.dot.tourism.dto.registration;

import java.io.Serializable;
import java.util.Date;

public class LicenseDetailDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1390543257020350287L;
	private long regId;
	private String registrationType;
	private String registrationTypeName;
	private String registrationProgress;
	private String registrationDate;
	private String registrationNo;
	private String approveDate;
	
	private long orgId;
	private String orgName;
	
	private long traderId;
	private String traderType;
	private String traderTypeName;
	private String licenseNo;
	private String traderName;
	private String traderNameEn;
	private String pronunciationName;
	private String traderCategory;
	private String traderCategoryName;

	private String traderArea;
	private String licenseStatus;
	private String effectiveDate;
	private String expireDate;

	private String traderOwnerName;
	private String traderOwnerNameEn;
	
	private long personId;
	private String personType;
	private String identityNo;
	private String firstName;
	private String lastName;
	private String personFullName;
	private String positionName;
	
	private long prefixId;
	private String prefixName;
	private String prefixNameEn;
	private long amphurId;
	private String amphurName;

	private long provinceId;
	private String provinceName;

	private long postfixId;
	private String postfixName;
	
	private String passportNo;
	private String firstNameEn;
	private String lastNameEn;
	private String gender;
	private String genderName;
	private String personNationality;
	private String birthDate;
	private Integer ageYear;
	private String identityNoExpire;
	private String corporateType;
	private String taxIdentityNo;
	private String identityDate;
	private String committeeName1;
	private String committeeName2;
	private String committeeNameSign;
	
	private String traderAddress;
	
	private String officeAddress;
	private String shopAddress;
	
	private String actName;
	private String docDate;
	
	
	private String revokeDate;
	
	public String getRevokeDate() {
		return revokeDate;
	}
	public void setRevokeDate(String revokeDate) {
		this.revokeDate = revokeDate;
	}
	public String getDocDate() {
		return docDate;
	}
	public void setDocDate(String docDate) {
		this.docDate = docDate;
	}
	public String getPositionName() {
		return positionName;
	}
	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}
	public String getOfficeAddress() {
		return officeAddress;
	}
	public void setOfficeAddress(String officeAddress) {
		this.officeAddress = officeAddress;
	}
	public String getShopAddress() {
		return shopAddress;
	}
	public void setShopAddress(String shopAddress) {
		this.shopAddress = shopAddress;
	}
	public String getActName() {
		return actName;
	}
	public void setActName(String actName) {
		this.actName = actName;
	}
	public String getTraderAddress() {
		return traderAddress;
	}
	public void setTraderAddress(String traderAddress) {
		this.traderAddress = traderAddress;
	}
	public long getRegId() {
		return regId;
	}
	public void setRegId(long regId) {
		this.regId = regId;
	}
	public String getRegistrationType() {
		return registrationType;
	}
	public void setRegistrationType(String registrationType) {
		this.registrationType = registrationType;
	}
	public String getRegistrationTypeName() {
		return registrationTypeName;
	}
	public void setRegistrationTypeName(String registrationTypeName) {
		this.registrationTypeName = registrationTypeName;
	}
	public String getRegistrationProgress() {
		return registrationProgress;
	}
	public void setRegistrationProgress(String registrationProgress) {
		this.registrationProgress = registrationProgress;
	}
	public String getRegistrationDate() {
		return registrationDate;
	}
	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}
	public String getRegistrationNo() {
		return registrationNo;
	}
	public void setRegistrationNo(String registrationNo) {
		this.registrationNo = registrationNo;
	}
	public long getOrgId() {
		return orgId;
	}
	public void setOrgId(long orgId) {
		this.orgId = orgId;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public long getTraderId() {
		return traderId;
	}
	public void setTraderId(long traderId) {
		this.traderId = traderId;
	}
	public String getTraderType() {
		return traderType;
	}
	public void setTraderType(String traderType) {
		this.traderType = traderType;
	}
	public String getTraderTypeName() {
		return traderTypeName;
	}
	public void setTraderTypeName(String traderTypeName) {
		this.traderTypeName = traderTypeName;
	}
	public String getLicenseNo() {
		return licenseNo;
	}
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	public String getTraderName() {
		return traderName;
	}
	public void setTraderName(String traderName) {
		this.traderName = traderName;
	}
	public String getTraderNameEn() {
		return traderNameEn;
	}
	public void setTraderNameEn(String traderNameEn) {
		this.traderNameEn = traderNameEn;
	}
	public String getPronunciationName() {
		return pronunciationName;
	}
	public void setPronunciationName(String pronunciationName) {
		this.pronunciationName = pronunciationName;
	}
	public String getTraderCategory() {
		return traderCategory;
	}
	public void setTraderCategory(String traderCategory) {
		this.traderCategory = traderCategory;
	}
	public String getTraderCategoryName() {
		return traderCategoryName;
	}
	public void setTraderCategoryName(String traderCategoryName) {
		this.traderCategoryName = traderCategoryName;
	}
	public String getTraderArea() {
		return traderArea;
	}
	public void setTraderArea(String traderArea) {
		this.traderArea = traderArea;
	}
	public String getLicenseStatus() {
		return licenseStatus;
	}
	public void setLicenseStatus(String licenseStatus) {
		this.licenseStatus = licenseStatus;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getExpireDate() {
		return expireDate;
	}
	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}
	public String getTraderOwnerName() {
		return traderOwnerName;
	}
	public void setTraderOwnerName(String traderOwnerName) {
		this.traderOwnerName = traderOwnerName;
	}
	public String getTraderOwnerNameEn() {
		return traderOwnerNameEn;
	}
	public void setTraderOwnerNameEn(String traderOwnerNameEn) {
		this.traderOwnerNameEn = traderOwnerNameEn;
	}
	public long getPersonId() {
		return personId;
	}
	public void setPersonId(long personId) {
		this.personId = personId;
	}
	public String getPersonType() {
		return personType;
	}
	public void setPersonType(String personType) {
		this.personType = personType;
	}
	public String getIdentityNo() {
		return identityNo;
	}
	public void setIdentityNo(String identityNo) {
		this.identityNo = identityNo;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPersonFullName() {
		return personFullName;
	}
	public void setPersonFullName(String personFullName) {
		this.personFullName = personFullName;
	}
	public long getPrefixId() {
		return prefixId;
	}
	public void setPrefixId(long prefixId) {
		this.prefixId = prefixId;
	}
	public String getPrefixName() {
		return prefixName;
	}
	public void setPrefixName(String prefixName) {
		this.prefixName = prefixName;
	}
	public String getPrefixNameEn() {
		return prefixNameEn;
	}
	public void setPrefixNameEn(String prefixNameEn) {
		this.prefixNameEn = prefixNameEn;
	}
	public long getAmphurId() {
		return amphurId;
	}
	public void setAmphurId(long amphurId) {
		this.amphurId = amphurId;
	}
	public String getAmphurName() {
		return amphurName;
	}
	public void setAmphurName(String amphurName) {
		this.amphurName = amphurName;
	}
	public long getProvinceId() {
		return provinceId;
	}
	public void setProvinceId(long provinceId) {
		this.provinceId = provinceId;
	}
	public String getProvinceName() {
		return provinceName;
	}
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}
	public long getPostfixId() {
		return postfixId;
	}
	public void setPostfixId(long postfixId) {
		this.postfixId = postfixId;
	}
	public String getPostfixName() {
		return postfixName;
	}
	public void setPostfixName(String postfixName) {
		this.postfixName = postfixName;
	}
	public String getPassportNo() {
		return passportNo;
	}
	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}
	public String getFirstNameEn() {
		return firstNameEn;
	}
	public void setFirstNameEn(String firstNameEn) {
		this.firstNameEn = firstNameEn;
	}
	public String getLastNameEn() {
		return lastNameEn;
	}
	public void setLastNameEn(String lastNameEn) {
		this.lastNameEn = lastNameEn;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getGenderName() {
		return genderName;
	}
	public void setGenderName(String genderName) {
		this.genderName = genderName;
	}
	public String getPersonNationality() {
		return personNationality;
	}
	public void setPersonNationality(String personNationality) {
		this.personNationality = personNationality;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public Integer getAgeYear() {
		return ageYear;
	}
	public void setAgeYear(Integer ageYear) {
		this.ageYear = ageYear;
	}
	public String getIdentityNoExpire() {
		return identityNoExpire;
	}
	public void setIdentityNoExpire(String identityNoExpire) {
		this.identityNoExpire = identityNoExpire;
	}
	public String getCorporateType() {
		return corporateType;
	}
	public void setCorporateType(String corporateType) {
		this.corporateType = corporateType;
	}
	public String getTaxIdentityNo() {
		return taxIdentityNo;
	}
	public void setTaxIdentityNo(String taxIdentityNo) {
		this.taxIdentityNo = taxIdentityNo;
	}
	public String getIdentityDate() {
		return identityDate;
	}
	public void setIdentityDate(String identityDate) {
		this.identityDate = identityDate;
	}
	public String getCommitteeName1() {
		return committeeName1;
	}
	public void setCommitteeName1(String committeeName1) {
		this.committeeName1 = committeeName1;
	}
	public String getCommitteeName2() {
		return committeeName2;
	}
	public void setCommitteeName2(String committeeName2) {
		this.committeeName2 = committeeName2;
	}
	public String getCommitteeNameSign() {
		return committeeNameSign;
	}
	public void setCommitteeNameSign(String committeeNameSign) {
		this.committeeNameSign = committeeNameSign;
	}
	public String getApproveDate() {
		return approveDate;
	}
	public void setApproveDate(String approveDate) {
		this.approveDate = approveDate;
	}
	
	
	
}
