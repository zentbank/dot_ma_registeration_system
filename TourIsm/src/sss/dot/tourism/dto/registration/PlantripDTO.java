package sss.dot.tourism.dto.registration;

import java.io.Serializable;

import sss.dot.tourism.domain.Country;
import sss.dot.tourism.domain.Trader;

public class PlantripDTO implements Serializable{
	private long planId;
	private long traderId;
	private long countryId;
	private String countryName;
	private String countryNameEn;
	private String areaName;
	private Integer plantripPerYear;
	private String recordStatus;
	
	private int count;
	
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public long getPlanId() {
		return planId;
	}
	public void setPlanId(long planId) {
		this.planId = planId;
	}
	public long getTraderId() {
		return traderId;
	}
	public void setTraderId(long traderId) {
		this.traderId = traderId;
	}
	public long getCountryId() {
		return countryId;
	}
	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getCountryNameEn() {
		return countryNameEn;
	}
	public void setCountryNameEn(String countryNameEn) {
		this.countryNameEn = countryNameEn;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public Integer getPlantripPerYear() {
		return plantripPerYear;
	}
	public void setPlantripPerYear(Integer plantripPerYear) {
		this.plantripPerYear = plantripPerYear;
	}
	public String getRecordStatus() {
		return recordStatus;
	}
	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}
	
	
}
