package sss.dot.tourism.dto.registration;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import sss.dot.tourism.domain.MasFee;
import sss.dot.tourism.domain.Receipt;

public class ReceiptDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4505551388644433361L;
	private long receiptId;
	private String bookNo;
	private String receiptNo;
	private String receiptName;
	private String receiveOfficerName;
	private String receiveOfficerDate;
	private String authority;
	

	private String receiptStatus;
	private String receiptRemark;

	
	private long receiptDetailId;

	private Double feeMny;
	private Double feeMny2;
	private Double feeMnyAll;
	
	private long feeId;
	private String feeName;
	private String registrationType;
	private String traderType;
	
	private long regId;
	
	private String registrationNo;
	
	private String traderCategory;
	private String traderCategoryName;
	private String cardGeneral;
	private String cardPrivate;
	private String totalGeneral;
	private String totalPrivate;
	private String receiptDateTo;
	private String receiptDateFrom;
	private Double sumFeeMnyGeneral;
	private Double sumFeeMnyPrivate;
	private Double sumFeeMnyReGeneral;
	private Double sumFeeMnyRePrivate;
	
	private String day;
	private String month;
	private String year;
	private String No;
	
	private String receiptDetail;
	private String receiveOfficerDateFrom;
	private String receiveOfficerDateTo;
	
	//guarantee
	private long guaranteeId;

	private String guaranteeType;
	
	private String receiveDate;
	private String receiveName;
	private String refundName;
	private String refundType;
	private BigDecimal refundMny;
	private BigDecimal refundInterest;
	private String refundDate;
	private String refundRemark;
	private String guaranteeStatus;
	private String guaranteeStatusName;
	private String guaranteeRemark;
	private BigDecimal guaranteeMny;
	private BigDecimal guaranteeInterest;
	private String gauranteeRecordStatus;

	
	private long traderId;
	private String licenseNo;
	private String traderTypeName;
	private String identityNo;
	private String firstName;
	private String lastName;
	private String traderOwnerName;

	private String traderName;
	private String traderNameEn;
	private String licenseStatus;
	private String licenseStatusName;
	
	private String guaranteeCategoryName;
	
	private long orgId;
	

	private long masBankByCashAccountBankId;

	private long masBankByBankGuaranteeBankId;
	private long masBankByBankId;

	private String cashAccountId;
	private String cashAccountName;
	private String cashAccountBankBranchName;
	private BigDecimal cashAccountMny;
	private String bankGuaranteeId;
	private String bankGuaranteeName;
	private BigDecimal bankGuaranteeMny;
	private String governmentBondId;
	private String governmentBondExpireDate;
	private BigDecimal governmentBondMny;	
	
	
	//reportfeebusinessguide
	private Double fee1;
	private Double fee2;
	private Double fee3;
	private Double fee4;
	private Double fee5;
	private Double fee6;
	private Double fee7;
	private Double fee8;
	private Double fee9;
	private Double fee10;
	private Double fee11;
	private Double fee12;
	private Double fee13;
	
	private Double total;
	private Double totalAll;
	private Double total1;
	private Double total2;
	private Double total3;
	private Double total4;
	private Double total5;
	private Double total6;
	private Double total7;
	private Double total8;
	private Double total9;
	private Double total10;
	private Double total11;
	private Double total12;
	private Double total13;
	
	
	
	//sek Edit 
	private String office;
	private String address;
	private String telephone;
	private String position;
	private String position1;
	
	public String getGuaranteeType() {
		return guaranteeType;
	}
	public void setGuaranteeType(String guaranteeType) {
		this.guaranteeType = guaranteeType;
	}
	public Double getFeeMnyAll() {
		return feeMnyAll;
	}
	public void setFeeMnyAll(Double feeMnyAll) {
		this.feeMnyAll = feeMnyAll;
	}
	public Double getFeeMny2() {
		return feeMny2;
	}
	public void setFeeMny2(Double feeMny2) {
		this.feeMny2 = feeMny2;
	}
	public String getGovernmentBondExpireDate() {
		return governmentBondExpireDate;
	}
	public String getGuaranteeCategoryName() {
		return guaranteeCategoryName;
	}
	public void setGuaranteeCategoryName(String guaranteeCategoryName) {
		this.guaranteeCategoryName = guaranteeCategoryName;
	}
	public void setGovernmentBondExpireDate(String governmentBondExpireDate) {
		this.governmentBondExpireDate = governmentBondExpireDate;
	}
	public String getReceiptDetail() {
		return receiptDetail;
	}
	public void setReceiptDetail(String receiptDetail) {
		this.receiptDetail = receiptDetail;
	}
	public String getReceiptStatus() {
		return receiptStatus;
	}
	public void setReceiptStatus(String receiptStatus) {
		this.receiptStatus = receiptStatus;
	}
	public String getReceiptRemark() {
		return receiptRemark;
	}
	public void setReceiptRemark(String receiptRemark) {
		this.receiptRemark = receiptRemark;
	}
	public String getTraderCategory() {
		return traderCategory;
	}
	public void setTraderCategory(String traderCategory) {
		this.traderCategory = traderCategory;
	}
	public String getTraderCategoryName() {
		return traderCategoryName;
	}
	public void setTraderCategoryName(String traderCategoryName) {
		this.traderCategoryName = traderCategoryName;
	}
	public String getRegistrationNo() {
		return registrationNo;
	}
	public void setRegistrationNo(String registrationNo) {
		this.registrationNo = registrationNo;
	}
	public long getRegId() {
		return regId;
	}
	public void setRegId(long regId) {
		this.regId = regId;
	}
	public long getReceiptId() {
		return receiptId;
	}
	public void setReceiptId(long receiptId) {
		this.receiptId = receiptId;
	}
	public String getBookNo() {
		return bookNo;
	}
	public void setBookNo(String bookNo) {
		this.bookNo = bookNo;
	}
	public String getReceiptNo() {
		return receiptNo;
	}
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}
	public String getReceiptName() {
		return receiptName;
	}
	public void setReceiptName(String receiptName) {
		this.receiptName = receiptName;
	}
	public String getReceiveOfficerName() {
		return receiveOfficerName;
	}
	public void setReceiveOfficerName(String receiveOfficerName) {
		this.receiveOfficerName = receiveOfficerName;
	}
	public String getReceiveOfficerDate() {
		return receiveOfficerDate;
	}
	public void setReceiveOfficerDate(String receiveOfficerDate) {
		this.receiveOfficerDate = receiveOfficerDate;
	}
	public String getAuthority() {
		return authority;
	}
	public void setAuthority(String authority) {
		this.authority = authority;
	}
	public long getReceiptDetailId() {
		return receiptDetailId;
	}
	public void setReceiptDetailId(long receiptDetailId) {
		this.receiptDetailId = receiptDetailId;
	}
	public Double getFeeMny() {
		return feeMny;
	}
	public void setFeeMny(Double feeMny) {
		this.feeMny = feeMny;
	}
	public long getFeeId() {
		return feeId;
	}
	public void setFeeId(long feeId) {
		this.feeId = feeId;
	}
	public String getFeeName() {
		return feeName;
	}
	public void setFeeName(String feeName) {
		this.feeName = feeName;
	}
	public String getRegistrationType() {
		return registrationType;
	}
	public void setRegistrationType(String registrationType) {
		this.registrationType = registrationType;
	}
	public String getTraderType() {
		return traderType;
	}
	public void setTraderType(String traderType) {
		this.traderType = traderType;
	}
	public String getCardGeneral() {
		return cardGeneral;
	}
	public void setCardGeneral(String cardGeneral) {
		this.cardGeneral = cardGeneral;
	}
	public String getCardPrivate() {
		return cardPrivate;
	}
	public void setCardPrivate(String cardPrivate) {
		this.cardPrivate = cardPrivate;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getTotalGeneral() {
		return totalGeneral;
	}
	public void setTotalGeneral(String totalGeneral) {
		this.totalGeneral = totalGeneral;
	}
	public String getTotalPrivate() {
		return totalPrivate;
	}
	public void setTotalPrivate(String totalPrivate) {
		this.totalPrivate = totalPrivate;
	}
	public String getReceiptDateTo() {
		return receiptDateTo;
	}
	public void setReceiptDateTo(String receiptDateTo) {
		this.receiptDateTo = receiptDateTo;
	}
	public String getReceiptDateFrom() {
		return receiptDateFrom;
	}
	public void setReceiptDateFrom(String receiptDateFrom) {
		this.receiptDateFrom = receiptDateFrom;
	}
	public Double getSumFeeMnyGeneral() {
		return sumFeeMnyGeneral;
	}
	public void setSumFeeMnyGeneral(Double sumFeeMnyGeneral) {
		this.sumFeeMnyGeneral = sumFeeMnyGeneral;
	}
	public Double getSumFeeMnyPrivate() {
		return sumFeeMnyPrivate;
	}
	public void setSumFeeMnyPrivate(Double sumFeeMnyPrivate) {
		this.sumFeeMnyPrivate = sumFeeMnyPrivate;
	}
	public Double getSumFeeMnyReGeneral() {
		return sumFeeMnyReGeneral;
	}
	public void setSumFeeMnyReGeneral(Double sumFeeMnyReGeneral) {
		this.sumFeeMnyReGeneral = sumFeeMnyReGeneral;
	}
	public Double getSumFeeMnyRePrivate() {
		return sumFeeMnyRePrivate;
	}
	public void setSumFeeMnyRePrivate(Double sumFeeMnyRePrivate) {
		this.sumFeeMnyRePrivate = sumFeeMnyRePrivate;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getNo() {
		return No;
	}
	public void setNo(String no) {
		No = no;
	}
	public long getGuaranteeId() {
		return guaranteeId;
	}
	public void setGuaranteeId(long guaranteeId) {
		this.guaranteeId = guaranteeId;
	}
	public String getReceiveDate() {
		return receiveDate;
	}
	public void setReceiveDate(String receiveDate) {
		this.receiveDate = receiveDate;
	}
	public String getReceiveName() {
		return receiveName;
	}
	public void setReceiveName(String receiveName) {
		this.receiveName = receiveName;
	}
	public String getRefundName() {
		return refundName;
	}
	public void setRefundName(String refundName) {
		this.refundName = refundName;
	}
	public String getRefundType() {
		return refundType;
	}
	public void setRefundType(String refundType) {
		this.refundType = refundType;
	}
	public BigDecimal getRefundMny() {
		return refundMny;
	}
	public void setRefundMny(BigDecimal refundMny) {
		this.refundMny = refundMny;
	}
	public BigDecimal getRefundInterest() {
		return refundInterest;
	}
	public void setRefundInterest(BigDecimal refundInterest) {
		this.refundInterest = refundInterest;
	}
	public String getRefundDate() {
		return refundDate;
	}
	public void setRefundDate(String refundDate) {
		this.refundDate = refundDate;
	}
	public String getRefundRemark() {
		return refundRemark;
	}
	public void setRefundRemark(String refundRemark) {
		this.refundRemark = refundRemark;
	}
	public String getGuaranteeStatus() {
		return guaranteeStatus;
	}
	public void setGuaranteeStatus(String guaranteeStatus) {
		this.guaranteeStatus = guaranteeStatus;
	}
	public String getGuaranteeStatusName() {
		return guaranteeStatusName;
	}
	public void setGuaranteeStatusName(String guaranteeStatusName) {
		this.guaranteeStatusName = guaranteeStatusName;
	}
	public String getGuaranteeRemark() {
		return guaranteeRemark;
	}
	public void setGuaranteeRemark(String guaranteeRemark) {
		this.guaranteeRemark = guaranteeRemark;
	}
	public BigDecimal getGuaranteeMny() {
		return guaranteeMny;
	}
	public void setGuaranteeMny(BigDecimal guaranteeMny) {
		this.guaranteeMny = guaranteeMny;
	}
	public BigDecimal getGuaranteeInterest() {
		return guaranteeInterest;
	}
	public void setGuaranteeInterest(BigDecimal guaranteeInterest) {
		this.guaranteeInterest = guaranteeInterest;
	}
	public String getGauranteeRecordStatus() {
		return gauranteeRecordStatus;
	}
	public void setGauranteeRecordStatus(String gauranteeRecordStatus) {
		this.gauranteeRecordStatus = gauranteeRecordStatus;
	}
	public long getTraderId() {
		return traderId;
	}
	public void setTraderId(long traderId) {
		this.traderId = traderId;
	}
	public String getLicenseNo() {
		return licenseNo;
	}
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	public String getTraderTypeName() {
		return traderTypeName;
	}
	public void setTraderTypeName(String traderTypeName) {
		this.traderTypeName = traderTypeName;
	}
	public String getIdentityNo() {
		return identityNo;
	}
	public void setIdentityNo(String identityNo) {
		this.identityNo = identityNo;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getTraderOwnerName() {
		return traderOwnerName;
	}
	public void setTraderOwnerName(String traderOwnerName) {
		this.traderOwnerName = traderOwnerName;
	}
	public String getTraderName() {
		return traderName;
	}
	public void setTraderName(String traderName) {
		this.traderName = traderName;
	}
	public String getTraderNameEn() {
		return traderNameEn;
	}
	public void setTraderNameEn(String traderNameEn) {
		this.traderNameEn = traderNameEn;
	}
	public String getLicenseStatus() {
		return licenseStatus;
	}
	public void setLicenseStatus(String licenseStatus) {
		this.licenseStatus = licenseStatus;
	}
	public String getLicenseStatusName() {
		return licenseStatusName;
	}
	public void setLicenseStatusName(String licenseStatusName) {
		this.licenseStatusName = licenseStatusName;
	}
	public long getOrgId() {
		return orgId;
	}
	public void setOrgId(long orgId) {
		this.orgId = orgId;
	}
	public long getMasBankByCashAccountBankId() {
		return masBankByCashAccountBankId;
	}
	public void setMasBankByCashAccountBankId(long masBankByCashAccountBankId) {
		this.masBankByCashAccountBankId = masBankByCashAccountBankId;
	}
	public long getMasBankByBankGuaranteeBankId() {
		return masBankByBankGuaranteeBankId;
	}
	public void setMasBankByBankGuaranteeBankId(long masBankByBankGuaranteeBankId) {
		this.masBankByBankGuaranteeBankId = masBankByBankGuaranteeBankId;
	}
	public long getMasBankByBankId() {
		return masBankByBankId;
	}
	public void setMasBankByBankId(long masBankByBankId) {
		this.masBankByBankId = masBankByBankId;
	}
	public String getCashAccountId() {
		return cashAccountId;
	}
	public void setCashAccountId(String cashAccountId) {
		this.cashAccountId = cashAccountId;
	}
	public String getCashAccountName() {
		return cashAccountName;
	}
	public void setCashAccountName(String cashAccountName) {
		this.cashAccountName = cashAccountName;
	}
	public String getCashAccountBankBranchName() {
		return cashAccountBankBranchName;
	}
	public void setCashAccountBankBranchName(String cashAccountBankBranchName) {
		this.cashAccountBankBranchName = cashAccountBankBranchName;
	}
	public BigDecimal getCashAccountMny() {
		return cashAccountMny;
	}
	public void setCashAccountMny(BigDecimal cashAccountMny) {
		this.cashAccountMny = cashAccountMny;
	}
	public String getBankGuaranteeId() {
		return bankGuaranteeId;
	}
	public void setBankGuaranteeId(String bankGuaranteeId) {
		this.bankGuaranteeId = bankGuaranteeId;
	}
	public String getBankGuaranteeName() {
		return bankGuaranteeName;
	}
	public void setBankGuaranteeName(String bankGuaranteeName) {
		this.bankGuaranteeName = bankGuaranteeName;
	}
	public BigDecimal getBankGuaranteeMny() {
		return bankGuaranteeMny;
	}
	public void setBankGuaranteeMny(BigDecimal bankGuaranteeMny) {
		this.bankGuaranteeMny = bankGuaranteeMny;
	}
	public String getGovernmentBondId() {
		return governmentBondId;
	}
	public void setGovernmentBondId(String governmentBondId) {
		this.governmentBondId = governmentBondId;
	}
	public BigDecimal getGovernmentBondMny() {
		return governmentBondMny;
	}
	public void setGovernmentBondMny(BigDecimal governmentBondMny) {
		this.governmentBondMny = governmentBondMny;
	}
	public String getReceiveOfficerDateFrom() {
		return receiveOfficerDateFrom;
	}
	public void setReceiveOfficerDateFrom(String receiveOfficerDateFrom) {
		this.receiveOfficerDateFrom = receiveOfficerDateFrom;
	}
	public String getReceiveOfficerDateTo() {
		return receiveOfficerDateTo;
	}
	public void setReceiveOfficerDateTo(String receiveOfficerDateTo) {
		this.receiveOfficerDateTo = receiveOfficerDateTo;
	}
	public Double getFee1() {
		return fee1;
	}
	public void setFee1(Double fee1) {
		this.fee1 = fee1;
	}
	public Double getFee2() {
		return fee2;
	}
	public void setFee2(Double fee2) {
		this.fee2 = fee2;
	}
	public Double getFee3() {
		return fee3;
	}
	public void setFee3(Double fee3) {
		this.fee3 = fee3;
	}
	public Double getFee4() {
		return fee4;
	}
	public void setFee4(Double fee4) {
		this.fee4 = fee4;
	}
	public Double getFee5() {
		return fee5;
	}
	public void setFee5(Double fee5) {
		this.fee5 = fee5;
	}
	public Double getFee6() {
		return fee6;
	}
	public void setFee6(Double fee6) {
		this.fee6 = fee6;
	}
	public Double getFee7() {
		return fee7;
	}
	public void setFee7(Double fee7) {
		this.fee7 = fee7;
	}
	public Double getFee8() {
		return fee8;
	}
	public void setFee8(Double fee8) {
		this.fee8 = fee8;
	}
	public Double getFee9() {
		return fee9;
	}
	public void setFee9(Double fee9) {
		this.fee9 = fee9;
	}
	public Double getFee10() {
		return fee10;
	}
	public void setFee10(Double fee10) {
		this.fee10 = fee10;
	}
	public Double getFee11() {
		return fee11;
	}
	public void setFee11(Double fee11) {
		this.fee11 = fee11;
	}
	public Double getFee12() {
		return fee12;
	}
	public void setFee12(Double fee12) {
		this.fee12 = fee12;
	}
	public Double getFee13() {
		return fee13;
	}
	public void setFee13(Double fee13) {
		this.fee13 = fee13;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public Double getTotalAll() {
		return totalAll;
	}
	public void setTotalAll(Double totalAll) {
		this.totalAll = totalAll;
	}
	public Double getTotal1() {
		return total1;
	}
	public void setTotal1(Double total1) {
		this.total1 = total1;
	}
	public Double getTotal2() {
		return total2;
	}
	public void setTotal2(Double total2) {
		this.total2 = total2;
	}
	public Double getTotal3() {
		return total3;
	}
	public void setTotal3(Double total3) {
		this.total3 = total3;
	}
	public Double getTotal4() {
		return total4;
	}
	public void setTotal4(Double total4) {
		this.total4 = total4;
	}
	public Double getTotal5() {
		return total5;
	}
	public void setTotal5(Double total5) {
		this.total5 = total5;
	}
	public Double getTotal6() {
		return total6;
	}
	public void setTotal6(Double total6) {
		this.total6 = total6;
	}
	public Double getTotal7() {
		return total7;
	}
	public void setTotal7(Double total7) {
		this.total7 = total7;
	}
	public Double getTotal8() {
		return total8;
	}
	public void setTotal8(Double total8) {
		this.total8 = total8;
	}
	public Double getTotal9() {
		return total9;
	}
	public void setTotal9(Double total9) {
		this.total9 = total9;
	}
	public Double getTotal10() {
		return total10;
	}
	public void setTotal10(Double total10) {
		this.total10 = total10;
	}
	public Double getTotal11() {
		return total11;
	}
	public void setTotal11(Double total11) {
		this.total11 = total11;
	}
	public Double getTotal12() {
		return total12;
	}
	public void setTotal12(Double total12) {
		this.total12 = total12;
	}
	public Double getTotal13() {
		return total13;
	}
	public void setTotal13(Double total13) {
		this.total13 = total13;
	}
	public String getOffice() {
		return office;
	}
	public void setOffice(String office) {
		this.office = office;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getPosition1() {
		return position1;
	}
	public void setPosition1(String position1) {
		this.position1 = position1;
	}
	
}
