package sss.dot.tourism.dto.registration;

import java.io.Serializable;


public class RegisterProgressDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8001344272230532695L;

	private long regProgId;
	
	private String progress;
	private String progressStatus;
	private String progressStatusName;
	private String progressStatusManager;
	
	private String progressDesc;
	private String progressDate;
	
	private long regId;
	
	private long userId;
	private String authorityName;
	
	private long orgId;
	private String orgName;
	
	
	public String getProgressStatusManager() {
		return progressStatusManager;
	}
	public void setProgressStatusManager(String progressStatusManager) {
		this.progressStatusManager = progressStatusManager;
	}
	public String getProgressStatusName() {
		return progressStatusName;
	}
	public void setProgressStatusName(String progressStatusName) {
		this.progressStatusName = progressStatusName;
	}
	public long getRegProgId() {
		return regProgId;
	}
	public void setRegProgId(long regProgId) {
		this.regProgId = regProgId;
	}
	public String getProgress() {
		return progress;
	}
	public void setProgress(String progress) {
		this.progress = progress;
	}
	public String getProgressStatus() {
		return progressStatus;
	}
	public void setProgressStatus(String progressStatus) {
		this.progressStatus = progressStatus;
	}
	public String getProgressDesc() {
		return progressDesc;
	}
	public void setProgressDesc(String progressDesc) {
		this.progressDesc = progressDesc;
	}
	public String getProgressDate() {
		return progressDate;
	}
	public void setProgressDate(String progressDate) {
		this.progressDate = progressDate;
	}
	public long getRegId() {
		return regId;
	}
	public void setRegId(long regId) {
		this.regId = regId;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getAuthorityName() {
		return authorityName;
	}
	public void setAuthorityName(String authorityName) {
		this.authorityName = authorityName;
	}
	public long getOrgId() {
		return orgId;
	}
	public void setOrgId(long orgId) {
		this.orgId = orgId;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
}
