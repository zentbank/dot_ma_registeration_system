package sss.dot.tourism.dto.registration;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import sss.dot.tourism.domain.DotTrFeeLicense;

public class RegistrationDTO implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5537323899311753349L;
	private long regId;
	private String registrationType;
	private String registrationTypeName;
	private String registrationProgress;
	private String registrationDate;
	private String registrationDateFrom;
	private String registrationDateTo;
	
	
	private String registrationNo;
	private String remark;
	private String regRecordStatus;
	private String roleAction;
	private String progressStatus;
	private String multipleJobs;
	private String progressBackStatus;
	
	private String roleColor;

	
	/**
	 * sss.dot.tourism.domain.Organization
	 */
	private long orgId;
	private String orgName;
	/**
	 * sss.dot.tourism.domain.Trader
	 */
	private long traderId;
	private String traderType;
	private String traderTypeName;
	private String licenseNo;
	private String licenseLeaderNo;
	private String traderName;
	private String traderNameEn;
	private String pronunciationName;
	private String traderCategory;
	private String traderCategoryName;
	private String traderCategoryGuideType;
	private String traderCategoryGuideTypeName;
	private String licenseGuideNo;
	private long traderGuideId;
	private long personGuideId;
	private long provinceGuideServiceId;
	private String provinceGuideServiceName;
	private String traderArea;
	private String licenseStatus;
	private String effectiveDate;
	private String expireDate;
	
	private String expireDateFrom;
	private String expireDateTo;
	private String expireDueDateFeeStatus;
	
	private String printLicense;
	private String printCard;
	private String bgImg;
	private String traderRecordStatus;
	private String traderOwnerName;
	private String traderOwnerNameEn;
	
	private String imgData;
	private String extension;
	
	private String traderGuideDetail;
	private String traderTrainedDetail;
	
	private Date punishmentDate;
	private Date punishmentExpireDate;
	
	/**
	 * sss.dot.tourism.domain.Person
	 */
	private long personId;
	private String personType;
	private String identityNo;
	private String firstName;
	private String lastName;
	private String personFullName;
	
	private long prefixId;
	private String prefixName;
	private String prefixNameEn;
	private long amphurId;
	private String amphurName;
	private long taxAmphurId;
	private String taxAmphurName;
	private long provinceId;
	private String provinceName;
	private long taxProvinceId;
	private String taxProvinceName;
	private long postfixId;
	private String postfixName;
	private String personRecordStatus;
	
	private String passportNo;
	private String firstNameEn;
	private String lastNameEn;
	private String gender;
	private String genderName;
	private String personNationality;
	private String birthDate;
	private Integer ageYear;
	private String identityNoExpire;
	private String corporateType;
	private String taxIdentityNo;
	private String identityDate;
	private String committeeName1;
	private String committeeName2;
	private String committeeNameSign;
	private String recordStatus;
	
	private String imageFile;
	
	//Sek Add รายงาน
	private String language;
	private BigDecimal sumGuide;
	private long countryId;
	private BigDecimal sumProvince;
	private BigDecimal sumStaticTourLeader;
	private String tourLeaderName;
	private String businessName;
	private BigDecimal sumChange;
	private BigDecimal sumBranch; 
	
	/**
	 * sss.dot.tourism.domain.Plantrip
	 */
	private long planId;
	private String areaName;
	private Integer plantripPerYear;
	private String planRecordStatus;
	
	//ผู้รับเรื่อง
	private String authorityName;
	
	private long prefixIdEn;
	private long prefixIdCorp;
	private String firstNameCorp;

	private String identityNoCorp;
	private String identityDateCorp;
	private long provinceIdCorp;
	
	private String personTypeCorporate;
	private String personTypeIndividual;
	
	private String traderAddress;
	private String address;
	private String mobileNo;
	private String telephone;
	
	//ผู้นำเที่ยวผ่านการอบรม
	private long eduId;
	private String graduationCourse;
	private Integer generationGraduate;
	private String universityName;
	private String studyDate;
	
	//เป็นมัคคุเทศก์และมาขอจดทะเบียนผู้นำเที่ยว
	
	//document
	private String approveDate;
	
	
	//Receipt
	private long receiptId;
	private String bookNo;
	private String receiptNo;
	private String receiptStatus;
	
	private int page;
	private int start;
	private int limit;
	
	private String regProgressStatus;
	private String regGroupProgress;
	
	private String licenseStatusName;

	private String headerInfo;

	

	//PrintLicense
	private String printLicenseStatus;
	private String approveDateFrom;
	private String approveDateTo;
	private Integer traderBranchNo;
	private String branchType;
	private long printLicenseId;
	private String printLicenseDate;
	private String detailAction;
	private String title;
	private String addressNo;
	private String soi;
	private String roadName;
	private String postCode;
	private String village;
	private Long tambolId;
	private String tambolName;
	private String today;
	private String day;
	private String month;
	private String year;
	private String todayForm;
	private String dayF;
	private String monthF;
	private String yearF;
	
	private String branchNo;
	private String branchAddressNo;
	private String branchSoi;
	private String branchRoadName;
	private String branchTambolName;	
	private String branchAmphurName;
	private String branchProvinceName;
	private String branchPostCode;
	
	private String managerBG;
	private String guideName;
	private String traderCategoryNameOth1;
	private String traderCategoryNameOth2;
	private String position;
	private String position1;
	private String position2;
	private String buildingName;
	private String floor;
	private String roomNo;
	private String moo;
	private String villageName;
	private long addressId;
	
	//printcard
	private long printCardId;
	private String printCardDate;
	private String printCardStatus;
	private String licenseNoTo;
	private String licenseNoFrom;
	private String imgAction;
	private String imgPath;
	private long personImgId;
	private String yearValid;
	private String monthValid;
	private String date;
	private String dateTo;
	private String dateFrom;
	private String email;
	private String check;
	private String committeePersonType;

	//chart
	private String templateFileName;
	private String chartName;
	
	private Double outBoundCount;
	private Double inBoundCount;
	private Double countryCount;
	private Double areaCount;
	private Double count;
	
	private Double oldCount;
	private Double newCount;
	private Double cancleCount;
	private Double revokeCount;
	
	private Double count1;
	private Double count2;
	private Double count3;
	private Double count4;
	private Double count5;
	private Double count6;
	private Double count7;
	private Double count8;
	private Double count9;
	private Double count10;
	private Double count11;
	private Double count12;
	private Double count13;
	private Double count14;
	private Double count15;
	private Double countAll1;
	private Double countAll2;
	
	private Double outBoundRevokeCount;
	private Double inBoundRevokeCount;
	private Double countryRevokeCount;
	private Double areaRevokeCount;
	private Double countRevoke;
	
	private Double countAll;
	
	private Double newGeneralCount;
	private Double newAreaCount;
	private Double cancleGeneralCount;
	private Double cancleAreaCount;
	
	
	
	//deactivate
	
	private long deactiveId;
	private String remark2;
	private String deactivateRemark;
	private String deactivateResonRemark;
	private String approveStatus;
	private String approveStatusName;
	private String deactivateBookNo;
	private String bookDate;
	private String renewLicenseStatus;
	private String deactivateNo;
	private String deactivateDate;
	private String officerName;
	private long officerId;
	private String deactivateTypeName;
	
	//refund guaranttee
	
	private long guaranteeId;
	
	private String receiveDate;
	private String receiveName;
	private String refundName;
	private String refundType;
	private BigDecimal refundMny;
	private BigDecimal refundInterest;
	private String refundDate;
	private String refundRemark;
	private String guaranteeStatus;
	private String guaranteeStatusName;
	private String guaranteeRemark;
	private BigDecimal guaranteeMny;
	private BigDecimal guaranteeInterest;
	private String gauranteeRecordStatus;
	private long masBankByCashAccountBankId;
	private String masBankByCashAccountBankName;

	private long masBankByBankGuaranteeBankId;
	private String masBankByBankGuaranteeBankName;
	
	private long masBankByBankId;

	private String cashAccountId;
	private String cashAccountName;
	private String cashAccountBankBranchName;
	private BigDecimal cashAccountMny;
	private String bankGuaranteeId;
	private String bankGuaranteeName;
	private BigDecimal bankGuaranteeMny;
	private String governmentBondId;
	private String governmentBondExpireDate;
	private BigDecimal governmentBondMny;
	
	private String countCountry;
	
	//print card with template
	
	private String  l_licenseNo;
    private String  l_traderOwnerName;
    private String  l_traderOwnerNameEn;
    private String  l_identityNo;
    private String  l_effectiveDate;
    private String  l_expireDate;
    private String  l_traderArea;
    private String l_director;
    
    private String applicationContextPath;
    
    private String feePaidStatus;
    private String feePaidStatusText;
    private BigDecimal diffFee;
    
    
    private BigDecimal feeMny;
    private BigDecimal totalFee;
    private BigDecimal totalFeePaid;
	private String paymentDate;
	private String paymentStatus;
	private String paymentStatusText;
	private long paymentRecId;
	private long billPaymentId;
	private String billPaymentNo;
	
	

	public String getL_director() {
		return l_director;
	}
	public void setL_director(String l_director) {
		this.l_director = l_director;
	}
	public long getPaymentRecId() {
		return paymentRecId;
	}
	public void setPaymentRecId(long paymentRecId) {
		this.paymentRecId = paymentRecId;
	}
	public long getBillPaymentId() {
		return billPaymentId;
	}
	public void setBillPaymentId(long billPaymentId) {
		this.billPaymentId = billPaymentId;
	}
	public String getBillPaymentNo() {
		return billPaymentNo;
	}
	public void setBillPaymentNo(String billPaymentNo) {
		this.billPaymentNo = billPaymentNo;
	}
	public BigDecimal getFeeMny() {
		return feeMny;
	}
	public void setFeeMny(BigDecimal feeMny) {
		this.feeMny = feeMny;
	}
	public String getPaymentStatusText() {
		return paymentStatusText;
	}
	public void setPaymentStatusText(String paymentStatusText) {
		this.paymentStatusText = paymentStatusText;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public String getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}
	public BigDecimal getTotalFee() {
		return totalFee;
	}
	public void setTotalFee(BigDecimal totalFee) {
		this.totalFee = totalFee;
	}
	public BigDecimal getTotalFeePaid() {
		return totalFeePaid;
	}
	public void setTotalFeePaid(BigDecimal totalFeePaid) {
		this.totalFeePaid = totalFeePaid;
	}
	public String getFeePaidStatus() {
		return feePaidStatus;
	}
	public void setFeePaidStatus(String feePaidStatus) {
		this.feePaidStatus = feePaidStatus;
	}
	public String getFeePaidStatusText() {
		return feePaidStatusText;
	}
	public void setFeePaidStatusText(String feePaidStatusText) {
		this.feePaidStatusText = feePaidStatusText;
	}
	public BigDecimal getDiffFee() {
		return diffFee;
	}
	public void setDiffFee(BigDecimal diffFee) {
		this.diffFee = diffFee;
	}

	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

	public String getApplicationContextPath() {
		return applicationContextPath;
	}
	public void setApplicationContextPath(String applicationContextPath) {
		this.applicationContextPath = applicationContextPath;
	}
	public String getImgData() {
		return imgData;
	}
	public void setImgData(String imgData) {
		this.imgData = imgData;
	}
	public String getExpireDueDateFeeStatus() {
		return expireDueDateFeeStatus;
	}
	public void setExpireDueDateFeeStatus(String expireDueDateFeeStatus) {
		this.expireDueDateFeeStatus = expireDueDateFeeStatus;
	}
	public String getExpireDateFrom() {
		return expireDateFrom;
	}
	public void setExpireDateFrom(String expireDateFrom) {
		this.expireDateFrom = expireDateFrom;
	}
	public String getExpireDateTo() {
		return expireDateTo;
	}
	public void setExpireDateTo(String expireDateTo) {
		this.expireDateTo = expireDateTo;
	}
	public String getCountCountry() {
		return countCountry;
	}
	public void setCountCountry(String countCountry) {
		this.countCountry = countCountry;
	}
	public long getMasBankByCashAccountBankId() {
		return masBankByCashAccountBankId;
	}
	public void setMasBankByCashAccountBankId(long masBankByCashAccountBankId) {
		this.masBankByCashAccountBankId = masBankByCashAccountBankId;
	}
	public String getMasBankByCashAccountBankName() {
		return masBankByCashAccountBankName;
	}
	public void setMasBankByCashAccountBankName(String masBankByCashAccountBankName) {
		this.masBankByCashAccountBankName = masBankByCashAccountBankName;
	}
	public long getMasBankByBankGuaranteeBankId() {
		return masBankByBankGuaranteeBankId;
	}
	public void setMasBankByBankGuaranteeBankId(long masBankByBankGuaranteeBankId) {
		this.masBankByBankGuaranteeBankId = masBankByBankGuaranteeBankId;
	}
	public String getMasBankByBankGuaranteeBankName() {
		return masBankByBankGuaranteeBankName;
	}
	public void setMasBankByBankGuaranteeBankName(
			String masBankByBankGuaranteeBankName) {
		this.masBankByBankGuaranteeBankName = masBankByBankGuaranteeBankName;
	}
	public long getMasBankByBankId() {
		return masBankByBankId;
	}
	public void setMasBankByBankId(long masBankByBankId) {
		this.masBankByBankId = masBankByBankId;
	}
	public String getCashAccountId() {
		return cashAccountId;
	}
	public void setCashAccountId(String cashAccountId) {
		this.cashAccountId = cashAccountId;
	}
	public String getCashAccountName() {
		return cashAccountName;
	}
	public void setCashAccountName(String cashAccountName) {
		this.cashAccountName = cashAccountName;
	}
	public String getCashAccountBankBranchName() {
		return cashAccountBankBranchName;
	}
	public void setCashAccountBankBranchName(String cashAccountBankBranchName) {
		this.cashAccountBankBranchName = cashAccountBankBranchName;
	}
	public BigDecimal getCashAccountMny() {
		return cashAccountMny;
	}
	public void setCashAccountMny(BigDecimal cashAccountMny) {
		this.cashAccountMny = cashAccountMny;
	}
	public String getBankGuaranteeId() {
		return bankGuaranteeId;
	}
	public void setBankGuaranteeId(String bankGuaranteeId) {
		this.bankGuaranteeId = bankGuaranteeId;
	}
	public String getBankGuaranteeName() {
		return bankGuaranteeName;
	}
	public void setBankGuaranteeName(String bankGuaranteeName) {
		this.bankGuaranteeName = bankGuaranteeName;
	}
	public BigDecimal getBankGuaranteeMny() {
		return bankGuaranteeMny;
	}
	public void setBankGuaranteeMny(BigDecimal bankGuaranteeMny) {
		this.bankGuaranteeMny = bankGuaranteeMny;
	}
	public String getGovernmentBondId() {
		return governmentBondId;
	}
	public void setGovernmentBondId(String governmentBondId) {
		this.governmentBondId = governmentBondId;
	}
	public String getGovernmentBondExpireDate() {
		return governmentBondExpireDate;
	}
	public void setGovernmentBondExpireDate(String governmentBondExpireDate) {
		this.governmentBondExpireDate = governmentBondExpireDate;
	}
	public BigDecimal getGovernmentBondMny() {
		return governmentBondMny;
	}
	public void setGovernmentBondMny(BigDecimal governmentBondMny) {
		this.governmentBondMny = governmentBondMny;
	}
	public String getRoleColor() {
		return roleColor;
	}
	public void setRoleColor(String roleColor) {
		this.roleColor = roleColor;
	}
	public Double getNewGeneralCount() {
		return newGeneralCount;
	}
	public void setNewGeneralCount(Double newGeneralCount) {
		this.newGeneralCount = newGeneralCount;
	}
	public Double getNewAreaCount() {
		return newAreaCount;
	}
	public void setNewAreaCount(Double newAreaCount) {
		this.newAreaCount = newAreaCount;
	}
	public Double getCancleGeneralCount() {
		return cancleGeneralCount;
	}
	public void setCancleGeneralCount(Double cancleGeneralCount) {
		this.cancleGeneralCount = cancleGeneralCount;
	}
	public Double getCancleAreaCount() {
		return cancleAreaCount;
	}
	public void setCancleAreaCount(Double cancleAreaCount) {
		this.cancleAreaCount = cancleAreaCount;
	}
	public String getMultipleJobs() {
		return multipleJobs;
	}
	public void setMultipleJobs(String multipleJobs) {
		this.multipleJobs = multipleJobs;
	}
	public Double getCountAll() {
		return countAll;
	}
	public void setCountAll(Double countAll) {
		this.countAll = countAll;
	}
	public Double getCount6() {
		return count6;
	}
	public void setCount6(Double count6) {
		this.count6 = count6;
	}
	public Double getCount7() {
		return count7;
	}
	public void setCount7(Double count7) {
		this.count7 = count7;
	}
	public Double getCount8() {
		return count8;
	}
	public void setCount8(Double count8) {
		this.count8 = count8;
	}
	public Double getCount9() {
		return count9;
	}
	public void setCount9(Double count9) {
		this.count9 = count9;
	}
	public Double getCount10() {
		return count10;
	}
	public void setCount10(Double count10) {
		this.count10 = count10;
	}
	public Double getCountAll1() {
		return countAll1;
	}
	public void setCountAll1(Double countAll1) {
		this.countAll1 = countAll1;
	}
	public Double getCountAll2() {
		return countAll2;
	}
	public void setCountAll2(Double countAll2) {
		this.countAll2 = countAll2;
	}
	public Double getOutBoundRevokeCount() {
		return outBoundRevokeCount;
	}
	public void setOutBoundRevokeCount(Double outBoundRevokeCount) {
		this.outBoundRevokeCount = outBoundRevokeCount;
	}
	public Double getInBoundRevokeCount() {
		return inBoundRevokeCount;
	}
	public void setInBoundRevokeCount(Double inBoundRevokeCount) {
		this.inBoundRevokeCount = inBoundRevokeCount;
	}
	public Double getCountryRevokeCount() {
		return countryRevokeCount;
	}
	public void setCountryRevokeCount(Double countryRevokeCount) {
		this.countryRevokeCount = countryRevokeCount;
	}
	public Double getAreaRevokeCount() {
		return areaRevokeCount;
	}
	public void setAreaRevokeCount(Double areaRevokeCount) {
		this.areaRevokeCount = areaRevokeCount;
	}
	public Double getCountRevoke() {
		return countRevoke;
	}
	public void setCountRevoke(Double countRevoke) {
		this.countRevoke = countRevoke;
	}
	public Double getCount1() {
		return count1;
	}
	public void setCount1(Double count1) {
		this.count1 = count1;
	}
	public Double getCount2() {
		return count2;
	}
	public void setCount2(Double count2) {
		this.count2 = count2;
	}
	public Double getCount3() {
		return count3;
	}
	public void setCount3(Double count3) {
		this.count3 = count3;
	}
	public Double getCount4() {
		return count4;
	}
	public void setCount4(Double count4) {
		this.count4 = count4;
	}
	public Double getCount5() {
		return count5;
	}
	public void setCount5(Double count5) {
		this.count5 = count5;
	}
	
	public Double getCount11() {
		return count11;
	}
	public void setCount11(Double count11) {
		this.count11 = count11;
	}
	public Double getCount12() {
		return count12;
	}
	public void setCount12(Double count12) {
		this.count12 = count12;
	}
	public Double getCount13() {
		return count13;
	}
	public void setCount13(Double count13) {
		this.count13 = count13;
	}
	public Double getCount14() {
		return count14;
	}
	public void setCount14(Double count14) {
		this.count14 = count14;
	}
	public Double getCount15() {
		return count15;
	}
	public void setCount15(Double count15) {
		this.count15 = count15;
	}
	public String getTemplateFileName() {
		return templateFileName;
	}
	public void setTemplateFileName(String templateFileName) {
		this.templateFileName = templateFileName;
	}
	public Double getOldCount() {
		return oldCount;
	}
	public void setOldCount(Double oldCount) {
		this.oldCount = oldCount;
	}
	public Double getNewCount() {
		return newCount;
	}
	public void setNewCount(Double newCount) {
		this.newCount = newCount;
	}
	public Double getCancleCount() {
		return cancleCount;
	}
	public void setCancleCount(Double cancleCount) {
		this.cancleCount = cancleCount;
	}
	public Double getRevokeCount() {
		return revokeCount;
	}
	public void setRevokeCount(Double revokeCount) {
		this.revokeCount = revokeCount;
	}
	public String getChartName() {
		return chartName;
	}
	public void setChartName(String chartName) {
		this.chartName = chartName;
	}
	public Double getOutBoundCount() {
		return outBoundCount;
	}
	public void setOutBoundCount(Double outBoundCount) {
		this.outBoundCount = outBoundCount;
	}
	public Double getInBoundCount() {
		return inBoundCount;
	}
	public void setInBoundCount(Double inBoundCount) {
		this.inBoundCount = inBoundCount;
	}
	public Double getCountryCount() {
		return countryCount;
	}
	public void setCountryCount(Double countryCount) {
		this.countryCount = countryCount;
	}
	public Double getAreaCount() {
		return areaCount;
	}
	public void setAreaCount(Double areaCount) {
		this.areaCount = areaCount;
	}
	public Double getCount() {
		return count;
	}
	public void setCount(Double count) {
		this.count = count;
	}

	public String getHeaderInfo() {
		return headerInfo;
	}
	public void setHeaderInfo(String headerInfo) {
		this.headerInfo = headerInfo;
	}
	public String getLicenseStatusName() {
		return licenseStatusName;
	}
	public void setLicenseStatusName(String licenseStatusName) {
		this.licenseStatusName = licenseStatusName;
	}

	public long getReceiptId() {
		return receiptId;
	}
	public void setReceiptId(long receiptId) {
		this.receiptId = receiptId;
	}
	public String getBookNo() {
		return bookNo;
	}
	public void setBookNo(String bookNo) {
		this.bookNo = bookNo;
	}
	public String getReceiptNo() {
		return receiptNo;
	}
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}
	public String getReceiptStatus() {
		return receiptStatus;
	}
	public void setReceiptStatus(String receiptStatus) {
		this.receiptStatus = receiptStatus;
	}
	public String getApproveDate() {
		return approveDate;
	}
	public void setApproveDate(String approveDate) {
		this.approveDate = approveDate;
	}
	public String getRegistrationDateFrom() {
		return registrationDateFrom;
	}
	public void setRegistrationDateFrom(String registrationDateFrom) {
		this.registrationDateFrom = registrationDateFrom;
	}
	public String getRegistrationDateTo() {
		return registrationDateTo;
	}
	public void setRegistrationDateTo(String registrationDateTo) {
		this.registrationDateTo = registrationDateTo;
	}
	public String getRegGroupProgress() {
		return regGroupProgress;
	}
	public void setRegGroupProgress(String regGroupProgress) {
		this.regGroupProgress = regGroupProgress;
	}
	public String getRegProgressStatus() {
		return regProgressStatus;
	}
	public void setRegProgressStatus(String regProgressStatus) {
		this.regProgressStatus = regProgressStatus;
	}
	public String getProgressBackStatus() {
		return progressBackStatus;
	}
	public void setProgressBackStatus(String progressBackStatus) {
		this.progressBackStatus = progressBackStatus;
	}

	public String getTraderOwnerNameEn() {
		return traderOwnerNameEn;
	}
	public void setTraderOwnerNameEn(String traderOwnerNameEn) {
		this.traderOwnerNameEn = traderOwnerNameEn;
	}

	public String getGraduationCourse() {
		return graduationCourse;
	}
	public void setGraduationCourse(String graduationCourse) {
		this.graduationCourse = graduationCourse;
	}
	public Integer getGenerationGraduate() {
		return generationGraduate;
	}
	public void setGenerationGraduate(Integer generationGraduate) {
		this.generationGraduate = generationGraduate;
	}
	public String getUniversityName() {
		return universityName;
	}
	public void setUniversityName(String universityName) {
		this.universityName = universityName;
	}
	public String getStudyDate() {
		return studyDate;
	}
	public void setStudyDate(String studyDate) {
		this.studyDate = studyDate;
	}
	public String getPrefixNameEn() {
		return prefixNameEn;
	}
	public void setPrefixNameEn(String prefixNameEn) {
		this.prefixNameEn = prefixNameEn;
	}
	public String getProgressStatus() {
		return progressStatus;
	}
	public void setProgressStatus(String progressStatus) {
		this.progressStatus = progressStatus;
	}
	public String getRoleAction() {
		return roleAction;
	}
	public void setRoleAction(String roleAction) {
		this.roleAction = roleAction;
	}
	public String getTraderGuideDetail() {
		return traderGuideDetail;
	}
	public void setTraderGuideDetail(String traderGuideDetail) {
		this.traderGuideDetail = traderGuideDetail;
	}
	public String getTraderTrainedDetail() {
		return traderTrainedDetail;
	}
	public void setTraderTrainedDetail(String traderTrainedDetail) {
		this.traderTrainedDetail = traderTrainedDetail;
	}
	public long getEduId() {
		return eduId;
	}
	public void setEduId(long eduId) {
		this.eduId = eduId;
	}
	public long getProvinceGuideServiceId() {
		return provinceGuideServiceId;
	}
	public void setProvinceGuideServiceId(long provinceGuideServiceId) {
		this.provinceGuideServiceId = provinceGuideServiceId;
	}
	public String getProvinceGuideServiceName() {
		return provinceGuideServiceName;
	}
	public void setProvinceGuideServiceName(String provinceGuideServiceName) {
		this.provinceGuideServiceName = provinceGuideServiceName;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public String getTraderOwnerName() {
		return traderOwnerName;
	}
	public void setTraderOwnerName(String traderOwnerName) {
		this.traderOwnerName = traderOwnerName;
	}
	public String getTraderAddress() {
		return traderAddress;
	}
	public void setTraderAddress(String traderAddress) {
		this.traderAddress = traderAddress;
	}
	public String getPersonTypeIndividual() {
		return personTypeIndividual;
	}
	public void setPersonTypeIndividual(String personTypeIndividual) {
		this.personTypeIndividual = personTypeIndividual;
	}
	public String getPersonTypeCorporate() {
		return personTypeCorporate;
	}
	public void setPersonTypeCorporate(String personTypeCorporate) {
		this.personTypeCorporate = personTypeCorporate;
	}
	public long getPrefixIdEn() {
		return prefixIdEn;
	}
	public void setPrefixIdEn(long prefixIdEn) {
		this.prefixIdEn = prefixIdEn;
	}
	public long getPrefixIdCorp() {
		return prefixIdCorp;
	}
	public void setPrefixIdCorp(long prefixIdCorp) {
		this.prefixIdCorp = prefixIdCorp;
	}
	public String getFirstNameCorp() {
		return firstNameCorp;
	}
	public void setFirstNameCorp(String firstNameCorp) {
		this.firstNameCorp = firstNameCorp;
	}
	public String getIdentityNoCorp() {
		return identityNoCorp;
	}
	public void setIdentityNoCorp(String identityNoCorp) {
		this.identityNoCorp = identityNoCorp;
	}
	public String getIdentityDateCorp() {
		return identityDateCorp;
	}
	public void setIdentityDateCorp(String identityDateCorp) {
		this.identityDateCorp = identityDateCorp;
	}
	public long getProvinceIdCorp() {
		return provinceIdCorp;
	}
	public void setProvinceIdCorp(long provinceIdCorp) {
		this.provinceIdCorp = provinceIdCorp;
	}
	public long getTraderGuideId() {
		return traderGuideId;
	}
	public void setTraderGuideId(long traderGuideId) {
		this.traderGuideId = traderGuideId;
	}
	public long getPersonGuideId() {
		return personGuideId;
	}
	public void setPersonGuideId(long personGuideId) {
		this.personGuideId = personGuideId;
	}
	public String getTraderCategoryName() {
		return traderCategoryName;
	}
	public void setTraderCategoryName(String traderCategoryName) {
		this.traderCategoryName = traderCategoryName;
	}
	public String getTraderCategoryGuideType() {
		return traderCategoryGuideType;
	}
	public void setTraderCategoryGuideType(String traderCategoryGuideType) {
		this.traderCategoryGuideType = traderCategoryGuideType;
	}
	public String getTraderCategoryGuideTypeName() {
		return traderCategoryGuideTypeName;
	}
	public void setTraderCategoryGuideTypeName(String traderCategoryGuideTypeName) {
		this.traderCategoryGuideTypeName = traderCategoryGuideTypeName;
	}
	public long getPrefixId() {
		return prefixId;
	}
	public void setPrefixId(long prefixId) {
		this.prefixId = prefixId;
	}
	public String getPrefixName() {
		return prefixName;
	}
	public void setPrefixName(String prefixName) {
		this.prefixName = prefixName;
	}
	public long getAmphurId() {
		return amphurId;
	}
	public void setAmphurId(long amphurId) {
		this.amphurId = amphurId;
	}
	public String getAmphurName() {
		return amphurName;
	}
	public void setAmphurName(String amphurName) {
		this.amphurName = amphurName;
	}
	public long getTaxAmphurId() {
		return taxAmphurId;
	}
	public void setTaxAmphurId(long taxAmphurId) {
		this.taxAmphurId = taxAmphurId;
	}
	public String getTaxAmphurName() {
		return taxAmphurName;
	}
	public void setTaxAmphurName(String taxAmphurName) {
		this.taxAmphurName = taxAmphurName;
	}
	public long getProvinceId() {
		return provinceId;
	}
	public void setProvinceId(long provinceId) {
		this.provinceId = provinceId;
	}
	public String getProvinceName() {
		return provinceName;
	}
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}
	public long getTaxProvinceId() {
		return taxProvinceId;
	}
	public void setTaxProvinceId(long taxProvinceId) {
		this.taxProvinceId = taxProvinceId;
	}
	public String getTaxProvinceName() {
		return taxProvinceName;
	}
	public void setTaxProvinceName(String taxProvinceName) {
		this.taxProvinceName = taxProvinceName;
	}
	public long getPostfixId() {
		return postfixId;
	}
	public void setPostfixId(long postfixId) {
		this.postfixId = postfixId;
	}
	public String getPostfixName() {
		return postfixName;
	}
	public void setPostfixName(String postfixName) {
		this.postfixName = postfixName;
	}
	public long getRegId() {
		return regId;
	}
	public void setRegId(long regId) {
		this.regId = regId;
	}
	public String getRegistrationType() {
		return registrationType;
	}
	public void setRegistrationType(String registrationType) {
		this.registrationType = registrationType;
	}
	public String getRegistrationProgress() {
		return registrationProgress;
	}
	public void setRegistrationProgress(String registrationProgress) {
		this.registrationProgress = registrationProgress;
	}
	public String getRegistrationDate() {
		return registrationDate;
	}
	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}
	public String getRegistrationNo() {
		return registrationNo;
	}
	public void setRegistrationNo(String registrationNo) {
		this.registrationNo = registrationNo;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public long getOrgId() {
		return orgId;
	}
	public void setOrgId(long orgId) {
		this.orgId = orgId;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public long getTraderId() {
		return traderId;
	}
	public void setTraderId(long traderId) {
		this.traderId = traderId;
	}
	public String getTraderType() {
		return traderType;
	}
	public void setTraderType(String traderType) {
		this.traderType = traderType;
	}
	public String getLicenseNo() {
		return licenseNo;
	}
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	public String getTraderName() {
		return traderName;
	}
	public void setTraderName(String traderName) {
		this.traderName = traderName;
	}
	public String getTraderNameEn() {
		return traderNameEn;
	}
	public void setTraderNameEn(String traderNameEn) {
		this.traderNameEn = traderNameEn;
	}
	public String getPronunciationName() {
		return pronunciationName;
	}
	public void setPronunciationName(String pronunciationName) {
		this.pronunciationName = pronunciationName;
	}
	public String getTraderCategory() {
		return traderCategory;
	}
	public void setTraderCategory(String traderCategory) {
		this.traderCategory = traderCategory;
	}
	public String getLicenseGuideNo() {
		return licenseGuideNo;
	}
	public void setLicenseGuideNo(String licenseGuideNo) {
		this.licenseGuideNo = licenseGuideNo;
	}
	public String getTraderArea() {
		return traderArea;
	}
	public void setTraderArea(String traderArea) {
		this.traderArea = traderArea;
	}
	public String getLicenseStatus() {
		return licenseStatus;
	}
	public void setLicenseStatus(String licenseStatus) {
		this.licenseStatus = licenseStatus;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getExpireDate() {
		return expireDate;
	}
	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}
	public String getPrintLicense() {
		return printLicense;
	}
	public void setPrintLicense(String printLicense) {
		this.printLicense = printLicense;
	}
	public String getPrintCard() {
		return printCard;
	}
	public void setPrintCard(String printCard) {
		this.printCard = printCard;
	}
	public long getPersonId() {
		return personId;
	}
	public void setPersonId(long personId) {
		this.personId = personId;
	}
	public String getPersonType() {
		return personType;
	}
	public void setPersonType(String personType) {
		this.personType = personType;
	}
	public String getIdentityNo() {
		return identityNo;
	}
	public void setIdentityNo(String identityNo) {
		this.identityNo = identityNo;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public long getPlanId() {
		return planId;
	}
	public void setPlanId(long planId) {
		this.planId = planId;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public Integer getPlantripPerYear() {
		return plantripPerYear;
	}
	public void setPlantripPerYear(Integer plantripPerYear) {
		this.plantripPerYear = plantripPerYear;
	}
	public String getAuthorityName() {
		return authorityName;
	}
	public void setAuthorityName(String authorityName) {
		this.authorityName = authorityName;
	}
	
	public String getRegRecordStatus() {
		return regRecordStatus;
	}
	public void setRegRecordStatus(String regRecordStatus) {
		this.regRecordStatus = regRecordStatus;
	}
	public String getTraderRecordStatus() {
		return traderRecordStatus;
	}
	public void setTraderRecordStatus(String traderRecordStatus) {
		this.traderRecordStatus = traderRecordStatus;
	}
	public String getPersonRecordStatus() {
		return personRecordStatus;
	}
	public void setPersonRecordStatus(String personRecordStatus) {
		this.personRecordStatus = personRecordStatus;
	}
	public String getPlanRecordStatus() {
		return planRecordStatus;
	}
	public void setPlanRecordStatus(String planRecordStatus) {
		this.planRecordStatus = planRecordStatus;
	}
	public String getPersonFullName() {
		return personFullName;
	}
	public void setPersonFullName(String personFullName) {
		this.personFullName = personFullName;
	}
	public String getPassportNo() {
		return passportNo;
	}
	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}
	public String getFirstNameEn() {
		return firstNameEn;
	}
	public void setFirstNameEn(String firstNameEn) {
		this.firstNameEn = firstNameEn;
	}
	public String getLastNameEn() {
		return lastNameEn;
	}
	public void setLastNameEn(String lastNameEn) {
		this.lastNameEn = lastNameEn;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getGenderName() {
		return genderName;
	}
	public void setGenderName(String genderName) {
		this.genderName = genderName;
	}
	public String getPersonNationality() {
		return personNationality;
	}
	public void setPersonNationality(String personNationality) {
		this.personNationality = personNationality;
	}

	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public Integer getAgeYear() {
		return ageYear;
	}
	public void setAgeYear(Integer ageYear) {
		this.ageYear = ageYear;
	}
	public String getIdentityNoExpire() {
		return identityNoExpire;
	}
	public void setIdentityNoExpire(String identityNoExpire) {
		this.identityNoExpire = identityNoExpire;
	}
	public String getCorporateType() {
		return corporateType;
	}
	public void setCorporateType(String corporateType) {
		this.corporateType = corporateType;
	}
	public String getTaxIdentityNo() {
		return taxIdentityNo;
	}
	public void setTaxIdentityNo(String taxIdentityNo) {
		this.taxIdentityNo = taxIdentityNo;
	}
	public String getIdentityDate() {
		return identityDate;
	}
	public void setIdentityDate(String identityDate) {
		this.identityDate = identityDate;
	}
	public String getCommitteeName1() {
		return committeeName1;
	}
	public void setCommitteeName1(String committeeName1) {
		this.committeeName1 = committeeName1;
	}
	public String getCommitteeName2() {
		return committeeName2;
	}
	public void setCommitteeName2(String committeeName2) {
		this.committeeName2 = committeeName2;
	}
	public String getCommitteeNameSign() {
		return committeeNameSign;
	}
	public void setCommitteeNameSign(String committeeNameSign) {
		this.committeeNameSign = committeeNameSign;
	}
	public String getRecordStatus() {
		return recordStatus;
	}
	public String getRegistrationTypeName() {
		return registrationTypeName;
	}
	public void setRegistrationTypeName(String registrationTypeName) {
		this.registrationTypeName = registrationTypeName;
	}
	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}
	public String getTraderTypeName() {
		return traderTypeName;
	}
	public void setTraderTypeName(String traderTypeName) {
		this.traderTypeName = traderTypeName;
	}
	public String getPrintLicenseStatus() {
		return printLicenseStatus;
	}
	public void setPrintLicenseStatus(String printLicenseStatus) {
		this.printLicenseStatus = printLicenseStatus;
	}
	public String getApproveDateFrom() {
		return approveDateFrom;
	}
	public void setApproveDateFrom(String approveDateFrom) {
		this.approveDateFrom = approveDateFrom;
	}
	public String getApproveDateTo() {
		return approveDateTo;
	}
	public void setApproveDateTo(String approveDateTo) {
		this.approveDateTo = approveDateTo;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Integer getTraderBranchNo() {
		return traderBranchNo;
	}
	public void setTraderBranchNo(Integer traderBranchNo) {
		this.traderBranchNo = traderBranchNo;
	}
	public String getBranchType() {
		return branchType;
	}
	public void setBranchType(String branchType) {
		this.branchType = branchType;
	}
	public long getPrintLicenseId() {
		return printLicenseId;
	}
	public void setPrintLicenseId(long printLicenseId) {
		this.printLicenseId = printLicenseId;
	}
	public String getPrintLicenseDate() {
		return printLicenseDate;
	}
	public void setPrintLicenseDate(String printLicenseDate) {
		this.printLicenseDate = printLicenseDate;
	}
	public String getDetailAction() {
		return detailAction;
	}
	public void setDetailAction(String detailAction) {
		this.detailAction = detailAction;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAddressNo() {
		return addressNo;
	}
	public void setAddressNo(String addressNo) {
		this.addressNo = addressNo;
	}
	public String getSoi() {
		return soi;
	}
	public void setSoi(String soi) {
		this.soi = soi;
	}
	public String getRoadName() {
		return roadName;
	}
	public void setRoadName(String roadName) {
		this.roadName = roadName;
	}
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getTambolName() {
		return tambolName;
	}
	public void setTambolName(String tambolName) {
		this.tambolName = tambolName;
	}
	public Long getTambolId() {
		return tambolId;
	}
	public void setTambolId(Long tambolId) {
		this.tambolId = tambolId;
	}
	public String getToday() {
		return today;
	}
	public void setToday(String today) {
		this.today = today;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getTodayForm() {
		return todayForm;
	}
	public void setTodayForm(String todayForm) {
		this.todayForm = todayForm;
	}
	public String getDayF() {
		return dayF;
	}
	public void setDayF(String dayF) {
		this.dayF = dayF;
	}
	public String getMonthF() {
		return monthF;
	}
	public void setMonthF(String monthF) {
		this.monthF = monthF;
	}
	public String getYearF() {
		return yearF;
	}
	public void setYearF(String yearF) {
		this.yearF = yearF;
	}
	public long getPrintCardId() {
		return printCardId;
	}
	public void setPrintCardId(long printCardId) {
		this.printCardId = printCardId;
	}
	public String getPrintCardDate() {
		return printCardDate;
	}
	public void setPrintCardDate(String printCardDate) {
		this.printCardDate = printCardDate;
	}
	public String getPrintCardStatus() {
		return printCardStatus;
	}
	public void setPrintCardStatus(String printCardStatus) {
		this.printCardStatus = printCardStatus;
	}
	public String getLicenseNoTo() {
		return licenseNoTo;
	}
	public void setLicenseNoTo(String licenseNoTo) {
		this.licenseNoTo = licenseNoTo;
	}
	public String getLicenseNoFrom() {
		return licenseNoFrom;
	}
	public void setLicenseNoFrom(String licenseNoFrom) {
		this.licenseNoFrom = licenseNoFrom;
	}
	public String getImgAction() {
		return imgAction;
	}
	public void setImgAction(String imgAction) {
		this.imgAction = imgAction;
	}
	public String getImgPath() {
		return imgPath;
	}
	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}
	public long getPersonImgId() {
		return personImgId;
	}
	public void setPersonImgId(long personImgId) {
		this.personImgId = personImgId;
	}
	public String getYearValid() {
		return yearValid;
	}
	public void setYearValid(String yearValid) {
		this.yearValid = yearValid;
	}
	public String getMonthValid() {
		return monthValid;
	}
	public void setMonthValid(String monthValid) {
		this.monthValid = monthValid;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getDateTo() {
		return dateTo;
	}
	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}
	public String getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String getCheck() {
		return check;
	}
	public void setCheck(String check) {
		this.check = check;
	}

	public Date getPunishmentDate() {
		return punishmentDate;
	}
	public void setPunishmentDate(Date punishmentDate) {
		this.punishmentDate = punishmentDate;
	}
	public Date getPunishmentExpireDate() {
		return punishmentExpireDate;
	}
	public void setPunishmentExpireDate(Date punishmentExpireDate) {
		this.punishmentExpireDate = punishmentExpireDate;
	}
	public String getCommitteePersonType() {
		return committeePersonType;
	}
	public void setCommitteePersonType(String committeePersonType) {
		this.committeePersonType = committeePersonType;
	}
	public String getBranchNo() {
		return branchNo;
	}
	public void setBranchNo(String branchNo) {
		this.branchNo = branchNo;
	}
	public String getBranchAddressNo() {
		return branchAddressNo;
	}
	public void setBranchAddressNo(String branchAddressNo) {
		this.branchAddressNo = branchAddressNo;
	}
	public String getBranchSoi() {
		return branchSoi;
	}
	public void setBranchSoi(String branchSoi) {
		this.branchSoi = branchSoi;
	}
	public String getBranchRoadName() {
		return branchRoadName;
	}
	public void setBranchRoadName(String branchRoadName) {
		this.branchRoadName = branchRoadName;
	}
	public String getBranchTambolName() {
		return branchTambolName;
	}
	public void setBranchTambolName(String branchTambolName) {
		this.branchTambolName = branchTambolName;
	}
	public String getBranchAmphurName() {
		return branchAmphurName;
	}
	public void setBranchAmphurName(String branchAmphurName) {
		this.branchAmphurName = branchAmphurName;
	}
	public String getBranchProvinceName() {
		return branchProvinceName;
	}
	public void setBranchProvinceName(String branchProvinceName) {
		this.branchProvinceName = branchProvinceName;
	}
	public String getBranchPostCode() {
		return branchPostCode;
	}
	public void setBranchPostCode(String branchPostCode) {
		this.branchPostCode = branchPostCode;
	}
	public String getManagerBG() {
		return managerBG;
	}
	public void setManagerBG(String managerBG) {
		this.managerBG = managerBG;
	}
	public String getGuideName() {
		return guideName;
	}
	public void setGuideName(String guideName) {
		this.guideName = guideName;
	}
	public String getTraderCategoryNameOth1() {
		return traderCategoryNameOth1;
	}
	public void setTraderCategoryNameOth1(String traderCategoryNameOth1) {
		this.traderCategoryNameOth1 = traderCategoryNameOth1;
	}
	public String getTraderCategoryNameOth2() {
		return traderCategoryNameOth2;
	}
	public void setTraderCategoryNameOth2(String traderCategoryNameOth2) {
		this.traderCategoryNameOth2 = traderCategoryNameOth2;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getPosition1() {
		return position1;
	}
	public void setPosition1(String position1) {
		this.position1 = position1;
	}
	public String getPosition2() {
		return position2;
	}
	public void setPosition2(String position2) {
		this.position2 = position2;
	}
	public String getBuildingName() {
		return buildingName;
	}
	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}
	public String getFloor() {
		return floor;
	}
	public void setFloor(String floor) {
		this.floor = floor;
	}
	public String getRoomNo() {
		return roomNo;
	}
	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}
	public String getMoo() {
		return moo;
	}
	public void setMoo(String moo) {
		this.moo = moo;
	}
	public String getVillageName() {
		return villageName;
	}
	public void setVillageName(String villageName) {
		this.villageName = villageName;
	}
	public long getAddressId() {
		return addressId;
	}
	public void setAddressId(long addressId) {
		this.addressId = addressId;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public long getDeactiveId() {
		return deactiveId;
	}
	public void setDeactiveId(long deactiveId) {
		this.deactiveId = deactiveId;
	}
	public String getRemark2() {
		return remark2;
	}
	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}
	public String getDeactivateRemark() {
		return deactivateRemark;
	}
	public void setDeactivateRemark(String deactivateRemark) {
		this.deactivateRemark = deactivateRemark;
	}
	public String getDeactivateResonRemark() {
		return deactivateResonRemark;
	}
	public void setDeactivateResonRemark(String deactivateResonRemark) {
		this.deactivateResonRemark = deactivateResonRemark;
	}
	public String getApproveStatus() {
		return approveStatus;
	}
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	public String getApproveStatusName() {
		return approveStatusName;
	}
	public void setApproveStatusName(String approveStatusName) {
		this.approveStatusName = approveStatusName;
	}
	public String getDeactivateBookNo() {
		return deactivateBookNo;
	}
	public void setDeactivateBookNo(String deactivateBookNo) {
		this.deactivateBookNo = deactivateBookNo;
	}
	public String getBookDate() {
		return bookDate;
	}
	public void setBookDate(String bookDate) {
		this.bookDate = bookDate;
	}
	public String getRenewLicenseStatus() {
		return renewLicenseStatus;
	}
	public void setRenewLicenseStatus(String renewLicenseStatus) {
		this.renewLicenseStatus = renewLicenseStatus;
	}
	public String getDeactivateNo() {
		return deactivateNo;
	}
	public void setDeactivateNo(String deactivateNo) {
		this.deactivateNo = deactivateNo;
	}
	public String getDeactivateDate() {
		return deactivateDate;
	}
	public void setDeactivateDate(String deactivateDate) {
		this.deactivateDate = deactivateDate;
	}
	public String getOfficerName() {
		return officerName;
	}
	public void setOfficerName(String officerName) {
		this.officerName = officerName;
	}
	public long getOfficerId() {
		return officerId;
	}
	public void setOfficerId(long officerId) {
		this.officerId = officerId;
	}
	public String getDeactivateTypeName() {
		return deactivateTypeName;
	}
	public void setDeactivateTypeName(String deactivateTypeName) {
		this.deactivateTypeName = deactivateTypeName;
	}
	public long getGuaranteeId() {
		return guaranteeId;
	}
	public void setGuaranteeId(long guaranteeId) {
		this.guaranteeId = guaranteeId;
	}
	public String getReceiveDate() {
		return receiveDate;
	}
	public void setReceiveDate(String receiveDate) {
		this.receiveDate = receiveDate;
	}
	public String getReceiveName() {
		return receiveName;
	}
	public void setReceiveName(String receiveName) {
		this.receiveName = receiveName;
	}
	public String getRefundName() {
		return refundName;
	}
	public void setRefundName(String refundName) {
		this.refundName = refundName;
	}
	public String getRefundType() {
		return refundType;
	}
	public void setRefundType(String refundType) {
		this.refundType = refundType;
	}
	public BigDecimal getRefundMny() {
		return refundMny;
	}
	public void setRefundMny(BigDecimal refundMny) {
		this.refundMny = refundMny;
	}
	public BigDecimal getRefundInterest() {
		return refundInterest;
	}
	public void setRefundInterest(BigDecimal refundInterest) {
		this.refundInterest = refundInterest;
	}
	public String getRefundDate() {
		return refundDate;
	}
	public void setRefundDate(String refundDate) {
		this.refundDate = refundDate;
	}
	public String getRefundRemark() {
		return refundRemark;
	}
	public void setRefundRemark(String refundRemark) {
		this.refundRemark = refundRemark;
	}
	public String getGuaranteeStatus() {
		return guaranteeStatus;
	}
	public void setGuaranteeStatus(String guaranteeStatus) {
		this.guaranteeStatus = guaranteeStatus;
	}
	public String getGuaranteeStatusName() {
		return guaranteeStatusName;
	}
	public void setGuaranteeStatusName(String guaranteeStatusName) {
		this.guaranteeStatusName = guaranteeStatusName;
	}
	public String getGuaranteeRemark() {
		return guaranteeRemark;
	}
	public void setGuaranteeRemark(String guaranteeRemark) {
		this.guaranteeRemark = guaranteeRemark;
	}
	public BigDecimal getGuaranteeMny() {
		return guaranteeMny;
	}
	public void setGuaranteeMny(BigDecimal guaranteeMny) {
		this.guaranteeMny = guaranteeMny;
	}
	public BigDecimal getGuaranteeInterest() {
		return guaranteeInterest;
	}
	public void setGuaranteeInterest(BigDecimal guaranteeInterest) {
		this.guaranteeInterest = guaranteeInterest;
	}
	public String getGauranteeRecordStatus() {
		return gauranteeRecordStatus;
	}
	public void setGauranteeRecordStatus(String gauranteeRecordStatus) {
		this.gauranteeRecordStatus = gauranteeRecordStatus;
	}
	public String getBgImg() {
		return bgImg;
	}
	public void setBgImg(String bgImg) {
		this.bgImg = bgImg;
	}
	public String getL_licenseNo() {
		return l_licenseNo;
	}
	public void setL_licenseNo(String l_licenseNo) {
		this.l_licenseNo = l_licenseNo;
	}
	public String getL_traderOwnerName() {
		return l_traderOwnerName;
	}
	public void setL_traderOwnerName(String l_traderOwnerName) {
		this.l_traderOwnerName = l_traderOwnerName;
	}
	public String getL_traderOwnerNameEn() {
		return l_traderOwnerNameEn;
	}
	public void setL_traderOwnerNameEn(String l_traderOwnerNameEn) {
		this.l_traderOwnerNameEn = l_traderOwnerNameEn;
	}
	public String getL_identityNo() {
		return l_identityNo;
	}
	public void setL_identityNo(String l_identityNo) {
		this.l_identityNo = l_identityNo;
	}
	public String getL_effectiveDate() {
		return l_effectiveDate;
	}
	public void setL_effectiveDate(String l_effectiveDate) {
		this.l_effectiveDate = l_effectiveDate;
	}
	public String getL_expireDate() {
		return l_expireDate;
	}
	public void setL_expireDate(String l_expireDate) {
		this.l_expireDate = l_expireDate;
	}
	public String getL_traderArea() {
		return l_traderArea;
	}
	public void setL_traderArea(String l_traderArea) {
		this.l_traderArea = l_traderArea;
	}
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	public String getLicenseLeaderNo() {
		return licenseLeaderNo;
	}
	public void setLicenseLeaderNo(String licenseLeaderNo) {
		this.licenseLeaderNo = licenseLeaderNo;
	}
	public String getImageFile() {
		return imageFile;
	}
	public void setImageFile(String imageFile) {
		this.imageFile = imageFile;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public BigDecimal getSumGuide() {
		return sumGuide;
	}
	public void setSumGuide(BigDecimal sumGuide) {
		this.sumGuide = sumGuide;
	}
	public long getCountryId() {
		return countryId;
	}
	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}
	public BigDecimal getSumProvince() {
		return sumProvince;
	}
	public void setSumProvince(BigDecimal sumProvince) {
		this.sumProvince = sumProvince;
	}
	public BigDecimal getSumStaticTourLeader() {
		return sumStaticTourLeader;
	}
	public void setSumStaticTourLeader(BigDecimal sumStaticTourLeader) {
		this.sumStaticTourLeader = sumStaticTourLeader;
	}
	public String getTourLeaderName() {
		return tourLeaderName;
	}
	public void setTourLeaderName(String tourLeaderName) {
		this.tourLeaderName = tourLeaderName;
	}
	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	public BigDecimal getSumChange() {
		return sumChange;
	}
	public void setSumChange(BigDecimal sumChange) {
		this.sumChange = sumChange;
	}
	public BigDecimal getSumBranch() {
		return sumBranch;
	}
	public void setSumBranch(BigDecimal sumBranch) {
		this.sumBranch = sumBranch;
	}
	
}
