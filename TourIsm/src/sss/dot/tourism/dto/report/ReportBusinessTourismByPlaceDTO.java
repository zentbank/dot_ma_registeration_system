package sss.dot.tourism.dto.report;

import java.io.Serializable;

public class ReportBusinessTourismByPlaceDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private Long provinceId;
	
	private String provinceName;
	
	private Long amphurId;
	
	private String amphurName;
	
	private Long tambolId;
	
	private String tambolName;
	
	private Integer businessTourismAmt;

	public Long getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(Long provinceId) {
		this.provinceId = provinceId;
	}

	
	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public Long getAmphurId() {
		return amphurId;
	}

	public void setAmphurId(Long amphurId) {
		this.amphurId = amphurId;
	}

	public String getAmphurName() {
		return amphurName;
	}

	public void setAmphurName(String amphurName) {
		this.amphurName = amphurName;
	}

	public Long getTambolId() {
		return tambolId;
	}

	public void setTambolId(Long tambolId) {
		this.tambolId = tambolId;
	}

	public String getTambolName() {
		return tambolName;
	}

	public void setTambolName(String tambolName) {
		this.tambolName = tambolName;
	}

	public Integer getBusinessTourismAmt() {
		return businessTourismAmt;
	}

	public void setBusinessTourismAmt(Integer businessTourismAmt) {
		this.businessTourismAmt = businessTourismAmt;
	}
	
	
}
