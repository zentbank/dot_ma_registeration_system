package sss.dot.tourism.dto.training;

import java.io.Serializable;
import java.util.Date;

import sss.dot.tourism.domain.Education;

public class EducationDTO implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7765185676043875199L;
	private long personTrainedId;


	private String identityNo;
	private String identityDate;
	private String passportNo;
	private String firstName;
	private String lastName;
	private String firstNameEn;
	private String lastNameEn;
	private long prefixId;
	private long prefixEnId;
	private String prefixName;
	private String gender;
	private String personNationality;
	private Integer ageYear;
	private String identityNoExpire;
	private String taxIdentityNo;
	
	private long taxProvinceId;
	private long provinceId;
	
	private long taxAmphurId;
	private long amphurId;
	
	private long eduId;


	private String educationType;
	private String studyDate;
	private String graduationDate;
	private String graduationYear;
	private String graduationCourse;
	private String educationMajor;
	private Integer generationGraduate;
	private String recordStatus;
	private String eduRecordStatus;
	
	
	private long masUniversityId;
	private long masEducationLevelId;
	private String universityName;
	private String educationLevelName;
	
	private long personId;
	private long regId;
	private long traderId;
	
	private int page;
	private int start;
	private int limit;
	private String provinceName;
	private String amphurName;
	
	private String courseName;
	private String fullName;
	private String birthDate;
	

	private String personRecordStatus;
	
	private long countryId;
	
	
	
	public long getCountryId() {
		return countryId;
	}
	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}
	public String getPersonRecordStatus() {
		return personRecordStatus;
	}
	public void setPersonRecordStatus(String personRecordStatus) {
		this.personRecordStatus = personRecordStatus;
	}
	public String getEduRecordStatus() {
		return eduRecordStatus;
	}
	public void setEduRecordStatus(String eduRecordStatus) {
		this.eduRecordStatus = eduRecordStatus;
	}

	public String getUniversityName() {
		return universityName;
	}
	public void setUniversityName(String universityName) {
		this.universityName = universityName;
	}
	public String getEducationLevelName() {
		return educationLevelName;
	}
	public void setEducationLevelName(String educationLevelName) {
		this.educationLevelName = educationLevelName;
	}
	public long getMasEducationLevelId() {
		return masEducationLevelId;
	}
	public void setMasEducationLevelId(long masEducationLevelId) {
		this.masEducationLevelId = masEducationLevelId;
	}
	public long getMasUniversityId() {
		return masUniversityId;
	}
	public void setMasUniversityId(long masUniversityId) {
		this.masUniversityId = masUniversityId;
	}
	public long getPersonId() {
		return personId;
	}
	public void setPersonId(long personId) {
		this.personId = personId;
	}
	public long getRegId() {
		return regId;
	}
	public void setRegId(long regId) {
		this.regId = regId;
	}
	public long getTraderId() {
		return traderId;
	}
	public void setTraderId(long traderId) {
		this.traderId = traderId;
	}
	public long getPersonTrainedId() {
		return personTrainedId;
	}
	public void setPersonTrainedId(long personTrainedId) {
		this.personTrainedId = personTrainedId;
	}

	public String getIdentityNo() {
		return identityNo;
	}
	public void setIdentityNo(String identityNo) {
		this.identityNo = identityNo;
	}
	public String getIdentityDate() {
		return identityDate;
	}
	public void setIdentityDate(String identityDate) {
		this.identityDate = identityDate;
	}
	public String getPassportNo() {
		return passportNo;
	}
	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstNameEn() {
		return firstNameEn;
	}
	public void setFirstNameEn(String firstNameEn) {
		this.firstNameEn = firstNameEn;
	}
	public String getLastNameEn() {
		return lastNameEn;
	}
	public void setLastNameEn(String lastNameEn) {
		this.lastNameEn = lastNameEn;
	}

	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getPersonNationality() {
		return personNationality;
	}
	public void setPersonNationality(String personNationality) {
		this.personNationality = personNationality;
	}
	public Integer getAgeYear() {
		return ageYear;
	}
	public void setAgeYear(Integer ageYear) {
		this.ageYear = ageYear;
	}
	public String getIdentityNoExpire() {
		return identityNoExpire;
	}
	public void setIdentityNoExpire(String identityNoExpire) {
		this.identityNoExpire = identityNoExpire;
	}
	public String getTaxIdentityNo() {
		return taxIdentityNo;
	}
	public void setTaxIdentityNo(String taxIdentityNo) {
		this.taxIdentityNo = taxIdentityNo;
	}
	public long getTaxProvinceId() {
		return taxProvinceId;
	}
	public void setTaxProvinceId(long taxProvinceId) {
		this.taxProvinceId = taxProvinceId;
	}
	public long getProvinceId() {
		return provinceId;
	}
	public void setProvinceId(long provinceId) {
		this.provinceId = provinceId;
	}
	public long getTaxAmphurId() {
		return taxAmphurId;
	}
	public void setTaxAmphurId(long taxAmphurId) {
		this.taxAmphurId = taxAmphurId;
	}
	public long getAmphurId() {
		return amphurId;
	}
	public void setAmphurId(long amphurId) {
		this.amphurId = amphurId;
	}
	public long getEduId() {
		return eduId;
	}
	public void setEduId(long eduId) {
		this.eduId = eduId;
	}
	public String getEducationType() {
		return educationType;
	}
	public void setEducationType(String educationType) {
		this.educationType = educationType;
	}
	
	public String getStudyDate() {
		return studyDate;
	}
	public void setStudyDate(String studyDate) {
		this.studyDate = studyDate;
	}
	public String getGraduationDate() {
		return graduationDate;
	}
	public void setGraduationDate(String graduationDate) {
		this.graduationDate = graduationDate;
	}
	public String getGraduationYear() {
		return graduationYear;
	}
	public void setGraduationYear(String graduationYear) {
		this.graduationYear = graduationYear;
	}
	public String getGraduationCourse() {
		return graduationCourse;
	}
	public void setGraduationCourse(String graduationCourse) {
		this.graduationCourse = graduationCourse;
	}
	public String getEducationMajor() {
		return educationMajor;
	}
	public void setEducationMajor(String educationMajor) {
		this.educationMajor = educationMajor;
	}
	public Integer getGenerationGraduate() {
		return generationGraduate;
	}
	public void setGenerationGraduate(Integer generationGraduate) {
		this.generationGraduate = generationGraduate;
	}
	public String getRecordStatus() {
		return recordStatus;
	}
	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}
	public String getPrefixName() {
		return prefixName;
	}
	public void setPrefixName(String prefixName) {
		this.prefixName = prefixName;
	}
	public void setPrefixId(long prefixId) {
		this.prefixId = prefixId;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public long getPrefixEnId() {
		return prefixEnId;
	}
	public void setPrefixEnId(long prefixEnId) {
		this.prefixEnId = prefixEnId;
	}
	public long getPrefixId() {
		return prefixId;
	}
	public String getProvinceName() {
		return provinceName;
	}
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}
	public String getAmphurName() {
		return amphurName;
	}
	public void setAmphurName(String amphurName) {
		this.amphurName = amphurName;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	
	
	
}
