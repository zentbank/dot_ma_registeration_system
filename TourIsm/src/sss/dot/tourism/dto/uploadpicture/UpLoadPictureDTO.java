package sss.dot.tourism.dto.uploadpicture;

import java.util.Date;



public class UpLoadPictureDTO {
	private long personImgId;
	private long traderId;
	private long regId;
	private String imgPath;
	private String imgRecordStatus;
	private String createUser;
	private Date createDtm;
	private String lastUpdUser;
	private Date lastUpdDtm;
	
	public long getPersonImgId() {
		return personImgId;
	}
	public void setPersonImgId(long personImgId) {
		this.personImgId = personImgId;
	}
	public long getTraderId() {
		return traderId;
	}
	public void setTraderId(long traderId) {
		this.traderId = traderId;
	}
	public long getRegId() {
		return regId;
	}
	public void setRegId(long regId) {
		this.regId = regId;
	}
	public String getImgPath() {
		return imgPath;
	}
	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}
	public String getImgRecordStatus() {
		return imgRecordStatus;
	}
	public void setImgRecordStatus(String imgRecordStatus) {
		this.imgRecordStatus = imgRecordStatus;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public Date getCreateDtm() {
		return createDtm;
	}
	public void setCreateDtm(Date createDtm) {
		this.createDtm = createDtm;
	}
	public String getLastUpdUser() {
		return lastUpdUser;
	}
	public void setLastUpdUser(String lastUpdUser) {
		this.lastUpdUser = lastUpdUser;
	}
	public Date getLastUpdDtm() {
		return lastUpdDtm;
	}
	public void setLastUpdDtm(Date lastUpdDtm) {
		this.lastUpdDtm = lastUpdDtm;
	}
	
}
