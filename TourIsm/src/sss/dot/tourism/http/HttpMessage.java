package sss.dot.tourism.http;



import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.thoughtworks.xstream.XStream;

public abstract class HttpMessage
{
	private HttpServletResponse response;

	private XStream serializer;

	private XStream deserializer;

	private IWriteMessagePerform writeMsgPerform;

	public HttpMessage(HttpServletResponse response,
			IWriteMessagePerform writeMsgPerform)
	{
		this.response = response;
	}

	public HttpMessage()
	{

	}

	public abstract void write(Object object) throws IOException;
	
	public abstract String writePagingMessage(Object object, Number total) throws Exception;
	
	public abstract String writeErrorMessage(String error);
	
  public abstract String writeMessage(Object object) throws Exception;
 
  
  public abstract String writeMessage() throws Exception;
  public abstract String writeTree(List<?> list) throws IOException;

	public abstract String transform(Object object) throws IOException;
	
	public abstract Object[] toArrayObject(String registerName ,Class alias, String data);
	
	public abstract Object toObject(String registerName ,Class alias, String data);

	public abstract Object[] toArrayObject(Class alias, String data);

	public abstract Object toObject(Class alias, String data);



	public String serialize(Object obj) throws IOException
	{
		return this.serializer.toXML(obj);
	}

	public Object deserializeTo(String data)
	{
		return this.deserializer.fromXML(data);
	}

	public Object[] deserializeToArray(Class alias, String data)
	{
	    this.deserializer.alias("", alias);
	    return (Object[]) this.deserializer.fromXML(data);
	}
	
	public Object[] deserializeToArray(String data)
	{
	    return (Object[]) this.deserializer.fromXML(data);
	}

	public XStream getSerializer()
	{
		return serializer;
	}

	public void setSerializer(XStream serializer)
	{
		this.serializer = serializer;
	}

	public XStream getDeserializer()
	{
		return deserializer;
	}

	public void setDeserializer(XStream deserializer)
	{
		this.deserializer = deserializer;
	}

}
