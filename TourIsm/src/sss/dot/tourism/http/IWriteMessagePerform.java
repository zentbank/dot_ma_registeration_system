package sss.dot.tourism.http;



import java.io.IOException;
import java.nio.charset.Charset;

import javax.servlet.http.HttpServletResponse;

public interface IWriteMessagePerform {
	/**
	 * Constant of Json Content type
	 */
	public static final String JSON_CONTENT_TYPE = "text/html;charSet=UTF-8";
	/**
	 * Constant of XSL Content type
	 */
	public static final String XSL_CONTENT_TYPE = "application/vnd.ms-excel;charSet=UTF-8";
	/**
	 * Constant of XML Content type
	 */
	public static final String XML_CONTENT_TYPE = "application/vnd.ms-excel;charSet=UTF-8";
	/**
	 * Constant of Json Content type
	 */
	public static final Charset UTF8 = Charset.forName("UTF-8");
	public void writeMessage(HttpServletResponse response, String msg) throws IOException;
}
