package sss.dot.tourism.http;



import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

public class JSONPerform implements IWriteMessagePerform {

	public void writeMessage(HttpServletResponse response, String msg)
			throws IOException {
		
		ServletOutputStream out = response.getOutputStream();
		try {
			response.setContentType(JSON_CONTENT_TYPE);
			out.write(msg.getBytes(UTF8.toString()));
			out.flush();
		} finally {
			if (out != null)
				out.close();
		}

	}

}
