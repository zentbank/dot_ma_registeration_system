package sss.dot.tourism.http;



import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class XMLHttpMessage extends HttpMessage
{

	public XMLHttpMessage()
	{
		super();
	    XStream serializer = new XStream(new DomDriver());
	    XStream deserializer = new XStream(new DomDriver());

	    super.setSerializer(serializer);
	    super.setDeserializer(deserializer);
		
		
	}

	public XMLHttpMessage(HttpServletResponse response,
			IWriteMessagePerform writeMsgPerform)
	{
		super(response, writeMsgPerform);
		
	}
	
	public  String writeTree(List<?> list) throws IOException
	{
		return null;
		
	}
	@Override
	public void write(Object object) throws IOException
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public String transform(Object object) throws IOException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object[] toArrayObject(String registerName ,Class alias, String data)
	{
		XStream deserializer = super.getDeserializer();
		deserializer.alias(registerName, alias);
		return super.deserializeToArray(data);
	}

	/**
	 *  <XML>
 <STATUS>OK</STATUS>
 <DETAIL></DETAIL>
 <SMID>1501458711552</SMID>
 </XML>
	 */
	@Override
	public Object toObject(String registerName ,Class alias, String data)
	{
		XStream deserializer = super.getDeserializer();
		deserializer.alias(registerName, alias);
		super.setDeserializer(deserializer);
		return super.deserializeTo(data);
	}

	@Override
	public String writePagingMessage(Object object, Number total)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object[] toArrayObject(Class alias, String data) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object toObject(Class alias, String data) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String writeErrorMessage(String error) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String writeMessage(Object object) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String writeMessage() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}


}
