package sss.dot.tourism.joborder.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import sss.dot.tourism.controller.report.ReportExcelBuilder;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.joborder.dto.DotTrJobOrderDTO;
import sss.dot.tourism.joborder.dto.JobOrderDTO;
import sss.dot.tourism.joborder.service.IJobOrderService;


@Controller
@RequestMapping("/joborder")
public class JobOrderController {

	@Autowired
	private IJobOrderService jobOrderService;
	
	@RequestMapping(value = "/read", method = RequestMethod.POST)
	public @ResponseBody 
	String readPrintCardGuide(@ModelAttribute JobOrderDTO param)
	{
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try{
			
			
			List list = this.jobOrderService.getByPage(
					param, 
					param.getStart(), 
					param.getLimit());
			
			return jsonmsg.writePagingMessage(list, Long.valueOf(param.getTotalCount()));
			
		}catch (Exception e){
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	   @RequestMapping(value = "/docx/read" , method = RequestMethod.GET)
	    public void printJoborder(@RequestParam("jobId") long jobId,HttpServletRequest request, HttpServletResponse resp)
	    {
//	      	try {
//		    	
//	      		JobOrderDTO param = new JobOrderDTO();
//		    	param.setJobId(jobId);
//		    	this.jobOrderService.getPrintData(param, model);
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//			
//	        return "joborderDocx";
		   
			try
		    {
				JobOrderDTO param = new JobOrderDTO();
		    	param.setJobId(jobId);
		    	
				resp.setContentType("application/vnd.ms-excel");
				resp.setHeader("Content-Disposition",
						"attachment; filename=joborder.xls");
		
				String templateFileName = "joborder.xls";
				Map model = new HashMap();
				ReportExcelBuilder builder = new ReportExcelBuilder(templateFileName);
				
				this.jobOrderService.getPrintData(param, model);
				
				builder.build(resp, model);

		    }
			catch(Exception e)
			{
				e.printStackTrace();
			}
	    }
	
	   	@RequestMapping(value = "/printReportUploadFile" , method = RequestMethod.POST)
	    public void printReportUploadFile(@ModelAttribute DotTrJobOrderDTO param,HttpServletResponse resp){
	   		try{
	   			System.out.println("param: "+ToStringBuilder.reflectionToString(param,ToStringStyle.MULTI_LINE_STYLE));
				resp.setContentType("application/vnd.ms-excel");
				resp.setHeader("Content-Disposition","attachment; filename=UploadJobOrderData.xls");
		
				String templateFileName = "UploadJobOrderData.xls";
				ReportExcelBuilder builder = new ReportExcelBuilder(templateFileName);
				List<DotTrJobOrderDTO> jobOrderDtos = this.jobOrderService.prepareDataUploadFileJobOrder(param);
				
				Map model = new HashMap();
				model.put("jobOrderDtos", jobOrderDtos);
				builder.build(resp, model);

		    }
			catch(Exception e)
			{
				e.printStackTrace();
			}
	    }
	
	
}
