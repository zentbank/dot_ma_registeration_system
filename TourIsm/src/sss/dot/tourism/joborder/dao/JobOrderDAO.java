package sss.dot.tourism.joborder.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.JobOrder;
import sss.dot.tourism.joborder.dto.DotTrJobOrderDTO;
import sss.dot.tourism.joborder.dto.JobOrderDTO;

@Repository("jobOrderDAO")
public class JobOrderDAO extends BaseDAO{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4305356174219290099L;

	public JobOrderDAO()
	{
		this.domainObj = JobOrder.class;
	}
	
	public List<JobOrder> findAll(JobOrderDTO param)
	{
		ArrayList params = new ArrayList();
		
		StringBuilder hql = new StringBuilder();
		hql.append(" from  JobOrder as tr ");
		hql.append(" where tr.recordStatus = 'N' ");

		if ((param.getLicenseNo() != null) && (!param.getLicenseNo().equals(""))) {
			hql.append(" and tr.licenseNo = ? ");
			params.add(param.getLicenseNo());
		}
		
		List<JobOrder> list = (List<JobOrder>) this.getHibernateTemplate().find(hql.toString(), params.toArray());

		return list;
	}
	
	public List<Object[]> findAll(JobOrderDTO param,int start, int limit)
	  {
		   Map<String,Object> parames = new HashMap<String,Object>();
		    
		    StringBuilder hql = new StringBuilder();
		    
		    hql.append(" SELECT  ");
		    hql.append(" TR.LICENSE_NO AS LICENSE_NO ");
		    hql.append(" ,TR.TRADER_NAME AS TRADER_NAME ");
		    hql.append(" ,J.JOB_NO AS JOB_NO ");
		    hql.append(" ,J.JOB_DATE AS JOB_DATE ");
		    hql.append(" ,J.FOREIGN_NAME AS FOREIGN_NAME ");
		    hql.append(" ,J.FOREIGN_LICENSE_NO AS FOREIGN_LICENSE_NO ");
		    hql.append(" ,J.GUIDE_LICENSE_NO AS GUIDE_LICENSE_NO ");
		    hql.append(" ,J.GUIDE_NAME AS GUIDE_NAME ");
		    hql.append(" ,J.CREATE_DTM AS CREATE_DTM ");
		    hql.append(" ,J.JOB_ID AS JOB_ID ");
		    hql.append(" ,J.GUIDE_SALARY AS GUIDE_SALARY ");
		    hql.append("  FROM JOB_ORDER J ");
		    hql.append(" INNER JOIN TRADER TR ");
		    hql.append(" ON (J.LICENSE_NO = TR.LICENSE_NO) ");
		    hql.append(" WHERE  TR.RECORD_STATUS = 'N' ");
		    hql.append(" AND TR.BRANCH_TYPE IS NULL ");
		    hql.append(" AND J.RECORD_STATUS = 'N' ");
		    
		    
		    if((param.getLicenseNo() != null) && (!param.getLicenseNo().isEmpty()))
		    {
		    	hql.append("  AND TR.LICENSE_NO = :LICENSE_NO ");
		    	parames.put("LICENSE_NO", param.getLicenseNo());
		    }
		    
		    if((param.getTraderName() != null) && (!param.getTraderName().isEmpty()))
		    {
		    	hql.append("  AND TR.TRADER_NAME LIKE :TRADER_NAME ");
		    	parames.put("TRADER_NAME", "%"+param.getTraderName()+"%");
		    }
		    if(param.getJobId() > 0){
		    	hql.append("  AND J.JOB_ID = :JOB_ID ");
		    	parames.put("JOB_ID", param.getJobId());
		    }
		   
		    hql.append(" ORDER BY J.CREATE_DTM DESC; ");
		    
		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
		    sqlQuery.setFirstResult(start);
		    sqlQuery.setMaxResults(limit);
		    
		   
			
		    sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING); //0
		    sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING); //1
		    sqlQuery.addScalar("JOB_NO", Hibernate.STRING); //2
		    sqlQuery.addScalar("JOB_DATE", Hibernate.STRING); //3
		    sqlQuery.addScalar("FOREIGN_NAME", Hibernate.STRING); //4
		    sqlQuery.addScalar("FOREIGN_LICENSE_NO", Hibernate.STRING); //5
		    sqlQuery.addScalar("GUIDE_LICENSE_NO", Hibernate.STRING); //6
		    sqlQuery.addScalar("GUIDE_NAME", Hibernate.STRING); //7
		    sqlQuery.addScalar("CREATE_DTM", Hibernate.DATE); //8
		    sqlQuery.addScalar("JOB_ID", Hibernate.LONG); //9
		    sqlQuery.addScalar("GUIDE_SALARY", Hibernate.BIG_DECIMAL); //10
		    
		    sqlQuery.setProperties(parames);
		    List<Object[]>  result = sqlQuery.list();
	  
		    return result;
	  }
	
	
	public List<DotTrJobOrderDTO> findJobOrderDTO(DotTrJobOrderDTO dto) throws Exception{
		Map<String,Object> params = new HashMap<String,Object>();
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT JO.JOB_ORDER_NO, CONVERT(VARCHAR(10),JO.JOB_ORDER_DATE,103) AS JOB_ORDER_DATE, JO.BUSINESS_LICENSE_NO,  ");
		sql.append(" TR.TRADER_NAME AS BUSINESS_TRADER_NAME, ");
		sql.append(" JO.GUIDE_LICENSE_NO,JO.GUIDE_NAME, CONVERT(VARCHAR(10),FT.ARRIVAL_DATE,103) AS ARRIVAL_DATE, CONVERT(VARCHAR(10),FT.DEPARTURE_DATE,103) AS DEPARTURE_DATE, ");
		sql.append(" CT.COUNTRY_NAME AS TOURIST_PERSON_NATIONALITY, TP.TOURIST_PERSON_TOTAL, CONVERT(VARCHAR(10),PT.PLANTRIP_DATE,103) AS PLANTRIP_DATE, ");
		sql.append(" PT.PLANTRIP_PLACE, (SELECT PROVINCE_NAME FROM MAS_PROVINCE WHERE PROVINCE_ID = PT.PLANTRIP_PROVINCE) AS PLANTRIP_PROVINCE, ");
		sql.append(" PT.PLANTRIP_HOTEL_NAME, ");
		sql.append(" (SELECT PROVINCE_NAME FROM MAS_PROVINCE WHERE PROVINCE_ID = PT.PLANTRIP_HOTEL_PROVINCE) AS PLANTRIP_HOTEL_PROVINCE, ");
		sql.append(" OG.ORG_FULL_NAME ");
		sql.append(" FROM DOT_TR_JOB_ORDER JO ");
		sql.append(" LEFT OUTER JOIN DOT_TR_FLIGHT_TIME_TABLE FT ON JO.JOB_ORDER_ID = FT.JOB_ORDER_ID ");
		sql.append(" LEFT OUTER JOIN DOT_TR_TOURIST_PERSON TP ON JO.JOB_ORDER_ID = TP.JOB_ORDER_ID ");
		sql.append(" LEFT OUTER JOIN DOT_TR_PLANTRIP PT ON JO.JOB_ORDER_ID = PT.JOB_ORDER_ID ");
		sql.append(" INNER JOIN TRADER TR ON JO.BUSINESS_LICENSE_NO = TR.LICENSE_NO ");
		sql.append(" LEFT OUTER JOIN COUNTRY CT ON TP.TOURIST_PERSON_NATIONALITY = CT.COUNTRY_ID ");
		sql.append(" INNER JOIN ORGANIZATION OG ON TR.ORG_ID = OG.ORG_ID ");
		sql.append(" WHERE JO.IS_DELETED = 'N' ");
		sql.append(" AND TR.RECORD_STATUS = 'N' ");
		sql.append(" AND TR.LICENSE_STATUS = 'N' ");
		sql.append(" AND TR.TRADER_TYPE = 'B' ");
		sql.append(" AND TR.BRANCH_PARENT_ID IS NULL ");
		if(StringUtils.isNotBlank(dto.getBusinessLicenseNo()) && !"undefined".equals(dto.getBusinessLicenseNo())){
			sql.append(" AND TR.LICENSE_NO = :businessLicenseNo ");
			params.put("businessLicenseNo", dto.getBusinessLicenseNo());
		}
		if(StringUtils.isNotBlank(dto.getBusinessTraderName()) && !"undefined".equals(dto.getBusinessTraderName())){
			sql.append(" AND TR.TRADER_NAME LIKE :businessTraderName ");
			params.put("businessTraderName", "%"+dto.getBusinessTraderName()+"%");
		}
		if(null != dto.getOrganizationId()){
			sql.append(" AND OG.ORG_ID = :organizationId ");
			params.put("organizationId", dto.getOrganizationId());
		}
		sql.append(" ORDER BY JO.JOB_ORDER_NO ");
		
		SQLQuery query = getSession().createSQLQuery(sql.toString());
		query.addScalar("JOB_ORDER_NO", Hibernate.STRING); //-- 0
		query.addScalar("JOB_ORDER_DATE", Hibernate.STRING); //-- 1
		query.addScalar("BUSINESS_LICENSE_NO", Hibernate.STRING); //-- 2
		query.addScalar("BUSINESS_TRADER_NAME", Hibernate.STRING); //-- 3
		query.addScalar("GUIDE_LICENSE_NO", Hibernate.STRING); //-- 4
		query.addScalar("GUIDE_NAME", Hibernate.STRING); //-- 5
		query.addScalar("ARRIVAL_DATE", Hibernate.STRING); //-- 6
		query.addScalar("DEPARTURE_DATE", Hibernate.STRING); //-- 7
		query.addScalar("TOURIST_PERSON_NATIONALITY", Hibernate.STRING); //-- 8
		query.addScalar("TOURIST_PERSON_TOTAL", Hibernate.INTEGER); //-- 9
		query.addScalar("PLANTRIP_DATE", Hibernate.STRING); //-- 10
		query.addScalar("PLANTRIP_PLACE", Hibernate.STRING); //-- 11
		query.addScalar("PLANTRIP_PROVINCE", Hibernate.STRING); //-- 12
		query.addScalar("PLANTRIP_HOTEL_NAME", Hibernate.STRING); //-- 13
		query.addScalar("PLANTRIP_HOTEL_PROVINCE", Hibernate.STRING); //-- 14
		query.addScalar("ORG_FULL_NAME", Hibernate.STRING); //-- 15
		
		query.setProperties(params);
		
		List<DotTrJobOrderDTO> jobOrderDtos = new ArrayList<DotTrJobOrderDTO>();
		List<Object[]> objs = query.list(); 
		for(Object[] obj : objs){
			DotTrJobOrderDTO jobOrderDto = new DotTrJobOrderDTO();
			jobOrderDto.setJobOrderNo((String)obj[0]);
			jobOrderDto.setJobOrderDate((String)obj[1]);
			jobOrderDto.setBusinessLicenseNo((String)obj[2]);
			jobOrderDto.setBusinessTraderName((String)obj[3]);
			jobOrderDto.setGuideLicenseNo((String)obj[4]);
			jobOrderDto.setGuideName((String)obj[5]);
			jobOrderDto.setArrivalDate((String)obj[6]);
			jobOrderDto.setDepartureDate((String)obj[7]);
			jobOrderDto.setTouristPersonNationality((String)obj[8]);
			jobOrderDto.setTouristPersonTotal((Integer)obj[9]);
			jobOrderDto.setPlantripDate((String)obj[10]);
			jobOrderDto.setPlantripPlace((String)obj[11]);
			jobOrderDto.setPlantripProvinceName((String)obj[12]);
			jobOrderDto.setPlantripHotelName((String)obj[13]);
			jobOrderDto.setPlantripHotelProvinceName((String)obj[14]);
			jobOrderDto.setOrganizationName((String)obj[15]);
			
			jobOrderDtos.add(jobOrderDto);
		}
		return jobOrderDtos;
	}
}
