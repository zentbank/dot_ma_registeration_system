package sss.dot.tourism.joborder.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.JobOrder;
import sss.dot.tourism.domain.JobOrderItinerary;
import sss.dot.tourism.joborder.dto.JobOrderDTO;


@Repository("jobOrderItineraryDAO")
public class JobOrderItineraryDAO extends BaseDAO {


	/**
	 * 
	 */
	private static final long serialVersionUID = -2926907917211502552L;

	public JobOrderItineraryDAO()
	{
		this.domainObj = JobOrderItinerary.class;
	}
	
	public List<JobOrderItinerary> findAllItinerary(JobOrderDTO param)
	{
		ArrayList params = new ArrayList();
		
		StringBuilder hql = new StringBuilder();
		hql.append(" from  JobOrderItinerary as tr ");
		hql.append(" where tr.recordStatus = 'N' ");
		
		if(StringUtils.isNotEmpty(param.getItineraryType())){
			hql.append(" and tr.itineraryType = ? ");
			params.add(param.getItineraryType());
		}
		if(param.getJobId() > 0){
			hql.append(" and tr.jobOrder.jobId = ? ");
			params.add(param.getJobId());
		}

		List<JobOrderItinerary> list = (List<JobOrderItinerary>) this.getHibernateTemplate().find(hql.toString(), params.toArray());

		return list;
	}

}
