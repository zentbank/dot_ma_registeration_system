package sss.dot.tourism.joborder.dto;

import java.io.Serializable;
import java.util.Date;

public class DotTrJobOrderDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long jobOrderId;
	
	private String jobOrderNo;
	
	private String jobOrderDate;
	
	private String businessLicenseNo;
	
	private String businessTraderName;
	
	private String outboundBusinessTourismName;
	
	private String guideName;
	
	private String guideLicenseNo;
	
	private Double guideWage;
	
	private String arrivalDate;
	
	private String departureDate;
	
	private String touristPersonNationality;
	
	private Integer touristPersonTotal;
	
	private String plantripDate;
	
	private String plantripPlace;
	
	private String plantripProvinceName;
	
	private String plantripHotelName;
	
	private String plantripHotelProvinceName;
	
	private Long organizationId;
	
	private String organizationName;

	public Long getJobOrderId() {
		return jobOrderId;
	}

	public void setJobOrderId(Long jobOrderId) {
		this.jobOrderId = jobOrderId;
	}

	public String getJobOrderNo() {
		return jobOrderNo;
	}

	public void setJobOrderNo(String jobOrderNo) {
		this.jobOrderNo = jobOrderNo;
	}

	public String getJobOrderDate() {
		return jobOrderDate;
	}

	public void setJobOrderDate(String jobOrderDate) {
		this.jobOrderDate = jobOrderDate;
	}

	public String getBusinessLicenseNo() {
		return businessLicenseNo;
	}

	public void setBusinessLicenseNo(String businessLicenseNo) {
		this.businessLicenseNo = businessLicenseNo;
	}

	public String getBusinessTraderName() {
		return businessTraderName;
	}

	public void setBusinessTraderName(String businessTraderName) {
		this.businessTraderName = businessTraderName;
	}

	public String getOutboundBusinessTourismName() {
		return outboundBusinessTourismName;
	}

	public void setOutboundBusinessTourismName(String outboundBusinessTourismName) {
		this.outboundBusinessTourismName = outboundBusinessTourismName;
	}

	public String getGuideName() {
		return guideName;
	}

	public void setGuideName(String guideName) {
		this.guideName = guideName;
	}

	public String getGuideLicenseNo() {
		return guideLicenseNo;
	}

	public void setGuideLicenseNo(String guideLicenseNo) {
		this.guideLicenseNo = guideLicenseNo;
	}

	public Double getGuideWage() {
		return guideWage;
	}

	public void setGuideWage(Double guideWage) {
		this.guideWage = guideWage;
	}

	public String getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public String getTouristPersonNationality() {
		return touristPersonNationality;
	}

	public void setTouristPersonNationality(String touristPersonNationality) {
		this.touristPersonNationality = touristPersonNationality;
	}

	public Integer getTouristPersonTotal() {
		return touristPersonTotal;
	}

	public void setTouristPersonTotal(Integer touristPersonTotal) {
		this.touristPersonTotal = touristPersonTotal;
	}

	public String getPlantripDate() {
		return plantripDate;
	}

	public void setPlantripDate(String plantripDate) {
		this.plantripDate = plantripDate;
	}

	public String getPlantripPlace() {
		return plantripPlace;
	}

	public void setPlantripPlace(String plantripPlace) {
		this.plantripPlace = plantripPlace;
	}

	public String getPlantripProvinceName() {
		return plantripProvinceName;
	}

	public void setPlantripProvinceName(String plantripProvinceName) {
		this.plantripProvinceName = plantripProvinceName;
	}

	public String getPlantripHotelName() {
		return plantripHotelName;
	}

	public void setPlantripHotelName(String plantripHotelName) {
		this.plantripHotelName = plantripHotelName;
	}

	public String getPlantripHotelProvinceName() {
		return plantripHotelProvinceName;
	}

	public void setPlantripHotelProvinceName(String plantripHotelProvinceName) {
		this.plantripHotelProvinceName = plantripHotelProvinceName;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public Long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	
		
}
