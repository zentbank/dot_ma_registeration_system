package sss.dot.tourism.joborder.dto;

import java.io.Serializable;

import sss.dot.tourism.domain.JobOrder;

public class JobOrderHotelDTO implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 245691659713872995L;
	private long hotelId;
	private Integer roomType1;
	private Integer roomType2;
	private Integer roomType3;
	private String remark;
	private Integer totalRoom;
	private String tourisType;
	private Integer totalTouris;
	
	public Integer getTotalTouris() {
		return totalTouris;
	}
	public void setTotalTouris(Integer totalTouris) {
		this.totalTouris = totalTouris;
	}
	public long getHotelId() {
		return hotelId;
	}
	public void setHotelId(long hotelId) {
		this.hotelId = hotelId;
	}
	public String getTourisType() {
		return tourisType;
	}
	public void setTourisType(String tourisType) {
		this.tourisType = tourisType;
	}
	public Integer getRoomType1() {
		return roomType1;
	}
	public void setRoomType1(Integer roomType1) {
		this.roomType1 = roomType1;
	}
	public Integer getRoomType2() {
		return roomType2;
	}
	public void setRoomType2(Integer roomType2) {
		this.roomType2 = roomType2;
	}
	public Integer getRoomType3() {
		return roomType3;
	}
	public void setRoomType3(Integer roomType3) {
		this.roomType3 = roomType3;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Integer getTotalRoom() {
		return totalRoom;
	}
	public void setTotalRoom(Integer totalRoom) {
		this.totalRoom = totalRoom;
	}
	
	
}
