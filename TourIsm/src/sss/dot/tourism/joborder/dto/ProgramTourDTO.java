package sss.dot.tourism.joborder.dto;

import java.io.Serializable;
import java.util.Comparator;

public class ProgramTourDTO  implements Serializable,Comparator<ProgramTourDTO> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6728826228749250151L;
	private long progId;
	private String progDate;
	private String progTime;
	private String progFood;
	private String progDesc;
	private String progHotel;
	private String createUser;
	
	
	
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public long getProgId() {
		return progId;
	}
	public void setProgId(long progId) {
		this.progId = progId;
	}
	public String getProgDate() {
		return progDate;
	}
	public void setProgDate(String progDate) {
		this.progDate = progDate;
	}
	public String getProgTime() {
		return progTime;
	}
	public void setProgTime(String progTime) {
		this.progTime = progTime;
	}
	public String getProgFood() {
		return progFood;
	}
	public void setProgFood(String progFood) {
		this.progFood = progFood;
	}
	public String getProgDesc() {
		return progDesc;
	}
	public void setProgDesc(String progDesc) {
		this.progDesc = progDesc;
	}
	public String getProgHotel() {
		return progHotel;
	}
	public void setProgHotel(String progHotel) {
		this.progHotel = progHotel;
	}
	@Override
    public int compare(ProgramTourDTO o1, ProgramTourDTO o2) {
	 int number1 = 0;
	 int number2 =0;
		try{
			number1 = Integer.parseInt(o1.getCreateUser());
		}catch(Exception e){}
		try{
			number2 = Integer.parseInt(o2.getCreateUser());
		}catch(Exception e){}
		
		if(number2 <  number1) return 1;
	       if(number1 == number2) return 0;
	       return -1;
		
    }
	
}
