package sss.dot.tourism.joborder.service;

import java.util.List;
import java.util.Map;

import sss.dot.tourism.joborder.dto.DotTrJobOrderDTO;

public interface IJobOrderService {

	public List getByPage(Object object, int start, int limit) throws Exception;
	public void getPrintData(Object object, Map model ) throws Exception;
	
	public List<DotTrJobOrderDTO> prepareDataUploadFileJobOrder(DotTrJobOrderDTO dto) throws Exception;
}
