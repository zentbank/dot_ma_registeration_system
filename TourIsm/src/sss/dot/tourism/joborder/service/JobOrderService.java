package sss.dot.tourism.joborder.service;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.domain.JobOrderHotel;
import sss.dot.tourism.domain.JobOrderItinerary;
import sss.dot.tourism.domain.TourisCustomer;
import sss.dot.tourism.domain.TravelProgram;
import sss.dot.tourism.joborder.dao.JobOrderDAO;
import sss.dot.tourism.joborder.dao.JobOrderHotelDAO;
import sss.dot.tourism.joborder.dao.JobOrderItineraryDAO;
import sss.dot.tourism.joborder.dao.TourisCustomerDAO;
import sss.dot.tourism.joborder.dao.TravelProgramDAO;
import sss.dot.tourism.joborder.dto.DotTrJobOrderDTO;
import sss.dot.tourism.joborder.dto.JobOrderDTO;
import sss.dot.tourism.joborder.dto.JobOrderHotelDTO;
import sss.dot.tourism.joborder.dto.LocalItinerayDTO;
import sss.dot.tourism.joborder.dto.ProgramTourDTO;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.ObjectUtil;

@Repository("jobOrderService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class JobOrderService implements IJobOrderService {

	@Autowired
	private JobOrderDAO jobOrderDAO;
	@Autowired
	private JobOrderHotelDAO jobOrderHotelDAO;
	@Autowired
	private JobOrderItineraryDAO jobOrderItineraryDAO;
	@Autowired
	private TravelProgramDAO travelProgramDAO;
	@Autowired
	private TraderDAO traderDAO;
	@Autowired
	private TourisCustomerDAO tourisCustomerDAO;
	
	@Override
	public List getByPage(Object object, int start, int limit) throws Exception {
		if (!(object instanceof JobOrderDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		List<JobOrderDTO> list = new ArrayList<JobOrderDTO>();
		
		JobOrderDTO param = (JobOrderDTO)object;
		
		List<Object[]> listAll = (List<Object[]>)this.jobOrderDAO.findAll(param, start, limit);
		
		param.setTotalCount(listAll.size());
		
		if(!listAll.isEmpty()){
			for(Object[] obj: listAll){
				JobOrderDTO dto = new JobOrderDTO();
			
				
				dto.setJobId(Long.valueOf(obj[9].toString()));
				dto.setLicenseNo(obj[0]==null?"":obj[0].toString());
				dto.setTraderName(obj[1]==null?"":obj[1].toString());
				dto.setJobNo(obj[2]==null?"":obj[2].toString());
				dto.setJobDate(obj[3]==null?"":obj[3].toString());
				dto.setForeignName(obj[4]==null?"":obj[4].toString());
				dto.setForeignLicenseNo(obj[5]==null?"":obj[5].toString());
				dto.setGuideLicenseNo(obj[6]==null?"":obj[6].toString());
				dto.setGuideName(obj[7]==null?"":obj[7].toString());
				DateFormat df = DateUtils.getProcessDateFormatThai();
				dto.setWriteDate(df.format(obj[8]));
				
				list.add(dto);
			}
		}
		
		return list;
	}
	@Override
	public void getPrintData(Object object, Map model) throws Exception {
		if (!(object instanceof JobOrderDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		JobOrderDTO param = (JobOrderDTO)object;
		
		List<Object[]> listAll = (List<Object[]>)this.jobOrderDAO.findAll(param, 0, Integer.MAX_VALUE);
		
		
		if(!listAll.isEmpty()){
			
			Object[] obj = listAll.get(0);
			JobOrderDTO dto = new JobOrderDTO();
			
			dto.setJobId(Long.valueOf(obj[9].toString()));
			dto.setLicenseNo(obj[0]==null?"":obj[0].toString());
			dto.setTraderName(obj[1]==null?"":obj[1].toString());
			dto.setTourisName(dto.getTraderName());
			dto.setJobNo(obj[2]==null?"":obj[2].toString());
			dto.setJobDate(obj[3]==null?"":obj[3].toString());
			dto.setForeignName(obj[4]==null?"":obj[4].toString());
			dto.setForeignLicenseNo(obj[5]==null?"":obj[5].toString());
			dto.setGuideLicenseNo(obj[6]==null?"":obj[6].toString());
			dto.setGuideName(obj[7]==null?"":obj[7].toString());
			DateFormat df = DateUtils.getProcessDateFormatThai();
			dto.setWriteDate(df.format(obj[8]));
			dto.setGuideSalary(obj[10]==null?new BigDecimal(0): new BigDecimal(obj[10].toString()));
			
			this.getItinerary(dto);
			model.put("joborder", dto);
			
			List local = this.getLocalItineraryAll(dto);
			model.put("locals", local);
			
			List hotel = this.getHotel(dto);
			model.put("listhotel", hotel);
			
			List programs = this.getTravelProgramAll(dto);
			model.put("lists", programs);
			
		}
	}
	
	private void getItinerary(JobOrderDTO dto) throws Exception {
		
		dto.setItineraryType("I");
		List<JobOrderItinerary> listAll = this.jobOrderItineraryDAO.findAllItinerary(dto);
		
		if(!listAll.isEmpty()){
			for(JobOrderItinerary itr: listAll){
				if(StringUtils.isNotEmpty(itr.getArriveTime())){
					dto.setArriveId(itr.getItrId());
					dto.setArriveBy(itr.getItineraryBy());
					dto.setArriveDate(itr.getItineraryDate());
					dto.setArriveFlightNo(itr.getItineraryFlight());
					dto.setArriveFrom(itr.getItineraryFrom());
					dto.setArriveTime(itr.getArriveTime());
				}else{
					dto.setLeaveId(itr.getItrId());
					dto.setLeaveBy(itr.getItineraryBy());
					dto.setLeaveDate(itr.getItineraryDate());
					dto.setLeaveFlightNo(itr.getItineraryFlight());
					dto.setLeaveTime(itr.getLeaveTime());
				}
			}
		}
	}
	private List getLocalItineraryAll(JobOrderDTO param) throws Exception {
		
		param.setItineraryType("D");
		List<JobOrderItinerary> listAll = this.jobOrderItineraryDAO.findAllItinerary(param);
		
		List<LocalItinerayDTO> list = new ArrayList<LocalItinerayDTO>();
		
		if(!listAll.isEmpty()){
			for(JobOrderItinerary itr: listAll){
				
				LocalItinerayDTO dto = new LocalItinerayDTO();
				
				ObjectUtil.copy(itr, dto);
				list.add(dto);
			}
		}

		return list;
	}
	
	public List getHotel(JobOrderDTO param) throws Exception {
		
		List<JobOrderHotel> listAll = (List<JobOrderHotel>)this.jobOrderHotelDAO.findHotel(param);
		
		List<JobOrderHotelDTO> list = new ArrayList<JobOrderHotelDTO>();
		
		JobOrderHotelDTO totalRoom = new JobOrderHotelDTO();
		totalRoom.setTourisType("รวม");
		totalRoom.setRoomType1(0);
		totalRoom.setRoomType2(0);
		totalRoom.setRoomType3(0);
		totalRoom.setTotalRoom(0);
		totalRoom.setTotalTouris(0);
		
		if(!listAll.isEmpty()){
			
			for(JobOrderHotel hotel: listAll){
				JobOrderHotelDTO dto = new JobOrderHotelDTO();
				ObjectUtil.copy(hotel, dto);
				
				dto.setRoomType1(dto.getRoomType1()==null?0:dto.getRoomType1());
				dto.setRoomType2(dto.getRoomType2()==null?0:dto.getRoomType2());
				dto.setRoomType3(dto.getRoomType3()==null?0:dto.getRoomType3());
				dto.setTotalTouris(dto.getTotalTouris()==null?0:dto.getTotalTouris());
				
				int totalRoomType = dto.getRoomType1()+dto.getRoomType2()+dto.getRoomType3();
				dto.setTotalRoom(totalRoomType);
				
				totalRoom.setRoomType1(totalRoom.getRoomType1()+dto.getRoomType1());
				totalRoom.setRoomType2(totalRoom.getRoomType2()+dto.getRoomType2());
				totalRoom.setRoomType3(totalRoom.getRoomType3()+dto.getRoomType3());
				totalRoom.setTotalRoom(totalRoom.getTotalRoom()+totalRoomType);
				
				
				if(dto.getTourisType().equals("A")){
					dto.setTourisType("ผู้ใหญ่");
					
					param.setTourisType("A");
					List<TourisCustomer> listTouris = this.tourisCustomerDAO.findAllTourisCustomer(param);
					if(!listTouris.isEmpty()){
						dto.setTotalTouris(listTouris.size());
					}
					
				}else if(dto.getTourisType().equals("C")){
					dto.setTourisType("เด็ก");
					
					param.setTourisType("C");
					List<TourisCustomer> listTouris = this.tourisCustomerDAO.findAllTourisCustomer(param);
					if(!listTouris.isEmpty()){
						dto.setTotalTouris(listTouris.size());
					}
					
				}else if(dto.getTourisType().equals("S")){
					dto.setTourisType("ผู้ติดตาม");
					
					param.setTourisType("S");
					List<TourisCustomer> listTouris = this.tourisCustomerDAO.findAllTourisCustomer(param);
					if(!listTouris.isEmpty()){
						dto.setTotalTouris(listTouris.size());
					}
				}
				
				totalRoom.setTotalTouris(totalRoom.getTotalTouris()+ dto.getTotalTouris());
				
				list.add(dto);
			}
			
			
		}
		list.add(totalRoom);
		return list;
	}
	
	public List getTravelProgramAll(JobOrderDTO param) throws Exception {
		
		List<TravelProgram> listAll = (List<TravelProgram>)this.travelProgramDAO.findAllProgram(param);
		
		List<ProgramTourDTO> list = new ArrayList<ProgramTourDTO>();
		if(!listAll.isEmpty()){
			for(TravelProgram p: listAll){
				ProgramTourDTO dto = new ProgramTourDTO();
				ObjectUtil.copy(p, dto);
				list.add(dto);
			}
			Collections.sort(list, new ProgramTourDTO());
		}
		return list;
	}
	
	@Override
	public List<DotTrJobOrderDTO> prepareDataUploadFileJobOrder(DotTrJobOrderDTO dto) throws Exception {
		// TODO Auto-generated method stub
		return jobOrderDAO.findJobOrderDTO(dto);
	}
}
