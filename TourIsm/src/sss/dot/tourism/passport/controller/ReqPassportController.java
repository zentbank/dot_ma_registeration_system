package sss.dot.tourism.passport.controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import sss.dot.tourism.controller.report.ReportExcelBuilder;
import sss.dot.tourism.dto.certificate.ReqLicenseCertificateDTO;
import sss.dot.tourism.dto.punishment.SuspensionLicenseDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.passport.dto.ReqPassportDTO;
import sss.dot.tourism.passport.service.IReqPassportService;

import com.sss.aut.service.User;

@Controller
@RequestMapping("/req/passport")
public class ReqPassportController {

	@Autowired
	private IReqPassportService reqPassportService;
	
	
	@Autowired
	private HttpServletRequest req;
	
	@RequestMapping(value = "/readByLeaderLicense", method = RequestMethod.POST)
	public @ResponseBody String readByLeaderLicense(@ModelAttribute ReqPassportDTO param) {
	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			Object obj = reqPassportService.getLeaderLicense(param, user);
			
			return jsonmsg.writeMessage(obj);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public @ResponseBody
	String save(@ModelAttribute ReqPassportDTO param ) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			User user = (User) req.getSession(true).getAttribute("user");
			this.reqPassportService.saveAll(param ,user);

			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
			// return null;
		}
	}
	
	@RequestMapping(value = "/readDocument", method = RequestMethod.POST)
	public @ResponseBody
	String readDocument(@ModelAttribute ReqPassportDTO param) {

	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			List list = reqPassportService.getDocuments(param, user);
			
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/read", method = RequestMethod.POST)
	public @ResponseBody String read(@ModelAttribute ReqPassportDTO param ) 
	{
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			
			User user = (User) req.getSession(true).getAttribute("user");

			param.setOrgId(user.getUserData().getOrgId());
			
			Long numAll = new Long(reqPassportService.countAll(param, user, param.getStart(), param.getLimit()));
			
			List list = this.reqPassportService.getByPage(param, user, param.getStart(), param.getLimit());
			
			return jsonmsg.writePagingMessage(list, numAll);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/readById", method = RequestMethod.POST)
	public @ResponseBody
	String readById(@ModelAttribute ReqPassportDTO param) {

	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			Object obj = reqPassportService.getById(param, user);
			
			return jsonmsg.writeMessage(obj);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/saveDocument", method = RequestMethod.POST)
	public @ResponseBody String saveDocument(@RequestParam("documentFile") MultipartFile documentFile 
			,@RequestParam("reqDocId") long reqDocId) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			
			User user = (User) req.getSession(true).getAttribute("user");
			this.reqPassportService.saveDocument(reqDocId, documentFile, user);
			
			ReqLicenseCertificateDTO param = new ReqLicenseCertificateDTO();
			
			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/viewReqDoc", method = RequestMethod.GET)
	public void viewfile(HttpServletRequest req, HttpServletResponse resp,@RequestParam("reqPassDocId") long reqPassDocId){
		
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			ReqPassportDTO param = new ReqPassportDTO();
			param.setReqPassDocId(reqPassDocId);
			ReqPassportDTO dto = (ReqPassportDTO)this.reqPassportService.getDocumentsById(param, user);
		
			
			String fileType = dto.getReqDocPath().substring(dto.getReqDocPath().lastIndexOf(".") + 1, dto.getReqDocPath().length());
			
			if(!fileType.equals("pdf"))
			{
				File f = new File(dto.getReqDocPath());
			
				resp.setContentType("image/" + fileType);
				BufferedImage bi = ImageIO.read(f);
				OutputStream out = resp.getOutputStream();
				ImageIO.write(bi, fileType, out);
				out.close();
			}
			else
			{
				File pdfFile = new File(dto.getReqDocPath());
				
				String fileName = dto.getReqDocPath().substring(dto.getReqDocPath().lastIndexOf("/") + 1, dto.getReqDocPath().length());

				resp.setContentType("application/pdf");
				resp.addHeader("Content-Disposition", "attachment; filename=" + fileName);
				resp.setContentLength((int) pdfFile.length());

				FileInputStream fileInputStream = new FileInputStream(pdfFile);
				OutputStream responseOutputStream = resp.getOutputStream();
				int bytes;
				while ((bytes = fileInputStream.read()) != -1) {
					responseOutputStream.write(bytes);
				
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
	
		}
	}
	
    @RequestMapping(value = "/docx/passportDocx" , method = RequestMethod.GET)
    public String suspensionDocx(@RequestParam("reqPassId") long reqPassId, ModelMap model )
    {
      	try {
	    	User user = (User) req.getSession(true).getAttribute("user");
	    	
	    	ReqPassportDTO param = new ReqPassportDTO();
	    	param.setReqPassId(reqPassId);
	    	
	    	Object dto = this.reqPassportService.getReqPassportDocx(param, user);
	    	model.addAttribute("reqpassport", dto);
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		
        return "passportDocx";
    }
    
	@RequestMapping(value = "/readexcel", method = RequestMethod.GET)
	public  ModelAndView reportFeeTotalExcel (HttpServletRequest request, HttpServletResponse resp,
			@RequestParam("leaderLicenseNo") String leaderLicenseNo,
			@RequestParam("reqIdCard") String reqIdCard,
			@RequestParam("reqDateFrom") String reqDateFrom,
			@RequestParam("reqDateTo")String reqDateTo,
			@RequestParam("guideLicenseNo")String guideLicenseNo,
			@RequestParam("passportNo")String passportNo,
			@RequestParam("personName")String personName) throws Exception
	{
		User user = (User) request.getSession(true).getAttribute("user");
		
		ReqPassportDTO param = new ReqPassportDTO();
		
		param.setLeaderLicenseNo(leaderLicenseNo);
		param.setReqIdCard(reqIdCard);
		param.setReqDateFrom(reqDateFrom);
		param.setReqDateTo(reqDateTo);
		param.setGuideLicenseNo(guideLicenseNo);
		param.setPassportNo(passportNo);
		param.setPersonName(personName);

		resp.setContentType("application/vnd.ms-excel");
		resp.setHeader("Content-Disposition",
				"attachment; filename=รายงานการขอหนังสือเดินทางเล่ม2.xls");

		String templateFileName = "reqpassport.xls";
		ReportExcelBuilder builder = new ReportExcelBuilder(
				templateFileName);
		builder.build(resp,this.reqPassportService.getReqPassportExcel(param, user));

		return null;
	}
}
