package sss.dot.tourism.passport.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.DeactivateDetail;
import sss.dot.tourism.domain.ReqPassport;
import sss.dot.tourism.passport.dto.ReqPassportDTO;
import sss.dot.tourism.util.DateUtils;

import com.sss.aut.service.User;

import fr.opensagres.xdocreport.utils.StringUtils;

@Repository("reqPassportDAO")
public class ReqPassportDAO extends BaseDAO {
	
	public ReqPassportDAO() {
		this.domainObj = ReqPassport.class;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<ReqPassport> findByPaging(ReqPassportDTO dto, int start, int limit) throws Exception{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  ReqPassport as dep ");
		hql.append(" where dep.recordStatus = 'N' ");
		
		if(StringUtils.isNotEmpty(dto.getPersonName())){
			hql.append(" and dep.personName like ? ");
			params.add("%"+dto.getPersonName()+"%");
		}
		
		if (StringUtils.isNotEmpty(dto.getReqDateFrom())) {
			if (StringUtils.isNotEmpty(dto.getReqDateTo())) {
				
				hql.append(" and (dep.reqDate >=  convert(datetime, ? ,20) and dep.reqDate <  convert(datetime, ? ,20)  ) ");
		

				String sendDtmFrom = DateUtils.getDateMSSQLFormat(
						dto.getReqDateFrom(), "00:00");
				String sendDtmTo = DateUtils.getDateMSSQLFormat(
						dto.getReqDateTo(), "23:59");
				
				params.add(sendDtmFrom);
				params.add(sendDtmTo);
			}
		}
		
		if(StringUtils.isNotEmpty(dto.getLeaderLicenseNo())){
			hql.append(" and dep.leaderLicenseNo = ? ");
			params.add(dto.getLeaderLicenseNo());
		}
		
		if(StringUtils.isNotEmpty(dto.getGuideLicenseNo())){
			hql.append(" and dep.guideLicenseNo = ? ");
			params.add(dto.getGuideLicenseNo());
		}
		
		return (List<ReqPassport>)this.findByAddPaging(hql.toString(), start, limit, params.toArray());
	  }

}
