package sss.dot.tourism.passport.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.MasDocument;
import sss.dot.tourism.domain.ReqPassportDoc;
import sss.dot.tourism.passport.dto.ReqPassportDTO;
import sss.dot.tourism.util.RegistrationType;
import sss.dot.tourism.util.TraderType;

@Repository("reqPassportDocDAO")
public class ReqPassportDocDAO extends BaseDAO {

	public ReqPassportDocDAO() {
		this.domainObj = ReqPassportDoc.class;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<MasDocument> findMasDoc()
	  {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  MasDocument as dep ");
		hql.append(" where dep.recordStatus = 'N' ");

		hql.append(" and dep.licenseType = ? ");
		params.add(TraderType.LEADER.getStatus());
		hql.append(" and dep.registrationType = ? ");
		params.add(RegistrationType.REQ_TOUR_LEADER_PASSPORT.getStatus());

		hql.append(" order by dep.docIndex ");
		
		return (List<MasDocument>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	  }
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<ReqPassportDoc> findAll(ReqPassportDTO dto)
	  {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  ReqPassportDoc as dep ");
		hql.append(" where dep.recordStatus = 'N' ");
		
		if(dto.isReqPassIdNotEmpty()){
			hql.append(" and dep.reqPassport.reqPassId = ? ");
			params.add(dto.getReqPassId());
		}

		hql.append(" order by dep.masDocument.docIndex ");
		
		return (List<ReqPassportDoc>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	  }
}
