package sss.dot.tourism.passport.dto;

import java.io.Serializable;
import java.util.Date;

import sss.dot.tourism.domain.MasDocument;
import sss.dot.tourism.domain.ReqPassport;
import sss.dot.tourism.dto.registration.RegistrationDTO;

public class ReqPassportDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6123738483620078760L;
	private long reqPassId;
	private String bookNo;
	private String bookDate;
	private String bookDocToManagerNo;
	private String bookDocManagerDate;
	private String bookDocToDirectorNo;
	private String bookDocToDirectorDate;
	private String passportNo;
	private String reqIdCard;
	private String agentsAssociation;
	private String tourLicenseNo;
	private String guideLicenseNo;
	private String leaderLicenseNo;
	private String reqDateFrom;
	private String reqDateTo;
	private String reqDate;
	
	private String ttAaNo;
	private String ttAaDate;
	
	private long reqPassDocId;
	private String reqDocPath;
	
	private long masDocId;
	private Integer docIndex;
	private String documentName;

	private long orgId;
	private String orgName;
	/**
	 * sss.dot.tourism.domain.Trader
	 */
	private long traderId;
	private String traderType;
	private String traderTypeName;
	
	private String traderName;
	private String traderOwnerName;
	private String traderNameEn;
	private String traderCategory;
	private String traderCategoryName;
	
	
	/**
	 * sss.dot.tourism.domain.Person
	 */
	private long personId;
	private String personType;
	private String identityNo;
	private String firstName;
	private String lastName;
	private String personName;
	
	private long traderGuideId;
	private String expireDate;
	private long traderLeaderId;
	
	private int page;
	private int start;
	private int limit;
	public long getReqPassId() {
		return reqPassId;
	}
	public void setReqPassId(long reqPassId) {
		this.reqPassId = reqPassId;
	}
	public boolean isReqPassIdNotEmpty() {
		return reqPassId > 0;
	}
	public String getBookNo() {
		return bookNo;
	}
	public void setBookNo(String bookNo) {
		this.bookNo = bookNo;
	}
	public String getBookDate() {
		return bookDate;
	}
	public void setBookDate(String bookDate) {
		this.bookDate = bookDate;
	}
	public String getPassportNo() {
		return passportNo;
	}
	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}
	public String getReqIdCard() {
		return reqIdCard;
	}
	public void setReqIdCard(String reqIdCard) {
		this.reqIdCard = reqIdCard;
	}
	public String getAgentsAssociation() {
		return agentsAssociation;
	}
	public void setAgentsAssociation(String agentsAssociation) {
		this.agentsAssociation = agentsAssociation;
	}
	public String getTourLicenseNo() {
		return tourLicenseNo;
	}
	public void setTourLicenseNo(String tourLicenseNo) {
		this.tourLicenseNo = tourLicenseNo;
	}
	public String getGuideLicenseNo() {
		return guideLicenseNo;
	}
	public void setGuideLicenseNo(String guideLicenseNo) {
		this.guideLicenseNo = guideLicenseNo;
	}
	public String getLeaderLicenseNo() {
		return leaderLicenseNo;
	}
	public void setLeaderLicenseNo(String leaderLicenseNo) {
		this.leaderLicenseNo = leaderLicenseNo;
	}
	public long getReqPassDocId() {
		return reqPassDocId;
	}
	public void setReqPassDocId(long reqPassDocId) {
		this.reqPassDocId = reqPassDocId;
	}
	public String getReqDocPath() {
		return reqDocPath;
	}
	public void setReqDocPath(String reqDocPath) {
		this.reqDocPath = reqDocPath;
	}
	public long getMasDocId() {
		return masDocId;
	}
	public void setMasDocId(long masDocId) {
		this.masDocId = masDocId;
	}
	public Integer getDocIndex() {
		return docIndex;
	}
	public void setDocIndex(Integer docIndex) {
		this.docIndex = docIndex;
	}
	public String getDocumentName() {
		return documentName;
	}
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	public long getOrgId() {
		return orgId;
	}
	public void setOrgId(long orgId) {
		this.orgId = orgId;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public long getTraderId() {
		return traderId;
	}
	public void setTraderId(long traderId) {
		this.traderId = traderId;
	}
	public String getTraderType() {
		return traderType;
	}
	public void setTraderType(String traderType) {
		this.traderType = traderType;
	}
	public String getTraderTypeName() {
		return traderTypeName;
	}
	public void setTraderTypeName(String traderTypeName) {
		this.traderTypeName = traderTypeName;
	}
	public String getTraderName() {
		return traderName;
	}
	public void setTraderName(String traderName) {
		this.traderName = traderName;
	}
	public String getTraderOwnerName() {
		return traderOwnerName;
	}
	public void setTraderOwnerName(String traderOwnerName) {
		this.traderOwnerName = traderOwnerName;
	}
	public String getTraderNameEn() {
		return traderNameEn;
	}
	public void setTraderNameEn(String traderNameEn) {
		this.traderNameEn = traderNameEn;
	}
	public String getTraderCategory() {
		return traderCategory;
	}
	public void setTraderCategory(String traderCategory) {
		this.traderCategory = traderCategory;
	}
	public String getTraderCategoryName() {
		return traderCategoryName;
	}
	public void setTraderCategoryName(String traderCategoryName) {
		this.traderCategoryName = traderCategoryName;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public String getReqDateFrom() {
		return reqDateFrom;
	}
	public void setReqDateFrom(String reqDateFrom) {
		this.reqDateFrom = reqDateFrom;
	}
	public String getReqDateTo() {
		return reqDateTo;
	}
	public void setReqDateTo(String reqDateTo) {
		this.reqDateTo = reqDateTo;
	}
	public long getPersonId() {
		return personId;
	}
	public void setPersonId(long personId) {
		this.personId = personId;
	}
	public String getPersonType() {
		return personType;
	}
	public void setPersonType(String personType) {
		this.personType = personType;
	}
	public String getIdentityNo() {
		return identityNo;
	}
	public void setIdentityNo(String identityNo) {
		this.identityNo = identityNo;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public long getTraderGuideId() {
		return traderGuideId;
	}
	public void setTraderGuideId(long traderGuideId) {
		this.traderGuideId = traderGuideId;
	}
	public long getTraderLeaderId() {
		return traderLeaderId;
	}
	public void setTraderLeaderId(long traderLeaderId) {
		this.traderLeaderId = traderLeaderId;
	}
	public String getExpireDate() {
		return expireDate;
	}
	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}
	public String getReqDate() {
		return reqDate;
	}
	public void setReqDate(String reqDate) {
		this.reqDate = reqDate;
	}
	public String getTtAaNo() {
		return ttAaNo;
	}
	public void setTtAaNo(String ttAaNo) {
		this.ttAaNo = ttAaNo;
	}
	public String getTtAaDate() {
		return ttAaDate;
	}
	public void setTtAaDate(String ttAaDate) {
		this.ttAaDate = ttAaDate;
	}
	public String getBookDocToManagerNo() {
		return bookDocToManagerNo;
	}
	public void setBookDocToManagerNo(String bookDocToManagerNo) {
		this.bookDocToManagerNo = bookDocToManagerNo;
	}
	public String getBookDocManagerDate() {
		return bookDocManagerDate;
	}
	public void setBookDocManagerDate(String bookDocManagerDate) {
		this.bookDocManagerDate = bookDocManagerDate;
	}
	public String getBookDocToDirectorNo() {
		return bookDocToDirectorNo;
	}
	public void setBookDocToDirectorNo(String bookDocToDirectorNo) {
		this.bookDocToDirectorNo = bookDocToDirectorNo;
	}
	public String getBookDocToDirectorDate() {
		return bookDocToDirectorDate;
	}
	public void setBookDocToDirectorDate(String bookDocToDirectorDate) {
		this.bookDocToDirectorDate = bookDocToDirectorDate;
	}
	
	
	
}
