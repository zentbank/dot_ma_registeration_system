package sss.dot.tourism.passport.service;

import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.sss.aut.service.User;

public interface IReqPassportService {
	
	public Object getById(Object obj, User user) throws Exception;
	
	public void saveAll(Object obj, User user)throws Exception;
	
	public void delete(Object obj, User user)throws Exception;
	
	
	public List getByPage(Object object, User user ,int start, int limit) throws Exception;
	
	public int countAll(Object object, User user ,int start, int limit) throws Exception;
	
	public List getDocuments(Object obj, User user)throws Exception;
	
	public void saveDocument(long reqDocId,MultipartFile documentFile, User user)throws Exception;
	
	public Object getDocumentsById(Object obj, User user)throws Exception;
	
	public Object getLeaderLicense(Object obj, User user)throws Exception;
	
	public Object  getReqPassportDocx (Object obj, User user)throws Exception;
	
	public Map<String, ?> getReqPassportExcel(Object object, User user)
			throws Exception;
	
	
}
