package sss.dot.tourism.passport.service;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.domain.MasDocument;
import sss.dot.tourism.domain.Person;
import sss.dot.tourism.domain.ReqPassport;
import sss.dot.tourism.domain.ReqPassportDoc;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.dto.certificate.ReqLicenseCertificateDTO;
import sss.dot.tourism.passport.dao.ReqPassportDAO;
import sss.dot.tourism.passport.dao.ReqPassportDocDAO;
import sss.dot.tourism.passport.dto.ReqPassportDTO;
import sss.dot.tourism.service.file.FileManage;
import sss.dot.tourism.service.trader.ITraderService;
import sss.dot.tourism.util.ConstantUtil;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.PersonType;
import sss.dot.tourism.util.RecordStatus;
import sss.dot.tourism.util.ThaiBahtUtil;
import sss.dot.tourism.util.TraderCategory;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.User;

import fr.opensagres.xdocreport.utils.StringUtils;

@Repository("reqPassportService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ReqPassportService implements IReqPassportService{

	@Autowired
	private ReqPassportDAO reqPassportDAO;

	@Autowired
	private ReqPassportDocDAO reqPassportDocDAO;

	@Autowired
	private TraderDAO traderDAO;
	
	DateFormat df = DateUtils.getProcessDateFormatThai();
	
	@Autowired
	private ITraderService traderService;
	
	@Override
	public Object getById(Object object, User user) throws Exception {
		if (!(object instanceof ReqPassportDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}		
		
		ReqPassportDTO param = (ReqPassportDTO) object;
		ReqPassport reqpassport = (ReqPassport)this.reqPassportDAO.findByPrimaryKey(param.getReqPassId());
		
		ReqPassportDTO dto = new ReqPassportDTO();
		ObjectUtil.copy(reqpassport, dto);
		
		List<Trader> listTour = this.traderDAO.findTraderByLicenseNo(
				dto.getTourLicenseNo(), TraderType.TOUR_COMPANIES.getStatus(),
				RecordStatus.NORMAL.getStatus(), user.getUserData().getOrgId());
		
		if(!listTour.isEmpty()){
			Trader tour = listTour.isEmpty()?null:listTour.get(0);
			ObjectUtil.copy(tour, dto);
			
			Person person = tour.getPerson();

			ObjectUtil.copy(person, dto);
			
			String traderOwnerName = traderService.getTraderOwnerName(person);
			dto.setTraderOwnerName(traderOwnerName);

			dto.setTraderCategoryName(TraderCategory.getTraderCategory(
					dto.getTraderType(), dto.getTraderCategory()).getMeaning());
		}
		
		List<Trader> listGuide = this.traderDAO.findTraderByLicenseNo(
				dto.getTourLicenseNo(), TraderType.GUIDE.getStatus(),
				RecordStatus.NORMAL.getStatus(), user.getUserData().getOrgId());
		
		if(!listGuide.isEmpty()){
			Trader guide = listGuide.isEmpty()?null:listGuide.get(0);
			
			dto.setExpireDate(df.format(guide.getExpireDate()));
			dto.setTraderGuideId(guide.getTraderId());
		}
		
		List<Trader> listLeader = this.traderDAO.findTraderByLicenseNo(
				dto.getTourLicenseNo(), TraderType.LEADER.getStatus(),
				RecordStatus.NORMAL.getStatus(), user.getUserData().getOrgId());
		
		if(!listLeader.isEmpty()){
			Trader leader = listLeader.isEmpty()?null:listLeader.get(0);
			dto.setTraderLeaderId(leader.getTraderId());
		}
		
		
		return dto;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveAll(Object object, User user) throws Exception {

		if (!(object instanceof ReqPassportDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		ReqPassportDTO param = (ReqPassportDTO) object;

		if (!param.isReqPassIdNotEmpty()) {
			this.create(object, user);
		} else {
			this.update(object, user);
		}

	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	private void create(Object object, User user) throws Exception {

		if (!(object instanceof ReqPassportDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		ReqPassportDTO param = (ReqPassportDTO) object;

		ReqPassport passport = new ReqPassport();
		
		ObjectUtil.copy(param, passport);
		
		passport.setTourLicenseNo(param.getTourLicenseNo());
		passport.setGuideLicenseNo(param.getGuideLicenseNo());
		passport.setLeaderLicenseNo(param.getLeaderLicenseNo());
		passport.setTraderName(param.getTraderName());
		passport.setPersonName(param.getPersonName());
		
		if(param.getReqDate() != null){
			
			passport.setReqDate(df.parse(param.getReqDate()));
		}

		passport.setRecordStatus(RecordStatus.NORMAL.getStatus());
		passport.setCreateUser(user.getUserName());
		passport.setCreateDtm(new Date());

		Long reqPassId = (Long) this.reqPassportDAO.insert(passport);
		param.setReqPassId(reqPassId);

		this.addDoc(passport, user);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	private void addDoc(ReqPassport reqPassport, User user) throws Exception {

		List<MasDocument> listMasDoc = (List<MasDocument>) this.reqPassportDocDAO
				.findMasDoc();

		if (!listMasDoc.isEmpty()) {

			for (MasDocument masDocument : listMasDoc) {
				ReqPassportDoc doc = new ReqPassportDoc();
				doc.setReqDocPath("");
				doc.setMasDocument(masDocument);
				doc.setReqPassport(reqPassport);

				doc.setRecordStatus(RecordStatus.NORMAL.getStatus());
				doc.setCreateDtm(new Date());
				doc.setCreateUser(user.getUserName());

				this.reqPassportDocDAO.insert(doc);
			}
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	private void update(Object object, User user) throws Exception {

		if (!(object instanceof ReqPassportDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		ReqPassportDTO param = (ReqPassportDTO) object;

		ReqPassport passport = (ReqPassport)this.reqPassportDAO.findByPrimaryKey(param.getReqPassId());
	
		ObjectUtil.copy(param, passport);
				
		passport.setTourLicenseNo(param.getTourLicenseNo());
		passport.setGuideLicenseNo(param.getGuideLicenseNo());
		passport.setLeaderLicenseNo(param.getLeaderLicenseNo());
		passport.setTraderName(param.getTraderName());
		passport.setPersonName(param.getPersonName());
		
		if(param.getReqDate() != null){
			
			passport.setReqDate(df.parse(param.getReqDate()));
		}

		passport.setRecordStatus(RecordStatus.NORMAL.getStatus());
		passport.setLastUpdUser(user.getUserName());
		passport.setLastUpdDtm(new Date());

		this.reqPassportDAO.update(passport);
	}

	@Override
	public void delete(Object obj, User user) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List getByPage(Object object, User user, int start, int limit)
			throws Exception {
		if (!(object instanceof ReqPassportDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}

		ReqPassportDTO param = (ReqPassportDTO) object;
		
		List<ReqPassportDTO> list = new ArrayList<ReqPassportDTO>();
		
		List<ReqPassport> listAll = (List<ReqPassport>)this.reqPassportDAO.findByPaging(param, start, limit);
		
		if(!listAll.isEmpty()){
			for(ReqPassport req: listAll){
				ReqPassportDTO dto = new ReqPassportDTO();
				ObjectUtil.copy(req, dto);
				
				dto.setTraderName(req.getTourLicenseNo()+" " + req.getTraderName());
				
				list.add(dto);
			}
		}

		return list;
	}

	@Override
	public int countAll(Object object, User user, int start, int limit)
			throws Exception {
		if (!(object instanceof ReqPassportDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}

		ReqPassportDTO param = (ReqPassportDTO) object;
		
		List<ReqPassportDTO> list = new ArrayList<ReqPassportDTO>();
		
		List<ReqPassport> listAll = (List<ReqPassport>)this.reqPassportDAO.findByPaging(param, start, limit);
		return listAll.isEmpty() ? 0 : listAll.size();
	}

	@Override
	public List getDocuments(Object object, User user) throws Exception {

		if (!(object instanceof ReqPassportDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}

		ReqPassportDTO param = (ReqPassportDTO) object;

		List<ReqLicenseCertificateDTO> list = new ArrayList<ReqLicenseCertificateDTO>();

		List<ReqPassportDoc> listAll = (List<ReqPassportDoc>) this.reqPassportDocDAO.findAll(param);

		if (!listAll.isEmpty()) {

			for (ReqPassportDoc doc : listAll) {

				ReqLicenseCertificateDTO dto = new ReqLicenseCertificateDTO();

				dto.setReqDocId(doc.getReqPassDocId());
				dto.setDocumentName(doc.getMasDocument().getDocumentName());
				dto.setCerId(doc.getReqPassport().getReqPassId());
				
				if(StringUtils.isNotEmpty(doc.getReqDocPath())){
					dto.setViewfile("view");
				}else{
					dto.setViewfile("empty");
				}

				list.add(dto);

			}
		}
		return list;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveDocument(long reqDocId, MultipartFile documentFile,
			User user) throws Exception {
		
		ReqPassportDoc reqDoc = (ReqPassportDoc) this.reqPassportDocDAO
				.findByPrimaryKey(reqDocId);

		ReqPassport passport = reqDoc.getReqPassport();

		String sap = java.io.File.separator;

		// TraderType
		String traderTypeName = "passport";
		String certificateDocFolder = "ReqPassportDoc";
		// LicenseNo
		String licenseNoStr = passport.getLeaderLicenseNo();
		licenseNoStr = licenseNoStr.replaceAll("[.]", "");
		licenseNoStr = licenseNoStr.replaceAll("[/]", "");
		licenseNoStr = licenseNoStr.replaceAll("[-]", "");

		String realFileName = Calendar.getInstance().getTime().getTime() + "";

		Calendar cal = Calendar.getInstance();
		DateFormat f = DateUtils.getProcessDateFormatEng();

		String date = f.format(cal.getTime());
		String fold = date.replaceAll("[-]", "");

		// postfix FileName
		String extension = ".pdf";
		String originalFilename = documentFile.getOriginalFilename();
		int dot = originalFilename.lastIndexOf('.');
		if (dot != -1) {
			extension = originalFilename.substring(dot);
		}

		String root = ConstantUtil.ROOT_PATH;
		String path = root + certificateDocFolder + sap + traderTypeName + sap
				+ licenseNoStr + sap + fold;
		String fileName = sap + realFileName + extension;

		String fullpath = path + fileName;

		reqDoc.setReqDocPath(fullpath);

		if (documentFile != null) {

			// Upload File
			FileManage fm = new FileManage();
			fm.uploadFile(documentFile.getInputStream(), path, fileName);

		}
		
		this.reqPassportDocDAO.update(reqDoc);

	}
		
	@Override
	public Object getDocumentsById(Object object, User user) throws Exception {
		ReqPassportDTO param = (ReqPassportDTO) object;
		
		ReqPassportDoc reqDoc = (ReqPassportDoc)this.reqPassportDocDAO.findByPrimaryKey(param.getReqPassDocId());
		
		ReqPassportDTO dto = new ReqPassportDTO();
		
		ObjectUtil.copy(reqDoc, dto);
		ObjectUtil.copy(reqDoc.getReqPassport(), dto);
		
		return dto;
	}

	@Override
	public Object getLeaderLicense(Object object, User user) throws Exception {

		if (!(object instanceof ReqPassportDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		ReqPassportDTO param = (ReqPassportDTO) object;
		
		ReqPassportDTO dto = new ReqPassportDTO();
		
		Trader guideLicense = null;
		Trader leaderLicense = null;
		
		if(TraderType.GUIDE.equals(param.getTraderType())){
			
			List<Trader> listGuideTrader = this.traderDAO.findTraderByLicenseNo(
					param.getGuideLicenseNo(), TraderType.GUIDE.getStatus(),
					RecordStatus.NORMAL.getStatus(), user.getUserData().getOrgId());
			if(!listGuideTrader.isEmpty()){
				guideLicense = listGuideTrader.get(0);
				
				List<Trader> listLeaderTrader = this.traderDAO.findLeaderByGuideLicenseNo(
						 guideLicense.getLicenseNo(), TraderType.LEADER.getStatus(),
						RecordStatus.NORMAL.getStatus(), user.getUserData().getOrgId());
				
				if(!listLeaderTrader.isEmpty()){
					leaderLicense = listLeaderTrader.get(0);
				}
			}
			
			
		}
		if(TraderType.LEADER.equals(param.getTraderType())){
			List<Trader> listLeaderTrader = this.traderDAO.findTraderByLicenseNo(
					param.getLeaderLicenseNo(), TraderType.LEADER.getStatus(),
					RecordStatus.NORMAL.getStatus(), 0);
			
			if(!listLeaderTrader.isEmpty()){
				leaderLicense = listLeaderTrader.get(0);
				
				List<Trader> listGuideTrader = this.traderDAO.findTraderByLicenseNo(
						leaderLicense.getLicenseGuideNo(), TraderType.GUIDE.getStatus(),
						RecordStatus.NORMAL.getStatus(), 0);
				if(!listGuideTrader.isEmpty()){
					guideLicense = listGuideTrader.get(0);
				}
			}
		}
		
		Person person = null;
		String leaderLicenseNo = "";
		if(leaderLicense != null)
		{
			person = leaderLicense.getPerson();
			leaderLicenseNo = leaderLicense.getLicenseNo()==null?"":leaderLicense.getLicenseNo();
		}else{
			person = guideLicense.getPerson();
		}
		
		if(null != guideLicense.getExpireDate()){
			String expireDate = df.format(guideLicense.getExpireDate());
			dto.setExpireDate(DateUtils.getDateWithMonthName(expireDate));
		}
		
		dto.setGuideLicenseNo(guideLicense.getLicenseNo());
		
		dto.setLeaderLicenseNo(leaderLicenseNo);
		
		String traderOwnerName = person.getMasPrefix() == null ? ""
				: person.getMasPrefix().getPrefixName()
						+ person.getFirstName() + " "
						+ person.getLastName();
		dto.setPersonName(traderOwnerName);
		
		dto.setPassportNo(person.getPassportNo()==null?"":person.getPassportNo());
		dto.setReqIdCard(person.getIdentityNo()==null?"":person.getIdentityNo());
		

		return dto;
	}

	@Override
	public Object getReqPassportDocx(Object object, User user) throws Exception {
		if (!(object instanceof ReqPassportDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}

		ReqPassportDTO param = (ReqPassportDTO) object;
		ReqPassportDTO dto = new ReqPassportDTO();
		
		ReqPassport req = (ReqPassport)this.reqPassportDAO.findByPrimaryKey(param.getReqPassId());
		
		List<Trader> listTourTrader = this.traderDAO.findTraderByLicenseNo(
				req.getTourLicenseNo(), TraderType.TOUR_COMPANIES.getStatus(),
				RecordStatus.NORMAL.getStatus(), user.getUserData().getOrgId());
		
		if(!listTourTrader.isEmpty()){
			Trader tourLicense = listTourTrader.get(0);
			Person person = tourLicense.getPerson();
			
			if (PersonType.CORPORATE.getStatus().equals(person.getPersonType())) {

				String traderOwnerName = person.getMasPrefix() == null ? ""
						: person.getMasPrefix().getPrefixName()
								+ person.getFirstName()
								+ (person.getMasPosfix() == null ? "" : person
										.getMasPosfix().getPostfixName());
				dto.setTraderName(traderOwnerName);
			} else {
				String traderOwnerName = person.getMasPrefix() == null ? ""
						: person.getMasPrefix().getPrefixName()
								+ person.getFirstName() + " "
								+ person.getLastName();
				dto.setTraderName(traderOwnerName);

			}
			
			dto.setTraderOwnerName(dto.getTraderName());
			
		}
		
		dto.setTourLicenseNo(ThaiBahtUtil.convertText2Numthai(req.getTourLicenseNo()));
		dto.setGuideLicenseNo(ThaiBahtUtil.convertText2Numthai(req.getGuideLicenseNo()));
		
		dto.setLeaderLicenseNo(ThaiBahtUtil.convertText2Numthai(req.getLeaderLicenseNo()));
		dto.setPersonName(req.getPersonName());
		
		String ttAaDate = DateUtils.getDateWithMonthName(df.format(req.getTtAaDate()==null?new Date():req.getTtAaDate()));
		dto.setTtAaDate(ThaiBahtUtil.convertText2Numthai(ttAaDate));
		String ttAaNo = req.getTtAaNo()==null? " ทีทีเอเอ ":req.getTtAaNo();
		dto.setTtAaNo(ThaiBahtUtil.convertText2Numthai(ttAaNo));
		
		String bookDocToManagerNo = req.getBookDocToManagerNo()==null? "กก":req.getBookDocToManagerNo();
		dto.setBookDocToManagerNo(ThaiBahtUtil.convertText2Numthai(bookDocToManagerNo));
		String bookDocToManagerDate = DateUtils.getDateWithMonthName(df.format(req.getBookDocManagerDate()==null?new Date():req.getBookDocManagerDate()));
		dto.setBookDocManagerDate(ThaiBahtUtil.convertText2Numthai(bookDocToManagerDate));
		
		String bookDocToDirectorNo = req.getBookDocToDirectorNo()==null? "กก":req.getBookDocToDirectorNo();
		dto.setBookDocToDirectorNo(ThaiBahtUtil.convertText2Numthai(bookDocToDirectorNo));
		String bookDocToDirectorDate = DateUtils.getDateWithMonthName(df.format(req.getBookDocToDirectorDate()==null?new Date():req.getBookDocToDirectorDate()));
		dto.setBookDocToDirectorDate(ThaiBahtUtil.convertText2Numthai(bookDocToDirectorDate));
		
		String bookNo = req.getBookNo()==null? "กก":req.getBookNo();
		dto.setBookNo(ThaiBahtUtil.convertText2Numthai(bookNo));
		int sizestr = 25;
		bookNo = dto.getBookNo();
		sizestr = sizestr - dto.getBookNo().length();
		char[] bytes = new char[sizestr];
		Arrays.fill(bytes, ' ');
		String str = new String(bytes);
		bookNo = dto.getBookNo().concat(str);
		dto.setBookNo(bookNo);
		String bookDate = DateUtils.getDateWithMonthName(df.format(req.getBookDate()==null?new Date():req.getBookDate()));
		dto.setBookDate(ThaiBahtUtil.convertText2Numthai(bookDate));
		
		return dto;
	}

	@Override
	public Map<String, ?> getReqPassportExcel(Object object, User user)
			throws Exception {
		if (!(object instanceof ReqPassportDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		ReqPassportDTO param = (ReqPassportDTO) object;
		
		List<ReqPassport> listAll = (List<ReqPassport>)this.reqPassportDAO.findByPaging(param, 0, Integer.MAX_VALUE);
				

		Map model = new HashMap();
		if (!listAll.isEmpty()) {
			List<ReqPassportDTO> list = new ArrayList<ReqPassportDTO>();
			for(ReqPassport req: listAll){

				ReqPassportDTO dto = new ReqPassportDTO();

				dto.setReqPassId(req.getReqPassId());
				
//				dto = (ReqPassportDTO)this.getReqPassportDocx(dto, user);
				
				List<Trader> listTourTrader = this.traderDAO.findTraderByLicenseNo(
						req.getTourLicenseNo(), TraderType.TOUR_COMPANIES.getStatus(),
						RecordStatus.NORMAL.getStatus(), user.getUserData().getOrgId());
				
				if(!listTourTrader.isEmpty()){
					Trader tourLicense = listTourTrader.get(0);
					Person person = tourLicense.getPerson();
					
					if (PersonType.CORPORATE.getStatus().equals(person.getPersonType())) {

						String traderOwnerName = person.getMasPrefix() == null ? ""
								: person.getMasPrefix().getPrefixName()
										+ person.getFirstName()
										+ (person.getMasPosfix() == null ? "" : person
												.getMasPosfix().getPostfixName());
						dto.setTraderName(traderOwnerName);
					} else {
						String traderOwnerName = person.getMasPrefix() == null ? ""
								: person.getMasPrefix().getPrefixName()
										+ person.getFirstName() + " "
										+ person.getLastName();
						dto.setTraderName(traderOwnerName);

					}
					
					dto.setTraderOwnerName(dto.getTraderName());
					
				}
				
				dto.setTourLicenseNo(req.getTourLicenseNo());
				dto.setGuideLicenseNo(req.getGuideLicenseNo());
				
				dto.setLeaderLicenseNo(req.getLeaderLicenseNo());
				dto.setPersonName(req.getPersonName());
				
				String ttAaDate = DateUtils.getDateWithMonthName(df.format(req.getTtAaDate()==null?new Date():req.getTtAaDate()));
				dto.setTtAaDate(ttAaDate);
				String ttAaNo = req.getTtAaNo()==null? " ทีทีเอเอ ":req.getTtAaNo();
				dto.setTtAaNo(ttAaNo);
				
				String bookDocToManagerNo = req.getBookDocToManagerNo()==null? "กก":req.getBookDocToManagerNo();
				dto.setBookDocToManagerNo(bookDocToManagerNo);
				String bookDocToManagerDate = DateUtils.getDateWithMonthName(df.format(req.getBookDocManagerDate()==null?new Date():req.getBookDocManagerDate()));
				dto.setBookDocManagerDate(bookDocToManagerDate);
				
				String bookDocToDirectorNo = req.getBookDocToDirectorNo()==null? "กก":req.getBookDocToDirectorNo();
				dto.setBookDocToDirectorNo(bookDocToDirectorNo);
				String bookDocToDirectorDate = DateUtils.getDateWithMonthName(df.format(req.getBookDocToDirectorDate()==null?new Date():req.getBookDocToDirectorDate()));
				dto.setBookDocToDirectorDate(bookDocToDirectorDate);
				
				String bookNo = req.getBookNo()==null? "กก":req.getBookNo();
				dto.setBookNo(bookNo);
				int sizestr = 25;
				bookNo = dto.getBookNo();
				sizestr = sizestr - dto.getBookNo().length();
				char[] bytes = new char[sizestr];
				Arrays.fill(bytes, ' ');
				String str = new String(bytes);
				bookNo = dto.getBookNo().concat(str);
				dto.setBookNo(bookNo);
				String bookDate = DateUtils.getDateWithMonthName(df.format(req.getBookDate()==null?new Date():req.getBookDate()));
				dto.setBookDate(bookDate);
				
				list.add(dto);
			}
			model.put("reqpassport", list);
		}
		
		return model;
	}
}
