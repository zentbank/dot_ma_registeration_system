package sss.dot.tourism.report.docx;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import sss.dot.tourism.joborder.dto.JobOrderDTO;
import sss.dot.tourism.joborder.dto.JobOrderHotelDTO;
import sss.dot.tourism.joborder.dto.LocalItinerayDTO;
import sss.dot.tourism.joborder.dto.ProgramTourDTO;
import fr.opensagres.xdocreport.core.XDocReportException;
import fr.opensagres.xdocreport.template.TemplateEngineKind;
import fr.opensagres.xdocreport.template.formatter.FieldsMetadata;

public class XdocreportGenerateXML {
	public static void main(String[] args) throws XDocReportException,IOException {
	    
//	    // 1) Create FieldsMetadata by setting Velocity as template engine
//	    FieldsMetadata fieldsMetadata = new FieldsMetadata(TemplateEngineKind.Velocity.name());
//	                
//	    // 2) Load fields metadata from Java Class
//	    fieldsMetadata.load("reqcertificate", ReqLicenseCertificateDTO.class);
//	    // Here load is called with true because model is a list of Developer.
//	    fieldsMetadata.load("reqpassport", ReqPassportDTO.class, true);
//	    
//	    // 3) Generate XML fields in the file "project.fields.xml".
//	    // Extension *.fields.xml is very important to use it with MS Macro XDocReport.dotm
//	    // FieldsMetadata#saveXML is called with true to indent the XML.
//	    File xmlFieldsFile = new File("project.fields.xml");
//	    fieldsMetadata.saveXML(new FileOutputStream(xmlFieldsFile), true);
//	    
//	    System.out.println("Done");
		
		//joborder
	    // 1) Create FieldsMetadata by setting Velocity as template engine
	    FieldsMetadata fieldsMetadata = new FieldsMetadata(TemplateEngineKind.Velocity.name());
	                
	    // 2) Load fields metadata from Java Class
	    fieldsMetadata.load("joborder", JobOrderDTO.class);
	    fieldsMetadata.load("locals", LocalItinerayDTO.class , true);
	    fieldsMetadata.load("hotel", JobOrderHotelDTO.class , true);
	    
	    
	    
	    // Here load is called with true because model is a list of Developer.
	    fieldsMetadata.load("lists", ProgramTourDTO.class, true);
	    
	    // 3) Generate XML fields in the file "project.fields.xml".
	    // Extension *.fields.xml is very important to use it with MS Macro XDocReport.dotm
	    // FieldsMetadata#saveXML is called with true to indent the XML.
	    File xmlFieldsFile = new File("joborder.fields.xml");
	    fieldsMetadata.saveXML(new FileOutputStream(xmlFieldsFile), true);
	    
	    System.out.println("Done");
	  }
}
