package sss.dot.tourism.reqtouroperator.controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.reqtouroperator.service.DotReqTourOperatorDTO;
import sss.dot.tourism.reqtouroperator.service.IDotReqTourOperatorService;

import com.sss.aut.service.User;

@Controller
@RequestMapping("/reqtouroperator")
public class DotReqTourOperatorController {

	@Autowired
	private IDotReqTourOperatorService dotReqTourOperatorService;
	@Autowired
	private HttpServletRequest req;
	
	@RequestMapping(value = "/read", method = RequestMethod.POST)
	public @ResponseBody String read(@ModelAttribute DotReqTourOperatorDTO param ) 
	{
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			
			User user = (User) req.getSession(true).getAttribute("user");

			param.setOrgId(user.getUserData().getOrgId());
			
			Long numAll = new Long(dotReqTourOperatorService.countAll(param, user, param.getStart(), param.getLimit()));
			
			List list = this.dotReqTourOperatorService.getByPage(param, user, param.getStart(), param.getLimit());
			
			return jsonmsg.writePagingMessage(list, numAll);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public @ResponseBody
	String save(@ModelAttribute DotReqTourOperatorDTO param ) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			User user = (User) req.getSession(true).getAttribute("user");
			this.dotReqTourOperatorService.saveAll(param ,user);

			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/readById", method = RequestMethod.POST)
	public @ResponseBody
	String readById(@ModelAttribute DotReqTourOperatorDTO param) {

	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			Object obj = dotReqTourOperatorService.getById(param, user);
			
			return jsonmsg.writeMessage(obj);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/doc/readDocument", method = RequestMethod.POST)
	public @ResponseBody
	String readDocument(@ModelAttribute DotReqTourOperatorDTO param) {

	
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			List list = dotReqTourOperatorService.getDocuments(param, user);
			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
		}
	}
	
	
	@RequestMapping(value = "/doc/saveDocument", method = RequestMethod.POST)
	public @ResponseBody String saveDocument(@RequestParam("documentFile") MultipartFile documentFile 
			,@RequestParam("reqDocId") long reqDocId) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			
			User user = (User) req.getSession(true).getAttribute("user");
			this.dotReqTourOperatorService.saveDocument(reqDocId, documentFile, user);
			
			DotReqTourOperatorDTO param = new DotReqTourOperatorDTO();
			
			return jsonmsg.writeMessage(param);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/doc/viewReqDoc", method = RequestMethod.GET)
	public void viewfile(HttpServletRequest req, HttpServletResponse resp,@RequestParam("reqDocId") long reqDocId){
		
		try {
			User user = (User) req.getSession(true).getAttribute("user");
			DotReqTourOperatorDTO param = new DotReqTourOperatorDTO();
			param.setReqDocId(reqDocId);
			DotReqTourOperatorDTO dto = (DotReqTourOperatorDTO)this.dotReqTourOperatorService.getDocumentsById(param, user);
		
			
			String fileType = dto.getReqDocPath().substring(dto.getReqDocPath().lastIndexOf(".") + 1, dto.getReqDocPath().length());
			
//			if(!fileType.equals("pdf"))
//			{
//				File f = new File(dto.getReqDocPath());
//			
//				resp.setContentType("image/" + fileType);
//				BufferedImage bi = ImageIO.read(f);
//				OutputStream out = resp.getOutputStream();
//				ImageIO.write(bi, fileType, out);
//				out.close();
//			}
//			else
//			{
//				File pdfFile = new File(dto.getReqDocPath());
//				
//				String fileName = dto.getReqDocPath().substring(dto.getReqDocPath().lastIndexOf("/") + 1, dto.getReqDocPath().length());
//
//				resp.setContentType("application/pdf");
//				resp.addHeader("Content-Disposition", "attachment; filename=" + fileName);
//				resp.setContentLength((int) pdfFile.length());
//
//				FileInputStream fileInputStream = new FileInputStream(pdfFile);
//				OutputStream responseOutputStream = resp.getOutputStream();
//				int bytes;
//				while ((bytes = fileInputStream.read()) != -1) {
//					responseOutputStream.write(bytes);
//				
//				}
//			}
			
			String[] imgType = new String[] {"JPG","jpg","PNG","png"};
			
			if(Arrays.asList(imgType).contains(fileType)){
				File f = new File(dto.getReqDocPath());
				
				resp.setContentType("image/" + fileType);
				BufferedImage bi = ImageIO.read(f);
				OutputStream out = resp.getOutputStream();
				ImageIO.write(bi, fileType, out);
				out.close();
			}
			
			if(fileType.equals("pdf")){
				File pdfFile = new File(dto.getReqDocPath());
				
				String fileName = dto.getReqDocPath().substring(dto.getReqDocPath().lastIndexOf("/") + 1, dto.getReqDocPath().length());

				resp.setContentType("application/pdf");
				resp.addHeader("Content-Disposition", "attachment; filename=" + fileName);
				resp.setContentLength((int) pdfFile.length());

				FileInputStream fileInputStream = new FileInputStream(pdfFile);
				OutputStream responseOutputStream = resp.getOutputStream();
				int bytes;
				while ((bytes = fileInputStream.read()) != -1) {
					responseOutputStream.write(bytes);
				}
			}
			
			String[] docType = new String[] {"doc","docx"};
			if(Arrays.asList(docType).contains(fileType)){
				File pdfFile_doc = new File(dto.getReqDocPath());
				
				String fileName_doc = dto.getReqDocPath().substring(dto.getReqDocPath().lastIndexOf("/") + 1, dto.getReqDocPath().length());

				resp.setContentType("application/octet-stream");
				resp.addHeader("Content-Disposition", "attachment; filename=" + fileName_doc);
				resp.setContentLength((int) pdfFile_doc.length());

				FileInputStream fileInputStream_doc = new FileInputStream(pdfFile_doc);
				OutputStream responseOutputStream_doc = resp.getOutputStream();
				int bytes_doc;
				while ((bytes_doc = fileInputStream_doc.read()) != -1) {
					responseOutputStream_doc.write(bytes_doc);
				}
			}
				
			
			
		} catch (Exception e) {
			e.printStackTrace();
			
	
		}
	}
	
    @RequestMapping(value = "/docx" , method = RequestMethod.GET)
    public String suspensionDocx(@RequestParam("taxId") String taxId, ModelMap model )
    {
      	try {
	    	User user = (User) req.getSession(true).getAttribute("user");
	    	
	    	DotReqTourOperatorDTO param = new DotReqTourOperatorDTO();
	    	param.setTaxId(taxId);
	    	
	    	Object dto = this.dotReqTourOperatorService.getReqDocx(param, user);
	    	model.addAttribute("req", dto);
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		
        return "reqTourOperator";
    }
}
