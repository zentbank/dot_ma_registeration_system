package sss.dot.tourism.reqtouroperator.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.DotReqTourOperator;
import sss.dot.tourism.domain.DotReqTourOperatorDoc;
import sss.dot.tourism.domain.MasDocument;
import sss.dot.tourism.util.RegistrationType;
import sss.dot.tourism.util.TraderType;
import fr.opensagres.xdocreport.utils.StringUtils;

@SuppressWarnings("serial")
@Repository("dotReqTourOperatorDAO")
public class DotReqTourOperatorDAO extends BaseDAO{
	public DotReqTourOperatorDAO() {
		this.domainObj = DotReqTourOperator.class;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<DotReqTourOperator> findByPaging(String taxId, String companyName, int start, int limit) throws Exception{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  DotReqTourOperator as dep ");
		hql.append(" where dep.recordStatus = 'N' ");
		
		if(StringUtils.isNotEmpty(companyName)){
			hql.append(" and dep.companyName like ? ");
			params.add("%"+companyName+"%");
		}
		
		if(StringUtils.isNotEmpty(taxId)){
			hql.append(" and dep.taxId = ? ");
			params.add(taxId);
		}
		
		
		return (List<DotReqTourOperator>)this.findByAddPaging(hql.toString(), start, limit, params.toArray());
	  }
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<MasDocument> findMasDoc()
	  {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  MasDocument as dep ");
		hql.append(" where dep.recordStatus = 'N' ");

		hql.append(" and dep.licenseType = ? ");
		params.add(TraderType.TOUR_COMPANIES.getStatus());
		hql.append(" and dep.registrationType = ? ");
		params.add(RegistrationType.TOUR_OPERATOR_CERTIFICATE.getStatus());

		hql.append(" order by dep.docIndex ");
		
		return (List<MasDocument>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	  }
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<DotReqTourOperatorDoc> findAllDoc(String taxId)
	  {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  DotReqTourOperatorDoc as dep ");
		hql.append(" where dep.recordStatus = 'N' ");
		
		if(StringUtils.isNotEmpty(taxId)){
			hql.append(" and dep.dotReqTourOperator.taxId = ? ");
			params.add(taxId);
		}

		hql.append(" order by dep.masDocument.docIndex ");
		
		return (List<DotReqTourOperatorDoc>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	  }

}
