package sss.dot.tourism.reqtouroperator.service;

import sss.dot.tourism.domain.DotReqTourOperator;
import sss.dot.tourism.domain.MasAmphur;
import sss.dot.tourism.domain.MasDocument;
import sss.dot.tourism.domain.MasPosfix;
import sss.dot.tourism.domain.MasPrefix;
import sss.dot.tourism.domain.MasProvince;
import sss.dot.tourism.domain.MasTambol;

public class DotReqTourOperatorDTO  implements java.io.Serializable {
	
	private String taxId;
	private long prefixId;
	private String prefixName;
	private long postfixId;
	private String postfixName;
	private String companyName;
	private String reqCardId;
	private String reqPrefixName;
	private long reqPrefix;
	private String reqName;
	private String reqLastName;
	private String reqRelationship;
	private String reqDesc1;
	private String reqDesc2;
	private String reqDesc3;
	private String reqDesc4;
	private String reqDesc5;
	private String reqDesc6;
	private String reqDesc7;
	private String reqDesc8;
	private String reqDesc9;
	private String reqDesc10;
	private String reqDesc11;
	private String reqDesc12;
	private String interviewer;
	private String witness;
	private String reqNation;
	
	private long amphurId;
	private long provinceId;
	private long tambolId;
	
	
	private String villageName;
	private String villageNameEn;
	private String buildingName;
	private String buildingNameEn;
	private String floor;
	private String roomNo;
	private String homeNo;
	private String mooNo;
	private String soi;
	private String soiEn;
	private String roadName;
	private String roadNameEn;
	private String postCode;
	private String email;
	private String fax;
	private String mobileNo;
	private String telephone;
	private String website;
	
	private long reqDocId;
	private String reqDocPath;
	
	private long masDocId;
	private Integer docIndex;
	private String documentName;
	
	private int page;
	private int start;
	private int limit;
	
	private long orgId;
	private String viewfile;
	
	public String getTaxId() {
		return taxId;
	}
	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}
	public long getPrefixId() {
		return prefixId;
	}
	public void setPrefixId(long prefixId) {
		this.prefixId = prefixId;
	}
	public long getPostfixId() {
		return postfixId;
	}
	public void setPostfixId(long postfixId) {
		this.postfixId = postfixId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getReqCardId() {
		return reqCardId;
	}
	public void setReqCardId(String reqCardId) {
		this.reqCardId = reqCardId;
	}
	public long getReqPrefix() {
		return reqPrefix;
	}
	public void setReqPrefix(long reqPrefix) {
		this.reqPrefix = reqPrefix;
	}
	public String getReqName() {
		return reqName;
	}
	public void setReqName(String reqName) {
		this.reqName = reqName;
	}
	public String getReqLastName() {
		return reqLastName;
	}
	public void setReqLastName(String reqLastName) {
		this.reqLastName = reqLastName;
	}
	public String getReqRelationship() {
		return reqRelationship;
	}
	public void setReqRelationship(String reqRelationship) {
		this.reqRelationship = reqRelationship;
	}
	public String getReqDesc1() {
		return reqDesc1;
	}
	public void setReqDesc1(String reqDesc1) {
		this.reqDesc1 = reqDesc1;
	}
	public String getReqDesc2() {
		return reqDesc2;
	}
	public void setReqDesc2(String reqDesc2) {
		this.reqDesc2 = reqDesc2;
	}
	public String getReqDesc3() {
		return reqDesc3;
	}
	public void setReqDesc3(String reqDesc3) {
		this.reqDesc3 = reqDesc3;
	}
	public String getReqDesc4() {
		return reqDesc4;
	}
	public void setReqDesc4(String reqDesc4) {
		this.reqDesc4 = reqDesc4;
	}
	public String getReqDesc5() {
		return reqDesc5;
	}
	public void setReqDesc5(String reqDesc5) {
		this.reqDesc5 = reqDesc5;
	}
	public String getReqDesc6() {
		return reqDesc6;
	}
	public void setReqDesc6(String reqDesc6) {
		this.reqDesc6 = reqDesc6;
	}
	public String getReqDesc7() {
		return reqDesc7;
	}
	public void setReqDesc7(String reqDesc7) {
		this.reqDesc7 = reqDesc7;
	}
	public String getReqDesc8() {
		return reqDesc8;
	}
	public void setReqDesc8(String reqDesc8) {
		this.reqDesc8 = reqDesc8;
	}
	public String getReqDesc9() {
		return reqDesc9;
	}
	public void setReqDesc9(String reqDesc9) {
		this.reqDesc9 = reqDesc9;
	}
	public String getReqDesc10() {
		return reqDesc10;
	}
	public void setReqDesc10(String reqDesc10) {
		this.reqDesc10 = reqDesc10;
	}
	public String getReqDesc11() {
		return reqDesc11;
	}
	public void setReqDesc11(String reqDesc11) {
		this.reqDesc11 = reqDesc11;
	}
	public String getReqDesc12() {
		return reqDesc12;
	}
	public void setReqDesc12(String reqDesc12) {
		this.reqDesc12 = reqDesc12;
	}
	public String getInterviewer() {
		return interviewer;
	}
	public void setInterviewer(String interviewer) {
		this.interviewer = interviewer;
	}
	public String getWitness() {
		return witness;
	}
	public void setWitness(String witness) {
		this.witness = witness;
	}
	public long getAmphurId() {
		return amphurId;
	}
	public void setAmphurId(long amphurId) {
		this.amphurId = amphurId;
	}
	public long getProvinceId() {
		return provinceId;
	}
	public void setProvinceId(long provinceId) {
		this.provinceId = provinceId;
	}
	public long getTambolId() {
		return tambolId;
	}
	public void setTambolId(long tambolId) {
		this.tambolId = tambolId;
	}
	public String getVillageName() {
		return villageName;
	}
	public void setVillageName(String villageName) {
		this.villageName = villageName;
	}
	public String getVillageNameEn() {
		return villageNameEn;
	}
	public void setVillageNameEn(String villageNameEn) {
		this.villageNameEn = villageNameEn;
	}
	public String getBuildingName() {
		return buildingName;
	}
	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}
	public String getBuildingNameEn() {
		return buildingNameEn;
	}
	public void setBuildingNameEn(String buildingNameEn) {
		this.buildingNameEn = buildingNameEn;
	}
	public String getFloor() {
		return floor;
	}
	public void setFloor(String floor) {
		this.floor = floor;
	}
	public String getRoomNo() {
		return roomNo;
	}
	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}
	public String getHomeNo() {
		return homeNo;
	}
	public void setHomeNo(String homeNo) {
		this.homeNo = homeNo;
	}
	public String getMooNo() {
		return mooNo;
	}
	public void setMooNo(String mooNo) {
		this.mooNo = mooNo;
	}
	public String getSoi() {
		return soi;
	}
	public void setSoi(String soi) {
		this.soi = soi;
	}
	public String getSoiEn() {
		return soiEn;
	}
	public void setSoiEn(String soiEn) {
		this.soiEn = soiEn;
	}
	public String getRoadName() {
		return roadName;
	}
	public void setRoadName(String roadName) {
		this.roadName = roadName;
	}
	public String getRoadNameEn() {
		return roadNameEn;
	}
	public void setRoadNameEn(String roadNameEn) {
		this.roadNameEn = roadNameEn;
	}
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public long getReqDocId() {
		return reqDocId;
	}
	public void setReqDocId(long reqDocId) {
		this.reqDocId = reqDocId;
	}
	public String getReqDocPath() {
		return reqDocPath;
	}
	public void setReqDocPath(String reqDocPath) {
		this.reqDocPath = reqDocPath;
	}
	public long getMasDocId() {
		return masDocId;
	}
	public void setMasDocId(long masDocId) {
		this.masDocId = masDocId;
	}
	public Integer getDocIndex() {
		return docIndex;
	}
	public void setDocIndex(Integer docIndex) {
		this.docIndex = docIndex;
	}
	public String getDocumentName() {
		return documentName;
	}
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	public String getReqNation() {
		return reqNation;
	}
	public void setReqNation(String reqNation) {
		this.reqNation = reqNation;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public long getOrgId() {
		return orgId;
	}
	public void setOrgId(long orgId) {
		this.orgId = orgId;
	}
	public String getViewfile() {
		return viewfile;
	}
	public void setViewfile(String viewfile) {
		this.viewfile = viewfile;
	}
	public String getPrefixName() {
		return prefixName;
	}
	public void setPrefixName(String prefixName) {
		this.prefixName = prefixName;
	}
	public String getPostfixName() {
		return postfixName;
	}
	public void setPostfixName(String postfixName) {
		this.postfixName = postfixName;
	}
	public String getReqPrefixName() {
		return reqPrefixName;
	}
	public void setReqPrefixName(String reqPrefixName) {
		this.reqPrefixName = reqPrefixName;
	}
}
