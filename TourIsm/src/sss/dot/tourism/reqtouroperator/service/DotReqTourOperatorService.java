package sss.dot.tourism.reqtouroperator.service;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import sss.dot.tourism.dao.CommonDAO;
import sss.dot.tourism.dao.mas.MasDocumentDAO;
import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.domain.DotReqTourOperator;
import sss.dot.tourism.domain.DotReqTourOperatorDoc;
import sss.dot.tourism.domain.MasAmphur;
import sss.dot.tourism.domain.MasDocument;
import sss.dot.tourism.domain.MasPosfix;
import sss.dot.tourism.domain.MasPrefix;
import sss.dot.tourism.domain.MasProvince;
import sss.dot.tourism.domain.MasTambol;
import sss.dot.tourism.reqtouroperator.dao.DotReqTourOperatorDAO;
import sss.dot.tourism.service.file.FileManage;
import sss.dot.tourism.service.trader.ITraderService;
import sss.dot.tourism.util.ConstantUtil;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.RecordStatus;

import com.sss.aut.service.User;

@Repository("dotReqTourOperatorService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class DotReqTourOperatorService implements IDotReqTourOperatorService{

	
	@Autowired
	private TraderDAO traderDAO;
	@Autowired
	private ITraderService traderService;
	
	@Autowired
	private DotReqTourOperatorDAO dotReqTourOperatorDAO;
	
	@Autowired
	private MasDocumentDAO masDocumentDAO;
	
	@Autowired private @Qualifier("commonDAO")CommonDAO commonDAO;

	DateFormat df = DateUtils.getProcessDateFormatThai();
	
	@Override
	public Object getById(Object object, User user) throws Exception {
		if (!(object instanceof DotReqTourOperatorDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		DotReqTourOperatorDTO param = (DotReqTourOperatorDTO) object;
		DotReqTourOperator opt = (DotReqTourOperator)this.dotReqTourOperatorDAO.findByPrimaryKey(param.getTaxId());
		
		DotReqTourOperatorDTO dto = new DotReqTourOperatorDTO();
		
		ObjectUtil.copy(opt, dto);
		if(null != opt.getMasPosfix()){
			dto.setPostfixId(opt.getMasPosfix().getPostfixId());
		}
		if(null != opt.getMasPrefix()){
			dto.setPrefixId(opt.getMasPrefix().getPrefixId());
		}
		if(null != opt.getReqPrefix()){
			dto.setReqPrefix(opt.getReqPrefix().getPrefixId());
		}
		if(null != opt.getMasTambol()){
			dto.setTambolId(opt.getMasTambol().getTambolId());
		}
		if(null != opt.getMasAmphur()){
			dto.setAmphurId(opt.getMasAmphur().getAmphurId());
		}
		if(null != opt.getMasProvince()){
			dto.setProvinceId(opt.getMasProvince().getProvinceId());
		}
		
		return dto;
		
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveAll(Object object, User user) throws Exception {
		if (!(object instanceof DotReqTourOperatorDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		DotReqTourOperatorDTO param = (DotReqTourOperatorDTO) object;
		
		DotReqTourOperator opt = (DotReqTourOperator)this.dotReqTourOperatorDAO.findByPrimaryKey(param.getTaxId());
		if (null == opt) {
			this.create(object, user);
		} else {
			this.update(object, user);
		}
		
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	private void create(Object object, User user) throws Exception {

		if (!(object instanceof DotReqTourOperatorDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		DotReqTourOperatorDTO param = (DotReqTourOperatorDTO) object;
		
		DotReqTourOperator opt = new DotReqTourOperator();
		ObjectUtil.copy(param, opt);
		
		if(param.getTambolId() > 0){
			MasTambol tambol = (MasTambol)this.commonDAO.findByPrimarykey(MasTambol.class, param.getTambolId());
			opt.setMasTambol(tambol);
		}
		if(param.getAmphurId() > 0){
			MasAmphur amphur = (MasAmphur)this.commonDAO.findByPrimarykey(MasAmphur.class, param.getAmphurId());
			opt.setMasAmphur(amphur);
		}
		if(param.getProvinceId() > 0){
			MasProvince province = (MasProvince)this.commonDAO.findByPrimarykey(MasProvince.class, param.getProvinceId());
			opt.setMasProvince(province);
		}
		
		if(param.getPostfixId() > 0){
			MasPosfix postfinx = (MasPosfix)this.commonDAO.findByPrimarykey(MasPosfix.class, param.getPostfixId());
			opt.setMasPosfix(postfinx);
		}
		
		if(param.getPrefixId() > 0){
			MasPrefix masprefix = (MasPrefix)this.commonDAO.findByPrimarykey(MasPrefix.class, param.getPrefixId());
			opt.setMasPrefix(masprefix);
		}
		
		if(param.getReqPrefix() > 0){
			MasPrefix masprefix = (MasPrefix)this.commonDAO.findByPrimarykey(MasPrefix.class, param.getReqPrefix());
			opt.setReqPrefix(masprefix);
		}
		
		opt.setRecordStatus(RecordStatus.NORMAL.getStatus());
		opt.setCreateUser(user.getUserName());
		opt.setCreateDtm(new Date());
		this.dotReqTourOperatorDAO.insert(opt);
		
		this.addDoc(opt, user);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	private void addDoc(DotReqTourOperator opt, User user) throws Exception {

		List<MasDocument> listMasDoc = (List<MasDocument>) this.dotReqTourOperatorDAO
				.findMasDoc();

		if (!listMasDoc.isEmpty()) {

			for (MasDocument masDocument : listMasDoc) {
				DotReqTourOperatorDoc doc = new DotReqTourOperatorDoc();
				doc.setReqDocPath("");
				doc.setMasDocument(masDocument);
				doc.setDotReqTourOperator(opt);

				doc.setRecordStatus(RecordStatus.NORMAL.getStatus());
				doc.setCreateDtm(new Date());
				doc.setCreateUser(user.getUserName());

				this.dotReqTourOperatorDAO.insert(doc);
			}
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	private void update(Object object, User user) throws Exception {

		if (!(object instanceof DotReqTourOperatorDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		DotReqTourOperatorDTO param = (DotReqTourOperatorDTO) object;

		DotReqTourOperator opt = (DotReqTourOperator)this.dotReqTourOperatorDAO.findByPrimaryKey(param.getTaxId());
		ObjectUtil.copy(param, opt);
		
		if(param.getTambolId() > 0){
			MasTambol tambol = (MasTambol)this.commonDAO.findByPrimarykey(MasTambol.class, param.getTambolId());
			opt.setMasTambol(tambol);
		}
		if(param.getAmphurId() > 0){
			MasAmphur amphur = (MasAmphur)this.commonDAO.findByPrimarykey(MasAmphur.class, param.getAmphurId());
			opt.setMasAmphur(amphur);
		}
		if(param.getProvinceId() > 0){
			MasProvince province = (MasProvince)this.commonDAO.findByPrimarykey(MasProvince.class, param.getProvinceId());
			opt.setMasProvince(province);
		}
		
		if(param.getPostfixId() > 0){
			MasPosfix postfinx = (MasPosfix)this.commonDAO.findByPrimarykey(MasPosfix.class, param.getPostfixId());
			opt.setMasPosfix(postfinx);
		}
		
		if(param.getPrefixId() > 0){
			MasPrefix masprefix = (MasPrefix)this.commonDAO.findByPrimarykey(MasPrefix.class, param.getPrefixId());
			opt.setMasPrefix(masprefix);
		}
		
		if(param.getReqPrefix() > 0){
			MasPrefix masprefix = (MasPrefix)this.commonDAO.findByPrimarykey(MasPrefix.class, param.getReqPrefix());
			opt.setReqPrefix(masprefix);
		}

		opt.setRecordStatus(RecordStatus.NORMAL.getStatus());
		opt.setLastUpdUser(user.getUserName());
		opt.setLastUpdDtm(new Date());

		this.dotReqTourOperatorDAO.update(opt);
	}

	@Override
	public void delete(Object obj, User user) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List getByPage(Object object, User user, int start, int limit)
			throws Exception {

		if (!(object instanceof DotReqTourOperatorDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}

		DotReqTourOperatorDTO param = (DotReqTourOperatorDTO) object;

		List<DotReqTourOperator> listAll = this.dotReqTourOperatorDAO.findByPaging(param.getTaxId(), param.getCompanyName(), param.getStart(), param.getLimit());
		List<DotReqTourOperatorDTO> list = new ArrayList<DotReqTourOperatorDTO>();

		if (CollectionUtils.isNotEmpty(listAll)) {
			for (DotReqTourOperator opt : listAll) {

				DotReqTourOperatorDTO dto = new DotReqTourOperatorDTO();
				ObjectUtil.copy(opt, dto);
				if(null != opt.getMasPosfix()){
					dto.setPostfixId(opt.getMasPosfix().getPostfixId());
				}
				if(null != opt.getMasPrefix()){
					dto.setPrefixId(opt.getMasPrefix().getPrefixId());
				}
				if(null != opt.getReqPrefix()){
					dto.setReqPrefix(opt.getReqPrefix().getPrefixId());
				}
				if(null != opt.getMasTambol()){
					dto.setTambolId(opt.getMasTambol().getTambolId());
				}
				if(null != opt.getMasAmphur()){
					dto.setAmphurId(opt.getMasAmphur().getAmphurId());
				}
				if(null != opt.getMasProvince()){
					dto.setProvinceId(opt.getMasProvince().getProvinceId());
				}
				

				list.add(dto);

			}
		}

		return list;
	}

	@Override
	public int countAll(Object object, User user, int start, int limit)
			throws Exception {

		if (!(object instanceof DotReqTourOperatorDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}

		DotReqTourOperatorDTO param = (DotReqTourOperatorDTO) object;

		List<DotReqTourOperator> listAll = this.dotReqTourOperatorDAO.findByPaging(param.getTaxId(), param.getCompanyName(), param.getStart(), param.getLimit());
		
		return listAll.isEmpty() ? 0 : listAll.size();
	}

	@Override
	public List getDocuments(Object object, User user) throws Exception {
		if (!(object instanceof DotReqTourOperatorDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}

		DotReqTourOperatorDTO param = (DotReqTourOperatorDTO) object;

		List<DotReqTourOperatorDTO> list = new ArrayList<DotReqTourOperatorDTO>();

		List<DotReqTourOperatorDoc> listAll = (List<DotReqTourOperatorDoc>) this.dotReqTourOperatorDAO
				.findAllDoc(param.getTaxId());

		if (!listAll.isEmpty()) {

			for (DotReqTourOperatorDoc doc : listAll) {

				DotReqTourOperatorDTO dto = new DotReqTourOperatorDTO();

				ObjectUtil.copy(doc, dto);
				ObjectUtil.copy(doc.getMasDocument(), dto);

				if(StringUtils.isNotEmpty(doc.getReqDocPath())){
					dto.setViewfile("view");
				}else{
					dto.setViewfile("empty");
				}
				list.add(dto);

			}
		}
		return list;
	}
	

	

	@Override
	public Object getDocumentsById(Object object, User user) throws Exception {
		if (!(object instanceof DotReqTourOperatorDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}

		DotReqTourOperatorDTO param = (DotReqTourOperatorDTO) object;
		
		DotReqTourOperatorDoc reqDoc = (DotReqTourOperatorDoc)this.commonDAO.findByPrimarykey(DotReqTourOperatorDoc.class, param.getReqDocId());
		
		DotReqTourOperatorDTO dto = new DotReqTourOperatorDTO();
		
		ObjectUtil.copy(reqDoc, dto);
		ObjectUtil.copy(reqDoc.getDotReqTourOperator(), dto);
		
		return dto;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveDocument(long reqDocId, MultipartFile documentFile,
			User user) throws Exception {

		DotReqTourOperatorDoc reqDoc = (DotReqTourOperatorDoc) this.commonDAO.findByPrimarykey(DotReqTourOperatorDoc.class,reqDocId);
				

		DotReqTourOperator cer = reqDoc.getDotReqTourOperator();

		String sap = java.io.File.separator;

		// TraderType
		String traderTypeName = "Business";
		String certificateDocFolder = "DotReqTourOperatorDoc";
		// LicenseNo
//		String licenseNoStr = cer.getLicenseNo();
//		licenseNoStr = licenseNoStr.replaceAll("[.]", "");
//		licenseNoStr = licenseNoStr.replaceAll("[/]", "");
//		licenseNoStr = licenseNoStr.replaceAll("[-]", "");

		String realFileName = Calendar.getInstance().getTime().getTime() + "";

		Calendar cal = Calendar.getInstance();
		DateFormat f = DateUtils.getProcessDateFormatEng();

		String date = f.format(cal.getTime());
		String fold = date.replaceAll("[-]", "");

		// postfix FileName
		String extension = ".pdf";
		String originalFilename = documentFile.getOriginalFilename();
		int dot = originalFilename.lastIndexOf('.');
		if (dot != -1) {
			extension = originalFilename.substring(dot);
		}

		String root = ConstantUtil.ROOT_PATH;
		String path = root + certificateDocFolder + sap + traderTypeName + sap + fold;
		String fileName = sap + realFileName + extension;

		String fullpath = path + fileName;

		reqDoc.setReqDocPath(fullpath);

		if (documentFile != null) {

			// Upload File
			FileManage fm = new FileManage();
			fm.uploadFile(documentFile.getInputStream(), path, fileName);

		}
		
		this.dotReqTourOperatorDAO.update(reqDoc);

	}

	public Object getReqDocx(Object object, User user) throws Exception {
		if (!(object instanceof DotReqTourOperatorDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		DotReqTourOperatorDTO param = (DotReqTourOperatorDTO) object;
		DotReqTourOperatorDTO dto = new DotReqTourOperatorDTO();

		DotReqTourOperator opt = (DotReqTourOperator) this.dotReqTourOperatorDAO
				.findByPrimaryKey(param.getTaxId());
		
		String docNo =  "กก ๐๔๐๓/";
		String currentDate = df.format(new Date());
		String[] dateStrArr = currentDate.split("/");
		String dateLicensing = DateUtils.getMonthName(dateStrArr[1]) +" " + dateStrArr[2];
		String tourName = "";
		if(null != opt.getMasPrefix()){
			tourName = opt.getMasPrefix().getPrefixName();
		}
		tourName += opt.getCompanyName();
		if(null != opt.getMasPosfix()){
			tourName += opt.getMasPosfix().getPostfixName();
		}
		String text1 = "กรรมการผู้จัดการ " + tourName;
		String reqName = "";
		if(null != opt.getReqPrefix()){
			reqName = opt.getReqPrefix().getPrefixName();
		}
		reqName += opt.getReqName() + opt.getReqLastName();
		String text2 = "กรรมการ " + tourName;
		
		
		dto.setReqDesc1(dateLicensing);
		dto.setReqDesc2(text1);
		dto.setReqDesc3(text2);
		dto.setCompanyName(tourName);
		dto.setReqName(reqName);
		
		return dto;
	}


}
