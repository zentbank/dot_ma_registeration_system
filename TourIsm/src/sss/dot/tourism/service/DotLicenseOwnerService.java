package sss.dot.tourism.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sss.aut.service.User;

import sss.dot.tourism.dao.DotLicOwnerCommitteeDAO;
import sss.dot.tourism.dao.DotLicOwnerEduBackgroundDAO;
import sss.dot.tourism.dao.DotLicOwnerLanguageDAO;
import sss.dot.tourism.dao.DotLicenseDAO;
import sss.dot.tourism.dao.DotLicenseOwnerDAO;
import sss.dot.tourism.dao.DotMsLanguageDAO;
import sss.dot.tourism.dao.DotSubLicenseDAO;
import sss.dot.tourism.dao.DotTrBillDetailDAO;
import sss.dot.tourism.dao.DotTrBillPaymentDAO;
import sss.dot.tourism.dao.DotTrFeeLicenseDAO;
import sss.dot.tourism.dao.DotTrFeePaymentDAO;


@Repository("dotLicenseOwnerService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class DotLicenseOwnerService implements IDotLicenseOwnerService{

	@Autowired private DotLicenseDAO dotLicenseDAO;
	@Autowired private DotLicenseOwnerDAO dotLicenseOwnerDAO;
	@Autowired private DotLicOwnerCommitteeDAO dotLicOwnerCommitteeDAO;
	@Autowired private DotLicOwnerEduBackgroundDAO dotLicOwnerEduBackgroundDAO;
	@Autowired private DotLicOwnerLanguageDAO dotLicOwnerLanguageDAO;
	@Autowired private DotMsLanguageDAO dotMsLanguageDAO;
	@Autowired private DotSubLicenseDAO dotSubLicenseDAO;
	@Autowired private DotTrBillDetailDAO dotTrBillDetailDAO;
	@Autowired private DotTrBillPaymentDAO dotTrBillPaymentDAO;
	@Autowired private DotTrFeeLicenseDAO dotTrFeeLicenseDAO;
	@Autowired private DotTrFeePaymentDAO dotTrFeePaymentDAO;
	@Override
	public void updateLicense(Object object, User user) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	
}
