package sss.dot.tourism.service;

import org.springframework.beans.factory.annotation.Autowired;

import sss.dot.tourism.dao.DotLicOwnerCommitteeDAO;
import sss.dot.tourism.dao.DotLicOwnerEduBackgroundDAO;
import sss.dot.tourism.dao.DotLicOwnerLanguageDAO;
import sss.dot.tourism.dao.DotLicenseDAO;
import sss.dot.tourism.dao.DotLicenseOwnerDAO;
import sss.dot.tourism.dao.DotMsLanguageDAO;
import sss.dot.tourism.dao.DotSubLicenseDAO;
import sss.dot.tourism.dao.DotTrBillDetailDAO;
import sss.dot.tourism.dao.DotTrBillPaymentDAO;
import sss.dot.tourism.dao.DotTrFeeLicenseDAO;
import sss.dot.tourism.dao.DotTrFeePaymentDAO;

import com.sss.aut.service.User;

public interface IDotLicenseOwnerService {
	public void updateLicense(Object object ,User user) throws Exception;
	

}
