package sss.dot.tourism.service.chart;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.chart.ChartDAO;
import sss.dot.tourism.dao.mas.OrganizationDAO;
import sss.dot.tourism.domain.Organization;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.TraderCategory;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.User;

@Repository("chartService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ChartService implements IChartService{

	@Autowired
	ChartDAO chartDAO;
	
	@Autowired
	OrganizationDAO organizationDAO;
	
	//chart
	public List getChartBusiness(Object object, User user) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("###Service ChartService method getChartPieBusiness");
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถดึงข้อมูลกราฟแสดงจำนวนของธุรกิจนำเที่ยวจำแนกตามประเภทได้");
		}
		
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		RegistrationDTO params = (RegistrationDTO)object;
		
		//Business
		if(params.getChartName().equals("piebusinesstradercategorychart"))
		{
			list =  this.getChartPieBusinessByTraderCategory(object, user);
		}
		else if(params.getChartName().equals("piebusinessorgidchart"))
		{
			list =  this.getChartPieBusinessByOrgId(object, user);
		}
		else if(params.getChartName().equals("columnbusinessallchart"))
		{
			list =  this.getChartColumnBusinessAll(object, user);
		}
		else if(params.getChartName().equals("columnbusinessnewchart"))
		{
			list = this.getChartColumnBusinessNew(object, user);
		}
		else if(params.getChartName().equals("columnbusinessrenewchart"))
		{
			list = this.getChartColumnBusinessRenew(object, user);
		}
		
		//Guide
		else if(params.getChartName().equals("pieguideallchart"))
		{
			list = this.getChartPieGuideAll(object, user);
		}
		else if(params.getChartName().equals("columnguideallchart"))
		{
			list = this.findChartColumnGuideAll(object, user);
		}
		
		else if(params.getChartName().equals("pieguideorgchart"))
		{
			list = this.getChartPieGuideOrg(object, user);
		}
		else if(params.getChartName().equals("columnguideorgchart"))
		{
			list = this.findChartColumnGuideOrg(object, user);
		}

		
		return list;
		
	}
	
	
//Guide Chart
	
	//ค้นหาแผนภูุมิแสดงจำนวนมัคคุเทศก์ จำแนกเป็นรายภาค
	public List<RegistrationDTO> findChartColumnGuideOrg(Object object, User user)throws Exception
	{
		System.out.println("###Service ChartService method findChartColumnGuideOrg");
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถดึงข้อมูลแผนภูุมิแสดงจำนวนมัคคุเทศก์ จำแนกเป็นรายภาคได้");
		}
		
		Map model = new HashMap();
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		List<Organization> organizationlist = (List<Organization>)this.organizationDAO.findAll();
		
		RegistrationDTO obj = (RegistrationDTO)object;
		
		RegistrationDTO dtoNG = new RegistrationDTO();
		RegistrationDTO dtoNA = new RegistrationDTO();
		RegistrationDTO dtoCG = new RegistrationDTO();
		RegistrationDTO dtoCA = new RegistrationDTO();
		
//		if(!organizationlist.isEmpty())
//		{
//			for(Organization persis: organizationlist)
//			{
//				System.out.println("#####persis.getOrgId() = "+persis.getOrgId());
				
				List<RegistrationDTO> listObj = (List<RegistrationDTO>)findExcelGuideOrg(obj, model);
				
				if(!listObj.isEmpty())
				{
					for(RegistrationDTO sel: listObj)
					{
						if(sel.getOrgId() == 1)
						{
							dtoNG.setCount1(sel.getNewGeneralCount());
							dtoNA.setCount1(sel.getNewAreaCount());
							dtoCG.setCount1(sel.getCancleGeneralCount());
							dtoCA.setCount1(sel.getCancleAreaCount());	
							
							dtoNG.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(sel.getOrgId())).getOrgName());
							dtoNA.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(sel.getOrgId())).getOrgName());
							dtoCG.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(sel.getOrgId())).getOrgName());
							dtoCA.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(sel.getOrgId())).getOrgName());

						}
						if(sel.getOrgId() == 2)
						{
							dtoNG.setCount2(sel.getNewGeneralCount());
							dtoNA.setCount2(sel.getNewAreaCount());
							dtoCG.setCount2(sel.getCancleGeneralCount());
							dtoCA.setCount2(sel.getCancleAreaCount());		
							
							dtoNG.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(sel.getOrgId())).getOrgName());
							dtoNA.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(sel.getOrgId())).getOrgName());
							dtoCG.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(sel.getOrgId())).getOrgName());
							dtoCA.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(sel.getOrgId())).getOrgName());

						}
						if(sel.getOrgId() == 3)
						{
							dtoNG.setCount3(sel.getNewGeneralCount());
							dtoNA.setCount3(sel.getNewAreaCount());
							dtoCG.setCount3(sel.getCancleGeneralCount());
							dtoCA.setCount3(sel.getCancleAreaCount());		
							
							dtoNG.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(sel.getOrgId())).getOrgName());
							dtoNA.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(sel.getOrgId())).getOrgName());
							dtoCG.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(sel.getOrgId())).getOrgName());
							dtoCA.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(sel.getOrgId())).getOrgName());

						}
						if(sel.getOrgId() == 4)
						{
							dtoNG.setCount4(sel.getNewGeneralCount());
							dtoNA.setCount4(sel.getNewAreaCount());
							dtoCG.setCount4(sel.getCancleGeneralCount());
							dtoCA.setCount4(sel.getCancleAreaCount());	
							
							dtoNG.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(sel.getOrgId())).getOrgName());
							dtoNA.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(sel.getOrgId())).getOrgName());
							dtoCG.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(sel.getOrgId())).getOrgName());
							dtoCA.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(sel.getOrgId())).getOrgName());

						}
						if(sel.getOrgId() == 5)
						{
							dtoNG.setCount5(sel.getNewGeneralCount());
							dtoNA.setCount5(sel.getNewAreaCount());
							dtoCG.setCount5(sel.getCancleGeneralCount());
							dtoCA.setCount5(sel.getCancleAreaCount());	
							
							dtoNG.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(sel.getOrgId())).getOrgName());
							dtoNA.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(sel.getOrgId())).getOrgName());
							dtoCG.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(sel.getOrgId())).getOrgName());
							dtoCA.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(sel.getOrgId())).getOrgName());

						}
						
					}
				}
				
								
//			}//for org
//		}
		
		dtoNG.setLicenseStatusName("ยื่นใหม่ ทั่วไป");
		dtoNA.setLicenseStatusName("ยื่นใหม่ เฉพาะ");
		dtoCG.setLicenseStatusName("ยกเลิก ทั่วไป");
		dtoCA.setLicenseStatusName("ยกเลิก เฉพาะ");
		
		list.add(dtoNG);
		list.add(dtoNA);
		list.add(dtoCG);
		list.add(dtoCA);
		
		return list;
	}
	//ค้นหากราฟแสดงจำนวนมัคคุเทศก์ จำแนกเป็นรายภาค
	public List<RegistrationDTO> getChartPieGuideOrg(Object object, User user)throws Exception
	{
		System.out.println("###Service ChartService method getChartPieGuideOrg");
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถดึงข้อมูลกราฟแสดงจำนวนมัคคุเทศก์ จำแนกเป็นรายภาคได้");
		}
		
		Map model = new HashMap();
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		List<Organization> organizationlist = (List<Organization>)this.organizationDAO.findAll();
		
		RegistrationDTO obj = (RegistrationDTO)object;
		
//		if(!organizationlist.isEmpty())
//		{
//			for(Organization persis: organizationlist)
//			{
//				System.out.println("#####persis.getOrgId() = "+persis.getOrgId());
//				obj.setOrgId(persis.getOrgId());
				
				List<RegistrationDTO> listObj = (List<RegistrationDTO>)findExcelGuideOrg(obj, model);
				
				if(!listObj.isEmpty())
				{
					for(RegistrationDTO sel: listObj)
					{
						RegistrationDTO dto = new RegistrationDTO();
						if(sel.getOrgId() ==1)
						{
							dto.setCount(sel.getCountAll2());
							dto.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(sel.getOrgId())).getOrgName());
						}
						if(sel.getOrgId() ==2)
						{
							dto.setCount(sel.getCountAll2());
							dto.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(sel.getOrgId())).getOrgName());
						}
						if(sel.getOrgId() ==3)
						{
							dto.setCount(sel.getCountAll2());
							dto.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(sel.getOrgId())).getOrgName());
						}
						if(sel.getOrgId() ==4)
						{
							dto.setCount(sel.getCountAll2());
							dto.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(sel.getOrgId())).getOrgName());
						}
						if(sel.getOrgId() ==5)
						{
							dto.setCount(sel.getCountAll2());
							dto.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(sel.getOrgId())).getOrgName());
						}
						
						list.add(dto);
					}//for
				}
				
				
//			}//for org
//		}
		return list;
	}
	//ค้นหาแผนภูมิแสดงจำนวนการจดทะเบียนมัคคุเทศก์ จำแนกเป็นรายภาค
	
	//ค้นหากราฟแสดงจำนวนผู้ที่ได้รับใบอนุญาตเป็นมัคคุเทศก์ ในภาพรวม
	public List<RegistrationDTO> getChartPieGuideAll(Object object, User user)throws Exception
	{
		System.out.println("###Service ChartService method getChartPieGuideByTraderCategory");
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถดึงข้อมูลกราฟแสดงจำนวนผู้ที่ได้รับใบอนุญาตเป็นมัคคุเทศก์ ในภาพรวมได้");
		}
		
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		RegistrationDTO params = (RegistrationDTO)object;
		
		//มัคคุเทศก์ทั่วไป
		List<Object> listTraderCategory = new ArrayList<Object>();
		listTraderCategory.add(TraderCategory.PLATINUM_GUIDE.getStatus());
		listTraderCategory.add(TraderCategory.GOLD_GUIDE.getStatus());
		//มัคคุเทศก์เฉพาะพื้นที่
		listTraderCategory.add(TraderCategory.PINK_GUIDE.getStatus());
		listTraderCategory.add(TraderCategory.BLUE_GUIDE.getStatus());
		listTraderCategory.add(TraderCategory.GREEN_GUIDE.getStatus());
		listTraderCategory.add(TraderCategory.RED_GUIDE.getStatus());
		listTraderCategory.add(TraderCategory.ORANGE_GUIDE.getStatus());
		listTraderCategory.add(TraderCategory.YELLOW_GUIDE.getStatus());
		listTraderCategory.add(TraderCategory.VIOLET_GUIDE.getStatus());
		listTraderCategory.add(TraderCategory.BROWN_GUIDE.getStatus());
		
		for(Object traderCategory : listTraderCategory)
		{
			params.setTraderCategory(traderCategory.toString());
			
			List<Object[]> listObj = this.chartDAO.findChartPieGuideAll(params);
			
			if(!listObj.isEmpty())
			{
				for(Object sel: listObj)
				{
					RegistrationDTO dto = new RegistrationDTO();
					dto.setCount(Double.valueOf(sel.toString()));
					
					if((traderCategory.toString().equals(TraderCategory.PLATINUM_GUIDE.getStatus())))
					{
						dto.setTraderCategoryName(TraderCategory.PLATINUM_GUIDE.getMeaning());
					}
					if((traderCategory.toString().equals(TraderCategory.GOLD_GUIDE.getStatus())))
					{
						dto.setTraderCategoryName(TraderCategory.GOLD_GUIDE.getMeaning());
					}
					
					if((traderCategory.toString().equals(TraderCategory.PINK_GUIDE.getStatus())))
					{
						dto.setTraderCategoryName(TraderCategory.PINK_GUIDE.getMeaning());
					}
					
					if((traderCategory.toString().equals(TraderCategory.BLUE_GUIDE.getStatus())))
					{
						dto.setTraderCategoryName(TraderCategory.BLUE_GUIDE.getMeaning());
					}
					
					if((traderCategory.toString().equals(TraderCategory.GREEN_GUIDE.getStatus())))
					{
						dto.setTraderCategoryName(TraderCategory.GREEN_GUIDE.getMeaning());
					}
					
					if((traderCategory.toString().equals(TraderCategory.RED_GUIDE.getStatus())))
					{
						dto.setTraderCategoryName(TraderCategory.RED_GUIDE.getMeaning());
					}
					
					if((traderCategory.toString().equals(TraderCategory.ORANGE_GUIDE.getStatus())))
					{
						dto.setTraderCategoryName(TraderCategory.ORANGE_GUIDE.getMeaning());
					}
					
					if((traderCategory.toString().equals(TraderCategory.YELLOW_GUIDE.getStatus())))
					{
						dto.setTraderCategoryName(TraderCategory.YELLOW_GUIDE.getMeaning());
					}
					
					if((traderCategory.toString().equals(TraderCategory.VIOLET_GUIDE.getStatus())))
					{
						dto.setTraderCategoryName(TraderCategory.VIOLET_GUIDE.getMeaning());
					}
					
					if((traderCategory.toString().equals(TraderCategory.BROWN_GUIDE.getStatus())))
					{
						dto.setTraderCategoryName(TraderCategory.BROWN_GUIDE.getMeaning());
					}
					
					list.add(dto);
				}
			}

		}//for
		
		return list;
	}
	
	//ค้นหาแผนภูมิแสดงจำนวนผู้ที่ได้รับใบอนุญาตเป็นมัคคุเทศก์ ในภาพรวม
	public List<RegistrationDTO> findChartColumnGuideAll(Object object, User user)throws Exception
	{
		System.out.println("###Service ChartService method findChartColumnGuideAll");
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถดึงข้อมูลแผนภูมิแสดงจำนวนผู้ที่ได้รับใบอนุญาตเป็นมัคคุเทศก์ ในภาพรวมได้");
		}
		
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		RegistrationDTO obj = (RegistrationDTO)object;
		
		for(int i=0; i<2; i++)
		{	
			//มัคคุเทศก์ทั่วไป
			List<Object> listTraderCategory = new ArrayList<Object>();
			if(i==0)
			{
				
				listTraderCategory.add(TraderCategory.PLATINUM_GUIDE.getStatus());
				listTraderCategory.add(TraderCategory.GOLD_GUIDE.getStatus());
			}
			//มัคคุเทศก์เฉพาะพื้นที่
			if(i==1)
			{
				listTraderCategory.add(TraderCategory.PINK_GUIDE.getStatus());
				listTraderCategory.add(TraderCategory.BLUE_GUIDE.getStatus());
				listTraderCategory.add(TraderCategory.GREEN_GUIDE.getStatus());
				listTraderCategory.add(TraderCategory.RED_GUIDE.getStatus());
				listTraderCategory.add(TraderCategory.ORANGE_GUIDE.getStatus());
				listTraderCategory.add(TraderCategory.YELLOW_GUIDE.getStatus());
				listTraderCategory.add(TraderCategory.VIOLET_GUIDE.getStatus());
				listTraderCategory.add(TraderCategory.BROWN_GUIDE.getStatus());
			}
			
			Double countAll1 = new Double(0);
			Double countAll2 = new Double(0);
			Double countAll3 = new Double(0);
			Double countAll4 = new Double(0);
			Double countAll5 = new Double(0);
			
			//for ทีละประเภทมัคคุเทศก์ตาม traderCategory
			for(Object traderCategory : listTraderCategory)
			{
				obj.setTraderCategory(traderCategory.toString());

				List<Organization> organizationlist = (List<Organization>)this.organizationDAO.findAll();
				
//				Double countAllRow = new Double(0);
				
				if(!organizationlist.isEmpty())
				{	
					int j = 1;
					RegistrationDTO dto = new RegistrationDTO();
					for(Organization persis: organizationlist)
					{
						System.out.println("#####persis.getOrgId() = "+persis.getOrgId());
						obj.setOrgId(persis.getOrgId());
						
						List<Object[]> listObj = this.chartDAO.findExcelGuideAll(obj);
						
						if(!listObj.isEmpty())
						{
							for(Object sel: listObj)
							{
								if(j==1)
									countAll1 += Double.valueOf(sel.toString());
								if(j==2)
									countAll2 += Double.valueOf(sel.toString());
								if(j==3)
									countAll3 += Double.valueOf(sel.toString());
								if(j==4)
									countAll4 += Double.valueOf(sel.toString());
								if(j==5)
									countAll5 += Double.valueOf(sel.toString());
		
							}
						}
						j++;
					}//for org
					
				}
				
			}//for traderCategory
			
			//Add Sum
			RegistrationDTO dto = new RegistrationDTO();
			if(i==0)
				dto.setTraderCategoryName("มัคคุเทศก์ประเภททั่วไป");
			if(i==1)
				dto.setTraderCategoryName("มัคคุเทศก์ประเภทเฉพาะพื้นที่");
			
			dto.setCount1(countAll1);
			dto.setCount2(countAll2);
			dto.setCount3(countAll3);
			dto.setCount4(countAll4);
			dto.setCount5(countAll5);
//			dto.setCount(countAll1+countAll2+countAll3+countAll4+countAll5);
			list.add(dto);
			
		}
		
		return list;
	}
	
	
//Business Chart
	//ค้นหาแผนภูมิแสดงจำนวนผู้มายื่นขอชำระค่าธรรมเนียมประกอบธุรกิจนำเที่ยว ราย 2 ปี
	public List<RegistrationDTO> getChartColumnBusinessRenew(Object object, User user)throws Exception {
			// TODO Auto-generated method stub
			
			System.out.println("###Service ChartService method getChartColumnBusinessRenew");
			if(!(object instanceof RegistrationDTO))
			{
				throw new IllegalArgumentException("ไม่สามารถค้นหาแผนภูมิแสดงจำนวนผู้มายื่นขอชำระค่าธรรมเนียมประกอบธุรกิจนำเที่ยว ราย 2 ปี ได้");
			}
			
			List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
			
			List<Organization> organizationlist = (List<Organization>)this.organizationDAO.findAll();
			
			RegistrationDTO obj = (RegistrationDTO)object;
			
			if(!organizationlist.isEmpty())
			{
				for(Organization persis: organizationlist)
				{
					System.out.println("#####persis.getOrgId() = "+persis.getOrgId());
					obj.setOrgId(persis.getOrgId());
					
					List<Object[]> listObj = this.chartDAO.findExcelBusinessRenew(obj);
					
					RegistrationDTO dto = new RegistrationDTO();
					
					if(!listObj.isEmpty())
					{
						for(Object[] sel: listObj)
						{
							dto.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(persis.getOrgId())).getOrgName());
							dto.setOutBoundCount(Double.valueOf(sel[0].toString()));
							dto.setInBoundCount(Double.valueOf(sel[1].toString()));
							dto.setCountryCount(Double.valueOf(sel[2].toString()));
							dto.setAreaCount(Double.valueOf(sel[3].toString()));
						}
					}
					
					list.add(dto);
				}//for org
			}
			return list;
		}
	
	//ค้นหาแผนภูมิแสดงจำนวนผู้มายื่นเรื่องขอรับใบอนุญาตประกอบธุรกิจนำเที่ยว (รายใหม่)
	public List<RegistrationDTO> getChartColumnBusinessNew(Object object, User user)throws Exception {
		// TODO Auto-generated method stub
		
		System.out.println("###Service ChartService method getChartColumnBusinessNew");
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถค้นหาแผนภูมิแสดงจำนวนผู้มายื่นเรื่องขอรับใบอนุญาตประกอบธุรกิจนำเที่ยว (รายใหม่) ได้");
		}
		
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		List<Organization> organizationlist = (List<Organization>)this.organizationDAO.findAll();
		
		RegistrationDTO obj = (RegistrationDTO)object;
		
		if(!organizationlist.isEmpty())
		{
			for(Organization persis: organizationlist)
			{
				System.out.println("#####persis.getOrgId() = "+persis.getOrgId());
				obj.setOrgId(persis.getOrgId());
				
				List<Object[]> listObj = this.chartDAO.findExcelBusinessByTraderCategory(obj);
				
				RegistrationDTO dto = new RegistrationDTO();
				
				if(!listObj.isEmpty())
				{
					for(Object[] sel: listObj)
					{
						dto.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(persis.getOrgId())).getOrgName());
						dto.setOutBoundCount(Double.valueOf(sel[0].toString()));
						dto.setInBoundCount(Double.valueOf(sel[1].toString()));
						dto.setCountryCount(Double.valueOf(sel[2].toString()));
						dto.setAreaCount(Double.valueOf(sel[3].toString()));
					}
				}
				
				list.add(dto);
			}//for org
		}
		return list;
	}
	
	//ค้นหาแผนภูมิแสดงผู้มาดำเนินการเกี่ยวกับธุรกิจนำเที่ยวในภาพรวม
	public List<RegistrationDTO> getChartColumnBusinessAll(Object object, User user)throws Exception {
		// TODO Auto-generated method stub
		
		System.out.println("###Service ChartService method getChartColumnBusinessAll");
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถค้นหาแผนภูมิแสดงผู้มาดำเนินการเกี่ยวกับธุรกิจนำเที่ยวในภาพรวมได้");
		}
		
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		List<Organization> organizationlist = (List<Organization>)this.organizationDAO.findAll();
		
		RegistrationDTO obj = (RegistrationDTO)object;
		
		if(!organizationlist.isEmpty())
		{
		  for(int i=1; i<4; i++)
		  {
			RegistrationDTO dto = new RegistrationDTO();
			
			int j=1;
			for(Organization persis: organizationlist)
			{
				System.out.println("#####persis.getOrgId() = "+persis.getOrgId());
				obj.setOrgId(persis.getOrgId());
				
				List<Object[]> listObj = this.chartDAO.findExcelBusinessAll(obj);
				
				if(!listObj.isEmpty())
				{
					for(Object[] sel: listObj)
					{
						if(j==1)
						{dto.setCount1(Double.valueOf(sel[i].toString()));}
						if(j==2)
						{dto.setCount2(Double.valueOf(sel[i].toString()));}
						if(j==3)
						{dto.setCount3(Double.valueOf(sel[i].toString()));}
						if(j==4)
						{dto.setCount4(Double.valueOf(sel[i].toString()));}
						if(j==5)
						{dto.setCount5(Double.valueOf(sel[i].toString()));}
						
//						dto.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(persis.getOrgId())).getOrgName());
//						dto.setNewCount(Double.valueOf(sel[1].toString()));
//						dto.setCancleCount(Double.valueOf(sel[2].toString()));
//						dto.setRevokeCount(Double.valueOf(sel[3].toString()));
						
					}
				}
				j++;
			}//for org
			
			if(i==1)
				dto.setLicenseStatusName("รายใหม่");
			if(i==2)
				dto.setLicenseStatusName("ยกเลิก");
			if(i==3)
				dto.setLicenseStatusName("เพิกถอน");
			
			list.add(dto);
		  }
			
		}

		
		return list;
	}
	
	//ค้นหากราฟแสดงจำนวนของธุรกิจนำเที่ยวจำแนกตามสาขา
	public List<RegistrationDTO> getChartPieBusinessByOrgId(Object object, User user)
			throws Exception {
		// TODO Auto-generated method stub
		
		System.out.println("###Service ChartService method getChartPieBusinessByOrgId");
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถดึงข้อมูลแสดงจำนวนของธุรกิจนำเที่ยวจำแนกตามประเภทสาขาได้");
		}
		
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		RegistrationDTO params = (RegistrationDTO)object;
		
		List<Object[]> listObj = this.chartDAO.findChartPieBusinessByOrgId(params, user);
		
		if(!listObj.isEmpty())
		{
			for(Object[] sel: listObj)
			{
				RegistrationDTO dto = new RegistrationDTO();
				
				dto.setOrgName( ((Organization)this.organizationDAO.findByPrimaryKey(Long.valueOf(sel[0].toString()))).getOrgName() );
				
				dto.setCount(Double.valueOf(sel[1].toString()));
				
				list.add(dto);
			}
		}
		
		return list;
	}

	//ค้นหากราฟแสดงจำนวนของธุรกิจนำเที่ยวจำแนกตามประเภทการจดทะเบียน
	public List<RegistrationDTO> getChartPieBusinessByTraderCategory(Object object, User user)
			throws Exception {
		// TODO Auto-generated method stub
		
		System.out.println("###Service ChartService method getChartBusinessByTraderCategory");
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถดึงข้อมูลแสดงจำนวนของธุรกิจนำเที่ยวจำแนกตามประเภทการจดทะเบียนได้");
		}
		
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		RegistrationDTO params = (RegistrationDTO)object;
		
		List<Object[]> listObj = this.chartDAO.findChartPieBusinessByTraderCategory(params, user);
		
		if(!listObj.isEmpty())
		{
			for(Object[] sel: listObj)
			{
				RegistrationDTO dto = new RegistrationDTO();

				if(TraderCategory.OUTBOUND.getStatus().equals(sel[0].toString()))
				{
					dto.setTraderCategoryName(TraderCategory.OUTBOUND.getMeaning());
				}
				else if(TraderCategory.INBOUND.getStatus().equals(sel[0].toString()))
				{
					dto.setTraderCategoryName(TraderCategory.INBOUND.getMeaning());
				} 
				else if(TraderCategory.COUNTRY.getStatus().equals(sel[0].toString()))
				{
					dto.setTraderCategoryName(TraderCategory.COUNTRY.getMeaning());
				}
				else if(TraderCategory.AREA.getStatus().equals(sel[0].toString()))
				{
					dto.setTraderCategoryName(TraderCategory.AREA.getMeaning());
				}
				
				dto.setCount(Double.valueOf(sel[1].toString()));
				
				list.add(dto);
			}
		}
		
		return list;
	}

//excel
	public Map<String, ?> getDetail(RegistrationDTO obj, int firstrow, int pagesize) throws Exception {
		// TODO Auto-generated method stub
		
		System.out.println("Service ChartService method getDetail");
		
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		DateFormat df =DateUtils.getProcessDateFormatThai();
		DateFormat df2 =DateUtils.getDisplayDateFormatTH();
		
		String approveDateFrom = "";
		String approveDateTo = "";

		Map model = new HashMap();
		
		try{
			if(obj.getApproveDateFrom() != null && !obj.getApproveDateFrom().equals(""))
			{
				Date dateForm = df.parse(obj.getApproveDateFrom());
				approveDateFrom = df2.format(dateForm);
				if(obj.getApproveDateTo() != null && !obj.getApproveDateTo().equals(""))
				{
					Date dateTo = df.parse(obj.getApproveDateTo());
					approveDateTo = df2.format(dateTo);
				}
				else
				{
					approveDateTo = df2.format(new Date());
				}
				
				model.put("approveDate", "ตั้งแต่  "+approveDateFrom+"  ถึง  "+approveDateTo);
			}
			model.put("date", df2.format(new Date()));
			
			//BUSINESS
			if(obj.getChartName().equals("excelbusinesstradercategorypiechart"))
			{
				list = this.findExcelBusinessByTraderCategory(obj, model);
			}
			else if(obj.getChartName().equals("excelbusinessallcolumnchart"))
			{
				list = this.findExcelBusinessAll(obj, model);
			}
			else if(obj.getChartName().equals("excelbusinessnewcolumnchart"))
			{
				list = this.findExcelBusinessNew(obj, model);
			}
			else if(obj.getChartName().equals("excelbusinessrenewcolumnchart"))
			{
				list = this.findExcelBusinessRenew(obj, model);
			}
			else if(obj.getChartName().equals("excelbusinessguaranteecolumnchart"))//5
			{
				list = this.findExcelBusinessGuarantee(obj, model);
			}
			else if(obj.getChartName().equals("excelbusinesssuspensionandrevokecolumnchart"))
			{
				list = this.findExcelBusinessSuspensionAndRevoke(obj, model);
			}
			
			
			//GUIDE
			else if(obj.getChartName().equals("excelguidetypecolumnchart"))
			{
				list = this.findExcelGuideType(obj, model);
			}
			else if(obj.getChartName().equals("excelguideallcolumnchart"))
			{
				list = this.findExcelGuideAll(obj, model);
			}
			else if(obj.getChartName().equals("excelguidenewcolumnchart"))
			{
				list = this.findExcelGuideNew(obj, model);
			}
			else if(obj.getChartName().equals("excelguiderenewcolumnchart"))
			{
				list = this.findExcelGuideRenew(obj, model);
			}
			else if(obj.getChartName().equals("excelguideorgcolumnchart"))
			{
				list = this.findExcelGuideOrg(obj, model);
			}
			
			if(list.isEmpty())
			{
				RegistrationDTO xdto = new RegistrationDTO();
				list.add(xdto);
			}
			
			model.put("chartdetail", list);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new Exception(e.getMessage());

		}
		return model;
	}
	
//Business Excel
	//ค้นหาตารางแสดงจำนวนของธุรกิจนำเที่ยวจำแนกตามประเภท (ค้นหาทีละสำนักงานตาม ordId)
	public List<RegistrationDTO> findExcelBusinessByTraderCategory(RegistrationDTO obj, Map model)throws Exception
	{
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		List<Organization> organizationlist = (List<Organization>)this.organizationDAO.findAll(0);
		
		if(!organizationlist.isEmpty())
		{ 
			Double outBoundCountAll = new Double(0);
			Double inBoundCountAll = new Double(0);
			Double countryCountAll = new Double(0);
			Double areaCountAll = new Double(0);
			
			for(Organization persis: organizationlist)
			{
				System.out.println("#####persis.getOrgId() = "+persis.getOrgId());
				obj.setOrgId(persis.getOrgId());
				
				List<Object[]> listObj = this.chartDAO.findExcelBusinessByTraderCategory2(obj);
				
				if(!listObj.isEmpty())
				{
					for(Object[] sel: listObj)
					{
						
						RegistrationDTO dto = new RegistrationDTO();
						dto.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(persis.getOrgId())).getOrgName());
						dto.setOutBoundCount(Double.valueOf(sel[0].toString()));
						dto.setInBoundCount(Double.valueOf(sel[1].toString()));
						dto.setCountryCount(Double.valueOf(sel[2].toString()));
						dto.setAreaCount(Double.valueOf(sel[3].toString()));
						dto.setCount(Double.valueOf(sel[0].toString())+Double.valueOf(sel[1].toString())+Double.valueOf(sel[2].toString())+Double.valueOf(sel[3].toString()));
						list.add(dto);
						
						//Sum
						outBoundCountAll += Double.valueOf(sel[0].toString());
						inBoundCountAll += Double.valueOf(sel[1].toString());
						countryCountAll += Double.valueOf(sel[2].toString());
						areaCountAll += Double.valueOf(sel[3].toString());
						
					}
				}
				
			}//for
			
			//Add Sum
			RegistrationDTO dto = new RegistrationDTO();
			dto.setOrgName("รวม");
			dto.setOutBoundCount(outBoundCountAll);
			dto.setInBoundCount(inBoundCountAll);
			dto.setCountryCount(countryCountAll);
			dto.setAreaCount(areaCountAll);
			dto.setCount(outBoundCountAll+inBoundCountAll+countryCountAll+areaCountAll);
			list.add(dto);
			
		}

		
		return list;
	}
	
	//ค้นหาตารางแสดงจำนวนผู้มาดำเนินการเกี่ยวกับธุรกิจนำเที่ยวในภาพรวม (ค้นหาทีละสำนักงานตาม ordId)
	public List<RegistrationDTO> findExcelBusinessAll(RegistrationDTO obj, Map model)throws Exception
	{
		System.out.println("###Service ChartService  Method findExcelBusinessAll");
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		List<Organization> organizationlist = (List<Organization>)this.organizationDAO.findAll(0);
		
		if(!organizationlist.isEmpty())
		{
			Double oldCountAll = new Double(0);
			Double newCountAll = new Double(0);
			Double cancleCountAll = new Double(0);
			Double revokeCountAll = new Double(0);
			
			for(Organization persis: organizationlist)
			{
				System.out.println("#####persis.getOrgId() = "+persis.getOrgId());
				obj.setOrgId(persis.getOrgId());
				
				List<Object[]> listObj = this.chartDAO.findExcelBusinessAll(obj);
				
				if(!listObj.isEmpty())
				{
					for(Object[] sel: listObj)
					{
						
						RegistrationDTO dto = new RegistrationDTO();
						dto.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(persis.getOrgId())).getOrgName());
						dto.setOldCount(Double.valueOf(sel[0].toString()));
						dto.setNewCount(Double.valueOf(sel[1].toString()));
						dto.setCancleCount(Double.valueOf(sel[2].toString()));
						dto.setRevokeCount(Double.valueOf(sel[3].toString()));
						dto.setCount(Double.valueOf(sel[0].toString())+Double.valueOf(sel[1].toString())-Double.valueOf(sel[2].toString())-Double.valueOf(sel[3].toString()));
						list.add(dto);
						
						//Sum
						oldCountAll += Double.valueOf(sel[0].toString());
						newCountAll += Double.valueOf(sel[1].toString());
						cancleCountAll += Double.valueOf(sel[2].toString());
						revokeCountAll += Double.valueOf(sel[3].toString());
						
					}
				}
				
			}//for
			
			//Add Sum
			RegistrationDTO dto = new RegistrationDTO();
			dto.setOrgName("รวม");
			dto.setOldCount(oldCountAll);
			dto.setNewCount(newCountAll);
			dto.setCancleCount(cancleCountAll);
			dto.setRevokeCount(revokeCountAll);
			dto.setCount(oldCountAll+newCountAll-cancleCountAll-revokeCountAll);
			list.add(dto);
			
		}

		
		return list;
	}
	
	//ค้นหาตารางแสดงจำนวนผู้มายื่นเรื่องขอรับใบอนุญาตประกอบธุรกิจนำเที่ยว(รายใหม่) (ค้นหาทีละสำนักงานตาม ordId)
	public List<RegistrationDTO> findExcelBusinessNew(RegistrationDTO obj, Map model)throws Exception
	{
		System.out.println("###Service ChartService  Method findExcelBusinessNew");
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		List<Organization> organizationlist = (List<Organization>)this.organizationDAO.findAll(0);
		
		if(!organizationlist.isEmpty())
		{
			Double outBoundCountAll = new Double(0);
			Double inBoundCountAll = new Double(0);
			Double countryCountAll = new Double(0);
			Double areaCountAll = new Double(0);
			
			for(Organization persis: organizationlist)
			{
				System.out.println("#####persis.getOrgId() = "+persis.getOrgId());
				obj.setOrgId(persis.getOrgId());
				
				List<Object[]> listObj = this.chartDAO.findExcelBusinessByTraderCategory(obj);
				
				if(!listObj.isEmpty())
				{
					for(Object[] sel: listObj)
					{
						
						RegistrationDTO dto = new RegistrationDTO();
						dto.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(persis.getOrgId())).getOrgName());
						dto.setOutBoundCount(Double.valueOf(sel[0].toString()));
						dto.setInBoundCount(Double.valueOf(sel[1].toString()));
						dto.setCountryCount(Double.valueOf(sel[2].toString()));
						dto.setAreaCount(Double.valueOf(sel[3].toString()));
						dto.setCount(Double.valueOf(sel[0].toString())+Double.valueOf(sel[1].toString())+Double.valueOf(sel[2].toString())+Double.valueOf(sel[3].toString()));
						list.add(dto);
						
						//Sum
						outBoundCountAll += Double.valueOf(sel[0].toString());
						inBoundCountAll += Double.valueOf(sel[1].toString());
						countryCountAll += Double.valueOf(sel[2].toString());
						areaCountAll += Double.valueOf(sel[3].toString());
						
					}
				}
				
			}//for
			
			//Add Sum
			RegistrationDTO dto = new RegistrationDTO();
			dto.setOrgName("รวม");
			dto.setOutBoundCount(outBoundCountAll);
			dto.setInBoundCount(inBoundCountAll);
			dto.setCountryCount(countryCountAll);
			dto.setAreaCount(areaCountAll);
			dto.setCount(outBoundCountAll+inBoundCountAll+countryCountAll+areaCountAll);
			list.add(dto);
			
		}

		
		return list;
	}

	//ค้นหาตารางแสดงจำนวนผู้มายื่นขอชำระค่าธรรมเนียมประกอบธุรกิจนำเที่ยว ราย 2 ปี (ค้นหาทีละสำนักงานตาม ordId)
	public List<RegistrationDTO> findExcelBusinessRenew(RegistrationDTO obj, Map model)throws Exception
	{
		System.out.println("###Service ChartService  Method findExcelBusinessRenew");
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		List<Organization> organizationlist = (List<Organization>)this.organizationDAO.findAll(0);
		
		if(!organizationlist.isEmpty())
		{
			Double outBoundCountAll = new Double(0);
			Double inBoundCountAll = new Double(0);
			Double countryCountAll = new Double(0);
			Double areaCountAll = new Double(0);
			
			for(Organization persis: organizationlist)
			{
				System.out.println("#####persis.getOrgId() = "+persis.getOrgId());
				obj.setOrgId(persis.getOrgId());
				
				List<Object[]> listObj = this.chartDAO.findExcelBusinessRenew(obj);
				
				if(!listObj.isEmpty())
				{
					for(Object[] sel: listObj)
					{
						
						RegistrationDTO dto = new RegistrationDTO();
						dto.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(persis.getOrgId())).getOrgName());
						dto.setOutBoundCount(Double.valueOf(sel[0].toString()));
						dto.setInBoundCount(Double.valueOf(sel[1].toString()));
						dto.setCountryCount(Double.valueOf(sel[2].toString()));
						dto.setAreaCount(Double.valueOf(sel[3].toString()));
						dto.setCount(Double.valueOf(sel[0].toString())+Double.valueOf(sel[1].toString())+Double.valueOf(sel[2].toString())+Double.valueOf(sel[3].toString()));
						list.add(dto);
						
						//Sum
						outBoundCountAll += Double.valueOf(sel[0].toString());
						inBoundCountAll += Double.valueOf(sel[1].toString());
						countryCountAll += Double.valueOf(sel[2].toString());
						areaCountAll += Double.valueOf(sel[3].toString());
						
					}
				}
				
			}//for
			
			//Add Sum
			RegistrationDTO dto = new RegistrationDTO();
			dto.setOrgName("รวม");
			dto.setOutBoundCount(outBoundCountAll);
			dto.setInBoundCount(inBoundCountAll);
			dto.setCountryCount(countryCountAll);
			dto.setAreaCount(areaCountAll);
			dto.setCount(outBoundCountAll+inBoundCountAll+countryCountAll+areaCountAll);
			list.add(dto);
			
		}

		
		return list;
	}
	
	//ตารางแสดง ยกเลิกและรับหลักประกันคืนและขอรับหลักประกันคืน(กรณีถูกเพิกถอน) (ค้นหาทีละสำนักงานตาม ordId)
	public List<RegistrationDTO> findExcelBusinessGuarantee(RegistrationDTO obj, Map model)throws Exception
	{
		System.out.println("###Service ChartService  Method findExcelBusinessGuarantee");
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		List<Organization> organizationlist = (List<Organization>)this.organizationDAO.findAll(0);
		
		if(!organizationlist.isEmpty())
		{
			Double outBoundCancleCount = new Double(0);
			Double inBoundCancleCount = new Double(0);
			Double countryCancleCount = new Double(0);
			Double areaCancleCount = new Double(0);
			
			Double outBoundRevokeCount = new Double(0);
			Double inBoundRevokeCount = new Double(0);
			Double countryRevokeCount = new Double(0);
			Double areaRevokeCount = new Double(0);
			
			Double oldCountAll = new Double(0);
			Double cancleCountAll = new Double(0);
			Double revokeCountAll = new Double(0);
			Double countAll = new Double(0);
			
			for(Organization persis: organizationlist)
			{
				System.out.println("#####persis.getOrgId() = "+persis.getOrgId());
				obj.setOrgId(persis.getOrgId());
				
				//หายอดเดิม + จดใหม่
				RegistrationDTO dto = new RegistrationDTO();
				List<Object[]> listObj = this.chartDAO.findBusinessOldNew(obj);
				
				if(!listObj.isEmpty())
				{
					for(Object[] sel: listObj)
					{
						dto.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(persis.getOrgId())).getOrgName());
						dto.setOldCount(Double.valueOf(sel[0].toString())+Double.valueOf(sel[1].toString()));
					}
				}
				
				//ยกเลิกและรับหลักประกันคืน
				List<Object[]> listCancle = this.chartDAO.findBusinessCancleGuarantee(obj);
				if(!listCancle.isEmpty())
				{
					for(Object[] sel: listCancle)
					{
						//Cancle
						dto.setOutBoundCount(Double.valueOf(sel[0].toString()));
						dto.setInBoundCount(Double.valueOf(sel[1].toString()));
						dto.setCountryCount(Double.valueOf(sel[2].toString()));
						dto.setAreaCount(Double.valueOf(sel[3].toString()));
						dto.setCancleCount(Double.valueOf(sel[0].toString())+Double.valueOf(sel[1].toString())+Double.valueOf(sel[2].toString())+Double.valueOf(sel[3].toString()));
					}
					
				}
				
				//ขอรับหลักประกันคืน(กรณีถูกเพิกถอน)
				List<Object[]> listRevoke = this.chartDAO.findBusinessRevokeGuarantee(obj);
				if(!listRevoke.isEmpty())
				{
					for(Object[] sel: listRevoke)
					{
						//Revoke
						dto.setOutBoundRevokeCount(Double.valueOf(sel[0].toString()));
						dto.setInBoundRevokeCount(Double.valueOf(sel[1].toString()));
						dto.setCountryRevokeCount(Double.valueOf(sel[2].toString()));
						dto.setAreaRevokeCount(Double.valueOf(sel[3].toString()));
						dto.setRevokeCount(Double.valueOf(sel[0].toString())+Double.valueOf(sel[1].toString())+Double.valueOf(sel[2].toString())+Double.valueOf(sel[3].toString()));
					}
					
				}
				
				dto.setCountAll(dto.getOldCount()-dto.getCancleCount()-dto.getRevokeCount());
				list.add(dto);
				
				
				outBoundCancleCount += dto.getOutBoundCount();
				inBoundCancleCount += dto.getInBoundCount();
				countryCancleCount += dto.getCountryCount();
				areaCancleCount += dto.getAreaCount();
				
				outBoundRevokeCount += dto.getOutBoundRevokeCount();
				inBoundRevokeCount += dto.getInBoundRevokeCount();
				countryRevokeCount += dto.getCountryRevokeCount();
				areaRevokeCount += dto.getAreaRevokeCount();
				
				oldCountAll += dto.getOldCount();
				cancleCountAll += dto.getCancleCount();
				revokeCountAll += dto.getRevokeCount();
				countAll += dto.getCountAll();
				
			}//for orgId
			
			//Add Sum
			RegistrationDTO dto = new RegistrationDTO();
			dto.setOrgName("รวม");
			dto.setOldCount(oldCountAll);
			
			dto.setOutBoundCount(outBoundCancleCount);
			dto.setInBoundCount(inBoundCancleCount);
			dto.setCountryCount(countryCancleCount);
			dto.setAreaCount(areaCancleCount);
			dto.setCancleCount(cancleCountAll);
			
			dto.setOutBoundRevokeCount(outBoundRevokeCount);
			dto.setInBoundRevokeCount(inBoundRevokeCount);
			dto.setCountryRevokeCount(countryRevokeCount);
			dto.setAreaRevokeCount(areaRevokeCount);
			dto.setRevokeCount(revokeCountAll);

			dto.setCountAll(countAll);
			
			list.add(dto);
			
		}

		
		return list;
	}

	//ค้าหาตารางแสดงจำนวนพักใช้และถูกเพิกถอนใบอนุญาตประกอบธุรกิจนำเที่ยว (ค้นหาทีละสำนักงานตาม ordId)
	public List<RegistrationDTO> findExcelBusinessSuspensionAndRevoke(RegistrationDTO obj, Map model)throws Exception
	{
		System.out.println("###Service ChartService  Method findExcelBusinessSuspensionAndRevoke");
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		List<Organization> organizationlist = (List<Organization>)this.organizationDAO.findAll(0);
		
		if(!organizationlist.isEmpty())
		{
			Double outBoundCountAll = new Double(0);
			Double inBoundCountAll = new Double(0);
			Double countryCountAll = new Double(0);
			Double areaCountAll = new Double(0);
			
			Double outBoundCountRevokeAll = new Double(0);
			Double inBoundCountRevokeAll = new Double(0);
			Double countryCountRevokeAll = new Double(0);
			Double areaCountRevokeAll = new Double(0);
			
			for(Organization persis: organizationlist)
			{
				System.out.println("#####persis.getOrgId() = "+persis.getOrgId());
				obj.setOrgId(persis.getOrgId());
				
				List<Object[]> listObj = this.chartDAO.findExcelBusinessSuspensionAndRevoke(obj);
				
				if(!listObj.isEmpty())
				{
					for(Object[] sel: listObj)
					{
						
						RegistrationDTO dto = new RegistrationDTO();
						dto.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(persis.getOrgId())).getOrgName());
						
						//Suspension
						dto.setOutBoundCount(Double.valueOf(sel[0].toString()));
						dto.setInBoundCount(Double.valueOf(sel[1].toString()));
						dto.setCountryCount(Double.valueOf(sel[2].toString()));
						dto.setAreaCount(Double.valueOf(sel[3].toString()));
						dto.setCount(Double.valueOf(sel[0].toString())+Double.valueOf(sel[1].toString())+Double.valueOf(sel[2].toString())+Double.valueOf(sel[3].toString()));
						
						//Revoke
						dto.setOutBoundRevokeCount(Double.valueOf(sel[4].toString()));
						dto.setInBoundRevokeCount(Double.valueOf(sel[5].toString()));
						dto.setCountryRevokeCount(Double.valueOf(sel[6].toString()));
						dto.setAreaRevokeCount(Double.valueOf(sel[7].toString()));
						dto.setCountRevoke(Double.valueOf(sel[4].toString())+Double.valueOf(sel[5].toString())+Double.valueOf(sel[6].toString())+Double.valueOf(sel[7].toString()));
						
						list.add(dto);
						
						//Sum Suspension
						outBoundCountAll += Double.valueOf(sel[0].toString());
						inBoundCountAll += Double.valueOf(sel[1].toString());
						countryCountAll += Double.valueOf(sel[2].toString());
						areaCountAll += Double.valueOf(sel[3].toString());
						
						//Sum Revoke
						outBoundCountRevokeAll += Double.valueOf(sel[4].toString());
						inBoundCountRevokeAll += Double.valueOf(sel[5].toString());
						countryCountRevokeAll += Double.valueOf(sel[6].toString());
						areaCountRevokeAll += Double.valueOf(sel[7].toString());
						
					}
				}
				
			}//for
			
			//Add Sum
			RegistrationDTO dto = new RegistrationDTO();
			dto.setOrgName("รวม");
			
			//Suspension
			dto.setOutBoundCount(outBoundCountAll);
			dto.setInBoundCount(inBoundCountAll);
			dto.setCountryCount(countryCountAll);
			dto.setAreaCount(areaCountAll);
			dto.setCount(outBoundCountAll+inBoundCountAll+countryCountAll+areaCountAll);
			
			//Revoke
			dto.setOutBoundRevokeCount(outBoundCountRevokeAll);
			dto.setInBoundRevokeCount(inBoundCountRevokeAll);
			dto.setCountryRevokeCount(countryCountRevokeAll);
			dto.setAreaRevokeCount(areaCountRevokeAll);
			dto.setCountRevoke(outBoundCountRevokeAll+inBoundCountRevokeAll+countryCountRevokeAll+areaCountRevokeAll);

			list.add(dto);
			
		}

		
		return list;
	}

//GUIDE Excel
	
	//รายงานการจดทะเบียนมัคคุเทศก์จำแนกเป็นรายภาค (ค้นหาทีละสำนักงานตาม ordId)
	public List<RegistrationDTO> findExcelGuideOrg(RegistrationDTO obj, Map model)throws Exception
	{
		System.out.println("###Service ChartService  Method findExcelGuideOrg");
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		List<Organization> organizationlist = (List<Organization>)this.organizationDAO.findAll(0);
		
		Double countOldAll = new Double(0);
		Double newGeneralCountAll = new Double(0);
		Double newAreaCountAll = new Double(0);
		Double cancleGeneralCountAll = new Double(0);
		Double cancleAreaCountAll = new Double(0);
		Double countAllAll1 = new Double(0);
		Double countAllAll2 = new Double(0);

		
		if(!organizationlist.isEmpty())
		{	
			for(Organization persis: organizationlist)
			{
				RegistrationDTO dto = new RegistrationDTO();
				
				obj.setOrgId(persis.getOrgId());

				Double GERNERAL_GUIDE = new Double(0);
				Double AREA_GUIDE = new Double(0);
				
				//หา cancle ทั้งหมดในช่วงวันที่เลือก
				List<Object[]> listDeactivateLicense = this.chartDAO.findGuideDeactivateLicense(obj);
				if(!listDeactivateLicense.isEmpty())
				{
					//for เพื่อนับจำนวน cancle ในแต่ละประเภทมัคคุเทศก์
					for(Object sel: listDeactivateLicense)
					{
						String licenseNo = sel.toString();
						System.out.println(">>>licenseNo = "+licenseNo);
						obj.setLicenseNo(licenseNo);
						
						List<Object[]> listTrader = this.chartDAO.findGuideTraderCategory(obj);
						
						if(!listTrader.isEmpty())
						{
							//for เพื่อนับจำนวน cancle ในแต่ละประเภทมัคคุเทศก์
							for(Object sel2: listTrader)
							{
								if(sel2.toString().equals(TraderCategory.PLATINUM_GUIDE.getStatus()))
									GERNERAL_GUIDE++;
								
								if(sel2.toString().equals(TraderCategory.GOLD_GUIDE.getStatus()))
									GERNERAL_GUIDE++;
								
								if(sel2.toString().equals(TraderCategory.PINK_GUIDE.getStatus()))
									AREA_GUIDE++;
								
								if(sel2.toString().equals(TraderCategory.BLUE_GUIDE.getStatus()))
									AREA_GUIDE++;
								
								if(sel2.toString().equals(TraderCategory.GREEN_GUIDE.getStatus()))
									AREA_GUIDE++;
								
								if(sel2.toString().equals(TraderCategory.RED_GUIDE.getStatus()))
									AREA_GUIDE++;
								
								if(sel2.toString().equals(TraderCategory.ORANGE_GUIDE.getStatus()))
									AREA_GUIDE++;
								
								if(sel2.toString().equals(TraderCategory.YELLOW_GUIDE.getStatus()))
									AREA_GUIDE++;
								
								if(sel2.toString().equals(TraderCategory.VIOLET_GUIDE.getStatus()))
									AREA_GUIDE++;
								
								if(sel2.toString().equals(TraderCategory.BROWN_GUIDE.getStatus()))
									AREA_GUIDE++;
							}//for
						}
					}//for cancle
				}
				
				dto.setOrgId(persis.getOrgId());
				dto.setOrgName(((Organization)this.organizationDAO.findByPrimaryKey(persis.getOrgId())).getOrgName());
				dto.setCancleGeneralCount(GERNERAL_GUIDE);
				dto.setCancleAreaCount(AREA_GUIDE);
				
				//ยอดรวม ยื่นใหม่
				List<Object[]> listObj = this.chartDAO.findExcelGuideOrg(obj);
				if(!listObj.isEmpty())
				{
					for(Object[] sel: listObj)
					{
						dto.setOldCount(Double.valueOf(sel[0].toString()));
						dto.setNewGeneralCount(Double.valueOf(sel[1].toString()));
						dto.setNewAreaCount(Double.valueOf(sel[2].toString()));
					}
				}
				
				dto.setCountAll1(dto.getOldCount()+dto.getNewGeneralCount()+dto.getNewAreaCount());
				dto.setCountAll2(dto.getCountAll1()-dto.getCancleGeneralCount()-dto.getCancleAreaCount());
				
				
				countOldAll += dto.getOldCount();
				newGeneralCountAll += dto.getNewGeneralCount();
				newAreaCountAll += dto.getNewAreaCount();
				cancleGeneralCountAll += dto.getCancleGeneralCount();
				cancleAreaCountAll += dto.getCancleAreaCount();
				countAllAll1 += dto.getCountAll1();
				countAllAll2 += dto.getCountAll2();
				
				list.add(dto);
			}//for org
			
			RegistrationDTO dto = new RegistrationDTO();
			dto.setOrgName("รวม");
			dto.setOldCount(countOldAll);
			dto.setNewGeneralCount(newGeneralCountAll);
			dto.setNewAreaCount(newAreaCountAll);
			dto.setCancleGeneralCount(cancleGeneralCountAll);
			dto.setCancleAreaCount(cancleAreaCountAll);
			dto.setCountAll1(countAllAll1);
			dto.setCountAll2(countAllAll2);
			list.add(dto);
		}
		
		return list;
	}
	
	//ค้นหา ภาพรวม มัคคุเทศก์ ทั้งส่วนกลาง และสาขา จำแนกตามประเภท (ค้นหาทีละประเภทมัคคุเทศก์ตาม traderCategory)
	public List<RegistrationDTO> findExcelGuideType(RegistrationDTO obj, Map model)throws Exception
	{
		System.out.println("###Service ChartService  Method findExcelGuideType");
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		Double PLATINUM_GUIDE = new Double(0);
		Double GOLD_GUIDE = new Double(0);
		Double PINK_GUIDE = new Double(0);
		Double BLUE_GUIDE = new Double(0);
		Double GREEN_GUIDE = new Double(0);
		Double RED_GUIDE = new Double(0);
		Double ORANGE_GUIDE = new Double(0);
		Double YELLOW_GUIDE = new Double(0);
		Double VIOLET_GUIDE = new Double(0);
		Double BROWN_GUIDE = new Double(0);
		
		//หา cancle ทั้งหมดในช่วงวันที่เลือก
		List<Object[]> listDeactivateLicense = this.chartDAO.findGuideDeactivateLicense(obj);
		if(!listDeactivateLicense.isEmpty())
		{
			//for เพื่อนับจำนวน cancle ในแต่ละประเภทมัคคุเทศก์
			for(Object sel: listDeactivateLicense)
			{
				String licenseNo = sel.toString();
				System.out.println(">>>licenseNo = "+licenseNo);
				RegistrationDTO dto = new RegistrationDTO();
				dto.setLicenseNo(licenseNo);
				
				List<Object[]> listTrader = this.chartDAO.findGuideTraderCategory(dto);
				
				if(!listTrader.isEmpty())
				{
					//for เพื่อนับจำนวน cancle ในแต่ละประเภทมัคคุเทศก์
					for(Object sel2: listTrader)
					{
				
					if(sel2.toString().equals(TraderCategory.PLATINUM_GUIDE.getStatus()))
						PLATINUM_GUIDE++;
					
					if(sel2.toString().equals(TraderCategory.GOLD_GUIDE.getStatus()))
						GOLD_GUIDE++;
					
					if(sel2.toString().equals(TraderCategory.PINK_GUIDE.getStatus()))
						PINK_GUIDE++;
					
					if(sel2.toString().equals(TraderCategory.BLUE_GUIDE.getStatus()))
						BLUE_GUIDE++;
					
					if(sel2.toString().equals(TraderCategory.GREEN_GUIDE.getStatus()))
						GREEN_GUIDE++;
					
					if(sel2.toString().equals(TraderCategory.RED_GUIDE.getStatus()))
						RED_GUIDE++;
					
					if(sel2.toString().equals(TraderCategory.ORANGE_GUIDE.getStatus()))
						ORANGE_GUIDE++;
					
					if(sel2.toString().equals(TraderCategory.YELLOW_GUIDE.getStatus()))
						YELLOW_GUIDE++;
					
					if(sel2.toString().equals(TraderCategory.VIOLET_GUIDE.getStatus()))
						VIOLET_GUIDE++;
					
					if(sel2.toString().equals(TraderCategory.BROWN_GUIDE.getStatus()))
						BROWN_GUIDE++;
					}//for
				}
				
			}
		}
		
		Double oldCountAllAll = new Double(0);
		Double newCountAllAll = new Double(0);
		Double cancleCountAllAll = new Double(0);
		
		for(int i=0; i<2; i++)
		{	
			//มัคคุเทศก์ทั่วไป
			List<Object> listTraderCategory = new ArrayList<Object>();
			if(i==0)
			{
				
				listTraderCategory.add(TraderCategory.PLATINUM_GUIDE.getStatus());
				listTraderCategory.add(TraderCategory.GOLD_GUIDE.getStatus());
			}
			//มัคคุเทศก์เฉพาะพื้นที่
			if(i==1)
			{
				listTraderCategory.add(TraderCategory.PINK_GUIDE.getStatus());
				listTraderCategory.add(TraderCategory.BLUE_GUIDE.getStatus());
				listTraderCategory.add(TraderCategory.GREEN_GUIDE.getStatus());
				listTraderCategory.add(TraderCategory.RED_GUIDE.getStatus());
				listTraderCategory.add(TraderCategory.ORANGE_GUIDE.getStatus());
				listTraderCategory.add(TraderCategory.YELLOW_GUIDE.getStatus());
				listTraderCategory.add(TraderCategory.VIOLET_GUIDE.getStatus());
				listTraderCategory.add(TraderCategory.BROWN_GUIDE.getStatus());
			}
			
			Double oldCountAll = new Double(0);
			Double newCountAll = new Double(0);
			Double cancleCountAll = new Double(0);
			//for ทีละประเภทมัคคุเทศก์ตาม traderCategory, มัคคุเทศก์ทั่วไป
			for(Object traderCategory : listTraderCategory)
			{
				obj.setTraderCategory(traderCategory.toString());
				List<Object[]> listObj = this.chartDAO.findExcelGuideType(obj);
				if(!listObj.isEmpty())
				{
					for(Object[] sel: listObj)
					{
						
						RegistrationDTO dto = new RegistrationDTO();
						dto.setOldCount(Double.valueOf(sel[0].toString()));
						System.out.println("dto.getOldCount()::::="+dto.getOldCount());
						dto.setNewCount(Double.valueOf(sel[1].toString()));
						
						if((traderCategory.toString().equals(TraderCategory.PLATINUM_GUIDE.getStatus())))
						{
							dto.setCancleCount(PLATINUM_GUIDE);
							dto.setTraderCategoryName(TraderCategory.PLATINUM_GUIDE.getMeaning());
						}
						if((traderCategory.toString().equals(TraderCategory.GOLD_GUIDE.getStatus())))
						{
							dto.setCancleCount(GOLD_GUIDE);
							dto.setTraderCategoryName(TraderCategory.GOLD_GUIDE.getMeaning());
						}
						
						if((traderCategory.toString().equals(TraderCategory.PINK_GUIDE.getStatus())))
						{
							dto.setCancleCount(PINK_GUIDE);
							dto.setTraderCategoryName(TraderCategory.PINK_GUIDE.getMeaning());
						}
						
						if((traderCategory.toString().equals(TraderCategory.BLUE_GUIDE.getStatus())))
						{
							dto.setCancleCount(BLUE_GUIDE);
							dto.setTraderCategoryName(TraderCategory.BLUE_GUIDE.getMeaning());
						}
						
						if((traderCategory.toString().equals(TraderCategory.GREEN_GUIDE.getStatus())))
						{
							dto.setCancleCount(GREEN_GUIDE);
							dto.setTraderCategoryName(TraderCategory.GREEN_GUIDE.getMeaning());
						}
						
						if((traderCategory.toString().equals(TraderCategory.RED_GUIDE.getStatus())))
						{
							dto.setCancleCount(RED_GUIDE);
							dto.setTraderCategoryName(TraderCategory.RED_GUIDE.getMeaning());
						}
						
						if((traderCategory.toString().equals(TraderCategory.ORANGE_GUIDE.getStatus())))
						{
							dto.setCancleCount(ORANGE_GUIDE);
							dto.setTraderCategoryName(TraderCategory.ORANGE_GUIDE.getMeaning());
						}
						
						if((traderCategory.toString().equals(TraderCategory.YELLOW_GUIDE.getStatus())))
						{
							dto.setCancleCount(YELLOW_GUIDE);
							dto.setTraderCategoryName(TraderCategory.YELLOW_GUIDE.getMeaning());
						}
						
						if((traderCategory.toString().equals(TraderCategory.VIOLET_GUIDE.getStatus())))
						{
							dto.setCancleCount(VIOLET_GUIDE);
							dto.setTraderCategoryName(TraderCategory.VIOLET_GUIDE.getMeaning());
						}
						
						if((traderCategory.toString().equals(TraderCategory.BROWN_GUIDE.getStatus())))
						{
							dto.setCancleCount(BROWN_GUIDE);
							dto.setTraderCategoryName(TraderCategory.BROWN_GUIDE.getMeaning());
						}
						
						dto.setCount(Double.valueOf(sel[0].toString())+Double.valueOf(sel[1].toString())-dto.getCancleCount());
						list.add(dto);
						
						//Sum
						oldCountAll += Double.valueOf(sel[0].toString());
						newCountAll += Double.valueOf(sel[1].toString());
						cancleCountAll += dto.getCancleCount();
						
					}
				}
			}
			
			//Add Sum 
			RegistrationDTO dto = new RegistrationDTO();
			dto.setTraderCategoryName("รวม");
			dto.setOldCount(oldCountAll);
			dto.setNewCount(newCountAll);
			dto.setCancleCount(cancleCountAll);
			dto.setCount(oldCountAll+newCountAll-cancleCountAll);
			list.add(dto);
			
			//Sum All
			oldCountAllAll += oldCountAll;
			newCountAllAll += newCountAll;
			cancleCountAllAll += cancleCountAll;
		}
		
		//Add SumAll
		RegistrationDTO dto = new RegistrationDTO();
		dto.setTraderCategoryName("รวมทั้งสิ้น");
		dto.setOldCount(oldCountAllAll);
		dto.setNewCount(newCountAllAll);
		dto.setCancleCount(cancleCountAllAll);
		dto.setCount(oldCountAllAll+newCountAllAll-cancleCountAllAll);
		list.add(dto);
		
		return list;
	}

	//ค้นหาตารางแสดงจำนวนผู้ที่ได้รับใบอนุญาตเป็นมัคคุเทศก์ ในภาพรวม
	public List<RegistrationDTO> findExcelGuideAll(RegistrationDTO obj, Map model)throws Exception
	{
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();

		Double countAllAll1 = new Double(0);
		Double countAllAll2 = new Double(0);
		Double countAllAll3 = new Double(0);
		Double countAllAll4 = new Double(0);
		Double countAllAll5 = new Double(0);
		Double countAllAll6 = new Double(0);
		Double countAllAll7 = new Double(0);
		Double countAllAll8 = new Double(0);
		Double countAllAll9 = new Double(0);
		Double countAllAll10 = new Double(0);
		Double countAllAll11 = new Double(0);
		Double countAllAll12 = new Double(0);
		Double countAllAll13 = new Double(0);
		Double countAllAll14 = new Double(0);
		Double countAllAll15 = new Double(0);
		
		for(int i=0; i<2; i++)
		{	
			//มัคคุเทศก์ทั่วไป
			List<Object> listTraderCategory = new ArrayList<Object>();
			if(i==0)
			{
				
				listTraderCategory.add(TraderCategory.PLATINUM_GUIDE.getStatus());
				listTraderCategory.add(TraderCategory.GOLD_GUIDE.getStatus());
			}
			//มัคคุเทศก์เฉพาะพื้นที่
			if(i==1)
			{
				listTraderCategory.add(TraderCategory.PINK_GUIDE.getStatus());
				listTraderCategory.add(TraderCategory.BLUE_GUIDE.getStatus());
				listTraderCategory.add(TraderCategory.GREEN_GUIDE.getStatus());
				listTraderCategory.add(TraderCategory.RED_GUIDE.getStatus());
				listTraderCategory.add(TraderCategory.ORANGE_GUIDE.getStatus());
				listTraderCategory.add(TraderCategory.YELLOW_GUIDE.getStatus());
				listTraderCategory.add(TraderCategory.VIOLET_GUIDE.getStatus());
				listTraderCategory.add(TraderCategory.BROWN_GUIDE.getStatus());
			}
			
			Double countAll1 = new Double(0);
			Double countAll2 = new Double(0);
			Double countAll3 = new Double(0);
			Double countAll4 = new Double(0);
			Double countAll5 = new Double(0);
			Double countAll6 = new Double(0);
			Double countAll7 = new Double(0);
			Double countAll8 = new Double(0);
			Double countAll9 = new Double(0);
			Double countAll10 = new Double(0);
			Double countAll11 = new Double(0);
			Double countAll12 = new Double(0);
			Double countAll13 = new Double(0);
			Double countAll14 = new Double(0);
			Double countAll15 = new Double(0);
			
			//for ทีละประเภทมัคคุเทศก์ตาม traderCategory
			for(Object traderCategory : listTraderCategory)
			{
				obj.setTraderCategory(traderCategory.toString());

				List<Organization> organizationlist = (List<Organization>)this.organizationDAO.findAll(0);
				
				Double countAllRow = new Double(0);
				
				if(!organizationlist.isEmpty())
				{	
					int j = 1;
					RegistrationDTO dto = new RegistrationDTO();
					for(Organization persis: organizationlist)
					{
						//System.out.println("#####persis.getOrgId() = "+persis.getOrgId());
						obj.setOrgId(persis.getOrgId());
						
						List<Object[]> listObj = this.chartDAO.findExcelGuideAll(obj);
						
						if(!listObj.isEmpty())
						{
							for(Object[] sel: listObj)
							{
//								if(j==1)
//								{
//									dto.setCount1(Double.valueOf(sel.toString()));
//									countAll1 += Double.valueOf(sel.toString());
//								}
//								if(j==2)
//								{
//									dto.setCount2(Double.valueOf(sel.toString()));
//									countAll2 += Double.valueOf(sel.toString());
//								}
//								if(j==3)
//								{
//									dto.setCount3(Double.valueOf(sel.toString()));
//									countAll3 += Double.valueOf(sel.toString());
//								}
//								if(j==4)
//								{
//									dto.setCount4(Double.valueOf(sel.toString()));
//									countAll4 += Double.valueOf(sel.toString());
//								}
//								if(j==5)
//								{
//									dto.setCount5(Double.valueOf(sel.toString()));
//									countAll5 += Double.valueOf(sel.toString());
//								}
								
								System.out.println("Double.valueOf(sel.toString()):::::="+Double.valueOf((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
								if(persis.getOrgId()==7)
								{
									dto.setCount1(Double.valueOf((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
									countAll1 += Double.valueOf((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0)));
								}
								if(persis.getOrgId()==8)
								{
									dto.setCount2(Double.valueOf((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
									countAll2 += Double.valueOf((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0)));
								}
								if(persis.getOrgId()==9)
								{
									dto.setCount3(Double.valueOf((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
									countAll3 += Double.valueOf((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0)));
								}
								if(persis.getOrgId()==10)
								{
									dto.setCount4(Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0)))));
									countAll4 += Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
								}
								if(persis.getOrgId()==11)
								{
									dto.setCount5(Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0)))));
									countAll5 += Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
								}
								if(persis.getOrgId()==12)
								{
									dto.setCount6(Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0)))));
									countAll6 += Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
								}
								if(persis.getOrgId()==13)
								{
									dto.setCount7(Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0)))));
									countAll7 += Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
								}
								if(persis.getOrgId()==14)
								{
									dto.setCount8(Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0)))));
									countAll8 += Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
								}
								if(persis.getOrgId()==15)
								{
									dto.setCount9(Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0)))));
									countAll9 += Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
								}
								if(persis.getOrgId()==16)
								{
									dto.setCount10(Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0)))));
									countAll10 += Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
								}
								if(persis.getOrgId()==17)
								{
									dto.setCount11(Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0)))));
									countAll11 += Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
								}
								if(persis.getOrgId()==18)
								{
									dto.setCount12(Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0)))));
									countAll12 += Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
								}
								if(persis.getOrgId()==19)
								{
									dto.setCount13(Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0)))));
									countAll13 += Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
								}
								if(persis.getOrgId()==20)
								{
									dto.setCount14(Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0)))));
									countAll14 += Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
								}
								if(persis.getOrgId()==21)
								{
									dto.setCount15(Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0)))));
									countAll15 += Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
								}

//End Oat Edit 12/05/58
								countAllRow += Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
		
							}
						}else{
							if(persis.getOrgId()==7)
							{
								dto.setCount1(Double.valueOf(0));
								countAll1 += Double.valueOf(0);
							}
							if(persis.getOrgId()==8)
							{
								dto.setCount2(Double.valueOf(0));
								countAll2 += Double.valueOf(0);
							}
							if(persis.getOrgId()==9)
							{
								dto.setCount3(Double.valueOf(0));
								countAll3 += Double.valueOf(0);
							}
							if(persis.getOrgId()==10)
							{
								dto.setCount4(Double.valueOf(0));
								countAll4 += Double.valueOf(0);
							}
							if(persis.getOrgId()==11)
							{
								dto.setCount5(Double.valueOf(0));
								countAll5 += Double.valueOf(0);
							}
							if(persis.getOrgId()==12)
							{
								dto.setCount6(Double.valueOf(0));
								countAll6 += Double.valueOf(0);
							}
							if(persis.getOrgId()==13)
							{
								dto.setCount7(Double.valueOf(0));
								countAll7 += Double.valueOf(0);
							}
							if(persis.getOrgId()==14)
							{
								dto.setCount8(Double.valueOf(0));
								countAll8 += Double.valueOf(0);
							}
							if(persis.getOrgId()==15)
							{
								dto.setCount9(Double.valueOf(0));
								countAll9 += Double.valueOf(0);
							}
							if(persis.getOrgId()==16)
							{
								dto.setCount10(Double.valueOf(0));
								countAll10 += Double.valueOf(0);
							}
							if(persis.getOrgId()==17)
							{
								dto.setCount11(Double.valueOf(0));
								countAll11 += Double.valueOf(0);
							}
							if(persis.getOrgId()==18)
							{
								dto.setCount12(Double.valueOf(0));
								countAll12 += Double.valueOf(0);
							}
							if(persis.getOrgId()==19)
							{
								dto.setCount13(Double.valueOf(0));
								countAll13 += Double.valueOf(0);
							}
							if(persis.getOrgId()==20)
							{
								dto.setCount14(Double.valueOf(0));
								countAll14 += Double.valueOf(0);
							}
							if(persis.getOrgId()==21)
							{
								dto.setCount15(Double.valueOf(0));
								countAll15 += Double.valueOf(0);
							}

//End Oat Edit 12/05/58
							countAllRow += Double.valueOf(0);

						}
						j++;
					}//for org
					
					if((traderCategory.toString().equals(TraderCategory.PLATINUM_GUIDE.getStatus())))
					{
						dto.setTraderCategoryName(TraderCategory.PLATINUM_GUIDE.getMeaning());
					}
					if((traderCategory.toString().equals(TraderCategory.GOLD_GUIDE.getStatus())))
					{
						dto.setTraderCategoryName(TraderCategory.GOLD_GUIDE.getMeaning());
					}
					
					if((traderCategory.toString().equals(TraderCategory.PINK_GUIDE.getStatus())))
					{
						dto.setTraderCategoryName(TraderCategory.PINK_GUIDE.getMeaning());
					}
					
					if((traderCategory.toString().equals(TraderCategory.BLUE_GUIDE.getStatus())))
					{
						dto.setTraderCategoryName(TraderCategory.BLUE_GUIDE.getMeaning());
					}
					
					if((traderCategory.toString().equals(TraderCategory.GREEN_GUIDE.getStatus())))
					{
						dto.setTraderCategoryName(TraderCategory.GREEN_GUIDE.getMeaning());
					}
					
					if((traderCategory.toString().equals(TraderCategory.RED_GUIDE.getStatus())))
					{
						dto.setTraderCategoryName(TraderCategory.RED_GUIDE.getMeaning());
					}
					
					if((traderCategory.toString().equals(TraderCategory.ORANGE_GUIDE.getStatus())))
					{
						dto.setTraderCategoryName(TraderCategory.ORANGE_GUIDE.getMeaning());
					}
					
					if((traderCategory.toString().equals(TraderCategory.YELLOW_GUIDE.getStatus())))
					{
						dto.setTraderCategoryName(TraderCategory.YELLOW_GUIDE.getMeaning());
					}
					
					if((traderCategory.toString().equals(TraderCategory.VIOLET_GUIDE.getStatus())))
					{
						dto.setTraderCategoryName(TraderCategory.VIOLET_GUIDE.getMeaning());
					}
					
					if((traderCategory.toString().equals(TraderCategory.BROWN_GUIDE.getStatus())))
					{
						dto.setTraderCategoryName(TraderCategory.BROWN_GUIDE.getMeaning());
					}
					
					dto.setCount(countAllRow);
					list.add(dto);
					
				}
				
			}//for traderCategory
			
			//Add Sum
			RegistrationDTO dto = new RegistrationDTO();
			dto.setTraderCategoryName("รวม");
			dto.setCount1(countAll1);
			dto.setCount2(countAll2);
			dto.setCount3(countAll3);
			dto.setCount4(countAll4);
			dto.setCount5(countAll5);
			dto.setCount6(countAll6);
			dto.setCount7(countAll7);
			dto.setCount8(countAll8);
			dto.setCount9(countAll9);
			dto.setCount10(countAll10);
			dto.setCount11(countAll11);
			dto.setCount12(countAll12);
			dto.setCount13(countAll13);
			dto.setCount14(countAll14);
			dto.setCount15(countAll15);
			dto.setCount(countAll1+countAll2+countAll3+countAll4+countAll5+countAll6+countAll7+countAll8+countAll9
					+countAll10+countAll11+countAll12+countAll13+countAll14+countAll15);
			list.add(dto);
			
			countAllAll1 += countAll1;
			countAllAll2 += countAll2;
			countAllAll3 += countAll3;
			countAllAll4 += countAll4;
			countAllAll5 += countAll5;
			countAllAll6 += countAll6;
			countAllAll7 += countAll7;
			countAllAll8 += countAll8;
			countAllAll9 += countAll9;
			countAllAll10 += countAll10;
			countAllAll11 += countAll11;
			countAllAll12 += countAll12;
			countAllAll13 += countAll13;
			countAllAll14 += countAll14;
			countAllAll15 += countAll15;
			
			
		}
		
		//Add SumAll
		RegistrationDTO dto = new RegistrationDTO();
		dto.setTraderCategoryName("รวมทั้งสิ้น");
		dto.setCount1(countAllAll1);
		dto.setCount2(countAllAll2);
		dto.setCount3(countAllAll3);
		dto.setCount4(countAllAll4);
		dto.setCount5(countAllAll5);
		dto.setCount6(countAllAll6);
		dto.setCount7(countAllAll7);
		dto.setCount8(countAllAll8);
		dto.setCount9(countAllAll9);
		dto.setCount10(countAllAll10);
		dto.setCount11(countAllAll11);
		dto.setCount12(countAllAll12);
		dto.setCount13(countAllAll13);
		dto.setCount14(countAllAll14);
		dto.setCount15(countAllAll15);
		dto.setCount(countAllAll1+countAllAll2+countAllAll3+countAllAll4+countAllAll5+countAllAll6+countAllAll7+countAllAll8
				+countAllAll9+countAllAll10+countAllAll11+countAllAll12+countAllAll13+countAllAll14+countAllAll15);
		list.add(dto);
		
		return list;
	}
	
	//ค้นหาตารางสรุปจำนวนผู้ขอรับใบอนุญาตเป็นมัคคุเทศก์(ยื่นใหม่)
	public List<RegistrationDTO> findExcelGuideNew(RegistrationDTO obj, Map model)throws Exception
	{
		System.out.println("###findExcelGuideNew");
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();

		Double countAllAll1 = new Double(0);
		Double countAllAll2 = new Double(0);
		Double countAllAll3 = new Double(0);
		Double countAllAll4 = new Double(0);
		Double countAllAll5 = new Double(0);
		Double countAllAll6 = new Double(0);
		Double countAllAll7 = new Double(0);
		Double countAllAll8 = new Double(0);
		Double countAllAll9 = new Double(0);
		Double countAllAll10 = new Double(0);
		Double countAllAll11 = new Double(0);
		Double countAllAll12 = new Double(0);
		Double countAllAll13 = new Double(0);
		Double countAllAll14 = new Double(0);
		Double countAllAll15 = new Double(0);
		
		for(int i=0; i<2; i++)
		{	
			//มัคคุเทศก์ทั่วไป
			List<Object> listTraderCategory = new ArrayList<Object>();
			if(i==0)
			{
				
				listTraderCategory.add(TraderCategory.PLATINUM_GUIDE.getStatus());
				listTraderCategory.add(TraderCategory.GOLD_GUIDE.getStatus());
			}
			//มัคคุเทศก์เฉพาะพื้นที่
			if(i==1)
			{
				listTraderCategory.add(TraderCategory.PINK_GUIDE.getStatus());
				listTraderCategory.add(TraderCategory.BLUE_GUIDE.getStatus());
				listTraderCategory.add(TraderCategory.GREEN_GUIDE.getStatus());
				listTraderCategory.add(TraderCategory.RED_GUIDE.getStatus());
				listTraderCategory.add(TraderCategory.ORANGE_GUIDE.getStatus());
				listTraderCategory.add(TraderCategory.YELLOW_GUIDE.getStatus());
				listTraderCategory.add(TraderCategory.VIOLET_GUIDE.getStatus());
				listTraderCategory.add(TraderCategory.BROWN_GUIDE.getStatus());
			}
			
			Double countAll1 = new Double(0);
			Double countAll2 = new Double(0);
			Double countAll3 = new Double(0);
			Double countAll4 = new Double(0);
			Double countAll5 = new Double(0);
			
			Double countAll6 = new Double(0);
			Double countAll7 = new Double(0);
			Double countAll8 = new Double(0);
			Double countAll9 = new Double(0);
			Double countAll10 = new Double(0);
			
			Double countAll11 = new Double(0);
			Double countAll12 = new Double(0);
			Double countAll13 = new Double(0);
			Double countAll14 = new Double(0);
			Double countAll15 = new Double(0);
			
			//for ทีละประเภทมัคคุเทศก์ตาม traderCategory
			for(Object traderCategory : listTraderCategory)
			{
				obj.setTraderCategory(traderCategory.toString());

				List<Organization> organizationlist = (List<Organization>)this.organizationDAO.findAll(0);
				
				Double countAllRow = new Double(0);
				
				if(!organizationlist.isEmpty())
				{	
//					int j = 1;
					RegistrationDTO dto = new RegistrationDTO();
					for(Organization persis: organizationlist)
					{
						System.out.println("#####persis.getOrgId() = "+persis.getOrgId());
						System.out.println("#####obj.getTraderCategory() = "+obj.getTraderCategory());
						obj.setOrgId(persis.getOrgId());
						
						//Oat Edit 04/01/59
//						List<Object[]> listObj = this.chartDAO.findExcelGuideAll(obj);
						List<Object[]> listObj = this.chartDAO.findExcelGuideNew(obj);
						//End Oat Edit 04/01/59
						
						if(!listObj.isEmpty())
						{
							for(Object[] sel: listObj)
							{
//Oat Edit 12/05/58
//								if(persis.getOrgId()==1)
//								{
//									dto.setCount1(Double.valueOf(sel.toString()));
//									countAll1 += Double.valueOf(sel.toString());
//								}
//								if(persis.getOrgId()==2)
//								{
//									dto.setCount2(Double.valueOf(sel.toString()));
//									countAll2 += Double.valueOf(sel.toString());
//								}
//								if(persis.getOrgId()==3)
//								{
//									dto.setCount3(Double.valueOf(sel.toString()));
//									countAll3 += Double.valueOf(sel.toString());
//								}
//								if(persis.getOrgId()==4)
//								{
//									dto.setCount4(Double.valueOf(sel.toString()));
//									countAll4 += Double.valueOf(sel.toString());
//								}
//								if(persis.getOrgId()==5)
//								{
//									dto.setCount5(Double.valueOf(sel.toString()));
//									countAll5 += Double.valueOf(sel.toString());
//								}
								System.out.println("Double.valueOf(sel.toString()):::::="+Double.valueOf((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
								if(persis.getOrgId()==7)
								{
									dto.setCount1(Double.valueOf((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
									countAll1 += Double.valueOf((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0)));
								}
								if(persis.getOrgId()==8)
								{
									dto.setCount2(Double.valueOf((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
									countAll2 += Double.valueOf((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0)));
								}
								if(persis.getOrgId()==9)
								{
									dto.setCount3(Double.valueOf((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
									countAll3 += Double.valueOf((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0)));
								}
								if(persis.getOrgId()==10)
								{
									dto.setCount4(Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0)))));
									countAll4 += Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
								}
								if(persis.getOrgId()==11)
								{
									dto.setCount5(Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0)))));
									countAll5 += Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
								}
								if(persis.getOrgId()==12)
								{
									dto.setCount6(Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0)))));
									countAll6 += Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
								}
								if(persis.getOrgId()==13)
								{
									dto.setCount7(Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0)))));
									countAll7 += Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
								}
								if(persis.getOrgId()==14)
								{
									dto.setCount8(Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0)))));
									countAll8 += Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
								}
								if(persis.getOrgId()==15)
								{
									dto.setCount9(Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0)))));
									countAll9 += Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
								}
								if(persis.getOrgId()==16)
								{
									dto.setCount10(Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0)))));
									countAll10 += Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
								}
								if(persis.getOrgId()==17)
								{
									dto.setCount11(Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0)))));
									countAll11 += Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
								}
								if(persis.getOrgId()==18)
								{
									dto.setCount12(Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0)))));
									countAll12 += Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
								}
								if(persis.getOrgId()==19)
								{
									dto.setCount13(Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0)))));
									countAll13 += Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
								}
								if(persis.getOrgId()==20)
								{
									dto.setCount14(Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0)))));
									countAll14 += Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
								}
								if(persis.getOrgId()==21)
								{
									dto.setCount15(Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0)))));
									countAll15 += Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
								}

//End Oat Edit 12/05/58
								countAllRow += Double.valueOf(((String) (sel[0].toString()!=null?sel[0].toString():Double.valueOf(0))));
		
							}
						}else{
								if(persis.getOrgId()==7)
								{
									dto.setCount1(Double.valueOf(0));
									countAll1 += Double.valueOf(0);
								}
								if(persis.getOrgId()==8)
								{
									dto.setCount2(Double.valueOf(0));
									countAll2 += Double.valueOf(0);
								}
								if(persis.getOrgId()==9)
								{
									dto.setCount3(Double.valueOf(0));
									countAll3 += Double.valueOf(0);
								}
								if(persis.getOrgId()==10)
								{
									dto.setCount4(Double.valueOf(0));
									countAll4 += Double.valueOf(0);
								}
								if(persis.getOrgId()==11)
								{
									dto.setCount5(Double.valueOf(0));
									countAll5 += Double.valueOf(0);
								}
								if(persis.getOrgId()==12)
								{
									dto.setCount6(Double.valueOf(0));
									countAll6 += Double.valueOf(0);
								}
								if(persis.getOrgId()==13)
								{
									dto.setCount7(Double.valueOf(0));
									countAll7 += Double.valueOf(0);
								}
								if(persis.getOrgId()==14)
								{
									dto.setCount8(Double.valueOf(0));
									countAll8 += Double.valueOf(0);
								}
								if(persis.getOrgId()==15)
								{
									dto.setCount9(Double.valueOf(0));
									countAll9 += Double.valueOf(0);
								}
								if(persis.getOrgId()==16)
								{
									dto.setCount10(Double.valueOf(0));
									countAll10 += Double.valueOf(0);
								}
								if(persis.getOrgId()==17)
								{
									dto.setCount11(Double.valueOf(0));
									countAll11 += Double.valueOf(0);
								}
								if(persis.getOrgId()==18)
								{
									dto.setCount12(Double.valueOf(0));
									countAll12 += Double.valueOf(0);
								}
								if(persis.getOrgId()==19)
								{
									dto.setCount13(Double.valueOf(0));
									countAll13 += Double.valueOf(0);
								}
								if(persis.getOrgId()==20)
								{
									dto.setCount14(Double.valueOf(0));
									countAll14 += Double.valueOf(0);
								}
								if(persis.getOrgId()==21)
								{
									dto.setCount15(Double.valueOf(0));
									countAll15 += Double.valueOf(0);
								}

//End Oat Edit 12/05/58
								countAllRow += Double.valueOf(0);

						}
//						j++;
					}//for org
					
					if((traderCategory.toString().equals(TraderCategory.PLATINUM_GUIDE.getStatus())))
					{
						dto.setTraderCategoryName(TraderCategory.PLATINUM_GUIDE.getMeaning());
					}
					if((traderCategory.toString().equals(TraderCategory.GOLD_GUIDE.getStatus())))
					{
						dto.setTraderCategoryName(TraderCategory.GOLD_GUIDE.getMeaning());
					}
					
					if((traderCategory.toString().equals(TraderCategory.PINK_GUIDE.getStatus())))
					{
						dto.setTraderCategoryName(TraderCategory.PINK_GUIDE.getMeaning());
					}
					
					if((traderCategory.toString().equals(TraderCategory.BLUE_GUIDE.getStatus())))
					{
						dto.setTraderCategoryName(TraderCategory.BLUE_GUIDE.getMeaning());
					}
					
					if((traderCategory.toString().equals(TraderCategory.GREEN_GUIDE.getStatus())))
					{
						dto.setTraderCategoryName(TraderCategory.GREEN_GUIDE.getMeaning());
					}
					
					if((traderCategory.toString().equals(TraderCategory.RED_GUIDE.getStatus())))
					{
						dto.setTraderCategoryName(TraderCategory.RED_GUIDE.getMeaning());
					}
					
					if((traderCategory.toString().equals(TraderCategory.ORANGE_GUIDE.getStatus())))
					{
						dto.setTraderCategoryName(TraderCategory.ORANGE_GUIDE.getMeaning());
					}
					
					if((traderCategory.toString().equals(TraderCategory.YELLOW_GUIDE.getStatus())))
					{
						dto.setTraderCategoryName(TraderCategory.YELLOW_GUIDE.getMeaning());
					}
					
					if((traderCategory.toString().equals(TraderCategory.VIOLET_GUIDE.getStatus())))
					{
						dto.setTraderCategoryName(TraderCategory.VIOLET_GUIDE.getMeaning());
					}
					
					if((traderCategory.toString().equals(TraderCategory.BROWN_GUIDE.getStatus())))
					{
						dto.setTraderCategoryName(TraderCategory.BROWN_GUIDE.getMeaning());
					}
					
					dto.setCount(countAllRow);
					list.add(dto);
					
				}
				
			}//for traderCategory
			
			//Add Sum
			RegistrationDTO dto = new RegistrationDTO();
			dto.setTraderCategoryName("รวม");
			dto.setCount1(countAll1);
			dto.setCount2(countAll2);
			dto.setCount3(countAll3);
			dto.setCount4(countAll4);
			dto.setCount5(countAll5);
			dto.setCount6(countAll6);
			dto.setCount7(countAll7);
			dto.setCount8(countAll8);
			dto.setCount9(countAll9);
			dto.setCount10(countAll10);
			dto.setCount11(countAll11);
			dto.setCount12(countAll12);
			dto.setCount13(countAll13);
			dto.setCount14(countAll14);
			dto.setCount15(countAll15);
			dto.setCount(countAll1+countAll2+countAll3+countAll4+countAll5+countAll6+countAll7+countAll8
					+countAll9+countAll10+countAll11+countAll12+countAll13+countAll14+countAll15);
			list.add(dto);
			
			countAllAll1 += countAll1;
			countAllAll2 += countAll2;
			countAllAll3 += countAll3;
			countAllAll4 += countAll4;
			countAllAll5 += countAll5;
			countAllAll6 += countAll6;
			countAllAll7 += countAll7;
			countAllAll8 += countAll8;
			countAllAll9 += countAll9;
			countAllAll10 += countAll10;
			countAllAll11 += countAll11;
			countAllAll12 += countAll12;
			countAllAll13 += countAll13;
			countAllAll14 += countAll14;
			countAllAll15 += countAll15;
			
			
		}
			
		//Add SumAll
		RegistrationDTO dto = new RegistrationDTO();
		dto.setTraderCategoryName("รวมทั้งสิ้น");
		dto.setCount1(countAllAll1);
		dto.setCount2(countAllAll2);
		dto.setCount3(countAllAll3);
		dto.setCount4(countAllAll4);
		dto.setCount5(countAllAll5);
		dto.setCount6(countAllAll6);
		dto.setCount7(countAllAll7);
		dto.setCount8(countAllAll8);
		dto.setCount9(countAllAll9);
		dto.setCount10(countAllAll10);
		dto.setCount11(countAllAll11);
		dto.setCount12(countAllAll12);
		dto.setCount13(countAllAll13);
		dto.setCount14(countAllAll14);
		dto.setCount15(countAllAll15);
		
		dto.setCount(countAllAll1+countAllAll2+countAllAll3+countAllAll4+countAllAll5+countAllAll6+countAllAll7+countAllAll8
				+countAllAll9+countAllAll10+countAllAll11+countAllAll12+countAllAll13+countAllAll14+countAllAll15);
		list.add(dto);
		
		return list;
	}
		
	//ค้นหาตารางมัคคุเทศก์ ต่ออายุ
	public List<RegistrationDTO> findExcelGuideRenew(RegistrationDTO obj, Map model)throws Exception
	{
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		List<Organization> organizationlist = (List<Organization>)this.organizationDAO.findAll(0);
		
		if(!organizationlist.isEmpty())
		{
			Double countAllR1 = new Double(0);
			Double countAllR2 = new Double(0);
			Double countAllR3 = new Double(0);
			Double countAllR4 = new Double(0);
			Double countAllR5 = new Double(0);
			Double countAllR6 = new Double(0);
			Double countAllR7 = new Double(0);
			Double countAllR8 = new Double(0);
			Double countAllR9 = new Double(0);
			Double countAllR10 = new Double(0);
			
			Double countAllT1 = new Double(0);
			Double countAllT2 = new Double(0);
			Double countAllT3 = new Double(0);
			Double countAllT4 = new Double(0);
			Double countAllT5 = new Double(0);
			Double countAllT6 = new Double(0);
			Double countAllT7 = new Double(0);
			Double countAllT8 = new Double(0);
			Double countAllT9 = new Double(0);
			Double countAllT10 = new Double(0);
			
			Double countAllC1 = new Double(0);
			Double countAllC2 = new Double(0);
			Double countAllC3 = new Double(0);
			Double countAllC4 = new Double(0);
			Double countAllC5 = new Double(0);
			Double countAllC6 = new Double(0);
			Double countAllC7 = new Double(0);
			Double countAllC8 = new Double(0);
			Double countAllC9 = new Double(0);
			Double countAllC10 = new Double(0);
			
			Double countAllAllR1 = new Double(0);
			Double countAllAllT1 = new Double(0);
			Double countAllAllR2 = new Double(0);
			Double countAllAllT2 = new Double(0);
			
			//for ทีละ org
			for(Organization persis: organizationlist)
			{
				System.out.println("#####persis.getOrgId() = "+persis.getOrgId());
				obj.setOrgId(persis.getOrgId());
				
				for(int i=0; i<3; i++)//for ต่ออายุ ใบแทน ยกเลิก
				{
					RegistrationDTO dto = new RegistrationDTO();
					
					int k = 0;
					
					Double countAll1 = new Double(0);
					Double countAll2 = new Double(0);
					Double count = new Double(0);
					
					Double countCAll1 = new Double(0);
					Double countCAll2 = new Double(0);
					
					if(i==0)
						obj.setRegistrationType("R");
					if(i==1)
						obj.setRegistrationType("T");
					
//					for(int j=0; j<2; j++)//for ทั่วไป เฉพาะ
//					{	
						//มัคคุเทศก์ทั่วไป
						List<Object> listTraderCategory = new ArrayList<Object>();
//						if(j==0)
//						{
							
						listTraderCategory.add(TraderCategory.PLATINUM_GUIDE.getStatus());
						listTraderCategory.add(TraderCategory.GOLD_GUIDE.getStatus());
//						}
						//มัคคุเทศก์เฉพาะพื้นที่
//						if(j==1)
//						{
						listTraderCategory.add(TraderCategory.PINK_GUIDE.getStatus());
						listTraderCategory.add(TraderCategory.BLUE_GUIDE.getStatus());
						listTraderCategory.add(TraderCategory.GREEN_GUIDE.getStatus());
						listTraderCategory.add(TraderCategory.RED_GUIDE.getStatus());
						listTraderCategory.add(TraderCategory.ORANGE_GUIDE.getStatus());
						listTraderCategory.add(TraderCategory.YELLOW_GUIDE.getStatus());
						listTraderCategory.add(TraderCategory.VIOLET_GUIDE.getStatus());
						listTraderCategory.add(TraderCategory.BROWN_GUIDE.getStatus());
//						}
						
						//for ทีละประเภทมัคคุเทศก์ตาม traderCategory
						for(Object traderCategory : listTraderCategory)
						{
							Double countC = new Double(0);
							
							obj.setTraderCategory(traderCategory.toString());
							
							List<Object[]> listObj = new ArrayList<Object[]>();
							
							if(i==0)
							{
								listObj = this.chartDAO.findExcelGuideRenew(obj);
							}
							if(i==1)
							{
								listObj = this.chartDAO.findExcelGuideRenew(obj);
							}
							if(i==2)
							{
//								this.chartDAO.findGuideDeactivateLicense(obj);
								//หา cancle ทั้งหมดในช่วงวันที่เลือก
								List<Object[]> listDeactivateLicense = this.chartDAO.findGuideDeactivateLicense(obj);
								if(!listDeactivateLicense.isEmpty())
								{
									//for เพื่อนับจำนวน cancle ในแต่ละประเภทมัคคุเทศก์
									for(Object sel: listDeactivateLicense)
									{
										String licenseNo = sel.toString();
										System.out.println(">>>licenseNo = "+licenseNo);
										obj.setLicenseNo(licenseNo);
										
										List<Object[]> listT = this.chartDAO.findGuideByTraderCategoryAndOrgId(obj);
										
										if(!listT.isEmpty())
										{
											//for เพื่อนับจำนวน cancle ในแต่ละประเภทมัคคุเทศก์
											for(Object sel2: listT)
											{
												countC += Double.valueOf(sel2.toString());
											}
										}
									}
								}
								
							}
							
							if(i==2)
							{
								if(k==0){
									dto.setCount1(countC);
									countCAll1 += countC;
									countAllC1 += countC;
								}if(k==1){
									dto.setCount2(countC);
									countCAll1 += countC;
									dto.setCountAll1(countCAll1);
									
									countAllC2 += countC;
								}if(k==2){
									dto.setCount3(countC);
									countCAll2 += countC;
									
									countAllC3 += countC;
								}if(k==3){
									dto.setCount4(countC);
									countCAll2 += countC;
									
									countAllC4 += countC;
								}if(k==4){
									dto.setCount5(countC);
									countCAll2 += countC;
									
									countAllC5 += countC;
								}if(k==5){
									dto.setCount6(countC);
									countCAll2 += countC;
									
									countAllC6 += countC;
								}if(k==6){
									dto.setCount7(countC);
									countCAll2 += countC;
									
									countAllC7 += countC;
								}if(k==7){
									dto.setCount8(countC);
									countCAll2 += countC;
									
									countAllC8 += countC;
								}if(k==8){
									dto.setCount9(countC);
									countCAll2 += countC;
									
									countAllC9 += countC;
								}if(k==9){
									dto.setCount10(countC);
									countCAll2 += countC;
									dto.setCountAll2(countCAll2);
									dto.setCount(countCAll1+countCAll2);
									
									countAllC10 += countC;
								}
								
							}
							
							if(!listObj.isEmpty())
							{
								for(Object sel: listObj)
								{
									if(k==0){
										count += Double.valueOf(sel.toString());
										dto.setCount1(Double.valueOf(sel.toString()));
										countAll1 += Double.valueOf(sel.toString());
										
										if(i==0){
											countAllR1 += Double.valueOf(sel.toString());
										}
										if(i==1){
											countAllT1 += Double.valueOf(sel.toString());
										}
										if(i==2){
											
										}
									}
									if(k==1){
										count += Double.valueOf(sel.toString());
										dto.setCount2(Double.valueOf(sel.toString()));
										countAll1 += Double.valueOf(sel.toString());
										dto.setCountAll1(countAll1);
										
										if(i==0){
											countAllR2 += Double.valueOf(sel.toString());
											countAllAllR1 += countAll1;
										}
										if(i==1){
											countAllT2 += Double.valueOf(sel.toString());
											countAllAllT1 += countAll1;
										}
										if(i==2){
											
										}
									}
									if(k==2){
										count += Double.valueOf(sel.toString());
										dto.setCount3(Double.valueOf(sel.toString()));
										countAll2 += Double.valueOf(sel.toString());
										
										if(i==0){
											countAllR3 += Double.valueOf(sel.toString());
										}
										if(i==1){
											countAllT3 += Double.valueOf(sel.toString());
										}
										if(i==2){
											
										}
									}
									if(k==3){
										count += Double.valueOf(sel.toString());
										dto.setCount4(Double.valueOf(sel.toString()));
										countAll2 += Double.valueOf(sel.toString());
										
										if(i==0){
											countAllR4 += Double.valueOf(sel.toString());
										}
										if(i==1){
											countAllT4 += Double.valueOf(sel.toString());
										}
										if(i==2){
											
										}
									}
									if(k==4){
										count += Double.valueOf(sel.toString());
										dto.setCount5(Double.valueOf(sel.toString()));
										countAll2 += Double.valueOf(sel.toString());
										
										if(i==0){
											countAllR5 += Double.valueOf(sel.toString());
										}
										if(i==1){
											countAllT5 += Double.valueOf(sel.toString());
										}
										if(i==2){
											
										}
									}
									if(k==5){
										count += Double.valueOf(sel.toString());
										dto.setCount6(Double.valueOf(sel.toString()));
										countAll2 += Double.valueOf(sel.toString());
										
										if(i==0){
											countAllR6 += Double.valueOf(sel.toString());
										}
										if(i==1){
											countAllT6 += Double.valueOf(sel.toString());
										}
										if(i==2){
											
										}
									}
									if(k==6){
										count += Double.valueOf(sel.toString());
										dto.setCount7(Double.valueOf(sel.toString()));
										countAll2 += Double.valueOf(sel.toString());
										
										if(i==0){
											countAllR7 += Double.valueOf(sel.toString());
										}
										if(i==1){
											countAllT7 += Double.valueOf(sel.toString());
										}
										if(i==2){
											
										}
									}
									if(k==7){
										count += Double.valueOf(sel.toString());
										dto.setCount8(Double.valueOf(sel.toString()));
										countAll2 += Double.valueOf(sel.toString());
										
										if(i==0){
											countAllR8 += Double.valueOf(sel.toString());
										}
										if(i==1){
											countAllT8 += Double.valueOf(sel.toString());
										}
										if(i==2){
											
										}
									}
									if(k==8){
										count += Double.valueOf(sel.toString());
										dto.setCount9(Double.valueOf(sel.toString()));
										countAll2 += Double.valueOf(sel.toString());
										
										if(i==0){
											countAllR9 += Double.valueOf(sel.toString());
										}
										if(i==1){
											countAllT9 += Double.valueOf(sel.toString());
										}
										if(i==2){
											
										}
									}
									if(k==9){
										count += Double.valueOf(sel.toString());
										dto.setCount10(Double.valueOf(sel.toString()));
										countAll2 += Double.valueOf(sel.toString());
										dto.setCountAll2(countAll2);
										
										dto.setCount(count);
										
										if(i==0){
											countAllR10 += Double.valueOf(sel.toString());
											countAllAllR2 += countAll2;
										}
										if(i==1){
											countAllT10 += Double.valueOf(sel.toString());
											countAllAllT2 += countAll2;
										}
										if(i==2){
											
										}
										
										
									}
									
								}
							}
							k++;
						}//for ทีละประเภทมัคคุเทศก์ตาม traderCategory
					
//					}//for ทั่วไป เฉพาะ
					if(i==0){
						dto.setRegistrationTypeName("ต่ออายุ");
						
						//Oat Edit 24/04/58
						dto.setOrgName(persis.getOrgName());
					
//						if(persis.getOrgId() == 1)
//							dto.setOrgName("กลาง");
//						if(persis.getOrgId() == 2)
//							dto.setOrgName("สาขา");
//						if(persis.getOrgId() == 3)
//							dto.setOrgName("สาขาภาค");
//						if(persis.getOrgId() == 4)
//							dto.setOrgName("สาขาภาคใต้");
//						if(persis.getOrgId() == 5)
//							dto.setOrgName("สาขาภาคใต้");
						
						//End Oat Edit 24/04/58
					}
					if(i==1){
						dto.setRegistrationTypeName("ใบแทน");
						
						//Oat Edit 24/04/58
//						if(persis.getOrgId() == 1)
//							dto.setOrgName("");
//						if(persis.getOrgId() == 2)
//							dto.setOrgName("ภาคเหนือ");
//						if(persis.getOrgId() == 3)
//							dto.setOrgName("ตะวันออก");
//						if(persis.getOrgId() == 4)
//							dto.setOrgName("เขต๑");
//						if(persis.getOrgId() == 5)
//							dto.setOrgName("เขต๒");
						//End Oat Edit 24/04/58
					}
					if(i==2){
						dto.setRegistrationTypeName("ยกเลิก");
						
						//Oat Edit 24/04/58
//						if(persis.getOrgId() == 3)
//							dto.setOrgName("เฉียงเหนือ");
						//End Oat Edit 24/04/58
					}
					list.add(dto);
				}//for ต่ออายุ ใบแทน ยกเลิก
				
			}//for Organization
			
			//SumAll ต่ออายุ
			RegistrationDTO dto = new RegistrationDTO();
			dto.setRegistrationTypeName("รวม ต่ออายุ");
			dto.setCount1(countAllR1);
			dto.setCount2(countAllR2);
			dto.setCountAll1(countAllAllR1);
			dto.setCount3(countAllR3);
			dto.setCount4(countAllR4);
			dto.setCount5(countAllR5);
			dto.setCount6(countAllR6);
			dto.setCount7(countAllR7);
			dto.setCount8(countAllR8);
			dto.setCount9(countAllR9);
			dto.setCount10(countAllR10);
			dto.setCountAll2(countAllAllR2);
			dto.setCount(countAllAllR1+countAllAllR2);
			list.add(dto);
			
			//SumAll ใบแทน
			dto = new RegistrationDTO();
			dto.setRegistrationTypeName("รวม ใบแทน");
			dto.setCount1(countAllT1);
			dto.setCount2(countAllT2);
			dto.setCountAll1(countAllAllT1);
			dto.setCount3(countAllT3);
			dto.setCount4(countAllT4);
			dto.setCount5(countAllT5);
			dto.setCount6(countAllT6);
			dto.setCount7(countAllT7);
			dto.setCount8(countAllT8);
			dto.setCount9(countAllT9);
			dto.setCount10(countAllT10);
			dto.setCountAll2(countAllAllT2);
			dto.setCount(countAllAllT1+countAllAllT2);
			list.add(dto);
			
			//SumAll ยกเลิก
			dto = new RegistrationDTO();
			dto.setRegistrationTypeName("รวม ยกเลิก");
			dto.setCount1(countAllC1);
			dto.setCount2(countAllC2);
			dto.setCountAll1(countAllC1+countAllC2);
			dto.setCount3(countAllC3);
			dto.setCount4(countAllC4);
			dto.setCount5(countAllC5);
			dto.setCount6(countAllC6);
			dto.setCount7(countAllC7);
			dto.setCount8(countAllC8);
			dto.setCount9(countAllC9);
			dto.setCount10(countAllC10);
			dto.setCountAll2(countAllC3+countAllC4+countAllC5+countAllC6+countAllC7+countAllC8+countAllC9+countAllC10);
			dto.setCount(countAllC1+countAllC2+countAllC3+countAllC4+countAllC5+countAllC6+countAllC7+countAllC8+countAllC9+countAllC10);
			list.add(dto);
			
			//SumAll รวมทั้งหมด
			dto = new RegistrationDTO();
			dto.setRegistrationTypeName("รวม ทั้งหมด");
			dto.setCount1(countAllR1+countAllT1+countAllC1);
			dto.setCount2(countAllR2+countAllT2+countAllC2);
			dto.setCountAll1(dto.getCount1()+dto.getCount2());
			dto.setCount3(countAllR3+countAllT3+countAllC3);
			dto.setCount4(countAllR4+countAllT4+countAllC4);
			dto.setCount5(countAllR5+countAllT5+countAllC5);
			dto.setCount6(countAllR6+countAllT6+countAllC6);
			dto.setCount7(countAllR7+countAllT7+countAllC7);
			dto.setCount8(countAllR8+countAllT8+countAllC8);
			dto.setCount9(countAllR9+countAllT9+countAllC9);
			dto.setCount10(countAllR10+countAllT10+countAllC10);
			dto.setCountAll2(dto.getCount3()+dto.getCount4()+dto.getCount5()+dto.getCount6()+dto.getCount7()+dto.getCount8()+dto.getCount9()+dto.getCount10());              
			dto.setCount(dto.getCount1()+dto.getCount2()+dto.getCount3()+dto.getCount4()+dto.getCount5()+dto.getCount6()+dto.getCount7()+dto.getCount8()+dto.getCount9()+dto.getCount10());
			list.add(dto);
			
		}
		return list;
	}


	public Map getLicenseDetail(RegistrationDTO param)
			throws Exception {
		
		Map detail = new HashMap();
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		detail.put("licenseType", TraderType.getMeaning(param.getTraderType()));
		
		List<Object[]> listLicense = (List<Object[]>)this.chartDAO.findLicenseDetail(param);
		
		DateFormat df = DateUtils.getProcessDateFormatThai();
		
		if(!listLicense.isEmpty())
		{
			for(Object[] license: listLicense)
			{
				RegistrationDTO dto = new RegistrationDTO();
				
				dto.setLicenseNo(license[7]==null?"":license[7].toString());
				dto.setTraderName(license[0]==null?"":license[0].toString());
				dto.setTraderNameEn(license[8]==null?"":license[8].toString());
				dto.setTraderCategory(license[1]==null?"":license[1].toString());
				
				
				if(license[2] != null)
				{
					dto.setEffectiveDate(df.format(license[2]));
				}
				if(license[3] != null)
				{
					dto.setExpireDate(df.format(license[3]));
				}
				
				dto.setTraderAddress(license[4]==null?"":license[4].toString());
				dto.setMobileNo(license[5]==null?"":license[5].toString());
				dto.setTelephone(license[6]==null?"":license[6].toString());
				
				dto.setEmail(license[9]==null?"":license[9].toString());
				
				dto.setIdentityNo(null==license[10]?"":license[10].toString());
				
				list.add(dto);
				
				
			}
			
			detail.put("chartdetail", list);
		}
		
		
		
		return detail;
	}

	
	
}
	













