package sss.dot.tourism.service.chart;

import java.util.List;
import java.util.Map;

import sss.dot.tourism.dto.registration.RegistrationDTO;

import com.sss.aut.service.User;

public interface IChartService {
	public List getChartBusiness(Object object, User user)throws Exception;
	
	public List getChartPieBusinessByOrgId(Object object, User user)throws Exception;
	public List getChartPieBusinessByTraderCategory(Object object, User user) throws Exception;
	
	public List getChartColumnBusinessAll(Object object, User user)throws Exception;

	public Map<String, ?> getDetail(RegistrationDTO obj, int firstrow,int pagesize) throws Exception;
	
	public Map getLicenseDetail(RegistrationDTO obj) throws Exception;
}

