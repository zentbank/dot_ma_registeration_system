package sss.dot.tourism.service.complaint;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import sss.dot.tourism.dao.AdmUserDAO;
import sss.dot.tourism.dao.complaint.ComplaintDocDAO;
import sss.dot.tourism.dao.complaint.ComplaintLicenseDAO;
import sss.dot.tourism.dao.complaint.ComplaintMapTraderDAO;
import sss.dot.tourism.dao.complaint.ComplaintProgressDAO;
import sss.dot.tourism.dao.mas.MasComplaintTypeDAO;
import sss.dot.tourism.dao.mas.MasPrefixDAO;
import sss.dot.tourism.dao.mas.MasRunningNoDAO;
import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.domain.AdmUser;
import sss.dot.tourism.domain.ComplaintDoc;
import sss.dot.tourism.domain.ComplaintLicense;
import sss.dot.tourism.domain.ComplaintMapTrader;
import sss.dot.tourism.domain.ComplaintProgress;
import sss.dot.tourism.domain.MasComplaintType;
import sss.dot.tourism.domain.MasPrefix;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.dto.complaint.ComplaintDocDTO;
import sss.dot.tourism.dto.complaint.ComplaintLicenseDTO;
import sss.dot.tourism.dto.complaint.ComplaintProgressDTO;
import sss.dot.tourism.service.file.FileManage;
import sss.dot.tourism.util.ComplaintStatus;
import sss.dot.tourism.util.ConstantUtil;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.PersonType;
import sss.dot.tourism.util.ProgressRole;
import sss.dot.tourism.util.QuestionType;
import sss.dot.tourism.util.RecordStatus;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.User;

@Repository("complaintService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ComplaintService implements IComplaintService{

	@Autowired
	ComplaintLicenseDAO complaintLicenseDAO;
	@Autowired
	ComplaintDocDAO complaintDocDAO;
	@Autowired
	MasRunningNoDAO masRunningNoDAO;
	@Autowired
	ComplaintMapTraderDAO complaintMapTraderDAO;
	@Autowired
	TraderDAO traderDAO;
	@Autowired
	ComplaintProgressDAO complaintProgressDAO;
	@Autowired
	MasComplaintTypeDAO complaintTypeDAO;
	@Autowired
	AdmUserDAO admUserDAO;
	@Autowired
	private MasPrefixDAO masPrefixDAO;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void createComplaint(Object object, User user) throws Exception {
		// TODO Auto-generated method stub
		if(!(object instanceof ComplaintLicenseDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถ เพิ่มการทำรายการรับเรื่องร้องเรียนใหม่ได้");
		}
				
		ComplaintLicenseDTO params = (ComplaintLicenseDTO)object;
		
		ComplaintLicense complaintLicense = new ComplaintLicense();
		
		//ComplaintNo
		String complaintNo = this.masRunningNoDAO.getComplaintNo(user.getUserData().getOrgId(), params.getTraderType());
		complaintLicense.setComplaintNo(complaintNo);
		
		complaintLicense.setCreateUser(user.getUserName());
		complaintLicense.setCreateDtm(new Date());
		complaintLicense.setRecordStatus(RecordStatus.NORMAL.getStatus());
		
		Long complaintLicenseId =  (Long)complaintLicenseDAO.insert(complaintLicense);
		
		params.setComplaintLicenseId(complaintLicenseId);
		
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void upload(Object object, MultipartFile file, User user)throws Exception {
		// TODO Auto-generated method stub
		
		System.out.println("###Service ComplaintService method upload");
		if(!(object instanceof ComplaintDocDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลการร้องเรียนได้");
		}

		ComplaintDocDTO params = (ComplaintDocDTO)object;
		
		String traderTypeName = "NONE";
		String licenseNoStr = "NONE";
		String questionType = "NONE";
		
		ComplaintLicense complaintLicense = (ComplaintLicense)this.complaintLicenseDAO.findByPrimaryKey(params.getComplaintLicenseId());
		List<ComplaintMapTrader> list = (List<ComplaintMapTrader>)this.complaintMapTraderDAO.findComplaintMapTraderByComplaintLicense(complaintLicense);
		if(!list.isEmpty() && list.size() > 0)
		{
			ComplaintMapTrader complaintMapTrader = list.get(0);
			
//			//find licenseNo
//			List<Trader> listTrader = (List<Trader>)this.traderDAO.findTraderByLicenseNo(complaintMapTrader.getLicenseNo(), complaintMapTrader.getTraderType(), RecordStatus.NORMAL.getStatus(), 0);
			
			//TraderType
			if(complaintMapTrader.getTraderType().equals(TraderType.TOUR_COMPANIES.getStatus()))
			{
				traderTypeName = "Business";
			}
			if(complaintMapTrader.getTraderType().equals(TraderType.GUIDE.getStatus()))
			{
				traderTypeName = "Guide";
			}
			if(complaintMapTrader.getTraderType().equals(TraderType.LEADER.getStatus()))
			{
				traderTypeName = "Tourleader";
			}
			
			//LicenseNo
			licenseNoStr = complaintMapTrader.getLicenseNo();
			licenseNoStr = licenseNoStr.replaceAll("[.]", "");
			licenseNoStr = licenseNoStr.replaceAll("[/]", "");
			licenseNoStr = licenseNoStr.replaceAll("[-]", "");
			
			//QuestionType
			questionType = complaintLicense.getQuestionType();
		}
		else
		{
			//TraderType
			if(params.getTraderType().equals(TraderType.TOUR_COMPANIES.getStatus()))
			{
				traderTypeName = "Business";
			}
			if(params.getTraderType().equals(TraderType.GUIDE.getStatus()))
			{
				traderTypeName = "Guide";
			}
			if(params.getTraderType().equals(TraderType.LEADER.getStatus()))
			{
				traderTypeName = "Tourleader";
			}
			
			//LicenseNo
			licenseNoStr = params.getLicenseNo();
			licenseNoStr = licenseNoStr.replaceAll("[.]", "");
			licenseNoStr = licenseNoStr.replaceAll("[/]", "");
			licenseNoStr = licenseNoStr.replaceAll("[-]", "");
			
			
			//QuestionType
			questionType = params.getQuestionType();
		}
		
		//RealFileName
		String realFileName = Calendar.getInstance().getTime().getTime()+"";
		
		//postfix FileName
		String extension = ".txt";
		String originalFilename = file.getOriginalFilename();
		int dot = originalFilename.lastIndexOf('.');
		if(dot != -1)
		{
			extension = originalFilename.substring(dot);
		}

		
		String root = ConstantUtil.ROOT_PATH; //"/Users/Apple/Desktop/fileUpload/"; 
    	String path = root+"Complaint"+java.io.File.separator+traderTypeName+java.io.File.separator+licenseNoStr+java.io.File.separator+questionType;
    	String fileName = java.io.File.separator+realFileName+extension;
    	
    	String fullpath = path+fileName;
    	System.out.println("#####full path = "+fullpath);
    	
//    	MasDocument document = (MasDocument)this.masDocumentDAO.findByPrimaryKey(params.getMasDocId());

    	if(file != null)
	    {
    		//Update
    		if(params.getComplaintDocId() >0)
    		{
    			System.out.println("###UPDATE");
    			
    			//Delete Old File
    			ComplaintDoc complaintDoc = (ComplaintDoc)this.complaintDocDAO.findByPrimaryKey(params.getComplaintDocId());
    			
    			String oldDocPath = complaintDoc.getComplaintDocPath();
    			
    			System.out.println("oldDocPath = "+oldDocPath);
    			
    			FileManage fm = new FileManage();
    			fm.deleteFile(oldDocPath);
    			
    			//Upload new File
		    	boolean b = fm.uploadFile(file.getInputStream(), path, fileName);
		    	
		    	//Update ComplaintDoc
		    	if(b)
		    	{
		    		complaintDoc.setComplaintDocName(params.getComplaintDocName());
		    		complaintDoc.setComplaintDocPath(fullpath);
		    		complaintDoc.setLastUpdDtm(new Date());
		    		complaintDoc.setLastUpdUser(user.getUserName());
		    	}
    			
    		}
    		//Create
    		else
    		{
    			System.out.println("###CREATE");
    			//Upload File
		    	FileManage fm = new FileManage();
		    	boolean b = fm.uploadFile(file.getInputStream(), path, fileName);
		    	
		    	//Save ComplaintDoc
		    	if(b)
		    	{	
		    		ComplaintDoc persis = new ComplaintDoc();
		    		persis.setComplaintDocType(params.getComplaintDocType());
		    		persis.setComplaintDocName(params.getComplaintDocName());
		    		persis.setComplaintDocPath(fullpath);
		    		persis.setComplaintLicense((ComplaintLicense)this.complaintLicenseDAO.findByPrimaryKey(params.getComplaintLicenseId()));
		    		
		    		persis.setRecordStatus(RecordStatus.NORMAL.getStatus());
		    		persis.setCreateUser(user.getUserName());
		    		persis.setCreateDtm(new Date());
		    		
		    		this.complaintDocDAO.insert(persis);
		    		
		    	}
    		}
	    	
	    }
    	
	}
	
	
	public List getDocument(Object object, User user) throws Exception
	{
		// TODO Auto-generated method stub
		System.out.println("###Service ComplaintService method getDocument");
		if(!(object instanceof ComplaintDocDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลการร้องเรียนได้");
		}
		
		List<ComplaintDocDTO> list = new ArrayList<ComplaintDocDTO>();
		
		ComplaintDocDTO params = (ComplaintDocDTO)object;
		
		List<ComplaintDoc> listComplaintDocs = this.complaintDocDAO.findComplaintDoc(params, user);
		
		if(!listComplaintDocs.isEmpty())
		{
			for(ComplaintDoc document: listComplaintDocs)
			{
				ComplaintDocDTO dto = new ComplaintDocDTO();
				
				ObjectUtil.copy(document, dto);
				
				dto.setHavefile("true");
				
				dto.setComplaintLicenseId(params.getComplaintLicenseId());
				
				list.add(dto);
			}
		}
		
		return list;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveComplaintReceive(Object object, User user) throws Exception {
		// TODO Auto-generated method stub
		if(!(object instanceof ComplaintLicenseDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลการร้องเรียนได้");
		}
		
		try
		{
			ComplaintLicenseDTO dto = (ComplaintLicenseDTO) object;
			
			ComplaintLicense persis = (ComplaintLicense)this.complaintLicenseDAO.findByPrimaryKey(dto.getComplaintLicenseId());
			
			ObjectUtil.copy(dto, persis);
			
			//ComplaintNo
//			String complaintNo = this.masRunningNoDAO.getComplaintNo(user.getUserData().getOrgId(), dto.getTraderType());
//			persis.setComplaintNo(complaintNo);
			
			//CHECK CASE FROM ComplaintStatus ,ComplaintProgress
			//Save ComplaintProgress
			if(dto.getComplaintStatus() == null || dto.getComplaintStatus().equals(""))
			{
				persis.setComplaintStatus(ComplaintStatus.ADD.getStatus());
				dto.setComplaintStatus(ComplaintStatus.ADD.getStatus());
			}
			
						
			//RL A นิติกรเพิ่มเรื่อง
			if(dto.getComplaintProgress().equals("RL") && dto.getComplaintStatus().equals(ComplaintStatus.ADD.getStatus()))
			{
				List<ComplaintProgress> complaintProgresslist = (List<ComplaintProgress>)this.complaintProgressDAO.findComplaintProgressByComplaintLicenseAndProgressStatus(dto, user);

				if(complaintProgresslist.isEmpty() || complaintProgresslist.size() == 0)
				{
					//INSERT ComplaintProgress RL A
					ComplaintProgress complaintProgressPersis = new ComplaintProgress();
					complaintProgressPersis.setProgressRole(persis.getComplaintProgress());
					complaintProgressPersis.setProgressStatus(persis.getComplaintStatus());
					
					complaintProgressPersis.setProgressDate(new Date());
					complaintProgressPersis.setAuthority(user.getUserData().getUserFullName());
					complaintProgressPersis.setRecordStatus(RecordStatus.NORMAL.getStatus());
					complaintProgressPersis.setCreateDtm(new Date());
					complaintProgressPersis.setCreateUser(user.getUserName());
					complaintProgressPersis.setComplaintLicense(persis);
					complaintProgressPersis.setProgressDesc(ComplaintStatus.ADD.getMeaning());
					this.complaintProgressDAO.insert(complaintProgressPersis);
					
					//INSERT ComplaintProgress RL W
					ComplaintProgress complaintProgressPersis2 = new ComplaintProgress();
					complaintProgressPersis2.setProgressRole(persis.getComplaintProgress());
					complaintProgressPersis2.setProgressStatus(ComplaintStatus.WAIT.getStatus());
					
					complaintProgressPersis2.setProgressDate(new Date());
					complaintProgressPersis2.setAuthority(user.getUserData().getUserFullName());
					complaintProgressPersis2.setRecordStatus(RecordStatus.NORMAL.getStatus());
					complaintProgressPersis2.setCreateDtm(new Date());
					complaintProgressPersis2.setCreateUser(user.getUserName());
					complaintProgressPersis2.setComplaintLicense(persis);
					complaintProgressPersis2.setProgressDesc(ComplaintStatus.WAIT.getMeaning());
					this.complaintProgressDAO.insert(complaintProgressPersis2);
					
				}
			}
			//RL G นิติกรรับเรื่อง
			if(dto.getComplaintProgress().equals("RL") && dto.getComplaintStatus().equals(ComplaintStatus.GETIT.getStatus()))
			{
				persis.setComplaintProgress("ML");
				
				ComplaintLicenseDTO complaintLicenseDTO = new ComplaintLicenseDTO();
				complaintLicenseDTO.setComplaintLicenseId(persis.getComplaintId());
				complaintLicenseDTO.setComplaintStatus(ComplaintStatus.WAIT.getStatus());
				complaintLicenseDTO.setComplaintProgress("RL");
				
				List<ComplaintProgress> complaintProgresslist = (List<ComplaintProgress>)this.complaintProgressDAO.findComplaintProgressByComplaintLicenseAndProgressStatus(complaintLicenseDTO, user);
				//check ว่ามี progress RL W  ก่อนหน้าอยู่ไหม 
				//ถ้าไม่มี insert RL A  กับ RL G กับ ML W เข้าไปใหม่ 
				//แต่ถ้ามี update RL W เป็น RL G แล้ว insert ML W 
				if(complaintProgresslist.isEmpty() || complaintProgresslist.size() == 0)
				{
					//INSERT ComplaintProgress RL A
					ComplaintProgress complaintProgressPersis = new ComplaintProgress();
					complaintProgressPersis.setProgressRole("RL");
					complaintProgressPersis.setProgressStatus(ComplaintStatus.ADD.getStatus());
					
					complaintProgressPersis.setProgressDate(new Date());
					complaintProgressPersis.setAuthority(user.getUserData().getUserFullName());
					complaintProgressPersis.setRecordStatus(RecordStatus.NORMAL.getStatus());
					complaintProgressPersis.setCreateDtm(new Date());
					complaintProgressPersis.setCreateUser(user.getUserName());
					complaintProgressPersis.setComplaintLicense(persis);
					complaintProgressPersis.setProgressDesc(ComplaintStatus.ADD.getMeaning());
					this.complaintProgressDAO.insert(complaintProgressPersis);
					
					//INSERT ComplaintProgress RL G
					ComplaintProgress complaintProgressPersisNew = new ComplaintProgress();
					complaintProgressPersisNew.setProgressRole("RL");
					complaintProgressPersisNew.setProgressStatus(ComplaintStatus.GETIT.getStatus());
					
					complaintProgressPersisNew.setProgressDate(new Date());
					complaintProgressPersisNew.setAuthority(user.getUserData().getUserFullName());
					complaintProgressPersisNew.setRecordStatus(RecordStatus.NORMAL.getStatus());
					complaintProgressPersisNew.setCreateDtm(new Date());
					complaintProgressPersisNew.setCreateUser(user.getUserName());
					complaintProgressPersisNew.setComplaintLicense(persis);
					complaintProgressPersisNew.setProgressDesc(ComplaintStatus.GETIT.getMeaning());
					this.complaintProgressDAO.insert(complaintProgressPersisNew);
					
					//INSERT ComplaintProgress ML W
					ComplaintProgress complaintProgressPersisNew2 = new ComplaintProgress();
					complaintProgressPersisNew2.setProgressRole("ML");
					complaintProgressPersisNew2.setProgressStatus(ComplaintStatus.WAIT.getStatus());
					
					complaintProgressPersisNew2.setProgressDate(new Date());
					complaintProgressPersisNew2.setAuthority(user.getUserData().getUserFullName());
					complaintProgressPersisNew2.setRecordStatus(RecordStatus.NORMAL.getStatus());
					complaintProgressPersisNew2.setCreateDtm(new Date());
					complaintProgressPersisNew2.setCreateUser(user.getUserName());
					complaintProgressPersisNew2.setComplaintLicense(persis);
					complaintProgressPersisNew2.setProgressDesc(ComplaintStatus.WAIT.getMeaning());
					this.complaintProgressDAO.insert(complaintProgressPersisNew2);
				}
				else
				{
					//UPDATE ComplaintProgress RL W >> RL G
					ComplaintProgress complaintProgressPersis = complaintProgresslist.get(0);
					complaintProgressPersis.setProgressStatus(ComplaintStatus.GETIT.getStatus());
					complaintProgressPersis.setProgressDesc(ComplaintStatus.GETIT.getMeaning());
					complaintProgressPersis.setLastUpdDtm(new Date());
					complaintProgressPersis.setLastUpdUser(user.getUserName());
					this.complaintProgressDAO.update(complaintProgressPersis);
					
					//INSERT ComplaintProgress ML W
					ComplaintProgress complaintProgressPersisNew = new ComplaintProgress();
					complaintProgressPersisNew.setProgressRole("ML");
					complaintProgressPersisNew.setProgressStatus(ComplaintStatus.WAIT.getStatus());
					
					complaintProgressPersisNew.setProgressDate(new Date());
					complaintProgressPersisNew.setAuthority(user.getUserData().getUserFullName());
					complaintProgressPersisNew.setRecordStatus(RecordStatus.NORMAL.getStatus());
					complaintProgressPersisNew.setCreateDtm(new Date());
					complaintProgressPersisNew.setCreateUser(user.getUserName());
					complaintProgressPersisNew.setComplaintLicense(persis);
					complaintProgressPersisNew.setProgressDesc(ComplaintStatus.WAIT.getMeaning());
					this.complaintProgressDAO.insert(complaintProgressPersisNew);
					
				}
				
				
				
			}
			//RL R นิติกรไม่รับเรื่อง
			if(dto.getComplaintProgress().equals("RL") && dto.getComplaintStatus().equals(ComplaintStatus.REJECT.getStatus()))
			{
				ComplaintLicenseDTO complaintLicenseDTO = new ComplaintLicenseDTO();
				complaintLicenseDTO.setComplaintLicenseId(persis.getComplaintId());
				complaintLicenseDTO.setComplaintStatus(ComplaintStatus.WAIT.getStatus());
				complaintLicenseDTO.setComplaintProgress("RL");
				
				List<ComplaintProgress> complaintProgresslist = (List<ComplaintProgress>)this.complaintProgressDAO.findComplaintProgressByComplaintLicenseAndProgressStatus(complaintLicenseDTO, user);
				//check ว่ามี progress RL W  ก่อนหน้าอยู่ไหม 
				//ถ้าไม่มี insert RL A  กับ RL R เข้าไปใหม่ 
				//แต่ถ้ามี update RL W เป็น RL R
				if(complaintProgresslist.isEmpty() || complaintProgresslist.size() == 0)
				{
					//INSERT ComplaintProgress RL A
					ComplaintProgress complaintProgressPersis = new ComplaintProgress();
					complaintProgressPersis.setProgressRole("RL");
					complaintProgressPersis.setProgressStatus(ComplaintStatus.ADD.getStatus());
					
					complaintProgressPersis.setProgressDate(new Date());
					complaintProgressPersis.setAuthority(user.getUserData().getUserFullName());
					complaintProgressPersis.setRecordStatus(RecordStatus.NORMAL.getStatus());
					complaintProgressPersis.setCreateDtm(new Date());
					complaintProgressPersis.setCreateUser(user.getUserName());
					complaintProgressPersis.setComplaintLicense(persis);
					complaintProgressPersis.setProgressDesc(ComplaintStatus.ADD.getMeaning());
					this.complaintProgressDAO.insert(complaintProgressPersis);
					
					//INSERT ComplaintProgress RL R
					ComplaintProgress complaintProgressPersisNew = new ComplaintProgress();
					complaintProgressPersisNew.setProgressRole("RL");
					complaintProgressPersisNew.setProgressStatus(ComplaintStatus.REJECT.getStatus());
					
					complaintProgressPersisNew.setProgressDate(new Date());
					complaintProgressPersisNew.setAuthority(user.getUserData().getUserFullName());
					complaintProgressPersisNew.setRecordStatus(RecordStatus.NORMAL.getStatus());
					complaintProgressPersisNew.setCreateDtm(new Date());
					complaintProgressPersisNew.setCreateUser(user.getUserName());
					complaintProgressPersisNew.setComplaintLicense(persis);
					complaintProgressPersisNew.setProgressDesc(ComplaintStatus.REJECT.getMeaning());
					this.complaintProgressDAO.insert(complaintProgressPersisNew);
				}
				else
				{
					//UPDATE ComplaintProgress RL W >> RL R
					ComplaintProgress complaintProgressPersis = complaintProgresslist.get(0);
					complaintProgressPersis.setProgressStatus(ComplaintStatus.REJECT.getStatus());
					complaintProgressPersis.setProgressDesc(ComplaintStatus.REJECT.getMeaning());
					complaintProgressPersis.setLastUpdDtm(new Date());
					complaintProgressPersis.setLastUpdUser(user.getUserName());
					this.complaintProgressDAO.update(complaintProgressPersis);
				}
				
				
			}
			//ML G หัวหน้านิติกรรับเรื่อง
			if(dto.getComplaintProgress().equals("ML") && dto.getComplaintStatus().equals(ComplaintStatus.GETIT.getStatus()))
			{
				persis.setComplaintProgress("WL");
				
				ComplaintLicenseDTO complaintLicenseDTO = new ComplaintLicenseDTO();
				complaintLicenseDTO.setComplaintLicenseId(persis.getComplaintId());
				complaintLicenseDTO.setComplaintStatus(ComplaintStatus.WAIT.getStatus());
				complaintLicenseDTO.setComplaintProgress("ML");
				
				//FIND ComplaintProgress ML W
				List<ComplaintProgress> complaintProgresslist = (List<ComplaintProgress>)this.complaintProgressDAO.findComplaintProgressByComplaintLicenseAndProgressStatus(complaintLicenseDTO, user);

				//UPDATE ComplaintProgress ML W >> ML G
				ComplaintProgress complaintProgressPersis = complaintProgresslist.get(0);
				complaintProgressPersis.setProgressStatus(ComplaintStatus.GETIT.getStatus());
				complaintProgressPersis.setProgressDesc(ComplaintStatus.GETIT.getMeaning());
				complaintProgressPersis.setLastUpdDtm(new Date());
				complaintProgressPersis.setLastUpdUser(user.getUserName());
				this.complaintProgressDAO.update(complaintProgressPersis);
				
				//INSERT ComplaintProgress WL W
//				ComplaintProgress complaintProgressPersisNew = new ComplaintProgress();
//				complaintProgressPersisNew.setProgressRole("WL");
//				complaintProgressPersisNew.setProgressStatus(ComplaintStatus.WAIT.getStatus());
//				
//				complaintProgressPersisNew.setProgressDate(new Date());
//				complaintProgressPersisNew.setAuthority(user.getUserData().getUserFullName());
//				complaintProgressPersisNew.setRecordStatus(RecordStatus.NORMAL.getStatus());
//				complaintProgressPersisNew.setCreateDtm(new Date());
//				complaintProgressPersisNew.setCreateUser(user.getUserName());
//				complaintProgressPersisNew.setComplaintLicense(persis);
////				complaintProgressPersisNew.setProgressDesc(ComplaintStatus.WAIT.getMeaning());
//				complaintProgressPersisNew.setProgressDesc("พิจารณาเรื่อง");
//				this.complaintProgressDAO.insert(complaintProgressPersisNew);
				
			} 
			//ML R หัวหน้านิติกรไม่รับเรื่อง
			if(dto.getComplaintProgress().equals("ML") && dto.getComplaintStatus().equals(ComplaintStatus.REJECT.getStatus()))
			{
				ComplaintLicenseDTO complaintLicenseDTO = new ComplaintLicenseDTO();
				complaintLicenseDTO.setComplaintLicenseId(persis.getComplaintId());
				complaintLicenseDTO.setComplaintStatus(ComplaintStatus.WAIT.getStatus());
				complaintLicenseDTO.setComplaintProgress("ML");
				
				//FIND ComplaintProgress ML W
				List<ComplaintProgress> complaintProgresslist = (List<ComplaintProgress>)this.complaintProgressDAO.findComplaintProgressByComplaintLicenseAndProgressStatus(complaintLicenseDTO, user);

				//UPDATE ComplaintProgress ML W >> ML R
				ComplaintProgress complaintProgressPersis = complaintProgresslist.get(0);
				complaintProgressPersis.setProgressStatus(ComplaintStatus.REJECT.getStatus());
				complaintProgressPersis.setProgressDesc(ComplaintStatus.REJECT.getMeaning());
				complaintProgressPersis.setLastUpdDtm(new Date());
				complaintProgressPersis.setLastUpdUser(user.getUserName());
				this.complaintProgressDAO.update(complaintProgressPersis);
			}
			
			//WL W นิติกรพิจาณาเรื่อง อยู่ระหว่างดำเนินการ
			if(dto.getComplaintProgress().equals("WL") && dto.getComplaintStatus().equals(ComplaintStatus.WAIT.getStatus()))
			{

			}
			//WL S นิติกรพิจาณาเรื่อง ยุติเรื่องร้องเรียน
			if(dto.getComplaintProgress().equals("WL") && dto.getComplaintStatus().equals(ComplaintStatus.SUCCESS.getStatus()))
			{

			}
			//End Save ComplaintProgress
			
			
			//Authority
			if(dto.getUserId() > 0)
			{
				persis.setAdmUser((AdmUser)this.admUserDAO.findByPrimaryKey(dto.getUserId()));
			}
			//MasComplaintType
			if(dto.getMasComplaintTypeId() > 0)
			{
				persis.setMasComplaintType((MasComplaintType)this.complaintTypeDAO.findByPrimaryKey(dto.getMasComplaintTypeId()));
			}
			persis.setComplaintDate(new Date());
			persis.setLastUpdUser(user.getUserName());
			persis.setLastUpdDtm(new Date());
			
			//Update ComplaintLicense
			complaintLicenseDAO.update(persis);
			
			//Save ComplaintMapTrader
			ComplaintMapTrader complaintMapTraderPersis = null;
			 List<ComplaintMapTrader> list = (List<ComplaintMapTrader>)this.complaintMapTraderDAO.findComplaintMapTraderByComplaintLicense(persis);
			if(!list.isEmpty() && list.size() > 0)
			{
				complaintMapTraderPersis = list.get(0);
				complaintMapTraderPersis.setLicenseNo(dto.getLicenseNo());
				complaintMapTraderPersis.setTraderType(dto.getTraderType());
				
				complaintMapTraderPersis.setTraderName(dto.getTraderName());
				
				//Check licenseNo มีอยู่จริงไหม
				List<Trader> listTrader = (List<Trader>)this.traderDAO.findTraderByLicenseNo(dto.getLicenseNo(), dto.getTraderType(), RecordStatus.NORMAL.getStatus(), dto.getOrgId());
				
				if(!listTrader.isEmpty() && listTrader.size() > 0)
				{
					complaintMapTraderPersis.setGenuineLicense("G");
				}
				else
				{
					complaintMapTraderPersis.setGenuineLicense("F");
				}
				//
				
				this.complaintMapTraderDAO.update(complaintMapTraderPersis);
			}
			else
			{
				complaintMapTraderPersis = new ComplaintMapTrader();
				complaintMapTraderPersis.setComplaintLicense(persis);
				complaintMapTraderPersis.setLicenseNo(dto.getLicenseNo());
				complaintMapTraderPersis.setTraderType(dto.getTraderType());
				
				complaintMapTraderPersis.setTraderName(dto.getTraderName());
				
				//Check licenseNo มีอยู่จริงไหม
				List<Trader> listTrader = (List<Trader>)this.traderDAO.findTraderByLicenseNo(dto.getLicenseNo(), dto.getTraderType(), RecordStatus.NORMAL.getStatus(), dto.getOrgId());
				
				if(!listTrader.isEmpty() && listTrader.size() > 0)
				{
					complaintMapTraderPersis.setGenuineLicense("G");
				}
				else
				{
					complaintMapTraderPersis.setGenuineLicense("F");
				}
				//
				
				this.complaintMapTraderDAO.insert(complaintMapTraderPersis);
			}
			
			
			
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("ไม่สามารถบันทึกข้อมูลการร้องเรียนได้");
		}
	}

	public List getComplaintPaging(Object object, User user, int start,int limit) throws Exception {

		if(!(object instanceof ComplaintLicenseDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถค้นหาข้อมูลการร้องเรียนได้");
		}
		
		List<ComplaintLicenseDTO> list = new ArrayList<ComplaintLicenseDTO>();
		
		try
		{
			ComplaintLicenseDTO params = (ComplaintLicenseDTO)object;
			
			DateFormat ft =  DateUtils.getProcessDateFormatThai();
			
			List<Object[]> listObj = this.complaintLicenseDAO.findComplaintPaging(params, user , start, limit);
			
			System.out.println("#####listObj.size() = "+listObj.size());
			
			if(!listObj.isEmpty())
			{
				for(Object[] sel: listObj)
				{
					ComplaintLicenseDTO dto = new ComplaintLicenseDTO();

					dto.setComplaintLicenseId(Long.valueOf(sel[0].toString()));
					
					dto.setQuestionType(sel[1]==null?"":sel[1].toString());
					dto.setComplaintDesc(sel[2]==null?"":sel[2].toString());
					dto.setComplaintCardId(sel[3]==null?"":sel[3].toString());
					dto.setComplaintName(sel[4]==null?"":sel[4].toString());
					dto.setComplaintTel(sel[5]==null?"":sel[5].toString());
					dto.setComplaintEmail(sel[6]==null?"":sel[6].toString());
					dto.setComplaintNo(sel[7]==null?"":sel[7].toString());
					if(sel[8] != null)
					{
						dto.setComplaintDate(ft.format((Date)sel[8]));
					}
					if(sel[9] != null)
					{
						dto.setMasComplaintTypeId(Long.valueOf(sel[9].toString()));
						dto.setComplaintTypeName(((MasComplaintType)this.complaintTypeDAO.findByPrimaryKey(Long.valueOf(sel[9].toString()))).getComplaintTypeName());
					}
					
					dto.setComplaintStatus(sel[10]==null?"":sel[10].toString());

					//
					
					dto.setAuthorityComment(sel[11]==null?"":sel[11].toString());
					dto.setComplaintProgress(sel[12]==null?"":sel[12].toString());
					if(sel[13] != null)
					{
						dto.setUserId(Long.valueOf(sel[13].toString()));
						AdmUser admUserPersis = (AdmUser)this.admUserDAO.findByPrimaryKey(Long.valueOf(sel[13].toString()));
						
						String userFullName = ((MasPrefix)this.masPrefixDAO.findByPrimaryKey
								(admUserPersis.getMasPrefix().getPrefixId())).getPrefixName()
								+admUserPersis.getUserName()+" "+admUserPersis.getUserLastname();
						dto.setUserFullName(userFullName);
						
						dto.setUserFullName(userFullName);
						
						if(user.getUserId() == admUserPersis.getUserId())
						{
							dto.setOfficerAsOwner("1");
						}
						else
						{
							dto.setOfficerAsOwner("0");
						}
					}
					
					//licenseNo
					dto.setLicenseNo(sel[14]==null?"":sel[14].toString());
					
					//traderType
					dto.setTraderType(sel[15]==null?"":sel[15].toString());
					
					if(TraderType.TOUR_COMPANIES.getStatus().equals(sel[15].toString()))
					{
						dto.setTraderTypeName(TraderType.TOUR_COMPANIES.getMeaning());
					}
					if(TraderType.GUIDE.getStatus().equals(sel[15].toString()))
					{
						dto.setTraderTypeName(TraderType.GUIDE.getMeaning());
					}
					if(TraderType.LEADER.getStatus().equals(sel[15].toString()))
					{
						dto.setTraderTypeName(TraderType.LEADER.getMeaning());
					}
					
					//traderOwnerName
					String traderName = "";
					List<Trader> listTrader = (List<Trader>)this.traderDAO.findTraderByLicenseNo(sel[14]==null?"":sel[14].toString(), sel[15]==null?"":sel[15].toString(), RecordStatus.NORMAL.getStatus(), params.getOrgId());
					
					if(!listTrader.isEmpty() && listTrader.size() > 0)
					{
						Trader traderPersis = listTrader.get(0);
						if(TraderType.TOUR_COMPANIES.getStatus().equals(traderPersis.getTraderType()))
						{
							traderName = traderPersis.getTraderName();
						}
						else
						{
							if(PersonType.CORPORATE.getStatus().equals(traderPersis.getPerson().getPersonType()))
							{
								traderName = traderPersis.getPerson().getMasPrefix().getPrefixName() + traderPersis.getPerson().getFirstName() + traderPersis.getPerson().getMasPosfix().getPostfixName();
							}
							else
							{
								traderName = traderPersis.getPerson().getMasPrefix().getPrefixName() + traderPersis.getPerson().getFirstName() + " " + traderPersis.getPerson().getLastName();
							}
	
						}
					}
					else
					{
						
						List<ComplaintMapTrader> complaintMapTraderlist = (List<ComplaintMapTrader>)this.complaintMapTraderDAO.findComplaintMapTraderByComplaintLicense((ComplaintLicense)this.complaintLicenseDAO.findByPrimaryKey(dto.getComplaintLicenseId()));
						if(!complaintMapTraderlist.isEmpty() && complaintMapTraderlist.size() > 0)
						{
							ComplaintMapTrader complaintMapTraderPersis = complaintMapTraderlist.get(0);
							traderName = complaintMapTraderPersis.getTraderName();
						}
					}
					
					dto.setTraderName(traderName);
					
					
					
					
					//ร้องเรียน
					String complaintStatusName = "";
					if(QuestionType.COMPLAINT.getStatus().equals(dto.getQuestionType()))
					{
						//ViewData Complaint
						if(params.getComplaintProgress().equals("RL"))
						{
							if(dto.getComplaintStatus().equals(ComplaintStatus.GETIT.getStatus()) 
									|| dto.getComplaintStatus().equals(ComplaintStatus.REJECT.getStatus()))
							{
								dto.setViewData(true);
							}
						}
						else if(params.getComplaintProgress().equals("ML"))
						{
							if(!(dto.getComplaintStatus().equals(ComplaintStatus.GETIT.getStatus()) 
									&& dto.getComplaintProgress().equals("ML")))
							{
								dto.setViewData(true);
							}
						}
						else if(params.getComplaintProgress().equals("WL"))
						{
							if(! ((dto.getComplaintStatus().equals(ComplaintStatus.GETIT.getStatus()) && dto.getComplaintProgress().equals("WL"))
									||(dto.getComplaintStatus().equals(ComplaintStatus.WAIT.getStatus()) && dto.getComplaintProgress().equals("WL"))))
							{
								dto.setViewData(true);
							}
						}
						
						if(dto.getComplaintStatus() != null)
						{
							if(dto.getComplaintStatus().equals(ComplaintStatus.WAIT.getStatus()))
							{
								complaintStatusName = ComplaintStatus.WAIT.getMeaning();
							}
							if(dto.getComplaintStatus().equals(ComplaintStatus.SUCCESS.getStatus()))
							{
								complaintStatusName = ComplaintStatus.SUCCESS.getMeaning();
							}
							if(dto.getComplaintStatus().equals(ComplaintStatus.REJECT.getStatus()))
							{
								complaintStatusName = ComplaintStatus.REJECT.getMeaning();
							}
							if(dto.getComplaintStatus().equals(ComplaintStatus.GETIT.getStatus()))
							{
								complaintStatusName = ComplaintStatus.GETIT.getMeaning();
							}
							if(dto.getComplaintStatus().equals(ComplaintStatus.ADD.getStatus()))
							{
								complaintStatusName = ComplaintStatus.ADD.getMeaning();
							}
							dto.setComplaintStatusName(complaintStatusName);
						}
					}
					//เป็นคำถาม
					else if(QuestionType.QUESTION.getStatus().equals(dto.getQuestionType()))
					{
						if(dto.getComplaintStatus() != null)
						{
							if(dto.getComplaintStatus().equals(ComplaintStatus.SUCCESS.getStatus()))
							{
								complaintStatusName = "ตอบข้อซักถามแล้ว";
								dto.setViewData(true);
							}
							else
							{
								complaintStatusName = "กำลังดำเนินการ";
								dto.setViewData(false);
							}
							dto.setComplaintStatusName(complaintStatusName);
						}
						if(sel[16] != null)
						{
							dto.setAuthorityDate(ft.format((Date)sel[16]));
						}
					}
					
				    //เช็คว่าเป็นแถบแสดงข้อมูลหรือไม่
					System.out.println("params.getComplaintProgress = "+params.getComplaintProgress());
					if(params.getComplaintProgress() != null && params.getComplaintProgress().equals("INFO"))
					{
						System.out.println("INFOINFOINFOINFOINFOINFOINFOINFOINFOINFOINFOINFOINFOINFOINFOINFOINFOINFOINFOINFOINFO");
						//เช็ค ComplaintLicense ว่าเป็น WL W กับ WL S หรือไม่ ถ้าเป็น แสดงว่าเป็นเรื่องร้องเรียนที่ต้องแสดง 
						if( ((dto.getComplaintStatus().equals(ComplaintStatus.WAIT.getStatus()) && dto.getComplaintProgress().equals("WL"))
								||(dto.getComplaintStatus().equals(ComplaintStatus.SUCCESS.getStatus()) && dto.getComplaintProgress().equals("WL"))))
						{
							list.add(dto);
						}
					}
					else
					{
						list.add(dto);
					}
				}
			}
			
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new Exception("ไม่สามารถค้นหาข้อมูลการร้องเรียนได้");
		}

		return list;
	}
	
	
	public List getComplaintProgressByConsider(Object object, User user) throws Exception
	{
		// TODO Auto-generated method stub
		System.out.println("###Service ComplaintService method getComplaintProgressByConsider");
		if(!(object instanceof ComplaintProgressDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถค้นหาข้อมูลการพิจารณาเรื่องได้");
		}
		
		List<ComplaintProgressDTO> list = new ArrayList<ComplaintProgressDTO>();
		
		ComplaintProgressDTO params = (ComplaintProgressDTO)object;
		
		List<ComplaintProgress> listComplaintProgress = this.complaintProgressDAO.findComplaintProgressByConsider(params, user);
		
		if(!listComplaintProgress.isEmpty())
		{
			int i = 0;
			for(ComplaintProgress progress: listComplaintProgress)
			{
				ComplaintProgressDTO dto = new ComplaintProgressDTO();
				
				ObjectUtil.copy(progress, dto);
				
				//เช็คว่าให้ edit รายละเอียด การพิจารณาเรื่องได้ไหม
//				if(i == 0)
//				{
//					dto.setEdit(false);
//				}
//				else
//				{
					dto.setEdit(true);
//				}
				
				if(ComplaintStatus.WAIT.getStatus().equals(dto.getProgressStatus()))
				{
					dto.setProgressStatusName(ComplaintStatus.WAIT.getMeaning());
//					dto.setProgressStatusName("อยู่ระหว่างพิจารณาเรื่องร้องเรียน");
//					dto.setProgressStatusName("อยู่ระหว่างดำเนินการ");
				}
				else if(ComplaintStatus.SUCCESS.getStatus().equals(dto.getProgressStatus()))
				{
					dto.setProgressStatusName("ยุติเรื่องร้องเรียน");
				}
				
				dto.setComplaintLicenseId(params.getComplaintLicenseId());
				
				list.add(dto);
				i++;
			}
		}
		
		return list;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveComplaintProgressConsider(Object object, User user)
			throws Exception {
		// TODO Auto-generated method stub
		if(!(object instanceof ComplaintProgressDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลการพิจารณาเรื่องได้");
		}
		
		try
		{
			ComplaintProgressDTO dto = (ComplaintProgressDTO) object;
			
			ComplaintProgress persis = null;
			
			if(dto.getComProgressId() > 0)
			{
				persis = (ComplaintProgress)this.complaintProgressDAO.findByPrimaryKey(dto.getComProgressId());
				ObjectUtil.copy(dto, persis);
				
				//ProgressDate
//				Timestamp stamp = new Timestamp(System.currentTimeMillis());
//			    String strDate = dto.getProgressDate();//"05/01/2557";
//			    DateFormat ft =  DateUtils.getProcessDateFormatThai();
//			    Date date = ft.parse(strDate);
//			    date.setHours(stamp.getHours());
//			    date.setMinutes(stamp.getMinutes());
//			    date.setSeconds(stamp.getSeconds());
//			    
//				persis.setProgressDate(date);
				//
				
				persis.setLastUpdDtm(new Date());
				persis.setLastUpdUser(user.getUserName());
				
				this.complaintProgressDAO.update(persis);
				
				//update ComplaintLicense
				ComplaintLicense complaintLicensePersis = (ComplaintLicense)this.complaintLicenseDAO.findByPrimaryKey(dto.getComplaintLicenseId());
				
				complaintLicensePersis.setComplaintStatus(dto.getProgressStatus());
				complaintLicensePersis.setLastUpdUser(user.getUserName());
				complaintLicensePersis.setLastUpdDtm(new Date());
				
				this.complaintLicenseDAO.update(complaintLicensePersis);
			}
			else
			{
				persis = new ComplaintProgress();
				ObjectUtil.copy(dto, persis);
				
				persis.setComplaintLicense((ComplaintLicense)this.complaintLicenseDAO.findByPrimaryKey(dto.getComplaintLicenseId()));
				persis.setAuthority(user.getUserData().getUserFullName());
				persis.setRecordStatus(RecordStatus.NORMAL.getStatus());
				
				//ProgressDate
//				Timestamp stamp = new Timestamp(System.currentTimeMillis());
//			    String strDate = dto.getProgressDate();//"05/01/2557";
//			    DateFormat ft =  DateUtils.getProcessDateFormatThai();
//			    Date date = ft.parse(strDate);
//			    date.setHours(stamp.getHours());
//			    date.setMinutes(stamp.getMinutes());
//			    date.setSeconds(stamp.getSeconds());
//			    
//				persis.setProgressDate(date);
				//
				
				persis.setCreateDtm(new Date());
				persis.setCreateUser(user.getUserName());
				
				this.complaintProgressDAO.insert(persis);
				
				//update ComplaintLicense
				ComplaintLicense complaintLicensePersis = (ComplaintLicense)this.complaintLicenseDAO.findByPrimaryKey(dto.getComplaintLicenseId());
				
				complaintLicensePersis.setComplaintStatus(dto.getProgressStatus());
				complaintLicensePersis.setLastUpdUser(user.getUserName());
				complaintLicensePersis.setLastUpdDtm(new Date());
				
				this.complaintLicenseDAO.update(complaintLicensePersis);
				
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("ไม่สามารถบันทึกข้อมูลการพิจารณาเรื่องได้");
		}
		
	}

	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(Object[] object , User user) throws Exception {
		
		for (Object obj : object) {
			ComplaintProgressDTO dto = (ComplaintProgressDTO) obj;
			ComplaintProgress complaintProgress = (ComplaintProgress)this.complaintProgressDAO.findByPrimaryKey(dto.getComProgressId());
			this.complaintProgressDAO.delete(complaintProgress);
			
		}
		
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveQuestion(Object object, User user) throws Exception 
	{
		if(!(object instanceof ComplaintLicenseDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลข้อซักถามได้");
		}
		
		try
		{
			ComplaintLicenseDTO dto = (ComplaintLicenseDTO) object;
			
			ComplaintLicense persis = (ComplaintLicense)this.complaintLicenseDAO.findByPrimaryKey(dto.getComplaintLicenseId());
			
			ObjectUtil.copy(dto, persis);
			
			persis.setAuthorityDate(new Date());
			persis.setLastUpdUser(user.getUserName());
			persis.setLastUpdDtm(new Date());
			
			this.complaintLicenseDAO.update(persis);
		
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("ไม่สามารถบันทึกข้อมูลข้อซักถามได้");
		}
	
	}


	public List readComplaintProgressAll(Object object, User user)throws Exception 
	{
		System.out.println("###Service ComplaintService method readComplaintProgressAll");
		if(!(object instanceof ComplaintProgressDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถค้นหาข้อมูลสถานะเรื่องร้องเรียนได้");
		}
		
		List<ComplaintProgressDTO> list = new ArrayList<ComplaintProgressDTO>();
		
		ComplaintProgressDTO params = (ComplaintProgressDTO)object;
		
		List<ComplaintProgress> listComplaintProgress = this.complaintProgressDAO.findComplaintProgressByConsider(params, user);
		
		if(!listComplaintProgress.isEmpty())
		{
			int size = listComplaintProgress.size();
			int i = 1;
			for(ComplaintProgress progress: listComplaintProgress)
			{
				ComplaintProgressDTO dto = new ComplaintProgressDTO();
				
				ObjectUtil.copy(progress, dto);
				
				//Authority
				if(ComplaintStatus.ADD.getStatus().equals(dto.getProgressStatus()))
				{
					dto.setAuthority(ProgressRole.PEOPLE.getMeaning()+" "+dto.getAuthority());
				}
				else if(ProgressRole.RECEIPTLAW.getStatus().equals(dto.getProgressRole()) && !ComplaintStatus.ADD.getStatus().equals(dto.getProgressStatus()))
				{
					dto.setAuthority(ProgressRole.RECEIPTLAW.getMeaning()+" "+dto.getAuthority());
				}
				else if(ProgressRole.MANAGERLAW.getStatus().equals(dto.getProgressRole()) )
				{
					dto.setAuthority(ProgressRole.MANAGERLAW.getMeaning()+" "+dto.getAuthority());
				}
				else if(ProgressRole.WORKERLAW.getStatus().equals(dto.getProgressRole()) )
				{
					dto.setAuthority(ProgressRole.WORKERLAW.getMeaning()+" "+dto.getAuthority());
				}
				
				//ProgressStatusName
				if(ComplaintStatus.ADD.getStatus().equals(dto.getProgressStatus()))
				{
					dto.setProgressStatusName(ComplaintStatus.ADD.getMeaning());
				}
				else if(ComplaintStatus.WAIT.getStatus().equals(dto.getProgressStatus()))
				{
					dto.setProgressStatusName(ComplaintStatus.WAIT.getMeaning());
				}
				else if(ComplaintStatus.GETIT.getStatus().equals(dto.getProgressStatus()))
				{
					dto.setProgressStatusName(ComplaintStatus.GETIT.getMeaning());
				}
				else if(ComplaintStatus.REJECT.getStatus().equals(dto.getProgressStatus()))
				{
					dto.setProgressStatusName(ComplaintStatus.REJECT.getMeaning());
				}
				else if(ComplaintStatus.SUCCESS.getStatus().equals(dto.getProgressStatus()))
				{
					dto.setProgressStatusName(ComplaintStatus.SUCCESS.getMeaning());
				}
				
				//เช็คว่าเป็น WL W หรือไม่ถ้าใช่ ให้เอาตัวล่าสุด(ตัวสุดท้าย)
				System.out.println("#####progress.getProgressRole() = "+progress.getProgressRole());
				if(progress.getProgressRole().equals("WL"))
				{
					if(i==size)
					{
						list.add(dto);
					}
				}
				else
				{
					list.add(dto);
				}
				
				i++;
			}
		}
		
		return list;
				
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void deleteDocument(Object object, User user) throws Exception {
		// TODO Auto-generated method stub
		
		System.out.println("###Service ComplaintService method deleteDocument");
		if(!(object instanceof ComplaintDocDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถลบเอกสารแนบได้");
		}
		
		ComplaintDocDTO params = (ComplaintDocDTO)object;
		
		//Delete Old File
		String oldDocPath = params.getComplaintDocPath();
		System.out.println("oldDocPath = "+oldDocPath);
		FileManage fm = new FileManage();
		fm.deleteFile(oldDocPath);
		
		//Delete ComplaintDoc
		ComplaintDoc persis = (ComplaintDoc)this.complaintDocDAO.findByPrimaryKey(params.getComplaintDocId());
		this.complaintDocDAO.delete(persis);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveChangeQuestionType(Object object, User user)throws Exception 
	{
		// TODO Auto-generated method stub
		if(!(object instanceof ComplaintLicenseDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลการส่งเรื่องไปกระบวนการตอบข้อซักถาม");
		}
		
		try
		{
			ComplaintLicenseDTO dto = (ComplaintLicenseDTO) object;
			
			ComplaintLicense persis = (ComplaintLicense)this.complaintLicenseDAO.findByPrimaryKey(dto.getComplaintLicenseId());
			
			ObjectUtil.copy(dto, persis);

			//set value to ComplaintLicense
			persis.setComplaintStatus(ComplaintStatus.ADD.getStatus());
			persis.setComplaintProgress(ProgressRole.RECEIPTLAW.getStatus());
			persis.setComplaintDate(new Date());
			persis.setLastUpdUser(user.getUserName());
			persis.setLastUpdDtm(new Date());
			
			//Update ComplaintLicense
			complaintLicenseDAO.update(persis);
			
			if(dto.getComplaintStatus() == null || dto.getComplaintStatus().equals(""))
			{
				dto.setComplaintStatus(ComplaintStatus.ADD.getStatus());
			}

			//Add ComplaintProgress
			//RL A นิติกรเพิ่มเรื่อง
			if(dto.getComplaintProgress().equals("RL") && dto.getComplaintStatus().equals(ComplaintStatus.ADD.getStatus()))
			{
				List<ComplaintProgress> complaintProgresslist = (List<ComplaintProgress>)this.complaintProgressDAO.findComplaintProgressByComplaintLicenseAndProgressStatus(dto, user);

				if(complaintProgresslist.isEmpty() || complaintProgresslist.size() == 0)
				{
					//INSERT ComplaintProgress RL A
					ComplaintProgress complaintProgressPersis = new ComplaintProgress();
					complaintProgressPersis.setProgressRole(persis.getComplaintProgress());
					complaintProgressPersis.setProgressStatus(persis.getComplaintStatus());
					
					complaintProgressPersis.setProgressDate(new Date());
					complaintProgressPersis.setAuthority(user.getUserData().getUserFullName());
					complaintProgressPersis.setRecordStatus(RecordStatus.NORMAL.getStatus());
					complaintProgressPersis.setCreateDtm(new Date());
					complaintProgressPersis.setCreateUser(user.getUserName());
					complaintProgressPersis.setComplaintLicense(persis);
					complaintProgressPersis.setProgressDesc(ComplaintStatus.ADD.getMeaning());
					this.complaintProgressDAO.insert(complaintProgressPersis);
					
					//INSERT ComplaintProgress RL W
					ComplaintProgress complaintProgressPersis2 = new ComplaintProgress();
					complaintProgressPersis2.setProgressRole(persis.getComplaintProgress());
					complaintProgressPersis2.setProgressStatus(ComplaintStatus.WAIT.getStatus());
					
					complaintProgressPersis2.setProgressDate(new Date());
					complaintProgressPersis2.setAuthority(user.getUserData().getUserFullName());
					complaintProgressPersis2.setRecordStatus(RecordStatus.NORMAL.getStatus());
					complaintProgressPersis2.setCreateDtm(new Date());
					complaintProgressPersis2.setCreateUser(user.getUserName());
					complaintProgressPersis2.setComplaintLicense(persis);
					complaintProgressPersis2.setProgressDesc(ComplaintStatus.WAIT.getMeaning());
					this.complaintProgressDAO.insert(complaintProgressPersis2);
					
					//Save ComplaintMapTrader
					ComplaintMapTrader complaintMapTraderPersis = null;
					 List<ComplaintMapTrader> list = (List<ComplaintMapTrader>)this.complaintMapTraderDAO.findComplaintMapTraderByComplaintLicense(persis);
					if(!list.isEmpty() && list.size() > 0)
					{
						complaintMapTraderPersis = list.get(0);
						complaintMapTraderPersis.setLicenseNo(dto.getLicenseNo());
						complaintMapTraderPersis.setTraderType(dto.getTraderType());
						
						complaintMapTraderPersis.setTraderName(dto.getTraderName());
						
						//Check licenseNo มีอยู่จริงไหม
						List<Trader> listTrader = (List<Trader>)this.traderDAO.findTraderByLicenseNo(dto.getLicenseNo(), dto.getTraderType(), RecordStatus.NORMAL.getStatus(), dto.getOrgId());
						
						if(!listTrader.isEmpty() && listTrader.size() > 0)
						{
							complaintMapTraderPersis.setGenuineLicense("G");
						}
						else
						{
							complaintMapTraderPersis.setGenuineLicense("F");
						}
						//
						
						this.complaintMapTraderDAO.update(complaintMapTraderPersis);
					}
					else
					{
						complaintMapTraderPersis = new ComplaintMapTrader();
						complaintMapTraderPersis.setComplaintLicense(persis);
						complaintMapTraderPersis.setLicenseNo(dto.getLicenseNo());
						complaintMapTraderPersis.setTraderType(dto.getTraderType());
						
						complaintMapTraderPersis.setTraderName(dto.getTraderName());
						
						//Check licenseNo มีอยู่จริงไหม
						List<Trader> listTrader = (List<Trader>)this.traderDAO.findTraderByLicenseNo(dto.getLicenseNo(), dto.getTraderType(), RecordStatus.NORMAL.getStatus(), dto.getOrgId());
						
						if(!listTrader.isEmpty() && listTrader.size() > 0)
						{
							complaintMapTraderPersis.setGenuineLicense("G");
						}
						else
						{
							complaintMapTraderPersis.setGenuineLicense("F");
						}
						//
						
						this.complaintMapTraderDAO.insert(complaintMapTraderPersis);
					}
					
				}
			}
			
			//Delete ComplaintProgress
			ComplaintLicenseDTO dto2 = new ComplaintLicenseDTO();
			dto2.setComplaintLicenseId(dto.getComplaintLicenseId());
			//find all complaintProgress by complaintLicenseId
			List<ComplaintProgress> complaintProgresslist = (List<ComplaintProgress>)this.complaintProgressDAO.findComplaintProgressByComplaintLicenseAndProgressStatus(dto2, user);

			if(!complaintProgresslist.isEmpty())
			{
				for(ComplaintProgress obj: complaintProgresslist)
				{
					if(obj.getProgressRole().equals(ProgressRole.MANAGERLAW.getStatus()))
					{
						this.complaintProgressDAO.delete(obj);
					}
					if(obj.getProgressRole().equals(ProgressRole.WORKERLAW.getStatus()))
					{
						this.complaintProgressDAO.delete(obj);
					}
					if(obj.getProgressRole().equals(ProgressRole.RECEIPTLAW.getStatus()) 
							&& obj.getProgressStatus().equals(ComplaintStatus.GETIT.getStatus()))
					{
						obj.setProgressStatus(ComplaintStatus.WAIT.getStatus());
						this.complaintProgressDAO.update(obj);
					}
				}
			}
			
		}catch(Exception e){
		e.printStackTrace();
		throw new Exception("ไม่สามารถบันทึกข้อมูลการส่งเรื่องไปกระบวนการตอบข้อซักถาม");
		}
	}
	


}












