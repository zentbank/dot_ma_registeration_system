package sss.dot.tourism.service.complaint;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.sss.aut.service.User;

public interface IComplaintService {
	public void createComplaint(Object object, User user) throws Exception;
	
	public void upload(Object object, MultipartFile file, User user)throws Exception;
	
	public List getDocument(Object object, User user) throws Exception;
	
	public void saveComplaintReceive(Object object, User user) throws Exception;
	
	public List getComplaintPaging(Object object, User user ,int start, int limit) throws Exception;
	
	public List getComplaintProgressByConsider(Object object, User user) throws Exception;
	
	public void saveComplaintProgressConsider(Object object, User user) throws Exception;
	
	public void delete(Object[] object, User user) throws Exception;
	
	public void saveQuestion(Object object, User user) throws Exception;
	
	public List readComplaintProgressAll(Object object, User user) throws Exception;
	
	public void deleteDocument(Object object, User user) throws Exception;
	
	public void saveChangeQuestionType(Object object, User user) throws Exception;
}
