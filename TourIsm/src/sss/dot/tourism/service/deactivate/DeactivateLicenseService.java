package sss.dot.tourism.service.deactivate;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.AdmUserDAO;
import sss.dot.tourism.dao.deactivate.DeactivateDetailDAO;
import sss.dot.tourism.dao.deactivate.DeactivateLicenseDAO;
import sss.dot.tourism.dao.deactivate.MasDeactivateTypeDAO;
import sss.dot.tourism.dao.mas.MasPrefixDAO;
import sss.dot.tourism.dao.mas.MasRunningNoDAO;
import sss.dot.tourism.dao.registration.GuaranteeDAO;
import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.domain.AdmUser;
import sss.dot.tourism.domain.DeactivateDetail;
import sss.dot.tourism.domain.DeactivateLicense;
import sss.dot.tourism.domain.Guarantee;
import sss.dot.tourism.domain.MasDeactivateType;
import sss.dot.tourism.domain.Organization;
import sss.dot.tourism.domain.Person;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.dto.deactivate.DeactivateLicenseDTO;
import sss.dot.tourism.dto.deactivate.MasDeactivateTypeDTO;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.service.trader.ITraderService;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.DeactivateStatus;
import sss.dot.tourism.util.GuaranteeStatus;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.PersonType;
import sss.dot.tourism.util.RecordStatus;
import sss.dot.tourism.util.SuspendStatus;
import sss.dot.tourism.util.TraderCategory;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.User;



@Repository("deactivateLicenseService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class DeactivateLicenseService implements IDeactivateLicenseService {

	@Autowired
	TraderDAO traderDAO;
	@Autowired
	MasDeactivateTypeDAO masDeactivateTypeDAO;
	
	@Autowired
	MasRunningNoDAO masRunningNoDAO;
	@Autowired
	AdmUserDAO admUserDAO;
	@Autowired
	MasPrefixDAO masPrefixDAO;
	@Autowired
	ITraderService traderService;
	@Autowired
	DeactivateLicenseDAO deactivateLicenseDAO;
	@Autowired
	DeactivateDetailDAO deactivateDetailDAO;
	
	@Autowired
	GuaranteeDAO guaranteeDAO;
	
	public List getDeactivateTypeAll(Object obj, User user) throws Exception {
		if(!(obj instanceof MasDeactivateTypeDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		MasDeactivateTypeDTO param =  (MasDeactivateTypeDTO)obj;
		
		List<MasDeactivateTypeDTO> list = new ArrayList<MasDeactivateTypeDTO>();
		
		if(param.getDeactiveId() > 0)
		{
			list = this.masDeactivateTypeDAO.findbyDeactiveIdAndTraderType(param.getDeactiveId(), param.getLicenseType());
		}
		else
		{
			List<MasDeactivateType> listAll = (List<MasDeactivateType>) this.masDeactivateTypeDAO.findByTraderType(param.getLicenseType() );
			
			
			if(!listAll.isEmpty())
			{
				for(MasDeactivateType act: listAll)
				{
					MasDeactivateTypeDTO dto = new MasDeactivateTypeDTO();
					ObjectUtil.copy(act, dto);
					list.add(dto);
				}
			}
		}
		
		return list;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public Object getTraderPrepareDeactivate(Object obj, User user)
			throws Exception {
		if(!(obj instanceof DeactivateLicenseDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		DeactivateLicenseDTO dto = (DeactivateLicenseDTO)obj; 
		
		DeactivateLicenseDTO param = (DeactivateLicenseDTO)obj; 
		
		Trader trader = (Trader)this.traderDAO.findByPrimaryKey(param.getTraderId());
		
		Person person = trader.getPerson();
		
		String runNo = masRunningNoDAO.getDeactivateTempNo(user.getUserData().getOrgId(), trader.getTraderType());
		ObjectUtil.copy(trader, dto);
		ObjectUtil.copy(person, dto);
		
		StringBuffer traderOwnerName = new StringBuffer();
		String prefixName = "";
		String postfixName = "";
		if(person.getMasPrefix() != null)
		{
			prefixName = person.getMasPrefix().getPrefixName();
		}
		if(person.getMasPosfix() != null)
		{
			postfixName = person.getMasPosfix().getPostfixName();
		}
		if(PersonType.CORPORATE.getStatus().equals(person.getPersonType()))
		{
			traderOwnerName.append(prefixName);
			traderOwnerName.append(person.getFirstName());
			traderOwnerName.append(postfixName);
		}
		else
		{
			traderOwnerName.append(prefixName);
			traderOwnerName.append(person.getFirstName());
			traderOwnerName.append(" ");
			traderOwnerName.append(person.getLastName());
			
		}
		dto.setTraderOwnerName(traderOwnerName.toString());
		
		dto.setTraderCategoryName(TraderCategory.getMeaning(trader.getTraderType(), trader.getTraderCategory()));
		
		Calendar cal = Calendar.getInstance();
		
		DateFormat df = DateUtils.getProcessDateFormatThai();
		
		dto.setDeactivateNo(runNo);
		dto.setDeactivateDate(df.format(cal.getTime()));
		dto.setOfficerName(user.getUserData().getUserFullName());
		
		return dto;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveDeactivation(Object deactivateParam, Object[] object, User user)
			throws Exception {
		
		if(!(deactivateParam instanceof DeactivateLicenseDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		DeactivateLicenseDTO param = (DeactivateLicenseDTO)deactivateParam;
		DeactivateLicense deactivate = null;
		
		Organization org = new Organization();
		org.setOrgId(user.getUserData().getOrgId());
		
		if(param.getDeactiveId() <= 0)
		{
			Trader trader = (Trader)this.traderDAO.findByPrimaryKey(param.getTraderId());
			trader.setOrganization(org);
			
			String runNo = masRunningNoDAO.getDeactivateNo(user.getUserData().getOrgId(), trader.getTraderType());
			
			deactivate = new DeactivateLicense();
			
			ObjectUtil.copy(param, deactivate);
			
			deactivate.setDeactivateNo(runNo);
			param.setDeactivateNo(runNo);
			
			AdmUser admUser = new AdmUser();
			admUser.setUserId(user.getUserId());
			
			deactivate.setAdmUser(admUser);
			
			deactivate.setOrganization(org);
			//
			
			deactivate.setLicenseNo(trader.getLicenseNo());
			deactivate.setTraderType(trader.getTraderType());
			deactivate.setApproveStatus(DeactivateStatus.WAIT.getStatus());
			deactivate.setRecordStatus(RecordStatus.NORMAL.getStatus());
			deactivate.setCreateUser(user.getUserName());
			deactivate.setCreateDtm(new Date());
			
			Long deactiveId = (Long)this.deactivateLicenseDAO.insert(deactivate);
			deactivate.setDeactiveId(deactiveId);
			param.setDeactiveId(deactiveId);

		}
		else
		{
			deactivate = (DeactivateLicense) this.deactivateLicenseDAO.findByPrimaryKey(param.getDeactiveId());
			
			ObjectUtil.copy(param, deactivate);
			
			AdmUser admUser = new AdmUser();
			admUser.setUserId(user.getUserId());
			
			deactivate.setAdmUser(admUser);
			deactivate.setOrganization(org);
			
			deactivate.setApproveStatus(DeactivateStatus.WAIT.getStatus());
			deactivate.setRecordStatus(RecordStatus.NORMAL.getStatus());
			deactivate.setLastUpdUser(user.getUserName());
			deactivate.setLastUpdDtm(new Date());
			
			this.deactivateLicenseDAO.update(deactivate);
		}
		
		this.addDeactivateDetail(object, deactivate, user);
		
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	private void addDeactivateDetail(Object[] object, DeactivateLicense deactivate, User user) throws Exception
	{
		if(object.length > 0)
		{
			List<DeactivateDetail> listOldAct = this.deactivateDetailDAO.findByDeactivate(deactivate.getDeactiveId());
			if(!listOldAct.isEmpty())
			{
				for(DeactivateDetail masDeactivateType: listOldAct)
				{
					this.deactivateDetailDAO.delete(masDeactivateType);
				}
			}
			
			for(Object obj: object)
			{
				MasDeactivateTypeDTO actDTO = (MasDeactivateTypeDTO)obj;
				DeactivateDetail detail = new DeactivateDetail();
				MasDeactivateType masDeactivateType = new MasDeactivateType();
				
				masDeactivateType.setMasDeactivateTypeId(actDTO.getMasDeactivateTypeId());
				
				detail.setMasDeactivateType(masDeactivateType);
				detail.setDeactivateDesc(actDTO.getDeactivateName());
				detail.setDeactivateLicense(deactivate);
				detail.setRecordStatus(RecordStatus.NORMAL.getStatus());
				detail.setCreateUser(user.getUserName());
				detail.setCreateDtm(new Date());
				
				deactivateDetailDAO.insert(detail);
			}
		}
	}

	public List countAll(Object obj, User user) throws Exception
	{
		if(!(obj instanceof DeactivateLicenseDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		
		
		DeactivateLicenseDTO param = (DeactivateLicenseDTO)obj;
		
		return this.deactivateLicenseDAO.findDeactivateLicensePaging(param, param.getStart(), param.getLimit());
	
	}
	
	public List getAll(Object obj, User user) throws Exception {
		if(!(obj instanceof DeactivateLicenseDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		List<DeactivateLicenseDTO> list = new ArrayList<DeactivateLicenseDTO>();
		
		DeactivateLicenseDTO param = (DeactivateLicenseDTO)obj;
		
		param.setOrgId(user.getUserData().getOrgId());
		
		List<Object[]> listAll = this.deactivateLicenseDAO.findDeactivateLicensePaging(param, param.getStart(), param.getLimit());
		
		DateFormat df = DateUtils.getProcessDateFormatThai();
		
		if(!listAll.isEmpty())
		{
			for(Object[] persistence :listAll)
			{
				DeactivateLicenseDTO dto = new DeactivateLicenseDTO();
				
				//   sqlQuery.addScalar("TRADER_ID", Hibernate.LONG); //0
				dto.setTraderId(Long.valueOf(persistence[0].toString()));
				
				//sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING); //1
				dto.setLicenseNo(persistence[1]==null?"":persistence[1].toString());
				
				//sqlQuery.addScalar("TRADER_TYPE", Hibernate.STRING);//3
				dto.setTraderType(persistence[3]==null?"":persistence[3].toString());
				dto.setTraderTypeName(TraderType.getMeaning(dto.getTraderType()));
				

	
				
//				sqlQuery.addScalar("IDENTITY_NO", Hibernate.STRING);//17
				dto.setIdentityNo(persistence[17]==null?"":persistence[17].toString());
//			    sqlQuery.addScalar("PERSON_ID", Hibernate.LONG);//4
				
				//sqlQuery.addScalar("TRADER_CATEGORY", Hibernate.STRING);//18
				dto.setTraderCategory(persistence[18]==null?"":persistence[18].toString());
				dto.setTraderCategoryName(TraderCategory.getMeaning(dto.getTraderType(), dto.getTraderCategory()));
			
//			   
//			    sqlQuery.addScalar("POSTFIX_NAME", Hibernate.STRING);//8
//			    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);//9
				
				
				
			    //sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING); //2
				if(TraderType.TOUR_COMPANIES.equals(dto.getTraderType()))
				{
					dto.setTraderName(persistence[2]==null?"":persistence[2].toString());
					
//				    sqlQuery.addScalar("TRADER_NAME_EN", Hibernate.STRING);//19
					dto.setTraderNameEn(persistence[19]==null?"":persistence[19].toString());
					
					//sqlQuery.addScalar("PREFIX_NAME", Hibernate.STRING);//5
					String prefixName = persistence[5]==null?"":persistence[5].toString();
					//sqlQuery.addScalar("FIRST_NAME", Hibernate.STRING);//6
					String firstName = persistence[6]==null?"":persistence[6].toString();
	
					
					String postfixName = persistence[8]==null?"":persistence[8].toString();
					
					dto.setTraderOwnerName(prefixName+firstName +postfixName);
				}
				else
				{
					//sqlQuery.addScalar("PREFIX_NAME", Hibernate.STRING);//5
					String prefixName = persistence[5]==null?"":persistence[5].toString();
					//sqlQuery.addScalar("FIRST_NAME", Hibernate.STRING);//6
					String firstName = persistence[6]==null?"":persistence[6].toString();
				    // sqlQuery.addScalar("LAST_NAME", Hibernate.STRING);//7
					String lastName = persistence[7]==null?"":persistence[7].toString();
					
					dto.setTraderName(prefixName+firstName +" " + lastName);
					dto.setTraderOwnerName(dto.getTraderName());
				    
				}
			    
//			    sqlQuery.addScalar("DEACTIVE_ID", Hibernate.LONG);//10
				dto.setDeactiveId(Long.valueOf(persistence[10].toString()));
				//sqlQuery.addScalar("DEACTIVATE_DATE", Hibernate.DATE);//11
				if(persistence[11]!=null)
				{
					dto.setDeactivateDate(df.format(persistence[11]));
				}
			    
//			    sqlQuery.addScalar("DEACTIVATE_NO", Hibernate.STRING);//12
				dto.setDeactivateNo(persistence[12]==null?"":persistence[12].toString());
				
				//sqlQuery.addScalar("SUSPEND_PERIOD", Hibernate.INTEGER);//20
				dto.setRenewLicenseStatus(persistence[20]==null?"":persistence[20].toString());
				


				
//				 sqlQuery.addScalar("DEACTIVATE_RESON_REMARK", Hibernate.INTEGER);//21
				dto.setDeactivateResonRemark(persistence[21]==null?"":persistence[21].toString());
				
//			    sqlQuery.addScalar("DEACTIVATE_DATE", Hibernate.DATE);//13
				if(persistence[13]!=null)
				{
					dto.setDeactivateDate(df.format(persistence[13]));
				}
//			    sqlQuery.addScalar("BOOK_DATE", Hibernate.DATE);//14
				if(persistence[14]!=null)
				{
					dto.setBookDate(df.format(persistence[14]));
				}
				
			   

				
//			     sqlQuery.addScalar("BOOK_NO", Hibernate.STRING);//15
				dto.setBookNo(persistence[15]==null?"":persistence[15].toString());
				
				
//			    sqlQuery.addScalar("APPROVE_STATUS", Hibernate.STRING);//16
				dto.setApproveStatus(persistence[16]==null?"":persistence[16].toString());
				dto.setApproveStatusName(DeactivateStatus.getMeaning(dto.getApproveStatus()));
				

//				sqlQuery.addScalar("OFFICER_NAME", Hibernate.STRING);//22
				if(persistence[22] != null)
				{
					AdmUser admUser = (AdmUser) this.admUserDAO.findByPrimaryKey(Long.valueOf(persistence[22].toString()));
					String prefix = admUser.getMasPrefix().getPrefixName();
					dto.setOfficerName(prefix + admUser.getUserName() +" "+ admUser.getUserLastname());
				}
				
				list.add(dto);

			}
		}
		
		return list;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void activate(Object obj, User user) throws Exception {
		if(!(obj instanceof DeactivateLicenseDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		DeactivateLicenseDTO param = (DeactivateLicenseDTO)obj;
		DeactivateLicense deactivateLicense = (DeactivateLicense)this.deactivateLicenseDAO.findByPrimaryKey(param.getDeactiveId());
		
		List<Guarantee> listGuarantee = (List<Guarantee>)this.guaranteeDAO.findByTrader(deactivateLicense.getLicenseNo(), deactivateLicense.getTraderType(), 0);
		
		if(!listGuarantee.isEmpty())
		{
			Guarantee guarantee = listGuarantee.get(0);
			
			if(GuaranteeStatus.REFUND.getStatus().equals(guarantee.getGuaranteeStatus()))
			{
				throw new Exception("ไม่สามารถยกเลิกได้ จะต้องยกเลิกการคืนหลักประกันก่อน");
			}
		}
		
		ObjectUtil.copy(param, deactivateLicense);
		
//		DateFormat df = DateUtils.getProcessDateFormatThai();
		
//		Date bookDate = df.parse(param.getBookDate());
//		deactivateLicense.setBookDate(bookDate);
		
	
		deactivateLicense.setApproveStatus(DeactivateStatus.CANCEL.getStatus());
		deactivateLicense.setLastUpdUser(user.getUserName());
		deactivateLicense.setLastUpdDtm(new Date());
		
		this.deactivateLicenseDAO.update(deactivateLicense);

		RegistrationDTO regDTO = new RegistrationDTO();

		regDTO.setLicenseNo(deactivateLicense.getLicenseNo());
		regDTO.setTraderType(deactivateLicense.getTraderType());
		
		// deActivate all trader
		this.traderService.activate(regDTO, user);
		
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void deActivate(Object obj,Object[] object, User user) throws Exception {
		if(!(obj instanceof DeactivateLicenseDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		DeactivateLicenseDTO param = (DeactivateLicenseDTO)obj;
		DeactivateLicense deactivateLicense = (DeactivateLicense)this.deactivateLicenseDAO.findByPrimaryKey(param.getDeactiveId());
		
		ObjectUtil.copy(param, deactivateLicense);
		
		DateFormat df = DateUtils.getProcessDateFormatThai();
		
		Date bookDate = df.parse(param.getBookDate());
		deactivateLicense.setBookDate(bookDate);
		
		
		deactivateLicense.setApproveStatus(DeactivateStatus.ACCEPT.getStatus());
		deactivateLicense.setLastUpdUser(user.getUserName());
		deactivateLicense.setLastUpdDtm(new Date());
		
		this.deactivateLicenseDAO.update(deactivateLicense);
		
		this.addDeactivateDetail(object, deactivateLicense, user);

		RegistrationDTO regDTO = new RegistrationDTO();

		regDTO.setLicenseNo(deactivateLicense.getLicenseNo());
		regDTO.setTraderType(deactivateLicense.getTraderType());
		regDTO.setPunishmentDate(deactivateLicense.getBookDate());
		
		// deActivate all trader
		this.traderService.deActivate(regDTO, user);
		
	}

}
