package sss.dot.tourism.service.deactivate;

import java.util.List;

import com.sss.aut.service.User;

public interface IDeactivateLicenseService {
	
	public List getDeactivateTypeAll(Object obj, User user)throws Exception;
	
	public Object getTraderPrepareDeactivate(Object obj, User user) throws Exception;
	
	public void saveDeactivation(Object deactivate,Object[] object, User user)throws Exception;
	
	public List getAll(Object obj, User user)throws Exception;
	
	public List countAll(Object obj, User user) throws Exception;
	
	public void deActivate(Object deactivate,Object[] object, User user) throws Exception;
		
	public void activate(Object object, User user)throws Exception;
}
