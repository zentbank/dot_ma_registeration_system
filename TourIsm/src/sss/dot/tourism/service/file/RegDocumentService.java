package sss.dot.tourism.service.file;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.file.RegDocumentDAO;
import sss.dot.tourism.domain.MasDocument;
import sss.dot.tourism.domain.RegDocument;
import sss.dot.tourism.domain.Registration;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.util.RecordStatus;

import com.sss.aut.service.User;

@Repository("regDocumentService")
public class RegDocumentService {
	
	@Autowired
	RegDocumentDAO regDocumentDAO;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveRegDocument(String fullpath, Trader trader, 	Registration registration, MasDocument document, User user)
			throws Exception {
		
		RegDocument regDocPersis = new RegDocument();
		regDocPersis.setDocPath(fullpath);
		regDocPersis.setTrader(trader);
		regDocPersis.setRegistration(registration);
		regDocPersis.setMasDocument(document);
		
		regDocPersis.setRecordStatus(RecordStatus.NORMAL.getStatus());
		regDocPersis.setCreateUser(user==null?"dummy":user.getUserName());
		regDocPersis.setCreateDtm(new Date());
		this.regDocumentDAO.insert(regDocPersis);
		
	}
}












