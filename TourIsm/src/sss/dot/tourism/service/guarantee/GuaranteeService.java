package sss.dot.tourism.service.guarantee;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.mas.MasBankDAO;
import sss.dot.tourism.dao.registration.GuaranteeDAO;
import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.domain.Guarantee;
import sss.dot.tourism.domain.MasBank;
import sss.dot.tourism.dto.registration.GuaranteeDTO;
import sss.dot.tourism.service.registration.IAccountService;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.GuaranteeStatus;
import sss.dot.tourism.util.LicenseStatus;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.TraderCategory;

import com.sss.aut.service.User;

@Repository("guaranteeService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class GuaranteeService implements IGuaranteeService{

	@Autowired
	GuaranteeDAO guaranteeDAO;
	@Autowired
	TraderDAO traderDAO;
	@Autowired
	MasBankDAO masBankDAO;
	@Autowired
	IAccountService accountService;
	
	public List countAll(Object obj, User user , boolean isRefund) throws Exception
	{
		if(!(obj instanceof GuaranteeDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
	
		GuaranteeDTO param = (GuaranteeDTO)obj;
		param.setOrgId(user.getUserData().getOrgId());
		return this.guaranteeDAO.findGuaranteePaging(param, param.getStart(), param.getLimit(), isRefund);
	}
	
	public List getAll(Object obj, User user, boolean isRefund) throws Exception {

		if(!(obj instanceof GuaranteeDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		List<GuaranteeDTO> list = new ArrayList<GuaranteeDTO>();
		DateFormat df = DateUtils.getProcessDateFormatThai();
		
		GuaranteeDTO param = (GuaranteeDTO)obj;
		param.setOrgId(user.getUserData().getOrgId());
		List<Object[]> listAll =  this.guaranteeDAO.findGuaranteePaging(param, param.getStart(), param.getLimit() ,isRefund);
		
		if(!listAll.isEmpty())
		{
			for(Object[] guar: listAll)
			{
				GuaranteeDTO dto = new GuaranteeDTO();
				
//			    sqlQuery.addScalar("TRADER_ID", Hibernate.LONG); //0
				dto.setTraderId(Long.valueOf(guar[0].toString()));
//			    sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING); //1
			    dto.setTraderName(guar[1]==null?"":guar[1].toString());
//			    sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING); //2
			    dto.setLicenseNo(guar[2]==null?"":guar[2].toString());
//			    sqlQuery.addScalar("LICENSE_STATUS", Hibernate.STRING);//3
			    dto.setLicenseStatus(guar[3]==null?"":guar[3].toString());
			    dto.setLicenseStatusName(LicenseStatus.getMeaning(dto.getLicenseStatus()));
			    
//			    sqlQuery.addScalar("TRADER_TYPE", Hibernate.STRING);//22
			    dto.setTraderType(guar[22]==null?"":guar[22].toString());
			    
//			    sqlQuery.addScalar("TRADER_CATEGORY", Hibernate.STRING);//4
			    dto.setTraderCategory(guar[4]==null?"":guar[4].toString());
			    dto.setTraderCategoryName(TraderCategory.getMeaning(dto.getTraderType(), dto.getTraderCategory()));
			    
//			    sqlQuery.addScalar("GUARANTEE_ID", Hibernate.LONG);//5
			    dto.setGuaranteeId(Long.valueOf(guar[5].toString()));
//			    sqlQuery.addScalar("GUARANTEE_STATUS", Hibernate.STRING);//6
			    dto.setGuaranteeStatus(guar[6]==null?"":guar[6].toString());
			    dto.setGuaranteeStatusName(GuaranteeStatus.getMeaning(dto.getGuaranteeStatus()));

//			    sqlQuery.addScalar("GUARANTEE_MNY", Hibernate.BIG_DECIMAL);//14
			    dto.setGuaranteeMny(new BigDecimal(guar[14]==null?"0":guar[14].toString()));
//			    sqlQuery.addScalar("GUARANTEE_INTEREST", Hibernate.BIG_DECIMAL);//15
			    dto.setGuaranteeInterest(new BigDecimal(guar[15]==null?"0":guar[15].toString()));
			    
			    
//			    sqlQuery.addScalar("REFUND_NAME", Hibernate.STRING);//16
			    dto.setRefundName(guar[16]==null?"":guar[16].toString());
			    
//			    sqlQuery.addScalar("REFUND_TYPE", Hibernate.STRING);//17
			    dto.setRefundType(guar[17]==null?"":guar[17].toString());
			    
//			    sqlQuery.addScalar("REFUND_MNY", Hibernate.BIG_DECIMAL);//18
			    if(guar[18]!=null)
			    {
			    	dto.setRefundMny(new BigDecimal(guar[18]==null?"0":guar[18].toString()));
			    }
			    else
			    {
			    	dto.setRefundMny(dto.getGuaranteeMny());
			    }
			    
			    
//			    sqlQuery.addScalar("REFUND_INTEREST", Hibernate.BIG_DECIMAL);//19
			    if(guar[19]!=null)
			    {
			    	dto.setRefundInterest(new BigDecimal(guar[19]==null?"0":guar[19].toString()));
			    }
			    else
			    {
			    	dto.setRefundInterest(dto.getGuaranteeInterest());
			    }
			    
			    
//			    sqlQuery.addScalar("REFUND_DATE", Hibernate.DATE);//20
			    if(guar[20] != null)
			    {
			    	dto.setRefundDate(df.format(guar[20]));
			    }
//			    sqlQuery.addScalar("REFUND_REMARK", Hibernate.STRING);//21
			    dto.setRefundRemark(guar[21]==null?"":guar[21].toString());
			    
			    
				

				
//			    sqlQuery.addScalar("PREFIX_NAME", Hibernate.STRING);//24
				String prefixName = guar[24]==null?"":guar[24].toString();
//				sqlQuery.addScalar("FIRST_NAME", Hibernate.STRING);//25
				String firstName = guar[25]==null?"":guar[25].toString();

//				sqlQuery.addScalar("POSTFIX_NAME", Hibernate.STRING);//27
				String postfixName = guar[27]==null?"":guar[27].toString();
				
				dto.setTraderOwnerName(prefixName+firstName +postfixName);
			    
//			    sqlQuery.addScalar("PERSON_ID", Hibernate.LONG);//23
//			    
//			    sqlQuery.addScalar("LAST_NAME", Hibernate.STRING);//26
//			    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);//28
				
//			    sqlQuery.addScalar("IDENTITY_NO", Hibernate.STRING);//29
			    dto.setIdentityNo(guar[29]==null?"":guar[29].toString());
				
//			    sqlQuery.addScalar("TRADER_NAME_EN", Hibernate.STRING);//30
				dto.setTraderNameEn(guar[30]==null?"":guar[30].toString());
				
				
//			    sqlQuery.addScalar("CASH_ACCOUNT_ID", Hibernate.STRING);//31
				dto.setCashAccountId(guar[31]==null?"":guar[31].toString());
				
//			    sqlQuery.addScalar("CASH_ACCOUNT_NAME", Hibernate.STRING);//32
			    dto.setCashAccountName(guar[32]==null?"":guar[32].toString());
			    
//			    sqlQuery.addScalar("CASH_ACCOUNT_BANK_ID", Hibernate.LONG);//33
			    if(guar[33]!=null)
			    {
			    	MasBank masBank = (MasBank) this.masBankDAO.findByPrimaryKey(Long.valueOf(guar[33].toString()));
			    	dto.setMasBankByCashAccountBankName(masBank.getBankName());
			    	dto.setMasBankByCashAccountBankId(masBank.getMasBankId());
			    }
			    
//			    sqlQuery.addScalar("CASH_ACCOUNT_BANK_BRANCH_NAME", Hibernate.STRING);//34
			    dto.setCashAccountBankBranchName(guar[34]==null?"":guar[34].toString());
			    
//			    sqlQuery.addScalar("CASH_ACCOUNT_MNY", Hibernate.BIG_DECIMAL);//35
			    if(guar[35]!=null)
			    {
			    	 dto.setCashAccountMny(guar[35]==null?new BigDecimal(0): new BigDecimal(guar[35].toString()));
			    }
			   
			    
//			    sqlQuery.addScalar("BANK_GUARANTEE_ID", Hibernate.STRING);//36
			    dto.setBankGuaranteeId(guar[36]==null?"":guar[36].toString());
			    
//			    sqlQuery.addScalar("BANK_GUARANTEE_NAME", Hibernate.STRING);//37
			    dto.setBankGuaranteeName(guar[37]==null?"":guar[37].toString());
			    
//			    sqlQuery.addScalar("BANK_GUARANTEE_BANK_ID", Hibernate.LONG);//38
			    if(guar[38]!=null)
			    {
			    	MasBank masBank = (MasBank) this.masBankDAO.findByPrimaryKey(Long.valueOf(guar[38].toString()));
			    	dto.setMasBankByBankGuaranteeBankName(masBank.getBankName());
			    	dto.setMasBankByBankGuaranteeBankId(masBank.getMasBankId());
			    }
			    
//			    sqlQuery.addScalar("BANK_GUARANTEE_MNY", Hibernate.BIG_DECIMAL);//39
			    dto.setBankGuaranteeMny(guar[39]==null?new BigDecimal(0): new BigDecimal(guar[39].toString()));
			    
//			    sqlQuery.addScalar("GOVERNMENT_BOND_ID", Hibernate.STRING);//40
			    dto.setGovernmentBondId(guar[40]==null?"":guar[40].toString());
			    
//			    sqlQuery.addScalar("GOVERNMENT_BOND_EXPIRE_DATE", Hibernate.STRING);//41
			    if(guar[41] != null)
			    {
			    	dto.setGovernmentBondExpireDate(df.format(guar[41]));
			    }
//			    sqlQuery.addScalar("GOVERNMENT_BOND_MNY", Hibernate.BIG_DECIMAL);//42
			    dto.setGovernmentBondMny(guar[42]==null?new BigDecimal(0): new BigDecimal(guar[42].toString()));
			    
//			    sqlQuery.addScalar("RECEIVE_DATE", Hibernate.BIG_DECIMAL);//43
			    if(guar[43]!=null)
			    {
			    	dto.setReceiveDate(df.format((Date)guar[43]));
			    }
//			    sqlQuery.addScalar("RECEIVE_NAME", Hibernate.BIG_DECIMAL);//44
			    dto.setReceiveName(guar[44]==null?"":guar[44].toString());
			    

			    
			    
			    list.add(dto);
			}
		}
		return list;
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
	public void save(Object obj, User user) throws Exception {
		if(!(obj instanceof GuaranteeDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		GuaranteeDTO param = (GuaranteeDTO)obj;
		
		Guarantee guarantee = (Guarantee) this.guaranteeDAO.findByPrimaryKey(param.getGuaranteeId());
		
		ObjectUtil.copy(param, guarantee);
		
//		guarantee.setGuaranteeStatus(GuaranteeStatus.REFUND.getStatus());
		guarantee.setLastUpdUser(user.getUserName());
		guarantee.setLastUpdDtm(new Date());
		
		this.guaranteeDAO.update(guarantee);
		
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveChangeGuarantee(Object object, User user) throws Exception {
	
		if(!(object instanceof GuaranteeDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		GuaranteeDTO dto = (GuaranteeDTO) object;
		
		Guarantee gua = (Guarantee)this.guaranteeDAO.findByPrimaryKey(dto.getGuaranteeId());
		dto.setLicenseNo(gua.getLicenseNo());
		dto.setTraderType(gua.getTraderType());
		this.accountService.saveGuaranteeAccount(object, user);
		
	}

	
}
