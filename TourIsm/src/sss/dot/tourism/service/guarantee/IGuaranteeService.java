package sss.dot.tourism.service.guarantee;

import java.util.List;

import com.sss.aut.service.User;

public interface IGuaranteeService {
	public List getAll(Object obj, User user , boolean isRefund)throws Exception;
	
	public List countAll(Object obj, User user , boolean isRefund) throws Exception;
	
	public void save(Object object, User user)throws Exception;
	
	public void saveChangeGuarantee(Object object, User user) throws Exception;
	
	
}
