package sss.dot.tourism.service.guidedetail;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.guidedetail.GuideDetailDAO;
import sss.dot.tourism.dao.guidedetail.GuideForeignLanguageDAO;
import sss.dot.tourism.dao.mas.CountryDAO;
import sss.dot.tourism.dao.registration.PersonDAO;
import sss.dot.tourism.dao.registration.TraderAddressDAO;
import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.domain.Country;
import sss.dot.tourism.domain.GuideDetail;
import sss.dot.tourism.domain.GuideForeignLanguage;
import sss.dot.tourism.domain.Person;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.domain.TraderAddress;
import sss.dot.tourism.dto.guidedetail.GuideDetailDTO;
import sss.dot.tourism.dto.guidedetail.GuideForeignLanguageDTO;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.RecordStatus;
import sss.dot.tourism.util.TraderAddressType;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.User;

@Repository("guidedetailService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class GuideDetailService implements IGuideDetailService{

	@Autowired
	GuideDetailDAO guidedetailDAO;
	
	@Autowired
	GuideForeignLanguageDAO guideForeignLanguageDAO;
	
	@Autowired
	CountryDAO countryDAO;
	
	@Autowired
	TraderDAO traderDAO;
	
	@Autowired
	TraderAddressDAO traderAddressDAO;
	
	@Autowired
	PersonDAO personDAO;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void createGuideDetail(Object object, User user) throws Exception 
	{
		System.out.println("class GuideDetailService method createGuideDetail");
		if(!(object instanceof GuideDetailDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถเพิ่มการทำรายการ แบบรายงานตัวมัคคุเทศก์ได้");
		}
				
		GuideDetailDTO params = (GuideDetailDTO)object;
		
		GuideDetail guideDetail = new GuideDetail();
		guideDetail.setCreateUser(user.getUserName());
		guideDetail.setCreateDtm(new Date());
		guideDetail.setRecordStatus(RecordStatus.TEMP.getStatus());
		
		Long guideId = (Long)guidedetailDAO.insert(guideDetail);
		params.setGuideId(guideId);
		
		
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveAllGuideDetail(Object object, User user) throws Exception 
	{
		if(!(object instanceof GuideDetailDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูล แบบรายงานตัวมัคคุเทศก์ได");
		}
		
		try{
			GuideDetailDTO dto = (GuideDetailDTO) object;
			
			this.saveGuideDetail(dto, user);
			
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("ไม่สามารถบันทึกข้อมูล แบบรายงานตัวมัคคุเทศก์ได้");
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveGuideDetail(Object object, User user) throws Exception {
		if(!(object instanceof GuideDetailDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูล แบบรายงานตัวมัคคุเทศก์ได้");
		}
		
		try{
			
			GuideDetailDTO dto =  (GuideDetailDTO)object;
			GuideDetail persis = (GuideDetail)this.guidedetailDAO.findByPrimaryKey(dto.getGuideId());
			
//			dto.setFax("");
//			System.out.println("#####persis.getFax1 = "+persis.getFax());
			ObjectUtil.copy(dto, persis);
			
//			System.out.println("#####dto.getFax = "+dto.getFax());
//			System.out.println("#####persis.getFax2 = "+persis.getFax());
			
			//EducationLevelName
			if(dto.getEducationLevelName() == null || dto.getEducationLevelName().equals(""))
			{
				persis.setEducationLevelName(dto.getEducationLevelNameOth());
			}
		
			//AmateurLevel
			if(dto.getAmateurLevel() != null && dto.getAmateurLevel().equals("A"))
			{
				persis.setAmateurLevelName("ประกอบเป็นอาชีพหลัก");
			}else if(dto.getAmateurLevel() != null && dto.getAmateurLevel().equals("B"))
			{
				persis.setAmateurLevelName("ประกอบอาชีพเป็นบางครั้งบางคราว");
			}else if(dto.getAmateurLevel() != null && dto.getAmateurLevel().equals("C"))
			{
				persis.setAmateurLevelName("ไม่ได้ประกอบอาชีพเป็นมัคคุเทศก์เลย");
			}
			
			//characterTypeNameOth
			if(dto.getCharacterTypeName() == null || dto.getCharacterTypeName().equals(""))
			{
				persis.setCharacterTypeName(dto.getCharacterTypeNameOth());
			}
			
			//clubName
			if(dto.getClubName() != null && dto.getClubName().equals("เป็น"))
			{
				persis.setClubName(dto.getClubNameTxt());
			}
			
			
			persis.setRecordStatus(RecordStatus.NORMAL.getStatus());
			persis.setLastUpdUser(user.getUserName());
			persis.setLastUpdDtm(new Date());
			
			guidedetailDAO.update(persis);

			
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List create(Object[] object, User user) throws Exception {
		
		List<GuideForeignLanguageDTO> list = new ArrayList<GuideForeignLanguageDTO>();
		for (Object obj : object) 
		{
			GuideForeignLanguageDTO dto = (GuideForeignLanguageDTO) obj;
			
			GuideForeignLanguage guideForeignLanguage = new GuideForeignLanguage();
			
			ObjectUtil.copy(dto, guideForeignLanguage);
			
			if(dto.getGuideId() > 0)
			{
				GuideDetail guideDetail = new GuideDetail();
				guideDetail.setGuideId(dto.getGuideId());;
				guideForeignLanguage.setGuideDetail(guideDetail);
			}
			
			if(dto.getCountryId() > 0)
			{
				Country country = new Country();
				country.setCountryId(dto.getCountryId());
				guideForeignLanguage.setCountry(country);
				
				Country countryPersis = (Country)this.countryDAO.findByPrimaryKey(dto.getCountryId());
				guideForeignLanguage.setLanguage(countryPersis.getLanguage());
			}
			
				
			guideForeignLanguage.setRecordStatus(RecordStatus.NORMAL.getStatus());
			guideForeignLanguage.setCreateUser(user.getUserName());
			guideForeignLanguage.setCreateDtm(new Date());
			
			Long foreignId = (Long)this.guideForeignLanguageDAO.insert(guideForeignLanguage);
			
			dto.setForeignId(foreignId);
			
			list.add(dto);
		}
		return list;
	}

	public List getAll(Object object, User user) throws Exception 
	{
		if(!(object instanceof GuideForeignLanguageDTO))
		{
			throw new IllegalArgumentException("object not instanceof GuideForeignLanguageDTO");
		}
		GuideForeignLanguageDTO param = (GuideForeignLanguageDTO)object;
		
		List<GuideForeignLanguageDTO> list = new ArrayList<GuideForeignLanguageDTO>();
		
		List<GuideForeignLanguage> listAll = (List<GuideForeignLanguage>)this.guideForeignLanguageDAO.findGuideForeignLanguageAll(param);
		
		if(!listAll.isEmpty())
		{
			for(GuideForeignLanguage guideForeignLanguage: listAll)
			{
				GuideForeignLanguageDTO dto = new GuideForeignLanguageDTO();
				
				ObjectUtil.copy(guideForeignLanguage, dto);
				ObjectUtil.copy(guideForeignLanguage.getCountry(), dto);
				
//				dto.setCountryName(guideForeignLanguage.getCountry().getCountryName());
				
//				if(guideForeignLanguage.getGuideDetail() != null)
//				{
					dto.setGuideId(guideForeignLanguage.getGuideDetail().getGuideId());
//				}
				
				list.add(dto);
			}
		}
		return list;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void update(Object[] object, User user) throws Exception 
	{
		List<GuideForeignLanguageDTO> list = new ArrayList<GuideForeignLanguageDTO>();
		for (Object obj : object) {
			GuideForeignLanguageDTO dto = (GuideForeignLanguageDTO) obj;
			
			GuideForeignLanguage guideForeignLanguage = (GuideForeignLanguage)this.guideForeignLanguageDAO.findByPrimaryKey(dto.getForeignId());
			
			ObjectUtil.copy(dto, guideForeignLanguage);
			
			if(dto.getGuideId() > 0)
			{
				GuideDetail guideDetail = new GuideDetail();
				guideDetail.setGuideId(dto.getGuideId());;
				guideForeignLanguage.setGuideDetail(guideDetail);
			}
			
			if(dto.getCountryId() > 0)
			{
				Country country = new Country();
				country.setCountryId(dto.getCountryId());
				guideForeignLanguage.setCountry(country);
				
				Country countryPersis = (Country)this.countryDAO.findByPrimaryKey(dto.getCountryId());
				guideForeignLanguage.setLanguage(countryPersis.getLanguage());
			}
			
			guideForeignLanguage.setLastUpdUser(user.getUserName());
			
			this.guideForeignLanguageDAO.update(guideForeignLanguage);
			
			list.add(dto);
		}
		
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(Object[] object, User user) throws Exception 
	{
		for (Object obj : object) {
			GuideForeignLanguageDTO dto = (GuideForeignLanguageDTO) obj;
			GuideForeignLanguage addr = (GuideForeignLanguage)guideForeignLanguageDAO.findByPrimaryKey(dto.getForeignId());
			guideForeignLanguageDAO.delete(addr);
			
		}
		
	}

	public List countGuideDetail(Object object, User user, int start, int limit)throws Exception 
	{
		if(!(object instanceof GuideDetailDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถโหลดข้อมูล แบบรายงานตัวมัคคุเทศก์ได้");
		}
		
		GuideDetailDTO params = (GuideDetailDTO)object;
		
		List<Object[]> listObj = this.guidedetailDAO.findGuideDetailPaging(params, user , start, limit);
		
		return listObj;
	}

	public List getGuideDetail(Object object, User user, int start, int limit)throws Exception 
	{
		System.out.println("##########Service GuideDetailService method getGuideDetail");
		
		if(!(object instanceof GuideDetailDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถโหลดข้อมูล แบบรายงานตัวมัคคุเทศก์ได้");
		}
		
		List<GuideDetailDTO> list = new ArrayList<GuideDetailDTO>();
		
		GuideDetailDTO params = (GuideDetailDTO)object;
		
		DateFormat ft =  DateUtils.getProcessDateFormatThai();
		
		List<Object[]> listObj = this.guidedetailDAO.findGuideDetailPaging(params, user , start, limit);
		
		System.out.println("#####listObj.size() = "+listObj.size());
		
		if(!listObj.isEmpty())
		{
			for(Object[] sel: listObj)
			{
				GuideDetailDTO dto = new GuideDetailDTO();

				dto.setGuideId(Long.valueOf(sel[0].toString()));
				dto.setPrefixId(sel[1]==null?null:Integer.valueOf(sel[1].toString()));
				dto.setPrefixName(sel[2]==null?"":sel[2].toString());
				dto.setFirstName(sel[3]==null?"":sel[3].toString());
				dto.setLastName(sel[4]==null?"":sel[4].toString());
				dto.setLicenseNo(sel[5]==null?"":sel[5].toString());
				
				dto.setFirstNameEn(sel[6]==null?"":sel[6].toString());
				dto.setLastNameEn(sel[7]==null?"":sel[7].toString());
				
				dto.setBirthDate(sel[8]==null?null:ft.format((Date)sel[8]));
				dto.setAgeYear(sel[9]==null?null:Integer.valueOf(sel[9].toString()));
				dto.setEffectiveDate(sel[10]==null?null:ft.format((Date)sel[10]));
				dto.setExpireDate(sel[11]==null?null:ft.format((Date)sel[11]));
				
				dto.setAddressNo(sel[12]==null?"":sel[12].toString());
				dto.setVillageName(sel[13]==null?"":sel[13].toString());
				dto.setMoo(sel[14]==null?"":sel[14].toString());
				dto.setSoi(sel[15]==null?"":sel[15].toString());
				dto.setRoadName(sel[16]==null?"":sel[16].toString());
				if(sel[17] != null)
					dto.setTambolId(Long.valueOf(sel[17].toString()));
				if(sel[18] != null)
					dto.setAmphurId(Long.valueOf(sel[18].toString()));
				if(sel[19] != null)
					dto.setProvinceId(Long.valueOf(sel[19].toString()));
				dto.setPostCode(sel[20]==null?"":sel[20].toString());
				dto.setTelephone(sel[21]==null?"":sel[21].toString());
				dto.setMobileNo(sel[22]==null?"":sel[22].toString());
				dto.setFax(sel[23]==null?"":sel[23].toString());
				dto.setEmail(sel[24]==null?"":sel[24].toString());
				
				if(sel[25] != null  && !sel[25].equals(""))
				{
					if(!sel[25].toString().equals("ต่ำกว่าปริญญาตรี") && !sel[25].toString().equals("ปริญญาตรี")
							&& !sel[25].toString().equals("ปริญญาโท") && !sel[25].toString().equals("สูงกว่าปริญญาโท"))
					{
						dto.setEducationLevelNameOth(sel[25].toString());
					}
					else
					{
						dto.setEducationLevelName(sel[25].toString());
					}
				}			
				
				dto.setAmateurLevel(sel[26]==null?"":sel[26].toString());
				dto.setAmateurLevelName(sel[27]==null?"":sel[27].toString());
				dto.setExperienceYear(sel[28]==null?"":sel[28].toString());
				dto.setWorkType(sel[29]==null?"":sel[29].toString());
				dto.setWorkTypeName(sel[30]==null?"":sel[30].toString());
				dto.setSalaryName(sel[31]==null?"":sel[31].toString());
				
				if(sel[32] != null  && !sel[32].equals(""))
				{
					if(!sel[32].toString().equals("การนำเที่ยว") && !sel[32].toString().equals("Transfer")
							&& !sel[32].toString().equals("การนำเที่ยว/Transfer"))
					{
						dto.setCharacterTypeNameOth(sel[32].toString());
					}
					else
					{
						dto.setCharacterTypeName(sel[32].toString());
					}
				}		
				
				if(sel[33] != null && !sel[33].equals(""))
				{
					if(sel[33].toString().equals("ไม่เป็น"))
					{
						dto.setClubName(sel[33].toString());
					}
					else
					{
						dto.setClubName("เป็น");
						dto.setClubNameTxt(sel[33].toString());
					}
				}

				dto.setRegisterDtm(sel[34]==null?null:ft.format((Date)sel[34]));				
				
				String fullName = dto.getPrefixName()+dto.getFirstName()+" "+dto.getLastName();
				dto.setFullName(fullName);
				
				list.add(dto);
			}

		}
		
		return list;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void deleteGuideDetail(Object object, User user) throws Exception 
	{
		
		GuideDetailDTO dto = (GuideDetailDTO) object;
		
		GuideForeignLanguageDTO guideForeignLanguageDTO = new GuideForeignLanguageDTO();
		guideForeignLanguageDTO.setGuideId(dto.getGuideId());
		
		//delete GuideForeignLanguage
		List<GuideForeignLanguage> listAll = (List<GuideForeignLanguage>)this.guideForeignLanguageDAO.findGuideForeignLanguageAll(guideForeignLanguageDTO);
		if(!listAll.isEmpty())
		{
			for(GuideForeignLanguage guideForeignLanguagePersis: listAll)
			{
				this.guideForeignLanguageDAO.delete(guideForeignLanguagePersis);
			}
		}
		
		//delete GuideDetail
		GuideDetail guideDetailPersis = (GuideDetail)guidedetailDAO.findByPrimaryKey(dto.getGuideId());
		guidedetailDAO.delete(guideDetailPersis);
		
	}

	public void searchLicenseGuideDetail(Object object, User user)throws Exception 
	{
		System.out.println("class GuideDetailService method searchLicenseGuideDetail");
		if(!(object instanceof GuideDetailDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถค้นหาข้อมูลเลขที่ใบอนุญาตการทำรายการ แบบรายงานตัวมัคคุเทศก์ได้");
		}
				
		DateFormat ft =  DateUtils.getProcessDateFormatThai();
		GuideDetailDTO params = (GuideDetailDTO)object;

		List<Trader> list = (List<Trader>)traderDAO.findTraderByLicenseNo(params.getLicenseNo(), TraderType.GUIDE.getStatus(), RecordStatus.NORMAL.getStatus(), 0);
		
		if(list.size() == 1)
		{
			Trader trader = list.get(0);
			
			Person person = (Person)this.personDAO.findByPrimaryKey(trader.getPerson().getPersonId());
			params.setPrefixId(Integer.parseInt(person.getMasPrefix().getPrefixId()+""));
			params.setFirstName(person.getFirstName());
			params.setLastName(person.getLastName());
			params.setFirstNameEn(person.getFirstNameEn());
			params.setLastNameEn(person.getLastNameEn());
			params.setBirthDate(person.getBirthDate()==null?null:ft.format((Date)person.getBirthDate()));
			params.setAgeYear(person.getAgeYear());
			
			//2
			params.setEffectiveDate(trader.getEffectiveDate()==null?null:ft.format((Date)trader.getEffectiveDate()));
			params.setExpireDate(trader.getExpireDate()==null?null:ft.format((Date)trader.getExpireDate()));
			//
			
			//3 setที่อยู่
			List<TraderAddress> listAddress = (List<TraderAddress>) this.traderAddressDAO.findAllByTrader(trader.getTraderId(), TraderAddressType.OFFICE_ADDRESS.getStatus(), RecordStatus.NORMAL.getStatus());
			if(!listAddress.isEmpty())
			{
				for(TraderAddress address: listAddress)
				{					
					params.setAddressNo(address.getAddressNo());
					params.setMoo(address.getMoo());
					params.setRoadName(address.getRoadName());
					params.setAmphurId(address.getMasAmphur().getAmphurId());
					params.setPostCode(address.getPostCode());
					params.setMobileNo(address.getMobileNo());
					params.setEmail(address.getEmail());
					
					params.setVillageName(address.getVillageName());
					params.setSoi(address.getSoi());
					params.setProvinceId(address.getMasProvince().getProvinceId());
					params.setTambolId(address.getMasTambol().getTambolId());
					params.setTelephone(address.getTelephone());
					params.setFax(address.getFax());
					
				}
			}
			//
		}
		

		
	}

}















