package sss.dot.tourism.service.guidedetail;

import java.util.List;

import com.sss.aut.service.User;

public interface IGuideDetailService {
	public void createGuideDetail(Object object, User user) throws Exception;
	
	public void saveAllGuideDetail(Object object, User user) throws Exception;
	
	public List getAll(Object object, User user) throws Exception;
	public List create(Object[] object, User user) throws Exception;
	public void update(Object[] object, User user) throws Exception;
	public void delete(Object[] object, User user) throws Exception;
	
	public List countGuideDetail(Object object, User user ,int start, int limit) throws Exception;
	public List getGuideDetail(Object object, User user ,int start, int limit) throws Exception;
	
	public void deleteGuideDetail(Object object, User user) throws Exception;
	
	public void searchLicenseGuideDetail(Object object, User user) throws Exception;
}
