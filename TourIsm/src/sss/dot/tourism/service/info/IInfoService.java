package sss.dot.tourism.service.info;

import java.util.List;

import org.springframework.ui.ModelMap;

import sss.dot.tourism.dto.registration.RegistrationDTO;

import com.sss.aut.service.User;

public interface IInfoService {
	public List getInfoPaging(Object object, User user ,int start, int limit) throws Exception;
	public List getInfoCheckDueDateFeeByPaging(Object object, User user, int start, int limit)throws Exception;
	
	public List countInfoPaging(Object object, User user ,int start, int limit) throws Exception;
	public List infoCheckDueDateFeeByPaging(Object object, User user ,int start, int limit) throws Exception;
	
	public List getReoeganizePaging(Object object, User user ,int start, int limit) throws Exception;
	public List countReoeganizePaging(Object object, User user ,int start, int limit) throws Exception;
	
	
	public void saveReorganizeLicense(Object object, User user ) throws Exception;
	
	//Oat Add 29/09/57
	public String printLicenseBusinessInfoDocx(ModelMap model, Object obj, User user)throws Exception ;
	//
	
	//Oat Add 16/02/58
	public List getInfoAccountWillCancleByPaging(Object object, User user, int start, int limit)throws Exception;
	//
}








