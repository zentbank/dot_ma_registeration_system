package sss.dot.tourism.service.info;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;

import sss.dot.tourism.dao.AdmUserDAO;
import sss.dot.tourism.dao.AdmUserGroupDAO;
import sss.dot.tourism.dao.deactivate.DeactivateDetailDAO;
import sss.dot.tourism.dao.file.RegDocumentDAO;
import sss.dot.tourism.dao.info.InfoDAO;
import sss.dot.tourism.dao.mas.CountryDAO;
import sss.dot.tourism.dao.mas.ForeignLanguageDAO;
import sss.dot.tourism.dao.mas.MasDocumentDAO;
import sss.dot.tourism.dao.mas.MasPosfixDAO;
import sss.dot.tourism.dao.mas.MasPrefixDAO;
import sss.dot.tourism.dao.mas.MasRunningNoDAO;
import sss.dot.tourism.dao.mas.RegisterFlowDAO;
import sss.dot.tourism.dao.registration.BranchDAO;
import sss.dot.tourism.dao.registration.CommitteeDAO;
import sss.dot.tourism.dao.registration.DocumentRegistrationDAO;
import sss.dot.tourism.dao.registration.GuaranteeDAO;
import sss.dot.tourism.dao.registration.PersonDAO;
import sss.dot.tourism.dao.registration.PlantripDAO;
import sss.dot.tourism.dao.registration.PrintingCardDAO;
import sss.dot.tourism.dao.registration.PrintingLicenseDAO;
import sss.dot.tourism.dao.registration.RegisterProgressDAO;
import sss.dot.tourism.dao.registration.RegistrationDAO;
import sss.dot.tourism.dao.registration.TraderAddressDAO;
import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.dao.training.EducationDAO;
import sss.dot.tourism.domain.Committee;
import sss.dot.tourism.domain.Country;
import sss.dot.tourism.domain.DeactivateDetail;
import sss.dot.tourism.domain.Education;
import sss.dot.tourism.domain.ForeignLanguage;
import sss.dot.tourism.domain.Guarantee;
import sss.dot.tourism.domain.MasAmphur;
import sss.dot.tourism.domain.MasPosfix;
import sss.dot.tourism.domain.MasPrefix;
import sss.dot.tourism.domain.MasProvince;
import sss.dot.tourism.domain.Person;
import sss.dot.tourism.domain.Plantrip;
import sss.dot.tourism.domain.RegisterProgress;
import sss.dot.tourism.domain.Registration;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.domain.TraderAddress;
import sss.dot.tourism.dto.deactivate.DeactivateLicenseDTO;
import sss.dot.tourism.dto.registration.BranchDTO;
import sss.dot.tourism.dto.registration.CommitteeDTO;
import sss.dot.tourism.dto.registration.GuaranteeDTO;
import sss.dot.tourism.dto.registration.PlantripDTO;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.dto.registration.TraderAddressDTO;
import sss.dot.tourism.service.deactivate.IDeactivateLicenseService;
import sss.dot.tourism.service.file.RegDocumentService;
import sss.dot.tourism.service.registration.BranchService;
import sss.dot.tourism.service.registration.IBranchService;
import sss.dot.tourism.service.registration.ICommitteeService;
import sss.dot.tourism.service.registration.ITraderAddressService;
import sss.dot.tourism.service.trader.ITourCompaniesService;
import sss.dot.tourism.service.trader.ITourLeaderService;
import sss.dot.tourism.service.trader.ITraderGuideService;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.EducationType;
import sss.dot.tourism.util.LicenseStatus;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.PersonType;
import sss.dot.tourism.util.ProgressStatus;
import sss.dot.tourism.util.RecordStatus;
import sss.dot.tourism.util.RegistrationType;
import sss.dot.tourism.util.RoleStatus;
import sss.dot.tourism.util.TraderCategory;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.User;

@Repository("infoService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
public class InfoService implements IInfoService{

	@Autowired
	MasRunningNoDAO masRunningNoDAO;
	
	@Autowired
	PersonDAO personDAO;
	
	@Autowired
	PlantripDAO plantripDAO;
	
	@Autowired
	RegisterFlowDAO registerFlowDAO;
	
	
	@Autowired
	CommitteeDAO committeeDAO;
	@Autowired
	BranchDAO branchDAO;
	@Autowired
	EducationDAO educationDAO;
	@Autowired
	ForeignLanguageDAO foreignLanguageDAO;
	
	@Autowired
	PrintingLicenseDAO printingLicenseDAO;
	@Autowired
	PrintingCardDAO printingCardDAO;
	
	@Autowired
	AdmUserDAO admUserDAO;
	@Autowired
	AdmUserGroupDAO admUserGroupDAO;	
	
	@Autowired
	DocumentRegistrationDAO documentRegistrationDAO;
	@Autowired
	TraderAddressDAO traderAddressDAO;
	@Autowired
	RegistrationDAO registrationDAO;
	@Autowired
	ITourCompaniesService tourCompaniesService;
	@Autowired
	ITourLeaderService tourLeaderService;
	@Autowired
	ITraderGuideService guideService;
	@Autowired
	RegisterProgressDAO registerProgressDAO;
	@Autowired
	TraderDAO traderDAO;
	@Autowired
	MasDocumentDAO masDocumentDAO;
	@Autowired
	RegDocumentService regDocumentService;
	@Autowired
	RegDocumentDAO regDocumentDAO;
	@Autowired
	InfoDAO infoDAO;
	
	@Autowired
	IDeactivateLicenseService deactivateLicenseService;
	
	@Autowired
	DeactivateDetailDAO deactivateDetailDAO;
	
	
	@Autowired
	GuaranteeDAO guaranteeDAO;
	
	//Oat Add 02/10/57
	@Autowired
	MasPrefixDAO masPrefixDAO;
	
	@Autowired
	MasPosfixDAO masPosfixDAO;
	
	@Autowired
	ICommitteeService committeeService;
	
	@Autowired
	ITraderAddressService traderAddressService;
	
	@Autowired
	CountryDAO countryDAO;
	
	@Autowired
	IBranchService branchService;
	//
	

	public List getInfoPaging(Object object, User user, int start, int limit)
			throws Exception {
		
		return this.getLicense((RegistrationDTO)object, user, start, limit, false);

	}
	
	//Oat Add 13/11/57
	public List getInfoCheckDueDateFeeByPaging(Object object, User user, int start, int limit)
			throws Exception {
		
		return this.getLicenseCheckDueDateFee((RegistrationDTO)object, user, start, limit, false);

	}
	//

	public List countInfoPaging(Object object, User user, int start, int limit)
			throws Exception {
		
		RegistrationDTO params = (RegistrationDTO)object;
		return this.infoDAO.findInfoPaging(params, user , start, limit, false);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	private List getLicense(RegistrationDTO params , User user, int start, int limit, boolean reorganize) throws Exception
	{
		if(!(params instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถค้นหาข้อมูลการจดทะเบียนได้");
		}
		
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
//		RegistrationDTO params = (RegistrationDTO)object;
		
		DateFormat ft =  DateUtils.getProcessDateFormatThai();
		
		List<Object[]> listObj = this.infoDAO.findInfoPaging(params, user , start, limit ,reorganize);
		
		String licenseNoOld = "";
		String traderNameNew = "";
		String traderOwnerNameNew = "";
		String traderCategoryNew = "";
		
		if(!listObj.isEmpty())
		{
			for(Object[] sel: listObj)
			{
				
				
				
				RegistrationDTO dto = new RegistrationDTO();
				
				dto.setRegId(Long.valueOf(sel[0].toString()));
				
				if(sel[2] != null)
				{
					
					dto.setRegistrationDate(ft.format((Date)sel[2]));
				}
				dto.setRegistrationProgress(sel[3]==null?"":sel[3].toString());
				if(sel[8] != null)
				{
					dto.setTraderType(sel[8]==null?"":sel[8].toString());
					dto.setTraderTypeName(TraderType.getMeaning(dto.getTraderType()));
				}
				
				if(sel[1]==null)
				{
					String registrationNo = masRunningNoDAO.getRegistrationNo(user.getUserData().getOrgId() ,dto.getTraderType(), null);
					dto.setRegistrationNo(registrationNo);
				}
				else
				{
					dto.setRegistrationNo(sel[1]==null?"":sel[1].toString());
				}
				
				if(sel[4]!=null)
				{
					dto.setRegistrationType(sel[4]==null?"":sel[4].toString());
					dto.setRegistrationTypeName(RegistrationType.getMeaning(dto.getTraderType(), dto.getRegistrationType()));
				}
				dto.setOrgId(Long.valueOf(sel[5].toString()));
				dto.setRegRecordStatus(sel[6]==null?"":sel[6].toString());
				
				System.out.println("##########sel[7].toString() = "+sel[7].toString());
				
				dto.setTraderId(Long.valueOf(sel[7].toString()));
				
				Trader trader = (Trader)this.traderDAO.findByPrimaryKey(dto.getTraderId());
				
				ObjectUtil.copy(trader, dto);
				
				
				dto.setTraderName(sel[9]==null?"":sel[9].toString());
				dto.setTraderNameEn(sel[10]==null?"":sel[10].toString());
				dto.setTraderCategory(sel[11]==null?"":sel[11].toString());
				
				
				dto.setTraderCategoryName(
						TraderCategory.getTraderCategory(
							dto.getTraderType()
							, dto.getTraderCategory()
						).getMeaning()
				);
				

				
				dto.setPersonId(Long.valueOf(sel[12].toString()));
				dto.setPersonType(sel[13]==null?"":sel[13].toString());
				dto.setPrefixName(sel[14]==null?"":sel[14].toString());
				dto.setFirstName(sel[15]==null?"":sel[15].toString());
				dto.setLastName(sel[16]==null?"":sel[16].toString());
				dto.setPostfixName(sel[17]==null?"":sel[17].toString());
								
				String traderOwnerName = "";
				if(PersonType.CORPORATE.getStatus().equals(dto.getPersonType()) && TraderType.TOUR_COMPANIES.equals(dto.getTraderType()))
				{
					traderOwnerName = dto.getPrefixName() + dto.getFirstName() + dto.getPostfixName();
					
				}
				else
				{
					traderOwnerName = dto.getPrefixName() + dto.getFirstName() + " " + dto.getLastName();
					
				}
				dto.setTraderOwnerName(traderOwnerName);
				if(sel[18] != null)
				{
					dto.setPrefixId(Long.valueOf(sel[18].toString()));
				}

				
				
				if(sel[19] != null)
				{
					dto.setPostfixId(Long.valueOf(sel[19].toString()));
				}
				
				
				if(sel[20] != null)
				{
					dto.setAmphurId(Long.valueOf(sel[20].toString()));
				}
				if(sel[21] != null)
				{
					dto.setTaxAmphurId(Long.valueOf(sel[21].toString()));
				}
				if(sel[22] != null)
				{
					dto.setProvinceId(Long.valueOf(sel[22].toString()));
				}
				if(sel[23] != null)
				{
					dto.setTaxProvinceId(Long.valueOf(sel[23].toString()));
				}
				
				if(sel[24] != null)
				{
					dto.setTraderGuideId(Long.valueOf(sel[24].toString()));
				}
				

				
				dto.setPronunciationName(sel[25]==null?"":sel[25].toString());
			    dto.setLicenseNo(sel[26]==null?"":sel[26].toString());
			    dto.setPersonType(sel[27]==null?"":sel[27].toString());
			    dto.setIdentityNo(sel[28]==null?"":sel[28].toString());
			    dto.setCommitteeName1(sel[29]==null?"":sel[29].toString());
			    dto.setCommitteeName2(sel[30]==null?"":sel[30].toString());
			    dto.setCommitteeNameSign(sel[31]==null?"":sel[31].toString());
			    dto.setPassportNo(sel[32]==null?"":sel[32].toString());
			    dto.setFirstNameEn(sel[33]==null?"":sel[33].toString());
			    dto.setLastNameEn(sel[34]==null?"":sel[34].toString());
			    dto.setGender(sel[35]==null?"":sel[35].toString());
			    dto.setPersonNationality(sel[36]==null?"":sel[36].toString());


			    if(sel[37] != null)
				{
					
					dto.setBirthDate(ft.format((Date)sel[37]));
				}
			    
			    dto.setAgeYear(sel[38]==null?0:Integer.valueOf(sel[38].toString()));
			    if(sel[39] != null)
				{
					
					dto.setIdentityNoExpire(ft.format((Date)sel[39]));
				}
			   
			    dto.setCorporateType(sel[40]==null?"":sel[40].toString());
			    dto.setTaxIdentityNo(sel[41]==null?"":sel[41].toString());
			    if(sel[42] != null)
				{
					
					dto.setIdentityDate(ft.format((Date)sel[42]));
				}
			    dto.setTraderRecordStatus(sel[43]==null?"":sel[43].toString());
			    
			    dto.setAmphurName(sel[44]==null?"":sel[44].toString());
			    dto.setTaxAmphurName(sel[45]==null?"":sel[45].toString());
			    dto.setProvinceName(sel[46]==null?"":sel[46].toString());
			    dto.setTaxProvinceName(sel[47]==null?"":sel[47].toString());
			    dto.setPrefixNameEn(sel[48]==null?"":sel[48].toString());
				
			    Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(dto.getRegId());
				if(TraderType.TOUR_COMPANIES.getStatus().equals(dto.getTraderType()))
				{
					//plantripPerYear
					tourCompaniesService.getRegistration(reg, dto, user);
				}
				
				if(TraderType.GUIDE.getStatus().equals(dto.getTraderType()))
				{
					guideService.getRegistration(reg, dto, user);
				}
				
				if(TraderType.LEADER.getStatus().equals(dto.getTraderType()))
				{
					tourLeaderService.getRegistration(reg, dto, user);
				}
				
				List<RegisterProgress> listprogress =  registerProgressDAO.findRegisterProgressbyRegistration(reg.getRegId(), ProgressStatus.GET_IT.getStatus());
				
				if(!listprogress.isEmpty())
				{
					RegisterProgress progress = listprogress.get(0);
					
					String authorityName = progress.getAdmUser().getMasPrefix().getPrefixName() + progress.getAdmUser().getUserName() +" "
							+progress.getAdmUser().getUserLastname();
					
					dto.setAuthorityName(authorityName);
				}
				else
				{
					dto.setAuthorityName(user.getUserData().getUserFullName());
				}
				

				dto.setApproveDate(sel[49]==null?"":ft.format((Date)sel[49]));
				dto.setEffectiveDate(sel[50]==null?"":ft.format((Date)sel[50]));
				dto.setExpireDate(sel[51]==null?"":ft.format((Date)sel[51]));
				
				if(sel[52]!=null)
				{
					if(RecordStatus.NORMAL.getStatus().equals(dto.getTraderRecordStatus()))
					{
						if(LicenseStatus.CANCEL.getStatus().equals(sel[52].toString()))
						{
							//ถาเปน ยกเลิกใบอนุญาต ให้ใบอนุญาตปัจจุบัน กลายเปน ประวัติและเพิ่มอีก record ให้เปนการยกเลิกใบอนญาต 
							dto.setLicenseStatus(RecordStatus.HISTORY.getStatus());
							dto.setLicenseStatusName(LicenseStatus.getMeaning(RecordStatus.HISTORY.getStatus()));
							
						}
						else
						{
							dto.setLicenseStatus(sel[52].toString());
							dto.setLicenseStatusName(LicenseStatus.getMeaning(dto.getLicenseStatus()));
						}
						
						
					}
					else
					{
						dto.setLicenseStatus(dto.getTraderRecordStatus());
						dto.setLicenseStatusName(RecordStatus.getMeaning(dto.getTraderRecordStatus()));
					}					
				}

				
				//------
				String licenseNew = sel[26]==null?"":sel[26].toString();
				
//				System.out.println("#####licenseNoOld = "+licenseNoOld+" ,licenseNew = "+licenseNew);
				
				if(!licenseNoOld.equals(licenseNew))
				{
					List<Trader> listTrader = (List<Trader>)this.traderDAO.findTraderByLicenseNo(sel[26]==null?"":sel[26].toString(), params.getTraderType(), RecordStatus.NORMAL.getStatus(), params.getOrgId());
					
					if(listTrader != null && listTrader.size() >0)
					{
						Trader traderPersis = listTrader.get(0);
						if(TraderType.TOUR_COMPANIES.getStatus().equals(traderPersis.getTraderType()))
						{
							traderNameNew = traderPersis.getTraderName();
						}
						else
						{
							String prefixName = "";
							if(null != traderPersis.getPerson().getMasPrefix())
							{
								prefixName = traderPersis.getPerson().getMasPrefix().getPrefixName();
							}
							
							String postfixName = "";
							if(null != traderPersis.getPerson().getMasPosfix())
							{
								postfixName = traderPersis.getPerson().getMasPosfix().getPostfixName();
							}
							if(PersonType.CORPORATE.getStatus().equals(traderPersis.getPerson().getPersonType()))
							{
								traderOwnerNameNew = prefixName + traderPersis.getPerson().getFirstName() + postfixName;
							}
							else
							{
								traderOwnerNameNew = prefixName + traderPersis.getPerson().getFirstName() + " " + traderPersis.getPerson().getLastName();
							}
	
						}
						
						traderCategoryNew = TraderCategory.getTraderCategory(traderPersis.getTraderType(), traderPersis.getTraderCategory()).getMeaning();
						
						
						
						licenseNoOld = licenseNew;
					}
				}
				
				dto.setImageFile(null==sel[53]?"":sel[53].toString());
				
				dto.setHeaderInfo("ใบอนุญาตเลขที่: "+dto.getLicenseNo());
				
				GuaranteeDTO guaranteeDTO = new GuaranteeDTO();
				if(TraderType.TOUR_COMPANIES.getStatus().equals(dto.getTraderType()))
				{
					//get guaranttee
					
					List<Guarantee> listGuarantees = this.guaranteeDAO.findByTrader(dto.getLicenseNo(), dto.getTraderType());
					if(!listGuarantees.isEmpty())
					{
						Guarantee guarantee = listGuarantees.get(0);
						
						ObjectUtil.copy(guarantee, dto);
						
						if(guarantee.getMasBankByBankGuaranteeBankId() != null)
						{
							dto.setMasBankByBankGuaranteeBankName(guarantee.getMasBankByBankGuaranteeBankId().getBankName());
						}
						if(guarantee.getMasBankByCashAccountBankId() != null)
						{
							dto.setMasBankByCashAccountBankName(guarantee.getMasBankByCashAccountBankId().getBankName());
						}
						
					}
				}
				
				
				
				list.add(dto);
				
				if(RecordStatus.NORMAL.getStatus().equals(dto.getTraderRecordStatus()))
				{
				
					if(LicenseStatus.CANCEL.getStatus().equals(sel[52].toString()))
					{
						RegistrationDTO cancelLicenseDTO = new RegistrationDTO();
						ObjectUtil.copy(dto, cancelLicenseDTO);
						
						DeactivateLicenseDTO param = new DeactivateLicenseDTO();
						param.setLicenseNo(dto.getLicenseNo());
						param.setTraderType(dto.getTraderType());
						param.setOrgId(user.getUserData().getOrgId());
						
						param.setStart(0);
						param.setLimit(10);
						
						List<DeactivateLicenseDTO> listDeactivate =  (List<DeactivateLicenseDTO>)this.deactivateLicenseService.getAll(param, user);
						
						if(!listDeactivate.isEmpty())
						{
							DeactivateLicenseDTO deactDto = listDeactivate.get(0);
							cancelLicenseDTO.setRegistrationDate(deactDto.getDeactivateDate());
							cancelLicenseDTO.setApproveDateFrom(deactDto.getBookDate());
							
							ObjectUtil.copy(deactDto, cancelLicenseDTO);
							
							cancelLicenseDTO.setDeactivateBookNo(deactDto.getBookNo());
							cancelLicenseDTO.setDeactivateRemark(deactDto.getRemark());
							
							List<DeactivateDetail> listDeactDetail = this.deactivateDetailDAO.findByDeactivate(deactDto.getDeactiveId());
							
							if(!listDeactDetail.isEmpty())
							{
								DeactivateDetail deactivateDetail = listDeactDetail.get(0);
								cancelLicenseDTO.setDeactivateTypeName(deactivateDetail.getMasDeactivateType()==null?"":deactivateDetail.getMasDeactivateType().getDeactivateName());
							}
						}
						
						cancelLicenseDTO.setRegistrationType(LicenseStatus.CANCEL.getStatus());
						cancelLicenseDTO.setRegistrationTypeName(LicenseStatus.getMeaning(LicenseStatus.CANCEL.getStatus()) + "ใบอนุญาต");
						cancelLicenseDTO.setLicenseStatus(LicenseStatus.CANCEL.getStatus());
						cancelLicenseDTO.setLicenseStatusName(LicenseStatus.getMeaning(LicenseStatus.CANCEL.getStatus()));
						
						
						cancelLicenseDTO.setEffectiveDate("");
						cancelLicenseDTO.setExpireDate("");
						cancelLicenseDTO.setRoleAction("C");
						
						if(guaranteeDTO != null)
						{
							ObjectUtil.copy(guaranteeDTO, cancelLicenseDTO);
						}
						
						list.add(cancelLicenseDTO);
						
					}
				}
			}
		}
		
		return list;
	}


	public List getReoeganizePaging(Object object, User user, int start,
			int limit) throws Exception {
		return this.getLicense((RegistrationDTO)object, user, start, limit, true);
	}


	public List countReoeganizePaging(Object object, User user, int start,
			int limit) throws Exception {
		RegistrationDTO params = (RegistrationDTO)object;
		return this.infoDAO.findInfoPaging(params, user , start, limit, true);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveReorganizeLicense(Object object, User user)
			throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
		try{
			RegistrationDTO dto = (RegistrationDTO) object;
			
//			this.saveLicense(dto, user);
			this.saveLicenseEditReorganize(dto, user);
			
			if(TraderType.TOUR_COMPANIES.getStatus().equals(dto.getTraderType()))
			{
//				tourCompaniesService.saveLicense(dto, user);
				saveLicenseEditReorganizeTour(dto, user);
			}
			
			if(TraderType.GUIDE.getStatus().equals(dto.getTraderType()))
			{
//				guideService.saveLicense(dto, user);
				saveLicenseEditReorganizeGuide(dto, user);
			}
			
			if(TraderType.LEADER.getStatus().equals(dto.getTraderType()))
			{
//				tourLeaderService.saveLicense(dto, user);
				saveLicenseEditReorganizeLeader(dto, user);
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
	}
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveLicense(Object object, User user) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
		try{
			
			RegistrationDTO dto =  (RegistrationDTO)object;
			
			Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(dto.getRegId());
			reg.setLastUpdUser(user.getUserName());
			reg.setLastUpdDtm(new Date());
			
			ObjectUtil.copy(dto, reg);
			reg.setRecordStatus(RecordStatus.NORMAL.getStatus());

			
			
			Trader trader = (Trader)reg.getTrader();
			
		
			
				
			ObjectUtil.copy(dto, trader);
			trader.setLicenseStatus(LicenseStatus.NORMAL.getStatus());
			trader.setRecordStatus(RecordStatus.NORMAL.getStatus());
			
			trader.setLastUpdUser(user.getUserName());
			trader.setLastUpdDtm(new Date());
							
			Person person = trader.getPerson();
			ObjectUtil.copy(dto, person);
			person.setRecordStatus(RecordStatus.NORMAL.getStatus());
			person.setLastUpdUser(user.getUserName());
			person.setLastUpdDtm(new Date());
			
			if(dto.getPrefixId() > 0)
			{
				MasPrefix masPrefix = new MasPrefix();
				masPrefix.setPrefixId(dto.getPrefixId());
				
				person.setMasPrefix(masPrefix);
			}
			
			if(dto.getPostfixId() > 0)
			{
				MasPosfix masPosfix = new MasPosfix();
				masPosfix.setPostfixId(dto.getPostfixId());
				
				person.setMasPosfix(masPosfix);
			}
			else
			{
				person.setMasPosfix(null);
			}
			
			if(dto.getAmphurId() > 0)
			{
				MasAmphur masAmphurByAmphurId = new MasAmphur();
				masAmphurByAmphurId.setAmphurId(dto.getAmphurId());
				
				person.setMasAmphurByAmphurId(masAmphurByAmphurId);
			}
		

			if(dto.getTaxAmphurId() > 0)
			{
				MasAmphur masAmphurByTaxAmphurId = new MasAmphur();
				masAmphurByTaxAmphurId.setAmphurId(dto.getTaxAmphurId());
				person.setMasAmphurByTaxAmphurId(masAmphurByTaxAmphurId);
				
			}
			
			if(dto.getTaxProvinceId() > 0)
			{
				MasProvince masProvinceByTaxProvinceId = new MasProvince();
				masProvinceByTaxProvinceId.setProvinceId(dto.getTaxProvinceId());
				person.setMasProvinceByTaxProvinceId(masProvinceByTaxProvinceId);
			}

			
			if(dto.getProvinceId() > 0)
			{
				MasProvince masProvinceByProvinceId = new MasProvince();
				masProvinceByProvinceId.setProvinceId(dto.getProvinceId());
				person.setMasProvinceByProvinceId(masProvinceByProvinceId);
			}
			
			person.setLastUpdUser(user.getUserName());
			person.setLastUpdDtm(new Date());
			
			personDAO.update(person);
			traderDAO.update(trader);
			registrationDAO.update(reg);
			
			
			//education and traning
			List<Education> listEducation = (List<Education>) this.educationDAO.findEducationByPerson(person.getPersonId(), RecordStatus.TEMP.getStatus(), null);
			if(!listEducation.isEmpty())
			{
				for(Education edu: listEducation)
				{
					edu.setRecordStatus(RecordStatus.NORMAL.getStatus());
					edu.setLastUpdUser(user.getUserName());
					edu.setLastUpdDtm(new Date());
					this.educationDAO.update(edu);
				}
			}
			//ภาษา
			List<ForeignLanguage> listLang = (List<ForeignLanguage>) this.foreignLanguageDAO.findForeignLanguageByPerson(person.getPersonId(), RecordStatus.TEMP.getStatus());
			if(!listLang.isEmpty())
			{
				for(ForeignLanguage lang: listLang)
				{
					lang.setRecordStatus(RecordStatus.NORMAL.getStatus());
					lang.setLastUpdUser(user.getUserName());
					lang.setLastUpdDtm(new Date());
					this.foreignLanguageDAO.update(lang);
				}
			}
			
			
			// trader addres
			List<TraderAddress> listAddress = (List<TraderAddress>) this.traderAddressDAO.findAllByTrader(trader.getTraderId(), null, RecordStatus.TEMP.getStatus());
			if(!listAddress.isEmpty())
			{
				for(TraderAddress address: listAddress)
				{
					address.setRecordStatus(RecordStatus.NORMAL.getStatus());
					address.setLastUpdUser(user.getUserName());
					address.setLastUpdDtm(new Date());
					this.traderAddressDAO.update(address);
				}
			}


			
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
		
	}

	//Oat Add 24/09/57
	public String printLicenseBusinessInfoDocx(ModelMap model, Object obj, User user) throws Exception
	{
		String reportInfoDocxStr="";
		DateFormat ft =  DateUtils.getProcessDateFormatThai();
		int count = 0;
		int countCountry = 0;
		
		RegistrationDTO dto = new RegistrationDTO();
		RegistrationDTO param = (RegistrationDTO)obj;
		
		Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(param.getRegId());
		
//		Trader trader = (Trader)this.traderDAO.findByPrimaryKey(param.getTraderId());
		Trader trader = (Trader)this.traderDAO.findByPrimaryKey(reg.getTrader().getTraderId());
		param.setTraderId(trader.getTraderId());
		
		Person person = (Person)this.personDAO.findByPrimaryKey(param.getPersonId());
		
		//รายละเอียด
		dto.setLicenseNo(trader.getLicenseNo());
		dto.setLicenseStatusName(LicenseStatus.getMeaning(trader.getLicenseStatus()));
		dto.setTraderName(trader.getTraderName());
		dto.setTraderNameEn(trader.getTraderNameEn());
		dto.setPronunciationName(trader.getPronunciationName());
		//
		
//ข้อมูลผู้ประกอบการ
		//บุคคลธรรมดา Individual
		if(param.getPersonType().equals("I"))
		{
			//file word
			reportInfoDocxStr = "printLicenseBusinessIndividualInfoDocx";
			
			//หมายเลขบัตรประชาชน
			dto.setIdentityNo(person.getIdentityNo());
			//ชื่อ(ไทย)
			dto.setTraderOwnerName(person.getMasPrefix().getPrefixName()+person.getFirstName()+" " + person.getLastName());
			//นามสกุล(ไทย)
			dto.setLastName(person.getLastName());
			//ชื่อ(อังกฤษ)
			dto.setPrefixNameEn(person.getMasPrefix().getPrefixNameEn());
			dto.setFirstNameEn(person.getFirstNameEn());
			//นามสกุล(อังกฤษ)
			dto.setLastNameEn(person.getLastNameEn());
			//เกิดวันที่
			if(person.getBirthDate() != null && !person.getBirthDate().equals(""))
				dto.setBirthDate(ft.format((Date)person.getBirthDate()));
			else
				dto.setBirthDate("");
			//สัญชาติ
			dto.setPersonNationality(person.getPersonNationality());
			//เพศ
			if(person.getGender() != null && !person.getGender().equals(""))
				dto.setGenderName(person.getGender().equals("M")?"ชาย":"หญิง");
			else
				dto.setGenderName("");
			//ออกให้ ณ จังหวัด
			dto.setProvinceName(person.getMasProvinceByProvinceId().getProvinceName());
			//เขตพื้นที่
			dto.setAmphurName(person.getMasAmphurByAmphurId().getAmphurName());
			//เมื่อวันที
			dto.setIdentityDate(ft.format((Date)person.getIdentityDate()));
		}
		else//นิติบุคคล Corporation
		{
			//file word
			reportInfoDocxStr = "printLicenseBusinessCorporationInfoDocx";
			
			//ชื่อนิติบุคคล
//			this.masPrefixDAO.findByPrimaryKey(person.getMasPrefix().getPrefixName());
			//Oat Edit 24/07/58
//			dto.setTraderOwnerName(person.getMasPrefix().getPrefixName()+person.getFirstName()+person.getMasPosfix().getPostfixName());
			dto.setTraderOwnerName(person.getMasPrefix().getPrefixName()+person.getFirstName()+(person.getMasPosfix()==null?"":person.getMasPosfix().getPostfixName()) );
			//
			
			//ทะเบียนนิติบุคคลเลขที่
			dto.setIdentityNoCorp(person.getIdentityNo());
			//ประเภทนิติบุคคล
			if(person.getCorporateType() == "1")
			{
				dto.setCorporateType("บริษัทจำกัด");
			}else if(person.getCorporateType() == "2")
			{
				dto.setCorporateType("ห้างหุ้นส่วนจำกัด");
			}else
			{
				dto.setCorporateType("ไม่ระบุ");
			}
			//โดย
			dto.setCommitteeNameSign(person.getCommitteeNameSign());
			//ออกให้ ณ สำนักงานทะเบียน
			dto.setProvinceName(person.getMasProvinceByProvinceId().getProvinceName());
			//เมื่อวันที่
			dto.setIdentityDate(ft.format((Date)person.getIdentityDate()));
			//เลขประจำตัวผู้เสียภาษีอากร
			dto.setTaxIdentityNo(person.getTaxIdentityNo());
			//ออกให้ ณ จังหวัด
			dto.setTaxProvinceName(person.getMasProvinceByTaxProvinceId().getProvinceName());
			//เขตพื้นที่
			dto.setTaxAmphurName(person.getMasAmphurByTaxAmphurId().getAmphurName());			
		}	
		
//หลักประกัน
			//เลขที่นิติบุคคล
			dto.setIdentityNo(person.getIdentityNo());
			//ประเภทธุรกิจนำเที่ยว
			dto.setTraderCategoryName(TraderCategory.getTraderCategory(trader.getTraderType(), trader.getTraderCategory()).getMeaning());
		List<Guarantee> listGuarantees = this.guaranteeDAO.findByTrader(trader.getLicenseNo(), trader.getTraderType());
		if(!listGuarantees.isEmpty())
		{
			Guarantee guarantee = listGuarantees.get(0);
			ObjectUtil.copy(guarantee, dto);
			
			//เงินสด, Cashier Check 1
			//เลขที่บัญชี
			if(guarantee.getCashAccountId() != null)
				dto.setCashAccountId(guarantee.getCashAccountId());
			else
				dto.setCashAccountId("");
			//ชื่อบัญชี
			if(guarantee.getCashAccountName() != null)
				dto.setCashAccountName(guarantee.getCashAccountName());
			else
				dto.setCashAccountName("");
			//ธนาคาร
			if(guarantee.getMasBankByCashAccountBankId() != null)
				dto.setMasBankByCashAccountBankName(guarantee.getMasBankByCashAccountBankId().getBankName());
			else
				dto.setMasBankByCashAccountBankName("");
			//สาขา
			if(guarantee.getCashAccountBankBranchName() != null)
				dto.setCashAccountBankBranchName(guarantee.getCashAccountBankBranchName());
			else
				dto.setCashAccountBankBranchName("");
			//จำนวนเงิน
			if(guarantee.getCashAccountMny() != null)
				dto.setCashAccountMny(guarantee.getCashAccountMny());
			else
				dto.setCashAccountMny(new BigDecimal(0));
			//--
			
			//หนังสือค้ำประกันธนาคาร 2
			//เลขที่หนังสือค้ำประกัน
			if(guarantee.getBankGuaranteeId() != null)
				dto.setBankGuaranteeId(guarantee.getBankGuaranteeId());
			else
				dto.setBankGuaranteeId("");
			//ชื่อบัญชี
			if(guarantee.getBankGuaranteeName() != null)
				dto.setBankGuaranteeName(guarantee.getBankGuaranteeName());
			else
				dto.setBankGuaranteeName("");
			//ธนาคาร
			if(guarantee.getMasBankByBankGuaranteeBankId() != null)
				dto.setMasBankByBankGuaranteeBankName(guarantee.getMasBankByBankGuaranteeBankId().getBankName());
			else
				dto.setMasBankByBankGuaranteeBankName("");
			//จำนวนเงิน
			if(guarantee.getBankGuaranteeMny() != null)
				dto.setBankGuaranteeMny(guarantee.getBankGuaranteeMny());
			else
				dto.setBankGuaranteeMny(new BigDecimal(0));
			//--
			
			//พันธบัตรรัฐบาล
			//เลขที่พันธบัตร
			if(guarantee.getGovernmentBondId() != null)
				dto.setGovernmentBondId(guarantee.getGovernmentBondId());
			else
				dto.setGovernmentBondId("");
			//วันหมดอายุ
			if(guarantee.getGovernmentBondExpireDate() != null)
				dto.setGovernmentBondExpireDate(ft.format((Date)guarantee.getGovernmentBondExpireDate()));
			else
				dto.setGovernmentBondExpireDate("");
			//จำนวนเงิน
			if(guarantee.getGovernmentBondMny() != null)
				dto.setGovernmentBondMny(guarantee.getGovernmentBondMny());
			else
				dto.setGovernmentBondMny(new BigDecimal(0));
			//--
			
			//จำนวนเงินหลักประกันรวม
			if(guarantee.getGuaranteeMny() != null)
				dto.setGuaranteeMny(guarantee.getGuaranteeMny());
			else
				dto.setGuaranteeMny(new BigDecimal(0));
			//ดอกเบี้ย
			if(guarantee.getGuaranteeInterest() != null)
				dto.setGuaranteeInterest(guarantee.getGuaranteeInterest());
			else
				dto.setGuaranteeInterest(new BigDecimal(0));
			//---
			
			//ชื่อผู้รับหลักประกันคืน
			if(guarantee.getRefundName() != null)
				dto.setRefundName(guarantee.getRefundName());
			else
				dto.setRefundName("");
			//สถานะผู้รับคืน
		if(guarantee.getRefundType() != null && guarantee.getRefundType().equals("T"))
				dto.setRefundType("ผู้รับมอบอำนาจ");
			else
				dto.setRefundType("กรรมการบริษัท");
			//จำนวนเงินคืน
			if(guarantee.getRefundMny() != null)
				dto.setRefundMny(guarantee.getRefundMny());
			else
				dto.setRefundMny(new BigDecimal(0));
			//ดอกผล 
			if(guarantee.getRefundInterest() != null)
				dto.setRefundInterest(guarantee.getRefundInterest());
			else
				dto.setRefundInterest(new BigDecimal(0));
			//วันที่รับหลักประกันคืน
			if(guarantee.getRefundDate() != null && !guarantee.getRefundDate().equals(""))
				dto.setRefundDate(ft.format((Date)guarantee.getRefundDate()));
			else
				dto.setRefundDate("");
			//สถานะหลักประกัน
			if(guarantee.getGuaranteeStatus() != null && guarantee.getGuaranteeStatus().equals("R"))
				dto.setGuaranteeStatus("คืนหลักประกัน");
			else
				dto.setGuaranteeStatus("ยังไม่มารับหลักประกันคืน");
			//หมายเหตุ
			if(guarantee.getRefundRemark() != null)
				dto.setRefundRemark(guarantee.getRefundRemark());
			else
				dto.setRefundRemark("");
		}
		//
		
//ที่ตั้งสำนักงาน
		TraderAddressDTO traderAddressDTO = new TraderAddressDTO();
		traderAddressDTO.setTraderId(param.getTraderId());
		List<TraderAddressDTO>  listTraderAddress = traderAddressService.getAll(traderAddressDTO, null);
		
		System.out.println("listTraderAddress.size() = "+listTraderAddress.size());
		List<TraderAddressDTO> listTraderAddress2 = new ArrayList<TraderAddressDTO>();
		
		for(TraderAddressDTO traderAddressDTO2 : listTraderAddress)
		{
			String traderName = "";
			if(traderAddressDTO2.getAddressType() != null && traderAddressDTO2.getAddressType().equals("S"))
			{
				traderName = "ภูมิลำเนาเฉพาะการ";
				if(traderAddressDTO2.getTraderType() != null && traderAddressDTO2.getTraderType().equals("B"))
				{
					traderName = "ที่ตั้งเฉพาะการ";
				}
			}
			
			if(traderAddressDTO2.getAddressType() != null && traderAddressDTO2.getAddressType().equals("O"))
			{
				traderName = "ที่อยู่ตามทะเบียนบ้าน";
				if(traderAddressDTO2.getTraderType() != null && traderAddressDTO2.getTraderType().equals("B"))
				{
					traderName = "ที่ตั้งสำนักงาน";
				}
			} 
			
			traderAddressDTO2.setTraderName(traderName);
			listTraderAddress2.add(traderAddressDTO2);
			
		}
		
		model.addAttribute("listTraderAddress",listTraderAddress2);
			
//รายชื่อกรรมการ
		CommitteeDTO CParam = new CommitteeDTO();
		CParam.setPersonId(param.getPersonId());
		List<CommitteeDTO> list = (List<CommitteeDTO>)this.committeeService.getAll(CParam, null);
		
		List<CommitteeDTO> listC = new ArrayList<CommitteeDTO>();
		
		for(CommitteeDTO committeeDTO: list)
		{
			CommitteeDTO Cdto = new CommitteeDTO();
			
			//ลำดับที่
			Cdto.setCount(++count);
			//ชื่อ-นามสกุล
			String committeeName = committeeDTO.getPrefixName()+committeeDTO.getCommitteeName()+" "+committeeDTO.getCommitteeLastname();
			if(committeeDTO.getCommitteePersonType().equals("C"))
			{
				committeeName += committeeName+committeeDTO.getPostfixName();
			}
			Cdto.setCommitteeName(committeeName);
			//ประเภทบุคคล
			String committeePersonTypeName ="";
			if(committeeDTO.getCommitteePersonType().equals("I"))
			{
				committeePersonTypeName = "บุคคลธรรมดา";
			}else if(committeeDTO.getCommitteePersonType().equals("C"))
			{
				committeePersonTypeName = "นิติบุคคล";
			}
			Cdto.setCommitteePersonTypeName(committeePersonTypeName);
			//ประเภทกรรมการ
			String committeeTypeName = "";
			if(committeeDTO.getCommitteeType().equals("C"))
			{
				committeeTypeName = "กรรมการบริษัท";
			}else if(committeeDTO.getCommitteeType().equals("S"))
			{
				committeeTypeName = "กรรมการผู้มีอำนาจลงนาม";
			}
			Cdto.setCommitteeTypeName(committeeTypeName);
			 
			listC.add(Cdto);
		} 
		dto.setCommitteeName2(person.getCommitteeName2());	
		model.addAttribute("listCommittee",listC);
		//End รายชื่อกรรมการ
		
//ประเภทธุรกิจนำเที่ยว
		//ประเภทการทำธุรกิจนำเที่ยว
		if(trader.getTraderCategory().equals("100"))
		{
			dto.setTraderCategoryName("ทั่วไป");
		}else if(trader.getTraderCategory().equals("200"))
		{
			dto.setTraderCategoryName("นำเที่ยวจากต่างประเทศ");
		}else if(trader.getTraderCategory().equals("300"))
		{
			dto.setTraderCategoryName("ในประเทศ");
		}else if(trader.getTraderCategory().equals("400"))
		{
			dto.setTraderCategoryName("เฉพาะพื้นที่");
		}
		//จัดนำเที่ยวจำนวน (ครั้ง/ปี)
		if(TraderType.TOUR_COMPANIES.getStatus().equals(trader.getTraderType()))
		{
			//plantripPerYear
			tourCompaniesService.getRegistration(reg, dto, null);
		}
	
		//ประเทศ
		if(trader.getTraderCategory().equals("100") || trader.getTraderCategory().equals("200"))
		{
			List<PlantripDTO> listP = new ArrayList<PlantripDTO>();
			List<Plantrip> listPlantrip = this.plantripDAO.findPlanTripbyTrader(trader.getTraderId(), trader.getRecordStatus());
			if(!listPlantrip.isEmpty())
			{
				for(Plantrip plantrip : listPlantrip)
				{
					PlantripDTO plantripDTO = new PlantripDTO();
//					ObjectUtil.copy(plantrip, plantripDTO);
					
					if(plantrip.getCountry() != null)
					{
						plantripDTO.setCount(++countCountry);
						plantripDTO.setCountryName(((Country)countryDAO.findByPrimaryKey(plantrip.getCountry().getCountryId())).getCountryName());
					}
					
					listP.add(plantripDTO);
				}
			}
			dto.setCountCountry("นำเที่ยวรวม "+countCountry+" ประเทศ");
			model.addAttribute("listPlantrip",listP);
		}
		else
		{
			dto.setCountCountry("");
		}
		
		//End ประเภทธุรกิจนำเที่ยว
		
//สาขา 
		BranchDTO branchDTO = new BranchDTO();
		branchDTO.setBranchParentId(trader.getTraderId());
		List<BranchDTO> listB = (List<BranchDTO>)branchService.getAll(branchDTO, null);
		
		model.addAttribute("listBranch",listB);
		//End สาขา
		
//ประวัติการจดทะเบียน
		RegistrationDTO registrationDTO = new RegistrationDTO();
		registrationDTO.setTraderType(trader.getTraderType());
		registrationDTO.setLicenseNo(trader.getLicenseNo());
		List<RegistrationDTO> listR = (List<RegistrationDTO>)this.getInfoPaging(registrationDTO, user, 0, 100);
		model.addAttribute("listRegistration",listR);
		//End ประวัติการจดทะเบียน
		
//ถ้าสถานะใบอนุญาต เป็นยกเลิก
		if(RecordStatus.NORMAL.getStatus().equals(trader.getRecordStatus()))
		{
		
			if(LicenseStatus.CANCEL.getStatus().equals(trader.getLicenseStatus()))
			{
				//file word
				reportInfoDocxStr = "printCancleLicenseBusinessInfoDocx";
				
//				RegistrationDTO dto = new RegistrationDTO();
//				ObjectUtil.copy(dto, cancelLicenseDTO); 
				
				DeactivateLicenseDTO deactivateLicenseDTO = new DeactivateLicenseDTO();
				deactivateLicenseDTO.setLicenseNo(trader.getLicenseNo());
				deactivateLicenseDTO.setTraderType(trader.getTraderType());
				deactivateLicenseDTO.setOrgId(user.getUserData().getOrgId());
				
				deactivateLicenseDTO.setStart(0);
				deactivateLicenseDTO.setLimit(10);
				
				List<DeactivateLicenseDTO> listDeactivate =  (List<DeactivateLicenseDTO>)this.deactivateLicenseService.getAll(deactivateLicenseDTO, user);
				
				if(!listDeactivate.isEmpty())
				{
					DeactivateLicenseDTO deactDto = listDeactivate.get(0);
					dto.setRegistrationDate(deactDto.getDeactivateDate());
					dto.setApproveDateFrom(deactDto.getBookDate());
					
					ObjectUtil.copy(deactDto, dto);
					
					dto.setDeactivateBookNo(deactDto.getBookNo());
					dto.setDeactivateRemark(deactDto.getRemark());
					
					List<DeactivateDetail> listDeactDetail = this.deactivateDetailDAO.findByDeactivate(deactDto.getDeactiveId());
					
					if(!listDeactDetail.isEmpty())
					{
						DeactivateDetail deactivateDetail = listDeactDetail.get(0);
						dto.setDeactivateTypeName(deactivateDetail.getMasDeactivateType()==null?"":deactivateDetail.getMasDeactivateType().getDeactivateName());
					}
				}
				
				dto.setRegistrationType(LicenseStatus.CANCEL.getStatus());
				dto.setRegistrationTypeName(LicenseStatus.getMeaning(LicenseStatus.CANCEL.getStatus()) + "ใบอนุญาต");
				dto.setLicenseStatus(LicenseStatus.CANCEL.getStatus());
				dto.setLicenseStatusName(LicenseStatus.getMeaning(LicenseStatus.CANCEL.getStatus()));
				
				
				dto.setEffectiveDate("");
				dto.setExpireDate("");
				dto.setRoleAction("C");
				
				if(dto.getDeactivateNo() == null)
					dto.setDeactivateNo("");
				if(dto.getDeactivateDate() == null)
					dto.setDeactivateDate("");
				if(dto.getOfficerName() == null)
					dto.setOfficerName("");
				if(dto.getDeactivateTypeName() == null)
					dto.setDeactivateTypeName("");
				if(dto.getDeactivateResonRemark() == null)
					dto.setDeactivateResonRemark("");
				if(dto.getBookDate() == null)
					dto.setBookDate("");
				if(dto.getDeactivateBookNo() == null)
					dto.setDeactivateBookNo("");
				
//				if(guaranteeDTO != null)
//				{
//					ObjectUtil.copy(guaranteeDTO, cancelLicenseDTO);
//				}
//				
//				list.add(cancelLicenseDTO);
				
			}
		}
		
		model.addAttribute("registrationDTO",dto);
		System.out.println("###reportInfoDocxStr = "+reportInfoDocxStr);
		
		return reportInfoDocxStr;
	}
	//End Oat Add 24/09/57
	
	//Oat Add 14/11/57
	public List infoCheckDueDateFeeByPaging(Object object, User user, int start, int limit)
			throws Exception {
		
		RegistrationDTO params = (RegistrationDTO)object;
		
		//เช็คเงื่อนไขการค้นหา วันชำระค่าธรรมเนียม
		if(params.getExpireDueDateFeeStatus() != null && params.getExpireDueDateFeeStatus().equals("E1"))
		{
			//ครบกำหนดชำระค่าธรรมเนียม 120 วัน
		    DateFormat df =  DateUtils.getProcessDateFormatThai();
		    String strDateNFrom = df.format(new Date());
		    System.out.println("strDateNFrom = "+strDateNFrom); //12/11/2557
		    
		    Date dateN = df.parse(strDateNFrom);
			dateN.setDate(dateN.getDate()+119);
			String strDateNTo = df.format(dateN);
			System.out.println("strDateNTo = "+strDateNTo); //11/03/2558
			
			params.setExpireDateFrom(strDateNFrom);
			params.setExpireDateTo(strDateNTo);
		}else if(params.getExpireDueDateFeeStatus() != null && params.getExpireDueDateFeeStatus().equals("E2"))
		{
			//ขาดชำระค่าธรรมเนียม 3 เดือน
		    DateFormat df =  DateUtils.getProcessDateFormatThai();
		    String strDateN = df.format(new Date());
		    System.out.println("strDateN = "+strDateN);
		    
		    Date dateNFrom = df.parse(strDateN);
		    dateNFrom.setMonth(dateNFrom.getMonth()-3);
			String strDateNFrom = df.format(dateNFrom);
			System.out.println("strDateNFrom = "+strDateNFrom); //12/08/2557
			
			Date dateNTo = df.parse(strDateN);
			dateNTo.setDate(dateNTo.getDate()-1);
			String strDateNTo = df.format(dateNTo);
			System.out.println("strDateNTo = "+strDateNTo);	//11/11/2557
			
			params.setExpireDateFrom(strDateNFrom); 
			params.setExpireDateTo(strDateNTo);
		}else if(params.getExpireDueDateFeeStatus() != null && params.getExpireDueDateFeeStatus().equals("E3"))
		{
			//ขาดชำระค่าธรรมเนียม 9 เดือน
		    DateFormat df =  DateUtils.getProcessDateFormatThai();
		    String strDateN = df.format(new Date());
		    System.out.println("strDateN = "+strDateN);
		    
		    Date dateNFrom = df.parse(strDateN);
		    dateNFrom.setMonth(dateNFrom.getMonth()-9);
			String strDateNFrom = df.format(dateNFrom);
			System.out.println("strDateNFrom = "+strDateNFrom); //12/02/2557
			
			Date dateNTo = df.parse(strDateN);
			dateNTo.setDate(dateNTo.getDate()-1);
			dateNTo.setMonth(dateNTo.getMonth()-3);
			String strDateNTo = df.format(dateNTo);
			System.out.println("strDateNTo = "+strDateNTo);	//11/08/2557
			
			params.setExpireDateFrom(strDateNFrom);
			params.setExpireDateTo(strDateNTo);
		}
//		else
//		{
//			DateFormat df =  DateUtils.getProcessDateFormatThai();
//			String strDateNFrom = df.format(new Date());
//			params.setExpireDateFrom(strDateNFrom);
//			params.setExpireDateTo(strDateNFrom);
//		}
		
//		List<Object[]> listObj = this.infoDAO.findInfoCheckDueDateFeeByPaging(params, user , start, limit ,reorganize);
		
		return this.infoDAO.findInfoCheckDueDateFeeByPaging(params, user , start, limit);
	}
	
	//Oat Add 13/11/57
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	private List getLicenseCheckDueDateFee(RegistrationDTO params , User user, int start, int limit, boolean reorganize) throws Exception
	{
		if(!(params instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถค้นหาข้อมูลการจดทะเบียนได้");
		}
		
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
//		RegistrationDTO params = (RegistrationDTO)object;
		
		DateFormat ft =  DateUtils.getProcessDateFormatThai();
		
		//เช็คเงื่อนไขการค้นหา วันชำระค่าธรรมเนียม
		if(params.getExpireDueDateFeeStatus() != null && params.getExpireDueDateFeeStatus().equals("E1"))
		{
			//ครบกำหนดชำระค่าธรรมเนียม 120 วัน
		    DateFormat df =  DateUtils.getProcessDateFormatThai();
		    String strDateNFrom = df.format(new Date());
		    System.out.println("strDateNFrom = "+strDateNFrom); //12/11/2557
		    
		    Date dateN = df.parse(strDateNFrom);
			dateN.setDate(dateN.getDate()+119);
			String strDateNTo = df.format(dateN);
			System.out.println("strDateNTo = "+strDateNTo); //11/03/2558
			
			params.setExpireDateFrom(strDateNFrom);
			params.setExpireDateTo(strDateNTo);
		}else if(params.getExpireDueDateFeeStatus() != null && params.getExpireDueDateFeeStatus().equals("E2"))
		{
			//ขาดชำระค่าธรรมเนียม 3 เดือน
		    DateFormat df =  DateUtils.getProcessDateFormatThai();
		    String strDateN = df.format(new Date());
		    System.out.println("strDateN = "+strDateN);
		    
		    Date dateNFrom = df.parse(strDateN);
		    dateNFrom.setMonth(dateNFrom.getMonth()-3);
			String strDateNFrom = df.format(dateNFrom);
			System.out.println("strDateNFrom = "+strDateNFrom); //12/08/2557
			
			Date dateNTo = df.parse(strDateN);
			dateNTo.setDate(dateNTo.getDate()-1);
			String strDateNTo = df.format(dateNTo);
			System.out.println("strDateNTo = "+strDateNTo);	//11/11/2557
			
			params.setExpireDateFrom(strDateNFrom);
			params.setExpireDateTo(strDateNTo);
		}else if(params.getExpireDueDateFeeStatus() != null && params.getExpireDueDateFeeStatus().equals("E3"))
		{
			//ขาดชำระค่าธรรมเนียม 9 เดือน
		    DateFormat df =  DateUtils.getProcessDateFormatThai();
		    String strDateN = df.format(new Date());
		    System.out.println("strDateN = "+strDateN);
		    
		    Date dateNFrom = df.parse(strDateN);
		    dateNFrom.setMonth(dateNFrom.getMonth()-9);
			String strDateNFrom = df.format(dateNFrom);
			System.out.println("strDateNFrom = "+strDateNFrom); //12/02/2557
			
			Date dateNTo = df.parse(strDateN);
			dateNTo.setDate(dateNTo.getDate()-1);
			dateNTo.setMonth(dateNTo.getMonth()-3);
			String strDateNTo = df.format(dateNTo);
			System.out.println("strDateNTo = "+strDateNTo);	//11/08/2557
			
			params.setExpireDateFrom(strDateNFrom);
			params.setExpireDateTo(strDateNTo);
		}
//		else
//		{ 
//			DateFormat df =  DateUtils.getProcessDateFormatThai();
//			String strDateNFrom = df.format(new Date());
//			params.setExpireDateFrom(strDateNFrom);
//			params.setExpireDateTo(strDateNFrom);
//		}
		
		List<Object[]> listObj = this.infoDAO.findInfoCheckDueDateFeeByPaging(params, user , start, limit);
		
		String licenseNoOld = "";
		String traderNameNew = "";
		String traderOwnerNameNew = "";
		String traderCategoryNew = "";
		
		if(!listObj.isEmpty())
		{
			for(Object[] sel: listObj)
			{
				RegistrationDTO dto = new RegistrationDTO();
				
				//Oat Add 25/06/58 Address
				dto.setAddress(sel[53]==null?"":sel[53].toString());
				//
				
				dto.setRegId(Long.valueOf(sel[0].toString()));
				
				if(sel[2] != null)
				{
					
					dto.setRegistrationDate(ft.format((Date)sel[2]));
				}
				dto.setRegistrationProgress(sel[3]==null?"":sel[3].toString());
				if(sel[8] != null)
				{
					dto.setTraderType(sel[8]==null?"":sel[8].toString());
					dto.setTraderTypeName(TraderType.getMeaning(dto.getTraderType()));
				}
				
//				if(sel[1]==null)
//				{
//					String registrationNo = masRunningNoDAO.getRegistrationNo(user.getUserData().getOrgId() ,dto.getTraderType(), null);
//					dto.setRegistrationNo(registrationNo);
//				}
//				else
//				{
//					dto.setRegistrationNo(sel[1]==null?"":sel[1].toString());
//				}
				
				if(sel[4]!=null)
				{
					dto.setRegistrationType(sel[4]==null?"":sel[4].toString());
					dto.setRegistrationTypeName(RegistrationType.getMeaning(dto.getTraderType(), dto.getRegistrationType()));
				}
				dto.setOrgId(Long.valueOf(sel[5].toString()));
				dto.setRegRecordStatus(sel[6]==null?"":sel[6].toString());
				
//				System.out.println("##########sel[7].toString() = "+sel[7].toString());
				
				dto.setTraderId(Long.valueOf(sel[7].toString()));
				
//				Trader trader = (Trader)this.traderDAO.findByPrimaryKey(dto.getTraderId());
//				
//				ObjectUtil.copy(trader, dto);
				
				dto.setTraderName(sel[9]==null?"":sel[9].toString());
				dto.setTraderNameEn(sel[10]==null?"":sel[10].toString());
				dto.setTraderCategory(sel[11]==null?"":sel[11].toString());
				
				
				dto.setTraderCategoryName(
						TraderCategory.getTraderCategory(
							dto.getTraderType()
							, dto.getTraderCategory()
						).getMeaning()
				);

				dto.setPersonId(Long.valueOf(sel[12].toString()));
				dto.setPersonType(sel[13]==null?"":sel[13].toString());
				dto.setPrefixName(sel[14]==null?"":sel[14].toString());
				dto.setFirstName(sel[15]==null?"":sel[15].toString());
				dto.setLastName(sel[16]==null?"":sel[16].toString());
				dto.setPostfixName(sel[17]==null?"":sel[17].toString());
								
				String traderOwnerName = "";
				if(PersonType.CORPORATE.getStatus().equals(dto.getPersonType()) && TraderType.TOUR_COMPANIES.equals(dto.getTraderType()))
				{
					traderOwnerName = dto.getPrefixName() + dto.getFirstName() + dto.getPostfixName();
					
				}
				else
				{
					traderOwnerName = dto.getPrefixName() + dto.getFirstName() + " " + dto.getLastName();
					
				}
				dto.setTraderOwnerName(traderOwnerName);
				if(sel[18] != null)
				{
					dto.setPrefixId(Long.valueOf(sel[18].toString()));
				}

				
				
				if(sel[19] != null)
				{
					dto.setPostfixId(Long.valueOf(sel[19].toString()));
				}
				
				
				if(sel[20] != null)
				{
					dto.setAmphurId(Long.valueOf(sel[20].toString()));
				}
				if(sel[21] != null)
				{
					dto.setTaxAmphurId(Long.valueOf(sel[21].toString()));
				}
				if(sel[22] != null)
				{
					dto.setProvinceId(Long.valueOf(sel[22].toString()));
				}
				if(sel[23] != null)
				{
					dto.setTaxProvinceId(Long.valueOf(sel[23].toString()));
				}
				
				if(sel[24] != null)
				{
					dto.setTraderGuideId(Long.valueOf(sel[24].toString()));
				}
				
				dto.setPronunciationName(sel[25]==null?"":sel[25].toString());
			    dto.setLicenseNo(sel[26]==null?"":sel[26].toString());
			    dto.setPersonType(sel[27]==null?"":sel[27].toString());
			    dto.setIdentityNo(sel[28]==null?"":sel[28].toString());
			    dto.setCommitteeName1(sel[29]==null?"":sel[29].toString());
			    dto.setCommitteeName2(sel[30]==null?"":sel[30].toString());
			    dto.setCommitteeNameSign(sel[31]==null?"":sel[31].toString());
			    dto.setPassportNo(sel[32]==null?"":sel[32].toString());
			    dto.setFirstNameEn(sel[33]==null?"":sel[33].toString());
			    dto.setLastNameEn(sel[34]==null?"":sel[34].toString());
			    dto.setGender(sel[35]==null?"":sel[35].toString());
			    dto.setPersonNationality(sel[36]==null?"":sel[36].toString());

			    if(sel[37] != null)
				{
					
					dto.setBirthDate(ft.format((Date)sel[37]));
				}
			    
			    dto.setAgeYear(sel[38]==null?0:Integer.valueOf(sel[38].toString()));
			    if(sel[39] != null)
				{
					
					dto.setIdentityNoExpire(ft.format((Date)sel[39]));
				}
			   
			    dto.setCorporateType(sel[40]==null?"":sel[40].toString());
			    dto.setTaxIdentityNo(sel[41]==null?"":sel[41].toString());
			    if(sel[42] != null)
				{
					
					dto.setIdentityDate(ft.format((Date)sel[42]));
				}
			    dto.setTraderRecordStatus(sel[43]==null?"":sel[43].toString());
			    
			    dto.setAmphurName(sel[44]==null?"":sel[44].toString());
			    dto.setTaxAmphurName(sel[45]==null?"":sel[45].toString());
			    dto.setProvinceName(sel[46]==null?"":sel[46].toString());
			    dto.setTaxProvinceName(sel[47]==null?"":sel[47].toString());
			    dto.setPrefixNameEn(sel[48]==null?"":sel[48].toString());
				
//			    Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(dto.getRegId());
//				if(TraderType.TOUR_COMPANIES.getStatus().equals(dto.getTraderType()))
//				{
//					//plantripPerYear
//					tourCompaniesService.getRegistration(reg, dto, user);
//				}
//				
//				if(TraderType.GUIDE.getStatus().equals(dto.getTraderType()))
//				{
//					guideService.getRegistration(reg, dto, user);
//				}
//				
//				if(TraderType.LEADER.getStatus().equals(dto.getTraderType()))
//				{
//					tourLeaderService.getRegistration(reg, dto, user);
//				}
//				
//				List<RegisterProgress> listprogress =  registerProgressDAO.findRegisterProgressbyRegistration(reg.getRegId(), ProgressStatus.GET_IT.getStatus());
//				
//				if(!listprogress.isEmpty())
//				{
//					RegisterProgress progress = listprogress.get(0);
//					
//					String authorityName = progress.getAdmUser().getMasPrefix().getPrefixName() + progress.getAdmUser().getUserName() +" "
//							+progress.getAdmUser().getUserLastname();
//					
//					dto.setAuthorityName(authorityName);
//				}
//				else
//				{
//					dto.setAuthorityName(user.getUserData().getUserFullName());
//				}

				dto.setApproveDate(sel[49]==null?"":ft.format((Date)sel[49]));
				dto.setEffectiveDate(sel[50]==null?"":ft.format((Date)sel[50]));
				dto.setExpireDate(sel[51]==null?"":ft.format((Date)sel[51]));
				
				if(sel[52]!=null)
				{
					if(RecordStatus.NORMAL.getStatus().equals(dto.getTraderRecordStatus()))
					{
						if(LicenseStatus.CANCEL.getStatus().equals(sel[52].toString()))
						{
							//ถาเปน ยกเลิกใบอนุญาต ให้ใบอนุญาตปัจจุบัน กลายเปน ประวัติและเพิ่มอีก record ให้เปนการยกเลิกใบอนญาต 
							dto.setLicenseStatus(RecordStatus.HISTORY.getStatus());
							dto.setLicenseStatusName(LicenseStatus.getMeaning(RecordStatus.HISTORY.getStatus()));
						}
						else
						{
							dto.setLicenseStatus(sel[52].toString());
							dto.setLicenseStatusName(LicenseStatus.getMeaning(dto.getLicenseStatus()));
						}	
					}
					else
					{
						dto.setLicenseStatus(dto.getTraderRecordStatus());
						dto.setLicenseStatusName(RecordStatus.getMeaning(dto.getTraderRecordStatus()));
					}					
				}
				
				//------
				String licenseNew = sel[26]==null?"":sel[26].toString();
				
//				System.out.println("#####licenseNoOld = "+licenseNoOld+" ,licenseNew = "+licenseNew);
				
//				if(!licenseNoOld.equals(licenseNew))
//				{
//					List<Trader> listTrader = (List<Trader>)this.traderDAO.findTraderByLicenseNo(sel[26]==null?"":sel[26].toString(), params.getTraderType(), RecordStatus.NORMAL.getStatus(), params.getOrgId());
//					
//					if(listTrader != null && listTrader.size() >0)
//					{
//						Trader traderPersis = listTrader.get(0);
//						if(TraderType.TOUR_COMPANIES.getStatus().equals(traderPersis.getTraderType()))
//						{
//							traderNameNew = traderPersis.getTraderName();
//						}
//						else
//						{
//							String prefixName = "";
//							if(null != traderPersis.getPerson().getMasPrefix())
//							{
//								prefixName = traderPersis.getPerson().getMasPrefix().getPrefixName();
//							}
//							
//							String postfixName = "";
//							if(null != traderPersis.getPerson().getMasPosfix())
//							{
//								postfixName = traderPersis.getPerson().getMasPosfix().getPostfixName();
//							}
//							if(PersonType.CORPORATE.getStatus().equals(traderPersis.getPerson().getPersonType()))
//							{
//								traderOwnerNameNew = prefixName + traderPersis.getPerson().getFirstName() + postfixName;
//							}
//							else
//							{
//								traderOwnerNameNew = prefixName + traderPersis.getPerson().getFirstName() + " " + traderPersis.getPerson().getLastName();
//							}
//	
//						}
//						traderCategoryNew = TraderCategory.getTraderCategory(traderPersis.getTraderType(), traderPersis.getTraderCategory()).getMeaning();	
//						licenseNoOld = licenseNew;
//					}
//				}
//				
//				dto.setHeaderInfo("ใบอนุญาตเลขที่: "+dto.getLicenseNo());
//				
//				GuaranteeDTO guaranteeDTO = new GuaranteeDTO();
//				if(TraderType.TOUR_COMPANIES.getStatus().equals(dto.getTraderType()))
//				{
//					//get guaranttee
//					
//					List<Guarantee> listGuarantees = this.guaranteeDAO.findByTrader(dto.getLicenseNo(), dto.getTraderType());
//					if(!listGuarantees.isEmpty())
//					{
//						Guarantee guarantee = listGuarantees.get(0);
//						
//						ObjectUtil.copy(guarantee, dto);
//						
//						if(guarantee.getMasBankByBankGuaranteeBankId() != null)
//						{
//							dto.setMasBankByBankGuaranteeBankName(guarantee.getMasBankByBankGuaranteeBankId().getBankName());
//						}
//						if(guarantee.getMasBankByCashAccountBankId() != null)
//						{
//							dto.setMasBankByCashAccountBankName(guarantee.getMasBankByCashAccountBankId().getBankName());
//						}
//						
//					}
//				}
				
				list.add(dto);
				
//				if(RecordStatus.NORMAL.getStatus().equals(dto.getTraderRecordStatus()))
//				{
//				
//					if(LicenseStatus.CANCEL.getStatus().equals(sel[52].toString()))
//					{
//						RegistrationDTO cancelLicenseDTO = new RegistrationDTO();
//						ObjectUtil.copy(dto, cancelLicenseDTO);
//						
//						DeactivateLicenseDTO param = new DeactivateLicenseDTO();
//						param.setLicenseNo(dto.getLicenseNo());
//						param.setTraderType(dto.getTraderType());
//						param.setOrgId(user.getUserData().getOrgId());
//						
//						param.setStart(0);
//						param.setLimit(10);
//						
//						List<DeactivateLicenseDTO> listDeactivate =  (List<DeactivateLicenseDTO>)this.deactivateLicenseService.getAll(param, user);
//						
//						if(!listDeactivate.isEmpty())
//						{
//							DeactivateLicenseDTO deactDto = listDeactivate.get(0);
//							cancelLicenseDTO.setRegistrationDate(deactDto.getDeactivateDate());
//							cancelLicenseDTO.setApproveDateFrom(deactDto.getBookDate());
//							
//							ObjectUtil.copy(deactDto, cancelLicenseDTO);
//							
//							cancelLicenseDTO.setDeactivateBookNo(deactDto.getBookNo());
//							cancelLicenseDTO.setDeactivateRemark(deactDto.getRemark());
//							
//							List<DeactivateDetail> listDeactDetail = this.deactivateDetailDAO.findByDeactivate(deactDto.getDeactiveId());
//							
//							if(!listDeactDetail.isEmpty())
//							{
//								DeactivateDetail deactivateDetail = listDeactDetail.get(0);
//								cancelLicenseDTO.setDeactivateTypeName(deactivateDetail.getMasDeactivateType()==null?"":deactivateDetail.getMasDeactivateType().getDeactivateName());
//							}
//						}
//						
//						cancelLicenseDTO.setRegistrationType(LicenseStatus.CANCEL.getStatus());
//						cancelLicenseDTO.setRegistrationTypeName(LicenseStatus.getMeaning(LicenseStatus.CANCEL.getStatus()) + "ใบอนุญาต");
//						cancelLicenseDTO.setLicenseStatus(LicenseStatus.CANCEL.getStatus());
//						cancelLicenseDTO.setLicenseStatusName(LicenseStatus.getMeaning(LicenseStatus.CANCEL.getStatus()));
//								
//						cancelLicenseDTO.setEffectiveDate("");
//						cancelLicenseDTO.setExpireDate("");
//						cancelLicenseDTO.setRoleAction("C");
//						
//						if(guaranteeDTO != null)
//						{
//							ObjectUtil.copy(guaranteeDTO, cancelLicenseDTO);
//						}
//						
//						list.add(cancelLicenseDTO);
//						
//					}
//				}
			}
		}
		
		return list;
	}
	//End Oat Add 13/11/57

	
	//Oat Add 16/02/58
	public List getInfoAccountWillCancleByPaging(Object object, User user, int start, int limit)
			throws Exception {
		
		return this.getAccountWillCancle((RegistrationDTO)object, user, start, limit);

	}
	//
		
	//Oat Add 16/02/58
	public List getAccountWillCancle(RegistrationDTO params, User user, int start, int limit) throws Exception 
	{
		if(!(params instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถค้นหาข้อมูลการจดทะเบียนได้");
		}
		
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
//		//ยังไม่พิมพ์ใบเสร็จ
//		if(!params.getRegProgressStatus().equals(ProgressStatus.ACCEPT.getStatus()))
//		{
//			list = this.getInfoAccountWillCancle(params, user, start, limit);
//		}
//		// พิมพ์ใบเสร็จแล้ว
//		else 
//		{
//			list = this.getReceiptTraderCancle(params, user, start, limit);
//		}
		
		list = this.getReceiptTraderCancle(params, user, start, limit);
		
		return list;
	}
	//End Oat Add
	
//	public List getInfoAccountWillCancle(RegistrationDTO params, User user, int start, int limit) throws Exception 
//	{
//		if(!(params instanceof RegistrationDTO))
//		{
//			throw new IllegalArgumentException("ไม่สามารถค้นหาข้อมูลการจดทะเบียนได้");
//		}
//		
//		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
//		
//		DateFormat ft =  DateUtils.getProcessDateFormatThai();
//		
//		List<Object[]> listObj = this.infoDAO.findInfoAccountWillCancleByPaging(params, user , start, limit);
//		
//		if(!listObj.isEmpty())
//		{
//			for(Object[] sel: listObj)
//			{
//				RegistrationDTO dto = new RegistrationDTO();
//				
//				dto.setRegId(Long.valueOf(sel[0].toString()));
//				
//				if(sel[1] != null)
//				{
//					dto.setRegistrationNo(sel[1]==null?"":sel[1].toString());
//				}
//				
//				if(sel[2] != null)
//				{
//					
//					dto.setRegistrationDate(ft.format((Date)sel[2]));
//				}
//				dto.setRegistrationProgress(sel[3]==null?"":sel[3].toString());
//				if(sel[8] != null)
//				{
//					dto.setTraderType(sel[8]==null?"":sel[8].toString());
//					dto.setTraderTypeName(TraderType.getMeaning(dto.getTraderType()));
//				}
//				
//				if(sel[4]!=null)
//				{
//					dto.setRegistrationType(sel[4]==null?"":sel[4].toString());
//					dto.setRegistrationTypeName(RegistrationType.getMeaning(dto.getTraderType(), dto.getRegistrationType()));
//				}
//				dto.setOrgId(Long.valueOf(sel[5].toString()));
//				dto.setRegRecordStatus(sel[6]==null?"":sel[6].toString());
//							
//				dto.setTraderId(Long.valueOf(sel[7].toString()));
//				
//				
//				dto.setTraderName(sel[9]==null?"":sel[9].toString());
//				dto.setTraderNameEn(sel[10]==null?"":sel[10].toString());
//				dto.setTraderCategory(sel[11]==null?"":sel[11].toString());
//				
//				
//				dto.setTraderCategoryName(
//						TraderCategory.getTraderCategory(
//							dto.getTraderType()
//							, dto.getTraderCategory()
//						).getMeaning()
//				);
//
//				dto.setPersonId(Long.valueOf(sel[12].toString()));
//				dto.setPersonType(sel[13]==null?"":sel[13].toString());
//				dto.setPrefixName(sel[14]==null?"":sel[14].toString());
//				dto.setFirstName(sel[15]==null?"":sel[15].toString());
//				dto.setLastName(sel[16]==null?"":sel[16].toString());
//				dto.setPostfixName(sel[17]==null?"":sel[17].toString());
//								
//				String traderOwnerName = "";
//				if(PersonType.CORPORATE.getStatus().equals(dto.getPersonType()) && TraderType.TOUR_COMPANIES.equals(dto.getTraderType()))
//				{
//					traderOwnerName = dto.getPrefixName() + dto.getFirstName() + dto.getPostfixName();
//					
//				}
//				else
//				{
//					traderOwnerName = dto.getPrefixName() + dto.getFirstName() + " " + dto.getLastName();
//					
//				}
//				dto.setTraderOwnerName(traderOwnerName);
//				if(sel[18] != null)
//				{
//					dto.setPrefixId(Long.valueOf(sel[18].toString()));
//				}
//
//				
//				
//				if(sel[19] != null)
//				{
//					dto.setPostfixId(Long.valueOf(sel[19].toString()));
//				}
//				
//				
//				if(sel[20] != null)
//				{
//					dto.setAmphurId(Long.valueOf(sel[20].toString()));
//				}
//				if(sel[21] != null)
//				{
//					dto.setTaxAmphurId(Long.valueOf(sel[21].toString()));
//				}
//				if(sel[22] != null)
//				{
//					dto.setProvinceId(Long.valueOf(sel[22].toString()));
//				}
//				if(sel[23] != null)
//				{
//					dto.setTaxProvinceId(Long.valueOf(sel[23].toString()));
//				}
//				
//				if(sel[24] != null)
//				{
//					dto.setTraderGuideId(Long.valueOf(sel[24].toString()));
//				}
//				
//				dto.setPronunciationName(sel[25]==null?"":sel[25].toString());
//			    dto.setLicenseNo(sel[26]==null?"":sel[26].toString());
//			    dto.setPersonType(sel[27]==null?"":sel[27].toString());
//			    dto.setIdentityNo(sel[28]==null?"":sel[28].toString());
//			    dto.setCommitteeName1(sel[29]==null?"":sel[29].toString());
//			    dto.setCommitteeName2(sel[30]==null?"":sel[30].toString());
//			    dto.setCommitteeNameSign(sel[31]==null?"":sel[31].toString());
//			    dto.setPassportNo(sel[32]==null?"":sel[32].toString());
//			    dto.setFirstNameEn(sel[33]==null?"":sel[33].toString());
//			    dto.setLastNameEn(sel[34]==null?"":sel[34].toString());
//			    dto.setGender(sel[35]==null?"":sel[35].toString());
//			    dto.setPersonNationality(sel[36]==null?"":sel[36].toString());
//
//			    if(sel[37] != null)
//				{
//					
//					dto.setBirthDate(ft.format((Date)sel[37]));
//				}
//			    
//			    dto.setAgeYear(sel[38]==null?0:Integer.valueOf(sel[38].toString()));
//			    if(sel[39] != null)
//				{
//					
//					dto.setIdentityNoExpire(ft.format((Date)sel[39]));
//				}
//			   
//			    dto.setCorporateType(sel[40]==null?"":sel[40].toString());
//			    dto.setTaxIdentityNo(sel[41]==null?"":sel[41].toString());
//			    if(sel[42] != null)
//				{
//					
//					dto.setIdentityDate(ft.format((Date)sel[42]));
//				}
//			    dto.setTraderRecordStatus(sel[43]==null?"":sel[43].toString());
//			    
//			    dto.setAmphurName(sel[44]==null?"":sel[44].toString());
//			    dto.setTaxAmphurName(sel[45]==null?"":sel[45].toString());
//			    dto.setProvinceName(sel[46]==null?"":sel[46].toString());
//			    dto.setTaxProvinceName(sel[47]==null?"":sel[47].toString());
//			    dto.setPrefixNameEn(sel[48]==null?"":sel[48].toString());
//
//				dto.setApproveDate(sel[49]==null?"":ft.format((Date)sel[49]));
//				dto.setEffectiveDate(sel[50]==null?"":ft.format((Date)sel[50]));
//				dto.setExpireDate(sel[51]==null?"":ft.format((Date)sel[51]));
//				
//				if(sel[52]!=null)
//				{
//					if(RecordStatus.NORMAL.getStatus().equals(dto.getTraderRecordStatus()))
//					{
//						if(LicenseStatus.CANCEL.getStatus().equals(sel[52].toString()))
//						{
//							//ถาเปน ยกเลิกใบอนุญาต ให้ใบอนุญาตปัจจุบัน กลายเปน ประวัติและเพิ่มอีก record ให้เปนการยกเลิกใบอนญาต 
//							dto.setLicenseStatus(RecordStatus.HISTORY.getStatus());
//							dto.setLicenseStatusName(LicenseStatus.getMeaning(RecordStatus.HISTORY.getStatus()));
//						}
//						else
//						{
//							dto.setLicenseStatus(sel[52].toString());
//							dto.setLicenseStatusName(LicenseStatus.getMeaning(dto.getLicenseStatus()));
//						}	
//					}
//					else
//					{
//						dto.setLicenseStatus(dto.getTraderRecordStatus());
//						dto.setLicenseStatusName(RecordStatus.getMeaning(dto.getTraderRecordStatus()));
//					}					
//				}
//				
//				//------
//				String licenseNew = sel[26]==null?"":sel[26].toString();
//				
//				list.add(dto);
//				
//			}
//		}
//		
//		return list;
//	}
	
	private List getReceiptTraderCancle(Object object, User user ,int start, int limit) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		RegistrationDTO params = (RegistrationDTO)object;
		
		DateFormat ft =  DateUtils.getProcessDateFormatThai();
		
		List<Object[]> listObj = this.infoDAO.findInfoReceiptWillCanclePaging(params, user , start, limit);
		
		if(!listObj.isEmpty())
		{
			for(Object[] sel: listObj)
			{
				RegistrationDTO dto = new RegistrationDTO();
				
			
				dto.setRegId(Long.valueOf(sel[0].toString()));
				dto.setRegistrationNo(sel[1]==null?"":sel[1].toString());
				if(sel[2] != null)
				{
					
					dto.setRegistrationDate(ft.format((Date)sel[2]));
				}
				dto.setRegistrationProgress(sel[3]==null?"":sel[3].toString());
				if(sel[8] != null)
				{
					dto.setTraderType(sel[8]==null?"":sel[8].toString());
					dto.setTraderTypeName(TraderType.getMeaning(dto.getTraderType()));
				}
				if(sel[4]!=null)
				{
					dto.setRegistrationType(sel[4]==null?"":sel[4].toString());
					dto.setRegistrationTypeName(RegistrationType.getMeaning(dto.getTraderType(), dto.getRegistrationType()));
				}
				dto.setOrgId(Long.valueOf(sel[5].toString()));
				dto.setRegRecordStatus(sel[6]==null?"":sel[6].toString());
				dto.setTraderId(Long.valueOf(sel[7].toString()));
				dto.setTraderName(sel[9]==null?"":sel[9].toString());
				dto.setTraderNameEn(sel[10]==null?"":sel[10].toString());
				dto.setTraderCategory(sel[11]==null?"":sel[11].toString());
				
				
				dto.setTraderCategoryName(
						TraderCategory.getTraderCategory(
							dto.getTraderType()
							, dto.getTraderCategory()
						).getMeaning()
				);
				

				
				dto.setPersonId(Long.valueOf(sel[12].toString()));
				dto.setPersonType(sel[13]==null?"":sel[13].toString());
				dto.setPrefixName(sel[14]==null?"":sel[14].toString());
				dto.setFirstName(sel[15]==null?"":sel[15].toString());
				dto.setLastName(sel[16]==null?"":sel[16].toString());
				dto.setPostfixName(sel[17]==null?"":sel[17].toString());
				

				
				if(PersonType.CORPORATE.getStatus().equals(dto.getPersonType()))
				{
					String traderOwnerName = dto.getPrefixName() + dto.getFirstName() + dto.getPostfixName();
					dto.setTraderOwnerName(traderOwnerName);
				}
				else
				{
					String traderOwnerName = dto.getPrefixName() + dto.getFirstName() + " " + dto.getLastName();
					dto.setTraderOwnerName(traderOwnerName);
				}
				

				dto.setPrefixId(Long.valueOf(sel[18].toString()));
				
				if(sel[19] != null)
				{
					dto.setPostfixId(Long.valueOf(sel[19].toString()));
				}
				
				
				if(sel[20] != null)
				{
					dto.setAmphurId(Long.valueOf(sel[20].toString()));
				}
				if(sel[21] != null)
				{
					dto.setTaxAmphurId(Long.valueOf(sel[21].toString()));
				}
				if(sel[22] != null)
				{
					dto.setProvinceId(Long.valueOf(sel[22].toString()));
				}
				if(sel[23] != null)
				{
					dto.setTaxProvinceId(Long.valueOf(sel[23].toString()));
				}
				
				if(sel[24] != null)
				{
					dto.setTraderGuideId(Long.valueOf(sel[24].toString()));
				}
				
				dto.setPronunciationName(sel[25]==null?"":sel[25].toString());
			    dto.setLicenseGuideNo(sel[26]==null?"":sel[26].toString());
			    dto.setPersonType(sel[27]==null?"":sel[27].toString());
			    dto.setIdentityNo(sel[28]==null?"":sel[28].toString());
			    dto.setCommitteeName1(sel[29]==null?"":sel[29].toString());
			    dto.setCommitteeName2(sel[30]==null?"":sel[30].toString());
			    dto.setCommitteeNameSign(sel[31]==null?"":sel[31].toString());
			    dto.setPassportNo(sel[32]==null?"":sel[32].toString());
			    dto.setFirstNameEn(sel[33]==null?"":sel[33].toString());
			    dto.setLastNameEn(sel[34]==null?"":sel[34].toString());
			    dto.setGender(sel[35]==null?"":sel[35].toString());
			    dto.setPersonNationality(sel[36]==null?"":sel[36].toString());


			    if(sel[37] != null)
				{
					
					dto.setBirthDate(ft.format((Date)sel[37]));
				}
			    
			    dto.setAgeYear(sel[38]==null?0:Integer.valueOf(sel[38].toString()));
			    if(sel[39] != null)
				{
					
					dto.setIdentityNoExpire(ft.format((Date)sel[39]));
				}
			   
			    dto.setCorporateType(sel[40]==null?"":sel[40].toString());
			    dto.setTaxIdentityNo(sel[41]==null?"":sel[41].toString());
			    if(sel[42] != null)
				{
					
					dto.setIdentityDate(ft.format((Date)sel[42]));
				}
			    dto.setTraderRecordStatus(sel[43]==null?"":sel[43].toString());
			    
			    dto.setAmphurName(sel[44]==null?"":sel[44].toString());
			    dto.setTaxAmphurName(sel[45]==null?"":sel[45].toString());
			    dto.setProvinceName(sel[46]==null?"":sel[46].toString());
			    dto.setTaxProvinceName(sel[47]==null?"":sel[47].toString());
			    dto.setPrefixNameEn(sel[48]==null?"":sel[48].toString());
			    dto.setLicenseNo(sel[49]==null?"":sel[49].toString());

			 
//			    dto.setReceiptId(Long.valueOf(sel[50].toString()));
			    dto.setReceiptId(sel[50]==null?0:Long.valueOf(sel[50].toString()));
			    
			    dto.setBookNo(sel[51]==null?"":sel[51].toString());
			    dto.setReceiptNo(sel[52]==null?"":sel[52].toString());
			    dto.setReceiptStatus(sel[53]==null?"":sel[53].toString());
			    
			    dto.setApproveDate(sel[54]==null?"":ft.format((Date)sel[54]));
				dto.setEffectiveDate(sel[55]==null?"":ft.format((Date)sel[55]));
				dto.setExpireDate(sel[56]==null?"":ft.format((Date)sel[56]));
				
				Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(dto.getRegId());
				if(TraderType.TOUR_COMPANIES.getStatus().equals(dto.getTraderType()))
				{
					tourCompaniesService.getRegistration(reg, dto, user);
				}
				
				if(TraderType.GUIDE.getStatus().equals(dto.getTraderType()))
				{
					guideService.getRegistration(reg, dto, user);
				}
				
				if(TraderType.LEADER.getStatus().equals(dto.getTraderType()))
				{
					tourLeaderService.getRegistration(reg, dto, user);
				}
				
				List<RegisterProgress> listprogress =  registerProgressDAO.findRegisterProgressbyRegistration(reg.getRegId(), ProgressStatus.GET_IT.getStatus());
				
				if(!listprogress.isEmpty())
				{
					RegisterProgress progress = listprogress.get(0);
					
					String authorityName = progress.getAdmUser().getMasPrefix().getPrefixName() + progress.getAdmUser().getUserName() +" "
							+progress.getAdmUser().getUserLastname();
					
					dto.setAuthorityName(authorityName);
				}
				
				//สามารถแก้ไขข้อมูลได้ ถ้า RegistrationProgress อยู่ที่ role ตัวเอง
				/**
				 * RECHECK
				 * SUPERVISOR
				 * MANAGER
				 * DIRECTOR
				 */

				dto.setRoleAction("P");
				
				list.add(dto);
				
				
			}

		}
			
		return list;
	}
	
	//Oat Add17/06/58
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveLicenseEditReorganize(Object object, User user) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
		try{
			
			RegistrationDTO dto =  (RegistrationDTO)object;
			
			Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(dto.getRegId());
			reg.setLastUpdUser(user.getUserName());
			reg.setLastUpdDtm(new Date());
			
			ObjectUtil.copy(dto, reg);
//			reg.setRecordStatus(RecordStatus.NORMAL.getStatus());
			
			Trader trader = (Trader)reg.getTrader();	
			ObjectUtil.copy(dto, trader);
			
			reg.setRecordStatus(trader.getRecordStatus());
//			trader.setLicenseStatus(LicenseStatus.NORMAL.getStatus());
//			trader.setRecordStatus(RecordStatus.NORMAL.getStatus());
			
			trader.setLastUpdUser(user.getUserName());
			trader.setLastUpdDtm(new Date());
							
			Person person = trader.getPerson();
			ObjectUtil.copy(dto, person);
//			person.setRecordStatus(RecordStatus.NORMAL.getStatus());
			person.setRecordStatus(trader.getRecordStatus());
			reg.setRecordStatus(trader.getRecordStatus());
			
			person.setLastUpdUser(user.getUserName());
			person.setLastUpdDtm(new Date());
			
			if(dto.getPrefixId() > 0)
			{
				MasPrefix masPrefix = new MasPrefix();
				masPrefix.setPrefixId(dto.getPrefixId());
				
				person.setMasPrefix(masPrefix);
			}
			
			if(dto.getPostfixId() > 0)
			{
				MasPosfix masPosfix = new MasPosfix();
				masPosfix.setPostfixId(dto.getPostfixId());
				
				person.setMasPosfix(masPosfix);
			}
			else
			{
				person.setMasPosfix(null);
			}
			
			if(dto.getAmphurId() > 0)
			{
				MasAmphur masAmphurByAmphurId = new MasAmphur();
				masAmphurByAmphurId.setAmphurId(dto.getAmphurId());
				
				person.setMasAmphurByAmphurId(masAmphurByAmphurId);
			}
		

			if(dto.getTaxAmphurId() > 0)
			{
				MasAmphur masAmphurByTaxAmphurId = new MasAmphur();
				masAmphurByTaxAmphurId.setAmphurId(dto.getTaxAmphurId());
				person.setMasAmphurByTaxAmphurId(masAmphurByTaxAmphurId);
				
			}
			
			if(dto.getTaxProvinceId() > 0)
			{
				MasProvince masProvinceByTaxProvinceId = new MasProvince();
				masProvinceByTaxProvinceId.setProvinceId(dto.getTaxProvinceId());
				person.setMasProvinceByTaxProvinceId(masProvinceByTaxProvinceId);
			}

			
			if(dto.getProvinceId() > 0)
			{
				MasProvince masProvinceByProvinceId = new MasProvince();
				masProvinceByProvinceId.setProvinceId(dto.getProvinceId());
				person.setMasProvinceByProvinceId(masProvinceByProvinceId);
			}
			
			person.setLastUpdUser(user.getUserName());
			person.setLastUpdDtm(new Date());
			
			personDAO.update(person);
			traderDAO.update(trader);
			registrationDAO.update(reg);
			
			
			//education and traning
			List<Education> listEducation = (List<Education>) this.educationDAO.findEducationByPerson(person.getPersonId(), RecordStatus.TEMP.getStatus(), null);
			if(!listEducation.isEmpty())
			{
				for(Education edu: listEducation)
				{
//					edu.setRecordStatus(RecordStatus.NORMAL.getStatus());
					edu.setRecordStatus(trader.getRecordStatus());
					edu.setLastUpdUser(user.getUserName());
					edu.setLastUpdDtm(new Date());
					this.educationDAO.update(edu);
				}
			}
			//ภาษา
			List<ForeignLanguage> listLang = (List<ForeignLanguage>) this.foreignLanguageDAO.findForeignLanguageByPerson(person.getPersonId(), RecordStatus.TEMP.getStatus());
			if(!listLang.isEmpty())
			{
				for(ForeignLanguage lang: listLang)
				{
//					lang.setRecordStatus(RecordStatus.NORMAL.getStatus());
					lang.setRecordStatus(trader.getRecordStatus());
					lang.setLastUpdUser(user.getUserName());
					lang.setLastUpdDtm(new Date());
					this.foreignLanguageDAO.update(lang);
				}
			}
			
			
			// trader addres
			List<TraderAddress> listAddress = (List<TraderAddress>) this.traderAddressDAO.findAllByTrader(trader.getTraderId(), null, RecordStatus.TEMP.getStatus());
			if(!listAddress.isEmpty())
			{
				for(TraderAddress address: listAddress)
				{
//					address.setRecordStatus(RecordStatus.NORMAL.getStatus());
					address.setRecordStatus(trader.getRecordStatus());
					address.setLastUpdUser(user.getUserName());
					address.setLastUpdDtm(new Date());
					this.traderAddressDAO.update(address);
				}
			}


			
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
		
	}
	//End 
	
	//Oat Add 26/06/58
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveLicenseEditReorganizeTour(Object object , User user) throws Exception {
		
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
		RegistrationDTO dto =  (RegistrationDTO)object;
		
		Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(dto.getRegId());

	
		Trader tourCom = (Trader)reg.getTrader();
//		tourCom.setLicenseStatus(LicenseStatus.NORMAL.getStatus());
		

		Person person = tourCom.getPerson();

		person.setCommitteeName1(dto.getCommitteeNameSign());
		
	
		personDAO.update(person);

		
		// set plan trip
		List<Plantrip> listplantrip = (List<Plantrip>) this.plantripDAO.findPlanTripbyTrader(tourCom.getTraderId(), null);
		if(!listplantrip.isEmpty())
		{
			for(Plantrip plan: listplantrip)
			{
				plan.setPlantripPerYear(dto.getPlantripPerYear());
				plan.setLastUpdUser(user.getUserName());
//				plan.setRecordStatus(RecordStatus.NORMAL.getStatus());
				plan.setRecordStatus(tourCom.getRecordStatus());
				plan.setLastUpdDtm(new Date());
				
				plantripDAO.update(plan);
			}
		}
		else
		{
			Country thaiCountry = countryDAO.findThaiCountry();
			Plantrip plantrip = new Plantrip();
			
			plantrip.setCountry(thaiCountry);
			plantrip.setPlantripPerYear(dto.getPlantripPerYear());
			plantrip.setCreateDtm(new Date());
			plantrip.setCreateUser(user.getUserName());
//			plantrip.setRecordStatus(RecordStatus.NORMAL.getStatus());
			plantrip.setRecordStatus(tourCom.getRecordStatus());
			plantrip.setTrader(tourCom);
			
			plantripDAO.insert(plantrip);
			
		}
		
		List<Trader> listBranch = (List<Trader>) this.branchDAO.findAllByBranchParent(tourCom.getTraderId(), null);
		if(!listBranch.isEmpty())
		{
			for(Trader branch: listBranch)
			{
				branch.setTraderCategory(tourCom.getTraderCategory());
				branch.setTraderType(tourCom.getTraderType());
				branch.setTraderName(tourCom.getTraderName());
				branch.setTraderNameEn(tourCom.getTraderNameEn());
				branch.setPronunciationName(tourCom.getPronunciationName());
				branch.setEffectiveDate(tourCom.getEffectiveDate());
				branch.setExpireDate(tourCom.getExpireDate());
				branch.setLicenseStatus(tourCom.getLicenseStatus());
				branch.setOrganization(tourCom.getOrganization());
				branch.setPerson(tourCom.getPerson());
				branch.setMasProvince(tourCom.getMasProvince());
				
				branch.setLicenseNo(tourCom.getLicenseNo());
		
				
				if(RegistrationType.TOUR_COMPANIES_ADD_NEW_BRANCH.getStatus().equals(branch.getAddNewBranch()))
				{
//					branch.setEffectiveDate(new Date());
				}
				
//				branch.setLicenseStatus(RecordStatus.NORMAL.getStatus());
//				branch.setRecordStatus(RecordStatus.NORMAL.getStatus());
				branch.setLicenseStatus(tourCom.getLicenseStatus());
				branch.setRecordStatus(tourCom.getRecordStatus());
				branch.setLastUpdUser(user.getUserName());
				
				branch.setLastUpdDtm(new Date());
				
				this.branchDAO.update(branch);
				
				List<TraderAddress> listBranchAddress = (List<TraderAddress>) this.traderAddressDAO.findAllByTrader(branch.getTraderId(), null, RecordStatus.TEMP.getStatus());
				if(!listBranchAddress.isEmpty())
				{
					for(TraderAddress branchAddress: listBranchAddress)
					{
//						branchAddress.setRecordStatus(RecordStatus.NORMAL.getStatus());
						branchAddress.setRecordStatus(tourCom.getRecordStatus());
						branchAddress.setLastUpdUser(user.getUserName());
						branchAddress.setLastUpdDtm(new Date());
						this.traderAddressDAO.update(branchAddress);
					}
				}
			}
		}
		

		
		//committee
		List<Committee> listCommittee =  (List<Committee>) this.committeeDAO.findAllByPerson(person.getPersonId(), null, RecordStatus.TEMP.getStatus());
		if(!listCommittee.isEmpty())
		{
			for(Committee comm: listCommittee)
			{
//				comm.setCommitteeStatus(LicenseStatus.NORMAL.getStatus());
//				comm.setRecordStatus(RecordStatus.NORMAL.getStatus());
				comm.setCommitteeStatus(tourCom.getLicenseStatus());
				comm.setRecordStatus(tourCom.getRecordStatus());
				comm.setLastUpdUser(user.getUserName());
				comm.setLastUpdDtm(new Date());
				this.committeeDAO.update(comm);
			}
		}
		
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveLicenseEditReorganizeGuide(Object object, User user) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
		RegistrationDTO dto =  (RegistrationDTO)object;
		Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(dto.getRegId());
		
		Trader guide = (Trader)reg.getTrader();
//		guide.setLicenseStatus(LicenseStatus.NORMAL.getStatus());
		
		
		//set ประเภทมัคคุเทศน์และชนิดของมัคคุเทศน์
		if(dto.getTraderCategory()!=null)
		{
			guide.setTraderCategory(dto.getTraderCategory());
		}
		
		if(dto.getTraderArea()!=null)
		{
			guide.setTraderArea(dto.getTraderArea());
		}else
		{
			guide.setTraderArea("");
		}
		
		System.out.println(dto.getProvinceGuideServiceId());
		if(dto.getProvinceGuideServiceId() > 0)
		{
			MasProvince masProvinceByProvinceId = new MasProvince();
			masProvinceByProvinceId.setProvinceId(dto.getProvinceGuideServiceId());
			guide.setMasProvince(masProvinceByProvinceId);
		}
				
		Person person = guide.getPerson();
		person.setPersonType(PersonType.INDIVIDUAL.getStatus());
//		person.setPersonNationality("ไทย");
		
		
		traderDAO.update(guide);
		personDAO.update(person);
		
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveLicenseEditReorganizeLeader(Object object, User user) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
		RegistrationDTO dto =  (RegistrationDTO)object;
		
		Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(dto.getRegId());
		
		Trader tourleader = (Trader)reg.getTrader();
//		tourleader.setLicenseStatus(LicenseStatus.NORMAL.getStatus());
						
		Person person = tourleader.getPerson();
		person.setPersonType(PersonType.INDIVIDUAL.getStatus());
		
		//เป็นผู้นำเที่ยวตามประกาศกฎกระทรวงฯ
		tourleader.setTraderByTraderGuideId(null);
		tourleader.setLicenseGuideNo(null);
		
		//เป็นมัคคุเทศก์และมาขอจดทะเบียนผู้นำเที่ยว
//		if(dto.getTraderGuideId() > 0)
		if(TraderCategory.GUIDE_LEADER.getStatus().equals(dto.getTraderCategory()))
		{
			Trader traderGuide =  (Trader)this.traderDAO.findByPrimaryKey(dto.getTraderGuideId());
			tourleader.setTraderByTraderGuideId(traderGuide);
			tourleader.setLicenseGuideNo(traderGuide.getLicenseNo());
		}

		//ผ่านการอบรมจากสถาบันที่สำนักทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ กำหนด
//		if(dto.getEduId() > 0)
		if(TraderCategory.TRAINED_LEADER.getStatus().equals(dto.getTraderCategory()))
		{
			Education course = (Education) this.educationDAO.findByPrimaryKey(dto.getEduId());
			
			Education trained = new Education();
			
			ObjectUtil.copy(course, trained);
			
			
			trained.setEducationType(EducationType.TRAINING.getStatus());
			trained.setPerson(person);
//			trained.setRecordStatus(RecordStatus.NORMAL.getStatus());
			trained.setRecordStatus(tourleader.getRecordStatus());
			trained.setCreateUser(user.getUserName());
			trained.setCreateDtm(Calendar.getInstance().getTime());
			
			if(course.getMasUniversity() != null)
			{
				trained.setMasUniversity(course.getMasUniversity());
			}
			if(course.getMasEducationLevel() != null)
			{
				trained.setMasEducationLevel(course.getMasEducationLevel());
			}
			
			educationDAO.insert(trained);
		}
		
		traderDAO.update(tourleader);
		personDAO.update(person);
		
	}
	
}



























