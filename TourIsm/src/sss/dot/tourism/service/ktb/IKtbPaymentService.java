package sss.dot.tourism.service.ktb;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import sss.dot.tourism.dto.ktb.KtbPaymentDTO;

import com.sss.aut.service.User;

public interface IKtbPaymentService {

//	public List getAll(Object object, User user) throws Exception;
	public void upload(KtbPaymentDTO param, MultipartFile file, User user) throws Exception;
//	public List getPrintPayment(Object object, User user) throws Exception;
	
//	public List<KtbPaymentDTO> findAttachPaymentFilesDetail(String progressDate, String paymentNo)throws Exception;
	public List<KtbPaymentDTO> findAttachPaymentFilesDetail(KtbPaymentDTO param)throws Exception;
	public List<KtbPaymentDTO> findAttachPaymentFiles(String progressDate)throws Exception;
	
	public void savePaymentDetail(KtbPaymentDTO[] object)throws Exception;
}
