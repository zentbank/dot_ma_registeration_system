package sss.dot.tourism.service.ktb;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import sss.dot.tourism.dao.AdmUserDAO;
import sss.dot.tourism.dao.DotTrBillDetailDAO;
import sss.dot.tourism.dao.DotTrBillPaymentDAO;
import sss.dot.tourism.dao.DotTrFeeLicenseDAO;
import sss.dot.tourism.dao.DotTrFeePaymentDAO;
import sss.dot.tourism.dao.ktb.KtbPaymentDAO;
import sss.dot.tourism.dao.mas.MasRunningNoDAO;
import sss.dot.tourism.dao.mas.OrganizationDAO;
import sss.dot.tourism.dao.registration.GuaranteeDAO;
import sss.dot.tourism.dao.registration.ReceiptDAO;
import sss.dot.tourism.dao.registration.ReceiptDetailDAO;
import sss.dot.tourism.dao.registration.ReceiptMapRegistrationDAO;
import sss.dot.tourism.dao.registration.RegistrationDAO;
import sss.dot.tourism.dao.registration.TraderAddressDAO;
import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.domain.DotLicenseOwner;
import sss.dot.tourism.domain.DotTrBillDetail;
import sss.dot.tourism.domain.DotTrBillPayment;
import sss.dot.tourism.domain.DotTrFeeLicense;
import sss.dot.tourism.domain.DotTrFeePayment;
import sss.dot.tourism.domain.KtbPayment;
import sss.dot.tourism.domain.Organization;
import sss.dot.tourism.domain.Receipt;
import sss.dot.tourism.domain.ReceiptDetail;
import sss.dot.tourism.domain.ReceiptMapRegistration;
import sss.dot.tourism.domain.Registration;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.dto.ktb.KtbPaymentDTO;
import sss.dot.tourism.service.printreceiptslip.IPrintReceiptSlipService;
import sss.dot.tourism.service.registration.IAccountService;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.LicenseFeeStatus;
import sss.dot.tourism.util.ReceiptStatus;
import sss.dot.tourism.util.RecordStatus;
import sss.dot.tourism.util.RegistrationType;
import sss.dot.tourism.util.RoleStatus;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.User;

@Repository("ktbPaymentService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class KtbPaymentService implements IKtbPaymentService {
	
	@Autowired
	private KtbPaymentDAO ktbPaymentDAO;

	
	@Autowired
	private IAccountService accountService;
	
	@Autowired
	private GuaranteeDAO guaranteeDAO;
	
	@Autowired
	private IPrintReceiptSlipService printReceiptService;
	
	@Autowired
	private AdmUserDAO admUserDAO;
	
	@Autowired
	private TraderAddressDAO traderAddressDAO;
	
	@Autowired private DotTrBillPaymentDAO dotTrBillPaymentDAO;
	@Autowired private DotTrBillDetailDAO dotTrBillDetailDAO;
	@Autowired private DotTrFeePaymentDAO dotTrFeePaymentDAO;
	@Autowired private DotTrFeeLicenseDAO dotTrFeeLicenseDAO;
	@Autowired private OrganizationDAO organizationDAO;
	
	@Autowired
	MasRunningNoDAO masRunningNoDAO;
	@Autowired
	ReceiptDAO receiptDAO;
	
	@Autowired
	ReceiptMapRegistrationDAO receiptMapRegistrationDAO;
	@Autowired
	ReceiptDetailDAO receiptDetailDAO;
	
	@Autowired
	TraderDAO traderDAO;
	
	@Autowired
	RegistrationDAO registrationDAO;
	
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
	private void savePayment(KtbPaymentDTO param, MultipartFile file, User user) throws Exception
	{
		String H_SEQ_NO = "";
		String H_POST_DATE = "";
		
		String REC_TYPE_DETAIL = "D";
		String REC_TYPE_HEADER = "H";
		 
		BufferedReader br = null;
		
		DateFormat df = new SimpleDateFormat("ddMMyyyy");
		String paymentNo = String.valueOf(Calendar.getInstance().getTimeInMillis());
		
		 String s = "03/24/2013 21:54";
	     SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

		try {

			String sCurrentLine;

			br = new BufferedReader(new InputStreamReader(file.getInputStream(), Charset.forName("UTF-8")));
	
			sCurrentLine = br.readLine();
			//REC-TYPE ‘H’ = Header
			int indH = sCurrentLine.indexOf(REC_TYPE_HEADER);
			
			H_SEQ_NO = sCurrentLine.substring(1,7);
			//Posting Date (DDMMCCYY)
			H_POST_DATE = sCurrentLine.substring(60,69);
			
			System.out.println("H_POST_DATE = " + H_POST_DATE);
			Date postDate = df.parse(H_POST_DATE);
			System.out.println(postDate);
			
			String recType = sCurrentLine.substring(indH,indH+1);			
			
			System.out.println("recType = " + recType);
			
			while ((sCurrentLine = br.readLine()) != null) {
				
				 String D_TRAN_DATE = null;
				 String D_TRAN_TIME = null;
				 String D_REFERENCE_1 = null;
				 String D_REFERENCE_2 = null;
				 String D_TRAN_AMT = null;
				 String D_AMT_POINT = null;
				 String D_RECEIPT_NO = null;
				 String D_SEQ_NO = null;
				 String D_REFERENCE_3 = null;
				 
				
		
				if(org.springframework.util.StringUtils.hasLength(sCurrentLine))
				{
					//REC-TYPE ‘D’ = Detail
					recType = sCurrentLine.substring(0,1);
					
					System.out.println("recType = " + recType);
					
					if(REC_TYPE_DETAIL.equals(recType))
					{
						D_SEQ_NO = sCurrentLine.substring(1,7);
						D_TRAN_DATE = sCurrentLine.substring(20,28);
						D_TRAN_TIME = sCurrentLine.substring(28,34);
						D_REFERENCE_1 = sCurrentLine.substring(84,104);
						D_REFERENCE_2 = sCurrentLine.substring(104,124);
						D_TRAN_AMT = sCurrentLine.substring(163,174);
						D_AMT_POINT = sCurrentLine.substring(174,176);
						D_RECEIPT_NO = D_SEQ_NO + D_TRAN_DATE + D_TRAN_TIME;
			
						D_TRAN_DATE= D_TRAN_DATE.replaceAll("\\s+", "");
						D_TRAN_TIME= D_TRAN_TIME.replaceAll("\\s+", "");
						D_REFERENCE_1= D_REFERENCE_1.replaceAll("\\s+", "");	
						D_REFERENCE_2= D_REFERENCE_2.replaceAll("\\s+", "");
						D_REFERENCE_3 = D_TRAN_DATE;
						D_TRAN_AMT= D_TRAN_AMT.replaceAll("\\s+", "");
						D_AMT_POINT= D_AMT_POINT.replaceAll("\\s+", "");
						D_RECEIPT_NO= D_RECEIPT_NO.replaceAll("\\s+", "");
												
						Date payDate = stringToDate(D_TRAN_DATE, D_TRAN_TIME);
						
						
						
						
						System.out.println("D_TRAN_DATE : " + D_TRAN_DATE);
						System.out.println("D_TRAN_TIME : " + D_TRAN_TIME);
						System.out.println("D_REFERENCE_1 : " + D_REFERENCE_1);
						System.out.println("D_REFERENCE_2 : " + D_REFERENCE_2);
						System.out.println(sCurrentLine.substring(163,176));
						System.out.println("D_TRAN_AMT : " + D_TRAN_AMT);
						System.out.println("D_RECEIPT_NO : " + D_RECEIPT_NO);
						System.out.println("payDate : " + payDate);
						BigDecimal amount = new BigDecimal(D_TRAN_AMT+"."+D_AMT_POINT);
						System.out.println("amount : " + amount);
						
						//check duplicate
						boolean duplicate = false;
						KtbPayment payment =  this.ktbPaymentDAO.findByRef(D_REFERENCE_1, D_REFERENCE_2, D_REFERENCE_3);
					
						if(null != payment){
							duplicate = true;
							payment.setRecordStatus(RecordStatus.TEMP.getStatus());
							payment.setLastUpdUser(user.getUserName());
							payment.setLastUpdDtm(new Date());
						}else{
							
							duplicate = false;
							payment = new KtbPayment();
							payment.setRecordStatus(RecordStatus.TEMP.getStatus());
							payment.setCreateUser(user.getUserName());
							payment.setCreateDtm(new Date());
							payment.setLastUpdUser(user.getUserName());
							payment.setLastUpdDtm(new Date());
						}
						
						payment.setHSeqNo(H_SEQ_NO);
						payment.setHPostDate(postDate);
						payment.setDSeqNo(D_SEQ_NO);
						payment.setDTranDate(payDate);
						payment.setDReference1(D_REFERENCE_1);
						payment.setDReference2(D_REFERENCE_2);
						payment.setDTranAmt(amount);
						payment.setDReceiptNo(D_RECEIPT_NO);
						payment.setProgressDate(new Date());
						payment.setPaymentNo(paymentNo);
						payment.setDReference3(D_REFERENCE_3);
						param.setProgressDate(DateUtils.getProcessDateFormatThai().format(new Date()));
						param.setRecordStatus(RecordStatus.TEMP.getStatus());
//							
						if(!duplicate){
							this.ktbPaymentDAO.insert(payment);
						}else{
							this.ktbPaymentDAO.update(payment);
						}
					}
					
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally {
			try {
				if (br != null)br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
//	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
//	private void updateReceipt(User user) throws Exception
//	{
//		List<KtbPayment> list =  (List<KtbPayment>)this.ktbPaymentDAO.findPayment();
//		DateFormat df = DateUtils.getProcessDateFormatThai();
//		
//		if(!list.isEmpty())
//		{
//			for(KtbPayment payment: list)
//			{
//				String ref1 = payment.getDReference1();
//				String registrationType = ref1.substring(ref1.length()-1,ref1.length());
//				registrationType = RegistrationType.getRegTypeByCode(payment.getTraderType(), registrationType);
//								
//				List<Registration> listReg = (List<Registration>)this.ktbPaymentDAO.findRegistrationByLicenseNo(payment.getLicenseNo()
//						, payment.getTraderType(), registrationType, RecordStatus.TEMP.getStatus(), RecordStatus.TEMP.getStatus(), 0);
//				
//				if(!listReg.isEmpty())
//				{
//					Registration reg = listReg.get(0);
//					Trader trader = reg.getTrader();
//					
//					//ตรวจสอบก่อนว่ารายการนี้จ่ายหรือยัง
//					List<Receipt> listreceipt =  (List<Receipt>) this.receiptDAO.findReceiptByRegId(reg.getRegId());
//					//ยังไม่จ่าย
//					if(listreceipt.isEmpty())
//					{
//						ReceiptDTO param = new ReceiptDTO();
//						param.setRegId(reg.getRegId());
//						param.setTraderId(trader.getTraderId());
//						
//						ReceiptDTO receiptDTO = (ReceiptDTO)this.accountService.getPayers(param, user);
//						receiptDTO.setReceiptName(payment.getDCustName() + "("+receiptDTO.getReceiptName()+")");
//						receiptDTO.setReceiveOfficerDate(df.format(payment.getDTranDate()));
//						receiptDTO.setReceiveOfficerName(user.getUserData().getUserFullName());
//						receiptDTO.setAuthority(user.getUserData().getUserFullName());
//						
//						ObjectUtil.copy(reg, receiptDTO);
//						ObjectUtil.copy(trader, receiptDTO);
//						
//						//tour get guarantee 
//						if(TraderType.TOUR_COMPANIES.getStatus().equals(trader.getTraderType()))
//						{
//							List<Guarantee> listGuarantee = (List<Guarantee>) this.guaranteeDAO.findByTrader(trader.getLicenseNo(), trader.getTraderType(), 0);
//							
//							if(!listGuarantee.isEmpty())
//							{
//								Guarantee guarantee = listGuarantee.get(0);
//								ObjectUtil.copy(guarantee, receiptDTO);
//							}
//						}
//						
//						ReceiptDetailDTO receiptDtlDTO = new ReceiptDetailDTO();
//						receiptDtlDTO.setRegId(reg.getRegId());
//						receiptDtlDTO.setTraderType(trader.getTraderType());
//						receiptDtlDTO.setRegistrationType(reg.getRegistrationType());
//						
//						
//						List<ReceiptDetailDTO> listReceiptDetailDTO =  this.accountService.getReceiptDetailFee(receiptDtlDTO, user);
//						
//						this.accountService.saveReceiptAccount(receiptDTO, listReceiptDetailDTO.toArray(), user);
//						
//						payment.setRecordStatus(ReceiptStatus.WAIT.getStatus());
//						payment.setLastUpdUser(user.getUserName());
//						payment.setLastUpdDtm(new Date());
//						
//						this.ktbPaymentDAO.update(payment);
//						
//					}
//					
//				}
//			}
//		}
//	}
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
	public void upload(KtbPaymentDTO param, MultipartFile file, User user)
			throws Exception {
		
		this.savePayment(param, file, user);
//		this.updateReceipt(user);	
		
		//send receipt mail
	}
	
	public List<KtbPaymentDTO> findAttachPaymentFiles(String progressDate)throws Exception {
		List<KtbPaymentDTO> list = new ArrayList<KtbPaymentDTO>();
		
		List<Object[]> listPay = this.ktbPaymentDAO.findAttachPaymentFiles(progressDate);
		if(CollectionUtils.isNotEmpty(listPay)){
			for(Object[] obj:listPay ){
				
				KtbPaymentDTO dto = new KtbPaymentDTO();
				if(StringUtils.isNotEmpty(obj[0].toString())){
					Date proDate = DateUtils.getProcessDateFormatEn().parse(obj[0].toString());
					String proDateThai = DateUtils.getProcessDateFormatThai().format(proDate);
					dto.setProgressDate(proDateThai);
				}
				if(StringUtils.isNotEmpty(obj[1].toString())){
					if("T".equals(obj[1].toString())){
						dto.setRecordStatusText("รอรับชำระ");
						dto.setRecordStatus("T");
					} else if("N".equals(obj[1].toString())){
						dto.setRecordStatusText("ชำระแล้ว");
						dto.setRecordStatus("N");
					}
				}
				if(null != obj[2]){
					dto.setDTranAmt(new BigDecimal(obj[2].toString()));
				}
//				if(null != obj[3]){
//					List<AdmUser> listUser = (List<AdmUser>)this.admUserDAO.findByLoginUser(obj[3].toString());
//					
//					if(CollectionUtils.isNotEmpty(listUser)){
//						AdmUser user = listUser.get(0);
//						String prefixName = "";
//						if(null != user.getMasPrefix()){
//							Hibernate.initialize(user.getMasPrefix());
//							prefixName = user.getMasPrefix().getPrefixName();
//						}
//						String personName = prefixName + user.getUserName() +" " + user.getUserLastname();
//						dto.setCreateUser(personName);
//					}
//					
//				}
				if(StringUtils.isNotEmpty(obj[3].toString())){
					dto.setPaymentNo(obj[3].toString());
				}
				list.add(dto);
			}
		}
		
		return list;
	}
	
	
	
//	public List<KtbPaymentDTO> findAttachPaymentFilesDetail(String progressDate, String paymentNo)throws Exception {
	public List<KtbPaymentDTO> findAttachPaymentFilesDetail(KtbPaymentDTO param)throws Exception {
		List<KtbPaymentDTO> list = new ArrayList<KtbPaymentDTO>();
		
		List<Object[]> listPay = this.ktbPaymentDAO.findAttachPaymentFilesDetail(param);
		if(CollectionUtils.isNotEmpty(listPay)){
			for(Object[] obj:listPay ){
				
				KtbPaymentDTO dto = new KtbPaymentDTO();
				if(StringUtils.isNotEmpty(obj[0].toString())){
					dto.setDReference1(obj[0].toString());
				}
				if(StringUtils.isNotEmpty(obj[1].toString())){
					dto.setDReference2(obj[1].toString());
				}
				if(StringUtils.isNotEmpty(obj[2].toString())){
					Date proDate = DateUtils.getProcessDateFormatEn().parse(obj[2].toString());
					String proDateThai = DateUtils.getProcessDateFormatThai().format(proDate);
					dto.setProgressDate(proDateThai);
				}
				if(StringUtils.isNotEmpty(obj[3].toString())){
					if("T".equals(obj[3].toString())){
						dto.setRecordStatusText("รอรับชำระ");
						dto.setRecordStatus("T");
					} else if("N".equals(obj[3].toString())){
						dto.setRecordStatusText("ชำระแล้ว");
						dto.setRecordStatus("N");
					}
				}
				if(null != obj[4]){
					dto.setDTranAmt(new BigDecimal(obj[4].toString()));
				}
				if(StringUtils.isNotEmpty(obj[5].toString())){
					String regType = RegistrationType.getMeaning(obj[7].toString(), obj[5].toString()) + " ใบอนุญาต" + TraderType.getMeaning(obj[7].toString());
					dto.setFeeRegistrationType(regType);
				}
				if(StringUtils.isNotEmpty(obj[6].toString())){
					dto.setPersonName(obj[6].toString());
				}
				if(null != obj[8]){
					dto.setKtbPayId(Long.valueOf(obj[8].toString()));
				}
				if(null != obj[9]){
					Organization org =  (Organization)this.organizationDAO.findByPrimaryKey(Long.valueOf(obj[9].toString()));
					dto.setOrgName(org.getOrgName());
				}
				list.add(dto);
			}
		}
		
		return list;
	}
	
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
	
//	public void savePaymentDetail(KtbPaymentDTO param)throws Exception {
	public void savePaymentDetail(KtbPaymentDTO[] object)throws Exception {
		
//		List<KtbPaymentDTO> list = this.findAttachPaymentFilesDetail(param);
		
//		System.out.println("KtbPaymentDTO[] = " + object.length);
		if(ArrayUtils.isNotEmpty(object)){
			for(KtbPaymentDTO dto:object ){
				
				DotTrBillPayment bill =  this.dotTrBillPaymentDAO.findBillPaymentByBillNo(dto.getDReference1(), dto.getDReference2());
				DotTrFeeLicense trFee = bill.getDotTrFeeLicense();
				Hibernate.initialize(trFee.getDotLicenseOwner());
				DotLicenseOwner owner = trFee.getDotLicenseOwner();
				if(null != bill){
					// จ่ายเงินครั้งเดียวเท่านั้น
					List<KtbPayment> listPaidments = (List<KtbPayment>)this.ktbPaymentDAO.findByRef(dto.getDReference1(), dto.getDReference2());
					
					if(CollectionUtils.isNotEmpty(listPaidments)){
						KtbPayment paidMent = listPaidments.get(0);
						
						DotTrFeePayment feePay = this.dotTrFeePaymentDAO.findTrFeePaymentByBill(bill.getBillPaymentId());
						
						BigDecimal fee = feePay.getTotalFee()==null?new BigDecimal(0):feePay.getTotalFee();
						BigDecimal interest = feePay.getTotalInterest()==null?new BigDecimal(0):feePay.getTotalInterest();
						
						
						BigDecimal totalPaid = paidMent.getDTranAmt()==null?new BigDecimal(0):paidMent.getDTranAmt();
						BigDecimal interestPaid = fee.subtract(totalPaid);
						BigDecimal feePaid = totalPaid.subtract(interestPaid);
						feePay.setTotalFeePaid(feePaid);
						feePay.setTotalInterestPaid(interestPaid);
						feePay.setPaymentDate(paidMent.getDTranDate());
						feePay.setBankPaymentNo(paidMent.getPaymentNo());
						
						feePay.setLastUpdUser("ktbPayment");
						feePay.setLastUpdDtm(new Date());
						this.dotTrFeePaymentDAO.update(feePay);
						
						BigDecimal totalFee = fee.add(interest);
						BigDecimal isFeeZero =  totalFee.subtract(totalPaid);
						isFeeZero = isFeeZero.divide(new BigDecimal(1), 2, BigDecimal.ROUND_HALF_UP);
						
						//isFeeZero == 0 ชำระครบ
						//isFeeZero < 0 ชำระเกิน
						//isFeeZero > 0 ชำระขาด
						if(isFeeZero.compareTo(new BigDecimal(0)) == -1){
							
							trFee.setPaymentStatus("3");
							
						}else{
							if(isFeeZero.compareTo(new BigDecimal(0)) == 1){
								trFee.setPaymentStatus("2");
							}else{
								trFee.setPaymentStatus("4");
							}
						}
						
						trFee.setLastUpdUser("ktbPayment");
						trFee.setLastUpdDtm(new Date());
						trFee.setLicenseFeeStatus(LicenseFeeStatus.PAID.getStatus());
						this.dotTrFeeLicenseDAO.update(trFee);
						paidMent.setRecordStatus("N");
						paidMent.setLastUpdUser("ktbPayment");
						paidMent.setLastUpdDtm(new Date());
						this.ktbPaymentDAO.update(paidMent);
						
						Hibernate.initialize(trFee.getRegistration());
						Registration reg = trFee.getRegistration();
						reg.setRegistrationProgress(RoleStatus.PID.getStatus());
						Hibernate.initialize(reg.getTrader());
						Trader trader = reg.getTrader();
						
						Hibernate.initialize(trader.getOrganization());
						String bookNo = this.masRunningNoDAO.getAccountBookNo(trader.getOrganization().getOrgId(), trader.getTraderType());
						String receiptNo = this.masRunningNoDAO.getAccountReceiptNo(trader.getOrganization().getOrgId(), trader.getTraderType());
						
						Receipt receipt = new Receipt();
						
						receipt.setBookNo(bookNo);
						receipt.setReceiptNo(receiptNo);
						receipt.setReceiptStatus(ReceiptStatus.WAIT.getStatus());
						receipt.setReceiveOfficerDate(paidMent.getDTranDate());
						receipt.setReceiptName(owner.getOwnerFullName());
						receipt.setReceiptRemark("รอพิมพ์ใบเสร็จ");
						receipt.setRecordStatus(RecordStatus.NORMAL.getStatus());
						receipt.setCreateUser("ktbPayment");
						receipt.setCreateDtm(new Date());
						
						Long receiptId = (Long)this.receiptDAO.insert(receipt);
						receipt.setReceiptId(receiptId);
						
						ReceiptMapRegistration mp = new ReceiptMapRegistration();
						mp.setReceipt(receipt);
						mp.setRegistration(reg);
						mp.setReceiptMapDate(receipt.getReceiveOfficerDate());
						
						this.receiptMapRegistrationDAO.insert(mp);
						this.registrationDAO.update(reg);
						
						List<DotTrBillDetail> listBillDtl = (List<DotTrBillDetail>)this.dotTrBillDetailDAO.findBillDetailByPayId(bill.getBillPaymentId());
						
						
						if(CollectionUtils.isNotEmpty(listBillDtl))
						{
							
							for(DotTrBillDetail billFee: listBillDtl){
								ReceiptDetail receiptDetail = new ReceiptDetail();
								
								Hibernate.initialize(billFee.getMasFee());
								receiptDetail.setMasFee(billFee.getMasFee());
								receiptDetail.setFeeName(billFee.getFeeName());
								receiptDetail.setFeeMny(billFee.getFee());
								receiptDetail.setReceipt(receipt);
								receiptDetail.setRecordStatus(RecordStatus.NORMAL.getStatus());
								receiptDetail.setCreateUser("ktbPayment");
								receiptDetail.setCreateDtm(new Date());
								
								receiptDetailDAO.insert(receiptDetail);
							}
						}
					}
				}
				
			}
		}
		
	}
	
//	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
//	public List getAll(Object object, User user)
//			throws Exception {
//		
//		if(!(object instanceof KtbPaymentDTO))
//		{
//	throw new IllegalArgumentException("!(object instanceof KtbPaymentDTO)");
//		}
//		
//		List<KtbPaymentDTO> list = new ArrayList<KtbPaymentDTO>();
//		
//		KtbPaymentDTO param = (KtbPaymentDTO)object;
//		
//		List<KtbPayment> listAll = (List<KtbPayment>) this.ktbPaymentDAO.findSumaryPayment(param.getDTranDate(), null);
//		
//		if(!listAll.isEmpty())
//		{
//			for(KtbPayment paym:listAll)
//			{
//				KtbPaymentDTO dto = new KtbPaymentDTO();
//				ObjectUtil.copy(paym, dto);
//				
//				dto.setTraderType(TraderType.getMeaning(dto.getTraderType()));
//				
//				if(dto.getRecordStatus().equals(ReceiptStatus.WAIT.getStatus()))
//				{
//					dto.setRecordStatus("ชำระเงิน");
//				}
//				
//				if(dto.getRecordStatus().equals(ReceiptStatus.PAY.getStatus()))
//				{
//					dto.setRecordStatus("พิมพ์ใบเสร็จแล้ว");
//				}
//				
//				if(dto.getRecordStatus().equals(ReceiptStatus.TEMP.getStatus()))
//				{
//					dto.setRecordStatus("ไม่พบข้อมูล");
//				}
//				
//				list.add(dto);
//			}
//		}
//		
//		return list;
//	}
//	@SuppressWarnings("unchecked")
//	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
//	public List getPrintPayment(Object object, User user) throws Exception {
//
//		ReceiptDTO params = (ReceiptDTO) object;
//
//		List<KtbPayment> listAll = (List<KtbPayment>) this.ktbPaymentDAO.findSumaryPayment(params.getReceiptDateTo(), ReceiptStatus.WAIT.getStatus());
//		
//		List<Map<String, ?>> receiptSlips = new ArrayList<Map<String, ?>>();
//		
//		List<EmailDTO> emails = new ArrayList<EmailDTO>();
//		
//		BufferedReader reader = new BufferedReader(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("MailApproveForm.html"),"utf-8"));
//		
//		StringBuffer body;
//		body = new StringBuffer();
//
//		String t =  reader.readLine();
//		while( t!=null  ){
//			body.append(t);
//			t =  reader.readLine();
//	    }
//		
//		if(!listAll.isEmpty())
//		{
//			for(KtbPayment payment: listAll)
//			{
//				String ref1 = payment.getDReference1();
//				String registrationType = ref1.substring(ref1.length()-1,ref1.length());
//				registrationType = RegistrationType.getRegTypeByCode(payment.getTraderType(), registrationType);
//								
//				List<Registration> listReg = (List<Registration>)this.ktbPaymentDAO.findRegistrationByLicenseNo(payment.getLicenseNo()
//						, payment.getTraderType(), registrationType, RecordStatus.NORMAL.getStatus(), RecordStatus.NORMAL.getStatus(), params.getOrgId());
//				
//				if(!listReg.isEmpty())
//				{
//					Registration reg = listReg.get(0);
//			
//					List<Receipt> listreceipt =  (List<Receipt>) this.receiptDAO.findReceiptByRegId(reg.getRegId());
//					
//					if(!listreceipt.isEmpty())
//					{
//						for(Receipt receipt: listreceipt)
//						{
//													
//														
//							params.setReceiptId(receipt.getReceiptId());
//							
//							
//							//พิมพ์ใบเสร็จที่ชำระผ่าน ธนาคารกรุงไทย
//							Receipt rpt = (Receipt)this.receiptDAO.findByPrimaryKey(params.getReceiptId());
//							
//							AdmUser author = (AdmUser)this.admUserDAO.findByPrimaryKey(Long.valueOf(params.getAuthority()));
//							String authoritName = author.getMasPrefix().getPrefixName() + author.getUserName()+" "+author.getUserLastname();
//							
//							AdmUser rOfficee = (AdmUser)this.admUserDAO.findByPrimaryKey(Long.valueOf(params.getReceiveOfficerName()));
//							String rName = rOfficee.getMasPrefix().getPrefixName() + rOfficee.getUserName()+" "+rOfficee.getUserLastname();
//							
//							rpt.setAuthority(authoritName);
//							rpt.setReceiveOfficerName(rName);
//							
//							rpt.setLastUpdUser(user.getUserName());
//							rpt.setLastUpdDtm(new Date());
//							this.receiptDAO.update(rpt);
//							
//							Map receiptMap = this.printReceiptService.getPrintReceiptSlip(params, user);
//							
//							receiptMap.put("receiptOfficerName", rName);
//							receiptMap.put("signature", author.getSignature()==null?"":author.getSignature());
//							receiptMap.put("authority", authoritName);
//							
//							receiptSlips.add(receiptMap);
//							
//						}
//						
//						payment.setRecordStatus(ReceiptStatus.PAY.getStatus());
//						payment.setLastUpdUser(user.getUserName());
//						payment.setLastUpdDtm(new Date());
//						this.ktbPaymentDAO.update(payment);
//						
//						
//						Trader trader = reg.getTrader();
//						List<String> listTradress  = this.traderAddressDAO.findAllEmaiByTrader(trader.getTraderId());
//						
//						
//						if(!listTradress.isEmpty())
//						{
//							String email = (String)listTradress.get(0);
//							Person person = trader.getPerson();
//							
//							String traderName = person.getFirstName();
//						    if(TraderType.TOUR_COMPANIES.getStatus().equals(trader.getTraderType()))
//						    {
//						    	  traderName = trader.getTraderName();
//						    }
//						    else
//						    {
//						    	  traderName = person.getMasPrefix()==null?"":person.getMasPrefix().getPrefixName() + person.getFirstName() +" "+ person.getLastName();
//						    }
//						    
//						    EmailDTO regDTO = new EmailDTO();
//						    
//						
//						    regDTO.setEmail(email);						    
//						    String message = MessageFormat.format(body.toString(), trader.getLicenseNo(), traderName, "ได้รับเงินเรียบร้อยแล้ว กรุณานำหลักฐานมารับใบอนุญาตภายใน 7 วัน");
//							regDTO.setMessage(message);
//						    
//						    emails.add(regDTO);
//						    
//						    
//						    		
//						}
//						
//					}
//				}
//				
//				
//			}//for(KtbPayment payment: listAll)
//			
//			//send mail 			
//			SendMailThread mailThred = new SendMailThread(emails, body);
//			mailThred.start();
//		}
//		
//		return receiptSlips;
//	}

	public Date stringToDate(String date,  String time){
		
		try{
//			String s = "03/24/2013 21:54";
//			 String date = "18062015";
//			 String time = "082941";
			 String day = date.substring(0, 2);
			 String month = date.substring(2, 4);
			 String year = date.substring(4, 8);
			 String hh = time.substring(0, 2);
			 String mm = time.substring(2, 4);
			 String ss = time.substring(4, 6);
			 
			 String payDate =  year + "-"+ month +"-"+ day+" "+hh+":"+mm+":"+ss;
//			 System.out.println("yyyy-MM-dd HH:mm:ss " + year + "-"+ month +"-"+ day+" "+hh+":"+mm+":"+ss);
		     SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		     
//		     System.out.println(sf.parse(payDate));
		     
		     return sf.parse(payDate);
		}catch(Exception e){
			e.printStackTrace();
			return new Date();
		}
		 
	}


	
}
