package sss.dot.tourism.service.mas;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.AdmUserDAO;
import sss.dot.tourism.dao.mas.CountryDAO;
import sss.dot.tourism.dao.mas.ForeignLanguageDAO;
import sss.dot.tourism.dao.mas.MasAmphurDAO;
import sss.dot.tourism.dao.mas.MasBankDAO;
import sss.dot.tourism.dao.mas.MasComplaintTypeDAO;
import sss.dot.tourism.dao.mas.MasEducationLevelDAO;
import sss.dot.tourism.dao.mas.MasPosfixDAO;
import sss.dot.tourism.dao.mas.MasPositionDAO;
import sss.dot.tourism.dao.mas.MasPrefixDAO;
import sss.dot.tourism.dao.mas.MasProvinceDAO;
import sss.dot.tourism.dao.mas.MasTambolDAO;
import sss.dot.tourism.dao.mas.MasUniversityDAO;
import sss.dot.tourism.dao.mas.OrganizationDAO;
import sss.dot.tourism.dao.training.EducationDAO;
import sss.dot.tourism.domain.AdmUser;
import sss.dot.tourism.domain.Country;
import sss.dot.tourism.domain.Education;
import sss.dot.tourism.domain.MasAmphur;
import sss.dot.tourism.domain.MasBank;
import sss.dot.tourism.domain.MasComplaintType;
import sss.dot.tourism.domain.MasEducationLevel;
import sss.dot.tourism.domain.MasPosfix;
import sss.dot.tourism.domain.MasPosition;
import sss.dot.tourism.domain.MasPrefix;
import sss.dot.tourism.domain.MasProvince;
import sss.dot.tourism.domain.MasTambol;
import sss.dot.tourism.domain.MasUniversity;
import sss.dot.tourism.domain.Organization;
import sss.dot.tourism.dto.mas.AdmUserDTO;
import sss.dot.tourism.dto.mas.CountryDTO;
import sss.dot.tourism.dto.mas.ForeignLanguageDTO;
import sss.dot.tourism.dto.mas.MasAmphurDTO;
import sss.dot.tourism.dto.mas.MasBankDTO;
import sss.dot.tourism.dto.mas.MasComplaintTypeDTO;
import sss.dot.tourism.dto.mas.MasEducationLevelDTO;
import sss.dot.tourism.dto.mas.MasPosfixDTO;
import sss.dot.tourism.dto.mas.MasPositionDTO;
import sss.dot.tourism.dto.mas.MasPrefixDTO;
import sss.dot.tourism.dto.mas.MasProvinceDTO;
import sss.dot.tourism.dto.mas.MasTambolDTO;
import sss.dot.tourism.dto.mas.MasUniversityDTO;
import sss.dot.tourism.dto.mas.OrganizationDTO;
import sss.dot.tourism.dto.training.EducationDTO;
import sss.dot.tourism.util.ObjectUtil;

import com.sss.aut.service.User;


@Service("comboService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ComboService implements IComboService {
	
	@Autowired
	private MasPrefixDAO masPrefixDAO;
	
	@Autowired
	private MasPosfixDAO masPosfixDAO;
	
	@Autowired
	MasProvinceDAO masProvinceDAO;
	
	@Autowired
	MasAmphurDAO masAmphurDAO;
	
	@Autowired
	MasTambolDAO masTambolDAO;
	
	@Autowired
	MasUniversityDAO masUniversityDAO;
	
	@Autowired
	ForeignLanguageDAO foreignLanguageDAO;
	
	@Autowired
	CountryDAO countryDAO;
	
	@Autowired
	MasEducationLevelDAO masEducationLevelDAO;
	
	
	@Autowired
	MasBankDAO masBankDAO;
	
	@Autowired 
	EducationDAO educationDAO;
	
	@Autowired 
	OrganizationDAO organizationDAO;
	
	@Autowired 
	MasComplaintTypeDAO complaintTypeDAO;
	
	@Autowired
	AdmUserDAO admUserDAO;
	
	@Autowired
	MasPositionDAO masPositionDAO;

	public List getPrefix(Object obj) throws Exception {
		
		if(!(obj instanceof MasPrefixDTO))
		{
			throw new IllegalArgumentException("argument is not instance of MasPrefixDTO");  
		}
		
		MasPrefixDTO param = new MasPrefixDTO();
		if(obj != null)
		{
			param = (MasPrefixDTO)obj;
		}

		
		List<MasPrefix> listAll = (List<MasPrefix>)this.masPrefixDAO.findAll(param);
		
		List<MasPrefixDTO> list = new ArrayList<MasPrefixDTO>();
		
		if(!listAll.isEmpty())
		{
			for(MasPrefix prefix: listAll)
			{
				MasPrefixDTO dto = new MasPrefixDTO();
				
				ObjectUtil.copy(prefix, dto);
				
				list.add(dto);
				
			}
		}
		
		return list;
	}
	

	public List getPosition(Object obj) throws Exception {
		
		if(!(obj instanceof MasPositionDTO))
		{
			throw new IllegalArgumentException("argument is not instance of MasPositionDTO");  
		}
		
		MasPositionDTO param = new MasPositionDTO();
		if(obj != null)
		{
			param = (MasPositionDTO)obj;
		}

		
		List<MasPosition> listAll = (List<MasPosition>)this.masPositionDAO.findAll(param);
		
		List<MasPositionDTO> list = new ArrayList<MasPositionDTO>();
		
		if(!listAll.isEmpty())
		{
			for(MasPosition prefix: listAll)
			{
				MasPositionDTO dto = new MasPositionDTO();
				
				ObjectUtil.copy(prefix, dto);
				
				list.add(dto);
				
			}
		}
		
		return list;
	}
	
	public List getPostfix () throws Exception {
		
		List<MasPosfix> listAll = (List<MasPosfix>) this.masPosfixDAO.findAll();
		
		List<MasPosfixDTO> list = new ArrayList<MasPosfixDTO>();
		
		if(!listAll.isEmpty())
		{
			for(MasPosfix postfix: listAll)
			{
				MasPosfixDTO dto = new MasPosfixDTO();
				
				ObjectUtil.copy(postfix, dto);
				
				list.add(dto);
			}
		}
		
		return list;
	}

	public List getProvince(Object obj) throws Exception {
		
		if(!(obj instanceof MasProvinceDTO))
		{
			throw new IllegalArgumentException("argument is not instance of MasProvinceDTO");  
		}
		
		MasProvinceDTO param = new MasProvinceDTO();
		if(obj != null)
		{
			param = (MasProvinceDTO)obj;
		}
		
		List<MasProvince> listAll = (List<MasProvince>) this.masProvinceDAO.findAll(param);
		
		List<MasProvinceDTO> list = new ArrayList<MasProvinceDTO>();
		
		if(!listAll.isEmpty())
		{
			for(MasProvince postfix: listAll)
			{
				MasProvinceDTO dto = new MasProvinceDTO();
				
				ObjectUtil.copy(postfix, dto);
				
				list.add(dto);
			}
		}
		
		return list;
	}

	public List getAmphur(Object obj) throws Exception {
		
		if(!(obj instanceof MasAmphurDTO))
		{
			throw new IllegalArgumentException("argument is not instance of MasAmphurDTO");  
		}
		
		MasAmphurDTO param = new MasAmphurDTO();
		if(obj != null)
		{
			param = (MasAmphurDTO)obj;
		}
		
		List<MasAmphur> listAll = (List<MasAmphur>)this.masAmphurDAO.findAll(param);
		List<MasAmphurDTO> list = new ArrayList<MasAmphurDTO>();
		
		if(!listAll.isEmpty())
		{
			for(MasAmphur amphur : listAll)
			{
				MasAmphurDTO dto = new MasAmphurDTO();
				ObjectUtil.copy(amphur, dto);
				
				if(amphur.getMasProvince() != null)
				{
					dto.setProvinceId(amphur.getMasProvince().getProvinceId());
				}
				
				list.add(dto);
			}
		}
		
		return list;
	}

	public List getTambol(Object obj) throws Exception {

		if(!(obj instanceof MasTambolDTO))
		{
			throw new IllegalArgumentException("argument is not instance of MasTambolDTO");  
		}
		
		MasTambolDTO param = new MasTambolDTO();
		if(obj != null)
		{
			param = (MasTambolDTO)obj;
		}
		
		List<MasTambol> listAll = (List<MasTambol>)this.masTambolDAO.findAll(param);
		List<MasTambolDTO> list = new ArrayList<MasTambolDTO>();
		
		if(!listAll.isEmpty())
		{
			for(MasTambol tambol : listAll)
			{
				MasTambolDTO dto = new MasTambolDTO();
				ObjectUtil.copy(tambol, dto);
				
				if(tambol.getMasAmphur() != null)
				{
					dto.setAmphurId(tambol.getMasAmphur().getAmphurId());
				}
				
				list.add(dto);
			}
		}
		
		return list;
	}


	public List getUniversity(Object obj) throws Exception {
		List<MasUniversity> listAll = (List<MasUniversity>) this.masUniversityDAO.findAll();
		
		List<MasUniversityDTO> list = new ArrayList<MasUniversityDTO>();
		
		if(!listAll.isEmpty())
		{
			for(MasUniversity university: listAll)
			{
				MasUniversityDTO dto = new MasUniversityDTO();
				
				ObjectUtil.copy(university, dto);
				
				list.add(dto);
			}
		}
		
		return list;
	}


	
	
	public List getForeignLanguage(Object obj) throws Exception {
		
		if(!(obj instanceof ForeignLanguageDTO))
		{
			throw new IllegalArgumentException("argument is not instance of ForeignLanguageDTO");  
		}
		
		ForeignLanguageDTO param = new ForeignLanguageDTO();
		if(obj != null)
		{
			param = (ForeignLanguageDTO)obj;
		}
		
		List<Country> listAll = (List<Country>) this.foreignLanguageDAO.findAll(param);
		
		List<ForeignLanguageDTO> list = new ArrayList<ForeignLanguageDTO>();
		
		if(!listAll.isEmpty())
		{
			for(Country lang: listAll)
			{
				ForeignLanguageDTO dto = new ForeignLanguageDTO();
				
				ObjectUtil.copy(lang, dto);
				
				list.add(dto);
			}
		}
		
		return list;
	}

	public List getCountry(Object obj) throws Exception {
		if(!(obj instanceof CountryDTO))
		{
			throw new IllegalArgumentException("argument is not instance of CountryDTO");  
		}
		
		CountryDTO param = new CountryDTO();
		if(obj != null)
		{
			param = (CountryDTO)obj;
		}
		
		List<Country> listAll = (List<Country>) this.countryDAO.findAll(param);
		
		List<CountryDTO> list = new ArrayList<CountryDTO>();
		
		if(!listAll.isEmpty())
		{
			for(Country lang: listAll)
			{
				CountryDTO dto = new CountryDTO();
				
				ObjectUtil.copy(lang, dto);
				
				list.add(dto);
			}
		}
		
		return list;
	}
	
	public List getEducationLevel(Object obj) throws Exception {
		if(!(obj instanceof MasEducationLevelDTO))
		{
			throw new IllegalArgumentException("argument is not instance of MasEducationLevelDTO");  
		}
		
//		MasEducationLevelDTO param = new MasEducationLevelDTO();
//		if(obj != null)
//		{
//			param = (MasEducationLevelDTO)obj;
//		}
		
		List<MasEducationLevel> listAll = (List<MasEducationLevel>) this.masEducationLevelDAO.findAll();
		
		List<MasEducationLevelDTO> list = new ArrayList<MasEducationLevelDTO>();
		
		if(!listAll.isEmpty())
		{
			for(MasEducationLevel edu: listAll)
			{
				MasEducationLevelDTO dto = new MasEducationLevelDTO();
				
				ObjectUtil.copy(edu, dto);
				
				list.add(dto);
			}
		}
		
		return list;
	}
	
	
	public List getBank(Object obj) throws Exception {
		if(!(obj instanceof MasBankDTO))
		{
			throw new IllegalArgumentException("argument is not instance of MasBankDTO");  
		}
		
		List<MasBank> listAll = (List<MasBank>) this.masBankDAO.findAll();
		
		List<MasBankDTO> list = new ArrayList<MasBankDTO>();
		
		if(!listAll.isEmpty())
		{
			for(MasBank bank: listAll)
			{
				MasBankDTO dto = new MasBankDTO();
				
				ObjectUtil.copy(bank, dto);
				
				list.add(dto);
			}
		}
		
		return list;
	}

	
	public List getCourse(Object obj) throws Exception {
		
		if(!(obj instanceof EducationDTO))
		{
			throw new IllegalArgumentException("argument is not instance of EducationDTO");  
		}
		EducationDTO educationDto = (EducationDTO)obj;
		
		List<Education> listAll = new ArrayList<Education>();
		
//		System.out.println("MasUniversity: "+ToStringBuilder.reflectionToString(educationDto,ToStringStyle.MULTI_LINE_STYLE));
		
		Boolean isConcatUniversityName = 0==educationDto.getMasUniversityId()?true:false;
		
		if(!isConcatUniversityName){
			
			MasUniversity university = this.masUniversityDAO.findByPrimarykey(MasUniversity.class,educationDto.getMasUniversityId());
			listAll = (List<Education>) this.educationDAO.findCourseByUniversity(university);
		}else{
			listAll = (List<Education>) this.educationDAO.findCourseAll();
		}
		
		List<EducationDTO> list = new ArrayList<EducationDTO>();
		
		String courseName = "";
		String universityName = "";
		if(!listAll.isEmpty())
		{
			for(Education edu: listAll)
			{
				EducationDTO dto = new EducationDTO();
				
				ObjectUtil.copy(edu, dto);
				//ObjectUtil.copy(edu.getMasUniversity(), dto);
				
				if(edu.getMasUniversity() != null)
				{
					dto.setMasUniversityId(edu.getMasUniversity().getMasUniversityId());
				}
				

				if(dto.getGraduationCourse() != null && !dto.getGraduationCourse().equals(""))
				{
					courseName = courseName.concat(dto.getGraduationCourse());
				}
				
				if(dto.getGenerationGraduate() != null && !dto.getGenerationGraduate().equals(""))
				{
					courseName = courseName.concat(" รุ่นที่ "+dto.getGenerationGraduate().toString());
				}
				
				if(isConcatUniversityName){
					if(dto.getMasUniversityId() > 0)
					{
						List<MasUniversity> listAllName = (List<MasUniversity>) this.masUniversityDAO.findUniversityName(dto.getMasUniversityId());
						
						List<MasUniversityDTO> listname = new ArrayList<MasUniversityDTO>();
						
						for(MasUniversity listAlluni: listAllName)
						{
							MasUniversityDTO uniDto = new MasUniversityDTO();
							
							ObjectUtil.copy(listAlluni, uniDto);
							
							universityName = uniDto.getUniversityName();
							
						}	
						
						courseName = courseName.concat(" "+universityName);
					}
					
				}
				
				dto.setCourseName(courseName);
				courseName = "";
				list.add(dto);	
			}	
			
			
		}
		
		return list;
	}
	
	
	
	public List getOrganization(Object obj) throws Exception {
		List<Organization> listAll = (List<Organization>) this.organizationDAO.findAll();
		
		List<OrganizationDTO> list = new ArrayList<OrganizationDTO>();
		
		if(!listAll.isEmpty())
		{
			for(Organization organization: listAll)
			{
				OrganizationDTO dto = new OrganizationDTO();
				
				ObjectUtil.copy(organization, dto);
				
				list.add(dto);
			}
		}
		
		return list;
	}

	
	public List getComplaintType(Object obj) throws Exception {
		List<MasComplaintType> listAll = (List<MasComplaintType>) this.complaintTypeDAO.findAll();
		
		List<MasComplaintTypeDTO> list = new ArrayList<MasComplaintTypeDTO>();
		
		if(!listAll.isEmpty())
		{
			for(MasComplaintType complaintType: listAll)
			{
				MasComplaintTypeDTO dto = new MasComplaintTypeDTO();
				
				ObjectUtil.copy(complaintType, dto);
				
				list.add(dto);
			}
		}
		
		return list;
	}
	
	public List getAdmUser(Object obj) throws Exception {
		List<AdmUser> listAll = (List<AdmUser>) this.admUserDAO.findAll();
		
		List<AdmUserDTO> list = new ArrayList<AdmUserDTO>();
		
		if(!listAll.isEmpty())
		{
			for(AdmUser admUser: listAll)
			{
				AdmUserDTO dto = new AdmUserDTO();
				
				ObjectUtil.copy(admUser, dto);
				
				String userFullName = ((MasPrefix)this.masPrefixDAO.findByPrimaryKey
						(admUser.getMasPrefix().getPrefixId())).getPrefixName()
						+admUser.getUserName()+" "+admUser.getUserLastname();
				dto.setUserFullName(userFullName);
				
				list.add(dto);
			}
		}
		
		return list;
	}

	public List getOfficerGroup(Object obj, User user) throws Exception {
		
		if(!(obj instanceof AdmUserDTO))
		{
			throw new IllegalArgumentException("argument is not instance of AdmUserDTO");  
		}
		
		AdmUserDTO param = (AdmUserDTO)obj;
		
		List<AdmUser> listAll = (List<AdmUser>) this.admUserDAO.findOfficerByGroupRole(param.getGroupRole(), user.getUserData().getOrgId());
		
		List<AdmUserDTO> list = new ArrayList<AdmUserDTO>();
		
		if(!listAll.isEmpty())
		{
			for(AdmUser admUser: listAll)
			{
				AdmUserDTO dto = new AdmUserDTO();
				
				ObjectUtil.copy(admUser, dto);
				
				String userFullName = ((MasPrefix)this.masPrefixDAO.findByPrimaryKey
						(admUser.getMasPrefix().getPrefixId())).getPrefixName()
						+admUser.getUserName()+" "+admUser.getUserLastname();
				dto.setUserFullName(userFullName);
				
				list.add(dto);
			}
		}
		
		return list;
	}
	
	
	

}
