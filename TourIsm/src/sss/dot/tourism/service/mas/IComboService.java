package sss.dot.tourism.service.mas;

import java.util.List;

import com.sss.aut.service.User;

public interface IComboService {
	
	public List getPrefix(Object obj) throws Exception;
	public List getPostfix() throws Exception;
	
	public List getProvince(Object obj) throws Exception;
	
	public List getAmphur(Object obj) throws Exception;
	public List getTambol(Object obj) throws Exception;
	
	public List getUniversity(Object obj) throws Exception;
	public List getForeignLanguage(Object obj) throws Exception;
	
	public List getCountry(Object obj) throws Exception;
	
	public List getEducationLevel(Object obj) throws Exception;

	public List getBank(Object obj) throws Exception;
	
	public List getCourse(Object obj) throws Exception;
	
	public List getOrganization(Object obj) throws Exception;
	
	public List getComplaintType(Object obj) throws Exception;
	
	public List getAdmUser(Object obj) throws Exception;
	
	public List getOfficerGroup(Object obj, User user) throws Exception;
	
	public List getPosition(Object obj) throws Exception;
}
