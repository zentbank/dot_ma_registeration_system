package sss.dot.tourism.service.mas;

import java.util.List;

public interface IOrganizationService {
	public List getAll(Object obj) throws Exception;
	public List create(Object[] object) throws Exception;
}
