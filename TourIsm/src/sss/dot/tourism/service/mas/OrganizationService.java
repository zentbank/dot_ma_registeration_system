package sss.dot.tourism.service.mas;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.mas.OrganizationDAO;
import sss.dot.tourism.domain.Organization;
import sss.dot.tourism.dto.mas.OrganizationDTO;
import sss.dot.tourism.util.ObjectUtil;

@Service("organizationService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class OrganizationService implements IOrganizationService{

	@Autowired
	private OrganizationDAO organizationDAO;


	
	public List getAll(Object obj) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}



	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List create(Object[] object) throws Exception {

		List<OrganizationDTO> list = new ArrayList<OrganizationDTO>();
		for (Object obj : object) {
			OrganizationDTO org = (OrganizationDTO) obj;
			
			Organization persistence = new Organization();
			
			ObjectUtil.copy(org, persistence);
			
			persistence.setRecordStatus("N");
			persistence.setCreateUser("Admin");
			persistence.setCreateDtm(new Date());
			
			Long orgId = (Long) this.organizationDAO.insert(persistence);
			org.setOrgId(orgId);

			list.add(org);
		}

		return list;
	}
}
