package sss.dot.tourism.service.menu;

import java.util.List;

import com.sss.aut.service.User;

public interface IMenuService {
	
	public List getMenu(User user) throws Exception;

}
