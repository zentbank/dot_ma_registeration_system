package sss.dot.tourism.service.menu;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.AdmMenuDAO;
import sss.dot.tourism.dao.AdmScreenDAO;
import sss.dot.tourism.domain.AdmScreen;
import sss.dot.tourism.dto.menu.Node;

import com.sss.aut.service.User;

@Repository("menuService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class MenuService implements IMenuService {

	@Autowired
	AdmMenuDAO admMenuDAO;
	
	@Autowired
	AdmScreenDAO admScreenDAO;
	
	public List getMenu(User user) throws Exception {
		
		List<Node> list = new ArrayList<Node>();
		
		Node root = new Node();
		
		root.setText(user.getUserData().getUserFullName());
		root.setExpanded(true);
		
		List<Object[]> listAll = this.admMenuDAO.findRootMenu(user.getUserId());
		
		if(!listAll.isEmpty())
		{
			for(Object[] obj: listAll)
			{
				AdmScreen screen =  (AdmScreen)this.admScreenDAO.findByPrimaryKey(Long.valueOf(obj[0].toString()));
				
				Node rootNode = new Node();
				rootNode.setNodeId(screen.getScreenId());
				rootNode.setText(screen.getMenuText());
				getRecursive(rootNode);
				
				root.addChild(rootNode);
				
			}
		}
		
		list.add(root);
		
		
		return list;
	}
	
	  private void getRecursive(Node rootNode) throws Exception
	  {

	    List<AdmScreen> children = admScreenDAO.findChildren(rootNode.getNodeId());
	    List<Node> childElements = new ArrayList<Node>();
	    if (children.isEmpty())
	    {
	      rootNode.changeToLeaf();
	    }
	    for (AdmScreen child: children)
	    {

	      Node childElement = new Node();
	      childElement.setNodeId(child.getScreenId());
	      childElement.setText(child.getMenuText());
	      childElement.setMenuId(child.getMenuId()==null?"":child.getMenuId());
	      childElements.add(childElement);
	      getRecursive(childElement);
	    }
	    rootNode.addChildren(childElements);
	  }

	
	
}
