package sss.dot.tourism.service.printaddress;

import java.util.List;

import com.sss.aut.service.User;

public interface IPrintAddressService {
	public List getPrintAddressByLicenseNo(Object object, User user) throws Exception;
	
	public List getPrintAddressByLicenseNo(Object[] object) throws Exception;
	
	
	//public void updatePrintAddressById(Object object, User user) throws Exception;
}
