package sss.dot.tourism.service.printaddress;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.mas.MasRunningNoDAO;
import sss.dot.tourism.dao.mas.RegisterFlowDAO;
import sss.dot.tourism.dao.registration.PersonDAO;
import sss.dot.tourism.dao.registration.PlantripDAO;
import sss.dot.tourism.dao.registration.PrintingLicenseDAO;
import sss.dot.tourism.dao.registration.ReceiptDetailDAO;
import sss.dot.tourism.dao.registration.ReceiptMapRegistrationDAO;
import sss.dot.tourism.dao.registration.RegisterProgressDAO;
import sss.dot.tourism.dao.registration.RegistrationDAO;
import sss.dot.tourism.dao.registration.TraderAddressDAO;
import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.domain.Person;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.domain.TraderAddress;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.service.registration.IApproveRegistration;
import sss.dot.tourism.service.registration.IRegisterProgressService;
import sss.dot.tourism.service.registration.IRegistrationService;
import sss.dot.tourism.service.trader.ITourCompaniesService;
import sss.dot.tourism.service.trader.ITourLeaderService;
import sss.dot.tourism.service.trader.ITraderGuideService;
import sss.dot.tourism.util.PersonType;
import sss.dot.tourism.util.RecordStatus;
import sss.dot.tourism.util.TraderAddressType;

import com.sss.aut.service.User;

@Repository("printAddressService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PrintAddressService implements IPrintAddressService{
	@Autowired
	PrintingLicenseDAO printingLicenseDAO;
	@Autowired
	TraderDAO traderDAO;
	@Autowired
	RegisterProgressDAO registerProgressDAO;
	@Autowired
	RegistrationDAO registrationDAO;
	@Autowired
	ITourLeaderService tourLeaderService;
	@Autowired
	RegisterFlowDAO registerFlowDAO;
	@Autowired
	IRegistrationService registrationService;
	@Autowired
	IRegisterProgressService registerProgressService;
	@Autowired
	MasRunningNoDAO masRunningNoDAO;
	@Autowired
	ReceiptMapRegistrationDAO receiptMapRegistrationDAO;
	@Autowired
	ReceiptDetailDAO receiptDetailDAO;
	@Autowired
	IApproveRegistration approveRegistrationService;
	
	
	
	@Autowired
	PersonDAO personDAO;
	@Autowired
	ITourCompaniesService tourCompaniesService;
	@Autowired
	TraderAddressDAO traderAddressDAO;
	@Autowired
	PlantripDAO plantripDAO;
	@Autowired
	ITraderGuideService guideService;

	public List getPrintAddressByLicenseNo(Object object, User user) throws Exception {
	
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		RegistrationDTO params = (RegistrationDTO)object;

		List<Object[]> listObj = this.traderDAO.findTraderBetweenLicenseNo(params, user);
		
		if((!listObj.isEmpty()) && listObj !=null)
		{
			for(Object[] sel: listObj)
			{
				RegistrationDTO dto = new RegistrationDTO();
				
				dto.setTraderId(Long.valueOf(sel[0].toString()));
				
				dto.setPersonId(Long.valueOf(sel[1].toString()));
				
				dto.setTraderName(sel[2]==null?"":sel[2].toString());
				
				dto.setLicenseNo(sel[3]==null?"":sel[3].toString());
				
				Trader trader = (Trader) this.traderDAO.findByPrimaryKey(dto.getTraderId());

//				System.out.println(dto.getTraderId());
//				System.out.println(dto.getPersonId());
				List<TraderAddress> listTraderAddress =  (List<TraderAddress>)this.traderAddressDAO.findAllByTraderPl(dto.getTraderId(),TraderAddressType.SHOP_ADDRESS.getStatus(), RecordStatus.NORMAL.getStatus());
				
				if(!listTraderAddress.isEmpty())
				{
					TraderAddress add = listTraderAddress.get(0);
					StringBuilder traderAddress = new StringBuilder();
				
					traderAddress.append("ตั้งอยู่เลขที่ ");
					traderAddress.append(add.getAddressNo());
					dto.setAddressNo(add.getAddressNo());
					
					//Oat Add 16/10/57 
					if(add.getBuildingName() != null && !add.getBuildingName().equals(""))
					{
						traderAddress.append(" อาคาร ");
						traderAddress.append(add.getBuildingName());
					}
					if(add.getFloor() != null && !add.getFloor().equals(""))
					{
						traderAddress.append(" ชั้น ");
						traderAddress.append(add.getFloor());
					}
					if(add.getRoomNo() != null && !add.getRoomNo().equals("")) 
					{
						traderAddress.append(" ห้องเลขที่ ");
						traderAddress.append(add.getRoomNo());
					}
					if(add.getMoo() != null && !add.getMoo().equals(""))
					{
						traderAddress.append(" หมู่ที่ ");
						traderAddress.append(add.getMoo());
					}
					if(add.getVillageName() != null && !add.getVillageName().equals(""))
					{
						traderAddress.append(" หมู่บ้าน ");
						traderAddress.append(add.getVillageName());
					}
					//End
					
					traderAddress.append(" ถนน ");
					traderAddress.append(add.getRoadName());
					if(add.getRoadName()!=null && !add.getRoadName().isEmpty())
					{
						dto.setRoadName(add.getRoadName());
					}else
					{
						dto.setRoadName("-");
					}
					traderAddress.append(" ตรอก/ซอย ");
					traderAddress.append(add.getSoi());
					if(add.getSoi()!=null &&!add.getSoi().isEmpty())
					{
						dto.setSoi(add.getSoi());	
					}else
					{
						dto.setSoi("-");
					}
				
					if(add.getMasTambol() != null)
					{
						traderAddress.append(" ตำบล/แขวง ");
						traderAddress.append(add.getMasTambol().getTambolName());
						dto.setTambolId(add.getMasTambol().getTambolId());
					}
					if(add.getMasAmphur()!= null)
					{
						traderAddress.append(" อำเภอ/เขต ");
						traderAddress.append(add.getMasAmphur().getAmphurName());
						dto.setAmphurId(add.getMasAmphur().getAmphurId());
					}
					if(add.getMasProvince() != null)
					{
						traderAddress.append(" จังหวัด ");
						traderAddress.append(add.getMasProvince().getProvinceName());
						dto.setProvinceId(add.getMasProvince().getProvinceId());
					}
					
					
					traderAddress.append(" รหัสไปรษณีย์ ");
					traderAddress.append(add.getPostCode()==null?"":add.getPostCode());
					dto.setPostCode(add.getPostCode()==null?"-":add.getPostCode());
					
					
					dto.setTraderAddress(traderAddress.toString());
					System.out.println(traderAddress.toString());
				}
				
				List<Person> listPerson = (List<Person>) this.personDAO.findByPersonId(dto.getPersonId());
				
				if(!listPerson.isEmpty())
				{
					 Person pr = listPerson.get(0);
					 dto.setPersonType(pr.getPersonType());
					 dto.setPrefixName(pr.getMasPrefix()==null?"":pr.getMasPrefix().getPrefixName());
					 StringBuffer traderOwnerName = new StringBuffer();
				     if(PersonType.CORPORATE.getStatus().equals(pr.getPersonType()))
				     {
//				    	 System.out.println(trader.getTraderName());
				    	 traderOwnerName.append(dto.getPrefixName());
				    	 traderOwnerName.append(" ");
				    	 traderOwnerName.append(trader.getTraderName());
				    	 traderOwnerName.append(pr.getMasPosfix().getPostfixName());
//			    		traderOwnerName = dto.getPrefixName() + trader.getTraderName() + pr.getMasPosfix()==null?"":pr.getMasPosfix().getPostfixName();
					
					 }
					 else
					 {
						 dto.setFirstName(pr.getFirstName());
						 dto.setLastName(pr.getLastName());
						 traderOwnerName.append(dto.getPrefixName());
						 traderOwnerName.append(dto.getFirstName());
				    	 traderOwnerName.append(" ");
				    	 traderOwnerName.append(dto.getLastName());
				    	
//						 traderOwnerName = dto.getPrefixName() + dto.getFirstName() + " " + dto.getLastName();
						 				
					 }
				     
				     dto.setTraderOwnerName(traderOwnerName.toString());
				}
				
				list.add(dto);
			}
		}
		
		
		return list;
	}

	public List getPrintAddressByLicenseNo(Object[] object) throws Exception {
		if(!(object instanceof RegistrationDTO[]))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		for(Object param: object)
		{
			RegistrationDTO dto = (RegistrationDTO)param;
			
			List printList = this.getPrintAddressByLicenseNo(dto, null);
			list.addAll(printList);
		}
		
		return list;
	}   

}
