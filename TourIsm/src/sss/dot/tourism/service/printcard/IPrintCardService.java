package sss.dot.tourism.service.printcard;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import sss.dot.tourism.dto.registration.RegistrationDTO;

import com.sss.aut.service.User;

public interface IPrintCardService {

	public List getPrintCard(Object object, User user ,int start, int limit) throws Exception;
	public List countPrintCard(Object object, User user, int start, int limit) throws Exception;
	public List<RegistrationDTO> getPrintCardExcel(Object object, User user) throws Exception;
	public void upload(Object object, MultipartFile file, User user) throws Exception;
	public void updateASPrint(List<RegistrationDTO> list, User user) throws Exception;
	
	public Object getPerson(Object object, User user) throws Exception;
	
	public void saveCardPhoto(Object object, User user) throws Exception;
	
	public String getPhotoBase64Format(Object object, User user) throws Exception;
	public String getPhotoLocation(Object object, User user) throws Exception;
	
	public String getPrepareQRCode(Object object, User user) throws Exception;
	
	public String getDirectorSignature(Object object, User user) throws Exception;
}
