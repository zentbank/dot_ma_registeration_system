package sss.dot.tourism.service.printcard;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import sss.dot.tourism.dao.AdmUserDAO;
import sss.dot.tourism.dao.DotTrFeeLicenseDAO;
import sss.dot.tourism.dao.file.PersonImageDAO;
import sss.dot.tourism.dao.file.RegDocumentDAO;
import sss.dot.tourism.dao.mas.MasDocumentDAO;
import sss.dot.tourism.dao.mas.MasRunningNoDAO;
import sss.dot.tourism.dao.mas.RegisterFlowDAO;
import sss.dot.tourism.dao.registration.PersonDAO;
import sss.dot.tourism.dao.registration.PlantripDAO;
import sss.dot.tourism.dao.registration.PrintingCardDAO;
import sss.dot.tourism.dao.registration.PrintingLicenseDAO;
import sss.dot.tourism.dao.registration.ReceiptDetailDAO;
import sss.dot.tourism.dao.registration.ReceiptMapRegistrationDAO;
import sss.dot.tourism.dao.registration.RegisterProgressDAO;
import sss.dot.tourism.dao.registration.RegistrationDAO;
import sss.dot.tourism.dao.registration.TraderAddressDAO;
import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.domain.AdmUser;
import sss.dot.tourism.domain.DotTrFeeLicense;
import sss.dot.tourism.domain.MasDocument;
import sss.dot.tourism.domain.Person;
import sss.dot.tourism.domain.PersonImg;
import sss.dot.tourism.domain.PrintingCard;
import sss.dot.tourism.domain.RegDocument;
import sss.dot.tourism.domain.Registration;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.dto.uploadpicture.UpLoadPictureDTO;
import sss.dot.tourism.service.file.FileManage;
import sss.dot.tourism.service.file.RegDocumentService;
import sss.dot.tourism.service.registration.IApproveRegistration;
import sss.dot.tourism.service.registration.IRegisterProgressService;
import sss.dot.tourism.service.registration.IRegistrationService;
import sss.dot.tourism.service.trader.ITourCompaniesService;
import sss.dot.tourism.service.trader.ITourLeaderService;
import sss.dot.tourism.service.trader.ITraderGuideService;
import sss.dot.tourism.util.ConstantUtil;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.ImageUtils;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.RecordStatus;
import sss.dot.tourism.util.RegistrationType;
import sss.dot.tourism.util.TraderCategory;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.User;

import fr.opensagres.xdocreport.core.utils.StringUtils;

  
@Repository("printCardService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PrintCardService implements IPrintCardService{

	@Autowired
	PrintingLicenseDAO printingLicenseDAO;
	@Autowired
	TraderDAO traderDAO;
	@Autowired
	RegisterProgressDAO registerProgressDAO;
	@Autowired
	RegistrationDAO registrationDAO;
	@Autowired
	ITourLeaderService tourLeaderService;
	@Autowired
	RegisterFlowDAO registerFlowDAO;
	@Autowired
	IRegistrationService registrationService;
	@Autowired
	IRegisterProgressService registerProgressService;
	@Autowired
	MasRunningNoDAO masRunningNoDAO;
	@Autowired
	ReceiptMapRegistrationDAO receiptMapRegistrationDAO;
	@Autowired
	ReceiptDetailDAO receiptDetailDAO;
	@Autowired
	IApproveRegistration approveRegistrationService;
	@Autowired
	PrintingCardDAO printingCardDAO;
	@Autowired
	PersonImageDAO personImageDAO;
	
	@Autowired
	PersonDAO personDAO;
	@Autowired
	ITourCompaniesService tourCompaniesService;
	@Autowired
	TraderAddressDAO traderAddressDAO;
	@Autowired
	PlantripDAO plantripDAO;
	@Autowired
	ITraderGuideService guideService;
	
	@Autowired
	RegDocumentService regDocumentService;
	
	@Autowired
	RegDocumentDAO regDocumentDAO;
	
	@Autowired
	MasDocumentDAO masDocumentDAO;
	
	@Autowired
	AdmUserDAO admUserDAO;
	
	@Autowired private DotTrFeeLicenseDAO dotTrFeeLicenseDAO;

	
	public List countPrintCard(Object object, User user, int start, int limit) throws Exception
	{
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		

		
		RegistrationDTO params = (RegistrationDTO)object;
		

		return this.printingCardDAO.findPrintCardAll(params, user, start, limit);
	}
	
	public List getPrintCard(Object object, User user, int start, int limit)
			throws Exception 
	{
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		RegistrationDTO params = (RegistrationDTO)object;
		
		List<Object[]> listObj = this.printingCardDAO.findPrintCardAll(params, user, start, limit);
		
		DateFormat dfTH = DateUtils.getProcessDateFormatThai();
		DateFormat dfEN = DateUtils.getProcessDateFormatEn();
		
		if(!listObj.isEmpty())
		{
			for(Object[] sel: listObj)
			{
				RegistrationDTO dto = new RegistrationDTO();
				dto.setPrintCardId(Long.valueOf(sel[0].toString()));
				if(sel[1] != null)
				{
					dto.setPrintCardDate(dfTH.format((Date)sel[1]));
				}
				
				dto.setPrintCardStatus(sel[2]==null?"":sel[2].toString());
				dto.setRegId(Long.valueOf(sel[3].toString()));
				
				Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(dto.getRegId());
				
				dto.setRegistrationType(sel[4].toString());
				dto.setTraderType(sel[17]==null?"":sel[17].toString());
				
				if(sel[17] != null)
				{
					dto.setRegistrationTypeName(RegistrationType.getMeaning(dto.getTraderType(), dto.getRegistrationType()));
					dto.setTraderTypeName(TraderType.getMeaning(dto.getTraderType()));
				}
				if(sel[5] != null)
				{
					dto.setApproveDate(dfTH.format((Date) sel[5]));
				}
				
				dto.setPersonId(Long.valueOf(sel[6].toString()));
				Person person = (Person)this.personDAO.findByPrimaryKey(dto.getPersonId());
				
				ObjectUtil.copy(person, dto);
//				dto.setFirstName(sel[7]==null?"":sel[7].toString());
//				dto.setLastName(sel[8]==null?"":sel[8].toString());
//				dto.setFirstNameEn(sel[9]==null?"":sel[9].toString());
//				dto.setLastNameEn(sel[10]==null?"":sel[10].toString());
//				dto.setIdentityNo(sel[11]==null?"":sel[11].toString());
				dto.setPrefixName(sel[12]==null?"":sel[12].toString());
				dto.setPrefixNameEn(sel[13]==null?"":sel[13].toString());
				
				
				dto.setTraderId(Long.valueOf(sel[14].toString()));
				
				Trader trader = (Trader)this.traderDAO.findByPrimaryKey(dto.getTraderId());
				ObjectUtil.copy(trader, dto);
				
				dto.setTraderOwnerName(dto.getPrefixName() + dto.getFirstName() +" "+ dto.getLastName());
				
							
				dto.setTraderCategoryName(
						TraderCategory.getTraderCategory(
							dto.getTraderType()
							, dto.getTraderCategory()
						).getMeaning()
				);
				
				
				
				
				if(person.getMasAmphurByAmphurId() != null)
				{
					dto.setAmphurId(person.getMasAmphurByAmphurId().getAmphurId());
					dto.setAmphurName(person.getMasAmphurByAmphurId().getAmphurName());
				}
				if(person.getMasAmphurByTaxAmphurId() != null)
				{
					dto.setTaxAmphurId(person.getMasAmphurByTaxAmphurId().getAmphurId());
					dto.setTaxAmphurName(person.getMasAmphurByTaxAmphurId().getAmphurName());
				}
				if(person.getMasProvinceByProvinceId() != null)
				{
					dto.setProvinceId(person.getMasProvinceByProvinceId().getProvinceId());
					dto.setProvinceName(person.getMasProvinceByProvinceId().getProvinceName());
			
				}
				if(person.getMasProvinceByTaxProvinceId() != null)
				{
					dto.setTaxProvinceId(person.getMasProvinceByTaxProvinceId().getProvinceId());
					dto.setTaxProvinceName(person.getMasProvinceByTaxProvinceId().getProvinceName());
				}
				
//				if(trader.getTraderByTraderGuideId() != null)
//				{
//					dto.setTraderGuideId(trader.getTraderByTraderGuideId().getTraderId());
//					dto.setLicenseGuideNo(trader.getLicenseGuideNo());
//					dto.setTraderCategoryGuideType(trader.getTraderByTraderGuideId().get);
//					dto.setTraderCategoryGuideTypeName(TraderCategory.getMeaning(trader.getTraderByTraderGuideId().getTraderType(), trader.getTraderByTraderGuideId().getTraderCategory()));
//					
//				}
				
				if(TraderType.GUIDE.getStatus().equals(dto.getTraderType()))
				{
					guideService.getRegistration(reg, dto, user);
				}
				
				if(TraderType.LEADER.getStatus().equals(dto.getTraderType()))
				{
					tourLeaderService.getRegistration(reg, dto, user);
				}
				
				
				
				long docImg = 0;
				if(TraderType.GUIDE.getStatus().equals(dto.getTraderType()))
				{
					docImg = ConstantUtil.GUIDE_DOC_IMG_ID;
				}
				else
				{
					docImg = ConstantUtil.TOURLEADER_DOC_IMG_ID;
					
					
				}
				
				List<RegDocument> listRecDoc = (List<RegDocument>) this.regDocumentDAO.findAllByTraderAndMasDoc(dto.getTraderId(), docImg);
				
				if(!listRecDoc.isEmpty())
				{
					RegDocument regDoc = listRecDoc.get(0);
					
					dto.setImgAction("P");
					dto.setImgPath(regDoc.getDocPath());
				}
				
				dto.setLicenseNo(sel[15]==null?"":sel[15].toString());
				
//				if(sel[16] != null)
//				{
//					dto.setTraderCategory(sel[16]==null?"":sel[16].toString());
//					dto.setTraderCategoryName(TraderCategory.getMeaning(dto.getTraderType(), dto.getTraderCategory()));
//				}
//				if(sel[18] != null)
//				{
//					dto.setEffectiveDate(dfTH.format((Date) sel[18]));
//				}
//				if(sel[19] != null)
//				{
//					dto.setExpireDate(dfTH.format((Date) sel[19]));
//				}
				
				if(sel[20] !=null)
				{
					dto.setTraderArea(sel[20].toString());
				}
				
				if(sel[21] !=null)
				{
					dto.setProvinceGuideServiceName(sel[21].toString());
				}
				
				String imgUrl =  params.getApplicationContextPath() + "/business/printcard/registration/photo/" + dto.getPrintCardId()+"/"+dto.getLicenseNo();
	             dto.setImgData(imgUrl);
	             
	             
	           //ตรวจสอบว่ายื่น online หรือเปล่า
				List<DotTrFeeLicense> listFee = (List<DotTrFeeLicense>)this.dotTrFeeLicenseDAO.findTrFeeLicenseByRegId(reg.getRegId());
				if(CollectionUtils.isNotEmpty(listFee)){
					dto.setRoleColor("true");
				}
	             
				list.add(dto);
			}
		}
		
		return list;
	}
	

	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void upload(Object object, MultipartFile file, User user)
			throws Exception {
		// TODO Auto-generated method stub
		
		System.out.println("###Service PrintCardService method upload");
		if(!(object instanceof UpLoadPictureDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}

		UpLoadPictureDTO params = (UpLoadPictureDTO)object;
		
		Trader trader = (Trader)this.traderDAO.findByPrimaryKey(params.getTraderId());
		System.out.println(params.getTraderId());
		//TraderType
		String traderTypeName = "";
		if(trader.getTraderType().equals(TraderType.TOUR_COMPANIES.getStatus()))
		{
			traderTypeName = "Business";
		}
		if(trader.getTraderType().equals(TraderType.GUIDE.getStatus()))
		{
			traderTypeName = "Guide";
		}
		if(trader.getTraderType().equals(TraderType.LEADER.getStatus()))
		{
			traderTypeName = "Tourleader";
		}
		
		//LicenseNo
		String licenseNoStr = trader.getLicenseNo();
		licenseNoStr = licenseNoStr.replaceAll("[.]", "");
		licenseNoStr = licenseNoStr.replaceAll("[/]", "");
		licenseNoStr = licenseNoStr.replaceAll("[-]", "");
		
		//RegistrationType
		List<Registration> listReg = (List<Registration>)this.registrationDAO.findAllByTrader(trader.getTraderId());
		String registrationType = (listReg.get(0)).getRegistrationType();
		//RealFileName
		String realFileName = params.getPersonImgId()+"";
		
		//postfix FileName
//		System.out.println("file.getOriginalFilename() = "+file.getOriginalFilename());
//		String str[] = file.getOriginalFilename().split("[.]");	
		String extension = ".txt";
		String originalFilename = file.getOriginalFilename();
		int dot = originalFilename.lastIndexOf('.');
		if(dot != -1)
		{
			extension = originalFilename.substring(dot);
		}
		
		Date d = new Date();
		DateFormat ft =  DateUtils.getProcessDateFormatThai();
		ft.format(d);
		
		String root = ConstantUtil.IMG_ROOT_PATH; //"/Users/Sek/Desktop/imgUpload/"; 
    	String path = root+"pic/"+traderTypeName+"/"+licenseNoStr+"/"+registrationType+"/"+ft.format(d);
    	String fileName = "/"+realFileName+extension;
    	
    	String fullpath = path+fileName;
    	System.out.println("full path = "+fullpath);
    	
    	PersonImg picture = (PersonImg)this.personImageDAO.findByPrimaryKey(params.getPersonImgId());
    	//System.out.println(picture.getTrader().getTraderId());
    	System.out.println(params.getPersonImgId());
    	if(file != null)
	    {
    		//Update
    		if(params.getPersonImgId() >0)
    		{
    			System.out.println("###UPDATE");
	
    			//Delete Old File
    			PersonImg regPersonImg = (PersonImg)this.personImageDAO.findByPrimaryKey(params.getPersonImgId());
    			
    			String oldDocPath = regPersonImg.getImgPath();
    			
    			System.out.println("oldDocPath = "+oldDocPath);
    			
    			FileManage fm = new FileManage();
    			fm.deleteFile(oldDocPath);
    			
    			//Upload new File
		    	boolean b = fm.uploadFile(file.getInputStream(), path, fileName);
		    	
		    	//Update RegDocument
		    	if(b)
		    	{
		    		regPersonImg.setImgPath(fullpath);
		    		regPersonImg.setLastUpdDtm(new Date());
		    		regPersonImg.setLastUpdUser(user.getUserName());
		    	}
    			
    		}
    		//Create
    		else
    		{
    			//Upload File
		    	FileManage fm = new FileManage();
		    	boolean b = fm.uploadFile(file.getInputStream(), path, fileName);
		    	System.out.println(b);
		    	//Save RegDocument
		    	if(b)
		    	{
	
		    		this.savePersonImage(fullpath, trader, listReg.get(0), picture, user);
		    		
		    	}
    		}
	    	
	    }
    	
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void savePersonImage(String fullpath, Trader trader, 	Registration registration, PersonImg personImg, User user)
			throws Exception {
		
		PersonImg personImgPersis = new PersonImg();
		personImgPersis.setImgPath(fullpath);
		personImgPersis.setTrader(trader);
		personImgPersis.setRegistration(registration);
		
		
		personImgPersis.setRecordStatus(RecordStatus.NORMAL.getStatus());
		personImgPersis.setCreateUser(user.getUserName());
		personImgPersis.setCreateDtm(new Date());
		this.personImageDAO.insert(personImgPersis);
		
	}
	
	public List<RegistrationDTO> getPrintCardExcel(Object object, User user)
			throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		RegistrationDTO params = (RegistrationDTO)object;
		
		List<Object[]> listObj = this.printingCardDAO.findPrintCardAll(params, user, 0, Integer.MAX_VALUE);
		
//		DateFormat dfTH = DateUtils.getProcessDateFormatThai();
		DateFormat dfEN = DateUtils.getProcessDateFormatEn();
		
		System.out.println("listObj.size(): "+listObj.size());
		if(!listObj.isEmpty())
		{
			for(Object[] sel: listObj)
			{
				RegistrationDTO dto = new RegistrationDTO();
				dto.setPrintCardId(Long.valueOf(sel[0].toString()));
				if(sel[1] != null)
				{
					dto.setPrintCardDate(dfEN.format((Date)sel[1]));
				}
				
				dto.setPrintCardStatus(sel[2]==null?"":sel[2].toString());
				dto.setRegId(Long.valueOf(sel[3].toString()));
				dto.setRegistrationType(sel[4].toString());
				dto.setTraderType(sel[17]==null?"":sel[17].toString());
				
				if(sel[17] != null)
				{
					dto.setRegistrationTypeName(RegistrationType.getMeaning(dto.getTraderType(), dto.getRegistrationType()));
					dto.setTraderTypeName(TraderType.getMeaning(dto.getTraderType()));
				}
				if(sel[5] != null)
				{
					dto.setApproveDate(dfEN.format((Date) sel[5]));
				}
				
				dto.setPersonId(Long.valueOf(sel[6].toString()));
				dto.setFirstName(sel[7]==null?"":sel[7].toString());
				dto.setLastName(sel[8]==null?"":sel[8].toString());
				dto.setFirstNameEn(sel[9]==null?"":sel[9].toString());
				dto.setLastNameEn(sel[10]==null?"":sel[10].toString());
				dto.setIdentityNo(sel[11]==null?"":sel[11].toString());
				dto.setPrefixName(sel[12]==null?"":sel[12].toString());
				dto.setPrefixNameEn(sel[13]==null?"":sel[13].toString());
				
				dto.setTraderOwnerName(dto.getPrefixName() +" "+ dto.getFirstName() +" "+ dto.getLastName());
				dto.setTraderOwnerNameEn(dto.getPrefixNameEn() +" "+ dto.getFirstNameEn() +" "+ dto.getLastNameEn());
				
				dto.setL_traderOwnerName(dto.getFirstName() +" "+ dto.getLastName());
			    dto.setL_traderOwnerNameEn(dto.getFirstNameEn() +" "+ dto.getLastNameEn());
				
				dto.setTraderId(Long.valueOf(sel[14].toString()));
				dto.setLicenseNo(sel[15]==null?"":sel[15].toString());
				dto.setTraderCategory(sel[16]==null?"":sel[16].toString());
				if(sel[16] != null)
				{
					dto.setBgImg(ConstantUtil.CARD_TEMPLATE+dto.getTraderCategory());
					dto.setTraderCategoryName(TraderCategory.getMeaning(dto.getTraderType(), dto.getTraderCategory()));
				}
				if(sel[18] != null)
				{
					dto.setEffectiveDate(dfEN.format((Date) sel[18]));
				}
				if(sel[19] != null)
				{
					dto.setExpireDate(dfEN.format((Date) sel[19]));
					
					String[] expireDate = dto.getExpireDate().split("/");
			    	
			    	dto.setYearValid(expireDate[2]);
			    	dto.setMonthValid("/"+expireDate[1]+"/"+expireDate[0]);
				}
				
				if(sel[21] != null){
					dto.setTraderArea(sel[21].toString());
				}
				else
				{
					dto.setTraderArea((sel[20]==null?"":sel[20].toString()));
				}
							    
			    dto.setL_licenseNo(dto.getLicenseNo());
//			    dto.setL_traderOwnerName(dto.getTraderOwnerName());
//			    dto.setL_traderOwnerNameEn(dto.getTraderOwnerNameEn());
			    dto.setL_identityNo(dto.getIdentityNo());
			    dto.setL_effectiveDate(dto.getEffectiveDate()==null?"":dto.getEffectiveDate());
			    dto.setL_expireDate(dto.getExpireDate());
			    dto.setL_traderArea(dto.getTraderArea()==null?"":dto.getTraderArea());
			    
			    if(StringUtils.isNotEmpty(dto.getEffectiveDate())){
					String[] effDate = dto.getEffectiveDate().split("\\/");
					dto.setLicenseLeaderNo(dto.getLicenseNo()+"/"+(Integer.parseInt(effDate[2])+543));
					System.out.println("LicenseLeaderNo = " + params.getLicenseLeaderNo());
				}
			    
			    AdmUser admuser = (AdmUser) this.admUserDAO.findManagerByOrg(Long.valueOf(sel[22].toString()), ConstantUtil.DIRECTOR_POSITION);
			    
				dto.setL_director(admuser.getMasPrefix()==null?"":admuser.getMasPrefix().getPrefixName()+ admuser.getUserName()+" "+admuser.getUserLastname());
				list.add(dto);
			}
		}
		
		return list;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void updateASPrint(List<RegistrationDTO> list, User user) throws Exception {
		
		if(!list.isEmpty())
		{
			for(RegistrationDTO dto: list)
			{
				Long printId = dto.getPrintCardId();
				PrintingCard printingCard =  (PrintingCard)this.printingCardDAO.findByPrimaryKey(printId.intValue());
				printingCard.setPrintCardStatus("A");
				printingCard.setPrintCardDate(new Date());
				this.printingCardDAO.update(printingCard);
			}
		}
		
	}

	@Override
	public Object getPerson(Object object, User user) throws Exception {
		
		RegistrationDTO dto = new RegistrationDTO();
		RegistrationDTO params = (RegistrationDTO)object;
		List<RegistrationDTO> list = getPrintCardExcel(object ,user);
		if(!list.isEmpty()){
			dto = list.get(0);
			
			//get image base64
//			String docPath = "/Users/SHAII_MACBOOK/Documents/workingfolder/TourismDepartment/DOT_PRINT_CARD/pic/Registration/Guide/1100307/R/101.png";
			String docPath = this.getPhotoLocation(dto, user);
			String fileType = docPath.substring(docPath.lastIndexOf(".") + 1, docPath.length());
			
			String header = "data:image/jpeg;base64,";
			if(fileType.matches("(?i).*png.*")){
				header = "data:image/png;base64,";
				fileType = "png";
			}
			if(fileType.matches("(?i).*jpg.*")){
				header = "data:image/jpeg;base64,";
				fileType = "jpg";
			}
			if(fileType.matches("(?i).*jpeg.*")){
				header = "data:image/jpeg;base64,";
				fileType = "jpeg";
			}
			
		    File file = new File(docPath);
		    
		    if (file.exists()) {  
	             // Reading a Image file from file system
	             FileInputStream imageInFile = new FileInputStream(file);
	             byte imageData[] = new byte[(int) file.length()];
	             imageInFile.read(imageData);
	  
	             // Converting Image byte array into Base64 String
//	             String imageDataString = ImageUtils.encodeImage(imageData);
//	             dto.setImgData(header+imageDataString);
			}
		    

             
             String imgUrl =  params.getApplicationContextPath() + "/business/printcard/registration/photo/" + dto.getPrintCardId()+"/"+dto.getLicenseNo();
             dto.setImgData(imgUrl);
		}
		return dto;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveCardPhoto(Object object, User user) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}

		RegistrationDTO params = (RegistrationDTO)object;
		
		PrintingCard printingCard = (PrintingCard) this.printingCardDAO.findByPrimaryKey(Integer.parseInt(params.getPrintCardId()+""));
		Registration reg = printingCard.getRegistration();
		Trader trader = reg.getTrader();
		
		//TraderType
		String traderTypeName = "";
		long masDocId = 0;
		//LicenseNo
		String licenseNoStr = trader.getLicenseNo();
		if(trader.getTraderType().equals(TraderType.TOUR_COMPANIES.getStatus()))
		{
			traderTypeName = "Business";
			licenseNoStr = licenseNoStr.replaceAll("[/]", "");
		}
		if(trader.getTraderType().equals(TraderType.GUIDE.getStatus()))
		{
			traderTypeName = "Guide";
			licenseNoStr = licenseNoStr.replaceAll("[-]", "");
			masDocId = 101;
		}
		if(trader.getTraderType().equals(TraderType.LEADER.getStatus()))
		{
			traderTypeName = "Tourleader";
			licenseNoStr = licenseNoStr.replaceAll("[.]", "");
			masDocId = 118;
		}
		
		String registrationType = reg.getRegistrationType();
		//RealFileName
		String realFileName = masDocId+"";
		
		String extension = "."+ params.getExtension();
		


		String root =  ConstantUtil.ROOT_PATH;

		
		String path = root+"Registration"+java.io.File.separator+traderTypeName+java.io.File.separator+licenseNoStr+java.io.File.separator+registrationType;
    	String fileName = java.io.File.separator+realFileName+extension;
    	
    	String fullpath = path+fileName;
    	
    	MasDocument document = (MasDocument)this.masDocumentDAO.findByPrimaryKey(masDocId);
    	
         // Converting a Base64 String into Image byte array
//         byte[] imageByteArray = ImageUtils.decodeImage(params.getImgData());
    	
   
    	//delete old file 
    	//Delete Old File
    	List<RegDocument> list = (List<RegDocument>)this.regDocumentDAO.findAllByTraderAndMasDoc(trader.getTraderId(), masDocId);
		
		if(!list.isEmpty()){
			RegDocument doc = list.get(0);
			String oldDocPath = doc.getDocPath();
			
			FileManage fm = new FileManage();
			fm.deleteFile(oldDocPath);
			this.regDocumentDAO.delete(doc);
		}
		
		
         
         // Write a image byte array into file system
         File pathfile = new File(path);
		  if(!pathfile.exists())
		  {
			  pathfile.mkdirs();
		  }
		  
//		   File newFile = new File(path+fileName); 
//		   
//		   if (!newFile.exists()) {  
//			   newFile.createNewFile();  
//		   }  
         
//         FileOutputStream imageOutFile = new FileOutputStream(fullpath);
//         imageOutFile.write(imageByteArray);
//         imageOutFile.close();
		   
//		   System.out.println(params.getImgData());
		   
           // Converting a Base64 String into Image byte array
           byte[] imageByteArray = ImageUtils.decodeImage(params.getImgData());
           
           
//           System.out.println(imageDataString);
            
           // Write a image byte array into file system
           FileOutputStream imageOutFile = new FileOutputStream(path+fileName);
           imageOutFile.write(imageByteArray);
           imageOutFile.close();
		   
//			BufferedImage newImg = ImageUtils.decodeImage(params.getImgData());
//			ImageIO.write(newImg, params.getExtension(), newFile);
         
         this.regDocumentService.saveRegDocument(fullpath, trader, reg, document, user);
		
	}

	@Override
	public String getPhotoLocation(Object object, User user) throws Exception {
		System.out.println("###Service DocumentRegistrationService method upload");
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		String path = "";
		RegistrationDTO params = (RegistrationDTO)object;
		
		PrintingCard printingCard = (PrintingCard) this.printingCardDAO.findByPrimaryKey(Integer.parseInt(params.getPrintCardId()+""));
		Registration reg = printingCard.getRegistration();
		Trader trader = reg.getTrader();
		
		//TraderType
		long masDocId = 0;
		if(trader.getTraderType().equals(TraderType.GUIDE.getStatus()))
		{
			masDocId = 101;
		}
		if(trader.getTraderType().equals(TraderType.LEADER.getStatus()))
		{
			masDocId = 118;
		}
		
		List<RegDocument> list = (List<RegDocument>)this.regDocumentDAO.findAllByTraderAndMasDoc(trader.getTraderId(), masDocId);
		
		if(!list.isEmpty()){
			RegDocument doc = list.get(0);		
			path = doc.getDocPath();
		}
		
		return path;
	}

	@Override
	public String getPhotoBase64Format(Object object, User user)
			throws Exception {
		
		String docPath = this.getPhotoLocation(object, user);
		
		StringBuffer photostr = new StringBuffer();
		if(StringUtils.isNotEmpty(docPath)){
				
			StringBuffer imgstr = new StringBuffer();			
			BufferedImage img = ImageIO.read(new File(docPath));
			String fileType = docPath.substring(docPath.lastIndexOf(".") + 1, docPath.length());
			
			 File file = new File(docPath);
			
			String header = "data:image/jpeg;base64,";
			if(fileType.matches("(?i).*png.*")){
				header = "data:image/png;base64,";
				fileType = "png";
			}
			if(fileType.matches("(?i).*jpg.*")){
				header = "data:image/jpeg;base64,";
				fileType = "jpg";
			}
			if(fileType.matches("(?i).*jpeg.*")){
				header = "data:image/jpeg;base64,";
				fileType = "jpeg";
			}
			
			FileInputStream imageInFile = new FileInputStream(file);
            byte imageData[] = new byte[(int) file.length()];
            imageInFile.read(imageData);
            String imageDataString = ImageUtils.encodeImage(imageData);
				
			photostr.append(header + imageDataString);
		}
		
		return photostr.toString();
	}

	@Override
	public String getPrepareQRCode(Object object, User user) throws Exception {
		System.out.println("###Service DocumentRegistrationService method upload");
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}

		RegistrationDTO params = (RegistrationDTO)object;
		
		PrintingCard printingCard = (PrintingCard) this.printingCardDAO.findByPrimaryKey(Integer.parseInt(params.getPrintCardId()+""));
		Registration reg = printingCard.getRegistration();
		Trader trader = reg.getTrader();
		
		String licenseNoStr = trader.getLicenseNo();
		if(trader.getTraderType().equals(TraderType.TOUR_COMPANIES.getStatus()))
		{
			
			licenseNoStr = licenseNoStr.replaceAll("[/]", "B");
		}
		if(trader.getTraderType().equals(TraderType.GUIDE.getStatus()))
		{
			
			licenseNoStr = licenseNoStr.replaceAll("[-]", "G");
			
		}
		if(trader.getTraderType().equals(TraderType.LEADER.getStatus()))
		{
			
			licenseNoStr = licenseNoStr.replaceAll("[.]", "L");
			
		}
		
		return MessageFormat.format(ConstantUtil.VIEW_LICENSE_PROFILE_URL,trader.getTraderType(), licenseNoStr);
	}

	@Override
	public String getDirectorSignature(Object object, User user)
			throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}

		RegistrationDTO params = (RegistrationDTO)object;
		
		PrintingCard printingCard = (PrintingCard) this.printingCardDAO.findByPrimaryKey(Integer.parseInt(params.getPrintCardId()+""));
		Registration reg = printingCard.getRegistration();
		Trader trader = reg.getTrader();
		
		AdmUser director = (AdmUser) this.admUserDAO.findManagerByOrg(trader.getOrganization().getOrgId(), ConstantUtil.DIRECTOR_POSITION);
		return director.getSignature();
	}


}
