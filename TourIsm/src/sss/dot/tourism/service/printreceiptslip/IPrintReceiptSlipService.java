package sss.dot.tourism.service.printreceiptslip;

import java.util.Map;

import com.sss.aut.service.User;

public interface IPrintReceiptSlipService {
	public Map<String, ?> getPrintReceiptSlip(Object object, User user) throws Exception;
}
