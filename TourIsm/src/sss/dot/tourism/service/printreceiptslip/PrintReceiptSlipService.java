package sss.dot.tourism.service.printreceiptslip;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.AdmUserDAO;
import sss.dot.tourism.dao.mas.MasPositionDAO;
import sss.dot.tourism.dao.registration.ReceiptDAO;
import sss.dot.tourism.domain.AdmUser;
import sss.dot.tourism.domain.Receipt;
import sss.dot.tourism.dto.registration.ReceiptDTO;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.ThaiBahtUtil;

import com.sss.aut.service.User;

@Repository("printReceiptService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PrintReceiptSlipService implements IPrintReceiptSlipService{

	@Autowired
	ReceiptDAO receiptDAO;
	
	@Autowired
	MasPositionDAO masPositionDAO;
	
	@Autowired
	AdmUserDAO admUserDAO;
	
	public Map<String, ?> getPrintReceiptSlip(Object object, User user)
			throws Exception {
		
		if(!(object instanceof ReceiptDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
	
		ReceiptDTO params = (ReceiptDTO)object;
		String receiptDetail= "";
		String office = "สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์กลาง";
		String office1 = "สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ สาขาภาคเหนือ";
		String office2 = "สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ สาขาภาคใต้เขต2";
		String office3 = "สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ สาขาภาคใต้เขต1";
		String office4 = "สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ สาขาภาคตะวันออกเฉียงเหนือ";
		
		String address = "ถนนพระราม 1 แขวงวังใหม่ เขตปทุมวัน กรุงเทพฯ 10330";
		String address1 = "414/20-21 ถ.ช้างคลาน ต.ช้างคลาน อ.เมือง จ.เชียงใหม่ 50100";
		String address2 = "50/9 ถ.เจ้าฟ้าตะวันตก ต.วิชิต อ.เมือง จ.ภูเก็ต 83000";
		String address3 = "บ้านเลขที่ 1 ถ.กุลวัฒน์อุทิศ อ.หาดใหญ่ จ.สงขลา 90110";
		String address4 = "230/31-32 ถ.มิตรภาพ-หนองคาย ต.ในเมือง อ.เมือง จ.นครราชสีมา";
		
		String telephone = "โทร. 0-2219-4010-7 ต่อ 630,631";
		String telephone1 = "โทร. 0-5320-4602 , 0-5320-4485";
		String telephone2 = "โทร. 0-7652-2299 , 0-7652-2300 แฟกซ์. 0-7652-2311";
		String telephone3 = "โทร. 0-7423-2230 , 0-7422-0376 แฟกซ์. 0-7423-0151";
		String telephone4 = "โทร. 0-4424-8740-1 โทรสาร. 0-4424-8742";
		
		//Oat Add 23/12/2557
		String office6 = "สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์กลาง";
		String office7 = "สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ สาขากรุงเทพมหานคร";
		String office8 = "สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ สาขาภาคกลางเขต 1";
		String office9 = "สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ สาขาภาคกลางเขต 2";
		String office10 = "สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ สาขาภาคกลางเขต 3";
		String office11 = "สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ สาขาภาคตะวันออก";
		String office12 = "สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ สาขาภาคเหนือเขต 1";
		String office13 = "สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ สาขาภาคเหนือเขต 2";
		String office14 = "สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ สาขาภาคเหนือเขต 3";
		String office15 = "สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ สาขาภาคตะวันออกเฉียงเหนือเขต 1";
		String office16 = "สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ สาขาภาคตะวันออกเฉียงเหนือเขต 2";
		String office17 = "สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ สาขาภาคตะวันออกเฉียงเหนือเขต 3";
		String office18 = "สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ สาขาภาคใต้เขต 1";
		String office19 = "สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ สาขาภาคใต้เขต 2";
		String office20 = "สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ สาขาภาคใต้เขต 3";
		String office21 = "สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ สาขาภาคใต้เขต 4";
		
		String address6 = "ถนนพระราม 1 แขวงวังใหม่ เขตปทุมวัน กรุงเทพฯ 10330";
		String address7 = "ถนนพระราม 1 แขวงวังใหม่ เขตปทุมวัน กรุงเทพฯ 10330";
		String address8 = "สนามกีฬาจังหวัดกาญจนบุรี ต.ท่ามะขาม อ.เมือง จ.กาญจนบุรี 71000";
		String address9 = "157 สนามกีฬากลางจังหวัดพระนครศรีอยุธยา ต.ไผ่ลิง อ.พระนครศรีอยุธยา จ.พระนครศรีอยุธยา";
		String address10 = "สนามกีฬาจังหวัดประจวบคีรีขันธ์ ถนนเทิดพระเกียรติ อำเภอเมือง จังหวัดประจวบคีรีขันธ์ 77000";
		String address11 = "223 หมู่ 1 ถนนบางนา-ตราด ตำบลหนองไม้แดง อำเภอเมือง จังหวัดชลบุรี 20000";
		String address12 = "414/20-21 ถ.ช้างคลาน ต.ช้างคลาน อ.เมือง จ.เชียงใหม่ 50100";//"สำนักงานการท่องเที่ยวและกีฬาจังหวัดเชียงใหม่ ตึกอุตสาหกรรม ชั้น 2 ศูนย์ราชการจังหวัดเชียงใหม่ 50300";
//		String address13 = "ศาลากลาง 750 ปีเมืองเชียงราย ถนนแม่ฟ้าหลวง ตำบลริมกก อำเภอเมือง จังหวัดเชียงราย 57100";
		String address13 = "สนามกีฬากลางจังหวัดเชียงราย อาคารอัฒจรรย์คบเพลิง ตำบลรอบเวียง อำเภอเมืองเชียงราย จังหวัดเชียงราย 57000";
		String address14 = "380 หมู่ 5 ตำบลหัวรอ อำเภอเมือง จังหวัดพิษณุโลก 65000";
		String address15 = "7/1 อาคารเฉลิมพระเกียรติ ชั้น 2 ถนนกำแหงสงคราม ต.ในเมือง อ.เมือง จ.นครราชสีมา 30000 ";
//		String address16 = "415/61 ถนนชยางกูร อำเภอเมือง จังหวัดอุบลราชธานี 34000";
		String address16 = "๒๘๘ หมู่ ๒ ถนนวารินฯ - พิบูลฯ ตำบลแสนสุข อำเภอวารินชำราบ จังหวัดอุบลราชธานี ๓๔๑๙๐ ";
		String address17 = "บริเวณศูนย์ราชการ ต.หนองกอมเกาะ อ.เมือง จ.หนองคาย 43000";
		String address18 = "บ้านเลขที่ ๑ ถนนกุลวัฒน์อุทิศ ตำบลหาดใหญ่ อำเภอหาดใหญ่ จังหวัดสงขลา ๙๐๑๑๐";//"สนามกีฬาติณสูลานนท์ ถนนราชดำเนิน ตำบลบ่อยาง อำเภอเมือง จังหวัดสงขลา 90000";
		String address19 = "71 หมู่ 5, ถนนวิชิตสงคราม, ตำบลวิชิต อำเภอเมือง จังหวัดภูเก็ต, 83000 ";
		String address20 = "อาคารสนามกีฬากลางจังหวัดกระบี่, ตำบลในเมือง อำเภอเมือง จังหวัดกระบี่, 81000";
		String address21 = "อาคารสนามกีฬากลางจังหวัดสุราษฎร์ธานี, ถนนดอนนก, ตำบลมะขามเตี้ย อำเภอเมืองสุราษฎร์ธานี";
		
		String telephone6 = "โทร. 0-2219-4010-7 ต่อ 630,631";
		String telephone7 = "โทร. 0-2219-4010-7 ต่อ 630,631";
		String telephone8 = "โทร. 0-3452-0335 , 09-8739-7879 , 0-3452-0266 โทรสาร 0-3452-0335";
		String telephone9 = "โทร. 0-3524-5417 โทรสาร. 0-3524-5417";
		String telephone10 = "โทร. 032 611 543, 032-604777 โทรสาร. 032 -661543";
		String telephone11 = "โทรศัพท์/โทรสาร: 038054199";//"โทรศัพท์/โทรสาร: 038 274 284, 0818516296";
		String telephone12 = "โทร. 0-5320-4602 , 0-5320-4485";//"โทร. 0-5311-2325-6 โทรสาร.0-5311-2325-6 ต่อ 17  038-758427";
//		String telephone13 = "โทร. 053-150192 โทรสาร. 053-177340 ";
		String telephone13 = "โทร. 053713637 โทรสาร. 053713637 ";
		String telephone14 = "โทร. 055-321280, 081-3792880 โทรสาร. 055321280 ต่อ 13";
		String telephone15 = "โทร. 0-44252444 โทรสาร. 0-44259691";
//		String telephone16 = "โทร. 045-311228 โทรสาร. 045-314104";
		String telephone16 = "โทร ๐ ๔๕๒๕ ๒๗๑๔   แฟกซ์ ๐ ๔๕๒๕ ๒๗๑๕";
//		String telephone17 = "โทรศัพท์/โทรสาร: 0-4242–0745";
		String telephone17 = "โทรศัพท์/โทรสาร  042411771";
		String telephone18 = "โทร. 074 232230 ,074 220376 แฟกซ์ 074 230151";//"โทร. 0-7431-1573 โทรสาร. 0-7431-3195";
		String telephone19 = "โทร. 0-7621-7054 โทรสาร. 0-7622-1765";
		String telephone20 = "โทรศัพท์/โทรสาร: 0-7566-3246";
		String telephone21 = "โทร. 077 283 612 โทรสาร. 0-77-282-964";
		//
		
		BigDecimal feeMnySum=new BigDecimal(0);
		String sumFee = "";
		String mny ="";
		ReceiptDTO dto = new ReceiptDTO();
		Map<String, Object> model = new HashMap<String, Object>();

		DateFormat ft = DateUtils.getProcessDateFormatThai();
		DateFormat fe = DateUtils.getProcessDateFormatEn();
		

		
		List<Object[]> listObj = (List<Object[]>)this.receiptDAO.findReceiptSlip(params);
		
		if(!listObj.isEmpty() && listObj!=null)
		{
			for(Object[] sel: listObj)
			{
				//Double feeMny = new Double(0);
				BigDecimal feeMny = new BigDecimal(0);
				if(sel[1] !=null)
				{
					dto.setBookNo(sel[1].toString());
				}
				
				if(sel[2] !=null)
				{
					dto.setReceiptNo(sel[2].toString());
				}
				
				if(sel[3] != null)
				{
					dto.setReceiptName(sel[3].toString());
				}
				
				if(sel[4] !=null)
				{
					String date = ft.format((Date)sel[4]);
					String day;
					String monthF;
					String yearF;
					String monthTH;
					String dayF  = date.substring(0,1);
					if(!dayF.equals("0"))
					{
						dayF = date.substring(0,2);
					}else
					{
						dayF = date.substring(1,2);
					}
						monthF = date.substring(3, 4);
					if(!monthF.equals("0"))
					{
						monthF = date.substring(3, 5);			 
					}else
					{
						monthF = date.substring(4, 5);			
					}
						yearF =  date.substring(6, 10);

						monthTH = DateUtils.getMonthName(monthF);
					
						day = dayF+" "+monthTH+" "+yearF;
						dto.setReceiveOfficerDate(day);
				}
				
				if(sel[5] !=null)
				{
					dto.setReceiveOfficerName(sel[5].toString());
				}
				
				if(sel[6] !=null)
				{
					dto.setAuthority(sel[6].toString());
				}
				
				if(sel[7] !=null && sel[8] !=null)
				{
					//feeMny = BigDecimal.valueOf(Long.valueOf(sel[7].toString()));
					feeMny = (BigDecimal)sel[7];
					feeMnySum = feeMnySum.add(feeMny);
					mny = mny+sel[7].toString()+"\n\n";
					receiptDetail = receiptDetail+sel[8].toString()+"\n\n";
				}
				
				if(sel[9] !=null)
				{
					dto.setIdentityNo(sel[9].toString());
				}
				
				if(sel[10] !=null)
				{
					dto.setOrgId(Long.valueOf(sel[10].toString()));
					params.setOrgId(Long.valueOf(sel[10].toString()));
				}
				
				if(sel[11] !=null)
				{
					dto.setPosition("นักพัฒนาการท่องเที่ยว");
				}
				
				
			}
			dto.setReceiptDetail(receiptDetail);
		}
		
		sumFee = feeMnySum.toString();
		String feeMnyText = ThaiBahtUtil.getBahtText(sumFee);
		System.out.println(receiptDetail);

		Date date = new Date();
		String day = fe.format(date);
		String firstName = "";
		String lastName = ""; 
		String name = "";
		String preName = dto.getAuthority().substring(0,3);
		System.out.println(preName);
		if(preName.equals("นาย") || preName.equals("นาง"))
		{
			name = dto.getAuthority().substring(3,dto.getAuthority().length());
			System.out.println(name);
			String check[] = name.split(" ");
			firstName = check[0];
			lastName = check[1]; 
		}else 
		{
			name = dto.getAuthority().substring(6,dto.getAuthority().length());
			System.out.println(name);
			String check[] = name.split(" ");
			firstName = check[0];
			lastName = check[1]; 
		}

		List<Object[]> listPos = (List<Object[]>)this.masPositionDAO.posiTionName(firstName, lastName);
		
		if(listPos!=null && !listPos.isEmpty())
		{
			for(Object[] sel : listPos)
			{
				if(sel[1] !=null)
				{
					dto.setPosition1(sel[1].toString());
				}else
				{
					dto.setPosition1("");
				}
			}
		}
		
		
		if(dto.getOrgId() == 2)
		{
			dto.setOffice(office1);
			dto.setAddress(address1);
			dto.setTelephone(telephone1);			
		}else if(dto.getOrgId() == 3)
		{
			dto.setOffice(office2);
			dto.setAddress(address2);
			dto.setTelephone(telephone2);
		}else if(dto.getOrgId() == 4)
		{
			dto.setOffice(office3);
			dto.setAddress(address3);
			dto.setTelephone(telephone3);
		}else if(dto.getOrgId() == 5)
		{
			dto.setOffice(office4);
			dto.setAddress(address4);
			dto.setTelephone(telephone4);
		}
		
		//Oat Add 23/12/2557
		else if(dto.getOrgId() == 6)
		{
			dto.setOffice(office6);
			dto.setAddress(address6);
			dto.setTelephone(telephone6);
		}
		else if(dto.getOrgId() == 7)
		{
			dto.setOffice(office7);
			dto.setAddress(address7);
			dto.setTelephone(telephone7);
		}
		else if(dto.getOrgId() == 8)
		{
			dto.setOffice(office8);
			dto.setAddress(address8);
			dto.setTelephone(telephone8);
		}
		else if(dto.getOrgId() == 9)
		{
			dto.setOffice(office9);
			dto.setAddress(address9);
			dto.setTelephone(telephone9);
		}
		else if(dto.getOrgId() == 10)
		{
			dto.setOffice(office10);
			dto.setAddress(address10);
			dto.setTelephone(telephone10);
		}
		else if(dto.getOrgId() == 11)
		{
			dto.setOffice(office11);
			dto.setAddress(address11);
			dto.setTelephone(telephone11);
		}
		else if(dto.getOrgId() == 12)
		{
			dto.setOffice(office12);
			dto.setAddress(address12);
			dto.setTelephone(telephone12);
		}
		else if(dto.getOrgId() == 13)
		{
			dto.setOffice(office13);
			dto.setAddress(address13);
			dto.setTelephone(telephone13);
		}
		else if(dto.getOrgId() == 14)
		{
			dto.setOffice(office14);
			dto.setAddress(address14);
			dto.setTelephone(telephone14);
		}
		else if(dto.getOrgId() == 15)
		{
			dto.setOffice(office15);
			dto.setAddress(address15);
			dto.setTelephone(telephone15);
		}
		else if(dto.getOrgId() == 16)
		{
			dto.setOffice(office16);
			dto.setAddress(address16);
			dto.setTelephone(telephone16);
		}
		else if(dto.getOrgId() == 17)
		{
			dto.setOffice(office17);
			dto.setAddress(address17);
			dto.setTelephone(telephone17);
		}
		else if(dto.getOrgId() == 18)
		{
			dto.setOffice(office18);
			dto.setAddress(address18);
			dto.setTelephone(telephone18);
		}
		else if(dto.getOrgId() == 19)
		{
			dto.setOffice(office19);
			dto.setAddress(address19);
			dto.setTelephone(telephone19);
		}else if(dto.getOrgId() == 20)
		{
			dto.setOffice(office20);
			dto.setAddress(address20);
			dto.setTelephone(telephone20);
		}else if(dto.getOrgId() == 21)
		{
			dto.setOffice(office21);
			dto.setAddress(address21);
			dto.setTelephone(telephone21);
		}
		
		//
		
	    model.put("bookNo", dto.getBookNo());
		model.put("receiptNo", dto.getReceiptNo());
		model.put("receiptName", dto.getReceiptName());
		model.put("receiptOfficerDate", dto.getReceiveOfficerDate());
		model.put("identityNo", dto.getIdentityNo());
		model.put("receiptDetail", receiptDetail);
		model.put("feeMny1", mny);
		model.put("feeMnyText", feeMnyText);
		model.put("feeMny", sumFee);
		model.put("receiptOfficerName", dto.getReceiveOfficerName());
		model.put("authority", dto.getAuthority());
		model.put("day", day);
		model.put("position", dto.getPosition());
		model.put("position1", dto.getPosition1());
		model.put("office", dto.getOffice());
		model.put("address", dto.getAddress());
		model.put("telephone", dto.getTelephone());
		
		System.out.println("dto.getAuthority() = "+dto.getAuthority());
		if((null != dto.getAuthority()) && (!dto.getAuthority().isEmpty()))
		{
//			String authority[] = dto.getAuthority().split(" ");
//			System.out.println("authority[1] = "+authority[1]);
//			List<AdmUser> listUserAuth = this.admUserDAO.findUserSignature(authority[1]);
			
			List<AdmUser> listUserAuth = this.admUserDAO.findUserSignatureWithFullName(dto.getAuthority());
			System.out.println("listUserAuth.isEmpty() = "+listUserAuth.isEmpty());
			if(!listUserAuth.isEmpty())
			{
				AdmUser auth = listUserAuth.get(0);
				model.put("signature", auth.getSignature());
			}
		}
		

		
		return model;
	}

}
