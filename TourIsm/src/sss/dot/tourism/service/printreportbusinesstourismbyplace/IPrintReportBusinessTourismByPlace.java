package sss.dot.tourism.service.printreportbusinesstourismbyplace;

import java.util.List;

import sss.dot.tourism.dto.report.ReportBusinessTourismByPlaceDTO;

public interface IPrintReportBusinessTourismByPlace {

	public List<ReportBusinessTourismByPlaceDTO> printReportBusinessTourismByPlace(Long provinceId,Long amphurId, Long tambolId)throws Exception;
}
