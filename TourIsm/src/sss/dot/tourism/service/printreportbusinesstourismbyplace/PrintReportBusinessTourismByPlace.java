package sss.dot.tourism.service.printreportbusinesstourismbyplace;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sss.dot.tourism.dao.ReportBusinessTourismByPlaceDAO;
import sss.dot.tourism.dto.report.ReportBusinessTourismByPlaceDTO;

@Service("printReportBusinessTourismByPlace")
public class PrintReportBusinessTourismByPlace implements IPrintReportBusinessTourismByPlace {

	@Autowired
	private ReportBusinessTourismByPlaceDAO reportBusinessTourismByPlaceDAO;
	
	@Override
	public List<ReportBusinessTourismByPlaceDTO> printReportBusinessTourismByPlace(Long provinceId,Long amphurId, Long tambolId) throws Exception{
		// TODO Auto-generated method stub
		return reportBusinessTourismByPlaceDAO.findReportBuisnessTourismDTO(provinceId, amphurId, tambolId);
	}

}
