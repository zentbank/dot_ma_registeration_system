package sss.dot.tourism.service.printreportfeebusinessguide;

import java.util.Map;

import com.sss.aut.service.User;

public interface IPrintReportFeeBusinessGuideService {
	public Map<String, ?> getPrintReportFeeBusienessGuide(Object object, User user) throws Exception;
}
