package sss.dot.tourism.service.printreportfeebusinessguide;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.registration.GuaranteeDAO;
import sss.dot.tourism.dao.registration.ReceiptDAO;
import sss.dot.tourism.dao.registration.ReceiptMapRegistrationDAO;
import sss.dot.tourism.dto.registration.GuaranteeDTO;
import sss.dot.tourism.dto.registration.ReceiptDTO;
import sss.dot.tourism.util.DateUtils;

import com.sss.aut.service.User;

@Repository("printReportFeeBusinessGuideService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PrintReportfeeBusinessGuideService implements IPrintReportFeeBusinessGuideService{
	@Autowired
	ReceiptMapRegistrationDAO receiptMapRegistrationDAO;
	@Autowired
	ReceiptDAO receiptDAO;
	@Autowired
	GuaranteeDAO guaranteeDAO;
	
	@SuppressWarnings("null")
	public Map<String, ?> getPrintReportFeeBusienessGuide(Object object,
			User user) throws Exception {
		
			if(!(object instanceof ReceiptDTO))
			{
				throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลการจดทะเบียนได้");
			}
		int end = 0;
		Map model = new HashMap();
		List<ReceiptDTO> list = new ArrayList<ReceiptDTO>();
		ReceiptDTO params = (ReceiptDTO)object;
		String month = params.getMonth();
		String monthT = null;
		Double total = new Double(0.00);
		Double total1 = new Double(0.00);
		Double total2 = new Double(0.00);
		Double total3 = new Double(0.00);
		Double total4 = new Double(0.00);
		Double total5 = new Double(0.00);
		Double total6 = new Double(0.00);
		Double total7 = new Double(0.00);
		Double total8 = new Double(0.00);
		Double total9 = new Double(0.00);
		Double total10 = new Double(0.00);
		Double total11 = new Double(0.00);
		Double total12 = new Double(0.00);
		Double total13 = new Double(0.00);
		Double fee1 = new Double(0.00);
		Double fee2 = new Double(0.00);
		Double fee3 = new Double(0.00);
		Double fee4 = new Double(0.00);
		Double fee5 = new Double(0.00);
		Double fee6 = new Double(0.00);
		Double fee7 = new Double(0.00);
		Double fee8 = new Double(0.00);
		Double fee9 = new Double(0.00);
		Double fee10 = new Double(0.00);
		Double fee11= new Double(0.00);
		Double fee12 = new Double(0.00);
		Double fee13 = new Double(0.00);
		Double totalAll = new Double(0.00);
//		System.out.println(month);
		String monthName = DateUtils.getMonthName(month);
		String year = params.getYear();
		int yearInt = Integer.valueOf(year);
		yearInt = yearInt -543;
		DateFormat ft =  DateUtils.getProcessDateFormatThaiSingle();
		
		Date endDate = new Date();
		String firstDate = null;
		String lastDate = null;
		if(month.equals("1")||month.equals("3")||month.equals("5")||month.equals("7")||month.equals("8")||month.equals("10")||month.equals("12"))
		{
		     lastDate = "31/";
			 endDate = ft.parse("31/"+month+"/"+year);
			 end = 31;
			 //System.out.println(endDate);
		}else if(month.equals("4")||month.equals("6")||month.equals("8")||month.equals("9")||month.equals("11"))
		{	
		     lastDate = "30/";
			 endDate = ft.parse("30/"+month+"/"+year);
			 end = 30;
		}else if(month.equals("2"))
		{
			if((yearInt % 4 == 0 && yearInt % 100 != 0)||(yearInt % 400 == 0))
			{	
			     lastDate = "29/";
				 endDate = ft.parse("29/"+month+"/"+year);
				 end = 29;
			}else
			{
				 
			     lastDate = "28/";
				 endDate = ft.parse("28/"+month+"/"+year);
				 end = 28;
			}
		}
		
		Date startDate = ft.parse("1/"+month+"/"+year);

		//System.out.println(startDate);
		
		/*int count = 1;
		int dayTH = getDay(startDate)-1;
		for(int i =1;i<=end;i++)
		{
			ReceiptDTO dto = new ReceiptDTO();
			String dayTh = String.valueOf(dayTH);
			String dayThName = DateUtils.getDayName(dayTh);
			//System.out.println(dayThName);
			if(!dayThName.equals("เสาร์") && !dayThName.equals("อาทิตย์"))
			{
				//dto.setNo(String.valueOf(count));
				//System.out.println(dto.getNo());
				//dto.setDay(String.valueOf(i)+" "+DateUtils.getMonthShotName(month)+year.substring(2,4));
				//System.out.println(dto.getDay());
				count = count + 1;
				dto.setReceiptName("ค่าธรรมเนียม");
			//	list.add(dto);
			}
		
			if(dayTH <= 6)
			{
				dayTH = dayTH+1;
			}else
			{
				dayTH = 1;
			}
			
			
		}*/
		if(month.equals("1")||month.equals("2")||month.equals("3")||month.equals("4")||month.equals("5")||month.equals("6")||month.equals("7")||month.equals("8")||month.equals("9"))
		{
			month = "0"+month;
			firstDate = "01/"+month+"/"+year;	
		    lastDate = lastDate+month+"/"+year;	
		    params.setReceiveOfficerDateFrom(firstDate);
		    params.setReceiveOfficerDateTo(lastDate);
		}else
		{
			firstDate = "01/"+month+"/"+year;	
		    lastDate = lastDate+month+"/"+year;	
			params.setReceiveOfficerDateFrom(firstDate);
		    params.setReceiveOfficerDateTo(lastDate);
		}
		
				String date1 = "0";
				String date2 = "0";
				int i = 1;
				ReceiptDTO dtoDay = new ReceiptDTO();
				GuaranteeDTO guaDto = new GuaranteeDTO();
				
				long guaranteeIdTemp = 0;
				long guaranteeId = 0;	
//				System.out.println(end);
				for(int j=1;j<=end;j++)
				{		
					if(j<10)
					{
						firstDate = "0"+j+"/"+month+"/"+year;
				    }else
				    {
				    	firstDate = j+"/"+month+"/"+year;
				    }
					dtoDay.setReceiveOfficerDateFrom(firstDate);
				    dtoDay.setReceiveOfficerDateTo(firstDate);
				    guaDto.setDate(firstDate);
					System.out.println("#####dateTo = "+dtoDay.getReceiveOfficerDateFrom());
					System.out.println("#####dateFrom = "+dtoDay.getReceiveOfficerDateTo());
					//Oat Add
					dtoDay.setOrgId(user.getUserData().getOrgId());
					//End Oat Add
					List<Object[]> listObjDay = (List<Object[]>)this.receiptDAO.findReceiptFeeDateForDay(dtoDay);
					List<Object[]> listObjGua = (List<Object[]>)this.guaranteeDAO.findGuaranteeRefund(guaDto, user);
					
					String check = "1";
					
				if(listObjGua!=null && !listObjGua.isEmpty())
				{
					for(Object[] sel2 : listObjGua)
					{
						if(sel2[2] !=null)
						{
							guaranteeIdTemp = Long.valueOf(sel2[2].toString());
						}
					}
				 }

				if(guaranteeIdTemp != guaranteeId)
				{
					check = "true";
				}
				guaranteeId = guaranteeIdTemp;
					
					
					ReceiptDTO dto = new ReceiptDTO();
					int num = 1;
					String date = null;
					String temp = null;

					DateFormat fm = DateUtils.getProcessDateFormatThai();
				if((listObjDay!=null && !listObjDay.isEmpty()) || (check.equals("true") && check!=null))
				{	
					System.out.println(check);
					if(check.equals("true") && check!=null)
					{
					for(Object[] sel2 : listObjGua)
					{
						if(sel2[1]!=null)
						{
							temp = sel2[1].toString();
							dto.setFeeMny(Double.valueOf(temp));
							fee9 = fee9 + dto.getFeeMny();
							total = total+dto.getFeeMny();
							total9 = total9+dto.getFeeMny();
							date = dtoDay.getReceiveOfficerDateFrom().substring(0,1);
							if(!date.equals("0"))
							{
								date = dtoDay.getReceiveOfficerDateFrom().substring(0,2);
							}else
							{
								date = dtoDay.getReceiveOfficerDateFrom().substring(1,2);	
							}
						}
					
					}
					}
				if(listObjDay!=null)
				{	
				for(Object[] sel1 : listObjDay)
				{
					if(sel1[0]!=null)
					{
						dto.setFeeId(Long.valueOf(sel1[0].toString()));
//						System.out.println("feeId: "+dto.getFeeId());
					}
					if(sel1[1]!=null)
					{
						temp = sel1[1].toString();
						//dto.setFeeMny(BigDecimal.valueOf(Long.valueOf(temp.substring(0, temp.lastIndexOf(".")))));
						dto.setFeeMny(Double.valueOf(sel1[1].toString()));
					}
					if(sel1[2]!=null)
					{
						dto.setReceiveOfficerDate(fm.format((Date)sel1[2]));
						date = dto.getReceiveOfficerDate().substring(0,1);
						if(!date.equals("0"))
						{
							date = dto.getReceiveOfficerDate().substring(0,2);
						}else
						{
							date = dto.getReceiveOfficerDate().substring(1,2);	
						}
					}
					
					if(sel1[3]!=null)
					{
						dto.setFeeName(sel1[3].toString());
					}
	
	
						if(dto.getFeeId() == 1)
							{
							    fee1 = fee1+(dto.getFeeMny());
								total = total+(dto.getFeeMny());
								total1 = total1+(dto.getFeeMny());
							}else if(dto.getFeeId() == 2)
							{
								fee2 = fee2+(dto.getFeeMny());
								total = total+(dto.getFeeMny());
								total2 = total2+(dto.getFeeMny());
							}else if(dto.getFeeId() == 3)
							{
								fee4 = fee4+(dto.getFeeMny());
								total = total+(dto.getFeeMny());
								total4 = total4+(dto.getFeeMny());
							}else if(dto.getFeeId() == 4)
							{
								fee3 = fee3+(dto.getFeeMny());
								total = total+(dto.getFeeMny());
								total3 = total3+(dto.getFeeMny());
							}else if(dto.getFeeId() == 5)
							{
								fee6 = fee6+(dto.getFeeMny());
								total = total+(dto.getFeeMny());
								total6 = total6+(dto.getFeeMny());
							}else if(dto.getFeeId() == 6)
							{
								fee8 = fee8+(dto.getFeeMny());
								total = total+(dto.getFeeMny());
								total8 = total8+(dto.getFeeMny());
							}else if(dto.getFeeId() == 14)
							{
								//if(dto.getFeeName().equals("เงินเพิ่ม"))
								//{
									fee5 = fee5+(dto.getFeeMny());
									total = total+(dto.getFeeMny());
									total5 = total5+(dto.getFeeMny());
								//}
							}
				            else if(dto.getFeeId() == 9 || dto.getFeeId() == 10 || dto.getFeeId() == 13|| dto.getFeeId() == 8 || dto.getFeeId() == 11 || dto.getFeeId() == 7)
							{
				            	fee10 = fee10+(dto.getFeeMny());
								total = total+(dto.getFeeMny());
								total10 = total10+(dto.getFeeMny());
							}else if(dto.getFeeId() == 12)
							{
								fee7 = fee7+(dto.getFeeMny());
								total = total+(dto.getFeeMny());
								total7 = total7+(dto.getFeeMny());
							}
			
				}	
			}
				dto.setFee1(fee1);
				dto.setFee2(fee2);
				dto.setFee3(fee3);
				dto.setFee4(fee4);
				dto.setFee5(fee5);
				dto.setFee6(fee6);
				dto.setFee7(fee7);
				dto.setFee8(fee8);
				dto.setFee9(fee9);
				dto.setFee10(fee10);
				dto.setFee11(fee11);
				dto.setFee12(fee12);
				dto.setFee13(fee13);
//				System.out.println(fee5);
//				System.out.println(fee10);
				dto.setTotal(total);
				System.out.println("#####total = "+total);
				totalAll = totalAll+(total);
				dto.setReceiptName("ค่าธรรมเนียม");
				dto.setNo(String.valueOf(i));
				dto.setDay(String.valueOf(date)+" "+DateUtils.getMonthShotName(month)+year.substring(2,4));

				list.add(dto);
				total = new Double(0.00);
				 fee1 = new Double(0.00);
				 fee2 = new Double(0.00);
				 fee3 = new Double(0.00);
				 fee4 = new Double(0.00);
				 fee5 = new Double(0.00);
				 fee6 = new Double(0.00);
				 fee7 = new Double(0.00);
				 fee8 = new Double(0.00);
				 fee9 = new Double(0.00);
				 fee10 = new Double(0.00);
				 fee11= new Double(0.00);
				 fee12 = new Double(0.00);
				 fee13 = new Double(0.00);
				i= i+1;
			//}
		}
				System.out.println("----------------------------------------------------");
	}
	
		model.put("totalAll", totalAll);
		model.put("total1", total1);
		model.put("total2", total2);
		model.put("total3", total3);
		model.put("total4", total4);
		model.put("total5", total5);
		model.put("total6", total6);
		model.put("total7", total7);
		model.put("total8", total8);
		model.put("total9", total9);
		model.put("total10", total10);
		model.put("total11", total11);
		model.put("total12", total12);
		model.put("total13", total13);
		model.put("regMonth",monthName);
		model.put("regYear", year);
		model.put("regPrintFeeBusinessGuide", list);
		return model;
}
	public int getWorkingDaysBetweenTwoDates(Date startDate, Date endDate) {  
	    List list = new ArrayList();
		Calendar startCal;  
	    Calendar endCal;  
	    startCal = Calendar.getInstance();  
	    startCal.setTime(startDate);  
	    endCal = Calendar.getInstance();  
	    endCal.setTime(endDate);  
	    int workDays = 0;  

	    //Return 0 if start and end are the same  
	    if (startCal.getTimeInMillis() == endCal.getTimeInMillis()) {  
	        return 0;  
	    }  

	    if (startCal.getTimeInMillis() > endCal.getTimeInMillis()) {  
	        startCal.setTime(endDate);  
	        endCal.setTime(startDate);  
	    }  

	    do {  
	        startCal.add(Calendar.DAY_OF_MONTH, 1);
	        
	        if (startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY ) {    
	        	++workDays;
	        }
	     
	    } while (startCal.getTimeInMillis() < endCal.getTimeInMillis());  
	    
	    
	    
	    return workDays;  
	}
	public int getDay(Date date)
	{
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
	  int day = cal.get(Calendar.DAY_OF_WEEK);
	  String dayName = null;
	  //System.out.print("Today is ");
	  switch(day){
	  case 1: dayName ="Sunday";
	  break;
	  case 2: dayName = "Monday";
	  break;
	  case 3: dayName = "Tueseday";
	  break;
	  case 4: dayName = "Wednesday";
	  break;
	  case 5: dayName = "Thursday";
	  break;
	  case 6: dayName = "Friday";
	  break;
	  case 7: dayName = "Saturday";
	  break;
	  }
		
		return day;
	}
	
}
