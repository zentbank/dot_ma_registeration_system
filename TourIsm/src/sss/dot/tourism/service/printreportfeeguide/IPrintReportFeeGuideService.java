package sss.dot.tourism.service.printreportfeeguide;

import java.util.List;
import java.util.Map;

import com.sss.aut.service.User;

public interface IPrintReportFeeGuideService {
	public Map<String, ?> getPrintReportFeeGuide(Object object, User user) throws Exception;
}
