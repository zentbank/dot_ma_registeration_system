package sss.dot.tourism.service.printreportfeeguide;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.registration.ReceiptMapRegistrationDAO;
import sss.dot.tourism.dto.registration.ReceiptDTO;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.util.DateUtils;

import com.sss.aut.service.User;

@Repository("printReportFeeGuideService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PrintReportFeeGuideService implements IPrintReportFeeGuideService {
	
    @Autowired
	ReceiptMapRegistrationDAO receiptMapRegistrationDAO;
    
	public Map<String, ?> getPrintReportFeeGuide(Object object, User user)
			throws Exception {
		if(!(object instanceof ReceiptDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
		List<ReceiptDTO> listN = new ArrayList<ReceiptDTO>();
		List<ReceiptDTO> listR = new ArrayList<ReceiptDTO>();
		List<ReceiptDTO> listT = new ArrayList<ReceiptDTO>();
		
		ReceiptDTO params = (ReceiptDTO)object;
		Map model = new HashMap(); 
		String monthTTH = null;
		String monthTH = null;
		String monthF = null;
		String yearF = null; 
		String dayT = null;
		String monthT = null;
		String yearT = null;
		String yearTH = null;
		String date = null;
		String yearTTH = null;
		String dateTo = null;
		BigDecimal total = new BigDecimal(0);
		DecimalFormat df = new DecimalFormat("#,##0.00");
		Double count = new Double(0.00);
		Double feeMnyGeneral = new Double(0.00);
		Double feeMnyPrivate = new Double(0.00);
		Double feeMnyTemporary= new Double(0.00);
		String dateFrom = params.getReceiptDateFrom();
		if(params.getReceiptDateTo()!=null)
		{
			dateTo = params.getReceiptDateTo();
		}
		String dayF  = dateFrom.substring(0,1);
		if(!dayF.equals("0"))
		{
			dayF = dateFrom.substring(0,2);
		}else
		{
			dayF = dateFrom.substring(1,2);
		}
			monthF = dateFrom.substring(3, 4);
		if(!monthF.equals("0"))
		{
			monthF = dateFrom.substring(3, 5);			 
		}else
		{
			monthF = dateFrom.substring(4, 5);			
		}
			yearF =  dateFrom.substring(6, 10);
		
		if(dateTo!=null && !dateTo.equals(""))
		{
			 dayT  = dateTo.substring(0,1);
			if(!dayT.equals("0"))
			{
				dayT = dateTo.substring(0,2);
			}else
			{
				dayT = dateTo.substring(1,2);
			}
			  monthT = dateTo.substring(3, 4);
			
			if(!monthT.equals("0"))
			{
				monthT = dateTo.substring(3, 5);
			}else
			{
				monthT = dateTo.substring(4, 5);
			}
				yearT =  dateTo.substring(6, 10);
		}
		
		if(yearT !=null && !yearT.equals(""))
		{
			if(yearT.equals(yearF))
			{
				if(monthT.equals(monthF))
				{
					monthTH = DateUtils.getMonthName(monthF);
					yearTH = yearT;
				    date = "ตั้งแต่วันที่ "+ dayF+" - "+dayT+" "+monthTH+" "+yearTH;
				}else
				{
					monthTH = DateUtils.getMonthName(monthF);
					monthTTH = DateUtils.getMonthName(monthT);
					yearTH = yearT;
				    date = "ตั้งแต่วันที่ "+ dayF+" "+monthTH+" - "+dayT+" "+monthTTH+" "+yearTH;
				}
			}else
			{
				if(monthT.equals(monthF))
				{
					monthTH = DateUtils.getMonthName(monthF);
					yearTH = yearF;
					yearTTH = yearT;
				    date = "ตั้งแต่วันที่ "+ dayF+" - "+dayT+" "+monthTH+" "+yearTH+" - "+yearTTH;
				}else
				{
					monthTH = DateUtils.getMonthName(monthF);
					monthTTH = DateUtils.getMonthName(monthT);
					yearTH = yearT;
					yearTTH = yearT;
				    date = "ตั้งแต่วันที่ "+ dayF+" "+monthTH+" "+yearTH+" - "+dayT+" "+monthTTH+" "+yearTTH;
				}
			}
		}else
		{
			monthTH = DateUtils.getMonthName(monthF);
			yearTH = yearF;
			date = "ตั้งแต่วันที่ "+ dayF+" "+monthTH+" "+yearTH;
		}
		model.put("regDate", date);
		List<Object[]> listObj = this.receiptMapRegistrationDAO.findReceiptRegistrationType(params, user);
		if((!listObj.isEmpty()) && listObj !=null)
		{
			for(Object[] sel: listObj)
			{
				ReceiptDTO dto = new ReceiptDTO();

				String temp = null;
				dto.setRegistrationType(sel[0].toString());
				if(sel[1] !=null)
				{
					 temp = sel[1].toString();
					 dto.setFeeMny(new Double(temp));
				}
				//dto.setFeeMny(BigDecimal.valueOf(Long.valueOf(temp.substring(0,temp.lastIndexOf(".")))));
				System.out.println(dto.getRegistrationType());
				if(dto.getRegistrationType().equals("N"))
				{
					List<Object[]> listObjNewGeneral = this.receiptMapRegistrationDAO.findReceiptSumMnyNewRegistrationGeneral(params, user);
					dto.setRegistrationType("จดใหม่");
					dto.setCardGeneral("-บัตรทั่วไป "+"("+df.format(dto.getFeeMny())+")");
					dto.setCardPrivate("-บัตรเฉพาะพื้นที่ "+"("+df.format(dto.getFeeMny())+")");
				   if(!listObjNewGeneral.isEmpty())
				   {
				    for(Object[] selNew: listObjNewGeneral)
					{
				    	String tempNew = null;
				    	String tempFeeMny = null;
				    	if(selNew[0] !=null)
						{
				    		tempNew = selNew[0].toString();
				    		System.out.println(tempNew);
				    		dto.setSumFeeMnyGeneral(Double.valueOf(tempNew));
				    		//dto.setSumFeeMnyGeneral(BigDecimal.valueOf(Long.valueOf(tempNew.substring(0,tempNew.lastIndexOf(".")))));	    		
						}
				    	else
						{
							dto.setSumFeeMnyGeneral(new Double(0));
						}
						
				    	if(selNew[2] !=null)
				    	{
				    		tempFeeMny = selNew[2].toString();
				    		System.out.println(tempFeeMny);
				    		feeMnyGeneral = Double.valueOf(selNew[2].toString());
				    		//feeMnyGeneral = BigDecimal.valueOf(Long.valueOf(tempFeeMny.substring(0,tempFeeMny.lastIndexOf("."))));
				    	}
				    	
						dto.setTotalGeneral(selNew[1].toString());
						count = count + dto.getSumFeeMnyGeneral();
						//count = count.add(dto.getSumFeeMnyGeneral());
						total = total.add(BigDecimal.valueOf(Long.valueOf(dto.getTotalGeneral())));
						dto.setCardGeneral("-บัตรทั่วไป "+"("+df.format(feeMnyGeneral)+")");
					}
				   }else{
					   dto.setSumFeeMnyGeneral(new Double(0));
					   dto.setTotalGeneral("0");
				   }
				    List<Object[]> listObjNewPrivate = this.receiptMapRegistrationDAO.findReceiptSumMnyNewRegistrationPrivate(params, user);
				  if(!listObjNewPrivate.isEmpty())
				  {
				    for(Object[] selPrivate: listObjNewPrivate)
					{
				    	String tempPrivate = null;
				    	String tempFeeMny = null;
				    	if(selPrivate[0] !=null)
						{
				    		tempPrivate = selPrivate[0].toString();
				    		System.out.println(tempPrivate);
				    		dto.setSumFeeMnyPrivate(Double.valueOf(tempPrivate));
				    		//dto.setSumFeeMnyPrivate(BigDecimal.valueOf(Long.valueOf(tempPrivate.substring(0,tempPrivate.lastIndexOf(".")))));
						}else
						{
							dto.setSumFeeMnyPrivate(new Double(0));
						}
						
				    	if(selPrivate[2] !=null)
				    	{
				    		tempFeeMny = selPrivate[2].toString();
				    		feeMnyPrivate = Double.valueOf(selPrivate[2].toString());
				    		//feeMnyPrivate = BigDecimal.valueOf(Long.valueOf(tempFeeMny.substring(0,tempFeeMny.lastIndexOf("."))));
				    	}
						dto.setTotalPrivate(selPrivate[1].toString());
						count = count + dto.getSumFeeMnyPrivate();
						//count = count.add(dto.getSumFeeMnyPrivate());
						total = total.add(BigDecimal.valueOf(Long.valueOf(dto.getTotalPrivate())));
						dto.setCardPrivate("-บัตรเฉพาะพื้นที่ "+"("+df.format(feeMnyPrivate)+")");
					}
				  }
				  listN.add(dto);
				}else if(dto.getRegistrationType().equals("R"))
				{
					List<Object[]> listObjNewGeneral = this.receiptMapRegistrationDAO.findReceiptSumMnyReNewRegistrationGeneral(params, user);

					dto.setRegistrationType("ต่ออายุ");
					dto.setCardGeneral("-บัตรทั่วไป "+"("+df.format(dto.getFeeMny())+")");
					dto.setCardPrivate("-บัตรเฉพาะพื้นที่ "+"("+df.format(dto.getFeeMny())+")");
					if(!listObjNewGeneral.isEmpty())
					   {
					    for(Object[] selNew: listObjNewGeneral)
						{
					    	String tempNew = null;
					    	String tempFeeMny = null;
					    	if(selNew[0] !=null)
							{
					    		tempNew = selNew[0].toString();
					    		System.out.println(tempNew);
					    		dto.setSumFeeMnyGeneral(Double.valueOf(tempNew));
					    		//dto.setSumFeeMnyGeneral(BigDecimal.valueOf(Long.valueOf(tempNew.substring(0,tempNew.lastIndexOf(".")))));
							}
					    	else
							{
								dto.setSumFeeMnyGeneral(new Double(0));
							}
							if(selNew[2] !=null)
					    	{
								tempFeeMny = selNew[2].toString();
								feeMnyGeneral = Double.valueOf(selNew[2].toString());
								//feeMnyGeneral = BigDecimal.valueOf(Long.valueOf(tempFeeMny.substring(0,tempFeeMny.lastIndexOf("."))));
					    	}
							dto.setTotalGeneral(selNew[1].toString());
							count = count + dto.getSumFeeMnyGeneral();
							//count = count.add(dto.getSumFeeMnyGeneral());
							total = total.add(BigDecimal.valueOf(Long.valueOf(dto.getTotalGeneral())));
							dto.setCardGeneral("-บัตรทั่วไป "+"("+df.format(feeMnyGeneral)+")");
						}
					   }else
					   {
						   dto.setSumFeeMnyGeneral(new Double(0));
						   dto.setTotalGeneral("0");
					   }
					    List<Object[]> listObjNewPrivate = this.receiptMapRegistrationDAO.findReceiptSumMnyReNewRegistrationPrivate(params, user);
					  if(!listObjNewPrivate.isEmpty())
					  {
					    for(Object[] selPrivate: listObjNewPrivate)
						{
					    	String tempPrivate = null;
					    	String tempFeeMny = null;
					    	if(selPrivate[0] !=null)
							{
					    		tempPrivate = selPrivate[0].toString();
					    		System.out.println(tempPrivate);
					    		dto.setSumFeeMnyPrivate(Double.valueOf(tempPrivate));
					    		//dto.setSumFeeMnyPrivate(BigDecimal.valueOf(Long.valueOf(tempPrivate.substring(0,tempPrivate.lastIndexOf(".")))));
							}else
							{
								dto.setSumFeeMnyPrivate(new Double(0));
							}
							
					    	if(selPrivate[2] !=null)
					    	{
								tempFeeMny = selPrivate[2].toString();
								feeMnyPrivate = Double.valueOf(selPrivate[2].toString());
					    		//feeMnyPrivate = BigDecimal.valueOf(Long.valueOf(tempFeeMny.substring(0,tempFeeMny.lastIndexOf("."))));
					    	}
					    	
							dto.setTotalPrivate(selPrivate[1].toString());
							count = count + dto.getSumFeeMnyPrivate();
							//count = count.add(dto.getSumFeeMnyPrivate());
							total = total.add(BigDecimal.valueOf(Long.valueOf(dto.getTotalPrivate())));
							dto.setCardPrivate("-บัตรเฉพาะพื้นที่ "+"("+df.format(feeMnyPrivate)+")");
						}
					  }else
					  {
						  dto.setSumFeeMnyPrivate(new Double(0));
						  dto.setTotalPrivate("0");
					  }
					  listR.add(dto);
				}else if(dto.getRegistrationType().equals("T"))
				{
					List<Object[]> listObjTempory = this.receiptMapRegistrationDAO.findReceiptSumMnyTemporaryRegistration(params, user);
					dto.setRegistrationType("ใบแทน "+"("+df.format(dto.getFeeMny())+")");
					if(!listObjTempory.isEmpty())
					  {
					    for(Object[] selTemporary: listObjTempory)
						{
					    	String tempSelTemporary = null;
					    	String tempFeeMny = null;
					    	if(selTemporary[0] !=null)
							{
					    		tempSelTemporary = selTemporary[0].toString();
					    		System.out.println(tempSelTemporary);
					    		dto.setSumFeeMnyGeneral(Double.valueOf(tempSelTemporary));
					    		//dto.setSumFeeMnyGeneral(BigDecimal.valueOf(Long.valueOf(tempSelTemporary.substring(0,tempSelTemporary.lastIndexOf(".")))));
							}else
							{
								dto.setSumFeeMnyGeneral(new Double(0));
							}
							
					    	if(selTemporary[2] !=null)
					    	{
								tempFeeMny = selTemporary[2].toString();
								feeMnyTemporary = Double.valueOf(selTemporary[2].toString());
								//feeMnyTemporary = BigDecimal.valueOf(Long.valueOf(tempFeeMny.substring(0,tempFeeMny.lastIndexOf("."))));
					    	}
					    	
							dto.setTotalGeneral(selTemporary[1].toString());
							count = count + dto.getSumFeeMnyGeneral();
							//count = count.add(dto.getSumFeeMnyGeneral());
							total = total.add(BigDecimal.valueOf(Long.valueOf(dto.getTotalGeneral())));
							dto.setRegistrationType("ใบแทน "+"("+df.format(feeMnyTemporary)+")");
						}
					    
					  }else{
						  dto.setSumFeeMnyGeneral(new Double(0));
						  dto.setTotalGeneral("0");
					  }
					
					  listT.add(dto);
				}

			}
		}
		
		model.put("regSumCount", total);
		model.put("regSumTotal", count);
		model.put("regTotal", "รวมทั้งสิ้น");
		model.put("regPrintFeeGuideN", listN);
		model.put("regPrintFeeGuideR", listR);
		model.put("regPrintFeeGuideT", listT);
		return model;
	}
	public static void main(String[] args) {
		 
        double d = 12002.126;
        DecimalFormat df = new DecimalFormat("#,##0.00");
        System.out.print(df.format(d));
    }
}
