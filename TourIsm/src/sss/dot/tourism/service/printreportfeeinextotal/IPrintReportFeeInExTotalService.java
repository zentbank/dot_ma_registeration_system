package sss.dot.tourism.service.printreportfeeinextotal;

import java.util.Map;

import com.sss.aut.service.User;

public interface IPrintReportFeeInExTotalService {
	public Map<String, ?> getPrintReportFeeInExTotal(Object object, User user)throws Exception;

}
