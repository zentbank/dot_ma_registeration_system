package sss.dot.tourism.service.printreportfeeinextotal;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.registration.GuaranteeDAO;
import sss.dot.tourism.dao.registration.ReceiptDAO;
import sss.dot.tourism.domain.Organization;
import sss.dot.tourism.dto.registration.GuaranteeDTO;
import sss.dot.tourism.dto.registration.ReceiptDTO;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.util.DateUtils;

import com.sss.aut.service.User;

@Repository("printReportFeeInExTotalService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PrintReportFeeInExTotalService implements IPrintReportFeeInExTotalService{

	@Autowired
	GuaranteeDAO guaranteeDAO;
	
	@Autowired
	ReceiptDAO receiptDAO;

	public Map<String, ?> getPrintReportFeeInExTotal(Object object, User user)throws Exception {
		
		Map model = new HashMap();
		
		try{
			
			if(!(object instanceof ReceiptDTO))
			{
				throw new IllegalArgumentException("ไม่สามารถค้นหาข้อมูลรายงานการรับ-จ่ายประจำเดือนได้");
			}
			int end = 0;
			
			List<ReceiptDTO> list = new ArrayList<ReceiptDTO>();
			ReceiptDTO params = (ReceiptDTO)object;
			String month = params.getMonth();
			Double fee1 = new Double(0);
			Double fee2 = new Double(0);
			Double fee3 = new Double(0);
			Double fee4 = new Double(0);
			Double fee5 = new Double(0);
			Double fee6 = new Double(0);
			Double fee7 = new Double(0);
			Double fee8 = new Double(0);
			Double fee9 = new Double(0);
			Double fee10 = new Double(0);
			Double fee11= new Double(0);
			Double fee12 = new Double(0);
			Double fee13 = new Double(0);
			Double fee14 = new Double(0);
			Double feeAll = new Double(0);
			System.out.println(month);
			String monthName = DateUtils.getMonthName(month);
			String year = params.getYear();
			int yearInt = Integer.valueOf(year);
			yearInt = yearInt -543;
			DateFormat ft =  DateUtils.getProcessDateFormatThaiSingle();
			DateFormat fd =  DateUtils.getDayFormatEn();
			
			Date endDate = new Date();
			String firstDate = null;
			String lastDate = null;
			if(month.equals("1")||month.equals("3")||month.equals("5")||month.equals("7")||month.equals("8")||month.equals("10")||month.equals("12"))
			{
			     lastDate = "31/";
				 endDate = ft.parse("31/"+month+"/"+year);
				 end = 31;
				 //System.out.println(endDate);
			}else if(month.equals("4")||month.equals("6")||month.equals("8")||month.equals("9")||month.equals("11"))
			{	
			     lastDate = "30/";
				 endDate = ft.parse("30/"+month+"/"+year);
				 end = 30;
			}else if(month.equals("2"))
			{
				if((yearInt % 4 == 0 && yearInt % 100 != 0)||(yearInt % 400 == 0))
				{	
				     lastDate = "29/";
					 endDate = ft.parse("29/"+month+"/"+year);
					 end = 29;
				}else
				{
					 
				     lastDate = "28/";
					 endDate = ft.parse("28/"+month+"/"+year);
					 end = 28;
				}
			}
			
			Date startDate = ft.parse("1/"+month+"/"+year);
	
			//System.out.println(startDate);
			
			if(month.equals("1")||month.equals("2")||month.equals("3")||month.equals("4")||month.equals("5")||month.equals("6")||month.equals("7")||month.equals("8")||month.equals("9"))
			{
				month = "0"+month;
				firstDate = "01/"+month+"/"+year;	
			    lastDate = lastDate+month+"/"+year;	
			    params.setReceiveOfficerDateFrom(firstDate);
			    params.setReceiveOfficerDateTo(lastDate);
			}else
			{
				firstDate = "01/"+month+"/"+year;	
			    lastDate = lastDate+month+"/"+year;	
				params.setReceiveOfficerDateFrom(firstDate);
			    params.setReceiveOfficerDateTo(lastDate);
			}
	
			ReceiptDTO dtoDay = new ReceiptDTO();
			GuaranteeDTO guaDto = new GuaranteeDTO();
	
			dtoDay.setOrgId(params.getOrgId());
			dtoDay.setReceiveOfficerDateFrom(params.getReceiveOfficerDateFrom());
		    dtoDay.setReceiveOfficerDateTo(params.getReceiveOfficerDateTo());
		    
//			System.out.println(dtoDay.getReceiveOfficerDateFrom());
//			System.out.println(dtoDay.getReceiveOfficerDateTo());
		    
			List<Object[]> listObjMonth = (List<Object[]>)this.receiptDAO.findReceiptFeeDateForDay(dtoDay);

			for(int j=1;j<=end;j++)
			{
				if(j<10)
				{
					firstDate = "0"+j+"/"+month+"/"+year;
			    }else
			    {
			    	firstDate = j+"/"+month+"/"+year;
			    }
				guaDto.setDate(firstDate);
				List<Object[]> listObjGua = (List<Object[]>)this.guaranteeDAO.findGuaranteeRefund(guaDto, user);
				if(!listObjGua.isEmpty())
				{
					for(Object[] sel: listObjGua)
					{
						fee9 += Double.valueOf(sel[1].toString());
					}
				}
			}
			
			if(!listObjMonth.isEmpty())
			{
				for(Object[] sel: listObjMonth)
				{
					if(sel[0].toString().equals("1"))
					{
						fee1 += Double.valueOf(sel[1].toString());
					}
					if(sel[0].toString().equals("2"))
					{
						fee2 += Double.valueOf(sel[1].toString());
					}
					if(sel[0].toString().equals("4"))
					{
						fee3 += Double.valueOf(sel[1].toString());
					}
					if(sel[0].toString().equals("3"))
					{
						fee4 += Double.valueOf(sel[1].toString());
					}
					if(sel[0].toString().equals("14"))
					{
						fee5 += Double.valueOf(sel[1].toString());
					}
					if(sel[0].toString().equals("5"))
					{
						fee6 += Double.valueOf(sel[1].toString());
					}
					if(sel[0].toString().equals("12"))
					{
						fee7 += Double.valueOf(sel[1].toString());
					}
					if(sel[0].toString().equals("6"))
					{
						fee8 += Double.valueOf(sel[1].toString());
					}
					if(sel[0].toString().equals("7") || sel[0].toString().equals("8") || sel[0].toString().equals("9") 
							|| sel[0].toString().equals("10") || sel[0].toString().equals("11") || sel[0].toString().equals("13") )
					{
						fee14 += Double.valueOf(sel[1].toString());
					}
	
					
	
				}
				
			}
			
//			System.out.println(fee1 +" ,"+fee2 +" ,"+fee3 +" ,"+fee4 +" ,"+fee5 +" ,"+fee6 +" ,"+fee7 +" ,"+fee8 +" ,"+fee9 +" ,"+fee10 +" ,"+fee11 +" ,"+fee12 +" ,"+fee13 +" ,"+fee14 +" ,");

			feeAll = fee1 +fee2 +fee3 +fee4 +fee5 +fee6 +fee7 +fee8 +fee9 +fee10 +fee11 +fee12 +fee13 +fee14;
			
			//บวก การรับเงิน
			ReceiptDTO dto = new ReceiptDTO();
			dto.setFeeName("รายได้ค่าใบอนุญาตประกอบธุรกิจนำเที่ยว");
			dto.setFeeMny2(fee1);
			list.add(dto);
			
			ReceiptDTO dto2 = new ReceiptDTO();
			dto2.setFeeName("รายได้ค่าธรรมเนียมประกอบธุรกิจนำเที่ยว");
			dto2.setFeeMny2(fee2);
			list.add(dto2);
			
			ReceiptDTO dto3 = new ReceiptDTO();
			dto3.setFeeName("รายได้ค่าใบแทนใบอนุญาตประกอบธุรกิจนำเที่ยว");
			dto3.setFeeMny2(fee3);
			list.add(dto3);
			
			ReceiptDTO dto4 = new ReceiptDTO();
			dto4.setFeeName("รายได้ค่าธรรมเนียมประกอบธุรกิจนำเที่ยวรายสองปี");
			dto4.setFeeMny2(fee4);
			list.add(dto4);
			
			ReceiptDTO dto5 = new ReceiptDTO();
			dto5.setFeeName("รายได้เงินเพิ่ม");
			dto5.setFeeMny2(fee5);
			list.add(dto5);
			
			ReceiptDTO dto6 = new ReceiptDTO();
			dto6.setFeeName("รายได้ค่าใบอนุญาตเป็นมัคคุเทศก์");
			dto6.setFeeMny2(fee6);
			list.add(dto6);
			
			ReceiptDTO dto7 = new ReceiptDTO();
			dto7.setFeeName("รายได้ค่าใบแทนใบอนุญาตเป็นมัคคุเทศก์");
			dto7.setFeeMny2(fee7);
			list.add(dto7);
			
			ReceiptDTO dto8 = new ReceiptDTO();
			dto8.setFeeName("รายได้การต่ออายุใบอนุญาตเป็นมัคคุเทศก์");
			dto8.setFeeMny2(fee8);
			list.add(dto8);
			
			ReceiptDTO dto9 = new ReceiptDTO();
			dto9.setFeeName("รายได้จากหลักประกันเกิน 2 ปี ตกเป็นของกองทุนฯ");
			dto9.setFeeMny2(fee9);
			list.add(dto9);
			
			ReceiptDTO dto10 = new ReceiptDTO();
			dto10.setFeeName("รายได้จากการรับบริจาค");
			dto10.setFeeMny2(fee10);
			list.add(dto10);
			
			ReceiptDTO dto11 = new ReceiptDTO();
			dto11.setFeeName("ดอกเบี้ยรับจากเงินฝากสถาบันการเงิน");
			dto11.setFeeMny2(fee11);
			list.add(dto11);
			
			ReceiptDTO dto12 = new ReceiptDTO();
			dto12.setFeeName("เงินที่ผู้ประกอบการธุรกิจนำเที่ยวจ่ายชดใช้คืน");
			dto12.setFeeMny2(fee12);
			list.add(dto12);
			
			ReceiptDTO dto13 = new ReceiptDTO();
			dto13.setFeeName("หนี้สูญได้รับคืน");
			dto13.setFeeMny2(fee13);
			list.add(dto13);
			
			ReceiptDTO dto14 = new ReceiptDTO();
			dto14.setFeeName("รายได้อื่นๆ");
			dto14.setFeeMny2(fee14);
			list.add(dto14);
			
			ReceiptDTO dto15 = new ReceiptDTO();
			dto15.setFeeName("รวม");
			dto15.setFeeMnyAll(feeAll);
			list.add(dto15);
			
			model.put("regFee", list);
			//
			
			//ประจำเดือน
			String strDate = "ประจำเดือน "+monthName+" "+year;
			model.put("regDate",strDate);
			//
			
			//เงินคงเหลือยกมา
			List<ReceiptDTO> listRemainC = new ArrayList<ReceiptDTO>();
			
			ReceiptDTO dtoRemainC = new ReceiptDTO();
			dtoRemainC.setReceiptName("เงินคงเหลือยกมา");
			dtoRemainC.setFeeName("เงินสดในมือ");
			dtoRemainC.setFeeMny2(new Double(0));
			listRemainC.add(dtoRemainC);
			
			ReceiptDTO dtoRemainC2 = new ReceiptDTO();
			dtoRemainC2.setReceiptName("");
			dtoRemainC2.setFeeName("เงินฝากธนาคาร-ออมทรัพย์");
			dtoRemainC2.setFeeMny2(new Double(0));
			listRemainC.add(dtoRemainC2);
			
			ReceiptDTO dtoRemainC3 = new ReceiptDTO();
			dtoRemainC3.setReceiptName("");
			dtoRemainC3.setFeeName("เงินฝากธนาคาร-กระแสรายวัน");
			dtoRemainC3.setFeeMny2(new Double(0));
			listRemainC.add(dtoRemainC3);
			
			ReceiptDTO dtoRemainC4 = new ReceiptDTO();
			dtoRemainC4.setReceiptName("");
			dtoRemainC4.setFeeName("เงินฝากคลัง");
			dtoRemainC4.setFeeMny2(new Double(0));
			dtoRemainC4.setFeeMnyAll(new Double(0));
			listRemainC.add(dtoRemainC4);
			
			model.put("regFeeRemainC", listRemainC);
			//
			
			//หัก การจ่ายเงิน
			List<ReceiptDTO> list2 = new ArrayList<ReceiptDTO>();
			
//			ReceiptDTO dtoPay = new ReceiptDTO();
//			dtoPay.setFeeName("การจ่ายเงิน");
//			dtoPay.setFeeMny2(new Double(0));
//			list2.add(dtoPay);
			
			ReceiptDTO dtoPay2 = new ReceiptDTO();
			dtoPay2.setFeeName("ค่าใช้จ่าย(ระบุประเภท)");
			dtoPay2.setFeeMny2(new Double(0));
			list2.add(dtoPay2);
			
			ReceiptDTO dtoPay3 = new ReceiptDTO();
			dtoPay3.setFeeName("ค่าชดเชยให้แก่นักท่องเที่ยว");
			dtoPay3.setFeeMny2(new Double(0));
			list2.add(dtoPay3);
			
			ReceiptDTO dtoPay4 = new ReceiptDTO();
			dtoPay4.setFeeName("เงินที่นำส่งให้กองทุนคุ้มครองธุรกิจนำเที่ยว");
			dtoPay4.setFeeMny2(new Double(0));
			list2.add(dtoPay4);
			

			ReceiptDTO dtoPay5 = new ReceiptDTO();
			dtoPay5.setFeeName("รวม");
			dtoPay5.setFeeMnyAll(new Double(0));
			list2.add(dtoPay5);
			
			model.put("regFee2", list2);
			//
			
			//เงินคงเหลือยกไป
			List<ReceiptDTO> listRemainG = new ArrayList<ReceiptDTO>();
			
			ReceiptDTO dtoRemainG = new ReceiptDTO();
			dtoRemainG.setReceiptName("เงินคงเหลือยกไป");
			dtoRemainG.setFeeName("เงินสดในมือ");
			dtoRemainG.setFeeMny2(new Double(0));
			listRemainG.add(dtoRemainG);
			
			ReceiptDTO dtoRemainG2 = new ReceiptDTO();
			dtoRemainG2.setReceiptName("");
			dtoRemainG2.setFeeName("เงินฝากธนาคาร-ออมทรัพย์");
			dtoRemainG2.setFeeMny2(new Double(0));
			listRemainG.add(dtoRemainG2);
			
			ReceiptDTO dtoRemainG3 = new ReceiptDTO();
			dtoRemainG3.setReceiptName("");
			dtoRemainG3.setFeeName("เงินฝากธนาคาร-กระแสรายวัน");
			dtoRemainG3.setFeeMny2(new Double(0));
			listRemainG.add(dtoRemainG3);
			
			ReceiptDTO dtoRemainG4 = new ReceiptDTO();
			dtoRemainG4.setReceiptName("");
			dtoRemainG4.setFeeName("เงินฝากคลัง");
			dtoRemainG4.setFeeMny2(new Double(0));
			dtoRemainG4.setFeeMnyAll(new Double(0));
			listRemainG.add(dtoRemainG4);
			
			model.put("regFeeRemainG", listRemainG);
			//
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return model;
		
	}
}
	








