package sss.dot.tourism.service.printreportfeetotal;

import java.util.Map;

import com.sss.aut.service.User;

public interface IPrintReportFeeTotalService {
	public Map<String, ?> getPrintReportFeeTotal(Object object, User user) throws Exception;
}
