package sss.dot.tourism.service.printreportfeetotal;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.registration.GuaranteeDAO;
import sss.dot.tourism.dao.registration.ReceiptDAO;
import sss.dot.tourism.dao.registration.ReceiptMapRegistrationDAO;
import sss.dot.tourism.dto.registration.GuaranteeDTO;
import sss.dot.tourism.dto.registration.ReceiptDTO;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.ThaiBahtUtil;

import com.sss.aut.service.User;

@Repository("printReportFeeTotalService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PrintReportFeeTotalService implements IPrintReportFeeTotalService{
	@Autowired
	ReceiptMapRegistrationDAO receiptMapRegistrationDAO;
	@Autowired
	ReceiptDAO receiptDAO;
	@Autowired
	GuaranteeDAO guaranteeDAO;
	
	public Map<String, ?> getPrintReportFeeTotal(Object object, User user)
			throws Exception {
		
		if(!(object instanceof ReceiptDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		Map model = new HashMap();
		List<ReceiptDTO> list = new ArrayList<ReceiptDTO>();
		List<ReceiptDTO> list1 = new ArrayList<ReceiptDTO>();
		List<ReceiptDTO> list2 = new ArrayList<ReceiptDTO>();
		List<ReceiptDTO> list3 = new ArrayList<ReceiptDTO>();
		List<ReceiptDTO> list4 = new ArrayList<ReceiptDTO>();
		List<ReceiptDTO> list5 = new ArrayList<ReceiptDTO>();
		List<ReceiptDTO> list6 = new ArrayList<ReceiptDTO>();
		List<ReceiptDTO> list7 = new ArrayList<ReceiptDTO>();
		List<ReceiptDTO> list8 = new ArrayList<ReceiptDTO>();
		List<ReceiptDTO> list9 = new ArrayList<ReceiptDTO>();
		List<ReceiptDTO> list10 = new ArrayList<ReceiptDTO>();
		
		ReceiptDTO params = (ReceiptDTO)object;

		String monthTH = null;
		String monthF = null;
		String yearF = null; 
		String yearTH = null;
		String day = null;
		Double total = new Double(0);
		Double fee1Mny = new Double(0);
		Double fee2Mny = new Double(0);
		Double fee3Mny = new Double(0);
		Double fee4Mny = new Double(0);
		Double fee5Mny = new Double(0);
		Double fee6Mny = new Double(0);
		Double fee7Mny = new Double(0);
		Double fee8Mny = new Double(0);
		Double fee9Mny = new Double(0);
		Double fee10Mny = new Double(0);
		Double totalAllNumber = new Double(0);
		String totalAllText = "";
		String bookNo = "";
		String dateFrom = params.getReceiptDateFrom();
		
		String dayF  = dateFrom.substring(0,1);
		if(!dayF.equals("0"))
		{
			dayF = dateFrom.substring(0,2);
		}else
		{
			dayF = dateFrom.substring(1,2);
		}
			monthF = dateFrom.substring(3, 4);
		if(!monthF.equals("0"))
		{
			monthF = dateFrom.substring(3, 5);			 
		}else
		{
			monthF = dateFrom.substring(4, 5);			
		}
			yearF =  dateFrom.substring(6, 10);

			monthTH = DateUtils.getMonthName(monthF);
			yearTH = yearF;
			day = dayF+" "+monthTH+" "+yearTH;
			String detail = "";
			String receiptNo = "";
			String receiptNoTemp = "";
		params.setReceiveOfficerDateFrom(params.getReceiptDateFrom());	
		List<Object[]> listregFee1 = (List<Object[]>)this.receiptDAO.findReceiptFeeTotalBookNo(params,"1", params.getOrgId());
		if(!listregFee1.isEmpty() && listregFee1!=null)
		{
			for(Object[] sel : listregFee1)
			{
				ReceiptDTO dto = new ReceiptDTO();
				if(sel[0] !=null)
				{
					dto.setBookNo(sel[0].toString());
					params.setBookNo(sel[0].toString());
					bookNo = dto.getBookNo();
				}
				
				List<Object[]> listregFee = (List<Object[]>)this.receiptDAO.findReceiptFeeTotal(params,"1");
				if(!listregFee.isEmpty() && listregFee!=null)
				{
					for(Object[] sel2 : listregFee)
					{
						if(sel2[2] !=null)
						{
							receiptNoTemp = sel2[2].toString().substring(0, sel2[2].toString().lastIndexOf("/"));
						}
						    receiptNo = receiptNo+" "+receiptNoTemp;
						    System.out.println(receiptNo);
						    
						if(sel2[3]!=null)
					    {
							dto.setFeeMny(Double.valueOf(sel2[3].toString()));
							fee1Mny = fee1Mny + dto.getFeeMny(); 
							System.out.println("fee1Mny==="+ fee1Mny);
					    }
					}
				}
	
				detail = "ใบเสร็จรับเงิน  "+"เล่มที่ "+dto.getBookNo()+" เลขที่  ";

				if(receiptNo!=null && !receiptNo.equals(""))
				{
					dto.setReceiptDetail(detail+"  "+receiptNo);
				}
			    System.out.println(dto.getReceiptDetail());
			    list.add(dto);
			    receiptNo="";
			}

		}else
		{
			ReceiptDTO dto = new ReceiptDTO();
			detail = "ใบเสร็จรับเงิน  "+"เล่มที่ - "+" เลขที่  -";
			dto.setReceiptDetail(detail);
			list.add(dto);
		}
		List<Object[]> listregFee2 = (List<Object[]>)this.receiptDAO.findReceiptFeeTotalBookNo(params,"2", params.getOrgId());
		if(!listregFee2.isEmpty() && listregFee2!=null)
		{
			for(Object[] sel : listregFee2)
			{
				ReceiptDTO dto = new ReceiptDTO();
				if(sel[0] !=null)
				{
					dto.setBookNo(sel[0].toString());
					params.setBookNo(sel[0].toString());
					bookNo = dto.getBookNo();
				}
				
				List<Object[]> listregFee = (List<Object[]>)this.receiptDAO.findReceiptFeeTotal(params,"2");
				if(!listregFee.isEmpty() && listregFee!=null)
				{
					for(Object[] sel2 : listregFee)
					{
						if(sel2[2] !=null)
						{
							receiptNoTemp = sel2[2].toString().substring(0, sel2[2].toString().lastIndexOf("/"));
						}
						    receiptNo = receiptNo+" "+receiptNoTemp;
						    System.out.println(receiptNo);
						    
						if(sel2[3]!=null)
					    {
							dto.setFeeMny(Double.valueOf(sel2[3].toString()));
							fee2Mny = fee2Mny + dto.getFeeMny(); ;
					    }
					}
				}
	
				detail = "ใบเสร็จรับเงิน  "+"เล่มที่ "+dto.getBookNo()+" เลขที่  ";

				dto.setReceiptDetail(detail+"  "+receiptNo);
			    System.out.println(dto.getReceiptDetail());
			    list1.add(dto);
			    receiptNo="";
			}

		}else
		{
			System.out.println("IN");
			ReceiptDTO dto = new ReceiptDTO();
			detail = "ใบเสร็จรับเงิน  "+"เล่มที่ - "+" เลขที่  -";
			dto.setReceiptDetail(detail);
			list1.add(dto);
		}
		List<Object[]> listregFee3 = (List<Object[]>)this.receiptDAO.findReceiptFeeTotalBookNo(params,"4", params.getOrgId());
		if(!listregFee3.isEmpty() && listregFee3!=null)
		{
			for(Object[] sel : listregFee3)
			{
				ReceiptDTO dto = new ReceiptDTO();
				if(sel[0] !=null)
				{
					dto.setBookNo(sel[0].toString());
					params.setBookNo(sel[0].toString());
					bookNo = dto.getBookNo();
				}
				
				List<Object[]> listregFee = (List<Object[]>)this.receiptDAO.findReceiptFeeTotal(params,"4");
				if(!listregFee.isEmpty() && listregFee!=null)
				{
					for(Object[] sel2 : listregFee)
					{
						if(sel2[2] !=null)
						{
							receiptNoTemp = sel2[2].toString().substring(0, sel2[2].toString().lastIndexOf("/"));
						}
						    receiptNo = receiptNo+" "+receiptNoTemp;
						    System.out.println(receiptNo);
						    
						if(sel2[3]!=null)
					    {
							dto.setFeeMny(Double.valueOf(sel2[3].toString()));
							fee3Mny = fee3Mny + dto.getFeeMny(); 
					    }
					}
				}
	
				detail = "ใบเสร็จรับเงิน  "+"เล่มที่ "+dto.getBookNo()+" เลขที่  ";
				
				dto.setReceiptDetail(detail+"  "+receiptNo);
			    System.out.println(dto.getReceiptDetail());
			    list2.add(dto);
			    receiptNo="";
			}

		}else
		{
			ReceiptDTO dto = new ReceiptDTO();
			detail = "ใบเสร็จรับเงิน  "+"เล่มที่ - "+" เลขที่  -";
			dto.setReceiptDetail(detail);
			list2.add(dto);
		}
		List<Object[]> listregFee4 = (List<Object[]>)this.receiptDAO.findReceiptFeeTotalBookNo(params,"3", params.getOrgId());
		if(!listregFee4.isEmpty() && listregFee4!=null)
		{
			for(Object[] sel : listregFee4)
			{
				ReceiptDTO dto = new ReceiptDTO();
				if(sel[0] !=null)
				{
					dto.setBookNo(sel[0].toString());
					params.setBookNo(sel[0].toString());
					bookNo = dto.getBookNo();
				}
				
				List<Object[]> listregFee = (List<Object[]>)this.receiptDAO.findReceiptFeeTotal(params,"3");
				if(!listregFee.isEmpty() && listregFee!=null)
				{
					for(Object[] sel2 : listregFee)
					{
						if(sel2[2] !=null)
						{
							receiptNoTemp = sel2[2].toString().substring(0, sel2[2].toString().lastIndexOf("/"));
						}
						    receiptNo = receiptNo+" "+receiptNoTemp;
						    System.out.println(receiptNo);
						    
						if(sel2[3]!=null)
					    {
							dto.setFeeMny(Double.valueOf(sel2[3].toString()));
							fee4Mny = fee4Mny + dto.getFeeMny(); 
					    }
					}
				}
	
				detail = "ใบเสร็จรับเงิน  "+"เล่มที่ "+dto.getBookNo()+" เลขที่  ";
				
				dto.setReceiptDetail(detail+"  "+receiptNo);
			    System.out.println(dto.getReceiptDetail());
			    list3.add(dto);
			    receiptNo="";
			}
		}else
		{
			ReceiptDTO dto = new ReceiptDTO();
			detail = "ใบเสร็จรับเงิน  "+"เล่มที่ - "+" เลขที่  -";
			dto.setReceiptDetail(detail);
			list3.add(dto);
		}
		List<Object[]> listregFee5 = (List<Object[]>)this.receiptDAO.findReceiptFeeTotalBookNo(params,"14", params.getOrgId());
		if(!listregFee5.isEmpty() && listregFee5!=null)
		{
			for(Object[] sel : listregFee5)
			{
				ReceiptDTO dto = new ReceiptDTO();
				if(sel[0] !=null)
				{
					dto.setBookNo(sel[0].toString());
					params.setBookNo(sel[0].toString());
					bookNo = dto.getBookNo();
				}
				
				List<Object[]> listregFee = (List<Object[]>)this.receiptDAO.findReceiptFeeTotal(params,"14");
				if(!listregFee.isEmpty() && listregFee!=null)
				{
					for(Object[] sel2 : listregFee)
					{
						if(sel2[2] !=null)
						{
							receiptNoTemp = sel2[2].toString().substring(0, sel2[2].toString().lastIndexOf("/"));
						}
						
						//Oat Edit 27/10/57 เอาใบเสร็จที่เงินเพิ่มเป็น 0 ออก
//						    receiptNo = receiptNo+" "+receiptNoTemp;
//						    System.out.println(receiptNo);
//						    
//						if(sel2[3]!=null)
//					    {
//							dto.setFeeMny(Double.valueOf(sel2[3].toString()));
//							fee5Mny = fee5Mny + dto.getFeeMny(); 
//					    }
						
						if(sel2[3]!=null)
					    {
							dto.setFeeMny(Double.valueOf(sel2[3].toString()));
							fee5Mny = fee5Mny + dto.getFeeMny(); 
					    }
						
						if(sel2[3]!=null && Double.valueOf(sel2[3].toString()) > 0)
							receiptNo = receiptNo+" "+receiptNoTemp;
						
					    System.out.println(receiptNo);
						
						
						//End Oat Edit 27/10/57
						
					}
				}
	
				detail = "ใบเสร็จรับเงิน  "+"เล่มที่ "+dto.getBookNo()+" เลขที่  ";

				dto.setReceiptDetail(detail+"  "+receiptNo);
			    System.out.println(dto.getReceiptDetail());
			    list4.add(dto);
			    receiptNo="";
			}
		}else
		{
			ReceiptDTO dto = new ReceiptDTO();
			detail = "ใบเสร็จรับเงิน  "+"เล่มที่ - "+" เลขที่  -";
			dto.setReceiptDetail(detail);
			list4.add(dto);
		}	
		List<Object[]> listregFee6 = (List<Object[]>)this.receiptDAO.findReceiptFeeTotalBookNo(params,"5", params.getOrgId());
		if(!listregFee6.isEmpty() && listregFee6!=null)
		{
			for(Object[] sel : listregFee6)
			{
				ReceiptDTO dto = new ReceiptDTO();
				if(sel[0] !=null)
				{
					dto.setBookNo(sel[0].toString());
					params.setBookNo(sel[0].toString());
					bookNo = dto.getBookNo();
				}
				
				List<Object[]> listregFee = (List<Object[]>)this.receiptDAO.findReceiptFeeTotal(params,"5");
				if(!listregFee.isEmpty() && listregFee!=null)
				{
					for(Object[] sel2 : listregFee)
					{
						if(sel2[2] !=null)
						{
							receiptNoTemp = sel2[2].toString().substring(0, sel2[2].toString().lastIndexOf("/"));
						}
						    receiptNo = receiptNo+" "+receiptNoTemp;
						    System.out.println(receiptNo);
						    
						if(sel2[3]!=null)
					    {
							dto.setFeeMny(Double.valueOf(sel2[3].toString()));
							fee6Mny = fee6Mny + dto.getFeeMny(); ;
					    }
					}
				}
	
				detail = "ใบเสร็จรับเงิน  "+"เล่มที่ "+dto.getBookNo()+" เลขที่  ";

				dto.setReceiptDetail(detail+"  "+receiptNo);
			    System.out.println(dto.getReceiptDetail());
			    list5.add(dto);
			    receiptNo="";
			}
		}else
		{
			ReceiptDTO dto = new ReceiptDTO();
			detail = "ใบเสร็จรับเงิน  "+"เล่มที่ - "+" เลขที่  -";
			dto.setReceiptDetail(detail);
			list5.add(dto);
		}	
		List<Object[]> listregFee7 = (List<Object[]>)this.receiptDAO.findReceiptFeeTotalBookNo(params,"12", params.getOrgId());
		if(!listregFee7.isEmpty() && listregFee7!=null)
		{
			for(Object[] sel : listregFee7)
			{
				ReceiptDTO dto = new ReceiptDTO();
				if(sel[0] !=null)
				{
					dto.setBookNo(sel[0].toString());
					params.setBookNo(sel[0].toString());
					bookNo = dto.getBookNo();
				}
				
				List<Object[]> listregFee = (List<Object[]>)this.receiptDAO.findReceiptFeeTotal(params,"12");
				if(!listregFee.isEmpty() && listregFee!=null)
				{
					for(Object[] sel2 : listregFee)
					{
						if(sel2[2] !=null)
						{
							receiptNoTemp = sel2[2].toString().substring(0, sel2[2].toString().lastIndexOf("/"));
						}
						    receiptNo = receiptNo+" "+receiptNoTemp;
						    System.out.println(receiptNo);
						    
						if(sel2[3]!=null)
					    {
							dto.setFeeMny(Double.valueOf(sel2[3].toString()));
							fee7Mny = fee7Mny + dto.getFeeMny(); 
					    }
					}
				}
	
				detail = "ใบเสร็จรับเงิน  "+"เล่มที่ "+dto.getBookNo()+" เลขที่  ";

				dto.setReceiptDetail(detail+"  "+receiptNo);
			    System.out.println(dto.getReceiptDetail());
			    list6.add(dto);
			    receiptNo="";
			}
		}else
		{
			ReceiptDTO dto = new ReceiptDTO();
			detail = "ใบเสร็จรับเงิน  "+"เล่มที่ - "+" เลขที่  -";
			dto.setReceiptDetail(detail);
			list6.add(dto);
		}		
		List<Object[]> listregFee8 = (List<Object[]>)this.receiptDAO.findReceiptFeeTotalBookNo(params,"6", params.getOrgId());
		if(!listregFee8.isEmpty() && listregFee8!=null)
		{
			for(Object[] sel : listregFee8)
			{
				ReceiptDTO dto = new ReceiptDTO();
				if(sel[0] !=null)
				{
					dto.setBookNo(sel[0].toString());
					params.setBookNo(sel[0].toString());
					bookNo = dto.getBookNo();
				}
				
				List<Object[]> listregFee = (List<Object[]>)this.receiptDAO.findReceiptFeeTotal(params,"6");
				if(!listregFee.isEmpty() && listregFee!=null)
				{
					for(Object[] sel2 : listregFee)
					{
						if(sel2[2] !=null)
						{
							receiptNoTemp = sel2[2].toString().substring(0, sel2[2].toString().lastIndexOf("/"));
						}
						    receiptNo = receiptNo+" "+receiptNoTemp;
						    System.out.println(receiptNo);
						    
						if(sel2[3]!=null)
					    {
							dto.setFeeMny(Double.valueOf(sel2[3].toString()));
							fee8Mny = fee8Mny + dto.getFeeMny(); 
					    }
					}
				}
	
				detail = "ใบเสร็จรับเงิน  "+"เล่มที่ "+dto.getBookNo()+" เลขที่  ";

				dto.setReceiptDetail(detail+"  "+receiptNo);
			    System.out.println(dto.getReceiptDetail());
			    list7.add(dto);
			    receiptNo="";
			}
		}else
		{
			ReceiptDTO dto = new ReceiptDTO();
			detail = "ใบเสร็จรับเงิน  "+"เล่มที่ - "+" เลขที่  -";
			dto.setReceiptDetail(detail);
			list7.add(dto);
		}		
		//หลักประกัน
		GuaranteeDTO guaDto = new GuaranteeDTO();
		guaDto.setDate(params.getReceiveOfficerDateFrom());
		List<Object[]> listregFee9 = (List<Object[]>)this.guaranteeDAO.findGuaranteeRefund(guaDto, params.getOrgId());
		if(!listregFee9.isEmpty() && listregFee9!=null)
		{
			for(Object[] sel : listregFee9)
			{
				ReceiptDTO dto = new ReceiptDTO();
				if(sel[1] !=null)
				{
					fee9Mny = Double.valueOf(sel[1].toString());
					System.out.println(">>>>>>>>>>>>>>>>>>>>GUARANTEE_ID = "+sel[2]);
				}
				
			/*	List<Object[]> listregFee = (List<Object[]>)this.receiptDAO.findReceiptFeeTotal(params,"7");
				if(!listregFee.isEmpty() && listregFee!=null)
				{
					for(Object[] sel2 : listregFee)
					{
						if(sel2[2] !=null)
						{
							receiptNoTemp = sel2[2].toString().substring(0, sel2[2].toString().lastIndexOf("/"));
						}
						    receiptNo = receiptNo+" "+receiptNoTemp;
						    System.out.println(receiptNo);
						    
						if(sel2[3]!=null)
					    {
							dto.setFeeMny(Double.valueOf(sel2[3].toString()));
							fee10Mny = fee10Mny + dto.getFeeMny(); 
							totalAllNumber = totalAllNumber + fee10Mny;
							System.out.println("totalAllNumber!!!!==="+ totalAllNumber);
					    }
					}
				}*/
	
				//detail = "ใบเสร็จรับเงิน  "+"เล่มที่ "+dto.getBookNo()+" เลขที่  ";
				detail = "ใบเสร็จรับเงิน  "+"เล่มที่ - "+" เลขที่  -";
				//dto.setReceiptDetail(detail+"  "+receiptNo);
				dto.setReceiptDetail(detail);
			    System.out.println(dto.getReceiptDetail());
			    list8.add(dto);
			    receiptNo="";
			}
		}else
		{
			ReceiptDTO dto = new ReceiptDTO();
			detail = "ใบเสร็จรับเงิน  "+"เล่มที่ - "+" เลขที่  -";
			dto.setReceiptDetail(detail);
			list8.add(dto);
		}
		
		List<Object[]> listregFee10 = (List<Object[]>)this.receiptDAO.findReceiptFeeTotalBookNo(params,"7", params.getOrgId());
		if(!listregFee10.isEmpty() && listregFee10!=null)
		{
			for(Object[] sel : listregFee10)
			{
				ReceiptDTO dto = new ReceiptDTO();
				if(sel[0] !=null)
				{
					dto.setBookNo(sel[0].toString());
					params.setBookNo(sel[0].toString());
					bookNo = dto.getBookNo();
				}
				
				List<Object[]> listregFee = (List<Object[]>)this.receiptDAO.findReceiptFeeTotal(params,"7");
				if(!listregFee.isEmpty() && listregFee!=null)
				{
					for(Object[] sel2 : listregFee)
					{
						if(sel2[2] !=null)
						{
							receiptNoTemp = sel2[2].toString().substring(0, sel2[2].toString().lastIndexOf("/"));
						}
						    receiptNo = receiptNo+" "+receiptNoTemp;
						    System.out.println(receiptNo);
						    
						if(sel2[3]!=null)
					    {
							dto.setFeeMny(Double.valueOf(sel2[3].toString()));
							fee10Mny = fee10Mny + dto.getFeeMny(); 
					    }
					}
				}
	
				//Oat Edit 10/10/57
				if(fee10Mny > 0)
				{
					detail = "ใบเสร็จรับเงิน  "+"เล่มที่ "+dto.getBookNo()+" เลขที่  ";
					dto.setReceiptDetail(detail+"  "+receiptNo);
				    System.out.println(dto.getReceiptDetail());
				    list9.add(dto);
				    receiptNo="";
				}
				else if(list9.size() == 0)
				{
					ReceiptDTO dto2 = new ReceiptDTO();
					detail = "ใบเสร็จรับเงิน  "+"เล่มที่ - "+" เลขที่  -";
					dto2.setReceiptDetail(detail);
					list9.add(dto2);
				}
				//
				
				
			}
		}else
		{
			ReceiptDTO dto = new ReceiptDTO();
			detail = "ใบเสร็จรับเงิน  "+"เล่มที่ - "+" เลขที่  -";
			dto.setReceiptDetail(detail);
			list9.add(dto);
		}
		
		totalAllNumber = fee1Mny+fee2Mny+fee3Mny+fee4Mny+fee5Mny+fee6Mny+fee7Mny+fee8Mny+fee9Mny+fee10Mny;
		totalAllText = totalAllNumber.toString();
		totalAllText = ThaiBahtUtil.getBahtText(totalAllText);
		
		
		model.put("day", day);
		model.put("fee1Mny", fee1Mny);
		model.put("fee2Mny", fee2Mny);
		model.put("fee3Mny", fee3Mny);
		model.put("fee4Mny", fee4Mny);
		model.put("fee5Mny", fee5Mny);
		model.put("fee6Mny", fee6Mny);
		model.put("fee7Mny", fee7Mny);
		model.put("fee8Mny", fee8Mny);
		model.put("fee9Mny", fee9Mny);
		model.put("fee10Mny", fee10Mny);
		model.put("totalAllNumber", totalAllNumber);
		model.put("totalAllText", totalAllText);
		model.put("regFee1", list);
		model.put("regFee2", list1);
		model.put("regFee3", list2);
		model.put("regFee4", list3);
		model.put("regFee5", list4);
		model.put("regFee6", list5);
		model.put("regFee7", list6);
		model.put("regFee8", list7);
		model.put("regFee9", list8);
		model.put("regFee10", list9);

		
		return model;
	}

}
