package sss.dot.tourism.service.printreportguaranteeandfeeregistrationtype;

import java.util.Map;

import com.sss.aut.service.User;

public interface IPrintReportGuaranteeAndFeeRegistrationTypeService {
	public Map<String, ?> getPrintReportGuaranteeAndFeeRegistrationType(Object object, User user) throws Exception;
}
