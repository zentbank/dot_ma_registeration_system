package sss.dot.tourism.service.printreportguaranteeandfeeregistrationtype;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.registration.ReceiptDAO;
import sss.dot.tourism.dto.registration.ReceiptDTO;
import sss.dot.tourism.util.DateUtils;

import com.sss.aut.service.User;

@Repository("printReportGuaranteeAndFeeRegistrationTypeService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PrintReportGuaranteeAndFeeRegistrationTypeService implements IPrintReportGuaranteeAndFeeRegistrationTypeService{

	@Autowired
	ReceiptDAO receiptDAO;
	
	public Map<String, ?> getPrintReportGuaranteeAndFeeRegistrationType(
			Object object, User user) throws Exception {
		
		Map model = new HashMap();
		//List list = new ArrayList();
		
		List<ReceiptDTO> list = new ArrayList<ReceiptDTO>();
		
		ReceiptDTO params = (ReceiptDTO)object;
		
		List<Object[]> listObj = (List<Object[]>)this.receiptDAO.findPrintReportGuaranteeAndFeeRegistrationType(params);
		
		if(listObj!=null && !listObj.isEmpty())
		{	
			
			for(Object[] sel : listObj)
			{
				ReceiptDTO dto = new ReceiptDTO();
				if(sel[0] !=null)
				{
					Date d = ((Date)sel[0]);
					String date = DateUtils.getProcessDateFormatThai().format(d);
				}
			}
			
		}
		model.put("regPrintGuaranteeAndFee", list);
		return model;
	}

}
