package sss.dot.tourism.service.punishment;

import java.util.List;

import com.sss.aut.service.User;

public interface IPunishmentEvidenceService {
	
	public List getAll(Object obj, User user)throws Exception;
	
	public Object getById(Object obj, User user) throws Exception;
	
	public void save(Object obj, User user)throws Exception;
	
	public void delete(Object[] obj, User user)throws Exception;
	
	
}
