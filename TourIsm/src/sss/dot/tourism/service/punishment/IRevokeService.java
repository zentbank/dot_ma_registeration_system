package sss.dot.tourism.service.punishment;

import java.util.List;

import com.sss.aut.service.User;

public interface IRevokeService {
	public List getActAll(Object obj, User user)throws Exception;
	
	public Object getRevokeById(Object obj, User user) throws Exception;
	public Object getTraderPrepareRevoke(Object obj, User user) throws Exception;
	
	public void saveRevoke(Object revoke,Object[] object, User user)throws Exception;
	
	public List getAll(Object obj, User user)throws Exception;
	public List countAll(Object obj, User user) throws Exception;
	
	public void revoke(Object object, User user)throws Exception;
	
	public void unRevoke(Object object, User user)throws Exception;
	
	public void cancelRevoke(Object object, User user)throws Exception;
	
	public Object getAlertAll(Object object, User user) throws Exception;
	public Object saveAlert(Object obj, User user) throws Exception;
	
	
	public Object getRevokeDocx(Object obj, User user) throws Exception;
}
