package sss.dot.tourism.service.punishment;

import java.util.List;

import com.sss.aut.service.User;

public interface ISuspensionAlertService {
	public Object getAlertAll(Object object, User user) throws Exception;
	public Object save(Object obj, User user) throws Exception;

	
}
