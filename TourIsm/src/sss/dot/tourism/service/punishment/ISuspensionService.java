package sss.dot.tourism.service.punishment;

import java.util.List;

import com.sss.aut.service.User;

import sss.dot.tourism.domain.MasAct;

public interface ISuspensionService {
	public List getActAll(Object obj, User user)throws Exception;
	
	public Object getSuspensionById(Object obj, User user) throws Exception;
	public Object getTraderPrepareSuspension(Object obj, User user) throws Exception;
	
	public void saveSuspension(Object suspension,Object[] object, User user)throws Exception;
	
	public List getAll(Object obj, User user)throws Exception;
	public List countAll(Object obj, User user) throws Exception;
	
	public void suspend(Object object, User user)throws Exception;
	
	public void unSuspend(Object object, User user)throws Exception;
	
	public void cancelSuspend(Object object, User user)throws Exception;
	
	public void deactivateSuspend(Object object, User user)throws Exception;
	
	
	public Object getSuspensionDocx(Object obj, User user) throws Exception;
	
	public Object getSuspensionOrderDocx(String licenseNo) throws Exception;
	public Object getSuspensionOrderDocx(Object obj, User user) throws Exception;
	
	public MasAct getActBySupendId(Object obj, User user)throws Exception;
	
	
}
