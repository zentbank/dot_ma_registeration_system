package sss.dot.tourism.service.punishment;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import sss.dot.tourism.dao.punishment.PunishmentEvidenceDAO;
import sss.dot.tourism.domain.ComplaintDoc;
import sss.dot.tourism.domain.ComplaintLicense;
import sss.dot.tourism.domain.PunishmentEvidence;
import sss.dot.tourism.domain.RevokeLicense;
import sss.dot.tourism.domain.SuspensionLicense;
import sss.dot.tourism.dto.punishment.PunishmentEvidenceDTO;
import sss.dot.tourism.service.file.FileManage;
import sss.dot.tourism.util.ConstantUtil;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.RecordStatus;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.User;
@Repository("punishmentEvidenceService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PunishmentEvidenceService implements IPunishmentEvidenceService {

	@Autowired
	PunishmentEvidenceDAO punishmentEvidenceDAO;
	
	public List getAll(Object obj, User user) throws Exception {
		if(!(obj instanceof PunishmentEvidenceDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		PunishmentEvidenceDTO param = (PunishmentEvidenceDTO)obj;
		
		List<PunishmentEvidenceDTO> list = new ArrayList<PunishmentEvidenceDTO>();
		
		List<PunishmentEvidence> listAll = new ArrayList<PunishmentEvidence>();
		if(param.getSuspendId() > 0)
		{
			listAll = this.punishmentEvidenceDAO.findBySuspensionLicense(param.getSuspendId());
		}else if(param.getRevokeId() > 0)
		{
			listAll = this.punishmentEvidenceDAO.findByRevokeLicense(param.getRevokeId());
		}
		
		if(!listAll.isEmpty())
		{
			for(PunishmentEvidence edv: listAll)
			{
				PunishmentEvidenceDTO dto = new PunishmentEvidenceDTO();
				
				ObjectUtil.copy(edv, dto);
				
				if(edv.getSuspensionLicense() != null)
				{
					dto.setSuspendId(edv.getSuspensionLicense().getSuspendId());
				}
				if(edv.getRevokeLicense() != null)
				{
					dto.setRevokeId(edv.getRevokeLicense().getRevokeId());
				}
				
				list.add(dto);
			}
		}
		
		return list;
	}

	public Object getById(Object obj, User user) throws Exception {
		if(!(obj instanceof PunishmentEvidenceDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		PunishmentEvidenceDTO param = (PunishmentEvidenceDTO)obj;
		
		PunishmentEvidence edv = (PunishmentEvidence)this.punishmentEvidenceDAO.findByPrimaryKey(param.getPunishmentId());
		
		PunishmentEvidenceDTO dto = new PunishmentEvidenceDTO();
		
		ObjectUtil.copy(edv, dto);
		
		if(edv.getSuspensionLicense() != null)
		{
			dto.setSuspendId(edv.getSuspensionLicense().getSuspendId());
		}
		if(edv.getRevokeLicense() != null)
		{
			dto.setRevokeId(edv.getRevokeLicense().getRevokeId());
		}
		
		return dto;
	}
	
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
	public void save(Object obj, User user) throws Exception {
		if(!(obj instanceof PunishmentEvidenceDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		PunishmentEvidenceDTO param = (PunishmentEvidenceDTO)obj;
		
		PunishmentEvidence edv = new PunishmentEvidence();
		
		if(this.saveFile(param, param.getEvidence(), user))
		{
			ObjectUtil.copy(param, edv);
			
			if(param.getSuspendId() > 0)
			{
				SuspensionLicense suspensionLicense = new SuspensionLicense();
				suspensionLicense.setSuspendId(param.getSuspendId());
				
				edv.setSuspensionLicense(suspensionLicense);
			}else if(param.getRevokeId() > 0)
			{
				RevokeLicense revokeLicense = new RevokeLicense();
				revokeLicense.setRevokeId(param.getRevokeId());
				
				edv.setRevokeLicense(revokeLicense);
			}
			
			edv.setCreateUser(user.getUserName());
			edv.setCreateDtm(new Date());
			edv.setRecordStatus(RecordStatus.NORMAL.getStatus());
			
			Long punishmentId = (Long)this.punishmentEvidenceDAO.insert(edv);
			param.setPunishmentId(punishmentId);
		}
		else
		{
			throw new IOException();
		}
		

		
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
	public void delete(Object[] object, User user) throws Exception {
		if(!(object instanceof PunishmentEvidenceDTO[]))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		for (Object obj : object) {
			
			PunishmentEvidenceDTO param = (PunishmentEvidenceDTO)obj;
			
			PunishmentEvidence edv = (PunishmentEvidence)this.punishmentEvidenceDAO.findByPrimaryKey(param.getPunishmentId());
			
			FileManage fm = new FileManage();
			fm.deleteFile(edv.getPunishmentDocPath());
			
			this.punishmentEvidenceDAO.delete(edv);
		}
	}
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public boolean saveFile(Object object, MultipartFile file, User user) throws Exception
	{
		if(!(object instanceof PunishmentEvidenceDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		PunishmentEvidenceDTO param = (PunishmentEvidenceDTO)object;
		
		//TraderType
		String traderTypeName = "";
		if(param.getTraderType().equals(TraderType.TOUR_COMPANIES.getStatus()))
		{
			traderTypeName = "Business";
		}
		if(param.getTraderType().equals(TraderType.GUIDE.getStatus()))
		{
			traderTypeName = "Guide";
		}
		if(param.getTraderType().equals(TraderType.LEADER.getStatus()))
		{
			traderTypeName = "Tourleader";
		}
		
		String punishmentFolder = "Fake";
		if(param.getSuspendId() > 0)
		{
			punishmentFolder = "Suspension";
		}else if(param.getRevokeId() > 0)
		{
			punishmentFolder = "Revoke";
		}
		
		//LicenseNo
		String licenseNoStr = param.getLicenseNo();
		licenseNoStr = licenseNoStr.replaceAll("[.]", "");
		licenseNoStr = licenseNoStr.replaceAll("[/]", "");
		licenseNoStr = licenseNoStr.replaceAll("[-]", "");
		
		String realFileName = Calendar.getInstance().getTime().getTime()+"";
		
		Calendar cal = Calendar.getInstance();
		DateFormat f = DateUtils.getProcessDateFormatEng();
		
		String date = f.format(cal.getTime());
		String fold = date.replaceAll("[-]", "");
		
		//postfix FileName
		String extension = ".pdf";
		String originalFilename = file.getOriginalFilename();
		int dot = originalFilename.lastIndexOf('.');
		if(dot != -1)
		{
			extension = originalFilename.substring(dot);
		}
		
		String root = ConstantUtil.ROOT_PATH; 
    	String path = root+punishmentFolder+java.io.File.separator+traderTypeName+java.io.File.separator+licenseNoStr+java.io.File.separator+fold;
    	String fileName = java.io.File.separator+realFileName+extension;
    	
    	String fullpath = path+fileName;
    	System.out.println("full path = "+fullpath);
    	
    	param.setPunishmentDocPath(fullpath);
    	
    	boolean savefile = false;
    	if(file != null)
	    {
   		
			//Upload File
	    	FileManage fm = new FileManage();
	    	savefile = fm.uploadFile(file.getInputStream(), path, fileName);
	    	
	    	
	    }
		
    	return savefile;
	}
	

}
