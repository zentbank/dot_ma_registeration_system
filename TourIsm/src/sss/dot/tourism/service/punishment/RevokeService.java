package sss.dot.tourism.service.punishment;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.AdmUserDAO;
import sss.dot.tourism.dao.mas.MasActDAO;
import sss.dot.tourism.dao.mas.MasPrefixDAO;
import sss.dot.tourism.dao.mas.MasRunningNoDAO;
import sss.dot.tourism.dao.punishment.RevokeAlertDAO;
import sss.dot.tourism.dao.punishment.RevokeDetailDAO;
import sss.dot.tourism.dao.punishment.RevokeLicenseDAO;
import sss.dot.tourism.dao.punishment.RevokeMapTraderDAO;
import sss.dot.tourism.dao.registration.GuaranteeDAO;
import sss.dot.tourism.dao.registration.RegistrationDAO;
import sss.dot.tourism.dao.registration.TraderAddressDAO;
import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.domain.AdmUser;
import sss.dot.tourism.domain.Guarantee;
import sss.dot.tourism.domain.MasAct;
import sss.dot.tourism.domain.Person;
import sss.dot.tourism.domain.Registration;
import sss.dot.tourism.domain.RevokeAlert;
import sss.dot.tourism.domain.RevokeDetail;
import sss.dot.tourism.domain.RevokeLicense;
import sss.dot.tourism.domain.RevokeMapTrader;
import sss.dot.tourism.domain.SuspensionDetail;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.domain.TraderAddress;
import sss.dot.tourism.dto.punishment.MasActDTO;
import sss.dot.tourism.dto.punishment.RevokeLicenseDTO;
import sss.dot.tourism.dto.punishment.SuspensionAlertDTO;
import sss.dot.tourism.dto.registration.LicenseDetailDTO;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.service.trader.ITraderService;
import sss.dot.tourism.util.ActType;
import sss.dot.tourism.util.AlertStatus;
import sss.dot.tourism.util.AlertType;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.GuaranteeStatus;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.PersonType;
import sss.dot.tourism.util.RecordStatus;
import sss.dot.tourism.util.RevokeStatus;
import sss.dot.tourism.util.TraderAddressType;
import sss.dot.tourism.util.TraderCategory;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.User;
@Repository("revokeService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class RevokeService implements IRevokeService {

	@Autowired
	TraderDAO traderDAO;
	@Autowired
	MasActDAO masActDAO;
	
	@Autowired
	MasRunningNoDAO masRunningNoDAO;
	@Autowired
	AdmUserDAO admUserDAO;
	@Autowired
	MasPrefixDAO masPrefixDAO;
	@Autowired
	ITraderService traderService;
	
	@Autowired
	RevokeLicenseDAO revokeLicenseDAO;
	@Autowired
	RevokeMapTraderDAO revokeMapTraderDAO;
	@Autowired
	RevokeDetailDAO revokeDetailDAO;
	@Autowired
	RevokeAlertDAO revokeAlertDAO;
	
	@Autowired
	GuaranteeDAO guaranteeDAO;
	
	@Autowired
	RegistrationDAO registrationDAO;
	
	@Autowired
	TraderAddressDAO traderAddressDAO;
	
	public List getActAll(Object obj, User user) throws Exception {
		if(!(obj instanceof MasActDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		MasActDTO param =  (MasActDTO)obj;
		
		List<MasActDTO> list = new ArrayList<MasActDTO>();
		
		if(param.getRevokeId() > 0)
		{
			list = this.masActDAO.findbyRevokeIdAndTraderType(param.getRevokeId(), param.getTraderType());
		}
		else
		{
			List<MasAct> listAll = (List<MasAct>) this.masActDAO.findByTraderType(param.getTraderType(), ActType.REVOKE.getStatus());
			
			
			if(!listAll.isEmpty())
			{
				for(MasAct act: listAll)
				{
					MasActDTO dto = new MasActDTO();
					ObjectUtil.copy(act, dto);
					list.add(dto);
				}
			}
		}
		
		return list;
	}

	public Object getRevokeById(Object obj, User user) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
	public Object getTraderPrepareRevoke(Object obj, User user)
			throws Exception {
		if(!(obj instanceof RevokeLicenseDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		RevokeLicenseDTO dto = (RevokeLicenseDTO)obj; 
		
		RevokeLicenseDTO param = (RevokeLicenseDTO)obj; 
		
		Trader trader = (Trader)this.traderDAO.findByPrimaryKey(param.getTraderId());
		
		Person person = trader.getPerson();
		
		String runNo = masRunningNoDAO.getRevokeTempNo(user.getUserData().getOrgId(), trader.getTraderType());
		ObjectUtil.copy(trader, dto);
		ObjectUtil.copy(person, dto);
		
		if(PersonType.CORPORATE.getStatus().equals(person.getPersonType()))
		{
			String traderOwnerName = person.getMasPrefix().getPrefixName() + person.getFirstName() + (null==person.getMasPosfix()?"":person.getMasPosfix().getPostfixName());
			dto.setTraderOwnerName(traderOwnerName);
		}
		else
		{
			String traderOwnerName = person.getMasPrefix().getPrefixName() + person.getFirstName() + " " + person.getLastName();
			dto.setTraderOwnerName(traderOwnerName);
		}
		
		dto.setTraderCategoryName(TraderCategory.getMeaning(trader.getTraderType(), trader.getTraderCategory()));
		
		Calendar cal = Calendar.getInstance();
		
		DateFormat df = DateUtils.getProcessDateFormatThai();
		
		dto.setReceiveNo(runNo);
		dto.setReceiveDate(df.format(cal.getTime()));
		dto.setOfficerName(user.getUserData().getUserFullName());
		
		return dto;
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
	public void saveRevoke(Object revokeParam, Object[] object, User user)
			throws Exception {
		if(!(revokeParam instanceof RevokeLicenseDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		RevokeLicenseDTO param = (RevokeLicenseDTO)revokeParam;
		RevokeLicense revoke = null;
		if(param.getRevokeId() <= 0)
		{
			Trader trader = (Trader)this.traderDAO.findByPrimaryKey(param.getTraderId());
			
			String runNo = masRunningNoDAO.getRevokeNo(user.getUserData().getOrgId(), trader.getTraderType());
			
			revoke = new RevokeLicense();
			
			ObjectUtil.copy(param, revoke);
			
			revoke.setReceiveNo(runNo);
			
			AdmUser admUser = new AdmUser();
			admUser.setUserId(user.getUserId());
			
			//OAT ADD
			revoke.setOrganization(trader.getOrganization());
			//
			
			revoke.setRevokeStatus(RevokeStatus.WAIT.getStatus());
			revoke.setAdmUser(admUser);
			revoke.setRecordStatus(RecordStatus.NORMAL.getStatus());
			revoke.setCreateUser(user.getUserName());
			revoke.setCreateDtm(new Date());
			
			Long revokeId = (Long)this.revokeLicenseDAO.insert(revoke);
			revoke.setRevokeId(revokeId);
			param.setRevokeId(revokeId);
			
			RevokeMapTrader mapTrader = new RevokeMapTrader();
			mapTrader.setRevokeLicense(revoke);
			mapTrader.setLicenseNo(trader.getLicenseNo());
			mapTrader.setTraderType(trader.getTraderType());
			
			this.revokeMapTraderDAO.insert(mapTrader);
			
			
			

			
			
		}
		else
		{
			revoke = (RevokeLicense) this.revokeLicenseDAO.findByPrimaryKey(param.getRevokeId());
			
			ObjectUtil.copy(param, revoke);
			
			AdmUser admUser = new AdmUser();
			admUser.setUserId(user.getUserId());
			
			revoke.setRevokeStatus(param.getRevokeStatus());
			revoke.setAdmUser(admUser);
			revoke.setRecordStatus(RecordStatus.NORMAL.getStatus());
			revoke.setLastUpdUser(user.getUserName());
			revoke.setLastUpdDtm(new Date());
			
			this.revokeLicenseDAO.update(revoke);
			
			RevokeAlert revokeAlert = new RevokeAlert();
			
			revokeAlert.setRevokeLicense(revoke);
			revokeAlert.setAlertType(AlertType.EMAIl.getStatus());
			revokeAlert.setAlertStatus(AlertStatus.WAIT.getStatus());
			revokeAlert.setAlertDate(new Date());
			revokeAlert.setRecordStatus(RecordStatus.NORMAL.getStatus());
			revokeAlert.setCreateDtm(new Date());
			revokeAlert.setCreateUser(user.getUserName());
			
			this.revokeAlertDAO.insert(revokeAlert);
		}
		
		this.addAct(object, revoke, user);
		
		
		// send mail

	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	private void addAct(Object[] object, RevokeLicense revoke, User user) throws Exception
	{
		if(object.length > 0)
		{
			List<RevokeDetail> listOldAct = this.revokeDetailDAO.findByRevokeLicense(revoke.getRevokeId());
			if(!listOldAct.isEmpty())
			{
				for(RevokeDetail masAct: listOldAct)
				{
					this.revokeDetailDAO.delete(masAct);
				}
				

			}
			
			for(Object obj: object)
			{
				MasActDTO actDTO = (MasActDTO)obj;
				RevokeDetail revokeDetail = new RevokeDetail();
				MasAct masAct = new MasAct();
				
				masAct.setMasActId(actDTO.getMasActId());
				
				revokeDetail.setMasAct(masAct);
				revokeDetail.setRevokeLicense(revoke);
				revokeDetail.setRecordStatus(RecordStatus.NORMAL.getStatus());
				revokeDetail.setCreateUser(user.getUserName());
				revokeDetail.setCreateDtm(new Date());
				
				revokeDetailDAO.insert(revokeDetail);
			}
		}
	}
	
	public List countAll(Object obj, User user) throws Exception
	{
		if(!(obj instanceof RevokeLicenseDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}

		RevokeLicenseDTO param = (RevokeLicenseDTO)obj;
		
		return this.revokeLicenseDAO.findRevokeLicensePaging(param, param.getStart(), param.getLimit());
		
	}

	public List getAll(Object obj, User user) throws Exception {
		if(!(obj instanceof RevokeLicenseDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		List<RevokeLicenseDTO> list = new ArrayList<RevokeLicenseDTO>();
		
		RevokeLicenseDTO param = (RevokeLicenseDTO)obj;
		
		List<Object[]> listAll = this.revokeLicenseDAO.findRevokeLicensePaging(param, param.getStart(), param.getLimit());
		
		DateFormat df = DateUtils.getProcessDateFormatThai();
		
		if(!listAll.isEmpty())
		{
			for(Object[] persistence :listAll)
			{
				RevokeLicenseDTO dto = new RevokeLicenseDTO();
				
				//   sqlQuery.addScalar("TRADER_ID", Hibernate.LONG); //0
				dto.setTraderId(Long.valueOf(persistence[0].toString()));
				
				//sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING); //1
				dto.setLicenseNo(persistence[1]==null?"":persistence[1].toString());
				
				//sqlQuery.addScalar("TRADER_TYPE", Hibernate.STRING);//3
				dto.setTraderType(persistence[3]==null?"":persistence[3].toString());
				dto.setTraderTypeName(TraderType.getMeaning(dto.getTraderType()));
				

	
				
//				sqlQuery.addScalar("IDENTITY_NO", Hibernate.STRING);//17
				dto.setIdentityNo(persistence[17]==null?"":persistence[17].toString());
//			    sqlQuery.addScalar("PERSON_ID", Hibernate.LONG);//4
				
				//sqlQuery.addScalar("TRADER_CATEGORY", Hibernate.STRING);//18
				dto.setTraderCategory(persistence[18]==null?"":persistence[18].toString());
				dto.setTraderCategoryName(TraderCategory.getMeaning(dto.getTraderType(), dto.getTraderCategory()));
			
//			   
//			    sqlQuery.addScalar("POSTFIX_NAME", Hibernate.STRING);//8
//			    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);//9
				
				
				
			    //sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING); //2
				if(TraderType.TOUR_COMPANIES.equals(dto.getTraderType()))
				{
					dto.setTraderName(persistence[2]==null?"":persistence[2].toString());
					
//				    sqlQuery.addScalar("TRADER_NAME_EN", Hibernate.STRING);//19
					dto.setTraderNameEn(persistence[19]==null?"":persistence[19].toString());
					
					//sqlQuery.addScalar("PREFIX_NAME", Hibernate.STRING);//5
					String prefixName = persistence[5]==null?"":persistence[5].toString();
					//sqlQuery.addScalar("FIRST_NAME", Hibernate.STRING);//6
					String firstName = persistence[6]==null?"":persistence[6].toString();
	
					
					String postfixName = persistence[8]==null?"":persistence[8].toString();
					
					dto.setTraderOwnerName(prefixName+firstName +postfixName);
				}
				else
				{
					//sqlQuery.addScalar("PREFIX_NAME", Hibernate.STRING);//5
					String prefixName = persistence[5]==null?"":persistence[5].toString();
					//sqlQuery.addScalar("FIRST_NAME", Hibernate.STRING);//6
					String firstName = persistence[6]==null?"":persistence[6].toString();
				    // sqlQuery.addScalar("LAST_NAME", Hibernate.STRING);//7
					String lastName = persistence[7]==null?"":persistence[7].toString();
					
					dto.setTraderName(prefixName+firstName +" " + lastName);
					dto.setTraderOwnerName(dto.getTraderName());
				    
				}
				
			    

			    
//			    sqlQuery.addScalar("REVOKE_ID", Hibernate.LONG);//10
				dto.setRevokeId(Long.valueOf(persistence[10].toString()));
				//sqlQuery.addScalar("RECEIVE_DATE", Hibernate.DATE);//11
				if(persistence[11]!=null)
				{
					dto.setReceiveDate(df.format(persistence[11]));
					dto.setSuspensionDate(dto.getReceiveDate());
				}
				
				
			    
//			    sqlQuery.addScalar("RECEIVE_NO", Hibernate.STRING);//12
				dto.setReceiveNo(persistence[12]==null?"":persistence[12].toString());
				dto.setSuspensionNo(dto.getReceiveNo());
				
				
//			    sqlQuery.addScalar("CANCEL_REMARK", Hibernate.STRING);//20	
//				 sqlQuery.addScalar("REVOKE_DETAIL", Hibernate.STRING);//21
				dto.setRevokeDetail(persistence[21]==null?"":persistence[21].toString());
				
				 
				    
				
//			    sqlQuery.addScalar("REVOKE_DATE", Hibernate.DATE);//13
				if(persistence[13]!=null)
				{
					dto.setRevokeDate(df.format(persistence[13]));
				}
//			   sqlQuery.addScalar("CANCEL_DATE", Hibernate.DATE);//14
				if(persistence[14]!=null)
				{
					dto.setCancelDate(df.format(persistence[14]));
				}
//			    sqlQuery.addScalar("OFFICER_NAME", Hibernate.LONG);//15
				if(persistence[15] != null)
				{
					AdmUser admUser = (AdmUser) this.admUserDAO.findByPrimaryKey(Long.valueOf(persistence[15].toString()));
					String prefix = admUser.getMasPrefix().getPrefixName();
					dto.setOfficerName(prefix + admUser.getUserName() +" "+ admUser.getUserLastname());
					
					if(admUser.getUserId() == user.getUserId())
					{
						dto.setOfficerAsOwner("1");
					}
					else
					{
						dto.setOfficerAsOwner("0");
					}
				}
				
				
//			    sqlQuery.addScalar("REVOKE_STATUS", Hibernate.STRING);//16
				dto.setRevokeStatus(persistence[16]==null?"":persistence[16].toString());
				dto.setRevokeStatusName(RevokeStatus.getMeaning(dto.getRevokeStatus()));
				
				list.add(dto);

			}
		}
		
		return list;
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
	public void revoke(Object obj, User user) throws Exception {
		if(!(obj instanceof RevokeLicenseDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		RevokeLicenseDTO param = (RevokeLicenseDTO)obj;
		RevokeLicense revokeLicense = (RevokeLicense)this.revokeLicenseDAO.findByPrimaryKey(param.getRevokeId());
		
		DateFormat df = DateUtils.getProcessDateFormatThai();
		
		Date revokeDate = df.parse(param.getRevokeDate());
		revokeLicense.setRevokeDate(revokeDate);
		
		revokeLicense.setRevokeStatus(RevokeStatus.REVOKE.getStatus());
		revokeLicense.setLastUpdUser(user.getUserName());
		revokeLicense.setLastUpdDtm(new Date());
		
		this.revokeLicenseDAO.update(revokeLicense);
		
		List<RevokeMapTrader> listRevokeMapTrader =  this.revokeMapTraderDAO.findByRevoleLicense(revokeLicense.getRevokeId());
		
		RegistrationDTO regDTO = new RegistrationDTO();
		
		if(!listRevokeMapTrader.isEmpty())
		{
			RevokeMapTrader map = listRevokeMapTrader.get(0);
			regDTO.setLicenseNo(map.getLicenseNo());
			regDTO.setTraderType(map.getTraderType());
			
			regDTO.setPunishmentDate(revokeLicense.getRevokeDate());
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(revokeLicense.getRevokeDate());
			
			cal.add(Calendar.MONTH, 60);
			cal.add(Calendar.DATE, -1);
			
			regDTO.setPunishmentExpireDate(cal.getTime());
		}
		
		// suspend all trader
		this.traderService.revoke(regDTO, user);

	}

	public void unRevoke(Object obj, User user) throws Exception {
		

	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
	public void cancelRevoke(Object obj, User user) throws Exception {
		if(!(obj instanceof RevokeLicenseDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		RevokeLicenseDTO param = (RevokeLicenseDTO)obj;
		RevokeLicense revokeLicense = (RevokeLicense)this.revokeLicenseDAO.findByPrimaryKey(param.getRevokeId());
		

		
		List<RevokeMapTrader> listRevokeMapTrader = (List<RevokeMapTrader>)this.revokeMapTraderDAO.findByRevoleLicense(revokeLicense.getRevokeId());
		RegistrationDTO regDTO = new RegistrationDTO();
		
		if(!listRevokeMapTrader.isEmpty())
		{
			RevokeMapTrader map = listRevokeMapTrader.get(0);
			regDTO.setLicenseNo(map.getLicenseNo());
			regDTO.setTraderType(map.getTraderType());
		}
		
		List<Guarantee> listGuarantee = (List<Guarantee>)this.guaranteeDAO.findByTrader(regDTO.getLicenseNo(), regDTO.getTraderType(), 0);
		
		if(!listGuarantee.isEmpty())
		{
			Guarantee guarantee = listGuarantee.get(0);
			
			if(GuaranteeStatus.REFUND.getStatus().equals(guarantee.getGuaranteeStatus()))
			{
				throw new Exception("ไม่สามารถยกเลิกได้ จะต้องยกเลิกการคืนหลักประกันก่อน");
			}
		}
		
	
		revokeLicense.setCancelDate(new Date());
		revokeLicense.setCancelRemark(param.getCancelRemark()==null?"":param.getCancelRemark());
		revokeLicense.setRevokeStatus(RevokeStatus.CANCEL.getStatus());
		revokeLicense.setLastUpdUser(user.getUserName());
		revokeLicense.setLastUpdDtm(new Date());
		
		this.revokeLicenseDAO.update(revokeLicense);
	
		this.traderService.unRevoke(regDTO, user);

	}

	public Object getAlertAll(Object object, User user) throws Exception {
		if(!(object instanceof SuspensionAlertDTO))
		{
			throw new IllegalArgumentException("object not instanceof SuspensionAlertDTO");
		}
		
		SuspensionAlertDTO dto = new SuspensionAlertDTO();
		
		SuspensionAlertDTO param = (SuspensionAlertDTO)object;
		
		List<RevokeAlert> listRevokeAlert = (List<RevokeAlert>) this.revokeAlertDAO.findByRevokeLicense(param.getRevokeId());

		
		if(!listRevokeAlert.isEmpty())
		{
			RevokeAlert emailAlert = listRevokeAlert.get(0);
			
			dto.setRevokeId(emailAlert.getRevokeLicense().getRevokeId());
			
			
			DateFormat df = DateUtils.getProcessDateFormatThai();
			
			if(emailAlert.getAlertDate() != null)
			{
				dto.setEmailAlertDate(df.format(emailAlert.getAlertDate()));
			}
			if(emailAlert.getResponseDate() != null)
			{
				dto.setEmailResponseDate(df.format(emailAlert.getResponseDate()));
			}
			if((emailAlert.getAlertStatus() != null) && (!emailAlert.getAlertStatus().isEmpty()))
			{
				dto.setEmailAlertStatus(AlertStatus.getMeaning(emailAlert.getAlertStatus()));
			}

			
			if(listRevokeAlert.size() > 1)
			{
				RevokeAlert postAlert = listRevokeAlert.get(1);
				
				
				ObjectUtil.copy(postAlert, dto);
				
				if((dto.getAlertType() != null) && (!dto.getAlertType().isEmpty()))
				{
					dto.setAlertTypeName(AlertType.getMeaning(dto.getAlertType()));
				}
				
				if((dto.getAlertStatus() != null) && (!dto.getAlertStatus().isEmpty()))
				{
					dto.setAlertStatusName(AlertStatus.getMeaning(dto.getAlertStatus()));
				}
			}	
		}
			
			
		return dto;
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
	public Object saveAlert(Object obj, User user) throws Exception {
		if(!(obj instanceof SuspensionAlertDTO))
		{
			throw new IllegalArgumentException("object not instanceof SuspensionAlertDTO");
		}
			
		SuspensionAlertDTO param = (SuspensionAlertDTO) obj;
		RevokeAlert susAlert = null;
		if(param.getSuspendAlertId() <= 0)
		{
			susAlert = new RevokeAlert();
			
			RevokeLicense revokeLicense = new RevokeLicense();
			revokeLicense.setRevokeId(param.getRevokeId());
			
			susAlert.setRevokeLicense(revokeLicense);
			
			ObjectUtil.copy(param, susAlert);
			susAlert.setAlertType(AlertType.POST.getStatus());
			susAlert.setRecordStatus(RecordStatus.NORMAL.getStatus());
			susAlert.setCreateDtm(new Date());
			susAlert.setCreateUser(user.getUserName());
			
			Long revokeAlertId = (Long)this.revokeAlertDAO.insert(susAlert);
			susAlert.setRevokeAlertId(revokeAlertId);
			
			param.setRevokeAlertId(revokeAlertId);

		}
		else
		{
			susAlert = (RevokeAlert) this.revokeAlertDAO.findByPrimaryKey(param.getRevokeAlertId());
			
			RevokeLicense revokeLicense = new RevokeLicense();
			revokeLicense.setRevokeId(param.getRevokeId());
			
			susAlert.setRevokeLicense(revokeLicense);
			
			ObjectUtil.copy(param, susAlert);
		
			susAlert.setAlertType(AlertType.POST.getStatus());
			susAlert.setRecordStatus(RecordStatus.NORMAL.getStatus());
			susAlert.setLastUpdDtm(new Date());
			susAlert.setLastUpdUser(user.getUserName());
			
			this.revokeAlertDAO.update(susAlert);
	
		}
				
		if(AlertStatus.ACCEPT.getStatus().equals(susAlert.getAlertStatus()) )
		{
			RevokeLicenseDTO revoke = new RevokeLicenseDTO();
			revoke.setRevokeId(param.getRevokeId());
			revoke.setRevokeDate(param.getResponseDate());
			
			this.revoke(revoke, user);
		}
		
		
		return param;
	}

	public Object getRevokeDocx(Object obj, User user) throws Exception {
		if(!(obj instanceof RevokeLicenseDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		int allOrg = 0;
		
		DateFormat df = DateUtils.getProcessDateFormatThai();
		
		RevokeLicenseDTO param = (RevokeLicenseDTO)obj;
		
		List<RevokeMapTrader> listRevokeMapTrader =  this.revokeMapTraderDAO.findByRevoleLicense(param.getRevokeId());
				
		LicenseDetailDTO dto = new LicenseDetailDTO();
		
		StringBuffer masActName = new StringBuffer();
		List<RevokeDetail> listRevoke = (List<RevokeDetail>) this.revokeDetailDAO.findByRevokeLicense(param.getRevokeId());
		if(!listRevoke.isEmpty())
		{
			for(RevokeDetail susDetail: listRevoke)
			{
				
				masActName.append(susDetail.getMasAct()==null?"":susDetail.getMasAct().getActDesc());
				masActName.append("");
			}
			dto.setActName(masActName.toString());
			
		}
		else
		{
			dto.setActName("");
		}
		
		if(!listRevokeMapTrader.isEmpty())
		{
			RevokeMapTrader mapTrader = listRevokeMapTrader.get(0);
			
			List<Trader> listTrader = traderDAO.findTraderByLicenseNo(mapTrader.getLicenseNo(), mapTrader.getTraderType(), RecordStatus.NORMAL.getStatus(), allOrg);
			
			
			if(!listTrader.isEmpty())
			{
				Trader trader = listTrader.get(0);
				
				Person person = trader.getPerson();
				
				String prefixName = "";
				if(null != person.getMasPrefix())
				{
					prefixName = person.getMasPrefix().getPrefixName();
				}
				
				String postfixName = "";
				if(null != person.getMasPosfix())
				{
					postfixName = person.getMasPosfix().getPostfixName();
				}
				
				if(PersonType.CORPORATE.getStatus().equals(person.getPersonType()))
				{
					
					String traderOwnerName = prefixName + person.getFirstName() + postfixName;
					dto.setTraderName(traderOwnerName);
				}
				else
				{
					String traderOwnerName = prefixName + person.getFirstName() + " " + person.getLastName();
					dto.setTraderName(traderOwnerName);
				}
				
				dto.setLicenseNo(trader.getLicenseNo());
				
				//Oat Add 20/01/59
				if(null != trader.getEffectiveDate())
				{
					String effectiveDate =DateUtils.getDateWithMonthName(df.format(trader.getEffectiveDate()));
					dto.setEffectiveDate(effectiveDate);
				}
				//
				
				if(null != trader.getExpireDate())
				{
					String expireDate =DateUtils.getDateWithMonthName(df.format(trader.getExpireDate()));
					dto.setExpireDate(expireDate);
				}
				
				List<Registration> listReg = (List<Registration>)this.registrationDAO.findAllByTrader(trader.getTraderId());	
				
				if(!listReg.isEmpty())
				{
					Registration reg = (Registration)listReg.get(0);
					
					if(null != reg.getApproveDate())
					{
						String approveDate =DateUtils.getDateWithMonthName(df.format(reg.getApproveDate()));
						dto.setApproveDate(approveDate);
					}
					
				}
				else
				{
					dto.setApproveDate("");
				}
				
				
				List<TraderAddress> listAddr = (List<TraderAddress>) this.traderAddressDAO.findAllByTrader(trader.getTraderId(), TraderAddressType.OFFICE_ADDRESS.getStatus(), null);
				
				if(!listAddr.isEmpty())
				{
					TraderAddress add = listAddr.get(0);
					StringBuilder traderAddress = new StringBuilder();
				
					
					traderAddress.append(add.getAddressNo());
				
					traderAddress.append(" ถนน ");
					traderAddress.append(add.getRoadName()==null?"-":add.getRoadName());
					
					traderAddress.append(" ตรอก/ซอย ");
					traderAddress.append(add.getSoi()==null?"-":add.getSoi());
					
				
					if(add.getMasTambol() != null)
					{
						traderAddress.append(" ตำบล/แขวง ");
						traderAddress.append(add.getMasTambol().getTambolName());
						
					}
					if(add.getMasAmphur()!= null)
					{
						traderAddress.append(" อำเภอ/เขต ");
						traderAddress.append(add.getMasAmphur().getAmphurName());
						
					}
					if(add.getMasProvince() != null)
					{
						traderAddress.append(" จังหวัด ");
						traderAddress.append(add.getMasProvince().getProvinceName());
						
					}
					
					dto.setTraderAddress(traderAddress.toString());
					
				}
				else
				{
					dto.setTraderAddress("");
				}
			}
		}
		
		dto.setDocDate(DateUtils.getDateWithMonthName(DateUtils.getProcessDateFormatThai().format(new Date())));
		
		return dto;
	}
	
	

}
