package sss.dot.tourism.service.punishment;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.punishment.SuspensionAlertDAO;
import sss.dot.tourism.domain.SuspensionAlert;
import sss.dot.tourism.domain.SuspensionLicense;
import sss.dot.tourism.dto.punishment.SuspensionAlertDTO;
import sss.dot.tourism.dto.punishment.SuspensionLicenseDTO;
import sss.dot.tourism.util.AlertStatus;
import sss.dot.tourism.util.AlertType;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.RecordStatus;

import com.sss.aut.service.User;

@Repository("suspensionAlertService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class SuspensionAlertService implements ISuspensionAlertService {

	@Autowired
	SuspensionAlertDAO suspensionAlertDAO;
	
	@Autowired
	ISuspensionService suspensionService;
	
	

	public Object getAlertAll(Object object, User user) throws Exception {
		if(!(object instanceof SuspensionAlertDTO))
		{
			throw new IllegalArgumentException("object not instanceof SuspensionAlertDTO");
		}
		
		SuspensionAlertDTO dto = new SuspensionAlertDTO();
		
		SuspensionAlertDTO param = (SuspensionAlertDTO)object;
		
		List<SuspensionAlert> listSuspensionAlert = (List<SuspensionAlert>) this.suspensionAlertDAO.findBySuspendLicense(param.getSuspendId());

		
		if(!listSuspensionAlert.isEmpty())
		{
			SuspensionAlert emailAlert = listSuspensionAlert.get(0);
			
			dto.setSuspendId(emailAlert.getSuspensionLicense().getSuspendId());
			
			
			DateFormat df = DateUtils.getProcessDateFormatThai();
			
			if(emailAlert.getAlertDate() != null)
			{
				dto.setEmailAlertDate(df.format(emailAlert.getAlertDate()));
			}
			if(emailAlert.getResponseDate() != null)
			{
				dto.setEmailResponseDate(df.format(emailAlert.getResponseDate()));
			}
			if((emailAlert.getAlertStatus() != null) && (!emailAlert.getAlertStatus().isEmpty()))
			{
				dto.setEmailAlertStatus(AlertStatus.getMeaning(emailAlert.getAlertStatus()));
			}

			
			if(listSuspensionAlert.size() > 1)
			{
				SuspensionAlert postAlert = listSuspensionAlert.get(1);
				
				
				ObjectUtil.copy(postAlert, dto);
				
				if((dto.getAlertType() != null) && (!dto.getAlertType().isEmpty()))
				{
					dto.setAlertTypeName(AlertType.getMeaning(dto.getAlertType()));
				}
				
				if((dto.getAlertStatus() != null) && (!dto.getAlertStatus().isEmpty()))
				{
					dto.setAlertStatusName(AlertStatus.getMeaning(dto.getAlertStatus()));
				}
			}	
		}
			
			
		return dto;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public Object save(Object obj, User user) throws Exception {
		if(!(obj instanceof SuspensionAlertDTO))
		{
			throw new IllegalArgumentException("object not instanceof SuspensionAlertDTO");
		}
			
		SuspensionAlertDTO param = (SuspensionAlertDTO) obj;
		SuspensionAlert susAlert = null;
		if(param.getSuspendAlertId() <= 0)
		{
			susAlert = new SuspensionAlert();
			
			SuspensionLicense suspensionLicense = new SuspensionLicense();
			suspensionLicense.setSuspendId(param.getSuspendId());
			
			susAlert.setSuspensionLicense(suspensionLicense);
			
			ObjectUtil.copy(param, susAlert);
			susAlert.setAlertType(AlertType.POST.getStatus());
			susAlert.setRecordStatus(RecordStatus.NORMAL.getStatus());
			susAlert.setCreateDtm(new Date());
			susAlert.setCreateUser(user.getUserName());
			
			Long suspendAlertId = (Long)this.suspensionAlertDAO.insert(susAlert);
			susAlert.setSuspendAlertId(suspendAlertId);
			
			param.setSuspendAlertId(suspendAlertId);

		}
		else
		{
			susAlert = (SuspensionAlert) this.suspensionAlertDAO.findByPrimaryKey(param.getSuspendAlertId());
			
			SuspensionLicense suspensionLicense = new SuspensionLicense();
			suspensionLicense.setSuspendId(param.getSuspendId());
			
			susAlert.setSuspensionLicense(suspensionLicense);
			
			ObjectUtil.copy(param, susAlert);
		
			susAlert.setAlertType(AlertType.POST.getStatus());
			susAlert.setRecordStatus(RecordStatus.NORMAL.getStatus());
			susAlert.setLastUpdDtm(new Date());
			susAlert.setLastUpdUser(user.getUserName());
			
			this.suspensionAlertDAO.update(susAlert);
	
		}
				
		if(AlertStatus.ACCEPT.getStatus().equals(susAlert.getAlertStatus()) )
		{
			SuspensionLicenseDTO suspend = new SuspensionLicenseDTO();
			suspend.setSuspendId(param.getSuspendId());
			suspend.setSuspendFrom(param.getResponseDate());
			
			this.suspensionService.suspend(suspend, user);
		}
		

		
		
		return param;
	}
}
