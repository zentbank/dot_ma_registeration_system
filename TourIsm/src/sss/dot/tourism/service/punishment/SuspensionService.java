package sss.dot.tourism.service.punishment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sss.aut.service.User;

import sss.dot.tourism.dao.AdmUserDAO;
import sss.dot.tourism.dao.mas.MasActDAO;
import sss.dot.tourism.dao.mas.MasPrefixDAO;
import sss.dot.tourism.dao.mas.MasRunningNoDAO;
import sss.dot.tourism.dao.punishment.SuspensionAlertDAO;
import sss.dot.tourism.dao.punishment.SuspensionDetailDAO;
import sss.dot.tourism.dao.punishment.SuspensionLicenseDAO;
import sss.dot.tourism.dao.punishment.SuspensionMapTraderDAO;
import sss.dot.tourism.dao.registration.RegistrationDAO;
import sss.dot.tourism.dao.registration.TraderAddressDAO;
import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.domain.AdmUser;
import sss.dot.tourism.domain.MasAct;
import sss.dot.tourism.domain.Person;
import sss.dot.tourism.domain.Registration;
import sss.dot.tourism.domain.SuspensionAlert;
import sss.dot.tourism.domain.SuspensionDetail;
import sss.dot.tourism.domain.SuspensionLicense;
import sss.dot.tourism.domain.SuspensionMapTrader;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.domain.TraderAddress;
import sss.dot.tourism.dto.punishment.MasActDTO;
import sss.dot.tourism.dto.punishment.SuspensionLicenseDTO;
import sss.dot.tourism.dto.registration.LicenseDetailDTO;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.service.sendmail.ISendMailService;
import sss.dot.tourism.service.trader.ITraderService;
import sss.dot.tourism.util.ActType;
import sss.dot.tourism.util.AlertStatus;
import sss.dot.tourism.util.AlertType;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.LicenseStatus;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.PersonType;
import sss.dot.tourism.util.RecordStatus;
import sss.dot.tourism.util.SuspendStatus;
import sss.dot.tourism.util.ThaiBahtUtil;
import sss.dot.tourism.util.TraderAddressType;
import sss.dot.tourism.util.TraderCategory;
import sss.dot.tourism.util.TraderType;

@Repository("suspensionService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class SuspensionService implements ISuspensionService {
	@Autowired
	TraderDAO traderDAO;
	@Autowired
	TraderAddressDAO traderAddressDAO;
	@Autowired
	SuspensionLicenseDAO suspensionLicenseDAO;
	@Autowired
	SuspensionMapTraderDAO suspensionMapTraderDAO;
	@Autowired
	SuspensionDetailDAO suspensionDetailDAO;
	@Autowired
	MasActDAO masActDAO;
	@Autowired
	SuspensionAlertDAO suspensionAlertDAO;
	
	@Autowired
	MasRunningNoDAO masRunningNoDAO;
	@Autowired
	AdmUserDAO admUserDAO;
	@Autowired
	MasPrefixDAO masPrefixDAO;
	@Autowired
	ITraderService traderService;
	@Autowired 
	ISendMailService sendMailService;
	
	@Autowired
	RegistrationDAO registrationDAO;
	
	
	
	@Override
	public MasAct getActBySupendId(Object obj, User user) throws Exception {
		if(!(obj instanceof SuspensionLicenseDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		LicenseDetailDTO dto = new LicenseDetailDTO();
		SuspensionLicenseDTO param = (SuspensionLicenseDTO)obj;
		
		List<SuspensionDetail> listSusDetail = (List<SuspensionDetail>) this.suspensionDetailDAO.findBySuspendLicense(param.getSuspendId());
		return CollectionUtils.isNotEmpty(listSusDetail)?listSusDetail.get(0).getMasAct():null;
	}

	public List getActAll(Object obj, User user) throws Exception{
		
		if(!(obj instanceof MasActDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		MasActDTO param =  (MasActDTO)obj;
		
		List<MasActDTO> list = new ArrayList<MasActDTO>();
		
		if(param.getSuspendId() > 0)
		{
			list = this.masActDAO.findbySuspendIdAndTraderTypr(param.getSuspendId(), param.getTraderType());
		}
		else
		{
			List<MasAct> listAll = (List<MasAct>) this.masActDAO.findByTraderType(param.getTraderType() , ActType.SUSPEND.getStatus());
			
			
			if(!listAll.isEmpty())
			{
				for(MasAct act: listAll)
				{
					MasActDTO dto = new MasActDTO();
					ObjectUtil.copy(act, dto);
					list.add(dto);
				}
			}
		}
		
		return list;
	}

	public Object getSuspensionById(Object obj, User user) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public Object getTraderPrepareSuspension(Object obj, User user)
			throws Exception {
		if(!(obj instanceof SuspensionLicenseDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		SuspensionLicenseDTO dto = (SuspensionLicenseDTO)obj; 
		
		SuspensionLicenseDTO param = (SuspensionLicenseDTO)obj; 
		
		Trader trader = (Trader)this.traderDAO.findByPrimaryKey(param.getTraderId());
		
		Person person = trader.getPerson();
		
		String runNo = masRunningNoDAO.getSuspensionTempNo(user.getUserData().getOrgId(), trader.getTraderType());
		ObjectUtil.copy(trader, dto);
		ObjectUtil.copy(person, dto);
		
		if(PersonType.CORPORATE.getStatus().equals(person.getPersonType()))
		{
			//OAT EDIT 11/11/58
//			String traderOwnerName = person.getMasPrefix().getPrefixName() + person.getFirstName() + person.getMasPosfix()==null?"":person.getMasPosfix().getPostfixName();
			String traderOwnerName = person.getMasPrefix().getPrefixName() + person.getFirstName();
			if(null != person.getMasPosfix())
			{
				traderOwnerName += person.getMasPosfix().getPostfixName();
			}
			dto.setTraderOwnerName(traderOwnerName);
		}
		else
		{
			String traderOwnerName = person.getMasPrefix().getPrefixName() + person.getFirstName() + " " + person.getLastName();
			dto.setTraderOwnerName(traderOwnerName);
		}
		
		dto.setTraderCategoryName(TraderCategory.getMeaning(trader.getTraderType(), trader.getTraderCategory()));
		
		Calendar cal = Calendar.getInstance();
		
		DateFormat df = DateUtils.getProcessDateFormatThai();
		
		dto.setSuspensionNo(runNo);
		dto.setSuspensionDate(df.format(cal.getTime()));
		dto.setOfficerName(user.getUserData().getUserFullName());
		
		return dto;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveSuspension(Object susParam,Object[] object, User user)
			throws Exception {
		if(!(susParam instanceof SuspensionLicenseDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		SuspensionLicenseDTO param = (SuspensionLicenseDTO)susParam;
		SuspensionLicense suspend = null;
		
		//เพิ่มข้อมูลการพักใช้ 
		if(param.getSuspendId() <= 0)
		{
			Trader trader = (Trader)this.traderDAO.findByPrimaryKey(param.getTraderId());
			
//			String runNo = masRunningNoDAO.getSuspensionNo(user.getUserData().getOrgId(), trader.getTraderType());
			String runNo = masRunningNoDAO.getSuspensionNo(trader.getOrganization().getOrgId(), trader.getTraderType());
			
			suspend = new SuspensionLicense();
			
			ObjectUtil.copy(param, suspend);
			
			suspend.setSuspensionNo(runNo);
			
			AdmUser admUser = new AdmUser();
			admUser.setUserId(user.getUserId());
			
			suspend.setOrganization(trader.getOrganization());
			suspend.setSuspendStatus(SuspendStatus.WAIT.getStatus());
			suspend.setAdmUser(admUser);
			suspend.setRecordStatus(RecordStatus.NORMAL.getStatus());
			suspend.setCreateUser(user.getUserName());
			suspend.setCreateDtm(new Date());
			
			Long suspendId = (Long)this.suspensionLicenseDAO.insert(suspend);
			suspend.setSuspendId(suspendId);
			param.setSuspendId(suspendId);
			
			SuspensionMapTrader mapTrader = new SuspensionMapTrader();
			mapTrader.setSuspensionLicense(suspend);
			mapTrader.setLicenseNo(trader.getLicenseNo());
			mapTrader.setTraderType(trader.getTraderType());
			
			this.suspensionMapTraderDAO.insert(mapTrader);
			
		}
		else//เลือกสถานะการอนุมัติเป็น รับเรื่องรออนุมัติ หรือ อนุมัติการพักใช้
		{
			suspend = (SuspensionLicense) this.suspensionLicenseDAO.findByPrimaryKey(param.getSuspendId());
			
			ObjectUtil.copy(param, suspend);
			
			AdmUser admUser = new AdmUser();
			admUser.setUserId(user.getUserId());
			
			suspend.setSuspendStatus(param.getSuspendStatus());
			suspend.setAdmUser(admUser);
			suspend.setRecordStatus(RecordStatus.NORMAL.getStatus());
			suspend.setLastUpdUser(user.getUserName());
			suspend.setLastUpdDtm(new Date());
			
			this.suspensionLicenseDAO.update(suspend);
			
			if(SuspendStatus.APPROVE.getStatus().equals(param.getSuspendStatus()))
			{
				SuspensionAlert susAlert = new SuspensionAlert();
				
				susAlert.setSuspensionLicense(suspend);
				susAlert.setAlertType(AlertType.EMAIl.getStatus());
				susAlert.setAlertStatus(AlertStatus.WAIT.getStatus());
				susAlert.setAlertDate(new Date());
				susAlert.setRecordStatus(RecordStatus.NORMAL.getStatus());
				susAlert.setCreateDtm(new Date());
				susAlert.setCreateUser(user.getUserName());
				
				this.suspensionAlertDAO.insert(susAlert);
			}
		}
		
		this.addAct(object, suspend, user);
		
		
		// send mail

	}
	
	//เพิ่มสาเหตุการพักใช้ SuspensionDetail
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	private void addAct(Object[] object, SuspensionLicense suspend, User user) throws Exception
	{
		//มีสาเหตุการพักใช้
		if(object.length > 0)
		{
			//ลบสาเหตุเก่าถ้ามีอยู่แล้ว
			List<SuspensionDetail> listOldAct = this.suspensionDetailDAO.findBySuspendLicense(suspend.getSuspendId());
			if(!listOldAct.isEmpty())
			{
				for(SuspensionDetail masAct: listOldAct)
				{
					this.suspensionDetailDAO.delete(masAct);
				}
				

			}
			
			//บันทึกสาเหตุการพักใช้ใหม่ SuspensionDetail 
			for(Object obj: object)
			{
				MasActDTO actDTO = (MasActDTO)obj;
				SuspensionDetail suspendDetail = new SuspensionDetail();
				MasAct masAct = new MasAct();
				
				masAct.setMasActId(actDTO.getMasActId());
				
				suspendDetail.setMasAct(masAct);
				suspendDetail.setSuspensionLicense(suspend);
				suspendDetail.setRecordStatus(RecordStatus.NORMAL.getStatus());
				suspendDetail.setCreateUser(user.getUserName());
				suspendDetail.setCreateDtm(new Date());
				
				suspensionDetailDAO.insert(suspendDetail);
			}
		}
	}
	public List countAll(Object obj, User user) throws Exception
	{
		if(!(obj instanceof SuspensionLicenseDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		SuspensionLicenseDTO param = (SuspensionLicenseDTO)obj;
		
		return this.suspensionLicenseDAO.findSuspensionLicensePaging(param, param.getStart(), param.getLimit());
		
		
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List getAll(Object obj, User user) throws Exception {
		
		if(!(obj instanceof SuspensionLicenseDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		List<SuspensionLicenseDTO> list = new ArrayList<SuspensionLicenseDTO>();
		
		SuspensionLicenseDTO param = (SuspensionLicenseDTO)obj;
		
		List<Object[]> listAll = this.suspensionLicenseDAO.findSuspensionLicensePaging(param, param.getStart(), param.getLimit());
		
		DateFormat df = DateUtils.getProcessDateFormatThai();
		
		if(!listAll.isEmpty())
		{
			for(Object[] persistence :listAll)
			{
				SuspensionLicenseDTO dto = new SuspensionLicenseDTO();
				
				//   sqlQuery.addScalar("TRADER_ID", Hibernate.LONG); //0
				dto.setTraderId(Long.valueOf(persistence[0].toString()));
				
				//sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING); //1
				dto.setLicenseNo(persistence[1]==null?"":persistence[1].toString());
				
				//sqlQuery.addScalar("TRADER_TYPE", Hibernate.STRING);//3
				dto.setTraderType(persistence[3]==null?"":persistence[3].toString());
				dto.setTraderTypeName(TraderType.getMeaning(dto.getTraderType()));
				

	
				
//				sqlQuery.addScalar("IDENTITY_NO", Hibernate.STRING);//17
				dto.setIdentityNo(persistence[17]==null?"":persistence[17].toString());
//			    sqlQuery.addScalar("PERSON_ID", Hibernate.LONG);//4
				
				//sqlQuery.addScalar("TRADER_CATEGORY", Hibernate.STRING);//18
				dto.setTraderCategory(persistence[18]==null?"":persistence[18].toString());
				dto.setTraderCategoryName(TraderCategory.getMeaning(dto.getTraderType(), dto.getTraderCategory()));
			
//			   
//			    sqlQuery.addScalar("POSTFIX_NAME", Hibernate.STRING);//8
//			    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);//9
				
				
				
			    //sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING); //2
				if(TraderType.TOUR_COMPANIES.equals(dto.getTraderType()))
				{
					dto.setTraderName(persistence[2]==null?"":persistence[2].toString());
					
//				    sqlQuery.addScalar("TRADER_NAME_EN", Hibernate.STRING);//19
					dto.setTraderNameEn(persistence[19]==null?"":persistence[19].toString());
					
					//sqlQuery.addScalar("PREFIX_NAME", Hibernate.STRING);//5
					String prefixName = persistence[5]==null?"":persistence[5].toString();
					//sqlQuery.addScalar("FIRST_NAME", Hibernate.STRING);//6
					String firstName = persistence[6]==null?"":persistence[6].toString();
	
					
					String postfixName = persistence[8]==null?"":persistence[8].toString();
					
					dto.setTraderOwnerName(prefixName+firstName +postfixName);
				}
				else
				{
					//sqlQuery.addScalar("PREFIX_NAME", Hibernate.STRING);//5
					String prefixName = persistence[5]==null?"":persistence[5].toString();
					//sqlQuery.addScalar("FIRST_NAME", Hibernate.STRING);//6
					String firstName = persistence[6]==null?"":persistence[6].toString();
				    // sqlQuery.addScalar("LAST_NAME", Hibernate.STRING);//7
					String lastName = persistence[7]==null?"":persistence[7].toString();
					
					dto.setTraderName(prefixName+firstName +" " + lastName);
					dto.setTraderOwnerName(dto.getTraderName());
				    
				}
				
			    

			    
//			    sqlQuery.addScalar("SUSPEND_ID", Hibernate.LONG);//10
				dto.setSuspendId(Long.valueOf(persistence[10].toString()));
				//sqlQuery.addScalar("SUSPENSION_DATE", Hibernate.DATE);//11
				if(persistence[11]!=null)
				{
					dto.setSuspensionDate(df.format(persistence[11]));
				}
				
				
			    
//			    sqlQuery.addScalar("SUSPENSION_NO", Hibernate.STRING);//12
				dto.setSuspensionNo(persistence[12]==null?"":persistence[12].toString());
				
				//sqlQuery.addScalar("SUSPEND_PERIOD", Hibernate.INTEGER);//20
				dto.setSuspendPeriod(Integer.valueOf(persistence[20].toString()));
				
				//sqlQuery.addScalar("SUSPEND_PERIOD_DAY", Hibernate.INTEGER);//22
				dto.setSuspendPeriodDay(persistence[22]==null?0:Integer.valueOf(persistence[22].toString()));
				
//				 sqlQuery.addScalar("SUSPEND_DETAIL", Hibernate.INTEGER);//21
				dto.setSuspendDetail(persistence[21]==null?"":persistence[21].toString());
				
//			    sqlQuery.addScalar("SUSPEND_FROM", Hibernate.DATE);//13
				if(persistence[13]!=null)
				{
					dto.setSuspendFrom(df.format(persistence[13]));
				}
//			    sqlQuery.addScalar("SUSPEND_TO", Hibernate.DATE);//14
				if(persistence[14]!=null)
				{
					dto.setSuspendTo(df.format(persistence[14]));
				}
//			    sqlQuery.addScalar("OFFICER_NAME", Hibernate.LONG);//15
				if(persistence[15] != null)
				{
					AdmUser admUser = (AdmUser) this.admUserDAO.findByPrimaryKey(Long.valueOf(persistence[15].toString()));
					String prefix = admUser.getMasPrefix().getPrefixName();
					dto.setOfficerName(prefix + admUser.getUserName() +" "+ admUser.getUserLastname());

					
					
					if(admUser.getUserId() == user.getUserId())
					{
						dto.setOfficerAsOwner("1");
					}
					else
					{
						dto.setOfficerAsOwner("0");
					}
					
					if(admUser.getUserId() == 1)
					{
						dto.setOfficerAsOwner("1");
					}
				}
				
				
//			    sqlQuery.addScalar("SUSPEND_STATUS", Hibernate.STRING);//16
				dto.setSuspendStatus(persistence[16]==null?"":persistence[16].toString());
				dto.setSuspendStatusName(SuspendStatus.getMeaning(dto.getSuspendStatus()));
				
				// update status ถ้าครบกำหนดพักใช้
				if(SuspendStatus.SUSPENSION.equals(dto.getSuspendStatus()))
				{
					if(persistence[14]!=null)
					{
						Date  suspendTo = (Date)persistence[14];
						
						if(suspendTo.before(new Date()))
						{
							System.out.println("suspendTo = "+suspendTo);
							
							SuspensionLicense suspensionLicense =  (SuspensionLicense)this.suspensionLicenseDAO.findByPrimaryKey(dto.getSuspendId());
							SuspensionLicenseDTO suspDTO = new SuspensionLicenseDTO();
							ObjectUtil.copy(suspensionLicense, suspDTO);
							this.unSuspend(suspDTO, user);
//							dto.setSuspendStatus(SuspendStatus.NORMAL.getStatus());
//							dto.setSuspendStatusName(SuspendStatus.getMeaning(dto.getSuspendStatus()));
//							
//							SuspensionLicense suspensionLicense =  (SuspensionLicense)this.suspensionLicenseDAO.findByPrimaryKey(dto.getSuspendId());
//							suspensionLicense.setSuspendStatus(SuspendStatus.NORMAL.getStatus());
//							suspensionLicense.setLastUpdUser("ครบกำหนดพักใช้");
//							suspensionLicense.setLastUpdDtm(new Date());
//							this.suspensionLicenseDAO.update(suspensionLicense);
//							
//							RegistrationDTO regDTO = new RegistrationDTO();
//							
//							List<SuspensionMapTrader> listSuspensionMapTrader =  this.suspensionMapTraderDAO.findBySuspendLicense(suspensionLicense.getSuspendId());
//							
//							
//							if(!listSuspensionMapTrader.isEmpty())
//							{
//								SuspensionMapTrader map = listSuspensionMapTrader.get(0);
//								regDTO.setLicenseNo(map.getLicenseNo());
//								regDTO.setTraderType(map.getTraderType());
//							}
//							
//							// suspend all trader
//							this.traderService.suspend(regDTO, user);
						}
					}
				}
				
				list.add(dto);

			}
		}
		
		return list;
	}
	/**
	 * พักใช้ใบอนุญาต
	 */

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void suspend(Object obj, User user) throws Exception {

		if(!(obj instanceof SuspensionLicenseDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		SuspensionLicenseDTO param = (SuspensionLicenseDTO)obj;
		SuspensionLicense suspendLicense = (SuspensionLicense)this.suspensionLicenseDAO.findByPrimaryKey(param.getSuspendId());
		
		DateFormat df = DateUtils.getProcessDateFormatThai();
		

		
		Date suspendFrom = df.parse(param.getSuspendFrom());
		suspendLicense.setSuspendFrom(suspendFrom);

		
		Calendar cal = Calendar.getInstance();
		cal.setTime(suspendFrom);
		
		if(suspendLicense.getSuspendPeriod() > 0)
		{
			cal.add(Calendar.MONTH, suspendLicense.getSuspendPeriod());
		}
		
		if(suspendLicense.getSuspendPeriodDay() > 0)
		{
			cal.add(Calendar.DATE, suspendLicense.getSuspendPeriodDay());
		}
		
		cal.add(Calendar.DATE, -1);
		suspendLicense.setSuspendTo(cal.getTime());
		
		suspendLicense.setSuspendStatus(SuspendStatus.SUSPENSION.getStatus());
		suspendLicense.setLastUpdUser(user.getUserName());
		suspendLicense.setLastUpdDtm(new Date());
		
		this.suspensionLicenseDAO.update(suspendLicense);
		
		List<SuspensionMapTrader> listSuspensionMapTrader =  this.suspensionMapTraderDAO.findBySuspendLicense(suspendLicense.getSuspendId());
		
		RegistrationDTO regDTO = new RegistrationDTO();
		
		if(!listSuspensionMapTrader.isEmpty())
		{
			SuspensionMapTrader map = listSuspensionMapTrader.get(0);
			regDTO.setLicenseNo(map.getLicenseNo());
			regDTO.setTraderType(map.getTraderType());
			
			regDTO.setPunishmentDate(suspendLicense.getSuspendFrom());
			regDTO.setPunishmentExpireDate(suspendLicense.getSuspendTo());
		}
		
		// suspend all trader
		this.traderService.suspend(regDTO, user);
		
		
	}

	/*
	 * ยกเลิกการพักใช้*/
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void cancelSuspend(Object obj, User user) throws Exception {
		if(!(obj instanceof SuspensionLicenseDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		SuspensionLicenseDTO param = (SuspensionLicenseDTO)obj;
		SuspensionLicense suspendLicense = (SuspensionLicense)this.suspensionLicenseDAO.findByPrimaryKey(param.getSuspendId());
		
	
		suspendLicense.setCancelDate(new Date());
		suspendLicense.setCancelRemark(param.getCancelRemark()==null?"":param.getCancelRemark());
		suspendLicense.setSuspendStatus(SuspendStatus.CANCEL.getStatus());
		suspendLicense.setLastUpdUser(user.getUserName());
		suspendLicense.setLastUpdDtm(new Date());
		
		this.suspensionLicenseDAO.update(suspendLicense);
		
		List<SuspensionMapTrader> listSuspensionMapTrader =  this.suspensionMapTraderDAO.findBySuspendLicense(suspendLicense.getSuspendId());
		
		RegistrationDTO regDTO = new RegistrationDTO();
		
		if(!listSuspensionMapTrader.isEmpty())
		{
			SuspensionMapTrader map = listSuspensionMapTrader.get(0);
			regDTO.setLicenseNo(map.getLicenseNo());
			regDTO.setTraderType(map.getTraderType());
		}
		
		// suspend all trader
		this.traderService.unSuspend(regDTO, user);
		
	}
	
	
	/*
	 * ครบกำหนดพักใช้้*/
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void unSuspend(Object obj, User user) throws Exception {
		if(!(obj instanceof SuspensionLicenseDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		SuspensionLicenseDTO param = (SuspensionLicenseDTO)obj;
		SuspensionLicense suspendLicense = (SuspensionLicense)this.suspensionLicenseDAO.findByPrimaryKey(param.getSuspendId());
		
	
		suspendLicense.setCancelDate(new Date());
		suspendLicense.setCancelRemark("ครบกำหนดพักใช้");
		suspendLicense.setSuspendStatus(SuspendStatus.NORMAL.getStatus());
		suspendLicense.setLastUpdUser("ยกเลิกโดยระบบ");
		suspendLicense.setLastUpdDtm(new Date());
		
		System.out.println("###suspendLicense.getSuspendId() = "+suspendLicense.getSuspendId());
		
		this.suspensionLicenseDAO.update(suspendLicense);
		
		List<SuspensionMapTrader> listSuspensionMapTrader =  this.suspensionMapTraderDAO.findBySuspendLicense(suspendLicense.getSuspendId());
		
		RegistrationDTO regDTO = new RegistrationDTO();
		
		if(!listSuspensionMapTrader.isEmpty())
		{
			SuspensionMapTrader map = listSuspensionMapTrader.get(0);
			regDTO.setLicenseNo(map.getLicenseNo());
			regDTO.setTraderType(map.getTraderType());
		}
		
		//Oat Edit 25/01/59
		List<SuspensionDetail> listSuspensionDetail = (List<SuspensionDetail>)this.suspensionDetailDAO.findBySuspendLicense(suspendLicense.getSuspendId());

		List<Trader> listTrader = (List<Trader>)this.traderDAO.findTraderByLicenseNo(regDTO.getLicenseNo(), TraderType.TOUR_COMPANIES.getStatus(), RecordStatus.NORMAL.getStatus(), 0);
		
		if(!listSuspensionDetail.isEmpty())
		{
			SuspensionDetail detail = listSuspensionDetail.get(0);
			//เฉพาะ suspension กรณีอื่นที่ไม่ใช่ ไม่ชำระค่าธรรมเนียม(masAct != 3) ถ้ากรณีไม่ชำระค่าธรรมเนียมไม่ต้อง update trader
			if(detail.getMasAct() != null && detail.getMasAct().getMasActId() != 3)
			{
				if(!listTrader.isEmpty())
				{
					Trader trader = listTrader.get(0);
					//หาเฉพาะ trader ที่พักใช้เท่านั้น(ถ้ายกเลิกไปแล้วจะไม่ต้อง update trader)
					if(trader.getLicenseStatus().equals(LicenseStatus.SUSPENSION.getStatus()))
					{
						// suspend all trader
						System.out.println("#####suspend all trader");
						this.traderService.unSuspend(regDTO, user);
					}

				}
			}
			else
			{
				System.out.println("#####don't suspend all trader");
			}
		}
		
//		// suspend all trader
//		this.traderService.unSuspend(regDTO, user);
		//End Oat Edit 25/01/59
		
	}

	public void deactivateSuspend(Object obj, User user) throws Exception {
		if(!(obj instanceof Trader))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		Trader trader = (Trader)obj;
		List<SuspensionLicense> listSuspend = (List<SuspensionLicense>) suspensionLicenseDAO.findByLicenseNo(trader.getLicenseNo(), trader.getTraderType(), SuspendStatus.SUSPENSION.getStatus());
		
		if(!listSuspend.isEmpty())
		{
			for(SuspensionLicense suspendLicense: listSuspend)
			{
				suspendLicense.setCancelDate(new Date());
				suspendLicense.setCancelRemark("ยกเลิกการพักใช้เนื่องจากมาชำระค่าธรรมเนียม");
				suspendLicense.setSuspendStatus(SuspendStatus.CANCEL.getStatus());
				suspendLicense.setLastUpdUser(user.getUserName());
				suspendLicense.setLastUpdDtm(new Date());
				
				this.suspensionLicenseDAO.update(suspendLicense);
			
				RegistrationDTO regDTO = new RegistrationDTO();
				regDTO.setLicenseNo(trader.getLicenseNo());
				regDTO.setTraderType(trader.getTraderType());
				
				// suspend all trader
				this.traderService.unSuspend(regDTO, user);
			}
		}
		
	}

	public Object getSuspensionDocx(Object obj, User user) throws Exception {
		
		if(!(obj instanceof SuspensionLicenseDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		int allOrg = 0;
		
		LicenseDetailDTO dto = new LicenseDetailDTO();
		
		DateFormat df = DateUtils.getProcessDateFormatThai();
		
		SuspensionLicenseDTO param = (SuspensionLicenseDTO)obj;
		
		List<SuspensionMapTrader> listSuspensionMapTrader =  this.suspensionMapTraderDAO.findBySuspendLicense(param.getSuspendId());
		
		StringBuffer masActName = new StringBuffer();
		List<SuspensionDetail> listSusDetail = (List<SuspensionDetail>) this.suspensionDetailDAO.findBySuspendLicense(param.getSuspendId());
		if(!listSusDetail.isEmpty())
		{
			for(SuspensionDetail susDetail: listSusDetail)
			{
				
				masActName.append(susDetail.getMasAct()==null?"":susDetail.getMasAct().getActDesc());
				masActName.append("");
			}
			dto.setActName(masActName.toString());
			
		}
		else
		{
			dto.setActName("");
		}
				
		
		if(!listSuspensionMapTrader.isEmpty())
		{
			SuspensionMapTrader mapTrader = listSuspensionMapTrader.get(0);
			
			List<Trader> listTrader = traderDAO.findTraderByLicenseNo(mapTrader.getLicenseNo(), mapTrader.getTraderType(), RecordStatus.NORMAL.getStatus(), allOrg);
			
			
			if(!listTrader.isEmpty())
			{
				Trader trader = listTrader.get(0);
				
				Person person = trader.getPerson();
				
				String prefixName = "";
				if(null != person.getMasPrefix())
				{
					prefixName = person.getMasPrefix().getPrefixName();
				}
				
				String postfixName = "";
				if(null != person.getMasPosfix())
				{
					postfixName = person.getMasPosfix().getPostfixName();
				}
				
				if(PersonType.CORPORATE.getStatus().equals(person.getPersonType()))
				{
					
					String traderOwnerName = prefixName + person.getFirstName() + postfixName;
					dto.setTraderName(traderOwnerName);
				}
				else
				{
					String traderOwnerName = prefixName + person.getFirstName() + " " + person.getLastName();
					dto.setTraderName(traderOwnerName);
				}
				
				dto.setLicenseNo(trader.getLicenseNo());
				
				if(null != trader.getExpireDate())
				{
					String expireDate =DateUtils.getDateWithMonthName(df.format(trader.getExpireDate()));
					dto.setExpireDate(expireDate);
				}
				else
				{
					dto.setExpireDate("-");
				}
				
				List<Registration> listReg = (List<Registration>)this.registrationDAO.findAllByTrader(trader.getTraderId());	
				
				if(!listReg.isEmpty())
				{
					Registration reg = (Registration)listReg.get(0);
					
					if(null != reg.getApproveDate())
					{
						String approveDate =DateUtils.getDateWithMonthName(df.format(reg.getApproveDate()));
						dto.setApproveDate(approveDate);
					}
					
				}
				
				
				List<TraderAddress> listAddr = (List<TraderAddress>) this.traderAddressDAO.findAllByTrader(trader.getTraderId(), TraderAddressType.OFFICE_ADDRESS.getStatus(), null);
				
				if(!listAddr.isEmpty())
				{
					TraderAddress add = listAddr.get(0);
					StringBuilder traderAddress = new StringBuilder();
				
					
					traderAddress.append(add.getAddressNo());
				
					traderAddress.append(" ถนน ");
					traderAddress.append(add.getRoadName()==null?"-":add.getRoadName());
					
					traderAddress.append(" ตรอก/ซอย ");
					traderAddress.append(add.getSoi()==null?"-":add.getSoi());
					
				
					if(add.getMasTambol() != null)
					{
						traderAddress.append(" ตำบล/แขวง ");
						traderAddress.append(add.getMasTambol().getTambolName());
						
					}
					if(add.getMasAmphur()!= null)
					{
						traderAddress.append(" อำเภอ/เขต ");
						traderAddress.append(add.getMasAmphur().getAmphurName());
						
					}
					if(add.getMasProvince() != null)
					{
						traderAddress.append(" จังหวัด ");
						traderAddress.append(add.getMasProvince().getProvinceName());
						
					}
					
					dto.setTraderAddress(traderAddress.toString());
					
				}
				else
				{
					dto.setTraderAddress("");
				}
			}
		}
		return dto;
	}


	@Override
	public Object getSuspensionOrderDocx(Object obj, User user) throws Exception {
		if(!(obj instanceof SuspensionLicenseDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		System.out.println("#####getSuspensionOrderDocx");
		
		SuspensionLicenseDTO param = (SuspensionLicenseDTO)obj;
		
		List<SuspensionMapTrader> listSuspensionMapTrader =  this.suspensionMapTraderDAO.findBySuspendLicense(param.getSuspendId());
		LicenseDetailDTO license = null;
		if(CollectionUtils.isNotEmpty(listSuspensionMapTrader)){
			SuspensionMapTrader mapTrader = listSuspensionMapTrader.get(0);
					
			 license = (LicenseDetailDTO)this.getSuspensionOrderDocx(mapTrader.getLicenseNo());
		}
		return license;
	}

	@Override
	public Object getSuspensionOrderDocx(String licNO)
			throws Exception {
		LicenseDetailDTO license = this.suspensionLicenseDAO.findSuspensionsForPrint(licNO);
		
		String licenseNo = license.getLicenseNo();
		String effectiveDate = license.getEffectiveDate();
		
		String expireDate = license.getExpireDate();
		System.out.println("#####expireDate = "+expireDate);
		String officeAddress = license.getOfficeAddress();
		String shopAddress = license.getShopAddress();
		String personType = license.getPersonType();
		String traderOwnerName = license.getTraderOwnerName();
		String positionName = "";
		license.setPositionName(positionName);
		
		if(StringUtils.isNotEmpty(personType)){
			if(PersonType.CORPORATE.equals(personType)){
//				ห้างหุ้นส่วนจำกัดห้างหุ้นส่วนจำกัด กู๊ดมอร์นิ่ง (เชียงใหม่) ทัวร์ 
//				traderOwnerName
				if(license.getTraderOwnerName().contains("ห้างหุ้นส่วนจำกัด")){
					personType = "ห้างหุ้นส่วนจำกัด";
					traderOwnerName = traderOwnerName.replaceAll("ห้างหุ้นส่วนจำกัด", "");
					traderOwnerName = traderOwnerName.replaceAll("จำกัด", "");
					license.setTraderOwnerName(personType + traderOwnerName);
					positionName = "หุ้นส่วนผู้จัดการ";
					license.setPositionName(positionName);
				}else{
					license.setPositionName("กรรมการผู้จัดการ");
				}
			}
		}
		
		if(StringUtils.isNotEmpty(licenseNo)){
			
			license.setLicenseNo(ThaiBahtUtil.convertText2Numthai(licenseNo));
		}
		if(StringUtils.isNotEmpty(effectiveDate)){
			license.setEffectiveDate(DateUtils.getDateWithMonthName(effectiveDate));
		}
		if(StringUtils.isNotEmpty(officeAddress)){
			license.setOfficeAddress(ThaiBahtUtil.convertText2Numthai(officeAddress));
		}else{
			if(StringUtils.isNotEmpty(shopAddress)){
				license.setOfficeAddress(ThaiBahtUtil.convertText2Numthai(shopAddress));
			}else{
				license.setOfficeAddress("");
			}
		}
		
		if(StringUtils.isNotEmpty(expireDate)){
			license.setExpireDate(DateUtils.getDateWithMonthName(expireDate));
			
			String revokeDate = expireDate;
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Calendar c = Calendar.getInstance();
			c.setTime(sdf.parse(revokeDate));
			c.add(Calendar.MONTH, 9);  // number of month to add
			revokeDate = sdf.format(c.getTime());
			System.out.println("revokeDate = "+revokeDate);
			license.setRevokeDate(DateUtils.getDateWithMonthName(revokeDate));
		}
		
		
		
		
		license.setDocDate(DateUtils.getDateWithMonthName(DateUtils.getProcessDateFormatThai().format(new Date())));
		
		return license;
	}
	public static void main(String arg[]){
		String str = "ห้างหุ้นส่วนจำกัดห้างหุ้นส่วนจำกัด กู๊ดมอร์นิ่ง (เชียงใหม่) ทัวร์ จำกัด";
		System.out.println(str.contains("ห้างหุ้นส่วนจำกัด"));
		System.out.println(str.replaceAll("ห้างหุ้นส่วนจำกัด", ""));
		System.out.println(str.replaceAll("จำกัด", ""));
		
	}
	
}
