package sss.dot.tourism.service.registration;


import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.AdmUserDAO;
import sss.dot.tourism.dao.DotTrBillPaymentDAO;
import sss.dot.tourism.dao.DotTrFeeLicenseDAO;
import sss.dot.tourism.dao.DotTrFeePaymentDAO;
import sss.dot.tourism.dao.mas.MasFeeDAO;
import sss.dot.tourism.dao.mas.MasPrefixDAO;
import sss.dot.tourism.dao.mas.MasRunningNoDAO;
import sss.dot.tourism.dao.mas.RegisterFlowDAO;
import sss.dot.tourism.dao.registration.GuaranteeDAO;
import sss.dot.tourism.dao.registration.PersonDAO;
import sss.dot.tourism.dao.registration.PlantripDAO;
import sss.dot.tourism.dao.registration.PrintingCardDAO;
import sss.dot.tourism.dao.registration.PrintingLicenseDAO;
import sss.dot.tourism.dao.registration.ReceiptDAO;
import sss.dot.tourism.dao.registration.ReceiptDetailDAO;
import sss.dot.tourism.dao.registration.ReceiptMapRegistrationDAO;
import sss.dot.tourism.dao.registration.RegisterProgressDAO;
import sss.dot.tourism.dao.registration.RegistrationDAO;
import sss.dot.tourism.dao.registration.TraderAddressDAO;
import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.domain.AdmUser;
import sss.dot.tourism.domain.DotLicense;
import sss.dot.tourism.domain.DotTrBillPayment;
import sss.dot.tourism.domain.DotTrFeeLicense;
import sss.dot.tourism.domain.DotTrFeePayment;
import sss.dot.tourism.domain.Guarantee;
import sss.dot.tourism.domain.MasBank;
import sss.dot.tourism.domain.MasFee;
import sss.dot.tourism.domain.MasPrefix;
import sss.dot.tourism.domain.Person;
import sss.dot.tourism.domain.Receipt;
import sss.dot.tourism.domain.ReceiptDetail;
import sss.dot.tourism.domain.ReceiptMapRegistration;
import sss.dot.tourism.domain.RegisterProgress;
import sss.dot.tourism.domain.Registration;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.domain.TraderAddress;
import sss.dot.tourism.dto.mas.OfficerDTO;
import sss.dot.tourism.dto.registration.GuaranteeDTO;
import sss.dot.tourism.dto.registration.ReceiptDTO;
import sss.dot.tourism.dto.registration.ReceiptDetailDTO;
import sss.dot.tourism.dto.registration.RegisterProgressDTO;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.service.trader.ITourCompaniesService;
import sss.dot.tourism.service.trader.ITourLeaderService;
import sss.dot.tourism.service.trader.ITraderGuideService;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.GuaranteeStatus;
import sss.dot.tourism.util.GuaranteeType;
import sss.dot.tourism.util.GuideCategory;
import sss.dot.tourism.util.LicenseFeeStatus;
import sss.dot.tourism.util.MailService;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.PersonType;
import sss.dot.tourism.util.ProgressStatus;
import sss.dot.tourism.util.PropertyLoader;
import sss.dot.tourism.util.ReceiptStatus;
import sss.dot.tourism.util.RecordStatus;
import sss.dot.tourism.util.RegistrationType;
import sss.dot.tourism.util.RoleStatus;
import sss.dot.tourism.util.TraderAddressType;
import sss.dot.tourism.util.TraderCategory;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.User;

@Repository("accountService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
public class AccountService implements IAccountService {
	@Autowired
	TraderDAO traderDAO;
	@Autowired
	RegisterProgressDAO registerProgressDAO;
	@Autowired
	RegistrationDAO registrationDAO;
	@Autowired
	ITourLeaderService tourLeaderService;
	@Autowired
	RegisterFlowDAO registerFlowDAO;
	@Autowired
	IRegistrationService registrationService;
	@Autowired
	IRegisterProgressService registerProgressService;
	@Autowired
	MasRunningNoDAO masRunningNoDAO;
	@Autowired
	MasFeeDAO masFeeDAO;
	@Autowired
	ReceiptDAO receiptDAO;
	@Autowired
	ReceiptMapRegistrationDAO receiptMapRegistrationDAO;
	@Autowired
	ReceiptDetailDAO receiptDetailDAO;
	@Autowired
	IApproveRegistration approveRegistrationService;
	@Autowired
	PrintingLicenseDAO printingLicenseDAO;
	@Autowired
	PrintingCardDAO printingCardDAO;
	@Autowired
	GuaranteeDAO guaranteeDAO;
	
	
	@Autowired
	PersonDAO personDAO;
	@Autowired
	ITourCompaniesService tourCompaniesService;
	@Autowired
	TraderAddressDAO traderAddressDAO;
	@Autowired
	PlantripDAO plantripDAO;
	@Autowired
	ITraderGuideService guideService;
	
	@Autowired
	AdmUserDAO admUserDAO;
	
	@Autowired
	private MasPrefixDAO masPrefixDAO;
	
	@Autowired private DotTrFeeLicenseDAO dotTrFeeLicenseDAO;
	@Autowired private DotTrBillPaymentDAO dotTrBillPaymentDAO;
	@Autowired private DotTrFeePaymentDAO dotTrFeePaymentDAO;
	private Properties resources;
	  protected Properties getResources()
	  {
	    if(this.resources == null)
	    {
	      this.resources = PropertyLoader.loadProperties("sss/dot/tourism/util/ApplicationResources.properties");
	    }
	    return this.resources;
	  }
	
	private String PREVIOUS_ROLE = "'KEY','LAW','CHK','SUP','MAN','DIR'";
	private String NEXT_ROLE = "";
	public List getTraderBetweenRegistration(Object object, User user, int start, int limit) throws Exception {
		
		System.out.println("##########Service AccountService method getTraderBetweenRegistration");
		
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถโหลดข้อมูลได้การจดทะเบียนได้");
		}
		
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
			
		RegistrationDTO params = (RegistrationDTO)object;
		if((null != params.getRegProgressStatus()) && (!"".equals(params.getRegProgressStatus())))
	    {
			params.setRegistrationProgress(RoleStatus.ACCOUNT.getStatus());
			
			//ยังไม่พิมพ์ใบเสร็จ
			if(!params.getRegProgressStatus().equals(ProgressStatus.ACCEPT.getStatus()))
			{
				// w ยังไม่พิมพ์ใบเสร็จ คือ รับเรื่องมาแล้วแต่ยังไม่ได้พิมพ์
		    	if(params.getRegProgressStatus().equals(ProgressStatus.WAITING.getStatus()))
		    	{
		    		params.setRegGroupProgress(null);
		    	}
		    	
		    	//ยังไม่ส่งเรื่องมาพิมพ์
		    	else if(params.getRegProgressStatus().equals(ProgressStatus.GET_IT.getStatus()))
		    	{
		    		params.setRegGroupProgress(PREVIOUS_ROLE);
		    	}
		    	params.setRegistrationProgress(RoleStatus.ACCOUNT.getStatus());
		    	list = registrationService.getTraderBetweenRegistrationPaging(object, user, start, limit);
		    	
		    	
//		    	if(CollectionUtils.isNotEmpty(list)){
//		    		for(RegistrationDTO regDTO: list){
//		    			List<DotTrFeeLicense> listTrFee = this.dotTrFeeLicenseDAO.findTrFeeLicenseByRegId(regDTO.getRegId());
//		    			
//		    			regDTO.setPaymentStatusText("รอชำระเงิน");
//		    			regDTO.setFeePaidStatus("0");
//		    			if(CollectionUtils.isNotEmpty(listTrFee)){
//		    				DotTrFeeLicense trFee = listTrFee.get(0);
//		    				if(LicenseFeeStatus.PAID.getStatus().equals(trFee.getLicenseFeeStatus())){
//		    					//getbill paymetn
//		    					DotTrBillPayment bill = this.dotTrBillPaymentDAO.findBillPaymentByTrFeeLicense(trFee.getLicenseFeeId());
//		    					if(null != bill){
//		    						//getFeePayment
//		    						DotTrFeePayment feePay = (DotTrFeePayment) this.dotTrFeePaymentDAO.findTrFeePaymentByBill(bill.getBillPaymentId());
//		    						if(null != feePay){
//		    							BigDecimal totalFee = new BigDecimal(0);
//		    							BigDecimal totalFeePaid = new BigDecimal(0);
//		    							BigDecimal isFeeZero = new BigDecimal(0);
//		    							
//		    							BigDecimal fee = feePay.getTotalFee()==null?new BigDecimal(0):feePay.getTotalFee();
//		    							BigDecimal interest = feePay.getTotalInterest()==null?new BigDecimal(0):feePay.getTotalInterest();
//		    							BigDecimal feePaid = feePay.getTotalFeePaid()==null?new BigDecimal(0):feePay.getTotalFeePaid();
//		    							BigDecimal interestPaid = feePay.getTotalInterestPaid()==null?new BigDecimal(0):feePay.getTotalInterestPaid();
//		    							
//		    							totalFee = fee.add(interest);
//		    							totalFeePaid = feePaid.add(interestPaid);
//		    							isFeeZero =  totalFee.subtract(totalFeePaid);
//		    							isFeeZero = isFeeZero.divide(new BigDecimal(1), 2, BigDecimal.ROUND_HALF_UP);
//		    							String paymentStatusText = LicenseFeeStatus.PAID.getMeaning();
//		    							String feeStatus = "0";
//		    							//isFeeZero == 0 ชำระครบ
//		    							//isFeeZero < 0 ชำระเกิน
//		    							//isFeeZero > 0 ชำระขาด
//		    							if(isFeeZero.compareTo(new BigDecimal(0)) == -1){
//		    								isFeeZero = isFeeZero.multiply(new BigDecimal(-1));
//		    								paymentStatusText = "<font color='blue'>ชำระเกิน ( "+isFeeZero.toString()+" ) </font>";
//		    								feeStatus = "1";
//		    							}else{
//		    								if(isFeeZero.compareTo(new BigDecimal(0)) == 1){
//		    									paymentStatusText = "<font color='red'>ชำระขาด ( "+isFeeZero.toString()+" ) </font>";
//		    									feeStatus = "2";
//		    								}
//		    								
//		    							}
//		    							regDTO.setPaymentRecId(feePay.getPaymentRecId());
//		    							regDTO.setTotalFee(totalFee);
//		    							regDTO.setTotalFeePaid(totalFeePaid);
//		    							regDTO.setPaymentStatus(LicenseFeeStatus.PAID.getStatus());
//		    							regDTO.setPaymentStatusText(paymentStatusText);
//		    							regDTO.setDiffFee(isFeeZero);
//		    							regDTO.setFeePaidStatus(feeStatus);
//		    							if(null != feePay.getPaymentDate()){
//		    								DateFormat df = DateUtils.getProcessDateFormatThai();
//		    								regDTO.setPaymentDate(df.format(feePay.getPaymentDate()));
//		    							}
//		    						}
//		    					}
//		    				}
//		    			}
//		    		}
//		    	}
			}
			// พิมพ์ใบเสร็จแล้ว
			else
			{
				list = this.getReceiptTrader(object, user, start, limit);
				if(CollectionUtils.isNotEmpty(list)){
		    		for(RegistrationDTO regDTO: list){
		    			regDTO.setFeePaidStatus("0");
		    		}
		    	}
			}
			
	    	
	    }
		params.setRegistrationProgress(RoleStatus.ACCOUNT.getStatus());
		
		return list;
	}
	
	

	public List countTraderBetweenRegistrationPaging(Object object, User user,
			int start, int limit) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถโหลดข้อมูลได้การจดทะเบียนได้");
		}
		
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
			
		RegistrationDTO params = (RegistrationDTO)object;
		if((null != params.getRegProgressStatus()) && (!"".equals(params.getRegProgressStatus())))
	    {
			params.setRegistrationProgress(RoleStatus.ACCOUNT.getStatus());
			
			//ยังไม่พิมพ์ใบเสร็จ
			if(!params.getRegProgressStatus().equals(ProgressStatus.ACCEPT.getStatus()))
			{
				// w ยังไม่พิมพ์ใบเสร็จ คือ รับเรื่องมาแล้วแต่ยังไม่ได้พิมพ์
		    	if(params.getRegProgressStatus().equals(ProgressStatus.WAITING.getStatus()))
		    	{
		    		params.setRegGroupProgress(null);
		    	}
		    	
		    	//ยังไม่ส่งเรื่องมาพิมพ์
		    	else if(params.getRegProgressStatus().equals(ProgressStatus.GET_IT.getStatus()))
		    	{
		    		params.setRegGroupProgress(PREVIOUS_ROLE);
		    	}
		    	params.setRegistrationProgress(RoleStatus.ACCOUNT.getStatus());
		    	list = registrationService.countTraderBetweenRegistrationPaging(object, user, start, limit);
		    	
		    	
		    	//สิ่งที่ควรจะเป็น
//		    	list = registrationService.countTraderBetweenRegistrationPaging(params, user, start, limit);
			}
			// พิมพ์ใบเสร็จแล้ว
			else
			{
				list = this.countReceiptTrader(object, user, start, limit);
			}
			
	    	
	    }
		params.setRegistrationProgress(RoleStatus.ACCOUNT.getStatus());
		
		return list;
	}

	private List countReceiptTrader(Object object, User user ,int start, int limit) throws Exception
	{	if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
		RegistrationDTO params = (RegistrationDTO)object;
		List<Object[]> listObj = this.receiptDAO.findReceiptPaging(params, user, start, limit);
		return listObj;
	}
	

	private List getReceiptTrader(Object object, User user ,int start, int limit) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		RegistrationDTO params = (RegistrationDTO)object;
		
		DateFormat ft =  DateUtils.getProcessDateFormatThai();
		
		List<Object[]> listObj = this.receiptDAO.findReceiptPaging(params, user, start, limit);
		
		if(!listObj.isEmpty())
		{
			for(Object[] sel: listObj)
			{
				RegistrationDTO dto = new RegistrationDTO();
				
			
				dto.setRegId(Long.valueOf(sel[0].toString()));
				dto.setRegistrationNo(sel[1]==null?"":sel[1].toString());
				if(sel[2] != null)
				{
					
					dto.setRegistrationDate(ft.format((Date)sel[2]));
				}
				dto.setRegistrationProgress(sel[3]==null?"":sel[3].toString());
				if(sel[8] != null)
				{
					dto.setTraderType(sel[8]==null?"":sel[8].toString());
					dto.setTraderTypeName(TraderType.getMeaning(dto.getTraderType()));
				}
				if(sel[4]!=null)
				{
					dto.setRegistrationType(sel[4]==null?"":sel[4].toString());
					dto.setRegistrationTypeName(RegistrationType.getMeaning(dto.getTraderType(), dto.getRegistrationType()));
				}
				dto.setOrgId(Long.valueOf(sel[5].toString()));
				dto.setRegRecordStatus(sel[6]==null?"":sel[6].toString());
				dto.setTraderId(Long.valueOf(sel[7].toString()));
				dto.setTraderName(sel[9]==null?"":sel[9].toString());
				dto.setTraderNameEn(sel[10]==null?"":sel[10].toString());
				dto.setTraderCategory(sel[11]==null?"":sel[11].toString());
				
				
				dto.setTraderCategoryName(
						TraderCategory.getTraderCategory(
							dto.getTraderType()
							, dto.getTraderCategory()
						).getMeaning()
				);
				

				
				dto.setPersonId(Long.valueOf(sel[12].toString()));
				dto.setPersonType(sel[13]==null?"":sel[13].toString());
				dto.setPrefixName(sel[14]==null?"":sel[14].toString());
				dto.setFirstName(sel[15]==null?"":sel[15].toString());
				dto.setLastName(sel[16]==null?"":sel[16].toString());
				dto.setPostfixName(sel[17]==null?"":sel[17].toString());
				

				
				if(PersonType.CORPORATE.getStatus().equals(dto.getPersonType()))
				{
					String traderOwnerName = dto.getPrefixName() + dto.getFirstName() + dto.getPostfixName();
					dto.setTraderOwnerName(traderOwnerName);
				}
				else
				{
					String traderOwnerName = dto.getPrefixName() + dto.getFirstName() + " " + dto.getLastName();
					dto.setTraderOwnerName(traderOwnerName);
				}
				

				dto.setPrefixId(Long.valueOf(sel[18].toString()));
				
				if(sel[19] != null)
				{
					dto.setPostfixId(Long.valueOf(sel[19].toString()));
				}
				
				
				if(sel[20] != null)
				{
					dto.setAmphurId(Long.valueOf(sel[20].toString()));
				}
				if(sel[21] != null)
				{
					dto.setTaxAmphurId(Long.valueOf(sel[21].toString()));
				}
				if(sel[22] != null)
				{
					dto.setProvinceId(Long.valueOf(sel[22].toString()));
				}
				if(sel[23] != null)
				{
					dto.setTaxProvinceId(Long.valueOf(sel[23].toString()));
				}
				
				if(sel[24] != null)
				{
					dto.setTraderGuideId(Long.valueOf(sel[24].toString()));
				}
				
				dto.setPronunciationName(sel[25]==null?"":sel[25].toString());
			    dto.setLicenseGuideNo(sel[26]==null?"":sel[26].toString());
			    dto.setPersonType(sel[27]==null?"":sel[27].toString());
			    dto.setIdentityNo(sel[28]==null?"":sel[28].toString());
			    dto.setCommitteeName1(sel[29]==null?"":sel[29].toString());
			    dto.setCommitteeName2(sel[30]==null?"":sel[30].toString());
			    dto.setCommitteeNameSign(sel[31]==null?"":sel[31].toString());
			    dto.setPassportNo(sel[32]==null?"":sel[32].toString());
			    dto.setFirstNameEn(sel[33]==null?"":sel[33].toString());
			    dto.setLastNameEn(sel[34]==null?"":sel[34].toString());
			    dto.setGender(sel[35]==null?"":sel[35].toString());
			    dto.setPersonNationality(sel[36]==null?"":sel[36].toString());


			    if(sel[37] != null)
				{
					
					dto.setBirthDate(ft.format((Date)sel[37]));
				}
			    
			    dto.setAgeYear(sel[38]==null?0:Integer.valueOf(sel[38].toString()));
			    if(sel[39] != null)
				{
					
					dto.setIdentityNoExpire(ft.format((Date)sel[39]));
				}
			   
			    dto.setCorporateType(sel[40]==null?"":sel[40].toString());
			    dto.setTaxIdentityNo(sel[41]==null?"":sel[41].toString());
			    if(sel[42] != null)
				{
					
					dto.setIdentityDate(ft.format((Date)sel[42]));
				}
			    dto.setTraderRecordStatus(sel[43]==null?"":sel[43].toString());
			    
			    dto.setAmphurName(sel[44]==null?"":sel[44].toString());
			    dto.setTaxAmphurName(sel[45]==null?"":sel[45].toString());
			    dto.setProvinceName(sel[46]==null?"":sel[46].toString());
			    dto.setTaxProvinceName(sel[47]==null?"":sel[47].toString());
			    dto.setPrefixNameEn(sel[48]==null?"":sel[48].toString());
			    dto.setLicenseNo(sel[49]==null?"":sel[49].toString());

			    dto.setReceiptId(Long.valueOf(sel[50].toString()));
			    dto.setBookNo(sel[51]==null?"":sel[51].toString());
			    dto.setReceiptNo(sel[52]==null?"":sel[52].toString());
			    dto.setReceiptStatus(sel[53]==null?"":sel[53].toString());
				
				Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(dto.getRegId());
				if(TraderType.TOUR_COMPANIES.getStatus().equals(dto.getTraderType()))
				{
					tourCompaniesService.getRegistration(reg, dto, user);
				}
				
				if(TraderType.GUIDE.getStatus().equals(dto.getTraderType()))
				{
					guideService.getRegistration(reg, dto, user);
				}
				
				if(TraderType.LEADER.getStatus().equals(dto.getTraderType()))
				{
					tourLeaderService.getRegistration(reg, dto, user);
				}
				
				List<RegisterProgress> listprogress =  registerProgressDAO.findRegisterProgressbyRegistration(reg.getRegId(), ProgressStatus.GET_IT.getStatus());
				
				if(!listprogress.isEmpty())
				{
					RegisterProgress progress = listprogress.get(0);
					
					String authorityName = progress.getAdmUser().getMasPrefix().getPrefixName() + progress.getAdmUser().getUserName() +" "
							+progress.getAdmUser().getUserLastname();
					
					dto.setAuthorityName(authorityName);
				}
				
				//สามารถแก้ไขข้อมูลได้ ถ้า RegistrationProgress อยู่ที่ role ตัวเอง
				/**
				 * RECHECK
				 * SUPERVISOR
				 * MANAGER
				 * DIRECTOR
				 */
//				System.out.println("regProgress===" + regProgress);
//				System.out.println("regProgress===" + dto.getRegistrationProgress());
				dto.setRoleAction("P");
//				if(dto.getRegistrationProgress().equals(params.getRegistrationProgress()))
//				{
//					
//					//CHECK ว่าเป็นเรื่องดีกลับหรือไม่
//					List<RegisterProgress> listBackStatus =  registerProgressDAO.findProgressBackStatusbyRegistration(reg.getRegId(), dto.getRegistrationProgress());
//					if(!listBackStatus.isEmpty())
//					{
//						RegisterProgress role = listBackStatus.get(0);
//						dto.setProgressStatus(role.getProgressStatus());
//						dto.setProgressBackStatus(ProgressStatus.BACK.getStatus());
//					}
//				}
				
				list.add(dto);
				
				
			}

		}
			
		return list;
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void verification(Object object, User user) throws Exception {
		
		if(!(object instanceof RegisterProgressDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้้");
		}
		
		RegisterProgressDTO param = (RegisterProgressDTO)object;
		
		Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(param.getRegId());
		
		//find law
		List<RegisterProgress> listProg = this.registerProgressDAO.findRoleProgressbyRegistration(reg.getRegId(), RoleStatus.ACCOUNT.getStatus());
		Calendar cal = Calendar.getInstance();
		if(CollectionUtils.isNotEmpty(listProg))
		{
			RegisterProgress progress =  listProg.get(0);
			
			
			AdmUser  admUser = new AdmUser();
			admUser.setUserId(user.getUserId());
			progress.setAdmUser(admUser);
//			progress.setProgress(RoleStatus.KEY.getStatus());
			progress.setProgressDate(cal.getTime());
//			String positionName = user.getUserData().getPositionName()==null?"":user.getUserData().getPositionName();
			progress.setProgressDesc(RoleStatus.ACCOUNT.getStatus()+" รับชำระเงิน ");
			progress.setProgressStatus(ProgressStatus.ACCEPT.getStatus());
			progress.setLastUpdUser(user.getUserName());
			progress.setLastUpdDtm(new Date());
			
			registerProgressDAO.update(progress);
		}

//		//get next role
//		RegisterFlow flow = registerFlowDAO.findNextRole(RoleStatus.ACCOUNT.getStatus());
//		
		//update registration next role
		
		reg.setLastUpdUser(user.getUserName());
		reg.setLastUpdDtm(new Date());
		
		registrationDAO.update(reg);
		
		//insert register progress next role waiting
		// ส่งไป print license
		RegisterProgress printLicenseProgress =  new RegisterProgress();
		printLicenseProgress.setRegistration(reg);
		
		cal.add(Calendar.MINUTE, 10);
		printLicenseProgress.setProgressDate(cal.getTime());
		
		reg.setRegistrationProgress(RoleStatus.PRINT_LICENSE.getStatus());
		printLicenseProgress.setProgress(RoleStatus.PRINT_LICENSE.getStatus());
		printLicenseProgress.setProgressDesc(RoleStatus.PRINT_LICENSE.getMeaning() +" "+ProgressStatus.WAITING.getMeaning());
		printLicenseProgress.setProgressStatus(ProgressStatus.WAITING.getStatus());
		registerProgressDAO.insert(printLicenseProgress);
		
		// notification to print
		if(!TraderType.TOUR_COMPANIES.getStatus().equals(reg.getTrader().getTraderType()))
		{
			//print card
			RegisterProgress printCardProgress =  new RegisterProgress();
			printCardProgress.setRegistration(reg);
			reg.setRegistrationProgress(RoleStatus.PRINT_CARD.getStatus());
			printCardProgress.setProgress(RoleStatus.PRINT_CARD.getStatus());
			printCardProgress.setProgressDesc(RoleStatus.PRINT_CARD.getMeaning() +" "+ProgressStatus.WAITING.getMeaning());
			printCardProgress.setProgressStatus(ProgressStatus.WAITING.getStatus());
			printCardProgress.setProgressDate(cal.getTime());
			registerProgressDAO.insert(printCardProgress);
			
			// notification to print
		}
		
		Trader trader = reg.getTrader();
		Person person = trader.getPerson();
		// if register on line send mail
		List<Object[]> listTrFee = this.registrationDAO.listDotTrLicense(reg.getRegId());
		if(CollectionUtils.isNotEmpty(listTrFee)){

			StringBuffer mailContent = new StringBuffer(this.getResources().getProperty("inform.mail.payment"));
			if(TraderType.GUIDE.getStatus().equals(trader.getTraderType())){
				mailContent = new StringBuffer(this.resources.getProperty("inform.mail.approve.guide"));
			}else{
				mailContent = new StringBuffer(this.resources.getProperty("inform.mail.approve"));
			}
			this.registerProgressDAO.executeUpdate("UPDATE DOT_TR_FEE_LICENSE SET LICENSE_FEE_STATUS = '9' ,LAST_UPD_USER = '"+user.getUserName()+"' ,LAST_UPD_DTM = GETDATE() WHERE REG_ID = " + reg.getRegId(), null);
			
			String personName = "";
			if(PersonType.CORPORATE.getStatus().equals(person.getPersonType()))
			{					
				String traderOwnerName = person.getMasPrefix()==null?"":person.getMasPrefix().getPrefixName() + person.getFirstName() + (person.getMasPosfix()==null?"":person.getMasPosfix().getPostfixName());
				personName = "กรรมการผู้จัดการ " + traderOwnerName;
			}
			else
			{
				String traderOwnerName =  person.getMasPrefix()==null?"":person.getMasPrefix().getPrefixName() + person.getFirstName() + " " + person.getLastName();
				personName = traderOwnerName;
			}
			
			String email = "shaii.kanith@gmail.com";
			List<TraderAddress> listAddress = this.traderAddressDAO.findAllByTrader(trader.getTraderId(), TraderAddressType.OFFICE_ADDRESS.getStatus(), null);
			if(CollectionUtils.isNotEmpty(listAddress)){
				TraderAddress addr = listAddress.get(0);
				if(StringUtils.isNotEmpty(addr.getEmail())){
					email = addr.getEmail();
				}
			}
			
			String orgName = reg.getOrganization().getOrgFullName();
			String orgAddress = reg.getOrganization().getOrgAddress();
			
			String registrationType = "";
			if(StringUtils.isNotEmpty(reg.getRegistrationType())){
				registrationType = RegistrationType.getMeaning(trader.getTraderType(), reg.getRegistrationType());
			}
			StringBuffer regType = new StringBuffer();
			regType.append(registrationType);
			regType.append(" ใบอนุญาต" + TraderType.getMeaning(trader.getTraderType()));
			regType.append(" คำขอเลขที่ " + reg.getRegistrationNo());
			
			
			StringBuffer megContent = new StringBuffer();
			megContent.append(MessageFormat.format(mailContent.toString(), regType.toString(), personName, orgName,orgName + " " + orgAddress));
			
			MailService mailThred = new MailService( email, registrationType, megContent);
			mailThred.run();
		}

	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public Object getPayers(Object object, User user) throws Exception {
		
		if(!(object instanceof ReceiptDTO))
		{
			throw new IllegalArgumentException("!(object instanceof ReceiptDTO)");
		}
		
		ReceiptDTO param = (ReceiptDTO) object;
		
		Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(param.getRegId());
		Trader trader = reg.getTrader();
		Person person = trader.getPerson();
		
		ReceiptDTO dto = new ReceiptDTO();
		

		String bookNo = this.masRunningNoDAO.getTempAccountBookNo(trader.getOrganization().getOrgId(), trader.getTraderType());
		String receiptNo = this.masRunningNoDAO.getTempAccountReceiptNo(trader.getOrganization().getOrgId(), trader.getTraderType());
		
		dto.setBookNo(bookNo);
		dto.setReceiptNo(receiptNo);
		dto.setRegistrationNo(reg.getRegistrationNo());
		dto.setTraderCategory(trader.getTraderCategory());
		dto.setTraderCategoryName(TraderCategory.getMeaning(trader.getTraderType(), trader.getTraderCategory()));
		dto.setTraderType(trader.getTraderType());
		
		if(PersonType.CORPORATE.getStatus().equals(person.getPersonType()))
		{
			String prefixName = person.getMasPrefix()==null?"":person.getMasPrefix().getPrefixName();
			String postfixName = person.getMasPosfix()==null?"":person.getMasPosfix().getPostfixName();
			String traderOwnerName = prefixName + person.getFirstName() + postfixName;
			dto.setReceiptName(traderOwnerName);
		}
		else
		{
			String prefixName = person.getMasPrefix()==null?"":person.getMasPrefix().getPrefixName();
//			String postfixName = person.getMasPosfix()==null?"":person.getMasPosfix().getPostfixName();
			String traderOwnerName = prefixName + person.getFirstName() + " " + person.getLastName();
			dto.setReceiptName(traderOwnerName);
		}
		
		
		
		return dto;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public Object getReceipt(Object object, User user) throws Exception {
		if(!(object instanceof ReceiptDTO))
		{
			throw new IllegalArgumentException("!(object instanceof ReceiptDTO)");
		}
		
		ReceiptDTO param = (ReceiptDTO) object;
		
		ReceiptDTO dto = new ReceiptDTO();
		
		Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(param.getRegId());
		Trader trader = reg.getTrader();
		   
		//พิมพ์ใบเสร็จไปแล้ว
		if(param.getReceiptId() > 0)
		{			
			dto.setRegistrationNo(reg.getRegistrationNo());
			dto.setTraderCategory(trader.getTraderCategory());
			dto.setTraderCategoryName(TraderCategory.getMeaning(trader.getTraderType(), trader.getTraderCategory()));
			dto.setTraderType(trader.getTraderType());
			dto.setRegId(reg.getRegId());
			dto.setRegistrationType(reg.getRegistrationType());
			
			Receipt receipt = (Receipt)this.receiptDAO.findByPrimaryKey(param.getReceiptId());
			ObjectUtil.copy(receipt, dto);
			
			
		}
		else
		{
			dto = (ReceiptDTO)this.getPayers(param, user);
			dto.setReceiveOfficerName(user.getUserData().getUserFullName());
			dto.setAuthority(param.getAuthority());
		}
		
		ObjectUtil.copy(reg, dto);
		ObjectUtil.copy(trader, dto);
		
		if(TraderType.TOUR_COMPANIES.getStatus().equals(trader.getTraderType()))
		{
			// get guarantee
			this.getGuarantee(dto, user);		
		}
		
		return dto;
	}

	public List getFee(Object object, User user) throws Exception {
		
		if(!(object instanceof ReceiptDTO))
		{
			throw new IllegalArgumentException("!(object instanceof ReceiptDTO)");
		}
		
		ReceiptDTO param = (ReceiptDTO) object;
		
		List<ReceiptDTO> list = new ArrayList<ReceiptDTO>();
		
		
		return list;
	}

	public List getOfficer(Object object, User user) throws Exception {
		
	
		
		if(!(object instanceof OfficerDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		
		OfficerDTO param = (OfficerDTO)object;
		
		List<AdmUser> listAll = (List<AdmUser>) this.admUserDAO.findOfficerByGroupRole(param.getGroupRole(), user.getUserData().getOrgId());
		
		List<OfficerDTO> list = new ArrayList<OfficerDTO>();
		
		if(!listAll.isEmpty())
		{
			for(AdmUser admUser: listAll)
			{
				OfficerDTO dto = new OfficerDTO();
				
				ObjectUtil.copy(admUser, dto);
				
				String userFullName = ((MasPrefix)this.masPrefixDAO.findByPrimaryKey
						(admUser.getMasPrefix().getPrefixId())).getPrefixName()
						+admUser.getUserName()+" "+admUser.getUserLastname();
				dto.setOfficerName(userFullName);
				dto.setOfficerId(admUser.getUserId());
				
				list.add(dto);
			}
		}
		
		return list;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveReceipt(Object[] object, User user) throws Exception {
				
		
	}


	

	


	public List getReceiptDetail(Object object, User user) throws Exception {
		if(!(object instanceof ReceiptDTO))
		{
			throw new IllegalArgumentException("!(object instanceof ReceiptDTO)");
		}
		List<ReceiptDTO> list = new ArrayList<ReceiptDTO>();
		
		return list;
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void updateReceipt(Object[] object, User user) throws Exception {
//		
	}

	//@shaii
	public List getReceiptDetailFee(Object object, User user) throws Exception {
		
		if(!(object instanceof ReceiptDetailDTO))
		{
			throw new IllegalArgumentException("!(object instanceof ReceiptDetailDTO)");
		}
		
		ReceiptDetailDTO param = (ReceiptDetailDTO)object;
		
		List<ReceiptDetailDTO> list = new ArrayList<ReceiptDetailDTO>();
		if(param.getReceiptId() > 0)
		{
			list = this.receiptDetailDAO.findReceiptDetailFee(param.getReceiptId(), param.getTraderType(), param.getRegistrationType());
		}
		else
		{
			Registration reg = (Registration) this.registrationDAO.findByPrimaryKey(param.getRegId());
			Trader trader = reg.getTrader();
			
			List<Trader> listCurr = this.traderDAO.findTraderByLicenseNo(trader.getLicenseNo(), trader.getTraderType(), RecordStatus.NORMAL.getStatus(), 0);

			Trader currTrader = null;
			
			BigDecimal totalfee = new BigDecimal(0);
			if(!listCurr.isEmpty())
			{
				currTrader = listCurr.get(0);
			}
			
			//ตรวจสอบว่าเกินกำหนดชำระ ถ้าเกินคิดค่าปรัป
			if(TraderType.TOUR_COMPANIES.equals(trader.getTraderType()))
			{
				if(RegistrationType.TOUR_COMPANIES_RENEW_REGISTRATION.getStatus().equals(reg.getRegistrationType()))
				{
					if((null != currTrader) && (currTrader.getExpireDate().before(new Date())))
					{
						Double fee_mny = new Double(1000);
						fee_mny = fee_mny * 0.02;
						
						
//						Date today = new Date();
//						long diff =  today.getTime() - currTrader.getExpireDate().getTime();
//						diff = Math.abs(diff);
//						long diffDays = diff / (24 * 60 * 60 * 1000);
//					
//						BigDecimal diffD = new BigDecimal(diffDays);
//						BigDecimal diffMonth = diffD.divide(new BigDecimal(30), BigDecimal.ROUND_UP);
//						diffMonth.setScale(0, BigDecimal.ROUND_UP);
						
						BigDecimal interest =  calculateInterest(new BigDecimal(1000), new BigDecimal(0.02), new Date(), currTrader.getExpireDate());
						
						
						totalfee = totalfee.add(interest);
						
					}
				}
				
			}
			
			List<MasFee> listAll = (List<MasFee>) this.masFeeDAO.findFee(param.getRegistrationType(), param.getTraderType());
			
			if(!listAll.isEmpty())
			{
				for(MasFee fee: listAll)
				{
					ReceiptDetailDTO dto = new ReceiptDetailDTO();
					ObjectUtil.copy(fee, dto);
				
					if(TraderType.GUIDE.getStatus().equals(trader.getTraderType()))
					{
						String guideGategory  = MessageFormat.format(fee.getFeeName(), GuideCategory.getMeaning(trader.getTraderCategory()));
						System.out.println(guideGategory);
						
						dto.setFeeName(guideGategory);
					}
					
					//ค่าปรัป
					if(totalfee.compareTo(new BigDecimal(0)) > 0)
					{
						if(dto.getFeeId() == 14)
						{
							dto.setFeeMny(totalfee);
						}
					}
					
					list.add(dto);
				}
			}
		}
		
		
		return list;
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	private void createReceiptAccount(Object obj, Object[] object, User user)throws Exception
	{
		ReceiptDTO receiptDTO = (ReceiptDTO) obj;
		
		Registration reg = (Registration) this.registrationDAO.findByPrimaryKey(receiptDTO.getRegId());
		Trader trader = reg.getTrader();
		
		String bookNo = this.masRunningNoDAO.getAccountBookNo(trader.getOrganization().getOrgId(), trader.getTraderType());
		String receiptNo = this.masRunningNoDAO.getAccountReceiptNo(trader.getOrganization().getOrgId(), trader.getTraderType());
		
		Receipt receipt = new Receipt();
		ObjectUtil.copy(receiptDTO, receipt);
		
		receipt.setBookNo(bookNo);
		receipt.setReceiptNo(receiptNo);
		receipt.setReceiptStatus(ReceiptStatus.PAY.getStatus());
		receipt.setReceiptRemark("ชำระเงิน");
		receipt.setRecordStatus(RecordStatus.NORMAL.getStatus());
		receipt.setCreateUser(user.getUserName());
		receipt.setCreateDtm(new Date());
		
		Long receiptId = (Long)this.receiptDAO.insert(receipt);
		receipt.setReceiptId(receiptId);
		receiptDTO.setReceiptId(receiptId);
		
		ReceiptMapRegistration mp = new ReceiptMapRegistration();
		mp.setReceipt(receipt);
		mp.setRegistration(reg);
		mp.setReceiptMapDate(receipt.getReceiveOfficerDate());
		
		
		this.receiptMapRegistrationDAO.insert(mp);
		
		
		this.updateReceiptDetail(object, receipt, user);
		
		// approve
		RegisterProgressDTO param = new RegisterProgressDTO();
		param.setRegId(reg.getRegId());
		
		this.approveRegistrationService.approve(reg, user);
		this.verification(param, user);

		boolean isTourCompanies = TraderType.TOUR_COMPANIES.getStatus().equals(trader.getTraderType());
		if(isTourCompanies)
		{
			//add new Guarantee
			GuaranteeDTO guaranteeDTO = new GuaranteeDTO();
			
			ObjectUtil.copy(receiptDTO, guaranteeDTO);
			
			guaranteeDTO.setTraderId(trader.getTraderId());
			guaranteeDTO.setLicenseNo(trader.getLicenseNo());
			guaranteeDTO.setTraderType(trader.getTraderType());
			
			this.saveGuaranteeAccount(guaranteeDTO, user);
			
			List<DotTrFeeLicense> listTrFee = (List<DotTrFeeLicense>)this.dotTrFeeLicenseDAO.findTrFeeLicenseByRegId(reg.getRegId());
			if(CollectionUtils.isNotEmpty(listTrFee)){
				
				DotTrFeeLicense trFee = listTrFee.get(0);
				DotLicense license = trFee.getDotLicense();
				
				//update guarantee
				List<Guarantee> listGuarantees =  (List<Guarantee>)this.guaranteeDAO.findByTrader(trader.getLicenseNo(), trader.getTraderType(), 0);
				if(CollectionUtils.isNotEmpty(listGuarantees)){
					Guarantee guarantee = listGuarantees.get(0);
					if(null != guarantee.getMasBankByBankGuaranteeBankId()){
						license.setMasBankByBankGuaranteeBankId(guarantee.getMasBankByBankGuaranteeBankId());
					}
					if(null != guarantee.getMasBankByCashAccountBankId()){
						license.setMasBankByCashAccountBankId(guarantee.getMasBankByCashAccountBankId());
					}
					if(StringUtils.isNotEmpty(guarantee.getCashAccountId())){
						license.setCashAccountId(guarantee.getCashAccountId());
					}
					if(StringUtils.isNotEmpty(guarantee.getCashAccountName())){
						license.setCashAccountName(guarantee.getCashAccountName());
					}
					if(StringUtils.isNotEmpty(guarantee.getCashAccountBankBranchName())){
						license.setCashAccountBankBranchName(guarantee.getCashAccountBankBranchName());
					}
					
					license.setCashAccountMny(guarantee.getCashAccountMny()==null?new BigDecimal(0):guarantee.getCashAccountMny());
					
					if(StringUtils.isNotEmpty(guarantee.getBankGuaranteeId())){
						license.setBankGuaranteeId(guarantee.getBankGuaranteeId());
					}
					if(StringUtils.isNotEmpty(guarantee.getBankGuaranteeName())){
						license.setBankGuaranteeName(guarantee.getBankGuaranteeName());
					}
					
					license.setBankGuaranteeMny(guarantee.getBankGuaranteeMny()==null?new BigDecimal(0):guarantee.getBankGuaranteeMny());
					
					if((null != guarantee.getGovernmentBondMny()) 
							&& (guarantee.getGovernmentBondMny().compareTo(new BigDecimal(0)) == 1)){
						
						if(StringUtils.isNotEmpty(guarantee.getBankGuaranteeBankBranchName())){
							license.setBankGuaranteeBankBranchName(guarantee.getBankGuaranteeBankBranchName());
						}
						if(StringUtils.isNotEmpty(guarantee.getGovernmentBondId())){
							license.setGovernmentBondId(guarantee.getGovernmentBondId());
						}
						if(null != guarantee.getGovernmentBondExpireDate()){
							license.setGovernmentBondExpireDate(guarantee.getGovernmentBondExpireDate());
						}
						license.setGovernmentBondMny(guarantee.getGovernmentBondMny()==null?new BigDecimal(0):guarantee.getGovernmentBondMny());
					}
				}
			}
			
			
		}


	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	private void updateReceiptAccount(Object obj, Object[] object, User user)throws Exception
	{
		ReceiptDTO param = (ReceiptDTO)obj;
		Receipt receipt = (Receipt) this.receiptDAO.findByPrimaryKey(param.getReceiptId());
		ObjectUtil.copy(param, receipt);
		
//		receipt.setReceiptStatus(ReceiptStatus.PAY.getStatus());
//		receipt.setReceiptRemark("ชำระเงิน");
//		receipt.setRecordStatus(RecordStatus.NORMAL.getStatus());
		receipt.setLastUpdUser(user.getUserName());
		receipt.setLastUpdDtm(new Date());
		
		this.receiptDAO.update(receipt);
		
		this.updateReceiptDetail(object, receipt, user);
		

		Registration reg = (Registration) this.registrationDAO.findByPrimaryKey(param.getRegId());
		Trader trader = reg.getTrader();
		
		boolean isTourCompanies = TraderType.TOUR_COMPANIES.getStatus().equals(param.getTraderType());
		if(isTourCompanies)
		{
			
			//update guarantee change and insert
			GuaranteeDTO guaranteeDTO = new GuaranteeDTO();
			ObjectUtil.copy(param, guaranteeDTO);
			
			guaranteeDTO.setLicenseNo(trader.getLicenseNo());
			guaranteeDTO.setTraderType(trader.getTraderType());
			
			List<Guarantee> listGuarantee =  (List<Guarantee>)this.guaranteeDAO.findByTrader(trader.getLicenseNo(), trader.getTraderType(), 0);
						
			if(!listGuarantee.isEmpty())
			{
				Guarantee guarantee = listGuarantee.get(0);
				guaranteeDTO.setGuaranteeId(guarantee.getGuaranteeId());
			}
			
			this.saveGuaranteeAccount(guaranteeDTO, user);
		}		
		
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	private void updateReceiptDetail(Object[] object, Receipt receipt, User user) throws Exception
	{
		if(object.length > 0)
		{
			List<ReceiptDetail> listOldAct = (List<ReceiptDetail>)this.receiptDetailDAO.findbyReceipt(receipt.getReceiptId());
			if(!listOldAct.isEmpty())
			{
				for(ReceiptDetail masAct: listOldAct)
				{
					this.receiptDetailDAO.delete(masAct);
				}
			}
			
			for(Object obj: object)
			{
				ReceiptDetailDTO receiptDetailDTO = (ReceiptDetailDTO)obj;
				ReceiptDetail receiptDetail = new ReceiptDetail();
				
				MasFee masFee = new MasFee();
				
				masFee.setFeeId(receiptDetailDTO.getFeeId());
				
				receiptDetail.setMasFee(masFee);
				receiptDetail.setFeeName(receiptDetailDTO.getFeeName());
				receiptDetail.setFeeMny(receiptDetailDTO.getFeeMny());
				receiptDetail.setReceipt(receipt);
				receiptDetail.setRecordStatus(RecordStatus.NORMAL.getStatus());
				receiptDetail.setCreateUser(user.getUserName());
				receiptDetail.setCreateDtm(new Date());
				
				receiptDetailDAO.insert(receiptDetail);
			}
		}
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveReceiptAccount(Object obj, Object[] object, User user)
			throws Exception {
		
		if(!(obj instanceof ReceiptDTO))
		{
			throw new IllegalArgumentException("!(object instanceof ReceiptDTO)");
		}
		
		ReceiptDTO receiptDTO = (ReceiptDTO)obj;
		
		if(receiptDTO.getReceiptId() > 0)
		{
			this.updateReceiptAccount( obj,  object,  user);
		}
		else
		{
			this.createReceiptAccount( obj,  object,  user);
		}
		
	}
	
	private void updateGuaranteeAccount(Object object, User user)  throws Exception
	{
		// update to Cancel
		GuaranteeDTO dto = (GuaranteeDTO) object;
		
		Guarantee gua = (Guarantee)this.guaranteeDAO.findByPrimaryKey(dto.getGuaranteeId());
		gua.setGuaranteeStatus(GuaranteeStatus.CHANGE.getStatus());
		gua.setGuaranteeRemark("มีการเปลี่ยนแปลงหลักประกัน");
		this.guaranteeDAO.update(gua);
		
		// insert new 
		this.createGuaranteeAccount(object, user);
	}
	
	private void createGuaranteeAccount(Object object, User user)  throws Exception
	{
		GuaranteeDTO dto = (GuaranteeDTO) object;
//		
//		boolean isNewGuarantee = true;
//		Guarantee  gua = null;
//		if(dto.getTraderId() > 0){
//			List<Guarantee> listAll = this.guaranteeDAO.findByTrader(dto.getTraderId());
//			if(CollectionUtils.isNotEmpty(listAll)){
//				gua = listAll.get(0);
//				isNewGuarantee = false;
//			}else{
//				gua = new Guarantee();
//			}
//		}else{
//			gua = new Guarantee();
//		}
		
		Guarantee  gua = new Guarantee();
		
		ObjectUtil.copy(dto, gua);

		if(dto.getMasBankByCashAccountBankId() > 0)
		{
			MasBank masBank = new MasBank();
			masBank.setMasBankId(dto.getMasBankByCashAccountBankId());
			gua.setMasBankByCashAccountBankId(masBank);
		}
		
		if(dto.getMasBankByBankGuaranteeBankId() > 0)
		{
			MasBank masBank = new MasBank();
			masBank.setMasBankId(dto.getMasBankByBankGuaranteeBankId());
			gua.setMasBankByBankGuaranteeBankId(masBank);
		}
		

		
		BigDecimal guaranteeMny = new BigDecimal(0);
		
		guaranteeMny = guaranteeMny.add(dto.getCashAccountMny()==null?new BigDecimal(0):dto.getCashAccountMny());
		guaranteeMny = guaranteeMny.add(dto.getBankGuaranteeMny()==null?new BigDecimal(0):dto.getBankGuaranteeMny());
		guaranteeMny = guaranteeMny.add(dto.getGovernmentBondMny()==null?new BigDecimal(0):dto.getGovernmentBondMny());
		
		gua.setGuaranteeMny(guaranteeMny);
		gua.setGuaranteeStatus(GuaranteeStatus.NORMAL.getStatus());
		gua.setRecordStatus(RecordStatus.NORMAL.getStatus());
		gua.setCreateDtm(new Date());
		gua.setCreateUser(user.getUserName());
		this.guaranteeDAO.insert(gua);
		
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveGuaranteeAccount(Object object, User user)  throws Exception
	{
		if(!(object instanceof GuaranteeDTO))
		{
			throw new IllegalArgumentException("!(object instanceof GuaranteeDTO)");
		}
		
		GuaranteeDTO dto = (GuaranteeDTO) object;
		
		if(dto.getGuaranteeId() > 0)
		{
			//update
			this.updateGuaranteeAccount(object, user);
		}
		else
		{
			//create
			this.createGuaranteeAccount(object, user);
		}
	}
	
	public Object getGuarantee(ReceiptDTO dto, User user) throws Exception 
	{
		List<Guarantee> listAll = null;
		System.out.println("--------------------getGuarantee getLicenseNo" + dto.getLicenseNo());
		System.out.println("--------------------getGuarantee getTraderId" + dto.getTraderId());
		if(StringUtils.isNotEmpty(dto.getLicenseNo())){
			listAll = this.guaranteeDAO.findByTrader(dto.getLicenseNo(), dto.getTraderType() ,0);
		}else{
			listAll = this.guaranteeDAO.findByTrader(dto.getTraderId());
		}
		
		if(CollectionUtils.isNotEmpty(listAll))
		{
			Guarantee guarantee = listAll.get(0);
			ObjectUtil.copy(guarantee, dto);
			dto.setGuaranteeCategoryName(dto.getTraderCategoryName());
			
			if(guarantee.getMasBankByBankGuaranteeBankId() != null)
			{
				dto.setMasBankByBankGuaranteeBankId(guarantee.getMasBankByBankGuaranteeBankId().getMasBankId());
			}
			
			if(guarantee.getMasBankByCashAccountBankId() != null)
			{
				dto.setMasBankByCashAccountBankId(guarantee.getMasBankByCashAccountBankId().getMasBankId());
			}
		}
		
		return dto;
	}

	@SuppressWarnings("unchecked")
	public Map getSummaryReceiptAndFee(Object object, User user)
			throws Exception {
		
		if(!(object instanceof ReceiptDTO))
		{
			throw new IllegalArgumentException("!(object instanceof GuaranteeDTO)");
		}
		
		ReceiptDTO param = (ReceiptDTO) object;
		
		DateFormat df = DateUtils.getProcessDateFormatThai();
		
		Map model = new HashMap();
		
		String regDate = param.getReceiveOfficerDateFrom();
		if(!param.getReceiveOfficerDateFrom().equals(param.getReceiveOfficerDateTo()))
		{
			regDate = regDate + " ถึงวันท่ี่ "+ param.getReceiveOfficerDateTo();
		}
		
		model.put("regDateFrom", regDate);
		
		List<ReceiptDTO> list = new ArrayList<ReceiptDTO>();
		
		List<Object[]> listAll = (List<Object[]>)this.receiptDAO.findSummaryReceipt(param, user);
		
		if(!listAll.isEmpty())
		{
			int receiptTotal = listAll.size();
			BigDecimal feeMnyTotal = new BigDecimal(0);
			BigDecimal guaranteeMnyTotal = new BigDecimal(0);
			BigDecimal cashAccountMnyTotal = new BigDecimal(0);
			BigDecimal bankGuaranteeMnyTotal = new BigDecimal(0);
			BigDecimal govermentBoundMnyTotal = new BigDecimal(0);
			
			for(Object[] obj : listAll)
			{
				ReceiptDTO dto = new ReceiptDTO();
				
//				dto.setReceiptId(Long.valueOf(obj[0].toString()));
				dto.setBookNo(obj[1]==null?"":obj[1].toString());
				dto.setReceiptNo(obj[2]==null?"":obj[2].toString());
				dto.setReceiptName(obj[3]==null?"":obj[3].toString());
//				dto.setTraderId(Long.valueOf(obj[4].toString()));
				dto.setLicenseNo(obj[5]==null?"":obj[5].toString());
				dto.setTraderName(obj[6]==null?"":obj[6].toString());
				dto.setTraderCategory(obj[7]==null?"":obj[7].toString());
				dto.setTraderType(obj[8]==null?"":obj[8].toString());
				
				if(TraderType.TOUR_COMPANIES.getStatus().equals(dto.getTraderType()))
				{
					dto.setGuaranteeId(1);
		
					model.put("guarantee", 1);
				}
				else
				{
					dto.setGuaranteeId(0);
					model.put("guarantee", 0);
				}
				
				if((null != dto.getTraderCategory()) && (!dto.getTraderCategory().isEmpty()))
				{
					if((null != dto.getTraderType()) && (!dto.getTraderType().isEmpty()))
					{
						if(TraderType.TOUR_COMPANIES.getStatus().equals(dto.getTraderType()))
						{
							dto.setTraderCategoryName(TraderCategory.getMeaning(dto.getTraderType(), dto.getTraderCategory()));
						}
						else
						{
							dto.setTraderCategoryName(GuideCategory.getMeaning(dto.getTraderCategory()));
						}
					}
				}
				if(null != obj[10])
				{
					dto.setRegistrationType(RegistrationType.getMeaning(dto.getTraderType(), obj[10].toString()));
				}
				
				BigDecimal feemny = obj[11]==null?new BigDecimal(0):new BigDecimal(obj[11].toString());
				
				dto.setFeeMny(feemny.doubleValue());
				dto.setGuaranteeMny(obj[12]==null?new BigDecimal(0):new BigDecimal(obj[12].toString()));
				dto.setCashAccountMny(obj[13]==null?new BigDecimal(0):new BigDecimal(obj[13].toString()));
				dto.setBankGuaranteeMny(obj[14]==null?new BigDecimal(0):new BigDecimal(obj[14].toString()));
				dto.setGovernmentBondMny(obj[15]==null?new BigDecimal(0):new BigDecimal(obj[15].toString()));
				
				if(obj[16] != null)
				{
					dto.setReceiveOfficerDate(df.format((Date)obj[16]));
				}
				
				
				 feeMnyTotal = feeMnyTotal.add(feemny);
				 guaranteeMnyTotal = guaranteeMnyTotal.add(dto.getGuaranteeMny());
				 cashAccountMnyTotal = cashAccountMnyTotal.add(dto.getCashAccountMny());
				 bankGuaranteeMnyTotal = bankGuaranteeMnyTotal.add(dto.getBankGuaranteeMny());
				 govermentBoundMnyTotal = govermentBoundMnyTotal.add(dto.getGovernmentBondMny());
				
				list.add(dto);
			}
			
			model.put("listReceipt", list);
			model.put("receiptTotal", receiptTotal);
			model.put("feeMnyTotal", feeMnyTotal);
			model.put("guaranteeMnyTotal", guaranteeMnyTotal);
			model.put("cashAccountMnyTotal", cashAccountMnyTotal);
			model.put("bankGuaranteeMnyTotal", bankGuaranteeMnyTotal);
			model.put("govermentBondMnyTotal", govermentBoundMnyTotal);
		}
		return model;
	}
	
	//Oat Add 30/10/57
	public Map getSummaryGuaranteeReceiptAndFee(Object object, User user)throws Exception 
	{
		
		if(!(object instanceof ReceiptDTO))
		{
			throw new IllegalArgumentException("!(object instanceof GuaranteeDTO)");
		}
		
		ReceiptDTO param = (ReceiptDTO) object;
		
		DateFormat df = DateUtils.getProcessDateFormatThai();
		DecimalFormat def = new DecimalFormat("#,##0");
		
		Map model = new HashMap();
		
		String regDate = param.getReceiveOfficerDateFrom();
		if(!param.getReceiveOfficerDateFrom().equals(param.getReceiveOfficerDateTo()))
		{
			regDate = regDate + " ถึงวันท่ี่ "+ param.getReceiveOfficerDateTo();
		}
		
		model.put("regDateFrom", regDate);
		
//		List<ReceiptDTO> list = new ArrayList<ReceiptDTO>();
		
//ค่าธรรมเนียม
		long countNew = 0;
		long countReNew = 0;
		long countAll = 0;
		long feeMnyNew = 0;
		long feeMnyReNew = 0;
		long feeMnyAdd = 0;//เงินเพิ่ม
		long feeMnyAll = 0;
		
		//รายใหม่
		param.setFeeId(1);
		List<Object[]> listReceiptFeeCountNew = (List<Object[]>)this.receiptDAO.findReceiptFeeCount(param, user);
		if((listReceiptFeeCountNew!=null && !listReceiptFeeCountNew.isEmpty()))
		{
			countNew = Long.valueOf(listReceiptFeeCountNew.get(0)[0].toString());
			feeMnyNew = countNew*3000;
		}
		model.put("countNew",countNew);
		model.put("feeMnyNew",def.format(feeMnyNew));
		
		//ชำระราย2ปี 
		param.setFeeId(3);
		List<Object[]> listReceiptFeeCountReNew = (List<Object[]>)this.receiptDAO.findReceiptFeeCount(param, user);
		if((listReceiptFeeCountReNew!=null && !listReceiptFeeCountReNew.isEmpty()))
		{
			countReNew = Long.valueOf(listReceiptFeeCountReNew.get(0)[0].toString());
			//OAT EDIT 11/03/58
			if(listReceiptFeeCountReNew.get(0)[2] != null && listReceiptFeeCountReNew.get(0)[2] != "")
				feeMnyReNew = Math.round(Double.valueOf(listReceiptFeeCountReNew.get(0)[2].toString()));
			else
				feeMnyReNew = Math.round(Double.valueOf("0.00"));
//			feeMnyReNew = countReNew*1000; 
			//END OAT EDIT 11/03/58
		}
		model.put("countReNew",countReNew);
		model.put("feeMnyReNew",def.format(feeMnyReNew));		
		
		//เงินเพิ่ม
		param.setFeeId(14);
		List<Object[]> listReceiptFeeAdd = (List<Object[]>)this.receiptDAO.findReceiptFeeAdd(param, user);
		if(listReceiptFeeAdd != null && !listReceiptFeeAdd.isEmpty())
		{
			feeMnyAdd = Math.round(Double.valueOf(listReceiptFeeAdd.get(0)[1].toString())); 
		}
		model.put("feeMnyAdd",def.format(feeMnyAdd));
		
		//จำนวนรวม
		countAll = countNew+countReNew;
		model.put("countAll",countAll); 
		//เงินรวม
		feeMnyAll = feeMnyNew+feeMnyReNew+feeMnyAdd;
		model.put("feeMnyAll",def.format(feeMnyAll));
		//------------------------------
		
//วงเงินและประเภทหลักประกัน
//รายใหม่ เงินสด
		//รายใหม่ เงินสด Outbound 
		//ธุรกิจนำเที่ยว
		param.setTraderType(TraderType.TOUR_COMPANIES.getStatus());
		//รายใหม่
		param.setRegistrationType(RegistrationType.TOUR_COMPANIES_NEW_REGISTRATION.getStatus());
		//เงินสด
		param.setGuaranteeType(GuaranteeType.CASH.getStatus());
		//outbound
		param.setTraderCategory(TraderCategory.OUTBOUND.getStatus());
		param.setGuaranteeStatus(GuaranteeStatus.NORMAL.getStatus());
		List<Object[]> listAll = (List<Object[]>)this.receiptDAO.findSummaryGuaranteeReceiptAndFee(param, user);
		BigDecimal newGuaranteeCashMoney = new BigDecimal(0);
		int newGuaranteeCashOutboundCount = 0;
		if(!listAll.isEmpty())
		{
			newGuaranteeCashOutboundCount = listAll.size();
			newGuaranteeCashMoney = new BigDecimal(newGuaranteeCashOutboundCount*200000);
		}
		model.put("newGuaranteeCashOutboundCount", newGuaranteeCashOutboundCount);
		//----------------
		//รายใหม่ เงินสด Inbound 
		//ธุรกิจนำเที่ยว
		param.setTraderType(TraderType.TOUR_COMPANIES.getStatus());
		//รายใหม่
		param.setRegistrationType(RegistrationType.TOUR_COMPANIES_NEW_REGISTRATION.getStatus());
		//เงินสด
		param.setGuaranteeType(GuaranteeType.CASH.getStatus());
		//inbound
		param.setTraderCategory(TraderCategory.INBOUND.getStatus());
		param.setGuaranteeStatus(GuaranteeStatus.NORMAL.getStatus());
		List<Object[]> listAll2 = (List<Object[]>)this.receiptDAO.findSummaryGuaranteeReceiptAndFee(param, user);
		int newGuaranteeCashInboundCount = 0;
		if(!listAll2.isEmpty())
		{
			newGuaranteeCashInboundCount = listAll2.size();
			newGuaranteeCashMoney = newGuaranteeCashMoney.add(new BigDecimal(newGuaranteeCashInboundCount*100000));
		}
		model.put("newGuaranteeCashInboundCount", newGuaranteeCashInboundCount);
		//----------------
		//รายใหม่ เงินสด ในประเทศ 
		//ธุรกิจนำเที่ยว
		param.setTraderType(TraderType.TOUR_COMPANIES.getStatus());
		//รายใหม่
		param.setRegistrationType(RegistrationType.TOUR_COMPANIES_NEW_REGISTRATION.getStatus());
		//เงินสด
		param.setGuaranteeType(GuaranteeType.CASH.getStatus());
		//country
		param.setTraderCategory(TraderCategory.COUNTRY.getStatus());
		param.setGuaranteeStatus(GuaranteeStatus.NORMAL.getStatus());
		List<Object[]> listAll3 = (List<Object[]>)this.receiptDAO.findSummaryGuaranteeReceiptAndFee(param, user);
		int newGuaranteeCashCountryCount = 0;
		if(!listAll3.isEmpty())
		{		
			newGuaranteeCashCountryCount = listAll3.size();
			newGuaranteeCashMoney = newGuaranteeCashMoney.add(new BigDecimal(newGuaranteeCashCountryCount*50000));
		}
		model.put("newGuaranteeCashCountryCount", newGuaranteeCashCountryCount);
		//----------------
		//รายใหม่ เงินสด เฉพาะพื้นที่ 
		//ธุรกิจนำเที่ยว
		param.setTraderType(TraderType.TOUR_COMPANIES.getStatus());
		//รายใหม่
		param.setRegistrationType(RegistrationType.TOUR_COMPANIES_NEW_REGISTRATION.getStatus());
		//เงินสด
		param.setGuaranteeType(GuaranteeType.CASH.getStatus()); 
		//area
		param.setTraderCategory(TraderCategory.AREA.getStatus());
		param.setGuaranteeStatus(GuaranteeStatus.NORMAL.getStatus());
		List<Object[]> listAll4 = (List<Object[]>)this.receiptDAO.findSummaryGuaranteeReceiptAndFee(param, user);
		int newGuaranteeCashAreaCount = 0;
		if(!listAll4.isEmpty())
		{
			newGuaranteeCashAreaCount = listAll4.size();
			newGuaranteeCashMoney = newGuaranteeCashMoney.add(new BigDecimal(newGuaranteeCashAreaCount*10000));
		}
		model.put("newGuaranteeCashAreaCount", newGuaranteeCashAreaCount);
		//-----------------
		//รวมเงินรายใหม่ เงินสด
		model.put("newGuaranteeCashMoney",def.format(newGuaranteeCashMoney));
		
//ชำระราย2ปี เงินสด
		//ชำระราย2ปี เงินสด Outbound 
		//ธุรกิจนำเที่ยว
		param.setTraderType(TraderType.TOUR_COMPANIES.getStatus());
		//ชำระราย2ปี
		param.setRegistrationType(RegistrationType.TOUR_COMPANIES_RENEW_REGISTRATION.getStatus());
		//เงินสด
		param.setGuaranteeType(GuaranteeType.CASH.getStatus());
		//outbound
		param.setTraderCategory(TraderCategory.OUTBOUND.getStatus());
		param.setGuaranteeStatus(GuaranteeStatus.NORMAL.getStatus());
		List<Object[]> listAll5 = (List<Object[]>)this.receiptDAO.findSummaryGuaranteeReceiptAndFee(param, user);
		BigDecimal reNewGuaranteeCashMoney = new BigDecimal(0);
		int reNewGuaranteeCashOutboundCount = 0;
		if(!listAll5.isEmpty())
		{
			reNewGuaranteeCashOutboundCount = listAll5.size();
			reNewGuaranteeCashMoney = new BigDecimal(reNewGuaranteeCashOutboundCount*200000);
		}
		model.put("reNewGuaranteeCashOutboundCount", reNewGuaranteeCashOutboundCount);
		//รวมจำนวนรายใหม่และชำระที่เป็น เงินสด Outbound
		int reNewGuaranteeCashOutboundCountAll = reNewGuaranteeCashOutboundCount + newGuaranteeCashOutboundCount;
		model.put("reNewGuaranteeCashOutboundCountAll",reNewGuaranteeCashOutboundCountAll);
		//-----------------
		//ชำระราย2ปี เงินสด Inbound 
		//ธุรกิจนำเที่ยว
		param.setTraderType(TraderType.TOUR_COMPANIES.getStatus());
		//ชำระราย2ปี
		param.setRegistrationType(RegistrationType.TOUR_COMPANIES_RENEW_REGISTRATION.getStatus());
		//เงินสด
		param.setGuaranteeType(GuaranteeType.CASH.getStatus());
		//inbound
		param.setTraderCategory(TraderCategory.INBOUND.getStatus());
		param.setGuaranteeStatus(GuaranteeStatus.NORMAL.getStatus());
		List<Object[]> listAll6 = (List<Object[]>)this.receiptDAO.findSummaryGuaranteeReceiptAndFee(param, user);
		int reNewGuaranteeCashInboundCount = 0;
		if(!listAll6.isEmpty())
		{
			reNewGuaranteeCashInboundCount = listAll6.size();
			reNewGuaranteeCashMoney = reNewGuaranteeCashMoney.add(new BigDecimal(reNewGuaranteeCashInboundCount*100000));
		}
		model.put("reNewGuaranteeCashInboundCount", reNewGuaranteeCashInboundCount);
		//รวมจำนวนรายใหม่และชำระที่เป็น เงินสด Inbound
		int reNewGuaranteeCashInboundCountAll = reNewGuaranteeCashInboundCount + newGuaranteeCashInboundCount;
		model.put("reNewGuaranteeCashInboundCountAll",reNewGuaranteeCashInboundCountAll);
		//-----------------
		//ชำระราย2ปี เงินสด ในประเทศ 
		//ธุรกิจนำเที่ยว
		param.setTraderType(TraderType.TOUR_COMPANIES.getStatus());
		//ชำระราย2ปี
		param.setRegistrationType(RegistrationType.TOUR_COMPANIES_RENEW_REGISTRATION.getStatus());
		//เงินสด
		param.setGuaranteeType(GuaranteeType.CASH.getStatus());
		//country
		param.setTraderCategory(TraderCategory.COUNTRY.getStatus());
		param.setGuaranteeStatus(GuaranteeStatus.NORMAL.getStatus());
		List<Object[]> listAll7 = (List<Object[]>)this.receiptDAO.findSummaryGuaranteeReceiptAndFee(param, user);
		int reNewGuaranteeCashCountryCount = 0;
		if(!listAll7.isEmpty())
		{
			reNewGuaranteeCashCountryCount = listAll7.size();
			reNewGuaranteeCashMoney = reNewGuaranteeCashMoney.add(new BigDecimal(reNewGuaranteeCashCountryCount*50000));
		}
		model.put("reNewGuaranteeCashCountryCount", reNewGuaranteeCashCountryCount);
		//รวมจำนวนรายใหม่และชำระที่เป็น เงินสด Country
		int guaranteeCashCountryCountAll = reNewGuaranteeCashCountryCount + newGuaranteeCashCountryCount;
		model.put("guaranteeCashCountryCountAll",guaranteeCashCountryCountAll);
		//-----------------
		//ชำระราย2ปี เงินสด เฉพาะพื้นที่ 
		//ธุรกิจนำเที่ยว
		param.setTraderType(TraderType.TOUR_COMPANIES.getStatus());
		//ชำระราย2ปี
		param.setRegistrationType(RegistrationType.TOUR_COMPANIES_RENEW_REGISTRATION.getStatus());
		//เงินสด
		param.setGuaranteeType(GuaranteeType.CASH.getStatus()); 
		//area
		param.setTraderCategory(TraderCategory.AREA.getStatus());
		param.setGuaranteeStatus(GuaranteeStatus.NORMAL.getStatus());
		List<Object[]> listAll8 = (List<Object[]>)this.receiptDAO.findSummaryGuaranteeReceiptAndFee(param, user);
		int reNewGuaranteeCashAreaCount = 0;
		if(!listAll8.isEmpty())
		{
			reNewGuaranteeCashAreaCount = listAll8.size();
			reNewGuaranteeCashMoney = reNewGuaranteeCashMoney.add(new BigDecimal(reNewGuaranteeCashAreaCount*10000));
		}
		model.put("reNewGuaranteeCashAreaCount", reNewGuaranteeCashAreaCount);
		//รวมจำนวนรายใหม่และชำระที่เป็น เงินสด AREA
		int guaranteeCashAreaCountAll = reNewGuaranteeCashAreaCount + newGuaranteeCashAreaCount;
		model.put("guaranteeCashAreaCountAll",guaranteeCashAreaCountAll);
		//-----------------
		//รวมเงินชำระราย2ปี เงินสด
		model.put("reNewGuaranteeCashMoney",def.format(reNewGuaranteeCashMoney));
		
		//รวมเงินรายใหม่และชำระราย2ปี เงินสด
		BigDecimal guaranteeCashMoneyAll = reNewGuaranteeCashMoney.add(newGuaranteeCashMoney);
		model.put("guaranteeCashMoneyAll",def.format(guaranteeCashMoneyAll));
		
////////////////////////////////////////////////////////////////////////////////////////////////////
//รายใหม่ หนังสือค้ำประกัน
		//รายใหม่ เงินสด Outbound 
		//ธุรกิจนำเที่ยว
		param.setTraderType(TraderType.TOUR_COMPANIES.getStatus());
		//รายใหม่
		param.setRegistrationType(RegistrationType.TOUR_COMPANIES_NEW_REGISTRATION.getStatus());
		//หนังสือค้ำประกัน
		param.setGuaranteeType(GuaranteeType.BANK.getStatus());
		//outbound
		param.setTraderCategory(TraderCategory.OUTBOUND.getStatus());
		param.setGuaranteeStatus(GuaranteeStatus.NORMAL.getStatus());
		List<Object[]> listAll9 = (List<Object[]>)this.receiptDAO.findSummaryGuaranteeReceiptAndFee(param, user);
		BigDecimal newGuaranteeBankMoney = new BigDecimal(0);
		int newGuaranteeBankOutboundCount = 0;
		if(!listAll9.isEmpty())
		{
			newGuaranteeBankOutboundCount = listAll9.size();
			newGuaranteeBankMoney = new BigDecimal(newGuaranteeBankOutboundCount*200000);
		}
		model.put("newGuaranteeBankOutboundCount", newGuaranteeBankOutboundCount);
		//--------------------
		//รายใหม่ หนังสือค้ำประกัน Inbound 
		//ธุรกิจนำเที่ยว
		param.setTraderType(TraderType.TOUR_COMPANIES.getStatus());
		//หนังสือค้ำประกัน
		param.setRegistrationType(RegistrationType.TOUR_COMPANIES_NEW_REGISTRATION.getStatus());
		//เงินสด
		param.setGuaranteeType(GuaranteeType.BANK.getStatus());
		//inbound
		param.setTraderCategory(TraderCategory.INBOUND.getStatus());
		param.setGuaranteeStatus(GuaranteeStatus.NORMAL.getStatus());
		List<Object[]> listAll10 = (List<Object[]>)this.receiptDAO.findSummaryGuaranteeReceiptAndFee(param, user);
		int newGuaranteeBankInboundCount = 0;
		if(!listAll10.isEmpty())
		{
			newGuaranteeBankInboundCount = listAll10.size();
			newGuaranteeBankMoney = newGuaranteeBankMoney.add(new BigDecimal(newGuaranteeBankInboundCount*100000));
		}
		model.put("newGuaranteeBankInboundCount", newGuaranteeBankInboundCount);
		//----------------
		//รายใหม่ หนังสือค้ำประกัน ในประเทศ 
		//ธุรกิจนำเที่ยว
		param.setTraderType(TraderType.TOUR_COMPANIES.getStatus());
		//รายใหม่
		param.setRegistrationType(RegistrationType.TOUR_COMPANIES_NEW_REGISTRATION.getStatus());
		//หนังสือค้ำประกัน
		param.setGuaranteeType(GuaranteeType.BANK.getStatus());
		//country
		param.setTraderCategory(TraderCategory.COUNTRY.getStatus());
		param.setGuaranteeStatus(GuaranteeStatus.NORMAL.getStatus());
		List<Object[]> listAll11 = (List<Object[]>)this.receiptDAO.findSummaryGuaranteeReceiptAndFee(param, user);
		int newGuaranteeBankCountryCount = 0;
		if(!listAll11.isEmpty())
		{		
			newGuaranteeBankCountryCount = listAll11.size();
			newGuaranteeBankMoney = newGuaranteeBankMoney.add(new BigDecimal(newGuaranteeBankCountryCount*50000));
		}
		model.put("newGuaranteeBankCountryCount", newGuaranteeBankCountryCount);
		//----------------
		//รายใหม่ หนังสือค้ำประกัน เฉพาะพื้นที่ 
		//ธุรกิจนำเที่ยว
		param.setTraderType(TraderType.TOUR_COMPANIES.getStatus());
		//รายใหม่
		param.setRegistrationType(RegistrationType.TOUR_COMPANIES_NEW_REGISTRATION.getStatus());
		//หนังสือค้ำประกัน
		param.setGuaranteeType(GuaranteeType.BANK.getStatus()); 
		//area
		param.setTraderCategory(TraderCategory.AREA.getStatus());
		param.setGuaranteeStatus(GuaranteeStatus.NORMAL.getStatus());
		List<Object[]> listAll12 = (List<Object[]>)this.receiptDAO.findSummaryGuaranteeReceiptAndFee(param, user);
		int newGuaranteeBankAreaCount = 0;
		if(!listAll12.isEmpty())
		{ 
			newGuaranteeBankAreaCount = listAll12.size();
			newGuaranteeBankMoney = newGuaranteeBankMoney.add(new BigDecimal(newGuaranteeBankAreaCount*10000));
		}
		model.put("newGuaranteeBankAreaCount", newGuaranteeBankAreaCount);
		//----------------- 
		//รวมเงินรายใหม่ หนังสือค้ำประกัน
		model.put("newGuaranteeBankMoney",def.format(newGuaranteeBankMoney));
//ชำระราย2ปี หนังสือค้ำประกัน
		//ชำระราย2ปี เงินสด Outbound 
		//ธุรกิจนำเที่ยว
		param.setTraderType(TraderType.TOUR_COMPANIES.getStatus());
		//ชำระราย2ปี
		param.setRegistrationType(RegistrationType.TOUR_COMPANIES_RENEW_REGISTRATION.getStatus());
		//หนังสือค้ำประกัน
		param.setGuaranteeType(GuaranteeType.BANK.getStatus());
		//outbound
		param.setTraderCategory(TraderCategory.OUTBOUND.getStatus());
		param.setGuaranteeStatus(GuaranteeStatus.NORMAL.getStatus());
		List<Object[]> listAll13 = (List<Object[]>)this.receiptDAO.findSummaryGuaranteeReceiptAndFee(param, user);
		BigDecimal reNewGuaranteeBankMoney = new BigDecimal(0);
		int reNewGuaranteeBankOutboundCount = 0;
		if(!listAll13.isEmpty())
		{
			reNewGuaranteeBankOutboundCount = listAll13.size();
			reNewGuaranteeBankMoney = new BigDecimal(reNewGuaranteeBankOutboundCount*200000);
		}
		model.put("reNewGuaranteeBankOutboundCount", reNewGuaranteeBankOutboundCount);
		//รวมจำนวนรายใหม่และชำระ2ปีที่เป็น เงินสด Outbound
		int guaranteeBankOutboundCountAll = reNewGuaranteeBankOutboundCount + newGuaranteeBankOutboundCount;
		model.put("guaranteeBankOutboundCountAll",guaranteeBankOutboundCountAll); 
		//-----------------
		//ชำระราย2ปี หนังสือค้ำประกัน Inbound 
		//ธุรกิจนำเที่ยว
		param.setTraderType(TraderType.TOUR_COMPANIES.getStatus());
		//ชำระราย2ปี
		param.setRegistrationType(RegistrationType.TOUR_COMPANIES_RENEW_REGISTRATION.getStatus());
		//หนังสือค้ำประกัน
		param.setGuaranteeType(GuaranteeType.BANK.getStatus());
		//inbound
		param.setTraderCategory(TraderCategory.INBOUND.getStatus());
		param.setGuaranteeStatus(GuaranteeStatus.NORMAL.getStatus());
		List<Object[]> listAll14 = (List<Object[]>)this.receiptDAO.findSummaryGuaranteeReceiptAndFee(param, user);
		int reNewGuaranteeBankInboundCount = 0;
		if(!listAll14.isEmpty())
		{
			reNewGuaranteeBankInboundCount = listAll14.size();
			reNewGuaranteeBankMoney = reNewGuaranteeBankMoney.add(new BigDecimal(reNewGuaranteeBankInboundCount*100000));
		}
		model.put("reNewGuaranteeBankInboundCount", reNewGuaranteeBankInboundCount);
		//รวมจำนวนรายใหม่และชำระ2ปีที่เป็น หนังสือค้ำประกัน Inbound
		int guaranteeBankInboundCountAll = reNewGuaranteeBankInboundCount + newGuaranteeBankInboundCount;
		model.put("guaranteeBankInboundCountAll",guaranteeBankInboundCountAll);
		//-----------------
		//ชำระราย2ปี หนังสือค้ำประกัน ในประเทศ 
		//ธุรกิจนำเที่ยว
		param.setTraderType(TraderType.TOUR_COMPANIES.getStatus());
		//ชำระราย2ปี
		param.setRegistrationType(RegistrationType.TOUR_COMPANIES_RENEW_REGISTRATION.getStatus());
		//หนังสือค้ำประกัน
		param.setGuaranteeType(GuaranteeType.BANK.getStatus());
		//country
		param.setTraderCategory(TraderCategory.COUNTRY.getStatus());
		param.setGuaranteeStatus(GuaranteeStatus.NORMAL.getStatus());
		List<Object[]> listAll15 = (List<Object[]>)this.receiptDAO.findSummaryGuaranteeReceiptAndFee(param, user);
		int reNewGuaranteeBankCountryCount = 0;
		if(!listAll15.isEmpty())
		{ 
			reNewGuaranteeBankCountryCount = listAll15.size();
			reNewGuaranteeBankMoney = reNewGuaranteeBankMoney.add(new BigDecimal(reNewGuaranteeBankCountryCount*50000));
		}
		model.put("reNewGuaranteeBankCountryCount", reNewGuaranteeBankCountryCount);
		//รวมจำนวนรายใหม่และชำระ2ปีที่เป็น หนังสือค้ำประกัน Country
		int guaranteeBankCountryCountAll = reNewGuaranteeBankCountryCount + newGuaranteeBankCountryCount;
		model.put("guaranteeBankCountryCountAll",guaranteeBankCountryCountAll);
		//-----------------
		//ชำระราย2ปี หนังสือค้ำประกัน เฉพาะพื้นที่ 
		//ธุรกิจนำเที่ยว
		param.setTraderType(TraderType.TOUR_COMPANIES.getStatus());
		//ชำระราย2ปี
		param.setRegistrationType(RegistrationType.TOUR_COMPANIES_RENEW_REGISTRATION.getStatus());
		//หนังสือค้ำประกัน
		param.setGuaranteeType(GuaranteeType.BANK.getStatus()); 
		//area
		param.setTraderCategory(TraderCategory.AREA.getStatus());
		param.setGuaranteeStatus(GuaranteeStatus.NORMAL.getStatus());
		List<Object[]> listAll16 = (List<Object[]>)this.receiptDAO.findSummaryGuaranteeReceiptAndFee(param, user);
		int reNewGuaranteeBankAreaCount = 0;
		if(!listAll16.isEmpty())
		{
			reNewGuaranteeBankAreaCount = listAll16.size();
			reNewGuaranteeBankMoney = reNewGuaranteeBankMoney.add(new BigDecimal(reNewGuaranteeBankAreaCount*10000));
		}
		model.put("reNewGuaranteeBankAreaCount", reNewGuaranteeBankAreaCount);
		//รวมจำนวนรายใหม่และชำระ2ปีที่เป็น หนังสือค้ำประกัน AREA
		int guaranteeBankAreaCountAll = reNewGuaranteeBankAreaCount + newGuaranteeBankAreaCount;
		model.put("guaranteeBankAreaCountAll",guaranteeBankAreaCountAll);
		//-----------------
		//รวมเงินชำระราย2ปี หนังสือค้ำประกัน
		model.put("reNewGuaranteeBankMoney",def.format(reNewGuaranteeBankMoney));
		
		//รวมเงินรายใหม่และชำระราย2ปี หนังสือค้ำประกัน
		BigDecimal guaranteeBankMoneyAll = reNewGuaranteeBankMoney.add(newGuaranteeBankMoney);
		model.put("guaranteeBankMoneyAll",def.format(guaranteeBankMoneyAll));
				 
		return model; 
	} 
	
	//Oat Add 25/02/58
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public Object getReceiptCancle(Object object, User user) throws Exception {
		if(!(object instanceof ReceiptDTO))
		{
			throw new IllegalArgumentException("!(object instanceof ReceiptDTO)");
		}
		
		ReceiptDTO param = (ReceiptDTO) object;
		
		ReceiptDTO dto = new ReceiptDTO();
		
		Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(param.getRegId());
		Trader trader = reg.getTrader();
		   
		//พิมพ์ใบเสร็จไปแล้ว
		if(param.getReceiptId() > 0)
		{			
			dto.setRegistrationNo(reg.getRegistrationNo());
			dto.setTraderCategory(trader.getTraderCategory());
			dto.setTraderCategoryName(TraderCategory.getMeaning(trader.getTraderType(), trader.getTraderCategory()));
			dto.setTraderType(trader.getTraderType());
			dto.setRegId(reg.getRegId());
			dto.setRegistrationType(reg.getRegistrationType());
			
			Receipt receipt = (Receipt)this.receiptDAO.findByPrimaryKey(param.getReceiptId());
			ObjectUtil.copy(receipt, dto);
			
			
		}
		else
		{
			dto = (ReceiptDTO)this.getPayersCancle(param, user);
			dto.setReceiveOfficerName(user.getUserData().getUserFullName());
			dto.setAuthority(param.getAuthority());
		}
		
		ObjectUtil.copy(reg, dto);
		ObjectUtil.copy(trader, dto);
		
		if(TraderType.TOUR_COMPANIES.getStatus().equals(trader.getTraderType()))
		{
			// get guarantee
			this.getGuarantee(dto, user);		
		}
		
		return dto;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public Object getPayersCancle(Object object, User user) throws Exception {
		
		if(!(object instanceof ReceiptDTO))
		{
			throw new IllegalArgumentException("!(object instanceof ReceiptDTO)");
		}
		
		ReceiptDTO param = (ReceiptDTO) object;
		
		Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(param.getRegId());
		Trader trader = reg.getTrader();
		Person person = trader.getPerson();
		
		ReceiptDTO dto = new ReceiptDTO();
		

		String bookNo = this.masRunningNoDAO.getTempAccountCancleBookNo(trader.getOrganization().getOrgId(), trader.getTraderType());
		String receiptNo = this.masRunningNoDAO.getTempAccountCancleReceiptNo(trader.getOrganization().getOrgId(), trader.getTraderType());
		
		dto.setBookNo(bookNo);
		dto.setReceiptNo(receiptNo);
		dto.setRegistrationNo(reg.getRegistrationNo());
		dto.setTraderCategory(trader.getTraderCategory());
		dto.setTraderCategoryName(TraderCategory.getMeaning(trader.getTraderType(), trader.getTraderCategory()));
		dto.setTraderType(trader.getTraderType());
		
		if(PersonType.CORPORATE.getStatus().equals(person.getPersonType()))
		{
			String prefixName = person.getMasPrefix()==null?"":person.getMasPrefix().getPrefixName();
			String postfixName = person.getMasPosfix()==null?"":person.getMasPosfix().getPostfixName();
			String traderOwnerName = prefixName + person.getFirstName() + postfixName;
			dto.setReceiptName(traderOwnerName);
		}
		else
		{
			String prefixName = person.getMasPrefix()==null?"":person.getMasPrefix().getPrefixName();
//			String postfixName = person.getMasPosfix()==null?"":person.getMasPosfix().getPostfixName();
			String traderOwnerName = prefixName + person.getFirstName() + " " + person.getLastName();
			dto.setReceiptName(traderOwnerName);
		}
		
		
		
		return dto;
	}
	//End Oat Add 25/02/58

	//Oat Add 26/02/58
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveReceiptAccountCancle(Object obj, Object[] object, User user)throws Exception {
		if(!(obj instanceof ReceiptDTO))
		{
			throw new IllegalArgumentException("!(object instanceof ReceiptDTO)");
		}
		
		ReceiptDTO receiptDTO = (ReceiptDTO)obj;
		
		if(receiptDTO.getReceiptId() > 0)
		{
			this.updateReceiptAccountCancle( obj,  object,  user);
		}
		else
		{
			this.createReceiptAccountCancle( obj,  object,  user);
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	private void createReceiptAccountCancle(Object obj, Object[] object, User user)throws Exception
	{
		ReceiptDTO receiptDTO = (ReceiptDTO) obj;
		
		Registration reg = (Registration) this.registrationDAO.findByPrimaryKey(receiptDTO.getRegId());
		Trader trader = reg.getTrader();
		
		String bookNo = this.masRunningNoDAO.getAccountCancleBookNo(trader.getOrganization().getOrgId(), trader.getTraderType());
		String receiptNo = this.masRunningNoDAO.getAccountCancleReceiptNo(trader.getOrganization().getOrgId(), trader.getTraderType());
		
		Receipt receipt = new Receipt();
		ObjectUtil.copy(receiptDTO, receipt);
		
		receipt.setBookNo(bookNo);
		receipt.setReceiptNo(receiptNo);
		receipt.setReceiptStatus(ReceiptStatus.PAY.getStatus());
		receipt.setReceiptRemark("ชำระเงิน");
		receipt.setRecordStatus(RecordStatus.NORMAL.getStatus());
		receipt.setCreateUser(user.getUserName());
		receipt.setCreateDtm(new Date());
		
		Long receiptId = (Long)this.receiptDAO.insert(receipt);
		receipt.setReceiptId(receiptId);
		receiptDTO.setReceiptId(receiptId);
		
		ReceiptMapRegistration mp = new ReceiptMapRegistration();
		mp.setReceipt(receipt);
		mp.setRegistration(reg);
		mp.setReceiptMapDate(receipt.getReceiveOfficerDate());
		
		
		this.receiptMapRegistrationDAO.insert(mp);
		
		
		this.updateReceiptDetail(object, receipt, user);
		
//		// approve
//		RegisterProgressDTO param = new RegisterProgressDTO();
//		param.setRegId(reg.getRegId());
//		
//		this.approveRegistrationService.approve(reg, user);
//		
//		this.verification(param, user);
		

//		boolean isTourCompanies = TraderType.TOUR_COMPANIES.getStatus().equals(trader.getTraderType());
//		if(isTourCompanies)
//		{
//			//add new Guarantee
//			GuaranteeDTO guaranteeDTO = new GuaranteeDTO();
//			
//			ObjectUtil.copy(receiptDTO, guaranteeDTO);
//			
//			guaranteeDTO.setLicenseNo(trader.getLicenseNo());
//			guaranteeDTO.setTraderType(trader.getTraderType());
//			
//			this.saveGuaranteeAccount(guaranteeDTO, user);
//		}
		

	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	private void updateReceiptAccountCancle(Object obj, Object[] object, User user)throws Exception
	{
		ReceiptDTO param = (ReceiptDTO)obj;
		Receipt receipt = (Receipt) this.receiptDAO.findByPrimaryKey(param.getReceiptId());
		ObjectUtil.copy(param, receipt);
		
		receipt.setLastUpdUser(user.getUserName());
		receipt.setLastUpdDtm(new Date());
		
		this.receiptDAO.update(receipt);
		
		this.updateReceiptDetail(object, receipt, user);
		

//		Registration reg = (Registration) this.registrationDAO.findByPrimaryKey(param.getRegId());
//		Trader trader = reg.getTrader();
//		
//		boolean isTourCompanies = TraderType.TOUR_COMPANIES.getStatus().equals(param.getTraderType());
//		if(isTourCompanies)
//		{
//			
//			//update guarantee change and insert
//			GuaranteeDTO guaranteeDTO = new GuaranteeDTO();
//			ObjectUtil.copy(param, guaranteeDTO);
//			
//			guaranteeDTO.setLicenseNo(trader.getLicenseNo());
//			guaranteeDTO.setTraderType(trader.getTraderType());
//			
//			List<Guarantee> listGuarantee =  (List<Guarantee>)this.guaranteeDAO.findByTrader(trader.getLicenseNo(), trader.getTraderType(), 0);
//						
//			if(!listGuarantee.isEmpty())
//			{
//				Guarantee guarantee = listGuarantee.get(0);
//				guaranteeDTO.setGuaranteeId(guarantee.getGuaranteeId());
//			}
//			
//			this.saveGuaranteeAccount(guaranteeDTO, user);
//		}		
		
	}
	//End Oat Add 26/02/58

	private  BigDecimal calculateNumberOfMonths(Date currentDate, Date referDate) throws Exception {
		  int years = 0;
	      int months = 0;
	      int days = 0;
	      
	      //create calendar object for birth day
	      Calendar referDay = Calendar.getInstance();
	      referDay.setTimeInMillis(referDate.getTime());
	      
	      Calendar now = Calendar.getInstance();
	      now.setTimeInMillis(currentDate.getTime());
	      //Get difference between years
	      years = now.get(Calendar.YEAR) - referDay.get(Calendar.YEAR);
	      int currMonth = now.get(Calendar.MONTH) + 1;
	      int referMonth = referDay.get(Calendar.MONTH) + 1;
	      //Get difference between months
	      months = (currMonth - referMonth);
	      
	      int m1 = now.get(Calendar.YEAR) * 12 + now.get(Calendar.MONTH);
	      int m2 = referDay.get(Calendar.YEAR) * 12 + referDay.get(Calendar.MONTH);
	      int numOfmonth = m1-m2;
	      
	      System.out.println("referDate = "+ referDay.getTime());
	      System.out.println("now = "+ now.getTime());
	      
	      System.out.println(String.format("numOfmonth = %s m1 = %s m2 = %s", numOfmonth, m1, m2));
	      System.out.println("now = " + now.get(Calendar.DATE));
	      System.out.println("referDay = " + referDay.get(Calendar.DATE));
	      
	      if(now.get(Calendar.DATE) > referDay.get(Calendar.DATE)){
	    	  numOfmonth++;
		  }
	      System.out.println("cal numOfmonth: "+numOfmonth);
	      BigDecimal numMonth = new BigDecimal(numOfmonth<0?0:numOfmonth); 
	      System.out.println("numOfmonth :" + numMonth.intValue());
	      
	      return numMonth;
	}
	
	public  BigDecimal calculateInterest(BigDecimal fee, BigDecimal interestRate, Date currentDate, Date referDate) throws Exception{
		
		System.out.println(String.format("Current Date is %s", currentDate));
		System.out.println(String.format("Refer Date is %s", referDate));

		if(currentDate.before(referDate) || currentDate.equals(referDate)){
			System.out.println("Current date is before or equal refer date then return Zero");
			return BigDecimal.ZERO;
		}else {
			
			System.out.println(String.format("Refer Date after shiftHoliday is %s", referDate));
			
			// cal month number
			BigDecimal numberOfMonths = calculateNumberOfMonths(currentDate, referDate);
			if(numberOfMonths.compareTo(BigDecimal.ZERO) <= 0){
				System.out.println("Number of month is less or equal than Zero then return Zero");
				return BigDecimal.ZERO;
			}
			System.out.println("fee : " + fee);
			BigDecimal interestAmount = fee.multiply(interestRate);
			System.out.println("interest * "+interestRate+" = "+interestAmount);

			interestAmount = interestAmount.divide(new BigDecimal(1),2, BigDecimal.ROUND_HALF_UP);
			interestAmount = interestAmount.multiply(numberOfMonths);
			System.out.println("interest * "+numberOfMonths+" = "+interestAmount);
			System.out.println(String.format("Number of month is %s then return %s", numberOfMonths, interestAmount));

			return interestAmount; 
		}					
	}

	@Override
	public void updateFeePaid(Object object, User user) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("!(object instanceof ReceiptDTO)");
		}
		
		RegistrationDTO param = (RegistrationDTO) object;
		DotTrFeePayment dotTrFeePayment = (DotTrFeePayment)this.dotTrFeePaymentDAO.findByPrimaryKey(param.getPaymentRecId());
		
		BigDecimal feePaid = dotTrFeePayment.getTotalFeePaid();
		feePaid = feePaid.add(param.getDiffFee());
		dotTrFeePayment.setTotalFeePaid(feePaid);
		dotTrFeePayment.setLastUpdUser(user.getUserName());
		dotTrFeePayment.setLastUpdDtm(new Date());
		dotTrFeePayment.setPaymentDate(new Date());
		
		this.dotTrFeePaymentDAO.update(dotTrFeePayment);
		
	}


// --------------------------------------------------------------------------------
// payment online
// --------------------------------------------------------------------------------
	@Override
	public List getPaymentOnlineForPrintReceipt(Object object, User user)
			throws Exception {
		return this.receiptDAO.findPaymentOnlineForPrintReceipt((ReceiptDTO)object);
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public Object getPaymentOnlineDetail(Object object, User user) throws Exception {
		if(!(object instanceof ReceiptDTO))
		{
			throw new IllegalArgumentException("!(object instanceof ReceiptDTO)");
		}
		
		ReceiptDTO param = (ReceiptDTO) object;
		Receipt receipt = (Receipt)this.receiptDAO.findByPrimaryKey(param.getReceiptId());
		
		ReceiptDTO dto = new ReceiptDTO();
		
		Registration reg = (Registration)this.receiptDAO.findRegistrationByReceiptId(param.getReceiptId());
		if(null != reg){
			Trader trader = reg.getTrader();
			   
			dto.setRegistrationNo(reg.getRegistrationNo());
			dto.setTraderCategory(trader.getTraderCategory());
			dto.setTraderCategoryName(TraderCategory.getMeaning(trader.getTraderType(), trader.getTraderCategory()));
			dto.setTraderType(trader.getTraderType());
			dto.setRegId(reg.getRegId());
			dto.setRegistrationType(reg.getRegistrationType());
			ObjectUtil.copy(receipt, dto);
			
			
			ObjectUtil.copy(reg, dto);
			ObjectUtil.copy(trader, dto);
			
			if(TraderType.TOUR_COMPANIES.getStatus().equals(trader.getTraderType()))
			{
				// get guarantee
				this.getGuarantee(dto, user);		
			}
		}
		return dto;
	}
	@Override
	public List getPaymentDetailFee(Object object, User user) throws Exception {
		ReceiptDetailDTO param = (ReceiptDetailDTO)object;
		return this.receiptDAO.findPaymentDetail(param.getReceiptId());
	}



	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void savePayment(Object obj,Object[] object, User user) throws Exception
	{
		ReceiptDTO param = (ReceiptDTO)obj;
		Receipt receipt = (Receipt) this.receiptDAO.findByPrimaryKey(param.getReceiptId());
		ObjectUtil.copy(param, receipt);
		
		receipt.setReceiptStatus(ReceiptStatus.PAY.getStatus());
		receipt.setReceiptRemark("ชำระเงิน");
		receipt.setRecordStatus(RecordStatus.NORMAL.getStatus());
		receipt.setLastUpdUser(user.getUserName());
		receipt.setLastUpdDtm(new Date());
		
		this.receiptDAO.update(receipt);
		
		this.updateReceiptDetail(object, receipt, user);
		
		Registration reg = (Registration)this.receiptDAO.findRegistrationByReceiptId(param.getReceiptId());
		Trader trader = reg.getTrader();
		
		this.approveRegistrationService.approve(reg, user);
		// approve
		RegisterProgressDTO progress = new RegisterProgressDTO();
		progress.setRegId(reg.getRegId());
		this.verification(progress, user);
		
		boolean isTourCompanies = TraderType.TOUR_COMPANIES.getStatus().equals(param.getTraderType());
		if(isTourCompanies)
		{
			
			//update guarantee change and insert
			GuaranteeDTO guaranteeDTO = new GuaranteeDTO();
			ObjectUtil.copy(param, guaranteeDTO);
			
			guaranteeDTO.setLicenseNo(trader.getLicenseNo());
			guaranteeDTO.setTraderType(trader.getTraderType());
			
			List<Guarantee> listGuarantee =  (List<Guarantee>)this.guaranteeDAO.findByTrader(trader.getLicenseNo(), trader.getTraderType(), 0);
						
			if(!listGuarantee.isEmpty())
			{
				Guarantee guarantee = listGuarantee.get(0);
				guaranteeDTO.setGuaranteeId(guarantee.getGuaranteeId());
			}
			
			this.saveGuaranteeAccount(guaranteeDTO, user);
		}		
		
	}
// --------------------------------------------------------------------------------
// end payment online
// --------------------------------------------------------------------------------	
}
















