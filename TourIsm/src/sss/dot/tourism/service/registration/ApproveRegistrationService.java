package sss.dot.tourism.service.registration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.CountryTourDAO;
import sss.dot.tourism.dao.DotLicOwnerCommitteeDAO;
import sss.dot.tourism.dao.DotLicOwnerEduBackgroundDAO;
import sss.dot.tourism.dao.DotLicOwnerLanguageDAO;
import sss.dot.tourism.dao.DotLicenseDAO;
import sss.dot.tourism.dao.DotLicenseOwnerDAO;
import sss.dot.tourism.dao.DotMsLanguageDAO;
import sss.dot.tourism.dao.DotSubLicenseDAO;
import sss.dot.tourism.dao.DotTrBillDetailDAO;
import sss.dot.tourism.dao.DotTrBillPaymentDAO;
import sss.dot.tourism.dao.DotTrFeeLicenseDAO;
import sss.dot.tourism.dao.DotTrFeePaymentDAO;
import sss.dot.tourism.dao.mas.ForeignLanguageDAO;
import sss.dot.tourism.dao.mas.MasRunningNoDAO;
import sss.dot.tourism.dao.mas.RegisterFlowDAO;
import sss.dot.tourism.dao.registration.BranchDAO;
import sss.dot.tourism.dao.registration.CommitteeDAO;
import sss.dot.tourism.dao.registration.GuaranteeDAO;
import sss.dot.tourism.dao.registration.PersonDAO;
import sss.dot.tourism.dao.registration.PlantripDAO;
import sss.dot.tourism.dao.registration.PrintingCardDAO;
import sss.dot.tourism.dao.registration.PrintingLicenseDAO;
import sss.dot.tourism.dao.registration.RegisterProgressDAO;
import sss.dot.tourism.dao.registration.RegistrationDAO;
import sss.dot.tourism.dao.registration.TraderAddressDAO;
import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.dao.training.EducationDAO;
import sss.dot.tourism.domain.Committee;
import sss.dot.tourism.domain.Country;
import sss.dot.tourism.domain.CountryTour;
import sss.dot.tourism.domain.DotLicOwnerCommittee;
import sss.dot.tourism.domain.DotLicOwnerEduBackground;
import sss.dot.tourism.domain.DotLicOwnerLanguage;
import sss.dot.tourism.domain.DotLicense;
import sss.dot.tourism.domain.DotLicenseOwner;
import sss.dot.tourism.domain.DotMsLanguage;
import sss.dot.tourism.domain.DotSubLicense;
import sss.dot.tourism.domain.DotTrFeeLicense;
import sss.dot.tourism.domain.Education;
import sss.dot.tourism.domain.ForeignLanguage;
import sss.dot.tourism.domain.Organization;
import sss.dot.tourism.domain.Person;
import sss.dot.tourism.domain.Plantrip;
import sss.dot.tourism.domain.PrintingCard;
import sss.dot.tourism.domain.PrintingLicense;
import sss.dot.tourism.domain.Registration;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.domain.TraderAddress;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.service.printcard.IPrintCardService;
import sss.dot.tourism.service.punishment.ISuspensionService;
import sss.dot.tourism.service.trader.ITourCompaniesService;
import sss.dot.tourism.service.trader.ITourLeaderService;
import sss.dot.tourism.service.trader.ITraderGuideService;
import sss.dot.tourism.util.ConstantUtil;
import sss.dot.tourism.util.LicenseFeeStatus;
import sss.dot.tourism.util.LicenseStatus;
import sss.dot.tourism.util.ProgressStatus;
import sss.dot.tourism.util.RecordStatus;
import sss.dot.tourism.util.RegistrationType;
import sss.dot.tourism.util.RoleStatus;
import sss.dot.tourism.util.TraderAddressType;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.User;

@Repository("approveRegistrationService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ApproveRegistrationService implements IApproveRegistration{


	@Autowired
	MasRunningNoDAO masRunningNoDAO;
	@Autowired
	RegistrationDAO registrationDAO;
	@Autowired
	TraderDAO traderDAO;
	@Autowired
	RegisterProgressDAO registerProgressDAO;
	@Autowired
	PersonDAO personDAO;
	@Autowired
	ITourCompaniesService tourCompaniesService;
	@Autowired
	TraderAddressDAO traderAddressDAO;
	@Autowired
	PlantripDAO plantripDAO;
	@Autowired
	ITourLeaderService tourLeaderService;
	@Autowired
	RegisterFlowDAO registerFlowDAO;
	@Autowired
	ITraderGuideService guideService;
	@Autowired
	CommitteeDAO committeeDAO;
	@Autowired
	BranchDAO branchDAO;
	@Autowired
	EducationDAO educationDAO;
	@Autowired
	ForeignLanguageDAO foreignLanguageDAO;
	
	@Autowired
	PrintingLicenseDAO printingLicenseDAO;
	@Autowired
	PrintingCardDAO printingCardDAO;
	
	@Autowired
	ISuspensionService suspensionService;
	
	@Autowired
	IPrintCardService printCardService;
	@Autowired
	GuaranteeDAO guaranteeDAO;
	
	@Autowired private CountryTourDAO countryTourDAO;
	@Autowired private DotLicenseDAO dotLicenseDAO;
	@Autowired private DotLicenseOwnerDAO dotLicenseOwnerDAO;
	@Autowired private DotLicOwnerCommitteeDAO dotLicOwnerCommitteeDAO;
	@Autowired private DotLicOwnerEduBackgroundDAO dotLicOwnerEduBackgroundDAO;
	@Autowired private DotLicOwnerLanguageDAO dotLicOwnerLanguageDAO;
	@Autowired private DotMsLanguageDAO dotMsLanguageDAO;
	@Autowired private DotSubLicenseDAO dotSubLicenseDAO;
	@Autowired private DotTrBillDetailDAO dotTrBillDetailDAO;
	@Autowired private DotTrBillPaymentDAO dotTrBillPaymentDAO;
	@Autowired private DotTrFeeLicenseDAO dotTrFeeLicenseDAO;
	@Autowired private DotTrFeePaymentDAO dotTrFeePaymentDAO;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void approve(Object object, User user) throws Exception {
		
		if(!(object instanceof Registration))
		{
			throw new IllegalArgumentException("ไม่สามารถอนุมัติการจดทะเบียนได้");
		}
		
		Registration reg = (Registration)object;
		Trader trader = reg.getTrader();
		
		if(!RegistrationType.TOUR_COMPANIES_NEW_REGISTRATION.getStatus().equals(reg.getRegistrationType()))
		{
//			this.updateTraderToHistory(trader.getLicenseNo(), trader.getTraderType(), trader.getOrganization().getOrgId(), user);
			//เปลี่ยน org ใหม่
			this.updateTraderToHistory(trader.getLicenseNo(), trader.getTraderType(), 0, user);
		}
		
				

		
		//ชำระค่าธรรมเนียมรายสองปี ให้ยกเลิกการพักใช้
		if(RegistrationType.TOUR_COMPANIES_RENEW_REGISTRATION.getStatus().equals(reg.getRegistrationType()))
		{
			this.suspensionService.deactivateSuspend(trader, user);
		}
					
		//update registration 
//		Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(params.getRegId());
		
		reg.setApproveStatus(ProgressStatus.ACCEPT.getStatus());
		reg.setApproveDate(new Date());
		reg.setRecordStatus(RecordStatus.NORMAL.getStatus());
		reg.setRegistrationProgress(RoleStatus.PRINT_LICENSE.getStatus());
		reg.setLastUpdUser(user.getUserName());
		reg.setLastUpdDtm(new Date());
		
		this.registrationDAO.update(reg);
		

		//กรณีที่จดใหม่
		if(RegistrationType.TOUR_COMPANIES_NEW_REGISTRATION.getStatus().equals(reg.getRegistrationType()))
		{

			String licenseNo =  this.masRunningNoDAO.getLicenseNo(reg.getOrganization().getOrgId(), trader.getTraderType(), trader.getTraderCategory());
			System.out.println("licenseNo: "+licenseNo);
			trader.setLicenseNo(licenseNo);
			
			Calendar effectiveDate = Calendar.getInstance();
			trader.setEffectiveDate(effectiveDate.getTime());
			Calendar expireDate = Calendar.getInstance();
			expireDate.setTime(trader.getEffectiveDate());	
			// กรณีจดใหม่ และต่ออายุ
			if(TraderType.TOUR_COMPANIES.equals(trader.getTraderType()))
			{
				expireDate.add(Calendar.DATE, -1);
				expireDate.add(Calendar.YEAR, 2);
				trader.setExpireDate(expireDate.getTime());
			}
			if(TraderType.GUIDE.equals(trader.getTraderType()))
			{
				expireDate.add(Calendar.DATE, -1);
				expireDate.add(Calendar.YEAR, 5);
				trader.setExpireDate(expireDate.getTime());
			}
		}
		
		if(TraderType.GUIDE.getStatus().equals(trader.getTraderType()))
		{
			Calendar effectiveDate = Calendar.getInstance();
			effectiveDate.setTime(trader.getEffectiveDate());
			
			Calendar expireDate = Calendar.getInstance();
			expireDate.setTime(trader.getExpireDate());
			
			if(RegistrationType.GUIDE_RENEW_REGISTRATION.getStatus().equals(reg.getRegistrationType()))
			{
//				Date day = new Date();
//				effectiveDate.setTime(day);
//				expireDate.setTime(day);
//				expireDate.add(Calendar.YEAR, 5);
//				expireDate.add(Calendar.DATE, -1);
				
//				effectiveDate.set(Calendar.YEAR, expireDate.get(Calendar.YEAR));
//				expireDate.add(Calendar.YEAR, 5);
				
//				expireDate.add(Calendar.DATE, 1);
//				
//				Date newEffectiveDate = expireDate.getTime();
//				expireDate.add(Calendar.YEAR, 5);
//				Date newExpireDate = expireDate.getTime();
				
				trader.setEffectiveDate(effectiveDate.getTime());
				trader.setExpireDate(expireDate.getTime());
			}
			
			if(RegistrationType.GUIDE_TEMPORARY_LICENSE.getStatus().equals(reg.getRegistrationType()))
			{
				trader.setEffectiveDate(new Date());
				trader.setExpireDate(trader.getExpireDate());
			}
		}
		
		
		
		trader.setLicenseStatus(RecordStatus.NORMAL.getStatus());

		// tourleader ไม่ set ExpireDate
		trader.setRecordStatus(RecordStatus.NORMAL.getStatus());
		trader.setLastUpdUser(user.getUserName());
		trader.setLastUpdDtm(new Date());
		
		this.traderDAO.update(trader);
		
		Person person = trader.getPerson();
		
		person.setPersonStatus(LicenseStatus.NORMAL.getStatus());
		person.setRecordStatus(RecordStatus.NORMAL.getStatus());
		person.setLastUpdUser(user.getUserName());
		person.setLastUpdDtm(new Date());

		this.personDAO.update(person);
		

		
		// trader addres
		List<TraderAddress> listAddress = (List<TraderAddress>) this.traderAddressDAO.findAllByTrader(trader.getTraderId(), null, RecordStatus.TEMP.getStatus());
		if(!listAddress.isEmpty())
		{
			for(TraderAddress address: listAddress)
			{
				address.setRecordStatus(RecordStatus.NORMAL.getStatus());
				address.setLastUpdUser(user.getUserName());
				address.setLastUpdDtm(new Date());
				this.traderAddressDAO.update(address);
			}
		}
		//Branch
		List<Trader> listBranch = (List<Trader>) this.branchDAO.findAllByBranchParent(trader.getTraderId(), RecordStatus.TEMP.getStatus());
		if(!listBranch.isEmpty())
		{
			for(Trader branch: listBranch)
			{
				// กรณีสาขา print สาขาด้วย
				PrintingLicense pBranchLicense = new PrintingLicense();
				pBranchLicense.setRegistration(reg);
				pBranchLicense.setTrader(branch);
				pBranchLicense.setPrintLicenseStatus("W");
				this.printingLicenseDAO.insert(pBranchLicense);
				
				branch.setLicenseNo(trader.getLicenseNo());
				branch.setEffectiveDate(trader.getEffectiveDate());
				branch.setExpireDate(trader.getExpireDate());
				
				if(RegistrationType.TOUR_COMPANIES_ADD_NEW_BRANCH.getStatus().equals(branch.getAddNewBranch()))
				{
//					branch.setEffectiveDate(new Date());
				}
				
				branch.setLicenseStatus(RecordStatus.NORMAL.getStatus());
				branch.setRecordStatus(RecordStatus.NORMAL.getStatus());
				branch.setLastUpdUser(user.getUserName());
				
				branch.setLastUpdDtm(new Date());
				
				this.branchDAO.update(branch);
				
				List<TraderAddress> listBranchAddress = (List<TraderAddress>) this.traderAddressDAO.findAllByTrader(branch.getTraderId(), null, RecordStatus.TEMP.getStatus());
				if(!listBranchAddress.isEmpty())
				{
					for(TraderAddress branchAddress: listBranchAddress)
					{
						branchAddress.setRecordStatus(RecordStatus.NORMAL.getStatus());
						branchAddress.setLastUpdUser(user.getUserName());
						branchAddress.setLastUpdDtm(new Date());
						this.traderAddressDAO.update(branchAddress);
					}
				}
			}
		}	
		// plantrip
		List<Plantrip> listPlanTrip = (List<Plantrip>) this.plantripDAO.findPlanTripbyTrader(trader.getTraderId(), RecordStatus.TEMP.getStatus());
		if(!listPlanTrip.isEmpty())
		{
			for(Plantrip plantrip: listPlanTrip)
			{
				plantrip.setRecordStatus(RecordStatus.NORMAL.getStatus());
				plantrip.setLastUpdUser(user.getUserName());
				plantrip.setLastUpdDtm(new Date());
				this.plantripDAO.update(plantrip);
			}
		}
		//committee
		List<Committee> listCommittee =  (List<Committee>) this.committeeDAO.findAllByPerson(person.getPersonId(), null, RecordStatus.TEMP.getStatus());
		if(!listCommittee.isEmpty())
		{
			for(Committee comm: listCommittee)
			{
				comm.setCommitteeStatus(LicenseStatus.NORMAL.getStatus());
				comm.setRecordStatus(RecordStatus.NORMAL.getStatus());
				comm.setLastUpdUser(user.getUserName());
				comm.setLastUpdDtm(new Date());
				this.committeeDAO.update(comm);
			}
		}
		//education and traning
		List<Education> listEducation = (List<Education>) this.educationDAO.findEducationByPerson(person.getPersonId(), RecordStatus.TEMP.getStatus(), null);
		if(!listEducation.isEmpty())
		{
			for(Education edu: listEducation)
			{
				edu.setRecordStatus(RecordStatus.NORMAL.getStatus());
				edu.setLastUpdUser(user.getUserName());
				edu.setLastUpdDtm(new Date());
				this.educationDAO.update(edu);
			}
		}
		//ภาษา
		List<ForeignLanguage> listLang = (List<ForeignLanguage>) this.foreignLanguageDAO.findForeignLanguageByPerson(person.getPersonId(), RecordStatus.TEMP.getStatus());
		if(!listLang.isEmpty())
		{
			for(ForeignLanguage lang: listLang)
			{
				lang.setRecordStatus(RecordStatus.NORMAL.getStatus());
				lang.setLastUpdUser(user.getUserName());
				lang.setLastUpdDtm(new Date());
				this.foreignLanguageDAO.update(lang);
			}
		}
		
		//save to print license
		PrintingLicense pLicense = new PrintingLicense();
		pLicense.setRegistration(reg);
		pLicense.setTrader(reg.getTrader());
		pLicense.setPrintLicenseStatus("W");
		this.printingLicenseDAO.insert(pLicense);
		
		
		if(!TraderType.TOUR_COMPANIES.getStatus().equals(reg.getTrader().getTraderType()))
		{
			//save to print card
			PrintingCard pCard = new PrintingCard();
			pCard.setRegistration(reg);
			pCard.setTrader(reg.getTrader());
			pCard.setPrintCardStatus("W");
			Integer printCardId =  (Integer)this.printingCardDAO.insert(pCard);
			
			String jsonPicFileName = person.getIdentityNo() + "_" + trader.getTraderType() + "_" + trader.getTraderCategory()+".json";
			
			try{
				
				JSONParser parser = new JSONParser();
				//get image data
				String imageData = "";
				Object obj = parser.parse(new FileReader(ConstantUtil.TEMPLATE_GUIDE_FILE_LOCATION + "picjson" + java.io.File.separator + jsonPicFileName));
				 
				JSONObject jsonObject = (JSONObject) obj;
		 
				imageData = (String) jsonObject.get("imageData");
				
				System.out.println(imageData);
				
				RegistrationDTO params = new RegistrationDTO();
				params.setPrintCardId(new Long(printCardId));
			
				String replace = "data:image/jpeg;base64,";
				if(imageData.matches("(?i).*image/png.*")){
					replace = "data:image/png;base64,";
					params.setExtension("png");
				} else if(imageData.matches("(?i).*image/jpg.*")){
					replace = "data:image/jpg;base64,";
					params.setExtension("jpg");
				} else if(imageData.matches("(?i).*image/jpeg.*")){
					replace = "data:image/jpeg;base64,";
					params.setExtension("jpeg");
				}
							
				params.setImgData(imageData.replace(replace, ""));
				this.printCardService.saveCardPhoto(params, user);
				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ParseException e) {
				e.printStackTrace();
			} finally {
				try{
					 
		    		File file = new File(ConstantUtil.TEMPLATE_GUIDE_FILE_LOCATION + "picjson" + java.io.File.separator + jsonPicFileName);
		 
		    		if(file.delete()){
		    			System.out.println(file.getName() + " is deleted!");
		    		}else{
		    			System.out.println("Delete operation is failed.");
		    		}
		 
		    	}catch(Exception e){
		 
		    		e.printStackTrace();
		 
		    	}
			}
		}
		
		DotLicenseOwner owner = (DotLicenseOwner) this.dotLicenseOwnerDAO.findByPrimaryKey(person.getIdentityNo());
		if(null != owner){
			initOwner(owner,trader, person, user);
			initOwnerAddress(owner, trader.getTraderId());
			initCommittee(owner, listCommittee, user);
			initLanguage(owner , listLang, user);
			
		
			DotLicense license = this.dotLicenseDAO.findByLicenseNo(trader.getLicenseNo());
			
			//กรณีที่ใบอนุญาตจดทะเบียน online update ใบอนุญาตนั้น
			System.out.println("update ข้อมูลผู้รับใบอนุญาต REG_ID = " + reg.getRegId());
			//update ข้อมูลผู้รับใบอนุญาต
			List<DotTrFeeLicense> listTrFee = (List<DotTrFeeLicense>)this.dotTrFeeLicenseDAO.findTrFeeLicenseByRegId(reg.getRegId());
			if(CollectionUtils.isNotEmpty(listTrFee)){
				
				DotTrFeeLicense trFee = listTrFee.get(0);
				
				license = trFee.getDotLicense();
				
				license.setLastUpdUser(user.getUserName());
				license.setLastUpdDtm(new Date());
				license.setDotLicenseOwner(owner);
				
				if(StringUtils.isNotEmpty(trader.getLicenseNo())){
					license.setLicenseNo(trader.getLicenseNo());
				}
				if(StringUtils.isNotEmpty(trader.getTraderType())){
					license.setLicenseType(trader.getTraderType());
				}
				if(StringUtils.isNotEmpty(trader.getTraderCategory())){
					license.setLicenseCategory(trader.getTraderCategory());
				}
				if(StringUtils.isNotEmpty(trader.getLicenseStatus())){
					license.setLicenseStatus(trader.getLicenseStatus());
				}
				if(StringUtils.isNotEmpty(trader.getLicenseGuideNo())){
					license.setLicenseGuideNo(trader.getLicenseGuideNo());
				}
				if(null != trader.getEffectiveDate()){
					license.setEffectiveDate(trader.getEffectiveDate());
				}
				if(null != trader.getExpireDate()){
					license.setExpireDate(trader.getExpireDate());
				}
				if(StringUtils.isNotEmpty(trader.getTraderName())){
					license.setLicenseName(trader.getTraderName());
				}
				if(StringUtils.isNotEmpty(trader.getTraderNameEn())){
					license.setLicenseNameEn(trader.getTraderNameEn());
				}
				if(StringUtils.isNotEmpty(trader.getPronunciationName())){
					license.setPronunciationName(trader.getPronunciationName());
				}
				if(null != trader.getMasProvince()){
					Hibernate.initialize(trader.getMasProvince());
					license.setLicenseServiceProvince(trader.getMasProvince());
				}
				if(StringUtils.isNotEmpty(trader.getTraderArea())){
					license.setLicenseServiceArea(trader.getTraderArea());
				}
				initEducation(owner, license, listEducation , user);
				initShopAddress(license, trader.getTraderId(), user);
				initContryTour(license, listPlanTrip,user);
				initSubLicense(license , listBranch, trader.getTraderId(), user);
				
				this.dotLicenseDAO.update(license);
				
				System.out.println("update LicenseFeeStatus = FINISH ");
				trFee.setLicenseFeeStatus(LicenseFeeStatus.FINISH.getStatus());
				trFee.setLicenseNo(trader.getLicenseNo());
				trFee.setLastUpdUser(user.getUserName());
				trFee.setLastUpdDtm(new Date());
				this.dotTrFeeLicenseDAO.update(trFee);
				
			}else{
				//กรณีที่เคยจดใบอนุญาตแล้ว แต่ไม่ได้จด online update ใบอนุญาตฝั่ง online ด้วย
				if(null != license){
					license.setLastUpdUser(user.getUserName());
					license.setLastUpdDtm(new Date());
					license.setDotLicenseOwner(owner);
					
					if(StringUtils.isNotEmpty(trader.getLicenseNo())){
						license.setLicenseNo(trader.getLicenseNo());
					}
					if(StringUtils.isNotEmpty(trader.getTraderType())){
						license.setLicenseType(trader.getTraderType());
					}
					if(StringUtils.isNotEmpty(trader.getTraderCategory())){
						license.setLicenseCategory(trader.getTraderCategory());
					}
					if(StringUtils.isNotEmpty(trader.getLicenseStatus())){
						license.setLicenseStatus(trader.getLicenseStatus());
					}
					if(StringUtils.isNotEmpty(trader.getLicenseGuideNo())){
						license.setLicenseGuideNo(trader.getLicenseGuideNo());
					}
					if(null != trader.getEffectiveDate()){
						license.setEffectiveDate(trader.getEffectiveDate());
					}
					if(null != trader.getExpireDate()){
						license.setExpireDate(trader.getExpireDate());
					}
					if(StringUtils.isNotEmpty(trader.getTraderName())){
						license.setLicenseName(trader.getTraderName());
					}
					if(StringUtils.isNotEmpty(trader.getTraderNameEn())){
						license.setLicenseNameEn(trader.getTraderNameEn());
					}
					if(StringUtils.isNotEmpty(trader.getPronunciationName())){
						license.setPronunciationName(trader.getPronunciationName());
					}
					if(null != trader.getMasProvince()){
						Hibernate.initialize(trader.getMasProvince());
						license.setLicenseServiceProvince(trader.getMasProvince());
					}
					if(StringUtils.isNotEmpty(trader.getTraderArea())){
						license.setLicenseServiceArea(trader.getTraderArea());
					}
					initEducation(owner, license, listEducation , user);
					initShopAddress(license, trader.getTraderId(), user);
					initContryTour(license, listPlanTrip,user);
					initSubLicense(license , listBranch, trader.getTraderId(), user);
					
					this.dotLicenseDAO.update(license);
				}else{
					//กรณีที่ยังไม่เคยจดใบอนุญาต นี้ online แต่มาจดที่สำนักงานทะเบียน ต้อง update ใบอนุญาตใบใหม่ให้ผู้ประกอบการด้วย
					license = new DotLicense();
					
					license.setRecordStatus(RecordStatus.NORMAL.getStatus());
					license.setCreateUser(user.getUserName());
					license.setCreateDtm(new Date());
					license.setLastUpdUser(user.getUserName());
					license.setLastUpdDtm(new Date());
					license.setDotLicenseOwner(owner);
					
					if(StringUtils.isNotEmpty(trader.getLicenseNo())){
						license.setLicenseNo(trader.getLicenseNo());
					}
					if(StringUtils.isNotEmpty(trader.getTraderType())){
						license.setLicenseType(trader.getTraderType());
					}
					if(StringUtils.isNotEmpty(trader.getTraderCategory())){
						license.setLicenseCategory(trader.getTraderCategory());
					}
					if(StringUtils.isNotEmpty(trader.getLicenseStatus())){
						license.setLicenseStatus(trader.getLicenseStatus());
					}
					if(StringUtils.isNotEmpty(trader.getLicenseGuideNo())){
						license.setLicenseGuideNo(trader.getLicenseGuideNo());
					}
					if(null != trader.getEffectiveDate()){
						license.setEffectiveDate(trader.getEffectiveDate());
					}
					if(null != trader.getExpireDate()){
						license.setExpireDate(trader.getExpireDate());
					}
					if(StringUtils.isNotEmpty(trader.getTraderName())){
						license.setLicenseName(trader.getTraderName());
					}
					if(StringUtils.isNotEmpty(trader.getTraderNameEn())){
						license.setLicenseNameEn(trader.getTraderNameEn());
					}
					if(StringUtils.isNotEmpty(trader.getPronunciationName())){
						license.setPronunciationName(trader.getPronunciationName());
					}
					if(null != trader.getMasProvince()){
						Hibernate.initialize(trader.getMasProvince());
						license.setLicenseServiceProvince(trader.getMasProvince());
					}
					if(StringUtils.isNotEmpty(trader.getTraderArea())){
						license.setLicenseServiceArea(trader.getTraderArea());
					}
					
					
					Long licenseId = (Long)this.dotLicenseDAO.insert(license);
					license.setLicenseId(licenseId);
					
					initEducation(owner, license, listEducation , user);
					initShopAddress(license, trader.getTraderId(), user);
					initContryTour(license, listPlanTrip,user);
					initSubLicense(license , listBranch, trader.getTraderId(), user);
				}
				
			}
			
			this.dotLicenseOwnerDAO.update(owner);
		}
		
//		System.out.println("update ข้อมูลผู้รับใบอนุญาต REG_ID = " + reg.getRegId());
//		//update ข้อมูลผู้รับใบอนุญาต
//		List<DotTrFeeLicense> listTrFee = (List<DotTrFeeLicense>)this.dotTrFeeLicenseDAO.findTrFeeLicenseByRegId(reg.getRegId());
//		if(CollectionUtils.isNotEmpty(listTrFee)){
//			
//			DotTrFeeLicense trFee = listTrFee.get(0);
//			System.out.println("update LicenseFeeStatus = FINISH ");
//			trFee.setLicenseFeeStatus(LicenseFeeStatus.FINISH.getStatus());
//			trFee.setLicenseNo(trader.getLicenseNo());
//			trFee.setLastUpdUser(user.getUserName());
//			trFee.setLastUpdDtm(new Date());
//			this.dotTrFeeLicenseDAO.update(trFee);
//		}
		//update ข้อมูลผู้รับใบอนุญาต
	}
	
	private void updateTraderToHistory(String licenseNo, String traderType,  long orgId , User user)  throws Exception
	{
		List<Trader> listTrader = (List<Trader>) this.traderDAO.findTraderByLicenseNo(licenseNo, traderType, RecordStatus.NORMAL.getStatus(), 0);
		
		if(!listTrader.isEmpty())
		{
			for(Trader trader: listTrader){				
				
				Person person = trader.getPerson();
				
				List<Registration> listRegistration = (List<Registration>) this.registrationDAO.findAllByTrader(trader.getTraderId());
				
				if(!listRegistration.isEmpty())
				{
					Registration reg = listRegistration.get(0);
					reg.setRecordStatus(RecordStatus.HISTORY.getStatus());
					reg.setLastUpdUser(user.getUserName());
					reg.setLastUpdDtm(new Date());
				}
				
				// trader addres
				List<TraderAddress> listAddress = (List<TraderAddress>) this.traderAddressDAO.findAllByTrader(trader.getTraderId(), null, RecordStatus.NORMAL.getStatus());
				if(!listAddress.isEmpty())
				{
					for(TraderAddress address: listAddress)
					{					
						address.setRecordStatus(RecordStatus.HISTORY.getStatus());
						address.setLastUpdUser(user.getUserName());
						address.setLastUpdDtm(new Date());
						this.traderAddressDAO.update(address);
					}
				}
				//Branch
				List<Trader> listBranch = (List<Trader>) this.branchDAO.findAllByBranchParent(trader.getTraderId(), RecordStatus.NORMAL.getStatus());
				if(!listBranch.isEmpty())
				{
					for(Trader branch: listBranch)
					{
						branch.setRecordStatus(RecordStatus.HISTORY.getStatus());
						branch.setLastUpdUser(user.getUserName());
						branch.setLastUpdDtm(new Date());
						
						this.branchDAO.update(branch);
						
						List<TraderAddress> listBranchAddress = (List<TraderAddress>) this.traderAddressDAO.findAllByTrader(branch.getTraderId(), null, RecordStatus.NORMAL.getStatus());
						if(!listBranchAddress.isEmpty())
						{
							for(TraderAddress branchAddress: listBranchAddress)
							{							
								branchAddress.setRecordStatus(RecordStatus.HISTORY.getStatus());
								branchAddress.setLastUpdUser(user.getUserName());
								branchAddress.setLastUpdDtm(new Date());
								this.traderAddressDAO.update(branchAddress);
							}
						}
					}
				}	
				// plantrip
				List<Plantrip> listPlanTrip = (List<Plantrip>) this.plantripDAO.findPlanTripbyTrader(trader.getTraderId(), RecordStatus.NORMAL.getStatus());
				if(!listPlanTrip.isEmpty())
				{
					for(Plantrip plantrip: listPlanTrip)
					{					
						plantrip.setRecordStatus(RecordStatus.HISTORY.getStatus());
						plantrip.setLastUpdUser(user.getUserName());
						plantrip.setLastUpdDtm(new Date());
						this.plantripDAO.update(plantrip);
					}
				}
				
				
				//committee
				List<Committee> listCommittee =  (List<Committee>) this.committeeDAO.findAllByPerson(person.getPersonId(), null, RecordStatus.NORMAL.getStatus());
				if(!listCommittee.isEmpty())
				{
					for(Committee comm: listCommittee)
					{
						
						
						comm.setRecordStatus(RecordStatus.HISTORY.getStatus());
						comm.setLastUpdUser(user.getUserName());
						comm.setLastUpdDtm(new Date());
						this.committeeDAO.update(comm);
					}
				}
				//education and traning
				List<Education> listEducation = (List<Education>) this.educationDAO.findEducationByPerson(person.getPersonId(), RecordStatus.NORMAL.getStatus(), null);
				if(!listEducation.isEmpty())
				{
					for(Education edu: listEducation)
					{
						
						edu.setRecordStatus(RecordStatus.HISTORY.getStatus());
						edu.setLastUpdUser(user.getUserName());
						edu.setLastUpdDtm(new Date());
						this.committeeDAO.update(edu);
					}
				}
				//ภาษา
				List<ForeignLanguage> listLang = (List<ForeignLanguage>) this.foreignLanguageDAO.findForeignLanguageByPerson(person.getPersonId(), RecordStatus.NORMAL.getStatus());
				if(!listLang.isEmpty())
				{
					for(ForeignLanguage lang: listLang)
					{
						
						lang.setRecordStatus(RecordStatus.HISTORY.getStatus());
						lang.setLastUpdUser(user.getUserName());
						lang.setLastUpdDtm(new Date());
						this.foreignLanguageDAO.insert(lang);
					}
				}			
				
				//OAT ADD 16/10/58
				trader.setLicenseStatus(RecordStatus.NORMAL.getStatus());
				
				trader.setRecordStatus(RecordStatus.HISTORY.getStatus());
				trader.setLastUpdUser(user.getUserName());
				trader.setLastUpdDtm(new Date());
				
				this.traderDAO.update(trader);
				
				//OAT ADD 16/10/58
				person.setPersonStatus(RecordStatus.NORMAL.getStatus());
				
				person.setRecordStatus(RecordStatus.HISTORY.getStatus());
				person.setLastUpdUser(user.getUserName());
				person.setLastUpdDtm(new Date());
				
				this.personDAO.update(person);
			}
		}
		
		List listOldOrg = (List<Trader>) this.traderDAO.findTraderByLicenseNo(licenseNo, traderType, RecordStatus.HISTORY.getStatus(), 0);
		//เปลี่ยน orgId HISTORY
		
		if(!listOldOrg.isEmpty()){
			Organization org = new Organization();
			org.setOrgId(user.getUserData().getOrgId());
			
			for(Trader trader: listTrader){			
				
				trader.setOrganization(org);
				this.traderDAO.update(trader);
				
				List<Registration> listRegistration = (List<Registration>) this.registrationDAO.findAllByTrader(trader.getTraderId());
				
				if(!listRegistration.isEmpty())
				{
					Registration reg = listRegistration.get(0);
					reg.setOrganization(org);
				}
				//Branch
				List<Trader> listBranch = (List<Trader>) this.branchDAO.findAllByBranchParent(trader.getTraderId(), RecordStatus.NORMAL.getStatus());
				if(!listBranch.isEmpty())
				{
					for(Trader branch: listBranch)
					{
						branch.setOrganization(org);
						this.branchDAO.update(branch);
					}
				}	
			}
			
		}
	}
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void rejectRegistration(Object object, User user) throws Exception {

		if(!(object instanceof Registration))
		{
			throw new IllegalArgumentException("ไม่สามารถอนุมัติการจดทะเบียนได้");
		}
		
				
		Registration reg = (Registration)object;
		Trader trader = reg.getTrader();
		
		List<Trader> listCurrTrader = (List<Trader>) this.traderDAO.findTraderByLicenseNo(trader.getLicenseNo(), trader.getTraderType(), RecordStatus.NORMAL.getStatus(), 0);
		
		if(!listCurrTrader.isEmpty())
		{
			Trader currTrader = listCurrTrader.get(0);
			currTrader.setLicenseStatus(LicenseStatus.NORMAL.getStatus());
		}
		
		reg.setApproveStatus(ProgressStatus.REJECT.getStatus());
		reg.setApproveDate(new Date());
		reg.setRecordStatus(RecordStatus.DISALLOW.getStatus());
		reg.setRegistrationProgress(reg.getRegistrationProgress());
		reg.setLastUpdUser(user.getUserName());
		reg.setLastUpdDtm(new Date());
		
		this.registrationDAO.update(reg);
		
		
		trader.setLicenseStatus(RecordStatus.DISALLOW.getStatus());
		trader.setRecordStatus(RecordStatus.DISALLOW.getStatus());
		trader.setLastUpdUser(user.getUserName());
		trader.setLastUpdDtm(new Date());
		
		this.traderDAO.update(trader);
		
		Person person = trader.getPerson();
		
		person.setPersonStatus(LicenseStatus.DISALLOW.getStatus());
		person.setRecordStatus(RecordStatus.DISALLOW.getStatus());
		person.setLastUpdUser(user.getUserName());
		person.setLastUpdDtm(new Date());

		this.personDAO.update(person);
		
		// trader addres
		List<TraderAddress> listAddress = (List<TraderAddress>) this.traderAddressDAO.findAllByTrader(trader.getTraderId(), null, RecordStatus.TEMP.getStatus());
		if(!listAddress.isEmpty())
		{
			for(TraderAddress address: listAddress)
			{
				address.setRecordStatus(RecordStatus.DISALLOW.getStatus());
				address.setLastUpdUser(user.getUserName());
				address.setLastUpdDtm(new Date());
				this.traderAddressDAO.update(address);
			}
		}
		//Branch
		List<Trader> listBranch = (List<Trader>) this.branchDAO.findAllByBranchParent(trader.getTraderId(), RecordStatus.TEMP.getStatus());
		if(!listBranch.isEmpty())
		{
			for(Trader branch: listBranch)
			{
				
				branch.setLicenseNo(trader.getLicenseNo());
				
				
				branch.setLicenseStatus(RecordStatus.DISALLOW.getStatus());
				branch.setRecordStatus(RecordStatus.DISALLOW.getStatus());
				branch.setLastUpdUser(user.getUserName());
				
				branch.setLastUpdDtm(new Date());
				
				this.branchDAO.update(branch);
				
				List<TraderAddress> listBranchAddress = (List<TraderAddress>) this.traderAddressDAO.findAllByTrader(branch.getTraderId(), null, RecordStatus.TEMP.getStatus());
				if(!listBranchAddress.isEmpty())
				{
					for(TraderAddress branchAddress: listBranchAddress)
					{
						branchAddress.setRecordStatus(RecordStatus.DISALLOW.getStatus());
						branchAddress.setLastUpdUser(user.getUserName());
						branchAddress.setLastUpdDtm(new Date());
						this.traderAddressDAO.update(branchAddress);
					}
				}
			}
		}	
		// plantrip
		List<Plantrip> listPlanTrip = (List<Plantrip>) this.plantripDAO.findPlanTripbyTrader(trader.getTraderId(), RecordStatus.TEMP.getStatus());
		if(!listPlanTrip.isEmpty())
		{
			for(Plantrip plantrip: listPlanTrip)
			{
				plantrip.setRecordStatus(RecordStatus.DISALLOW.getStatus());
				plantrip.setLastUpdUser(user.getUserName());
				plantrip.setLastUpdDtm(new Date());
				this.plantripDAO.update(plantrip);
			}
		}
		//committee
		List<Committee> listCommittee =  (List<Committee>) this.committeeDAO.findAllByPerson(person.getPersonId(), null, RecordStatus.TEMP.getStatus());
		if(!listCommittee.isEmpty())
		{
			for(Committee comm: listCommittee)
			{
				comm.setCommitteeStatus(LicenseStatus.DISALLOW.getStatus());
				comm.setRecordStatus(RecordStatus.DISALLOW.getStatus());
				comm.setLastUpdUser(user.getUserName());
				comm.setLastUpdDtm(new Date());
				this.committeeDAO.update(comm);
			}
		}
		//education and traning
		List<Education> listEducation = (List<Education>) this.educationDAO.findEducationByPerson(person.getPersonId(), RecordStatus.TEMP.getStatus(), null);
		if(!listEducation.isEmpty())
		{
			for(Education edu: listEducation)
			{
				edu.setRecordStatus(RecordStatus.DISALLOW.getStatus());
				edu.setLastUpdUser(user.getUserName());
				edu.setLastUpdDtm(new Date());
				this.committeeDAO.update(edu);
			}
		}
		//ภาษา
		List<ForeignLanguage> listLang = (List<ForeignLanguage>) this.foreignLanguageDAO.findForeignLanguageByPerson(person.getPersonId(), RecordStatus.TEMP.getStatus());
		if(!listLang.isEmpty())
		{
			for(ForeignLanguage lang: listLang)
			{
				lang.setRecordStatus(RecordStatus.DISALLOW.getStatus());
				lang.setLastUpdUser(user.getUserName());
				lang.setLastUpdDtm(new Date());
				this.foreignLanguageDAO.update(lang);
			}
		}
		
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	private void initOwner(DotLicenseOwner owner, Trader trader, Person person, User user) throws Exception{
		owner.setTaxId(person.getIdentityNo());
		owner.setVatId(person.getTaxIdentityNo());
		if(null != person.getMasProvinceByTaxProvinceId()){
			Hibernate.initialize(person.getMasProvinceByTaxProvinceId());
			owner.setMasProvinceByTaxProvince(person.getMasProvinceByTaxProvinceId());
		}
		if(null != person.getMasPosfix()){
			Hibernate.initialize(person.getMasPosfix());
			owner.setMasPosfix(person.getMasPosfix());
		}
		if(null != person.getMasPrefix()){
			Hibernate.initialize(person.getMasPrefix());
			owner.setMasPrefix(person.getMasPrefix());
		}
		if(null != person.getMasAmphurByTaxAmphurId()){
			Hibernate.initialize(person.getMasAmphurByTaxAmphurId());
			owner.setMasAmphurByTaxAmphur(person.getMasAmphurByTaxAmphurId());
		}
		owner.setOwnerName(person.getFirstName());
		owner.setOwnerLastName(person.getLastName());
		owner.setOwnerNameEn(person.getFirstNameEn());
		owner.setOwnerLastNameEn(person.getLastNameEn());
		owner.setOwnerType(person.getPersonType());
		if(StringUtils.isNotEmpty(person.getPassportNo())){
			owner.setOwnerPassport(person.getPassportNo());
		}
		if(StringUtils.isNotEmpty(person.getGender())){
			owner.setOwnerGender(person.getGender());
		}
		if(null != person.getBirthDate()){
			owner.setOwnerBirthDate(person.getBirthDate());
		}
		if(StringUtils.isNotEmpty(person.getPersonNationality())){
			owner.setOwnerNationality(person.getPersonNationality());
		}
		if(null != person.getIdentityDate()){
			owner.setTaxDate(person.getIdentityDate());
		}
		
		owner.setLastUpdUser(user.getUserName());
		owner.setLastUpdDtm(new Date());
		
		this.dotLicenseOwnerDAO.update(owner);
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	private void initOwnerAddress(DotLicenseOwner licenseOwner, long traderId) throws Exception{
		
		List<TraderAddress> listOffice = traderAddressDAO.findAllByTrader(traderId, TraderAddressType.OFFICE_ADDRESS.getStatus(), null);
		
		if(CollectionUtils.isNotEmpty(listOffice)){
			TraderAddress officeAddress = listOffice.get(0);
			
			licenseOwner.setBuildingName(officeAddress.getBuildingName());
			licenseOwner.setBuildingNameEn(officeAddress.getBuildingNameEn());
			licenseOwner.setVillageName(officeAddress.getVillageName());
			licenseOwner.setVillageNameEn(officeAddress.getVillageNameEn());
			licenseOwner.setFloor(officeAddress.getFloor());
			licenseOwner.setRoomNo(officeAddress.getRoomNo());
			licenseOwner.setHomeNo(officeAddress.getAddressNo());
			licenseOwner.setMooNo(officeAddress.getMoo());
			licenseOwner.setSoi(officeAddress.getSoi());
			licenseOwner.setRoadName(officeAddress.getRoadName());
			licenseOwner.setRoadNameEn(officeAddress.getRoadNameEn());
			licenseOwner.setPostCode(officeAddress.getPostCode());
			licenseOwner.setTelephone(officeAddress.getTelephone());
			licenseOwner.setMobileNo(officeAddress.getMobileNo());
			licenseOwner.setEmail(officeAddress.getEmail());
			licenseOwner.setFax(officeAddress.getFax());
			licenseOwner.setWebsite(officeAddress.getWebsite());
			
			if(null != officeAddress.getMasProvince()){
				Hibernate.initialize(officeAddress.getMasProvince());
				licenseOwner.setMasProvinceByProvinceId(officeAddress.getMasProvince());
			}
			if(null != officeAddress.getMasAmphur()){
				Hibernate.initialize(officeAddress.getMasAmphur());
				licenseOwner.setMasAmphurByAmphurId(officeAddress.getMasAmphur());
			}
			if(null != officeAddress.getMasTambol()){
				Hibernate.initialize(officeAddress.getMasTambol());
				licenseOwner.setMasTambolByTambolId(officeAddress.getMasTambol());
			}
			
		}
		
		List<TraderAddress> listContact = traderAddressDAO.findAllByTrader(traderId, TraderAddressType.CONTACT.getStatus(), null);
		if(CollectionUtils.isNotEmpty(listContact)){
			TraderAddress contactAddress = listContact.get(0);
			
			licenseOwner.setContactBuildingName(contactAddress.getBuildingName());
			licenseOwner.setContactBuildingNameEn(contactAddress.getBuildingNameEn());
			licenseOwner.setContactVillageName(contactAddress.getVillageName());
			licenseOwner.setContactVillageNameEn(contactAddress.getVillageNameEn());
			licenseOwner.setContactFloor(contactAddress.getFloor());
			licenseOwner.setContactRoomNo(contactAddress.getRoomNo());
			licenseOwner.setContactHomeNo(contactAddress.getAddressNo());
			licenseOwner.setContactMooNo(contactAddress.getMoo());
			licenseOwner.setContactSoi(contactAddress.getSoi());
			licenseOwner.setContactRoadName(contactAddress.getRoadName());
			licenseOwner.setContactRoadNameEn(contactAddress.getRoadNameEn());
			licenseOwner.setContactPostCode(contactAddress.getPostCode());
			licenseOwner.setContactTelephone(contactAddress.getTelephone());
			licenseOwner.setContactMobileNo(contactAddress.getMobileNo());
			licenseOwner.setContactEmail(contactAddress.getEmail());
			licenseOwner.setContactFax(contactAddress.getFax());
			licenseOwner.setContactWebsite(contactAddress.getWebsite());
			
			if(null != contactAddress.getMasProvince()){
				Hibernate.initialize(contactAddress.getMasProvince());
				licenseOwner.setMasProvinceByContactProvinceId(contactAddress.getMasProvince());
			}
			if(null != contactAddress.getMasAmphur()){
				Hibernate.initialize(contactAddress.getMasAmphur());
				licenseOwner.setMasAmphurByContactAmphurId(contactAddress.getMasAmphur());
			}
			if(null != contactAddress.getMasTambol()){
				Hibernate.initialize(contactAddress.getMasTambol());
				licenseOwner.setMasTambolByContactTambolId(contactAddress.getMasTambol());
			}
		}else{
			if(CollectionUtils.isNotEmpty(listOffice)){
				TraderAddress officeAddress = listOffice.get(0);
				
				licenseOwner.setContactBuildingName(officeAddress.getBuildingName());
				licenseOwner.setContactBuildingNameEn(officeAddress.getBuildingNameEn());
				licenseOwner.setContactVillageName(officeAddress.getVillageName());
				licenseOwner.setContactVillageNameEn(officeAddress.getVillageNameEn());
				licenseOwner.setContactFloor(officeAddress.getFloor());
				licenseOwner.setContactRoomNo(officeAddress.getRoomNo());
				licenseOwner.setContactHomeNo(officeAddress.getAddressNo());
				licenseOwner.setContactMooNo(officeAddress.getMoo());
				licenseOwner.setContactSoi(officeAddress.getSoi());
				licenseOwner.setContactRoadName(officeAddress.getRoadName());
				licenseOwner.setContactRoadNameEn(officeAddress.getRoadNameEn());
				licenseOwner.setContactPostCode(officeAddress.getPostCode());
				licenseOwner.setContactTelephone(officeAddress.getTelephone());
				licenseOwner.setContactMobileNo(officeAddress.getMobileNo());
//				licenseOwner.setContactEmail(officeAddress.getEmail());
				licenseOwner.setContactFax(officeAddress.getFax());
				licenseOwner.setContactWebsite(officeAddress.getWebsite());
				
				if(null != officeAddress.getMasProvince()){
					Hibernate.initialize(officeAddress.getMasProvince());
					licenseOwner.setMasProvinceByContactProvinceId(officeAddress.getMasProvince());
				}
				if(null != officeAddress.getMasAmphur()){
					Hibernate.initialize(officeAddress.getMasAmphur());
					licenseOwner.setMasAmphurByContactAmphurId(officeAddress.getMasAmphur());
				}
				if(null != officeAddress.getMasTambol()){
					Hibernate.initialize(officeAddress.getMasTambol());
					licenseOwner.setMasTambolByContactTambolId(officeAddress.getMasTambol());
				}
			}
			
		}
		
		this.dotLicenseOwnerDAO.update(licenseOwner);
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	private void initCommittee(DotLicenseOwner owner , List<Committee> listCommittee, User user) throws Exception{
		
		if(CollectionUtils.isNotEmpty(listCommittee)){
			
			// delete old committee
			Map<String,Object> param = new HashMap<String,Object>();
			param.put("taxId", owner.getTaxId());
			this.dotLicOwnerCommitteeDAO.executeUpdate(" UPDATE DOT_LIC_OWNER_COMMITTEE SET RECORD_STATUS = 'D' WHERE TAX_ID = :taxId ", param);
			// delete old committee
			for(Committee traderCommittee: listCommittee){
				DotLicOwnerCommittee committee = new DotLicOwnerCommittee();
				committee.setDotLicenseOwner(owner);
				committee.setRecordStatus(RecordStatus.NORMAL.getStatus());
				committee.setCreateUser(user.getUserName());
				committee.setCreateDtm(new Date());
				committee.setLastUpdUser(user.getUserName());
				committee.setLastUpdDtm(new Date());
				
				if(null != traderCommittee.getMasAmphurByTaxAmphurId()){
					Hibernate.initialize(traderCommittee.getMasAmphurByTaxAmphurId());
					committee.setMasAmphurByTaxAmphurId(traderCommittee.getMasAmphurByTaxAmphurId());
				}
				if(null != traderCommittee.getMasPosfix()){
					Hibernate.initialize(traderCommittee.getMasPosfix());
					committee.setMasPosfix(traderCommittee.getMasPosfix());
				}
				if(null != traderCommittee.getMasProvinceByTaxProvinceId()){
					Hibernate.initialize(traderCommittee.getMasProvinceByTaxProvinceId());
					committee.setMasProvinceByTaxProvinceId(traderCommittee.getMasProvinceByTaxProvinceId());
				}
				if(null != traderCommittee.getMasAmphurByAmphurId()){
					Hibernate.initialize(traderCommittee.getMasAmphurByAmphurId());
					committee.setMasAmphurByAmphurId(traderCommittee.getMasAmphurByAmphurId());
				}
				if(null != traderCommittee.getMasProvinceByProvinceId()){
					Hibernate.initialize(traderCommittee.getMasProvinceByProvinceId());
					committee.setMasProvinceByProvinceId(traderCommittee.getMasProvinceByProvinceId());
				}
				if(null != traderCommittee.getMasPrefix()){
					Hibernate.initialize(traderCommittee.getMasPrefix());
					committee.setMasPrefix(traderCommittee.getMasPrefix());
				}
				committee.setCommitteeType(traderCommittee.getCommitteeType());
				if(StringUtils.isNotEmpty(traderCommittee.getCommitteeIdentityNo())){
					committee.setCommitteeIdentityNo(traderCommittee.getCommitteeIdentityNo());
				}
				if(null != traderCommittee.getCommitteeIdentityDate()){
					committee.setCommitteeIdentityDate(traderCommittee.getCommitteeIdentityDate());
				}
				committee.setCommitteeName(traderCommittee.getCommitteeName());
				committee.setCommitteeLastname(traderCommittee.getCommitteeLastname());
				committee.setCommitteeNameEn(traderCommittee.getCommitteeNameEn());
				committee.setCommitteeLastnameEn(traderCommittee.getCommitteeLastnameEn());
				committee.setCommitteePersonType(traderCommittee.getCommitteePersonType());
				if(null != traderCommittee.getBirthDate()){
					committee.setBirthDate(traderCommittee.getBirthDate());
				}
				committee.setTaxIdentityNo(traderCommittee.getTaxIdentityNo());
				committee.setGender(traderCommittee.getGender());
				committee.setCommitteeNationality(traderCommittee.getCommitteeNationality());
				
				this.dotLicOwnerCommitteeDAO.insert(committee);
			}
		}
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	private void initEducation(DotLicenseOwner owner , DotLicense license ,List<Education> listEducations, User user) throws Exception{
			
		if(CollectionUtils.isNotEmpty(listEducations)){
			
			//delete old
			Map<String,Object> params = new HashMap<String,Object>();
            params.put("taxId",owner.getTaxId());
            params.put("licenseId", license.getLicenseId());
            this.dotLicOwnerEduBackgroundDAO.executeUpdate("UPDATE DOT_LIC_OWNER_EDU_BACKGROUND SET RECORD_STATUS = 'D'  WHERE TAX_ID = :taxId AND LICENSE_ID = :licenseId", params);
			//delete old
			
			for(Education src: listEducations){
				DotLicOwnerEduBackground dest = new DotLicOwnerEduBackground();
				
				dest.setDotLicenseOwner(owner);
				dest.setDotLicense(license);
				dest.setRecordStatus(RecordStatus.NORMAL.getStatus());
				dest.setCreateUser(user.getUserName());
				dest.setCreateDtm(new Date());
				dest.setLastUpdUser(user.getUserName());
				dest.setLastUpdDtm(new Date());
				
				if(null != src.getMasUniversity()){
					Hibernate.initialize(src.getMasUniversity());
					dest.setMasUniversity(src.getMasUniversity());
				}
				if(null != src.getMasEducationLevel()){
					Hibernate.initialize(src.getMasEducationLevel());
					dest.setMasEducationLevel(src.getMasEducationLevel());
				}
				if(StringUtils.isNotEmpty(src.getEducationType())){
					dest.setEducationType(src.getEducationType());
				}
				if(StringUtils.isNotEmpty(src.getEducationMajor())){
					dest.setEducationMajor(src.getEducationMajor());
				}
				if(null != src.getGenerationGraduate()){
					dest.setGenerationGraduate(src.getGenerationGraduate());
				}
				if(StringUtils.isNotEmpty(src.getGraduationCourse())){
					dest.setGraduationCourse(src.getGraduationCourse());
				}
				
				if(null != src.getStudyDate()){
					dest.setStudyDate(src.getStudyDate());
				}
				if(null != src.getGraduationDate()){
					dest.setGraduationDate(src.getGraduationDate());
				}
				if(StringUtils.isNotEmpty(src.getGraduationYear())){
					dest.setGraduationYear(src.getGraduationYear());
				}
				this.dotLicOwnerEduBackgroundDAO.insert(dest);
			}
		}
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	private void initLanguage(DotLicenseOwner licenseOwner, List<ForeignLanguage> listTraderLang, User user) throws Exception{
		if(CollectionUtils.isNotEmpty(listTraderLang)){
			
			//delete old
			Map<String,Object> params = new HashMap<String,Object>();
            params.put("taxId",licenseOwner.getTaxId());
            this.dotMsLanguageDAO.executeUpdate("UPDATE DOT_LIC_OWNER_LANGUAGE SET RECORD_STATUS = 'D' WHERE TAX_ID = :taxId ", params);
			//delete old
			for(ForeignLanguage f: listTraderLang){
				
				if(null != f.getCountry()){
					List<DotMsLanguage> listLang = this.dotMsLanguageDAO.listLanguageByName(f.getCountry().getLanguage());
					
					DotMsLanguage dotMsLanguage = listLang.get(0);
					DotLicOwnerLanguage lang = new DotLicOwnerLanguage();
					lang.setDotLicenseOwner(licenseOwner);
					lang.setRecordStatus(RecordStatus.NORMAL.getStatus());
					lang.setCreateUser(user.getUserName());
					lang.setCreateDtm(new Date());
					lang.setLastUpdUser(user.getUserName());
					lang.setLastUpdDtm(new Date());
					lang.setDotMsLanguage(dotMsLanguage);
					this.dotMsLanguageDAO.insert(lang);
				}	
			}
			
		}
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	private void initShopAddress(DotLicense license, long traderId, User user) throws Exception{
		
		List<TraderAddress> listShop = traderAddressDAO.findAllByTrader(traderId, TraderAddressType.SHOP_ADDRESS.getStatus(), null);
		if(CollectionUtils.isNotEmpty(listShop)){
			TraderAddress shopAddress = listShop.get(0);
			
			license.setLicenseBuildingName(shopAddress.getBuildingName());
			license.setLicenseBuildingNameEn(shopAddress.getBuildingNameEn());
			license.setLicenseVillageName(shopAddress.getVillageName());
			license.setLicenseVillageNameEn(shopAddress.getVillageNameEn());
			license.setLicenseFloor(shopAddress.getFloor());
			license.setLicenseRoomNo(shopAddress.getRoomNo());
			license.setLicenseHomeNo(shopAddress.getAddressNo());
			license.setLicenseMooNo(shopAddress.getMoo());
			license.setLicenseSoi(shopAddress.getSoi());
			license.setLicenseRoadName(shopAddress.getRoadName());
			license.setLicenseRoadNameEn(shopAddress.getRoadNameEn());
			license.setLicensePostCode(shopAddress.getPostCode());
			license.setLicenseTelephone(shopAddress.getTelephone());
			license.setLicenseMobileNo(shopAddress.getMobileNo());
			license.setLicenseEmail(shopAddress.getEmail());
			license.setLicenseFax(shopAddress.getFax());
			license.setLicenseWebsite(shopAddress.getWebsite());
			
			if(null != shopAddress.getMasProvince()){
				Hibernate.initialize(shopAddress.getMasProvince());
				license.setMasProvince(shopAddress.getMasProvince());
			}
			if(null != shopAddress.getMasAmphur()){
				Hibernate.initialize(shopAddress.getMasAmphur());
				license.setMasAmphur(shopAddress.getMasAmphur());
			}
			if(null != shopAddress.getMasTambol()){
				Hibernate.initialize(shopAddress.getMasTambol());
				license.setMasTambol(shopAddress.getMasTambol());
			}
		}
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	private void initContryTour(DotLicense license, List<Plantrip> listPlantrip, User user) throws Exception{
		
		//จำนวนครั้งที่นำเที่ยว ประเทศที่นำเที่ยว

		if(CollectionUtils.isNotEmpty(listPlantrip)){
			
			//delete old
			Map<String,Object> params = new HashMap<String,Object>();
            params.put("licenseId",license.getLicenseId());
            this.countryTourDAO.executeUpdate("UPDATE COUNTRY_TOUR SET RECORD_STATUS = 'D' WHERE LICENSE_ID = :licenseId ", params);
			//delete old
			
			//จำนวนครั้งที่นำเที่ยว
			Plantrip numOftrip = listPlantrip.get(0);
			
			license.setPlanTrips(numOftrip.getPlantripPerYear()==null?0:numOftrip.getPlantripPerYear());
			//ประเทศที่นำเที่ยว
			for(Plantrip plantrip: listPlantrip){
				
				CountryTour countryTour = new CountryTour();
				countryTour.setRecordStatus(RecordStatus.NORMAL.getStatus());
				countryTour.setCreateUser(user.getUserName());
				countryTour.setCreateDtm(new Date());
				countryTour.setLastUpdUser(user.getUserName());
				countryTour.setLastUpdDtm(new Date());
				countryTour.setDotLicense(license);
				
				if(null != plantrip.getCountry()){
					Hibernate.initialize(plantrip.getCountry());
					countryTour.setCountry(plantrip.getCountry());
				}else{
					Country country = (Country)this.countryTourDAO.findByPrimaryKey(Long.valueOf(41));
					countryTour.setCountry(country);
				}
				this.countryTourDAO.insert(countryTour);
			}
		}
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	private void initSubLicense(DotLicense license, List<Trader> branchs, long traderId, User user) throws Exception{
		
		//set sublicense
		if(CollectionUtils.isNotEmpty(branchs)){
			//delete old
			Map<String,Object> params = new HashMap<String,Object>();
            params.put("licenseId",license.getLicenseId());
            this.dotSubLicenseDAO.executeUpdate("UPDATE DOT_SUB_LICENSE SET RECORD_STATUS = 'D' WHERE LICENSE_ID = :licenseId ", params);
			//delete old
			
			for(Trader branch: branchs){
				DotSubLicense subLic = new DotSubLicense();
				subLic.setBranchNo(branch.getTraderBranchNo());
				subLic.setRecordStatus(RecordStatus.NORMAL.getStatus());
				subLic.setCreateUser(user.getUserName());
				subLic.setCreateDtm(new Date());
				subLic.setLastUpdUser(user.getUserName());
				subLic.setLastUpdDtm(new Date());
				subLic.setDotLicense(license);
				
				List<TraderAddress> listAddressBranch = traderAddressDAO.findAllByTrader(branch.getTraderId(), TraderAddressType.BRANCH.getStatus(), null);
				if(CollectionUtils.isNotEmpty(listAddressBranch)){
					
					TraderAddress addrBranch = listAddressBranch.get(0);
					subLic.setBuildingName(addrBranch.getBuildingName());
					subLic.setBuildingNameEn(addrBranch.getBuildingNameEn());
					subLic.setVillageName(addrBranch.getVillageName());
					subLic.setVillageNameEn(addrBranch.getVillageNameEn());
					subLic.setFloor(addrBranch.getFloor());
					subLic.setRoomNo(addrBranch.getRoomNo());
					subLic.setHomeNo(addrBranch.getAddressNo());
					subLic.setMooNo(addrBranch.getMoo());
					subLic.setSoi(addrBranch.getSoi());
					subLic.setRoadName(addrBranch.getRoadName());
					subLic.setRoadNameEn(addrBranch.getRoadNameEn());
					subLic.setPostCode(addrBranch.getPostCode());
					subLic.setTelephone(addrBranch.getTelephone());
					subLic.setMobileNo(addrBranch.getMobileNo());
					subLic.setEmail(addrBranch.getEmail());
					subLic.setFax(addrBranch.getFax());
					subLic.setWebsite(addrBranch.getWebsite());
					
					if(null != addrBranch.getMasProvince()){
						Hibernate.initialize(addrBranch.getMasProvince());
						subLic.setMasProvince(addrBranch.getMasProvince());
					}
					if(null != addrBranch.getMasAmphur()){
						Hibernate.initialize(addrBranch.getMasAmphur());
						subLic.setMasAmphur(addrBranch.getMasAmphur());
					}
					if(null != addrBranch.getMasTambol()){
						Hibernate.initialize(addrBranch.getMasTambol());
						subLic.setMasTambol(addrBranch.getMasTambol());
					}
				}
				this.dotSubLicenseDAO.insert(subLic);
			}
			
		}
	}
}
