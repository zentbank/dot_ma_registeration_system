package sss.dot.tourism.service.registration;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.mas.MasAmphurDAO;
import sss.dot.tourism.dao.mas.MasProvinceDAO;
import sss.dot.tourism.dao.mas.MasTambolDAO;
import sss.dot.tourism.dao.registration.BranchDAO;
import sss.dot.tourism.dao.registration.PrintingLicenseDAO;
import sss.dot.tourism.dao.registration.RegistrationDAO;
import sss.dot.tourism.dao.registration.TraderAddressDAO;
import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.domain.MasAmphur;
import sss.dot.tourism.domain.MasProvince;
import sss.dot.tourism.domain.MasTambol;
import sss.dot.tourism.domain.PrintingLicense;
import sss.dot.tourism.domain.Registration;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.domain.TraderAddress;
import sss.dot.tourism.dto.registration.BranchDTO;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.RecordStatus;
import sss.dot.tourism.util.RegistrationType;
import sss.dot.tourism.util.TraderAddressType;

import com.sss.aut.service.User;
@Repository("branchService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class BranchService implements IBranchService {

	@Autowired
	private TraderAddressDAO traderAddressDAO;
	
	@Autowired
	private MasProvinceDAO masProvinceDAO;
	
	@Autowired
	private MasAmphurDAO masAmphurDAO;
	
	@Autowired
	private MasTambolDAO masTambolDAO;
	
	@Autowired
	private TraderDAO traderDAO;
	@Autowired
	private BranchDAO branchDAO;
	
	@Autowired
	private RegistrationDAO registrationDAO;
	
	@Autowired
	private PrintingLicenseDAO printingLicenseDAO;
	
	public List getAll(Object object, User user) throws Exception {
		
		if(!(object instanceof BranchDTO))
		{
			throw new IllegalArgumentException("object not instanceof BranchDTO");
		}
		List<BranchDTO> list = new ArrayList<BranchDTO>();
		
		
		
		BranchDTO param = (BranchDTO)object;
		Trader branchParent = (Trader)this.traderDAO.findByPrimaryKey(param.getBranchParentId());
		List<Trader> listBranch = this.branchDAO.findAllByBranchParent(branchParent.getTraderId(), branchParent.getRecordStatus());
		if(!listBranch.isEmpty())
		{
			for(Trader branch: listBranch)
			{
				BranchDTO dto = new BranchDTO();		
				
				ObjectUtil.copy(branch, dto);
				dto.setBranchId(branch.getTraderId());
				dto.setBranchParentId(branch.getTraderByBranchParentId().getTraderId());
				
				List<TraderAddress> listAddr =  this.traderAddressDAO.findAllByTrader(branch.getTraderId(), null, branch.getRecordStatus());
				
				if(!listAddr.isEmpty())
				{
					TraderAddress addr = listAddr.get(0);
					ObjectUtil.copy(addr, dto);
					
					if(addr.getMasProvince() != null)
					{
						dto.setProvinceId(addr.getMasProvince().getProvinceId());
						dto.setProvinceIdEn(addr.getMasAmphur().getAmphurId());
						dto.setProvinceName(addr.getMasProvince().getProvinceName());
						dto.setProvinceNameEn(addr.getMasProvince().getProvinceNameEn());
					}
					
					if(addr.getMasAmphur() != null)
					{
						dto.setAmphurId(addr.getMasAmphur().getAmphurId());
						dto.setAmphurIdEn(addr.getMasAmphur().getAmphurId());
						dto.setAmphurName(addr.getMasAmphur().getAmphurName());
						dto.setAmphurNameEn(addr.getMasAmphur().getAmphurNameEn());
					}
					
					if(addr.getMasTambol() != null)
					{
						dto.setTambolId(addr.getMasTambol().getTambolId());
						dto.setTambolIdEn(addr.getMasTambol().getTambolId());
						dto.setTambolName(addr.getMasTambol().getTambolName());
						dto.setTambolNameEn(addr.getMasTambol().getTambolNameEn());
					}
				}
				
				list.add(dto);
			}
		}
		
		return list;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List create(Object[] object , User user) throws Exception {
		List<BranchDTO> list = new ArrayList<BranchDTO>();
		for (Object obj : object) {
			
			BranchDTO dto = (BranchDTO) obj;
			//insert branch (Trader table)
			Trader branch = new Trader();
			branch.setBranchType("1");//เป็นสาขา
			branch.setTraderBranchNo(dto.getTraderBranchNo());
			
			
			
			Trader branchParent =  (Trader)this.branchDAO.findByPrimaryKey(dto.getBranchParentId());
			List<Registration> listRegistration = (List<Registration>)registrationDAO.findAllByTrader(branchParent.getTraderId());
			
			Registration reg = listRegistration.get(0);
			

					
			ObjectUtil.copy(branchParent, branch);
			branch.setTraderId(0);
			//set BRANCH_PARENT_ID
			branch.setTraderByBranchParentId(branchParent);
			branch.setOrganization(branchParent.getOrganization());
			branch.setPerson(branchParent.getPerson());
			branch.setLicenseStatus(RecordStatus.TEMP.getStatus());
			branch.setRecordStatus(RecordStatus.TEMP.getStatus());
			branch.setCreateUser(user.getUserName());
			branch.setCreateDtm(new Date());
			
			// ตามกฎหมายใหม่ ตั้งแต่วันที่ 28 ธันวาคม 2559 ต้องแสดงวันที่เริ่มต้น-สิ้นสุดในใบอนุญาตสาขาด้วย ดังนั้น ต้องใส่วันที่เริ่มต้น-สิ้นสุดของสำนักงานใหญ่ ที่ใบอนุญาตสาขาด้วย
			if(!RegistrationType.TOUR_COMPANIES_NEW_REGISTRATION.getStatus().equals(reg.getRegistrationType()))
			{
				branch.setAddNewBranch(RegistrationType.TOUR_COMPANIES_ADD_NEW_BRANCH.getStatus());
				
//				System.out.println("Set effectiveDate of parent to branch: "+branchParent.getEffectiveDate());
				branch.setEffectiveDate(branchParent.getEffectiveDate());
//				System.out.println("Set expire of parent to branch: "+branchParent.getExpireDate());
				branch.setExpireDate(branchParent.getExpireDate());
			}
			
			Long branchId = (Long) this.branchDAO.insert(branch);
			branch.setTraderId(branchId);
			
			dto.setBranchId(branchId);
			
			//end insert branch (Trader table)
			
			//insert branch address
			TraderAddress branchAddr = new TraderAddress();
			
			ObjectUtil.copy(dto, branchAddr);
			
			branchAddr.setAddressType(TraderAddressType.BRANCH.getStatus());
			branchAddr.setTrader(branch);
			
			if(dto.getProvinceId() > 0)
			{
				MasProvince province = new MasProvince();
				province.setProvinceId(dto.getProvinceId());
				
				branchAddr.setMasProvince(province);
			}
			
			if(dto.getAmphurId() > 0)
			{
				MasAmphur amphur = new MasAmphur();
				amphur.setAmphurId(dto.getAmphurId());
				
				branchAddr.setMasAmphur(amphur);
			}
			
			if(dto.getTambolId() > 0)
			{
				MasTambol tambol = new MasTambol();
				tambol.setTambolId(dto.getTambolId());
				
				branchAddr.setMasTambol(tambol);
			}
			
			
			branchAddr.setRecordStatus(RecordStatus.TEMP.getStatus());
			branchAddr.setCreateUser(user.getUserName());
			branchAddr.setCreateDtm(new Date());
			
			Long addressId = (Long) this.traderAddressDAO.insert(branchAddr);
		
			dto.setAddressId(addressId.longValue());
			//end insert branch address
			list.add(dto);
		}

		return list;
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void update(Object[] object , User user) throws Exception {
		
		for (Object obj : object) {
			
			BranchDTO dto = (BranchDTO) obj;
			
			Trader branch =  (Trader)this.branchDAO.findByPrimaryKey(dto.getBranchId());
			branch.setTraderBranchNo(dto.getTraderBranchNo());
			
			//insert branch address
			TraderAddress branchAddr = (TraderAddress)this.traderAddressDAO.findByPrimaryKey(dto.getAddressId());
			
			ObjectUtil.copy(dto, branchAddr);
			
			if(dto.getProvinceId() > 0)
			{
				MasProvince province = new MasProvince();
				province.setProvinceId(dto.getProvinceId());
				
				branchAddr.setMasProvince(province);
			}
			
			if(dto.getAmphurId() > 0)
			{
				MasAmphur amphur = new MasAmphur();
				amphur.setAmphurId(dto.getAmphurId());
				
				branchAddr.setMasAmphur(amphur);
			}
			
			if(dto.getTambolId() > 0)
			{
				MasTambol tambol = new MasTambol();
				tambol.setTambolId(dto.getTambolId());
				
				branchAddr.setMasTambol(tambol);
			}
				
			branchAddr.setLastUpdUser(user.getUserName());
			branchAddr.setLastUpdDtm(new Date());
			
			traderAddressDAO.update(branchAddr);
			//end insert branch address
			
			
		}
		
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(Object[] object , User user) throws Exception {
		
		for (Object obj : object) {
			
			BranchDTO dto = (BranchDTO) obj;
			
			TraderAddress addr = (TraderAddress)traderAddressDAO.findByPrimaryKey(dto.getAddressId());
			traderAddressDAO.delete(addr);
			
			//Oat Add Edit 05/11/57
			List<PrintingLicense> pril = (List<PrintingLicense>)printingLicenseDAO.findByTraderId(addr.getTrader().getTraderId());
			
			if(!pril.isEmpty() && pril.size() > 0)
			{
				printingLicenseDAO.delete((PrintingLicense)pril.get(0));
			}
			// 
			
			Trader branch = (Trader)addr.getTrader();
			branchDAO.delete(branch);
			
		}
		
	}

}
