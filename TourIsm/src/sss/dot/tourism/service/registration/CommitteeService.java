package sss.dot.tourism.service.registration;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.mas.MasPosfixDAO;
import sss.dot.tourism.dao.mas.MasPrefixDAO;
import sss.dot.tourism.dao.registration.CommitteeDAO;
import sss.dot.tourism.dao.registration.PersonDAO;
import sss.dot.tourism.dao.registration.RegistrationDAO;
import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.domain.Committee;
import sss.dot.tourism.domain.MasAmphur;
import sss.dot.tourism.domain.MasPosfix;
import sss.dot.tourism.domain.MasPrefix;
import sss.dot.tourism.domain.MasProvince;
import sss.dot.tourism.domain.Person;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.dto.registration.CommitteeDTO;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.util.CommitteeType;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.PersonType;
import sss.dot.tourism.util.RecordStatus;

import com.sss.aut.service.User;

@Repository("committeeService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class CommitteeService implements ICommitteeService {
	@Autowired
	private CommitteeDAO committeeDAO;
	
	@Autowired
	private MasPosfixDAO masPosfixDAO;
	
	@Autowired
	private MasPrefixDAO masPrefixDAO;

	@Autowired
	RegistrationDAO registrationDAO;
	
	@Autowired
	TraderDAO traderDAO;
	@Autowired
	PersonDAO personDAO;
	public List getAll(Object object , User user) throws Exception {
		if(!(object instanceof CommitteeDTO))
		{
			throw new IllegalArgumentException("object not instanceof CommitteeDTO");
		}
		CommitteeDTO param = (CommitteeDTO)object;
		
		List<CommitteeDTO> list = new ArrayList<CommitteeDTO>();
		Person person = (Person) personDAO.findByPrimaryKey(param.getPersonId());
	

		List<Committee> listAll = this.committeeDAO.findAllByPerson(person.getPersonId(), null, person.getRecordStatus());
		
		if(!listAll.isEmpty())
		{
			for(Committee comm: listAll)
			{
				CommitteeDTO dto = new CommitteeDTO();
				
				ObjectUtil.copy(comm, dto);
				
				if((null != dto.getCommitteePersonType()) && (!"".equals(dto.getCommitteePersonType())))
				{
					
					dto.setCommitteePersonTypeName(PersonType.getMeaning(dto.getCommitteePersonType()));
				}
				if((null != dto.getCommitteeType()) && (!"".equals(dto.getCommitteeType())))
				{
					dto.setCommitteeTypeName(CommitteeType.getMeaning(dto.getCommitteeType()));
				}
				
				if(comm.getMasPrefix() != null)
				{
					dto.setPrefixId(comm.getMasPrefix().getPrefixId());
					dto.setPrefixName(comm.getMasPrefix().getPrefixName());
				}
				if(comm.getMasAmphurByAmphurId() != null)
				{
					dto.setAmphurId(comm.getMasAmphurByAmphurId().getAmphurId());
					dto.setAmphurName(comm.getMasAmphurByAmphurId().getAmphurName());
				
					
				}
				if(comm.getMasAmphurByTaxAmphurId() != null)
				{
					dto.setTaxAmphurId(comm.getMasAmphurByTaxAmphurId().getAmphurId());
					dto.setTaxAmphurName(comm.getMasAmphurByTaxAmphurId().getAmphurName());
				}
				if(comm.getMasProvinceByProvinceId() != null)
				{
					dto.setProvinceId(comm.getMasProvinceByProvinceId().getProvinceId());
					dto.setProvinceName(comm.getMasProvinceByProvinceId().getProvinceName());
				}
				if(comm.getMasProvinceByTaxProvinceId() != null)
				{
					dto.setTaxProvinceId(comm.getMasProvinceByTaxProvinceId().getProvinceId());
					dto.setTaxProvinceName(comm.getMasProvinceByTaxProvinceId().getProvinceName());
				}
				if(comm.getPerson() != null)
				{
					dto.setPersonId(comm.getPerson().getPersonId());
				}
				if(comm.getMasPosfix() != null)
				{
					dto.setPostfixId(comm.getMasPosfix().getPostfixId());
					dto.setPostfixName(comm.getMasPosfix().getPostfixName());
				}
					
				list.add(dto);
			}
		}
		
		return list;
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List create(Object[] object , User user) throws Exception {
		List<CommitteeDTO> list = new ArrayList<CommitteeDTO>();
		for (Object obj : object) {
			CommitteeDTO dto = (CommitteeDTO) obj;
			
			//ตรวจสอบข้อมูลกรรมการอยู่ระหว่างพักใช้ หรือ เพิกถอน
			
			Committee commttee = new Committee();
			
			ObjectUtil.copy(dto, commttee);
			
			RegistrationDTO param = new RegistrationDTO();
			
			if(dto.getCommitteeIdentityNo() != null)
			{
				param.setIdentityNo(dto.getCommitteeIdentityNo());
			}
			
			if(dto.getCommitteePersonType() !=null)
			{
				param.setCommitteePersonType(dto.getCommitteePersonType());
			}
			List<Object[]> listObjS = (List<Object[]>)this.registrationDAO.findCheckIdentityNoRegistrationPCMRevokeSuspend(param, user);
			DateFormat ft = DateUtils.getProcessDateFormatThai();
			Date date = new  Date();
			long num = 0;
			Date commitExpireDate = new Date();
			if(listObjS != null && !listObjS.isEmpty())
			{
				for(Object[] sel: listObjS)
				{
					if(sel[15]!=null)
					{
						commttee.setPunishmentDate((Date)sel[15]);
					}
					
					if(sel[16] !=null)
					{
						 commitExpireDate = (Date)(sel[16]);
						 num = DateUtils.DateDiff(date, commitExpireDate);
						 commttee.setPunishmentExpireDate((Date)sel[15]);
					}
					
					if(sel[11] !=null)
					{
						if(sel[11].toString().equals("S"))
						{
							commttee.setCommitteeStatus(sel[11].toString());
							//break;
						}else if(sel[11].toString().equals("R"))
						{
							commttee.setCommitteeStatus(sel[11].toString());
							//break;
						}
					}
				}
			}
			if(num > 0)
			{
				commttee.setCommitteeStatus("N");
			}
			
			if(dto.getPrefixId() > 0)
			{
				MasPrefix prefix = new MasPrefix();
				prefix.setPrefixId(dto.getPrefixId());
				commttee.setMasPrefix(prefix);
			}
			
			if(dto.getPostfixId() > 0)
			{
				MasPosfix posfix = new MasPosfix();
				posfix.setPostfixId(dto.getPostfixId());
				commttee.setMasPosfix(posfix);
			}
			
			if(dto.getPersonId() > 0)
			{
				Person person = new Person();
				person.setPersonId(dto.getPersonId());
				
				commttee.setPerson(person);
			}
			
			if(dto.getAmphurId() > 0)
			{
				MasAmphur masAmphur = new MasAmphur();
				masAmphur.setAmphurId(dto.getAmphurId());
				
				commttee.setMasAmphurByAmphurId(masAmphur);
			}
			
			if(dto.getTaxAmphurId() > 0)
			{
				MasAmphur masAmphur = new MasAmphur();
				masAmphur.setAmphurId(dto.getTaxAmphurId());
				
				commttee.setMasAmphurByTaxAmphurId(masAmphur);
			}
			if(dto.getProvinceId() > 0)
			{
				MasProvince masProvince = new MasProvince();
				masProvince.setProvinceId(dto.getProvinceId());
				
				commttee.setMasProvinceByProvinceId(masProvince);
			}
			if(dto.getTaxProvinceId() > 0)
			{
				MasProvince masProvince = new MasProvince();
				masProvince.setProvinceId(dto.getProvinceId());
				
				commttee.setMasProvinceByTaxProvinceId(masProvince);
			}
			
			commttee.setRecordStatus(RecordStatus.TEMP.getStatus());
			commttee.setCreateUser(user.getUserName());
			commttee.setCreateDtm(new Date());
			
			Long committeeId = (Long) this.committeeDAO.insert(commttee);
			commttee.setCommitteeId(committeeId);
			dto.setCommitteeId(committeeId);
			
			dto.setCommitteeStatus(commttee.getCommitteeStatus());
			

			list.add(dto);
		}

		return list;
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List update(Object[] object , User user) throws Exception {
		List<CommitteeDTO> list = new ArrayList<CommitteeDTO>();
		for (Object obj : object) {
			CommitteeDTO dto = (CommitteeDTO) obj;
			////ตรวจสอบข้อมูลกรรมการอยู่ระหว่างพักใช้ หรือ เพิกถอน
			Committee commttee = (Committee)committeeDAO.findByPrimaryKey(dto.getCommitteeId());
			
			Date date = new  Date();
			long num = 0;
			Date commitExpireDate = new Date();
			
			if(commttee.getPunishmentExpireDate() !=null)
			{
				commitExpireDate = commttee.getPunishmentExpireDate();
				num = DateUtils.DateDiff(date, commitExpireDate);
				System.out.println(num);
			}
			
			if(num > 0)
			{
				commttee.setCommitteeStatus("N");
			}
			
			if(!commttee.getCommitteeStatus().equals("N"))
			{
				dto.setCommitteeStatus(commttee.getCommitteeStatus());
			}
			
			ObjectUtil.copy(dto, commttee);
			//commttee.getCommitteeStatus();
			System.out.println(commttee.getCommitteeStatus());
			if(dto.getPrefixId() > 0)
			{
				MasPrefix prefix = new MasPrefix();
				prefix.setPrefixId(dto.getPrefixId());
				commttee.setMasPrefix(prefix);
			}
			
			if(dto.getPostfixId() > 0)
			{
				MasPosfix posfix = new MasPosfix();
				posfix.setPostfixId(dto.getPostfixId());
				commttee.setMasPosfix(posfix);
			}
			
			if(dto.getPersonId() > 0)
			{
				Person person = new Person();
				person.setPersonId(dto.getPersonId());
				
				commttee.setPerson(person);
			}
			
			if(dto.getAmphurId() > 0)
			{
				MasAmphur masAmphur = new MasAmphur();
				masAmphur.setAmphurId(dto.getAmphurId());
				
				commttee.setMasAmphurByAmphurId(masAmphur);
			}
			
			if(dto.getTaxAmphurId() > 0)
			{
				MasAmphur masAmphur = new MasAmphur();
				masAmphur.setAmphurId(dto.getTaxAmphurId());
				
				commttee.setMasAmphurByTaxAmphurId(masAmphur);
			}
			if(dto.getProvinceId() > 0)
			{
				MasProvince masProvince = new MasProvince();
				masProvince.setProvinceId(dto.getProvinceId());
				
				commttee.setMasProvinceByProvinceId(masProvince);
			}
			if(dto.getTaxProvinceId() > 0)
			{
				MasProvince masProvince = new MasProvince();
				masProvince.setProvinceId(dto.getProvinceId());
				
				commttee.setMasProvinceByTaxProvinceId(masProvince);
			}
			
			commttee.setRecordStatus(RecordStatus.TEMP.getStatus());
			commttee.setLastUpdUser(user.getUserName());
			
			committeeDAO.update(commttee);
			list.add(dto);
		}
		return list;
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(Object[] object , User user) throws Exception {
		
		for (Object obj : object) {
			CommitteeDTO dto = (CommitteeDTO) obj;
			Committee commttee = (Committee)committeeDAO.findByPrimaryKey(dto.getCommitteeId());
			committeeDAO.delete(commttee);
			
		}
		
	}
}
