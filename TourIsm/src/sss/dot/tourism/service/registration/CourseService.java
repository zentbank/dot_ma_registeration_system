package sss.dot.tourism.service.registration;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.mas.MasUniversityDAO;
import sss.dot.tourism.dao.training.EducationDAO;
import sss.dot.tourism.dao.training.PersonTrainedDAO;
import sss.dot.tourism.domain.Country;
import sss.dot.tourism.domain.Education;
import sss.dot.tourism.domain.MasAmphur;
import sss.dot.tourism.domain.MasPrefix;
import sss.dot.tourism.domain.MasProvince;
import sss.dot.tourism.domain.MasUniversity;
import sss.dot.tourism.domain.Person;
import sss.dot.tourism.domain.PersonTrained;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.dto.training.CourseDTO;
import sss.dot.tourism.dto.training.EducationDTO;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.DateUtils;

import com.sss.aut.service.User;

@Repository("courseService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class CourseService implements ICourseService {

	@Autowired
	private EducationDAO educationDAO;
	@Autowired
	private MasUniversityDAO masUniversityDAO;
	@Autowired
	private PersonTrainedDAO personTrainedDAO;
	
	public List countCourseAll(Object object, User user ,int start, int limit) throws Exception
	{
		EducationDTO params = (EducationDTO) object;
		return this.educationDAO.findEducationCourseAll(params, user , start, limit);		
	}

	public List getAll(Object object, User user ,int start, int limit) throws Exception {
		
		if(!(object instanceof EducationDTO))
		{
			throw new IllegalArgumentException("object not instanceof EducationDTO");
		}
		
		List<EducationDTO> list = new ArrayList<EducationDTO>();
		EducationDTO params = (EducationDTO) object;
		DateFormat ft =  DateUtils.getProcessDateFormatThai();
		List<Object[]> listObj = this.educationDAO.findEducationCourseAll(params, user , start, limit);
		
		if(!listObj.isEmpty())
		{
			for(Object[] sel: listObj)
			{
				EducationDTO dto = new EducationDTO();
				
				dto.setEduId(Long.valueOf(sel[0].toString()));
				if(sel[1] !=null)
				{
					dto.setStudyDate(ft.format((Date)sel[1]));
				}
				if(sel[2] !=null)
				{
					dto.setGraduationDate(ft.format((Date)sel[2]));
				}
				if(sel[3] !=null)
				{
					dto.setGraduationCourse(sel[3].toString());
				}
				if(sel[4] !=null)
				{
					dto.setMasUniversityId(Long.valueOf(sel[4].toString()));
					List<MasUniversity> listUni =  masUniversityDAO.findUniversityName(dto.getMasUniversityId());
					MasUniversity university = listUni.get(0);
					
					String universityName =  university.getUniversityName();
					dto.setUniversityName(universityName);
				}
				
				
				
				if(sel[5] !=null)
				{
					dto.setGenerationGraduate(Integer.valueOf(sel[5].toString()));
				}
				
				Education edu = (Education)this.educationDAO.findByPrimaryKey(dto.getEduId());
				if(edu.getCountry() != null)
				{
					dto.setCountryId(edu.getCountry().getCountryId());
				}
				
				list.add(dto);
				
			}
		}
		
		
		return list;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void create(Object object, User user) throws Exception {
		
		if(!(object instanceof EducationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถ เพิ่มหลักสูตรฝึกอบรมใหม่ได้");
		}
			
		   EducationDTO params = (EducationDTO) object;
			
		   System.out.println(params.getMasUniversityId());
			Education education = new Education();
			
			ObjectUtil.copy(params, education);
			
			if(params.getMasUniversityId() > 0)
			{
				MasUniversity masUniversity = new MasUniversity();
				masUniversity.setMasUniversityId(params.getMasUniversityId());
				education.setMasUniversity(masUniversity);
			}
			
			if(params.getCountryId() > 0)
			{
				Country co = new Country();
				co.setCountryId(params.getCountryId());
				education.setCountry(co);
			}
			
			education.setRecordStatus("E");
			education.setCreateUser(user.getUserName());
			education.setCreateDtm(new Date());

			Long eduId = (Long)educationDAO.insert(education);
			params.setEduId(eduId);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void update(Object object, User user) throws Exception {
		
		if(!(object instanceof EducationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถ แก้ไขหลักสูตรฝึกอบรม");
		}
		
		EducationDTO params = (EducationDTO)object;
		

		Education education = (Education)this.educationDAO.findByPrimaryKey(params.getEduId());
				
		ObjectUtil.copy(params, education);
		
		if(params.getMasUniversityId() > 0)
		{
			MasUniversity masUniversity = new MasUniversity();
			masUniversity.setMasUniversityId(params.getMasUniversityId());
			education.setMasUniversity(masUniversity);
		}
		
		if(params.getCountryId() > 0)
		{
			Country co = new Country();
			co.setCountryId(params.getCountryId());
			education.setCountry(co);
		}
		
		education.setLastUpdUser(user.getUserName());
		education.setLastUpdDtm(new Date());

		educationDAO.update(education);
	
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(Object[] object, User user) throws Exception {
		
		for(Object obj : object)
		{
			EducationDTO dto = (EducationDTO) obj;
			Education edu = (Education)educationDAO.findByPrimaryKey(dto.getEduId());
			educationDAO.delete(edu);
		}
	}

	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void createPersonTrained(Object object, User user) throws Exception {
		
		if(!(object instanceof EducationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถ เพิ่มหลักสูตรบุคคลในหลักสูตรฝึกอบรมนี้ใหม่ได้");
		}
		
		EducationDTO params = (EducationDTO) object;
		
		PersonTrained personTrained = new PersonTrained();
		
		ObjectUtil.copy(params, personTrained);
		
		
		if(params.getAmphurId() > 0)
		{
			MasAmphur amphur = new MasAmphur();
			amphur.setAmphurId(params.getAmphurId());
			personTrained.setMasAmphurByAmphurId(amphur);
		}
		
		if(params.getProvinceId() > 0)
		{
			MasProvince province = new MasProvince();
			province.setProvinceId(params.getProvinceId());
			personTrained.setMasProvinceByProvinceId(province);
		}
		
		if(params.getPrefixId() > 0)
		{
			MasPrefix prefix = new MasPrefix();
			prefix.setPrefixId(params.getPrefixId());
			personTrained.setMasPrefix(prefix);
		}
		
		if(params.getEduId() > 0)
		{
			Education education = new Education();
			education.setEduId(params.getEduId());
			personTrained.setEducation(education);
		}
		
		personTrained.setPersonNationality("ไทย");
		personTrained.setRecordStatus("N");
		personTrained.setCreateUser(user.getUserName());
		personTrained.setCreateDtm(new Date());
		
		Long personTrainedId = (Long)personTrainedDAO.insert(personTrained);
		params.setPersonTrainedId(personTrainedId);		
	}

	public List countPersonAll(Object object, User user, int start, int limit)
			throws Exception
	{
		
		EducationDTO params = (EducationDTO) object;

		return this.personTrainedDAO.findPersonTrainedAll(params, user , start, limit);
	}

	public List getPersonAll(Object object, User user, int start, int limit)
			throws Exception {
		if(!(object instanceof EducationDTO))
		{
			throw new IllegalArgumentException("object not instanceof EducationDTO");
		}
		
		List<EducationDTO> list = new ArrayList<EducationDTO>();
		EducationDTO params = (EducationDTO) object;
		DateFormat ft =  DateUtils.getProcessDateFormatThai();
		List<Object[]> listObj = this.personTrainedDAO.findPersonTrainedAll(params, user , start, limit);
		
		String fullName = null; 
		String courseName = null;
		
		if(!listObj.isEmpty())
		{
			for(Object[] sel: listObj)
			{
				EducationDTO dto = new EducationDTO();
				if(sel[0]!=null)
				{
					dto.setPersonTrainedId(Long.valueOf(sel[0].toString()));
				}
				if(sel[1] !=null)
				{
					dto.setIdentityNo(sel[1].toString());
				}
				if(sel[2] !=null)
				{
					dto.setEduId(Long.valueOf(sel[2].toString()));
					Education edu = (Education)this.educationDAO.findByPrimaryKey(dto.getEduId());
					EducationDTO param = new EducationDTO();					
					ObjectUtil.copy(edu, param);					
					courseName = param.getGraduationCourse();					
				}
				if(sel[3] !=null)
				{
					dto.setGender(sel[3].toString());
				}
				if(sel[4] !=null)
				{
					dto.setPassportNo(sel[4].toString());
				}
				if(sel[5] !=null)
				{
					dto.setPrefixName(sel[5].toString());
					fullName = dto.getPrefixName();
				}
				if(sel[6] !=null)
				{
					dto.setFirstName(sel[6].toString());
					if(fullName!=null)
					{
						fullName = fullName.concat(" "+dto.getFirstName());
					}else
					{
						fullName = dto.getFirstName();
					}
				}
				if(sel[7] !=null)
				{
					dto.setLastName(sel[7].toString());
					if(fullName!=null)
					{
						fullName = fullName.concat(" "+dto.getLastName());
					}else
					{
						fullName = dto.getLastName();
					}
				}
				if(sel[8] !=null)
				{
					dto.setFirstNameEn(sel[8].toString());
				}
				if(sel[9] !=null)
				{
					dto.setLastNameEn(sel[9].toString());
				}
				if(sel[10] !=null)
				{
					dto.setAgeYear(Integer.valueOf(sel[10].toString()));
				}
				if(sel[11] !=null)
				{
					dto.setIdentityNoExpire(ft.format((Date)sel[11]));
				}
				if(sel[12] !=null)
				{
					dto.setProvinceName(sel[12].toString());
				}
				if(sel[13] !=null)
				{
					dto.setAmphurName(sel[13].toString());
				}
				if(sel[14] !=null)
				{
					dto.setStudyDate(ft.format((Date)sel[14]));
				}
				if(sel[15] !=null)
				{
					dto.setGraduationDate(ft.format((Date)sel[15]));
				}
				if(sel[16] !=null)
				{
					dto.setGraduationCourse(sel[16].toString());
				}
				if(sel[17] !=null)
				{
					dto.setMasUniversityId(Long.valueOf(sel[17].toString()));
					List<MasUniversity> listUni =  masUniversityDAO.findUniversityName(dto.getMasUniversityId());
					MasUniversity university = listUni.get(0);
					
					String universityName =  university.getUniversityName();
					dto.setUniversityName(universityName);
				}				
				if(sel[18] !=null)
				{
					dto.setGenerationGraduate(Integer.valueOf(sel[18].toString()));
					courseName = courseName.concat(" รุ่นที่ "+dto.getGenerationGraduate());	
				}
				if(sel[19] !=null)
				{
					dto.setProvinceId(Long.valueOf(sel[19].toString()));
				}if(sel[20] !=null)
				{
					dto.setAmphurId(Long.valueOf(sel[20].toString()));
				}if(sel[21] !=null)
				{
					dto.setPrefixId(Long.valueOf(sel[21].toString()));
				}
				if(sel[22] !=null)
				{
					dto.setBirthDate(ft.format((Date)sel[22]));
				}
								
				if(fullName != null)
				{
					dto.setFullName(fullName);
				}
				
				if(courseName !=null)
				{
					dto.setCourseName(courseName);
				}
				
				courseName = "";
				fullName = "";
				list.add(dto);
				
			}
		}
		
		
		return list;
	}


	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void updatePersonTrained(Object object, User user) throws Exception {
		
		if(!(object instanceof EducationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถ เพิ่มหลักสูตรบุคคลในหลักสูตรฝึกอบรมนี้ใหม่ได้");
		}
		
		EducationDTO params = (EducationDTO) object;
		
		PersonTrained personTrained = new PersonTrained();
		
		personTrained = (PersonTrained)this.personTrainedDAO.findByPrimaryKey(params.getPersonTrainedId());
		ObjectUtil.copy(params, personTrained);
		
		
		if(params.getAmphurId() > 0)
		{
			MasAmphur amphur = new MasAmphur();
			amphur.setAmphurId(params.getAmphurId());
			personTrained.setMasAmphurByAmphurId(amphur);
		}
		
		if(params.getProvinceId() > 0)
		{
			MasProvince province = new MasProvince();
			province.setProvinceId(params.getProvinceId());
			personTrained.setMasProvinceByProvinceId(province);
		}
		
		if(params.getPrefixId() > 0)
		{
			MasPrefix prefix = new MasPrefix();
			prefix.setPrefixId(params.getPrefixId());
			personTrained.setMasPrefix(prefix);
		}
		
		if(params.getEduId() > 0)
		{
			Education education = new Education();
			education.setEduId(params.getEduId());
			personTrained.setEducation(education);
		}
		
		
		personTrained.setLastUpdUser(user.getUserName());
		personTrained.setLastUpdDtm(new Date());
		
		personTrainedDAO.update(personTrained);
			
	}


	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void deletePersonTrained(Object[] object, User user)
			throws Exception {		
		for(Object obj : object)
		{
			EducationDTO dto = (EducationDTO) obj;
			PersonTrained personTrained = (PersonTrained) this.personTrainedDAO.findByPrimaryKey(dto.getPersonTrainedId());
			personTrainedDAO.delete(personTrained);
		}
		
	}
	
}
