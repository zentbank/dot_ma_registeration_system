package sss.dot.tourism.service.registration;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.mas.OrganizationDAO;
import sss.dot.tourism.dao.mas.RegisterFlowDAO;
import sss.dot.tourism.dao.registration.PrintingCardDAO;
import sss.dot.tourism.dao.registration.PrintingLicenseDAO;
import sss.dot.tourism.dao.registration.RegisterProgressDAO;
import sss.dot.tourism.dao.registration.RegistrationDAO;
import sss.dot.tourism.dao.registration.TraderAddressDAO;
import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.domain.AdmUser;
import sss.dot.tourism.domain.Person;
import sss.dot.tourism.domain.RegisterFlow;
import sss.dot.tourism.domain.RegisterProgress;
import sss.dot.tourism.domain.Registration;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.domain.TraderAddress;
import sss.dot.tourism.dto.mas.EmailDTO;
import sss.dot.tourism.dto.registration.RegisterProgressDTO;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.service.trader.ITourLeaderService;
import sss.dot.tourism.util.ConstantUtil;
import sss.dot.tourism.util.MailService;
import sss.dot.tourism.util.PersonType;
import sss.dot.tourism.util.ProgressStatus;
import sss.dot.tourism.util.PropertyLoader;
import sss.dot.tourism.util.RecordStatus;
import sss.dot.tourism.util.RegistrationType;
import sss.dot.tourism.util.RoleStatus;
import sss.dot.tourism.util.SendMailThread;
import sss.dot.tourism.util.TraderAddressType;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.User;

@Repository("directorService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class DirectorService implements IApprovalProcessService {

	@Autowired
	TraderDAO traderDAO;
	@Autowired
	RegisterProgressDAO registerProgressDAO;
	@Autowired
	RegistrationDAO registrationDAO;
	@Autowired
	ITourLeaderService tourLeaderService;
	@Autowired
	RegisterFlowDAO registerFlowDAO;
	@Autowired
	IRegistrationService registrationService;
	@Autowired
	IRegisterProgressService registerProgressService;
	@Autowired
	IApproveRegistration approveRegistrationService;
	@Autowired
	PrintingLicenseDAO printingLicenseDAO;
	@Autowired
	PrintingCardDAO printingCardDAO;

	@Autowired
	TraderAddressDAO traderAddressDAO;
	
	@Autowired
	OrganizationDAO organizationDAO;

	private final String REGISTER_USER = "ONLINE";

	private String PREVIOUS_ROLE = "'KEY','LAW','CHK','SUP','MAN'";
	private String NEXT_ROLE = "'ACC'";
	
	
	private Properties resources;
	  protected Properties getResources()
	  {
	    if(this.resources == null)
	    {
	      this.resources = PropertyLoader.loadProperties("sss/dot/tourism/util/ApplicationResources.properties");
	    }
	    return this.resources;
	  }

	public List getTraderBetweenRegistration(Object object, User user,
			int start, int limit) throws Exception {
		if (!(object instanceof RegistrationDTO)) {
			throw new IllegalArgumentException(
					"ไม่สามารถโหลดข้อมูลได้การจดทะเบียนได้");
		}

		RegistrationDTO params = (RegistrationDTO) object;
		if ((null != params.getRegProgressStatus())
				&& (!"".equals(params.getRegProgressStatus()))) {
			// w ยังไม่ส่งเรื่อง
			if (params.getRegProgressStatus().equals(
					ProgressStatus.WAITING.getStatus())) {
				params.setRegGroupProgress(null);
			} else if (params.getRegProgressStatus().equals(
					ProgressStatus.ACCEPT.getStatus())) {
				params.setRegGroupProgress(this.NEXT_ROLE);

			} else if (params.getRegProgressStatus().equals(
					ProgressStatus.GET_IT.getStatus())) {
				params.setRegGroupProgress(PREVIOUS_ROLE);
			}
		}
		params.setRegistrationProgress(RoleStatus.DIRECTOR.getStatus());

		return registrationService.getTraderBetweenRegistrationPaging(object,
				user, start, limit);
	}

	public List countTraderBetweenRegistration(Object object, User user,
			int start, int limit) throws Exception {
		if (!(object instanceof RegistrationDTO)) {
			throw new IllegalArgumentException(
					"ไม่สามารถโหลดข้อมูลได้การจดทะเบียนได้");
		}

		RegistrationDTO params = (RegistrationDTO) object;
		if ((null != params.getRegProgressStatus())
				&& (!"".equals(params.getRegProgressStatus()))) {
			// w ยังไม่ส่งเรื่อง
			if (params.getRegProgressStatus().equals(
					ProgressStatus.WAITING.getStatus())) {
				params.setRegGroupProgress(null);
			} else if (params.getRegProgressStatus().equals(
					ProgressStatus.ACCEPT.getStatus())) {
				params.setRegGroupProgress(this.NEXT_ROLE);

			} else if (params.getRegProgressStatus().equals(
					ProgressStatus.GET_IT.getStatus())) {
				params.setRegGroupProgress(PREVIOUS_ROLE);
			}
		}
		params.setRegistrationProgress(RoleStatus.DIRECTOR.getStatus());

		return registrationService.countTraderBetweenRegistrationPaging(object,
				user, start, limit);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void verification(Object object, User user) throws Exception {

		if (!(object instanceof RegisterProgressDTO)) {
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้้");
		}
		RegisterProgressDTO param = (RegisterProgressDTO) object;

		Registration reg = (Registration) this.registrationDAO
				.findByPrimaryKey(param.getRegId());
		Trader trader = reg.getTrader();
		Person person = trader.getPerson();

		if (RegistrationType.isPayFee(trader.getTraderType(),
				reg.getRegistrationType())) {

			registerProgressService.verification(object, user,
					RoleStatus.DIRECTOR.getStatus());

		} else {
			this.approveRegistrationService.approve(reg, user);
			this.updateProgress(object, user, RoleStatus.DIRECTOR.getStatus());
			
		}
		
		// if register on line send mail
		List<Object[]> listTrFee = this.registrationDAO.listDotTrLicense(reg.getRegId());
		if(CollectionUtils.isNotEmpty(listTrFee)){

			StringBuffer mailContent = new StringBuffer(this.getResources().getProperty("inform.mail.payment"));
			if (RegistrationType.isPayFee(trader.getTraderType(),
					reg.getRegistrationType())) {

				mailContent = new StringBuffer(this.getResources().getProperty("inform.mail.payment"));
				this.registerProgressDAO.executeUpdate("UPDATE DOT_TR_FEE_LICENSE SET LICENSE_FEE_STATUS = '7' ,LAST_UPD_USER = '"+user.getUserName()+"' ,LAST_UPD_DTM = GETDATE() WHERE REG_ID = " + reg.getRegId(), null);

			} else {
				if(TraderType.GUIDE.getStatus().equals(trader.getTraderType())){
					mailContent = new StringBuffer(this.resources.getProperty("inform.mail.approve.guide"));
				}else{
					mailContent = new StringBuffer(this.resources.getProperty("inform.mail.approve"));
				}
				this.registerProgressDAO.executeUpdate("UPDATE DOT_TR_FEE_LICENSE SET LICENSE_FEE_STATUS = '9' ,LAST_UPD_USER = '"+user.getUserName()+"' ,LAST_UPD_DTM = GETDATE() WHERE REG_ID = " + reg.getRegId(), null);
			}
			
			String personName = "";
			if(PersonType.CORPORATE.getStatus().equals(person.getPersonType()))
			{					
				String traderOwnerName = person.getMasPrefix()==null?"":person.getMasPrefix().getPrefixName() + person.getFirstName() + (person.getMasPosfix()==null?"":person.getMasPosfix().getPostfixName());
				personName = "กรรมการผู้จัดการ " + traderOwnerName;
			}
			else
			{
				String traderOwnerName =  person.getMasPrefix()==null?"":person.getMasPrefix().getPrefixName() + person.getFirstName() + " " + person.getLastName();
				personName = traderOwnerName;
			}
			
			String email = "";
			List<TraderAddress> listAddress = this.traderAddressDAO.findAllByTrader(trader.getTraderId(), TraderAddressType.OFFICE_ADDRESS.getStatus(), null);
			if(CollectionUtils.isNotEmpty(listAddress)){
				TraderAddress addr = listAddress.get(0);
				if(StringUtils.isNotEmpty(addr.getEmail())){
					email = addr.getEmail();
				}else{
					List<TraderAddress> listAddressShop = this.traderAddressDAO.findAllByTrader(trader.getTraderId(), TraderAddressType.SHOP_ADDRESS.getStatus(), null);
					if(CollectionUtils.isNotEmpty(listAddressShop)){
						if(StringUtils.isNotEmpty(addr.getEmail())){
							email = addr.getEmail();
						}else{
							email = "licene.tourism.go.th@gmail.com";
						}
					}
				}
			}
			
			String orgName = reg.getOrganization().getOrgFullName();
			String orgAddress = reg.getOrganization().getOrgAddress();
			
			String registrationType = "";
			if(StringUtils.isNotEmpty(reg.getRegistrationType())){
				registrationType = RegistrationType.getMeaning(trader.getTraderType(), reg.getRegistrationType());
			}
			
			StringBuffer regType = new StringBuffer();
			if (RegistrationType.isPayFee(trader.getTraderType(),
					reg.getRegistrationType())) {

				regType.append(registrationType);

			} else {
				regType.append(registrationType);
			}
			regType.append(" ใบอนุญาต" + TraderType.getMeaning(trader.getTraderType()));
			regType.append(" คำขอเลขที่ " + reg.getRegistrationNo());
			
			
			
			
			StringBuffer megContent = new StringBuffer();
			megContent.append(MessageFormat.format(mailContent.toString(), regType.toString(), personName, orgName,orgName + " " + orgAddress));
			
			MailService mailThred = new MailService( email, registrationType, megContent);
			mailThred.run();
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void updateProgress(Object object, User user, String roleStatus)
			throws Exception {
		if (!(object instanceof RegisterProgressDTO)) {
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้้");
		}

		RegisterProgressDTO param = (RegisterProgressDTO) object;

		Registration reg = (Registration) this.registrationDAO
				.findByPrimaryKey(param.getRegId());

		// find law
		List<RegisterProgress> listProg = this.registerProgressDAO
				.findRoleProgressbyRegistration(reg.getRegId(), roleStatus);

		Calendar cal = Calendar.getInstance();

		if (!listProg.isEmpty()) {
			RegisterProgress progress = listProg.get(0);

			AdmUser admUser = new AdmUser();
			admUser.setUserId(user.getUserId());
			progress.setAdmUser(admUser);
			// progress.setProgress(RoleStatus.KEY.getStatus());
			progress.setProgressDate(cal.getTime());
			// String positionName =
			// user.getUserData().getPositionName()==null?"":user.getUserData().getPositionName();
			progress.setProgressDesc(roleStatus + " " + param.getProgressDesc() == null ? ""
					: param.getProgressDesc());
			progress.setProgressStatus(param.getProgressStatus());
			progress.setLastUpdUser(user.getUserName());
			progress.setLastUpdDtm(new Date());

			registerProgressDAO.update(progress);
		}

		// get next role
		RegisterFlow flow = registerFlowDAO.findNextRole(roleStatus);

		// update registration next role

		reg.setLastUpdUser(user.getUserName());
		reg.setLastUpdDtm(new Date());

		// insert register progress next role waiting

		RegisterProgress nextProgress = new RegisterProgress();
		nextProgress.setRegistration(reg);

		cal.add(Calendar.MINUTE, 10);
		nextProgress.setProgressDate(cal.getTime());

		if (ProgressStatus.ACCEPT.getStatus().equals(param.getProgressStatus())) {
			// insert register progress next role waiting
			// ส่งไป print license
			RegisterProgress printLicenseProgress = new RegisterProgress();
			printLicenseProgress.setRegistration(reg);

			cal.add(Calendar.MINUTE, 10);
			printLicenseProgress.setProgressDate(cal.getTime());

			reg.setRegistrationProgress(RoleStatus.PRINT_LICENSE.getStatus());
			printLicenseProgress.setProgress(RoleStatus.PRINT_LICENSE
					.getStatus());
			printLicenseProgress.setProgressDesc(RoleStatus.PRINT_LICENSE
					.getMeaning() + " " + ProgressStatus.WAITING.getMeaning());
			printLicenseProgress.setProgressStatus(ProgressStatus.WAITING
					.getStatus());
			registerProgressDAO.insert(printLicenseProgress);

			// notification to print

			if (!TraderType.TOUR_COMPANIES.getStatus().equals(
					reg.getTrader().getTraderType())) {
				// print card
				RegisterProgress printCardProgress = new RegisterProgress();
				printCardProgress.setRegistration(reg);
				reg.setRegistrationProgress(RoleStatus.PRINT_CARD.getStatus());
				printCardProgress
						.setProgress(RoleStatus.PRINT_CARD.getStatus());
				printCardProgress.setProgressDesc(RoleStatus.PRINT_CARD
						.getMeaning()
						+ " "
						+ ProgressStatus.WAITING.getMeaning());
				printCardProgress.setProgressStatus(ProgressStatus.WAITING
						.getStatus());
				printCardProgress.setProgressDate(cal.getTime());
				registerProgressDAO.insert(printCardProgress);

				// notification to print
			}
		} else {
			// ตีเรื่องกลับ
			reg.setRegistrationProgress(flow.getPreviousRole());
			nextProgress.setProgress(flow.getPreviousRole());
			// nextProgress.setProgressDesc(RoleStatus.LAW.getMeaning()+" "+param.getProgressDesc()==null?"ตีเรื่องกลับ":param.getProgressDesc());
			nextProgress.setProgressDesc(RoleStatus.getMeaning(flow
					.getPreviousRole())
					+ " "
					+ ProgressStatus.WAITING.getMeaning());
			nextProgress.setProgressStatus(ProgressStatus.WAITING.getStatus());
			nextProgress.setProgressBackStatus(ProgressStatus.BACK.getStatus());

			// TODO Notification to previous role
		}
		registerProgressDAO.insert(nextProgress);

		registrationDAO.update(reg);

	}

	public void sendApproveMail(Registration reg) throws Exception {
		Trader trader = reg.getTrader();

		Person person = trader.getPerson();

		List<TraderAddress> listAddr = (List<TraderAddress>) traderAddressDAO
				.findAllByTrader(trader.getTraderId(), null,
						RecordStatus.TEMP.getStatus());

		List<EmailDTO> emails = new ArrayList<EmailDTO>();

		String email = ConstantUtil.MAIL_TOURISM;
		if (!listAddr.isEmpty()) {
			TraderAddress address = listAddr.get(0);
			email = address.getEmail();
		}

		BufferedReader reader = new BufferedReader(new InputStreamReader(this
				.getClass().getClassLoader()
				.getResourceAsStream("MailApproveForm.html"), "utf-8"));

		StringBuffer body = new StringBuffer();

		String t = reader.readLine();
		while (t != null) {
			body.append(t);
			t = reader.readLine();
		}

		String traderName = person.getFirstName();
		if (TraderType.TOUR_COMPANIES.getStatus()
				.equals(trader.getTraderType())) {
			traderName = trader.getTraderName();
		} else {
			traderName = person.getMasPrefix() == null ? "" : person
					.getMasPrefix().getPrefixName()
					+ person.getFirstName()
					+ " " + person.getLastName();
		}

		String printPaymentUrl = MessageFormat.format(
				ConstantUtil.PRINT_PAYMENT, reg.getRegId() + "");

		EmailDTO regDTO = new EmailDTO();

		regDTO.setEmail(email);
		String message = MessageFormat.format(body.toString(),
				trader.getLicenseNo(), traderName, printPaymentUrl);
		regDTO.setMessage(message);

		emails.add(regDTO);

		// send mail
		SendMailThread mailThred = new SendMailThread(emails, body);
		mailThred.start();

	}
	
	public static void main(String arg[]){

		Properties resources = PropertyLoader.loadProperties("sss/dot/tourism/util/ApplicationResources.properties");
		StringBuffer mailContent = new StringBuffer(resources.getProperty("inform.mail.payment"));
		mailContent = new StringBuffer(resources.getProperty("inform.mail.payment"));
		
		
		String email = "shaii.kanith@gmail.com";
		
		
		String orgName = "สำนักทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ กรุงเทพมหานคร";
		String orgAddress = "189/27 di";
		
		String registrationType = "คำขอ";
		
		registrationType += "ใบอนุญาต" + "มัคคุเทศก์";
		registrationType += "เลขที่ " + "123456";
		
		
		StringBuffer megContent = new StringBuffer();
		megContent.append(MessageFormat.format(mailContent.toString(), registrationType, "lajsklfjla;", orgName,orgName + " " + orgAddress));
		
		MailService mailThred = new MailService( email, "อนุมัติ คำขอยื่น" + registrationType, megContent);
		mailThred.run();
	}

}
