package sss.dot.tourism.service.registration;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import sss.dot.tourism.dao.file.RegDocumentDAO;
import sss.dot.tourism.dao.mas.MasDocumentDAO;
import sss.dot.tourism.dao.registration.DocumentRegistrationDAO;
import sss.dot.tourism.dao.registration.PersonDAO;
import sss.dot.tourism.dao.registration.RegisterProgressDAO;
import sss.dot.tourism.dao.registration.RegistrationDAO;
import sss.dot.tourism.dao.registration.TraderAddressDAO;
import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.domain.MasDocument;
import sss.dot.tourism.domain.RegDocument;
import sss.dot.tourism.domain.RegisterProgress;
import sss.dot.tourism.domain.Registration;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.dto.mas.MasDocumentDTO;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.service.file.FileManage;
import sss.dot.tourism.service.file.RegDocumentService;
import sss.dot.tourism.service.trader.ITourCompaniesService;
import sss.dot.tourism.service.trader.ITourLeaderService;
import sss.dot.tourism.service.trader.ITraderGuideService;
import sss.dot.tourism.util.ConstantUtil;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.PersonType;
import sss.dot.tourism.util.ProgressStatus;
import sss.dot.tourism.util.RegistrationType;
import sss.dot.tourism.util.TraderCategory;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.User;

@Repository("documentRegistrationService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class DocumentRegistrationService implements IDocumentRegistrationService{

	@Autowired
	DocumentRegistrationDAO documentRegistrationDAO;
	@Autowired
	TraderAddressDAO traderAddressDAO;
	@Autowired
	RegistrationDAO registrationDAO;
	@Autowired
	ITourCompaniesService tourCompaniesService;
	@Autowired
	ITourLeaderService tourLeaderService;
	@Autowired
	ITraderGuideService guideService;
	@Autowired
	RegisterProgressDAO registerProgressDAO;
	@Autowired
	TraderDAO traderDAO;
	@Autowired
	MasDocumentDAO masDocumentDAO;
	@Autowired
	RegDocumentService regDocumentService;
	@Autowired
	RegDocumentDAO regDocumentDAO;
	@Autowired
	PersonDAO personDAO;
	
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List countDocumentRegistrationPaging(Object object, User user,
			int start, int limit) throws Exception
	{
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
		
		RegistrationDTO params = (RegistrationDTO)object;
			
		return this.documentRegistrationDAO.findDocumentRegistrationPaging(params, user , start, limit);
	
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List getDocumentRegistrationPaging(Object object, User user,
			int start, int limit) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("###Service DocumentRegistrationService method getDocumentRegistrationPaging");
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		RegistrationDTO params = (RegistrationDTO)object;
		
		DateFormat ft =  DateUtils.getProcessDateFormatThai();
		
		List<Object[]> listObj = this.documentRegistrationDAO.findDocumentRegistrationPaging(params, user , start, limit);
		
		if(!listObj.isEmpty())
		{
			for(Object[] sel: listObj)
			{
				RegistrationDTO dto = new RegistrationDTO();

//				int i= 0;
//				String result = "";
//				while(i<sel.length)
//				{
//					if(sel[i]!=null)
//						result += ", "+i+":"+sel[i].toString();
//					else
//						result += ", "+i+": null";
//					
//					i++;
//				}
//				System.out.println(result);
				
				dto.setRegId(Long.valueOf(sel[0].toString()));
				dto.setRegistrationNo(sel[1]==null?"":sel[1].toString());
				if(sel[2] != null)
				{
					
					dto.setRegistrationDate(ft.format((Date)sel[2]));
				}
				dto.setRegistrationProgress(sel[3]==null?"":sel[3].toString());
				if(sel[8] != null)
				{
					dto.setTraderType(sel[8]==null?"":sel[8].toString());
					dto.setTraderTypeName(TraderType.getMeaning(dto.getTraderType()));
				}
				if(sel[4]!=null)
				{
					dto.setRegistrationType(sel[4]==null?"":sel[4].toString());
					dto.setRegistrationTypeName(RegistrationType.getMeaning(dto.getTraderType(), dto.getRegistrationType()));
				}
				dto.setOrgId(Long.valueOf(sel[5].toString()));
				dto.setRegRecordStatus(sel[6]==null?"":sel[6].toString());
				dto.setTraderId(Long.valueOf(sel[7].toString()));
				dto.setTraderName(sel[9]==null?"":sel[9].toString());
				dto.setTraderNameEn(sel[10]==null?"":sel[10].toString());
				dto.setTraderCategory(sel[11]==null?"":sel[11].toString());
				
				
				dto.setTraderCategoryName(
						TraderCategory.getTraderCategory(
							dto.getTraderType()
							, dto.getTraderCategory()
						).getMeaning()
				);
				

				
				dto.setPersonId(Long.valueOf(sel[12].toString()));
				dto.setPersonType(sel[13]==null?"":sel[13].toString());
				dto.setPrefixName(sel[14]==null?"":sel[14].toString());
				dto.setFirstName(sel[15]==null?"":sel[15].toString());
				dto.setLastName(sel[16]==null?"":sel[16].toString());
				dto.setPostfixName(sel[17]==null?"":sel[17].toString());
				

				
				if(PersonType.CORPORATE.getStatus().equals(dto.getPersonType()))
				{
					String traderOwnerName = dto.getPrefixName() + dto.getFirstName() + dto.getPostfixName();
					dto.setTraderOwnerName(traderOwnerName);
				}
				else
				{
					String traderOwnerName = dto.getPrefixName() + dto.getFirstName() + " " + dto.getLastName();
					dto.setTraderOwnerName(traderOwnerName);
				}
				
				if(sel[18] != null)
				{
					dto.setPrefixId(Long.valueOf(sel[18].toString()));
				}
				
				if(sel[19] != null)
				{
					dto.setPostfixId(Long.valueOf(sel[19].toString()));
				}
				
				
				if(sel[20] != null)
				{
					dto.setAmphurId(Long.valueOf(sel[20].toString()));
				}
				if(sel[21] != null)
				{
					dto.setTaxAmphurId(Long.valueOf(sel[21].toString()));
				}
				if(sel[22] != null)
				{
					dto.setProvinceId(Long.valueOf(sel[22].toString()));
				}
				if(sel[23] != null)
				{
					dto.setTaxProvinceId(Long.valueOf(sel[23].toString()));
				}
				
				if(sel[24] != null)
				{
					dto.setTraderGuideId(Long.valueOf(sel[24].toString()));
				}
				

				
				dto.setPronunciationName(sel[25]==null?"":sel[25].toString());
			    dto.setLicenseNo(sel[26]==null?"":sel[26].toString());
			    dto.setPersonType(sel[27]==null?"":sel[27].toString());
			    dto.setIdentityNo(sel[28]==null?"":sel[28].toString());
			    dto.setCommitteeName1(sel[29]==null?"":sel[29].toString());
			    dto.setCommitteeName2(sel[30]==null?"":sel[30].toString());
			    dto.setCommitteeNameSign(sel[31]==null?"":sel[31].toString());
			    dto.setPassportNo(sel[32]==null?"":sel[32].toString());
			    dto.setFirstNameEn(sel[33]==null?"":sel[33].toString());
			    dto.setLastNameEn(sel[34]==null?"":sel[34].toString());
			    dto.setGender(sel[35]==null?"":sel[35].toString());
			    dto.setPersonNationality(sel[36]==null?"":sel[36].toString());


			    if(sel[37] != null)
				{
					
					dto.setBirthDate(ft.format((Date)sel[37]));
				}
			    
			    dto.setAgeYear(sel[38]==null?0:Integer.valueOf(sel[38].toString()));
			    if(sel[39] != null)
				{
					
					dto.setIdentityNoExpire(ft.format((Date)sel[39]));
				}
			   
			    dto.setCorporateType(sel[40]==null?"":sel[40].toString());
			    dto.setTaxIdentityNo(sel[41]==null?"":sel[41].toString());
			    if(sel[42] != null)
				{
					
					dto.setIdentityDate(ft.format((Date)sel[42]));
				}
			    dto.setTraderRecordStatus(sel[43]==null?"":sel[43].toString());
			    
			    dto.setAmphurName(sel[44]==null?"":sel[44].toString());
			    dto.setTaxAmphurName(sel[45]==null?"":sel[45].toString());
			    dto.setProvinceName(sel[46]==null?"":sel[46].toString());
			    dto.setTaxProvinceName(sel[47]==null?"":sel[47].toString());
			    dto.setPrefixNameEn(sel[48]==null?"":sel[48].toString());
				
			    Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(dto.getRegId());
				if(TraderType.TOUR_COMPANIES.getStatus().equals(dto.getTraderType()))
				{
					tourCompaniesService.getRegistration(reg, dto, user);
				}
				
				if(TraderType.GUIDE.getStatus().equals(dto.getTraderType()))
				{
					guideService.getRegistration(reg, dto, user);
				}
				
				if(TraderType.LEADER.getStatus().equals(dto.getTraderType()))
				{
					tourLeaderService.getRegistration(reg, dto, user);
				}
				
				List<RegisterProgress> listprogress =  registerProgressDAO.findRegisterProgressbyRegistration(reg.getRegId(), ProgressStatus.GET_IT.getStatus());
				
				if(!listprogress.isEmpty())
				{
					RegisterProgress progress = listprogress.get(0);
					
					String authorityName = progress.getAdmUser().getMasPrefix().getPrefixName() + progress.getAdmUser().getUserName() +" "
							+progress.getAdmUser().getUserLastname();
					
					dto.setAuthorityName(authorityName);
				}
				
				if(dto.getRegistrationProgress().equals(params.getRegistrationProgress()))
				{
					dto.setRoleAction("1");
					//CHECK ว่าเป็นเรื่องดีกลับหรือไม่
					List<RegisterProgress> listBackStatus =  registerProgressDAO.findProgressBackStatusbyRegistration(reg.getRegId(), dto.getRegistrationProgress());
					if(!listBackStatus.isEmpty())
					{
						RegisterProgress role = listBackStatus.get(0);
						dto.setProgressStatus(role.getProgressStatus());
						dto.setProgressBackStatus(ProgressStatus.BACK.getStatus());
					}
				}

				dto.setApproveDate(sel[49]==null?"":ft.format((Date)sel[49]));
				
				list.add(dto);
			}
		}
		
		return list;
	}

	
	public List getDocument(Object object, User user) throws Exception
	{
		// TODO Auto-generated method stub
		System.out.println("###Service DocumentRegistrationService method getDocument");
		if(!(object instanceof MasDocumentDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
		List<MasDocumentDTO> list = new ArrayList<MasDocumentDTO>();
		
		MasDocumentDTO params = (MasDocumentDTO)object;

		Trader trader  = (Trader)this.traderDAO.findByPrimaryKey(params.getTraderId());
		
		
		params.setPersonType(trader.getPerson().getPersonType());
		
		if(StringUtils.isEmpty(params.getLicenseType())){
			params.setLicenseType(trader.getTraderType());
		}
		if(!TraderType.TOUR_COMPANIES.getStatus().equals(trader.getTraderType())){
			params.setRegistrationType(RegistrationType.TOUR_COMPANIES_NEW_REGISTRATION.getStatus());
		}else{
			
			if(StringUtils.isEmpty(params.getRegistrationType())){
				List<Registration> listRegistration = registrationDAO.findAllByTrader(trader.getTraderId());
				if(CollectionUtils.isNotEmpty(listRegistration)){
					Registration reg = listRegistration.get(0);
					params.setRegistrationType(reg.getRegistrationType());
				}
				
			}
		}
		System.out.println("LicenseType = " + params.getLicenseType());
		System.out.println("RegistrationType = " + params.getRegistrationType());
		System.out.println("recordStatus = " + params.getRecordStatus());
		System.out.println("PersonType = " + params.getPersonType());

		//Main
		List<MasDocument> listDocumentMain = this.documentRegistrationDAO.findDocument(params, user);
		
		if(!listDocumentMain.isEmpty())
		{
			for(MasDocument document: listDocumentMain)
			{
				MasDocumentDTO dto = new MasDocumentDTO();
				
				dto.setBranchTypeName("1.ใบอนุญาตหลัก");
				dto.setTraderId(trader.getTraderId());
				
				ObjectUtil.copy(document, dto);
				
				//set havafile
				List<RegDocument> listDoc = this.regDocumentDAO.findAllByTraderAndMasDoc(trader.getTraderId(), document.getMasDocId());
				if(listDoc != null && listDoc.size()>0)
				{
					RegDocument doc = listDoc.get(0);
					
					dto.setHavefile("true");
					dto.setRegDocId(listDoc.get(0).getRegDocId());
					dto.setDocPath(listDoc.get(0).getDocPath());
					if(StringUtils.isNotEmpty(doc.getDocStatus())){
						dto.setDocStatus(doc.getDocStatus());
					}
					if(StringUtils.isNotEmpty(doc.getDocRemark())){
						dto.setDocRemark(doc.getDocRemark());
					}
					
					
				}
				
				list.add(dto);
			}
		}
		
		//Branch
		System.out.println("###params.getTraderId() = "+params.getTraderId());
		List<Trader> listTraderBranch = this.traderDAO.findTraderBranch(trader);
		System.out.println("listTraderBranch = "+listTraderBranch.size());
		if(CollectionUtils.isNotEmpty(listTraderBranch))
		{
			int i = 2;
			List<MasDocument> listDocumentBranch = this.documentRegistrationDAO.findDocumentBranch(params, user);
			for(Trader traderpersis: listTraderBranch)
			{
				for(MasDocument document: listDocumentBranch)
				{
					MasDocumentDTO dto = new MasDocumentDTO();
					
					dto.setBranchTypeName(i+".สาขาที่ "+traderpersis.getTraderBranchNo());
					ObjectUtil.copy(document, dto);
					dto.setTraderId(traderpersis.getTraderId());//set sub trader
					
					//set havafile
					List<RegDocument> listDoc = this.regDocumentDAO.findAllByTraderAndMasDoc(traderpersis.getTraderId(), document.getMasDocId());
					if(CollectionUtils.isNotEmpty(listDoc))
					{
						dto.setHavefile("true");
						dto.setRegDocId(listDoc.get(0).getRegDocId());
						dto.setDocPath(listDoc.get(0).getDocPath());
						dto.setDocStatus(listDoc.get(0).getDocStatus());
					}
					
					list.add(dto);
				}
				i++;
			}
			
		}
		
		
		return list;
	}



	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void upload(Object object, MultipartFile file, User user)
			throws Exception {
		// TODO Auto-generated method stub
		
		System.out.println("###Service DocumentRegistrationService method upload");
		if(!(object instanceof MasDocumentDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}

		MasDocumentDTO params = (MasDocumentDTO)object;
		
		Trader trader = (Trader)this.traderDAO.findByPrimaryKey(params.getTraderId());
		
		//TraderType
		String traderTypeName = "";
		//LicenseNo
		String licenseNoStr = trader.getLicenseNo();
		if(trader.getTraderType().equals(TraderType.TOUR_COMPANIES.getStatus()))
		{
			traderTypeName = "Business";
			licenseNoStr = licenseNoStr.replaceAll("[/]", "");
		}
		if(trader.getTraderType().equals(TraderType.GUIDE.getStatus()))
		{
			traderTypeName = "Guide";
			licenseNoStr = licenseNoStr.replaceAll("[-]", "");
		}
		if(trader.getTraderType().equals(TraderType.LEADER.getStatus()))
		{
			traderTypeName = "Tourleader";
			licenseNoStr = licenseNoStr.replaceAll("[.]", "");
		}
		
		
		
		
		
		
		//RegistrationType
		List<Registration> listReg = (List<Registration>)this.registrationDAO.findAllByTrader(trader.getTraderId());
		String registrationType = (listReg.get(0)).getRegistrationType();
		//RealFileName
		String realFileName = params.getMasDocId()+"";
		
		//postfix FileName
//		System.out.println("file.getOriginalFilename() = "+file.getOriginalFilename());
//		String str[] = file.getOriginalFilename().split("[.]");	
		String extension = ".txt";
		String originalFilename = file.getOriginalFilename();
		int dot = originalFilename.lastIndexOf('.');
		if(dot != -1)
		{
			extension = originalFilename.substring(dot);
		}

		
		String root = ConstantUtil.ROOT_PATH; //"/Users/Apple/Desktop/fileUpload/"; 
		//String root = ConstantUtil.IMG_ROOT_PATH; //"/Users/Apple/Desktop/fileUpload/";
		String path = root+"Registration"+java.io.File.separator+traderTypeName+java.io.File.separator+licenseNoStr+java.io.File.separator+registrationType;
    	String fileName = java.io.File.separator+realFileName+extension;
    	
    	String fullpath = path+fileName;
    	
    	MasDocument document = (MasDocument)this.masDocumentDAO.findByPrimaryKey(params.getMasDocId());

    	if(file != null)
	    {
    		//Update
    		if(params.getRegDocId() >0)
    		{
    			
    			//Delete Old File
    			RegDocument regDocument = (RegDocument)this.regDocumentDAO.findByPrimaryKey(params.getRegDocId());
    			
    			String oldDocPath = regDocument.getDocPath();
    			
    			FileManage fm = new FileManage();
    			fm.deleteFile(oldDocPath);
    			
    			//Upload new File
		    	boolean b = fm.uploadFile(file.getInputStream(), path, fileName);
		    	
		    	//Update RegDocument
		    	if(b)
		    	{
	    			regDocument.setDocPath(fullpath);
	    			regDocument.setLastUpdDtm(new Date());
	    			regDocument.setLastUpdUser(user.getUserName());
		    	}
    			
    		}
    		//Create
    		else
    		{
    			//Upload File
		    	FileManage fm = new FileManage();
		    	boolean b = fm.uploadFile(file.getInputStream(), path, fileName);
		    	
		    	//Save RegDocument
		    	if(b)
		    	{
	
		    		this.regDocumentService.saveRegDocument(fullpath, trader, listReg.get(0), document, user);
		    		
		    	}
    		}
	    	
	    }
    	
	}
	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void updateCheckdocs(Object object, User user) throws Exception {
		if (!(object instanceof MasDocumentDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		MasDocumentDTO param = (MasDocumentDTO) object;

		RegDocument regDoc = (RegDocument)this.documentRegistrationDAO.findByPrimaryKey(param.getRegDocId());
		if(StringUtils.isNotEmpty(param.getDocStatus())){
			regDoc.setDocStatus(param.getDocStatus());
		}
		if(StringUtils.isNotEmpty(param.getDocRemark())){
			regDoc.setDocRemark(param.getDocRemark());
		}
		regDoc.setLastUpdUser(user.getUserName());
		regDoc.setLastUpdDtm(new Date());
		this.documentRegistrationDAO.update(regDoc);
		
		
	}

	
//	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
//	public void upload(Object object, MultipartFile file, User user)throws Exception 
//	{
//		// TODO Auto-generated method stub
//		
//		System.out.println("###Service DocumentRegistrationService method upload");
//		if(!(object instanceof MasDocumentDTO))
//		{
//			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
//		}
//		
//		String filename = file.getOriginalFilename();
//		String extension = filename.substring(filename.lastIndexOf(".") + 1, filename.length());
//		
//		System.out.println("filename = "+filename);
//		System.out.println("extension = "+extension);
//
//		String excel = "xls";
//		if (!excel.equals(extension)) {
//		    JOptionPane.showMessageDialog(null, "Choose an excel file!");
//		}
//		else {
//			try {
//				File convFile = new File( file.getOriginalFilename());
//				file.transferTo(convFile);
//		        
//			    POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(convFile));
//			    HSSFWorkbook wb = new HSSFWorkbook(fs);
//			    HSSFSheet sheet = wb.getSheetAt(0);
//			    HSSFRow row;
//			    HSSFCell cell;
//
//			    int rows; // No of rows
//			    rows = sheet.getPhysicalNumberOfRows();
//
//			    String result = "";
//			    String result2 = "";
//			    
//			    for(int r = 0; r < 10; r++) {
//			        row = sheet.getRow(r);
//			        
//                	String licenseNo = "";
//                	long prefixId = 0;
//                	String firstName = "";
//                	String lastName = "";
//                	String firstNameEN = "";
//                	String lastNameEN = "";
//                	String identityNo = "";
//                	
//                	boolean haveLicenseNo = true;
//                	
//			        if(row != null) {
//			            for(int c = 0; c < 6; c++) 
//			            {
//			                cell = row.getCell((short)c);
//			                if(cell != null) 
//			                {
//			                	cell.setCellType(Cell.CELL_TYPE_STRING);
//			                	String value = cell.getStringCellValue();
//			                	if(c == 0)
//			                	{
//			                		licenseNo = cell.getStringCellValue();			                		
//			                		for(int i = licenseNo.length(); i<7; i++)
//			                		{
//			                			licenseNo+="0";
//			                		}			                		
//			                		List<Trader> list = (List<Trader>)this.traderDAO.findTraderByLicenseNo(value, null, null, 0);
//			                		if(list.size() > 0)
//			                		{
//			                			result+=" "+value;
//			                			break;
//			                		}
//			                		else
//			                		{
//			                			haveLicenseNo = false;
//			                			result2+=" "+value;		                						          			                			
//			                		}			                		
//			                	}
//			                	else if(c == 1)
//			                	{
//			                		value = value.replaceAll("\\s", "");
//			                		if(value.equals("นาย"))
//			                		{
//			                			prefixId = 1;
//			                		}else if(value.equals("นาง"))
//			                		{
//			                			prefixId = 2;
//			                		}else if(value.equals("น.ส."))
//			                		{
//			                			prefixId = 3;
//			                		}
//			                	}
//			                	else if(c == 2)
//			                	{
//			                		String[] arr = value.split("\\s");
//			                		int i = 1;
//			                		for(String s : arr)
//			                		{
//			                			if(i == 1)
//			                				firstName = s;
//			                			else if(i == 2)
//			                				lastName = s;
//			                			
//			                			i++;
//			                		}
//			                	}
//			                	else if(c == 3)
//			                	{
//			                		firstNameEN = value;
//			                	}
//			                	else if(c == 4)
//			                	{
//			                		lastNameEN = value;
//			                	}
//			                	else if(c == 5)
//			                	{
//			                		identityNo = value;
//			                	}
//			                	
//			                	
//			                }
//			            }//for cell
//
//			            if(!haveLicenseNo)
//			            {
////			            	System.out.println("INSERT");
////			            	System.out.println(licenseNo+" "+prefixId+" "+firstName+" "+lastName+" "+firstNameEN+" "+lastNameEN+" "+identityNo);
//			            	
//			            	//PERSON
//			            	Person person = new Person();
//			            	person.setPersonType("I");
//			            	if(identityNo != null && !identityNo.equals(""))
//			            		person.setIdentityNo(identityNo);
//			            	if(firstName != null && !firstName.equals(""))
//			            		person.setFirstName(firstName);
//			            	if(lastName != null && !lastName.equals(""))
//			            		person.setLastName(lastName);
//			            	if(firstNameEN != null && !firstNameEN.equals(""))
//			            		person.setFirstNameEn(firstNameEN);
//			            	if(lastNameEN != null && !lastNameEN.equals(""))
//			            		person.setLastNameEn(lastNameEN);
//			            	if(prefixId != 0)
//			            	{
//			            		MasPrefix p = new MasPrefix();
//			            		p.setPrefixId(prefixId);
//			            		person.setMasPrefix(p);
//			            	}
//			            	person.setRecordStatus(RecordStatus.NORMAL.getStatus());
//			            	person.setCreateUser("VORARATS");
//			            	person.setPersonStatus(RecordStatus.NORMAL.getStatus());
//			            	person.setCreateDtm(new Date());
//			            	
//			            	Long personId = (Long)personDAO.insert(person);
//			            	person.setPersonId(personId);
//			            	
//			            	//TRADER
//			            	Trader trader = new Trader();
//			            	trader.setPerson(person);
//			            	trader.setTraderType(TraderType.LEADER.getStatus());
//			            	trader.setLicenseNo(licenseNo);
//			            	trader.setTraderCategory(TraderCategory.ACT_LEADER.getStatus());
//			            	trader.setEffectiveDate(new Date());
//			            	Organization org = new Organization();
//			            	org.setOrgId(7);
//			            	trader.setOrganization(org);
//			            	trader.setLicenseStatus(RecordStatus.NORMAL.getStatus());
//			            	trader.setRecordStatus(RecordStatus.NORMAL.getStatus());
//			            	trader.setCreateUser("VORARATS");
//			            	trader.setCreateDtm(new Date());
//			            	
//			            	Long traderId =  (Long)traderDAO.insert(trader);
//			        		trader.setTraderId(traderId);
//			        		
//			        		//Registration
//			        		Registration reg = new Registration();
//			        		reg.setTrader(trader);
//			            	reg.setOrganization(org);
//			        		reg.setRegistrationType(RecordStatus.NORMAL.getStatus());
//			        		reg.setRecordStatus(RecordStatus.NORMAL.getStatus());
//			        		reg.setRegistrationDate(new Date());
//			        		reg.setApproveDate(new Date());
//			        		reg.setCreateUser("VORARATS");
//			            	reg.setCreateDtm(new Date());
//			            	reg.setApproveStatus("A");
//			            	
//			            	Long regId =  (Long)this.registrationDAO.insert(reg);
//			            }
//			        }
//			    }//for row
//			    
//			    System.out.println("rows = "+rows);
//			    System.out.println("result = "+result);
//			    System.out.println("result2 = "+result2);
//			    
//			} catch(Exception ioe) {
//			    ioe.printStackTrace();
//			}
//		}
//		
//		
//
////		MasDocumentDTO params = (MasDocumentDTO)object;
////		
////		Trader trader = (Trader)this.traderDAO.findByPrimaryKey(params.getTraderId());
//
//    	
//	}
	
	
}











