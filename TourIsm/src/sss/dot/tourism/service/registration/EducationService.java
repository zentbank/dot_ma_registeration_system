package sss.dot.tourism.service.registration;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.mas.MasAmphurDAO;
import sss.dot.tourism.dao.mas.MasEducationLevelDAO;
import sss.dot.tourism.dao.mas.MasProvinceDAO;
import sss.dot.tourism.dao.mas.MasTambolDAO;
import sss.dot.tourism.dao.mas.MasUniversityDAO;
import sss.dot.tourism.dao.training.EducationDAO;
import sss.dot.tourism.domain.Education;
import sss.dot.tourism.domain.MasEducationLevel;
import sss.dot.tourism.domain.MasUniversity;
import sss.dot.tourism.domain.Person;
import sss.dot.tourism.dto.training.EducationDTO;
import sss.dot.tourism.util.ObjectUtil;

import com.sss.aut.service.User;
@Repository("educationService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class EducationService implements IEducationService {

	@Autowired
	private MasProvinceDAO masProvinceDAO;
	@Autowired
	private MasAmphurDAO masAmphurDAO;
	@Autowired
	private MasTambolDAO masTambolDAO;
	@Autowired
	private EducationDAO educationDAO;
	@Autowired
	private MasEducationLevelDAO masEducationLevelDAO;
	@Autowired
	private MasUniversityDAO masUniversityDAO;
	
	public List getAll(Object object, User user) throws Exception {
		
		if(!(object instanceof EducationDTO))
		{
			throw new IllegalArgumentException("object not instanceof EducationDTO");
		}
		EducationDTO param = (EducationDTO)object;
		
		List<EducationDTO> list = new ArrayList<EducationDTO>();
		
		List<Education> listAll = (List<Education>)this.educationDAO.findEducationAll(param);
		
		if(!listAll.isEmpty())
		{
			for(Education education: listAll)
			{
				EducationDTO dto = new EducationDTO();
				
				ObjectUtil.copy(education, dto);
				
				if(education.getMasUniversity() != null)
				{
					dto.setMasUniversityId(education.getMasUniversity().getMasUniversityId());
					dto.setUniversityName(education.getMasUniversity().getUniversityName());
				}
				if(education.getPerson() != null)
				{
					dto.setPersonId(education.getPerson().getPersonId());
				}
				if(education.getMasEducationLevel() != null)
				{
					dto.setMasEducationLevelId(education.getMasEducationLevel().getMasEducationLevelId());
					dto.setEducationLevelName(education.getMasEducationLevel().getEducationLevelName());
				}
				
				list.add(dto);
			}
		}
		return list;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List create(Object[] object, User user) throws Exception {
		
		List<EducationDTO> list = new ArrayList<EducationDTO>();
		for (Object obj : object) {
			EducationDTO dto = (EducationDTO) obj;
			
			Education education = new Education();
			
			ObjectUtil.copy(dto, education);
			
			if(dto.getPersonId() > 0)
			{
				Person person = new Person();
				person.setPersonId(dto.getPersonId());
				education.setPerson(person);
				
			}
			
			if(dto.getMasUniversityId() > 0)
			{
				MasUniversity masUniversity = new MasUniversity();
				masUniversity.setMasUniversityId(dto.getMasUniversityId());
				education.setMasUniversity(masUniversity);
			}
			
			if(dto.getMasEducationLevelId() > 0)
			{
				MasEducationLevel masEducationLevel =  new MasEducationLevel();
				masEducationLevel.setMasEducationLevelId(dto.getMasEducationLevelId());
				education.setMasEducationLevel(masEducationLevel);
			}
				
			education.setRecordStatus("T");
			education.setCreateUser(user.getUserName());
			education.setCreateDtm(new Date());
			
			Long eduId = (Long)this.educationDAO.insert(education);
			
			dto.setEduId(eduId);
			
			list.add(dto);
		}
		return list;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void update(Object[] object, User user) throws Exception {

		List<EducationDTO> list = new ArrayList<EducationDTO>();
		for (Object obj : object) {
			EducationDTO dto = (EducationDTO) obj;
			
//			Education education = new Education();
			Education education = (Education)this.educationDAO.findByPrimaryKey(dto.getEduId());
			
			ObjectUtil.copy(dto, education);
			
			if(dto.getPersonId() > 0)
			{
				Person person = new Person();
				person.setPersonId(dto.getPersonId());
				education.setPerson(person);
				
			}
			
			if(dto.getMasUniversityId() > 0)
			{
				MasUniversity masUniversity = new MasUniversity();
				masUniversity.setMasUniversityId(dto.getMasUniversityId());
				education.setMasUniversity(masUniversity);
			}
			
			if(dto.getMasEducationLevelId() > 0)
			{
				MasEducationLevel masEducationLevel =  new MasEducationLevel();
				masEducationLevel.setMasEducationLevelId(dto.getMasEducationLevelId());
				education.setMasEducationLevel(masEducationLevel);
			}

			education.setLastUpdUser(user.getUserName());
			
			this.educationDAO.update(education);
			
			list.add(dto);
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(Object[] object, User user) throws Exception {
		
		for (Object obj : object) {
			EducationDTO dto = (EducationDTO) obj;
			Education addr = (Education)educationDAO.findByPrimaryKey(dto.getEduId());
			educationDAO.delete(addr);
			
		}

	}

}
