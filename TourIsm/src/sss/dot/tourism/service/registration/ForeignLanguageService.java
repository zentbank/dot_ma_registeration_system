package sss.dot.tourism.service.registration;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.mas.ForeignLanguageDAO;
import sss.dot.tourism.domain.Country;
import sss.dot.tourism.domain.ForeignLanguage;
import sss.dot.tourism.domain.Person;
import sss.dot.tourism.dto.mas.ForeignLanguageDTO;
import sss.dot.tourism.util.ObjectUtil;

import com.sss.aut.service.User;

@Repository("foreignLanguageService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ForeignLanguageService implements IForeignLanguageService {

	@Autowired
	ForeignLanguageDAO foreignLanguageDAO;
	
	public List getAll(Object object, User user) throws Exception {
		
		if(!(object instanceof ForeignLanguageDTO))
		{
			throw new IllegalArgumentException("object not instanceof ForeignLanguageDTO");
		}
		ForeignLanguageDTO param = (ForeignLanguageDTO)object;
		
		List<ForeignLanguageDTO> list = new ArrayList<ForeignLanguageDTO>();
		
		List<ForeignLanguage> listAll = (List<ForeignLanguage>)this.foreignLanguageDAO.findForeignLanguageAll(param);
		
		if(!listAll.isEmpty())
		{
			for(ForeignLanguage foreignLanguage: listAll)
			{
				ForeignLanguageDTO dto = new ForeignLanguageDTO();
				
				ObjectUtil.copy(foreignLanguage, dto);
				ObjectUtil.copy(foreignLanguage.getCountry(), dto);
				
				dto.setCountryName(foreignLanguage.getCountry().getCountryName());
				
				if(foreignLanguage.getPerson() != null)
				{
					dto.setPersonId(foreignLanguage.getPerson().getPersonId());
				}
				
				list.add(dto);
			}
		}
		return list;
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List create(Object[] object, User user) throws Exception {
		
		List<ForeignLanguageDTO> list = new ArrayList<ForeignLanguageDTO>();
		for (Object obj : object) {
			ForeignLanguageDTO dto = (ForeignLanguageDTO) obj;
			
			ForeignLanguage foreignLanguage = new ForeignLanguage();
			
			ObjectUtil.copy(dto, foreignLanguage);
			
			if(dto.getPersonId() > 0)
			{
				Person person = new Person();
				person.setPersonId(dto.getPersonId());
				foreignLanguage.setPerson(person);
				
			}
			
			if(dto.getCountryId() > 0)
			{
				Country country = new Country();
				country.setCountryId(dto.getCountryId());
				foreignLanguage.setCountry(country);
				
			}
			
				
			foreignLanguage.setRecordStatus("T");
			foreignLanguage.setCreateUser(user.getUserName());
			foreignLanguage.setCreateDtm(new Date());
			
			Long foreignId = (Long)this.foreignLanguageDAO.insert(foreignLanguage);
			
			dto.setForeignId(foreignId);
			
			list.add(dto);
		}
		return list;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void update(Object[] object, User user) throws Exception {
		
		List<ForeignLanguageDTO> list = new ArrayList<ForeignLanguageDTO>();
		for (Object obj : object) {
			ForeignLanguageDTO dto = (ForeignLanguageDTO) obj;
			
//			ForeignLanguage foreignLanguage = new ForeignLanguage();
			ForeignLanguage foreignLanguage = (ForeignLanguage)this.foreignLanguageDAO.findByPrimaryKey(dto.getForeignId());
			
			ObjectUtil.copy(dto, foreignLanguage);
			
			if(dto.getPersonId() > 0)
			{
				Person person = new Person();
				person.setPersonId(dto.getPersonId());
				foreignLanguage.setPerson(person);
				
			}
			
			if(dto.getCountryId() > 0)
			{
				Country country = new Country();
				country.setCountryId(dto.getCountryId());
				foreignLanguage.setCountry(country);
				
			}
			
			foreignLanguage.setLastUpdUser(user.getUserName());
			
			this.foreignLanguageDAO.update(foreignLanguage);
			
			list.add(dto);
		}
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(Object[] object, User user) throws Exception {
		for (Object obj : object) {
			ForeignLanguageDTO dto = (ForeignLanguageDTO) obj;
			ForeignLanguage addr = (ForeignLanguage)foreignLanguageDAO.findByPrimaryKey(dto.getForeignId());
			foreignLanguageDAO.delete(addr);
			
		}

	}

}
