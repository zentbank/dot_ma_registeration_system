package sss.dot.tourism.service.registration;

import java.util.List;
import java.util.Map;

import com.sss.aut.service.User;

public interface IAccountService {
	public List getTraderBetweenRegistration(Object object, User user ,int start, int limit) throws Exception;
	public List countTraderBetweenRegistrationPaging(Object object, User user ,int start, int limit) throws Exception;
	public void verification(Object object, User user) throws Exception;
	
	public Object getPayers(Object object, User user) throws Exception;
	public List getFee(Object object, User user) throws Exception;
	public List getOfficer(Object object, User user) throws Exception;
	
	public void saveReceipt(Object[] object, User user) throws Exception;

//	public void saveGaurantee(Object object, User user) throws Exception;
//	public List getById(long traderId, User user) throws Exception;
//	public Object getById(long traderId, User user) throws Exception;

	
	
	public Object getReceipt(Object object, User user) throws Exception;
	public List getReceiptDetail(Object object, User user) throws Exception;
	
	public void updateReceipt(Object[] object, User user) throws Exception;
	
	//@shaii
	public List getReceiptDetailFee(Object object, User user) throws Exception;
	
	
	public void saveReceiptAccount(Object obj,Object[] object, User user)throws Exception;
	
	public void saveGuaranteeAccount(Object object, User user)  throws Exception;
	
	public Map getSummaryReceiptAndFee(Object object, User user)  throws Exception;
	
	//Oat Add 30/10/57
	public Map getSummaryGuaranteeReceiptAndFee(Object object, User user)  throws Exception;
	//Oat Add 25/02/58
	public Object getReceiptCancle(Object object, User user) throws Exception;
	//Oat Add 26/02/58
	public void saveReceiptAccountCancle(Object obj,Object[] object, User user)throws Exception;
	
	public void updateFeePaid(Object obj, User user)throws Exception;
	
	public List getPaymentOnlineForPrintReceipt(Object object, User user) throws Exception;
	public Object getPaymentOnlineDetail(Object object, User user) throws Exception;
	
	
	public List getPaymentDetailFee(Object object, User user) throws Exception;
	public void savePayment(Object obj,Object[] object, User user)throws Exception;
	
	
	
}





