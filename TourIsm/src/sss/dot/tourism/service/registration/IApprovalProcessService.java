package sss.dot.tourism.service.registration;

import java.util.List;

import com.sss.aut.service.User;

public interface IApprovalProcessService {
	public List getTraderBetweenRegistration(Object object, User user ,int start, int limit) throws Exception;
	public List countTraderBetweenRegistration(Object object, User user ,int start, int limit) throws Exception;
	
	public void verification(Object object, User user) throws Exception;
}
