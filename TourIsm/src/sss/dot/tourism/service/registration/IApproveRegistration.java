package sss.dot.tourism.service.registration;

import com.sss.aut.service.User;

public interface IApproveRegistration {

	public void approve(Object object ,User user) throws Exception;
	
	public void rejectRegistration(Object object ,User user) throws Exception;
	
}
