package sss.dot.tourism.service.registration;

import java.util.List;

import sss.dot.tourism.dto.training.EducationDTO;

import com.sss.aut.service.User;

public interface ICourseService {

	public List getAll(Object object, User user ,int start, int limit) throws Exception;
	public List countCourseAll(Object object, User user ,int start, int limit) throws Exception;
	public void create(Object object, User user) throws Exception;
	public void update(Object object, User user) throws Exception;
	public void delete(Object[] object, User user) throws Exception;
	
	//PersonTrained
	public List getPersonAll(Object object, User user ,int start, int limit) throws Exception;
	public List countPersonAll(Object object, User user, int start, int limit)  throws Exception;
	public void createPersonTrained(Object object, User user) throws Exception;
	public void updatePersonTrained(Object object, User user) throws Exception;
	public void deletePersonTrained(Object[] object, User user) throws Exception;
}
