package sss.dot.tourism.service.registration;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.sss.aut.service.User;

public interface IDocumentRegistrationService {
	public List getDocumentRegistrationPaging(Object object, User user ,int start, int limit) throws Exception;
	public List getDocument(Object object, User user) throws Exception;
	
	public void upload(Object object, MultipartFile file, User user) throws Exception;
	public void updateCheckdocs(Object object, User user) throws Exception;
}
