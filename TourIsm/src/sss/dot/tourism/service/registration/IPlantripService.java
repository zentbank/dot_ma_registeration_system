package sss.dot.tourism.service.registration;

import java.util.List;

import com.sss.aut.service.User;

public interface IPlantripService {
	public List getAll(Object object, User user) throws Exception;
	public List create(Object[] object, User user) throws Exception;
	public void update(Object[] object, User user) throws Exception;
	public void delete(Object[] object, User user) throws Exception;
}
