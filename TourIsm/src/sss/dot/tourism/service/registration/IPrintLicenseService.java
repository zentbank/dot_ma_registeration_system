package sss.dot.tourism.service.registration;

import java.io.FileInputStream;
import java.util.List;
import java.util.Map;

import com.sss.aut.service.User;

public interface IPrintLicenseService {
	public List getPrintLicense(Object object, User user ,int start, int limit) throws Exception;
	public List countPrintLicense(Object object, User user ,int start, int limit) throws Exception;
//	public List countPrintLicense2(Object object, User user ,int start, int limit) throws Exception;
	
//	public List getPrintLicneseById(Object object, User user) throws Exception;
	
	public List getPrintLicneseById(int printId, User user) throws Exception;
	
	public void updatePrintLicneseById(Object object, User user) throws Exception;
	public void savePrintLicneseById(Object object, User user) throws Exception;
	
	public List getPrintLicenseExcel(Map model, Object object, User user ,int start, int limit) throws Exception;
	
	public FileInputStream getQRCode(Object object, User user) throws Exception;
	
	public void getLicenseForPrint(Object object, User user) throws Exception;
}
