package sss.dot.tourism.service.registration;

import java.util.List;

import com.sss.aut.service.User;

public interface IRegisterProgressService {
	public List getRegistrationProgressStatus(Object object, User user) throws Exception;
	public void verification(Object object, User user, String roleStatus) throws Exception;
}
