package sss.dot.tourism.service.registration;

import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;

import com.sss.aut.service.User;

public interface IRegistrationService {
	@SuppressWarnings("rawtypes")
	public void createRegistration(Object object, User user) throws Exception;
	public List getGuideLicense(Object object, User user) throws Exception;
	
	public void saveAllRegistration(Object object, User user) throws Exception;
//	public List getTraderBetweenRegistration(Object object, User user ,int start, int limit) throws Exception;
	public List getTraderBetweenRegistrationPaging(Object object, User user ,int start, int limit) throws Exception;
	public List countTraderBetweenRegistrationPaging(Object object, User user ,int start, int limit) throws Exception;
	
	public void cancelAllRegistration(Object object, User user) throws Exception;
	
	
	public List getTraderByLicenseNo(Object object, User user) throws Exception;
	public List getLicenseByLicenseNo(Object object, User user) throws Exception;
	
	
	public void verification(Object object, User user) throws Exception;	
	
	public Object addNewRegistration(Object object, User user) throws Exception;
	
	public Object verificationName(Object object, User user) throws Exception;	
	
	public Object verificationIdentityNo(Object object, User user) throws Exception;
	
	public Object verificationIdentityNoTourleader(Object object, User user) throws Exception;
	
	public List verificationIdentityNoDetail(Object object, User user) throws Exception;
	
	public void requestAdditionalDoc(Object object, User user) throws Exception;

}
