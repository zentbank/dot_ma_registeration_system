package sss.dot.tourism.service.registration;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sss.aut.service.User;

import sss.dot.tourism.dao.mas.RegisterFlowDAO;
import sss.dot.tourism.dao.registration.RegisterProgressDAO;
import sss.dot.tourism.dao.registration.RegistrationDAO;
import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.domain.AdmUser;
import sss.dot.tourism.domain.RegisterFlow;
import sss.dot.tourism.domain.RegisterProgress;
import sss.dot.tourism.domain.Registration;
import sss.dot.tourism.dto.registration.RegisterProgressDTO;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.service.trader.ITourLeaderService;
import sss.dot.tourism.util.ProgressStatus;
import sss.dot.tourism.util.RoleStatus;


@Repository("keyRegistrationService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class KeyRegistrationService implements IApprovalProcessService {

	@Autowired
	TraderDAO traderDAO;
	@Autowired
	RegisterProgressDAO registerProgressDAO;
	@Autowired
	RegistrationDAO registrationDAO;
	@Autowired
	ITourLeaderService tourLeaderService;
	@Autowired
	RegisterFlowDAO registerFlowDAO;
	@Autowired
	IRegistrationService registrationService;
	@Autowired
	IRegisterProgressService registerProgressService;
	@Autowired
	IApproveRegistration approveRegistrationService;
	
	
	private String NEXT_ROLE = "'LAW','CHK','SUP','MAN','DIR','ACC'";
	
	public List getTraderBetweenRegistration(Object object, User user,
			int start, int limit) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถโหลดข้อมูลได้การจดทะเบียนได้");
		}
			
		RegistrationDTO params = (RegistrationDTO)object;
		
		if((null != params.getRegProgressStatus()) && (!"".equals(params.getRegProgressStatus())))
	    {
	    	// w ยังไม่ส่งเรื่อง
	    	if(params.getRegProgressStatus().equals(ProgressStatus.WAITING.getStatus()))
	    	{
	    		params.setRegGroupProgress(null);
	    	}
	    	else if(params.getRegProgressStatus().equals(ProgressStatus.ACCEPT.getStatus()))
	    	{
	    		params.setRegGroupProgress(this.NEXT_ROLE);
	    		
	    	}else if(params.getRegProgressStatus().equals(ProgressStatus.GET_IT.getStatus()))
	    	{
	    		params.setRegGroupProgress(null);
	    	}
	    }
		
		params.setRegistrationProgress(RoleStatus.KEY.getStatus());
		
		return registrationService.getTraderBetweenRegistrationPaging(object, user, start, limit);
	}
	

	
	public List countTraderBetweenRegistration(Object object, User user,
			int start, int limit) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถโหลดข้อมูลได้การจดทะเบียนได้");
		}
			
		RegistrationDTO params = (RegistrationDTO)object;
		
		if((null != params.getRegProgressStatus()) && (!"".equals(params.getRegProgressStatus())))
	    {
	    	// w ยังไม่ส่งเรื่อง
	    	if(params.getRegProgressStatus().equals(ProgressStatus.WAITING.getStatus()))
	    	{
	    		params.setRegGroupProgress(null);
	    	}
	    	else if(params.getRegProgressStatus().equals(ProgressStatus.ACCEPT.getStatus()))
	    	{
	    		params.setRegGroupProgress(this.NEXT_ROLE);
	    		
	    	}else if(params.getRegProgressStatus().equals(ProgressStatus.GET_IT.getStatus()))
	    	{
	    		params.setRegGroupProgress(null);
	    	}
	    }
		
		params.setRegistrationProgress(RoleStatus.KEY.getStatus());
		
		return registrationService.countTraderBetweenRegistrationPaging(object, user, start, limit);
	}



	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void verification(Object object, User user) throws Exception {

		if(!(object instanceof RegisterProgressDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้้");
		}
		
		RegisterProgressDTO param = (RegisterProgressDTO)object;
		
		Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(param.getRegId());
		
		RegisterProgress progress =  new RegisterProgress();
		progress.setRegistration(reg);
		
		AdmUser  admUser = new AdmUser();
		admUser.setUserId(user.getUserId());
		progress.setAdmUser(admUser);
		progress.setProgress(RoleStatus.KEY.getStatus());
		progress.setProgressDate(Calendar.getInstance().getTime());
//		String positionName = user.getUserData().getPositionName()==null?"":user.getUserData().getPositionName();
		progress.setProgressDesc(RoleStatus.KEY.getMeaning()+" "+param.getProgressDesc()==null?"":param.getProgressDesc());
		progress.setProgressStatus(param.getProgressStatus());
		registerProgressDAO.insert(progress);
		
		if(ProgressStatus.ACCEPT.getStatus().equals(param.getProgressStatus()))
		{
			//get next role
			RegisterFlow flow = registerFlowDAO.findNextRole(RoleStatus.KEY.getStatus());
			
			//update registration next role
			reg.setRegistrationProgress(flow.getNextRole());
			reg.setLastUpdUser(user.getUserName());
			reg.setLastUpdDtm(new Date());
			
			//insert register progress next role waiting
			
			RegisterProgress nextProgress =  new RegisterProgress();
			nextProgress.setRegistration(reg);
			
//			AdmUser  admUser = new AdmUser();
//			admUser.setUserId(user.getUserId());
//			progress.setAdmUser(admUser);
			nextProgress.setProgress(flow.getNextRole());
			nextProgress.setProgressDate(Calendar.getInstance().getTime());
			nextProgress.setProgressDesc(RoleStatus.getMeaning(flow.getNextRole())+" "+ProgressStatus.WAITING.getMeaning());
			nextProgress.setProgressStatus(ProgressStatus.WAITING.getStatus());
			registerProgressDAO.insert(nextProgress);
			
			// TODO Notification to next role
		}
		else
		{
			//ยกเลิกการจดทะเบียน
			reg.setRegistrationProgress(RoleStatus.KEY.getStatus());
			reg.setLastUpdUser(user.getUserName());
			reg.setLastUpdDtm(new Date());
			
			this.approveRegistrationService.rejectRegistration(reg, user);
			
			
		}
		
		
	}



}
