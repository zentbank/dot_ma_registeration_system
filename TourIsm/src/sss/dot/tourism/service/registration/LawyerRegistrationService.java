package sss.dot.tourism.service.registration;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.mas.RegisterFlowDAO;
import sss.dot.tourism.dao.registration.RegisterProgressDAO;
import sss.dot.tourism.dao.registration.RegistrationDAO;
import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.dto.registration.RegisterProgressDTO;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.service.trader.ITourLeaderService;
import sss.dot.tourism.util.ProgressStatus;
import sss.dot.tourism.util.RoleStatus;

import com.sss.aut.service.User;
@Repository("lawyerRegistrationService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class LawyerRegistrationService implements IApprovalProcessService {

	@Autowired
	TraderDAO traderDAO;
	@Autowired
	RegisterProgressDAO registerProgressDAO;
	@Autowired
	RegistrationDAO registrationDAO;
	@Autowired
	ITourLeaderService tourLeaderService;
	@Autowired
	RegisterFlowDAO registerFlowDAO;
	@Autowired
	IRegistrationService registrationService;
	@Autowired
	IRegisterProgressService registerProgressService;
	
	private String PREVIOUS_ROLE = "'KEY'";
	private String NEXT_ROLE = "'CHK','SUP','MAN','DIR','ACC'";
	
	public List getTraderBetweenRegistration(Object object, User user,
			int start, int limit) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถโหลดข้อมูลได้การจดทะเบียนได้");
		}
			
		RegistrationDTO params = (RegistrationDTO)object;
		
		if((null != params.getRegProgressStatus()) && (!"".equals(params.getRegProgressStatus())))
	    {
	    	// w ยังไม่ส่งเรื่อง
	    	if(params.getRegProgressStatus().equals(ProgressStatus.WAITING.getStatus()))
	    	{
	    		params.setRegGroupProgress(null);
	    	}
	    	else if(params.getRegProgressStatus().equals(ProgressStatus.ACCEPT.getStatus()))
	    	{
	    		params.setRegGroupProgress(this.NEXT_ROLE);
	    		
	    	}else if(params.getRegProgressStatus().equals(ProgressStatus.GET_IT.getStatus()))
	    	{
	    		params.setRegGroupProgress(PREVIOUS_ROLE);
	    	}
	    }
		
		params.setRegistrationProgress(RoleStatus.LAW.getStatus());
		
		return registrationService.getTraderBetweenRegistrationPaging(object, user, start, limit);
	}
	
	

	public List countTraderBetweenRegistration(Object object, User user,
			int start, int limit) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถโหลดข้อมูลได้การจดทะเบียนได้");
		}
			
		RegistrationDTO params = (RegistrationDTO)object;
		
		if((null != params.getRegProgressStatus()) && (!"".equals(params.getRegProgressStatus())))
	    {
	    	// w ยังไม่ส่งเรื่อง
	    	if(params.getRegProgressStatus().equals(ProgressStatus.WAITING.getStatus()))
	    	{
	    		params.setRegGroupProgress(null);
	    	}
	    	else if(params.getRegProgressStatus().equals(ProgressStatus.ACCEPT.getStatus()))
	    	{
	    		params.setRegGroupProgress(this.NEXT_ROLE);
	    		
	    	}else if(params.getRegProgressStatus().equals(ProgressStatus.GET_IT.getStatus()))
	    	{
	    		params.setRegGroupProgress(PREVIOUS_ROLE);
	    	}
	    }
		
		params.setRegistrationProgress(RoleStatus.LAW.getStatus());
		
		return registrationService.countTraderBetweenRegistrationPaging(object, user, start, limit);
	}



	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void verification(Object object, User user) throws Exception {
		if(!(object instanceof RegisterProgressDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้้");
		}
		
		registerProgressService.verification(object, user, RoleStatus.LAW.getStatus());
		
//		RegisterProgressDTO param = (RegisterProgressDTO)object;
//		
//		Registration reg = (Registration)this.lawyerRegistrationDAO.findByPrimaryKey(param.getRegId());
//		
//		//find law
//		List<RegisterProgress> listProg = this.registerProgressDAO.findRoleProgressbyRegistration(reg.getRegId(), RoleStatus.LAW.getStatus());
//		
//		if(!listProg.isEmpty())
//		{
//			RegisterProgress progress =  listProg.get(0);
//			
//			
//			AdmUser  admUser = new AdmUser();
//			admUser.setUserId(user.getUserId());
//			progress.setAdmUser(admUser);
////			progress.setProgress(RoleStatus.KEY.getStatus());
//			progress.setProgressDate(Calendar.getInstance().getTime());
////			String positionName = user.getUserData().getPositionName()==null?"":user.getUserData().getPositionName();
//			progress.setProgressDesc(RoleStatus.LAW.getMeaning()+" "+param.getProgressDesc()==null?"":param.getProgressDesc());
//			progress.setProgressStatus(param.getProgressStatus());
//			progress.setLastUpdUser(user.getUserName());
//			progress.setLastUpdDtm(new Date());
//			
//			registerProgressDAO.update(progress);
//		}
//
//		//get next role
//		RegisterFlow flow = registerFlowDAO.findNextRole(RoleStatus.LAW.getStatus());
//		
//		//update registration next role
//		
//		reg.setLastUpdUser(user.getUserName());
//		reg.setLastUpdDtm(new Date());
//		
//		//insert register progress next role waiting
//		
//		RegisterProgress nextProgress =  new RegisterProgress();
//		nextProgress.setRegistration(reg);
//		
//		
//		nextProgress.setProgressDate(Calendar.getInstance().getTime());
//		
//		if(ProgressStatus.ACCEPT.getStatus().equals(param.getProgressStatus()))
//		{
//			reg.setRegistrationProgress(flow.getNextRole());
//			nextProgress.setProgress(flow.getNextRole());
//			nextProgress.setProgressDesc(RoleStatus.getMeaning(flow.getNextRole())+" "+ProgressStatus.WAITING.getMeaning());
//			nextProgress.setProgressStatus(ProgressStatus.WAITING.getStatus());
//			
//			// TODO Notification to next role
//		}
//		else
//		{
//			//ตีเรื่องกลับ
//			reg.setRegistrationProgress(flow.getPreviousRole());
//			nextProgress.setProgress(flow.getPreviousRole());
////			nextProgress.setProgressDesc(RoleStatus.LAW.getMeaning()+" "+param.getProgressDesc()==null?"ตีเรื่องกลับ":param.getProgressDesc());
//			nextProgress.setProgressDesc(RoleStatus.getMeaning(flow.getPreviousRole())+" "+ProgressStatus.WAITING.getMeaning());
//			nextProgress.setProgressStatus(ProgressStatus.WAITING.getStatus());
//			nextProgress.setProgressBackStatus(ProgressStatus.BACK.getStatus());
//			
//			// TODO Notification to previous role
//		}
//		registerProgressDAO.insert(nextProgress);
//		
//		lawyerRegistrationDAO.update(reg);
		
	}

}
