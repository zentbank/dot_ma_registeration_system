package sss.dot.tourism.service.registration;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.registration.PlantripDAO;
import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.domain.Country;
import sss.dot.tourism.domain.Plantrip;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.dto.registration.CommitteeDTO;
import sss.dot.tourism.dto.registration.PlantripDTO;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.RecordStatus;

import com.sss.aut.service.User;

@Repository("plantripService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PlantripService implements IPlantripService {

	@Autowired
	PlantripDAO plantripDAO;
	
	@Autowired
	TraderDAO traderDAO;
	
	public List getAll(Object object, User user) throws Exception {
		
		if(!(object instanceof PlantripDTO))
		{
			throw new IllegalArgumentException("object not instanceof PlantripDTO");
		}
		
		PlantripDTO param = (PlantripDTO)object;
		List<PlantripDTO> list = new ArrayList<PlantripDTO>();
		
		Trader trader = (Trader) traderDAO.findByPrimaryKey(param.getTraderId());
		
		List<Plantrip> listPlan = this.plantripDAO.findPlanTripbyTrader(trader.getTraderId(), trader.getRecordStatus());
		if(!listPlan.isEmpty())
		{
			for(Plantrip plantrip : listPlan)
			{
				PlantripDTO dto = new PlantripDTO();
				ObjectUtil.copy(plantrip, dto);
				
				if(plantrip.getCountry() != null)
				{
					dto.setCountryId(plantrip.getCountry().getCountryId());
				}
				
				list.add(dto);
			}
		}
		return list;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List create(Object[] object, User user) throws Exception {
		List<PlantripDTO> list = new ArrayList<PlantripDTO>();
		for (Object obj : object) {
			PlantripDTO dto = (PlantripDTO) obj;
			
			Plantrip plantrip = new Plantrip();
			
			ObjectUtil.copy(dto, plantrip);
			
			if(dto.getTraderId() > 0)
			{
				Trader trader = new Trader();
				trader.setTraderId(dto.getTraderId());
				plantrip.setTrader(trader);
			}
			
			if(dto.getCountryId() > 0)
			{
				Country country = new Country();
				country.setCountryId(dto.getCountryId());
				plantrip.setCountry(country);
			}
			
			plantrip.setRecordStatus(RecordStatus.TEMP.getStatus());
			plantrip.setCreateUser(user.getUserName());
			plantrip.setCreateDtm(new Date());
			
			Long planId = (Long) this.plantripDAO.insert(plantrip);
		
			dto.setPlanId(planId);
			list.add(dto);
		}
		return list;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void update(Object[] object, User user) throws Exception {
		List<PlantripDTO> list = new ArrayList<PlantripDTO>();
		for (Object obj : object) {
			PlantripDTO dto = (PlantripDTO) obj;
			
			Plantrip plantrip = (Plantrip)plantripDAO.findByPrimaryKey(dto.getPlanId());
			
			
			ObjectUtil.copy(dto, plantrip);
			
			if(dto.getTraderId() > 0)
			{
				Trader trader = new Trader();
				trader.setTraderId(dto.getTraderId());
				plantrip.setTrader(trader);
			}
			
			if(dto.getCountryId() > 0)
			{
				Country country = new Country();
				country.setCountryId(dto.getCountryId());
				plantrip.setCountry(country);
			}
			
			
			plantrip.setLastUpdUser(user.getUserName());
			
			this.plantripDAO.update(plantrip);
		
		
		}
	

	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(Object[] object, User user) throws Exception {
		for (Object obj : object) {
			PlantripDTO dto = (PlantripDTO) obj;
			Plantrip addr = (Plantrip)plantripDAO.findByPrimaryKey(dto.getPlanId());
			plantripDAO.delete(addr);
		}

	}

}
