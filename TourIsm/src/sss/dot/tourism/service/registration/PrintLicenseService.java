package sss.dot.tourism.service.registration;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.AdmUserDAO;
import sss.dot.tourism.dao.DotTrFeeLicenseDAO;
import sss.dot.tourism.dao.mas.MasProvinceDAO;
import sss.dot.tourism.dao.mas.MasRunningNoDAO;
import sss.dot.tourism.dao.mas.RegisterFlowDAO;
import sss.dot.tourism.dao.registration.PersonDAO;
import sss.dot.tourism.dao.registration.PlantripDAO;
import sss.dot.tourism.dao.registration.PrintingLicenseDAO;
import sss.dot.tourism.dao.registration.ReceiptDetailDAO;
import sss.dot.tourism.dao.registration.ReceiptMapRegistrationDAO;
import sss.dot.tourism.dao.registration.RegisterProgressDAO;
import sss.dot.tourism.dao.registration.RegistrationDAO;
import sss.dot.tourism.dao.registration.TraderAddressDAO;
import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.domain.AdmUser;
import sss.dot.tourism.domain.DotTrFeeLicense;
import sss.dot.tourism.domain.MasAmphur;
import sss.dot.tourism.domain.MasPosfix;
import sss.dot.tourism.domain.MasPrefix;
import sss.dot.tourism.domain.MasProvince;
import sss.dot.tourism.domain.MasTambol;
import sss.dot.tourism.domain.Person;
import sss.dot.tourism.domain.PrintingLicense;
import sss.dot.tourism.domain.RegisterProgress;
import sss.dot.tourism.domain.Registration;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.domain.TraderAddress;
import sss.dot.tourism.dto.registration.BranchDTO;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.service.trader.ITourCompaniesService;
import sss.dot.tourism.service.trader.ITourLeaderService;
import sss.dot.tourism.service.trader.ITraderGuideService;
import sss.dot.tourism.util.ConstantUtil;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.GuideCategory;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.PersonType;
import sss.dot.tourism.util.ProgressStatus;
import sss.dot.tourism.util.RecordStatus;
import sss.dot.tourism.util.RegistrationType;
import sss.dot.tourism.util.TraderAddressType;
import sss.dot.tourism.util.TraderCategory;
import sss.dot.tourism.util.TraderType;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.sss.aut.service.User;


@Repository("printLicenseService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PrintLicenseService implements IPrintLicenseService{
	
	@Autowired
	PrintingLicenseDAO printingLicenseDAO;
	@Autowired
	TraderDAO traderDAO;
	@Autowired
	RegisterProgressDAO registerProgressDAO;
	@Autowired
	RegistrationDAO registrationDAO;
	@Autowired
	ITourLeaderService tourLeaderService;
	@Autowired
	RegisterFlowDAO registerFlowDAO;
	@Autowired
	IRegistrationService registrationService;
	@Autowired
	IRegisterProgressService registerProgressService;
	@Autowired
	MasRunningNoDAO masRunningNoDAO;
	@Autowired
	ReceiptMapRegistrationDAO receiptMapRegistrationDAO;
	@Autowired
	ReceiptDetailDAO receiptDetailDAO;
	@Autowired
	IApproveRegistration approveRegistrationService;
	
	@Autowired
	PersonDAO personDAO;
	@Autowired
	ITourCompaniesService tourCompaniesService;
	@Autowired
	TraderAddressDAO traderAddressDAO;
	@Autowired
	PlantripDAO plantripDAO;
	@Autowired
	ITraderGuideService guideService;
	
	@Autowired
	MasProvinceDAO masProvinceDAO;
	@Autowired
	AdmUserDAO admUserDAO;
	@Autowired private DotTrFeeLicenseDAO dotTrFeeLicenseDAO;
	
	public FileInputStream getQRCode(Object object, User user) throws Exception {
		System.out.println("###Service DocumentRegistrationService method upload");
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}

		RegistrationDTO params = (RegistrationDTO)object;
		
		PrintingLicense printingLicense = (PrintingLicense)this.printingLicenseDAO.findByPrimaryKey(Integer.parseInt(params.getPrintLicenseId()+""));

		Registration reg = printingLicense.getRegistration();
		Trader trader = reg.getTrader();
		
		String licenseNoStr = trader.getLicenseNo();
		if(trader.getTraderType().equals(TraderType.TOUR_COMPANIES.getStatus()))
		{
			
			licenseNoStr = licenseNoStr.replaceAll("[/]", "B");
		}
		if(trader.getTraderType().equals(TraderType.GUIDE.getStatus()))
		{
			
			licenseNoStr = licenseNoStr.replaceAll("[-]", "G");
			
		}
		if(trader.getTraderType().equals(TraderType.LEADER.getStatus()))
		{
			
			licenseNoStr = licenseNoStr.replaceAll("[.]", "L");
			
		}
		
		String qrData = MessageFormat.format(ConstantUtil.VIEW_LICENSE_PROFILE_URL,trader.getTraderType(), licenseNoStr);
		
		int size = 200;
        String fileType = "png";
		
       // Create the ByteMatrix for the QR-Code that encodes the given String
       Hashtable hintMap = new Hashtable();
       hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
       QRCodeWriter qrCodeWriter = new QRCodeWriter();
       BitMatrix byteMatrix = qrCodeWriter.encode(qrData,
               BarcodeFormat.QR_CODE, size, size, hintMap);
       // Make the BufferedImage that are to hold the QRCode
       int matrixWidth = byteMatrix.getWidth();
       BufferedImage image = new BufferedImage(matrixWidth, matrixWidth,
               BufferedImage.TYPE_INT_RGB);
       image.createGraphics();

       Graphics2D graphics = (Graphics2D) image.getGraphics();
       graphics.setColor(Color.WHITE);
       graphics.fillRect(0, 0, matrixWidth, matrixWidth);
       // Paint and save the image using the ByteMatrix
       graphics.setColor(Color.BLACK);

       for (int i = 0; i < matrixWidth; i++) {
           for (int j = 0; j < matrixWidth; j++) {
               if (byteMatrix.get(i, j)) {
                   graphics.fillRect(i, j, 1, 1);
               }
           }
       }
       
       File pathfile = new File(ConstantUtil.QR_TEMP_LOCATION);
	  if(!pathfile.exists())
	  {
		  pathfile.mkdirs();
	  }
       
       File file = new File(ConstantUtil.QR_TEMP_LOCATION+licenseNoStr+"_"+Calendar.getInstance().getTimeInMillis()+"."+fileType);
       System.out.println("file path: "+(ConstantUtil.QR_TEMP_LOCATION+licenseNoStr+"_"+Calendar.getInstance().getTimeInMillis()+"."+fileType));
       System.out.println("qr code is exists: "+file.exists());
       if(!file.exists()){
    	   file.createNewFile();
       }
       
       ImageIO.write(image, fileType, file);
       FileInputStream fis = new FileInputStream(file);
       
       return fis;
	}
	
	@SuppressWarnings("unchecked")
	public List getPrintLicense(Object object, User user, int start, int limit)
			throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถโหลดข้อมูลได้การจดทะเบียนได้");
		}
		
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		RegistrationDTO params = (RegistrationDTO)object;
		
		/*if((null != params.getPrintLicenseStatus()) && (!"".equals(params.getPrintLicenseStatus())))
	    {
			//ยังไม่พิมพ์ใบอนุญาต
			if(!params.getPrintLicenseStatus().equals(PrintStatus.WAITING.getStatus()))
			{
		    	list = this.getPrintLicenseTrader(object, user, start, limit);
			}
			// พิมพ์ใบอนุญาตแล้ว
			else
			{
				list = this.getPrintLicenseTrader(object, user, start, limit);
			}	
	    }*/
		
		list = this.getPrintLicenseTrader(object, user, start, limit);
		
		return list;

	}
	public List countPrintLicense(Object object, User user ,int start, int limit) throws Exception
	{
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
		RegistrationDTO params = (RegistrationDTO)object;

		return this.printingLicenseDAO.findPrintLicensePaging(params, user, start, limit);
	}
	
	public List getPrintLicenseTrader(Object object, User user ,int start, int limit) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถค้นหาข้อมูลพิมพ์ใบอนุญาตได้");
		}
		
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		RegistrationDTO params = (RegistrationDTO)object;
		
		DateFormat ft =  DateUtils.getProcessDateFormatThai();
		
		int count = 1;
		String checkTN = "";
		String checkTNN = "";
		String checkTGN = "";
		String checkTGNN = "";
		
		long checkRegId = 0;
		long checkRegIdN = 0;
		
//		System.out.println("params: "+ToStringBuilder.reflectionToString(params,ToStringStyle.MULTI_LINE_STYLE));
		
		List<Object[]> listObj = this.printingLicenseDAO.findPrintLicensePaging(params, user, start, limit);
		
		if(!listObj.isEmpty())
		{
			for(Object[] sel: listObj)
			{
				RegistrationDTO dto = new RegistrationDTO();
				
				dto.setRegId(Long.valueOf(sel[0].toString()));
				dto.setRegistrationNo(sel[1]==null?"":sel[1].toString());
				if(sel[2] != null)
				{
					
					dto.setRegistrationDate(ft.format((Date)sel[2]));
				}
				dto.setRegistrationProgress(sel[3]==null?"":sel[3].toString());
				if(sel[8] != null)
				{
					dto.setTraderType(sel[8]==null?"":sel[8].toString());
					dto.setTraderTypeName(TraderType.getMeaning(dto.getTraderType()));
				}
				if(sel[4]!=null)
				{
					dto.setRegistrationType(sel[4]==null?"":sel[4].toString());
					dto.setRegistrationTypeName(RegistrationType.getMeaning(dto.getTraderType(), dto.getRegistrationType()));
				}
				dto.setOrgId(Long.valueOf(sel[5].toString()));
				dto.setRegRecordStatus(sel[6]==null?"":sel[6].toString());
				dto.setTraderId(Long.valueOf(sel[7].toString()));
				dto.setTraderName(sel[9]==null?"":sel[9].toString());
				dto.setTraderNameEn(sel[10]==null?"":sel[10].toString());
				dto.setTraderCategory(sel[11]==null?"":sel[11].toString());
				
			 if(TraderType.TOUR_COMPANIES.equals(dto.getTraderType()))
			 {
				dto.setTraderCategoryName(
						TraderCategory.getTraderCategory(
							dto.getTraderType()
							, dto.getTraderCategory()
						).getMeaning()
				);
			 }else
			 {
				dto.setTraderCategoryName(GuideCategory.getGuideCategory(dto.getTraderCategory()).getMeaning());
			 }

				
				dto.setPersonId(Long.valueOf(sel[12].toString()));
				dto.setPersonType(sel[13]==null?"":sel[13].toString());
				dto.setPrefixName(sel[14]==null?"":sel[14].toString());
				dto.setFirstName(sel[15]==null?"":sel[15].toString());
				dto.setLastName(sel[16]==null?"":sel[16].toString());
				dto.setPostfixName(sel[17]==null?"":sel[17].toString());

				
				dto.setPrefixId(Long.valueOf(sel[18].toString()));
				
				if(sel[19] != null)
				{
					dto.setPostfixId(Long.valueOf(sel[19].toString()));
				}
				
				
				if(sel[20] != null)
				{
					//dto.setAmphurId(Long.valueOf(sel[20].toString()));
				}
				if(sel[21] != null)
				{
					dto.setTaxAmphurId(Long.valueOf(sel[21].toString()));
				}
				if(sel[22] != null)
				{
					//dto.setProvinceId(Long.valueOf(sel[22].toString()));
				}
				if(sel[23] != null)
				{
					dto.setTaxProvinceId(Long.valueOf(sel[23].toString()));
				}
				
				if(sel[24] != null)
				{
					dto.setTraderGuideId(Long.valueOf(sel[24].toString()));
				}
				
				dto.setPronunciationName(sel[25]==null?"":sel[25].toString());
			    dto.setLicenseGuideNo(sel[26]==null?"":sel[26].toString());
			    dto.setPersonType(sel[27]==null?"":sel[27].toString());
			    dto.setIdentityNo(sel[28]==null?"":sel[28].toString());
			    dto.setCommitteeName1(sel[29]==null?"":sel[29].toString());
			    dto.setCommitteeName2(sel[30]==null?"":sel[30].toString());
			    dto.setCommitteeNameSign(sel[31]==null?"":sel[31].toString());
			    dto.setPassportNo(sel[32]==null?"":sel[32].toString());
			    dto.setFirstNameEn(sel[33]==null?"":sel[33].toString());
			    dto.setLastNameEn(sel[34]==null?"":sel[34].toString());
			    dto.setGender(sel[35]==null?"":sel[35].toString());
			    dto.setPersonNationality(sel[36]==null?"":sel[36].toString());


			    if(sel[37] != null)
				{
					
					dto.setBirthDate(ft.format((Date)sel[37]));
				}
			    
			    dto.setAgeYear(sel[38]==null?0:Integer.valueOf(sel[38].toString()));
			    if(sel[39] != null)
				{
					
					dto.setIdentityNoExpire(ft.format((Date)sel[39]));
				}
			   
			    dto.setCorporateType(sel[40]==null?"":sel[40].toString());
			    dto.setTaxIdentityNo(sel[41]==null?"":sel[41].toString());
			    if(sel[42] != null)
				{
					
					dto.setIdentityDate(ft.format((Date)sel[42]));
				}
			    dto.setTraderRecordStatus(sel[43]==null?"":sel[43].toString());
			    
			    dto.setAmphurName(sel[44]==null?"":sel[44].toString());
			    dto.setTaxAmphurName(sel[45]==null?"":sel[45].toString());
			    dto.setProvinceName(sel[46]==null?"":sel[46].toString());
			    dto.setTaxProvinceName(sel[47]==null?"":sel[47].toString());
			    dto.setPrefixNameEn(sel[48]==null?"":sel[48].toString());
			    dto.setLicenseNo(sel[49]==null?"":sel[49].toString());
			    
			    //Oat Edit 16/09/57
			    //ถ้าเพิ่ม list ของการเงินมา ต้อง เช็ค size sel 
			    if(sel.length > 50)
			    {
				    //Oat Edit 12/09/57
	//			    dto.setPrintLicenseId(Long.valueOf(sel[50].toString()));
				    if(sel[50] != null)
				    {
				    	dto.setPrintLicenseId(Long.valueOf(sel[50].toString()));
				    }
				    //End Oat Edit 12/09/57
				    if(sel[51] != null)
					{
				    dto.setPrintLicenseDate(ft.format((Date)sel[51]));
					}
				    
				    if(sel[52] != null)
				    {
				    	dto.setPrintLicenseStatus(sel[52]==null?"":sel[52].toString());
				    }
				    
				    if(sel[53] != null)
				    {
				    	dto.setTraderBranchNo(Integer.valueOf(sel[53].toString()));
				    }
				   
				    if(sel[54] != null)
				    {
				    	dto.setApproveDate(ft.format((Date)sel[54]));
				    }
				    if(sel[55] != null)
				    {
				    	dto.setBranchType(sel[55]==null?"":sel[55].toString());
				    }
				    
			    }
			    //End Oat Edit 16/09/57
				
				Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(dto.getRegId());
				if(TraderType.TOUR_COMPANIES.getStatus().equals(dto.getTraderType()))
				{
					tourCompaniesService.getRegistration(reg, dto, user);
				}
				
				if(TraderType.GUIDE.getStatus().equals(dto.getTraderType()))
				{
					guideService.getRegistration(reg, dto, user);
				}
				
				if(TraderType.LEADER.getStatus().equals(dto.getTraderType()))
				{
					tourLeaderService.getRegistration(reg, dto, user);
				}
				
				List<RegisterProgress> listprogress =  registerProgressDAO.findRegisterProgressbyRegistration(reg.getRegId(), ProgressStatus.GET_IT.getStatus());
				
				if(!listprogress.isEmpty())
				{
					RegisterProgress progress = listprogress.get(0);
					
					String authorityName = progress.getAdmUser().getMasPrefix().getPrefixName() + progress.getAdmUser().getUserName() +" "
							+progress.getAdmUser().getUserLastname();
					
					dto.setAuthorityName(authorityName);
				}
				
				//สามารถแก้ไขข้อมูลได้ ถ้า RegistrationProgress อยู่ที่ role ตัวเอง
				/**
				 * RECHECK
				 * SUPERVISOR
				 * MANAGER
				 * DIRECTOR
				 */
//				System.out.println("regProgress===" + regProgress);
//				System.out.println("regProgress===" + dto.getRegistrationProgress());
//				dto.setRoleAction("1");
//				if(dto.getRegistrationProgress().equals(params.getRegistrationProgress()))
//				{
//					
//					//CHECK ว่าเป็นเรื่องดีกลับหรือไม่
//					List<RegisterProgress> listBackStatus =  registerProgressDAO.findProgressBackStatusbyRegistration(reg.getRegId(), dto.getRegistrationProgress());
//					if(!listBackStatus.isEmpty())
//					{
//						RegisterProgress role = listBackStatus.get(0);
//						dto.setProgressStatus(role.getProgressStatus());
//						dto.setProgressBackStatus(ProgressStatus.BACK.getStatus());
//					}
//				}
				
				//Oat Edit 16/09/57 เพิ่ม dto.getPrintLicenseStatus() != null
				if(dto.getPrintLicenseStatus() != null && dto.getPrintLicenseStatus().equals("W"))
				{
					dto.setRoleAction("1");
				}else if(dto.getPrintLicenseStatus() != null)
				{
					dto.setRoleAction("P");
				}
				
				if(dto.getTraderBranchNo() == null)
				{
					dto.setDetailAction("1");
				}
				
				checkTN = dto.getTraderName();
				checkTGN = dto.getTraderCategoryName();
				checkRegId = dto.getRegId();
				if(PersonType.CORPORATE.getStatus().equals(dto.getPersonType()) && TraderType.TOUR_COMPANIES.equals(dto.getTraderType()))
				{
//					if((checkTNN.equals(checkTN)) && (checkTGNN.equals(checkTGN)) && (checkRegId == checkRegIdN) && dto.getTraderBranchNo() > 0)
					if(dto.getBranchType() != null)
					{
						//Oat Edit 18/09/57
						dto.setTraderOwnerName("สาขา "+dto.getTraderBranchNo());
//						dto.setTraderOwnerName("สาขา "+count);
						//End Oat Edit 18/09/57
						count ++;
					}else
					{
						String traderOwnerName = dto.getPrefixName()+" "+dto.getFirstName() +" "+dto.getPostfixName();
						dto.setTraderOwnerName(traderOwnerName);
						count = 1;
					}
				//Oat Add 18/09/57
				}else if(PersonType.INDIVIDUAL.getStatus().equals(dto.getPersonType()) && TraderType.TOUR_COMPANIES.equals(dto.getTraderType()))
				{
					if(dto.getBranchType() != null)
					{
						dto.setTraderOwnerName("สาขา "+dto.getTraderBranchNo());
						count ++;
					}else
					{
						String traderOwnerName = dto.getPrefixName()+dto.getFirstName() +" "+dto.getLastName();
						dto.setTraderOwnerName(traderOwnerName);
						count = 1;
					}
				}
				//End Oat Add 18/09/57
				else
				{
				  	
				/*	if((checkTNN.equals(checkTN)) && (checkTGNN.equals(checkTGN)) && (checkRegId == checkRegIdN) && dto.getTraderBranchNo() > 0)
					{
						dto.setTraderOwnerName("สาขา "+count);
						count ++;
					}else
					{*/
						String traderOwnerName = dto.getPrefixName()+dto.getFirstName() +"  "+ dto.getLastName();
						//String guideName  = traderOwnerName + " (" + dto.getLicenseNo() +")";
						dto.setTraderOwnerName(traderOwnerName);
						//dto.setGuideName(guideName);
						count = 1;
					//}
					
				}
				
				System.out.println("dto.getLicenseNo() = "+dto.getLicenseNo());
				
				if(StringUtils.isNotEmpty(dto.getLicenseNo())){
					dto.setHeaderInfo("เลขที่ใบอนุญาต "+ dto.getLicenseNo());
				}
				//ทำให้ระบบช้า เวลาดึงข้อมูล timeout 
//				List<Trader> listTrader = this.traderDAO.findTraderByLicenseNo(dto.getLicenseNo(), dto.getTraderType(), "N", user.getUserData().getOrgId()) ;
//				
//				//Oat Edit 16/09/57 เพิ่ม && listTrader.size() == 1 ถ้าจดใหม่จะไม่มี licenseNo ทำให้ listTrader มากกว่า 1
//				if(!listTrader.isEmpty() && listTrader.size() == 1)
//				{
//					Trader trader = listTrader.get(0);
//					
//					if(TraderType.TOUR_COMPANIES.getStatus().equals(dto.getTraderType()))
//					{
//						
////						dto.setHeaderInfo(trader.getTraderName()+" เลขที่ใบอนุญาต "+trader.getLicenseNo());
//						dto.setHeaderInfo("เลขที่ใบอนุญาต "+trader.getLicenseNo());
//					}else
//					{
//						
////						dto.setHeaderInfo(dto.getTraderOwnerName()+" เลขที่ใบอนุญาต "+trader.getLicenseNo());
//						//dto.setHeaderInfo("เลขที่ใบอนุญาต "+trader.getLicenseNo());
//					}
//				}
				
				
				
			    checkTNN = checkTN;
				checkTGNN = checkTGN;
				checkRegIdN = checkRegId;
				
				dto.setToday(ft.format((Date)new Date()));
				
				dto.setImageFile(null==sel[56]?"":sel[56].toString());
				
				//ตรวจสอบว่ายื่น online หรือเปล่า
				List<DotTrFeeLicense> listFee = (List<DotTrFeeLicense>)this.dotTrFeeLicenseDAO.findTrFeeLicenseByRegId(dto.getRegId());
				if(CollectionUtils.isNotEmpty(listFee)){
					dto.setRoleColor("true");
				}
				
				list.add(dto);
				
				
			}

		}
		
		return list;
	}
	
//	//Oat Add 16/09/57 ยังไม่ได้ใช้
//	public List countPrintLicense2(Object object, User user ,int start, int limit) throws Exception
//	{
//		if(!(object instanceof RegistrationDTO))
//		{
//			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
//		}
//		
//		
//		RegistrationDTO params = (RegistrationDTO)object;
//		
//		//Oat Edit 16/09/57
//		List<Object[]> listObj = this.printingLicenseDAO.findPrintLicensePaging(params, user, start, limit);
//		
//		if(params.getPrintLicenseStatus().equals("W"))
//		{
//			params.setTraderRecordStatus(RecordStatus.TEMP.getStatus());
//		    params.setRegRecordStatus(RecordStatus.TEMP.getStatus());
//			List<Object[]> listObj2 = this.printingLicenseDAO.findBetweenRegistrationPaging2(params, user , start, limit);
//			listObj.addAll(listObj2);
//		}
////		return this.printingLicenseDAO.findPrintLicensePaging(params, user, start, limit);
//		
//		return listObj;
//		
//		//End Oat Edit 16/09/57
//	}
//	//
//
//	//Oat Add 16/09/57 ยังไม่ได้ใช้
//	public List getPrintLicenseTrader2(Object object, User user ,int start, int limit) throws Exception {
//		if(!(object instanceof RegistrationDTO))
//		{
//			throw new IllegalArgumentException("ไม่สามารถค้นหาข้อมูลพิมพ์ใบอนุญาตได้");
//		}
//		
//		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
//		
//		RegistrationDTO params = (RegistrationDTO)object;
//		
//		DateFormat ft =  DateUtils.getProcessDateFormatThai();
//		
//		int count = 1;
//		String checkTN = "";
//		String checkTNN = "";
//		String checkTGN = "";
//		String checkTGNN = "";
//		
//		long checkRegId = 0;
//		long checkRegIdN = 0;
//		
//		//Oat Add 16/09/57 
//		//calculate size list to display สำหรับสถานะเรื่องที่ยังไม่ได้พิมพ์ใบอนุญาต
//		params.setTraderRecordStatus(RecordStatus.TEMP.getStatus());
//	    params.setRegRecordStatus(RecordStatus.TEMP.getStatus());
//		List<Object[]> listObj2All = this.printingLicenseDAO.findBetweenRegistrationPaging2(params, user , 0, Integer.MAX_VALUE);
//		
//		int page = (start/20)+1;
//		int divid = listObj2All.size()%20;
//		int pageObj2= (listObj2All.size()/20)+1;
//		int start2 = 0;
//		int limit2 = 20;
//		
//		if(page < pageObj2)
//		{
//			start2 = 0;
//			limit2 = 0;
//		}
//		else if(page == pageObj2)
//		{
//			start2 = 0;
//			limit2 = 20-divid;
//		}
//		else
//		{
//			start2 = ((page-pageObj2)*20)-divid;
//			limit2 = 20;
//		}
//		//
//		
//		List<Object[]> listObj = null;
//		List<Object[]> listObj2 = null;
//		
//		//เช็คว่าถ้าเลือกยังไม่ได้พิมพ์ใบอนุญาต ถึง add list เพิ่ม
//		if(params.getPrintLicenseStatus().equals("W"))
//		{
//			//list1
//			listObj = this.printingLicenseDAO.findPrintLicensePaging(params, user, start2, limit2);
//			System.out.println("###listObj.size() = "+listObj.size());
//			
//			//list2
//			params.setTraderRecordStatus(RecordStatus.TEMP.getStatus());
//		    params.setRegRecordStatus(RecordStatus.TEMP.getStatus());
//			listObj2 = this.printingLicenseDAO.findBetweenRegistrationPaging2(params, user , start, limit);
//			System.out.println("###listObj2.size() = "+listObj2.size());
//			//list All
//			listObj.addAll(listObj2);
//			System.out.println("###listObj.size() after AddAll = "+listObj.size());
//		}
//		else if(params.getPrintLicenseStatus().equals("A"))
//		{
//			listObj = this.printingLicenseDAO.findPrintLicensePaging(params, user, start, limit);
//			System.out.println("###listObj.size() = "+listObj.size());
//		}
//		//End Oat Add 16/09/57
//		
//		if(!listObj.isEmpty())
//		{
//			for(Object[] sel: listObj)
//			{
//				RegistrationDTO dto = new RegistrationDTO();
//				
//				dto.setRegId(Long.valueOf(sel[0].toString()));
//				dto.setRegistrationNo(sel[1]==null?"":sel[1].toString());
//				if(sel[2] != null)
//				{
//					
//					dto.setRegistrationDate(ft.format((Date)sel[2]));
//				}
//				dto.setRegistrationProgress(sel[3]==null?"":sel[3].toString());
//				if(sel[8] != null)
//				{
//					dto.setTraderType(sel[8]==null?"":sel[8].toString());
//					dto.setTraderTypeName(TraderType.getMeaning(dto.getTraderType()));
//				}
//				if(sel[4]!=null)
//				{
//					dto.setRegistrationType(sel[4]==null?"":sel[4].toString());
//					dto.setRegistrationTypeName(RegistrationType.getMeaning(dto.getTraderType(), dto.getRegistrationType()));
//				}
//				dto.setOrgId(Long.valueOf(sel[5].toString()));
//				dto.setRegRecordStatus(sel[6]==null?"":sel[6].toString());
//				dto.setTraderId(Long.valueOf(sel[7].toString()));
//				dto.setTraderName(sel[9]==null?"":sel[9].toString());
//				dto.setTraderNameEn(sel[10]==null?"":sel[10].toString());
//				dto.setTraderCategory(sel[11]==null?"":sel[11].toString());
//				
//			 if(TraderType.TOUR_COMPANIES.equals(dto.getTraderType()))
//			 {
//				dto.setTraderCategoryName(
//						TraderCategory.getTraderCategory(
//							dto.getTraderType()
//							, dto.getTraderCategory()
//						).getMeaning()
//				);
//			 }else
//			 {
//				dto.setTraderCategoryName(GuideCategory.getGuideCategory(dto.getTraderCategory()).getMeaning());
//			 }
//
//				
//				dto.setPersonId(Long.valueOf(sel[12].toString()));
//				dto.setPersonType(sel[13]==null?"":sel[13].toString());
//				dto.setPrefixName(sel[14]==null?"":sel[14].toString());
//				dto.setFirstName(sel[15]==null?"":sel[15].toString());
//				dto.setLastName(sel[16]==null?"":sel[16].toString());
//				dto.setPostfixName(sel[17]==null?"":sel[17].toString());
//
//				
//				dto.setPrefixId(Long.valueOf(sel[18].toString()));
//				
//				if(sel[19] != null)
//				{
//					dto.setPostfixId(Long.valueOf(sel[19].toString()));
//				}
//				
//				
//				if(sel[20] != null)
//				{
//					//dto.setAmphurId(Long.valueOf(sel[20].toString()));
//				}
//				if(sel[21] != null)
//				{
//					dto.setTaxAmphurId(Long.valueOf(sel[21].toString()));
//				}
//				if(sel[22] != null)
//				{
//					//dto.setProvinceId(Long.valueOf(sel[22].toString()));
//				}
//				if(sel[23] != null)
//				{
//					dto.setTaxProvinceId(Long.valueOf(sel[23].toString()));
//				}
//				
//				if(sel[24] != null)
//				{
//					dto.setTraderGuideId(Long.valueOf(sel[24].toString()));
//				}
//				
//				dto.setPronunciationName(sel[25]==null?"":sel[25].toString());
//			    dto.setLicenseGuideNo(sel[26]==null?"":sel[26].toString());
//			    dto.setPersonType(sel[27]==null?"":sel[27].toString());
//			    dto.setIdentityNo(sel[28]==null?"":sel[28].toString());
//			    dto.setCommitteeName1(sel[29]==null?"":sel[29].toString());
//			    dto.setCommitteeName2(sel[30]==null?"":sel[30].toString());
//			    dto.setCommitteeNameSign(sel[31]==null?"":sel[31].toString());
//			    dto.setPassportNo(sel[32]==null?"":sel[32].toString());
//			    dto.setFirstNameEn(sel[33]==null?"":sel[33].toString());
//			    dto.setLastNameEn(sel[34]==null?"":sel[34].toString());
//			    dto.setGender(sel[35]==null?"":sel[35].toString());
//			    dto.setPersonNationality(sel[36]==null?"":sel[36].toString());
//
//
//			    if(sel[37] != null)
//				{
//					
//					dto.setBirthDate(ft.format((Date)sel[37]));
//				}
//			    
//			    dto.setAgeYear(sel[38]==null?0:Integer.valueOf(sel[38].toString()));
//			    if(sel[39] != null)
//				{
//					
//					dto.setIdentityNoExpire(ft.format((Date)sel[39]));
//				}
//			   
//			    dto.setCorporateType(sel[40]==null?"":sel[40].toString());
//			    dto.setTaxIdentityNo(sel[41]==null?"":sel[41].toString());
//			    if(sel[42] != null)
//				{
//					
//					dto.setIdentityDate(ft.format((Date)sel[42]));
//				}
//			    dto.setTraderRecordStatus(sel[43]==null?"":sel[43].toString());
//			    
//			    dto.setAmphurName(sel[44]==null?"":sel[44].toString());
//			    dto.setTaxAmphurName(sel[45]==null?"":sel[45].toString());
//			    dto.setProvinceName(sel[46]==null?"":sel[46].toString());
//			    dto.setTaxProvinceName(sel[47]==null?"":sel[47].toString());
//			    dto.setPrefixNameEn(sel[48]==null?"":sel[48].toString());
//			    dto.setLicenseNo(sel[49]==null?"":sel[49].toString());
//			    
//			    //Oat Edit 16/09/57
//			    //ถ้าเพิ่ม list ของการเงินมา ต้อง เช็ค size sel 
//			    if(sel.length > 50)
//			    {
//				    //Oat Edit 12/09/57
//	//			    dto.setPrintLicenseId(Long.valueOf(sel[50].toString()));
//				    if(sel[50] != null)
//				    {
//				    	dto.setPrintLicenseId(Long.valueOf(sel[50].toString()));
//				    }
//				    //End Oat Edit 12/09/57
//				    if(sel[51] != null)
//					{
//				    dto.setPrintLicenseDate(ft.format((Date)sel[51]));
//					}
//				    
//				    if(sel[52] != null)
//				    {
//				    	dto.setPrintLicenseStatus(sel[52]==null?"":sel[52].toString());
//				    }
//				    
//				    if(sel[53] != null)
//				    {
//				    	dto.setTraderBranchNo(Integer.valueOf(sel[53].toString()));
//				    }
//				   
//				    if(sel[54] != null)
//				    {
//				    	dto.setApproveDate(ft.format((Date)sel[54]));
//				    }
//				    if(sel[55] != null)
//				    {
//				    	dto.setBranchType(sel[55]==null?"":sel[55].toString());
//				    }
//				    
//			    }
//			    //End Oat Edit 16/09/57
//				
//				Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(dto.getRegId());
//				if(TraderType.TOUR_COMPANIES.getStatus().equals(dto.getTraderType()))
//				{
//					tourCompaniesService.getRegistration(reg, dto, user);
//				}
//				
//				if(TraderType.GUIDE.getStatus().equals(dto.getTraderType()))
//				{
//					guideService.getRegistration(reg, dto, user);
//				}
//				
//				if(TraderType.LEADER.getStatus().equals(dto.getTraderType()))
//				{
//					tourLeaderService.getRegistration(reg, dto, user);
//				}
//				
//				List<RegisterProgress> listprogress =  registerProgressDAO.findRegisterProgressbyRegistration(reg.getRegId(), ProgressStatus.GET_IT.getStatus());
//				
//				if(!listprogress.isEmpty())
//				{
//					RegisterProgress progress = listprogress.get(0);
//					
//					String authorityName = progress.getAdmUser().getMasPrefix().getPrefixName() + progress.getAdmUser().getUserName() +" "
//							+progress.getAdmUser().getUserLastname();
//					
//					dto.setAuthorityName(authorityName);
//				}
//				
//				//สามารถแก้ไขข้อมูลได้ ถ้า RegistrationProgress อยู่ที่ role ตัวเอง
//				/**
//				 * RECHECK
//				 * SUPERVISOR
//				 * MANAGER
//				 * DIRECTOR
//				 */
//				
//				//Oat Edit 16/09/57 เพิ่ม dto.getPrintLicenseStatus() != null
//				if(dto.getPrintLicenseStatus() != null && dto.getPrintLicenseStatus().equals("W"))
//				{
//					dto.setRoleAction("1");
//				}else if(dto.getPrintLicenseStatus() != null)
//				{
//					dto.setRoleAction("P");
//				}
//				
//				if(dto.getTraderBranchNo() == null)
//				{
//					dto.setDetailAction("1");
//				}
//				
//				checkTN = dto.getTraderName();
//				checkTGN = dto.getTraderCategoryName();
//				checkRegId = dto.getRegId();
//				if(PersonType.CORPORATE.getStatus().equals(dto.getPersonType()) && TraderType.TOUR_COMPANIES.equals(dto.getTraderType()))
//				{
////					if((checkTNN.equals(checkTN)) && (checkTGNN.equals(checkTGN)) && (checkRegId == checkRegIdN) && dto.getTraderBranchNo() > 0)
//					if(dto.getBranchType() != null)
//					{
//						//Oat Edit 18/09/57
//						dto.setTraderOwnerName("สาขา "+dto.getTraderBranchNo());
////						dto.setTraderOwnerName("สาขา "+count);
//						//End Oat Edit 18/09/57
//						count ++;
//					}else
//					{
//						String traderOwnerName = dto.getPrefixName()+" "+dto.getFirstName() +" "+dto.getPostfixName();
//						dto.setTraderOwnerName(traderOwnerName);
//						count = 1;
//					}
//				//Oat Add 18/09/57
//				}else if(PersonType.INDIVIDUAL.getStatus().equals(dto.getPersonType()) && TraderType.TOUR_COMPANIES.equals(dto.getTraderType()))
//				{
//					if(dto.getBranchType() != null)
//					{
//						dto.setTraderOwnerName("สาขา "+dto.getTraderBranchNo());
//						count ++;
//					}else
//					{
//						String traderOwnerName = dto.getPrefixName()+" "+dto.getFirstName() +" "+dto.getPostfixName();
//						dto.setTraderOwnerName(traderOwnerName);
//						count = 1;
//					}
//				}
//				//End Oat Add 18/09/57
//				else
//				{
//				  	
//				/*	if((checkTNN.equals(checkTN)) && (checkTGNN.equals(checkTGN)) && (checkRegId == checkRegIdN) && dto.getTraderBranchNo() > 0)
//					{
//						dto.setTraderOwnerName("สาขา "+count);
//						count ++;
//					}else
//					{*/
//						String traderOwnerName = dto.getPrefixName()+dto.getFirstName() +"  " + dto.getLastName();
//						//String guideName  = traderOwnerName + " (" + dto.getLicenseNo() +")";
//						dto.setTraderOwnerName(traderOwnerName);
//						//dto.setGuideName(guideName);
//						count = 1;
//					//}
//					
//				}
//				
//				System.out.println("dto.getLicenseNo() = "+dto.getLicenseNo());
//				
//				List<Trader> listTrader = this.traderDAO.findTraderByLicenseNo(dto.getLicenseNo(), dto.getTraderType(), "N", user.getUserData().getOrgId()) ;
//				
//				//Oat Edit 16/09/57 เพิ่ม && listTrader.size() == 1 ถ้าจดใหม่จะไม่มี licenseNo ทำให้ listTrader มากกว่า 1
//				if(!listTrader.isEmpty() && listTrader.size() == 1)
//				{
//					Trader trader = listTrader.get(0);
//					
//					if(TraderType.TOUR_COMPANIES.getStatus().equals(dto.getTraderType()))
//					{
//						
////						dto.setHeaderInfo(trader.getTraderName()+" เลขที่ใบอนุญาต "+trader.getLicenseNo());
//						dto.setHeaderInfo("เลขที่ใบอนุญาต "+trader.getLicenseNo());
//					}else
//					{
//						
////						dto.setHeaderInfo(dto.getTraderOwnerName()+" เลขที่ใบอนุญาต "+trader.getLicenseNo());
//						//dto.setHeaderInfo("เลขที่ใบอนุญาต "+trader.getLicenseNo());
//					}
//				}
//				
//				
//				
//			    checkTNN = checkTN;
//				checkTGNN = checkTGN;
//				checkRegIdN = checkRegId;
//				
//				dto.setToday(ft.format((Date)new Date()));
//				
//				list.add(dto);
//				
//				
//			}
//
//		}
////		System.out.println("start = "+start+", limit = "+limit);
////		System.out.println("start2 = "+start2+", limit2 = "+limit2);
////		System.out.println("listObjAll = "+listObj.size());
////		System.out.println("listObj2 = "+listObj2.size());
////		System.out.println("###list = "+list.size());
//		return list;
//	}
	//
	

	public List getPrintLicneseById(int printId, User user) throws Exception {

		PrintingLicense pril = (PrintingLicense) this.printingLicenseDAO
				.findByPrimaryKey(printId);
		Trader trader = pril.getTrader();
		
		Registration reg = pril.getRegistration();
		
		MasProvince province = trader.getMasProvince(); 

		RegistrationDTO dto = new RegistrationDTO();
		
		
		
		dto.setPrintLicenseId(pril.getPrintLicenseId());
		dto.setTraderId(trader.getTraderId());
		dto.setRegId(pril.getRegistration().getRegId());
		
		// sek edit
		dto.setRegistrationType(pril.getRegistration().getRegistrationType());
		// sek edit end
		dto.setBranchType(trader.getBranchType()==null?null:trader.getBranchType());
		dto.setLicenseNo(trader.getLicenseNo());
		dto.setTraderType(trader.getTraderType());
		dto.setTraderCategory(trader.getTraderCategory());
		
		if(TraderType.TOUR_COMPANIES.equals(dto.getTraderType()))
		{
			dto.setTraderCategoryName(TraderCategory.getTraderCategory(
				dto.getTraderType(), dto.getTraderCategory()).getMeaning());
			if(dto.getTraderCategoryName().equals("ในประเทศ"))
			{
				dto.setTraderCategoryName("ในประเทศ");
			}
		}else
		{
			if(TraderCategory.PINK_GUIDE.equals(dto.getTraderType(), dto.getTraderCategory()))
			{
				String name = "ประเภท เฉพาะพื้นที่ "+(province==null?"":" จ. "+province.getProvinceName())+" (ให้บริการนำเที่ยวแก่นักท่องเที่ยวไทยและต่างประเทศ)";
				dto.setTraderCategoryName(name);
				long provinceGuideId = (Long) (province==null?0:province.getProvinceId());
				String provinceGuideServiceName = (province==null?"":province.getProvinceName());
				dto.setProvinceGuideServiceId(provinceGuideId);
				dto.setProvinceGuideServiceName(provinceGuideServiceName);
				String traderArea = (!trader.getTraderArea().equals("")&&trader.getTraderArea()!=null?"":trader.getTraderArea());
				dto.setTraderArea(traderArea);
			}else if(TraderCategory.BLUE_GUIDE.equals(dto.getTraderType(), dto.getTraderCategory()))
			{
				String name = "ประเภท เฉพาะพื้นที่ "+(province==null?"":" จ. "+province.getProvinceName())+" (ให้บริการนำเที่ยวเฉพาะนักท่องเที่ยวไทย)";
				dto.setTraderCategoryName(name);
				long provinceGuideId = (Long) (province==null?0:province.getProvinceId());
				String provinceGuideServiceName = (province==null?"":province.getProvinceName());
				dto.setProvinceGuideServiceId(provinceGuideId);
				dto.setProvinceGuideServiceName(provinceGuideServiceName);
				String traderArea = (!trader.getTraderArea().equals("")&&trader.getTraderArea()!=null?"":trader.getTraderArea());
				dto.setTraderArea(traderArea);
			}else if(TraderCategory.VIOLET_GUIDE.equals(dto.getTraderType(), dto.getTraderCategory()))
			{
				String traderCategoryNameOth1 = "ประเภทเฉพาะ แหล่งท่องเที่ยวธรรมชาติ "+(!trader.getTraderArea().equals("")&&trader.getTraderArea()!=null?trader.getTraderArea():(province==null?"":"จ. "+province.getProvinceName()));
				String traderCategoryNameOth2 = " (ให้บริการนำเที่ยวแก่นักท่องเที่ยวไทยและต่างประเทศในแหล่งท่องเที่ยวธรรมชาติ)";
				dto.setTraderCategoryNameOth1(traderCategoryNameOth1);
				dto.setTraderCategoryNameOth2(traderCategoryNameOth2);
				dto.setTraderCategoryName(traderCategoryNameOth1+traderCategoryNameOth2);
				String traderArea = (!trader.getTraderArea().equals("")&&trader.getTraderArea()!=null?"":trader.getTraderArea());
				dto.setTraderArea(traderArea);
				
				//Oat Edit
//				long provinceGuideId = (Long) (province==null?"":province.getProvinceId());
				long provinceGuideId = (Long) (province==null?0:province.getProvinceId());
				//End Oat Edit
				
				String provinceGuideServiceName = (province==null?"":province.getProvinceName());
				dto.setProvinceGuideServiceId(provinceGuideId);
				dto.setProvinceGuideServiceName(provinceGuideServiceName);
			}else if(TraderCategory.BROWN_GUIDE.equals(dto.getTraderType(), dto.getTraderCategory()))
			{
				String traderCategoryNameOth1 = "ประเภทเฉพาะ วัฒนธรรมท้องถิ่น ";
			    String traderCategoryNameOth2 = (province==null?"":"จ. "+province.getProvinceName())+" (ให้บริการนำเที่ยวแก่นักท่องเที่ยวไทยและต่างประเทศด้านวัฒนธรรมท้องถิ่น)";
			    dto.setTraderCategoryNameOth1(traderCategoryNameOth1);
				dto.setTraderCategoryNameOth2(traderCategoryNameOth2);
			    dto.setTraderCategoryName(traderCategoryNameOth1+traderCategoryNameOth2);
			    
			  //Oat Edit
//			    long provinceGuideId = (Long) (province==null?"":province.getProvinceId());
			    long provinceGuideId = (Long) (province==null?0:province.getProvinceId());
			  //End Oat Edit
			    
				String provinceGuideServiceName = (province==null?"":province.getProvinceName());
				dto.setProvinceGuideServiceId(provinceGuideId);
				dto.setProvinceGuideServiceName(provinceGuideServiceName);
				String traderArea = (!trader.getTraderArea().equals("")&&trader.getTraderArea()!=null?"":trader.getTraderArea());
				dto.setTraderArea(traderArea);
			}
			else
			{
				dto.setTraderCategoryName(GuideCategory.getGuideCategory(dto.getTraderCategory()).getMeaning());
				long provinceGuideId = (Long) (province==null?0:province.getProvinceId());
				String provinceGuideServiceName = (province==null?"":province.getProvinceName());
				dto.setProvinceGuideServiceId(provinceGuideId);
				dto.setProvinceGuideServiceName(provinceGuideServiceName);
				if(null != trader.getTraderArea()){
					String traderArea = (!trader.getTraderArea().equals("")&&trader.getTraderArea()!=null?"":trader.getTraderArea());
					dto.setTraderArea(traderArea);
				}
				
			}
		}
		dto.setTraderName(trader.getTraderName());
		dto.setTraderNameEn(trader.getTraderNameEn());
		dto.setPronunciationName(trader.getPronunciationName());
		dto.setPersonId(trader.getPerson().getPersonId());

		Person person = (Person) trader.getPerson();
		
		//Oat Edit 31/07/2014
//		dto.setIdentityNo(TraderType.TOUR_COMPANIES.getStatus().equals(dto.getTraderType())?person.getIdentityNo():"-");
		dto.setIdentityNo((TraderType.TOUR_COMPANIES.getStatus().equals(dto.getTraderType()) && PersonType.CORPORATE.getStatus().equals(person.getPersonType()))?person.getIdentityNo():"-");
		//End Oat Edit
		
		dto.setPersonType(person.getPersonType());
		
		dto.setPrefixName(person.getMasPrefix()==null?"":person.getMasPrefix().getPrefixName());
		
		if (person.getFirstNameEn() != null
				&& !person.getFirstNameEn().equals("")) {
			if (!person.getLastNameEn().equals("")
					&& person.getLastNameEn() != null) {
				dto.setFirstNameEn(person.getFirstNameEn() + "  "
						+ person.getLastNameEn());
			} else {
				dto.setFirstNameEn(person.getFirstNameEn());
			}
		}
		
		//dto.setFirstNameEn(person.getFirstNameEn()==null?"":person.getFirstNameEn());
		//dto.setLastNameEn(person.getLastNameEn()==null?"":person.getLastNameEn());
		
		dto.setLastName(person.getLastName());
		dto.setFirstName(person.getFirstName());
		
		if (PersonType.CORPORATE.getStatus().equals(dto.getPersonType())) {
			dto.setPostfixId(person.getMasPosfix()==null?0:person.getMasPosfix().getPostfixId());
			dto.setPostfixName(person.getMasPosfix()==null?"":person.getMasPosfix().getPostfixName());
			String traderOwnerName = dto.getPrefixName() + " "
					+ dto.getFirstName() + " "+dto.getPostfixName();

			dto.setTraderOwnerName(traderOwnerName);
		} 
		else {
			if (TraderType.TOUR_COMPANIES.getStatus().equals(
					dto.getTraderType())) 
			{
				if(PersonType.CORPORATE.getStatus().equals(dto.getPersonType()))
				{
					dto.setFirstName(person.getFirstName());
					String traderOwnerName = dto.getPrefixName()+" "
							+ dto.getFirstName() + " " + dto.getLastName();
					dto.setTraderOwnerName(traderOwnerName);
				}else {
					dto.setFirstName(person.getFirstName());
					String traderOwnerName = dto.getPrefixName()+dto.getFirstName() + "  " + dto.getLastName();
					dto.setTraderOwnerName(traderOwnerName);
				}
			} else {
				dto.setFirstName(person.getFirstName());
				String traderOwnerName = dto.getPrefixName()
						+ dto.getFirstName() + "  " + dto.getLastName();
				dto.setTraderOwnerName(traderOwnerName);
			}
		}
		
		dto.setExpireDate(DateUtils.getProcessDateFormatThai().format(trader.getExpireDate()));
		System.out.println(dto.getExpireDate());
		Date date = new Date();
		DateFormat ftDay = DateUtils.getDayFormat();
		DateFormat ftMonth = DateUtils.getMonthNameFormat();
		DateFormat ftYear = DateUtils.getBuddhaYearFormat();
		DateFormat ftYearEn = DateUtils.getYearFormat();
		String dayF = ftDay.format(trader.getExpireDate());
		int parseDay = Integer.valueOf(dayF);
		parseDay = parseDay - 1;
		String dayForm = String.valueOf(dayF);
		String yearF = ftYear.format(trader.getExpireDate());
		//int parseYear = Integer.valueOf(yearF);
		//parseYear = parseYear + 543;
		String yearForm = String.valueOf(yearF);

//		dto.setDay(ftDay.format(trader.getEffectiveDate()));
//		dto.setMonth(ftMonth.format(trader.getEffectiveDate()));
//		dto.setYear(ftYear.format(trader.getEffectiveDate()));
		
		dto.setDay(ftDay.format(reg.getApproveDate()));
		dto.setMonth(ftMonth.format(reg.getApproveDate()));
		dto.setYear(ftYear.format(reg.getApproveDate()));
		
//		dto.setDay(ftDay.format(date));
//		dto.setMonth(ftMonth.format(date));
//		dto.setYear(ftYear.format(date));
		
//		if ((TraderType.TOUR_COMPANIES.getStatus().equals(dto.getTraderType())) && (!RegistrationType.TOUR_COMPANIES_NEW_REGISTRATION.equals(reg.getRegistrationType()))) {
//			dto.setDay(ftDay.format(reg.getApproveDate()));
//			dto.setMonth(ftMonth.format(reg.getApproveDate()));
//			dto.setYear(ftYear.format(reg.getApproveDate()));
//		}
		

		dto.setToday(dto.getDay() + " " + dto.getMonth() + " " + "พ.ศ. "
				+ dto.getYear());

		if (TraderType.GUIDE.getStatus().equals(dto.getTraderType())) {
			dto.setDayF(dayForm);
			dto.setMonthF(ftMonth.format(trader.getExpireDate()));
			dto.setYearF(yearForm);
			dto.setTodayForm(dto.getDayF() + " " + dto.getMonthF() + " "
					+ "พ.ศ. " + dto.getYearF());
		}

		dto.setTraderTypeName(TraderType.getTraderType(dto.getTraderType())
				.getMeaning());

		List<TraderAddress> listTraderAddress = new ArrayList<TraderAddress>();
		if(TraderType.TOUR_COMPANIES.equals(dto.getTraderType()))
		{
			long traderParentId = 0;
			if((null != trader.getBranchType()) && "1".equals(trader.getBranchType()))
			{
				traderParentId = trader.getTraderByBranchParentId().getTraderId();
			}
			else
			{
				traderParentId = trader.getTraderId();
			}
			listTraderAddress = (List<TraderAddress>) this.traderAddressDAO
				.findAllByTraderPl(traderParentId,
						TraderAddressType.SHOP_ADDRESS.getStatus(),
						RecordStatus.NORMAL.getStatus());
		}else
		{
			listTraderAddress = (List<TraderAddress>) this.traderAddressDAO
					.findAllByTraderPl(trader.getTraderId(),
							TraderAddressType.OFFICE_ADDRESS.getStatus(),
							RecordStatus.NORMAL.getStatus());
		}
		if (!listTraderAddress.isEmpty()) {
			
			TraderAddress add = listTraderAddress.get(0);
			StringBuilder traderAddress = new StringBuilder();
			
			dto.setAddressId(add.getAddressId());
			traderAddress.append("ตั้งอยู่เลขที่ ");
			traderAddress.append(add.getAddressNo());
			
			if (add.getAddressNo() != null && !add.getAddressNo().isEmpty()) {				
				String addressNo = add.getAddressNo(); 
				addressNo = add.getBuildingName()==null || add.getBuildingName().equals("")?addressNo:addressNo+" อาคาร"+add.getBuildingName();
				addressNo = add.getFloor()==null || add.getFloor().equals("")?addressNo:addressNo+" ชั้น"+" "+add.getFloor();
				addressNo = add.getRoomNo()==null || add.getRoomNo().equals("")?addressNo:addressNo+" ห้อง"+" "+add.getRoomNo();
				addressNo = add.getVillageName()==null || add.getVillageName().equals("")?addressNo:addressNo+" หมู่บ้าน"+" "+add.getVillageName();
				addressNo = add.getMoo()==null || add.getMoo().equals("")?addressNo:addressNo+" หมู่ที่"+" "+add.getMoo();
				dto.setAddressNo(addressNo);
//				dto.setBuildingName(add.getBuildingName()==null || add.getBuildingName().equals("")?"-":add.getBuildingName());
//				dto.setFloor(add.getFloor()==null || add.getFloor().equals("")?"-":add.getFloor());
//				dto.setRoomNo(add.getRoomNo()==null || add.getRoomNo().equals("")?"-":add.getRoomNo());
//				dto.setVillageName(add.getVillageName()==null || add.getVillageName().equals("")?"-":add.getVillageName());
//				dto.setMoo(add.getMoo()==null || add.getMoo().equals("")?"-":add.getMoo());
			} else {
				dto.setAddressNo("-");
//				dto.setBuildingName("-");
//				dto.setFloor("-");
//				dto.setRoomNo("-");
//				dto.setVillageName("-");
//				dto.setMoo("-");
			}

			traderAddress.append(" ถนน ");
			traderAddress.append(add.getRoadName());
			if (add.getRoadName() != null && !add.getRoadName().isEmpty()) {
				dto.setRoadName(add.getRoadName());
			} else {
				dto.setRoadName("-");
			}
			traderAddress.append(" ตรอก/ซอย ");
			traderAddress.append(add.getSoi());
			if (add.getSoi() != null && !add.getSoi().isEmpty()) {
				dto.setSoi(add.getSoi());
			} else {
				dto.setSoi("-");
			}

			if (add.getMasTambol() != null) {
				traderAddress.append(" ตำบล/แขวง ");
				traderAddress.append(add.getMasTambol().getTambolName());
				dto.setTambolId(add.getMasTambol().getTambolId());
				dto.setTambolName(add.getMasTambol().getTambolName());
			}
			if (add.getMasAmphur() != null) {
				traderAddress.append(" อำเภอ/เขต ");
				traderAddress.append(add.getMasAmphur().getAmphurName());
				dto.setAmphurId(add.getMasAmphur().getAmphurId());
				dto.setAmphurName(add.getMasAmphur().getAmphurName());
			}
			if (add.getMasProvince() != null) {
				traderAddress.append(" จังหวัด ");
				traderAddress.append(add.getMasProvince().getProvinceName());
				dto.setProvinceId(add.getMasProvince().getProvinceId());
				dto.setProvinceName(add.getMasProvince().getProvinceName());
			}

			traderAddress.append(" รหัสไปรษณีย์ ");
			traderAddress.append(add.getPostCode() == null ? "" : add
					.getPostCode());
			dto.setPostCode(add.getPostCode() == null ? "-" : add.getPostCode());

			dto.setTraderAddress(traderAddress.toString());
		}
		else
		{
			dto.setAddressNo("-");
			dto.setPostCode("-");
			dto.setSoi("-");
			dto.setProvinceName("-");
			dto.setAmphurName("-");
			dto.setTambolName("-");
			dto.setRoadName("-");
		}

		// is branch
		BranchDTO branchDTO = new BranchDTO();
		if ((trader.getBranchType() != null) && (trader.getBranchType().equals("1"))) {
			// address = parant
			Trader traderParent = trader.getTraderByBranchParentId();
			
			
			ObjectUtil.copy(dto, branchDTO);
			dto.setBranchType("1");
			
			branchDTO.setBranchNo(trader.getTraderBranchNo().toString());
			
			
			
			
			List<TraderAddress> listParentTraderAddress = (List<TraderAddress>) this.traderAddressDAO
					.findAllByTraderPl(trader.getTraderId(),
							TraderAddressType.BRANCH.getStatus(),
							RecordStatus.NORMAL.getStatus());

			if (!listParentTraderAddress.isEmpty()) {
				
				TraderAddress add = listParentTraderAddress.get(0);
				StringBuilder traderAddress = new StringBuilder();
			
				traderAddress.append("ตั้งอยู่เลขที่ ");
				traderAddress.append(add.getAddressNo());
				
				//OAT EDIT
				
				if (add.getAddressNo() != null && !add.getAddressNo().isEmpty()) {
					String addressNo = add.getAddressNo(); 
					addressNo = add.getBuildingName()==null || add.getBuildingName().equals("")?addressNo:addressNo+" อาคาร"+add.getBuildingName();
					addressNo = add.getFloor()==null || add.getFloor().equals("")?addressNo:addressNo+" ชั้น"+" "+add.getFloor();
					addressNo = add.getRoomNo()==null || add.getRoomNo().equals("")?addressNo:addressNo+" ห้อง"+" "+add.getRoomNo();
					addressNo = add.getVillageName()==null || add.getVillageName().equals("")?addressNo:addressNo+" หมู่บ้าน"+" "+add.getVillageName();
					addressNo = add.getMoo()==null || add.getMoo().equals("")?addressNo:addressNo+" หมู่ที่"+" "+add.getMoo();
					branchDTO.setAddressNo(addressNo);
					System.out.println("##########Branch addressNo = "+addressNo);
				}
				else
				{
					branchDTO.setAddressNo("-");
				}
				
//				if (add.getAddressNo() != null && !add.getAddressNo().isEmpty()) {
//					if (add.getVillageName() != null) {
//						branchDTO.setAddressNo(add.getAddressNo() + "	"
//								+ add.getVillageName());
//					} else {
//						branchDTO.setAddressNo(add.getAddressNo());
//					}
//				} else {
//					branchDTO.setAddressNo("-");
//				}
				//END OAT EDIT

				traderAddress.append(" ถนน ");
				traderAddress.append(add.getRoadName());
				if (add.getRoadName() != null && !add.getRoadName().isEmpty()) {
					branchDTO.setRoadName(add.getRoadName());
				} else {
					branchDTO.setRoadName("-");
				}
				traderAddress.append(" ตรอก/ซอย ");
				traderAddress.append(add.getSoi());
				if (add.getSoi() != null && !add.getSoi().isEmpty()) {
					branchDTO.setSoi(add.getSoi());
				} else {
					branchDTO.setSoi("-");
				}

				if (add.getMasTambol() != null) {
					traderAddress.append(" ตำบล/แขวง ");
					traderAddress.append(add.getMasTambol().getTambolName());
					branchDTO.setTambolId(add.getMasTambol().getTambolId());
					branchDTO.setTambolName(add.getMasTambol().getTambolName());
				}
				if (add.getMasAmphur() != null) {
					traderAddress.append(" อำเภอ/เขต ");
					traderAddress.append(add.getMasAmphur().getAmphurName());
					branchDTO.setAmphurId(add.getMasAmphur().getAmphurId());
					branchDTO.setAmphurName(add.getMasAmphur().getAmphurName());
				}
				if (add.getMasProvince() != null) {
					traderAddress.append(" จังหวัด ");
					traderAddress.append(add.getMasProvince().getProvinceName());
					branchDTO.setProvinceId(add.getMasProvince().getProvinceId());
					branchDTO.setProvinceName(add.getMasProvince().getProvinceName());
				}

				traderAddress.append(" รหัสไปรษณีย์ ");
				traderAddress.append(add.getPostCode() == null ? "" : add
						.getPostCode());
				branchDTO.setPostCode(add.getPostCode() == null ? "-" : add.getPostCode());

				branchDTO.setTraderAddress(traderAddress.toString());
			}
			else
			{
				branchDTO.setAddressNo("-");
				branchDTO.setPostCode("-");
				branchDTO.setSoi("-");
				branchDTO.setProvinceName("-");
				branchDTO.setAmphurName("-");
				branchDTO.setTambolName("-");
				branchDTO.setRoadName("-");
			}
			
		}
		
		if(pril.getRegistration().getOrganization().getOrgId() == 2) //2
		{
			//Oat Edit
//			dto.setManagerBG("(นายนภดล อาวุธกรรมปรีชา)");
			dto.setManagerBG("(นายมนตรี ปิยากูล)");
			//End Oat Edit
			
			dto.setPosition("นายทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์");
			dto.setPosition1("สาขาภาคเหนือ จังหวัดเชียงใหม่");
		}else if(pril.getRegistration().getOrganization().getOrgId() == 3) //3
		{
			dto.setManagerBG("(นายสันติ ป่าหวาย)");
			dto.setPosition("นายทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์");
			dto.setPosition1("สาขาภาคใต้เขต 2 จังหวัดภูเก็ต");
		}else if(pril.getRegistration().getOrganization().getOrgId() == 4) //4
		{
			dto.setManagerBG("(นายบัวยัญ สุวรรณมณี)");
			dto.setPosition("นายทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์");
			dto.setPosition1("สาขาภาคใต้เขต 1 จังหวัดสงขลา");
		}else if(pril.getRegistration().getOrganization().getOrgId() == 5) //5
		{
			dto.setManagerBG("(นายอักษร แสนใหม่)");
			dto.setPosition("นายทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์");
			dto.setPosition1("สาขาภาคตะวันออกเฉียงเหนือ จังหวัดนครราชสีมา");
		}
		
		//Oat Edit 23/12/2557
//		else
//		{
//			dto.setManagerBG("(นางธิดา จงก้องเกียรติ)");
//			dto.setPosition("นายทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์กลาง");
//			System.out.println(dto.getPosition());
//		}
		
		else if(pril.getRegistration().getOrganization().getOrgId() == 1) //1
		{
			dto.setManagerBG("(นางธิดา จงก้องเกียรติ)");
			dto.setPosition("นายทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์กลาง");
			System.out.println(dto.getPosition());
		}
		else if(pril.getRegistration().getOrganization().getOrgId() == 6) //6
		{
			dto.setManagerBG("(นางธิดา จงก้องเกียรติ)");
			dto.setPosition("นายทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์กลาง");
			System.out.println(dto.getPosition());
		}
		else{
			AdmUser manager = (AdmUser)this.admUserDAO.findManagerByOrg(pril.getRegistration().getOrganization().getOrgId(), ConstantUtil.DIRECTOR_POSITION);
			String managerName = "";
			if(manager.getMasPrefix()!=null){
				managerName = manager.getMasPrefix().getPrefixName();
			}
			managerName =managerName+ manager.getUserName() +" "+manager.getUserLastname();
			dto.setManagerBG("("+managerName+")");
			String position = "นายทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์";
			if(manager.getMasPosition() != null){
				position = manager.getMasPosition().getPosName();
			}
			dto.setPosition(position);
			String orgName = "";
			orgName = manager.getOrganization().getOrgName().replaceAll("สนง.", "");
			dto.setPosition1(orgName);
		}
		/*
		else if(pril.getRegistration().getOrganization().getOrgId() == 7) //7
		{
			dto.setManagerBG("(นางสาวเพ็ญศิริรัตน์ อาจทวีกุล)");
			dto.setPosition("นายทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์");
			dto.setPosition1("สาขากรุงเทพมหานคร");
			System.out.println(dto.getPosition());
		}
		else if(pril.getRegistration().getOrganization().getOrgId() == 8) //8
		{
			dto.setManagerBG("(นายนเรศ จุลบุตร)");
			dto.setPosition("นายทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์");
			dto.setPosition1("สาขาภาคกลางเขต 1");
			System.out.println(dto.getPosition());
		}
		else if(pril.getRegistration().getOrganization().getOrgId() == 9) //9
		{
			dto.setManagerBG("(นายธารทิพย์ มีลักษณะ)");
			dto.setPosition("นายทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์");
			dto.setPosition1("สาขาภาคกลางเขต 2");
			System.out.println(dto.getPosition());
		}
		else if(pril.getRegistration().getOrganization().getOrgId() == 10) //10
		{
			dto.setManagerBG("(นายเสมา วนะสิทธิ์)");
			dto.setPosition("นายทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์");
			dto.setPosition1("สาขาภาคกลางเขต 3");
			System.out.println(dto.getPosition());
		}
		else if(pril.getRegistration().getOrganization().getOrgId() == 11) //11
		{
			dto.setManagerBG("(นายบุญชัย ทันสมัย)");
			dto.setPosition("นายทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์");
			dto.setPosition1("สาขาภาคตะวันออก");
			System.out.println(dto.getPosition());
		}
		else if(pril.getRegistration().getOrganization().getOrgId() == 12) //12
		{
			dto.setManagerBG("(นายมนตรี ปิยากูล)");
			dto.setPosition("นายทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์");
			dto.setPosition1("สาขาภาคเหนือเขต 1");
			System.out.println(dto.getPosition());
		}
		else if(pril.getRegistration().getOrganization().getOrgId() == 13) //13
		{
			dto.setManagerBG("(นางสาวปราณปริยา พลเยี่ยม)");
			dto.setPosition("นายทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์");
			dto.setPosition1("สาขาภาคเหนือเขต 2");
			System.out.println(dto.getPosition());
		}
		else if(pril.getRegistration().getOrganization().getOrgId() == 14) //14
		{
			dto.setManagerBG("(นายขจรเกียรติ แพงศรี)");
			dto.setPosition("นายทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์");
			dto.setPosition1("สาขาภาคเหนือเขต 3");
			System.out.println(dto.getPosition());
		}
		else if(pril.getRegistration().getOrganization().getOrgId() == 15) //15
		{
			dto.setManagerBG("(นายอักษร แสนใหม่)");
			dto.setPosition("นายทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์");
			dto.setPosition1("สาขาภาคตะวันออกเฉียงเหนือเขต 1");
			System.out.println(dto.getPosition());
		}
		else if(pril.getRegistration().getOrganization().getOrgId() == 16) //16***
		{
			dto.setManagerBG("(นายอักษร แสนใหม่)");
			dto.setPosition("นายทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์");
			dto.setPosition1("สาขาภาคตะวันออกเฉียงเหนือเขต 2");
			System.out.println(dto.getPosition());
		}
		else if(pril.getRegistration().getOrganization().getOrgId() == 17) //17
		{
			dto.setManagerBG("(นายประภาส บุญสุข)");
			dto.setPosition("นายทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์");
			dto.setPosition1("สาขาภาคตะวันออกเฉียงเหนือเขต 3");
			System.out.println(dto.getPosition());
		}
		else if(pril.getRegistration().getOrganization().getOrgId() == 18) //18
		{
			dto.setManagerBG("(นายบัวยัญ สุวรรณมณ)");
			dto.setPosition("นายทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์");
			dto.setPosition1("สาขาภาคใต้เขต 1");
			System.out.println(dto.getPosition());
		}
		else if(pril.getRegistration().getOrganization().getOrgId() == 19) //19
		{
			dto.setManagerBG("(นายสันติ ป่าหวาย)");
			dto.setPosition("นายทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์");
			dto.setPosition1("สาขาภาคใต้เขต 2");
			System.out.println(dto.getPosition());
		}
		else if(pril.getRegistration().getOrganization().getOrgId() == 20) //20
		{
			dto.setManagerBG("(นางศิรวี วาเล๊าะ)");
			dto.setPosition("นายทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์");
			dto.setPosition1("สาขาภาคใต้เขต 3");
			System.out.println(dto.getPosition());
		}
		else if(pril.getRegistration().getOrganization().getOrgId() == 21) //21
		{
			dto.setManagerBG("(นายสุทธิ ศิลมัย)");
			dto.setPosition("นายทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์");
			dto.setPosition1("สาขาภาคใต้เขต 4");
			System.out.println(dto.getPosition());
		}
		*/
		//
		
		dto.setOrgId(pril.getRegistration().getOrganization().getOrgId());
		List list = new ArrayList();
		list.add(dto);
		list.add(branchDTO);
	
		return list;
	}
	

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void updatePrintLicneseById(Object object, User user)
			throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
		RegistrationDTO param = (RegistrationDTO)object;
		
		List<PrintingLicense> listPl = (List<PrintingLicense>) this.printingLicenseDAO.findByPrintLicenseId(param.getPrintLicenseId());
		
		if(!listPl.isEmpty())
		{
			PrintingLicense pl  = listPl.get(0);
			pl.setPrintLicenseStatus("A");
			pl.setPrintLicenseDate(new Date());
			printingLicenseDAO.update(pl);
		}
		
	}
	
	public void getLicenseForPrint(Object object, User user)
			throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
		DateFormat df = DateUtils.getProcessDateFormatThai();
		
		RegistrationDTO param = (RegistrationDTO)object;
		
		PrintingLicense printLicense = (PrintingLicense) this.printingLicenseDAO.findByPrimaryKey(Integer.parseInt(param.getPrintLicenseId()+""));
		
		Trader trader = printLicense.getTrader();
		if(null != trader.getEffectiveDate()){
			param.setEffectiveDate(df.format(trader.getEffectiveDate()));
		}
		if(null != trader.getExpireDate()){
			param.setExpireDate(df.format(trader.getExpireDate()));
		}
		
		
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void savePrintLicneseById(Object object, User user) throws Exception {
		
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
		RegistrationDTO dto = (RegistrationDTO)object;
		List<PrintingLicense> listPl = (List<PrintingLicense>) this.printingLicenseDAO.findByPrintLicenseId(dto.getPrintLicenseId());
		PrintingLicense pl = new PrintingLicense();
		if(!listPl.isEmpty())
		{
			pl  = listPl.get(0);
			dto.setRegId(pl.getRegistration().getRegId());
			dto.setTraderId(pl.getTrader().getTraderId());
		}
		
		try{
			
	
			Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(dto.getRegId());
			reg.setLastUpdUser(user.getUserName());
			reg.setLastUpdDtm(new Date());
			System.out.println(reg.getTrader().getTraderId());
			
		
			Trader trader = (Trader)reg.getTrader();
			ObjectUtil.copy(dto, trader);

			long personId = trader.getPerson().getPersonId();
			System.out.println("traderId: "+trader.getPerson().getPersonId());
			System.out.println(dto.getProvinceGuideServiceId());
			if(dto.getProvinceGuideServiceId() > 0)
			{
				MasProvince provinceGuideServiceId = new MasProvince();
				provinceGuideServiceId.setProvinceId(dto.getProvinceGuideServiceId());
				trader.setMasProvince(provinceGuideServiceId);
			}
			
			trader.setLastUpdUser(user.getUserName());
			trader.setLastUpdDtm(new Date());
							
	    
			Person person = trader.getPerson();

			System.out.println("personId: "+person.getPersonId());
			dto.setPersonId(person.getPersonId() > 0?person.getPersonId():person.getPersonId());
			ObjectUtil.copy(dto, person);
			
			System.out.println("personId New: "+person.getPersonId());
			System.out.println(person.getFirstName());
			
			person.setLastUpdUser(user.getUserName());
			person.setLastUpdDtm(new Date());
						
			if(dto.getPrefixId() > 0)
			{
				MasPrefix masPrefix = new MasPrefix();
				masPrefix.setPrefixId(dto.getPrefixId());
				
				person.setMasPrefix(masPrefix);
			}
			
			if(dto.getPostfixId() > 0)
			{
				MasPosfix masPosfix = new MasPosfix();
				masPosfix.setPostfixId(dto.getPostfixId());
				
				person.setMasPosfix(masPosfix);
			}
			
			TraderAddress traderAddress = (TraderAddress)this.traderAddressDAO.findByPrimaryKey(dto.getAddressId());

			ObjectUtil.copy(dto, traderAddress);
			
			if(dto.getTambolId() > 0)
			{
				MasTambol masTambol = new MasTambol();
				masTambol.setTambolId(dto.getTambolId());
				
				traderAddress.setMasTambol(masTambol);
			}
			
			if(dto.getAmphurId() > 0)
			{
				MasAmphur masAmphurByAmphurId = new MasAmphur();
				masAmphurByAmphurId.setAmphurId(dto.getAmphurId());
				
				traderAddress.setMasAmphur(masAmphurByAmphurId);
			}
		
			if(dto.getProvinceId() > 0)
			{
				MasProvince masProvinceByProvinceId = new MasProvince();
				masProvinceByProvinceId.setProvinceId(dto.getProvinceId());
				
				traderAddress.setMasProvince(masProvinceByProvinceId);
			}
			
			
			personDAO.update(person);
			traderDAO.update(trader);
			registrationDAO.update(reg);

		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
	}
	
	//Oat Add 18/09/57
	public List getPrintLicenseExcel(Map model, Object object, User user, int start, int limit)
			throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถค้นหาข้อมูลพิมพ์ใบอนุญาตได้");
		}
		
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		RegistrationDTO params = (RegistrationDTO)object;
		
		list = this.getPrintLicenseTraderExcel(model, object, user, start, limit);
		
		return list;

	}
	
	public List getPrintLicenseTraderExcel(Map model, Object object, User user ,int start, int limit) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถค้นหาข้อมูลพิมพ์ใบอนุญาตได้");
		}
		
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		RegistrationDTO params = (RegistrationDTO)object;
		
		DateFormat ft =  DateUtils.getProcessDateFormatThai();
		
		//Oat Add 19/09/57
		int countBusiness = 0;
		int countBusinessBranch = 0;
		//
		
		int count = 1;
		String checkTN = "";
		String checkTNN = "";
		String checkTGN = "";
		String checkTGNN = "";
		
		long checkRegId = 0;
		long checkRegIdN = 0;
		
		List<Object[]> listObj = this.printingLicenseDAO.findPrintLicensePaging(params, user, start, limit);
		
		if(!listObj.isEmpty())
		{
			for(Object[] sel: listObj)
			{
				RegistrationDTO dto = new RegistrationDTO();
				
				dto.setRegId(Long.valueOf(sel[0].toString()));
				dto.setRegistrationNo(sel[1]==null?"":sel[1].toString());
				if(sel[2] != null)
				{
					
					dto.setRegistrationDate(ft.format((Date)sel[2]));
				}
				dto.setRegistrationProgress(sel[3]==null?"":sel[3].toString());
				if(sel[8] != null)
				{
					dto.setTraderType(sel[8]==null?"":sel[8].toString());
					dto.setTraderTypeName(TraderType.getMeaning(dto.getTraderType()));
				}
				if(sel[4]!=null)
				{
					dto.setRegistrationType(sel[4]==null?"":sel[4].toString());
					dto.setRegistrationTypeName(RegistrationType.getMeaning(dto.getTraderType(), dto.getRegistrationType()));
				}
				dto.setOrgId(Long.valueOf(sel[5].toString()));
				dto.setRegRecordStatus(sel[6]==null?"":sel[6].toString());
				dto.setTraderId(Long.valueOf(sel[7].toString()));
				dto.setTraderName(sel[9]==null?"":sel[9].toString());
				dto.setTraderNameEn(sel[10]==null?"":sel[10].toString());
				dto.setTraderCategory(sel[11]==null?"":sel[11].toString());
				
			 if(TraderType.TOUR_COMPANIES.equals(dto.getTraderType()))
			 {
				dto.setTraderCategoryName(
						TraderCategory.getTraderCategory(
							dto.getTraderType()
							, dto.getTraderCategory()
						).getMeaning()
				);
			 }else
			 {
				dto.setTraderCategoryName(GuideCategory.getGuideCategory(dto.getTraderCategory()).getMeaning());
			 }

				
				dto.setPersonId(Long.valueOf(sel[12].toString()));
				dto.setPersonType(sel[13]==null?"":sel[13].toString());
				dto.setPrefixName(sel[14]==null?"":sel[14].toString());
				dto.setFirstName(sel[15]==null?"":sel[15].toString());
				dto.setLastName(sel[16]==null?"":sel[16].toString());
				dto.setPostfixName(sel[17]==null?"":sel[17].toString());

				
				dto.setPrefixId(Long.valueOf(sel[18].toString()));
				
				if(sel[19] != null)
				{
					dto.setPostfixId(Long.valueOf(sel[19].toString()));
				}
				
				
				if(sel[20] != null)
				{
					//dto.setAmphurId(Long.valueOf(sel[20].toString()));
				}
				if(sel[21] != null)
				{
					dto.setTaxAmphurId(Long.valueOf(sel[21].toString()));
				}
				if(sel[22] != null)
				{
					//dto.setProvinceId(Long.valueOf(sel[22].toString()));
				}
				if(sel[23] != null)
				{
					dto.setTaxProvinceId(Long.valueOf(sel[23].toString()));
				}
				
				if(sel[24] != null)
				{
					dto.setTraderGuideId(Long.valueOf(sel[24].toString()));
				}
				
				dto.setPronunciationName(sel[25]==null?"":sel[25].toString());
			    dto.setLicenseGuideNo(sel[26]==null?"":sel[26].toString());
			    dto.setPersonType(sel[27]==null?"":sel[27].toString());
			    dto.setIdentityNo(sel[28]==null?"":sel[28].toString());
			    dto.setCommitteeName1(sel[29]==null?"":sel[29].toString());
			    dto.setCommitteeName2(sel[30]==null?"":sel[30].toString());
			    dto.setCommitteeNameSign(sel[31]==null?"":sel[31].toString());
			    dto.setPassportNo(sel[32]==null?"":sel[32].toString());
			    dto.setFirstNameEn(sel[33]==null?"":sel[33].toString());
			    dto.setLastNameEn(sel[34]==null?"":sel[34].toString());
			    dto.setGender(sel[35]==null?"":sel[35].toString());
			    dto.setPersonNationality(sel[36]==null?"":sel[36].toString());


			    if(sel[37] != null)
				{
					
					dto.setBirthDate(ft.format((Date)sel[37]));
				}
			    
			    dto.setAgeYear(sel[38]==null?0:Integer.valueOf(sel[38].toString()));
			    if(sel[39] != null)
				{
					
					dto.setIdentityNoExpire(ft.format((Date)sel[39]));
				}
			   
			    dto.setCorporateType(sel[40]==null?"":sel[40].toString());
			    dto.setTaxIdentityNo(sel[41]==null?"":sel[41].toString());
			    if(sel[42] != null)
				{
					
					dto.setIdentityDate(ft.format((Date)sel[42]));
				}
			    dto.setTraderRecordStatus(sel[43]==null?"":sel[43].toString());
			    
			    dto.setAmphurName(sel[44]==null?"":sel[44].toString());
			    dto.setTaxAmphurName(sel[45]==null?"":sel[45].toString());
			    dto.setProvinceName(sel[46]==null?"":sel[46].toString());
			    dto.setTaxProvinceName(sel[47]==null?"":sel[47].toString());
			    dto.setPrefixNameEn(sel[48]==null?"":sel[48].toString());
			    dto.setLicenseNo(sel[49]==null?"":sel[49].toString());
			    
			    //Oat Edit 16/09/57
			    //ถ้าเพิ่ม list ของการเงินมา ต้อง เช็ค size sel 
			    if(sel.length > 50)
			    {
				    //Oat Edit 12/09/57
	//			    dto.setPrintLicenseId(Long.valueOf(sel[50].toString()));
				    if(sel[50] != null)
				    {
				    	dto.setPrintLicenseId(Long.valueOf(sel[50].toString()));
				    }
				    //End Oat Edit 12/09/57
				    if(sel[51] != null)
					{
				    dto.setPrintLicenseDate(ft.format((Date)sel[51]));
					}
				    
				    if(sel[52] != null)
				    {
				    	dto.setPrintLicenseStatus(sel[52]==null?"":sel[52].toString());
				    }
				    
				    if(sel[53] != null)
				    {
				    	dto.setTraderBranchNo(Integer.valueOf(sel[53].toString()));
				    }
				   
				    if(sel[54] != null)
				    {
				    	dto.setApproveDate(ft.format((Date)sel[54]));
				    }
				    if(sel[55] != null)
				    {
				    	dto.setBranchType(sel[55]==null?"":sel[55].toString());
				    }
				    
			    }
			    //End Oat Edit 16/09/57
				
//				Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(dto.getRegId());
//				if(TraderType.TOUR_COMPANIES.getStatus().equals(dto.getTraderType()))
//				{
//					tourCompaniesService.getRegistration(reg, dto, user);
//				}
//				
//				if(TraderType.GUIDE.getStatus().equals(dto.getTraderType()))
//				{
//					guideService.getRegistration(reg, dto, user);
//				}
//				
//				if(TraderType.LEADER.getStatus().equals(dto.getTraderType()))
//				{
//					tourLeaderService.getRegistration(reg, dto, user);
//				}
//				
//				List<RegisterProgress> listprogress =  registerProgressDAO.findRegisterProgressbyRegistration(reg.getRegId(), ProgressStatus.GET_IT.getStatus());
//				
//				if(!listprogress.isEmpty())
//				{
//					RegisterProgress progress = listprogress.get(0);
//					
//					String authorityName = progress.getAdmUser().getMasPrefix().getPrefixName() + progress.getAdmUser().getUserName() +" "
//							+progress.getAdmUser().getUserLastname();
//					
//					dto.setAuthorityName(authorityName);
//				}
				
				//สามารถแก้ไขข้อมูลได้ ถ้า RegistrationProgress อยู่ที่ role ตัวเอง
				/**
				 * RECHECK
				 * SUPERVISOR
				 * MANAGER
				 * DIRECTOR
				 */
				
				//Oat Edit 16/09/57 เพิ่ม dto.getPrintLicenseStatus() != null
				if(dto.getPrintLicenseStatus() != null && dto.getPrintLicenseStatus().equals("W"))
				{
					dto.setRoleAction("1");
				}else if(dto.getPrintLicenseStatus() != null)
				{
					dto.setRoleAction("P");
				}
				
				if(dto.getTraderBranchNo() == null)
				{
					dto.setDetailAction("1");
				}
				
				checkTN = dto.getTraderName();
				checkTGN = dto.getTraderCategoryName();
				checkRegId = dto.getRegId();
				if(PersonType.CORPORATE.getStatus().equals(dto.getPersonType()) && TraderType.TOUR_COMPANIES.equals(dto.getTraderType()))
				{
//					if((checkTNN.equals(checkTN)) && (checkTGNN.equals(checkTGN)) && (checkRegId == checkRegIdN) && dto.getTraderBranchNo() > 0)
					if(dto.getBranchType() != null)
					{
						//Oat Edit 18/09/57
						dto.setTraderOwnerName("สาขา "+dto.getTraderBranchNo());
//						dto.setTraderOwnerName("สาขา "+count);
						//End Oat Edit 18/09/57
						count ++;
						countBusinessBranch++;
					}else
					{
						String traderOwnerName = dto.getPrefixName()+" "+dto.getFirstName() +" "+dto.getPostfixName();
						dto.setTraderOwnerName(traderOwnerName);
						count = 1;
						countBusiness++;
					}
				//Oat Add 18/09/57
				}else if(PersonType.INDIVIDUAL.getStatus().equals(dto.getPersonType()) && TraderType.TOUR_COMPANIES.equals(dto.getTraderType()))
				{
					if(dto.getBranchType() != null)
					{
						dto.setTraderOwnerName("สาขา "+dto.getTraderBranchNo());
						count ++;
						countBusinessBranch++;
					}else
					{
						String traderOwnerName = dto.getPrefixName()+dto.getFirstName() +" "+dto.getLastName();
//						String traderOwnerName = dto.getPrefixName()+" "+dto.getFirstName() +" "+dto.getPostfixName();
						dto.setTraderOwnerName(traderOwnerName);
						count = 1;
						countBusiness++;
					}
				}
				//End Oat Add 18/09/57
				else
				{
				  	
				/*	if((checkTNN.equals(checkTN)) && (checkTGNN.equals(checkTGN)) && (checkRegId == checkRegIdN) && dto.getTraderBranchNo() > 0)
					{
						dto.setTraderOwnerName("สาขา "+count);
						count ++;
					}else
					{*/
						String traderOwnerName = dto.getPrefixName()+dto.getFirstName() +"  " + dto.getLastName();
						//String guideName  = traderOwnerName + " (" + dto.getLicenseNo() +")";
						dto.setTraderOwnerName(traderOwnerName);
						//dto.setGuideName(guideName);
						count = 1;
						countBusiness++;
					//}
					
				}
				
				System.out.println("dto.getLicenseNo() = "+dto.getLicenseNo());
				
//				List<Trader> listTrader = this.traderDAO.findTraderByLicenseNo(dto.getLicenseNo(), dto.getTraderType(), "N", user.getUserData().getOrgId()) ;
//				
//				//Oat Edit 16/09/57 เพิ่ม && listTrader.size() == 1 ถ้าจดใหม่จะไม่มี licenseNo ทำให้ listTrader มากกว่า 1
//				if(!listTrader.isEmpty() && listTrader.size() == 1)
//				{
//					Trader trader = listTrader.get(0);
//					
//					if(TraderType.TOUR_COMPANIES.getStatus().equals(dto.getTraderType()))
//					{
//						
////						dto.setHeaderInfo(trader.getTraderName()+" เลขที่ใบอนุญาต "+trader.getLicenseNo());
//						dto.setHeaderInfo("เลขที่ใบอนุญาต "+trader.getLicenseNo());
//					}else
//					{
//						
////						dto.setHeaderInfo(dto.getTraderOwnerName()+" เลขที่ใบอนุญาต "+trader.getLicenseNo());
//						//dto.setHeaderInfo("เลขที่ใบอนุญาต "+trader.getLicenseNo());
//					}
//				}
				
				
				
			    checkTNN = checkTN;
				checkTGNN = checkTGN;
				checkRegIdN = checkRegId;
				
				dto.setToday(ft.format((Date)new Date()));
				
				list.add(dto);
				
				
			}

		}
		
		//Oat Add 19/09/57
		model.put("countBusiness", "จำนวน "+countBusiness+" ราย");
		model.put("countBusinessBranch", "จำนวน "+countBusinessBranch+" สาขา");
		//
		
		return list;
	}
	
	//Oat Add 18/09/57 ยังไม่ได้ใช้ print Excel
//	public List getPrintLicenseTraderExcel2(Map model, Object object, User user ,int start, int limit) throws Exception {
//		if(!(object instanceof RegistrationDTO))
//		{
//			throw new IllegalArgumentException("ไม่สามารถค้นหาข้อมูลพิมพ์ใบอนุญาตได้");
//		}
//		
//		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
//		
//		RegistrationDTO params = (RegistrationDTO)object;
//		
//		DateFormat ft =  DateUtils.getProcessDateFormatThai();
//		
//		//Oat Add 19/09/57
//		int countBusiness = 0;
//		int countBusinessBranch = 0;
//		//
//		
//		int count = 1;
//		String checkTN = "";
//		String checkTNN = "";
//		String checkTGN = "";
//		String checkTGNN = "";
//		
//		long checkRegId = 0;
//		long checkRegIdN = 0;
//		
//		List<Object[]> listObj = null;
//		List<Object[]> listObj2 = null;
//		
//		//เช็คว่าถ้าเลือกยังไม่ได้พิมพ์ใบอนุญาต ถึง add list เพิ่ม
//		if(params.getPrintLicenseStatus().equals("W"))
//		{
//			//list1
//			listObj = this.printingLicenseDAO.findPrintLicensePaging(params, user, start, limit);
//			System.out.println("###listObj.size() = "+listObj.size());
//			
//			//list2
//			params.setTraderRecordStatus(RecordStatus.TEMP.getStatus());
//		    params.setRegRecordStatus(RecordStatus.TEMP.getStatus());
//			listObj2 = this.printingLicenseDAO.findBetweenRegistrationPaging2(params, user , start, limit);
//			System.out.println("###listObj2.size() = "+listObj2.size());
//			//list All
//			listObj.addAll(listObj2);
//			System.out.println("###listObj.size() after AddAll = "+listObj.size());
//		}
//		else if(params.getPrintLicenseStatus().equals("A"))
//		{
//			listObj = this.printingLicenseDAO.findPrintLicensePaging(params, user, start, limit);
//			System.out.println("###listObj.size() = "+listObj.size());
//		}
//		//End Oat Add 16/09/57
//		
//		if(!listObj.isEmpty())
//		{
//			for(Object[] sel: listObj)
//			{
//				RegistrationDTO dto = new RegistrationDTO();
//				
//				dto.setRegId(Long.valueOf(sel[0].toString()));
//				dto.setRegistrationNo(sel[1]==null?"":sel[1].toString());
//				if(sel[2] != null)
//				{
//					
//					dto.setRegistrationDate(ft.format((Date)sel[2]));
//				}
//				dto.setRegistrationProgress(sel[3]==null?"":sel[3].toString());
//				if(sel[8] != null)
//				{
//					dto.setTraderType(sel[8]==null?"":sel[8].toString());
//					dto.setTraderTypeName(TraderType.getMeaning(dto.getTraderType()));
//				}
//				if(sel[4]!=null)
//				{
//					dto.setRegistrationType(sel[4]==null?"":sel[4].toString());
//					dto.setRegistrationTypeName(RegistrationType.getMeaning(dto.getTraderType(), dto.getRegistrationType()));
//				}
//				dto.setOrgId(Long.valueOf(sel[5].toString()));
//				dto.setRegRecordStatus(sel[6]==null?"":sel[6].toString());
//				dto.setTraderId(Long.valueOf(sel[7].toString()));
//				dto.setTraderName(sel[9]==null?"":sel[9].toString());
//				dto.setTraderNameEn(sel[10]==null?"":sel[10].toString());
//				dto.setTraderCategory(sel[11]==null?"":sel[11].toString());
//				
//			 if(TraderType.TOUR_COMPANIES.equals(dto.getTraderType()))
//			 {
//				dto.setTraderCategoryName(
//						TraderCategory.getTraderCategory(
//							dto.getTraderType()
//							, dto.getTraderCategory()
//						).getMeaning()
//				);
//			 }else
//			 {
//				dto.setTraderCategoryName(GuideCategory.getGuideCategory(dto.getTraderCategory()).getMeaning());
//			 }
//
//				
//				dto.setPersonId(Long.valueOf(sel[12].toString()));
//				dto.setPersonType(sel[13]==null?"":sel[13].toString());
//				dto.setPrefixName(sel[14]==null?"":sel[14].toString());
//				dto.setFirstName(sel[15]==null?"":sel[15].toString());
//				dto.setLastName(sel[16]==null?"":sel[16].toString());
//				dto.setPostfixName(sel[17]==null?"":sel[17].toString());
//
//				
//				dto.setPrefixId(Long.valueOf(sel[18].toString()));
//				
//				if(sel[19] != null)
//				{
//					dto.setPostfixId(Long.valueOf(sel[19].toString()));
//				}
//				
//				
//				if(sel[20] != null)
//				{
//					//dto.setAmphurId(Long.valueOf(sel[20].toString()));
//				}
//				if(sel[21] != null)
//				{
//					dto.setTaxAmphurId(Long.valueOf(sel[21].toString()));
//				}
//				if(sel[22] != null)
//				{
//					//dto.setProvinceId(Long.valueOf(sel[22].toString()));
//				}
//				if(sel[23] != null)
//				{
//					dto.setTaxProvinceId(Long.valueOf(sel[23].toString()));
//				}
//				
//				if(sel[24] != null)
//				{
//					dto.setTraderGuideId(Long.valueOf(sel[24].toString()));
//				}
//				
//				dto.setPronunciationName(sel[25]==null?"":sel[25].toString());
//			    dto.setLicenseGuideNo(sel[26]==null?"":sel[26].toString());
//			    dto.setPersonType(sel[27]==null?"":sel[27].toString());
//			    dto.setIdentityNo(sel[28]==null?"":sel[28].toString());
//			    dto.setCommitteeName1(sel[29]==null?"":sel[29].toString());
//			    dto.setCommitteeName2(sel[30]==null?"":sel[30].toString());
//			    dto.setCommitteeNameSign(sel[31]==null?"":sel[31].toString());
//			    dto.setPassportNo(sel[32]==null?"":sel[32].toString());
//			    dto.setFirstNameEn(sel[33]==null?"":sel[33].toString());
//			    dto.setLastNameEn(sel[34]==null?"":sel[34].toString());
//			    dto.setGender(sel[35]==null?"":sel[35].toString());
//			    dto.setPersonNationality(sel[36]==null?"":sel[36].toString());
//
//
//			    if(sel[37] != null)
//				{
//					
//					dto.setBirthDate(ft.format((Date)sel[37]));
//				}
//			    
//			    dto.setAgeYear(sel[38]==null?0:Integer.valueOf(sel[38].toString()));
//			    if(sel[39] != null)
//				{
//					
//					dto.setIdentityNoExpire(ft.format((Date)sel[39]));
//				}
//			   
//			    dto.setCorporateType(sel[40]==null?"":sel[40].toString());
//			    dto.setTaxIdentityNo(sel[41]==null?"":sel[41].toString());
//			    if(sel[42] != null)
//				{
//					
//					dto.setIdentityDate(ft.format((Date)sel[42]));
//				}
//			    dto.setTraderRecordStatus(sel[43]==null?"":sel[43].toString());
//			    
//			    dto.setAmphurName(sel[44]==null?"":sel[44].toString());
//			    dto.setTaxAmphurName(sel[45]==null?"":sel[45].toString());
//			    dto.setProvinceName(sel[46]==null?"":sel[46].toString());
//			    dto.setTaxProvinceName(sel[47]==null?"":sel[47].toString());
//			    dto.setPrefixNameEn(sel[48]==null?"":sel[48].toString());
//			    dto.setLicenseNo(sel[49]==null?"":sel[49].toString());
//			    
//			    //Oat Edit 16/09/57
//			    //ถ้าเพิ่ม list ของการเงินมา ต้อง เช็ค size sel 
//			    if(sel.length > 50)
//			    {
//				    //Oat Edit 12/09/57
//	//			    dto.setPrintLicenseId(Long.valueOf(sel[50].toString()));
//				    if(sel[50] != null)
//				    {
//				    	dto.setPrintLicenseId(Long.valueOf(sel[50].toString()));
//				    }
//				    //End Oat Edit 12/09/57
//				    if(sel[51] != null)
//					{
//				    dto.setPrintLicenseDate(ft.format((Date)sel[51]));
//					}
//				    
//				    if(sel[52] != null)
//				    {
//				    	dto.setPrintLicenseStatus(sel[52]==null?"":sel[52].toString());
//				    }
//				    
//				    if(sel[53] != null)
//				    {
//				    	dto.setTraderBranchNo(Integer.valueOf(sel[53].toString()));
//				    }
//				   
//				    if(sel[54] != null)
//				    {
//				    	dto.setApproveDate(ft.format((Date)sel[54]));
//				    }
//				    if(sel[55] != null)
//				    {
//				    	dto.setBranchType(sel[55]==null?"":sel[55].toString());
//				    }
//				    
//			    }
//			    //End Oat Edit 16/09/57
//				
//				Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(dto.getRegId());
//				if(TraderType.TOUR_COMPANIES.getStatus().equals(dto.getTraderType()))
//				{
//					tourCompaniesService.getRegistration(reg, dto, user);
//				}
//				
//				if(TraderType.GUIDE.getStatus().equals(dto.getTraderType()))
//				{
//					guideService.getRegistration(reg, dto, user);
//				}
//				
//				if(TraderType.LEADER.getStatus().equals(dto.getTraderType()))
//				{
//					tourLeaderService.getRegistration(reg, dto, user);
//				}
//				
//				List<RegisterProgress> listprogress =  registerProgressDAO.findRegisterProgressbyRegistration(reg.getRegId(), ProgressStatus.GET_IT.getStatus());
//				
//				if(!listprogress.isEmpty())
//				{
//					RegisterProgress progress = listprogress.get(0);
//					
//					String authorityName = progress.getAdmUser().getMasPrefix().getPrefixName() + progress.getAdmUser().getUserName() +" "
//							+progress.getAdmUser().getUserLastname();
//					
//					dto.setAuthorityName(authorityName);
//				}
//				
//				//สามารถแก้ไขข้อมูลได้ ถ้า RegistrationProgress อยู่ที่ role ตัวเอง
//				/**
//				 * RECHECK
//				 * SUPERVISOR
//				 * MANAGER
//				 * DIRECTOR
//				 */
//				
//				//Oat Edit 16/09/57 เพิ่ม dto.getPrintLicenseStatus() != null
//				if(dto.getPrintLicenseStatus() != null && dto.getPrintLicenseStatus().equals("W"))
//				{
//					dto.setRoleAction("1");
//				}else if(dto.getPrintLicenseStatus() != null)
//				{
//					dto.setRoleAction("P");
//				}
//				
//				if(dto.getTraderBranchNo() == null)
//				{
//					dto.setDetailAction("1");
//				}
//				
//				checkTN = dto.getTraderName();
//				checkTGN = dto.getTraderCategoryName();
//				checkRegId = dto.getRegId();
//				if(PersonType.CORPORATE.getStatus().equals(dto.getPersonType()) && TraderType.TOUR_COMPANIES.equals(dto.getTraderType()))
//				{
////					if((checkTNN.equals(checkTN)) && (checkTGNN.equals(checkTGN)) && (checkRegId == checkRegIdN) && dto.getTraderBranchNo() > 0)
//					if(dto.getBranchType() != null)
//					{
//						//Oat Edit 18/09/57
//						dto.setTraderOwnerName("สาขา "+dto.getTraderBranchNo());
////						dto.setTraderOwnerName("สาขา "+count);
//						//End Oat Edit 18/09/57
//						count ++;
//						countBusinessBranch++;
//					}else
//					{
//						String traderOwnerName = dto.getPrefixName()+" "+dto.getFirstName() +" "+dto.getPostfixName();
//						dto.setTraderOwnerName(traderOwnerName);
//						count = 1;
//						countBusiness++;
//					}
//				//Oat Add 18/09/57
//				}else if(PersonType.INDIVIDUAL.getStatus().equals(dto.getPersonType()) && TraderType.TOUR_COMPANIES.equals(dto.getTraderType()))
//				{
//					if(dto.getBranchType() != null)
//					{
//						dto.setTraderOwnerName("สาขา "+dto.getTraderBranchNo());
//						count ++;
//						countBusinessBranch++;
//					}else
//					{
//						String traderOwnerName = dto.getPrefixName()+" "+dto.getFirstName() +" "+dto.getPostfixName();
//						dto.setTraderOwnerName(traderOwnerName);
//						count = 1;
//						countBusiness++;
//					}
//				}
//				//End Oat Add 18/09/57
//				else
//				{
//				  	
//				/*	if((checkTNN.equals(checkTN)) && (checkTGNN.equals(checkTGN)) && (checkRegId == checkRegIdN) && dto.getTraderBranchNo() > 0)
//					{
//						dto.setTraderOwnerName("สาขา "+count);
//						count ++;
//					}else
//					{*/
//						String traderOwnerName = dto.getPrefixName()+dto.getFirstName() +"  " + dto.getLastName();
//						//String guideName  = traderOwnerName + " (" + dto.getLicenseNo() +")";
//						dto.setTraderOwnerName(traderOwnerName);
//						//dto.setGuideName(guideName);
//						count = 1;
//						countBusiness++;
//					//}
//					
//				}
//				
//				System.out.println("dto.getLicenseNo() = "+dto.getLicenseNo());
//				
//				List<Trader> listTrader = this.traderDAO.findTraderByLicenseNo(dto.getLicenseNo(), dto.getTraderType(), "N", user.getUserData().getOrgId()) ;
//				
//				//Oat Edit 16/09/57 เพิ่ม && listTrader.size() == 1 ถ้าจดใหม่จะไม่มี licenseNo ทำให้ listTrader มากกว่า 1
//				if(!listTrader.isEmpty() && listTrader.size() == 1)
//				{
//					Trader trader = listTrader.get(0);
//					
//					if(TraderType.TOUR_COMPANIES.getStatus().equals(dto.getTraderType()))
//					{
//						
////						dto.setHeaderInfo(trader.getTraderName()+" เลขที่ใบอนุญาต "+trader.getLicenseNo());
//						dto.setHeaderInfo("เลขที่ใบอนุญาต "+trader.getLicenseNo());
//					}else
//					{
//						
////						dto.setHeaderInfo(dto.getTraderOwnerName()+" เลขที่ใบอนุญาต "+trader.getLicenseNo());
//						//dto.setHeaderInfo("เลขที่ใบอนุญาต "+trader.getLicenseNo());
//					}
//				}
//				
//				
//				
//			    checkTNN = checkTN;
//				checkTGNN = checkTGN;
//				checkRegIdN = checkRegId;
//				
//				dto.setToday(ft.format((Date)new Date()));
//				
//				list.add(dto);
//				
//				
//			}
//
//		}
//		
//		//Oat Add 19/09/57
//		model.put("countBusiness", "จำนวน "+countBusiness+" ราย");
//		model.put("countBusinessBranch", "จำนวน "+countBusinessBranch+" สาขา");
//		//
//		
//		return list;
//	}
	//End Oat Add 18/09/57
	

	
}
