package sss.dot.tourism.service.registration;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.mas.MasRunningNoDAO;
import sss.dot.tourism.dao.mas.RegisterFlowDAO;
import sss.dot.tourism.dao.registration.PersonDAO;
import sss.dot.tourism.dao.registration.PlantripDAO;
import sss.dot.tourism.dao.registration.RegisterProgressDAO;
import sss.dot.tourism.dao.registration.RegistrationDAO;
import sss.dot.tourism.dao.registration.TraderAddressDAO;
import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.domain.AdmUser;
import sss.dot.tourism.domain.RegisterFlow;
import sss.dot.tourism.domain.RegisterProgress;
import sss.dot.tourism.domain.Registration;
import sss.dot.tourism.dto.registration.RegisterProgressDTO;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.service.trader.ITourCompaniesService;
import sss.dot.tourism.service.trader.ITourLeaderService;
import sss.dot.tourism.util.ProgressStatus;
import sss.dot.tourism.util.RoleStatus;

import com.sss.aut.service.User;

@Repository("recheckRegistrationService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class RecheckRegistrationService implements IApprovalProcessService {

	@Autowired
	TraderDAO traderDAO;
	@Autowired
	RegisterProgressDAO registerProgressDAO;
	@Autowired
	RegistrationDAO registrationDAO;
	@Autowired
	ITourLeaderService tourLeaderService;
	@Autowired
	RegisterFlowDAO registerFlowDAO;
	@Autowired
	IRegistrationService registrationService;
	@Autowired
	IRegisterProgressService registerProgressService;
	
	@Autowired
	IApprovalProcessService supervisorService;
	
	private String PREVIOUS_ROLE = "'KEY','LAW'";
	private String NEXT_ROLE = "'SUP','MAN','DIR','ACC'";
	public List getTraderBetweenRegistration(Object object, User user,
			int start, int limit) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถโหลดข้อมูลได้การจดทะเบียนได้");
		}
			
		RegistrationDTO params = (RegistrationDTO)object;
		
		if((null != params.getRegProgressStatus()) && (!"".equals(params.getRegProgressStatus())))
	    {
	    	// w ยังไม่ส่งเรื่อง
	    	if(params.getRegProgressStatus().equals(ProgressStatus.WAITING.getStatus()))
	    	{
	    		params.setRegGroupProgress(null);
	    	}
	    	else if(params.getRegProgressStatus().equals(ProgressStatus.ACCEPT.getStatus()))
	    	{
	    		params.setRegGroupProgress(this.NEXT_ROLE);
	    		
	    	}else if(params.getRegProgressStatus().equals(ProgressStatus.GET_IT.getStatus()))
	    	{
	    		params.setRegGroupProgress(PREVIOUS_ROLE);
	    	}
	    }
		
		params.setRegistrationProgress(RoleStatus.RECHECK.getStatus());
		
		List<RegistrationDTO> list = registrationService.getTraderBetweenRegistrationPaging(object, user, start, limit);
		
		if(user.getUserData().getOrgId() != 1)
		{
			if(!list.isEmpty())
			{
				for(RegistrationDTO reg :list)
				{
					reg.setMultipleJobs(RoleStatus.MANAGER.getStatus());
				}
			}
		}
		

		
		return list;
	}
	


	public List countTraderBetweenRegistration(Object object, User user,
			int start, int limit) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถโหลดข้อมูลได้การจดทะเบียนได้");
		}
			
		RegistrationDTO params = (RegistrationDTO)object;
		
		if((null != params.getRegProgressStatus()) && (!"".equals(params.getRegProgressStatus())))
	    {
	    	// w ยังไม่ส่งเรื่อง
	    	if(params.getRegProgressStatus().equals(ProgressStatus.WAITING.getStatus()))
	    	{
	    		params.setRegGroupProgress(null);
	    	}
	    	else if(params.getRegProgressStatus().equals(ProgressStatus.ACCEPT.getStatus()))
	    	{
	    		params.setRegGroupProgress(this.NEXT_ROLE);
	    		
	    	}else if(params.getRegProgressStatus().equals(ProgressStatus.GET_IT.getStatus()))
	    	{
	    		params.setRegGroupProgress(PREVIOUS_ROLE);
	    	}
	    }
		
		params.setRegistrationProgress(RoleStatus.RECHECK.getStatus());
		
		return registrationService.countTraderBetweenRegistrationPaging(object, user, start, limit);
	}



	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void verification(Object object, User user) throws Exception {
		
		if(!(object instanceof RegisterProgressDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้้");
		}
		
		RegisterProgressDTO param = (RegisterProgressDTO)object;
		registerProgressService.verification(object, user, RoleStatus.RECHECK.getStatus());
		
		
		if(ProgressStatus.ACCEPT.getStatus().equals(param.getProgressStatusManager()))
		{
			this.supervisorService.verification(object, user);
		}

	}

}
