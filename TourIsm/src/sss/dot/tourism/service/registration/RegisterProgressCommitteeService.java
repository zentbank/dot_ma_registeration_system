package sss.dot.tourism.service.registration;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.codehaus.groovy.tools.shell.commands.RegisterCommand;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.registration.CommitteeDAO;
import sss.dot.tourism.dao.registration.PersonDAO;
import sss.dot.tourism.dao.registration.RegistrationDAO;
import sss.dot.tourism.domain.Committee;
import sss.dot.tourism.domain.Person;
import sss.dot.tourism.dto.registration.CommitteeDTO;
import sss.dot.tourism.dto.registration.RegisterProgressDTO;
import sss.dot.tourism.util.CommitteeStatus;
import sss.dot.tourism.util.CommitteeType;
import sss.dot.tourism.util.DateUtils;

import com.sss.aut.service.User;

@Repository("registerProgressCommitteeService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class RegisterProgressCommitteeService implements IRegisterProgressCommitteeService{

	@Autowired
	CommitteeDAO committeeDAO;
	@Autowired
	PersonDAO personDAO;
	@Autowired
	RegistrationDAO registrationDAO;

	public List getRegistrationProgressCommitteeStatus(Object object, User user)
			throws Exception {
			
		if(!(object instanceof CommitteeDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถดูข้อมูลได้้");
		}
		
		CommitteeDTO params = (CommitteeDTO)object;
		List<CommitteeDTO> list = new ArrayList<CommitteeDTO>();
		DateFormat ft = DateUtils.getProcessDateFormatThai();
		List<Object[]> listObjS = (List<Object[]>)this.registrationDAO.findCheckIdentityNoRegistrationRevokeSuspend(params, user);
		
	 if(params.getCommitteeIdentityNo()!=null && !params.getCommitteeIdentityNo().equals(""))
		{
		if(!listObjS.isEmpty() && listObjS !=null)
		{
			for(Object[] sel : listObjS)
			{
				CommitteeDTO dto = new CommitteeDTO();

				if(sel[0]!=null)
				{
					dto.setPersonId(Long.valueOf(sel[0].toString()));
				}
				if(sel[1]!=null)
				{
					dto.setPersonType(sel[1].toString());
				}
				
				if(sel[2]!=null)
				{
					dto.setPrefixName(sel[2].toString());
				}
				
				if(sel[3]!=null)
				{
					dto.setPostfixName(sel[3].toString());
				}
				
				if(sel[4]!=null)
				{
					dto.setPrefixNameCommittee(sel[4].toString());
				}
				
				if(sel[5]!=null)
				{
					dto.setPostfixNameCommittee(sel[5].toString());
				}
				
				if(sel[6]!=null)
				{
					dto.setFirstName(sel[6].toString());
				}
				dto.setLastName(sel[7]==null?"":sel[7].toString());
				dto.setPersonStatus(sel[8]==null?"":sel[8].toString());
				
				if(sel[9]!=null)
				{
					dto.setPunishmentDatePerson(ft.format((Date)sel[9]));
				}
				if(sel[10]!=null)
				{
					dto.setPunishmentExpireDatePerson(ft.format((Date)sel[10]));
				}
				dto.setCommitteeStatus(sel[11]==null?"":sel[11].toString());
				dto.setCommitteeStatusName(CommitteeStatus.getMeaning(dto.getCommitteeStatus()));
				System.out.println(dto.getCommitteeStatusName());
				dto.setCommitteeName(sel[12]==null?"":sel[12].toString());
				dto.setCommitteeLastname(sel[13]==null?"":sel[13].toString());
				
				dto.setCommitteeType(sel[14]==null?"":sel[14].toString());
				dto.setCommitteeTypeName(CommitteeType.getMeaning(dto.getCommitteeType()));
				
				if(sel[15]!=null)
				{
					dto.setPunishmentDateCommittee(ft.format((Date)sel[15]));
				}
				if(sel[16]!=null)
				{
					dto.setPunishmentExpireDateCommittee(ft.format((Date)sel[16]));
				}
				
				dto.setTraderName(sel[17]==null?"":sel[17].toString());
				
				if(dto.getPunishmentExpireDateCommittee()!=null && dto.getPunishmentDateCommittee()!=null)
				{
					long date = DateUtils.DateDiff((Date)sel[16],(Date)sel[15]);
					int period = (int) date;
					System.out.println(period);
					period = period/30;
					dto.setPeriod("เป็นเวลา" +String.valueOf(period)+" เดือน");
					System.out.println(period);
					if(period >= 12)
					{
						period = period/12;
						dto.setPeriod("เป็นเวลา " +String.valueOf(period)+" ปี");
					}
					System.out.println(dto.getPeriod());
					
					//dto.setPeriod(String.valueOf(period));
				}
				if(dto.getPersonType().equals("I"))
				{
					dto.setCommitteeOwnerName("มีชื่อเป็น"+dto.getCommitteeTypeName()+" ของผู้ขอจดทะเบียน "+dto.getPrefixName()+dto.getFirstName()+dto.getLastName());
				}else
				{
					dto.setCommitteeOwnerName("มีชื่อเป็น"+dto.getCommitteeTypeName()+" "+dto.getPrefixName()+dto.getFirstName()+dto.getPostfixName());
				}
					System.out.println(dto.getCommitteeOwnerName());
				list.add(dto);
			}
		}		
}else
{
	List<Object[]> listObj = (List<Object[]>)this.registrationDAO.findCheckIdentityNoRegistrationPersonRevokeSuspend(params, user);
	if(!listObj.isEmpty() && listObj !=null)
	{
		for(Object[] sel : listObj)
		{
			CommitteeDTO dto = new CommitteeDTO();

			if(sel[0]!=null)
			{
				dto.setPersonId(Long.valueOf(sel[0].toString()));
			}
			if(sel[1]!=null)
			{
				dto.setPersonType(sel[1].toString());
			}
			
			if(sel[2]!=null)
			{
				dto.setPrefixName(sel[2].toString());
			}
			
			if(sel[3]!=null)
			{
				dto.setPostfixName(sel[3].toString());
			}
			
			if(sel[4]!=null)
			{
				dto.setPrefixNameCommittee(sel[4].toString());
			}
			
			if(sel[5]!=null)
			{
				dto.setPostfixNameCommittee(sel[5].toString());
			}
			
			if(sel[6]!=null)
			{
				dto.setFirstName(sel[6].toString());
			}
			dto.setLastName(sel[7]==null?"":sel[7].toString());
			//dto.setPersonStatus(sel[8]==null?"":sel[8].toString());
			dto.setCommitteeStatus(sel[8]==null?"":sel[8].toString());
			/*if(sel[9]!=null)
			{
				dto.setPunishmentDatePerson(ft.format((Date)sel[9]));
			}
			if(sel[10]!=null)
			{
				dto.setPunishmentExpireDatePerson(ft.format((Date)sel[10]));
			}*/
			if(sel[9]!=null)
			{
				dto.setPunishmentDateCommittee(ft.format((Date)sel[9]));
			}
			if(sel[10]!=null)
			{
				dto.setPunishmentExpireDateCommittee(ft.format((Date)sel[10]));
			}
			//dto.setCommitteeStatus(sel[11]==null?"":sel[11].toString());
			dto.setCommitteeStatusName(CommitteeStatus.getMeaning(dto.getCommitteeStatus()));
			System.out.println(dto.getCommitteeStatusName());
			dto.setCommitteeName(sel[12]==null?"":sel[12].toString());
			dto.setCommitteeLastname(sel[13]==null?"":sel[13].toString());
			
			if(sel[14]!=null)
			{
				dto.setCommitteeType(sel[14]==null?"":sel[14].toString());
				dto.setCommitteeTypeName(CommitteeType.getMeaning(dto.getCommitteeType()));
			}
			//if(sel[15]!=null)
			//{
			//	dto.setPunishmentDateCommittee(ft.format((Date)sel[15]));
		//	}
			//if(sel[16]!=null)
			//{
			//	dto.setPunishmentExpireDateCommittee(ft.format((Date)sel[16]));
			//}
			
			dto.setTraderName(sel[17]==null?"":sel[17].toString());
			
			if(dto.getPunishmentExpireDateCommittee()!=null && dto.getPunishmentDateCommittee()!=null)
			{
				long date = DateUtils.DateDiff((Date)sel[10],(Date)sel[9]);
				int period = (int) date;
				System.out.println(period);
				period = period/30;
				dto.setPeriod("เป็นเวลา" +String.valueOf(period)+" เดือน");
				System.out.println(period);
				if(period >= 12)
				{
					period = period/12;
					dto.setPeriod("เป็นเวลา " +String.valueOf(period)+" ปี");
				}
				System.out.println(dto.getPeriod());
				
				//dto.setPeriod(String.valueOf(period));
			}
			if(dto.getPersonType().equals("I"))
			{
				if(dto.getCommitteeTypeName()!=null)
				{
					dto.setCommitteeOwnerName("มีชื่อเป็น"+dto.getCommitteeTypeName()+" ของผู้ขอจดทะเบียน "+dto.getPrefixName()+" "+dto.getFirstName()+" "+dto.getLastName());
				}else
				{
					dto.setCommitteeOwnerName("ผู้ขอจดทะเบียน "+dto.getPrefixName()+" "+dto.getFirstName()+" "+dto.getLastName());
				}
			}else
			{	if(dto.getCommitteeTypeName()!=null)
				{
					dto.setCommitteeOwnerName("มีชื่อเป็น"+dto.getCommitteeTypeName()+" "+dto.getPrefixName()+" "+dto.getFirstName()+" "+dto.getPostfixName());
				}else{
					dto.setCommitteeOwnerName("ผู้ขอจดทะเบียน "+dto.getPrefixName()+" "+dto.getFirstName()+" "+dto.getPostfixName());
				}
			}
				System.out.println(dto.getCommitteeOwnerName());
			list.add(dto);
		}
	}
}
		
		return list;
	}

}
