package sss.dot.tourism.service.registration;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.mas.RegisterFlowDAO;
import sss.dot.tourism.dao.registration.RegisterProgressDAO;
import sss.dot.tourism.dao.registration.RegistrationDAO;
import sss.dot.tourism.domain.AdmUser;
import sss.dot.tourism.domain.RegisterFlow;
import sss.dot.tourism.domain.RegisterProgress;
import sss.dot.tourism.domain.Registration;
import sss.dot.tourism.dto.registration.RegisterProgressDTO;
import sss.dot.tourism.service.trader.ITourCompaniesService;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.ProgressStatus;
import sss.dot.tourism.util.RoleStatus;

import com.sss.aut.service.User;
@Repository("registerProgressService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class RegisterProgressService implements IRegisterProgressService {
	
	@Autowired
	RegistrationDAO registrationDAO;

	@Autowired
	RegisterProgressDAO registerProgressDAO;

	@Autowired
	ITourCompaniesService tourCompaniesService;

	@Autowired
	RegisterFlowDAO registerFlowDAO;


	public List getRegistrationProgressStatus(Object object, User user)
			throws Exception {
		if(!(object instanceof RegisterProgressDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้้");
		}
		
		List<RegisterProgressDTO> list = new ArrayList<RegisterProgressDTO>();
		
		RegisterProgressDTO param = (RegisterProgressDTO) object;
		
		List<RegisterProgress> listAll = (List<RegisterProgress>) this.registerProgressDAO.findRegisterProgressbyRegistration(param.getRegId(), null);
		
		if(!listAll.isEmpty())
		{
			for(RegisterProgress pr: listAll)
			{
				RegisterProgressDTO dto = new RegisterProgressDTO();
				ObjectUtil.copy(pr, dto);
				
				dto.setProgress(RoleStatus.getMeaning(pr.getProgress()));
				
				if(!(ProgressStatus.GET_IT.getStatus().equals(pr.getProgressStatus())))
				{
//					if(!(pr.getProgressStatus().equals(ProgressStatus.WAITING.getStatus())))
					if(!ProgressStatus.WAITING.getStatus().equals(pr.getProgressStatus()))
					{
						dto.setProgressStatusName("ผลการตรวจเรื่อง " + ProgressStatus.getMeaning(pr.getProgressStatus()));
					}
					else
					{
						dto.setProgressStatusName("");
					}
				}
				else
				{
					dto.setProgressStatusName("");
				}
				
				if(pr.getAdmUser() != null)
				{
					String authorityName = pr.getAdmUser().getMasPrefix().getPrefixName() + pr.getAdmUser().getUserName() +" "
							+pr.getAdmUser().getUserLastname();
					
					dto.setAuthorityName(authorityName);
				}
				
				list.add(dto);
			}
		}
		
		return list;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void verification(Object object, User user, String roleStatus) throws Exception {
		if(!(object instanceof RegisterProgressDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้้");
		}
		
		RegisterProgressDTO param = (RegisterProgressDTO)object;
		
		Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(param.getRegId());
		
		//find law
		List<RegisterProgress> listProg = this.registerProgressDAO.findRoleProgressbyRegistration(reg.getRegId(), roleStatus);
		
		Calendar cal = Calendar.getInstance();
	
		
		if(!listProg.isEmpty())
		{
			RegisterProgress progress =  listProg.get(0);
			
			
			AdmUser  admUser = new AdmUser();
			admUser.setUserId(user.getUserId());
			progress.setAdmUser(admUser);
//			progress.setProgress(RoleStatus.KEY.getStatus());
			progress.setProgressDate(cal.getTime());
//			String positionName = user.getUserData().getPositionName()==null?"":user.getUserData().getPositionName();
			progress.setProgressDesc(roleStatus+" "+param.getProgressDesc()==null?"":param.getProgressDesc());
			progress.setProgressStatus(param.getProgressStatus());
			progress.setLastUpdUser(user.getUserName());
			progress.setLastUpdDtm(new Date());
			
			registerProgressDAO.update(progress);
		}

		//get next role
		RegisterFlow flow = registerFlowDAO.findNextRole(roleStatus);
		
		//update registration next role
		
		reg.setLastUpdUser(user.getUserName());
		reg.setLastUpdDtm(new Date());
		
		//insert register progress next role waiting
		
		RegisterProgress nextProgress =  new RegisterProgress();
		nextProgress.setRegistration(reg);
		
		cal.add(Calendar.MINUTE, 10);
		nextProgress.setProgressDate(cal.getTime());
		
		if(ProgressStatus.ACCEPT.getStatus().equals(param.getProgressStatus()))
		{
			reg.setRegistrationProgress(flow.getNextRole());
			nextProgress.setProgress(flow.getNextRole());
			nextProgress.setProgressDesc(RoleStatus.getMeaning(flow.getNextRole())+" "+ProgressStatus.WAITING.getMeaning());
			nextProgress.setProgressStatus(ProgressStatus.WAITING.getStatus());
			
			// TODO Notification to next role
		}
		else
		{
			//ตีเรื่องกลับ
			reg.setRegistrationProgress(flow.getPreviousRole());
			nextProgress.setProgress(flow.getPreviousRole());
//			nextProgress.setProgressDesc(RoleStatus.LAW.getMeaning()+" "+param.getProgressDesc()==null?"ตีเรื่องกลับ":param.getProgressDesc());
			nextProgress.setProgressDesc(RoleStatus.getMeaning(flow.getPreviousRole())+" "+ProgressStatus.WAITING.getMeaning());
			nextProgress.setProgressStatus(ProgressStatus.WAITING.getStatus());
			nextProgress.setProgressBackStatus(ProgressStatus.BACK.getStatus());
			
			// TODO Notification to previous role
		}
		registerProgressDAO.insert(nextProgress);
		
		registrationDAO.update(reg);
		
	}
	
	public static void main(String arg[])
	{
		Calendar cal = Calendar.getInstance();
		System.out.println(cal.getTime());
		cal.add(Calendar.HOUR, 1);
		System.out.println(cal.getTime());
	}

}
