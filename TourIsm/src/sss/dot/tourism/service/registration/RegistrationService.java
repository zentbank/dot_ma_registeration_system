package sss.dot.tourism.service.registration;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.AdmUserDAO;
import sss.dot.tourism.dao.AdmUserGroupDAO;
import sss.dot.tourism.dao.DotTrFeeLicenseDAO;
import sss.dot.tourism.dao.mas.ForeignLanguageDAO;
import sss.dot.tourism.dao.mas.MasRunningNoDAO;
import sss.dot.tourism.dao.mas.OrganizationDAO;
import sss.dot.tourism.dao.mas.RegisterFlowDAO;
import sss.dot.tourism.dao.registration.BranchDAO;
import sss.dot.tourism.dao.registration.CommitteeDAO;
import sss.dot.tourism.dao.registration.PersonDAO;
import sss.dot.tourism.dao.registration.PlantripDAO;
import sss.dot.tourism.dao.registration.PrintingCardDAO;
import sss.dot.tourism.dao.registration.PrintingLicenseDAO;
import sss.dot.tourism.dao.registration.RegisterProgressDAO;
import sss.dot.tourism.dao.registration.RegistrationDAO;
import sss.dot.tourism.dao.registration.TraderAddressDAO;
import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.dao.training.EducationDAO;
import sss.dot.tourism.domain.AdmGroup;
import sss.dot.tourism.domain.AdmUser;
import sss.dot.tourism.domain.Committee;
import sss.dot.tourism.domain.DotTrFeeLicense;
import sss.dot.tourism.domain.Education;
import sss.dot.tourism.domain.ForeignLanguage;
import sss.dot.tourism.domain.MasAmphur;
import sss.dot.tourism.domain.MasPosfix;
import sss.dot.tourism.domain.MasPrefix;
import sss.dot.tourism.domain.MasProvince;
import sss.dot.tourism.domain.Organization;
import sss.dot.tourism.domain.Person;
import sss.dot.tourism.domain.Plantrip;
import sss.dot.tourism.domain.RegisterFlow;
import sss.dot.tourism.domain.RegisterProgress;
import sss.dot.tourism.domain.Registration;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.domain.TraderAddress;
import sss.dot.tourism.dto.registration.RegisterProgressDTO;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.service.trader.ITourCompaniesService;
import sss.dot.tourism.service.trader.ITourLeaderService;
import sss.dot.tourism.service.trader.ITraderGuideService;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.LicenseStatus;
import sss.dot.tourism.util.MailService;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.PersonType;
import sss.dot.tourism.util.ProgressStatus;
import sss.dot.tourism.util.PropertyLoader;
import sss.dot.tourism.util.RecordStatus;
import sss.dot.tourism.util.RegistrationType;
import sss.dot.tourism.util.RoleStatus;
import sss.dot.tourism.util.TraderAddressType;
import sss.dot.tourism.util.TraderCategory;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.User;

@Repository("registrationService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class RegistrationService implements IRegistrationService{
	
	@Autowired
	MasRunningNoDAO masRunningNoDAO;
	@Autowired
	RegistrationDAO registrationDAO;
	@Autowired
	TraderDAO traderDAO;
	@Autowired
	RegisterProgressDAO registerProgressDAO;
	@Autowired
	PersonDAO personDAO;
	@Autowired
	ITourCompaniesService tourCompaniesService;
	@Autowired
	TraderAddressDAO traderAddressDAO;
	@Autowired
	PlantripDAO plantripDAO;
	@Autowired
	ITourLeaderService tourLeaderService;
	@Autowired
	RegisterFlowDAO registerFlowDAO;
	@Autowired
	ITraderGuideService guideService;
	
	@Autowired
	CommitteeDAO committeeDAO;
	@Autowired
	BranchDAO branchDAO;
	@Autowired
	EducationDAO educationDAO;
	@Autowired
	ForeignLanguageDAO foreignLanguageDAO;
	
	@Autowired
	PrintingLicenseDAO printingLicenseDAO;
	@Autowired
	PrintingCardDAO printingCardDAO;
	
	@Autowired
	AdmUserDAO admUserDAO;
	@Autowired
	AdmUserGroupDAO admUserGroupDAO;	
	
	@Autowired
	OrganizationDAO organizationDAO;
	
	@Autowired private DotTrFeeLicenseDAO dotTrFeeLicenseDAO;
	private Properties resources;
	  protected Properties getResources()
	  {
	    if(this.resources == null)
	    {
	      this.resources = PropertyLoader.loadProperties("sss/dot/tourism/util/ApplicationResources.properties");
	    }
	    return this.resources;
	  }
	
	private final String REGISTER_USER = "ONLINE";
	
	
	public static void main(String arg[])
	{
		Calendar effDate = Calendar.getInstance();
		Calendar expDate = Calendar.getInstance();
		expDate.add(Calendar.YEAR, 2);
		expDate.add(Calendar.DATE, -1);
		
		System.out.println("effDate " + effDate.getTime());
		System.out.println("expDate " + expDate.getTime());
		
		effDate.set(Calendar.YEAR, expDate.get(Calendar.YEAR));
		
		System.out.println(effDate.getTime());
		
		expDate.add(Calendar.YEAR, 2);
		
		System.out.println("effDate 2 " + effDate.getTime());
		System.out.println("expDate 2 " + expDate.getTime());
		
	}
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)	
	public Object addNewRegistration(Object object, User user) throws Exception {
		
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถ เพิ่มการทำรายการทะเบียนใบอนุญาตใหม่ได้");
		}	
		RegistrationDTO params = (RegistrationDTO)object;
		if((null == params.getRegistrationType()) && (params.getRegistrationType().isEmpty()))
		{
			throw new IllegalArgumentException("ไม่มีข้อมูล  RegistrationType");
		}
		
		Trader trader =  (Trader)this.traderDAO.findByPrimaryKey(params.getTraderId());
		
		if(trader.getLicenseStatus().equals(LicenseStatus.EXPIRE.getStatus())){
			throw new IllegalArgumentException("ไม่สามารถดำเนินการได้ เนื่องจากใบอนุญาตหมดอายุ");
		}
		Person person = trader.getPerson();
		
		String registrationNo = "0";
		if(!TraderType.LEADER.equals(trader.getTraderType()))
		{
			registrationNo = masRunningNoDAO.getRegistrationNo(user.getUserData().getOrgId() ,trader.getTraderType(), null);

		}
		
		Organization org = new Organization();
		org.setOrgId(user.getUserData().getOrgId());
		
		Person newRegPerson = new Person();
		
		ObjectUtil.copy(person, newRegPerson);
		
		newRegPerson.setPersonNationality("ไทย");

		if(person.getMasPrefix() != null)
		{
			newRegPerson.setMasPrefix(person.getMasPrefix());
		}
		
		if(person.getMasPosfix() != null)
		{
			newRegPerson.setMasPosfix(person.getMasPosfix());
		}
		
		if(person.getMasAmphurByAmphurId() != null)
		{
			newRegPerson.setMasAmphurByAmphurId(person.getMasAmphurByAmphurId());
		}
		
		if(person.getMasAmphurByTaxAmphurId() != null)
		{
			newRegPerson.setMasAmphurByTaxAmphurId(person.getMasAmphurByTaxAmphurId());
		}
		
		if(person.getMasProvinceByProvinceId() != null)
		{
			newRegPerson.setMasProvinceByProvinceId(person.getMasProvinceByProvinceId());
		}	
		
		if(person.getMasProvinceByTaxProvinceId() != null)
		{
			newRegPerson.setMasProvinceByTaxProvinceId(person.getMasProvinceByTaxProvinceId());
		}
		
		newRegPerson.setCreateUser(user.getUserName());
		newRegPerson.setRecordStatus(RecordStatus.TEMP.getStatus());
		
		Long personId = (Long)personDAO.insert(newRegPerson);
		newRegPerson.setPersonId(personId);
		
		Trader newRegTrader = new Trader();
		
		ObjectUtil.copy(trader, newRegTrader);
		
		
		if(trader.getTraderByTraderGuideId() != null)
		{
			newRegTrader.setTraderByTraderGuideId(trader.getTraderByTraderGuideId());
		}
		if(trader.getMasProvince() != null)
		{
			newRegTrader.setMasProvince(trader.getMasProvince());
		}
		newRegTrader.setOrganization(org);
		newRegTrader.setPerson(newRegPerson);
		
		if(TraderType.TOUR_COMPANIES.getStatus().equals(newRegTrader.getTraderType()))
		{
			Calendar effectiveDate = Calendar.getInstance();
			effectiveDate.setTime(trader.getEffectiveDate());
			
			Calendar expireDate = Calendar.getInstance();
			expireDate.setTime(trader.getExpireDate());
			
			if(RegistrationType.TOUR_COMPANIES_RENEW_REGISTRATION.getStatus().equals(params.getRegistrationType()))
			{
				
				effectiveDate.set(Calendar.YEAR, expireDate.get(Calendar.YEAR));
				expireDate.add(Calendar.YEAR, 2);
				
//				expireDate.add(Calendar.DATE, 1);
//				
//				Date newEffectiveDate = expireDate.getTime();
//				expireDate.add(Calendar.YEAR, 2);
//				Date newExpireDate = expireDate.getTime();
				
				newRegTrader.setEffectiveDate(effectiveDate.getTime());
				newRegTrader.setExpireDate(expireDate.getTime());
			}
			
			if(RegistrationType.TOUR_COMPANIES_TEMPORARY_LICENSE.getStatus().equals(params.getRegistrationType()))
			{
				//OAT EDIT 03/11/58
//				newRegTrader.setEffectiveDate(new Date());
				newRegTrader.setEffectiveDate(trader.getEffectiveDate());
				newRegTrader.setExpireDate(trader.getExpireDate());
			}

			if(RegistrationType.TOUR_COMPANIES_MINOR_CHANGE.getStatus().equals(params.getRegistrationType())){
				newRegTrader.setEffectiveDate(trader.getEffectiveDate());
				newRegTrader.setExpireDate(trader.getExpireDate());
			}
			
		}
		
		if(TraderType.GUIDE.getStatus().equals(newRegTrader.getTraderType()))
		{
			Calendar effectiveDate = Calendar.getInstance();
			effectiveDate.setTime(trader.getEffectiveDate()==null?new Date():trader.getEffectiveDate());
			
			Calendar expireDate = Calendar.getInstance();
			expireDate.setTime(trader.getExpireDate()==null?new Date():trader.getExpireDate());
			
			if(RegistrationType.GUIDE_RENEW_REGISTRATION.getStatus().equals(params.getRegistrationType()))
			{
				
//				Date day = new Date();
//				effectiveDate.setTime(day);
//				expireDate.setTime(day);
//				expireDate.add(Calendar.YEAR, 5);
//				expireDate.add(Calendar.DATE, -1);
				
				//OAT EDIT 16/10/2015
				effectiveDate.set(Calendar.YEAR, expireDate.get(Calendar.YEAR));
				expireDate.add(Calendar.YEAR, 5);
				
//				expireDate.add(Calendar.DATE, 1);
//				
//				Date newEffectiveDate = expireDate.getTime();
//				expireDate.add(Calendar.YEAR, 5);
//				Date newExpireDate = expireDate.getTime();
				
				newRegTrader.setEffectiveDate(effectiveDate.getTime());
				newRegTrader.setExpireDate(expireDate.getTime());
			}
			
			if(RegistrationType.GUIDE_TEMPORARY_LICENSE.getStatus().equals(params.getRegistrationType()))
//				||	RegistrationType.GUIDE_NAME_CHANGE.getStatus().equals(params.getRegistrationType()))
			{
				//OAT EDIT 03/11/58
//				newRegTrader.setEffectiveDate(new Date());
				newRegTrader.setEffectiveDate(trader.getEffectiveDate());
				newRegTrader.setExpireDate(trader.getExpireDate());
			}
		}
		
		newRegTrader.setCreateUser(user.getUserName());
		newRegTrader.setRecordStatus(RecordStatus.TEMP.getStatus());
//		trader.setLicenseStatus(RecordStatus.TEMP.getStatus());
		
		Long traderId =  (Long)traderDAO.insert(newRegTrader);
		newRegTrader.setTraderId(traderId);
		
		Registration reg = new Registration();
		reg.setTrader(newRegTrader);
		reg.setOrganization(org);
		reg.setRegistrationType(params.getRegistrationType());
		reg.setRegistrationNo(registrationNo==null?"":registrationNo);
		reg.setRegistrationDate(Calendar.getInstance().getTime());
		reg.setRegistrationProgress(RoleStatus.KEY.getStatus());
		reg.setRecordStatus(RecordStatus.TEMP.getStatus());
		
		Long regId =  (Long)this.registrationDAO.insert(reg);
		reg.setRegId(regId);
		
		RegisterProgress progress =  new RegisterProgress();
		progress.setRegistration(reg);
		
		AdmUser  admUser = (AdmUser)this.admUserDAO.findByPrimaryKey(user.getUserId());

		progress.setAdmUser(admUser);
		progress.setProgress(RoleStatus.KEY.getStatus());
		progress.setProgressDate(Calendar.getInstance().getTime());
//		String positionName = user.getUserData().getPositionName()==null?"":user.getUserData().getPositionName();
		progress.setProgressDesc(RoleStatus.KEY.getMeaning()+" "+ProgressStatus.GET_IT.getMeaning());
		progress.setProgressStatus(ProgressStatus.GET_IT.getStatus());
		registerProgressDAO.insert(progress);
		
		// trader addres
		List<TraderAddress> listAddress = (List<TraderAddress>) this.traderAddressDAO.findAllByTrader(trader.getTraderId(), null, RecordStatus.NORMAL.getStatus());
		if(!listAddress.isEmpty())
		{
			for(TraderAddress address: listAddress)
			{
				TraderAddress newRegTraderAddress = new TraderAddress();
				
				ObjectUtil.copy(address ,newRegTraderAddress);
				
				newRegTraderAddress.setMasAmphur(address.getMasAmphur());
				newRegTraderAddress.setMasProvince(address.getMasProvince());
				newRegTraderAddress.setTrader(newRegTrader);
				newRegTraderAddress.setMasTambol(address.getMasTambol());
				
				newRegTraderAddress.setRecordStatus(RecordStatus.TEMP.getStatus());
				newRegTraderAddress.setCreateUser(user.getUserName());
				newRegTraderAddress.setCreateDtm(new Date());
				this.traderAddressDAO.insert(newRegTraderAddress);
			}
		}
		//Branch
		List<Trader> listBranch = (List<Trader>) this.branchDAO.findAllByBranchParent(trader.getTraderId(), RecordStatus.NORMAL.getStatus());
		if(!listBranch.isEmpty())
		{
			for(Trader branch: listBranch)
			{
				Trader newBranch = new Trader();
				
				ObjectUtil.copy(branch, newBranch);
				
				newBranch.setPerson(newRegPerson);
				
				//------- Change data of branch have to set effective date and expire date same as parent branch;
				newBranch.setEffectiveDate(trader.getEffectiveDate());
				newBranch.setExpireDate(trader.getExpireDate());
				
				newBranch.setTraderByBranchParentId(newRegTrader);
				newBranch.setRecordStatus(RecordStatus.TEMP.getStatus());
				newBranch.setCreateUser(user.getUserName());
				newBranch.setCreateDtm(new Date());
				
				this.branchDAO.insert(newBranch);
				
				List<TraderAddress> listBranchAddress = (List<TraderAddress>) this.traderAddressDAO.findAllByTrader(branch.getTraderId(), null, RecordStatus.NORMAL.getStatus());
				if(!listBranchAddress.isEmpty())
				{
					for(TraderAddress branchAddress: listBranchAddress)
					{
						TraderAddress newBranchAddress = new TraderAddress();
						
						ObjectUtil.copy(branchAddress, newBranchAddress);
						
						if(branchAddress.getMasAmphur() != null)
						{
							newBranchAddress.setMasAmphur(branchAddress.getMasAmphur());
						}
						
						if(branchAddress.getMasProvince() != null)
						{
							newBranchAddress.setMasProvince(branchAddress.getMasProvince());
						}
						newBranchAddress.setTrader(newBranch);
						
						newBranchAddress.setRecordStatus(RecordStatus.TEMP.getStatus());
						newBranchAddress.setCreateUser(user.getUserName());
						newBranchAddress.setCreateDtm(new Date());
						this.traderAddressDAO.insert(newBranchAddress);
					}
				}
			}
		}	
		// plantrip
		List<Plantrip> listPlanTrip = (List<Plantrip>) this.plantripDAO.findPlanTripbyTrader(trader.getTraderId(), RecordStatus.NORMAL.getStatus());
		if(!listPlanTrip.isEmpty())
		{
			for(Plantrip plantrip: listPlanTrip)
			{
				Plantrip newRegPlantrip = new Plantrip();
				ObjectUtil.copy(plantrip, newRegPlantrip);
				
				newRegPlantrip.setCountry(plantrip.getCountry());
				newRegPlantrip.setTrader(newRegTrader);
				newRegPlantrip.setRecordStatus(RecordStatus.TEMP.getStatus());
				newRegPlantrip.setCreateUser(user.getUserName());
				newRegPlantrip.setCreateDtm(new Date());
				this.plantripDAO.insert(newRegPlantrip);
			}
		}
		//committee
		List<Committee> listCommittee =  (List<Committee>) this.committeeDAO.findAllByPerson(person.getPersonId(), null, RecordStatus.NORMAL.getStatus());
		if(!listCommittee.isEmpty())
		{
			for(Committee comm: listCommittee)
			{
				Committee newCommittee = new Committee();
				
				ObjectUtil.copy(comm, newCommittee);
				newCommittee.setPerson(newRegPerson);
				
				newCommittee.setMasPrefix(comm.getMasPrefix());
				newCommittee.setMasAmphurByAmphurId(comm.getMasAmphurByAmphurId());
				newCommittee.setMasAmphurByTaxAmphurId(comm.getMasAmphurByTaxAmphurId());
				newCommittee.setMasProvinceByProvinceId(comm.getMasProvinceByProvinceId());
				newCommittee.setMasProvinceByTaxProvinceId(comm.getMasProvinceByTaxProvinceId());
				
				newCommittee.setMasPosfix(comm.getMasPosfix());
				
				newCommittee.setRecordStatus(RecordStatus.TEMP.getStatus());
				newCommittee.setCreateUser(user.getUserName());
				newCommittee.setCreateDtm(new Date());
				this.committeeDAO.insert(newCommittee);
			}
		}
		//education and traning
		List<Education> listEducation = (List<Education>) this.educationDAO.findEducationByPerson(person.getPersonId(), RecordStatus.NORMAL.getStatus(), null);
		if(!listEducation.isEmpty())
		{
			for(Education edu: listEducation)
			{
				Education newRegEdu = new Education();
				
				ObjectUtil.copy(edu, newRegEdu);
				
				newRegEdu.setMasUniversity(edu.getMasUniversity());
				newRegEdu.setMasEducationLevel(edu.getMasEducationLevel());
				newRegEdu.setPerson(newRegPerson);
				
				newRegEdu.setRecordStatus(RecordStatus.TEMP.getStatus());
				newRegEdu.setCreateUser(user.getUserName());
				newRegEdu.setCreateDtm(new Date());
				this.committeeDAO.insert(newRegEdu);
			}
		}
		//ภาษา
		List<ForeignLanguage> listLang = (List<ForeignLanguage>) this.foreignLanguageDAO.findForeignLanguageByPerson(person.getPersonId(), RecordStatus.NORMAL.getStatus());
		if(!listLang.isEmpty())
		{
			for(ForeignLanguage lang: listLang)
			{
				ForeignLanguage newRegLang = new ForeignLanguage();
				
				ObjectUtil.copy(lang, newRegLang);
				newRegLang.setCountry(lang.getCountry());
				newRegLang.setPerson(newRegPerson);
				newRegLang.setRecordStatus(RecordStatus.TEMP.getStatus());
				newRegLang.setCreateUser(user.getUserName());
				newRegLang.setCreateDtm(new Date());
				this.foreignLanguageDAO.insert(newRegLang);
			}
		}
		
		RegistrationDTO dto = new RegistrationDTO();
		
		dto.setTraderId(newRegTrader.getTraderId());
//		dto.setTraderType(newRegTrader.getTraderType());
		dto.setTraderRecordStatus(RecordStatus.TEMP.getStatus());
		dto.setRegRecordStatus(RecordStatus.TEMP.getStatus());
		
		dto.setPersonId(newRegPerson.getPersonId());
		
		List<RegistrationDTO> listRegistrationDTO = (List<RegistrationDTO>) this.getTraderBetweenRegistrationPaging(dto, user, 0, Integer.MAX_VALUE);
		
		if(!listRegistrationDTO.isEmpty())
		{
			dto = listRegistrationDTO.get(0);
		}
		
		return dto;
		
	}


	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void createRegistration(Object object, User user) throws Exception {
		
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถ เพิ่มการทำรายการทะเบียนใบอนุญาตใหม่ได้");
		}
				
		RegistrationDTO params = (RegistrationDTO)object;
		
		if((null == params.getTraderType()) && (params.getTraderType().isEmpty()))
		{
			throw new IllegalArgumentException("ไม่มีข้อมูล  TraderType");
		}
		
		if((null == params.getRegistrationType()) && (params.getRegistrationType().isEmpty()))
		{
			throw new IllegalArgumentException("ไม่มีข้อมูล  RegistrationType");
		}
		String registrationNo = "0";
		if(!TraderType.LEADER.equals(params.getTraderType()))
		{
			registrationNo = masRunningNoDAO.getRegistrationNo(user.getUserData().getOrgId() ,params.getTraderType(), null);

		}						
		Organization org = new Organization();
		org.setOrgId(user.getUserData().getOrgId());
		
		Person person = new Person();
		person.setCreateUser(user.getUserName());
		person.setRecordStatus(RecordStatus.TEMP.getStatus());
		
		Long personId = (Long)personDAO.insert(person);
		person.setPersonId(personId);
		params.setPersonId(personId);
		
		Trader trader = new Trader();
		
		trader.setTraderType(params.getTraderType());
		trader.setCreateUser(user.getUserName());
		trader.setOrganization(org);
		trader.setPerson(person);
		trader.setRecordStatus(RecordStatus.TEMP.getStatus());
		trader.setLicenseStatus(RecordStatus.TEMP.getStatus());
		
		Long traderId =  (Long)traderDAO.insert(trader);
		trader.setTraderId(traderId);
		params.setTraderId(traderId);
		
		Registration reg = new Registration();
		reg.setTrader(trader);
		reg.setOrganization(org);
		reg.setRegistrationType(params.getRegistrationType());
		reg.setRegistrationNo(registrationNo==null?"":registrationNo);
		reg.setRegistrationDate(Calendar.getInstance().getTime());
		reg.setRegistrationProgress(RoleStatus.KEY.getStatus());
		reg.setRecordStatus(RecordStatus.TEMP.getStatus());
		
		Long regId =  (Long)this.registrationDAO.insert(reg);
		reg.setRegId(regId);
		params.setRegId(regId);
		params.setRegistrationNo(registrationNo);
		
		DateFormat ft = DateUtils.getProcessDateFormatThai();
		params.setRegistrationDate(ft.format(reg.getRegistrationDate()));

		
		RegisterProgress progress =  new RegisterProgress();
		progress.setRegistration(reg);
		
		AdmUser  admUser = new AdmUser();
		admUser.setUserId(user.getUserId());
		progress.setAdmUser(admUser);
		progress.setProgress(RoleStatus.KEY.getStatus());
		progress.setProgressDate(Calendar.getInstance().getTime());
//		String positionName = user.getUserData().getPositionName()==null?"":user.getUserData().getPositionName();
		progress.setProgressDesc(RoleStatus.KEY.getMeaning()+" "+ProgressStatus.GET_IT.getMeaning());
		progress.setProgressStatus(ProgressStatus.GET_IT.getStatus());
		registerProgressDAO.insert(progress);
		
		params.setAuthorityName(user.getUserData().getUserFullName());
				
	}


	public List getGuideLicense(Object object, User user) throws Exception {
		
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("object not instanceof RegistrationDTO");
		}
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		RegistrationDTO param = (RegistrationDTO)object;
		List<Trader> listTrader = (List<Trader>) this.traderDAO.findLicenseGuideAll(param.getIdentityNo(), param.getLicenseNo(),
				param.getFirstName(),
				param.getLastName(),
				param.getTraderRecordStatus(),
				param.getPersonRecordStatus());
		
		if(!listTrader.isEmpty())
		{
			for(Trader trader: listTrader)
			{
				RegistrationDTO dto = new RegistrationDTO();
				
				ObjectUtil.copy(trader, dto);
				
				dto.setTraderGuideId(trader.getTraderId());
				dto.setTraderId(0);
				
				
				dto.setLicenseGuideNo(trader.getLicenseNo());
				dto.setTraderCategoryName(TraderCategory.getMeaning(trader.getTraderType(), trader.getTraderCategory()));
				int category = Integer.parseInt(trader.getTraderCategory());
				int traderCategoryGuideType = category / 100;
				
				dto.setTraderCategoryGuideType(String.valueOf(traderCategoryGuideType));
				dto.setTraderCategoryGuideTypeName(TraderCategory.getMeaning(trader.getTraderType(), dto.getTraderCategoryGuideType()));
				
				Person person = trader.getPerson();
				ObjectUtil.copy(person, dto);
				
				dto.setPersonGuideId(person.getPersonId());
				dto.setPersonId(0);
				
				String prefixName = "";
				if(person.getMasPrefix() != null)
				{
					MasPrefix prefix = person.getMasPrefix();
					prefixName = prefix.getPrefixName();
				}
				
				
				
				dto.setPersonFullName(prefixName + person.getFirstName() +" "+ person.getLastName());
				
				if(dto.getGender().equals("M"))
				{
					dto.setGenderName("ชาย");
				}
				else
				{
					dto.setGenderName("หญิง");
				}
				list.add(dto);
			}
		}
		
		return list;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveRegistration(Object object, User user) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
		try{
			
			RegistrationDTO dto =  (RegistrationDTO)object;
			
			Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(dto.getRegId());
			
			
			ObjectUtil.copy(dto, reg);
			
			reg.setLastUpdUser(user.getUserName());
			reg.setLastUpdDtm(new Date());
			
			
			Trader trader = (Trader)reg.getTrader();
			
			if(TraderType.LEADER.equals(trader.getTraderType()))
			{
				if(reg.getRegistrationNo().equals("0"))
				{
					String registrationNo = masRunningNoDAO.getRegistrationNo(user.getUserData().getOrgId() ,trader.getTraderType(), dto.getTraderCategory());
					reg.setRegistrationNo(registrationNo);
				}
			}
			
				
			ObjectUtil.copy(dto, trader);
			trader.setLicenseStatus(LicenseStatus.TEMP.getStatus());
			trader.setRecordStatus(RecordStatus.TEMP.getStatus());
			
			trader.setLastUpdUser(user.getUserName());
			trader.setLastUpdDtm(new Date());
							
			Person person = trader.getPerson();
			ObjectUtil.copy(dto, person);
			
			person.setLastUpdUser(user.getUserName());
			person.setLastUpdDtm(new Date());
			
			if(dto.getPrefixId() > 0)
			{
				MasPrefix masPrefix = new MasPrefix();
				masPrefix.setPrefixId(dto.getPrefixId());
				
				person.setMasPrefix(masPrefix);
			}
			
			if(dto.getPostfixId() > 0)
			{
				MasPosfix masPosfix = new MasPosfix();
				masPosfix.setPostfixId(dto.getPostfixId());
				
				person.setMasPosfix(masPosfix);
			}
			else
			{
				person.setMasPosfix(null);
			}
			
			if(dto.getAmphurId() > 0)
			{
				MasAmphur masAmphurByAmphurId = new MasAmphur();
				masAmphurByAmphurId.setAmphurId(dto.getAmphurId());
				
				person.setMasAmphurByAmphurId(masAmphurByAmphurId);
			}
		

			if(dto.getTaxAmphurId() > 0)
			{
				MasAmphur masAmphurByTaxAmphurId = new MasAmphur();
				masAmphurByTaxAmphurId.setAmphurId(dto.getTaxAmphurId());
				person.setMasAmphurByTaxAmphurId(masAmphurByTaxAmphurId);
				
			}
			
			if(dto.getTaxProvinceId() > 0)
			{
				MasProvince masProvinceByTaxProvinceId = new MasProvince();
				masProvinceByTaxProvinceId.setProvinceId(dto.getTaxProvinceId());
				person.setMasProvinceByTaxProvinceId(masProvinceByTaxProvinceId);
			}

			
			if(dto.getProvinceId() > 0)
			{
				MasProvince masProvinceByProvinceId = new MasProvince();
				masProvinceByProvinceId.setProvinceId(dto.getProvinceId());
				person.setMasProvinceByProvinceId(masProvinceByProvinceId);
			}
			
			if(StringUtils.isNotBlank(dto.getImageFile())){
				System.out.println("String image file length: "+dto.getImageFile().length());
				person.setImageFile(dto.getImageFile());
			}
			
			person.setLastUpdUser(user.getUserName());
			person.setLastUpdDtm(new Date());
			
			personDAO.update(person);
			traderDAO.update(trader);
			registrationDAO.update(reg);


			
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
		
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveAllRegistration(Object object, User user) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
		try{
			RegistrationDTO dto = (RegistrationDTO) object;
			
			this.saveRegistration(dto, user);
			
			if(TraderType.TOUR_COMPANIES.getStatus().equals(dto.getTraderType()))
			{
				tourCompaniesService.saveRegistration(dto, user);
			}
			
			if(TraderType.GUIDE.getStatus().equals(dto.getTraderType()))
			{
				guideService.saveRegistration(dto, user);
			}
			
			if(TraderType.LEADER.getStatus().equals(dto.getTraderType()))
			{
				tourLeaderService.saveRegistration(dto, user);
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
		
	}
	
	public List countTraderBetweenRegistrationPaging(Object object, User user ,int start, int limit) throws Exception
	{
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		

		
		RegistrationDTO params = (RegistrationDTO)object;
		
		List<Object[]> listObj = this.registrationDAO.findBetweenRegistrationPaging(params, user , start, limit);
		
		return listObj;
	}
	
	public List getTraderBetweenRegistrationPaging(Object object, User user ,int start, int limit) throws Exception {
		
		System.out.println("##########Service RegistrationService method getTraderBetweenRegistrationPaging");
		
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		RegistrationDTO params = (RegistrationDTO)object;
		
		DateFormat ft =  DateUtils.getProcessDateFormatThai();
		
		List<Object[]> listObj = this.registrationDAO.findBetweenRegistrationPaging(params, user , start, limit);
		
		System.out.println("#####listObj.size() = "+listObj.size());
		
		if(!listObj.isEmpty())
		{
			for(Object[] sel: listObj)
			{
				RegistrationDTO dto = new RegistrationDTO();

				
				dto.setRegId(Long.valueOf(sel[0].toString()));
				
				Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(dto.getRegId());
				ObjectUtil.copy(reg, dto);
				
				dto.setRegistrationNo(sel[1]==null?"":sel[1].toString());
				if(sel[2] != null)
				{
					
					dto.setRegistrationDate(ft.format((Date)sel[2]));
				}
				dto.setRegistrationProgress(sel[3]==null?"":sel[3].toString());
				if(sel[8] != null)
				{
					dto.setTraderType(sel[8]==null?"":sel[8].toString());
					dto.setTraderTypeName(TraderType.getMeaning(dto.getTraderType()));
				}
				if(sel[4]!=null)
				{
					dto.setRegistrationType(sel[4]==null?"":sel[4].toString());
					dto.setRegistrationTypeName(RegistrationType.getMeaning(dto.getTraderType(), dto.getRegistrationType()));
				}
				dto.setOrgId(Long.valueOf(sel[5].toString()));
				dto.setRegRecordStatus(sel[6]==null?"":sel[6].toString());
				

				
				dto.setTraderId(Long.valueOf(sel[7].toString()));
				
				Trader trader = (Trader)this.traderDAO.findByPrimaryKey(dto.getTraderId());
				ObjectUtil.copy(trader, dto);
				
				dto.setTraderName(sel[9]==null?"":sel[9].toString());
				dto.setTraderNameEn(sel[10]==null?"":sel[10].toString());
				dto.setTraderCategory(sel[11]==null?"":sel[11].toString());
				
				if((dto.getTraderCategory() != null) && (!dto.getTraderCategory().isEmpty()))
				{
					dto.setTraderCategoryName(
							TraderCategory.getTraderCategory(
								dto.getTraderType()
								, dto.getTraderCategory()
							).getMeaning()
					);
				}
				
				
				dto.setPersonId(Long.valueOf(sel[12].toString()));
				
				Person person = (Person)this.personDAO.findByPrimaryKey(dto.getPersonId());
				ObjectUtil.copy(person, dto);
				
				dto.setPersonType(sel[13]==null?"":sel[13].toString());
				dto.setPrefixName(sel[14]==null?"":sel[14].toString());
				dto.setFirstName(sel[15]==null?"":sel[15].toString());
				dto.setLastName(sel[16]==null?"":sel[16].toString());
				dto.setPostfixName(sel[17]==null?"":sel[17].toString());
				
				
				if(PersonType.CORPORATE.getStatus().equals(dto.getPersonType()))
				{
					String traderOwnerName = dto.getPrefixName() + dto.getFirstName() + dto.getPostfixName();
					dto.setTraderOwnerName(traderOwnerName);
				}
				else
				{
					String traderOwnerName = dto.getPrefixName() + dto.getFirstName() + " " + dto.getLastName();
					dto.setTraderOwnerName(traderOwnerName);
					
					if(!TraderType.TOUR_COMPANIES.getStatus().equals(dto.getTraderType()))
					{
						dto.setTraderName(traderOwnerName);
					}
					
				}
				
				if(sel[18] != null)
				{
					dto.setPrefixId(Long.valueOf(sel[18].toString()));
				}
				
				
				
				if(sel[19] != null)
				{
					dto.setPostfixId(Long.valueOf(sel[19].toString()));
				}
				
				
				if(sel[20] != null)
				{
					dto.setAmphurId(Long.valueOf(sel[20].toString()));
				}
				if(sel[21] != null)
				{
					dto.setTaxAmphurId(Long.valueOf(sel[21].toString()));
				}
				if(sel[22] != null)
				{
					dto.setProvinceId(Long.valueOf(sel[22].toString()));
				}
				if(sel[23] != null)
				{
					dto.setTaxProvinceId(Long.valueOf(sel[23].toString()));
				}
				
				if(sel[24] != null)
				{
					dto.setTraderGuideId(Long.valueOf(sel[24].toString()));
				}
				
				dto.setPronunciationName(sel[25]==null?"":sel[25].toString());
			    dto.setLicenseGuideNo(sel[26]==null?"":sel[26].toString());
			    dto.setPersonType(sel[27]==null?"":sel[27].toString());
			    dto.setIdentityNo(sel[28]==null?"":sel[28].toString());
			    dto.setCommitteeName1(sel[29]==null?"":sel[29].toString());
			    dto.setCommitteeName2(sel[30]==null?"":sel[30].toString());
			    dto.setCommitteeNameSign(sel[31]==null?"":sel[31].toString());
			    dto.setPassportNo(sel[32]==null?"":sel[32].toString());
			    dto.setFirstNameEn(sel[33]==null?"":sel[33].toString());
			    dto.setLastNameEn(sel[34]==null?"":sel[34].toString());
			    dto.setGender(sel[35]==null?"":sel[35].toString());
			    dto.setPersonNationality(sel[36]==null?"":sel[36].toString());


			    if(sel[37] != null)
				{
					
					dto.setBirthDate(ft.format((Date)sel[37]));
				}
			    
			    dto.setAgeYear(sel[38]==null?0:Integer.valueOf(sel[38].toString()));
			    if(sel[39] != null)
				{
					
					dto.setIdentityNoExpire(ft.format((Date)sel[39]));
				}
			   
			    dto.setCorporateType(sel[40]==null?"":sel[40].toString());
			    dto.setTaxIdentityNo(sel[41]==null?"":sel[41].toString());
			    if(sel[42] != null)
				{
					
					dto.setIdentityDate(ft.format((Date)sel[42]));
				}
			    dto.setTraderRecordStatus(sel[43]==null?"":sel[43].toString());
			    
			    dto.setAmphurName(sel[44]==null?"":sel[44].toString());
			    dto.setTaxAmphurName(sel[45]==null?"":sel[45].toString());
			    dto.setProvinceName(sel[46]==null?"":sel[46].toString());
			    dto.setTaxProvinceName(sel[47]==null?"":sel[47].toString());
			    dto.setPrefixNameEn(sel[48]==null?"":sel[48].toString());
			    dto.setLicenseNo(sel[49]==null?"":sel[49].toString());
		
				
				
				if(TraderType.TOUR_COMPANIES.getStatus().equals(dto.getTraderType()))
				{
					tourCompaniesService.getRegistration(reg, dto, user);
				}
				
				if(TraderType.GUIDE.getStatus().equals(dto.getTraderType()))
				{
					guideService.getRegistration(reg, dto, user);
				}
				
				if(TraderType.LEADER.getStatus().equals(dto.getTraderType()))
				{
					tourLeaderService.getRegistration(reg, dto, user);
				}
				
				dto.setAuthorityName(user.getUserData()!=null?user.getUserData().getUserFullName():"reg");
				
				List<RegisterProgress> listprogress =  registerProgressDAO.findRegisterProgressbyRegistration(reg.getRegId(), ProgressStatus.GET_IT.getStatus());
				
				if(!listprogress.isEmpty())
				{
					RegisterProgress progress = listprogress.get(0);
					
					if(progress.getAdmUser() != null)
					{
						AdmUser officer = progress.getAdmUser();
						MasPrefix mf = officer.getMasPrefix();
						
						String authorityName = mf==null?"":mf.getPrefixName() + officer.getUserName() +" "
								+officer.getUserLastname();
						
						dto.setAuthorityName(authorityName);
					}
				}
				
				//สามารถแก้ไขข้อมูลได้ ถ้า RegistrationProgress อยู่ที่ role ตัวเอง
				/**
				 * RECHECK
				 * SUPERVISOR
				 * MANAGER
				 * DIRECTOR
				 */
//				System.out.println("regProgress===" + regProgress);
//				System.out.println("regProgress===" + dto.getRegistrationProgress());
				 
				if(dto.getRegistrationProgress().equals(params.getRegistrationProgress()))
				{
					dto.setRoleAction("1");
					
					
					
//					if((this.REGISTER_USER.equals(reg.getCreateUser())) && (RoleStatus.DIRECTOR.getStatus().equals(dto.getRegistrationProgress())))
//					{
//						dto.setRoleColor("1");
//					}
//					
//					if((this.REGISTER_USER.equals(reg.getCreateUser())) && (RoleStatus.ACCOUNT.getStatus().equals(dto.getRegistrationProgress())))
//					{
//						dto.setRoleColor("1");
//					}
					
					
					//CHECK ว่าเป็นเรื่องดีกลับหรือไม่
					List<RegisterProgress> listBackStatus =  registerProgressDAO.findProgressBackStatusbyRegistration(reg.getRegId(), dto.getRegistrationProgress());
					if(!listBackStatus.isEmpty())
					{
						RegisterProgress role = listBackStatus.get(0);
						dto.setProgressStatus(role.getProgressStatus());
						dto.setProgressBackStatus(ProgressStatus.BACK.getStatus());
					}
					//check ว่ามีเป็น supviser และ manager ด้วยหรือไม่
					List<AdmGroup> listGroup = (List<AdmGroup>)this.admUserGroupDAO.findRole(user.getUserId(), RoleStatus.MANAGER.getStatus());
					
					if(!listGroup.isEmpty())
					{
						dto.setMultipleJobs(RoleStatus.MANAGER.getStatus());
					}
					
				}
				
				//ตรวจสอบว่ายื่น online หรือเปล่า
				List<DotTrFeeLicense> listFee = (List<DotTrFeeLicense>)this.dotTrFeeLicenseDAO.findTrFeeLicenseByRegId(reg.getRegId());
				if(CollectionUtils.isNotEmpty(listFee)){
					dto.setRoleColor("true");
				}
				
				
				list.add(dto);
				
				
			}

		}
			
		return list;
	}
	





	public List getTraderByLicenseNo(Object object, User user) throws Exception {

		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		RegistrationDTO params = (RegistrationDTO)object;
		
		List<Trader>  listTrader = (List<Trader>)this.traderDAO.findTraderByLicenseNo(params.getLicenseNo(), params.getTraderType(), RecordStatus.NORMAL.getStatus(), user.getUserData().getOrgId());
		if(!listTrader.isEmpty())
		{
			for(Trader trader: listTrader)
			{
				RegistrationDTO dto = new RegistrationDTO();
				ObjectUtil.copy(trader, dto);

				Person person = trader.getPerson();
				
				ObjectUtil.copy(person,dto);
				
				if(PersonType.CORPORATE.getStatus().equals(person.getPersonType()))
				{
					String postFixName = "";
					if(person.getMasPosfix() != null)
					{
						postFixName = person.getMasPosfix().getPostfixName();
					}
					String traderOwnerName = person.getMasPrefix()==null?"":person.getMasPrefix().getPrefixName() + person.getFirstName() + postFixName;
					dto.setTraderOwnerName(traderOwnerName);
				}
				else
				{
					String traderOwnerName = person.getMasPrefix()==null?"":person.getMasPrefix().getPrefixName() + person.getFirstName() + " " + person.getLastName();
					dto.setTraderOwnerName(traderOwnerName);
				}	
				
				dto.setTraderCategoryName(
						TraderCategory.getTraderCategory(
							dto.getTraderType()
							, dto.getTraderCategory()
						).getMeaning()
				);
			
				
				list.add(dto);
			}
			
		}
		
		return list;
	}
	public List getLicenseByLicenseNo(Object object, User user) throws Exception {

		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		RegistrationDTO params = (RegistrationDTO)object;
		
		List<Trader>  listTrader = (List<Trader>)this.traderDAO.findLicenseNoForRegistration(params.getLicenseNo(), params.getTraderType(), RecordStatus.NORMAL.getStatus());
		Organization geoLocation = (Organization)this.traderDAO.findGeoLocationByPrimaryKey(user.getUserData().getOrgId());
		if(!listTrader.isEmpty())
		{
			
				Trader trader = listTrader.get(0);
				RegistrationDTO dto = new RegistrationDTO();
				ObjectUtil.copy(trader, dto);

				Person person = trader.getPerson();
				
				ObjectUtil.copy(person,dto);
				
				if(PersonType.CORPORATE.getStatus().equals(person.getPersonType()))
				{
					String postFixName = "";
					if(person.getMasPosfix() != null)
					{
						postFixName = person.getMasPosfix().getPostfixName();
					}
					String traderOwnerName = person.getMasPrefix()==null?"":person.getMasPrefix().getPrefixName() + person.getFirstName() + postFixName;
					dto.setTraderOwnerName(traderOwnerName);
				}
				else
				{
					String traderOwnerName = person.getMasPrefix()==null?"":person.getMasPrefix().getPrefixName() + person.getFirstName() + " " + person.getLastName();
					dto.setTraderOwnerName(traderOwnerName);
				}	
				
				dto.setTraderCategoryName(
						TraderCategory.getTraderCategory(
							dto.getTraderType()
							, dto.getTraderCategory()
						).getMeaning()
				);
			
				
				list.add(dto);
				
				System.out.println("trader.getOrganization().getOrgId() : " +trader.getOrganization().getOrgId());
				System.out.println("user.getUserData().getOrgId() : " +user.getUserData().getOrgId());
				System.out.println("geoLocation.getOrganization().getOrgId() : " +geoLocation.getOrganization().getOrgId());
				System.out.println("dto.getTraderId() : " +dto.getTraderId());
				
				List<TraderAddress> listAddr = this.traderAddressDAO.findAllByTrader(dto.getTraderId()
						, TraderType.TOUR_COMPANIES.getStatus().equals(dto.getTraderType())?TraderAddressType.OFFICE_ADDRESS.getStatus():TraderAddressType.SHOP_ADDRESS.getStatus()
						, RecordStatus.NORMAL.getStatus());
				
				if(!listAddr.isEmpty()){
					TraderAddress addr = listAddr.get(0);
					
					if(addr.getMasProvince() != null){
						System.out.println("addr.getMasProvince() : " + addr.getMasProvince().getProvinceName());
					}else{
						System.out.println("addr.getMasProvince() : null");
					}
					if(addr.getMasProvince() != null){
						System.out.println("addr.getMasProvince().getOrgId() : " + addr.getMasProvince().getOrgId());
					}else{
						System.out.println("addr.getMasProvince() : null");
					}
					
					
					if(addr.getMasProvince() != null){
						
						if(user.getUserData().getOrgId() != addr.getMasProvince().getOrgId()){
							Organization orgi = (Organization)this.organizationDAO.findByPrimaryKey(addr.getMasProvince().getOrgId());
							throw new IllegalAccessException("ไม่สามารถทำรายการได้ เนื่องจาก ใบอนุญาตเลขที่ " + params.getLicenseNo() + " อยู่ในความรับผิดชอบของ " + orgi.getOrgName());
						}
					}
					else{
						if((trader.getOrganization().getOrgId() != user.getUserData().getOrgId())&& (trader.getOrganization().getOrgId() != geoLocation.getOrganization().getOrgId())){
							throw new IllegalAccessException("ไม่สามารถทำรายการได้ เนื่องจาก ใบอนุญาตเลขที่ " + params.getLicenseNo() + " ไม่อยู่ในความรับผิดชอบของสำนักงานท่าน");
						}	
					}
				}
				else{
					if((trader.getOrganization().getOrgId() != user.getUserData().getOrgId())&& (trader.getOrganization().getOrgId() != geoLocation.getOrganization().getOrgId())){
						throw new IllegalAccessException("ไม่สามารถทำรายการได้ เนื่องจาก ใบอนุญาตเลขที่ " + params.getLicenseNo() + " ไม่อยู่ในความรับผิดชอบของสำนักงานท่าน");
					}	
				}
		}
		
		return list;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void cancelAllRegistration(Object object, User user)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void verification(Object object, User user) throws Exception {

		if(!(object instanceof RegisterProgressDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้้");
		}
		
		RegisterProgressDTO param = (RegisterProgressDTO)object;
		
		Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(param.getRegId());
		
//		List<RegisterProgress> listProg = this.registerProgressDAO.findRoleProgressbyRegistration(reg.getRegId(), RoleStatus.KEY.getStatus());
//		RegisterProgress progress = null;
//		if(!listProg.isEmpty())
//		{
//			progress = listProg.get(0);
//		}
//		else
//		{
//			progress =  new RegisterProgress();
//		}
		RegisterProgress progress =  new RegisterProgress();
		progress.setRegistration(reg);
		
		AdmUser  admUser = new AdmUser();
		admUser.setUserId(user.getUserId());
		progress.setAdmUser(admUser);
		progress.setProgress(RoleStatus.KEY.getStatus());
		progress.setProgressDate(Calendar.getInstance().getTime());
//		String positionName = user.getUserData().getPositionName()==null?"":user.getUserData().getPositionName();
		progress.setProgressDesc(RoleStatus.KEY.getMeaning()+" "+param.getProgressDesc()==null?"":param.getProgressDesc());
		progress.setProgressStatus(param.getProgressStatus());
		registerProgressDAO.insert(progress);
		
		if(ProgressStatus.ACCEPT.getStatus().equals(param.getProgressStatus()))
		{
			//get next role
			RegisterFlow flow = registerFlowDAO.findNextRole(RoleStatus.KEY.getStatus());
			
			//update registration next role
			reg.setRegistrationProgress(flow.getNextRole());
			reg.setLastUpdUser(user.getUserName());
			reg.setLastUpdDtm(new Date());
			
			//insert register progress next role waiting
			
			RegisterProgress nextProgress =  new RegisterProgress();
			nextProgress.setRegistration(reg);
			
//			AdmUser  admUser = new AdmUser();
//			admUser.setUserId(user.getUserId());
//			progress.setAdmUser(admUser);
			nextProgress.setProgress(flow.getNextRole());
			nextProgress.setProgressDate(Calendar.getInstance().getTime());
			nextProgress.setProgressDesc(RoleStatus.getMeaning(flow.getNextRole())+" "+ProgressStatus.WAITING.getMeaning());
			nextProgress.setProgressStatus(ProgressStatus.WAITING.getStatus());
			registerProgressDAO.insert(nextProgress);
			
			// TODO Notification to next role
		}
		else
		{
			//ยกเลิกการจดทะเบียน
			// TODO cancel registration
		}
		
		
		
	}


	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public Object verificationName(Object object, User user) throws Exception {
		
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		String check = null;
		String traderName = null;
		String traderNameEn = null;
		RegistrationDTO params = (RegistrationDTO)object;
		RegistrationDTO dto = new RegistrationDTO();
		if(params.getTraderName()!=null && !params.getTraderName().equals(""))
		{
			if(params.getTraderName()!=null && !params.getTraderName().equals(""))
			{
				traderName = params.getTraderName().trim().replaceAll(" ", "");
			}
		}else
		{
			if(params.getTraderNameEn()!=null && !params.getTraderNameEn().equals(""))
			{
				traderNameEn = params.getTraderNameEn().toUpperCase().trim().replaceAll(" ", "");
			}
		}
		//List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		List<Trader> listObj = (List<Trader>)this.traderDAO.findAllTraderCheckName(params);
		
		if(!listObj.isEmpty() && !listObj.equals(""))
		{
			RegistrationDTO dtoCheck = (RegistrationDTO)object;
			String tempTraderName = null;
			String tempTraderNameEn = null;
			for(Trader trader: listObj)
			{
				if(traderName!=null && !traderName.equals(""))
				{
					if(trader.getTraderName()!=null && !trader.getTraderName().equals(""))
					{
						tempTraderName = trader.getTraderName().trim().replaceAll(" ", "");
						System.out.println(traderName);
						System.out.println(tempTraderName);
						if(traderName.equals(tempTraderName))
						{
							dto.setCheck("false"); 
							break;
						}else{
							dto.setCheck("true");
						}
					}
				}else if(traderNameEn!=null && !traderNameEn.equals(""))
				{
					if(trader.getTraderNameEn()!=null && !trader.getTraderNameEn().equals(""))
					{
						tempTraderNameEn = trader.getTraderNameEn().toUpperCase().trim().replaceAll(" ", "");	
						System.out.println(traderNameEn);
						System.out.println(tempTraderNameEn);
						if(traderNameEn.equals(tempTraderNameEn))
						{
							dto.setCheck("false"); 
							break;
						}else{
							dto.setCheck("true");
						}
					}
				}
			}
		
		}
		
		return dto;

	}


	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public Object verificationIdentityNo(Object object, User user)
			throws Exception {
		
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		RegistrationDTO params = (RegistrationDTO)object;
		
		List<Object[]> listObjS = (List<Object[]>)this.registrationDAO.findCheckIdentityNoRegistrationPCMRevokeSuspend(params, user);
		//List<Object[]> listObjR = (List<Object[]>)this.registrationDAO.findCheckIdentityNoRegistrationPersonRevoke(params, user);
		RegistrationDTO dto = new RegistrationDTO();
		if(!listObjS.isEmpty() && listObjS!=null)
		{
			for(Object[] sel:listObjS)
			{
				if(sel[10]!=null)
				{
					dto.setCheck("true");
					break;
				}else
				{
					dto.setCheck("false");
				}
			}
		}
		
		/*if(!listObjR.isEmpty() && listObjR!=null)
		{
			for(Object[] sel:listObjR)
			{
				if(sel[10]!=null)
				{
					dto.setCheck("true");
					break;
				}else
				{
					dto.setCheck("false");
				}
			}
		}*/
		System.out.println(dto.getCheck());
		return dto;
	}


	
	public List verificationIdentityNoDetail(Object object, User user)
			throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		RegistrationDTO param = (RegistrationDTO)object;
		
		List<Object[]> listObjS = (List<Object[]>)this.registrationDAO.findCheckIdentityNoRegistrationPersonSuspend(param, user);
		//List<Object[]> listObjR = (List<Object[]>)this.registrationDAO.findCheckIdentityNoRegistrationPersonRevoke(param, user);

		
		
		return null;
	}

	//Oat Add 02/02/58
	public Object verificationIdentityNoTourleader(Object object, User user)throws Exception 
	{
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		RegistrationDTO params = (RegistrationDTO)object;
		
		List<Object[]> listObjS = (List<Object[]>)this.registrationDAO.findCheckIdentityNoTourleader(params, user);
		RegistrationDTO dto = new RegistrationDTO();
		if(!listObjS.isEmpty() && listObjS!=null)
		{
			if(listObjS.size() > 0)
			{
				dto.setCheck("true");
			}
			else
			{
				dto.setCheck("false");
			}
		}
		
		System.out.println(dto.getCheck());
		return dto;
	}
	//End Oat Add


	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void requestAdditionalDoc(Object object, User user) throws Exception {
		if(!(object instanceof RegisterProgressDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้้");
		}
		
		
		
		RegisterProgressDTO param = (RegisterProgressDTO)object;
		
		Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(param.getRegId());
		
		List<DotTrFeeLicense> listTrFee = this.dotTrFeeLicenseDAO.findTrFeeLicenseByRegId(param.getRegId());
		if(CollectionUtils.isEmpty(listTrFee)){
			throw new IllegalArgumentException("ไม่สามารถบันทึกการขอเอกสารเพิ่มเติมได้เนื่องจากไม่ใช่การยื่นแบบออนไลน์");
		}
		
		this.registerProgressDAO.executeUpdate("UPDATE DOT_TR_FEE_LICENSE SET LICENSE_FEE_STATUS = '4' ,LAST_UPD_USER = '"+user.getUserName()+"' ,LAST_UPD_DTM = GETDATE() WHERE REG_ID = " + reg.getRegId(), null);
		
		List<RegisterProgress> listProg = this.registerProgressDAO.findRoleProgressbyRegistration(reg.getRegId(), reg.getRegistrationProgress());
		
		Calendar cal = Calendar.getInstance();
	
		
		if(!listProg.isEmpty())
		{
			RegisterProgress progress =  listProg.get(0);
			
			
			AdmUser  admUser = new AdmUser();
			admUser.setUserId(user.getUserId());
			progress.setAdmUser(admUser);
			progress.setProgressDate(cal.getTime());
			progress.setProgressDesc(reg.getRegistrationProgress()+" "+param.getProgressDesc()==null?"":param.getProgressDesc());
			progress.setProgressStatus(param.getProgressStatus());
			progress.setLastUpdUser(user.getUserName());
			progress.setLastUpdDtm(new Date());
			registerProgressDAO.update(progress);
		}
		
		reg.setRegistrationProgress(RoleStatus.LIC_OWNER.getStatus());
		reg.setRecordStatus(RecordStatus.CANCEL.getStatus());
		reg.setLastUpdUser(user.getUserName());
		reg.setLastUpdDtm(new Date());
		
		RegisterProgress nextProgress =  new RegisterProgress();
		nextProgress.setRegistration(reg);
		
		nextProgress.setProgress(RoleStatus.LIC_OWNER.getStatus());
		nextProgress.setProgressDate(Calendar.getInstance().getTime());
		nextProgress.setProgressDesc(param.getProgressDesc());
		nextProgress.setProgressStatus(ProgressStatus.WAITING.getStatus());
		registerProgressDAO.insert(nextProgress);
		
		Trader trader = reg.getTrader();
		Person person = trader.getPerson();
		// if register on line send mail
		
		if(CollectionUtils.isNotEmpty(listTrFee)){

			StringBuffer mailContent = new StringBuffer(this.getResources().getProperty("inform.mail.request.doc"));
			
		
			String personName = "";
			if(PersonType.CORPORATE.getStatus().equals(person.getPersonType()))
			{					
				String traderOwnerName = person.getMasPrefix()==null?"":person.getMasPrefix().getPrefixName() + person.getFirstName() + (person.getMasPosfix()==null?"":person.getMasPosfix().getPostfixName());
				personName = "กรรมการผู้จัดการ " + traderOwnerName;
			}
			else
			{
				String traderOwnerName =  person.getMasPrefix()==null?"":person.getMasPrefix().getPrefixName() + person.getFirstName() + " " + person.getLastName();
				personName = traderOwnerName;
			}
			
			String email = "shaii.kanith@gmail.com";
			List<TraderAddress> listAddress = this.traderAddressDAO.findAllByTrader(trader.getTraderId(), TraderAddressType.OFFICE_ADDRESS.getStatus(), null);
			if(CollectionUtils.isNotEmpty(listAddress)){
				TraderAddress addr = listAddress.get(0);
				if(StringUtils.isNotEmpty(addr.getEmail())){
					email = addr.getEmail();
				}
			}
			
			String orgName = reg.getOrganization().getOrgFullName();
			String orgAddress = reg.getOrganization().getOrgAddress();
			
			String registrationType = "";
			if(StringUtils.isNotEmpty(reg.getRegistrationType())){
				registrationType = RegistrationType.getMeaning(trader.getTraderType(), reg.getRegistrationType());
			}
			registrationType += "ใบอนุญาต" + TraderType.getMeaning(trader.getTraderType());
			registrationType += " เลขที่คำขอ " + reg.getRegistrationNo();
			
			
			StringBuffer megContent = new StringBuffer();
			megContent.append(MessageFormat.format(mailContent.toString(), registrationType, personName, orgName,orgName + " " + orgAddress));
			
			MailService mailThred = new MailService( email, "ขอเอกสารเพิ่มเติม คำขอยื่น" + registrationType, megContent);
			mailThred.run();
		
		}
		
	}
	
	
	
	
	
}
