package sss.dot.tourism.service.registration;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.mas.MasAmphurDAO;
import sss.dot.tourism.dao.mas.MasProvinceDAO;
import sss.dot.tourism.dao.mas.MasTambolDAO;
import sss.dot.tourism.dao.registration.TraderAddressDAO;
import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.domain.Education;
import sss.dot.tourism.domain.MasAmphur;
import sss.dot.tourism.domain.MasProvince;
import sss.dot.tourism.domain.MasTambol;
import sss.dot.tourism.domain.Person;
import sss.dot.tourism.domain.Registration;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.domain.TraderAddress;
import sss.dot.tourism.dto.registration.TraderAddressDTO;
import sss.dot.tourism.dto.training.EducationDTO;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.RecordStatus;

import com.sss.aut.service.User;

@Repository("traderAddressService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class TraderAddressService implements ITraderAddressService{

	@Autowired
	private TraderAddressDAO traderAddressDAO;
	
	@Autowired
	private MasProvinceDAO masProvinceDAO;
	
	@Autowired
	private MasAmphurDAO masAmphurDAO;
	
	@Autowired
	private MasTambolDAO masTambolDAO;
	
	@Autowired
	TraderDAO traderDAO;
	
	public List getAll(Object object , User user) throws Exception {
		if(!(object instanceof TraderAddressDTO))
		{
			throw new IllegalArgumentException("object not instanceof TraderAddressDTO");
		}
		TraderAddressDTO param = (TraderAddressDTO)object;
		
		List<TraderAddressDTO> list = new ArrayList<TraderAddressDTO>();
		
		System.out.println("param.getTraderId() = "+param.getTraderId());
		
		Trader trader = (Trader) traderDAO.findByPrimaryKey(param.getTraderId());
		
		System.out.println("trader.getTraderId() = "+trader.getTraderId()+ " , "+trader.getRecordStatus());
		
		List<TraderAddress> listAll = this.traderAddressDAO.findAllByTrader(trader.getTraderId(), null, trader.getRecordStatus());
		
		
		
		if(!listAll.isEmpty())
		{
			for(TraderAddress addr: listAll)
			{
				TraderAddressDTO dto = new TraderAddressDTO();
				
				ObjectUtil.copy(addr, dto);
				ObjectUtil.copy(trader, dto);
				
				dto.setAddressNoEn(addr.getAddressNo()==null?"":addr.getAddressNo());
				dto.setFloorEn(addr.getFloor()==null?"":addr.getFloor());
				
				dto.setRoomNoEn(addr.getRoomNo()==null?"":addr.getRoomNo());
				dto.setMooEn(addr.getMoo()==null?"":addr.getMoo());
				dto.setSoi(addr.getSoi()==null?"":addr.getSoi());				
				dto.setSoiEn(addr.getSoiEn()==null?"":addr.getSoiEn());
				dto.setPostCodeEn(addr.getPostCode()==null?"":addr.getPostCode());
				dto.setTelephoneEn(addr.getTelephone()==null?"":addr.getTelephone());
				dto.setMobileNoEn(addr.getMobileNo()==null?"":addr.getMobileNo());
				dto.setFaxEn(addr.getFax()==null?"":addr.getFax());
				dto.setEmailEn(addr.getEmail()==null?"":addr.getEmail());
				dto.setWebsiteEn(addr.getWebsite()==null?"":addr.getWebsite());
				
				
				if(addr.getMasAmphur() != null)
				{
					dto.setAmphurId(addr.getMasAmphur().getAmphurId());
					dto.setAmphurIdEn(addr.getMasAmphur().getAmphurId());
					dto.setAmphurName(addr.getMasAmphur().getAmphurName());
					dto.setAmphurNameEn(addr.getMasAmphur().getAmphurNameEn());
				}
				if(addr.getMasProvince() != null)
				{
					dto.setProvinceId(addr.getMasProvince().getProvinceId());
					dto.setProvinceIdEn(addr.getMasProvince().getProvinceId());
					dto.setProvinceName(addr.getMasProvince().getProvinceName());
					dto.setProvinceNameEn(addr.getMasProvince().getProvinceNameEn());
				}
				if(addr.getTrader() != null)
				{
					dto.setTraderId(addr.getTrader().getTraderId());
				}
				if(addr.getMasTambol() != null)
				{
					dto.setTambolId(addr.getMasTambol().getTambolId());
					dto.setTambolIdEn(addr.getMasTambol().getTambolId());
					dto.setTambolName(addr.getMasTambol().getTambolName());
					dto.setTambolNameEn(addr.getMasTambol().getTambolNameEn());
				}
				
				
								
				list.add(dto);
			}
		}
		
		return list;
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List create(Object[] object , User user) throws Exception {
		List<TraderAddressDTO> list = new ArrayList<TraderAddressDTO>();
		for (Object obj : object) {
			TraderAddressDTO dto = (TraderAddressDTO) obj;
			
			TraderAddress addr = new TraderAddress();
			
			ObjectUtil.copy(dto, addr);
			
			if(dto.getProvinceId() > 0)
			{
				MasProvince province = new MasProvince();
				province.setProvinceId(dto.getProvinceId());
				
				addr.setMasProvince(province);
			}
			
			if(dto.getAmphurId() > 0)
			{
				MasAmphur amphur = new MasAmphur();
				amphur.setAmphurId(dto.getAmphurId());
				
				addr.setMasAmphur(amphur);
			}
			
			if(dto.getTambolId() > 0)
			{
				MasTambol tambol = new MasTambol();
				tambol.setTambolId(dto.getTambolId());
				
				addr.setMasTambol(tambol);
			}
			
			if(dto.getTraderId() > 0)
			{
				Trader trader = new Trader();
				trader.setTraderId(dto.getTraderId());
				
				addr.setTrader(trader);
			}
			
			if(dto.getPersonId() > 0)
			{
				Person person = new Person();
				person.setPersonId(dto.getPersonId());
				
				addr.setPerson(person);
			}
			
			addr.setRecordStatus("T");
			addr.setCreateUser(user.getUserName());
			addr.setCreateDtm(new Date());
			
			Long addressId = (Long) this.traderAddressDAO.insert(addr);
		
			dto.setAddressId(addressId.longValue());

			list.add(dto);
		}

		return list;
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void update(Object[] object , User user) throws Exception {
		
		for (Object obj : object) {
			TraderAddressDTO dto = (TraderAddressDTO) obj;
			TraderAddress addr = (TraderAddress)traderAddressDAO.findByPrimaryKey(dto.getAddressId());
			
			ObjectUtil.copy(dto, addr);
			
			addr.setRecordStatus(RecordStatus.TEMP.getStatus());
			
			if(dto.getProvinceId() > 0)
			{
				MasProvince province = new MasProvince();
				province.setProvinceId(dto.getProvinceId());
				
				addr.setMasProvince(province);
			}
			
			if(dto.getAmphurId() > 0)
			{
				MasAmphur amphur = new MasAmphur();
				amphur.setAmphurId(dto.getAmphurId());
				
				addr.setMasAmphur(amphur);
			}
			
			if(dto.getTambolId() > 0)
			{
				MasTambol tambol = new MasTambol();
				tambol.setTambolId(dto.getTambolId());
				
				addr.setMasTambol(tambol);
			}
			
			if(dto.getTraderId() > 0)
			{
				Trader trader = new Trader();
				trader.setTraderId(dto.getTraderId());
				
				addr.setTrader(trader);
			}
			
			if(dto.getPersonId() > 0)
			{
				Person person = new Person();
				person.setPersonId(dto.getPersonId());
				
				addr.setPerson(person);
			}
			addr.setLastUpdUser(user.getUserName());
			
			traderAddressDAO.update(addr);
		}
		
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(Object[] object , User user) throws Exception {
		
		for (Object obj : object) {
			TraderAddressDTO dto = (TraderAddressDTO) obj;
			TraderAddress addr = (TraderAddress)traderAddressDAO.findByPrimaryKey(dto.getAddressId());
			traderAddressDAO.delete(addr);
			
		}
		
	}

}
