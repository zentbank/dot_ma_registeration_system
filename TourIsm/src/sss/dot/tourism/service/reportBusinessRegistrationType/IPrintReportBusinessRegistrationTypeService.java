package sss.dot.tourism.service.reportBusinessRegistrationType;

import java.util.Map;

import com.sss.aut.service.User;

public interface IPrintReportBusinessRegistrationTypeService {
	public Map<String,?> getPrintReportBusinessRegistrationType(Object obj,User user) throws Exception;
}
