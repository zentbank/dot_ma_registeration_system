package sss.dot.tourism.service.reportBusinessRegistrationType;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sss.aut.service.User;

import sss.dot.tourism.dao.report.ReportDAO;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.RegistrationType;

@Repository("printReportBusinessRegistrationTypeService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PrintReportBusinessRegistrationTypeService implements IPrintReportBusinessRegistrationTypeService{
	@Autowired
	ReportDAO reportDAO;
	
	@Override
	public Map<String, ?> getPrintReportBusinessRegistrationType(Object obj, User user) throws Exception {
		// TODO Auto-generated method stub
		
		if(!(obj instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถออกรายงานได้");
		}
		
		DateFormat ft = DateUtils.getDisplayDateFormatTH();
		
		Date d = new Date();
		
		String date = ft.format(d);
		
		RegistrationDTO param = (RegistrationDTO)obj;
		
		Map model = new HashMap();
		List<RegistrationDTO> listBusinessRegistrationType = new ArrayList();
		
		List<Object[]> list = (List<Object[]>)this.reportDAO.findSumBusinessRegistrationType(param);
		
		if(!list.isEmpty()){
			for(Object[] sel :list){
				RegistrationDTO dto = new RegistrationDTO();
				if(sel[0] !=null){
					dto.setBusinessName(RegistrationType.getMeaning("B", sel[0].toString()));
				}else{
					dto.setBusinessName("");
				}
				
				if(sel[1] !=null){
					dto.setSumChange(new BigDecimal(sel[1].toString()));
				}else
				{
					dto.setSumChange(new BigDecimal(0));
				}
				listBusinessRegistrationType.add(dto);
			}
		}
		
		model.put("listBusinessRegistrationType", listBusinessRegistrationType);
		model.put("date", date);
		return model;
	}

}
