package sss.dot.tourism.service.reportMasdeactypeBusiness;

import java.util.Map;

import com.sss.aut.service.User;

public interface IPrintReportMasDeactypeBusinessService {
     public Map<String, ?> getPrintReportMasdeactypeBusiness(Object object, User user) throws Exception;
}
