package sss.dot.tourism.service.reportlanguageguide;

import java.util.Map;

import com.sss.aut.service.User;

public interface IPrintReportLanguageGuideService {
	public Map<String, ?> getPrintReportLanguageGuide(Object object, User user) throws Exception;
}
