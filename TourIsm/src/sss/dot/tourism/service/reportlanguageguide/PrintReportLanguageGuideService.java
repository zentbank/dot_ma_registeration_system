package sss.dot.tourism.service.reportlanguageguide;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.mas.CountryDAO;
import sss.dot.tourism.dao.registration.GuaranteeDAO;
import sss.dot.tourism.dao.registration.ReceiptDAO;
import sss.dot.tourism.dao.registration.ReceiptMapRegistrationDAO;
import sss.dot.tourism.dao.report.ReportDAO;
import sss.dot.tourism.domain.Country;
import sss.dot.tourism.dto.registration.GuaranteeDTO;
import sss.dot.tourism.dto.registration.ReceiptDTO;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.ThaiBahtUtil;

import com.sss.aut.service.User;

@Repository("printReportLanguageGuideService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PrintReportLanguageGuideService implements IPrintReportLanguageGuideService{
	@Autowired
	ReportDAO reportDAO;
	@Autowired
	CountryDAO countryDAO;

	
	public Map<String, ?> getPrintReportLanguageGuide(Object object, User user)
			throws Exception {
		
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถออกรายงานได้");
		}
		Map model = new HashMap();
		long zero = 0;
		List<RegistrationDTO> listLanguageGuide = new ArrayList();
		RegistrationDTO dto =(RegistrationDTO)object;
		
		if(dto.getCountryId() == 0){
			
			List<Country> countryList = (List<Country>)this.countryDAO.findAllLanguage();
			
			for(Country country : countryList)
			{
				RegistrationDTO registrationDTO = new RegistrationDTO();
				dto.setCountryId(country.getCountryId());
				
				registrationDTO.setLanguage(country.getLanguage());
				
				List<Object[]> list = (List<Object[]>)this.reportDAO.findSumLanguageGuide(dto);
				
				if(!list.isEmpty()){
					for(Object[] sel: list){
						
						if(sel[1] !=null){
							registrationDTO.setSumGuide(new BigDecimal(sel[1].toString()));
						}else{
							registrationDTO.setSumGuide(new BigDecimal(0));
						}
					} 
				}
				
				listLanguageGuide.add(registrationDTO);
			}
		}else{			
			RegistrationDTO registrationDTO = new RegistrationDTO();
			List<Country> countryList = (List<Country>)this.countryDAO.findAllLanguageById(dto.getCountryId());
			
			Country country = countryList.get(0);
			registrationDTO.setLanguage(country.getLanguage());
			
			List<Object[]> list = (List<Object[]>)this.reportDAO.findSumLanguageGuide(dto);
			if(!list.isEmpty()){
				for(Object[] sel: list){
					
					if(sel[1] !=null){
						registrationDTO.setSumGuide(new BigDecimal(sel[1].toString()));
					}else{
						registrationDTO.setSumGuide(new BigDecimal(0));
					}
				} 
			}
			
			listLanguageGuide.add(registrationDTO);
		}
		
		model.put("listLanguageGuide", listLanguageGuide);
		
		return model;
	}

}
