package sss.dot.tourism.service.reportnewbusiness;

import java.util.Map;

import com.sss.aut.service.User;

public interface IPrintReportNewBusinessService {
     public Map<String, ?> getPrintReportNewBusiness(Object object, User user) throws Exception;
}
