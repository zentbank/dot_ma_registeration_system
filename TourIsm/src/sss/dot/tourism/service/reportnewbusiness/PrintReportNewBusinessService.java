package sss.dot.tourism.service.reportnewbusiness;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sss.aut.service.User;

import sss.dot.tourism.dao.mas.MasProvinceDAO;
import sss.dot.tourism.dao.report.ReportDAO;
import sss.dot.tourism.domain.MasProvince;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.util.DateUtils;

@Repository("printReportNewBusinessService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PrintReportNewBusinessService implements IPrintReportNewBusinessService{

	@Autowired
	ReportDAO reportDAO;
	@Autowired
	MasProvinceDAO masProvinceDAO;
	
	@Override
	public Map<String, ?> getPrintReportNewBusiness(Object object, User user) throws Exception {
		// TODO Auto-generated method stub
		
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถออกรายงานได้");
		}
		
		DateFormat ft = DateUtils.getDisplayDateFormatTH();
		
		Date d = new Date();
		
		String date = ft.format(d);
		List<RegistrationDTO> newdetail = new ArrayList();
		RegistrationDTO dto1 =(RegistrationDTO)object;
		
		Map model = new HashMap<>();
		DateFormat df = DateUtils.getProcessDateFormatThai();

				List<Object[]> list = (List<Object[]>)this.reportDAO.findNewBusinessLicenseDetail(dto1);
				
				if(!list.isEmpty())
				{
					for(Object[] license: list)
					{
						RegistrationDTO dto = new RegistrationDTO();
						
						dto.setLicenseNo(license[7]==null?"":license[7].toString());
						dto.setTraderName(license[0]==null?"":license[0].toString());
						dto.setTraderNameEn(license[8]==null?"":license[8].toString());
						dto.setTraderCategory(license[1]==null?"":license[1].toString());
						
						
						if(license[2] != null)
						{
							dto.setEffectiveDate(df.format(license[2]));
						}
						if(license[3] != null)
						{
							dto.setExpireDate(df.format(license[3]));
						}
						
						dto.setTraderAddress(license[4]==null?"":license[4].toString());
						dto.setMobileNo(license[5]==null?"":license[5].toString());
						dto.setTelephone(license[6]==null?"":license[6].toString());
						
						dto.setEmail(license[9]==null?"":license[9].toString());
						
						//sek add
						dto.setBuildingName(license[10]==null?"":license[10].toString());
						dto.setFloor(license[11]==null?"":license[11].toString());
						dto.setMoo(license[12]==null?"":license[12].toString());
						dto.setSoi(license[13]==null?"":license[13].toString());
						dto.setRoadName(license[14]==null?"":license[14].toString());
						dto.setTambolName(license[15]==null?"":license[15].toString());
						dto.setAmphurName(license[16]==null?"":license[16].toString());
						dto.setProvinceName(license[17]==null?"":license[17].toString());
						dto.setPostCode(license[18]==null?"":license[18].toString());
						//
						
						newdetail.add(dto);
	
					}
					
					model.put("newdetail", newdetail);
				}	
				model.put("date", date);
		return model;
	}

}
