package sss.dot.tourism.service.reportprovincebusiness;

import java.util.Map;

import com.sss.aut.service.User;

public interface IPrintReportProvinceBusinessService {
     public Map<String, ?> getPrintReportProvinceBusiness(Object object, User user) throws Exception;
}
