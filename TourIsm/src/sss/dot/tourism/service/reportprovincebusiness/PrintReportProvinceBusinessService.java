package sss.dot.tourism.service.reportprovincebusiness;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sss.aut.service.User;

import sss.dot.tourism.dao.mas.MasProvinceDAO;
import sss.dot.tourism.dao.report.ReportDAO;
import sss.dot.tourism.domain.MasProvince;
import sss.dot.tourism.dto.registration.RegistrationDTO;

@Repository("printReportProvinceBusinessService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PrintReportProvinceBusinessService implements IPrintReportProvinceBusinessService{

	@Autowired
	ReportDAO reportDAO;
	@Autowired
	MasProvinceDAO masProvinceDAO;
	
	@Override
	public Map<String, ?> getPrintReportProvinceBusiness(Object object, User user) throws Exception {
		// TODO Auto-generated method stub
		
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถออกรายงานได้");
		}
		
		long zero = 0;
		List<RegistrationDTO> listProvinceBusiness = new ArrayList();
		RegistrationDTO dto =(RegistrationDTO)object;
		
		Map model = new HashMap<>();
		
		if(dto.getProvinceId() == 0){
//			List<MasProvince> masProvinces = (List<MasProvince>)this.masProvinceDAO.findAll();

				List<Object[]> list = (List<Object[]>)this.reportDAO.findSumProvinceBusiness(dto);
				
				if(!list.isEmpty()){
					for(Object[] sel : list){
						RegistrationDTO registrationDTO = new RegistrationDTO();
						
						if(sel[0] !=null){
							registrationDTO.setProvinceName(sel[0].toString());
						}else{
							registrationDTO.setProvinceName("");
						}
						
						if(sel[1] !=null){
							registrationDTO.setSumProvince(new BigDecimal(sel[1].toString()));
						}else{
							registrationDTO.setSumProvince(new BigDecimal(0));
						}
					
						listProvinceBusiness.add(registrationDTO);
					}
				}
			
		}else{
			MasProvince masProvinces = (MasProvince)this.masProvinceDAO.findByPrimaryKey(dto.getProvinceId());
			
				RegistrationDTO registrationDTO = new RegistrationDTO();
				registrationDTO.setProvinceName(masProvinces.getProvinceName());
				
				List<Object[]> list = (List<Object[]>)this.reportDAO.findSumProvinceBusiness(dto);
				
				if(!list.isEmpty()){
					for(Object[] sel : list){
						
						if(sel[1] !=null){
							registrationDTO.setSumProvince(new BigDecimal(sel[1].toString()));
						}else{
							registrationDTO.setSumProvince(new BigDecimal(0));
						}
					}
				}
				listProvinceBusiness.add(registrationDTO);
			
		}
	
		model.put("listProvinceBusiness",listProvinceBusiness);
		return model;
	}

}
