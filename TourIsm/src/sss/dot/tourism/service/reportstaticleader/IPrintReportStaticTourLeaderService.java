package sss.dot.tourism.service.reportstaticleader;

import java.util.Map;

import com.sss.aut.service.User;

public interface IPrintReportStaticTourLeaderService {
	public Map<String,?> getPrintReportStaticTourLeader(Object obj,User user) throws Exception;
}
