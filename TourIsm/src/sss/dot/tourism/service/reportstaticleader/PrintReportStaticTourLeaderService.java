package sss.dot.tourism.service.reportstaticleader;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sss.aut.service.User;

import sss.dot.tourism.dao.report.ReportDAO;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.TraderType;

@Repository("printReportStaticTourLeaderService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PrintReportStaticTourLeaderService implements IPrintReportStaticTourLeaderService{
	@Autowired
	ReportDAO reportDAO;
	
	@Override
	public Map<String, ?> getPrintReportStaticTourLeader(Object obj, User user) throws Exception {
		// TODO Auto-generated method stub
		
		if(!(obj instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถออกรายงานได้");
		}
		DateFormat ft = DateUtils.getDisplayDateFormatTH();
		
		Date d = new Date();
		
		String date = ft.format(d);
		
		RegistrationDTO param = (RegistrationDTO)obj;
		
		Map model = new HashMap();
		List<RegistrationDTO> listStaticTourLeader = new ArrayList();
		
		List<Object[]> list = (List<Object[]>)this.reportDAO.findSumStaticTourLeader(param);
		
		
		if(!list.isEmpty()){
			for(Object[] sel : list){
				RegistrationDTO dto = new RegistrationDTO();
				
				if(sel[0] !=null){
					dto.setTourLeaderName(TraderType.getMeaning(sel[0].toString()));
				}
				
				if(sel[1] !=null){
					dto.setSumStaticTourLeader(new BigDecimal(sel[1].toString()));
				}
				
				listStaticTourLeader.add(dto);
			}
		}
 		
		model.put("listStaticTourLeader", listStaticTourLeader);
		model.put("date", date);
		return model;
	}

}
