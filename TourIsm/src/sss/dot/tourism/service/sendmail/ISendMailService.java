package sss.dot.tourism.service.sendmail;

import sss.dot.tourism.dto.complaint.ComplaintLicenseDTO;

public interface ISendMailService {
	public void SendMailExpireDate() throws Exception;
	public void SendMail(long id, String type,long traderId) throws Exception;
	public void SendMailComplaint(ComplaintLicenseDTO param) throws Exception;
}
