package sss.dot.tourism.service.sendmail;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.TimeZone;

import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.complaint.ComplaintLicenseDAO;
import sss.dot.tourism.dao.punishment.PunishmentEvidenceDAO;
import sss.dot.tourism.dao.punishment.RevokeAlertDAO;
import sss.dot.tourism.dao.punishment.SuspensionAlertDAO;
import sss.dot.tourism.dao.punishment.SuspensionLicenseDAO;
import sss.dot.tourism.dao.registration.PersonDAO;
import sss.dot.tourism.dao.registration.TraderAddressDAO;
import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.domain.PunishmentEvidence;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.domain.TraderAddress;
import sss.dot.tourism.dto.complaint.ComplaintLicenseDTO;
import sss.dot.tourism.dto.punishment.SuspensionAlertDTO;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.util.ConstantUtil;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.RecordStatus;
import sss.dot.tourism.util.TraderCategory;
import sss.dot.tourism.util.TraderType;
//import org.apache.velocity.app.VelocityEngine;


//@Component("sendMailService")
@Repository("sendMailService")
public class SendMailService implements ISendMailService{
	
	@Autowired
	TraderDAO traderDAO;
	@Autowired
	PersonDAO personDAO;
	@Autowired
	TraderAddressDAO traderAddressDAO;
	@Autowired
	SuspensionAlertDAO suspensionAlertDAO;
	@Autowired
	SuspensionLicenseDAO suspensionLicenseDAO;
	@Autowired
	RevokeAlertDAO revokeAlertDAO;
	@Autowired
	ComplaintLicenseDAO complaintLicenseDAO;
	@Autowired
	PunishmentEvidenceDAO punishmentEvidenceDAO;
	
// Example set up cron	
//	0 0 12 * * ?	Fire at 12:00 PM (noon) every day
//	0 15 10 ? * *	Fire at 10:15 AM every day
//	0 15 10 * * ?	Fire at 10:15 AM every day
//	0 15 10 * * ? *	Fire at 10:15 AM every day
//	0 15 10 * * ? 2005	Fire at 10:15 AM every day during the year 2005
//	0 * 14 * * ?	Fire every minute starting at 2:00 PM and ending at 2:59 PM, every day
//	0 0/5 14 * * ?	Fire every 5 minutes starting at 2:00 PM and ending at 2:55 PM, every day
//	0 0/5 14,18 * * ?	Fire every 5 minutes starting at 2:00 PM and ending at 2:55 PM, AND fire every 5 minutes starting at 6:00 PM and ending at 6:55 PM, every day
//	0 0-5 14 * * ?	Fire every minute starting at 2:00 PM and ending at 2:05 PM, every day
//	0 10,44 14 ? 3 WED	Fire at 2:10 PM and at 2:44 PM every Wednesday in the month of March
//	0 15 10 ? * MON-FRI	Fire at 10:15 AM every Monday, Tuesday, Wednesday, Thursday and Friday
//	0 15 10 15 * ?	Fire at 10:15 AM on the 15th day of every month
//	0 15 10 L * ?	Fire at 10:15 AM on the last day of every month
//	0 15 10 ? * 6L	Fire at 10:15 AM on the last Friday of every month
//	0 15 10 ? * 6L	Fire at 10:15 AM on the last Friday of every month
//	0 15 10 ? * 6L 2002-2005	Fire at 10:15 AM on every last friday of every month during the years 2002, 2003, 2004, and 2005
//	0 15 10 ? * 6#3	Fire at 10:15 AM on the third Friday of every month
//	0 0 12 1/5 * ?	Fire at 12 PM (noon) every 5 days every month, starting on the first day of the month
//	0 11 11 11 11 ?	Fire every November 11 at 11:11 AM
	

	@Scheduled(cron="0 0 4 * * ?") //ที่เวลาตี 4 ของทุกๆ วัน ให้ทำงานที่ method นี้Expression	Means
	public void SendMailExpireDate() throws Exception {
		System.out.println("INNNNN");
		RegistrationDTO param = new RegistrationDTO();
		SuspensionAlertDTO params = new SuspensionAlertDTO();
		String emailOffice = "";
		String emailShop ="";
		List<Trader> listTrader = (List<Trader>)this.traderDAO.findAllByTraderType(param);
		System.out.println(listTrader.size());
		try
		{
		if(!listTrader.isEmpty() && listTrader!=null)
		{
			for(Trader trader : listTrader)
			{
				RegistrationDTO dto = new RegistrationDTO();
				DateFormat ft = DateUtils.getDisplayDateFormatTH();
				params.setTraderId(trader.getTraderId());
				dto.setTraderId(trader.getTraderId());
				params.setTraderType(trader.getTraderType());
				System.out.println(params.getTraderId());
				params.setLicenseNo(trader.getLicenseNo());
				List<TraderAddress> listTraderAddress =  (List<TraderAddress>)this.traderAddressDAO.findAllEmaiByTrader(params.getTraderId(),RecordStatus.NORMAL.getStatus());
				if(!listTraderAddress.isEmpty() && listTraderAddress !=null)
				{
					for(TraderAddress traderAddress : listTraderAddress)
					{
						emailOffice = traderAddress.getEmail();
						System.out.println(traderAddress.getTrader().getTraderId());
						System.out.println(traderAddress.getEmail());
						params.setEmail(traderAddress.getEmail());
				
						Date date = new Date();
						String today = ft.format(date);
						
						
						String expireDate = ft.format(trader.getExpireDate());
						dto.setExpireDate(expireDate);
						
						long diff =  DateUtils.DateDiff(trader.getExpireDate(), date);
						if(!emailOffice.equals(emailShop))
						{
							if(diff>=1800)
							{
								params.setLicenseNo(trader.getLicenseNo());
								System.out.println(params.getLicenseNo());
								System.out.println(trader.getPerson().getPersonId());
								dto.setPersonId(trader.getPerson().getPersonId());
								List<Object[]> listObj = (List<Object[]>)this.traderDAO.findPersonByTraderId(dto);
								if(!listObj.isEmpty() && listObj!=null)
								{
									for(Object[] sel : listObj)
									{	
										if(sel[2] !=null)
										{
											params.setPersonType(sel[2].toString());
										}
										if(sel[3] !=null)
										{
											params.setPrefixName(sel[3].toString());
										}
										if(sel[4] !=null)
										{
											params.setPostfixName(sel[4].toString());
										}
										if(sel[5] !=null)
										{
											params.setFirstName(sel[5].toString());
										}
										if(sel[6] !=null)
										{
											params.setLastName(sel[6].toString());
										}
										if(params.getPersonType().equals("I") && params.getPersonType()!=null)
										{
											params.setTraderOwnerName(params.getPrefixName()+" "+params.getFirstName()+" "+params.getLastName());
										}else if(params.getPersonType().equals("C") && params.getPersonType()!=null)
										{
											params.setTraderOwnerName(params.getPrefixName()+" "+params.getFirstName()+" "+params.getPostfixName());
										}	
										System.out.println(params.getTraderOwnerName());
									}
								}
								if(trader.getTraderType()!=null && !trader.getTraderType().isEmpty())
								{
									if(trader.getTraderCategory()!=null && !trader.getTraderCategory().isEmpty())
									{
										params.setTraderType(TraderType.getMeaning(trader.getTraderType())+" ประเภท "+TraderCategory.getMeaning(trader.getTraderType(), trader.getTraderCategory()));
									}else
									{
										params.setTraderType(TraderType.getMeaning(trader.getTraderType()));
									}
								}
								params.setAlertType("<h2>"+"ใบอนุญาตจะหมดอายุในอีก 120 วัน"+" ณ วันที่ "+expireDate+"</h2>");
								params.setEmailAlertStatus("<a href='https://www.google.co.th'>คลิกที่นี่เพื่อยืนยัน</a>");
								this.sendMailAddress(params,"D");
							}else if(diff==60)
							{
								params.setLicenseNo(trader.getLicenseNo());
								dto.setPersonId(trader.getPerson().getPersonId());
								List<Object[]> listObj = (List<Object[]>)this.traderDAO.findPersonByTraderId(dto);
								if(!listObj.isEmpty() && listObj!=null)
								{
									for(Object[] sel : listObj)
									{	
										if(sel[2] !=null)
										{
											params.setPersonType(sel[2].toString());
										}
										if(sel[3] !=null)
										{
											params.setPrefixName(sel[3].toString());
										}
										if(sel[4] !=null)
										{
											params.setPostfixName(sel[4].toString());
										}
										if(sel[5] !=null)
										{
											params.setFirstName(sel[5].toString());
										}
										if(sel[6] !=null)
										{
											params.setLastName(sel[6].toString());
										}
										if(params.getPersonType().equals("I") && params.getPersonType()!=null)
										{
											params.setTraderOwnerName(params.getPrefixName()+" "+params.getFirstName()+" "+params.getLastName());
										}else if(params.getPersonType().equals("C") && params.getPersonType()!=null)
										{
											params.setTraderOwnerName(params.getPrefixName()+" "+params.getFirstName()+" "+params.getPostfixName());
										}	
										System.out.println(params.getTraderOwnerName());
									}
								}
								if(trader.getTraderType()!=null && !trader.getTraderType().isEmpty())
								{
									if(trader.getTraderCategory()!=null && !trader.getTraderCategory().isEmpty())
									{
										params.setTraderType(TraderType.getMeaning(trader.getTraderType())+" ประเภท "+TraderCategory.getMeaning(trader.getTraderType(), trader.getTraderCategory()));
									}else
									{
										params.setTraderType(TraderType.getMeaning(trader.getTraderType()));
									}
								}
								params.setAlertType("<h2>"+"ใบอนุญาตจะหมดอายุในอีก 60 วัน"+" ณ วันที่ "+expireDate+"</h2>");
								params.setEmailAlertStatus("<a href='https://www.google.co.th'>คลิกที่นี่เพื่อยืนยัน</a>");
								this.sendMailAddress(params,"D");
							}else if(diff==30)
							{
								params.setLicenseNo(trader.getLicenseNo());
								dto.setPersonId(trader.getPerson().getPersonId());
								List<Object[]> listObj = (List<Object[]>)this.traderDAO.findPersonByTraderId(dto);
								if(!listObj.isEmpty() && listObj!=null)
								{
									for(Object[] sel : listObj)
									{	
										if(sel[2] !=null)
										{
											params.setPersonType(sel[2].toString());
										}
										if(sel[3] !=null)
										{
											params.setPrefixName(sel[3].toString());
										}
										if(sel[4] !=null)
										{
											params.setPostfixName(sel[4].toString());
										}
										if(sel[5] !=null)
										{
											params.setFirstName(sel[5].toString());
										}
										if(sel[6] !=null)
										{
											params.setLastName(sel[6].toString());
										}
										if(params.getPersonType().equals("I") && params.getPersonType()!=null)
										{
											params.setTraderOwnerName(params.getPrefixName()+" "+params.getFirstName()+" "+params.getLastName());
										}else if(params.getPersonType().equals("C") && params.getPersonType()!=null)
										{
											params.setTraderOwnerName(params.getPrefixName()+" "+params.getFirstName()+" "+params.getPostfixName());
										}	
										System.out.println(params.getTraderOwnerName());
									}
								}
								if(trader.getTraderType()!=null && !trader.getTraderType().isEmpty())
								{
									if(trader.getTraderCategory()!=null && !trader.getTraderCategory().isEmpty())
									{
										params.setTraderType(TraderType.getMeaning(trader.getTraderType())+" ประเภท "+TraderCategory.getMeaning(trader.getTraderType(), trader.getTraderCategory()));
									}else
									{
										params.setTraderType(TraderType.getMeaning(trader.getTraderType()));
									}
								}
								params.setAlertType("<h2>"+"ใบอนุญาตจะหมดอายุในอีก 30 วัน"+" ณ วันที่ "+expireDate+"</h2>");
								params.setEmailAlertStatus("<a href='https://www.google.co.th'>คลิกที่นี่เพื่อยืนยัน</a>");
								this.sendMailAddress(params,"D");
							}
						}
						emailShop =  emailOffice;
					}
				}else{
					System.out.println("NO SEND MAIL");
				}
				emailShop="";
			    emailOffice="";
			}
		}
	}catch(Exception e)
	{
		e.printStackTrace();
	}
}
	 
	public static long[] getTimeDifference(Date d1, Date d2) {
	        long[] result = new long[5];
	        Calendar cal = Calendar.getInstance();
	        cal.setTimeZone(TimeZone.getTimeZone("UTC"));
	        cal.setTime(d1);

	        long t1 = cal.getTimeInMillis();
	        cal.setTime(d2);

	        long diff = Math.abs(cal.getTimeInMillis() - t1);
	        final int ONE_DAY = 1000 * 60 * 60 * 24;
	        final int ONE_HOUR = ONE_DAY / 24;
	        final int ONE_MINUTE = ONE_HOUR / 60;
	        final int ONE_SECOND = ONE_MINUTE / 60;

	        long d = diff / ONE_DAY;
	        diff %= ONE_DAY;

	        long h = diff / ONE_HOUR;
	        diff %= ONE_HOUR;

	        long m = diff / ONE_MINUTE;
	        diff %= ONE_MINUTE;

	        long s = diff / ONE_SECOND;
	        long ms = diff % ONE_SECOND;
	        result[0] = d;
	        result[1] = h;
	        result[2] = m;
	        result[3] = s;
	        result[4] = ms;

	        return result;
	    }


	public void SendMail(long id, String type,long traderId) throws Exception {
		
		SuspensionAlertDTO dto = new SuspensionAlertDTO();
		
			String actDetail ="";
			String revokeDetail ="";
			String traderType ="";
			String category = "";
			String actTitle = "";
			String suspendTitle = "";
			String revokeTitle = "";
			String emailOffice = "";
			String emailShop ="";
			SuspensionAlertDTO params = new SuspensionAlertDTO();
			//params.setSuspendId(id);
			params.setTraderId(traderId);
			
		List<TraderAddress> traderAddressList =  (List<TraderAddress>)this.traderAddressDAO.findAllEmaiByTrader(params.getTraderId(),"N");
            
	if(!traderAddressList.isEmpty() && traderAddressList!=null)
	{	
		for(TraderAddress traddress : traderAddressList)
		{
			emailOffice = traddress.getEmail();
			if(!emailOffice.equals(emailShop))
			{
				dto.setEmail(emailOffice);
				if(type.equals("S")) //TYPE "S" Suspension
				{
					params.setSuspendId(id);
					List<Object[]> listObj = (List<Object[]>)this.suspensionAlertDAO.findSuspendDetail(params);
				
					if(!listObj.isEmpty() && listObj!=null)
					{
						System.out.println("INS");
						for(Object[] sel: listObj)
						{
							if(sel[0]!=null)
							{
								dto.setTraderName(sel[0].toString());
							}
							if(sel[1]!=null)
							{
								dto.setLicenseNo(sel[1].toString());
							}
							if(sel[2]!=null)
							{
							    traderType = sel[2].toString();
								//dto.setTraderType(TraderType.getMeaning(traderType));
							}
							if(sel[3]!=null)
							{
								dto.setPersonType(sel[3].toString());
							}
							if(sel[4]!=null)
							{
								dto.setPrefixName(sel[4].toString());
							}
							if(sel[5]!=null)
							{
								dto.setPostfixName(sel[5].toString());
							}
							if(sel[6]!=null)
							{
								dto.setFirstName(sel[6].toString());
							}
							if(sel[7]!=null)
							{
								dto.setLastName(sel[7].toString());
							}
							if(sel[8]!=null)
							{
								actDetail = actDetail+ sel[8].toString()+"<br>";
							}
							if(sel[9]!=null)
							{
								dto.setSuspendPeriod(sel[9].toString());
							}
							if(sel[10]!=null)
							{
								dto.setSuspendPeriodDay(sel[10].toString());
							}
							if(sel[11]!=null)
							{
								suspendTitle = "รายละเอียดการพักใช้: ";
								dto.setSuspendDetail(suspendTitle+sel[11].toString());
							}
							if(sel[12]!=null)
							{
								dto.setSuspendNo(sel[12].toString());
							}
							if(sel[13]!=null)
							{
								category = sel[13].toString();
								dto.setTraderCategoryType(TraderCategory.getMeaning(traderType, category));
							}
						}
						if(dto.getPersonType().equals("I") && dto.getPersonType()!=null)
						{
							dto.setTraderOwnerName(dto.getPrefixName()+" "+dto.getFirstName()+" "+dto.getLastName());
						}else if(dto.getPersonType().equals("C") && dto.getPersonType()!=null)
						{
							dto.setTraderOwnerName(dto.getPrefixName()+" "+dto.getFirstName()+" "+dto.getPostfixName());
						}
						if(dto.getSuspendPeriod()!=null && !dto.getSuspendPeriod().isEmpty())
						{
							if(!dto.getSuspendPeriodDay().equals("") && dto.getSuspendPeriodDay()!=null && !dto.getSuspendPeriodDay().equals("0"))
							{
								dto.setPeriod("ระยะเวลาที่ถูกพักใช้: "+dto.getSuspendPeriod()+" เดือน"+" "+dto.getSuspendPeriodDay()+" วัน");
							}else
							{
								dto.setPeriod("ระยะเวลาที่ถูกพักใช้: "+dto.getSuspendPeriod()+" เดือน");
							}
						}
						if(traderType!=null && !traderType.isEmpty())
						{
							if(dto.getTraderCategoryType()!=null && !dto.getTraderCategoryType().isEmpty())
							{
								dto.setTraderType(TraderType.getMeaning(traderType)+" ประเภท "+dto.getTraderCategoryType());
							}else
							{
								dto.setTraderType(TraderType.getMeaning(traderType));
							}
						}
						actTitle = "สาหตุการพักใช้: "+"<br>";
						dto.setActDesc(actTitle+actDetail);
					    dto.setAlertType("มีการพักใช้ใบอนุญาต เลขที่พักใช้ใบอนุญาต: "+dto.getSuspendNo());
					    
					    List<PunishmentEvidence> listEvid = (List<PunishmentEvidence>) this.punishmentEvidenceDAO.findBySuspensionLicense(params.getSuspendId());
					    if(!listEvid.isEmpty())
					    {
					    	PunishmentEvidence edv = listEvid.get(0);
					    	String url = ConstantUtil.VIEW_PUNISH_URL + edv.getPunishmentId();
					    	 dto.setEmailAlertStatus("<a href='"+url+"'>คลิกที่นี่เพื่อดูรายละเอียด</a>");
					    }
					   
					    //
					   
					}
					 this.sendMailAddress(dto,type);
				}else if(type.equals("R")) // type "R" = Revoke
				{
					params.setRevokeId(id);
					List<Object[]> listObj = (List<Object[]>)this.revokeAlertDAO.findRevokeDetail(params);
					if(!listObj.isEmpty() && listObj !=null)
					{
						for(Object[] sel: listObj)
						{
							if(sel[0]!=null)
							{
								dto.setTraderName(sel[0].toString());
							}
							if(sel[1]!=null)
							{
								dto.setLicenseNo(sel[1].toString());
							}
							if(sel[2]!=null)
							{
							    traderType = sel[2].toString();
								//dto.setTraderType(TraderType.getMeaning(traderType));
							}
							if(sel[3]!=null)
							{
								dto.setPersonType(sel[3].toString());
							}
							if(sel[4]!=null)
							{
								dto.setPrefixName(sel[4].toString());
							}
							if(sel[5]!=null)
							{
								dto.setPostfixName(sel[5].toString());
							}
							if(sel[6]!=null)
							{
								dto.setFirstName(sel[6].toString());
							}
							if(sel[7]!=null)
							{
								dto.setLastName(sel[7].toString());
							}
							if(sel[8]!=null)
							{
								actDetail = actDetail+ sel[8].toString()+"<br>";
							}
							if(sel[9]!=null)
							{
								revokeTitle = "รายละเอียดการเพิกถอน: ";
								dto.setRevokeDetail(revokeTitle+sel[11].toString());
							}
							if(sel[10]!=null)
							{
								dto.setReceiveNo(sel[10].toString());
							}
							if(sel[11]!=null)
							{
								category = sel[11].toString();
								dto.setTraderCategoryType(TraderCategory.getMeaning(traderType, category));
							}
						}
						if(dto.getPersonType().equals("I") && dto.getPersonType()!=null)
						{
							dto.setTraderOwnerName(dto.getPrefixName()+" "+dto.getFirstName()+" "+dto.getLastName());
						}else if(dto.getPersonType().equals("C") && dto.getPersonType()!=null)
						{
							dto.setTraderOwnerName(dto.getPrefixName()+" "+dto.getFirstName()+" "+dto.getPostfixName());
						}
						if(traderType!=null && !traderType.isEmpty())
						{
							if(dto.getTraderCategoryType()!=null && !dto.getTraderCategoryType().isEmpty())
							{
								dto.setTraderType(TraderType.getMeaning(traderType)+" ประเภท "+dto.getTraderCategoryType());
							}else
							{
								dto.setTraderType(TraderType.getMeaning(traderType));
							}
						}
						 dto.setAlertType("มีการเพิกถอนใบอนุญาต เลขที่เพิกถอนใบอนุญาต: "+dto.getReceiveNo());
						
						 dto.setPeriod("");
						 actTitle = "สาหตุการเพิกถอน: "+"<br>";
						 dto.setActDesc(actTitle+actDetail);
						 
						 List<PunishmentEvidence> listEvid = (List<PunishmentEvidence>) this.punishmentEvidenceDAO.findByRevokeLicense(params.getRevokeId());
					    if(!listEvid.isEmpty())
					    {
					    	PunishmentEvidence edv = listEvid.get(0);
					    	String url = ConstantUtil.VIEW_PUNISH_URL + edv.getPunishmentId();
					    	 dto.setEmailAlertStatus("<a href='"+url+"'>คลิกที่นี่เพื่อดูรายละเอียด</a>");
					    }
					}
					this.sendMailAddress(dto,type);
				}else if(type.equals("C"))
				{
					
				}
			}
	    
		emailShop = emailOffice;
		System.out.println(emailShop);
		System.out.println(emailOffice);
	}
		if(traderType.equals("L") && category.equals("100")) // if TourLeader = Outbound 
		{
			SuspensionAlertDTO param = new SuspensionAlertDTO();
			String emailOfficeB ="";
			String emailShopB="";
			param.setTraderType(traderType);
			param.setTraderCategory(category);
			List<Trader> listTrader = (List<Trader>)this.traderDAO.findAllTraderCategory(param);
			if(!listTrader.isEmpty() && listTrader !=null)
			{
				for(Trader trader : listTrader)
				{
					List<TraderAddress> traderAddressListOutbound =  (List<TraderAddress>)this.traderAddressDAO.findAllEmaiByTrader(trader.getTraderId(),"N");
					for(TraderAddress traddress : traderAddressListOutbound)
					{
						emailOfficeB = traddress.getEmail();
						System.out.println(emailOfficeB);
						System.out.println(emailShopB);
						if(!emailOfficeB.equals(emailShopB))
						{
							dto.setEmail(emailOfficeB);
							this.sendMailAddress(dto,type);
						}
						emailShopB = emailOfficeB;	
					}
					 emailOfficeB ="";
					 emailShopB="";
				}
			}
		}
}else
{
	System.out.println("NO EMAIL SEND");
}
		
}
	
	public void sendMailAddress(SuspensionAlertDTO dto,String type) throws Exception
	{
		JavaMailSenderImpl sender = new JavaMailSenderImpl();
		Properties pro = new Properties();
	    pro.setProperty("mail.smtps.auth", "true");
	    pro.setProperty("mail.smtps.starttls.enable", "true");
	    pro.setProperty("mail.debug", "true");
	    sender.setJavaMailProperties(pro);
		sender.setProtocol("smtps");
		//แก้ email ให้เป็น ของ สำนักงานการท่องเที่ยว
		sender.setUsername("sunsonsoft@gmail.com");
        sender.setPassword("sss123456");
        //
        
		sender.setHost("smtp.gmail.com");
		sender.setPort(465);
        sender.setDefaultEncoding("utf-8");

		MimeMessage message = sender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message,true);
		
		helper.setSubject("TEST-SENT-MAIL");
		
		helper.setFrom("sek.beck@gmail.com");
		 

		if(dto.getEmail()!=null && !dto.getEmail().isEmpty())	
		{
			helper.setTo(dto.getEmail());
			//helper.setTo("assiduously_in_love@hotmail.com");
		}
		//helper.setTo("assiduously_in_love@hotmail.com");
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("MailTourismForm.html"),"utf-8"));
		
		System.out.println("//send mail");
		System.out.println(reader.toString());
		
		
		StringBuffer body = new StringBuffer();

		String t =  reader.readLine();
		while( t!=null  ){
			body.append(t);
			t =  reader.readLine();
	    }
		
		message.setContent(body.toString(), "text/html; charset=utf-8" );
		String[] lang = {"th"};
		message.setContentLanguage(lang);
		
	      // create the message part    
	      MimeBodyPart messageBodyPart = new MimeBodyPart();   

	      //fill message   
	      messageBodyPart.setText("");   
	      //messageBodyPart.setContent(MessageFormat.format(body.toString()  , insureRequest.getRefNo1(), insureRequest.getRefNo2() ,insureRequest.getReqSeq(),cusName), "text/html; charset=utf-8");
	      if(type.equals("S"))
	      {
	    	  messageBodyPart.setContent(MessageFormat.format(body.toString(), dto.getLicenseNo(),dto.getTraderOwnerName(),dto.getTraderType(),dto.getAlertType(),dto.getActDesc(),dto.getSuspendDetail(),dto.getPeriod(),dto.getEmailAlertStatus()), "text/html; charset=utf-8");
	      }else if(type.equals("R"))
	      {
	    	  messageBodyPart.setContent(MessageFormat.format(body.toString(), dto.getLicenseNo(),dto.getTraderOwnerName(),dto.getTraderType(),dto.getAlertType(),dto.getActDesc(),dto.getRevokeDetail(),dto.getPeriod(),dto.getEmailAlertStatus()), "text/html; charset=utf-8");
	      }else if(type.equals("C"))
	      {
	    	  messageBodyPart.setContent(MessageFormat.format(body.toString(), dto.getLicenseNo(),dto.getTraderOwnerName(),dto.getTraderType(),dto.getAlertType(),dto.getActDesc(),"","",dto.getEmailAlertStatus()), "text/html; charset=utf-8");
	      }else if(type.equals("D"))
	      {
	    	  BufferedReader readerExpire = new BufferedReader(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("MailTourismFormExpireDate.html"),"utf-8"));
	  		
	  			System.out.println("//send mail");
	  			System.out.println(reader.toString());
	  		
	  		
	  			StringBuffer bodyExpire = new StringBuffer();

	  			String tExpire =  reader.readLine();
	  			while( tExpire!=null  ){
	  			bodyExpire.append(t);
	  			tExpire =  readerExpire.readLine();
	  			}
	  		
	  			message.setContent(bodyExpire.toString(), "text/html; charset=utf-8" );
	  			String[] langExpire = {"th"};
	  			message.setContentLanguage(langExpire);
	    	    messageBodyPart.setContent(MessageFormat.format(bodyExpire.toString(), dto.getLicenseNo(),dto.getTraderOwnerName(),dto.getTraderType(),dto.getEmailAlertStatus()), "text/html; charset=utf-8"); 
	      }
	      
	      Multipart multipart = new MimeMultipart(); 
	      multipart.addBodyPart(messageBodyPart);   
  
	     message.setContent(multipart);
		
		
		
		sender.send(message);
	}
	
	public void SendMailComplaint(ComplaintLicenseDTO dto) throws Exception {
		
		String traderType ="";
		String category = "";
		String emailOffice = "";
		String emailShop ="";
		String complaintNo ="";
		SuspensionAlertDTO param = new SuspensionAlertDTO();
		List<TraderAddress> traderAddressList =  (List<TraderAddress>)this.traderAddressDAO.findAllEmaiByTrader(dto.getTraderId(),"N");
		if(!traderAddressList.isEmpty() && traderAddressList!=null)
		{
			for(TraderAddress traddress : traderAddressList)
			{
				emailOffice = traddress.getEmail();
				if(!emailOffice.equals(emailShop))
				{
					param.setEmail(emailOffice);
					List<Object[]> listObj = (List<Object[]>)this.complaintLicenseDAO.findComplaintDetail(dto);
					if(!listObj.isEmpty() && listObj!=null)
					{
						for(Object[] sel : listObj)
						{
							if(sel[0] !=null)
							{
								param.setPrefixName(sel[0].toString());
							}
							if(sel[1] !=null)
							{
								param.setPostfixName(sel[1].toString());
							}
							if(sel[2] !=null)
							{
								param.setFirstName(sel[2].toString());
							}
							if(sel[3] !=null)
							{
								param.setLastName(sel[3].toString());
							}
							if(sel[4] !=null)
							{
								category = sel[4].toString();
								param.setTraderCategory(TraderCategory.getMeaning(dto.getTraderType(), category));
							}
							if(sel[5] !=null)
							{
								param.setPersonType(sel[5].toString());
							}
							if(sel[6] !=null)
							{
								complaintNo = sel[6].toString();
							}
						}
					}
					if(param.getPersonType().equals("I") && param.getPersonType()!=null)
					{
						param.setTraderOwnerName(param.getPrefixName()+" "+param.getFirstName()+" "+param.getLastName());
					}else if(param.getPersonType().equals("C") && param.getPersonType()!=null)
					{
						param.setTraderOwnerName(param.getPrefixName()+" "+param.getFirstName()+" "+param.getPostfixName());
					}
					if(dto.getTraderType()!=null && !dto.getTraderType().isEmpty())
					{
						if(param.getTraderCategory()!=null && !param.getTraderCategory().isEmpty())
						{
							param.setTraderType(TraderType.getMeaning(dto.getTraderType())+" ประเภท "+param.getTraderCategory());
						}else
						{
							param.setTraderType(TraderType.getMeaning(dto.getTraderType()));
						}
					}
					param.setLicenseNo(dto.getLicenseNo());
					param.setAlertType("มีการร้องเรียนใบอนุญาต เลขที่ร้องเรียน: "+complaintNo);
					//param.setTraderType("");
					param.setActDesc("รายละเอียดเรื่องร้องเรียน: "+dto.getComplaintDesc());
					param.setEmailAlertStatus("<a href='https://www.google.co.th'>คลิกที่นี่เพื่อยืนยัน</a>");
					
					this.sendMailAddress(param,"C");
				}
			emailShop = emailOffice;
		}
	}else
	{
		System.out.println("NO EMAIL SEND");
	}
 }
	
/*	public static void main(String args[]) {
		      SendMailService s = new SendMailService();
		      try {
				s.SendMailExpireDate();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		  }*/
}
