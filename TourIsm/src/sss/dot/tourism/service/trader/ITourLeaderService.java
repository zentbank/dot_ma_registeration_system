package sss.dot.tourism.service.trader;

import java.util.List;

import sss.dot.tourism.domain.Registration;
import sss.dot.tourism.dto.registration.RegistrationDTO;

import com.sss.aut.service.User;

public interface ITourLeaderService {
	public void saveRegistration(Object object, User user) throws Exception;
	public List getRegistration(Object object, User user) throws Exception;
	public void getRegistration(Registration reg, RegistrationDTO dto, User user) throws Exception;
	public void cancelRegistration(Object object, User user) throws Exception;
	
	public void saveLicense(Object object , User user) throws Exception;
}
