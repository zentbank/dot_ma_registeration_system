package sss.dot.tourism.service.trader;

import java.util.List;

import sss.dot.tourism.domain.Person;

import com.sss.aut.service.User;

public interface ITraderService {
	
	public List getTraderAll(Object object, User user) throws Exception;
	public List getTraderAllName(Object object, User user) throws Exception;
	public List getTraderByLicenseNo(Object object, User user) throws Exception;
	
	public void suspend(Object object, User user) throws Exception;
	
	public void unSuspend(Object object, User user) throws Exception;
	
	public void revoke(Object object, User user) throws Exception;
	
	public void unRevoke(Object object, User user) throws Exception;
	
	public void deActivate(Object object, User user) throws Exception;
	
	public void activate(Object object, User user) throws Exception;
	
	public void updateGuide() throws Exception;
	public void updateTour() throws Exception;
	
	public String getTraderOwnerName(Person person) throws Exception;
	
	
	
}
