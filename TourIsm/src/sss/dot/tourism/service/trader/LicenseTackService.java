package sss.dot.tourism.service.trader;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import sss.dot.tourism.dao.AdmUserDAO;
import sss.dot.tourism.dao.registration.TraderAddressDAO;
import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.dto.deactivate.DeactivateLicenseDTO;
import sss.dot.tourism.dto.deactivate.MasDeactivateTypeDTO;
import sss.dot.tourism.dto.mas.EmailDTO;
import sss.dot.tourism.dto.punishment.MasActDTO;
import sss.dot.tourism.dto.punishment.SuspensionLicenseDTO;
import sss.dot.tourism.service.deactivate.IDeactivateLicenseService;
import sss.dot.tourism.service.punishment.ISuspensionService;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.LicenseStatus;
import sss.dot.tourism.util.SendMailThread;

import com.sss.aut.service.User;

import fr.opensagres.xdocreport.utils.StringUtils;

public class LicenseTackService {

	@Autowired
	private  TraderDAO traderDAO;
	@Autowired
	private  TraderAddressDAO traderAddressDAO;
	
	@Autowired
	private  AdmUserDAO admUserDAO;
	
	@Autowired
	private sss.dot.tourism.service.admin.IUserAccountService userAccountService;
	
	@Autowired
	private  ISuspensionService suspensionService;
	@Autowired
	private  IDeactivateLicenseService deactivateLicenseService;
	
	// Example set up cron	
//		0 0 12 * * ?	Fire at 12:00 PM (noon) every day
//		0 15 10 ? * *	Fire at 10:15 AM every day
//		0 15 10 * * ?	Fire at 10:15 AM every day
//		0 15 10 * * ? *	Fire at 10:15 AM every day
//		0 15 10 * * ? 2005	Fire at 10:15 AM every day during the year 2005
//		0 * 14 * * ?	Fire every minute starting at 2:00 PM and ending at 2:59 PM, every day
//		0 0/5 14 * * ?	Fire every 5 minutes starting at 2:00 PM and ending at 2:55 PM, every day
//		0 0/5 14,18 * * ?	Fire every 5 minutes starting at 2:00 PM and ending at 2:55 PM, AND fire every 5 minutes starting at 6:00 PM and ending at 6:55 PM, every day
//		0 0-5 14 * * ?	Fire every minute starting at 2:00 PM and ending at 2:05 PM, every day
//		0 10,44 14 ? 3 WED	Fire at 2:10 PM and at 2:44 PM every Wednesday in the month of March
//		0 15 10 ? * MON-FRI	Fire at 10:15 AM every Monday, Tuesday, Wednesday, Thursday and Friday
//		0 15 10 15 * ?	Fire at 10:15 AM on the 15th day of every month
//		0 15 10 L * ?	Fire at 10:15 AM on the last day of every month
//		0 15 10 ? * 6L	Fire at 10:15 AM on the last Friday of every month
//		0 15 10 ? * 6L	Fire at 10:15 AM on the last Friday of every month
//		0 15 10 ? * 6L 2002-2005	Fire at 10:15 AM on every last friday of every month during the years 2002, 2003, 2004, and 2005
//		0 15 10 ? * 6#3	Fire at 10:15 AM on the third Friday of every month
//		0 0 12 1/5 * ?	Fire at 12 PM (noon) every 5 days every month, starting on the first day of the month
//		0 11 11 11 11 ?	Fire every November 11 at 11:11 AM
		

	@Scheduled(cron="0 0 2 * * ?") //ที่เวลาตี 2 ของทุกๆ วัน ให้ทำงานที่ method นี้Expression	Means
	public void warningPayFeesByMail() {
		
		try{
			List<Object[]> listObj = this.traderDAO.findWarningPayFeesByMail();
			
			if(!listObj.isEmpty())
			{
				List<EmailDTO> emails = new ArrayList<EmailDTO>();
				
				BufferedReader reader = new BufferedReader(new InputStreamReader(this
						.getClass().getClassLoader()
						.getResourceAsStream("MailWarningPayFees.html"), "utf-8"));
				
				StringBuffer body = new StringBuffer();

				String t = reader.readLine();
				while (t != null) {
					body.append(t);
					t = reader.readLine();
				}
				
				for(Object[] obj: listObj)
				{
					if(StringUtils.isNotEmpty(obj[4].toString()))
					{
						EmailDTO regDTO = new EmailDTO();

						regDTO.setEmail(obj[4].toString());
						regDTO.setSubject("เรียน ผู้ประกอบธุรกิจนำเที่ยว ใบอนุญาตประกอบธุรกิจนำเที่ยวเลขที่ " + obj[1].toString());
						
						String message = body.toString();
						regDTO.setMessage(message);
						
						emails.add(regDTO);

						// send mail
						SendMailThread mailThred = new SendMailThread(emails, body);
						mailThred.run();
						Thread.sleep(180);
					}
					
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
	}
	@Scheduled(cron="0 0 3 * * ?")
	public void SuspensionLicense() {
		
		DateFormat df = DateUtils.getProcessDateFormatThai();
		
		try{
			User user = (User) userAccountService.getUser("reg", "03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4");
			
			List<Object[]> listObj = this.traderDAO.findTourExpire();
			
			if(!listObj.isEmpty())
			{
				for(Object[] obj: listObj){
					
					SuspensionLicenseDTO param = new SuspensionLicenseDTO();
					param.setSuspensionDate(df.format(new Date()));
					param.setTraderId(Long.valueOf(obj[0].toString()));
					param.setSuspendPeriod(6);
					param.setSuspendPeriodDay(0);
					
					MasActDTO act = new MasActDTO();
					act.setMasActId(3);
					
					Object[] object = {act};
					
					this.suspensionService.saveSuspension(param, object, user);
				}
				
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Scheduled(cron="0 30 3 * * ?")
	public void deactivateGuideLicense() {
		
		DateFormat df = DateUtils.getProcessDateFormatThai();
		
		try{
			User user = (User) userAccountService.getUser("reg", "03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4");
			
			List<Object[]> listObj = this.traderDAO.findGuideExpire();
			
			if(!listObj.isEmpty())
			{
				for(Object[] obj: listObj){
					
					DeactivateLicenseDTO param = new DeactivateLicenseDTO();
					param.setTraderId(Long.valueOf(obj[0].toString()));
					param.setDeactivateDate(df.format(new Date()));
					param.setRenewLicenseStatus("2");
					
					MasDeactivateTypeDTO decType = new MasDeactivateTypeDTO();
					decType.setMasDeactivateTypeId(18);
					decType.setDeactivateName("ยกเลิกใบอนุญาต กรณีใบอนุญาตหมดอายุ");
					
					Object[] object = {decType};
					
					this.deactivateLicenseService.saveDeactivation(param, object, user);
					
					param.setApproveStatus("A");
					param.setBookDate(df.format(new Date()));
					param.setBookNo(Calendar.getInstance().getTimeInMillis()+"");
					this.deactivateLicenseService.deActivate(param,object, user);
				}
				
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	@Scheduled(cron="0 0 2 * * ?")
	public void genMemberPasswordTableForMobileTourGuide(){
		
	}
	
	@Scheduled(cron="0 0 1 * * ?")
	public void doSetExpireLicense(){
		//----- get trader that expired with all branch
		//----- set licenseStatus = 'E'
		try{
			User user = (User) userAccountService.getUser("reg", "03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4");
			List<Object[]> tourExpireds = this.traderDAO.findTourAllExpire();
			if(CollectionUtils.isNotEmpty(tourExpireds)){
				for(Object[] obj : tourExpireds){
					Long traderId = Long.valueOf(obj[0].toString());
					Trader expireTrader = (Trader)this.traderDAO.findByPrimaryKey(traderId);
					List<Trader> expireBranchTraders = this.traderDAO.findTraderBranch(expireTrader);
					System.out.println("Expire trader licenseNo: "+expireTrader.getLicenseNo());
					if(CollectionUtils.isNotEmpty(expireBranchTraders)){
						System.out.println("branch ----------------");
						for(Trader branch : expireBranchTraders){
							System.out.println("Branch no: "+branch.getTraderBranchNo());
							branch.setLicenseStatus(LicenseStatus.EXPIRE.getStatus());
							branch.setLastUpdDtm(new Date());
							branch.setLastUpdUser(user.getUserName());
							this.traderDAO.update(branch);
						}
						System.out.println("-----------------------");
					}
					expireTrader.setLicenseStatus(LicenseStatus.EXPIRE.getStatus());
					expireTrader.setLastUpdDtm(new Date());
					expireTrader.setLastUpdUser(user.getUserName());
					this.traderDAO.update(expireTrader);
				}
			}else{
				System.out.println("Not found tourism expire!!");
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
//	@Scheduled(cron="0 30 00 * * ?")
//	public void deactivateGuideAllLicense() {
//		
//		DateFormat df = DateUtils.getProcessDateFormatThai();
//		
//		try{
//			User user = (User) userAccountService.getUser("reg", "03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4");
//			
//			List<Object[]> listObj = this.traderDAO.findGuideAllExpire();
//			
//			if(!listObj.isEmpty())
//			{
//				for(Object[] obj: listObj){
//					
//					DeactivateLicenseDTO param = new DeactivateLicenseDTO();
//					param.setTraderId(Long.valueOf(obj[0].toString()));
//					param.setDeactivateDate(df.format(new Date()));
//					param.setRenewLicenseStatus("2");
//					
//					MasDeactivateTypeDTO decType = new MasDeactivateTypeDTO();
//					decType.setMasDeactivateTypeId(18);
//					decType.setDeactivateName("ยกเลิกใบอนุญาต กรณีใบอนุญาตหมดอายุ");
//					
//					Object[] object = {decType};
//					
//					this.deactivateLicenseService.saveDeactivation(param, object, user);
//					
//					param.setApproveStatus("A");
//					param.setBookDate(df.format(new Date()));
//					param.setBookNo(Calendar.getInstance().getTimeInMillis()+"");
//					this.deactivateLicenseService.deActivate(param,object, user);
//				}
//				
//			}
//		}catch(Exception e){
//			e.printStackTrace();
//		}
//	}
}
