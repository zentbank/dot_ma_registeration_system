package sss.dot.tourism.service.trader;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.mas.CountryDAO;
import sss.dot.tourism.dao.mas.MasRunningNoDAO;
import sss.dot.tourism.dao.registration.BranchDAO;
import sss.dot.tourism.dao.registration.CommitteeDAO;
import sss.dot.tourism.dao.registration.PersonDAO;
import sss.dot.tourism.dao.registration.PlantripDAO;
import sss.dot.tourism.dao.registration.RegisterProgressDAO;
import sss.dot.tourism.dao.registration.RegistrationDAO;
import sss.dot.tourism.dao.registration.TraderAddressDAO;
import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.domain.Committee;
import sss.dot.tourism.domain.Country;
import sss.dot.tourism.domain.Person;
import sss.dot.tourism.domain.Plantrip;
import sss.dot.tourism.domain.Registration;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.domain.TraderAddress;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.util.LicenseStatus;
import sss.dot.tourism.util.RecordStatus;
import sss.dot.tourism.util.RegistrationType;

import com.sss.aut.service.User;

@Repository("tourCompaniesService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class TourCompaniesService implements ITourCompaniesService{

	@Autowired
	MasRunningNoDAO masRunningNoDAO;
	@Autowired
	RegistrationDAO registrationDAO;
	@Autowired
	TraderDAO traderDAO;
	@Autowired
	RegisterProgressDAO registerProgressDAO;
	@Autowired
	PersonDAO personDAO;
	@Autowired
	PlantripDAO plantripDAO;
	@Autowired
	CountryDAO countryDAO;
	
	@Autowired
	BranchDAO branchDAO;
	
	@Autowired
	TraderAddressDAO traderAddressDAO;
	@Autowired
	CommitteeDAO committeeDAO;
	
	
	public  List getRegistration(Object object, User user) throws Exception {
		// TODO Auto-generated method stub
		return null;
		
	}
	
	
	public void getRegistration(Registration reg, RegistrationDTO dto, User user)
			throws Exception {
		Trader trader = (Trader)reg.getTrader();
		
		String recordStatus = trader.getRecordStatus();
		
		//plantripPerYear
		List<Plantrip> listPlan = plantripDAO.findPlanTripbyTrader(trader.getTraderId(), recordStatus);
		if(!listPlan.isEmpty())
		{
			Plantrip plantrip = (Plantrip)listPlan.get(0);
			dto.setPlantripPerYear(plantrip.getPlantripPerYear());
		}
		
	}



	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveRegistration(Object object , User user) throws Exception {
		
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
		RegistrationDTO dto =  (RegistrationDTO)object;
		
		Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(dto.getRegId());

	
		Trader tourCom = (Trader)reg.getTrader();
		tourCom.setLicenseStatus(LicenseStatus.NORMAL.getStatus());
		

		Person person = tourCom.getPerson();

		person.setCommitteeName1(dto.getCommitteeNameSign());
		
	
		personDAO.update(person);

		
		// set plan trip
		List<Plantrip> listplantrip = (List<Plantrip>) this.plantripDAO.findPlanTripbyTrader(tourCom.getTraderId(), RecordStatus.TEMP.getStatus());
		if(!listplantrip.isEmpty())
		{
			for(Plantrip plan: listplantrip)
			{
				plan.setPlantripPerYear(dto.getPlantripPerYear());
				plan.setLastUpdUser(user.getUserName());
				plan.setLastUpdDtm(new Date());
				
				plantripDAO.update(plan);
			}
		}
		else
		{
			Country thaiCountry = countryDAO.findThaiCountry();
			Plantrip plantrip = new Plantrip();
			
			plantrip.setCountry(thaiCountry);
			plantrip.setPlantripPerYear(dto.getPlantripPerYear());
			plantrip.setCreateDtm(new Date());
			plantrip.setCreateUser(user.getUserName());
			plantrip.setRecordStatus(RecordStatus.TEMP.getStatus());
			plantrip.setTrader(tourCom);
			
			plantripDAO.insert(plantrip);
			
		}
		
		List<Trader> listBranch = (List<Trader>)this.traderDAO.findTraderBranch(tourCom);
		
		if(!listBranch.isEmpty())
		{
			for(Trader branch: listBranch)
			{
				if((tourCom.getLicenseNo() != null) && (!tourCom.getLicenseNo().isEmpty()))
				{
					branch.setLicenseNo(tourCom.getLicenseNo());
					
				}
				
				branch.setTraderCategory(tourCom.getTraderCategory());
				branch.setTraderType(tourCom.getTraderType());
				branch.setTraderName(tourCom.getTraderName());
				branch.setTraderNameEn(tourCom.getTraderNameEn());
				branch.setPronunciationName(tourCom.getPronunciationName());
				branch.setEffectiveDate(tourCom.getEffectiveDate());
				branch.setExpireDate(tourCom.getExpireDate());
				branch.setLicenseStatus(tourCom.getLicenseStatus());
				branch.setOrganization(tourCom.getOrganization());
				branch.setPerson(tourCom.getPerson());
				branch.setMasProvince(tourCom.getMasProvince());
				
			}
		}
	}


	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void cancelRegistration(Object object, User user) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveLicense(Object object , User user) throws Exception {
		
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
		RegistrationDTO dto =  (RegistrationDTO)object;
		
		Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(dto.getRegId());

	
		Trader tourCom = (Trader)reg.getTrader();
		tourCom.setLicenseStatus(LicenseStatus.NORMAL.getStatus());
		

		Person person = tourCom.getPerson();

		person.setCommitteeName1(dto.getCommitteeNameSign());
		
	
		personDAO.update(person);

		
		// set plan trip
		List<Plantrip> listplantrip = (List<Plantrip>) this.plantripDAO.findPlanTripbyTrader(tourCom.getTraderId(), null);
		if(!listplantrip.isEmpty())
		{
			for(Plantrip plan: listplantrip)
			{
				plan.setPlantripPerYear(dto.getPlantripPerYear());
				plan.setLastUpdUser(user.getUserName());
				plan.setRecordStatus(RecordStatus.NORMAL.getStatus());
				plan.setLastUpdDtm(new Date());
				
				plantripDAO.update(plan);
			}
		}
		else
		{
			Country thaiCountry = countryDAO.findThaiCountry();
			Plantrip plantrip = new Plantrip();
			
			plantrip.setCountry(thaiCountry);
			plantrip.setPlantripPerYear(dto.getPlantripPerYear());
			plantrip.setCreateDtm(new Date());
			plantrip.setCreateUser(user.getUserName());
			plantrip.setRecordStatus(RecordStatus.NORMAL.getStatus());
			plantrip.setTrader(tourCom);
			
			plantripDAO.insert(plantrip);
			
		}
		
		List<Trader> listBranch = (List<Trader>) this.branchDAO.findAllByBranchParent(tourCom.getTraderId(), null);
		if(!listBranch.isEmpty())
		{
			for(Trader branch: listBranch)
			{
				branch.setTraderCategory(tourCom.getTraderCategory());
				branch.setTraderType(tourCom.getTraderType());
				branch.setTraderName(tourCom.getTraderName());
				branch.setTraderNameEn(tourCom.getTraderNameEn());
				branch.setPronunciationName(tourCom.getPronunciationName());
				branch.setEffectiveDate(tourCom.getEffectiveDate());
				branch.setExpireDate(tourCom.getExpireDate());
				branch.setLicenseStatus(tourCom.getLicenseStatus());
				branch.setOrganization(tourCom.getOrganization());
				branch.setPerson(tourCom.getPerson());
				branch.setMasProvince(tourCom.getMasProvince());
				
				branch.setLicenseNo(tourCom.getLicenseNo());
		
				
				if(RegistrationType.TOUR_COMPANIES_ADD_NEW_BRANCH.getStatus().equals(branch.getAddNewBranch()))
				{
//					branch.setEffectiveDate(new Date());
				}
				
				branch.setLicenseStatus(RecordStatus.NORMAL.getStatus());
				branch.setRecordStatus(RecordStatus.NORMAL.getStatus());
				branch.setLastUpdUser(user.getUserName());
				
				branch.setLastUpdDtm(new Date());
				
				this.branchDAO.update(branch);
				
				List<TraderAddress> listBranchAddress = (List<TraderAddress>) this.traderAddressDAO.findAllByTrader(branch.getTraderId(), null, RecordStatus.TEMP.getStatus());
				if(!listBranchAddress.isEmpty())
				{
					for(TraderAddress branchAddress: listBranchAddress)
					{
						branchAddress.setRecordStatus(RecordStatus.NORMAL.getStatus());
						branchAddress.setLastUpdUser(user.getUserName());
						branchAddress.setLastUpdDtm(new Date());
						this.traderAddressDAO.update(branchAddress);
					}
				}
			}
		}
		

		
		//committee
		List<Committee> listCommittee =  (List<Committee>) this.committeeDAO.findAllByPerson(person.getPersonId(), null, RecordStatus.TEMP.getStatus());
		if(!listCommittee.isEmpty())
		{
			for(Committee comm: listCommittee)
			{
				comm.setCommitteeStatus(LicenseStatus.NORMAL.getStatus());
				comm.setRecordStatus(RecordStatus.NORMAL.getStatus());
				comm.setLastUpdUser(user.getUserName());
				comm.setLastUpdDtm(new Date());
				this.committeeDAO.update(comm);
			}
		}
		
	}
	  
}
