package sss.dot.tourism.service.trader;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.mas.CountryDAO;
import sss.dot.tourism.dao.mas.MasRunningNoDAO;
import sss.dot.tourism.dao.registration.PersonDAO;
import sss.dot.tourism.dao.registration.PlantripDAO;
import sss.dot.tourism.dao.registration.RegisterProgressDAO;
import sss.dot.tourism.dao.registration.RegistrationDAO;
import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.dao.training.EducationDAO;
import sss.dot.tourism.domain.Education;
import sss.dot.tourism.domain.MasEducationLevel;
import sss.dot.tourism.domain.MasUniversity;
import sss.dot.tourism.domain.Person;
import sss.dot.tourism.domain.Registration;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.EducationType;
import sss.dot.tourism.util.Gender;
import sss.dot.tourism.util.LicenseStatus;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.PersonType;
import sss.dot.tourism.util.RecordStatus;
import sss.dot.tourism.util.TraderCategory;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.User;

@Repository("tourLeaderService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class TourLeaderService implements ITourLeaderService{
	@Autowired
	MasRunningNoDAO masRunningNoDAO;
	@Autowired
	RegistrationDAO registrationDAO;
	@Autowired
	TraderDAO traderDAO;
	@Autowired
	RegisterProgressDAO registerProgressDAO;
	@Autowired
	PersonDAO personDAO;
	@Autowired
	PlantripDAO plantripDAO;
	@Autowired
	CountryDAO countryDAO;
	@Autowired
	EducationDAO educationDAO;
	
	
	
	public void getRegistration(Registration reg, RegistrationDTO dto, User user)
			throws Exception {

		Trader trader = (Trader)reg.getTrader();
		Person person = trader.getPerson();

		String prefixNameEn = person.getMasPrefix()==null?"":person.getMasPrefix().getPrefixNameEn();
		String traderOwnerNameEn = prefixNameEn + person.getFirstNameEn() + " "+ person.getLastNameEn();
		dto.setTraderOwnerNameEn(traderOwnerNameEn);
		
		
//		label200.setText('ใบอนุญาตเลขที่: '+model[0].get('licenseNo')+'  ชื่อ-สกุล: '+model[0].get('personFullName')+'  เพศ: '+model[0].get('genderName')+'  ประเภทมัคคุเทศก์: '+model[0].get('traderCategoryName')+'  ชนิดมัคคุเทศก์: '+model[0].get('traderCategoryGuideTypeName'));
		if(trader.getTraderByTraderGuideId() != null)
		{
			Trader guide = trader.getTraderByTraderGuideId();
			StringBuilder txtG = new StringBuilder();
			
			txtG.append("ใบอนุญาตเลขที่ ");
			txtG.append(guide.getLicenseNo());
			
			txtG.append("\nชื่อ-สกุล ");
			Person guideOwner = guide.getPerson();
			String prefixName = "";
			if(guideOwner.getMasPrefix() != null)
			{
				prefixName = guideOwner.getMasPrefix().getPrefixName();
			}
						
//			String personFullName = prefixName + guideOwner.getFirstName()==null?"":guideOwner.getFirstName()
//					+ " " + guideOwner.getLastName()==null?"":guideOwner.getLastName();
			
			String firstName = guideOwner.getFirstName()==null?"":guideOwner.getFirstName();
			String lastName = guideOwner.getLastName()==null?"":guideOwner.getLastName();
			String personFullName = prefixName+firstName+" "+lastName;
			System.out.println("personFullName = "+personFullName);
			txtG.append(personFullName);
			
			txtG.append("\nเพศ ");
			if(StringUtils.isNotEmpty(guideOwner.getGender())){
				txtG.append(Gender.getMeaning(guideOwner.getGender()));
			}
			
			
			int category = Integer.parseInt(guide.getTraderCategory());
			int traderCategoryGuideType = category / 100;
			
			String traderCategoryGuideTypeName = TraderCategory.getMeaning(guide.getTraderType(), String.valueOf(traderCategoryGuideType));
			
			txtG.append("\nประเภทมัคคุเทศก์ ");
			txtG.append(traderCategoryGuideTypeName);
			String traderCategoryName = TraderCategory.getMeaning(guide.getTraderType(), String.valueOf(guide.getTraderCategory()));
			
			txtG.append("\nชนิดมัคคุเทศก์ ");
			txtG.append(traderCategoryName);
			
			dto.setTraderGuideDetail(txtG.toString());
			
			//Oat Add
			
			dto.setTraderGuideId(guide.getTraderId());
			dto.setLicenseGuideNo(guide.getLicenseNo());
			dto.setPersonFullName(personFullName);
			dto.setGenderName(Gender.getMeaning(guideOwner.getGender()));
			
			dto.setTraderCategoryName(traderCategoryName);
			
			dto.setTraderCategoryGuideTypeName(traderCategoryGuideTypeName);
			
			//
			
			
		}
		
//		label300.setText('ชื่อหลักสูตร: '+model[0].get('graduationCourse')+'  รุ่นที่: '+model[0].get('generationGraduate')+'  สถาบัน: '+model[0].get('universityName')+'  วันที่อบรม: '+model[0].get('studyDate')+'-'+model[0].get('graduationDate'));	
		List<Education> listEdu = (List<Education>) this.educationDAO.findEducationByPerson(person.getPersonId(), person.getRecordStatus(), EducationType.TRAINING.getStatus());
		if(!listEdu.isEmpty())
		{
			DateFormat df = DateUtils.getProcessDateFormatThai();
			Education edu = listEdu.get(0);
			
			StringBuilder course = new StringBuilder();
			course.append("ชื่อหลักสูตร ");
			course.append(edu.getGraduationCourse()==null?"":edu.getGraduationCourse());
			course.append("\nรุ่นที่ ");
			course.append(edu.getGenerationGraduate()==null?"":edu.getGenerationGraduate());
			if(edu.getMasUniversity() != null)
			{
				course.append("\nสถาบัน ");
				course.append(edu.getMasUniversity().getUniversityName());
				
				dto.setUniversityName(edu.getMasUniversity().getUniversityName());
			}
			
			if(edu.getStudyDate() != null)
			{
				course.append("\nวันที่อบรม ");
				course.append(df.format(edu.getStudyDate()));
				course.append(" - ");
				course.append(df.format(edu.getGraduationDate()));
				
				dto.setStudyDate(df.format(edu.getStudyDate()));
			}
			
			dto.setTraderTrainedDetail(course.toString());
			
			dto.setEduId(edu.getEduId());
			dto.setGraduationCourse(edu.getGraduationCourse()==null?"":edu.getGraduationCourse());
			dto.setGenerationGraduate(edu.getGenerationGraduate());
			
			
		}
	}


	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveRegistration(Object object, User user) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
		RegistrationDTO dto =  (RegistrationDTO)object;
		
		Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(dto.getRegId());
		
		Trader tourleader = (Trader)reg.getTrader();
		tourleader.setLicenseStatus(LicenseStatus.NORMAL.getStatus());
						
		Person person = tourleader.getPerson();
		person.setPersonType(PersonType.INDIVIDUAL.getStatus());
		
		//เป็นผู้นำเที่ยวตามประกาศกฎกระทรวงฯ
		tourleader.setTraderByTraderGuideId(null);
		tourleader.setLicenseGuideNo(null);
		
		//เป็นมัคคุเทศก์และมาขอจดทะเบียนผู้นำเที่ยว
//		if(dto.getTraderGuideId() > 0)
		if(TraderCategory.GUIDE_LEADER.getStatus().equals(dto.getTraderCategory()))
		{
			Trader traderGuide =  (Trader)this.traderDAO.findByPrimaryKey(dto.getTraderGuideId());
			tourleader.setTraderByTraderGuideId(traderGuide);
			tourleader.setLicenseGuideNo(traderGuide.getLicenseNo());
		}

		//ผ่านการอบรมจากสถาบันที่สำนักทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ กำหนด
//		if(dto.getEduId() > 0)
		if(TraderCategory.TRAINED_LEADER.getStatus().equals(dto.getTraderCategory()))
		{
			Education course = (Education) this.educationDAO.findByPrimaryKey(dto.getEduId());
			
			Education trained = new Education();
			
			ObjectUtil.copy(course, trained);
			
			
			trained.setEducationType(EducationType.TRAINING.getStatus());
			trained.setPerson(person);
			trained.setRecordStatus(RecordStatus.TEMP.getStatus());
			trained.setCreateUser(user.getUserName());
			trained.setCreateDtm(Calendar.getInstance().getTime());
			
			if(course.getMasUniversity() != null)
			{
				trained.setMasUniversity(course.getMasUniversity());
			}
			if(course.getMasEducationLevel() != null)
			{
				trained.setMasEducationLevel(course.getMasEducationLevel());
			}
			
			educationDAO.insert(trained);
		}
		
		traderDAO.update(tourleader);
		personDAO.update(person);
//		registrationDAO.update(reg);

	
	}
	
	
	public List getRegistration(Object object, User user) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void cancelRegistration(Object object, User user) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveLicense(Object object, User user) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
		RegistrationDTO dto =  (RegistrationDTO)object;
		
		Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(dto.getRegId());
		
		Trader tourleader = (Trader)reg.getTrader();
		tourleader.setLicenseStatus(LicenseStatus.NORMAL.getStatus());
						
		Person person = tourleader.getPerson();
		person.setPersonType(PersonType.INDIVIDUAL.getStatus());
		
		//เป็นผู้นำเที่ยวตามประกาศกฎกระทรวงฯ
		tourleader.setTraderByTraderGuideId(null);
		tourleader.setLicenseGuideNo(null);
		
		//เป็นมัคคุเทศก์และมาขอจดทะเบียนผู้นำเที่ยว
//		if(dto.getTraderGuideId() > 0)
		if(TraderCategory.GUIDE_LEADER.getStatus().equals(dto.getTraderCategory()))
		{
			Trader traderGuide =  (Trader)this.traderDAO.findByPrimaryKey(dto.getTraderGuideId());
			tourleader.setTraderByTraderGuideId(traderGuide);
			tourleader.setLicenseGuideNo(traderGuide.getLicenseNo());
		}

		//ผ่านการอบรมจากสถาบันที่สำนักทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ กำหนด
//		if(dto.getEduId() > 0)
		if(TraderCategory.TRAINED_LEADER.getStatus().equals(dto.getTraderCategory()))
		{
			Education course = (Education) this.educationDAO.findByPrimaryKey(dto.getEduId());
			
			Education trained = new Education();
			
			ObjectUtil.copy(course, trained);
			
			
			trained.setEducationType(EducationType.TRAINING.getStatus());
			trained.setPerson(person);
			trained.setRecordStatus(RecordStatus.NORMAL.getStatus());
			trained.setCreateUser(user.getUserName());
			trained.setCreateDtm(Calendar.getInstance().getTime());
			
			if(course.getMasUniversity() != null)
			{
				trained.setMasUniversity(course.getMasUniversity());
			}
			if(course.getMasEducationLevel() != null)
			{
				trained.setMasEducationLevel(course.getMasEducationLevel());
			}
			
			educationDAO.insert(trained);
		}
		
		traderDAO.update(tourleader);
		personDAO.update(person);
		
	}
	
	
}
