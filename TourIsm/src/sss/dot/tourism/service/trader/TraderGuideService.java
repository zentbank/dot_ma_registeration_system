package sss.dot.tourism.service.trader;

import java.text.DateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.mas.CountryDAO;
import sss.dot.tourism.dao.mas.MasRunningNoDAO;
import sss.dot.tourism.dao.registration.PersonDAO;
import sss.dot.tourism.dao.registration.PlantripDAO;
import sss.dot.tourism.dao.registration.RegisterProgressDAO;
import sss.dot.tourism.dao.registration.RegistrationDAO;
import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.dao.training.EducationDAO;
import sss.dot.tourism.domain.Education;
import sss.dot.tourism.domain.MasProvince;
import sss.dot.tourism.domain.Person;
import sss.dot.tourism.domain.Registration;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.EducationType;
import sss.dot.tourism.util.Gender;
import sss.dot.tourism.util.LicenseStatus;
import sss.dot.tourism.util.PersonType;
import sss.dot.tourism.util.TraderCategory;

import com.sss.aut.service.User;

@Repository("guideService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class TraderGuideService implements ITraderGuideService {
	
	@Autowired
	MasRunningNoDAO masRunningNoDAO;
	@Autowired
	RegistrationDAO registrationDAO;
	@Autowired
	TraderDAO traderDAO;
	@Autowired
	RegisterProgressDAO registerProgressDAO;
	@Autowired
	PersonDAO personDAO;
	@Autowired
	PlantripDAO plantripDAO;
	@Autowired
	CountryDAO countryDAO;
	@Autowired
	EducationDAO educationDAO;
	
	public void getRegistration(Registration reg, RegistrationDTO dto, User user)
			throws Exception {
		
		Trader guideS = (Trader)reg.getTrader();
		Person personGuide = guideS.getPerson();
		
		String prefixNameEn = personGuide.getMasPrefix()==null?"":personGuide.getMasPrefix().getPrefixNameEn() ;
		
		String traderOwnerNameEn = prefixNameEn + personGuide.getFirstNameEn() + " "+ personGuide.getLastNameEn();
		dto.setTraderOwnerNameEn(traderOwnerNameEn);
		
		//sek edit
		if(dto.getTraderType()!=null && dto.getTraderType().equals("G"))
		{
			
			if((dto.getTraderCategory() != null) && (!dto.getTraderCategory().isEmpty()))
			{
				int category = Integer.parseInt(dto.getTraderCategory());
				
				if(category == 100 || category == 101 )
				{
				
					dto.setTraderCategoryGuideTypeName(TraderCategory.getMeaning(dto.getTraderType(), "1"));
				}else
				{
					dto.setTraderCategoryGuideTypeName(TraderCategory.getMeaning(dto.getTraderType(), "2"));
				}
				}
			}
		
		//end
		
		dto.setTraderArea(guideS.getTraderArea());
		
		if(guideS.getMasProvince()!=null && !guideS.getMasProvince().equals(""))
		{
			dto.setProvinceGuideServiceId(guideS.getMasProvince().getProvinceId());
			dto.setProvinceGuideServiceName(guideS.getMasProvince().getProvinceName());
		}
		
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveRegistration(Object object, User user) throws Exception {
		
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
		RegistrationDTO dto =  (RegistrationDTO)object;
		Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(dto.getRegId());
		
		Trader guide = (Trader)reg.getTrader();
		guide.setLicenseStatus(LicenseStatus.NORMAL.getStatus());
		
		
		System.out.println(dto.getTraderCategory());
		//set ประเภทมัคคุเทศน์และชนิดของมัคคุเทศน์
		if(dto.getTraderCategory()!=null)
		{
			guide.setTraderCategory(dto.getTraderCategory());
		}
		
		if(dto.getTraderArea()!=null)
		{
			guide.setTraderArea(dto.getTraderArea());
		}else
		{
			guide.setTraderArea("");
		}
		
		System.out.println(dto.getProvinceGuideServiceId());
		if(dto.getProvinceGuideServiceId() > 0)
		{
			MasProvince masProvinceByProvinceId = new MasProvince();
			masProvinceByProvinceId.setProvinceId(dto.getProvinceGuideServiceId());
			guide.setMasProvince(masProvinceByProvinceId);
		}
				
		Person person = guide.getPerson();
		person.setPersonType(PersonType.INDIVIDUAL.getStatus());
		person.setPersonNationality("ไทย");
		
		
		traderDAO.update(guide);
		personDAO.update(person);
	}

	public List getRegistration(Object object, User user) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public void cancelRegistration(Object object, User user) throws Exception {
		// TODO Auto-generated method stub

	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveLicense(Object object, User user) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลได้การจดทะเบียนได้");
		}
		
		RegistrationDTO dto =  (RegistrationDTO)object;
		Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(dto.getRegId());
		
		Trader guide = (Trader)reg.getTrader();
		guide.setLicenseStatus(LicenseStatus.NORMAL.getStatus());
		
		
		//set ประเภทมัคคุเทศน์และชนิดของมัคคุเทศน์
		if(dto.getTraderCategory()!=null)
		{
			guide.setTraderCategory(dto.getTraderCategory());
		}
		
		if(dto.getTraderArea()!=null)
		{
			guide.setTraderArea(dto.getTraderArea());
		}else
		{
			guide.setTraderArea("");
		}
		
		System.out.println(dto.getProvinceGuideServiceId());
		if(dto.getProvinceGuideServiceId() > 0)
		{
			MasProvince masProvinceByProvinceId = new MasProvince();
			masProvinceByProvinceId.setProvinceId(dto.getProvinceGuideServiceId());
			guide.setMasProvince(masProvinceByProvinceId);
		}
				
		Person person = guide.getPerson();
		person.setPersonType(PersonType.INDIVIDUAL.getStatus());
//		person.setPersonNationality("ไทย");
		
		
		traderDAO.update(guide);
		personDAO.update(person);
		
	}

	
	
}
