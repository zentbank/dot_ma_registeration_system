package sss.dot.tourism.service.trader;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.mas.OrganizationDAO;
import sss.dot.tourism.dao.registration.CommitteeDAO;
import sss.dot.tourism.dao.registration.PersonDAO;
import sss.dot.tourism.dao.registration.RegistrationDAO;
import sss.dot.tourism.dao.registration.TraderAddressDAO;
import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.domain.Committee;
import sss.dot.tourism.domain.Organization;
import sss.dot.tourism.domain.Person;
import sss.dot.tourism.domain.Registration;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.domain.TraderAddress;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.LicenseStatus;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.PersonType;
import sss.dot.tourism.util.RecordStatus;
import sss.dot.tourism.util.RegistrationType;
import sss.dot.tourism.util.TraderAddressType;
import sss.dot.tourism.util.TraderCategory;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.User;

@Repository("traderService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class TraderService implements ITraderService {

	@Autowired
	TraderDAO traderDAO;
	@Autowired
	TraderAddressDAO traderAddressDAO;
	@Autowired
	PersonDAO personDAO;
	@Autowired
	CommitteeDAO committeeDAO;
	@Autowired
	RegistrationDAO registrationDAO;
	
	@Autowired
	OrganizationDAO organizationDAO;
	
	public List getTraderAll(Object object, User user) throws Exception {

		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		RegistrationDTO params = (RegistrationDTO)object;
		
		params.setRecordStatus(RecordStatus.NORMAL.getStatus());
		
		if((params.getRoleAction() != null) && (params.getRoleAction().equals("deactivate")))
		{
			params.setOrgId(user.getUserData().getOrgId());
		}
		
		List<Trader>  listTrader = (List<Trader>)this.traderDAO.findAllTrader(params);
		if(!listTrader.isEmpty())
		{
			for(Trader trader: listTrader)
			{
				RegistrationDTO dto = new RegistrationDTO();
				ObjectUtil.copy(trader, dto);
				
				dto.setLicenseStatusName(LicenseStatus.getMeaning(dto.getLicenseStatus()));

				Person person = trader.getPerson();
				
				ObjectUtil.copy(person,dto);
				
				if(PersonType.CORPORATE.getStatus().equals(person.getPersonType()))
				{				
					
					String traderOwnerName = person.getMasPrefix()==null?"":person.getMasPrefix().getPrefixName() + person.getFirstName() + (person.getMasPosfix()==null?"":person.getMasPosfix().getPostfixName());
					dto.setTraderOwnerName(traderOwnerName);
				}
				else
				{
					String traderOwnerName =  person.getMasPrefix()==null?"":person.getMasPrefix().getPrefixName() + person.getFirstName() + " " + person.getLastName();
					dto.setTraderOwnerName(traderOwnerName);
					
				}	
				
				//Oat Add
				if(dto.getTraderType().equals(TraderType.TOUR_COMPANIES.getStatus()))
				{
					dto.setTraderTypeName(TraderType.TOUR_COMPANIES.getMeaning());
				}
					
				if(dto.getTraderType().equals(TraderType.GUIDE.getStatus()))
				{
					dto.setTraderTypeName(TraderType.GUIDE.getMeaning());
					dto.setTraderName(dto.getTraderOwnerName());
				}
					
				if(dto.getTraderType().equals(TraderType.LEADER.getStatus()))
				{
					dto.setTraderTypeName(TraderType.LEADER.getMeaning());
					dto.setTraderName(dto.getTraderOwnerName());
				}
					
				//End Oat Add
				
				dto.setTraderCategoryName(
						TraderCategory.getTraderCategory(
							dto.getTraderType()
							, dto.getTraderCategory()
						).getMeaning()
				);
				
				if((params.getRoleAction() != null) && (params.getRoleAction().equals("deactivate")))
				{
				
					List<TraderAddress> listAddr = this.traderAddressDAO.findAllByTrader(dto.getTraderId()
							, dto.getTraderType()=="B"?TraderAddressType.OFFICE_ADDRESS.getStatus():TraderAddressType.SHOP_ADDRESS.getStatus()
							, RecordStatus.NORMAL.getStatus());
					
					if(!listAddr.isEmpty()){
						TraderAddress addr = listAddr.get(0);
						
						if(addr.getMasProvince() != null){
							
							if(user.getUserData().getOrgId() != addr.getMasProvince().getOrgId()){
								Organization orgi = (Organization)this.organizationDAO.findByPrimaryKey(addr.getMasProvince().getOrgId());
								throw new IllegalAccessException("ไม่สามารถทำรายการได้ เนื่องจาก ใบอนุญาตเลขที่ " + dto.getLicenseNo() + " อยู่ในความรับผิดชอบของ " + orgi.getOrgName());
							}
						}
					}
				}
				
				
				list.add(dto);
			}
			
		}
		
		return list;
	}
	
	public List getTraderByLicenseNo(Object object, User user) throws Exception {

		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		RegistrationDTO params = (RegistrationDTO)object;
		
		List<Trader>  listTrader = (List<Trader>)this.traderDAO.findTraderByLicenseNo(params.getLicenseNo(), params.getTraderType(), RecordStatus.NORMAL.getStatus(), user.getUserData().getOrgId());
		if(!listTrader.isEmpty())
		{
			for(Trader trader: listTrader)
			{
				RegistrationDTO dto = new RegistrationDTO();
				ObjectUtil.copy(trader, dto);

				Person person = trader.getPerson();
				
				ObjectUtil.copy(person,dto);
				
				if(PersonType.CORPORATE.getStatus().equals(person.getPersonType()))
				{

					String traderOwnerName = person.getMasPrefix()==null?"":person.getMasPrefix().getPrefixName() + person.getFirstName() + (person.getMasPosfix()==null?"":person.getMasPosfix().getPostfixName());
					dto.setTraderOwnerName(traderOwnerName);
				}
				else
				{
					String traderOwnerName =  person.getMasPrefix()==null?"":person.getMasPrefix().getPrefixName() + person.getFirstName() + " " + person.getLastName();
					dto.setTraderOwnerName(traderOwnerName);
				}	
				
				dto.setTraderCategoryName(
						TraderCategory.getTraderCategory(
							dto.getTraderType()
							, dto.getTraderCategory()
						).getMeaning()
				);
				
				list.add(dto);
			}
			
		}
		
		return list;
	}
	
	public void updateLicense (Object object, User user) throws Exception
	{
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		RegistrationDTO params = (RegistrationDTO)object;
		
		List<Trader>  listTrader = (List<Trader>)this.traderDAO.findTraderByLicenseNo(params.getLicenseNo(), params.getTraderType(), RecordStatus.NORMAL.getStatus(), 0);
		List<Trader>  listTraderH = (List<Trader>)this.traderDAO.findTraderByLicenseNo(params.getLicenseNo(), params.getTraderType(), RecordStatus.HISTORY.getStatus(), 0);
		
		listTrader.addAll(listTraderH);
		
		if(!listTrader.isEmpty())
		{
			for(Trader trader: listTrader)
			{
				Person person = trader.getPerson();
				if(params.getPunishmentDate() != null)
				{
					trader.setPunishmentDate(params.getPunishmentDate());
					person.setPunishmentDate(params.getPunishmentDate());
					
				}
				if(params.getPunishmentExpireDate() != null)
				{
					trader.setPunishmentExpireDate(params.getPunishmentExpireDate());
					person.setPunishmentExpireDate(params.getPunishmentExpireDate());
				}
				trader.setLicenseStatus(params.getLicenseStatus());
				trader.setLastUpdUser(user.getUserName());
				trader.setLastUpdDtm(new Date());
				
				this.traderDAO.update(trader);
				
				person.setPersonStatus(params.getLicenseStatus());
				person.setLastUpdUser(user.getUserName());
				person.setLastUpdDtm(new Date());
				
				this.personDAO.update(person);
				
				List<Committee> listComm =  (List<Committee>)this.committeeDAO.findAllByPerson(person.getPersonId(), null, null);
				
				if(!listComm.isEmpty())
				{
					for(Committee comm: listComm)
					{
						
						if(params.getPunishmentDate() != null)
						{
							comm.setPunishmentDate(params.getPunishmentDate());
						}
						
						if(params.getPunishmentExpireDate() != null)
						{
							comm.setPunishmentExpireDate(params.getPunishmentExpireDate());
						}
						
						comm.setCommitteeStatus(params.getLicenseStatus());
						comm.setLastUpdUser(user.getUserName());
						comm.setLastUpdDtm(new Date());
						
						this.committeeDAO.update(comm);
					}
				}
			}
		}		
	}

	public void suspend(Object object, User user) throws Exception {
		
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		RegistrationDTO params = (RegistrationDTO)object;
		params.setLicenseStatus(LicenseStatus.SUSPENSION.getStatus());
		
		this.updateLicense(params, user);
		
	}

	public void unSuspend(Object object, User user) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		RegistrationDTO params = (RegistrationDTO)object;
		params.setLicenseStatus(LicenseStatus.NORMAL.getStatus());
		this.updateLicense(params, user);
		
	}

	public void revoke(Object object, User user) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		RegistrationDTO params = (RegistrationDTO)object;
		params.setLicenseStatus(LicenseStatus.REVOKE.getStatus());
		
		this.updateLicense(params, user);
		
	}

	public void unRevoke(Object object, User user) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		RegistrationDTO params = (RegistrationDTO)object;
		params.setLicenseStatus(LicenseStatus.NORMAL.getStatus());
		this.updateLicense(params, user);
		
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
	public void deActivate(Object object, User user) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		RegistrationDTO params = (RegistrationDTO)object;
		params.setLicenseStatus(LicenseStatus.CANCEL.getStatus());
		
		this.updateLicense(params, user);
		
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
	public void activate(Object object, User user) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		RegistrationDTO params = (RegistrationDTO)object;
		params.setLicenseStatus(LicenseStatus.NORMAL.getStatus());
		this.updateLicense(params, user);
		
	}

	public List getTraderAllName(Object object, User user) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		DateFormat ft = DateUtils.getProcessDateFormatThai();
		RegistrationDTO params = (RegistrationDTO)object;
		
		//params.setRecordStatus(RecordStatus.NORMAL.getStatus());
		
		List<Trader>  listTrader = (List<Trader>)this.traderDAO.findAllTraderName(params);
		if(!listTrader.isEmpty())
		{
			for(Trader trader: listTrader)
			{
				RegistrationDTO dto = new RegistrationDTO();
				ObjectUtil.copy(trader, dto);
				System.out.println(dto.getTraderId());
				//dto.setLicenseStatusName(LicenseStatus.getMeaning(dto.getLicenseStatus()));

				Person person = trader.getPerson();
				
				ObjectUtil.copy(person,dto);
				
				if(PersonType.CORPORATE.getStatus().equals(person.getPersonType()))
				{					
					String traderOwnerName = person.getMasPrefix()==null?"":person.getMasPrefix().getPrefixName() + person.getFirstName() + (person.getMasPosfix()==null?"":person.getMasPosfix().getPostfixName());
					dto.setTraderOwnerName(traderOwnerName);
				}
				else
				{
					String traderOwnerName =  person.getMasPrefix()==null?"":person.getMasPrefix().getPrefixName() + person.getFirstName() + " " + person.getLastName();
					dto.setTraderOwnerName(traderOwnerName);
				}	
				
				if(dto.getTraderCategory() !=null && !dto.getTraderCategory().equals(""))
				{
				dto.setTraderCategoryName(
						TraderCategory.getTraderCategory(
							dto.getTraderType()
							, dto.getTraderCategory()
						).getMeaning()
				);
				}
				/*List<TraderAddress> listTraderAddress =  (List<TraderAddress>)this.traderAddressDAO.findAllByTraderName(dto.getTraderId(),TraderAddressType.SHOP_ADDRESS.getStatus(), RecordStatus.TEMP.getStatus());

				if(!listTraderAddress.isEmpty())
				{
					TraderAddress add = listTraderAddress.get(0);
				StringBuilder traderAddress = new StringBuilder();
			
				traderAddress.append("ตั้งอยู่เลขที่ ");
				traderAddress.append(add.getAddressNo());
				traderAddress.append(" ถนน ");
				traderAddress.append(add.getRoadName());
				traderAddress.append(" ตรอก/ซอย ");
				traderAddress.append(add.getSoi());
					
					if(add.getMasTambol() != null)
					{
						traderAddress.append(" ตำบล/แขวง ");
						traderAddress.append(add.getMasTambol().getTambolName());
					}
					if(add.getMasAmphur()!= null)
					{
						traderAddress.append(" อำเภอ/เขต ");
						traderAddress.append(add.getMasAmphur().getAmphurName());
					}
					if(add.getMasProvince() != null)
					{
						traderAddress.append(" จังหวัด ");
						traderAddress.append(add.getMasProvince().getProvinceName());
					}
					
					
					traderAddress.append(" รหัสไปรษณีย์ ");
					traderAddress.append(add.getPostCode()==null?"":add.getPostCode());
					
					dto.setTraderAddress(traderAddress.toString());
				}*/
				if(trader.getEffectiveDate()!=null && !trader.getEffectiveDate().equals(""))
				{
					dto.setEffectiveDate(ft.format(trader.getEffectiveDate()));
				}
				
				if(trader.getExpireDate()!=null && !trader.getExpireDate().equals(""))
				{
					dto.setExpireDate(ft.format(trader.getExpireDate()));
				}
				
				list.add(dto);
			}
			
		}
		
		return list;
	
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void updateGuide() throws Exception {
		
		try{
			List<Object[]> listLicenseNo = this.traderDAO.findAllLicenseGuide();
			
			if(!listLicenseNo.isEmpty())
			{
				for(Object[] licenseNo: listLicenseNo)
				{
					if((null != licenseNo[0]) && (!licenseNo[0].toString().isEmpty()))
					{
						List<Trader> listTrader = (List<Trader>) this.traderDAO.findLicense(licenseNo[0].toString(), TraderType.GUIDE.getStatus());
						
						if(!listTrader.isEmpty())
						{
							for(int i = 0; i < listTrader.size(); i++)
							{
								String registrationType = RegistrationType.GUIDE_NEW_REGISTRATION.getStatus();
								if(i != 0)
								{
									registrationType = RegistrationType.GUIDE_RENEW_REGISTRATION.getStatus();
								}
								else
								{
									registrationType = RegistrationType.GUIDE_NEW_REGISTRATION.getStatus();
								}
								
								Trader trader = listTrader.get(i);
								
								List<Registration> listOldRegistration = this.registrationDAO.findAllByTrader(trader.getTraderId());
								
								if(!listOldRegistration.isEmpty())
								{
									for(Registration oldReg: listOldRegistration)
									{
										Trader t200001 = new Trader();
										t200001.setTraderId(200001);
										oldReg.setTrader(t200001);
										oldReg.setRecordStatus("D");
										oldReg.setLastUpdUser("Shaii");
										oldReg.setLastUpdDtm(new Date());
										
										this.registrationDAO.update(oldReg);
										
									}
								}
								
								Registration reg = new Registration();
								reg.setOrganization(trader.getOrganization());
								reg.setTrader(trader);
								reg.setRegistrationType(registrationType);
								reg.setApproveDate(trader.getEffectiveDate());
								reg.setRegistrationDate(trader.getEffectiveDate());
								reg.setApproveStatus("A");
								reg.setCreateUser("Shaii");
								reg.setCreateDtm(new Date());
								reg.setRecordStatus(RecordStatus.NORMAL.getStatus());
								this.registrationDAO.insert(reg);
								
							}
						}
					}
				}
			}
			
		}catch(Exception e)
		{
			e.printStackTrace();
			throw new IllegalAccessError(e.getMessage());
		}
		
		
		
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void updateTour()
	{
		try{
			List<Object[]> listLicenseNo = this.traderDAO.findLicenseNoAll();
			
			if(!listLicenseNo.isEmpty())
			{
				for(Object[] licenseNo: listLicenseNo)
				{
					if((null != licenseNo[0]) && (!licenseNo[0].toString().isEmpty()))
					{
						List<Trader> listTrader = (List<Trader>) this.traderDAO.findLicense(licenseNo[0].toString(), TraderType.TOUR_COMPANIES.getStatus());
						
						if(!listTrader.isEmpty())
						{
							for(int i = 0; i < listTrader.size(); i++)
							{
								String registrationType = RegistrationType.TOUR_COMPANIES_NEW_REGISTRATION.getStatus();
								if(i != 0)
								{
									registrationType = RegistrationType.TOUR_COMPANIES_RENEW_REGISTRATION.getStatus();
								}
								else
								{
									registrationType = RegistrationType.TOUR_COMPANIES_NEW_REGISTRATION.getStatus();
								}
								
								Trader trader = listTrader.get(i);
								
								List<Registration> listOldRegistration = this.registrationDAO.findAllByTrader(trader.getTraderId());
								
								if(!listOldRegistration.isEmpty())
								{
									for(Registration oldReg: listOldRegistration)
									{
										Trader t200001 = new Trader();
										t200001.setTraderId(200001);
										oldReg.setTrader(t200001);
										oldReg.setRecordStatus("D");
										oldReg.setLastUpdUser("Shaii");
										oldReg.setLastUpdDtm(new Date());
										
										this.registrationDAO.update(oldReg);
										
									}
								}
								
								Registration reg = new Registration();
								reg.setOrganization(trader.getOrganization());
								reg.setTrader(trader);
								reg.setRegistrationType(registrationType);
								reg.setApproveDate(trader.getEffectiveDate());
								reg.setRegistrationDate(trader.getEffectiveDate());
								reg.setApproveStatus("A");
								reg.setCreateUser("Shaii");
								reg.setCreateDtm(new Date());
								reg.setRecordStatus(RecordStatus.NORMAL.getStatus());
								this.registrationDAO.insert(reg);
								
							}
						}
					}
				}
			}
			
		}catch(Exception e)
		{
			e.printStackTrace();
			throw new IllegalAccessError(e.getMessage());
		}
	}

	@Override
	public String getTraderOwnerName(Person person) throws Exception {
		String traderOwnerName = "";
		
		if (PersonType.CORPORATE.getStatus().equals(person.getPersonType())) {

			traderOwnerName = person.getMasPrefix() == null ? ""
					: person.getMasPrefix().getPrefixName()
							+ person.getFirstName()
							+ (person.getMasPosfix() == null ? "" : person
									.getMasPosfix().getPostfixName());
		} else {
			traderOwnerName = person.getMasPrefix() == null ? ""
					: person.getMasPrefix().getPrefixName()
							+ person.getFirstName() + " "
							+ person.getLastName();
		}
		return traderOwnerName;
	}

	
	
}
