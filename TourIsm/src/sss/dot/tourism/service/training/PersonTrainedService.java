package sss.dot.tourism.service.training;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.mas.MasAmphurDAO;
import sss.dot.tourism.dao.mas.MasEducationLevelDAO;
import sss.dot.tourism.dao.mas.MasProvinceDAO;
import sss.dot.tourism.dao.mas.MasTambolDAO;
import sss.dot.tourism.dao.mas.MasUniversityDAO;
import sss.dot.tourism.dao.training.EducationDAO;
import sss.dot.tourism.dao.training.PersonTrainedDAO;
import sss.dot.tourism.domain.Education;
import sss.dot.tourism.domain.MasAmphur;
import sss.dot.tourism.domain.MasEducationLevel;
import sss.dot.tourism.domain.MasProvince;
import sss.dot.tourism.domain.MasUniversity;
import sss.dot.tourism.domain.PersonTrained;
import sss.dot.tourism.dto.training.EducationDTO;
import sss.dot.tourism.util.ObjectUtil;

import com.sss.aut.service.User;

@Repository("personTrainedService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PersonTrainedService implements IPersonTrainedService {

	@Autowired
	private MasProvinceDAO masProvinceDAO;
	@Autowired
	private MasAmphurDAO masAmphurDAO;
	@Autowired
	private MasTambolDAO masTambolDAO;
	@Autowired
	private PersonTrainedDAO personTrainedDAO;
	@Autowired
	private EducationDAO educationDAO;
	@Autowired
	private MasEducationLevelDAO masEducationLevelDAO;
	@Autowired
	private MasUniversityDAO masUniversityDAO;
	
	
	public List getAll(Object object, User user) throws Exception {
		
		if(!(object instanceof EducationDTO))
		{
			throw new IllegalArgumentException("object not instanceof PersonTrainedDTO");
		}
		EducationDTO param = (EducationDTO)object;
		
		List<EducationDTO> list = new ArrayList<EducationDTO>();
		
		List<PersonTrained> listAll = (List<PersonTrained>)this.personTrainedDAO.findPersonTrainedAll(param);
		
		if(!listAll.isEmpty())
		{
			for(PersonTrained trained: listAll)
			{
				EducationDTO dto = new EducationDTO();
				
				ObjectUtil.copy(trained, dto);
				
				if(trained.getEducation() != null)
				{
					ObjectUtil.copy(trained.getEducation(), dto);
				}
				
			
				
				if(trained.getMasAmphurByAmphurId() != null)
				{
					dto.setAmphurId(trained.getMasAmphurByAmphurId().getAmphurId());
				}
				
				if(trained.getMasAmphurByTaxAmphurId() != null)
				{
					dto.setTaxAmphurId(trained.getMasAmphurByTaxAmphurId().getAmphurId());
				}
				if(trained.getMasProvinceByProvinceId() != null)
				{
					dto.setProvinceId(trained.getMasProvinceByProvinceId().getProvinceId());
				}
				if(trained.getMasProvinceByTaxProvinceId() != null)
				{
					dto.setTaxProvinceId(trained.getMasProvinceByTaxProvinceId().getProvinceId());
				}
				if(trained.getEducation().getMasUniversity() != null)
				{
					dto.setMasUniversityId(trained.getEducation().getMasUniversity().getMasUniversityId());
					dto.setUniversityName(trained.getEducation().getMasUniversity().getUniversityName());
				}
				if(trained.getEducation().getMasEducationLevel() != null)
				{
					dto.setMasEducationLevelId(trained.getEducation().getMasEducationLevel().getMasEducationLevelId());
					dto.setEducationLevelName(trained.getEducation().getMasEducationLevel().getEducationLevelName());
				}
				if(trained.getMasPrefix() != null)
				{
					dto.setPrefixId(trained.getMasPrefix().getPrefixId());
					dto.setPrefixName(trained.getMasPrefix().getPrefixName());
				}
				
				list.add(dto);
			}
		}
		
		return list;
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List create(Object[] object, User user) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void update(Object[] object, User user) throws Exception {
		// TODO Auto-generated method stub

	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(Object[] object, User user) throws Exception {
		// TODO Auto-generated method stub

	}

}
