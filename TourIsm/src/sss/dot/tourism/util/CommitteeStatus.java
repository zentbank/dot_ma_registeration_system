package sss.dot.tourism.util;


/*
 * ประเภทกรรมการบริษัท
N=normal ปกติ 
S=suspension ระหว่างพักใช้ใบอนุญาต
R=revoke ระหว่างเพิกถอน
*/
public enum CommitteeStatus {
	NORMAL("N","ปกติ"),
	SUSPENSON("S","ระหว่างพักใช้ใบอนุญาต"),
	REVOKE("R","ระหว่างเพิกถอนใบอนุญาต");
	
	private final String status;
	private final String meaning;

	CommitteeStatus(String status,String meaning){
		this.status = status;
		this.meaning = meaning;
	}

	public String getStatus() {
		return status;
	}

	public String getMeaning() {
		return meaning;
	}
	/**
	 * overloading
	 * @param flag
	 * @return
	 */
	public boolean equals(String status){
		CommitteeStatus eStatus =  getCommitteeStatus(status);
		return super.equals(eStatus);
	}
	public static CommitteeStatus getCommitteeStatus(String status) {
		CommitteeStatus record = null;

		
		if(NORMAL.getStatus().equals(status)){
			record = NORMAL;
		}
		else if(SUSPENSON.getStatus().equals(status)){
			record = SUSPENSON;
		}
		else if(REVOKE.getStatus().endsWith(status)){
			record = REVOKE;
		}
		
		return record;
	}
	public static String getMeaning(String status) {
		return getCommitteeStatus(status).getMeaning();
	}
	
}
