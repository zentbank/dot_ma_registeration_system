package sss.dot.tourism.util;

public  class ConstantUtil {
	
	public final static String ROOT_PATH = "\\\\122.155.9.62\\Database\\documents"+java.io.File.separator;
//	public final static String ROOT_PATH = "/Users/Shaii/Documents/workingfolder/TourismDepartment/DOT_PRINT_CARD/pic/";

	
	
	public static String TEMPLATE_GUIDE_FILE_LOCATION = "\\\\122.155.9.62\\Database\\documents"+java.io.File.separator + "resources"+java.io.File.separator +"cardtemplate"+java.io.File.separator;
//	public static String TEMPLATE_GUIDE_FILE_LOCATION = "/Users/Shaii/Documents/workingfolder/TourismDepartment/base64" +java.io.File.separator;

	public static String QR_TEMP_LOCATION = "\\\\122.155.9.62\\Database\\documents"+java.io.File.separator + "resources"+java.io.File.separator +"temp"+java.io.File.separator;
//	public static String QR_TEMP_LOCATION = "/Users/Shaii/Documents/workingfolder/TourismDepartment/base64" +java.io.File.separator;
	
	public final static String IMG_ROOT_PATH = ROOT_PATH;
	public final static String VIEW_PUNISH_URL = "http://122.155.9.61:8087/mobiletourguide/info/punishment/evidence/viewfile/";
	
	public final static String PRINT_PAYMENT = "<a href=\"http://122.155.9.61:8087/mobiletourguide/info/renew/license/request/{0}\">คลิกที่นี่เพื่อพิมพ์ใบชำระหนี้</a>";
	
	public final static String MAIL_TOURISM = "tourism.go.th@gmail.com";
	public final static String MAIL_PASSWORD = "sss123456";
	
	public final static long GUIDE_DOC_IMG_ID = 101;
	public final static long TOURLEADER_DOC_IMG_ID = 107;
	
	public final static String CARD_TEMPLATE = "business/printcard/registration/img/template/";

	public final static String VIEW_LICENSE_PROFILE_URL = "http://122.155.9.61:8087/mobiletourguide/info/license/LicenseType/{0}/LicNo/{1}";
	
	public final static int DIRECTOR_POSITION = 4;
}