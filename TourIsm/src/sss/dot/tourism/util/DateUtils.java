package sss.dot.tourism.util;





import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;



public class DateUtils
{
  private static final Locale THAI_LOCALE = new Locale("TH", "th");

  private static final Locale ENGLISH_LOCALE = Locale.ENGLISH;

  public static DateFormat getProcessDateFormat()
  {
    return new SimpleDateFormat("yyyy-MM-dd", THAI_LOCALE);
  }

  public static DateFormat getProcessDateFormatEng()
  {
    return new SimpleDateFormat("yyyy-MM-dd", ENGLISH_LOCALE);
  }

  public static DateFormat getProcessDateFormatEngReverse()
  {
    return new SimpleDateFormat("dd-MM-yyyy", ENGLISH_LOCALE);
  }

  public static DateFormat getProcessDateFormatThai()
  {
    return new SimpleDateFormat("dd/MM/yyyy", THAI_LOCALE);
  }

  public static DateFormat getProcessDateFormatEn()
  {
    return new SimpleDateFormat("dd/MM/yyyy", ENGLISH_LOCALE);
  }
  
  public static DateFormat getProcessDateFormatEngMonth()
  {
    return new SimpleDateFormat("MM/dd/yyyy", ENGLISH_LOCALE);
  }

  public static DateFormat getProcessDateFormatThaiSingle()
  {
    return new SimpleDateFormat("d/M/yyyy", THAI_LOCALE);
  }

  public static DateFormat getDisplayDateFormat()
  {
    return new SimpleDateFormat("d MMMM yyyy", THAI_LOCALE);
  }

  public static DateFormat getDisplayDateFormatTH()
  {
    return new SimpleDateFormat("dd MMMM yyyy", THAI_LOCALE);
  }

  public static DateFormat getOracleDateFormat()
  {
    return new SimpleDateFormat("dd-MMM-yyyy", ENGLISH_LOCALE);
  }

  public static DateFormat getShortDateFormat()
  {
    return new SimpleDateFormat("d MMM yyyy", THAI_LOCALE);
  }
  public static DateFormat getDisplayDateFormatEn()
  {
    return new SimpleDateFormat("dd MMMMMM yyyy", ENGLISH_LOCALE);
  }
  
  public static DateFormat getPersistentDateFormat()
  {
    return new SimpleDateFormat("yyyy-MM-dd");
  }

  public static DateFormat getPersistentTimeFormat()
  {
    return new SimpleDateFormat("HH:mm:ss");
  }

  public static DateFormat getTimeFormat()
  {
    return new SimpleDateFormat("HH:mm");
  }

  public static DateFormat getMonthNameFormat()
  {
    return new SimpleDateFormat("MMMM", THAI_LOCALE);
  }

  public static DateFormat getMonthCodeFormat()
  {
    return new SimpleDateFormat("MM");
  }

  public static DateFormat getBuddhaYearFormat()
  {
    return new SimpleDateFormat("yyyy", THAI_LOCALE);
  }

  public static DateFormat getDayFormat()
  {
    return new SimpleDateFormat("d", THAI_LOCALE);
  }

  public static DateFormat getYearFormat()
  {
    return new SimpleDateFormat("yyyy", ENGLISH_LOCALE);
  }
  public static DateFormat getDayFormatEn()
  {
    return new SimpleDateFormat("d", ENGLISH_LOCALE);
  }
  public static String getSysdate()
  {
    return getProcessDateFormatEngReverse().format(new Date());
  }

  /**
   * @deprecated We've already changed display date format to
   *             "dd/MM/yyyy".Please use
   *             {@link DateUtils#getProcessDateFormatThai()} instead
   * @return
   */
  @Deprecated
  public static DateFormat getExtJSOutputDate()
  {
    return new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
  }

  /**
   * @deprecated We've already changed display date format to
   *             "dd/MM/yyyy".Please use
   *             {@link DateUtils#getProcessDateFormatThai()} instead
   * @return
   */
  @Deprecated
  public static DateFormat getExtJSInputDate()
  {
    return new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
  }
  
  public static DateFormat getProcessDateFormatTextEn()
  {
    return new SimpleDateFormat("ddMMyyyy", ENGLISH_LOCALE);
  }
  
  public static String getDayName(String value)
  {
    String result = null;
    if (value != null)
    {
      switch (Integer.parseInt(value.trim()))
      {
      case 1:
        result = "\u0e08\u0e31\u0e19\u0e17\u0e23\u0e4c";
        break;
      case 2:
        result = "\u0e2d\u0e31\u0e07\u0e04\u0e32\u0e23";
        break;
      case 3:
        result = "\u0e1e\u0e38\u0e18";
        break;
      case 4:
        result = "\u0e1e\u0e24\u0e2b\u0e31\u0e2a\u0e1a\u0e14\u0e35";
        break;
      case 5:
        result = "\u0e28\u0e38\u0e01\u0e23\u0e4c";
        break;
      case 6:
        result = "\u0e40\u0e2a\u0e32\u0e23\u0e4c";
        break;
      case 7:
        result = "\u0e2d\u0e32\u0e17\u0e34\u0e15\u0e22\u0e4c";
        break;
      default:
        result = "";
      }// End switch
    }// End if
    return result;
  }
  
  public static String getMonthName(String value)
  {
    String result = null;
    if (value != null)
    {
      switch (Integer.parseInt(value.trim()))
      {
      case 1:
        result = "\u0e21\u0e01\u0e23\u0e32\u0e04\u0e21";
        break;
      case 2:
        result = "\u0e01\u0e38\u0e21\u0e20\u0e32\u0e1e\u0e31\u0e19\u0e18\u0e4c";
        break;
      case 3:
        result = "\u0e21\u0e35\u0e19\u0e32\u0e04\u0e21";
        break;
      case 4:
        result = "\u0e40\u0e21\u0e29\u0e32\u0e22\u0e19";
        break;
      case 5:
        result = "\u0e1e\u0e24\u0e29\u0e20\u0e32\u0e04\u0e21";
        break;
      case 6:
        result = "\u0e21\u0e34\u0e16\u0e38\u0e19\u0e32\u0e22\u0e19";
        break;
      case 7:
        result = "\u0e01\u0e23\u0e01\u0e0e\u0e32\u0e04\u0e21";
        break;
      case 8:
        result = "\u0e2a\u0e34\u0e07\u0e2b\u0e32\u0e04\u0e21";
        break;
      case 9:
        result = "\u0e01\u0e31\u0e19\u0e22\u0e32\u0e22\u0e19";
        break;
      case 10:
        result = "\u0e15\u0e38\u0e25\u0e32\u0e04\u0e21";
        break;
      case 11:
        result = "\u0e1e\u0e24\u0E28\u0e08\u0e34\u0e01\u0e32\u0e22\u0e19";
        break;
      case 12:
        result = "\u0e18\u0e31\u0e19\u0e27\u0e32\u0e04\u0e21";
        break;
      default:
        result = "";
      }// End switch
    }// End if
    return result;
  }
  
  
  public static String getMonthShotName(String value)
  {
    String result = null;
    if (value != null)
    {
      switch (Integer.parseInt(value.trim()))
      {
      case 1:
        result = "\u0e21.\u0e04.";
        break;
      case 2:
        result = "\u0e01.\u0e1e.";
        break;
      case 3:
        result = "\u0e21\u0e35.\u0e04.";
        break;
      case 4:
        result = "\u0e40\u0e21.\u0e22.";
        break;
      case 5:
        result = "\u0e1e.\u0e04.";
        break;
      case 6:
        result = "\u0e21\u0e34.\u0e22.";
        break;
      case 7:
        result = "\u0e01.\u0e04.";
        break;
      case 8:
        result = "\u0e2a.\u0e04.";
        break;
      case 9:
        result = "\u0e01.\u0e22.";
        break;
      case 10:
        result = "\u0e15.\u0e04.";
        break;
      case 11:
        result = "\u0e1e.\u0e22.";
        break;
      case 12:
        result = "\u0e18.\u0e04.";
        break;
      default:
        result = "";
      }// End switch
    }// End if
    return result;
  }
  public static String getDateWithMonthName(String str)
  {
    String day = str.substring(0, 2);
    String month = str.substring(3, 5);
    
    String[] date = str.split("/");
    month = getMonthName(date[1]);
    String year = str.substring(6);    
    String dateWithMonth = date[0] + " " + month + " " + date[2];
    return dateWithMonth;
  }
  public static String getDateWithMonthShotName(String str)
  {
    String day = str.substring(0, 2);
    String month = str.substring(3, 5);
    month = getMonthShotName(month);
    String year = str.substring(6);    
    String date = day + " " + month + " " + year;
    return date;
  }
  public static long DateDiff(Date todate ,Date fromdate) {
   
    // Get msec from each, and subtract.
    long diff = todate.getTime() - fromdate.getTime();
    
    diff = diff / (1000 * 60 * 60 * 24) ;

    System.out.println("diff is " + diff );
    
    return diff;
  }
  
  public static String getDateMSSQLFormat(String dateParam, String timeParam) throws Exception
  {
	     String[] timeSp = timeParam.split(":");
	    
	     
//	     System.out.println("since = " + timeSp[0]+":"+ timeSp[1]);
	    
	    
		  DateFormat df = getProcessDateFormatThai();
		  Date date = df.parse(dateParam);
		  
//	  System.out.println(date);
		  
		  String dateStr = DateUtils.getProcessDateFormatEng().format(date);
		  dateStr = dateStr +" "+ timeParam +":00";
		  
		  
//		  2014-7-22 20:40:0
		  
//		  System.out.println(dateStr);
		  
//		  Calendar calDtm = Calendar.getInstance(Locale.ENGLISH);
//		  
//		  Calendar today = Calendar.getInstance();
//		  
//		  System.out.println(calDtm.get(Calendar.YEAR));
//		  System.out.println(today.get(Calendar.YEAR));
//		  
//		  if(calDtm.get(Calendar.YEAR) > today.get(Calendar.YEAR))
//		  {
//			  calDtm.set(Calendar.YEAR, today.get(Calendar.YEAR));
//		  }
//
//		   
//		  calDtm.setTime(date);
//		  calDtm.set(Calendar.MILLISECOND,  0  );
//		  calDtm.set(Calendar.SECOND,  0);
//		  calDtm.set(Calendar.MINUTE,  Integer.parseInt(timeSp[1]) );
//		  calDtm.set(Calendar.HOUR_OF_DAY,  Integer.parseInt(timeSp[0]) );
//		  
//	    String fromDateStr = "{0}-{1}-{2} {3}:{4}:{5}";
//	    String fromDateParams  = MessageFormat.format(fromDateStr 
//	    		, calDtm.get(Calendar.YEAR)+ ""
//	    		, (calDtm.get(Calendar.MONTH) +1) + ""
//	    		, calDtm.get(Calendar.DATE)+ ""
//	    		, calDtm.get(Calendar.HOUR_OF_DAY)+ ""
//	    		,  calDtm.get(Calendar.MINUTE)+ ""
//	    		,  calDtm.get(Calendar.SECOND)+ "");
//		  
//	    System.out.println("format date = " + fromDateParams);
		return dateStr;
  }
  
  public static void main(String arg[])
  {
	  try{
		  System.out.println(getMonthName("8"));
	  }catch(Exception e){
		  e.printStackTrace();
	  }
//	  String ddd = getDateWithMonthName("05/08/2555");
//	  
//	  
//	  System.out.println(getMonthShotName("8"));
//	  
//	  System.out.println(ddd);
	  
	  
  }
}
