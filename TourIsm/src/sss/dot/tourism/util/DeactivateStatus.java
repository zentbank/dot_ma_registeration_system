package sss.dot.tourism.util;

public enum DeactivateStatus {
	
	WAIT("W" , "รับเรื่องรออนุมัติ"),
	ACCEPT("A" , "อนุมัติแล้ว"),
	CANCEL("C", "ยกเลิกการยกเลิก");
	
	private final String status;
	private final String meaning;
	
	DeactivateStatus(String status, String meaning){
		this.status = status;
		this.meaning = meaning;
	}
	
	/**
	 * @return the category
	 */
	public String getStatusString() {
		return status.toString();
	}
	public String getStatus() {
		return status;
	}
	/**
	 * @return the meaning
	 */
	public String getMeaning() {
		return meaning;
	}
	/**
	 * overloading
	 * @param flag
	 * @return
	 */
	public boolean equals(String status){
		DeactivateStatus eStatus =  getDeactivateStatus(status);
		return super.equals(eStatus);
	}
	public static DeactivateStatus getDeactivateStatus(String status) {
		DeactivateStatus record = null;
	
		if(WAIT.getStatus().equals(status)){
			record = WAIT;
		}
		else if(ACCEPT.getStatus().equals(status)){
			record = ACCEPT;
		}
		else if(CANCEL.getStatus().equals(status)){
			record = CANCEL;
		}
		
		return record;
	}
	public static String getMeaning(String status) {
		return getDeactivateStatus(status ).getMeaning();
	}
}
