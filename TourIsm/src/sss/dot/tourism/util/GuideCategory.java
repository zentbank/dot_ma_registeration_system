package sss.dot.tourism.util;

public enum GuideCategory {


	PLATINUM_GUIDE("100" , "ประเภท ทั่วไป (ให้บริการนำเที่ยวแก่นักท่องเที่ยวไทยและต่างประเทศ)"),
	GOLD_GUIDE("101" , "ประเภท ทั่วไป (ให้บริการนำเที่ยวเฉพาะนักท่องเที่ยวไทย)"),
	PINK_GUIDE("200" , "ประเภท เฉพาะพื้นที่ (ให้บริการนำเที่ยวแก่นักท่องเที่ยวไทยและต่างประเทศ)"),
	BLUE_GUIDE("201" , "ประเภท เฉพาะพื้นที่ (ให้บริการนำเที่ยวเฉพาะนักท่องเที่ยวไทย)"),
	GREEN_GUIDE("202" , "ประเภท เดินป่า (ให้บริการนำเที่ยวแก่นักท่องเที่ยวไทยและต่างประเทศในเขตป่า)"),
	RED_GUIDE("203" , "ประเภท ศิลปวัฒนธรรม (ให้บริการนำเที่ยวแก่นักท่องเที่ยวไทยและต่างประเทศด้านศิลปวัฒนธรรม)"),
	ORANGE_GUIDE("204" , "ประเภท ทางทะเล (ให้บริการนำเที่ยวแก่นักท่องเที่ยวไทยและต่างประเทศทางทะเล)"),
	YELLOW_GUIDE("205" , "ประเภท ทางทะเลชายฝั่ง (ให้บริการนำเที่ยวแก่นักท่องเที่ยวไทยและต่างประเทศทางทะเลชายฝั่ง ไม่เกิน 40 ไมล์ทะเล)"),
	VIOLET_GUIDE("206" , "ประเภทเฉพาะ แหล่งท่องเที่ยวธรรมชาติ (ให้บริการนำเที่ยวแก่นักท่องเที่ยวไทยและต่างประเทศในแหล่งท่องเที่ยวธรรมชาติ)"),
	BROWN_GUIDE("207" , "ประเภทเฉพาะ วัฒนธรรมท้องถิ่น (ให้บริการนำเที่ยวแก่นักท่องเที่ยวไทยและต่างประเทศด้านวัฒนธรรมท้องถิ่น)");

	

	private final String status;
	private final String meaning;

	GuideCategory(String status,String meaning){
		this.status = status;
		this.meaning = meaning;
	}

	/**
	 * @return the status
	 */
	public String getStatusString() {
		return status.toString();
	}
	public String getStatus() {
		return status;
	}
	/**
	 * @return the meaning
	 */
	public String getMeaning() {
		return meaning;
	}
	/**
	 * overloading
	 * @param flag
	 * @return
	 */
	public boolean equals(String status){
		GuideCategory eStatus =  getGuideCategory(status);
		return super.equals(eStatus);
	}
	public static GuideCategory getGuideCategory(String status) {
		GuideCategory record = null;
		
		
		if(PLATINUM_GUIDE.getStatus().equals(status)){
			record = PLATINUM_GUIDE;
		}
		else if(GOLD_GUIDE.getStatus().equals(status)){
			record = GOLD_GUIDE;
		}
		else if(PINK_GUIDE.getStatus().equals(status)){
			record = PINK_GUIDE;
		}
		else if(BLUE_GUIDE.getStatus().equals(status)){
			record = BLUE_GUIDE;
		}
		else if(GREEN_GUIDE.getStatus().equals(status)){
			record = GREEN_GUIDE;
		}
		else if(RED_GUIDE.getStatus().equals(status)){
			record = RED_GUIDE;
		}
		else if(ORANGE_GUIDE.getStatus().equals(status)){
			record = ORANGE_GUIDE;
		}
		else if(YELLOW_GUIDE.getStatus().equals(status)){
			record = YELLOW_GUIDE;
		}
		else if(VIOLET_GUIDE.getStatus().equals(status)){
			record = VIOLET_GUIDE;
		}
		else if(BROWN_GUIDE.getStatus().equals(status)){
			record = BROWN_GUIDE;
		}
		return record;
	}
	public static String getMeaning(String status) {
		return getGuideCategory(status).getMeaning();
	}
}
