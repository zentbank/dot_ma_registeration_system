package sss.dot.tourism.util;



public enum LicenseFeeStatus  {
	
	DRAFT("1" , "รอบันทึก"),
	WAIT_ATTACH("2" , "รอแนบเอกสาร"),
	CHECK_DOCS("3" , "รอตรวจสอบเอกสาร"),
	REQUIRE_DOCS("4" , "ขอเอกสารเพิ่มเติม"),
	PROGRESS("5","รออนุมัติ"),
	APPROVE("6" , "อนุมัติ"),
	WAIT_PAY("7" , "รอชำระเงิน"),
	PAID("8" , "ชำระเงินแล้ว"),
	FINISH("9","ทำรายการเสร็จสมบูรณ์");
	
	private final String status;
	private final String meaning;
	
	LicenseFeeStatus(String status, String meaning){
		this.status = status;
		this.meaning = meaning;
	}
	
	/**
	 * @return the category
	 */
	public String getStatusString() {
		return status.toString();
	}
	public String getStatus() {
		return status;
	}
	/**
	 * @return the meaning
	 */
	public String getMeaning() {
		return meaning;
	}
	/**
	 * overloading
	 * @param flag
	 * @return
	 */
	public boolean equals(String status){
		LicenseFeeStatus eStatus =  getLicenseStatus(status);
		return super.equals(eStatus);
	}
	public static LicenseFeeStatus getLicenseStatus(String status) {
		LicenseFeeStatus record = null;

		if(DRAFT.getStatus().equals(status)){
			record = DRAFT;
		}
		else if(WAIT_ATTACH.getStatus().equals(status)){
			record = WAIT_ATTACH;
		}
		else if(CHECK_DOCS.getStatus().equals(status)){
			record = CHECK_DOCS;
		}
		else if(REQUIRE_DOCS.getStatus().equals(status)){
			record = REQUIRE_DOCS;
		}
		else if(PROGRESS.getStatus().equals(status)){
			record = PROGRESS;
		}
		else if(APPROVE.getStatus().equals(status)){
			record = APPROVE;
		}
		else if(WAIT_PAY.getStatus().equals(status)){
			record = WAIT_PAY;
		}
		else if(PAID.getStatus().equals(status)){
			record = PAID;
		}
		
		return record;
	}
	public static String getMeaning(String status) {
		return getLicenseStatus(status ).getMeaning();
	}
}
