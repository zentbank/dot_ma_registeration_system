package sss.dot.tourism.util;

/**
 * N=Normal ปกติ
C=Cancel ยกเลิก
S=Suspension พักใช้
R=Revoke เพิกถอน
T=TEMP
 * @author SHAII_MACBOOK
 *
 */
public enum LicenseStatus {
	NORMAL("N" , "ปกติ"),
	CANCEL("C" , "ยกเลิกใบอนุญาต"),
	SUSPENSION("S" , "พักใช้"),
	REVOKE("R" , "เพิกถอน"),
	HISTORY("H","ประวัติ"),
	TEMP("T" , "อยู่ระหว่างจดทะเบียน"),
	DISALLOW("D" , "ไม่ผ่านการตรวจสอบ"),
	EXPIRE("E","หมดอายุตามกฎหมาย");
	
	private final String status;
	private final String meaning;
	
	LicenseStatus(String status, String meaning){
		this.status = status;
		this.meaning = meaning;
	}
	
	/**
	 * @return the category
	 */
	public String getStatusString() {
		return status.toString();
	}
	public String getStatus() {
		return status;
	}
	/**
	 * @return the meaning
	 */
	public String getMeaning() {
		return meaning;
	}
	/**
	 * overloading
	 * @param flag
	 * @return
	 */
	public boolean equals(String status){
		LicenseStatus eStatus =  getLicenseStatus(status);
		return super.equals(eStatus);
	}
	public static LicenseStatus getLicenseStatus(String status) {
		LicenseStatus record = null;

		if(NORMAL.getStatus().equals(status)){
			record = NORMAL;
		}
		else if(CANCEL.getStatus().equals(status)){
			record = CANCEL;
		}
		else if(SUSPENSION.getStatus().equals(status)){
			record = SUSPENSION;
		}
		else if(REVOKE.getStatus().equals(status)){
			record = REVOKE;
		}
		else if(TEMP.getStatus().equals(status)){
			record = TEMP;
		}
		else if(HISTORY.getStatus().equals(status)){
			record = HISTORY;
		}
		else if(DISALLOW.getStatus().equals(status)){
			record = DISALLOW;
		}
		else if(EXPIRE.getStatus().equals(status)){
			record = EXPIRE;
		}
		
		return record;
	}
	public static String getMeaning(String status) {
		return getLicenseStatus(status ).getMeaning();
	}
}
