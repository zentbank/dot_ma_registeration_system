package sss.dot.tourism.util;

import java.util.List;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;




public class MailService extends Thread{

	String subject;
	StringBuffer mailContent;
	String sendTo;
	
	public MailService(String sendTo, String subject, StringBuffer mailContent)
	{
		this.subject = subject;
		this.sendTo = sendTo;
		this.mailContent = mailContent;

	}
	public MailService() {
		

	}
	public  boolean sendMail(String sendTo, String subject, StringBuffer mailContent) {
		Properties props = new Properties();

		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "465");

		Session session = Session.getInstance(props, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(
						"licene.tourism.go.th@gmail.com", "sss123456");
			}
		});
		session.setDebug(false);
		boolean ckSendMail = false;
		try {
			MimeMessage mail = new MimeMessage(session);

			mail.setFrom(new InternetAddress("licene.tourism.go.th@gmail.com",
					"สำนักธุรกิจนำเที่ยวและมัคคุเทศก์ กรมการท่องเที่ยว"));
			// InternetAddress[] to_address = InternetAddress.parse(receiver);
			mail.setRecipients(Message.RecipientType.TO,
					sendTo);
			mail.setSubject(MimeUtility.encodeText(subject != null ? subject
					: "สำนักธุรกิจนำเที่ยวและมัคคุเทศก์", "utf-8", "B"));

			mail.setContent(mailContent.toString(), "text/html; charset=\"utf-8\"");
			
			Transport.send(mail);

			ckSendMail = true;
		} catch (MessagingException mex) {
			mex.printStackTrace();
			
		} catch (Exception e) {
			e.printStackTrace();
			
		} finally {

			if (ckSendMail)
				System.out.println("Send E-Mail Success..");
			else {
				System.out.println("Send E-Mail Fail..");
			}
		}
		
		return ckSendMail;
	}
	public void run(){
		Properties props = new Properties();

		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "465");

		Session session = Session.getInstance(props, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(
						"licene.tourism.go.th@gmail.com", "sss123456");
			}
		});
		session.setDebug(false);
		boolean ckSendMail = false;
		try {
			MimeMessage mail = new MimeMessage(session);

			mail.setFrom(new InternetAddress("licene.tourism.go.th@gmail.com",
					"สำนักธุรกิจนำเที่ยวและมัคคุเทศก์ กรมการท่องเที่ยว"));
			// InternetAddress[] to_address = InternetAddress.parse(receiver);
			mail.setRecipients(Message.RecipientType.TO,
					sendTo);
			mail.setSubject(MimeUtility.encodeText(subject != null ? subject
					: "สำนักธุรกิจนำเที่ยวและมัคคุเทศก์", "utf-8", "B"));

			mail.setContent(mailContent.toString(), "text/html; charset=\"utf-8\"");
			
			Transport.send(mail);

			ckSendMail = true;
		} catch (MessagingException mex) {
			mex.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			if (ckSendMail)
				System.out.println("Send E-Mail Success..");
			else {
				System.out.println("Send E-Mail Fail..");
			}
		}
	}
//	 public void run() {
//
//		 try {
//	        
//	    		JavaMailSenderImpl sender = new JavaMailSenderImpl();
//	    		Properties pro = new Properties();
//	    	    pro.setProperty("mail.smtps.auth", "true");
//	    	    pro.setProperty("mail.smtps.starttls.enable", "true");
//	    	    pro.setProperty("mail.debug", "true");
//	    	    sender.setJavaMailProperties(pro);
//	    		sender.setProtocol("smtps");
//	    		//แก้ email ให้เป็น ของ สำนักงานการท่องเที่ยว
//	    		sender.setUsername("licene.tourism.go.th@gmail.com");
//	            sender.setPassword("sss123456");
//	    		sender.setHost("smtp.gmail.com");
//	    		sender.setPort(465);
//	            sender.setDefaultEncoding("utf-8");
//
//	    		MimeMessage message = sender.createMimeMessage();
//	    		MimeMessageHelper helper = new MimeMessageHelper(message,true);
//	    		
//	    		helper.setSubject(subject !=null ?subject:"สำนักธุรกิจนำเที่ยวและมัคคุเทศก์");
//	    		
//	    		helper.setFrom(new InternetAddress("licene.tourism.go.th@gmail.com",
//	    				"สำนักธุรกิจนำเที่ยวและมัคคุเทศก์ กรมการท่องเที่ยว"));
//	    		helper.setTo(sendTo);
//	    	
//	    		
//	    		message.setContent(mailContent.toString(), "text/html; charset=utf-8" );
//	    		String[] lang = {"th"};
//	    		message.setContentLanguage(lang);
//	    		
////	    	      // create the message part    
////	    	      MimeBodyPart messageBodyPart = new MimeBodyPart();   
////
////	    	      //fill message   
////	    	      messageBodyPart.setText(""); 
////	    	      
////	    	      messageBodyPart.setContent(mailContent , "text/html; charset=utf-8");
////	    		     
////	    	      
////	    	      Multipart multipart = new MimeMultipart(); 
////	    	      multipart.addBodyPart(messageBodyPart);   
////	      
////	    	     message.setContent(multipart);
//	    		
//	    		
//	    		sender.send(message);
//	
//	            Thread.sleep(50);
//	         
//	     } catch (InterruptedException e) {
//	        e.printStackTrace();
//	     }
//	      catch (MessagingException me)
//	      {
//	    	  me.printStackTrace();
//	      }
//	      catch (Exception me)
//	      {
//	    	  me.printStackTrace();
//	      }
//	
//	  }
	 
	 public static void main(String arg[]) {
		 try{
			 StringBuffer message =  new StringBuffer();
			 message.append("คุณได้ลงทะเบียนกับระบบเรียบร้อยแล้ว <br/> ชื่อผู้ใช้งาน 0813407749 <br/> รหัสผ่าน 1235");
			 MailService mailThred = new MailService( "shaii.kanith@gmail.com", "ยืนยันการลงทะเบียนใช้งานระบบ", message);
			mailThred.run();
		 }catch(Exception e){e.printStackTrace();}
	 }
}
