package sss.dot.tourism.util;






import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import sss.dot.tourism.domain.Registration;
import sss.dot.tourism.dto.registration.RegistrationDTO;



/**
 * 
 * @author Jakkrit Sittiwerapong
 * @since 2 ธ.ค. 2008
 * @version 1.0.1
 * @category CommonService ObjectUtil.java
 * 
 */

public class ObjectUtil
{
  private static Logger log = Logger.getLogger(ObjectUtil.class);
  /**
   * 
   * @param orgs
   * @param dest
   * @throws IllegalArgumentException
   * @throws SecurityException
   * @throws IllegalAccessException
   * @throws InvocationTargetException
   * @throws NoSuchMethodException
   * @throws ParseException
   */
  public static void copy(Object[] orgs, Object dest) throws IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException, ParseException
  {
    for (Object org : orgs)
    {
      copy(org, dest);
    }
  }

  /**
   * 
   * @param org
   * @param dest
   * @throws IllegalArgumentException
   * @throws SecurityException
   * @throws IllegalAccessException
   * @throws InvocationTargetException
   * @throws NoSuchMethodException
   * @throws ParseException
   */
  public static void copy(Object org, Object... dest) throws IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException, ParseException
  {

    for (Object des : dest)
    {
      copy(org, des);
    }
  }

  /**
   * 
   * @param src
   * @param dest
   * @throws IllegalArgumentException
   * @throws IllegalAccessException
   * @throws InvocationTargetException
   * @throws SecurityException
   * @throws NoSuchMethodException
   * @throws ParseException
   */
  public static void copy(Object src, Object dest) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, SecurityException, NoSuchMethodException, ParseException
  {
    Class<? extends Object> clazzOrg = src.getClass();
    Method[] methodsOrg = clazzOrg.getDeclaredMethods();
    for (Method methodOrg : methodsOrg)
    {
      String methodName = methodOrg.getName();
      if (Pattern.matches("^get.*", methodName))
      {
        String setter = methodName.replaceFirst("get", "set");
        try
        {
          if (methodOrg.getReturnType().equals(Date.class))
          {
        	  Class destType = dest.getClass().getMethod(methodName, null).getReturnType();
              Method methodDest = dest.getClass().getMethod(setter, destType);
              if (methodDest.getParameterTypes()[0].equals(Date.class))
              {
            	  Date tDate = (Date) methodOrg.invoke(src, null);
            	  methodDest.invoke(dest, tDate);
              }
              else
              {
                  DateFormat df = sss.dot.tourism.util.DateUtils.getProcessDateFormatThai();
                  Date tDate = (Date) methodOrg.invoke(src, null);
                  if (tDate != null)
                  {
                    String date = df.format(tDate);
                    methodDest = dest.getClass().getMethod(setter, String.class);
                    methodDest.invoke(dest, date);
                  }
              }
      
        	  

          }
          else
          {
            Class destType = dest.getClass().getMethod(methodName, null).getReturnType();
            Method methodDest = dest.getClass().getMethod(setter, destType);
            if (methodDest.getParameterTypes()[0].equals(Date.class))
            {
              try
              {
                DateFormat df = sss.dot.tourism.util.DateUtils.getProcessDateFormatThai();
                String dateString = (String) methodOrg.invoke(src, null);
                Date date = (dateString == null || dateString.trim().length() == 0) ? null : df.parse(dateString);
                if (date != null)
                {
                  methodDest.invoke(dest, date);
                }
              }
              catch (ParseException e)
              {
                DateFormat df = sss.dot.tourism.util.DateUtils.getExtJSOutputDate();
                String dateString = (String) methodOrg.invoke(src, null);
                Date date = (dateString == null || dateString.trim().length() == 0) ? null : df.parse(dateString);
                if (date != null)
                {
                  methodDest.invoke(dest, date);
                }
              }
            }
            else
            {
              Object res = methodOrg.invoke(src, null);
              if (res != null)
              {
                methodDest.invoke(dest, res);
              }
            }
          }
        }
        catch (NoSuchMethodException e)
        {
          log.warn("!Couldn't apply values in method : ".concat(methodName));
          continue;
        }
        catch(IllegalArgumentException iax)
        {
          log.error("IllegalArgumentException@".concat(methodName));
          continue;
        }
      }

    }

  }
  
  public static void main(String arg[])
  {
	  try
	  {
		  Registration reg1 = new Registration();
		  reg1.setRegistrationDate(new Date());
		  reg1.setRegId(5);
		  reg1.setApproveStatus("A0");
		  reg1.setRegistrationProgress("progress");
		 
		  
		  Registration reg2 = new Registration();
		  RegistrationDTO dto = new RegistrationDTO();
		  
		  ObjectUtil.copy(reg1, reg2);
		  ObjectUtil.copy(reg1, dto);
		  
		  System.out.println(dto.getRegistrationDate());
	  }catch(Exception e)
	  {
		  e.printStackTrace();
	  }

  }

}
