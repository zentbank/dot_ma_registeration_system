package sss.dot.tourism.util;

public enum PrintStatus {

	WAITING("W","ยังไม่ได้พิมพ์"),
	APPROVE("A","พิมพ์แล้ว");
	
	private final String status;
	private final String meaning;
	
	PrintStatus(String status,String meaning){
		this.status = status;
		this.meaning = meaning;
	}
	
	/**
	 * @return the status
	 */
	public String getStatusString() {
		return status.toString();
	}
	public String getStatus() {
		return status;
	}
	/**
	 * @return the meaning
	 */
	public String getMeaning() {
		return meaning;
	}
	/**
	 * overloading
	 * @param flag
	 * @return
	 */
	
	public boolean equals(String status){
		PrintStatus eStatus =  getPrintStatus(status);
		return super.equals(eStatus);
	}
	public static PrintStatus getPrintStatus(String status) {
		PrintStatus record = null;

		
		if(WAITING.getStatus().equals(status)){
			record = WAITING;
		}
		else if(APPROVE.getStatus().equals(status)){
			record = APPROVE;
		}
		
		return record;
	}
	public static String getMeaning(String status) {
		return getPrintStatus(status).getMeaning();
	}
}
