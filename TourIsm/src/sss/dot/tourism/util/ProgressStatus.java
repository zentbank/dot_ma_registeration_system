package sss.dot.tourism.util;


public enum ProgressStatus {
	ACCEPT("A","ผ่าน"),
	REJECT("R","ไม่ผ่าน / ตีเรื่องกลับ"),
	CANCEL("C","ลบ"),
	WAITING("W","กำลังดำเนินการ"),
	GET_IT("G","รับเรื่อง"),
	BACK("B","ตีเรื่องกลับ"),
	REQUIRE_DOC("0","ขอเอกสารเพิ่มเติม");
	private final String status;
	private final String meaning;

	ProgressStatus(String status,String meaning){
		this.status = status;
		this.meaning = meaning;
	}

	/**
	 * @return the status
	 */
	public String getStatusString() {
		return status.toString();
	}
	public String getStatus() {
		return status;
	}
	/**
	 * @return the meaning
	 */
	public String getMeaning() {
		return meaning;
	}
	/**
	 * overloading
	 * @param flag
	 * @return
	 */
	public boolean equals(String status){
		ProgressStatus eStatus =  getProgressStatus(status);
		return super.equals(eStatus);
	}
	public static ProgressStatus getProgressStatus(String status) {
		ProgressStatus record = null;
		
		
		if(ACCEPT.getStatus().equals(status)){
			record = ACCEPT;
		}
		else if(REJECT.getStatus().equals(status)){
			record = REJECT;
		}
		else if(CANCEL.getStatus().equals(status)){
			record = CANCEL;
		}
		
		else if(WAITING.getStatus().equals(status)){
			record = WAITING;
		}
		else if(GET_IT.getStatus().equals(status)){
			record = GET_IT;
		}
		else if(BACK.getStatus().equals(status)){
			record = BACK;
		}
		else if(REQUIRE_DOC.getStatus().equals(status)){
			record = REQUIRE_DOC;
		}
		
		return record;
	}
	public static String getMeaning(String status) {
		return getProgressStatus(status).getMeaning();
	}
}
