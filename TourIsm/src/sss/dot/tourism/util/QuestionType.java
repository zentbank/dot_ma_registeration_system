package sss.dot.tourism.util;

public enum QuestionType {
	COMPLAINT("C" , "เรื่องร้องเรียน"),
	QUESTION("Q" , "ข้อซักถาม");
	
	private final String status;
	private final String meaning;
	
	QuestionType(String status, String meaning){
		this.status = status;
		this.meaning = meaning;
	}
	
	public String getStatusString() {
		return status.toString();
	}
	public String getStatus() {
		return status;
	}
	public String getMeaning() {
		return meaning;
	}
	
	public boolean equals(String status){
		QuestionType cStatus =  getQuestionType(status);
		return super.equals(cStatus);
	}
	public static QuestionType getQuestionType(String status) {
		QuestionType record = null;

		if(COMPLAINT.getStatus().equals(status)){
			record = COMPLAINT;
		}
		else if(QUESTION.getStatus().equals(status)){
			record = QUESTION;
		}
		return record;
	}
	public static String getMeaning(String status) {
		return getQuestionType(status).getMeaning();
	}
	
}
