package sss.dot.tourism.util;

import java.util.List;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import sss.dot.tourism.dto.mas.EmailDTO;

public class SendMailThread  extends Thread{

	private List<EmailDTO> emails;
	StringBuffer body;
	
	public SendMailThread(List<EmailDTO> emails, StringBuffer body)
	{
		this.emails = emails;
		this.body = body;

	}
	
	 public void run() {

	      send();
	
	   }
	
	 public void send(){
		 try {
	         for(EmailDTO email: emails) {
	        	 
//	     		String sunsonsoft = "sunsonsoft@gmail.com";
	    		
	    		
	    		JavaMailSenderImpl sender = new JavaMailSenderImpl();
	    		Properties pro = new Properties();
	    	    pro.setProperty("mail.smtps.auth", "true");
	    	    pro.setProperty("mail.smtps.starttls.enable", "true");
	    	    pro.setProperty("mail.debug", "true");
	    	    sender.setJavaMailProperties(pro);
	    		sender.setProtocol("smtps");
	    		//แก้ email ให้เป็น ของ สำนักงานการท่องเที่ยว
	    		sender.setUsername(ConstantUtil.MAIL_TOURISM);
	            sender.setPassword(ConstantUtil.MAIL_PASSWORD);
	            //
	            
	    		sender.setHost("smtp.gmail.com");
	    		sender.setPort(465);
	            sender.setDefaultEncoding("utf-8");

	    		MimeMessage message = sender.createMimeMessage();
	    		MimeMessageHelper helper = new MimeMessageHelper(message,true);
	    		
	    		helper.setSubject(email.getSubject() !=null ?email.getSubject():"สำนักธุรกิจนำเที่ยวและมัคคุเทศก์");
	    		
	    		helper.setFrom(new InternetAddress(ConstantUtil.MAIL_TOURISM,
	    				"สำนักธุรกิจนำเที่ยวและมัคคุเทศก์ กรมการท่องเที่ยว"));
	    		helper.setTo(email.getEmail());
	    	
	    		
	    		message.setContent(body.toString(), "text/html; charset=utf-8" );
	    		String[] lang = {"th"};
	    		message.setContentLanguage(lang);
	    		
	    	      // create the message part    
	    	      MimeBodyPart messageBodyPart = new MimeBodyPart();   

	    	      //fill message   
	    	      messageBodyPart.setText(""); 
	    	      
	    	      messageBodyPart.setContent(email.getMessage() , "text/html; charset=utf-8");
	    		     
	    	      
	    	      Multipart multipart = new MimeMultipart(); 
	    	      multipart.addBodyPart(messageBodyPart);   
	      
	    	     message.setContent(multipart);
	    		
	    		
	    		sender.send(message);
	
	            Thread.sleep(50);
	         }
	     } catch (InterruptedException e) {
	        e.printStackTrace();
	     }
	      catch (MessagingException me)
	      {
	    	  me.printStackTrace();
	      }
	      catch (Exception me)
	      {
	    	  me.printStackTrace();
	      }
	 }

}
