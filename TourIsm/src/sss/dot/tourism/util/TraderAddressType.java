package sss.dot.tourism.util;


public enum TraderAddressType {
	OFFICE_ADDRESS("O","ที่ตั้งสำนักงาน"),
	SHOP_ADDRESS("S","ที่ตั้งเฉพาะการ"),
	BRANCH("B","ที่ตั้งสาขา"),
	CONTACT("C", "ที่อยู่ติดต่อได้");
	

	private final String status;
	private final String meaning;

	TraderAddressType(String status,String meaning){
		this.status = status;
		this.meaning = meaning;
	}

	/**
	 * @return the status
	 */
	public String getStatusString() {
		return status.toString();
	}
	public String getStatus() {
		return status;
	}
	/**
	 * @return the meaning
	 */
	public String getMeaning() {
		return meaning;
	}
	/**
	 * overloading
	 * @param flag
	 * @return
	 */
	public boolean equals(String status){
		TraderAddressType eStatus =  getTraderAddressType(status);
		return super.equals(eStatus);
	}
	public static TraderAddressType getTraderAddressType(String status) {
		TraderAddressType record = null;
		
		
		if(OFFICE_ADDRESS.getStatus().equals(status)){
			record = OFFICE_ADDRESS;
		}
		else if(SHOP_ADDRESS.getStatus().equals(status)){
			record = SHOP_ADDRESS;
		}
		else if(BRANCH.getStatus().equals(status)){
			record = BRANCH;
		}
		else if(CONTACT.getStatus().equals(status)){
			record = CONTACT;
		}
		
			
		return record;
	}
	public static String getMeaning(String status) {
		return getTraderAddressType(status).getMeaning();
	}
}

