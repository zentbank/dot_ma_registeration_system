/**
 * @author Jakkrit Sittiwerapong
 * */
package com.sss.http.util;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import com.thoughtworks.xstream.XStream;

public abstract class HttpMessage
{

  private HttpServletResponse response;

  private XStream serializer;

  private XStream deserializer;

  private WriteMessagePerform writeMsgPerform;

  /**
   * 
   * @param response
   * @param perform
   */
  public HttpMessage(HttpServletResponse response, WriteMessagePerform perform)
  {
    this(response);
    this.writeMsgPerform = perform;
  }
  /**
   * 
   * @param response
   */
  public HttpMessage(HttpServletResponse response)
  {
    this.response = response;
  }
  /**
   * 
   */
  public HttpMessage()
  {
    // empty
  }


  /**
   * 
   * @param errorMsg
   * @throws Exception
   */
  public abstract void writeErrorMessage(String errorMsg) ;


  /**
   * 
   * @param object
   * @throws IOException
   */
  public abstract void write(Object object) throws IOException;



  /**
   * 
   * @param alias
   * @param data
   * @return
   */
  @SuppressWarnings("unchecked")
public abstract Object[] toArrayObject(Class alias, String data);

  /**
   * 
   * @param data
   * @return
   */
  public abstract Object toObject(String data);

  /**
   * 
   * @param msg
   * @throws IOException
   */
  public void writeMessage(String msg)
  {
	  try{
		  this.writeMsgPerform.writeMessage(response, msg);
	  }catch(Exception e){
		  e.printStackTrace();
	  }
    
  }

  /**
   * 
   * @param obj
   * @return
   */
  public void serialize(Object obj)
  {
    String msg = this.serializer.toXML(obj);
    this.writeMessage(msg);
  }

  /**
   * 
   * @param obj
   * @return
   */
  public String serializeTo(Object obj)
  {
    return this.serializer.toXML(obj);
  }

  /**
   * 
   * @param data
   * @return
   */
  public Object deserializeTo(String data)
  {
    return this.deserializer.fromXML(data);
  }

  /**
   * 
   * @param alias
   * @param data
   * @return
   */
  @SuppressWarnings("unchecked")
public Object[] deserializeToArray(Class alias, String data)
  {
    this.deserializer.alias("data-array", alias);
    return (Object[]) this.deserializer.fromXML(data);
  }

  /**
   * @return the response
   */
  @SuppressWarnings("unused")
private HttpServletResponse getResponse()
  {
    return response;
  }

  /**
   * @param response
   *          the response to set
   */
  public void setResponse(HttpServletResponse response)
  {
    this.response = response;
  }

  /**
   * @return the serializer
   */
  private XStream getSerializer()
  {
    return serializer;
  }

  /**
   * @param serializer
   *          the serializer to set
   */
  public void setSerializer(XStream serializer)
  {
    this.serializer = serializer;
  }

  /**
   * @return the deserializer
   */
  private XStream getDeserializer()
  {
    return deserializer;
  }

  /**
   * @param deserializer
   *          the deserializer to set
   */
  public void setDeserializer(XStream deserializer)
  {
    this.deserializer = deserializer;
  }

  /**
   * @return the writeMsgPerform
   */
  private WriteMessagePerform getWriteMsgPerform()
  {
    return writeMsgPerform;
  }

  /**
   * @param writeMsgPerform
   *          the writeMsgPerform to set
   */
  public void setWriteMsgPerform(WriteMessagePerform writeMsgPerform)
  {
    this.writeMsgPerform = writeMsgPerform;
  }

}
