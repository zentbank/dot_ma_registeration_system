/**
 * @author Jakkrit Sittiwerapong
 * */
package com.sss.http.util;

import javax.servlet.http.HttpServletResponse;

public class HttpMessageFactory {
	
	
	public static HttpMessage create(HttpMessageType type, HttpServletResponse response){
		HttpMessage httpMessage = null;
		switch(type){
			case JSON:{
				httpMessage = new JsonHttpMessage(response,new JSONPerform());
				break;
			}
			case XML:{
				httpMessage = new XMLHttpMessage(response,new XMLPerform());
				break;
			}
			

		}
		return httpMessage;
	}
}
