package com.sss.http.util;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;
import com.thoughtworks.xstream.io.json.JsonHierarchicalStreamDriver;
import com.thoughtworks.xstream.io.json.JsonWriter;

/**
 * 
 * @author Jakkrit Sittiwerapong
 * @since 7 Jan 2009
 * @version 1.0.1
 * @category CommonUtil JsonHttpMessage.java
 */
public class JsonHttpMessage extends HttpMessage {

	private XStream dropRootSerializer;

	public JsonHttpMessage(HttpServletResponse response,
			WriteMessagePerform perform) {
		super(response, perform);
		this.init();
	}

	public JsonHttpMessage(HttpServletResponse response) {
		super(response);
		this.init();
	}

	/**
   * 
   */
	private void init() {
		XStream serializer = new XStream(new JsonHierarchicalStreamDriver() {
			public HierarchicalStreamWriter createWriter(Writer writer) {
				return new JsonWriter(writer, JsonWriter.DROP_ROOT_MODE);
			}
		});
		/*
		 * Since JSON has no possibility to express references, you should
		 * always set the NO_REFERENCES mode writing JSON
		 */
		serializer.setMode(XStream.NO_REFERENCES);
		/*
		 * Simple XStream driver wrapping Jettison's Mapped reader and writer.
		 * Serializes object from and to JSON.
		 */
		XStream deserializer = new XStream(new JettisonMappedXmlDriver());

		super.setSerializer(serializer);

		super.setDeserializer(deserializer);

		this.dropRootSerializer = new XStream(
				new JsonHierarchicalStreamDriver() {
					public HierarchicalStreamWriter createWriter(Writer writer) {
						return new JsonWriter(writer, JsonWriter.DROP_ROOT_MODE);
					}
				});
		this.dropRootSerializer.setMode(XStream.NO_REFERENCES);

	}

	/**
	 * @see com.inec.http.util.HttpMessage#writeErrorMessage(com.inec.util.JsonErrorMessage)
	 */
	public void writeErrorMessage(String errorMsg) {
		MessageWrapper<?> message = new MessageWrapper(errorMsg, false);
		super.serialize(message);
	}

	/**
	 * 
	 * @see com.inec.http.util.HttpMessage#write(java.lang.Object)
	 */
	public void write(Object object) throws IOException {
		MessageWrapper<?> message = message = new MessageWrapper(
				(List<?>) object, ((List<?>) object).size() > 0?true:false, ((List<?>) object).size() < 1?"Not found":"");

		super.serialize(message);
	}

	public Object[] toArrayObject(Class alias, String data) {
		return super.deserializeToArray(alias, data);
	}

	/**
	 * 
	 * @see com.inec.http.util.HttpMessage#toObject(java.lang.String)
	 */

	public Object toObject(String data) {
		return super.deserializeTo(data);
	}

	/**
	 * @return the dropRootSerializer
	 */
	public XStream getDropRootSerializer() {
		return dropRootSerializer;
	}

}
