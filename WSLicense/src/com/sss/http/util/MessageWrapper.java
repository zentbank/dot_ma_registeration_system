package com.sss.http.util;





import java.io.Serializable;
import java.util.List;

public class MessageWrapper<E> implements Serializable {
	
	private List<E> data;
	private boolean success;
	private String message;
	
	public MessageWrapper(boolean success) {
		this.success = success;
	}

	public MessageWrapper(String message, boolean success) {
		this.message = message;
		this.success = success;
	}

	public MessageWrapper(List<E> data, boolean success) {
		this.data = (List<E>) data;
		this.success = success;
	}
	
	public MessageWrapper(List<E> data, boolean success, String message) {
		this.data = (List<E>) data;
		this.success = success;
		this.message = message;
	}

	public List<E> getData() {
		return data;
	}

	public void setData(List<E> data) {
		this.data = data;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
