/**
 * @author Jakkrit Sittiwerapong
 * */

package com.sss.http.util;

import java.io.IOException;
import java.nio.charset.Charset;

import javax.servlet.http.HttpServletResponse;
/**
 * 
 * @author Pradit
 * @since 8 Jan 2009
 * @version 1.0.1
 * @category CommonUtil WriteMessagePerform.java
 */
public interface WriteMessagePerform {
	/**
	 * Constant of Json Content type
	 */
	public static final String JSON_CONTENT_TYPE = "application/json;charSet=UTF-8";
	
	/**
	 * Constant of XML Content type
	 */
	public static final String XML_CONTENT_TYPE = "application/xml;charset=UTF-8";
	/**
	 * Constant of Json Content type
	 */
	public static final Charset UTF8 = Charset.forName("UTF-8");
	public void writeMessage(HttpServletResponse response, String msg) throws IOException;
	 
}
