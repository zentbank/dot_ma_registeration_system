/**
 * @author Jakkrit Sittiwerapong
 * */

package com.sss.http.util;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import sss.dot.tourism.license.dto.LicenseDTO;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.json.JsonHierarchicalStreamDriver;
import com.thoughtworks.xstream.io.json.JsonWriter;
import com.thoughtworks.xstream.io.xml.XppDriver;

/**
 * 
 * @author Pradit
 * @since 8 Jan 2009
 * @version 1.0.1
 * @category CommonUtil XMLHttpMessage.java
 *
 */
public class XMLHttpMessage extends HttpMessage {

	private XStream dropRootSerializer;

	public XMLHttpMessage(HttpServletResponse response,
			WriteMessagePerform perform) {
		super(response, perform);
		XStream serializer = new XStream(new XppDriver());
//		serializer.setMode(XStream.NO_REFERENCES);
		serializer.alias("license", LicenseDTO.class);
		serializer.alias("xml", MessageWrapper.class);
		serializer.addImplicitCollection(MessageWrapper.class, "data");
		// serializer.setMode(XStream.NO_REFERENCES);
		XStream deserializer = new XStream(new XppDriver());

		super.setSerializer(serializer);
		super.setDeserializer(deserializer);

		this.dropRootSerializer = new XStream(
				new JsonHierarchicalStreamDriver() {
					public HierarchicalStreamWriter createWriter(Writer writer) {
						return new JsonWriter(writer, JsonWriter.DROP_ROOT_MODE);
					}
				});
		this.dropRootSerializer.setMode(XStream.NO_REFERENCES);
	}

	/**
	 * @see com.inec.http.util.HttpMessage#toArrayObject(java.lang.Class,
	 *      java.lang.String)
	 */

	public Object[] toArrayObject(Class alias, String data) {
		return deserializeToArray(alias, data);
	}

	/**
	 * @see com.inec.http.util.HttpMessage#toObject(java.lang.String)
	 */

	public Object toObject(String data) {
		return deserializeTo(data);
	}

	/**
	 * @see com.inec.http.util.HttpMessage#writeErrorMessage(com.inec.util.JsonErrorMessage)
	 */
	public void writeErrorMessage(String errorMsg) {
		MessageWrapper<?> message = new MessageWrapper(errorMsg, false);
		super.serialize(message);
	}

	/**
	 * 
	 * @see com.inec.http.util.HttpMessage#write(java.lang.Object)
	 */
	public void write(Object object) throws IOException {
		
		
		
		MessageWrapper<?> message = message = new MessageWrapper(
				(List<?>) object, ((List<?>) object).size() > 0?true:false, ((List<?>) object).size() < 1?"Not found":"");

		super.serialize(message);
		
//		super.writeMessage(response, msg);
		
	}

}
