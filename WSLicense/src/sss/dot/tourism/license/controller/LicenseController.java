package sss.dot.tourism.license.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import sss.dot.tourism.admin.service.IServicePermissionService;
import sss.dot.tourism.license.dto.LicenseDTO;
import sss.dot.tourism.license.service.ILicenseSerive;

import com.sss.http.util.HttpMessage;
import com.sss.http.util.HttpMessageFactory;
import com.sss.http.util.HttpMessageType;

@Controller
@RequestMapping("/license")
public class LicenseController{

	@Autowired
	private ILicenseSerive licenseSerive;
	
	@Autowired
	private IServicePermissionService servicePermissionService;
	
	private HttpHeaders addAccessControlAllowOrigin(){
		HttpHeaders headers = new HttpHeaders();
        headers.add("Access-Control-Allow-Origin", "*");
        return headers;
	}
	
	
	
	@RequestMapping(value = "/{org}/licenseno/{id}/json", method = RequestMethod.POST)
	@ResponseBody
	public void getLicensesAsJson(@PathVariable("org") String org,@PathVariable("id") String id, HttpServletRequest req, HttpServletResponse resp) {
		
		HttpMessage msgwrite = HttpMessageFactory.create(HttpMessageType.JSON, resp);
		
		try {
			
            
			String ipAddress = req.getRemoteAddr();
			System.out.println("ipAddress: "+ipAddress);
			if(!servicePermissionService.register(ipAddress, org)){
				msgwrite.writeErrorMessage("permission denied");
			}else{
				LicenseDTO param = new LicenseDTO();
				param.setLicenseNo(id);
				List list = licenseSerive.getLicense(param);
			    msgwrite.write(list);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			msgwrite.writeErrorMessage(e.getMessage());
		}

	}

	@RequestMapping(value = "/{org}/licenseno/{id}/xml", method = RequestMethod.POST)
	@ResponseBody
	public void getLicensesAsXml(@PathVariable("org") String org ,@PathVariable("id") String id , HttpServletRequest req, HttpServletResponse resp) {
		
		HttpMessage msgwrite = HttpMessageFactory.create(HttpMessageType.XML, resp);
		try {
			
			String ipAddress = req.getRemoteAddr();
			if(!servicePermissionService.register(ipAddress, org)){
				msgwrite.writeErrorMessage("permission denied");
			}else{
				LicenseDTO param = new LicenseDTO();
				param.setLicenseNo(id);
				
				
				List list = licenseSerive.getLicense(param);
			    msgwrite.write(list);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			msgwrite.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/{org}/licenseno/{type}/{id}/json", method = RequestMethod.POST)
	@ResponseBody
	public void getTourLicensesAsJson(@PathVariable("org") String org,@PathVariable("type") String type, @PathVariable("id") String id, HttpServletRequest req, HttpServletResponse resp) {
		
		HttpMessage msgwrite = HttpMessageFactory.create(HttpMessageType.JSON, resp);
		
		try {
			
			String ipAddress = req.getRemoteAddr();
			if(!servicePermissionService.register(ipAddress, org)){
				msgwrite.writeErrorMessage("permission denied");
			}else{
				LicenseDTO param = new LicenseDTO();
				param.setLicenseNo(type+"/"+id);
				List list = licenseSerive.getLicense(param);
			    msgwrite.write(list);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			msgwrite.writeErrorMessage(e.getMessage());
		}

	}

	@RequestMapping(value = "/{org}/licenseno/{type}/{id}/xml", method = RequestMethod.POST)
	@ResponseBody
	public void getTourLicensesAsXml(@PathVariable("org") String org,@PathVariable("type") String type, @PathVariable("id") String id, HttpServletRequest req, HttpServletResponse resp) {
		
		HttpMessage msgwrite = HttpMessageFactory.create(HttpMessageType.XML, resp);
		try {
			String ipAddress = req.getRemoteAddr();
			if(!servicePermissionService.register(ipAddress, org)){
				msgwrite.writeErrorMessage("permission denied");
			}else{
				LicenseDTO param = new LicenseDTO();
				param.setLicenseNo(type+"/"+id);
				List list = licenseSerive.getLicense(param);
			    msgwrite.write(list);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			msgwrite.writeErrorMessage(e.getMessage());
		}
	}
	
	//identityNo
	
	@RequestMapping(value = "/{org}/id/{id}/json", method = RequestMethod.POST)
	@ResponseBody
	public void getByIdNoAsJson(@PathVariable("org") String org,@PathVariable("id") String id, HttpServletRequest req, HttpServletResponse resp) {
		
		HttpMessage msgwrite = HttpMessageFactory.create(HttpMessageType.JSON, resp);
		
		try {
			String ipAddress = req.getRemoteAddr();
			if(!servicePermissionService.register(ipAddress, org)){
				msgwrite.writeErrorMessage("permission denied");
			}else{
				LicenseDTO param = new LicenseDTO();
				param.setLicenseIdentityNo(id);
				List list = licenseSerive.getLicense(param);
			    msgwrite.write(list);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			msgwrite.writeErrorMessage(e.getMessage());
		}

	}

	@RequestMapping(value = "/{org}/id/{id}/xml", method = RequestMethod.POST)
	@ResponseBody
	public void getByIdNoAsXml(@PathVariable("org") String org,@PathVariable("id") String id, HttpServletRequest req, HttpServletResponse resp) {
		
		HttpMessage msgwrite = HttpMessageFactory.create(HttpMessageType.XML, resp);
		try {
			String ipAddress = req.getRemoteAddr();
			if(!servicePermissionService.register(ipAddress, org)){
				msgwrite.writeErrorMessage("permission denied");
			}else{
				LicenseDTO param = new LicenseDTO();
				param.setLicenseIdentityNo(id);
				List list = licenseSerive.getLicense(param);
			    msgwrite.write(list);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			msgwrite.writeErrorMessage(e.getMessage());
		}
	}
	
	//province
	@RequestMapping(value = "/{org}/type/{licenseType}/provincename/{provinceName}/json", method = RequestMethod.POST)
	@ResponseBody
	public void getByProvinceNoAsJson(@PathVariable("org") String org, @PathVariable("licenseType") String licenseType,@PathVariable("provinceName") String provinceName, HttpServletRequest req, HttpServletResponse resp) {
		
		HttpMessage msgwrite = HttpMessageFactory.create(HttpMessageType.JSON, resp);
		
		try {
			String ipAddress = req.getRemoteAddr();
			if(!servicePermissionService.register(ipAddress, org)){
				msgwrite.writeErrorMessage("permission denied");
			}else{
				LicenseDTO param = new LicenseDTO();
				param.setLicenseType(licenseType);
				param.setLicenseProvinceName(provinceName);
				List list = licenseSerive.getLicense(param);
			    msgwrite.write(list);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			msgwrite.writeErrorMessage(e.getMessage());
		}

	}

	@RequestMapping(value = "/{org}/type/{licenseType}/provincename/{provinceName}/xml", method = RequestMethod.POST)
	@ResponseBody
	public void getByProvinceNoAsXml(@PathVariable("org") String org, @PathVariable("licenseType") String licenseType,@PathVariable("provinceName") String provinceName, HttpServletRequest req, HttpServletResponse resp) {
		
		HttpMessage msgwrite = HttpMessageFactory.create(HttpMessageType.XML, resp);
		try {
			String ipAddress = req.getRemoteAddr();
			if(!servicePermissionService.register(ipAddress, org)){
				msgwrite.writeErrorMessage("permission denied");
			}else{
				LicenseDTO param = new LicenseDTO();
				param.setLicenseType(licenseType);
				param.setLicenseProvinceName(provinceName);			
				List list = licenseSerive.getLicense(param);
			    msgwrite.write(list);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			msgwrite.writeErrorMessage(e.getMessage());
		}
	}
	
	
	@RequestMapping(value = "/{org}/complaint/{id}/json", method = RequestMethod.POST)
	@ResponseBody
	public void getComplaintAsJson(@PathVariable("org") String org,@PathVariable("id") String id, HttpServletRequest req, HttpServletResponse resp) {
		
		HttpMessage msgwrite = HttpMessageFactory.create(HttpMessageType.JSON, resp);
		
		try {
			
			String ipAddress = req.getRemoteAddr();
			
			if(!servicePermissionService.register(ipAddress, org)){
				msgwrite.writeErrorMessage("permission denied");
			}else{
				LicenseDTO param = new LicenseDTO();
				param.setLicenseNo(id);
				List list = licenseSerive.getComplaint(param);
			    msgwrite.write(list);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			msgwrite.writeErrorMessage(e.getMessage());
		}

	}

	@RequestMapping(value = "/{org}/complaint/{id}/xml", method = RequestMethod.POST)
	@ResponseBody
	public void getComplaintAsXml(@PathVariable("org") String org ,@PathVariable("id") String id , HttpServletRequest req, HttpServletResponse resp) {
		
		HttpMessage msgwrite = HttpMessageFactory.create(HttpMessageType.XML, resp);
		try {
			
			String ipAddress = req.getRemoteAddr();
			if(!servicePermissionService.register(ipAddress, org)){
				msgwrite.writeErrorMessage("permission denied");
			}else{
				LicenseDTO param = new LicenseDTO();
				param.setLicenseNo(id);
				
				
				List list = licenseSerive.getComplaint(param);
			    msgwrite.write(list);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			msgwrite.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/{org}/complaint/{type}/{id}/json", method = RequestMethod.POST)
	@ResponseBody
	public void getTourComplaintAsJson(@PathVariable("org") String org,@PathVariable("type") String type, @PathVariable("id") String id, HttpServletRequest req, HttpServletResponse resp) {
		
		HttpMessage msgwrite = HttpMessageFactory.create(HttpMessageType.JSON, resp);
		
		try {
			
			String ipAddress = req.getRemoteAddr();
			if(!servicePermissionService.register(ipAddress, org)){
				msgwrite.writeErrorMessage("permission denied");
			}else{
				LicenseDTO param = new LicenseDTO();
				param.setLicenseNo(type+"/"+id);
				List list = licenseSerive.getComplaint(param);
			    msgwrite.write(list);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			msgwrite.writeErrorMessage(e.getMessage());
		}

	}

	@RequestMapping(value = "/{org}/complaint/{type}/{id}/xml", method = RequestMethod.POST)
	@ResponseBody
	public void getTourComplaintAsXml(@PathVariable("org") String org,@PathVariable("type") String type, @PathVariable("id") String id, HttpServletRequest req, HttpServletResponse resp) {
		
		HttpMessage msgwrite = HttpMessageFactory.create(HttpMessageType.XML, resp);
		try {
			String ipAddress = req.getRemoteAddr();
			if(!servicePermissionService.register(ipAddress, org)){
				msgwrite.writeErrorMessage("permission denied");
			}else{
				LicenseDTO param = new LicenseDTO();
				param.setLicenseNo(type+"/"+id);
				List list = licenseSerive.getComplaint(param);
			    msgwrite.write(list);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			msgwrite.writeErrorMessage(e.getMessage());
		}
	}
	
	//suspend
	

	@RequestMapping(value = "/{org}/suspend/{id}/json", method = RequestMethod.POST)
	@ResponseBody
	public void getSuspendAsJson(@PathVariable("org") String org,@PathVariable("id") String id, HttpServletRequest req, HttpServletResponse resp) {
		
		HttpMessage msgwrite = HttpMessageFactory.create(HttpMessageType.JSON, resp);
		
		try {
			
			String ipAddress = req.getRemoteAddr();
			
			if(!servicePermissionService.registerAsL2(ipAddress, org)){
				msgwrite.writeErrorMessage("permission denied");
			}else{
				LicenseDTO param = new LicenseDTO();
				param.setLicenseNo(id);
				List list = licenseSerive.getSuspend(param);
			    msgwrite.write(list);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			msgwrite.writeErrorMessage(e.getMessage());
		}

	}

	@RequestMapping(value = "/{org}/suspend/{id}/xml", method = RequestMethod.POST)
	@ResponseBody
	public void getSuspendAsXml(@PathVariable("org") String org ,@PathVariable("id") String id , HttpServletRequest req, HttpServletResponse resp) {
		
		HttpMessage msgwrite = HttpMessageFactory.create(HttpMessageType.XML, resp);
		try {
			
			String ipAddress = req.getRemoteAddr();
			if(!servicePermissionService.registerAsL2(ipAddress, org)){
				msgwrite.writeErrorMessage("permission denied");
			}else{
				LicenseDTO param = new LicenseDTO();
				param.setLicenseNo(id);
				
				
				List list = licenseSerive.getComplaint(param);
			    msgwrite.write(list);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			msgwrite.writeErrorMessage(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/{org}/suspend/{type}/{id}/json", method = RequestMethod.POST)
	@ResponseBody
	public void getTourSuspendAsJson(@PathVariable("org") String org,@PathVariable("type") String type, @PathVariable("id") String id, HttpServletRequest req, HttpServletResponse resp) {
		
		HttpMessage msgwrite = HttpMessageFactory.create(HttpMessageType.JSON, resp);
		
		try {
			
			String ipAddress = req.getRemoteAddr();
			if(!servicePermissionService.registerAsL2(ipAddress, org)){
				msgwrite.writeErrorMessage("permission denied");
			}else{
				LicenseDTO param = new LicenseDTO();
				param.setLicenseNo(type+"/"+id);
				List list = licenseSerive.getSuspend(param);
			    msgwrite.write(list);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			msgwrite.writeErrorMessage(e.getMessage());
		}

	}

	@RequestMapping(value = "/{org}/suspend/{type}/{id}/xml", method = RequestMethod.POST)
	@ResponseBody
	public void getTourSuspendAsXml(@PathVariable("org") String org,@PathVariable("type") String type, @PathVariable("id") String id, HttpServletRequest req, HttpServletResponse resp) {
		
		HttpMessage msgwrite = HttpMessageFactory.create(HttpMessageType.XML, resp);
		try {
			String ipAddress = req.getRemoteAddr();
			if(!servicePermissionService.registerAsL2(ipAddress, org)){
				msgwrite.writeErrorMessage("permission denied");
			}else{
				LicenseDTO param = new LicenseDTO();
				param.setLicenseNo(type+"/"+id);
				List list = licenseSerive.getSuspend(param);
			    msgwrite.write(list);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			msgwrite.writeErrorMessage(e.getMessage());
		}
	}
}
