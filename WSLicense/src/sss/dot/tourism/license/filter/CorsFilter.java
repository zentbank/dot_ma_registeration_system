package sss.dot.tourism.license.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.filter.OncePerRequestFilter;

public class CorsFilter extends OncePerRequestFilter{

	@Override
	protected void doFilterInternal(HttpServletRequest req, HttpServletResponse resp, FilterChain filterChain) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		if (req.getHeader("Access-Control-Request-Method") != null && "OPTIONS".equals(req.getMethod())) {
//            // CORS "pre-flight" request
//            resp.addHeader("Access-Control-Allow-Origin", "*");
//            resp.addHeader("Access-Control-Allow-Methods", "GET, POST");
//            resp.addHeader("Access-Control-Allow-Headers", "Content-Type, X-Requested-With");
//            resp.addHeader("Access-Control-Max-Age", "60");//30 min
//        }
		resp.addHeader("Access-Control-Allow-Origin", "*");
        resp.addHeader("Access-Control-Allow-Methods", "GET, POST");
        resp.addHeader("Access-Control-Allow-Headers", "Content-Type, X-Requested-With");
        resp.addHeader("Access-Control-Max-Age", "60");//30 min
        
        if (!req.getMethod().equals("OPTIONS")){
        	filterChain.doFilter(req, resp);
	    }
        
	}

}
