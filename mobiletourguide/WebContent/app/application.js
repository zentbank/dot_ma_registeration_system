Ext.define('News.Application', {
    name: 'News',

    extend: 'Ext.app.Application',

    views: [
        // TODO: add views here
    ],

    controllers: [
        'News.controller.menu.TreeMenuCrtl',
        'News.controller.news.JournalCtrl',
        'News.controller.news.MemberNewsCtrl'
    ],

    stores: [
        'News.store.combo.TraderTypeStore'
    ]
});
