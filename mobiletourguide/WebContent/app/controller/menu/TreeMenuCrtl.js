Ext.define('News.controller.menu.TreeMenuCrtl', {
	extend : 'Ext.app.Controller',

   	stores: ['News.store.menu.MenuStore'],

    models: [],

    views: ['News.view.menu.TreeMenu'],

    refs: [    
    	{
            ref: 'treemenu',
            selector: 'treemenu'
      } 
    ],	 
	init : function (application) {

    this.control({
        'treemenu': {
            itemclick: function(tree, model, ele, index, e, eOpts){

            	
              var centerView = Ext.ComponentQuery.query('centerview')[0]; 
              
              var itemIndex = '';
              if(model.raw.menuId == 'MEMBERNEWS')
              {
                itemIndex = 0;
              }

              // if(model.raw.menuId == 'MEMBERNEWS')
              // {
              //   itemIndex = 1;
              // }

              
              
              if(!Ext.isEmpty(itemIndex))
              {
                centerView.getLayout().setActiveItem(itemIndex);
              }

              
            }
        }
    });

		
	},
	showBusinessRegistrationWindow: function(grid, model) {

         // var edit = Ext.create('tourism.view.business.registration.BusinessRegistrationAddEditWindow').show();
         var edit = Ext.create('tourism.view.business.registration.registrationType.RegistrationTypeAddEditWindow').show();
         
         
         // alert('showBusinessRegistrationWindow');
                    
        
    }
});