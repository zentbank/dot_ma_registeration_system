Ext.define('News.controller.news.JournalCtrl', {
	extend : 'Ext.app.Controller',

    refs: [    
       {  
            ref: '#publicnews-journal-grid',
            selector: '#publicnews-journal-grid'
        } 
       
        ,{
            ref: 'publicnews-formaddedit',
            selector: 'panel'
        },
        {
          ref: '#publicnews-journal-formsearch',
          selector: 'panel'
        }
       
    ],	 
	init : function (application) {

        this.control({
                       
            // '#director-approvalprocess-formsearch button[action=searchLicenseBetweenRegistration]': {
            //     click: this.searchLicenseBetweenRegistration
            // }, 
            // '#director-approvalprocess-grid actioncolumn': {
            //     itemclick: this.handleActionColumn
            // },
            // '#director-registration-verification-form button[action=saveVerification]': {
            //     click: this.saveVerification
            // }
            //  '#director-business-approvalprocess-formsearch button[action=searchLicenseBetweenRegistration]': {
            //     click: this.searchBusinessLicense
            // },
            //  '#director-guide-approvalprocess-formsearch button[action=searchLicenseBetweenRegistration]': {
            //     click: this.searchGuideLicense
            // },
             '#publicnews-journal-formsearch button[action=searchJournal]': {
                click: this.searchJournal
            },
            '#publicnews-journal-grid actioncolumn': {
                itemclick: this.handleActionColumn
            },
            '#publicnews-journal-grid button[action=addpublicnews]': {
              click: this.addpublicnews
            },
            'publicnews-formaddedit button[action=savePublicNews]': {
                click: this.savePublicNews
            }

            
            // ,
            // '#director-guide-approvalprocess-grid actioncolumn': {
            //     itemclick: this.handleActionColumn
            // },
            // '#director-tourleader-approvalprocess-grid actioncolumn': {
            //     itemclick: this.handleActionColumn
            // },
            // '#director-registration-verification-form button[action=saveVerification]': {
            //     click: this.saveVerification
            // }
            
        });

		
	},
  searchJournal: function(btn)
  {
    var form = btn.up('form'),
    values = form.getValues();
    var dataParams = this.normalizeData(values);
    // dataParams.traderType = 'B';

    var grid = Ext.ComponentQuery.query('#publicnews-journal-grid')[0];
    var store = grid.getStore();

     dataParams.page = '1';
     dataParams.start = '0';
     dataParams.limit = store.pageSize;

      store.load({
          params: dataParams,
          callback: function(records, operation, success) {
              // console.log(success);
          },
          scope: this
      }); 
  },

  normalizeData: function(dataObj)
  {
      // remove null data;
     for (field in dataObj) 
      {
        if (Ext.isEmpty(dataObj[field])) 
        {
          delete dataObj[field];
        }
        
      }

      return dataObj;
  }
  ,addpublicnews: function(btn)
  {
    var win = Ext.create('News.view.news.publicnews.PublicNewsAddEditWindow');
    win.show();
  }
  ,handleActionColumn : function(column, action, grid, rowIndex, colIndex, model, node) {

          
          if(action){
              switch(action){
                case 'deleteNews':
                  grid.getStore().remove(model);
                  grid.getStore().sync();

                  break;

                  case 'viewimg':

                  var reqUrl = "info/journal/publicnews/viewimg/"+model.get('newsId');
                  window.open(reqUrl,"","toolbar=0 width=600 height=400");
                

                  break;
              }
          }
      
  },
  savePublicNews: function(btn)
  {
    var form = btn.up('window').down('form').getForm();
    if(form.isValid())
    {
        form.submit({
            url: 'info/journal/savepublicnews',
            method: 'POST',
            waitMsg: 'กำลังอัปโหลด..',
            success: function(form, action) {
              
              Ext.Msg.alert('', 'บันทึกข้อมูลเรียบร้อยแล้ว');

              var grid = Ext.ComponentQuery.query('#publicnews-journal-grid')[0];
              grid.getStore().reload();

            
              
            }
          ,failure: function(form, action)
          {
            Ext.Msg.alert('Failure', "ไม่สามารถบันทึกข้อมูลได้");
          }
        });
    }
  }
 
});