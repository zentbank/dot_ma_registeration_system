Ext.define('News.controller.news.MemberNewsCtrl', {
	extend : 'Ext.app.Controller',

    refs: [    
       {  
            ref: '#membernews-journal-grid',
            selector: '#membernews-journal-grid'
        } 
       
        ,{
            ref: 'membernews-formaddedit',
            selector: 'panel'
        },
        {
          ref: '#membernews-journal-formsearch',
          selector: 'panel'
        }
       
    ],	 
	init : function (application) {

        this.control({
                       
             '#membernews-journal-formsearch button[action=searchJournal]': {
                click: this.searchJournal
            },
            '#membernews-journal-grid actioncolumn': {
                itemclick: this.handleActionColumn
            },
            '#membernews-journal-grid button[action=addmembernews]': {
              click: this.addmembernews
            },
            'membernews-formaddedit button[action=savemembernews]': {
                click: this.savemembernews
            }
            
        });

		
	},
  searchJournal: function(btn)
  {
    var form = btn.up('form'),
    values = form.getValues();
    var dataParams = this.normalizeData(values);
    // dataParams.traderType = 'B';

    var grid = Ext.ComponentQuery.query('#membernews-journal-grid')[0];
    var store = grid.getStore();

     dataParams.page = '1';
     dataParams.start = '0';
     dataParams.limit = store.pageSize;

      store.load({
          params: dataParams,
          callback: function(records, operation, success) {
              // console.log(success);
          },
          scope: this
      }); 
  },

  normalizeData: function(dataObj)
  {
      // remove null data;
     for (field in dataObj) 
      {
        if (Ext.isEmpty(dataObj[field])) 
        {
          delete dataObj[field];
        }
        
      }

      return dataObj;
  }
  ,addmembernews: function(btn)
  {
    var win = Ext.create('News.view.news.membernews.MemberNewsAddEditWindow');
    win.show();
  }
  ,handleActionColumn : function(column, action, grid, rowIndex, colIndex, model, node) {

          
          if(action){
              switch(action){
                case 'deleteNews':
                  grid.getStore().remove(model);
                  grid.getStore().sync();

                  break;

                  case 'viewimg':

                  var reqUrl = "info/journal/membernews/viewimg/"+model.get('memberNewsId');
                  window.open(reqUrl,"","toolbar=0 width=600 height=400");
                

                  break;
              }
          }
      
  },
  savemembernews: function(btn)
  {
    var form = btn.up('window').down('form').getForm();
    if(form.isValid())
    {
        form.submit({
            url: 'info/journal/membernews/savemembernews',
            method: 'POST',
            waitMsg: 'กรุณารอสักครู่..',
            success: function(form, action) {
              
              Ext.Msg.alert('', 'บันทึกข้อมูลเรียบร้อยแล้ว');

              var grid = Ext.ComponentQuery.query('#membernews-journal-grid')[0];
              grid.getStore().reload();

            
              
            }
          ,failure: function(form, action)
          {
            Ext.Msg.alert('Failure', "ไม่สามารถบันทึกข้อมูลได้");
          }
        });
    }
  }
 
});