Ext.define('News.model.news.JournalModel',{
	extend: 'Ext.data.Model',
	fields:[
			{name: 'newsId' ,persist: true},
			{name: 'newsHeader'},
			{name: 'newsDesc'},
			{name: 'newsImgPath'},
			{name: 'memberNews'},
			{name: 'licenseNo'},
			{name: 'traderType'},
			{name: 'newsTextPath'},
			{name: 'createDtm'}
	  ]
});