Ext.define('News.model.news.MemberNewsModel',{
	extend: 'Ext.data.Model',
	fields:[
		{name: 'memberNewsId' ,persist: true},
		{name: 'memberNewsHeader'},
		{name: 'memberNewsDesc'},
		{name: 'newsStartDate'},
		{name: 'newsEndDate'},
		{name: 'mewsImgPath'},
		{name: 'traderType'},
		{name: 'memberId'},
		{name: 'licenseNo'},
		{name: 'indentityNo'}
	  ]
});