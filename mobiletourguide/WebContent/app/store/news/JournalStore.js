Ext.define('News.store.news.JournalStore', {
	extend : 'Ext.data.Store',
	requires : [
		'News.model.news.JournalModel',
		'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json',
        'Ext.data.writer.Json'
	],
	// storeId : 'news-registrationstore',
	// groupField: 'registrationTypeName',
	model : 'News.model.news.JournalModel',
	// pageSize : 20,
 //    proxy: {
	//     type: 'ajax',
	//     actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
	//     api: {
	        
	//         read: 'info/journal/read'
	//     },
	//     reader: {
	//         type: 'json',
	//         // metaProperty: '',
	//         root: 'list',
	//         // idProperty: 'emailId',
	//         totalProperty: 'totalCount',
	//         successProperty: 'success',
	//         messageProperty: 'msg'
	//     },
	//     writer: {
	//         type: 'json',
	//         // encode: true,
	//         writeAllFields: true, ////just send changed fields
	//         // root: 'data',
	//         allowSingle: false, //always wrap in an array
	//         batch: false
	//     },
	//     listeners: {
	//         exception: function(proxy, response, operation){

	//             Ext.MessageBox.show({
	//                 title: 'REMOTE EXCEPTION',
	//                 msg: operation.getError(),
	//                 icon: Ext.MessageBox.ERROR,
	//                 buttons: Ext.Msg.OK
	//             });
	//         }
	//     }
	// }
});