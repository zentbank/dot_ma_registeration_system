Ext.define('News.store.news.MemberNewsStore', {
	extend : 'Ext.data.Store',
	requires : [
		'News.model.news.MemberNewsModel',
		'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json',
        'Ext.data.writer.Json'
	],

	model : 'News.model.news.MemberNewsModel',

});