Ext.define('News.view.CenterView' ,{
    extend: 'Ext.panel.Panel',
    alias : 'widget.centerview',
    requires : [
        'News.view.news.MemberNewsPanel'
    ],
   layout: 'card',
    activeItem: 0,

    items:[
        // {
        //     xtype: 'journal-panel',
        //     id: 'publicnews-journal-panel',
        //     roleAction: 'publicnews'

        // },
        {
            xtype: 'journal-membernews-panel',
            id: 'membernews-journal-panel',
            roleAction: 'membernews'

        }
        // ,
        // {
        //     xtype: 'journal-panel',
        //     id: 'membernews-journal-panel',
        //     roleAction: 'membernews'

        // }
    ]

});    







