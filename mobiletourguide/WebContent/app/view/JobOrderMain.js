Ext.define('JobOrder.view.JobOrderMain', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
        'Ext.layout.CardLayout'
    ],
    
    xtype: 'joborder-view-main',

    layout: {
        type: 'border'
    },
    // style: 'background: #D3D6D8;',
    items: [
    {
        html: 'joborder-view-main'
    }]
});