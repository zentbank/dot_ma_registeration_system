Ext.define('JobOrder.view.JobOrderViewport', {
    extend: 'Ext.container.Viewport',
    requires:[
        'Ext.layout.container.Fit',
        'JobOrder.view.JobOrderMain'
    ],

    layout: {
        type: 'fit'
    },

    items: [{
        xtype: 'joborder-view-main'
    }]
});
