// Ext.define('News.view.Main', {
//     extend: 'Ext.container.Container',
//     requires:[
//         'Ext.tab.Panel',
//         'Ext.layout.container.Border'
//     ],
    
//     xtype: 'app-main',

//     layout: {
//         type: 'border'
//     },

//     items: [{
//         region: 'west',
//         xtype: 'panel',
//         title: 'west',
//         width: 150
//     },{
//         region: 'center',
//         xtype: 'tabpanel',
//         items:[{
//             title: 'Center Tab 1'
//         }]
//     }]
// });

Ext.define('News.view.Main', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
        'Ext.layout.CardLayout',
        'News.view.CenterView'
    ],
    
    xtype: 'app-main',

    layout: {
        type: 'border'
    },
    // style: 'background: #D3D6D8;',
    items: [
    {
        region: 'west',
        xtype: 'treemenu',
        title: 'เมนู',
        padding: '5px 0px 5px 5px',
        collapsible: true,   // make collapsible
        width: 280,
        frame: false,
        border: false,
        split: true,
        splitterResize: true
    },{
        region: 'center',
        xtype: 'centerview',
        plain: true,
        padding: '5px 5px 5px 2px',
        frame: false,
        border: false

    }]
});