Ext.define('News.view.menu.TreeMenu' ,{
    extend: 'Ext.tree.Panel',
    alias : 'widget.treemenu',
    requires: [
        'Ext.data.TreeStore',
        'Ext.selection.TreeModel',
        'Ext.tree.Column',
        'Ext.tree.View',
        'News.store.menu.MenuStore'
    ],
    title: 'เมนู',
    // width: 200,
    // height: 150,
    store: 'News.store.menu.MenuStore',
    rootVisible: false,
    root: 'เมนู'

});    
