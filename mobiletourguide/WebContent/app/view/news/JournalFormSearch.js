Ext.define('News.view.news.JournalFormSearch' ,{
    // extend: 'Ext.form.FieldSet',
    extend: 'Ext.form.FormPanel',
    alias : 'widget.journal-formsearch',
    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.Time',
        'Ext.form.CheckboxGroup',
        'Ext.layout.container.HBox',
        'Ext.form.FieldSet'
    ],
    bodyPadding: 10,
    // width: '100%',
    items: [
        {
                xtype: 'container',
                // title: 'Payment',
                layout: 'anchor',
                defaults: {
                    anchor: '100%',
                    labelWidth: 150
                },
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'หัวข้อข่าวสาร',
                        name: 'newsHeader',
                        anchor: '50%'
                    }
                    
                    
                ]
        },
        {
            xtype: 'toolbar',
            border: false,
            padding: '6px 0 6px 0px',
            items: [{
                xtype: 'button',
                text : 'ค้นหา',
                action: 'searchJournal'
            }]
        }

    ]
});    
