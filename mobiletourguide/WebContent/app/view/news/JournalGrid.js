Ext.define('News.view.news.JournalGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.journal-grid',
		stripeRows: true,
		requires: [
			'Ext.grid.Column',
			'News.store.news.JournalStore',
			'Ext.toolbar.Paging',
			'Ext.grid.column.Template',
			'Ext.grid.column.Action',
			'News.view.news.publicnews.PublicNewsAddEditWindow'
		],
		initComponent: function(){
			var journalStore = Ext.create('News.store.news.JournalStore',{
				storeId: this.storeId,
				pageSize : this.storepageSize,
				autoLoad: true,
				// groupField: 'registrationTypeName',
				proxy: {
					type: 'ajax',
					actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
					api: {
					    read: this.storeUrl + '/read',
					    destroy: this.storeUrl + '/delete'
					},
					reader: {
					    type: 'json',
					    root: 'list',
					    totalProperty: 'totalCount',
					    successProperty: 'success',
					    messageProperty: 'msg'
					},
					listeners: {
					    exception: function(proxy, response, operation){

					        Ext.MessageBox.show({
					            title: 'REMOTE EXCEPTION',
					            msg: operation.getError(),
					            icon: Ext.MessageBox.ERROR,
					            buttons: Ext.Msg.OK
					        });
					    }
					}
				}
			});

			


			Ext.apply(this, {
				store: journalStore,
				dockedItems:[
					{
						xtype: 'pagingtoolbar',
						store: journalStore,
						dock: 'bottom',
						displayInfo: true,
						items:[
							{
								text: 'เพิ่มข่าวสาร',
								action: 'addpublicnews'
			        		}
		        		]

					}
				],
				columns: [{
						header: 'newsId',
						dataIndex: 'newsId',
						hidden: true
					},
					{
						header: 'หัวข้อข่าวสาร',
						dataIndex: 'newsHeader',
						flex: 6
					},
					{
						header: 'วันที่เพิ่มข่าว',
						dataIndex: 'createDtm',
						flex: 1
					},
			        {
			        	header: 'รูปภาพ',
			            xtype: 'actioncolumn',
			           	flex: 1,
			           	align : 'center',
			            items: [{
			                // iconCls: 'icon-imgview-xsmall',
			                // tooltip: 'รูปภาพ',
			                getClass: function(v, meta, rec) {          
			                    if (!Ext.isEmpty(rec.get('newsImgPath'))) {
			                        this.items[0].tooltip = 'รูปภาพ';
			                        return 'icon-imgview-xsmall';
			                    } else {
			                      
			                        return '';
			                    }
			                },
				            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {

				            	if (!Ext.isEmpty(record.get('newsImgPath'))) {
				            		this.fireEvent('itemclick', this, 'viewimg', grid, rowIndex, colIndex, record, node);
								}
				            }
			            }]
			        },
			        {
			        	header: 'ลบ',
			            xtype: 'actioncolumn',
			           	flex: 1,
			           	align : 'center',
			            items: [{
			                iconCls: 'icon-delete-xsmall',
			                tooltip: 'ลบ',
				            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {

				            		this.fireEvent('itemclick', this, 'deleteNews', grid, rowIndex, colIndex, record, node);

				            }
			            }]
			        }]
			});

			this.callParent(arguments);
		}
	});