Ext.define('News.view.news.JournalPanel', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
        'News.view.news.JournalGrid',
        'News.view.news.JournalFormSearch'
    ],
    
    xtype: 'journal-panel',
    initComponent: function(){

    	Ext.apply(this, {
    		layout: {
		        type: 'border'
		    },
		    items: [

		    {
		        region: 'north',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items:[{
    		
			            title:'ค้นหาข่าวสาร',
			            collapsible: true,   // make collapsible
			            // collapsed : true,
			            xtype: 'journal-formsearch',
			            id: this.roleAction+'-journal-formsearch',
			            padding: '5px 5px 0px 5px',
			            frame: false,
			            border: false
		    	}]
		    },

		    {
		        region: 'center',
		        xtype: 'panel',
		        layout: 'fit',
		        frame: false,
		        border: false,
		        items:[{
		            // title: 'การทำรายการใบอนุญาตธุรกิจนำเที่ยว',
		            xtype: 'journal-grid',
		            id: this.roleAction+'-journal-grid',
		            padding: '5px 5px 5px 5px',
		            autoScroll: true,
		            // frame: false 
		            border: false,
		            storeId: this.roleAction+'-news-store',
		            storepageSize: 20,
		            storeUrl: 'info/journal/'+this.roleAction,
		            roleAction: this.roleAction,
		            traderType: this.traderType
		            
		        }]
		    }]
    	});

    	this.callParent(arguments);
    }
});