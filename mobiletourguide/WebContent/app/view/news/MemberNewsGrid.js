Ext.define('News.view.news.MemberNewsGrid',{
		extend: 'Ext.grid.GridPanel',
		alias: 'widget.membernews-grid',
		stripeRows: true,
		requires: [
			'Ext.grid.Column',
			'News.store.news.MemberNewsStore',
			'Ext.toolbar.Paging',
			'Ext.grid.column.Template',
			'Ext.grid.column.Action',
			'Ext.grid.plugin.RowExpander',
			'News.view.news.membernews.MemberNewsAddEditWindow'
		],
		initComponent: function(){
			var journalStore = Ext.create('News.store.news.MemberNewsStore',{
				storeId: this.storeId,
				pageSize : this.storepageSize,
				autoLoad: true,
				// groupField: 'registrationTypeName',
				proxy: {
					type: 'ajax',
					actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
					api: {
					    read: this.storeUrl + '/read',
					    destroy: this.storeUrl + '/delete'
					},
					reader: {
					    type: 'json',
					    root: 'list',
					    totalProperty: 'totalCount',
					    successProperty: 'success',
					    messageProperty: 'msg'
					},
					listeners: {
					    exception: function(proxy, response, operation){

					        Ext.MessageBox.show({
					            title: 'REMOTE EXCEPTION',
					            msg: operation.getError(),
					            icon: Ext.MessageBox.ERROR,
					            buttons: Ext.Msg.OK
					        });
					    }
					}
				}
			});

			


			Ext.apply(this, {
				store: journalStore,
				dockedItems:[
					{
						xtype: 'pagingtoolbar',
						store: journalStore,
						dock: 'bottom',
						displayInfo: true,
						items:[
							{
								text: 'เพิ่มข่าวสารสมาชิก',
								action: 'addmembernews'
			        		}
		        		]

					}
				],
				columns: [{
						header: 'newsId',
						dataIndex: 'memberNewsId',
						hidden: true
					},
					{
						header: 'หัวข้อข่าวสาร',
						dataIndex: 'memberNewsHeader',
						flex: 4
					},
					{
						header: 'วันที่ประกาศ',
						dataIndex: 'newsStartDate',
						flex: 1
					},
					{
						header: 'วันที่สิ้นสุด',
						dataIndex: 'newsEndDate',
						flex: 1
					},
			        {
			        	header: 'รูปภาพ',
			            xtype: 'actioncolumn',
			           	flex: 1,
			           	align : 'center',
			            items: [{
			                // iconCls: 'icon-imgview-xsmall',
			                // tooltip: 'รูปภาพ',
			                getClass: function(v, meta, rec) {          
			                    if (!Ext.isEmpty(rec.get('mewsImgPath'))) {
			                        this.items[0].tooltip = 'รูปภาพ';
			                        return 'icon-imgview-xsmall';
			                    } else {
			                      
			                        return '';
			                    }
			                },
				            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {

				            	if (!Ext.isEmpty(record.get('mewsImgPath'))) {
				            		this.fireEvent('itemclick', this, 'viewimg', grid, rowIndex, colIndex, record, node);
								}
				            }
			            }]
			        },
			        {
			        	header: 'ลบ',
			            xtype: 'actioncolumn',
			           	flex: 1,
			           	align : 'center',
			            items: [{
			                iconCls: 'icon-delete-xsmall',
			                tooltip: 'ลบ',
				            handler : function(grid, rowIndex, colIndex, node, e, record, rowNode) {

				            		this.fireEvent('itemclick', this, 'deleteNews', grid, rowIndex, colIndex, record, node);

				            }
			            }]
			        }]
			});

			this.callParent(arguments);
		},
		 plugins: [{
            ptype: 'rowexpander',
            pluginId: 'rowexpandernews',
            rowBodyTpl : new Ext.XTemplate(
            	'<div>',
            		'<p>{memberNewsDesc}</p>',
                '<div>'

            )
        }]
	});