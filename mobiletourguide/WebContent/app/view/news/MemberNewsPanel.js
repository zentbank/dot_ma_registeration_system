Ext.define('News.view.news.MemberNewsPanel', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
        'News.view.news.MemberNewsGrid',
        'News.view.news.MemberNewsFormSearch'
    ],
    xtype: 'journal-membernews-panel',
    initComponent: function(){

    	Ext.apply(this, {
    		layout: {
		        type: 'border'
		    },
		    items: [

		    {
		        region: 'north',
		        xtype: 'panel',
		        frame: false,
		        border: false,
		        items:[{
    		
			            title:'ค้นหาข่าวสาร',
			            collapsible: true,   // make collapsible
			            // collapsed : true,
			            xtype: 'membernews-formsearch',
			            id: this.roleAction+'-journal-formsearch',
			            padding: '5px 5px 0px 5px',
			            frame: false,
			            border: false
		    	}]
		    },

		    {
		        region: 'center',
		        xtype: 'panel',
		        layout: 'fit',
		        frame: false,
		        border: false,
		        items:[{
		            // title: 'การทำรายการใบอนุญาตธุรกิจนำเที่ยว',
		            xtype: 'membernews-grid',
		            id: this.roleAction+'-journal-grid',
		            padding: '5px 5px 5px 5px',
		            autoScroll: true,
		            // frame: false 
		            border: false,
		            storeId: this.roleAction+'-membernews-store',
		            storepageSize: 20,
		            storeUrl: 'info/journal/'+this.roleAction,
		            roleAction: this.roleAction,
		            traderType: this.traderType
		        }]
		    }]
    	});

    	this.callParent(arguments);
    }
});