Ext.define('News.view.news.membernews.MemberNewsAddEditForm' ,{
    extend: 'Ext.form.FormPanel',
   // alias : 'widget.account-guarantee-formaddedit',
    xtype: 'membernews-formaddedit',
    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.Time',
        'Ext.form.CheckboxGroup',
        'Ext.form.FieldSet',
        'Ext.util.Positionable',
        'Ext.form.FieldContainer',
        'Ext.form.field.VTypes',
        'Ext.form.field.Hidden',
        'Ext.form.field.File'
    ],
    bodyPadding: 5,
    initComponent: function(){
    	var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

        Ext.apply(this, {
            width: '100%',
            height: '100%',
            fieldDefaults: {
                labelAlign: 'left',
                // labelWidth: 50,
                msgTarget: 'qtip'
            },

            items: 
            [ 
                {
                    xtype: 'container',
                    layout: 'anchor',
                    
                    defaults: {
                        anchor: '100%',
                        labelWidth: 150
                    },
                    items:
                    [

                        // {
                        //     xtype: 'hiddenfield',
                        //     name: 'newsId'
                        // },
                        {
                            
                            xtype: 'textfield',
                            name: 'memberNewsHeader',
                            fieldLabel: 'หัวข้อข่าว',
                            afterLabelTextTpl: required,
                            allowBlank: false
                           
                        },
                         {
                            xtype: 'textareafield',
                            fieldLabel: 'รายละเอียดของข่าว',
                            name: 'memberNewsDesc',
                            margins: '0',
                            afterLabelTextTpl: required,
                            allowBlank: false
                        }
                        ,{
                            xtype: 'combo',
                            fieldLabel: 'ประเภทการใบอนุญาต',
                            store: 'News.store.combo.TraderTypeStore',
                            queryMode: 'local',
                            displayField: 'traderTypeName',
                            valueField: 'traderType',
                            hiddenName: 'traderType',
                            name: 'traderType',
                            afterLabelTextTpl: required,
                            allowBlank: false
                        }
                        ,{
                            xtype: 'datefield',
                            fieldLabel: 'วันที่ประกาศ',
                            name: 'newsStartDate',
                            
                            format: 'd/m/B',
                            allowBlank: false
                            // ,
                            // margin: '0 0 0 5'
                            // ,value: Ext.Date.add(new Date(), Ext.Date.YEAR, 543), //2554 ณ ปัจจุบัน
                           
                        },{
                            xtype: 'datefield',
                            fieldLabel: 'วันที่สิ้นสุด',
                            name: 'newsEndDate',
                         
                            format: 'd/m/B',
                            allowBlank: false
                            // ,
                            // margin: '0 0 0 5'
                            // ,value: Ext.Date.add(new Date(), Ext.Date.YEAR, 543), //2554 ณ ปัจจุบัน
                           
                        },
                        ,{
                            xtype: 'filefield',
                            id: 'form-file',
                            emptyText: 'กรุณาเลือก ไฟล์ที่ต้องการ',
                            fieldLabel: 'ภาพข่าว',
                            name: 'newsImg',
                            buttonText: '',
                            afterLabelTextTpl: required,
                            allowBlank: false,
                            buttonConfig: {
                                iconCls: 'upload-icon'
                            }
                        }
                        

                    ]
                }
            ],

            buttons: [{
                text: 'บันทึก',
                scope: this,
                action: 'savemembernews'
                // handler: this.onCompleteClick
            }, {
                text: 'ยกเลิก',
                // width: 150,
                scope: this,
                handler: this.onResetClick
            }]    
        });
        this.callParent(arguments);
    },
   
    onResetClick: function()
    {
    	var winx = this.up('window');
    	winx.close();
    }

});  


