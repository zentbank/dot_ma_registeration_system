Ext.define('News.view.news.membernews.MemberNewsAddEditWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.news-membernews-addeditwindow',

    requires: [
        'News.view.news.membernews.MemberNewsAddEditForm'
    ],

    title : 'บันทึกข้อมูลข่าวสาร',
    layout: 'fit',
    // autoShow: true,
    width: 500,
    // height: 300,
    //True to make the window modal and mask everything behind it when displayed
    modal: true,
    // iconCls: 'icon-form',

    initComponent: function() {

        this.items = [
            {
                xtype: 'membernews-formaddedit',
                frame: false,
                border: false
            }
        ];

        this.callParent(arguments);
    },

});
