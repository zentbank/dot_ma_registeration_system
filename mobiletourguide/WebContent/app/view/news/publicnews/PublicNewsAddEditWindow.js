Ext.define('News.view.news.publicnews.PublicNewsAddEditWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.news-publicnews-addeditwindow',

    requires: [
        'News.view.news.publicnews.PublicNewsAddEditForm'
    ],

    title : 'บันทึกข้อมูลข่าวสาร',
    layout: 'fit',
    // autoShow: true,
    width: 500,
    height: 250,
    //True to make the window modal and mask everything behind it when displayed
    modal: true,
    // iconCls: 'icon-form',

    initComponent: function() {

        this.items = [
            {
                xtype: 'publicnews-formaddedit',
                frame: false,
                border: false
            }
        ];

        this.callParent(arguments);
    },

});
