/*
This file is part of Ext JS 4.2

Copyright (c) 2011-2013 Sencha Inc

Contact:  http://www.sencha.com/contact

GNU General Public License Usage
This file may be used under the terms of the GNU General Public License version 3.0 as
published by the Free Software Foundation and appearing in the file LICENSE included in the
packaging of this file.

Please review the following information to ensure the GNU General Public License version 3.0
requirements will be met: http://www.gnu.org/copyleft/gpl.html.

If you are unsure which license is appropriate for your use, please contact the sales department
at http://www.sencha.com/contact.

Build date: 2013-05-16 14:36:50 (f9be68accb407158ba2b1be2c226a6ce1f649314)
*/
/**
 * List compiled by KillerNay on the extjs.com forums.
 * Thank you KillerNay!
 *
 * Thailand Translations
 */
Ext.onReady(function() {

   
    Ext.Ajax.timeout = 120000;
    Ext.data.proxy.Server = 120000;
    Ext.data.JsonP = 120000;

    if (Ext.Date) {
        Ext.Date.monthNames = ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."];

        Ext.Date.getShortMonthName = function(month) {
            return Ext.Date.monthNames[month].substring(0, 4);
        };

        Ext.Date.monthNumbers = {
            "ม.ค.": 0,
            "ก.พ.": 1,
            "มี.ค.": 2,
            "เม.ย.": 3,
            "พ.ค.": 4,
            "มิ.ย.": 5,
            "ก.ค.": 6,
            "ส.ค.": 7,
            "ก.ย.": 8,
            "ต.ค.": 9,
            "พ.ย.": 10,
            "ธ.ค.": 11
        };

        Ext.Date.getMonthNumber = function(name) {
            return Ext.Date.monthNumbers[name.substring(0, 1).toUpperCase() + name.substring(1, 4).toLowerCase()];
        };

        Ext.Date.dayNames = ["อ.", "จ.","อ.", "พ.", "พ.", "ศ.", "ส."];

        Ext.Date.getShortDayName = function(day) {
            return Ext.Date.dayNames[day].substring(0, 3);
        };

        Ext.Date.formatCodes = {
            d: "Ext.String.leftPad(this.getDate(), 2, '0')",
            D: "Ext.Date.getShortDayName(this.getDay())", // get localized short day name
            j: "this.getDate()",
            l: "Ext.Date.dayNames[this.getDay()]",
            N: "(this.getDay() ? this.getDay() : 7)",
            S: "Ext.Date.getSuffix(this)",
            w: "this.getDay()",
            z: "Ext.Date.getDayOfYear(this)",
            W: "Ext.String.leftPad(Ext.Date.getWeekOfYear(this), 2, '0')",
            F: "Ext.Date.monthNames[this.getMonth()]",
            m: "Ext.String.leftPad(this.getMonth() + 1, 2, '0')",
            M: "Ext.Date.getShortMonthName(this.getMonth())", // get localized short month name
            n: "(this.getMonth() + 1)",
            t: "Ext.Date.getDaysInMonth(this)",
            L: "(Ext.Date.isLeapYear(this) ? 1 : 0)",
            o: "(this.getFullYear() + (Ext.Date.getWeekOfYear(this) == 1 && this.getMonth() > 0 ? +1 : (Ext.Date.getWeekOfYear(this) >= 52 && this.getMonth() < 11 ? -1 : 0)))",
            Y: "Ext.String.leftPad(this.getFullYear(), 4, '0')",
            y: "('' + this.getFullYear()).substring(2, 4)",
            B: "Ext.String.leftPad(this.getFullYear()+543, 4, '0')",
            a: "(this.getHours() < 12 ? 'am' : 'pm')",
            A: "(this.getHours() < 12 ? 'AM' : 'PM')",
            g: "((this.getHours() % 12) ? this.getHours() % 12 : 12)",
            G: "this.getHours()",
            h: "Ext.String.leftPad((this.getHours() % 12) ? this.getHours() % 12 : 12, 2, '0')",
            H: "Ext.String.leftPad(this.getHours(), 2, '0')",
            i: "Ext.String.leftPad(this.getMinutes(), 2, '0')",
            s: "Ext.String.leftPad(this.getSeconds(), 2, '0')",
            u: "Ext.String.leftPad(this.getMilliseconds(), 3, '0')",
            O: "Ext.Date.getGMTOffset(this)",
            P: "Ext.Date.getGMTOffset(this, true)",
            T: "Ext.Date.getTimezone(this)",
            Z: "(this.getTimezoneOffset() * -60)",

            c: function() { // ISO-8601 -- GMT format
                var c, code, i, l, e;
                for (c = "Y-m-dTH:i:sP", code = [], i = 0, l = c.length; i < l; ++i) {
                    e = c.charAt(i);
                    code.push(e == "T" ? "'T'" : utilDate.getFormatCode(e)); // treat T as a character literal
                }
                return code.join(" + ");
            },
            U: "Math.round(this.getTime() / 1000)"
        };

        Ext.Date.parseCodes = {
            /*
             * Notes:
             * g = {Number} calculation group (0 or 1. only group 1 contributes to date calculations.)
             * c = {String} calculation method (required for group 1. null for group 0. {0} = currentGroup - position in regex result array)
             * s = {String} regex pattern. all matches are stored in results[], and are accessible by the calculation mapped to 'c'
             */
            d: {
                g:1,
                c:"d = parseInt(results[{0}], 10);\n",
                s:"(3[0-1]|[1-2][0-9]|0[1-9])" // day of month with leading zeroes (01 - 31)
            },
            j: {
                g:1,
                c:"d = parseInt(results[{0}], 10);\n",
                s:"(3[0-1]|[1-2][0-9]|[1-9])" // day of month without leading zeroes (1 - 31)
            },
            D: function() {
                for (var a = [], i = 0; i < 7; a.push(utilDate.getShortDayName(i)), ++i); // get localised short day names
                return {
                    g:0,
                    c:null,
                    s:"(?:" + a.join("|") +")"
                };
            },
            l: function() {
                return {
                    g:0,
                    c:null,
                    s:"(?:" + utilDate.dayNames.join("|") + ")"
                };
            },
            N: {
                g:0,
                c:null,
                s:"[1-7]" // ISO-8601 day number (1 (monday) - 7 (sunday))
            },
            //<locale type="object" property="parseCodes">
            S: {
                g:0,
                c:null,
                s:"(?:st|nd|rd|th)"
            },
            //</locale>
            w: {
                g:0,
                c:null,
                s:"[0-6]" // JavaScript day number (0 (sunday) - 6 (saturday))
            },
            z: {
                g:1,
                c:"z = parseInt(results[{0}], 10);\n",
                s:"(\\d{1,3})" // day of the year (0 - 364 (365 in leap years))
            },
            W: {
                g:1,
                c:"W = parseInt(results[{0}], 10);\n",
                s:"(\\d{2})" // ISO-8601 week number (with leading zero)
            },
            F: function() {
                return {
                    g:1,
                    c:"m = parseInt(me.getMonthNumber(results[{0}]), 10);\n", // get localised month number
                    s:"(" + utilDate.monthNames.join("|") + ")"
                };
            },
            M: function() {
                for (var a = [], i = 0; i < 12; a.push(utilDate.getShortMonthName(i)), ++i); // get localised short month names
                return Ext.applyIf({
                    s:"(" + a.join("|") + ")"
                }, utilDate.formatCodeToRegex("F"));
            },
            m: {
                g:1,
                c:"m = parseInt(results[{0}], 10) - 1;\n",
                s:"(1[0-2]|0[1-9])" // month number with leading zeros (01 - 12)
            },
            n: {
                g:1,
                c:"m = parseInt(results[{0}], 10) - 1;\n",
                s:"(1[0-2]|[1-9])" // month number without leading zeros (1 - 12)
            },
            t: {
                g:0,
                c:null,
                s:"(?:\\d{2})" // no. of days in the month (28 - 31)
            },
            L: {
                g:0,
                c:null,
                s:"(?:1|0)"
            },
            o: { 
                g: 1,
                c: "y = parseInt(results[{0}], 10);\n",
                s: "(\\d{4})" // ISO-8601 year number (with leading zero)

            },
            Y: {
                g:1,
                c:"y = parseInt(results[{0}], 10);\n",
                s:"(\\d{4})" // 4-digit year
            },
            y: {
                g:1,
                c:"var ty = parseInt(results[{0}], 10);\n"
                    + "y = ty > me.y2kYear ? 1900 + ty : 2000 + ty;\n", // 2-digit year
                s:"(\\d{1,2})"
            },
            B: {
                g:1,
                // c:"y = parseInt(results[{0}]-543, 10);\n",
                c:"y = parseInt(results[{0}]-543, 10);\n",
                s:"(\\d{4})" // 4-digit year
            },
            /*
             * In the am/pm parsing routines, we allow both upper and lower case
             * even though it doesn't exactly match the spec. It gives much more flexibility
             * in being able to specify case insensitive regexes.
             */
            //<locale type="object" property="parseCodes">
            a: {
                g:1,
                c:"if (/(am)/i.test(results[{0}])) {\n"
                    + "if (!h || h == 12) { h = 0; }\n"
                    + "} else { if (!h || h < 12) { h = (h || 0) + 12; }}",
                s:"(am|pm|AM|PM)",
                calcAtEnd: true
            },
            //</locale>
            //<locale type="object" property="parseCodes">
            A: {
                g:1,
                c:"if (/(am)/i.test(results[{0}])) {\n"
                    + "if (!h || h == 12) { h = 0; }\n"
                    + "} else { if (!h || h < 12) { h = (h || 0) + 12; }}",
                s:"(AM|PM|am|pm)",
                calcAtEnd: true
            },
            //</locale>
            g: {
                g:1,
                c:"h = parseInt(results[{0}], 10);\n",
                s:"(1[0-2]|[0-9])" //  12-hr format of an hour without leading zeroes (1 - 12)
            },
            G: {
                g:1,
                c:"h = parseInt(results[{0}], 10);\n",
                s:"(2[0-3]|1[0-9]|[0-9])" // 24-hr format of an hour without leading zeroes (0 - 23)
            },
            h: {
                g:1,
                c:"h = parseInt(results[{0}], 10);\n",
                s:"(1[0-2]|0[1-9])" //  12-hr format of an hour with leading zeroes (01 - 12)
            },
            H: {
                g:1,
                c:"h = parseInt(results[{0}], 10);\n",
                s:"(2[0-3]|[0-1][0-9])" //  24-hr format of an hour with leading zeroes (00 - 23)
            },
            i: {
                g:1,
                c:"i = parseInt(results[{0}], 10);\n",
                s:"([0-5][0-9])" // minutes with leading zeros (00 - 59)
            },
            s: {
                g:1,
                c:"s = parseInt(results[{0}], 10);\n",
                s:"([0-5][0-9])" // seconds with leading zeros (00 - 59)
            },
            u: {
                g:1,
                c:"ms = results[{0}]; ms = parseInt(ms, 10)/Math.pow(10, ms.length - 3);\n",
                s:"(\\d+)" // decimal fraction of a second (minimum = 1 digit, maximum = unlimited)
            },
            O: {
                g:1,
                c:[
                    "o = results[{0}];",
                    "var sn = o.substring(0,1),", // get + / - sign
                        "hr = o.substring(1,3)*1 + Math.floor(o.substring(3,5) / 60),", // get hours (performs minutes-to-hour conversion also, just in case)
                        "mn = o.substring(3,5) % 60;", // get minutes
                    "o = ((-12 <= (hr*60 + mn)/60) && ((hr*60 + mn)/60 <= 14))? (sn + Ext.String.leftPad(hr, 2, '0') + Ext.String.leftPad(mn, 2, '0')) : null;\n" // -12hrs <= GMT offset <= 14hrs
                ].join("\n"),
                s: "([+-]\\d{4})" // GMT offset in hrs and mins
            },
            P: {
                g:1,
                c:[
                    "o = results[{0}];",
                    "var sn = o.substring(0,1),", // get + / - sign
                        "hr = o.substring(1,3)*1 + Math.floor(o.substring(4,6) / 60),", // get hours (performs minutes-to-hour conversion also, just in case)
                        "mn = o.substring(4,6) % 60;", // get minutes
                    "o = ((-12 <= (hr*60 + mn)/60) && ((hr*60 + mn)/60 <= 14))? (sn + Ext.String.leftPad(hr, 2, '0') + Ext.String.leftPad(mn, 2, '0')) : null;\n" // -12hrs <= GMT offset <= 14hrs
                ].join("\n"),
                s: "([+-]\\d{2}:\\d{2})" // GMT offset in hrs and mins (with colon separator)
            },
            T: {
                g:0,
                c:null,
                s:"[A-Z]{1,5}" // timezone abbrev. may be between 1 - 5 chars
            },
            Z: {
                g:1,
                c:"zz = results[{0}] * 1;\n" // -43200 <= UTC offset <= 50400
                      + "zz = (-43200 <= zz && zz <= 50400)? zz : null;\n",
                s:"([+-]?\\d{1,5})" // leading '+' sign is optional for UTC offset
            },
            c: function() {
                var calc = [],
                    arr = [
                        utilDate.formatCodeToRegex("Y", 1), // year
                        utilDate.formatCodeToRegex("m", 2), // month
                        utilDate.formatCodeToRegex("d", 3), // day
                        utilDate.formatCodeToRegex("H", 4), // hour
                        utilDate.formatCodeToRegex("i", 5), // minute
                        utilDate.formatCodeToRegex("s", 6), // second
                        {c:"ms = results[7] || '0'; ms = parseInt(ms, 10)/Math.pow(10, ms.length - 3);\n"}, // decimal fraction of a second (minimum = 1 digit, maximum = unlimited)
                        {c:[ // allow either "Z" (i.e. UTC) or "-0530" or "+08:00" (i.e. UTC offset) timezone delimiters. assumes local timezone if no timezone is specified
                            "if(results[8]) {", // timezone specified
                                "if(results[8] == 'Z'){",
                                    "zz = 0;", // UTC
                                "}else if (results[8].indexOf(':') > -1){",
                                    utilDate.formatCodeToRegex("P", 8).c, // timezone offset with colon separator
                                "}else{",
                                    utilDate.formatCodeToRegex("O", 8).c, // timezone offset without colon separator
                                "}",
                            "}"
                        ].join('\n')}
                    ],
                    i,
                    l;

                for (i = 0, l = arr.length; i < l; ++i) {
                    calc.push(arr[i].c);
                }

                return {
                    g:1,
                    c:calc.join(""),
                    s:[
                        arr[0].s, // year (required)
                        "(?:", "-", arr[1].s, // month (optional)
                            "(?:", "-", arr[2].s, // day (optional)
                                "(?:",
                                    "(?:T| )?", // time delimiter -- either a "T" or a single blank space
                                    arr[3].s, ":", arr[4].s,  // hour AND minute, delimited by a single colon (optional). MUST be preceded by either a "T" or a single blank space
                                    "(?::", arr[5].s, ")?", // seconds (optional)
                                    "(?:(?:\\.|,)(\\d+))?", // decimal fraction of a second (e.g. ",12345" or ".98765") (optional)
                                    "(Z|(?:[-+]\\d{2}(?::)?\\d{2}))?", // "Z" (UTC) or "-0530" (UTC offset without colon delimiter) or "+08:00" (UTC offset with colon delimiter) (optional)
                                ")?",
                            ")?",
                        ")?"
                    ].join("")
                };
            },
            U: {
                g:1,
                c:"u = parseInt(results[{0}], 10);\n",
                s:"(-?\\d+)" // leading minus sign indicates seconds before UNIX epoch
            }
        };
    }

    if (Ext.util && Ext.util.Format) {
        Ext.apply(Ext.util.Format, {
            thousandSeparator: ',',
            decimalSeparator: '.',
            // currencySign: '\u0e3f',
            currencySign: '',
            // Thai Baht
            dateFormat: 'm/d/B'
        });
    }
});

Ext.define("Ext.locale.th.view.View", {
    override: "Ext.view.View",
    emptyText: ""
});

Ext.define("Ext.locale.th.grid.plugin.DragDrop", {
    override: "Ext.grid.plugin.DragDrop",
    dragText: "{0} selected row{1}"
});

Ext.define("Ext.locale.th.tab.Tab", {
    override: "Ext.tab.Tab",
    closeText: "xxx"
});

Ext.define("Ext.locale.th.form.field.Base", {
    override: "Ext.form.field.Base",
    invalidText: "xxx"
});

// changing the msg text below will affect the LoadMask
Ext.define("Ext.locale.th.view.AbstractView", {
    override: "Ext.view.AbstractView",
    loadingText: "กำลังโหลดข้อมูล..."
});

Ext.define("Ext.locale.th.picker.Date", {
    override: "Ext.picker.Date",
    todayText: "วันนี้",
    minText: "This date is before the minimum date",
    maxText: "This date is after the maximum date",
    disabledDaysText: "",
    disabledDatesText: "",
    nextText: 'เดือนถัดไป (Control+Right)',
    prevText: 'เดือนก่อนหน้า (Control+Left)',
    monthYearText: 'ปิด (Control+Up/Down to move years)',
    todayTip: "{0} (Spacebar)",
    format: "m/d/B",
    startDay: 0
});

Ext.define("Ext.locale.th.picker.Month", {
    override: "Ext.picker.Month",
    okText: "ตกลง",
    cancelText: "ยกเลิก"
});

Ext.define("Ext.locale.th.toolbar.Paging", {
    override: "Ext.PagingToolbar",
    beforePageText: "แสดง",
    afterPageText: "จาก {0}",
    firstText: "หน้าแรก",
    prevText: "ก่อนหน้า",
    nextText: "ถัดไป",
    lastText: "หน้าสุดท้าย",
    refreshText: "Refresh",
    displayMsg: "แสดง {0} - {1} จาก {2}",
    emptyMsg: 'ไม่พบข้อมูล'
});

Ext.define("Ext.locale.th.form.field.Text", {
    override: "Ext.form.field.Text",
    minLengthText: "จำนวนตัวอักษรอย่างน้อย {0}",
    maxLengthText: "จำนวนตัวอักษรไม่มากกว่า {0}",
    blankText: "ไม่สามารถว่างได้",
    regexText: "",
    emptyText: null
});

Ext.define("Ext.locale.th.form.field.Number", {
    override: "Ext.form.field.Number",
    minText: "จำนวน อย่างน้อย {0}",
    maxText: "จำนวน ไม่มากกว่า {0}",
    nanText: "{0} ไม่ใช่ตัวเลข"
});

Ext.define("Ext.locale.th.form.field.Date", {
    override: "Ext.form.field.Date",
    disabledDaysText: "Disabled",
    disabledDatesText: "Disabled",
    minText: "ต้องเป็นวันหลังจากวันที่ {0}",
    maxText: "ต้องเป็นวันก่อนวันที่ {0}",
    invalidText: "{0} is not a valid date - it must be in the format {1}",
    format: "m/d/y",
    altFormats: "m/d/B|m-d-y|m-d-B|m/d|m-d|md|mdy|mdY|d|B-m-d"
});

Ext.define("Ext.locale.th.form.field.ComboBox", {
    override: "Ext.form.field.ComboBox",
    valueNotFoundText: undefined
}, function() {
    Ext.apply(Ext.form.field.ComboBox.prototype.defaultListConfig, {
        loadingText: "กำลังโหลดข้อมูล..."
    });
});

Ext.define("Ext.locale.th.form.field.VTypes", {
    override: "Ext.form.field.VTypes",
    emailText: 'กรุณากรอก อีเมล ในรูปแบบ "user@example.com"',
    urlText: 'กรุณากรอก URL ในรูปแบบ "http:/' + '/www.example.com"',
    alphaText: 'This field should only contain letters and _',
    alphanumText: 'This field should only contain letters, numbers and _'
});

Ext.define("Ext.locale.th.form.field.HtmlEditor", {
    override: "Ext.form.field.HtmlEditor",
    createLinkText: 'Please enter the URL for the link:'
}, function() {
    Ext.apply(Ext.form.field.HtmlEditor.prototype, {
        buttonTips: {
            bold: {
                title: 'Bold (Ctrl+B)',
                text: 'Make the selected text bold.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            italic: {
                title: 'Italic (Ctrl+I)',
                text: 'Make the selected text italic.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            underline: {
                title: 'Underline (Ctrl+U)',
                text: 'Underline the selected text.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            increasefontsize: {
                title: 'Grow Text',
                text: 'Increase the font size.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            decreasefontsize: {
                title: 'Shrink Text',
                text: 'Decrease the font size.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            backcolor: {
                title: 'Text Highlight Color',
                text: 'Change the background color of the selected text.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            forecolor: {
                title: 'Font Color',
                text: 'Change the color of the selected text.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            justifyleft: {
                title: 'Align Text Left',
                text: 'Align text to the left.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            justifycenter: {
                title: 'Center Text',
                text: 'Center text in the editor.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            justifyright: {
                title: 'Align Text Right',
                text: 'Align text to the right.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            insertunorderedlist: {
                title: 'Bullet List',
                text: 'Start a bulleted list.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            insertorderedlist: {
                title: 'Numbered List',
                text: 'Start a numbered list.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            createlink: {
                title: 'Hyperlink',
                text: 'Make the selected text a hyperlink.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            sourceedit: {
                title: 'Source Edit',
                text: 'Switch to source editing mode.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            }
        }
    });
});

Ext.define("Ext.locale.th.grid.header.Container", {
    override: "Ext.grid.header.Container",
    sortAscText: "Sort Ascending",
    sortDescText: "Sort Descending",
    lockText: "Lock Column",
    unlockText: "Unlock Column",
    columnsText: "Columns"
});

Ext.define("Ext.locale.th.grid.GroupingFeature", {
    override: "Ext.grid.GroupingFeature",
    emptyGroupText: '(None)',
    groupByText: 'Group By This Field',
    showGroupsText: 'Show in Groups'
});

Ext.define("Ext.locale.th.grid.PropertyColumnModel", {
    override: "Ext.grid.PropertyColumnModel",
    nameText: "Name",
    valueText: "Value",
    dateFormat: "m/j/Y"
});

Ext.define("Ext.locale.th.window.MessageBox", {
    override: "Ext.window.MessageBox",
    buttonText: {
        ok: "ตกลง",
        cancel: "ยกเลิก",
        yes: "ใช่",
        no: "ไม่"
    }    
});

// This is needed until we can refactor all of the locales into individual files
Ext.define("Ext.locale.th.Component", {	
    override: "Ext.Component"
});

