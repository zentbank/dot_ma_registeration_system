/*
    This file is generated and updated by Sencha Cmd. You can edit this file as
    needed for your application, but these edits will have to be merged by
    Sencha Cmd when upgrading.
*/

// DO NOT DELETE - this directive is required for Sencha Cmd packages to work.
//@require @packageOverrides

Ext.application({
    name: 'JobOrder',
    requires:[
        'JobOrder.view.JobOrderMain',
        'Ext.container.Viewport',
        'Ext.layout.container.Fit'
    ],

    extend: 'JobOrder.JobOrderApplication',
    
    // autoCreateViewport: true
     launch: function() {
        Ext.create('Ext.container.Viewport', {
                layout: {
			        type: 'fit'
			    },

			    items: [{
			        xtype: 'joborder-view-main'
			    }]
        });
    }
});
 
    
    