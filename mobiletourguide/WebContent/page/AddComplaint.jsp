<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="target-densitydpi=device-dpi, width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta name="description" content="Metro UI CSS">
    <meta name="author" content="Sergey Pimenov">
    <meta name="keywords" content="windows 8, modern style, Metro UI, style, modern, css, framework">

    <link href="${pageContext.request.contextPath}/page/css/modern.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/modern-responsive.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/site.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/page/js/google-code-prettify/prettify.css" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment_langs.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/dropdown.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/accordion.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/buttonset.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/carousel.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/input-control.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/pagecontrol.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/rating.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-drag.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/calendar.js"></script>

    <title>สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์</title>


</head>
<body class="metrouicss" >

<jsp:include page="navigation.jsp" /> 
 
<div class="page secondary">
    <div class="page-header">
        <div class="page-header-content">
            <h1>ร้องเรียน-ซักถาม</h1>
            <a href="javascript:history.back()" class="back-button big page-back"></a>
        </div>
       
    </div>

    <div class="page-region">
        <div class="page-region-content">
                

                <div class="grid">
                    <div class="row">
                        <div class="span8">
                             <blockquote class="place-right">
                               
                                

                                <br />

                                <h3>${model.traderName}</h3>
                                <h3>${model.traderNameEn}</h3>

                                <br /> 

                                <p>ใบอนุญาตเลขที่ ${model.licenseNo}</p>
                                <p>สถานะการจดทะเบียน <span class="fg-color-greenLight">${model.licenseStatus}</span></p>

                                <br /> 

                                <h4>ประเภทการจดทะเบียน</h4>
                                <h4>${model.traderCategory}</h4> 
                            </blockquote> 
                            
                            <br /> 
                            <h2>บันทึกเรื่อง</h2>

                             <h4><span class="fg-color-red">${model.errMsg}</span></h4>



                        </div>
                    </div>
                </div>

                <form  id="saveComp" action="${pageContext.request.contextPath}/info/license/complaint/save" enctype="multipart/form-data" method="post" >
                    <input type="hidden" name="traderId" value="${model.traderId}" />
                    <input type="hidden" name="traderType" value="${model.traderType}" />
                    <input type="hidden" name="licenseNo" value="${model.licenseNo}" />
                    <input type="hidden" name="traderName" value="${model.traderName}" />
                

                    <div class="grid">

                        <div class="row">
                             <div class="span3">
                                <label class="input-control text">
                                    ประเภทเรื่อง
                                </label>
                            </div>
                            <div class="span5">
                                <label class="input-control radio">
                                     <input type="radio" name="questionType" value="C" checked="" />
                                     <span class="helper">เรื่องร้องเรียน</span>
                                </label>
                                <label class="input-control radio">
                                     <input type="radio" name="questionType" value="Q"  />
                                     <span class="helper">สอบถามข้อสงสัย</span>
                                </label>

                            </div>
                        </div>

                        <div class="row">
                             <div class="span3">
                                <label class="input-control text">
                                    รายละเอียดเรื่อง
                                </label>
                            </div>
                            <div class="span5">
                                <div class="input-control textarea">
                                        <textarea id="complaintDesc" name="complaintDesc" value="${model.complaintDesc}" placeholder="บันทึกรายละเอียดเรื่อง" required="required">${model.complaintDesc}</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                             <div class="span3">
                                <label class="input-control text">
                                    หมายเลขบัตรประชาชน
                                </label>
                            </div>
                            <div class="span5">
                                <div class="input-control text">
                                    <input type="text" id="complaintCardId" name="complaintCardId" value="${model.complaintCardId}" placeholder="กรุณากรอกหมายเลขบัตประชาชน" required="required"/>
                                    <button class="btn-clear"></button>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                             <div class="span3">
                                <label class="input-control text">
                                    ชื่อ-นามสกุล
                                </label>
                            </div>
                            <div class="span5">
                                <div class="input-control text">
                                    <input type="text" id="complaintName" name="complaintName" value="${model.complaintName}" placeholder="กรุณากรอก ชื่อ-นามสกุล" required="required"/>
                                    <button class="btn-clear"></button>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                             <div class="span3">
                                <label class="input-control text">
                                    หมายเลขโทรศัพท์
                                </label>
                            </div>
                            <div class="span5">
                                <div class="input-control text">
                                    <input type="text" id="complaintTel" name="complaintTel" value="${model.complaintTel}" placeholder="กรุณากรอกหมายเลขหมายเลขโทรศัพท์" required="required"/>
                                    <button class="btn-clear"></button>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                             <div class="span3">
                                <label class="input-control text">
                                    อีเมลล์
                                </label>
                            </div>
                            <div class="span5">
                                <div class="input-control text">
                                    <input type="text" id="complaintEmail" name="complaintEmail" value="${model.complaintEmail}" placeholder="กรุณากรอกอีเมลล์" required="required"/>
                                    <button class="btn-clear"></button>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                             <div class="span3">
                                <label class="input-control text">
                                    เอกสารแนบ
                                </label>
                            </div>
                            <div class="span5">
                                <div class="input-control file">
                                    <input type="file" name="complaintPic" placeholder="เลือกรูปภาพ"/>
                                   
                                </div>
                            </div>
                        </div>

                        <div class="row">
                             <div class="span3">
                                <label class="input-control text">
                                    เอกสารแนบ 2
                                </label>
                            </div>
                            <div class="span5">
                                <div class="input-control file">
                                    <input type="file" name="complaintPic" placeholder="เลือกรูปภาพ"/>
                                   
                                </div>
                            </div>
                        </div>

                        <div class="row">
                             <div class="span3">
                                <label class="input-control text">
                                    เอกสารแนบ 3
                                </label>
                            </div>
                            <div class="span5">
                                <div class="input-control file">
                                    <input type="file" name="complaintPic" placeholder="เลือกรูปภาพ"/>
                                   
                                </div>
                            </div>
                        </div>

                    </div>

                    <button type="submit" class="big image-button bg-color-teal fg-color-white span8"> 
                       บันทึกเรื่องร้องเรียน<i class="icon-save bg-color-teal"></i>
                    </button>
                </form>

        </div>
    </div>

    <div class="page">
        <div class="nav-bar">
            <div class="nav-bar-inner padding10">
                <span class="element">
                    สำนักทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ กรมการท่องเที่ยว</a>
                </span>
            </div>
        </div>
    </div>

    </body>
</html>
