<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="target-densitydpi=device-dpi, width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta name="description" content="Metro UI CSS">
    <meta name="author" content="Sergey Pimenov">
    <meta name="keywords" content="windows 8, modern style, Metro UI, style, modern, css, framework">

    <link href="${pageContext.request.contextPath}/page/css/modern.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/modern-responsive.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/site.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/page/js/google-code-prettify/prettify.css" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment_langs.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/dropdown.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/accordion.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/buttonset.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/carousel.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/input-control.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/pagecontrol.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/rating.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-drag.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/calendar.js"></script>

</head>
<body class="metrouicss" >

<jsp:include page="navigation.jsp" />
 
<div class="page secondary">
    <div class="page-header">
        <div class="page-header-content">
            <h1>ร้องเรียน-ซักถาม</h1>
            <a href="javascript:history.back()" class="back-button big page-back"></a>
        </div>
       
    </div>

    <div class="page-region">
        <div class="page-region-content">
                
            <div class="grid">
                <div class="row">
                    <a href="${pageContext.request.contextPath}/info/license/tour">
                        <div  class="span6 bg-color-blue">
                            
                            <h2 class="fg-color-white padding-top5">&nbsp;ธุรกิจนำเที่ยว</h2>
                            <p class="fg-color-white">&nbsp;&nbsp;ร้องเรียนธุรกิจนำเที่ยว ซักถามข้อสงสัย</p>
                        </div>
                    </a>
                    <a href="${pageContext.request.contextPath}/info/license/guide">
                        <div class="span6 bg-color-green">
                           
                           
                            <h2 class="fg-color-white padding-top5">&nbsp;มัคคุเทศก์</h2>
                            <p class="fg-color-white">&nbsp;&nbsp;ร้องเรียนมัคคุเทศก์ ซักถามข้อสงสัย</p>
                        </div>
                    </a>
                    <a href="${pageContext.request.contextPath}/info/license/leader">
                        <div class="span6 bg-color-yellow">
                            
                            <h2 class="fg-color-white padding-top5">&nbsp;ผู้นำเที่ยว</h2>
                            <p class="fg-color-white">&nbsp;&nbsp;ร้องเรียนผู้นำเที่ยว ซักถามข้อสงสัย</p>
                        </div>
                    </a>
                    <a href="${pageContext.request.contextPath}/info/license/complaint/other">
                        <div class="span6 bg-color-redLight">
                           
                            <h2 class="fg-color-white padding-top5">&nbsp;ร้องเรียนอื่นๆ</h2>
                            <p class="fg-color-white">&nbsp;&nbsp;ร้องเรียน ซักถามข้อสงสัยอื่นๆ</p>
                        </div>
                    </a>
                </div>
            </div>

        </div>
    </div>

    <div class="page">
        <div class="nav-bar">
            <div class="nav-bar-inner padding10">
                <span class="element">
                    สำนักทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ กรมการท่องเที่ยว</a>
                </span>
            </div>
        </div>
    </div>

    </body>
</html>
