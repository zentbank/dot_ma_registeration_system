<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="initial-scale=0.9, user-scalable=no">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/page/css/stylesheet.css" type="text/css" charset="utf-8" />
    <title>Guide</title>
</head>
<body>
    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-width:0px;border-style:none;">
        <tr>
            <td style="text-align:center;background-image:url('${model.imgUrl}');background-position:center;background-repeat:no-repeat;background-size:300px 315px;">
                <img src='${pageContext.request.contextPath}/page/images/guide/circle.png' alt="circle" />
            </td>
        </tr>
        <tr>
            <td class="guide_name">
                ${model.traderName}
            </td>
        </tr>
        <tr>
            <td align="center">
                <table style="width:90%;">
                    <tr>
                        <td style="background-color:#5f8d85;height:2px;"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; padding:15px 0px 0px 50px;">
                <p style="margin: 0px;">
                    <span class="guide_label1">ใบอนุญาตเลขที่ </span><span class="guide_value1">${model.licenseNo}</span></p>
                <p style="margin: 0px 0px 15px 0px; line-height: 15px;">
                    <span class="guide_label1">สถานะการจดทะเบียน </span><span class="guide_value1">${model.licenseStatus}</span></p>
            </td>
        </tr>
        <tr>
            <td style="background-color: #75c589">
                <p style="margin: 0px; padding-left: 20px; padding-top: 10px;" class="guide_label2">
                    ประเภทการจดทะเบียน</p>
                <p style="margin: 0px; padding-left: 20px; padding-right: 10px;" class="guide_label3">
                    ${model.traderCategory}</p>
            </td>
        </tr>
        <tr>
            <td style="background-color: #75c589; text-align: right;">
                <p style="margin: 5px 0px 0px 0px; padding-right: 20px; color: #ffffff; font-size: 20px;">
                    ที่อยู่</p>
                <p style="margin: 5px 0px 0px 0px; padding-right: 20px; color: #075B50; font-size: 16px;">
                    ${model.traderAddress}</p>
            </td>
        </tr>
        <tr>
            <td style="background-color: #2aad5b;">
                <p style="margin: 5px 0px 5px 0px; padding-left: 45px; color: #ffffff; font-size: 20px;
                    text-decoration: underline;">
                    ประวัติการจดทะเบียน</p>
            </td>
        </tr>
        <tr>
            <td>
                <ul style="margin-left: 15px;">
                	<c:forEach var="reg" items="${model.registration}">
	                    <li style="color: #19b99a; font-size: 18px;"><span style="color: #115b50; font-size: 18px;">
	                        ${reg.registrationTypeName}</span><p style="margin: 0px;">
	                        </p>
	                        <span style="margin: 0px; color: #2aad5b; font-size: 18px;">${reg.registrationDate}</span>
	                    </li>
                    </c:forEach>
                    
                </ul>
            </td>
        </tr>
        <tr>
            <td style="background-color: #b2dbb8;">
                <ul style="margin-left: 15px;">
                    <li style="color: #19b99a; font-size: 18px;"><span style="color: #115b50; font-size: 18px;">
                        เรื่องร้องเรียน</span><p style="margin: 0px;">
                        </p>
                        <c:forEach var="complaintName" items="${model.complaint}">
                        <span style="margin: 0px; color: #2aad5b; font-size: 18px;">${complaintName}</span><p
                            style="margin: 0px;">
                        </p>
                        </c:forEach>
                    </li>
                </ul>
            </td>
        </tr>
    </table>
</body>
</html>
