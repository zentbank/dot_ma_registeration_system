<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="target-densitydpi=device-dpi, width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta name="description" content="Metro UI CSS">
    <meta name="author" content="Sergey Pimenov">
    <meta name="keywords" content="windows 8, modern style, Metro UI, style, modern, css, framework">

    <link href="${pageContext.request.contextPath}/page/css/modern.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/modern-responsive.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/site.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/page/js/google-code-prettify/prettify.css" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment_langs.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/dropdown.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/accordion.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/buttonset.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/carousel.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/input-control.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/pagecontrol.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/rating.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-drag.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/calendar.js"></script>

    <title><spring:message code="registry.tourism.department" text="สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์" /></title>

<script type="text/javascript">
    $(document).ready(function(){
        $(".listview li").click(function(){
   

            var form = document.form1;
            form.action = "${pageContext.request.contextPath}/info/license/guide/profile";
            form.traderId.value = this.id;
            form.submit();

            // window.location = "TourComProfile.html";
            // window.location = "../TourComProfile.html/"+this.id;
        });
    });
</script>
</head>
<body class="metrouicss" >

<jsp:include page="navigation.jsp" /> 
 
<div class="page secondary">
    <div class="page-header">
        <div class="page-header-content">
        	<h1><spring:message code="info.license.guide.label" text="มัคคุเทศก์" /></h1>
            <a href="javascript:history.back()" class="back-button big page-back"></a>
        </div>
       
    </div>

<br />

    <div class="page-region">
        <div class="page-region-content">
            <div class="span10">
                <form action="${pageContext.request.contextPath}/info/license/guide/read" method="post">
                <div class="input-control text">

                    <input type="text" name="searchParam" placeholder="<spring:message code='info.license.guide.search.placeHolder' text='กรอกข้อมูล เลขที่ใบอนุญาต ชื่อมัคคุเทศก์...'/>" />
                    <input type="hidden" name="searchType" value="G"/>
                    <button type="submit" class="btn-search"></button>
                </div>
                </form>
            </div>

            <ul class="listview">

                <c:forEach var="model" items="${list}">
                    <li id="${model.id}">
                        <div class="icon">
                            <img src="${model.img_url}" />
                        </div>

                        <div class="data">
                            <h4>${model.name_th}</h4>
                            <p>
                               ${model.name_en}
                            </p>
                        </div>
                    </li>
                </c:forEach>
             </ul>
         </div>
    </div>
</div>


    <div class="page">
        <div class="nav-bar">
            <div class="nav-bar-inner padding10">
                <span class="element">
                    <spring:message code="dot.or.th" text="สำนักทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ กรมการท่องเที่ยว"/>
                </span>
            </div>
        </div>
    </div>
                <form name="form1" method="post">
                     <input type="hidden" name="traderId" value=""/>
                </form>
    </body>
</html>
