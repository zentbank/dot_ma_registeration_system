<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="target-densitydpi=device-dpi, width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta name="description" content="Metro UI CSS">
    <meta name="author" content="Sergey Pimenov">
    <meta name="keywords" content="windows 8, modern style, Metro UI, style, modern, css, framework">

    <link href="${pageContext.request.contextPath}/page/css/modern.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/modern-responsive.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/site.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/page/js/google-code-prettify/prettify.css" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment_langs.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/dropdown.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/accordion.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/buttonset.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/carousel.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/input-control.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/pagecontrol.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/rating.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-drag.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/calendar.js"></script>

    <title><spring:message code="registry.tourism.department" text="สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์" /></title>

</head>
<body class="metrouicss" >

<jsp:include page="navigation.jsp" /> 
 
<div class="page secondary">
    <div class="page-header">
        <div class="page-header-content">
           
            <a href="javascript:history.back()" class="back-button big page-back"></a>
        </div>
       
    </div>

    <div class="page-region">
        <div class="page-region-content">
                <div class="row">
                    <div class="span8">

                        <div class="grid">
                            <div class="row">
                                <div class="span2">
                                    &nbsp;
                                </div>

                                <div class="span4">
                                    <div class="image-container bg-color-lighten" style="width: 100%">
                                        <div style="height: 250px; overflow: hidden"><img src="${model.imgUrl}" /></div>
                                        <div class="overlay">
                                            &nbsp;
                                        </div>
                                    </div>
                                </div>

                                <div class="span2">
                                    &nbsp;
                                </div>
                            </div>

                            <div class="row">
                               

                            <div class="span8 bg-color-yellow">
                                <h4 class="fg-color-white padding-top5" >&nbsp;${model.traderName}</h4>
                                <h4 class="fg-color-white">&nbsp;${model.traderNameEn}</h4>
                            </div>

                       
                            </div>
                        </div>
                    

                        <blockquote >
                           <p><spring:message code="info.license.tourLeader.licenseNo" text="ใบอนุญาตเลขที่"/> ${model.licenseNo}</p>
                           <p><spring:message code="info.license.tourLeader.licenseStatus" text="สถานะการจดทะเบียน"/> <span class="fg-color-greenLight">${model.licenseStatus}</span></p>
                        </blockquote>

                        <br />     

                        <blockquote class="place-right" >
                           <h3><spring:message code="info.license.tourLeader.traderCategory" text="ประเภทการจดทะเบียน"/></h3>
                           <h4>${model.traderCategory}</h4> 
                        </blockquote>

   

                        <br />    

                        <h3><spring:message code="info.license.tourLeader.address" text="ที่อยู่"/></h3>
                        <address>
                            ${model.traderAddress}
                        </address>

                        <blockquote class="bg-color-blueLight padding20 tertiary-text1">
                            <h3><spring:message code="info.license.tourLeader.history.registration" text="ประวัติการจดทะเบียน"/></h3>
                            <c:forEach var="reg" items="${model.registration}">
                                <p>${reg.registrationTypeName}</p>
                                <p>${reg.registrationDate}</p>
                            </c:forEach>

                            <br />  

                            <h3><spring:message code="info.license.tourLeader.complaint" text="เรื่องร้องเรียน"/></h3>
                            <c:forEach var="complaintName" items="${model.complaint}">
                                <p>${complaintName}</p>
                            </c:forEach>
                            
                        

                         
                        </blockquote> 

                        <br />  


                        <%--  <form action="${pageContext.request.contextPath}/info/license/complaint" method="post">
                            <input type="hidden" name="traderId" value="${model.traderId}" />
                            <button type="submit" class="big image-button bg-color-redLight fg-color-white span8"> 
                                <spring:message code="info.license.tourLeader.complaint.tourLeader" text="ร้องเรียนใบอนุญาตผู้นำเที่ยว"/> 
                                <i class="icon-cone bg-color-red"></i>
                            </button>
                             

                        </form> --%>
                </div>
         </div>
    </div>
</div>




    <div class="page">
        <div class="nav-bar">
            <div class="nav-bar-inner padding10">
                <span class="element">
                    <spring:message code="dot.or.th" text="สำนักทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ กรมการท่องเที่ยว"/>
                </span>
            </div>
        </div>
    </div>

    </body>
</html>
