<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="initial-scale=0.9, user-scalable=no">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/page/css/stylesheet.css" type="text/css" charset="utf-8" />
    <title>Member</title>
</head>
<body>

    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: center; background-image: url('${model}'); background-position: center;
                background-repeat: no-repeat;background-size:325px 315px;">
                <img style="width:354px;height:325px;" src='${pageContext.request.contextPath}/page/images/member/circle.png' alt="circle" />
            </td>
        </tr>
    </table>
	<c:forEach var="lic" items="${list}">


	    <table width="100%" border="0" cellpadding="0" cellspacing="0">
	        <tr>
	            <td>
	                <table width="100%" border="0" cellpadding="0" cellspacing="0">
	                    <tr>
	                        <td style="width: 20px;">
	                        </td>
	                        <td style="vertical-align: top; padding: 7px 0px 12px 0px;">
	                            <p style="margin: 0px; padding-left: 5px; color: #e84d3b; font-size: 22px;">
	                                ${lic.traderName}</p>
							<p style="margin: -8px 0px 0px 0px; padding-left: 5px; color: #a7a9ac; font-size: 18px;">
	                                ${lic.traderNameEn}</p>
	                        </td>
	                    </tr>
	                </table>
	            </td>
	        </tr>
	        <tr>
	            <td style="text-align: center; padding: 7px 0px 14px 0px">
	                <span style="color: #ed1c24; font-size: 22px;">ใบอนุญาต${lic.traderTypeName}เลขที่</span><span style="color: #c13a2d;
	                    font-size: 24px;">&nbsp;${lic.licenseNo}</span>
	                <p style="margin: 0px;">
	                </p>
	                <span style="color: #ed1c24; font-size: 22px;">สถานะการจดทะเบียน</span><span style="color: #c13a2d;
	                    font-size: 24px;">&nbsp;${lic.licenseStatus}</span>
	            </td>
	        </tr>
	        <tr>
	            <td style="padding-left: 45px;">
	                <span style="font-size: 18px; color: #939598">ประเภทการจดทะเบียน</span>
	            </td>
	        </tr>
	        <tr>
	            <td>
	                <table width="100%" cellpadding="0" cellspacing="0">
	                    <tr>
	                        <td style="width: 43px; background-color: #e84d3b;">
	                        </td>
	                        <td style="padding: 0px 5px 0px 5px; color: #ed1c24; font-size: 22px;">
	                            ${lic.traderCategory}
	                            <p style="margin: 0px;">
	                            </p>
	                            ที่อยู่
	                            <p style="margin: 0px;">
	                            </p>
	                            
	                        </td>
	                        <td style="width: 60px; background-color: #e84d3b;">
	                        </td>
	                    </tr>
	                </table>
	            </td>
	        </tr>
	        <tr>
	            <td style="padding-left: 45px;">
	                <span style="font-size: 18px; color: #939598">${lic.traderAddress}</span>
	            </td>
	        </tr>
	        <tr>
	            <td style="height: 10px;">
	            </td>
	        </tr>
	        <tr>
	            <td style="background-color: #fdf5f0;padding:0px 0px 5px 0px;">
	                <table width="100%" cellpadding="0" cellspacing="0">
	                    <tr>
	                        <td style="width: 45px;">
	                        </td>
	                        <td style="color: #ed1c24; font-size: 22px; padding: 0px 0px 0px 0px;">
	                            ประวัติการจดทะเบียน
	                        </td>
	                    </tr>
	                    <tr>
	                        <td style="width: 45px;">
	                        </td>
	                        <td>
	                        	<c:forEach var="reg" items="${lic.registration}">
		                            <span style="color: #bdbec0; font-size: 18px;">${reg.registrationTypeName}</span><p style="margin: 0px;">
		                            </p>

		                            <span style="margin: 0px; color: #e84d3b; font-size: 18px;">${reg.registrationDate}</span>
		                            <p style="margin: 0px;">
		                            </p>
		                        </c:forEach>
	                            
	                            <span style="color: #bdbec0; font-size: 18px;">เรื่องร้องเรียน</span><p style="margin: 0px;">
	                            </p>
	                            <c:forEach var="complaintName" items="${lic.complaint}">
		                            <span style="margin: 0px; color: #e84d3b; font-size: 18px; line-height: 18px;">${complaintName}</span><p
		                                style="margin: 0px;">
		                            </p>
		                        </c:forEach>
	                            

	                        </td>
	                    </tr>
	                </table>
	            </td>
	        </tr>
	        <tr>
	            <td style="background-color:#f6b29b;height:30px;"></td>
	        </tr>
	    </table>
    </c:forEach>
</body>
</html>
