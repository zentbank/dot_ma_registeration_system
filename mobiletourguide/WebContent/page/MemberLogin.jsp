<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="target-densitydpi=device-dpi, width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta name="description" content="Metro UI CSS">
    <meta name="author" content="Sergey Pimenov">
    <meta name="keywords" content="windows 8, modern style, Metro UI, style, modern, css, framework">

    <link href="${pageContext.request.contextPath}/page/css/modern.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/modern-responsive.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/site.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/page/js/google-code-prettify/prettify.css" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment_langs.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/dropdown.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/accordion.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/buttonset.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/carousel.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/input-control.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/pagecontrol.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/rating.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-drag.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/calendar.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/sha256.js"></script>

    <title>สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์</title>
<script type="text/javascript">

function validateForm(formData, jqForm, options) { 
    // formData is an array of objects representing the name and value of each field 
    // that will be sent to the server;  it takes the following form: 
    // 
    // [ 
    //     { name:  username, value: valueOfUsernameInput }, 
    //     { name:  password, value: valueOfPasswordInput } 
    // ] 
    // 
    // To validate, we can examine the contents of this array to see if the 
    // username and password fields have values.  If either value evaluates 
    // to false then we return false from this method. 
 


    for (var i=0; i < formData.length; i++) { 
        if (!formData[i].value) { 
            $("#simple-msg").html('<pre><code class="fg-color-red">กรุณากรอกชื่อผู้ใช้</code></pre>');
            return false; 
        } 
    } 
    alert('Both fields contain values.'); 
}

$(document).ready(function()
{

    if(localStorage.token != null)
    {
        //window.location = "../info/news/member/web/" + localStorage.token;
        window.location = "../page/MemberWebNewsMenu.jsp";
    }


    
    $("#simple-post").click(function()
    {
        $("#loginForm").submit(function(e)
        {
            var indxUserName = 0;
            var indxPassword = 1;
            var postData = $(this).serializeArray();

            postData[indxPassword].value = sha256_digest(postData[indxPassword].value);


            var formURL = $(this).attr("action");
       
            $.ajax(
            {
                url : formURL,
                type: "POST",
                data : postData,
                success:function(data, textStatus, jqXHR) 
                {
                    var returnValue = jQuery.parseJSON( data );
                    
                    
                    if(!(returnValue.status == 'success'))
                    {
                        $("#simple-msg").html('<pre><code class="">ชื่อผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง</code></pre>');
                    }
                    else
                    {

                        localStorage.token = returnValue.data[0].token;
                        // window.location = "../info/news/member/web/" + returnValue.data[0].token;
                         window.location = "../page/MemberWebNewsMenu.jsp";
                        
                    }
                    

                },
                error: function(jqXHR, textStatus, errorThrown) 
                {
                    $("#simple-msg").html('<pre><code class="fg-color-red">เกิดข้อผิดพลาด ไม่สามารถเชื่อมต่อได้</code></pre>');
                }
            });
            e.preventDefault(); //STOP default action
        });
            

        $("#loginForm").submit(); //SUBMIT FORM

        
    });

});
</script>
</head>
<body class="metrouicss" >

<jsp:include page="navigation.jsp" /> 

<div class="page secondary">
    <div class="page-header">
        <div class="page-header-content">
           
            <a href="javascript:history.back()" class="back-button big page-back"></a>
        </div>
       
    </div>

    <div class="page-region">
        <div class="page-region-content">
           

                         <form name="loginForm" id="loginForm" action="${pageContext.request.contextPath}/info/member/login" method="post">
                            <div class="grid">

                                <div class="row">
                                     <div class="span3">
                                        <label class="input-control text">
                                            ชื่อผู้ใช้งาน
                                        </label>
                                    </div>
                                    <div class="span5">
                                        <div class="input-control text">
                                            <input type="text" name="UserName" placeholder="เลขบัตรประชาชน/นิติบุคคล"  required="required"/>
                                            <button class="btn-clear"></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                     <div class="span3">
                                        <label class="input-control text">
                                            รหัสผ่าน
                                        </label>
                                    </div>
                                    <div class="span5">
                                        <div class="input-control text">
                                            <input type="password" name="Password" placeholder="สี่ตัวท้ายของ เลขบัตรประชาชน/นิติบุคคล"  required="required"/>
                                            <button class="btn-clear"></button>
                                        </div>
                                    </div>
                                </div>   

                               <!--  <div class="row">
                                     <div class="span3">
                                        <label class="input-control text">
                                            &nbsp;
                                        </label>
                                    </div>
                                    <div class="span5">
                                        <div class="input-control text">
                                           ลืมรหัสผ่าน
                                        </div>
                                    </div>
                                </div>   -->                        
                         
                         
                                <button id="simple-post" type="button" class="big image-button bg-color-blue fg-color-white span8"> 
                                    ลงชื่อเข้าใช้ 
                                </button>

                                <div id="simple-msg"></div>
                             
                            </div>
                        </form>
         
    </div>
</div>




    <div class="page">
        <div class="nav-bar">
            <div class="nav-bar-inner padding10">
                <span class="element">
                    สำนักทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ กรมการท่องเที่ยว</a>
                </span>
            </div>
        </div>
    </div>

    </body>
</html>
