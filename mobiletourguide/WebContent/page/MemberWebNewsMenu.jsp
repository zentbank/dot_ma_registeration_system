<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="target-densitydpi=device-dpi, width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta name="description" content="Metro UI CSS">
    <meta name="author" content="Sergey Pimenov">
    <meta name="keywords" content="windows 8, modern style, Metro UI, style, modern, css, framework">

    <link href="${pageContext.request.contextPath}/page/css/modern.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/modern-responsive.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/site.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/page/js/google-code-prettify/prettify.css" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment_langs.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/dropdown.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/accordion.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/buttonset.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/carousel.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/input-control.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/pagecontrol.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/rating.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-drag.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/calendar.js"></script>

    <title>สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์</title>
<script type="text/javascript">


$(document).ready(function()
{

    
    $("#memberprofile").click(function()
    {
            var form = document.form1;
            form.action = "${pageContext.request.contextPath}/info/member/profile/web/"+ localStorage.token;
            // form.traderId.value = this.id;
            form.submit();
        
        // window.location = "${pageContext.request.contextPath}/info/member/profile/web/" + localStorage.token;
    });

    $("#membernews").click(function()
    {
        window.location = "${pageContext.request.contextPath}/info/news/member/web/" + localStorage.token;
        
    });

    $("#logoutmember").click(function()
    {
        window.location = "${pageContext.request.contextPath}/page/MemberLogin.jsp";
        localStorage.removeItem('token');
        
    });

    $("#renewlicense").click(function()
    {
        window.location = "${pageContext.request.contextPath}/page/TourRenewProfile.jsp";
    });

    $("#reqcertificate").click(function()
    {
        var form = document.form1;
        form.action = "${pageContext.request.contextPath}/service/license/reqcertificate/read/"+ localStorage.token;
        // form.traderId.value = this.id;
        form.submit();
    });

     $("#joborder").click(function()
    {
        var form = document.form1;
        form.action = "${pageContext.request.contextPath}/service/joborder/read";
        form.token.value = localStorage.token;
        form.submit();
    });

    

});
</script>
</head>
<body class="metrouicss" >

<jsp:include page="navigation.jsp" /> 

<div class="page secondary">
    <div class="page-header">
        <div class="page-header-content">
            <h1>สมาชิก</h1>
            <a href="javascript:history.back()" class="back-button big page-back"></a>
        </div>
       
    </div>

<br />

    <div class="page-region">
        <div class="page-region-content">
                
            <div class="grid">
                <div class="row">
                    
                        <div id="memberprofile" class="span6 bg-color-orangeDark" style="cursor:pointer">
                            <h2 class="fg-color-white padding-top5">&nbsp;ประวัติ</h2>
                            <p class="fg-color-white">&nbsp;&nbsp;ประวัติสมาชิก</p>
                        </div>
                  

                   
                       <!--  <div id="membernews" class="span6 bg-color-orange" style="cursor:pointer">
                            <h2 class="fg-color-white padding-top5">&nbsp;ข่าวสารสมาชิก</h2>
                            <p class="fg-color-white">&nbsp;&nbsp;ประกาศจาก สนง. แจ้งเตือนให้มาชำระค่าธรรมเนียมเพิ่มเติม</p>
                        </div> -->

                        <!-- <div id="renewlicense" class="span6 bg-color-orange" style="cursor:pointer">
                            <h2 class="fg-color-white padding-top5">&nbsp;ค่าธรรมเนียม</h2>
                            <p class="fg-color-white">&nbsp;&nbsp;ชำระค่าธรรมเนียมรายสองปี</p>
                        </div> -->

                        <div id="reqcertificate" class="span6 bg-color-orange" style="cursor:pointer">
                            <h2 class="fg-color-white padding-top5">&nbsp;หนังสือรับรองใบอนุญาต</h2>
                            <p class="fg-color-white">&nbsp;&nbsp;ขอหนังสือรับรองใบอนุญาต</p>
                        </div>

                        <div id="joborder" class="span6 bg-color-teal" style="cursor:pointer">
                            <h2 class="fg-color-white padding-top5">&nbsp;บันทึกใบสั่งงานมัคคุเทศก์</h2>
                            <p class="fg-color-white">&nbsp;&nbsp;จัดทำใบสั่งงานมัคคุเทศก์</p>
                        </div>

                        <div id="logoutmember" class="span6 bg-color-red" style="cursor:pointer">
                            <h2 class="fg-color-white padding-top5">&nbsp;ออกจากระบบ</h2>
                            <p class="fg-color-white">&nbsp;&nbsp;</p>
                        </div>
               

                   
                </div>
            </div>

        </div>
    </div>
    </div>
</div>


    <div class="page">
        <div class="nav-bar">
            <div class="nav-bar-inner padding10">
                <span class="element">
                    สำนักทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ กรมการท่องเที่ยว</a>
                </span>
            </div>
        </div>
    </div>
                <form name="form1" method="post">
                     <input type="hidden" name="token" value=""/>
                </form>
    </body>
</html>
