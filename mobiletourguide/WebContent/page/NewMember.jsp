<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="initial-scale=0.9, user-scalable=no">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/page/css/stylesheet.css" type="text/css" charset="utf-8" />
    <title>&nbsp;</title>
</head>
<body>

    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: center;">
			    <c:if test="${model.news_img != ''}">
			       <img src="${model.news_img}"/>
			    </c:if>
            </td>
        </tr>
    </table>
	


	    <table width="100%" border="0" cellpadding="0" cellspacing="0">
	        <tr>
	            <td style="background-color: #fdf5f0;padding:0px 0px 5px 0px;">
	                <table width="100%" cellpadding="0" cellspacing="0">
	                    <tr>
	                        <td style="width: 45px;">
	                        </td>
	                        <td style="color: #ed1c24; font-size: 22px; padding: 0px 0px 0px 0px;">
	                            รายละเอียด
	                        </td>
	                    </tr>
	                    <tr>
	                        <td style="width: 45px;">
	                        </td>
	                        <td>
		                            <span style="color: #e84d3b; font-size: 18px;">${model.header}</span><p style="margin: 0px;">
		                            </p>

		                            <span style="margin: 0px; color: #bdbec0; font-size: 18px;">${model.details}</span>
		                            <p style="margin: 0px;">
		                            </p>
	                        </td>
	                    </tr>
	                </table>
	            </td>
	        </tr>

	    </table>

</body>
</html>
