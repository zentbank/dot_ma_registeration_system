<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<body>
	<h3>สารจากสำนักธุรกิจนำเที่ยวและมัคคุเทศก์</h3>
	<img src="${model.news_img}"/>
	<div>${model.header}</div>
	<p>${model.details}</p>
</body>
</html>