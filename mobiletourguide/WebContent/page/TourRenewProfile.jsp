<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="target-densitydpi=device-dpi, width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta name="description" content="Metro UI CSS">
    <meta name="author" content="Sergey Pimenov">
    <meta name="keywords" content="windows 8, modern style, Metro UI, style, modern, css, framework">

    <link href="${pageContext.request.contextPath}/page/css/modern.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/modern-responsive.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/site.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/page/js/google-code-prettify/prettify.css" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment_langs.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/dropdown.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/accordion.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/buttonset.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/carousel.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/input-control.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/pagecontrol.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/rating.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-drag.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/calendar.js"></script>

    <title>สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์</title>

    <script>
$(document).ready(function() {
    //add more file components if Add is clicked
    $('#addFile').click(function() {
        var fileIndex = $('#fileTable tr').children().length - 1;
        $('#fileTable').append(
                '<tr><td>'+
                '   <input type="file" name="files['+ fileIndex +']" />'+
                '</td></tr>');
    });
     
});
</script>

</head>
<body class="metrouicss" >

<jsp:include page="navigation.jsp" /> 
 
<div class="page secondary">
    <div class="page-header">
        <div class="page-header-content">
           
            <a href="javascript:history.back()" class="back-button big page-back"></a>
        </div>
       
    </div>

    <div class="page-region">
        <div class="page-region-content">
            <div class="span10">
                <form action="${pageContext.request.contextPath}/service/renew/license/read" method="post">
                <div class="input-control text">

                    <input type="text" name="searchParam" placeholder="กรอกข้อมูล เลขที่ใบอนุญาต" />
                    <input type="hidden" name="searchType" value="T"/>
                    <button type="submit" class="btn-search"></button>
                </div>
                </form>
            </div>
            <c:forEach var="model" items="${list}">
            <form:form action="${pageContext.request.contextPath}/service/renew/license/request" method="post" modelAttribute="uploadForm" enctype="multipart/form-data">
                <div class="row">
                    <div class="span8">
                        <h1 class="fg-color-blue">${model.traderName}</h1>
                        <h3>${model.traderNameEn}</h3>
                        <blockquote >
                           <p>ใบอนุญาตเลขที่ ${model.licenseNo}</p>
                           <p>สถานะการจดทะเบียน <span class="fg-color-greenLight">${model.licenseStatus}</span></p>
                        </blockquote>

                        <br />     

                        <blockquote class="place-right" >
                           <h3>ประเภทการจดทะเบียน</h3>
                           <h4>${model.traderCategory}</h4> 
                        </blockquote>

   

                        <br />    

                        <h3>ที่อยู่</h3>
                        <address>
                            ${model.traderAddress}
                        </address>

                        <blockquote class="bg-color-blueLight padding20 tertiary-text1">
                            <h3>ประวัติการจดทะเบียน</h3>
                            <c:forEach var="reg" items="${model.registration}">
                                <p>${reg.registrationTypeName}</p>
                                <p>${reg.registrationDate}</p>
                            </c:forEach>

                            <br />  

                            <h3>เรื่องร้องเรียน</h3>
                            <c:forEach var="complaintName" items="${model.complaint}">
                                <p>${complaintName}</p>
                            </c:forEach>
                            
                            <br />  

                            <h3>กรรมการบริษัท</h3>
                            <c:forEach var="committeeName" items="${model.committee}">
                                <div>${committeeName}</div>
                            </c:forEach>
                        </blockquote> 

                        

                         
                            
                             

                        
                    </div>
                </div>

                 <div class="row">
                     <div class="span3">
                        <label class="input-control text">
                            เอกสารแนบ ใบอนุญาตธุรกิจนำเที่ยว
                            
                        </label>
                    </div>
                    <div class="span5">
                        <div class="input-control file">
                            <!-- <input type="file" name="licensePic" placeholder="เลือกรูปภาพ"/> -->
                            <input name="files[0]" type="file" /> 
                          <!--   <input id="addFile" type="button" value="Add File" />
                            <table id="fileTable">
                                <tr>
                                    <td><input name="files[0]" type="file" /></td>
                                </tr>
                                <tr>
                                    <td><input name="files[1]" type="file" /></td>
                                </tr>
                            </table> -->
                           
                        </div>
                    </div>
                </div>

                <div class="row">
                     <div class="span3">
                        <label class="input-control text">
                            อีเมล
                            
                        </label>
                    </div>
                    <div class="span5">
                        <div class="input-control file">
                            <input name="email" type="text" /> 
                        </div>
                    </div>
                </div>

                <input type="hidden" name="traderId" value="${model.traderId}" />
                <button type="submit" class="big image-button bg-color-blue fg-color-white span4"> 
                    ยื่นคำร้อง 
                </button>

            </form:form>
            </c:forEach>
    </div>
</div>




    <div class="page">
        <div class="nav-bar">
            <div class="nav-bar-inner padding10">
                <span class="element">
                    สำนักทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ กรมการท่องเที่ยว</a>
                </span>
            </div>
        </div>
    </div>

    </body>
</html>
