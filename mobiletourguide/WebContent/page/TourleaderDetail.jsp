<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="initial-scale=0.9, user-scalable=no">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/page/css/stylesheet.css" type="text/css" charset="utf-8" />
    <title>Tour Leader</title>
</head>
<body>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: right; background-image: url('${model.imgUrl}'); background-position: right;
                background-repeat: no-repeat;background-size:325px 315px;">
                <img src='${pageContext.request.contextPath}/page/images/leader/circle.png' alt="circle" />
            </td>
        </tr>
        <tr>
            <td style="color: #E67E25; font-size: 24px; text-align: center;">
                ${model.traderName}
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="background-color: #f7de7b; vertical-align: top; padding: 13px 0px 0px 0px;">
                            <p style="margin: 0px; padding-left: 40px;">
                                <span class="guide_label1">ใบอนุญาตเลขที่ </span><span style="color: #E67E25; font-size: 22px;
                                    text-align: left;">${model.licenseNo}</span></p>
                            <p style="margin: 3px 0px 15px 0px; padding-left: 40px; line-height: 15px;">
                                <span class="guide_label1">สถานะการจดทะเบียน </span><span style="color: #E67E25;
                                    font-size: 22px; text-align: left;">${model.licenseStatus}</span></p>
                            <p style="margin: 3px 0px 0px 0px; padding: 30px 0px 0px 40px; color: #E67E25; line-height: 15px;
                                font-size: 24px;">
                                ที่อยู่
                            </p>
                        </td>
                        <td style="background-image: url('images/leader/circle_2.png'); background-repeat: no-repeat;
                            width: 94px; height: 140px;">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="text-align: left;">
                <p style="margin: 5px 0px 0px 0px; padding-left: 30px; color: #D05527; font-size: 18px;">
                    ${model.traderAddress}</p>
            </td>
        </tr>
        <tr>
            <td>
                <p style="margin: 5px 0px 0px 0px; padding-left: 30px; color: #E67E25; font-size: 20px;
                    text-decoration: underline;">
                    ประวัติการจดทะเบียน</p>
            </td>
        </tr>
        <tr>
            <td>
            	<c:forEach var="reg" items="${model.registration}">
	                <p style="margin: 0px 0px 0px 0px; padding-left: 30px; color: #F39D1F; font-size: 18px;">
	                    ${reg.registrationTypeName}</p>
	                <p style="margin: 0px 0px 0px 0px; padding-left: 30px; color: #F39D1F; font-size: 18px;">
	                    ${reg.registrationDate}</p>
                </c:forEach>
            </td>
        </tr>
        <tr>
            <td>
                <p style="margin: 15px 0px 0px 0px; padding-left: 30px; color: #E66A1F; font-size: 18px;">
                    เรื่องร้องเรียน</p>
                <c:forEach var="complaintName" items="${model.complaint}">
	                <p style="margin: 0px 0px 0px 0px; padding-left: 30px; color: #E66A1F; font-size: 18px;">
	                    ${complaintName}</p>
                </c:forEach>
            </td>
        </tr>
        <tr>
            <td style="text-align:right;">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width:10%">&nbsp;</td>
                        <td style="width:34px;height:35px;background-image:url('images/leader/footer.png');background-repeat:no-repeat;">
                        </td>
                        <td style="background-color:#fbcb8b;">&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
