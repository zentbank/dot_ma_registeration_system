<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="initial-scale=0.9, user-scalable=no">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/page/css/stylesheet.css" type="text/css" charset="utf-8" />
    <title>Tour</title>
</head>
<body>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 25px;">
                        </td>
                        <td style="width: 3px; background-color: #5FACDF;">
                        </td>
                        <td style="vertical-align: top; padding: 7px 0px 12px 0px;">
                            <p style="margin: 0px; padding-left: 5px; color: #2981BA; font-size: 22px;">
                                ${model.traderName}</p>
                            <p style="margin: 0px; padding-left: 5px; color: #2981BA; font-size: 22px;">
                                ${model.traderNameEn}</p>
                      <!--      <p style="margin: 0px; padding-left: 5px; color: #94A4A5; font-size: 16px;">
                                Bureau of Tourism Business and Guide Registration,</p>
                            <p style="margin: 0px; padding-left: 5px; color: #94A4A5; font-size: 16px;">
                                Department of Tourism</p> -->
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <ul style="margin-left: 35px;">
                    <li style="color: #5FACDF; font-size: 18px;"><span style="color: #94A4A5; font-size: 18px;">
                        ใบอนุญาตเลขที่</span><span style="color: #5FACDF; font-size: 18px;"> ${model.licenseNo}</span>
                    </li>
                    <li style="color: #5FACDF; font-size: 18px;"><span style="color: #94A4A5; font-size: 18px;">
                        สถานะการจดทะเบียน</span><span style="color: #5FACDF; font-size: 18px;"> ${model.licenseStatus}</span>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="vertical-align: top; padding: 7px 0px 12px 0px;">
                            <p style="margin: 0px; padding: 5px 7px 0px 0px; color: #2981BA; font-size: 20px;
                                text-align: right; line-height: 15px;">
                                ประเภทการจดทะเบียน</p>
                            <p style="margin: 0px; padding-right: 7px; color: #2C3F51; font-size: 22px; text-align: right;">
                                ${model.traderCategory}</p>
                            <p style="margin: 0px; padding-left: 45px; color: #000000; font-size: 22px;">
                                ที่อยู่</p>
                            <p style="margin: 0px; padding-left: 45px; color: #2981BA; font-size: 16px;">
                                ${model.traderAddress}</p>
                        <!--    <p style="margin: 0px; padding-left: 45px; color: #2981BA; font-size: 16px;">
                                อ.ถลาง จ.ภูเก็ต 10240 -->
                            </p>
                        </td>
                        <td style="width: 3px; background-color: #5FACDF;">
                        </td>
                        <td style="width: 25px;">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 30px;">
            </td>
        </tr>
        <tr>
            <td style="background-color: #bbd7f1;">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 20px;">
                        </td>
                        <td style="width: 20px;">
                        </td>
                        <td style="width: 5px;">
                        </td>
                        <td style="color: #2581bc; font-size: 22px; padding: 10px 0px 10px 0px;">
                            ประวัติการจดทะเบียน
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 20px;">
                        </td>
                        <td style="width: 20px; border-right: 2px solid #78b6e4;">
                        </td>
                        <td style="width: 5px;">
                        </td>
                        <td>
                            <ul style="margin: 0px 0px 10px -15px;">
                            	<c:forEach var="reg" items="${model.registration}">
	                                <li style="color: #5FACDF; font-size: 18px;"><span style="color: #000000; font-size: 18px;">
	                                    ${reg.registrationTypeName}</span><p style="margin: 0px;">
	                                    </p>
	                                    <span style="margin: 0px; color: #2981BA; font-size: 18px;">${reg.registrationDate}</span>
	                                </li>
                               </c:forEach>

                               
	                                <li style="color: #5FACDF; font-size: 18px;"><span style="color: #000000; font-size: 18px;">
	                                    เรื่องร้องเรียน</span><p style="margin: 0px;">
	                                    </p>
	                                    <c:forEach var="complaintName" items="${model.complaint}">
	                                    	<span style="margin: 0px; color: #2981BA; font-size: 18px;">${complaintName}</span>
	                                    </c:forEach>
	                                </li>
	                                <li style="color: #5FACDF; font-size: 18px;"><span style="color: #000000; font-size: 18px;">
	                                    กรรมการบริษัท</span><p style="margin: 0px;">
	                                    </p>
	                                    <c:forEach var="committeeName" items="${model.committee}">
	                                    	<span style="margin: 0px; color: #2981BA; font-size: 18px;">${committeeName}</span>
	                                    </c:forEach>
	                                </li>	                           
                               
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 20px;height:40px;">
                                    </td>
                                    <td style="width: 20px; border-right: 2px solid #78b6e4; border-top: 2px solid #78b6e4;">
                                    </td>
                                    <td style="width: 200px; border-top: 2px solid #78b6e4;">
                                    </td>
                                    <td>
                                    &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:10px;"></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
