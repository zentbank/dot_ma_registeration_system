<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="target-densitydpi=device-dpi, width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta name="description" content="Metro UI CSS">
    <meta name="author" content="Sergey Pimenov">
    <meta name="keywords" content="windows 8, modern style, Metro UI, style, modern, css, framework">

    <link href="${pageContext.request.contextPath}/page/css/modern.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/modern-responsive.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/site.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/page/js/google-code-prettify/prettify.css" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment_langs.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/dropdown.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/accordion.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/buttonset.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/carousel.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/input-control.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/pagecontrol.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/rating.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-drag.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/calendar.js"></script>

    <title>สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์</title>

<script type="text/javascript">

    
</script>
</head>
<body class="metrouicss" >

<jsp:include page="../navigation.jsp" /> 
 

    <div class="page secondary">
        <div class="page-header">
            <div class="page-header-content">
                <h2>จำนวนห้องพัก</h2>
                <a href="javascript:history.back()" class="back-button big page-back"></a>
            </div>
        </div>

        <div></div>

        <div class="page-region">
            <div class="page-region-content">

                <form  action="${pageContext.request.contextPath}/service/joborder/toruisname/saveHotel" method="post">

                    <input type="hidden" name="token" value="${model.token}" />
                    <input type="hidden" name="jobId" value="${model.jobId}" />
                

                 <div class="grid">

                    <div class="row">
                         <div class="span10">
                            <fieldset>
                                <legend>จำนวนนักท่องเที่ยว ผู้ใหญ่&nbsp;${model.tourisAdult}&nbsp;คน</legend>

                                <div class="grid">
                                    <div class="row">
                                        <div class="span3">
                                            <label class="input-control text">
                                                ห้องเดี่ยว 
                                            </label>
                                        </div>
                                        <div class="span5">
                                            <div class="input-control text">
                                                 <input type="hidden" name="typeAHotelId" value="${model.typeAHotelId}" />
                                                <input type="text" name="typeARoomType1" placeholder="จำนวนห้องเดี่ยว" value="${model.typeARoomType1}"/>
                                                <button class="btn-clear"></button>
                                            </div>
                                        </div>
                                    </div>     
                                    <div class="row">
                                        <div class="span3">
                                            <label class="input-control text">
                                                ห้องคู่ 
                                            </label>
                                        </div>
                                        <div class="span5">
                                            <div class="input-control text">
                                                <input type="text" name="typeARoomType2" placeholder="จำนวนห้องคู่" value="${model.typeARoomType2}"/>
                                                <button class="btn-clear"></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="span3">
                                            <label class="input-control text">
                                                ห้องรวม 3 คน 
                                            </label>
                                        </div>
                                        <div class="span5">
                                            <div class="input-control text">
                                                <input type="text" name="typeARoomType3" placeholder="จำนวนห้องรวม 3 คน" value="${model.typeARoomType3}"/>
                                                <button class="btn-clear"></button>
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="row">
                                        <div class="span3">
                                            <label class="input-control text">
                                                หมายเหตุ
                                            </label>
                                        </div>
                                        <div class="span5">
                                            <div class="input-control text">
                                                <input type="text" name="typeARemark" value="${model.typeARemark}"/>
                                                <button class="btn-clear"></button>
                                            </div>
                                        </div>
                                    </div>  
                                </div>

                            </fieldset>
                        </div>
                        
                    </div>
<!-- จำนวนนักท่องเที่ยวเด็ก -->
                    <div class="row">
                         <div class="span10">
                            <fieldset>
                                <legend>จำนวนนักท่องเที่ยว เด็ก&nbsp;${model.tourisChild}&nbsp;คน</legend>

                                <div class="grid">
                                    <div class="row">
                                        <div class="span3">
                                            <label class="input-control text">
                                                ห้องเดี่ยว 
                                            </label>
                                        </div>
                                        <div class="span5">
                                            <div class="input-control text">
                                                <input type="hidden" name="typeCHotelId" value="${model.typeCHotelId}" />
                                                <input type="text" name="typeCRoomType1" placeholder="จำนวนห้องเดี่ยว" value="${model.typeCRoomType1}"/>
                                                <button class="btn-clear"></button>
                                            </div>
                                        </div>
                                    </div>     
                                    <div class="row">
                                        <div class="span3">
                                            <label class="input-control text">
                                                ห้องคู่ 
                                            </label>
                                        </div>
                                        <div class="span5">
                                            <div class="input-control text">
                                                <input type="text" name="typeCRoomType2" placeholder="จำนวนห้องคู่" value="${model.typeCRoomType2}"/>
                                                <button class="btn-clear"></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="span3">
                                            <label class="input-control text">
                                                ห้องรวม 3 คน 
                                            </label>
                                        </div>
                                        <div class="span5">
                                            <div class="input-control text">
                                                <input type="text" name="typeCRoomType3" placeholder="จำนวนห้องรวม 3 คน" value="${model.typeCRoomType3}"/>
                                                <button class="btn-clear"></button>
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="row">
                                        <div class="span3">
                                            <label class="input-control text">
                                                หมายเหตุ
                                            </label>
                                        </div>
                                        <div class="span5">
                                            <div class="input-control text">
                                                <input type="text" name="typeCRemark"  value="${model.typeCRemark}"/>
                                                <button class="btn-clear"></button>
                                            </div>
                                        </div>
                                    </div>  
                                </div>

                            </fieldset>
                        </div>
                        
                    </div>
<!-- จำนวนผู้ติดตาม -->
                    <div class="row">
                         <div class="span10">
                            <fieldset>
                                <legend>จำนวนผู้ติดตาม&nbsp;${model.tourisStraff}&nbsp;คน</legend>

                                <div class="grid">
                                    <div class="row">
                                        <div class="span3">
                                            <label class="input-control text">
                                                ห้องเดี่ยว 
                                            </label>
                                        </div>
                                        <div class="span5">
                                            <div class="input-control text">
                                                <input type="hidden" name="typeSHotelId" value="${model.typeSHotelId}" />
                                                <input type="text" name="typeSRoomType1" placeholder="จำนวนห้องเดี่ยว" value="${model.typeSRoomType1}"/>
                                                <button class="btn-clear"></button>
                                            </div>
                                        </div>
                                    </div>     
                                    <div class="row">
                                        <div class="span3">
                                            <label class="input-control text">
                                                ห้องคู่ 
                                            </label>
                                        </div>
                                        <div class="span5">
                                            <div class="input-control text">
                                                <input type="text" name="typeSRoomType2" placeholder="จำนวนห้องคู่" value="${model.typeSRoomType2}"/>
                                                <button class="btn-clear"></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="span3">
                                            <label class="input-control text">
                                                ห้องรวม 3 คน 
                                            </label>
                                        </div>
                                        <div class="span5">
                                            <div class="input-control text">
                                                <input type="text" name="typeSRoomType3" placeholder="ห้องรวม 3 คน" value="${model.typeSRoomType3}"/>
                                                <button class="btn-clear"></button>
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="row">
                                        <div class="span3">
                                            <label class="input-control text">
                                                หมายเหตุ
                                            </label>
                                        </div>
                                        <div class="span5">
                                            <div class="input-control text">
                                                <input type="text" name="typeSRemark" value="${model.typeSRemark}"/>
                                                <button class="btn-clear"></button>
                                            </div>
                                        </div>
                                    </div>  
                                </div>

                            </fieldset>
                        </div>
                        
                    </div>
                <div class="row">
                     <div class="span5">
                       <button><i class="icon-save"></i></i>บันทึก</button>
                    </div>
                  
                </div>
                <div id="simple-msg"></div>
             
            </div>
        </form>

                
               
                
                
                </div>
            </div>
        </div>
    </div>

<div class="page">
    <div class="nav-bar">
        <div class="nav-bar-inner padding10">
            <span class="element">
                สำนักทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ กรมการท่องเที่ยว</a>
            </span>
        </div>
    </div>
</div>

    </body>
</html>
