<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="target-densitydpi=device-dpi, width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta name="description" content="Metro UI CSS">
    <meta name="author" content="Sergey Pimenov">
    <meta name="keywords" content="windows 8, modern style, Metro UI, style, modern, css, framework">

    <link href="${pageContext.request.contextPath}/page/css/modern.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/modern-responsive.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/site.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/page/js/google-code-prettify/prettify.css" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment_langs.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/dropdown.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/accordion.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/buttonset.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/carousel.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/input-control.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/pagecontrol.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/rating.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-drag.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/calendar.js"></script>

    <title>สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์</title>

<script type="text/javascript">

    
</script>
</head>
<body class="metrouicss" >

<jsp:include page="../navigation.jsp" /> 
 

    <div class="page secondary">
        <div class="page-header">
            <div class="page-header-content">
                <h2>การเดินทาง<small>(กรณีนำนักท่องเที่ยวจากต่างประเทศ)</small></h2>
                <a href="javascript:history.back()" class="back-button big page-back"></a>
            </div>
        </div>

        <div></div>

        <div class="page-region">
            <div class="page-region-content">

                <form name="loginForm" id="loginForm" action="${pageContext.request.contextPath}/service/joborder/itinerary/saveItinerary" method="post">

                    <input type="hidden" name="token" value="${model.token}" />
                    <input type="hidden" name="jobId" value="${model.jobId}" />
                

                 <div class="grid">

                     <div class="row">
                         <div class="span10">
                            <fieldset>
                                <legend>การเดินทางมา</legend>

                                <div class="grid">
                                    <div class="row">
                                        <div class="span3">
                                            <label class="input-control text">
                                                เดินทางโดย
                                            </label>
                                        </div>
                                        <div class="span5">
                                            <div class="input-control text">
                                                <input type="hidden" name="arriveId" value="${model.arriveId}" />
                                                <input type="text" name="arriveBy" placeholder="เครื่องบิน รถยนต์ เป็นต้น" value="${model.arriveBy}"/>
                                                <button class="btn-clear"></button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="span3">
                                            <label class="input-control text">
                                                วัน เดือน ปี เดินทางมาถึง
                                            </label>
                                        </div>
                                        <div class="span5">
                                            <div class="input-control text">
                                                <input type="text" name="arriveDate" value="${model.arriveDate}"/>
                                                <button class="btn-clear"></button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="span3">
                                            <label class="input-control text">
                                                เที่ยวบิน
                                            </label>
                                        </div>
                                        <div class="span5">
                                            <div class="input-control text">
                                                <input type="text" name="arriveFlightNo" placeholder="เฉพาะเดินทางโดยเครื่องบิน" value="${model.arriveFlightNo}"/>
                                                <button class="btn-clear"></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="span3">
                                            <label class="input-control text">
                                                จาก
                                            </label>
                                        </div>
                                        <div class="span5">
                                            <div class="input-control text">
                                                <input type="text" name="arriveFrom" placeholder="จีน ญี่ปุ่น เป็นต้น" value="${model.arriveFrom}"/>
                                                <button class="btn-clear"></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="span3">
                                            <label class="input-control text">
                                                ถึงกรุงเทพเวลา
                                            </label>
                                        </div>
                                        <div class="span5">
                                            <div class="input-control text">
                                                <input type="text" name="arriveTime" value="${model.arriveTime}"/>
                                                <button class="btn-clear"></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </fieldset>
                        </div>
                        
                    </div>

                    <div class="row">
                         <div class="span10">
                            <fieldset>
                                <legend>การเดินทางกลับ</legend>

                                <div class="grid">
                                    <div class="row">
                                        <div class="span3">
                                            <label class="input-control text">
                                                ประเภทการเดินทาง
                                            </label>
                                        </div>
                                        <div class="span5">
                                            <div class="input-control text">
                                                <input type="hidden" name="leaveId" value="${model.leaveId}" />
                                                <input type="text" name="leaveBy" placeholder="เครื่องบิน รถยนต์ เป็นต้น" value="${model.leaveBy}"/>
                                                <button class="btn-clear"></button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="span3">
                                            <label class="input-control text">
                                                วัน เดือน ปี เดินทางกลับ
                                            </label>
                                        </div>
                                        <div class="span5">
                                            <div class="input-control text">
                                                <input type="text" name="leaveDate" value="${model.leaveDate}"/>
                                                <button class="btn-clear"></button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="span3">
                                            <label class="input-control text">
                                                เที่ยวบิน
                                            </label>
                                        </div>
                                        <div class="span5">
                                            <div class="input-control text">
                                                <input type="text" name="leaveFlightNo" placeholder="เฉพาะเดินทางโดยเครื่องบิน" value="${model.leaveFlightNo}"/>
                                                <button class="btn-clear"></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="span3">
                                            <label class="input-control text">
                                                ออกจากกรุงเทพเวลา
                                            </label>
                                        </div>
                                        <div class="span5">
                                            <div class="input-control text">
                                                <input type="text" name="leaveTime" value="${model.leaveTime}"/>
                                                <button class="btn-clear"></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </fieldset>
                        </div>
                        
                    </div>
                    

                <div class="row">
                     <div class="span5">
                       <button><i class="icon-save"></i></i>บันทึกและดำเนินการต่อ</button>
                    </div>
                  
                </div>
         
                
                

                <div id="simple-msg"></div>
             
            </div>
        </form>

       <span class="label warning">${error}</span> 
               
                
                
                </div>
            </div>
        </div>
    </div>

<div class="page">
    <div class="nav-bar">
        <div class="nav-bar-inner padding10">
            <span class="element">
                สำนักทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ กรมการท่องเที่ยว</a>
            </span>
        </div>
    </div>
</div>

    </body>
</html>
