<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="target-densitydpi=device-dpi, width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta name="description" content="Metro UI CSS">
    <meta name="author" content="Sergey Pimenov">
    <meta name="keywords" content="windows 8, modern style, Metro UI, style, modern, css, framework">

    <link href="${pageContext.request.contextPath}/page/css/modern.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/modern-responsive.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/site.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/page/js/google-code-prettify/prettify.css" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment_langs.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/dropdown.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/accordion.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/buttonset.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/carousel.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/input-control.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/pagecontrol.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/rating.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-drag.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/calendar.js"></script>

    <title>สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์</title>

<script type="text/javascript">

    function jobOwnerDetail(jobId){
        
        var form = document.form1;
        form.action = "${pageContext.request.contextPath}/service/joborder/jobOwnerdetail";
       
        form.jobId.value = jobId;
        form.submit();
    }
        
    function loadJobOrder(jobId){
        alert("printJobOrder");
        window.open('${pageContext.request.contextPath}/service/joborder/excel/read?jobId='+jobId);
    }
    
</script>
</head>
<body class="metrouicss" >

<jsp:include page="../navigation.jsp" /> 
 

    <div class="page secondary">
        <div class="page-header">
            <div class="page-header-content">
                <h1>ใบสั่งงานมัคคุเทศก์</h1>
                <a href="../../page/MemberWebNewsMenu.jsp" class="back-button big page-back"></a>
            </div>
        </div>

        <div class="page-region">
            <div class="page-region-content">
                <div class="span10">

                <form action="${pageContext.request.contextPath}/service/joborder/jobOwnerdetail" method="post">
                    <input type="hidden" name="token" value="${token}" />
                    <input type="hidden" name="licenseNo" value="${licenseNo}" />
                    <input type="hidden" name="jobId" value="" />
                    <div>
                        <button><i class="icon-plus"></i>บันทึกใบสั่งงานมัคคุเทศก์</button>
                    </div> 
                     

                </form>

                
               
                <table class="striped">
                    <thead>
                        <tr>
                            
                            <th>เลขที่ใบสั่งงาน</th>
                            <th>วันที่</th>
                            <th>ชื่อมัคคุเทศก์</th>
                            <th>รายละเอียด</th>
                            <th>พิมพ์</th>
                        </tr>
                    </thead>

                    <tbody>
                         <c:forEach var="job" items="${joborders}">
                            <tr><td >${job.jobNo}</td><td>${job.jobDate}</td><td>${job.guideName}</td><td><a href="javascript:jobOwnerDetail(${job.jobId});" ><i class="icon-clipboard-2"></a></td><td><a href="javascript:loadJobOrder(${job.jobId});" ><i class="icon-printer"></a></td></tr>
                        </c:forEach>
                    </tbody>

                    <tfoot></tfoot>
                </table>
                
                </div>
            </div>
        </div>
    </div>

<div class="page">
    <div class="nav-bar">
        <div class="nav-bar-inner padding10">
            <span class="element">
                สำนักทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ กรมการท่องเที่ยว</a>
            </span>
        </div>
    </div>
</div>
<form name="form1" method="post">
     <input type="hidden" name="token" value="${token}"/>
     <input type="hidden" name="licenseNo" value="${licenseNo}"/>
     <input type="hidden" name="jobId" value=""/>

</form>
    </body>
</html>
