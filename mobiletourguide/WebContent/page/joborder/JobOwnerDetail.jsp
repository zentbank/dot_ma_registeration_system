<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="target-densitydpi=device-dpi, width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta name="description" content="Metro UI CSS">
    <meta name="author" content="Sergey Pimenov">
    <meta name="keywords" content="windows 8, modern style, Metro UI, style, modern, css, framework">

    <link href="${pageContext.request.contextPath}/page/css/modern.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/modern-responsive.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/site.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/page/js/google-code-prettify/prettify.css" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment_langs.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/dropdown.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/accordion.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/buttonset.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/carousel.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/input-control.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/pagecontrol.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/rating.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-drag.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/calendar.js"></script>

    <title>สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์</title>

<script type="text/javascript">

    
</script>
</head>
<body class="metrouicss" >

<jsp:include page="../navigation.jsp" /> 
 

    <div class="page secondary">
        <div class="page-header">
            <div class="page-header-content">
                <h2>ใบสั่งงานมัคคุเทศก์</h2>
                <a href="javascript:history.back()" class="back-button big page-back"></a>
            </div>
        </div>

        <div></div>

        <div class="page-region">
            <div class="page-region-content">

                <form name="loginForm"  action="${pageContext.request.contextPath}/service/joborder/save" method="post">

                    <input type="hidden" name="token" value="${model.token}" />
                    <input type="hidden" name="jobId" value="${model.jobId}" />
                    <input type="hidden" name="licenseNo" value="${model.licenseNo}" />
                

                 <div class="grid">

                     <div class="row">
                         <div class="span3">
                            <label class="input-control text">
                                ชื่อธุรกิจนำเที่ยว
                            </label>
                        </div>
                        <div class="span7">
                             <label class="input-control text">
                               <h4>${model.traderName}</h4>&nbsp;
                            </label>
                        </div>
                    </div>
                    <div class="row">
                         <div class="span3">
                            <label class="input-control text">
                                ใบอนุญาตเลขที่
                            </label>
                        </div>
                        <div class="span7">
                             <label class="input-control text">
                                <h4>${model.licenseNo}</h4>&nbsp;
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="span3">
                            <label class="input-control text">
                                ใบสั่งงานเลขที่
                            </label>
                        </div>
                        <div class="span7">
                            <div class="input-control text">
                                <input type="text" name="jobNo" value="${model.jobNo}"/>
                                <button class="btn-clear"></button>
                            </div>
                        </div>
                    </div> 
                    <div class="row">
                        <div class="span3">
                            <label class="input-control text">
                                วันที่
                            </label>
                        </div>
                        <div class="span7">
                            <div class="input-control text">
                                <input type="text" name="jobDate" value="${model.jobDate}"/>
                                <button class="btn-clear"></button>
                            </div>
                        </div>
                    </div> 
                    <div class="row">
                        <div class="span3">
                            <label class="input-control text">
                                ชื่อบริษัทนำเที่ยวจากต่างประเทศ
                            </label>
                        </div>
                        <div class="span7">
                            <div class="input-control text">
                                <input type="text" name="foreignName" value="${model.foreignName}"/>
                                <button class="btn-clear"></button>
                            </div>
                        </div>
                    </div>            
                    <div class="row">
                        <div class="span3">
                            <label class="input-control text">
                                ใบอนุญาตบริษัทนำเที่ยวจากต่างประเทศเลขที่
                            </label>
                        </div>
                        <div class="span7">
                            <div class="input-control text">
                                <input type="text" name="foreignLicenseNo" value="${model.foreignLicenseNo}"/>
                                <button class="btn-clear"></button>
                            </div>
                        </div>
                    </div>   
                    <div class="row">
                        <div class="span3">
                            <label class="input-control text">
                                ชื่อมัคคุเทศก์
                            </label>
                        </div>
                        <div class="span7">
                            <div class="input-control text">
                                <input type="text" name="guideName" value="${model.guideName}"/>
                                <button class="btn-clear"></button>
                            </div>
                        </div>
                    </div>  
                    <div class="row">
                        <div class="span3">
                            <label class="input-control text">
                                ใบอนุญาตมัคคุเทศก์เลขที่
                            </label>
                        </div>
                        <div class="span7">
                            <div class="input-control text">
                                <input type="text" name="guideLicenseNo" value="${model.guideLicenseNo}"/>
                                <button class="btn-clear"></button>
                            </div>
                        </div>
                    </div> 
                    <div class="row">
                        <div class="span3">
                            <label class="input-control text">
                                ค่าจ้างมัคคุเทศก์
                            </label>
                        </div>
                        <div class="span7">
                            <div class="input-control text">
                                <input type="text" name="guideSalary" value="${model.guideSalary}"/>
                                <button class="btn-clear"></button>
                            </div>
                        </div>
                    </div> 

                <div class="row">
                     <div class="span5">
                       <button><i class="icon-save"></i>บันทึกและดำเนินการต่อ</button>
                    </div>
                   
                </div>
         
                
                

                <div id="simple-msg"></div>
             
            </div>
        </form>

                
               
                
                
                </div>
            </div>
        </div>
    </div>

<div class="page">
    <div class="nav-bar">
        <div class="nav-bar-inner padding10">
            <span class="element">
                สำนักทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ กรมการท่องเที่ยว</a>
            </span>
        </div>
    </div>
</div>

    </body>
</html>
