<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="target-densitydpi=device-dpi, width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta name="description" content="Metro UI CSS">
    <meta name="author" content="Sergey Pimenov">
    <meta name="keywords" content="windows 8, modern style, Metro UI, style, modern, css, framework">

    <link href="${pageContext.request.contextPath}/page/css/modern.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/modern-responsive.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/site.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/page/js/google-code-prettify/prettify.css" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment_langs.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/dropdown.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/accordion.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/buttonset.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/carousel.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/input-control.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/pagecontrol.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/rating.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-drag.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/calendar.js"></script>

    <title>สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์</title>

<script type="text/javascript">

    function editTouris(tourisId){
        var form = document.form1;
        form.action = "${pageContext.request.contextPath}/service/joborder/toruisname/add";
       
        form.tourisId.value = tourisId;
        form.submit();
    }
</script>
</head>
<body class="metrouicss" >

<jsp:include page="../navigation.jsp" /> 
 

    <div class="page secondary">
        <div class="page-header">
            <div class="page-header-content">
                <h2>รายชื่อนักท่องเที่ยว</h2>
                <a href="javascript:history.back()" class="back-button big page-back"></a>
            </div>
        </div>

        <div></div>

        <div class="page-region">
            <div class="page-region-content">

               
                    <input type="hidden" name="token" value="${model.token}" />
                    <input type="hidden" name="jobId" value="${model.jobId}" />
                

                 <div class="grid">

                     <div class="row">
                         <div class="span10">
                            <div class="grid">                                    
                                    <div class="row">
                                         <div class="span8">
                                           <form action="${pageContext.request.contextPath}/service/joborder/toruisname/add" method="post">
                                                <input type="hidden" name="token" value="${model.token}" />
                                                <input type="hidden" name="jobId" value="${model.jobId}"/>
                                                <div>
                                                    <button><i class="icon-plus"></i>บันทึกรายชื่อนักท่องเที่ยว</button>
                                                </div> 
                                            </form>
                                        </div>
                                        <div class="span2 right">
                                            <!-- <form action="${pageContext.request.contextPath}/service/joborder/toruisname/tourisHotel" method="post"> -->
                                                 <div>
                                                    <a href="${pageContext.request.contextPath}/service/joborder/toruisname/tourisHotel/${model.token}/${model.jobId}"><button>ดำเนินการต่อ&nbsp;<i class="icon-arrow-right-3"></i></button></a>
                                                </div> 
                                            <!-- </form> -->
                                        </div>
                                    </div>
                                    <div class="row"></div>
                                    <div class="row">
                                        <div class="span3">
                                            <label class="input-control text">
                                                จำนวนนักท่องเที่ยว ผู้ใหญ่&nbsp;${model.tourisAdult}&nbsp;คน
                                            </label>
                                        </div>
                                        <div class="span3">
                                            <label class="input-control text">
                                                จำนวนนักท่องเที่ยว เด็ก&nbsp;${model.tourisChild}&nbsp;คน
                                            </label>
                                        </div>
                                        <div class="span3">
                                            <label class="input-control text">
                                                จำนวนผู้ติดตาม&nbsp;${model.tourisStraff}&nbsp;คน
                                            </label>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        
                    </div>
                </div>

                <table class="striped">
                    <thead>
                        <tr>
                            
                            <th>ชื่อนักท่องเที่ยว</th>
                            <th>ประเภท</th>
                            <th>รายละเอียด</th>
                        </tr>
                    </thead>

                    <tbody>
                        <c:forEach var="dto" items="${touris}">
                            <tr>
                                <td>${dto.tourisName}</td>
                                <td>${dto.tourisType}</td>
                                <td><a href="javascript:editTouris(${dto.tourisId})" ><i class="icon-clipboard-2"></a></td>
                            </tr>
                        </c:forEach>
                        
                    </tbody>

                    <tfoot></tfoot>
                </table>

                
               
                
                
                </div>
            </div>
        </div>
    </div>

<div class="page">
    <div class="nav-bar">
        <div class="nav-bar-inner padding10">
            <span class="element">
                สำนักทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ กรมการท่องเที่ยว</a>
            </span>
        </div>
    </div>
</div>
<form name="form1" method="post">
     <input type="hidden" name="token" value="${model.token}" />
     <input type="hidden" name="jobId" value="${model.jobId}"/>
     <input type="hidden" name="tourisId" value=""/>
</form>
    </body>
</html>
