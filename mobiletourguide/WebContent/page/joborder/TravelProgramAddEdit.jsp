<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="target-densitydpi=device-dpi, width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta name="description" content="Metro UI CSS">
    <meta name="author" content="Sergey Pimenov">
    <meta name="keywords" content="windows 8, modern style, Metro UI, style, modern, css, framework">

    <link href="${pageContext.request.contextPath}/page/css/modern.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/modern-responsive.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/site.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/page/js/google-code-prettify/prettify.css" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment_langs.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/dropdown.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/accordion.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/buttonset.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/carousel.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/input-control.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/pagecontrol.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/rating.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-drag.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/calendar.js"></script>

    <title>สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์</title>

<script type="text/javascript">

    
</script>
</head>
<body class="metrouicss" >

<jsp:include page="../navigation.jsp" /> 
 

    <div class="page secondary">
        <div class="page-header">
            <div class="page-header-content">
                <h2>บันทึกรายนำเที่ยว</h2>
                <a href="javascript:history.back()" class="back-button big page-back"></a>
            </div>
        </div>

        <div></div>

        <div class="page-region">
            <div class="page-region-content">

                <form  action="${pageContext.request.contextPath}/service/joborder/travelprogram/save" method="post">

                    <input type="hidden" name="token" value="${model.token}" />
                    <input type="hidden" name="jobId" value="${model.jobId}" />
                

                 <div class="grid">

                     <div class="row">
                         <div class="span10">
                            <div class="grid">
                                    <div class="row">
                                        <div class="span3">
                                            <label class="input-control text">
                                                ลำดับที่
                                            </label>
                                        </div>
                                        <div class="span5">
                                            <div class="input-control text">
                                                
                                                <input type="text" name="createUser" placeholder="1 หรือ 2 หรือ 3...." value="${model.createUser}"/>
                                                <button class="btn-clear"></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="span3">
                                            <label class="input-control text">
                                                วัน เดือน ปี
                                            </label>
                                        </div>
                                        <div class="span5">
                                            <div class="input-control text">
                                                <input type="hidden" name="progId" value="${model.progId}" />
                                                <input type="text" name="progDate" placeholder="วันที่นำเที่ยว" value="${model.progDate}"/>
                                                <button class="btn-clear"></button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="span3">
                                            <label class="input-control text">
                                                เวลา
                                            </label>
                                        </div>
                                        <div class="span5">
                                            <div class="input-control text">
                                                <input type="text" name="progTime" placeholder="เวลาที่ไปเที่ยว" value="${model.progTime}"/>
                                                <button class="btn-clear"></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="span3">
                                            <label class="input-control text">
                                                อาหาร
                                            </label>
                                        </div>
                                        <div class="span5">
                                            <div class="input-control text">
                                                <input type="text" name="progFood" placeholder="" value="${model.progFood}"/>
                                                <button class="btn-clear"></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="span3">
                                            <label class="input-control text">
                                                สถานที่ท่องเที่ยว
                                            </label>
                                        </div>
                                        <div class="span5">
                                            <div class="input-control text">
                                                <textarea name="progDesc" placeholder="รายละเอียดสถามที่ท่องเที่ยว" >${model.progDesc}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="span3">
                                            <label class="input-control text">
                                                โรงแรมที่พัก
                                            </label>
                                        </div>
                                        <div class="span5">
                                            <div class="input-control text">
                                                <input type="text" name="progHotel" placeholder="" value="${model.progHotel}"/>
                                                <button class="btn-clear"></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        
                    </div>

                    

                <div class="row">
                     <div class="span5">
                       <button><i class="icon-save"></i></i>บันทึก</button>
                    </div>
                  
                </div>
         
                
                

                <div id="simple-msg"></div>
             
            </div>
        </form>

                
               
                
                
                </div>
            </div>
        </div>
    </div>

<div class="page">
    <div class="nav-bar">
        <div class="nav-bar-inner padding10">
            <span class="element">
                สำนักทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ กรมการท่องเที่ยว</a>
            </span>
        </div>
    </div>
</div>

    </body>
</html>
