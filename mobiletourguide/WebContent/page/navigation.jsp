<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div class="page">
    <div class="nav-bar">
        <div class="nav-bar-inner padding10">
            <span class="pull-menu"></span> 

            <a href="${pageContext.request.contextPath}/info/main">
            	<span class="element brand">
                	<img class="place-left" src="${pageContext.request.contextPath}/page/images/logo32.png" style="height: 20px"/> 
                	<spring:message code="registry.tourism.department" text="สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์" />
            	</span>
            </a>

            <div class="divider"></div> 

            <ul class="menu">
                <li>
                	<a href="${pageContext.request.contextPath}/info/main">
                		<spring:message code="nav.mainPage" text="หน้าหลัก" />
                	</a>
                </li>
                <li data-role="dropdown">
                    <a href="#"><spring:message code="nav.licenseInfo" text="ข้อมูลใบอนุญาต" /></a>
                    <ul class="dropdown-menu">
	                    <li>
	                    	<a href="${pageContext.request.contextPath}/info/license/tour">
	                    		<spring:message code="nav.licenseTour" text="ข้อมูลใบอนุญาต" />
	                    	</a>
	                    </li>
	                    <li>
	                    	<a href="${pageContext.request.contextPath}/info/license/guide">
	                    		<spring:message code="nav.licenseGuide" text="ใบอนุญาตมัคคุเทศก์" />
	                    	</a>
	                    </li>
	                    <li>
	                    	<a href="${pageContext.request.contextPath}/info/license/leader">
	                    		<spring:message code="nav.licenseTourLeader" text="ใบอนุญาตผู้นำเที่ยว" />
	                    	</a>
	                    </li>
                    </ul>
                </li>
                <%-- <li>
                	<a href="${pageContext.request.contextPath}/info/license/complaint/menu">
                		<spring:message code="nav.complaint" text="ร้องเรียนใบอนุญาต" />
                	</a>
                </li>
        		<li>
        			<a href="${pageContext.request.contextPath}/page/MemberLogin.jsp">
        				<spring:message code="nav.member" text="สมาชิก" />
        			</a>
        		</li>  --%>
        		<li>
        			&nbsp;
        		</li>
        	</ul>
        	<div class="divider"></div>
        	<ul class="menu">
	        	<li>
		        	<a href="${pageContext.request.contextPath}/info/main?language=th">TH</a>
	        	</li>
	        	<li> 
	        		<a href="${pageContext.request.contextPath}/info/main?language=en">EN</a>
	        	</li>
        	</ul>
            
        </div>
    </div>
</div>