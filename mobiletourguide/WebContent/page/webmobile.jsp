<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="target-densitydpi=device-dpi, width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta name="description" content="Metro UI CSS">
    <meta name="author" content="Sergey Pimenov">
    <meta name="keywords" content="windows 8, modern style, Metro UI, style, modern, css, framework">

    <link href="${pageContext.request.contextPath}/page/css/modern.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/modern-responsive.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/page/css/site.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/page/js/google-code-prettify/prettify.css" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/assets/moment_langs.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/dropdown.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/accordion.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/buttonset.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/carousel.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/input-control.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/pagecontrol.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/rating.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-slider.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/tile-drag.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/page/js/modern/calendar.js"></script>

    <title><spring:message code="registry.tourism.department" text="สำนักงานทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์" /></title>
</head>
<body class="metrouicss" >

<jsp:include page="navigation.jsp" /> 

<div class="page" id="page-index">
    <div class="page-region">
        <div class="page-region-content">
            <div class="grid">
                <div class="row">
                    <div class="span12">
                        <div class="hero-unit">
                        	<%-- <a href="?language=th_TH">TH</a> | <a href="?language=en">EN</a>
                        	<spring:message code="main.page.license.tourism" text="default text" />
                        	<br/>
                        	${pageContext.response.locale} --%>
                            <div id="carousel1" class="carousel" data-role="carousel" data-param-duration="300">
                                <div class="slides">

                                    <div class="slide" id="slide1">
                                        <h2 class="padding-top5"><spring:message code="main.page.license.tourism" text="ใบอนุญาตธุรกิจนำเที่ยว" /></h2>
                                       

                                        <p class="bg-color-blueDark padding20 fg-color-white">
                                        	<spring:message code="main.page.tourism.description" text="ธุรกิจนำเที่ยว จดทะเบียนกับสำนักทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ กรมการท่องเที่ยว" />
                                        	
                                        </p>

                                        <div class="span3 place-left">
                                            <ul class="unstyled sprite-details">
                                                <li><i class="icon-checkmark"></i> <spring:message code="main.page.tourism.search" text="ค้นหา" /></li>
                                                <li><i class="icon-checkmark"></i> <spring:message code="main.page.tourism.investigate" text="ตรวจสอบ" /></li>                                            
                                            </ul>
                                        </div>
                                        <div class="span3 place-left">
                                            <ul class="unstyled sprite-details">
                                                <li><i class="icon-checkmark"></i> <spring:message code="main.page.tourism.complain" text="ร้องเรียน" /></li>
                                                <li><i class="icon-checkmark"></i> <spring:message code="main.page.tourism.FAQ" text="ซักถามข้อสงสัย" /></li>
                                            </ul>
                                        </div>

                                        <div class="span10 place-left">
                                            <ul class="unstyled sprite-details">
                                                <li>
                                                	<a href="${pageContext.request.contextPath}/info/license/tour">
		                                     			<strong><spring:message code="label.detail" text="รายละเอียด" /><i class="icon-arrow-right-3"></i></strong>
		                                     		</a>
	                                     		</li>
                                              
                                            </ul>
                                        </div>
                                       
                                        
                                        
                                    </div>

                                    <div class="slide" id="slide2">
                                        <h2 class="padding-top5"><spring:message code="main.page.license.guide" text="ใบอนุญาตมัคคุเทศก์" /></h2>

                                        <p class="bg-color-green padding20 fg-color-white">
                                        	<spring:message code="main.page.guide.description" text="มัคุเทศก์ จดทะเบียนกับสำนักทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ กรมการท่องเที่ยว" />
                                        	
                                        </p>
                                        <div class="span3 place-left">
                                            <ul class="unstyled sprite-details">
                                                <li><i class="icon-checkmark"></i> <spring:message code="main.page.guide.search" text="ค้นหา" /></li>
                                                <li><i class="icon-checkmark"></i> <spring:message code="main.page.guide.investigate" text="ตรวจสอบ" /></li>                                            
                                            </ul>
                                        </div>
                                        <div class="span3 place-left">
                                            <ul class="unstyled sprite-details">
                                                <li><i class="icon-checkmark"></i> <spring:message code="main.page.guide.complain" text="ร้องเรียน" /></li>
                                                <li><i class="icon-checkmark"></i> <spring:message code="main.page.guide.FAQ" text="ซักถามข้อสงสัย" /></li>
                                            </ul>
                                        </div>

                                        <div class="span10 place-left">
                                            <ul class="unstyled sprite-details">
                                                <li>
                                                	<a href="${pageContext.request.contextPath}/info/license/guide">
		                                     			<strong><spring:message code="label.detail" text="รายละเอียด" /><i class="icon-arrow-right-3"></i></strong>
		                                     		</a>
	                                     		</li>
                                              
                                            </ul>
                                        </div>
                                      	<%-- <div class="span3 place-left">
                                            <ul class="unstyled sprite-details">
                                                <li><i class="icon-checkmark"></i> ค้นหา</li>
                                                <li><i class="icon-checkmark"></i> ตรวจสอบ</li>
                                      
                                               
                                            </ul>
                                        </div>
                                        <div class="span3 place-left">
                                            <ul class="unstyled sprite-details">
                                                <li><i class="icon-checkmark"></i> ร้องเรียน</li>
                                                <li><i class="icon-checkmark"></i> ซักถามข้อสงสัย</li>
                                            </ul>
                                        </div>

                                        <div class="span10 place-left">
                                            <ul class="unstyled sprite-details">
                                                <li>
                                                	<a href="${pageContext.request.contextPath}/info/license/guide">
		                                     			<strong>รายละเอียด<i class="icon-arrow-right-3"></i></strong>
		                                     		</a>
	                                     		</li>
                                              
                                            </ul>
                                        </div> --%>

                                    </div>

                                    <div class="slide" id="slide3">
                                        <h2 class="padding-top5"><spring:message code="main.page.license.tourLeader" text="ใบอนุญาตผู้นำเที่ยว" /></h2>

                                        <p class="bg-color-orange padding20 fg-color-white">
                                        	<spring:message code="main.page.tourLeader.description" text="ผู้นำเที่ยว จดทะเบียนกับสำนักทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ กรมการท่องเที่ยว" />
                                        	
                                        </p>
                                        <div class="span3 place-left">
                                            <ul class="unstyled sprite-details">
                                                <li><i class="icon-checkmark"></i> <spring:message code="main.page.tourLeader.search" text="ค้นหา" /></li>
                                                <li><i class="icon-checkmark"></i> <spring:message code="main.page.tourLeader.investigate" text="ตรวจสอบ" /></li>                                            
                                            </ul>
                                        </div>
                                        <div class="span3 place-left">
                                            <ul class="unstyled sprite-details">
                                                <li><i class="icon-checkmark"></i> <spring:message code="main.page.tourLeader.complain" text="ร้องเรียน" /></li>
                                                <li><i class="icon-checkmark"></i> <spring:message code="main.page.tourLeader.FAQ" text="ซักถามข้อสงสัย" /></li>
                                            </ul>
                                        </div>

                                        <div class="span10 place-left">
                                            <ul class="unstyled sprite-details">
                                                <li>
                                                	<a href="${pageContext.request.contextPath}/info/license/complaint/menu">
		                                     			<strong><spring:message code="label.detail" text="รายละเอียด" /><i class="icon-arrow-right-3"></i></strong>
		                                     		</a>
	                                     		</li>
                                              
                                            </ul>
                                        </div>
                                     	<%-- <div class="span3 place-left">
                                            <ul class="unstyled sprite-details">
                                                <li><i class="icon-checkmark"></i> ค้าหา</li>
                                                <li><i class="icon-checkmark"></i> ตรวจสอบ</li>
                                             
                                               
                                            </ul>
                                        </div>
                                        <div class="span3 place-left">
                                            <ul class="unstyled sprite-details">
                                                <li><i class="icon-checkmark"></i> ร้องเรียน</li>
                                                <li><i class="icon-checkmark"></i> ซักถามข้อสงสัย</li>
                                            </ul>
                                        </div>
                                        <div class="span10 place-left">
                                            <ul class="unstyled sprite-details">
                                                <li>
                                                	<a href="${pageContext.request.contextPath}/info/license/complaint/menu">
		                                     			<strong>รายละเอียด<i class="icon-arrow-right-3"></i></strong>
		                                     		</a>
	                                     		</li>
                                              
                                            </ul>
                                        </div> --%>

                                    </div>
<!-- 
                                    <div class="slide" id="slide3">
                                        <h2 class="padding-top5">ร้องเรียนใบอนญาต</h2>

                                        <p class="bg-color-redLight padding20 fg-color-white">ร้องเรียนผู้ประกอบการธุรกิจนำเที่ยว มัคคุเทศก์ หรือผู้นำเที่ยว และสอบถามข้อสงสัยกับเจ้าหน้าที่</p>
                                        
										<div class="span10 place-left">
                                            <ul class="unstyled sprite-details">
                                                <li>
                                                	<a href="http://bizspark.com">
		                                     			<strong>รายละเอียด<i class="icon-arrow-right-3"></i></strong>
		                                     		</a>
	                                     		</li>
                                              
                                            </ul>
                                        </div>
                                    </div> -->



                                </div>

                                <span class="control left"><i class="icon-arrow-left-3"></i></span>
                                <span class="control right"><i class="icon-arrow-right-3"></i></span>

                            </div>
                        </div>
                    </div>
                  
                </div>
            </div>


        </div>
    </div>
</div>

    <div class="page">
        <div class="nav-bar">
            <div class="nav-bar-inner padding10">
                <span class="element">
                    <spring:message code="dot.or.th" text="สำนักทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ กรมการท่องเที่ยว"/>
                </span>
            </div>
        </div>
    </div>

    </body>
</html>
