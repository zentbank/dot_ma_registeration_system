package sss.dot.tourism.auth;

import com.sss.aut.service.User;
import com.sss.aut.service.UserData;
import com.sss.aut.service.imp.DefaultUser;
import com.sss.aut.service.imp.DefaultUserData;




public  class DummyUser 
{
	    private static DummyUser instance = null;
	    private static User user = null;

	    private DummyUser() {
	    	UserData userData = new DefaultUserData(1, "reg", "นายมัคคุเทศก์ รักดี",
	    			"มัคคุเทศก", "รักดี", "1", 1,
	    			"เจ้าหน้าที่ทะเบียน", 1);
	    
	    	user = new DefaultUser(userData.getUserId() , userData.getUserName(),userData);
	    }

	    public static DummyUser getInstance() {
	        if (instance == null) {
	            instance = new DummyUser();
	        }
	        return instance;
	    }
	    
	    public static User getUser() {
	        
	        return user;
	    }
}
