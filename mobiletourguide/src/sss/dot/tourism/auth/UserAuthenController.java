package sss.dot.tourism.auth;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import sss.dot.tourism.dto.info.LicenseDetailDTO;
import sss.dot.tourism.dto.mas.OfficerDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.http.MessageWrapper;
import sss.dot.tourism.service.info.ILicenseService;

import com.sss.aut.service.User;

@Controller
@RequestMapping("officer")
public class UserAuthenController {
	@Autowired
	private sss.dot.tourism.service.admin.IUserAccountService userAccountService;
	@Autowired 
	ILicenseService licenseService;
	
	Logger logger = Logger.getLogger(this.getClass());
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public @ResponseBody
	String userLogin(@RequestParam("userName") String userName,@RequestParam("password") String password) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			User user = (User) userAccountService.getUser(userName, password);
			
			if(user != null)
			{
				OfficerDTO dto = new OfficerDTO();

				dto.setOfficerId(user.getUserId());
				dto.setOfficerName(user.getUserData().getUserFullName());
				dto.setPosition(user.getUserData().getPositionName());
				
				MessageWrapper<OfficerDTO> msgWrapper = new MessageWrapper<OfficerDTO>(dto, true);

				return jsonmsg.writeInfoMessage(msgWrapper);
			}
			else
			{
				MessageWrapper<OfficerDTO> msgWrapper = new MessageWrapper<OfficerDTO>("กรุณาตรวจสอบข้อมูล", false);
				return jsonmsg.writeInfoMessage(msgWrapper);
			}



		} catch (Exception e) {
			e.printStackTrace();


			MessageWrapper<OfficerDTO> msgWrapper = new MessageWrapper<OfficerDTO>("เกิดข้อผิดพลาด กรุณาตรวจสอบข้อมูล", false);
			return jsonmsg.writeInfoMessage(msgWrapper);

		}
	}
	
	@RequestMapping(value = "/profile/{token}", method = RequestMethod.GET)
	public @ResponseBody  String readUser(@PathVariable("token") String userId)  {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			OfficerDTO dto = (OfficerDTO) userAccountService.getUserById(Long.valueOf(userId));
			
			if((dto.getOfficerName() != null) && (!dto.getOfficerName().isEmpty()))
			{
				
				MessageWrapper<OfficerDTO> msgWrapper = new MessageWrapper<OfficerDTO>(dto, true);

				return jsonmsg.writeInfoMessage(msgWrapper);
			}
			else
			{
				MessageWrapper<OfficerDTO> msgWrapper = new MessageWrapper<OfficerDTO>("กรุณาตรวจสอบข้อมูล", false);
				return jsonmsg.writeInfoMessage(msgWrapper);
			}

		} catch (Exception e) {
			e.printStackTrace();
			MessageWrapper<OfficerDTO> msgWrapper = new MessageWrapper<OfficerDTO>("เกิดข้อผิดพลาด กรุณาตรวจสอบข้อมูล", false);
			return jsonmsg.writeInfoMessage(msgWrapper);

		}
	}
	
	@RequestMapping(value = "/verify/{token}/{start}/{limit}", method = RequestMethod.GET)
	public @ResponseBody  String readVerify(@PathVariable("token") String userId ,@PathVariable("start") String start ,@PathVariable("limit") String limit)  {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
					Long numAll = new Long(licenseService.getByOfficer(
							Long.valueOf(userId),
							0,
							Integer.MAX_VALUE).size());
					
					List list = this.licenseService.getByOfficer(
							Long.valueOf(userId), 
							Integer.parseInt(start), 
							Integer.parseInt(limit));
			
			MessageWrapper<LicenseDetailDTO> msgWrapper = new MessageWrapper<LicenseDetailDTO>(list, true, numAll);
			return jsonmsg.writeInfoMessage(msgWrapper);

		} catch (Exception e) {
			e.printStackTrace();
			MessageWrapper<OfficerDTO> msgWrapper = new MessageWrapper<OfficerDTO>("เกิดข้อผิดพลาด กรุณาตรวจสอบข้อมูล", false);
			return jsonmsg.writeInfoMessage(msgWrapper);

		}
	}
	
	@RequestMapping(value = "/verify/save", method = RequestMethod.POST)
	public @ResponseBody  String saveVerify(@RequestParam("data") String json)  {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
						
			Object[] object = (Object[]) jsonmsg.toArrayObject(
					LicenseDetailDTO.class, json);

			List list = this.licenseService.saveVerify(object);

			return jsonmsg.writeMessage(list);

		} catch (Exception e) {
			e.printStackTrace();
			MessageWrapper<OfficerDTO> msgWrapper = new MessageWrapper<OfficerDTO>("เกิดข้อผิดพลาด กรุณาตรวจสอบข้อมูล", false);
			return jsonmsg.writeInfoMessage(msgWrapper);

		}
	}
	
}
