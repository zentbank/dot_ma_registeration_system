package sss.dot.tourism.controller.complaint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import sss.dot.tourism.dto.complaint.ComplaintLicenseDTO;
import sss.dot.tourism.dto.info.LicenseDetailDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.service.complaint.IComplaintService;
import sss.dot.tourism.service.info.ILicenseService;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.ValidatorUtil;

@Controller
@RequestMapping("/license/complaint")
public class ComplaintController {
	@Autowired 
	ILicenseService licenseService;
	@Autowired
	IComplaintService complaintService;
	
	@RequestMapping( method=RequestMethod.POST )
	public  String view(@RequestParam("traderId") String traderId , Model model) {

		try {
			LicenseDetailDTO dto = new LicenseDetailDTO();
			dto.setTraderId(new Long(String.valueOf(traderId)));
			
			LicenseDetailDTO licenseDetail = (LicenseDetailDTO)licenseService.getByLicenseId(dto);
			
			model.addAttribute("model", licenseDetail);
			
			return "/page/AddComplaint.jsp";

		} catch (Exception e) {
			e.printStackTrace();			
			return "/page/pageerror.jsp";
	
		}
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public  String upload(
			@RequestParam("traderId") String traderId
			,@RequestParam("questionType") String questionType
			,@RequestParam("complaintDesc") String complaintDesc
			,@RequestParam("complaintCardId") String complaintCardId
			,@RequestParam("complaintName") String complaintName
			,@RequestParam("complaintTel") String complaintTel
			,@RequestParam("complaintEmail") String complaintEmail
//			,@RequestParam("traderName") String traderName
			,@RequestParam("complaintPic") MultipartFile[] f
			, Model model) 
	{

	    try{
	    	
	    	boolean isValid = true;
	    	String errorMsg = "เกิดข้อผิดพลาด<br />";
	    	
	    	ComplaintLicenseDTO param =  new ComplaintLicenseDTO();
	    	
	    	if((traderId != null) && (!traderId.isEmpty()))
	    	{
	    		param.setTraderId(new Long(String.valueOf(traderId)));
	    	}
//	    	else if((traderName != null) && (!traderName.isEmpty()))
//	    	{
//	    		param.setTraderName(traderName);
//	    	}
	    	else
	    	{
	    		isValid = false;
	    		errorMsg += "กรุณาตรวจสอบ ชื่อใบอนุญาต (ธุรกิจนำเที่ยว มัคคุเทศกื หรือผู้นำเที่ยว) <br />";
	    	}
	    	
	    	
	    	param.setQuestionType(questionType);
	    	param.setComplaintDesc(complaintDesc);
	    	param.setComplaintCardId(complaintCardId);
	    	param.setComplaintName(complaintName);
	    	param.setComplaintTel(complaintTel);
	    	param.setComplaintEmail(complaintEmail);
	    	
	    	if(complaintDesc == null || (complaintDesc.isEmpty()))
	    	{
	    		isValid = false;
	    		errorMsg += "กรุณาตรวจสอบ รายละเอียด <br />";
	    	}
	    	
	    	if(complaintCardId == null || (!ValidatorUtil.cardId(complaintCardId)))
	    	{
	    		isValid = false;
	    		errorMsg += "กรุณาตรวจสอบ หมายเลขบัตรประชาชน <br />";
	    	}
	    	
	    	if(complaintName == null || (complaintName.isEmpty()))
	    	{
	    		isValid = false;
	    		errorMsg += "กรุณาตรวจสอบ ชื่อ-นามสกุล <br />";
	    	}
	    	
	    	if(complaintTel == null || (!ValidatorUtil.phone(complaintTel)))
	    	{
	    		isValid = false;
	    		errorMsg += "กรุณาตรวจสอบ หมายเลขโทรศัพท์ <br />";
	    	}
	    	
	    	if(complaintEmail == null || (!ValidatorUtil.email(complaintEmail)))
	    	{
	    		isValid = false;
	    		errorMsg += "กรุณาตรวจสอบ Email <br />";
	    	}
	    	
	    	String forward = "/page/savesuccess.jsp";
	    	
	    	if(isValid)
	    	{
		    	this.complaintService.saveComplaintReceive(param, f);

		    	forward = "/page/savesuccess.jsp";
	    	}
	    	else
	    	{
				LicenseDetailDTO dto = new LicenseDetailDTO();
				dto.setTraderId(new Long(String.valueOf(traderId)));
				
				LicenseDetailDTO licenseDetail = (LicenseDetailDTO)licenseService.getByLicenseId(dto);
				
				licenseDetail.setErrMsg(errorMsg);
				
				
				ObjectUtil.copy(param,  licenseDetail);
				
				model.addAttribute("model", licenseDetail);
				
				forward = "/page/AddComplaint.jsp";
	    		
	    	}
	    	
	    	
	    	return forward;

	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    	return "/page/pageerror.jsp";
	    }
	
	}
	
	@RequestMapping(value = "/other/save", method = RequestMethod.POST)
	public  String uploadOth(
		
			@RequestParam("questionType") String questionType
			,@RequestParam("complaintDesc") String complaintDesc
			,@RequestParam("complaintCardId") String complaintCardId
			,@RequestParam("complaintName") String complaintName
			,@RequestParam("complaintTel") String complaintTel
			,@RequestParam("complaintEmail") String complaintEmail
			,@RequestParam("traderName") String traderName
			,@RequestParam("traderType") String traderType
			,@RequestParam("complaintPic") MultipartFile[] f
			, Model model) 
	{

	    try{
	    	
	    	boolean isValid = true;
	    	String errorMsg = "เกิดข้อผิดพลาด<br />";
	    	
	    	ComplaintLicenseDTO param =  new ComplaintLicenseDTO();
	    	
	   
	    	if((traderName != null) && (!traderName.isEmpty()))
	    	{
	    		param.setTraderName(traderName);
	    	}
	    	else
	    	{
	    		isValid = false;
	    		errorMsg += "กรุณาตรวจสอบ ชื่อใบอนุญาต (ธุรกิจนำเที่ยว มัคคุเทศกื หรือผู้นำเที่ยว) <br />";
	    	}
	    	
	    	param.setTraderType(traderType);
	    	param.setQuestionType(questionType);
	    	param.setComplaintDesc(complaintDesc);
	    	param.setComplaintCardId(complaintCardId);
	    	param.setComplaintName(complaintName);
	    	param.setComplaintTel(complaintTel);
	    	param.setComplaintEmail(complaintEmail);
	    	
	    	if(complaintDesc == null || (complaintDesc.isEmpty()))
	    	{
	    		isValid = false;
	    		errorMsg += "กรุณาตรวจสอบ รายละเอียด <br />";
	    	}
	    	
	    	if(complaintCardId == null || (!ValidatorUtil.cardId(complaintCardId)))
	    	{
	    		isValid = false;
	    		errorMsg += "กรุณาตรวจสอบ หมายเลขบัตรประชาชน <br />";
	    	}
	    	
	    	if(complaintName == null || (complaintName.isEmpty()))
	    	{
	    		isValid = false;
	    		errorMsg += "กรุณาตรวจสอบ ชื่อ-นามสกุล <br />";
	    	}
	    	
	    	if(complaintTel == null || (!ValidatorUtil.phone(complaintTel)))
	    	{
	    		isValid = false;
	    		errorMsg += "กรุณาตรวจสอบ หมายเลขโทรศัพท์ <br />";
	    	}
	    	
	    	if(complaintEmail == null || (!ValidatorUtil.email(complaintEmail)))
	    	{
	    		isValid = false;
	    		errorMsg += "กรุณาตรวจสอบ Email <br />";
	    	}
	    	
	    	String forward = "/page/savesuccess.jsp";
	    	
	    	if(isValid)
	    	{
		    	this.complaintService.saveComplaintReceive(param, f);

		    	forward = "/page/savesuccess.jsp";
	    	}
	    	else
	    	{
				LicenseDetailDTO dto = new LicenseDetailDTO();
			
	
				
				dto.setErrMsg(errorMsg);
				
				
				ObjectUtil.copy(param,  dto);
				
				model.addAttribute("model", dto);
				
				forward = "/page/AddComplaintOth.jsp";
	    		
	    	}
	    	
	    	
	    	return forward;

	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    	return "/page/pageerror.jsp";
	    }
	
	}
	
	@RequestMapping( value = "/menu", method=RequestMethod.GET )
	public  String complaintmenu() {

		try {
			
			return "/page/ComplaintMenu.jsp";

		} catch (Exception e) {
			e.printStackTrace();			
			return "/page/pageerror.jsp";
	
		}
	}
	
	@RequestMapping( value = "/other", method=RequestMethod.GET )
	public  String complaintother() {
		try {
			
			return "/page/AddComplaintOth.jsp";

		} catch (Exception e) {
			e.printStackTrace();			
			return "/page/pageerror.jsp";
	
		}
	}
}
