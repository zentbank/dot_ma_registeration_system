package sss.dot.tourism.controller.info;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import sss.dot.tourism.dto.info.Fruit;

@Controller
@RequestMapping("/fruit")
public class FruitController {

	@RequestMapping(value="{fruitName}", method = RequestMethod.GET)
	public String getFruit(@PathVariable String fruitName, ModelMap model) {

		System.out.println("fruitNames = " + fruitName);
		
		List list = new ArrayList();

		
		Fruit fruit = new Fruit(fruitName, 1000);
		
		
		list.add("name1");
		list.add("name2");
		list.add("name3");
		list.add("name4");
		
		fruit.setMylist(list);
	
		model.addAttribute("model", fruit);
		model.addAttribute("list", list);
		
		return "/page/list.jsp";

	}
}
