package sss.dot.tourism.controller.info;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.OutputStream;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import sss.dot.tourism.service.info.ILicenseService;
import sss.dot.tourism.util.ConstantUtil;

@Controller
@RequestMapping("/image")
public class ImageController {
	
	@Autowired
	ILicenseService licenseService;
	
	@RequestMapping(value="/id/{id}", method=RequestMethod.GET )
	//id traderId
	public void readPicProfile(@PathVariable("id") String id
			,HttpServletResponse response) {

		try {
			
//			resp.setContentType("image/jpeg");
			
			System.out.println("@PathVariable ===== " + id);
			
//			String imgPath = this.licenseService.getImage(Long.valueOf(id));
//
//			File f = new File(imgPath);
//			
//			BufferedImage bi = ImageIO.read(f);
//			OutputStream out = resp.getOutputStream();
//			ImageIO.write(bi, "jpg", out);
//			out.close();
			
			String docPath = this.licenseService.getImage(Long.valueOf(id));
			
			String fileType = docPath.substring(docPath.lastIndexOf(".") + 1, docPath.length());
			
			File f = new File(docPath);
			if (!f.exists()) {  
				docPath = ConstantUtil.TEMPLATE_GUIDE_FILE_LOCATION+"blank.jpg";
			}
			f = new File(docPath);
			if (f.exists()) { 
				response.setContentType("image/" + fileType);
				BufferedImage bi = ImageIO.read(f);
				OutputStream out = response.getOutputStream();
				ImageIO.write(bi, fileType, out);
				out.close();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
	
		}
	}
}
