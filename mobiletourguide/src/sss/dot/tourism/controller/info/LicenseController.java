package sss.dot.tourism.controller.info;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import sss.dot.tourism.dto.info.InfoMessageWrapper;
import sss.dot.tourism.dto.info.LicenseDetailDTO;
import sss.dot.tourism.dto.info.LicenseInfoDTO;
import sss.dot.tourism.dto.info.RequestParamDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.service.info.ILicenseService;
import sss.dot.tourism.util.TraderType;

@Controller
@RequestMapping("/license")
public class LicenseController {
	
	@Autowired ILicenseService licenseService;
	
	@RequestMapping(value="/Language/{Language}/SearchType/{SearchType}", method=RequestMethod.GET )
	public @ResponseBody String readLicenseAllGETMethod(@PathVariable("Language") String language
			,@PathVariable("SearchType") String searchType
			, Model model ,HttpServletResponse resp) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			
	
			
			RequestParamDTO param = new RequestParamDTO();
			
			param.setLanguage(language);
			param.setSearchType(searchType);
	
			
			List list = licenseService.getAll(param);
			
			String status = "success";
			String message = "";
			if(list.isEmpty())
			{
				status = "fail";
				message = "ไม่พบข้อมูล";
				list = new ArrayList();
			}
			
			InfoMessageWrapper<LicenseInfoDTO> msg = new InfoMessageWrapper<LicenseInfoDTO>(list, searchType, status);
			msg.setMessage(message);
		
			
//			resp.setContentType("text/html;charSet=UTF-8");
			
			return jsonmsg.writeInfoMessage(msg);

		} catch (Exception e) {
			e.printStackTrace();			
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value="/Language/{Language}/SearchType/{SearchType}", method=RequestMethod.POST )
	public @ResponseBody String readLicenseAllPostMethod(@PathVariable("Language") String language
			,@PathVariable("SearchType") String searchType

			, Model model ,HttpServletResponse resp) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			

			
			RequestParamDTO param = new RequestParamDTO();
			
			param.setLanguage(language);
			param.setSearchType(searchType);

			
			List list = licenseService.getAll(param);
			
			String status = "success";
			String message = "";
			if(list.isEmpty())
			{
				status = "fail";
				message = "ไม่พบข้อมูล";
				list = new ArrayList();
			}
			
			InfoMessageWrapper<LicenseInfoDTO> msg = new InfoMessageWrapper<LicenseInfoDTO>(list, searchType, status);
			msg.setMessage(message);
		
			
//			resp.setContentType("text/html;charSet=UTF-8");
			
			return jsonmsg.writeInfoMessage(msg);

		} catch (Exception e) {
			e.printStackTrace();			
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	

	
	@RequestMapping(value="/Language/{Language}/SearchType/{SearchType}/SearchParam/{SearchParam}", method=RequestMethod.GET )
	public @ResponseBody String readLicenseByParamGetMethod(@PathVariable("Language") String language
			,@PathVariable("SearchType") String searchType
			,@PathVariable("SearchParam") String searchParam
			, Model model ,HttpServletResponse resp) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			
			System.out.println("searchParam = " + searchParam);
			
			RequestParamDTO param = new RequestParamDTO();
			
			param.setLanguage(language);
			param.setSearchType(searchType);
			param.setSearchParam(searchParam);
			
			List list = licenseService.getAll(param);
			
			/*
			 * 	private List<E> data;
	private String status;
	private String data_type;
	private String message;*/
			String status = "success";
			String message = "";
			if(list.isEmpty())
			{
				status = "fail";
				message = "ไม่พบข้อมูล";
				list = new ArrayList();
			}
			
			InfoMessageWrapper<LicenseInfoDTO> msg = new InfoMessageWrapper<LicenseInfoDTO>(list, searchType, status);
			msg.setMessage(message);
		
			
//			resp.setContentType("text/html;charSet=UTF-8");
			
			return jsonmsg.writeInfoMessage(msg);

		} catch (Exception e) {
			e.printStackTrace();			
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value="/Language/{Language}/SearchType/{SearchType}/SearchParam/{SearchParam}", method=RequestMethod.POST)
	public @ResponseBody String readLicenseByParamPostMethod(@PathVariable("Language") String language
			,@PathVariable("SearchType") String searchType
			,@PathVariable("SearchParam") String searchParam
			, Model model) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			
			System.out.println("searchParam = " + searchParam);
			
			RequestParamDTO param = new RequestParamDTO();
			
			param.setLanguage(language);
			param.setSearchType(searchType);
			param.setSearchParam(searchParam);
			
			List list = licenseService.getAll(param);
			
			/*
			 * 	private List<E> data;
	private String status;
	private String data_type;
	private String message;*/
			String status = "success";
			String message = "";
			if(list.isEmpty())
			{
				status = "fail";
				message = "ไม่พบข้อมูล";
				list = new ArrayList();
			}
			
			InfoMessageWrapper<LicenseInfoDTO> msg = new InfoMessageWrapper<LicenseInfoDTO>(list, searchType, status);
			msg.setMessage(message);
		
			
//			resp.setContentType("text/html;charSet=UTF-8");
			
			return jsonmsg.writeInfoMessage(msg);

		} catch (Exception e) {
			e.printStackTrace();	
			
			String status = "fail";
			String message = "ไม่พบข้อมูล";
			List list = new ArrayList();
			
			InfoMessageWrapper<LicenseInfoDTO> msg = new InfoMessageWrapper<LicenseInfoDTO>(list, searchType, status);
			msg.setMessage(message);
			
			return jsonmsg.writeErrorMessage(msg);
	
		}
	}
	
	@RequestMapping(value="/LicenseType/{LicenseType}/LicNo/{LicNo}", method=RequestMethod.GET )
	public String readLicenseGET(@PathVariable("LicenseType") String licenseType
			,@PathVariable("LicNo") String licenseNo
			, Model model ,HttpServletRequest req ,HttpServletResponse resp) {

		try
		{
			if(licenseNo != null)
			{
				if(TraderType.TOUR_COMPANIES.getStatus().equals(licenseType))
				{
					licenseNo = licenseNo.replace(TraderType.TOUR_COMPANIES.getStatus(), "/");
				}
				
				if(TraderType.GUIDE.getStatus().equals(licenseType))
				{
					licenseNo = licenseNo.replace(TraderType.GUIDE.getStatus(), "-");
				}
				
				if(TraderType.LEADER.getStatus().equals(licenseType))
				{
					licenseNo = licenseNo.replace(TraderType.LEADER.getStatus(), ".");
				}
			}
			
				
			LicenseDetailDTO licenseDetail = (LicenseDetailDTO)licenseService.getByLicenseNo(licenseNo, licenseType);
			
			model.addAttribute("model", licenseDetail);
			
			String forwordPage = "/page/pageerror.jsp";
			
			if(TraderType.TOUR_COMPANIES.getStatus().equals(licenseDetail.getTraderType()))
			{
				forwordPage = "/page/TourristDetail.jsp";
			}
			
			if(TraderType.GUIDE.getStatus().equals(licenseDetail.getTraderType()))
			{
				forwordPage = "/page/GuideDetail.jsp";
			}
			
			if(TraderType.LEADER.getStatus().equals(licenseDetail.getTraderType()))
			{
				forwordPage = "/page/TourleaderDetail.jsp";
			}
			
			return forwordPage;
		
		} catch (Exception e) {
			e.printStackTrace();
			return "/page/pageerror.jsp";
	
		}
	}
	
	@RequestMapping(value="/LicenseType/{LicenseType}/LicNo/{LicNo}", method=RequestMethod.POST )
	public String readLicensePOST(@PathVariable("LicenseType") String licenseType
			,@PathVariable("LicNo") String licenseNo
			, Model model ,HttpServletRequest req,HttpServletResponse resp) {

		try
		{
			System.out.println(req.getHeader("User-Agent"));
			LicenseDetailDTO licenseDetail = (LicenseDetailDTO)licenseService.getByLicenseNo(licenseNo, licenseType);
			
			model.addAttribute("model", licenseDetail);
			
			String forwordPage = "/page/TourristDetail.jsp";
			
			if(TraderType.GUIDE.getStatus().equals(licenseDetail.getTraderType()))
			{
				forwordPage = "/page/GuideDetail.jsp";
			}
			
			if(TraderType.LEADER.getStatus().equals(licenseDetail.getTraderType()))
			{
				forwordPage = "/page/TourleaderDetail.jsp";
			}
			
			return forwordPage;
		
		} catch (Exception e) {
			e.printStackTrace();
			return "/page/pageerror.jsp";
	
		}
	}
	

}
