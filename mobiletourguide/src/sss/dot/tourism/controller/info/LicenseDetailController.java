package sss.dot.tourism.controller.info;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import sss.dot.tourism.dto.info.LicenseDetailDTO;
import sss.dot.tourism.service.info.ILicenseService;
import sss.dot.tourism.util.TraderType;

@Controller
@RequestMapping("/license/detail")
public class LicenseDetailController {
	@Autowired 
	ILicenseService licenseService;
	
	@RequestMapping(value="{id}", method = RequestMethod.GET)
	public String getLicenseDetail(@PathVariable String id, ModelMap model) {
		
		try
		{
			LicenseDetailDTO dto = new LicenseDetailDTO();
			dto.setTraderId(new Long(String.valueOf(id)));
			
			LicenseDetailDTO licenseDetail = (LicenseDetailDTO)licenseService.getByLicenseId(dto);
			
			model.addAttribute("model", licenseDetail);
			
			String forwordPage = "/page/TourristDetail.jsp";
			
			if(TraderType.GUIDE.getStatus().equals(licenseDetail.getTraderType()))
			{
				forwordPage = "/page/GuideDetail.jsp";
			}
			
			if(TraderType.LEADER.getStatus().equals(licenseDetail.getTraderType()))
			{
				forwordPage = "/page/TourleaderDetail.jsp";
			}
			
			
			
			return forwordPage;
		
		} catch (Exception e) {
			e.printStackTrace();
			return "/page/pageerror.jsp";
	
		}



	}
	
	@RequestMapping(value="{id}", method = RequestMethod.POST)
	public String getLicenseDetailPost(@PathVariable String id, ModelMap model) {
		
		try
		{
			LicenseDetailDTO dto = new LicenseDetailDTO();
			dto.setTraderId(new Long(String.valueOf(id)));
			
			LicenseDetailDTO licenseDetail = (LicenseDetailDTO)licenseService.getByLicenseId(dto);
			
			model.addAttribute("model", licenseDetail);
			
			String forwordPage = "/page/TourristDetail.jsp";
			
			if(TraderType.GUIDE.getStatus().equals(licenseDetail.getTraderType()))
			{
				forwordPage = "/page/GuideDetail.jsp";
			}
			
			if(TraderType.LEADER.getStatus().equals(licenseDetail.getTraderType()))
			{
				forwordPage = "/page/TourleaderDetail.jsp";
			}
			
			
			
			return forwordPage;
		
		} catch (Exception e) {
			e.printStackTrace();
			return "/page/pageerror.jsp";
	
		}



	}
	
	@RequestMapping(value="{licenseNo}/{licenseType}", method = RequestMethod.GET)
	public String readLicenseDetail(@PathVariable("licenseNo") String licenseNo ,@PathVariable("licenseType") String licenseType , ModelMap model) {
		
		try
		{
						
			LicenseDetailDTO licenseDetail = (LicenseDetailDTO)licenseService.getByLicenseNo(licenseNo, licenseType);
			
			model.addAttribute("model", licenseDetail);
			
			String forwordPage = "/page/TourristDetail.jsp";
			
			if(TraderType.GUIDE.getStatus().equals(licenseDetail.getTraderType()))
			{
				forwordPage = "/page/GuideDetail.jsp";
			}
			
			if(TraderType.LEADER.getStatus().equals(licenseDetail.getTraderType()))
			{
				forwordPage = "/page/TourleaderDetail.jsp";
			}
			
			
			
			return forwordPage;
		
		} catch (Exception e) {
			e.printStackTrace();
			return "/page/pageerror.jsp";
	
		}



	}
}
