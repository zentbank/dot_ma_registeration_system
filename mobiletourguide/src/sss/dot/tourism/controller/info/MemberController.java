package sss.dot.tourism.controller.info;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import sss.dot.tourism.dto.info.InfoMessageWrapper;
import sss.dot.tourism.dto.info.LicenseDetailDTO;
import sss.dot.tourism.dto.info.LicenseInfoDTO;
import sss.dot.tourism.dto.info.MemberDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.service.info.ILicenseService;
import sss.dot.tourism.service.trader.IMemberLicenseService;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.User;
import com.sss.aut.service.UserData;
import com.sss.aut.service.imp.DefaultUser;
import com.sss.aut.service.imp.DefaultUserData;

@Controller
@RequestMapping("/member")
public class MemberController {
	
	@Autowired ILicenseService licenseService;
	@Autowired 
	IMemberLicenseService memberLicenseService;
	
	
	@Autowired
	private HttpServletRequest httpreq;
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public @ResponseBody
	String memberLogin(@RequestParam("UserName") String userName,@RequestParam("Password") String password) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			List list = new ArrayList();

			MemberDTO dto = new MemberDTO();
			
			this.memberLicenseService.doLogin(userName, password, dto);
			
			
			
			if(dto.isValid())
			{
				UserData userData = new DefaultUserData(0, dto.getIndentityNo(), dto.getIndentityNo(),
						dto.getIndentityNo(), dto.getIndentityNo(), "", 0,
						"", 0);
				User user = new DefaultUser(userData.getUserId(),
						userData.getUserName(), userData);
				
				httpreq.getSession(true).setAttribute("member", user);  
				
				list.add(dto);

				InfoMessageWrapper<MemberDTO> msg = new InfoMessageWrapper<MemberDTO>(
						list, "LoginResponse", "success");
				msg.setMessage("");

				return jsonmsg.writeInfoMessage(msg);
			}
			else
			{
				String status = "fail";
				String message = "ไม่พบข้อมูล";
				
				InfoMessageWrapper<LicenseInfoDTO> msg = new InfoMessageWrapper<LicenseInfoDTO>(
						list, "LoginResponse", status);
				msg.setMessage(message);

				return jsonmsg.writeInfoMessage(msg);
			}



		} catch (Exception e) {
			e.printStackTrace();

			String status = "fail";
			String message = "ไม่พบข้อมูล";
			List list = new ArrayList();

			InfoMessageWrapper<LicenseInfoDTO> msg = new InfoMessageWrapper<LicenseInfoDTO>(
					list, "LoginResponse", status);
			msg.setMessage(message);

			return jsonmsg.writeInfoMessage(msg);

		}
	}
	
	@RequestMapping(value = "/profile/{token}", method = RequestMethod.GET)
	public  String memberProfileGet(@PathVariable String token, ModelMap model) {

		try {
			
			String identityNo = token;//getIdentityNo(token);
			LicenseDetailDTO param = new LicenseDetailDTO();
			param.setIndentityNo(identityNo);
			List list = this.licenseService.getByIdentityNo(param);
			LicenseDetailDTO licenseDetail = (LicenseDetailDTO)list.get(0);
			
			model.addAttribute("list", list);
			model.addAttribute("model", licenseDetail.getImgUrl());
			
			String forwordPage = "/page/MemberToruisDetail.jsp";
			
			if(TraderType.GUIDE.getStatus().equals(licenseDetail.getTraderType()))
			{
				forwordPage = "/page/MemberGuideDetail.jsp";
			}
			
			if(TraderType.LEADER.getStatus().equals(licenseDetail.getTraderType()))
			{
				forwordPage = "/page/MemberGuideDetail.jsp";
			}
			
			
			
			return forwordPage;


		} catch (Exception e) {
			e.printStackTrace();
			return "/page/pageerror.jsp";

		}
	}
	
	@RequestMapping(value = "/profile/{token}", method = RequestMethod.POST)
	public @ResponseBody
	String memberProfile(@PathVariable String token, ModelMap model) {

		try {

			String identityNo = token;//getIdentityNo(token);
			LicenseDetailDTO param = new LicenseDetailDTO();
			param.setIndentityNo(identityNo);
			List list = this.licenseService.getByIdentityNo(param);
			LicenseDetailDTO licenseDetail = (LicenseDetailDTO)list.get(0);
			
			model.addAttribute("list", list);
			model.addAttribute("model", licenseDetail.getImgUrl());
			
			String forwordPage = "/page/MemberToruisDetail.jsp";
			
			if(TraderType.GUIDE.getStatus().equals(licenseDetail.getTraderType()))
			{
				forwordPage = "/page/MemberGuideDetail.jsp";
			}
			
			if(TraderType.LEADER.getStatus().equals(licenseDetail.getTraderType()))
			{
				forwordPage = "/page/MemberGuideDetail.jsp";
			}
			
			
			
			return forwordPage;


		} catch (Exception e) {
			e.printStackTrace();
			return "/page/pageerror.jsp";

		}
	}
	
	@RequestMapping(value = "/profile/web/{token}", method = RequestMethod.POST)
	public	String memberWebProfile(@PathVariable String token, ModelMap model) {

		try {

			String identityNo = token;//getIdentityNo(token);
			LicenseDetailDTO param = new LicenseDetailDTO();
			param.setIndentityNo(identityNo);
			List list = this.licenseService.getByIdentityNo(param);
			LicenseDetailDTO licenseDetail = (LicenseDetailDTO)list.get(0);
			
			model.addAttribute("model", licenseDetail.getImgUrl());
			model.addAttribute("list", list);
			
			String forwordPage = "/page/MemberTourisProfile.jsp";
//			String forwordPage = "/page/MemberToruisDetail.jsp";
			
			
			if(TraderType.GUIDE.getStatus().equals(licenseDetail.getTraderType()))
			{
				forwordPage = "/page/MemberGuideProfile.jsp";
//				forwordPage = "/page/MemberGuideDetail.jsp";
			}
			
			if(TraderType.LEADER.getStatus().equals(licenseDetail.getTraderType()))
			{
				forwordPage = "/page/MemberGuideProfile.jsp";
//				forwordPage = "/page/MemberGuideDetail.jsp";
			}
			
			
			
			return forwordPage;


		} catch (Exception e) {
			e.printStackTrace();
			return "/page/pageerror.jsp";

		}
	}
	
	@RequestMapping(value = "/resetpassword/{username}/{email}", method = RequestMethod.POST)
	public @ResponseBody
	String memberResetPassword(@PathVariable("username") String userName, @PathVariable("email") String email) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			List list = new ArrayList();

			
			this.memberLicenseService.resetPass(userName, email);

			InfoMessageWrapper<MemberDTO> msg = new InfoMessageWrapper<MemberDTO>(
					list, "resetpassword", "success");
			msg.setMessage("");

			return jsonmsg.writeInfoMessage(msg);

		}		
		catch (IllegalAccessError ex)
		{
			ex.printStackTrace();

			String status = "fail";
			List list = new ArrayList();

			InfoMessageWrapper<LicenseInfoDTO> msg = new InfoMessageWrapper<LicenseInfoDTO>(
					list, "resetpassword", status);
			msg.setMessage(ex.getMessage().toString());

			return jsonmsg.writeErrorMessage(msg);
		}
		catch (Exception e) {
			e.printStackTrace();

			String status = "fail";
			String message = "ไม่พบข้อมูล";
			List list = new ArrayList();

			InfoMessageWrapper<LicenseInfoDTO> msg = new InfoMessageWrapper<LicenseInfoDTO>(
					list, "resetpassword", status);
			msg.setMessage(message);

			return jsonmsg.writeErrorMessage(msg);

		}
	}
	
	@RequestMapping(value = "/changepassword/{token}/{password}", method = RequestMethod.POST)
	public @ResponseBody
	String memberChangePasswordGetMethod(@PathVariable("token") String token,@PathVariable("password") String password) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			List list = new ArrayList();
			
			this.memberLicenseService.changePassword(token, password);

			InfoMessageWrapper<MemberDTO> msg = new InfoMessageWrapper<MemberDTO>(
					list, "changepassword", "success");
			msg.setMessage("");

			return jsonmsg.writeInfoMessage(msg);

		}
		catch (IllegalAccessError ex)
		{
			ex.printStackTrace();

			String status = "fail";
			List list = new ArrayList();

			InfoMessageWrapper<LicenseInfoDTO> msg = new InfoMessageWrapper<LicenseInfoDTO>(
					list, "changepassword", status);
			msg.setMessage(ex.getMessage().toString());

			return jsonmsg.writeErrorMessage(msg);
		}
		catch (Exception e) {
			e.printStackTrace();

			String status = "fail";
			String message = "ไม่พบข้อมูล";
			List list = new ArrayList();

			InfoMessageWrapper<LicenseInfoDTO> msg = new InfoMessageWrapper<LicenseInfoDTO>(
					list, "changepassword", status);
			msg.setMessage(message);

			return jsonmsg.writeErrorMessage(msg);

		}
	}
	
	@RequestMapping(value = "/addmember", method = RequestMethod.GET)
	public @ResponseBody
	String memberResetPassword() {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			List list = new ArrayList();

			
			this.memberLicenseService.addMember();

			InfoMessageWrapper<MemberDTO> msg = new InfoMessageWrapper<MemberDTO>(
					list, "addmember", "success");
			msg.setMessage("");

			return jsonmsg.writeInfoMessage(msg);

		}		
		catch (IllegalAccessError ex)
		{
			ex.printStackTrace();

			String status = "fail";
			List list = new ArrayList();

			InfoMessageWrapper<LicenseInfoDTO> msg = new InfoMessageWrapper<LicenseInfoDTO>(
					list, "resetpassword", status);
			msg.setMessage(ex.getMessage().toString());

			return jsonmsg.writeErrorMessage(msg);
		}
		catch (Exception e) {
			e.printStackTrace();

			String status = "fail";
			String message = "ไม่พบข้อมูล";
			List list = new ArrayList();

			InfoMessageWrapper<LicenseInfoDTO> msg = new InfoMessageWrapper<LicenseInfoDTO>(
					list, "resetpassword", status);
			msg.setMessage(message);

			return jsonmsg.writeErrorMessage(msg);

		}
	}
}
