package sss.dot.tourism.controller.info;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import sss.dot.tourism.dto.info.InfoMessageWrapper;
import sss.dot.tourism.dto.info.LicenseInfoDTO;
import sss.dot.tourism.dto.info.NewsDTO;
import sss.dot.tourism.dto.info.RequestParamDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.service.trader.IMemberLicenseService;

@Controller
@RequestMapping("/news")
public class NewsController {
	
	@Autowired
	IMemberLicenseService memberLicenseService;

	@RequestMapping(value = "/member/{token}", method = RequestMethod.GET)
	public @ResponseBody
	String readNews(@PathVariable String token, ModelMap model) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			
			List list =  this.memberLicenseService.getMemberNews("0535553000179");

			InfoMessageWrapper<LicenseInfoDTO> msg = new InfoMessageWrapper<LicenseInfoDTO>(
					list, "MemberNews", "success");
			msg.setMessage("");
			msg.setUnread(5);

			return jsonmsg.writeInfoMessage(msg);

		} catch (Exception e) {
			e.printStackTrace();

			String status = "fail";
			String message = "ไม่พบข้อมูล";
			List list = new ArrayList();

			InfoMessageWrapper<LicenseInfoDTO> msg = new InfoMessageWrapper<LicenseInfoDTO>(
					list, "news", status);
			msg.setMessage(message);

			return jsonmsg.writeErrorMessage(msg);

		}
	}
	
	@RequestMapping(value="member/detail/{id}", method = RequestMethod.GET)
	public String readMemberNewsDetail(@PathVariable String id, ModelMap model) {
		
		try
		{
//			LicenseDetailDTO dto = new LicenseDetailDTO();
//			dto.setTraderId(new Long(String.valueOf(id)));
//			
//			LicenseDetailDTO licenseDetail = (LicenseDetailDTO)licenseService.getByLicenseId(dto);
			
			NewsDTO dto = (NewsDTO) this.memberLicenseService.getMemberNewsById(Long.valueOf(id));
			
//			NewsDTO dto = new NewsDTO();
//			dto.setId(1);
//			dto.setHeader("ให้มาชำระค่าธรรมเนียม");
//			dto.setDetails("ใบอนุญาตจะครบกำหนดชำระค่าธรรมเนียมวันที่ 15 ตุลาคม 2556");
//			dto.setNews_img("../../../../info/image/id/1");
		
			model.addAttribute("model", dto);
			
			String forwordPage = "/page/NewMember.jsp";
		
			return forwordPage;
		
		} catch (Exception e) {
			e.printStackTrace();
			return "/page/pageerror.jsp";
	
		}



	}
	
	@RequestMapping(value="member/detail/{id}", method = RequestMethod.POST)
	public String readMemberNewsDetailPOST(@PathVariable String id, ModelMap model) {
		
		try
		{

			
			NewsDTO dto = (NewsDTO) this.memberLicenseService.getMemberNewsById(Long.valueOf(id));
		
			model.addAttribute("model", dto);
			
			String forwordPage = "/page/NewMember.jsp";
		
			return forwordPage;
		
		} catch (Exception e) {
			e.printStackTrace();
			return "/page/pageerror.jsp";
	
		}



	}
	
	@RequestMapping(value = "member/web/{token}", method = RequestMethod.GET)
	public String readWebNews(@PathVariable String token, ModelMap model) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			
			List list =  this.memberLicenseService.getMemberNews(token);

			model.addAttribute("list", list);

			return "/page/MemberWebNews.jsp";


		} catch (Exception e) {
			e.printStackTrace();
			return "/page/pageerror.jsp";

		}
	}
	
	@RequestMapping(value="member/web/detail", method = RequestMethod.POST)
	public String readMemberNewsWebDetail(@RequestParam("newsId") String newsId, ModelMap model) {
		
		try
		{
			
			NewsDTO dto = (NewsDTO) this.memberLicenseService.getMemberNewsById(Long.valueOf(newsId));
		
			model.addAttribute("model", dto);
			
			String forwordPage = "/page/MemberWebNewsDetail.jsp";
		
			return forwordPage;
		
		} catch (Exception e) {
			e.printStackTrace();
			return "/page/pageerror.jsp";
	
		}



	}
	
	@RequestMapping(value = "/public", method = RequestMethod.GET)
	public @ResponseBody
	String readPublicNews() {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			List list = new ArrayList();

			NewsDTO dto = new NewsDTO();
			dto.setId(1);
			dto.setHeader("เรื่อง งานเที่ยวทั่วไทยไปทั่วโลก (THAI INTERNATIONAL TRAVEL FAIR) ครั้งที่ 12");
			list.add(dto);
			
			dto = new NewsDTO();
			dto.setId(2);
			dto.setHeader("สำนักทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ จัดสัมมนาสร้างเครือข่ายและให้ความรู้เกี่ยวกับพระราชบัญญัติธุรกิจนำเที่ยวและมัคคุเทศก์ พ.ศ. 2551");
			list.add(dto);

			

			InfoMessageWrapper<LicenseInfoDTO> msg = new InfoMessageWrapper<LicenseInfoDTO>(
					list, "news", "success");
			msg.setMessage("");

			return jsonmsg.writeInfoMessage(msg);

		} catch (Exception e) {
			e.printStackTrace();

			String status = "fail";
			String message = "ไม่พบข้อมูล";
			List list = new ArrayList();

			InfoMessageWrapper<LicenseInfoDTO> msg = new InfoMessageWrapper<LicenseInfoDTO>(
					list, "news", status);
			msg.setMessage(message);

			return jsonmsg.writeErrorMessage(msg);

		}
	}
	
	@RequestMapping(value="public/{id}", method = RequestMethod.GET)
	public String readPublicNewsDetail(@PathVariable String id, ModelMap model) {
		
		try
		{
//			LicenseDetailDTO dto = new LicenseDetailDTO();
//			dto.setTraderId(new Long(String.valueOf(id)));
//			
//			LicenseDetailDTO licenseDetail = (LicenseDetailDTO)licenseService.getByLicenseId(dto);
			
			NewsDTO dto = new NewsDTO();
			dto.setId(1);
			dto.setHeader("สำนักทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ จัดสัมมนาสร้างเครือข่ายและให้ความรู้เกี่ยวกับพระราชบัญญัติธุรกิจนำเที่ยวและมัคคุเทศก์ พ.ศ. 2551");
			dto.setDetails("สำนักทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์ จัดสัมมนาสร้างเครือข่ายและให้ความรู้เกี่ยวกับพระราชบัญญัติธุรกิจนำเที่ยวและมัคคุเทศก์ พ.ศ. 2551 แก่ผู้ประกอบการธุรกิจนำเที่ยวและมัคคุเทศก์ เมื่อวันที่ 8 สิงหาคม 2556 เวลา 08.30-16.30 น. ณ ห้องศรีสุริยวงศ์ A ชั้น 11 โรงแรมตะวันนา สุรวงศ์ กรุงเทพมหานคร โดย นางนิรมล เปลี่ยนจรูญ นักพัฒนาการท่องเที่ยวเชี่ยวชาญ เป็นประธานเปิดการการสัมมนา");
			dto.setNews_img("../../../info/image/id/1");
			model.addAttribute("model", dto);
			
			String forwordPage = "/page/PublicNews.jsp";
			
//			if(TraderType.GUIDE.getStatus().equals(licenseDetail.getTraderType()))
//			{
//				forwordPage = "/page/GuideDetail.jsp";
//			}
//			
//			if(TraderType.LEADER.getStatus().equals(licenseDetail.getTraderType()))
//			{
//				forwordPage = "/page/TourleaderDetail.jsp";
//			}
			
			
			
			return forwordPage;
		
		} catch (Exception e) {
			e.printStackTrace();
			return "/page/pageerror.jsp";
	
		}



	}
}
