package sss.dot.tourism.controller.info;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import sss.dot.tourism.dto.info.LicenseDetailDTO;
import sss.dot.tourism.dto.info.RequestParamDTO;
import sss.dot.tourism.service.info.ILicenseService;

@Controller
@RequestMapping("/license/tour")
public class TourCompaniesLicenseController {
	
	@Autowired ILicenseService licenseService;
	
	@RequestMapping( method=RequestMethod.GET )
	public  String view() {

		try {
			
			return "/page/TourComListView.jsp";

		} catch (Exception e) {
			e.printStackTrace();			
			return "/page/pageerror.jsp";
	
		}
	}
	
	@RequestMapping(value = "/read", method = RequestMethod.POST)
	public String readLicenseBySearchType(
			@RequestParam("searchType") String searchType,
			@RequestParam("searchParam") String searchParam, Model model) {

		try {

			RequestParamDTO param = new RequestParamDTO();
			param.setLanguage("TH");
			param.setSearchParam(searchParam);
			param.setSearchType(searchType);

			List list = licenseService.getAll(param);

			model.addAttribute("list", list);

			return "/page/TourComListView.jsp";

		} catch (Exception e) {
			e.printStackTrace();
			return "/page/pageerror.jsp";

		}
	}
	
	@RequestMapping(value = "/profile", method = RequestMethod.POST)
	public String read(@RequestParam("traderId") String traderId, Model model, HttpServletRequest req, HttpServletResponse res) {

		try {
			
			Iterator headerNames = res.getHeaderNames().iterator();
			while (headerNames.hasNext()) {
				String key = (String) headerNames.next();
				String value = req.getHeader(key);
				System.out.println("key: "+key+" --- value: "+value);
			}
			LicenseDetailDTO dto = new LicenseDetailDTO();
			dto.setTraderId(new Long(String.valueOf(traderId)));

			LicenseDetailDTO licenseDetail = (LicenseDetailDTO) licenseService
					.getByLicenseId(dto);

			model.addAttribute("model", licenseDetail);
			
//			return "/page/TourristDetail.jsp";
			return "/page/TourComProfile.jsp";

		} catch (Exception e) {
			e.printStackTrace();
			return "/page/pageerror.jsp";

		}
	}
}
