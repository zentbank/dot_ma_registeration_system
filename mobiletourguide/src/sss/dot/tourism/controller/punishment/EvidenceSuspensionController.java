package sss.dot.tourism.controller.punishment;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import sss.dot.tourism.dto.punishment.PunishmentEvidenceDTO;
import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.service.punishment.IPunishmentEvidenceService;

import com.sss.aut.service.User;
import com.sss.aut.service.imp.DefaultUser;


@Controller 
@RequestMapping("/punishment/evidence")
public class EvidenceSuspensionController {
	@Autowired
	IPunishmentEvidenceService punishmentEvidenceService;
	
	@Autowired
	private HttpServletRequest httpreq;
	

	
	@RequestMapping(value = "/viewfile/{pid}", method = RequestMethod.GET)
	public void viewfile(HttpServletRequest req, HttpServletResponse resp,@PathVariable("pid") String punishmentId){
		
		try {
			User user = new DefaultUser(0, "punishment", null);
			PunishmentEvidenceDTO param = new PunishmentEvidenceDTO();
			param.setPunishmentId(Long.parseLong(punishmentId));
			PunishmentEvidenceDTO dto = (PunishmentEvidenceDTO)this.punishmentEvidenceService.getById(param, user);
		
			System.out.println(dto.getPunishmentDocPath());
			
			String fileType = dto.getPunishmentDocPath().substring(dto.getPunishmentDocPath().lastIndexOf(".") + 1, dto.getPunishmentDocPath().length());
			
			if(!fileType.equals("pdf"))
			{
				File f = new File(dto.getPunishmentDocPath());
			
				resp.setContentType("image/" + fileType);
				BufferedImage bi = ImageIO.read(f);
				OutputStream out = resp.getOutputStream();
				ImageIO.write(bi, fileType, out);
				out.close();
			}
			else
			{
				File pdfFile = new File(dto.getPunishmentDocPath());
				
				String fileName = dto.getPunishmentDocPath().substring(dto.getPunishmentDocPath().lastIndexOf("/") + 1, dto.getPunishmentDocPath().length());

				resp.setContentType("application/pdf");
				resp.addHeader("Content-Disposition", "attachment; filename=" + fileName);
				resp.setContentLength((int) pdfFile.length());

				FileInputStream fileInputStream = new FileInputStream(pdfFile);
				OutputStream responseOutputStream = resp.getOutputStream();
				int bytes;
				while ((bytes = fileInputStream.read()) != -1) {
					responseOutputStream.write(bytes);
				
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
	
		}
	}
}
