package sss.dot.tourism.controller.renew;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import sss.dot.tourism.controller.report.PDFReport;
import sss.dot.tourism.dto.info.LicenseDetailDTO;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.dto.renew.FileUploadForm;
import sss.dot.tourism.service.info.ILicenseService;
import sss.dot.tourism.service.renew.ILicenseRenewService;

@Controller
@RequestMapping("/renew/license")
public class LicenseRenewController {
	
	private static final Logger logger = Logger.getLogger(LicenseRenewController.class);
	public final String MIME_PDF = "application/pdf";
	
	@Autowired ILicenseService licenseService;
	@Autowired ILicenseRenewService licenseRenewService;
	

	@Autowired
	ServletContext context;

	@RequestMapping(value = "/read", method = RequestMethod.POST)
	public String readLicenseBySearchType(
			@RequestParam("searchType") String searchType,
			@RequestParam("searchParam") String searchParam, Model model) {

		try {
			
			List<LicenseDetailDTO> list = new ArrayList<LicenseDetailDTO>();

			LicenseDetailDTO dto = new LicenseDetailDTO();
			dto.setLicenseNo(searchParam);

			LicenseDetailDTO licenseDetail = (LicenseDetailDTO) licenseService
					.getByLicenseNo(searchParam, null);
			
			list.add(licenseDetail);
			
			model.addAttribute("list", list);
			
			return "/page/TourRenewProfile.jsp";

		} catch (Exception e) {
			e.printStackTrace();
			return "/page/pageerror.jsp";

		}
	}
	
	
	@RequestMapping(value = "/request", method = RequestMethod.POST)
    public String save(
            @ModelAttribute("uploadForm") FileUploadForm uploadForm,
                    Model map) {
		
		try{
	        List<MultipartFile> files = uploadForm.getFiles();
	        
	        if(null != files && files.size() > 0) {
	            
	        	this.licenseRenewService.applyForLicenseRenew(uploadForm);
	        }
		}
		catch(Exception e)
		{
			this.logger.error(e.getMessage());
			e.printStackTrace();
		}
         
        return "/page/TourRenewProfile.jsp";
    }
	
	
	@RequestMapping(value = "/request/{regId}", method = RequestMethod.GET)
    public ModelAndView printFormPayment(@PathVariable String regId , HttpServletRequest request, HttpServletResponse resp) {

		try{
			Map<String, Object> model = new HashMap<String, Object>();
			List render = new ArrayList();
			Map map = new HashMap();
			
			RegistrationDTO dto = new RegistrationDTO();
			dto.setRegId(Long.valueOf(regId));
			model = this.licenseRenewService.getPayments(dto);
			render.add(model);

			String sep = java.io.File.separator; /* sep === "/" */
			List jasperPrintList = new ArrayList();
			
			String jasperFile = sep + "WEB-INF" + sep + "reports" + sep
					+ "requestPayment.jasper";
			
					String jasperName = getReportPath(jasperFile);

					PDFReport report1 = new PDFReport(jasperName, map);
					jasperPrintList.add(report1
							.getJasperPrint(new JRMapCollectionDataSource(render)));

					

					//resp.setContentType("application/msword");
					resp.setContentType(this.MIME_PDF);
					
					
					
					JRPdfExporter exporter = new JRPdfExporter();
					exporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST,
							jasperPrintList);
					exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
							resp.getOutputStream());
					exporter.setParameter(
							JRPdfExporterParameter.IS_CREATING_BATCH_MODE_BOOKMARKS,
							Boolean.TRUE);
					exporter.exportReport();
		}
		catch(Exception e)
		{
			this.logger.error(e.getMessage());
			
//			StringWriter stringWriter = new StringWriter();
//			PrintWriter printWriter = new PrintWriter(stringWriter);
//			e.printStackTrace(printWriter);
//			resp.setContentType("text/plain");
//			resp.getOutputStream().print("");
			e.printStackTrace();
		}
         
        return null;
    }
	
	public String getReportPath(String jasperFile)
	{
		String realPath = this.context.getRealPath(jasperFile);

		System.out.println("******realPath = " + realPath);

		File realFile = new File(realPath);

		String absolutePath = realFile.getAbsolutePath();

		System.out.println("****absolutePath = " + absolutePath);

		return absolutePath;
	}
}
