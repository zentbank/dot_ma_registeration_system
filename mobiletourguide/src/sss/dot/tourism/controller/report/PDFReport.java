package sss.dot.tourism.controller.report;

import java.io.IOException;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

public class PDFReport extends AbstractJasperReport
{
  public PDFReport(String jasperClass, Map param) throws JRException, IOException
  {
    super(jasperClass, param);
  }

  private JasperPrint jPrint = null;

  public JasperPrint getJasperPrint(JRDataSource report) throws JRException
  {
    jPrint = JasperFillManager.fillReport(m_report, m_mapParameters, report);
    return jPrint;
  }

  public int getAmountOfReport()
  {
    return jPrint.getPages().size();
  }
}
