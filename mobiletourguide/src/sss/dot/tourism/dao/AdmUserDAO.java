package sss.dot.tourism.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;

import sss.dot.tourism.domain.AdmGroup;
import sss.dot.tourism.domain.AdmScreen;
import sss.dot.tourism.domain.AdmUser;

@Repository("admUserDAO")
public class AdmUserDAO extends BaseDAO{

	public AdmUserDAO()
	{
		this.domainObj = AdmUser.class;
	}
	
	public List<AdmUser> findUser(String userLogin, String loginPassword) throws Exception
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  AdmUser as dep ");
		hql.append(" where dep.recordStatus = 'N' ");
		
		hql.append(" and dep.userLogin = ? ");
		params.add(userLogin);
		
		hql.append(" and dep.loginPassword = ? ");
		params.add(loginPassword);
	
		
		return (List<AdmUser>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
	
	public List<AdmUser> findOfficerByGroupRole(String groupRole) throws Exception
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" select dep.admUser from  AdmUserGroup as dep ");
		hql.append(" where dep.admGroup.recordStatus = 'N' ");
		hql.append(" and dep.admGroup.officerGroup = 1 ");
		hql.append(" and dep.admGroup.groupRole = ? ");
		params.add(groupRole);
		
				
		return (List<AdmUser>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
	
	
	public List<AdmUser> findTempUserByOrg(long orgId) throws Exception
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  AdmUser as dep ");
		hql.append(" where dep.recordStatus = 'T' ");
		
		hql.append(" and dep.organization.orgId = ? ");
		params.add(orgId);
		
		return (List<AdmUser>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
}
