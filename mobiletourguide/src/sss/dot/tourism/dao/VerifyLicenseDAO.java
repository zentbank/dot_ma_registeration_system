package sss.dot.tourism.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.domain.VerifyLicense;

@Repository("verifyLicenseDAO")
public class VerifyLicenseDAO extends BaseDAO {

	public VerifyLicenseDAO()
	{
		this.domainObj = VerifyLicense.class;
	}
	
	public List<VerifyLicense> findVerifyTrader(long userId, int start, int limit)
	{
		ArrayList params = new ArrayList();
		
		StringBuilder hql = new StringBuilder();
		hql.append(" from  VerifyLicense as tr ");
		hql.append(" where tr.recordStatus = 'N' ");
		hql.append(" and tr.admUser.userId = ? ");
		
		hql.append(" order by tr.verifyDate DESC ");
		
		params.add(userId);
		
		List<VerifyLicense> list = (List<VerifyLicense>) this.findByAddPaging(hql.toString(), start, limit, params.toArray());

		return list;
	}
}
