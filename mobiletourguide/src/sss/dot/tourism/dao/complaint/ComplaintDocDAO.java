package sss.dot.tourism.dao.complaint;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.ComplaintDoc;

@Repository("complaintDocDAO")
public class ComplaintDocDAO extends BaseDAO{
	
	private static final long serialVersionUID = -6455075565457388105L;
	
	public ComplaintDocDAO()
	{
		this.domainObj = ComplaintDoc.class;
	}
	

}
