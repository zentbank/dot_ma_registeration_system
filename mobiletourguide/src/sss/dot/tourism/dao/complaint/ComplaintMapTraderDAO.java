package sss.dot.tourism.dao.complaint;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;

import net.sf.cglib.reflect.FastClass;
import net.sf.cglib.reflect.FastMethod;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.ComplaintMapTrader;

@Repository("complaintMapTraderDAO")
public class ComplaintMapTraderDAO extends BaseDAO{
	
	private static final long serialVersionUID = -6455075565457388105L;
	
	public ComplaintMapTraderDAO()
	{
		this.domainObj = ComplaintMapTrader.class;
	}
	
	public Object insert(Object persistence)
	  {

	    return this.getHibernateTemplate().save(persistence);
	  }
}
