package sss.dot.tourism.dao.complaint;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.ComplaintProgress;
@Repository("complaintProgressDAO")
public class ComplaintProgressDAO extends BaseDAO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6695777565641842534L;

	public ComplaintProgressDAO()
	{
		this.domainObj = ComplaintProgress.class;
	}
}
