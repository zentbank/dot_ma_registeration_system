package sss.dot.tourism.dao.info;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.Trader;
@Repository("traderDAO")
public class TraderDAO extends BaseDAO {

	public TraderDAO()
	{
		this.domainObj = Trader.class;
	}
	
	public List<Object[]> findLicenseByTypeAndParam(String traderType, String param)
	{
		Map<String,Object> params = new HashMap<String,Object>();
	    
	    StringBuilder hql = new StringBuilder();
	    
	    hql.append(" SELECT  ");
	    hql.append(" 	TR.TRADER_NAME, ");
	    hql.append(" 	TR.TRADER_NAME_EN, ");
	    hql.append(" 	TR.LICENSE_NO, ");
	    hql.append(" 	TR.LICENSE_STATUS, ");
	    hql.append(" 	PE.FIRST_NAME, ");
	    hql.append(" 	PE.LAST_NAME, ");
	    hql.append(" 	PE.FIRST_NAME_EN, ");
	    hql.append(" 	PE.LAST_NAME_EN, ");
	    hql.append(" 	PX.PREFIX_NAME, ");
	    hql.append(" 	PX.PREFIX_NAME_EN, ");
	    hql.append(" 	PO.POSTFIX_NAME, ");
	    hql.append(" 	TR.TRADER_ID ");
	    hql.append(" FROM TRADER TR ");
	    hql.append(" INNER JOIN PERSON PE ");
	    hql.append(" ON(TR.PERSON_ID = PE.PERSON_ID) ");
	    hql.append(" INNER JOIN MAS_PREFIX PX ");
	    hql.append(" ON (PX.PREFIX_ID = PE.PREFIX_ID) ");
	    hql.append(" LEFT JOIN MAS_POSFIX PO ");
	    hql.append(" ON(PO.POSTFIX_ID = PE.POSTFIX_ID) ");
	    hql.append(" WHERE TR.RECORD_STATUS = 'N' ");
	    hql.append(" AND TR.LICENSE_NO IS NOT NULL ");
	    hql.append(" AND TR.BRANCH_TYPE IS NULL ");
	    
	    hql.append(" AND TR.TRADER_TYPE= :TRADER_TYPE ");
	    params.put("TRADER_TYPE", traderType);
	    
	    
	    
	    if((null != param) && (!param.isEmpty()))
	    {
		    hql.append(" AND (TR.LICENSE_NO = :LICENSE_NO  ");
		    params.put("LICENSE_NO", param);
		    hql.append(" OR TR.TRADER_NAME LIKE :TRADER_NAME  ");
		    params.put("TRADER_NAME", "%"+param+"%");
		    hql.append(" OR TR.TRADER_NAME_EN LIKE :TRADER_NAME_EN   ");
		    params.put("TRADER_NAME_EN", "%"+param+"%");
		    hql.append(" OR PE.FIRST_NAME LIKE :FIRST_NAME  ");
		    params.put("FIRST_NAME", "%"+param+"%");
		    hql.append(" OR PE.LAST_NAME LIKE :LAST_NAME ) ");
		    params.put("LAST_NAME", "%"+param+"%");
	    }
	    
	    
	    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
	    
	    sqlQuery.setFirstResult(0);
	    sqlQuery.setMaxResults(50);
		
	    
	    sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING); 
	    sqlQuery.addScalar("TRADER_NAME_EN", Hibernate.STRING); 
	    sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING); 
	    sqlQuery.addScalar("LICENSE_STATUS", Hibernate.STRING); 
	    sqlQuery.addScalar("FIRST_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("LAST_NAME", Hibernate.STRING); 
	    sqlQuery.addScalar("FIRST_NAME_EN", Hibernate.STRING); 
	    sqlQuery.addScalar("LAST_NAME_EN", Hibernate.STRING); 
	    sqlQuery.addScalar("PREFIX_NAME", Hibernate.STRING); 
	    sqlQuery.addScalar("PREFIX_NAME_EN", Hibernate.STRING); 
	    sqlQuery.addScalar("POSTFIX_NAME", Hibernate.STRING); 
	    sqlQuery.addScalar("TRADER_ID", Hibernate.LONG); 
	    
	    sqlQuery.setProperties(params);
	    List<Object[]>  result = sqlQuery.list();
  
	    return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Trader> findLicense(String identityNo,
			String licenseNo,
			String firstName, 
			String lastName,
			String traderRecordStatus,
			String personRecordStatus,
			String traderType
			)
			throws Exception {
		
		ArrayList params = new ArrayList();
		
		StringBuilder hql = new StringBuilder();
		hql.append(" select tr from  Trader as tr ");
		hql.append(" inner join tr.person pe ");
		
		hql.append(" where  tr.recordStatus = ? ");
		params.add(traderRecordStatus);
		
//		hql.append(" and pe.recordStatus = ? ");
//		params.add(personRecordStatus);
		
		hql.append(" and tr.branchType is null ");

		if ((identityNo != null) && (!identityNo.equals(""))) {
			hql.append(" and pe.identityNo = ? ");
			params.add(identityNo);
		}
		
		if ((traderType != null) && (!traderType.equals(""))) {
			hql.append(" and tr.traderType = ? ");
			params.add(traderType);
		}
		
		if ((licenseNo != null) && (!licenseNo.equals(""))) {
			hql.append(" and tr.licenseNo = ? ");
			params.add(licenseNo);
		}

		if ((firstName != null) && (!firstName.equals(""))) {
			hql.append(" and pe.firstName like ? ");
			params.add("%"+firstName+"%");
		}
	
		if ((lastName != null) && (!lastName.equals(""))) {
			hql.append(" and pe.lastName like ? ");
			params.add("%"+lastName+"%");
		}
	

		List<Trader> list = (List<Trader>) this
				.getHibernateTemplate().find(hql.toString(), params.toArray());

		return list;
	}
	
	public List<Trader> findTraderByLicenseNo(String licenseNo, String traderType, String recordStatus, long orgId)
	{
		ArrayList params = new ArrayList();
		
		StringBuilder hql = new StringBuilder();
		hql.append(" from  Trader as tr ");
		hql.append(" where tr.branchType is null ");

		if ((licenseNo != null) && (!licenseNo.equals(""))) {
			hql.append(" and tr.licenseNo = ? ");
			params.add(licenseNo);
		}
		
		if ((traderType != null) && (!traderType.equals(""))) {
			hql.append(" and tr.traderType = ? ");
			params.add(traderType);
		}
		
		if ((recordStatus != null) && (!recordStatus.equals(""))) {
			hql.append(" and tr.recordStatus = ? ");
			params.add(recordStatus);
		}
		
		if (orgId > 0) {
			hql.append(" and tr.organization.orgId = ? ");
			params.add(orgId);
		}
		
		List<Trader> list = (List<Trader>) this.getHibernateTemplate().find(hql.toString(), params.toArray());

		return list;
	}
	
	public List<Trader> findVerifyTrader(int start, int limit)
	{
		ArrayList params = new ArrayList();
		
		StringBuilder hql = new StringBuilder();
		hql.append(" from  Trader as tr ");
		hql.append(" where tr.branchType is null ");
		hql.append(" and tr.recordStatus = 'N' ");
//		hql.append(" and tr.traderType = 'G' ");
		
		List<Trader> list = (List<Trader>) this.findByAddPaging(hql.toString(), start, limit);

		return list;
	}
}
