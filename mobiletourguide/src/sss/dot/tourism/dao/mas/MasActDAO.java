package sss.dot.tourism.dao.mas;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.MasAct;
import sss.dot.tourism.dto.punishment.MasActDTO;

@Repository("masActDAO")
public class MasActDAO extends BaseDAO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8247923855591392535L;

	public MasActDAO()
	{
		this.domainObj = MasAct.class;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<MasAct> findByTraderType(String traderType ,String actType)
	  {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  MasAct as dep ");
		hql.append(" where dep.recordStatus = 'N' ");
		
		if((actType != null) && (!(actType.isEmpty())))
		{
			hql.append(" and dep.actType = ? ");
			params.add(actType);
		}
		
		if((traderType != null) && (!(traderType.isEmpty())))
		{
			hql.append(" and dep.traderType = ? ");
			params.add(traderType);
		}
	
		
		return (List<MasAct>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	  }
	
	public List<MasActDTO> findbySuspendIdAndTraderTypr(long suspendId,String traderType)
	{
		Map<String,Object> parames = new HashMap<String,Object>();
		    
	    StringBuilder hql = new StringBuilder();
	    
//	    hql.append(" SELECT  ");
//	    hql.append(" MC.MAS_ACT_ID ");
//	    hql.append(" ,MC.ACT_DESC ");
//	    hql.append(" ,MC.TRADER_TYPE ");
//	    hql.append(" ,SD.SUSPEND_ID ");
//	    hql.append(" FROM SUSPENSION_DETAIL SD ");
//	    hql.append(" RIGHT  JOIN MAS_ACT MC ");
//	    hql.append(" ON(SD.ACT = MC.MAS_ACT_ID) ");
//	    hql.append(" WHERE MC.RECORD_STATUS = 'N' ");
//	    hql.append(" AND MC.ACT_TYPE = 'SUS' ");
//	    
//   	 	hql.append(" AND MC.TRADER_TYPE = :TRADER_TYPE ");
//	    
//		parames.put("TRADER_TYPE", traderType);
//		
//		hql.append("  ");
		hql.append(" SELECT  ");
		hql.append(" MAC.MAS_ACT_ID ");
		hql.append(" ,MAC.ACT_DESC ");
		hql.append(" ,MAC.TRADER_TYPE ");
		hql.append(" ,SEL.SUSPEND_ID  ");
		hql.append(" FROM  ");
		hql.append(" MAS_ACT MAC ");
		hql.append(" LEFT JOIN ( ");
		hql.append(" SELECT  ");
		hql.append(" MC.MAS_ACT_ID ");
		hql.append(" ,MC.ACT_DESC ");
		hql.append(" ,MC.TRADER_TYPE ");
		hql.append(" ,SD.SUSPEND_ID ");
		hql.append(" FROM SUSPENSION_DETAIL SD ");
		hql.append(" RIGHT  JOIN MAS_ACT MC ");
		hql.append(" ON(SD.ACT = MC.MAS_ACT_ID) ");
		hql.append(" WHERE MC.RECORD_STATUS = 'N' ");
//		hql.append(" /* AND MC.TRADER_TYPE = 'B' ");
//		hql.append(" AND MC.ACT_TYPE = 'SUS' */ ");
//		hql.append(" AND SD.SUSPEND_ID = 8 ");
   	 	hql.append(" AND SD.SUSPEND_ID = :SUSPEND_ID ");
		parames.put("SUSPEND_ID", suspendId);
		hql.append(" ) SEL ");
		hql.append(" ON(SEL.MAS_ACT_ID = MAC.MAS_ACT_ID) ");
		hql.append(" WHERE MAC.RECORD_STATUS = 'N' ");
		
   	 	hql.append(" AND MAC.TRADER_TYPE = :TRADER_TYPE ");
		parames.put("TRADER_TYPE", traderType);
		
		hql.append(" AND MAC.ACT_TYPE = 'SUS'  ");
	    
	    
	    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
    
		
	    sqlQuery.addScalar("MAS_ACT_ID", Hibernate.LONG); //0
	    sqlQuery.addScalar("ACT_DESC", Hibernate.STRING); //1
	    sqlQuery.addScalar("TRADER_TYPE", Hibernate.STRING); //2
	    sqlQuery.addScalar("SUSPEND_ID", Hibernate.LONG);//3
	   
	    
	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
	    
	    List<MasActDTO> list = new ArrayList<MasActDTO>();
	    
	    if(!result.isEmpty())
	    {
	    	for(Object[] obj: result)
	    	{
	    		MasActDTO dto = new MasActDTO();
	    		
	    		dto.setMasActId(Long.valueOf(obj[0].toString()));
	    		dto.setActDesc(obj[1]==null?"":obj[1].toString());
	    		dto.setTraderType(obj[2]==null?"":obj[2].toString());
	    		
	    		if(obj[3] !=null)
	    		{
	    			dto.setSuspendId(Long.valueOf(obj[3].toString()));
	    			dto.setActive(true);
	    		}
	    		
	    		
	    		list.add(dto);
	    	}
	    }
  
	    return list;
	}

	public List<MasActDTO> findbyRevokeIdAndTraderType(long revokeId,String traderType)
	{
		Map<String,Object> parames = new HashMap<String,Object>();
		    
	    StringBuilder hql = new StringBuilder();

		hql.append(" SELECT  ");
		hql.append(" MAC.MAS_ACT_ID ");
		hql.append(" ,MAC.ACT_DESC ");
		hql.append(" ,MAC.TRADER_TYPE ");
		hql.append(" ,SEL.REVOKE_ID  ");
		hql.append(" FROM  ");
		hql.append(" MAS_ACT MAC ");
		hql.append(" LEFT JOIN ( ");
		hql.append(" SELECT  ");
		hql.append(" MC.MAS_ACT_ID ");
		hql.append(" ,MC.ACT_DESC ");
		hql.append(" ,MC.TRADER_TYPE ");
		hql.append(" ,SD.REVOKE_ID ");
		hql.append(" FROM REVOKE_DETAIL SD ");
		hql.append(" RIGHT  JOIN MAS_ACT MC ");
		hql.append(" ON(SD.ACT = MC.MAS_ACT_ID) ");
		hql.append(" WHERE MC.RECORD_STATUS = 'N' ");

   	 	hql.append(" AND SD.REVOKE_ID = :REVOKE_ID ");
		parames.put("REVOKE_ID", revokeId);
		hql.append(" ) SEL ");
		hql.append(" ON(SEL.MAS_ACT_ID = MAC.MAS_ACT_ID) ");
		hql.append(" WHERE MAC.RECORD_STATUS = 'N' ");
		
   	 	hql.append(" AND MAC.TRADER_TYPE = :TRADER_TYPE ");
		parames.put("TRADER_TYPE", traderType);
		
		hql.append(" AND MAC.ACT_TYPE = 'REV'  ");
	    
	    
	    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
    
		
	    sqlQuery.addScalar("MAS_ACT_ID", Hibernate.LONG); //0
	    sqlQuery.addScalar("ACT_DESC", Hibernate.STRING); //1
	    sqlQuery.addScalar("TRADER_TYPE", Hibernate.STRING); //2
	    sqlQuery.addScalar("REVOKE_ID", Hibernate.LONG);//3
	   
	    
	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
	    
	    List<MasActDTO> list = new ArrayList<MasActDTO>();
	    
	    if(!result.isEmpty())
	    {
	    	for(Object[] obj: result)
	    	{
	    		MasActDTO dto = new MasActDTO();
	    		
	    		dto.setMasActId(Long.valueOf(obj[0].toString()));
	    		dto.setActDesc(obj[1]==null?"":obj[1].toString());
	    		dto.setTraderType(obj[2]==null?"":obj[2].toString());
	    		
	    		if(obj[3] !=null)
	    		{
	    			dto.setRevokeId(Long.valueOf(obj[3].toString()));
	    			dto.setActive(true);
	    		}
	    		
	    		
	    		list.add(dto);
	    	}
	    }
  
	    return list;
	}
	
	
}
