package sss.dot.tourism.dao.mas;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.MasAmphur;
import sss.dot.tourism.dto.mas.MasAmphurDTO;

@Repository("masAmphurDAO")
public class MasAmphurDAO extends BaseDAO{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2839210315637208624L;

	public MasAmphurDAO()
	{
		this.domainObj = MasAmphur.class;
	}
	
	  @SuppressWarnings({ "unchecked", "rawtypes" })
	public List<MasAmphur> findAll(MasAmphurDTO dto)
	  {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  MasAmphur as dep ");
		hql.append(" where dep.recordStatus = 'N' ");
		
		if(dto.getProvinceId() > 0 )
		{
			hql.append(" and dep.masProvince.provinceId = ? ");
			params.add(dto.getProvinceId());
		}
		
		if((dto.getAmphurName() != null) && (!dto.getAmphurName().isEmpty()))
		{
			hql.append(" and dep.amphurName like ? ");
			params.add("%"+dto.getAmphurName()+"%");
		}
		
		if((dto.getAmphurNameEn() != null) && (!dto.getAmphurNameEn().isEmpty()))
		{
			hql.append(" and dep.amphurNameEn like ? ");
			params.add("%"+dto.getAmphurNameEn()+"%");
		}
		
		hql.append(" order by dep.masProvince.provinceName ");
		
		return (List<MasAmphur>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	  }

}
