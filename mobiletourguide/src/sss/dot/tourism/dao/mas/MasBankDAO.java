package sss.dot.tourism.dao.mas;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.MasBank;


@Repository("masBankDAO")
public class MasBankDAO extends BaseDAO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public MasBankDAO()
	{
		this.domainObj = MasBank.class;
	}
	
	/*@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<MasBank> findAll(MasBankDTO dto)
	  {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  MasBank as dep ");
		hql.append(" where dep.recordStatus = 'N' ");
		
		
		
		if((dto.getAmphurName() != null) && (!dto.getAmphurName().isEmpty()))
		{
			hql.append(" and dep.amphurName like ? ");
			params.add("%"+dto.getAmphurName()+"%");
		}
		
		if((dto.getAmphurNameEn() != null) && (!dto.getAmphurNameEn().isEmpty()))
		{
			hql.append(" and dep.amphurNameEn like ? ");
			params.add("%"+dto.getAmphurNameEn()+"%");
		}
		
		hql.append(" order by dep.masProvince.provinceName ");
		
		return (List<MasBank>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	  }
*/
	
}
