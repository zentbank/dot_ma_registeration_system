package sss.dot.tourism.dao.mas;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.MasComplaintType;

@Repository("complaintTypeDAO")
public class MasComplaintTypeDAO extends BaseDAO {
	
	private static final long serialVersionUID = 1L;


	public MasComplaintTypeDAO()
	{
		this.domainObj = MasComplaintType.class;
	}
}
