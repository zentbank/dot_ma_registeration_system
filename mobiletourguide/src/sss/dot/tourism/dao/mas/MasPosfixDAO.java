package sss.dot.tourism.dao.mas;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.MasPosfix;

@Repository("masPosfixDAO")
public class MasPosfixDAO extends BaseDAO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8206036447659299809L;

	public MasPosfixDAO()
	{
		this.domainObj = MasPosfix.class;
	}
}
