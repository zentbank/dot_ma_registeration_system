package sss.dot.tourism.dao.mas;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.MasPosition;
import sss.dot.tourism.domain.MasUniversity;

@Repository("masPositionDAO")
public class MasPositionDAO extends BaseDAO{
	
	public MasPositionDAO()
	{
		this.domainObj = MasPosition.class;
	}
	
	public List<Object[]> posiTionName(String firstName, String lastName)
	{
		Map<String,Object> parames = new HashMap<String,Object>();
		  StringBuilder hql = new StringBuilder();
		  
		  hql.append(" SELECT ");
		  hql.append(" AU.USER_ID AS USER_ID, ");
		  hql.append(" MP.POS_NAME AS POS_NAME ");
		  hql.append(" FROM ADM_USER AU ");
		  hql.append(" INNER JOIN MAS_POSITION MP ON MP.POS_ID = AU.POS_ID ");
		  hql.append(" WHERE AU.RECORD_STATUS = 'N' ");
		  
		  if(firstName!=null && !firstName.equals(""))
		  {
			  hql.append(" AND AU.USER_NAME =:firstName ");
			  parames.put("firstName", firstName);
		  }
		  
		  if(lastName!=null && !lastName.equals(""))
		  {
			  hql.append(" AND AU.USER_LASTNAME =:lastName ");
			  parames.put("lastName", lastName);
		  }
		  
		  SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
		  
		  sqlQuery.addScalar("USER_ID", Hibernate.LONG);
		  sqlQuery.addScalar("POS_NAME", Hibernate.STRING);
		  
		  sqlQuery.setProperties(parames);
		  List<Object[]>  result = sqlQuery.list();

		  return result; 
	}
}
