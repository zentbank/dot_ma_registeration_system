package sss.dot.tourism.dao.mas;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.Organization;
import sss.dot.tourism.domain.RunningNo;
import sss.dot.tourism.util.RecordStatus;
import sss.dot.tourism.util.RunningCategory;
import sss.dot.tourism.util.TraderType;


@Repository("masRunningNoDAO")
public class MasRunningNoDAO extends BaseDAO{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1820068304953251334L;


	public MasRunningNoDAO()
	{
		this.domainObj = RunningNo.class;
	}
	
	public static void main(String arg[])
	{
		String runNo2Str = String.valueOf(2);
		String no2 = "00000";
		
		no2 = no2.substring(0, ((no2.length()-1) - (runNo2Str.length()-1)));
		
		System.out.println(no2.concat(runNo2Str));
	}
	
	public String getRegistrationNo(long orgId, String traderType, String traderCategory)
	{
		RunningNo masRunningNo = this.findRegistrationNo(orgId ,traderType , traderCategory);
		String registrationNo = "";
		if(!TraderType.LEADER.equals(traderType))
		{
			int runningNo2 = masRunningNo.getRunningNo2() + 1;
			registrationNo = masRunningNo.getRunningPrefix().concat(masRunningNo.getBudgetYear());
			registrationNo = registrationNo.concat(masRunningNo.getRuningSeparater());
			
			String runNo2Str = String.valueOf(runningNo2);
			String no2 = "00000";
			no2 = no2.substring(0, ((no2.length()-1) - (runNo2Str.length()-1)));
			
			registrationNo = registrationNo.concat(no2.concat(runNo2Str));
			masRunningNo.setRunningNo2(runningNo2);
			
			
		}
		else
		{
			
			int runningNo2 = masRunningNo.getRunningNo2() + 1;
			registrationNo = masRunningNo.getRunningNo1().concat(masRunningNo.getRuningSeparater());
			
			String runNo2Str = String.valueOf(runningNo2);
			String no2 = "0000";
			no2 = no2.substring(0, ((no2.length()-1) - (runNo2Str.length()-1)));
			registrationNo = registrationNo.concat(no2.concat(runNo2Str));
			registrationNo = registrationNo.concat("/"+ masRunningNo.getBudgetYear());
			
			masRunningNo.setRunningNo2(runningNo2);
		}
		
		this.update(masRunningNo);
		
		return registrationNo;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private RunningNo findRegistrationNo(long orgId, String traderType, String traderCategory)
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  RunningNo as dep ");
		hql.append(" where dep.runningName = 'REGISTRATION_NO'  ");
		
		
		
		hql.append(" and dep.traderType = ? ");
		params.add(traderType);
		
		hql.append(" and dep.budgetYear = ? ");
		params.add(String.valueOf(getCalendarYear()));
		
		hql.append(" and dep.organization.orgId = ? ");
		params.add(orgId);
		
		if(TraderType.LEADER.equals(traderType))
		{
			hql.append(" and dep.traderCategory = ? ");
			params.add(traderCategory);
		}

		List list =  (List<RunningNo>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
		
		RunningNo runningNo = null;
		if(!list.isEmpty())
		{
		    runningNo = (RunningNo)list.get(0);
			
		}
		else
		{
			
			/*
			 * 
	private Integer runningNo1;

			 * */
			
			runningNo = new RunningNo();
			runningNo.setBudgetYear(String.valueOf(getCalendarYear()));
			runningNo.setTraderType(traderType);
			runningNo.setRunningName("REGISTRATION_NO");
			runningNo.setRecordStatus(RecordStatus.NORMAL.getStatus());
			runningNo.setRunningNo2(0);
			Organization organization = new Organization();
			organization.setOrgId(orgId);
			runningNo.setOrganization(organization);
			
			if(!TraderType.LEADER.equals(traderType))
			{
				runningNo.setRuningSeparater("/");
				if(TraderType.TOUR_COMPANIES.equals(traderType))
				{
					runningNo.setRunningPrefix("ธ");
				}
				else
				{
					runningNo.setRunningPrefix("ม");
				}
				
			}
			else
			{
				runningNo.setRuningSeparater(".");
				runningNo.setTraderCategory(traderCategory);
				//ACT_LEADER
//				String runningCategory = "1";
//				if(TraderCategory.GUIDE_LEADER.getStatus().equals(traderCategory))
//				{
//					runningCategory = "3";
//				}
//				if(TraderCategory.TRAINED_LEADER.getStatus().equals(traderCategory))
//				{ //TRAINED_LEADER
//					runningCategory = "2";
//				}
				String runningCategory =  RunningCategory.getLicenseCategoryNo(traderType, traderCategory);
				
				String runningNo1 = String.valueOf(orgId).concat(runningCategory);
				runningNo.setRunningNo1(runningNo1);
			}
			
			Long runningId = (Long)this.getHibernateTemplate().save(runningNo);
			runningNo.setRunningId(runningId.longValue());
			
		}
		
		return runningNo;
	}
	
	public String getLicenseNo(long orgId, String traderType, String traderCategory)
	{
		RunningNo masRunningNo = this.findLicenseNo(orgId ,traderType , traderCategory);
		String licenseNo = "";
		if(!TraderType.LEADER.equals(traderType))
		{
			licenseNo = masRunningNo.getRunningNo1();
			licenseNo = licenseNo.concat(masRunningNo.getRuningSeparater());
			
			int runningNo2 = masRunningNo.getRunningNo2() + 1;
			String runNo2Str = String.valueOf(runningNo2);
			String no2 = "00000";
			no2 = no2.substring(0, ((no2.length()-1) - (runNo2Str.length()-1)));
			licenseNo = licenseNo.concat(no2.concat(runNo2Str));
			
			masRunningNo.setRunningNo2(runningNo2);
		}
		else
		{
			licenseNo = masRunningNo.getRunningNo1();
			licenseNo = licenseNo.concat(masRunningNo.getRuningSeparater());
			
			int runningNo2 = masRunningNo.getRunningNo2() + 1;
			String runNo2Str = String.valueOf(runningNo2);
			String no2 = "0000";
			no2 = no2.substring(0, ((no2.length()-1) - (runNo2Str.length()-1)));
			licenseNo = licenseNo.concat(no2.concat(runNo2Str));
			
			masRunningNo.setRunningNo2(runningNo2);
		}
		
		this.update(masRunningNo);
		
		return licenseNo;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private RunningNo findLicenseNo(long orgId, String traderType, String traderCategory)
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  RunningNo as dep ");
		hql.append(" where dep.runningName = 'LICENSE_NO'  ");
		
		hql.append(" and dep.traderType = ? ");
		params.add(traderType);
		
//		hql.append(" and dep.budgetYear = ? ");
//		params.add(String.valueOf(getCalendarYear()));
		
		hql.append(" and dep.organization.orgId = ? ");
		params.add(orgId);
	
		hql.append(" and dep.traderCategory = ? ");
		params.add(traderCategory);
		

		List list =  (List<RunningNo>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
		
		RunningNo runningNo = null;
		if(!list.isEmpty())
		{
		    runningNo = (RunningNo)list.get(0);
			
		}
		else
		{				
			runningNo = new RunningNo();
//			runningNo.setBudgetYear(String.valueOf(getCalendarYear()));
			runningNo.setTraderType(traderType);
			runningNo.setRunningName("LICENSE_NO");
			runningNo.setRecordStatus(RecordStatus.NORMAL.getStatus());
			runningNo.setRunningNo2(0);
			Organization organization = new Organization();
			organization.setOrgId(orgId);
			runningNo.setOrganization(organization);
			runningNo.setTraderCategory(traderCategory);
			
			String licenCategory = RunningCategory.getLicenseCategoryNo(traderType, traderCategory);
			String runningNo1 = String.valueOf(orgId).concat(licenCategory);
			runningNo.setRunningNo1(runningNo1);
			
			if(TraderType.TOUR_COMPANIES.equals(traderType))
			{
				runningNo.setRuningSeparater("/");
			}
			if(TraderType.GUIDE.equals(traderType))
			{
				runningNo.setRuningSeparater("-");
			}
			if(TraderType.LEADER.equals(traderType))
			{
				runningNo.setRuningSeparater(".");
			}
			
			Long runningId = (Long)this.getHibernateTemplate().save(runningNo);
			runningNo.setRunningId(runningId.longValue());
			
		}
		
		return runningNo;
	}
	
	public String getTempAccountReceiptNo(long orgId, String traderType)
	{
		RunningNo masRunningNo = this.findAccountReceiptNo(orgId ,traderType);
		
		int runningNo2 = masRunningNo.getRunningNo2() + 1;
		String runNo2Str = String.valueOf(runningNo2);
		String no2 = "00000";
		no2 = no2.substring(0, ((no2.length()-1) - (runNo2Str.length()-1)));
		String licenseNo = no2.concat(runNo2Str);
		licenseNo = licenseNo.concat("/");
		licenseNo = licenseNo.concat(masRunningNo.getBudgetYear());
		
//		masRunningNo.setRunningNo2(runningNo2);
		
//		this.update(masRunningNo);
		
		return licenseNo;
	}
	
	public String getAccountReceiptNo(long orgId, String traderType)
	{
		RunningNo masRunningNo = this.findAccountReceiptNo(orgId ,traderType);
		
		int runningNo2 = masRunningNo.getRunningNo2() + 1;
		String runNo2Str = String.valueOf(runningNo2);
		String no2 = "00000";
		no2 = no2.substring(0, ((no2.length()-1) - (runNo2Str.length()-1)));
		String licenseNo = no2.concat(runNo2Str);
		licenseNo = licenseNo.concat("/");
		licenseNo = licenseNo.concat(masRunningNo.getBudgetYear());
		
		masRunningNo.setRunningNo2(runningNo2);
		
		this.update(masRunningNo);
		
		return licenseNo;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public RunningNo findAccountReceiptNo(long orgId, String traderType)
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  RunningNo as dep ");
		hql.append(" where dep.runningName = 'ACCOUNT_RECEIPT_NO'  ");
		
		hql.append(" and dep.traderType = ? ");
		params.add(traderType);
		
		hql.append(" and dep.budgetYear = ? ");
		params.add(String.valueOf(getBudgetYear()));
		
		hql.append(" and dep.organization.orgId = ? ");
		params.add(orgId);

		List list =  (List<RunningNo>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
		
		RunningNo runningNo = null;
		if(!list.isEmpty())
		{
		    runningNo = (RunningNo)list.get(0);
			
		}
		else
		{				
			runningNo = new RunningNo();
			runningNo.setBudgetYear(String.valueOf(getBudgetYear()));
			runningNo.setTraderType(traderType);
			runningNo.setRunningName("ACCOUNT_RECEIPT_NO");
			runningNo.setRecordStatus(RecordStatus.NORMAL.getStatus());
			runningNo.setRunningNo2(0);
			Organization organization = new Organization();
			organization.setOrgId(orgId);
			runningNo.setOrganization(organization);
			
			Long runningId = (Long)this.getHibernateTemplate().save(runningNo);
			runningNo.setRunningId(runningId.longValue());
			
		}
		
		return runningNo;
	}
	
	public String getTempAccountBookNo(long orgId, String traderType)
	{
		RunningNo masRunningNo = this.findAccountBookNo(orgId ,traderType);
		String licenseNo = masRunningNo.getRunningPrefix();
		
//		int runningNo2 = masRunningNo.getRunningNo2() + 1;
		int runningNo2 = masRunningNo.getRunningNo2();
		String runNo2Str = String.valueOf(runningNo2);
		String no2 = "000";
		no2 = no2.substring(0, ((no2.length()-1) - (runNo2Str.length()-1)));
		licenseNo = licenseNo.concat(no2.concat(runNo2Str));
		licenseNo = licenseNo.concat("/");
		licenseNo = licenseNo.concat(masRunningNo.getBudgetYear());
		
//		masRunningNo.setRunningNo2(runningNo2);
		
		
//		this.update(masRunningNo);
		
		return licenseNo;
	}
	
	public String getAccountBookNo(long orgId, String traderType)
	{
		RunningNo masRunningNo = this.findAccountBookNo(orgId ,traderType);
		String licenseNo = masRunningNo.getRunningPrefix();
		
//		int runningNo2 = masRunningNo.getRunningNo2() + 1;
		int runningNo2 = masRunningNo.getRunningNo2();
		String runNo2Str = String.valueOf(runningNo2);
		String no2 = "000";
		no2 = no2.substring(0, ((no2.length()-1) - (runNo2Str.length()-1)));
		licenseNo = licenseNo.concat(no2.concat(runNo2Str));
		licenseNo = licenseNo.concat("/");
		licenseNo = licenseNo.concat(masRunningNo.getBudgetYear());
		
//		masRunningNo.setRunningNo2(runningNo2);
		
		
//		this.update(masRunningNo);
		
		return licenseNo;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public RunningNo findAccountBookNo(long orgId, String traderType)
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  RunningNo as dep ");
		hql.append(" where dep.runningName = 'ACCOUNT_BOOK_NO'  ");
		
		hql.append(" and dep.traderType = ? ");
		params.add(traderType);
		
		hql.append(" and dep.budgetYear = ? ");
		params.add(String.valueOf(getBudgetYear()));
		
		hql.append(" and dep.organization.orgId = ? ");
		params.add(orgId);

		List list =  (List<RunningNo>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
		
		RunningNo runningNo = null;
		if(!list.isEmpty())
		{
		    runningNo = (RunningNo)list.get(0);
			
		}
		else
		{				
			runningNo = new RunningNo();
			runningNo.setBudgetYear(String.valueOf(getBudgetYear()));
			runningNo.setTraderType(traderType);
			runningNo.setRunningName("ACCOUNT_BOOK_NO");
			runningNo.setRecordStatus(RecordStatus.NORMAL.getStatus());
			runningNo.setRunningNo2(Integer.parseInt(orgId+""));
			Organization organization = new Organization();
			organization.setOrgId(orgId);
			runningNo.setOrganization(organization);
			
			
			if(TraderType.TOUR_COMPANIES.equals(traderType))
			{
				runningNo.setRunningPrefix("ธ ");
			}
			if(TraderType.GUIDE.equals(traderType))
			{
				runningNo.setRunningPrefix("ม ");
			}
			
			
			Long runningId = (Long)this.getHibernateTemplate().save(runningNo);
			runningNo.setRunningId(runningId.longValue());
			
		}
		
		return runningNo;
	}
	
	public String getSuspensionTempNo(long orgId, String traderType)
	{
		RunningNo masRunningNo = this.findSuspensionNo(orgId ,traderType);
		String licenseNo = masRunningNo.getRunningPrefix();
		
		int runningNo2 = masRunningNo.getRunningNo2() + 1;
		String runNo2Str = String.valueOf(runningNo2);
		String no2 = "00000";
		no2 = no2.substring(0, ((no2.length()-1) - (runNo2Str.length()-1)));
		licenseNo = licenseNo.concat(no2.concat(runNo2Str));
		licenseNo = licenseNo.concat("/");
		licenseNo = licenseNo.concat(masRunningNo.getBudgetYear());
		
//		masRunningNo.setRunningNo2(runningNo2);
//		this.update(masRunningNo);
		
		return licenseNo;
	}
	
	public String getSuspensionNo(long orgId, String traderType)
	{
		RunningNo masRunningNo = this.findSuspensionNo(orgId ,traderType);
		String licenseNo = masRunningNo.getRunningPrefix();
		
		int runningNo2 = masRunningNo.getRunningNo2() + 1;
		String runNo2Str = String.valueOf(runningNo2);
		String no2 = "00000";
		no2 = no2.substring(0, ((no2.length()-1) - (runNo2Str.length()-1)));
		licenseNo = licenseNo.concat(no2.concat(runNo2Str));
		licenseNo = licenseNo.concat("/");
		licenseNo = licenseNo.concat(masRunningNo.getBudgetYear());
		
		masRunningNo.setRunningNo2(runningNo2);
		this.update(masRunningNo);
		
		return licenseNo;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public RunningNo findSuspensionNo(long orgId, String traderType)
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  RunningNo as dep ");
		hql.append(" where dep.runningName = 'SUSPENSION_NO'  ");
		
		hql.append(" and dep.traderType = ? ");
		params.add(traderType);
		
		hql.append(" and dep.budgetYear = ? ");
		params.add(String.valueOf(getCalendarYear()));
		
		hql.append(" and dep.organization.orgId = ? ");
		params.add(orgId);

		List list =  (List<RunningNo>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
		
		RunningNo runningNo = null;
		if(!list.isEmpty())
		{
		    runningNo = (RunningNo)list.get(0);
			
		}
		else
		{				
			runningNo = new RunningNo();
			runningNo.setBudgetYear(String.valueOf(getCalendarYear()));
			runningNo.setTraderType(traderType);
			runningNo.setRunningName("SUSPENSION_NO");
			runningNo.setRecordStatus(RecordStatus.NORMAL.getStatus());
			runningNo.setRunningNo2(0);
			Organization organization = new Organization();
			organization.setOrgId(orgId);
			runningNo.setOrganization(organization);
			
			if(TraderType.TOUR_COMPANIES.equals(traderType))
			{
				runningNo.setRunningPrefix("ธ");
			}
			if(TraderType.GUIDE.equals(traderType))
			{
				runningNo.setRunningPrefix("ม");
			}
			if(TraderType.LEADER.equals(traderType))
			{
				runningNo.setRunningPrefix("น");
			}			
			
			Long runningId = (Long)this.getHibernateTemplate().save(runningNo);
			runningNo.setRunningId(runningId.longValue());
			
		}
		
		return runningNo;
	}	
	
	
	public String getDeactivateTempNo(long orgId, String traderType)
	{
		RunningNo masRunningNo = this.findDeactivateNo(orgId ,traderType);
		String licenseNo = masRunningNo.getRunningPrefix();
		
		int runningNo2 = masRunningNo.getRunningNo2() + 1;
		String runNo2Str = String.valueOf(runningNo2);
		String no2 = "00000";
		no2 = no2.substring(0, ((no2.length()-1) - (runNo2Str.length()-1)));
		licenseNo = licenseNo.concat(no2.concat(runNo2Str));
		licenseNo = licenseNo.concat("/");
		licenseNo = licenseNo.concat(masRunningNo.getBudgetYear());
		
//		masRunningNo.setRunningNo2(runningNo2);
//		this.update(masRunningNo);
		
		return licenseNo;
	}
	
	public String getDeactivateNo(long orgId, String traderType)
	{
		RunningNo masRunningNo = this.findDeactivateNo(orgId ,traderType);
		String licenseNo = masRunningNo.getRunningPrefix();
		
		int runningNo2 = masRunningNo.getRunningNo2() + 1;
		String runNo2Str = String.valueOf(runningNo2);
		String no2 = "00000";
		no2 = no2.substring(0, ((no2.length()-1) - (runNo2Str.length()-1)));
		licenseNo = licenseNo.concat(no2.concat(runNo2Str));
		licenseNo = licenseNo.concat("/");
		licenseNo = licenseNo.concat(masRunningNo.getBudgetYear());
		
		masRunningNo.setRunningNo2(runningNo2);
		this.update(masRunningNo);
		
		return licenseNo;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public RunningNo findDeactivateNo(long orgId, String traderType)
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  RunningNo as dep ");
		hql.append(" where dep.runningName = 'DEACTIVATE_NO'  ");
		
		hql.append(" and dep.traderType = ? ");
		params.add(traderType);
		
		hql.append(" and dep.budgetYear = ? ");
		params.add(String.valueOf(getCalendarYear()));
		
		hql.append(" and dep.organization.orgId = ? ");
		params.add(orgId);

		List list =  (List<RunningNo>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
		
		RunningNo runningNo = null;
		if(!list.isEmpty())
		{
		    runningNo = (RunningNo)list.get(0);
			
		}
		else
		{				
			runningNo = new RunningNo();
			runningNo.setBudgetYear(String.valueOf(getCalendarYear()));
			runningNo.setTraderType(traderType);
			runningNo.setRunningName("DEACTIVATE_NO");
			runningNo.setRecordStatus(RecordStatus.NORMAL.getStatus());
			runningNo.setRunningNo2(0);
			Organization organization = new Organization();
			organization.setOrgId(orgId);
			runningNo.setOrganization(organization);
			
			if(TraderType.TOUR_COMPANIES.equals(traderType))
			{
				runningNo.setRunningPrefix("ธ");
			}
			if(TraderType.GUIDE.equals(traderType))
			{
				runningNo.setRunningPrefix("ม");
			}
			if(TraderType.LEADER.equals(traderType))
			{
				runningNo.setRunningPrefix("น");
			}			
			
			Long runningId = (Long)this.getHibernateTemplate().save(runningNo);
			runningNo.setRunningId(runningId.longValue());
			
		}
		
		return runningNo;
	}	
	
	public String getComplaintNo(long orgId, String traderType)
	{
		RunningNo masRunningNo = this.findComplaintNo(orgId ,traderType);
		String licenseNo = masRunningNo.getRunningPrefix();
		
		int runningNo2 = masRunningNo.getRunningNo2() + 1;
		String runNo2Str = String.valueOf(runningNo2);
		String no2 = "00000";
		no2 = no2.substring(0, ((no2.length()-1) - (runNo2Str.length()-1)));
		licenseNo = licenseNo.concat(no2.concat(runNo2Str));
		licenseNo = licenseNo.concat("/");
		licenseNo = licenseNo.concat(masRunningNo.getBudgetYear());
		
		masRunningNo.setRunningNo2(runningNo2);
		this.update(masRunningNo);
		
		return licenseNo;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public RunningNo findComplaintNo(long orgId, String traderType)
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  RunningNo as dep ");
		hql.append(" where dep.runningName = 'COMPLAINT_NO'  ");
		
		hql.append(" and dep.traderType = ? ");
		params.add(traderType);
		
		hql.append(" and dep.budgetYear = ? ");
		params.add(String.valueOf(getCalendarYear()));
		
		if(orgId > 0)
		{
			hql.append(" and dep.organization.orgId = ? ");
			params.add(orgId);
		}
		else
		{
			hql.append(" and dep.organization.orgId is null ");
			
		}
	

		List list =  (List<RunningNo>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
		
		RunningNo runningNo = null;
		if(!list.isEmpty())
		{
		    runningNo = (RunningNo)list.get(0);
			
		}
		else
		{				
			runningNo = new RunningNo();
			runningNo.setBudgetYear(String.valueOf(getCalendarYear()));
			runningNo.setTraderType(traderType);
			runningNo.setRunningName("COMPLAINT_NO");
			runningNo.setRecordStatus(RecordStatus.NORMAL.getStatus());
			runningNo.setRunningNo2(0);
			Organization organization = new Organization();
			organization.setOrgId(orgId);
			runningNo.setOrganization(organization);
			
			if(TraderType.TOUR_COMPANIES.equals(traderType))
			{
				runningNo.setRunningPrefix("ธ");
			}
			if(TraderType.GUIDE.equals(traderType))
			{
				runningNo.setRunningPrefix("ม");
			}
			if(TraderType.LEADER.equals(traderType))
			{
				runningNo.setRunningPrefix("น");
			}			
			
			Long runningId = (Long)this.getHibernateTemplate().save(runningNo);
			runningNo.setRunningId(runningId.longValue());
			
		}
		
		return runningNo;
	}	
	
	public String getRevokeTempNo(long orgId, String traderType)
	{
		RunningNo masRunningNo = this.findRevokeNo(orgId ,traderType);
		String licenseNo = masRunningNo.getRunningPrefix();
		
		int runningNo2 = masRunningNo.getRunningNo2() + 1;
		String runNo2Str = String.valueOf(runningNo2);
		String no2 = "00000";
		no2 = no2.substring(0, ((no2.length()-1) - (runNo2Str.length()-1)));
		licenseNo = licenseNo.concat(no2.concat(runNo2Str));
		licenseNo = licenseNo.concat("/");
		licenseNo = licenseNo.concat(masRunningNo.getBudgetYear());
		
//		masRunningNo.setRunningNo2(runningNo2);
//		this.update(masRunningNo);
		
		return licenseNo;
	}
	
	public String getRevokeNo(long orgId, String traderType)
	{
		RunningNo masRunningNo = this.findRevokeNo(orgId ,traderType);
		String licenseNo = masRunningNo.getRunningPrefix();
		
		int runningNo2 = masRunningNo.getRunningNo2() + 1;
		String runNo2Str = String.valueOf(runningNo2);
		String no2 = "00000";
		no2 = no2.substring(0, ((no2.length()-1) - (runNo2Str.length()-1)));
		licenseNo = licenseNo.concat(no2.concat(runNo2Str));
		licenseNo = licenseNo.concat("/");
		licenseNo = licenseNo.concat(masRunningNo.getBudgetYear());
		
		masRunningNo.setRunningNo2(runningNo2);
		this.update(masRunningNo);
		
		return licenseNo;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public RunningNo findRevokeNo(long orgId, String traderType)
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  RunningNo as dep ");
		hql.append(" where dep.runningName = 'REVOKE_NO'  ");
		
		hql.append(" and dep.traderType = ? ");
		params.add(traderType);
		
		hql.append(" and dep.budgetYear = ? ");
		params.add(String.valueOf(getCalendarYear()));
		
		hql.append(" and dep.organization.orgId = ? ");
		params.add(orgId);

		List list =  (List<RunningNo>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
		
		RunningNo runningNo = null;
		if(!list.isEmpty())
		{
		    runningNo = (RunningNo)list.get(0);
			
		}
		else
		{				
			runningNo = new RunningNo();
			runningNo.setBudgetYear(String.valueOf(getCalendarYear()));
			runningNo.setTraderType(traderType);
			runningNo.setRunningName("REVOKE_NO");
			runningNo.setRecordStatus(RecordStatus.NORMAL.getStatus());
			runningNo.setRunningNo2(0);
			Organization organization = new Organization();
			organization.setOrgId(orgId);
			runningNo.setOrganization(organization);
			
			if(TraderType.TOUR_COMPANIES.equals(traderType))
			{
				runningNo.setRunningPrefix("ธ");
			}
			if(TraderType.GUIDE.equals(traderType))
			{
				runningNo.setRunningPrefix("ม");
			}
			if(TraderType.LEADER.equals(traderType))
			{
				runningNo.setRunningPrefix("น");
			}			
			
			Long runningId = (Long)this.getHibernateTemplate().save(runningNo);
			runningNo.setRunningId(runningId.longValue());
			
		}
		
		return runningNo;
	}
	


	 public int getBudgetYear()
	  {

		  Calendar cal = Calendar.getInstance(Locale.ENGLISH);
		  int month = cal.get(Calendar.MONTH) +1;
		  System.out.println("month = " + month);

		  int year = cal.get(Calendar.YEAR);
		  int bhudaYear = year + 543;
		  if(month > 9)
		  {
			  bhudaYear = bhudaYear + 1;
			  year = year + 1;
		  }
		  
		  return bhudaYear;
	  }
	 
	 public int getCalendarYear()
	  {

		  Calendar cal = Calendar.getInstance(Locale.ENGLISH);
		  int month = cal.get(Calendar.MONTH) +1;
		  System.out.println("month = " + month);

		  int year = cal.get(Calendar.YEAR);
		  int bhudaYear = year + 543;


		  System.out.println("year = " + year);
		  System.out.println("bhudaYear = " + bhudaYear);
		  
		  return bhudaYear;
	  }
}
