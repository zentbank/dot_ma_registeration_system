package sss.dot.tourism.dao.mas;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.MasTambol;
import sss.dot.tourism.dto.mas.MasTambolDTO;

@Repository("masTambolDAO")
public class MasTambolDAO extends BaseDAO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -64535231350347074L;

	public MasTambolDAO()
	{
		this.domainObj = MasTambol.class;
	}
	
	 @SuppressWarnings({ "unchecked", "rawtypes" })
	public List<MasTambol> findAll(MasTambolDTO dto)
	  {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  MasTambol as dep ");
		hql.append(" where dep.recordStatus = 'N' ");
		
		
		if(dto.getAmphurId() > 0 )
		{
			hql.append(" and dep.masAmphur.amphurId = ? ");
			params.add(dto.getAmphurId());
		}
		
		if((dto.getTambolName() != null) && (!dto.getTambolName().isEmpty()))
		{
			hql.append(" and dep.tambolName like ? ");
			params.add("%"+dto.getTambolName()+"%");
		}
		
		if((dto.getTambolNameEn() != null) && (!dto.getTambolNameEn().isEmpty()))
		{
			hql.append(" and dep.tambolNameEn like ? ");
			params.add("%"+dto.getTambolNameEn()+"%");
		}
		
		hql.append(" order by dep.tambolName ");
		
		return (List<MasTambol>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	  }
}
