package sss.dot.tourism.dao.mas;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.Organization;

@Repository("organizationDAO")
public class OrganizationDAO extends BaseDAO{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1134083508458780677L;

	public OrganizationDAO()
	{
		this.domainObj = Organization.class;
	}

}
