package sss.dot.tourism.dao.punishment;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.RevokeDetail;

@Repository("revokeDetailDAO")
public class RevokeDetailDAO extends BaseDAO {

	public RevokeDetailDAO()
	{
		this.domainObj = RevokeDetail.class;
	}

	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<RevokeDetail> findByRevokeLicense(long revokeId)
	  {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  RevokeDetail as dep ");
		hql.append(" where dep.recordStatus = 'N' ");
		
		hql.append(" and dep.revokeLicense.revokeId = ? ");
		params.add(revokeId);
	
		
		return (List<RevokeDetail>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	  }
}
