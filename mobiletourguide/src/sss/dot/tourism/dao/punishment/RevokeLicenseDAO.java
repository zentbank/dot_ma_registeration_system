package sss.dot.tourism.dao.punishment;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.RevokeLicense;
import sss.dot.tourism.dto.punishment.RevokeLicenseDTO;
import sss.dot.tourism.dto.punishment.SuspensionLicenseDTO;

@Repository("revokeLicenseDAO")
public class RevokeLicenseDAO extends BaseDAO {
	
	public RevokeLicenseDAO()
	{
		this.domainObj = RevokeLicense.class;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Object[]> findRevokeLicensePaging(RevokeLicenseDTO param,int start, int limit)
	  {
		   Map<String,Object> parames = new HashMap<String,Object>();
		    
		    StringBuilder hql = new StringBuilder();
	    
		    hql.append(" SELECT ");
		    hql.append("  TR.TRADER_ID ");
		    hql.append(" ,TR.LICENSE_NO ");
		    hql.append(" ,TR.TRADER_NAME ");
		    hql.append(" ,TR.TRADER_TYPE  ");
		    hql.append(" ,PE.PERSON_ID ");
		    hql.append(" ,PX.PREFIX_NAME ");
		    hql.append(" ,PE.FIRST_NAME ");
		    hql.append(" ,PE.LAST_NAME ");
		    hql.append(" ,PO.POSTFIX_NAME ");
		    hql.append(" ,PE.PERSON_TYPE ");
		    hql.append(" ,SL.REVOKE_ID ");
		    hql.append(" ,SL.RECEIVE_DATE ");
		    hql.append(" ,SL.RECEIVE_NO ");
		    hql.append(" ,SL.REVOKE_DATE ");
		    hql.append(" ,SL.CANCEL_DATE ");
		    hql.append(" ,SL.OFFICER_NAME ");
		    hql.append(" ,SL.REVOKE_STATUS ");
		    hql.append(" ,PE.IDENTITY_NO ");
		    hql.append(" ,TR.TRADER_CATEGORY ");
		    hql.append(" ,TR.TRADER_NAME_EN ");
		    hql.append(" ,SL.CANCEL_REMARK ");
		    hql.append(" ,SL.REVOKE_DETAIL ");
		    
		    hql.append(" FROM REVOKE_MAP_TRADER ST ");
		    hql.append(" INNER JOIN REVOKE_LICENSE SL ");
		    hql.append(" ON(SL.REVOKE_ID = ST.REVOKE_ID) ");
		    hql.append(" INNER JOIN TRADER TR ");
		    hql.append(" ON((TR.LICENSE_NO = ST.LICENSE_NO) AND (ST.TRADER_TYPE = TR.TRADER_TYPE)) ");
		    hql.append(" INNER JOIN PERSON PE ");
		    hql.append(" ON(PE.PERSON_ID = TR.PERSON_ID) ");
		    hql.append(" LEFT JOIN MAS_PREFIX PX ");
		    hql.append(" ON(PE.PREFIX_ID = PX.PREFIX_ID) ");
		    hql.append(" LEFT JOIN MAS_POSFIX PO ");
		    hql.append(" ON(PE.POSTFIX_ID = PO.POSTFIX_ID ) ");
		    hql.append(" WHERE TR.RECORD_STATUS = 'N' ");
		    hql.append(" AND TR.BRANCH_TYPE IS NULL ");
		    
		    if((param.getTraderType() != null) && (!param.getTraderType().isEmpty()))
		    {
		    	hql.append("  AND TR.TRADER_TYPE = :TRADER_TYPE ");
		    	parames.put("TRADER_TYPE", param.getTraderType());
		    }
		    
		    if((param.getTraderName() != null) && (!param.getTraderName().isEmpty()))
		    {
		    	hql.append("  AND TR.TRADER_NAME LIKE :TRADER_NAME ");
		    	parames.put("TRADER_NAME", "%"+param.getTraderName()+"%");
		    }
		    
		    if((param.getIdentityNo() != null) && (!param.getIdentityNo().isEmpty()))
		    {
		    	hql.append("  AND PE.IDENTITY_NO = :IDENTITY_NO ");
		    	parames.put("IDENTITY_NO", param.getIdentityNo());
		    }
		    if((param.getFirstName() != null) && (!param.getFirstName().isEmpty()))
		    {
		    	hql.append("  AND PE.FIRST_NAME LIKE :FIRST_NAME ");
		    	parames.put("FIRST_NAME", "%"+param.getFirstName()+"%");
		    }
		    if((param.getLastName() != null) && (!param.getLastName().isEmpty()))
		    {
		    	hql.append("  AND PE.LAST_NAME LIKE :LAST_NAME ");
		    	parames.put("LAST_NAME","%"+param.getLastName()+"%");
		    }		    
		    if((param.getRevokeStatus() != null) && (!param.getRevokeStatus().isEmpty()))
		    {
		    	hql.append("  AND SL.REVOKE_STATUS = :REVOKE_STATUS ");
		    	parames.put("REVOKE_STATUS", param.getRevokeStatus());
		    }		
		    if(param.getOfficerId() > 0)
		    {
		    	hql.append("  AND SL.OFFICER_NAME = :OFFICER_NAME ");
		    	parames.put("OFFICER_NAME", param.getOfficerId());
		    }		    
	
		    if(param.getRevokeId() > 0)
		    {
		    	hql.append("  AND SL.REVOKE_ID = :REVOKE_ID ");
		    	parames.put("REVOKE_ID", param.getRevokeId());
		    }
		    
		    if((param.getLicenseNo() != null) && (!param.getLicenseNo().isEmpty()))
		    {
		    	hql.append("  AND TR.LICENSE_NO = :LICENSE_NO ");
		    	parames.put("LICENSE_NO", param.getLicenseNo());
		    }	
		    
		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
		    sqlQuery.setFirstResult(start);
		    sqlQuery.setMaxResults(limit);
		    
			
		    sqlQuery.addScalar("TRADER_ID", Hibernate.LONG); //0
		    sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING); //1
		    sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING); //2
		    sqlQuery.addScalar("TRADER_TYPE", Hibernate.STRING);//3
		    sqlQuery.addScalar("PERSON_ID", Hibernate.LONG);//4
		    sqlQuery.addScalar("PREFIX_NAME", Hibernate.STRING);//5
		    sqlQuery.addScalar("FIRST_NAME", Hibernate.STRING);//6
		    sqlQuery.addScalar("LAST_NAME", Hibernate.STRING);//7
		    sqlQuery.addScalar("POSTFIX_NAME", Hibernate.STRING);//8
		    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);//9
		    sqlQuery.addScalar("REVOKE_ID", Hibernate.LONG);//10
		    sqlQuery.addScalar("RECEIVE_DATE", Hibernate.DATE);//11
		    sqlQuery.addScalar("RECEIVE_NO", Hibernate.STRING);//12
		    sqlQuery.addScalar("REVOKE_DATE", Hibernate.DATE);//13
		    sqlQuery.addScalar("CANCEL_DATE", Hibernate.DATE);//14
		    sqlQuery.addScalar("OFFICER_NAME", Hibernate.LONG);//15
		    sqlQuery.addScalar("REVOKE_STATUS", Hibernate.STRING);//16
		    sqlQuery.addScalar("IDENTITY_NO", Hibernate.STRING);//17
		    sqlQuery.addScalar("TRADER_CATEGORY", Hibernate.STRING);//18
		    sqlQuery.addScalar("TRADER_NAME_EN", Hibernate.STRING);//19
		    sqlQuery.addScalar("CANCEL_REMARK", Hibernate.STRING);//20	
		    sqlQuery.addScalar("REVOKE_DETAIL", Hibernate.STRING);//21
		
		    
		    sqlQuery.setProperties(parames);
		    List<Object[]>  result = sqlQuery.list();
	  
		    return result;
	  }
}
