package sss.dot.tourism.dao.punishment;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.RevokeMapTrader;
import sss.dot.tourism.domain.SuspensionMapTrader;

@Repository("revokeMapTraderDAO")
public class RevokeMapTraderDAO extends BaseDAO {

	public RevokeMapTraderDAO() {
		this.domainObj = RevokeMapTrader.class;
	}

	public Object insert(Object persistence) {

		return this.getHibernateTemplate().save(persistence);
	}

	public void update(Object persistence) {
		this.getHibernateTemplate().update(persistence);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<RevokeMapTrader> findByRevoleLicense(long revokeId) {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  RevokeMapTrader as dep ");
		hql.append(" where dep.revokeLicense.revokeId = ? ");

		params.add(revokeId);

		return (List<RevokeMapTrader>) this.getHibernateTemplate().find(
				hql.toString(), params.toArray());
	}
}
