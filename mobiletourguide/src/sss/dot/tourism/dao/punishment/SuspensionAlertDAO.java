package sss.dot.tourism.dao.punishment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.SuspensionAlert;
import sss.dot.tourism.domain.SuspensionDetail;
import sss.dot.tourism.dto.punishment.SuspensionAlertDTO;

@Repository("suspensionAlertDAO")
public class SuspensionAlertDAO extends BaseDAO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3563691751534377957L;

	public SuspensionAlertDAO()
	{
		this.domainObj = SuspensionAlert.class;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<SuspensionAlert> findBySuspendLicense(long suspendId)
	  {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  SuspensionAlert as dep ");
		hql.append(" where dep.recordStatus = 'N' ");
		
		hql.append(" and dep.suspensionLicense.suspendId = ? ");
		params.add(suspendId);
	
		
		return (List<SuspensionAlert>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	  }
	
	public List<Object[]> findSuspendDetail(SuspensionAlertDTO param)
	{
		Map<String,Object> parames = new HashMap<String,Object>();
	    
	    StringBuilder hql = new StringBuilder();
	    hql.append(" SELECT TR.TRADER_NAME AS TRADER_NAME, ");
	    hql.append("        TR.LICENSE_NO AS LICENSE_NO, ");
	    hql.append("        TR.TRADER_TYPE AS TRADER_TYPE, ");
	    hql.append("        PE.PERSON_TYPE AS PERSON_TYPE, ");
	    hql.append("        PRE.PREFIX_NAME AS PREFIX_NAME, ");
	    hql.append("        PO.POSTFIX_NAME AS POSTFIX_NAME, ");
	    hql.append("        PE.FIRST_NAME AS FIRST_NAME, ");
	    hql.append("        PE.LAST_NAME AS LAST_NAME, ");
	    hql.append("        MA.ACT_DESC AS ACT_DESC, ");
	    hql.append("        SL.SUSPEND_PERIOD AS SUSPEND_PERIOD, ");
	    hql.append("        SL.SUSPEND_PERIOD_DAY AS SUSPEND_PERIOD_DAY, ");
	    hql.append("        SL.SUSPEND_DETAIL AS SUSPEND_DETAIL, ");
	    hql.append("        SL.SUSPENSION_NO AS SUSPENSION_NO, ");
	    hql.append("        TR.TRADER_CATEGORY AS TRADER_CATEGORY ");

	    hql.append(" FROM SUSPENSION_LICENSE SL ");
	    hql.append(" INNER JOIN SUSPENSION_DETAIL SD ON SD.SUSPEND_ID = SL.SUSPEND_ID ");
	    hql.append(" INNER JOIN SUSPENSION_MAP_TRADER SMT ON SMT.SUSPEND_ID = SD.SUSPEND_ID ");
	    hql.append(" INNER JOIN TRADER TR ON TR.LICENSE_NO = SMT.LICENSE_NO AND TR.TRADER_TYPE = SMT.TRADER_TYPE ");
	    hql.append(" INNER JOIN MAS_ACT MA ON MA.MAS_ACT_ID = SD.ACT ");
	    hql.append(" INNER JOIN PERSON PE ON PE.PERSON_ID = TR.PERSON_ID ");
	    hql.append(" LEFT JOIN MAS_PREFIX PRE ON PRE.PREFIX_ID = PE.PREFIX_ID ");
	    hql.append(" LEFT JOIN MAS_POSFIX PO ON PO.POSTFIX_ID = PE.POSTFIX_ID ");

	    hql.append(" WHERE SL.SUSPEND_STATUS IN ('A','W') ");
	    hql.append(" AND SL.RECORD_STATUS = 'N' ");
	    hql.append(" AND TR.RECORD_STATUS = 'N' ");
	    System.out.println(param.getSuspendId());
	    if(param.getSuspendId() > 0)
	    {
	    	hql.append(" AND SL.SUSPEND_ID = :SUSPEND_ID ");
	    	parames.put("SUSPEND_ID", param.getSuspendId());
	    }

//	    if(param.getTraderId() > 0)
//	    {
//	    	hql.append(" AND TR.TRADER_ID = :TRADER_ID ");
//	   / 	parames.put("TRADER_ID", param.getTraderId() );
//	    }

	    
	    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
	    
	    sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING);
	    sqlQuery.addScalar("TRADER_TYPE", Hibernate.STRING);
	    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);
	    sqlQuery.addScalar("PREFIX_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("POSTFIX_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("FIRST_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("LAST_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("ACT_DESC", Hibernate.STRING);
	    sqlQuery.addScalar("SUSPEND_PERIOD", Hibernate.INTEGER);
	    sqlQuery.addScalar("SUSPEND_PERIOD_DAY", Hibernate.INTEGER);
	    sqlQuery.addScalar("SUSPEND_DETAIL", Hibernate.STRING);
	    sqlQuery.addScalar("SUSPENSION_NO", Hibernate.STRING);
	    sqlQuery.addScalar("TRADER_CATEGORY", Hibernate.STRING);
	    
	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
  
	    return result;
	}
}
