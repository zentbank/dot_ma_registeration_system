package sss.dot.tourism.dao.punishment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.MasAct;
import sss.dot.tourism.domain.SuspensionDetail;
import sss.dot.tourism.domain.SuspensionLicense;
import sss.dot.tourism.dto.punishment.SuspensionLicenseDTO;

@Repository("suspensionLicenseDAO")
public class SuspensionLicenseDAO extends BaseDAO{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5647937760999947941L;

	public SuspensionLicenseDAO()
	{
		this.domainObj = SuspensionLicense.class;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Object[]> findSuspensionLicensePaging(SuspensionLicenseDTO param,int start, int limit)
	  {
		   Map<String,Object> parames = new HashMap<String,Object>();
		    
		    StringBuilder hql = new StringBuilder();
		    
		    hql.append(" SELECT ");
		    hql.append("  TR.TRADER_ID ");
		    hql.append(" ,TR.LICENSE_NO ");
		    hql.append(" ,TR.TRADER_NAME ");
		    hql.append(" ,TR.TRADER_TYPE  ");
		    hql.append(" ,PE.PERSON_ID ");
		    hql.append(" ,PX.PREFIX_NAME ");
		    hql.append(" ,PE.FIRST_NAME ");
		    hql.append(" ,PE.LAST_NAME ");
		    hql.append(" ,PO.POSTFIX_NAME ");
		    hql.append(" ,PE.PERSON_TYPE ");
		    hql.append(" ,SL.SUSPEND_ID ");
		    hql.append(" ,SL.SUSPENSION_DATE ");
		    hql.append(" ,SL.SUSPENSION_NO ");
		    hql.append(" ,SL.SUSPEND_FROM ");
		    hql.append(" ,SL.SUSPEND_TO ");
		    hql.append(" ,SL.OFFICER_NAME ");
		    hql.append(" ,SL.SUSPEND_STATUS ");
		    hql.append(" ,PE.IDENTITY_NO ");
		    hql.append(" ,TR.TRADER_CATEGORY ");
		    hql.append(" ,TR.TRADER_NAME_EN ");
		    hql.append(" ,SL.SUSPEND_PERIOD ");
		    hql.append(" ,SL.SUSPEND_DETAIL ");
		    hql.append(" ,SL.SUSPEND_PERIOD_DAY ");
		    
		    hql.append(" FROM SUSPENSION_MAP_TRADER ST ");
		    hql.append(" INNER JOIN SUSPENSION_LICENSE SL ");
		    hql.append(" ON(SL.SUSPEND_ID = ST.SUSPEND_ID) ");
		    hql.append(" INNER JOIN TRADER TR ");
		    hql.append(" ON((TR.LICENSE_NO = ST.LICENSE_NO) AND (ST.TRADER_TYPE = TR.TRADER_TYPE)) ");
		    hql.append(" INNER JOIN PERSON PE ");
		    hql.append(" ON(PE.PERSON_ID = TR.PERSON_ID) ");
		    hql.append(" LEFT JOIN MAS_PREFIX PX ");
		    hql.append(" ON(PE.PREFIX_ID = PX.PREFIX_ID) ");
		    hql.append(" LEFT JOIN MAS_POSFIX PO ");
		    hql.append(" ON(PE.POSTFIX_ID = PO.POSTFIX_ID ) ");
		    hql.append(" WHERE TR.RECORD_STATUS = 'N' ");
		    hql.append(" AND TR.BRANCH_TYPE IS NULL ");
		    
		    if((param.getTraderType() != null) && (!param.getTraderType().isEmpty()))
		    {
		    	hql.append("  AND TR.TRADER_TYPE = :TRADER_TYPE ");
		    	parames.put("TRADER_TYPE", param.getTraderType());
		    }
		    
		    if((param.getTraderName() != null) && (!param.getTraderName().isEmpty()))
		    {
		    	hql.append("  AND TR.TRADER_NAME LIKE :TRADER_NAME ");
		    	parames.put("TRADER_NAME", "%"+param.getTraderName()+"%");
		    }
		    
		    if((param.getIdentityNo() != null) && (!param.getIdentityNo().isEmpty()))
		    {
		    	hql.append("  AND PE.IDENTITY_NO = :IDENTITY_NO ");
		    	parames.put("IDENTITY_NO", param.getIdentityNo());
		    }
		    if((param.getFirstName() != null) && (!param.getFirstName().isEmpty()))
		    {
		    	hql.append("  AND PE.FIRST_NAME LIKE :FIRST_NAME ");
		    	parames.put("FIRST_NAME", "%"+param.getFirstName()+"%");
		    }
		    if((param.getLastName() != null) && (!param.getLastName().isEmpty()))
		    {
		    	hql.append("  AND PE.LAST_NAME LIKE :LAST_NAME ");
		    	parames.put("LAST_NAME","%"+param.getLastName()+"%");
		    }		    
		    if((param.getSuspendStatus() != null) && (!param.getSuspendStatus().isEmpty()))
		    {
		    	hql.append("  AND SL.SUSPEND_STATUS = :SUSPEND_STATUS ");
		    	parames.put("SUSPEND_STATUS", param.getSuspendStatus());
		    }		
		    if(param.getOfficerId() > 0)
		    {
		    	hql.append("  AND SL.OFFICER_NAME = :OFFICER_NAME ");
		    	parames.put("OFFICER_NAME", param.getOfficerId());
		    }		
		    
		    if(param.getSuspendId() > 0)
		    {
		    	hql.append("  AND SL.SUSPEND_ID = :SUSPEND_ID ");
		    	parames.put("SUSPEND_ID", param.getSuspendId());
		    }
	
		    if((param.getLicenseNo() != null) && (!param.getLicenseNo().isEmpty()))
		    {
		    	hql.append("  AND TR.LICENSE_NO = :LICENSE_NO ");
		    	parames.put("LICENSE_NO", param.getLicenseNo());
		    }	
		    
		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
		    sqlQuery.setFirstResult(start);
		    sqlQuery.setMaxResults(limit);
		    
			
		    sqlQuery.addScalar("TRADER_ID", Hibernate.LONG); //0
		    sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING); //1
		    sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING); //2
		    sqlQuery.addScalar("TRADER_TYPE", Hibernate.STRING);//3
		    sqlQuery.addScalar("PERSON_ID", Hibernate.LONG);//4
		    sqlQuery.addScalar("PREFIX_NAME", Hibernate.STRING);//5
		    sqlQuery.addScalar("FIRST_NAME", Hibernate.STRING);//6
		    sqlQuery.addScalar("LAST_NAME", Hibernate.STRING);//7
		    sqlQuery.addScalar("POSTFIX_NAME", Hibernate.STRING);//8
		    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);//9
		    sqlQuery.addScalar("SUSPEND_ID", Hibernate.LONG);//10
		    sqlQuery.addScalar("SUSPENSION_DATE", Hibernate.DATE);//11
		    sqlQuery.addScalar("SUSPENSION_NO", Hibernate.STRING);//12
		    sqlQuery.addScalar("SUSPEND_FROM", Hibernate.DATE);//13
		    sqlQuery.addScalar("SUSPEND_TO", Hibernate.DATE);//14
		    sqlQuery.addScalar("OFFICER_NAME", Hibernate.LONG);//15
		    sqlQuery.addScalar("SUSPEND_STATUS", Hibernate.STRING);//16
		    sqlQuery.addScalar("IDENTITY_NO", Hibernate.STRING);//17
		    sqlQuery.addScalar("TRADER_CATEGORY", Hibernate.STRING);//18
		    sqlQuery.addScalar("TRADER_NAME_EN", Hibernate.STRING);//19
		    sqlQuery.addScalar("SUSPEND_PERIOD", Hibernate.INTEGER);//20	
		    sqlQuery.addScalar("SUSPEND_DETAIL", Hibernate.STRING);//21
		    sqlQuery.addScalar("SUSPEND_PERIOD_DAY", Hibernate.INTEGER);//22
		    
		   
		   
		    
		    sqlQuery.setProperties(parames);
		    List<Object[]>  result = sqlQuery.list();
	  
		    return result;
	  }
	
	public List<SuspensionLicense> findByLicenseNo(String licenseNo, String traderType, String suspendStatus)
	  {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" select sl from  SuspensionMapTrader as dep ");
		hql.append(" inner join  dep.suspensionLicense sl ");
		hql.append(" where  dep.licenseNo = ? ");
		params.add(licenseNo);
		
		if((suspendStatus != null) && (!suspendStatus.equals("")))
		{
			hql.append(" and sl.suspendStatus = ? ");
			params.add(suspendStatus);
		}
	
		if((traderType != null) && (!traderType.equals("")))
		{
			hql.append(" and dep.traderType = ? ");
			params.add(traderType);
		}
		
		return (List<SuspensionLicense>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	  }
}
