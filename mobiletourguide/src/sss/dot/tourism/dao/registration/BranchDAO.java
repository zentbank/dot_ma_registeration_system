package sss.dot.tourism.dao.registration;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.Committee;
import sss.dot.tourism.domain.Trader;

@Repository("branchDAO")
public class BranchDAO extends BaseDAO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8254259701386998423L;

	public BranchDAO()
	{
		this.domainObj = Trader.class;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Trader> findAllByBranchParent(long branchParentId, String recordStatus)
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  Trader as dep ");
		hql.append(" where dep.recordStatus = ? ");
		hql.append(" and dep.traderByBranchParentId.traderId = ? ");
		hql.append(" and dep.branchType = '1' ");
		
		params.add(recordStatus);
		params.add(branchParentId);
		
		return (List<Trader>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
	
	
}
