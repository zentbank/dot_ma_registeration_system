package sss.dot.tourism.dao.registration;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.Country;
import sss.dot.tourism.dto.mas.CountryDTO;

@Repository("countryDAO")
public class CountryDAO extends BaseDAO{

	public CountryDAO()
	{
		this.domainObj = Country.class;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Country> findAll(CountryDTO dto)
	  {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  Country as dep ");
		hql.append(" where dep.recordStatus = 'N' ");

		if((dto.getCountryName() != null) && (!dto.getCountryName().isEmpty()))
		{
			hql.append(" and dep.countryName like ? ");
			params.add("%"+dto.getCountryName()+"%");
			
			hql.append(" order by dep.countryName ");
		}

		if((dto.getCountryName() != null) && (!dto.getCountryName().isEmpty()))
		{
			hql.append(" and dep.countryNameEn like ? ");
			params.add("%"+dto.getCountryNameEn()+"%");
			
			hql.append(" order by dep.countryNameEn ");
		}
	
		
		return (List<Country>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	  }
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Country findThaiCountry()
	  {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  Country as dep ");
		hql.append(" where dep.recordStatus = 'N' ");
		hql.append(" and dep.countryName = 'ไทย' ");

		
		
		return (Country)this.getHibernateTemplate().find(hql.toString(), params.toArray()).get(0);
	  }
}
