package sss.dot.tourism.dao.registration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.Education;
import sss.dot.tourism.domain.MasDocument;
import sss.dot.tourism.domain.Registration;
import sss.dot.tourism.dto.mas.MasDocumentDTO;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.util.RegistrationType;

import com.sss.aut.service.User;

@Repository("documentRegistrationDAO")
public class DocumentRegistrationDAO extends BaseDAO{
	private static final long serialVersionUID = -6455075565457388105L;
	
	public DocumentRegistrationDAO()
	{
		this.domainObj = Registration.class;
	}
	
	public List<Object[]> findDocumentRegistrationPaging(RegistrationDTO dto, User user ,int start, int limit)throws Exception
	{
		Map<String,Object> parames = new HashMap<String,Object>();
	    
	    StringBuilder hql = new StringBuilder();
	
	    hql.append(" SELECT RE.REG_ID AS REG_ID, ");
	    hql.append(" RE.REGISTRATION_NO AS REGISTRATION_NO, ");
	    hql.append(" RE.REGISTRATION_DATE AS REGISTRATION_DATE, ");
	    hql.append(" RE.REGISTRATION_PROGRESS AS REGISTRATION_PROGRESS, ");
	    hql.append(" RE.REGISTRATION_TYPE AS REGISTRATION_TYPE, ");
	    hql.append(" RE.ORG_ID AS ORG_ID, ");
	    hql.append(" RE.RECORD_STATUS AS RECORD_STATUS, ");
	    hql.append(" TR.TRADER_ID AS TRADER_ID, ");
	    hql.append(" TR.TRADER_TYPE AS TRADER_TYPE, ");
	    hql.append(" TR.TRADER_NAME AS TRADER_NAME, ");
	    hql.append(" TR.TRADER_NAME_EN AS TRADER_NAME_EN, ");
	    hql.append(" TR.TRADER_CATEGORY AS TRADER_CATEGORY, ");
	    hql.append(" PE.PERSON_ID AS PERSON_ID, ");
	    hql.append(" PE.PERSON_TYPE AS PERSON_TYPE, ");
	    hql.append(" MP.PREFIX_NAME AS PREFIX_NAME, ");
	    hql.append(" PE.FIRST_NAME AS FIRST_NAME, ");
	    hql.append(" PE.LAST_NAME AS LAST_NAME, ");
	    hql.append(" POSF.POSTFIX_NAME AS POSTFIX_NAME, ");
	    hql.append(" MP.PREFIX_ID AS PREFIX_ID, ");
	    hql.append(" POSF.POSTFIX_ID AS POSTFIX_ID, ");
	    hql.append(" PEAMP.AMPHUR_ID AS PERSON_AMPHUR_ID, ");
	    hql.append(" TAXPEAMP.AMPHUR_ID AS PERSON_TAXAMPHUR_ID, ");
	    hql.append(" PEPROV.PROVINCE_ID AS PERSON_PROVINCE_ID, ");
	    hql.append(" TAXPEPROV.PROVINCE_ID AS PERSON_TAXPROVINCE_ID, ");
	    
		
	    hql.append(" TR.TRADER_GUIDE_ID AS TRADER_GUIDE_ID, ");
	    hql.append(" TR.PRONUNCIATION_NAME AS PRONUNCIATION_NAME, ");
	    hql.append(" TR.LICENSE_NO AS LICENSE_NO, ");
	    hql.append(" PE.PERSON_TYPE AS PERSON_TYPE, ");
	    hql.append(" PE.IDENTITY_NO AS IDENTITY_NO, ");
	    hql.append(" PE.COMMITTEE_NAME1 AS COMMITTEE_NAME1, ");
	    hql.append(" PE.COMMITTEE_NAME2 AS COMMITTEE_NAME2, ");
	    hql.append(" PE.COMMITTEE_NAME_SIGN AS COMMITTEE_NAME_SIGN, ");
	    hql.append(" PE.PASSPORT_NO AS PASSPORT_NO, ");
	    hql.append(" PE.FIRST_NAME_EN AS FIRST_NAME_EN, ");
	    hql.append(" PE.LAST_NAME_EN AS LAST_NAME_EN, ");
	    hql.append(" PE.GENDER AS GENDER, ");
	    hql.append(" PE.PERSON_NATIONALITY AS PERSON_NATIONALITY, ");
	    hql.append(" PE.BIRTH_DATE AS BIRTH_DATE, ");
	    hql.append(" PE.AGE_YEAR AS AGE_YEAR, ");
	    hql.append(" PE.IDENTITY_NO_EXPIRE AS IDENTITY_NO_EXPIRE, ");
	    hql.append(" PE.CORPORATE_TYPE AS CORPORATE_TYPE, ");
	    hql.append(" PE.TAX_IDENTITY_NO AS TAX_IDENTITY_NO, ");
	    hql.append(" PE.IDENTITY_DATE AS IDENTITY_DATE, ");
	    hql.append(" TR.RECORD_STATUS AS RECORD_STATUS, ");
	    
	    hql.append(" PEAMP.AMPHUR_NAME AS PERSON_AMPHUR_NAME, ");
	    hql.append(" TAXPEAMP.AMPHUR_NAME AS PERSON_TAXAMPHUR_NAME, ");
	    hql.append(" PEPROV.PROVINCE_NAME AS PERSON_PROVINCE_NAME, ");
	    hql.append(" TAXPEPROV.PROVINCE_NAME AS PERSON_TAXPROVINCE_NAME, ");
	    hql.append(" MP.PREFIX_NAME_EN AS PREFIX_NAME_EN ");
	    
	    //APPROVE_DATE
	    hql.append(" ,RE.APPROVE_DATE AS APPROVE_DATE ");

	    hql.append(" FROM REGISTRATION RE ");
	    hql.append(" INNER JOIN TRADER TR ");
	    hql.append(" ON(RE.TRADER_ID = TR.TRADER_ID) ");
	 
	    hql.append(" INNER JOIN PERSON PE ");
	    hql.append(" ON(PE.PERSON_ID = TR.PERSON_ID) ");

	    hql.append(" LEFT JOIN MAS_PREFIX MP ");
	    hql.append(" ON(PE.PREFIX_ID = MP.PREFIX_ID) ");

	    hql.append(" LEFT JOIN MAS_POSFIX POSF ");
	    hql.append(" ON(POSF.POSTFIX_ID = PE.POSTFIX_ID) ");
	    hql.append(" LEFT JOIN MAS_AMPHUR PEAMP ");
	    hql.append(" ON(PEAMP.AMPHUR_ID = PE.AMPHUR_ID) ");

	    hql.append(" LEFT JOIN MAS_AMPHUR TAXPEAMP ");
	    hql.append(" ON(TAXPEAMP.AMPHUR_ID = PE.TAX_AMPHUR_ID) ");

	    hql.append(" LEFT JOIN MAS_PROVINCE PEPROV ");
	    hql.append(" ON(PEPROV.PROVINCE_ID = PE.PROVINCE_ID) ");

	    hql.append(" LEFT JOIN MAS_PROVINCE TAXPEPROV ");
	    hql.append(" ON(TAXPEPROV.PROVINCE_ID = PE.TAX_PROVINCE_ID) ");
	    
	    //Where
	    hql.append(" WHERE  TR.BRANCH_TYPE IS NULL ");
	    
//	    //ถ้าเป็นจดใหม่ ต้องอนุมัติแล้วเท่านั้นถึงจะแนบ file ได้(recordstatus = N เพราะจะมี licenseNo แล้ว)
//	    if(RegistrationType.TOUR_COMPANIES_NEW_REGISTRATION.getStatus().equals(dto.getRegistrationType()))
//	    {
//	    	hql.append(" AND RE.RECORD_STATUS in ('N', 'H') ");
//	    }
//	    else
//	    {
//	    	hql.append(" AND RE.RECORD_STATUS in ('N','T', 'H') ");
//	    	hql.append(" AND RE.REGISTRATION_PROGRESS NOT IN ('KEY','LAW') ");
//	    	 
//	    }
	    hql.append(" AND RE.RECORD_STATUS in ('N','T', 'H') ");
	    
	    
	    //TraderType
	    if((null != dto.getTraderType()) && (!"".equals(dto.getTraderType())))
	    {
	    	 hql.append(" AND TR.TRADER_TYPE = :TRADER_TRADER_TYPE ");
			    
			 parames.put("TRADER_TRADER_TYPE", dto.getTraderType());
	    }
	    
	    //BranchType
//	    hql.append(" AND TR.BRANCH_TYPE IS NULL ");
	    
	    //PERSON_TYPE
	    hql.append(" AND PE.PERSON_TYPE IS NOT NULL ");
	    
	    //OrgId
	    hql.append(" AND RE.ORG_ID = :REGISTRATION_ORG_ID ");
	    parames.put("REGISTRATION_ORG_ID", user.getUserData().getOrgId());
	    
	    //BUSINESS--
	    //IDENTITY_NO
	    if(dto.getIdentityNo() != null && !dto.getIdentityNo().equals(""))
	    {
		    hql.append(" AND PE.IDENTITY_NO LIKE :PERSON_IDENTITY_NO ");
		    parames.put("PERSON_IDENTITY_NO", "%"+dto.getIdentityNo()+"%");
	    }
	    
	    //TRADER_NAME
	    if((null != dto.getTraderName()) && (!"".equals(dto.getTraderName())))
	    {
		    hql.append(" AND TR.TRADER_NAME LIKE :TRADER_TRADER_NAME ");
		    
		    parames.put("TRADER_TRADER_NAME", "%"+dto.getTraderName()+"%");
	    }
	    
	    //TRADER_NAME_EN
	    if((null != dto.getTraderNameEn()) && (!"".equals(dto.getTraderNameEn())))
	    {
		    hql.append(" AND TR.TRADER_NAME_EN LIKE :TRADER_TRADER_NAME_EN ");
		    
		    parames.put("TRADER_TRADER_NAME_EN", "%"+dto.getTraderNameEn()+"%");
	    }
	    
	    //FIRST_NAME
	    if((null != dto.getFirstName()) && (!"".equals(dto.getFirstName())))
	    {
		    hql.append(" AND PE.FIRST_NAME LIKE :PERSON_FIRST_NAME ");
		    
		    parames.put("PERSON_FIRST_NAME", "%"+dto.getFirstName()+"%");
	    }
	    
	    //TRADER_CATEGORY
	    if((null != dto.getTraderCategory()) && (!"".equals(dto.getTraderCategory())))
	    {
	    	 hql.append(" AND TR.TRADER_CATEGORY = :TRADER_TRADER_CATEGORY ");
			    
			 parames.put("TRADER_TRADER_CATEGORY", dto.getTraderCategory());
	    }
	    

	    
	    //licenseNo
	    if((null != dto.getLicenseNo()) && (!"".equals(dto.getLicenseNo())))
	    {
	    	 hql.append(" AND TR.LICENSE_NO = :TRADER_LICENSE_NO ");
			    
			 parames.put("TRADER_LICENSE_NO", dto.getLicenseNo());
	    }
	    
	    //LASTNAME
	    if((null != dto.getLastName()) && (!"".equals(dto.getLastName())))
	    {
		    hql.append(" AND PE.LAST_NAME LIKE :PERSON_LAST_NAME ");
		    
		    parames.put("PERSON_LAST_NAME", "%"+dto.getLastName()+"%");
	    }

	    //registrationType
	    if((null != dto.getRegistrationType()) && (!"".equals(dto.getRegistrationType())))
	    {
	    	hql.append(" AND RE.REGISTRATION_TYPE = :REGISTRATION_REGISTRATION_TYPE ");
		    
		    parames.put("REGISTRATION_REGISTRATION_TYPE", dto.getRegistrationType());
	    }
	    
	    
	    //End Where
	    
	    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
	    sqlQuery.setFirstResult(start);
	    sqlQuery.setMaxResults(limit);
	    
		sqlQuery.addScalar("REG_ID", Hibernate.LONG); //1
		sqlQuery.addScalar("REGISTRATION_NO", Hibernate.STRING); //2
	    sqlQuery.addScalar("REGISTRATION_DATE", Hibernate.DATE); //3
	    sqlQuery.addScalar("REGISTRATION_PROGRESS", Hibernate.STRING); //4
	    sqlQuery.addScalar("REGISTRATION_TYPE", Hibernate.STRING);//5 
	    sqlQuery.addScalar("ORG_ID", Hibernate.LONG);//6
	    sqlQuery.addScalar("RECORD_STATUS", Hibernate.STRING);//7
	    sqlQuery.addScalar("TRADER_ID", Hibernate.LONG); //8
	    sqlQuery.addScalar("TRADER_TYPE", Hibernate.STRING);//9
	    sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING);//10
	    sqlQuery.addScalar("TRADER_NAME_EN", Hibernate.STRING);//11
	    sqlQuery.addScalar("TRADER_CATEGORY", Hibernate.STRING);//12
	    sqlQuery.addScalar("PERSON_ID", Hibernate.LONG);//13
	    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);//14
	    sqlQuery.addScalar("PREFIX_NAME", Hibernate.STRING);//15
	    sqlQuery.addScalar("FIRST_NAME", Hibernate.STRING);//16
	    sqlQuery.addScalar("LAST_NAME", Hibernate.STRING);//17
	    sqlQuery.addScalar("POSTFIX_NAME", Hibernate.STRING);//18
	    sqlQuery.addScalar("PREFIX_ID", Hibernate.LONG);//26
	    sqlQuery.addScalar("POSTFIX_ID", Hibernate.LONG);//27
	    sqlQuery.addScalar("PERSON_AMPHUR_ID", Hibernate.LONG);//28
	    sqlQuery.addScalar("PERSON_TAXAMPHUR_ID", Hibernate.LONG);//29
	    sqlQuery.addScalar("PERSON_PROVINCE_ID", Hibernate.LONG);//30
	    sqlQuery.addScalar("PERSON_TAXPROVINCE_ID", Hibernate.LONG);//31
	    
	    sqlQuery.addScalar("TRADER_GUIDE_ID", Hibernate.LONG);
	    sqlQuery.addScalar("PRONUNCIATION_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING);
	    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);
	    sqlQuery.addScalar("IDENTITY_NO", Hibernate.STRING);
	    sqlQuery.addScalar("COMMITTEE_NAME1", Hibernate.STRING);
	    sqlQuery.addScalar("COMMITTEE_NAME2", Hibernate.STRING);
	    sqlQuery.addScalar("COMMITTEE_NAME_SIGN", Hibernate.STRING);
	    sqlQuery.addScalar("PASSPORT_NO", Hibernate.STRING);
	    sqlQuery.addScalar("FIRST_NAME_EN", Hibernate.STRING);
	    sqlQuery.addScalar("LAST_NAME_EN", Hibernate.STRING);
	    sqlQuery.addScalar("GENDER", Hibernate.STRING);
	    sqlQuery.addScalar("PERSON_NATIONALITY", Hibernate.STRING);
	    sqlQuery.addScalar("BIRTH_DATE", Hibernate.DATE);
	    sqlQuery.addScalar("AGE_YEAR", Hibernate.STRING);
	    sqlQuery.addScalar("IDENTITY_NO_EXPIRE", Hibernate.DATE);
	    sqlQuery.addScalar("CORPORATE_TYPE", Hibernate.STRING);
	    sqlQuery.addScalar("TAX_IDENTITY_NO", Hibernate.STRING);
	    sqlQuery.addScalar("IDENTITY_DATE", Hibernate.DATE);
	    sqlQuery.addScalar("RECORD_STATUS", Hibernate.STRING);
	    
	    sqlQuery.addScalar("PERSON_AMPHUR_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("PERSON_TAXAMPHUR_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("PERSON_PROVINCE_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("PERSON_TAXPROVINCE_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("PREFIX_NAME_EN", Hibernate.STRING);
	    sqlQuery.addScalar("APPROVE_DATE", Hibernate.DATE);
	    

	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
  
	    return result;
	}
	
	public List<MasDocument> findDocument(MasDocumentDTO dto, User user)throws Exception
	{
//		Map<String,Object> parames = new HashMap<String,Object>();
		ArrayList params = new ArrayList();
	    StringBuilder hql = new StringBuilder();
	
	    hql.append(" from  MasDocument as doc ");
		hql.append(" where doc.recordStatus = ? ");
		params.add(dto.getRecordStatus());
		
		hql.append(" and doc.licenseType = ? ");
		params.add(dto.getLicenseType());
		
		hql.append(" and doc.registrationType = ? ");
		params.add(dto.getRegistrationType());
		
		hql.append(" and doc.personType = ? ");
		params.add(dto.getPersonType());
		
		hql.append(" and doc.branchType is null ");
		

	    
	    List<MasDocument> list = (List<MasDocument>) this.getHibernateTemplate().find(hql.toString(), params.toArray());

		return list;
	    

	}
	
	public List<MasDocument> findDocumentBranch(MasDocumentDTO dto, User user)throws Exception
	{
		ArrayList params = new ArrayList();
	    StringBuilder hql = new StringBuilder();
	
	    hql.append(" from  MasDocument as doc ");
		hql.append(" where doc.recordStatus = ? ");
		params.add(dto.getRecordStatus());
		
		hql.append(" and doc.licenseType = ? ");
		params.add(dto.getLicenseType());
		
		hql.append(" and doc.registrationType = ? ");
		params.add(dto.getRegistrationType());
		
		hql.append(" and doc.personType = ? ");
		params.add(dto.getPersonType());
		
		hql.append(" and doc.branchType is not null ");
	    
	    List<MasDocument> list = (List<MasDocument>) this.getHibernateTemplate().find(hql.toString(), params.toArray());

		return list;
	}
	
}













