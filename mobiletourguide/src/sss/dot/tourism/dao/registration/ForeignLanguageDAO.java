package sss.dot.tourism.dao.registration;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.Country;
import sss.dot.tourism.domain.Education;
import sss.dot.tourism.domain.ForeignLanguage;
import sss.dot.tourism.dto.mas.ForeignLanguageDTO;
import sss.dot.tourism.dto.training.EducationDTO;

@Repository("foreignLanguageDAO")
public class ForeignLanguageDAO extends BaseDAO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5961041972954632219L;

	public ForeignLanguageDAO()
	{
		this.domainObj = ForeignLanguage.class;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Country> findAll(ForeignLanguageDTO dto)
	  {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  Country as dep ");
		hql.append(" where dep.recordStatus = 'N' ");
		hql.append(" and dep.language is not null ");
		

		if((dto.getLanguage() != null) && (!dto.getLanguage().isEmpty()))
		{
			hql.append(" and dep.language like ? ");
			params.add("%"+dto.getLanguage()+"%");
		}

		hql.append(" order by dep.language ");
		
		
		return (List<Country>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	  }
	
	@SuppressWarnings("unchecked")
	public List<ForeignLanguage> findForeignLanguageAll(ForeignLanguageDTO dto)
			throws Exception {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  ForeignLanguage as tr ");
		hql.append(" where tr.recordStatus = ? ");
		params.add(dto.getRecordStatus());
		
		if (dto.getPersonId() > 0) {
			hql.append(" and tr.person.personId = ? ");
			params.add(dto.getPersonId());
		}

		List<ForeignLanguage> list = (List<ForeignLanguage>) this
				.getHibernateTemplate().find(hql.toString(), params.toArray());

		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<ForeignLanguage> findForeignLanguageByPerson(long personId, String recordStatus)
			throws Exception {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  ForeignLanguage as tr ");
		hql.append(" where tr.recordStatus = ? ");
		params.add(recordStatus);
		
		hql.append(" and tr.person.personId = ? ");
		params.add(personId);

		List<ForeignLanguage> list = (List<ForeignLanguage>) this
				.getHibernateTemplate().find(hql.toString(), params.toArray());

		return list;
	}
	
	
	
	
	
}
