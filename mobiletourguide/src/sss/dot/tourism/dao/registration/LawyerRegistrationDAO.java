package sss.dot.tourism.dao.registration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import com.sss.aut.service.User;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.Registration;
import sss.dot.tourism.dto.registration.RegistrationDTO;

@Repository("lawyerRegistrationDAO")
public class LawyerRegistrationDAO extends BaseDAO{

	public LawyerRegistrationDAO()
	{
		this.domainObj = Registration.class;
	}
	
	public List<Object[]> findTraderBetweenRegistration(RegistrationDTO dto, User user ,int start, int limit)throws Exception
	{
		   Map<String,Object> parames = new HashMap<String,Object>();
		    
		    StringBuilder hql = new StringBuilder();
		    
		  
		    hql.append(" SELECT RE.REG_ID AS REG_ID, ");
		    hql.append(" TR.TRADER_ID AS TRADER_ID, ");
		    hql.append(" PE.PERSON_ID AS PERSON_ID ");
		    hql.append(" FROM REGISTRATION RE ");
		    hql.append(" INNER JOIN TRADER TR ");
		    hql.append(" ON(RE.TRADER_ID = TR.TRADER_ID) ");
		    hql.append(" INNER JOIN PERSON PE ");
		    hql.append(" ON(PE.PERSON_ID = TR.PERSON_ID) ");
		    
		    hql.append(" WHERE RE.RECORD_STATUS = :REGISTRATION_RECORD_STATUS ");
		    
		    parames.put("REGISTRATION_RECORD_STATUS", dto.getRegRecordStatus());
		    
		    hql.append(" AND TR.RECORD_STATUS = :TRADER_RECORD_STATUS ");
		    
		    parames.put("TRADER_RECORD_STATUS", dto.getTraderRecordStatus());
		    
		    hql.append(" AND RE.ORG_ID = :REGISTRATION_ORG_ID ");
		    
		    parames.put("REGISTRATION_ORG_ID", user.getUserData().getOrgId());
			    
//		    if((null != dto.getTraderName()) && (!"".equals(dto.getTraderName())))
//		    {
//			    hql.append(" AND TR.TRADER_NAME LIKE :TRADER_TRADER_NAME ");
//			    
//			    parames.put("TRADER_TRADER_NAME", "%"+dto.getTraderName()+"%");
//		    }
//		    
//		    if((null != dto.getTraderNameEn()) && (!"".equals(dto.getTraderNameEn())))
//		    {
//			    hql.append(" AND TR.TRADER_NAME_EN LIKE :TRADER_TRADER_NAME_EN ");
//			    
//			    parames.put("TRADER_TRADER_NAME_EN", "%"+dto.getTraderNameEn()+"%");
//		    }
//		    
//		    if((null != dto.getFirstName()) && (!"".equals(dto.getFirstName())))
//		    {
//			    hql.append(" AND TR.FIRST_NAME LIKE :TRADER_FIRST_NAME ");
//			    
//			    parames.put("TRADER_FIRST_NAME", "%"+dto.getFirstName()+"%");
//		    }
		    
		    if(dto.getTraderId() > 0)
		    {
		    	 hql.append(" AND TR.TRADER_ID = :TRADER_TRADER_ID ");
				    
				 parames.put("TRADER_TRADER_ID", dto.getTraderId());
		    }
		    if((null != dto.getTraderType()) && (!"".equals(dto.getTraderType())))
		    {
		    	 hql.append(" AND TR.TRADER_TYPE = :TRADER_TRADER_TYPE ");
				    
				 parames.put("TRADER_TRADER_TYPE", dto.getTraderType());
		    }
//		    if((null != dto.getTraderCategory()) && (!"".equals(dto.getTraderCategory())))
//		    {
//		    	 hql.append(" AND TR.TRADER_CATEGORY = :TRADER_TRADER_CATEGORY ");
//				    
//				 parames.put("TRADER_TRADER_CATEGORY", dto.getTraderCategory());
//		    }
		    
		    /*
		    AND RE.REGISTRATION_TYPE = ''
		    AND RE.REGISTRATION_NO = ''
		    AND RE.REGISTRATION_DATE BETWEEN X AND B */
//		    -- ยังไม่ส่งเรื่อง
//		    AND (RE.REGISTRATION_PROGRESS = 'KEY'
//		    -- ส่งเรื่องแล้ว
//		    OR RE.REGISTRATION_PROGRESS <> 'KEY')
//		    -- ตรวจสอบจาก register_progress
//		    ;
		    
		    if((null != dto.getRegistrationType()) && (!"".equals(dto.getRegistrationType())))
		    {
			    hql.append(" AND RE.REGISTRATION_TYPE = :REGISTRATION_REGISTRATION_TYPE ");
			    
			    parames.put("REGISTRATION_REGISTRATION_TYPE", dto.getRegistrationType());
		    }
		    
		    if((null != dto.getRegistrationNo()) && (!"".equals(dto.getRegistrationNo())))
		    {
			    hql.append(" AND RE.REGISTRATION_NO = :REGISTRATION_REGISTRATION_NO ");
			    
			    parames.put("REGISTRATION_REGISTRATION_NO", dto.getRegistrationNo());
		    }
		    if((null != dto.getRegistrationProgress()) && (!"".equals(dto.getRegistrationProgress())))
		    {
			    hql.append(" AND RE.REGISTRATION_PROGRESS = :REGISTRATION_REGISTRATION_PROGRESS ");
			    
			    parames.put("REGISTRATION_REGISTRATION_PROGRESS", dto.getRegistrationProgress());
		    }

		    
		    
		    
		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
		    sqlQuery.setFirstResult(start);
		    sqlQuery.setMaxResults(limit);
			
		    sqlQuery.addScalar("REG_ID", Hibernate.LONG); 
		    sqlQuery.addScalar("TRADER_ID", Hibernate.LONG); 
		    sqlQuery.addScalar("PERSON_ID", Hibernate.LONG); 
		    
		    sqlQuery.setProperties(parames);
		    List<Object[]>  result = sqlQuery.list();
	  
		    return result;
	}
	
	public List<Object[]> findBetweenRegistrationPaging(RegistrationDTO dto, User user ,int start, int limit)throws Exception
	{
		Map<String,Object> parames = new HashMap<String,Object>();
	    
	    StringBuilder hql = new StringBuilder();
		
		hql.append(" SELECT RE.REG_ID AS REG_ID, ");
		hql.append(" RE.REGISTRATION_NO AS REGISTRATION_NO, ");
		hql.append(" RE.REGISTRATION_DATE AS REGISTRATION_DATE, ");
		hql.append(" RE.REGISTRATION_PROGRESS AS REGISTRATION_PROGRESS, ");
		hql.append(" RE.REGISTRATION_TYPE AS REGISTRATION_TYPE, ");
		hql.append(" RE.ORG_ID AS ORG_ID, ");
		hql.append(" RE.RECORD_STATUS AS RECORD_STATUS, ");
		hql.append(" TR.TRADER_ID AS TRADER_ID, ");
		hql.append(" TR.TRADER_TYPE AS TRADER_TYPE, ");
		hql.append(" TR.TRADER_NAME AS TRADER_NAME, ");
		hql.append(" TR.TRADER_NAME_EN AS TRADER_NAME_EN, ");
		hql.append(" TR.TRADER_CATEGORY AS TRADER_CATEGORY, ");
		hql.append(" PE.PERSON_ID AS PERSON_ID, ");
		hql.append(" PE.PERSON_TYPE AS PERSON_TYPE, ");
		hql.append(" MP.PREFIX_NAME AS PREFIX_NAME, ");
		hql.append(" PE.FIRST_NAME AS FIRST_NAME, ");
		hql.append(" PE.LAST_NAME AS LAST_NAME, ");
		hql.append(" POSF.POSTFIX_NAME AS POSTFIX_NAME, ");
		hql.append(" TRD.ADDRESS_NO AS ADDRESS_NO, ");
		hql.append(" TRD.ROAD_NAME AS ROAD_NAME, ");
		hql.append(" TRD.SOI AS SOI, ");
		hql.append("  MTOL.TAMBOL_NAME AS TAMBOL_NAME, ");
		hql.append(" MPH.AMPHUR_NAME AS AMPHUR_NAME, ");
		hql.append(" MPRO.PROVINCE_NAME AS PROVINCE_NAME,  ");
		hql.append(" TRD.POST_CODE AS POST_CODE ");
//		hql.append(" USER_PREF.PREFIX_NAME AS PREFIX_NAME, ");
//		hql.append(" USER1.USER_NAME AS USER_NAME, ");
//		hql.append(" USER1.USER_LASTNAME AS USER_LASTNAME ");

		hql.append(" FROM REGISTRATION RE ");
		hql.append(" INNER JOIN TRADER TR ");
		hql.append(" ON(RE.TRADER_ID = TR.TRADER_ID) ");

		hql.append(" INNER JOIN PERSON PE ");
		hql.append(" ON(PE.PERSON_ID = TR.PERSON_ID) ");

		hql.append(" INNER JOIN MAS_PREFIX MP ");
		hql.append(" ON(PE.PREFIX_ID = MP.PREFIX_ID) ");

		hql.append(" LEFT JOIN MAS_POSFIX POSF ");
		hql.append(" ON(POSF.POSTFIX_ID = PE.POSTFIX_ID) ");

		hql.append(" INNER JOIN TRADER_ADDRESS TRD ");
		hql.append(" ON(TRD.TRADER_ID = TR.TRADER_ID) ");

		hql.append(" INNER JOIN MAS_TAMBOL MTOL ");
		hql.append(" ON(MTOL.TAMBOL_ID = TRD.TAMBOL_ID) ");

		hql.append(" INNER JOIN MAS_AMPHUR MPH ");
		hql.append(" ON(MPH.AMPHUR_ID = TRD.AMPHUR_ID) ");

		hql.append(" INNER JOIN MAS_PROVINCE MPRO ");
		hql.append(" ON(MPRO.PROVINCE_ID = TRD.PROVINCE_ID)  ");
	
//		hql.append(" INNER JOIN REGISTER_PROGRESS RPG ");
//		hql.append(" ON(RPG.REG_ID = RE.REG_ID) ");
//		
//		hql.append(" INNER JOIN ADM_USER USER1 ");
//		hql.append(" ON(USER1.USER_ID = RPG.AUTHORITY) ");
//
//		hql.append(" INNER JOIN MAS_PREFIX USER_PREF ");
//		hql.append(" ON(USER_PREF.PREFIX_ID = USER1.PREFIX_ID) ");


		
		
	    hql.append(" WHERE RE.RECORD_STATUS = :REGISTRATION_RECORD_STATUS ");
	    
	    parames.put("REGISTRATION_RECORD_STATUS", dto.getRegRecordStatus());
	    
	    hql.append(" AND TR.RECORD_STATUS = :TRADER_RECORD_STATUS ");
	    
	    parames.put("TRADER_RECORD_STATUS", dto.getTraderRecordStatus());
	    
	    hql.append(" AND RE.ORG_ID = :REGISTRATION_ORG_ID ");
	    
	    parames.put("REGISTRATION_ORG_ID", user.getUserData().getOrgId());
	    
	    hql.append(" AND TRD.ADDRESS_TYPE = 'S' ");

	    if((null != dto.getTraderName()) && (!"".equals(dto.getTraderName())))
	    {
		    hql.append(" AND TR.TRADER_NAME LIKE :TRADER_TRADER_NAME ");
		    
		    parames.put("TRADER_TRADER_NAME", "%"+dto.getTraderName()+"%");
	    }
	    
	    if((null != dto.getTraderNameEn()) && (!"".equals(dto.getTraderNameEn())))
	    {
		    hql.append(" AND TR.TRADER_NAME_EN LIKE :TRADER_TRADER_NAME_EN ");
		    
		    parames.put("TRADER_TRADER_NAME_EN", "%"+dto.getTraderNameEn()+"%");
	    }
	    
	    if((null != dto.getFirstName()) && (!"".equals(dto.getFirstName())))
	    {
		    hql.append(" AND PE.FIRST_NAME LIKE :TRADER_FIRST_NAME ");
		    
		    parames.put("TRADER_FIRST_NAME", "%"+dto.getFirstName()+"%");
	    }
	    
	    if(dto.getTraderId() > 0)
	    {
	    	 hql.append(" AND TR.TRADER_ID = :TRADER_TRADER_ID ");
			    
			 parames.put("TRADER_TRADER_ID", dto.getTraderId());
	    }
	    if((null != dto.getTraderType()) && (!"".equals(dto.getTraderType())))
	    {
	    	 hql.append(" AND TR.TRADER_TYPE = :TRADER_TRADER_TYPE ");
			    
			 parames.put("TRADER_TRADER_TYPE", dto.getTraderType());
	    }
	    if((null != dto.getTraderCategory()) && (!"".equals(dto.getTraderCategory())))
	    {
	    	 hql.append(" AND TR.TRADER_CATEGORY = :TRADER_TRADER_CATEGORY ");
			    
			 parames.put("TRADER_TRADER_CATEGORY", dto.getTraderCategory());
	    }
		
	    if((null != dto.getRegistrationType()) && (!"".equals(dto.getRegistrationType())))
	    {
		    hql.append(" AND RE.REGISTRATION_TYPE = :REGISTRATION_REGISTRATION_TYPE ");
		    
		    parames.put("REGISTRATION_REGISTRATION_TYPE", dto.getRegistrationType());
	    }
	    
	    if((null != dto.getRegistrationNo()) && (!"".equals(dto.getRegistrationNo())))
	    {
		    hql.append(" AND RE.REGISTRATION_NO = :REGISTRATION_REGISTRATION_NO ");
		    
		    parames.put("REGISTRATION_REGISTRATION_NO", dto.getRegistrationNo());
	    }
		
	    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
	    sqlQuery.setFirstResult(start);
	    sqlQuery.setMaxResults(limit);
	    
		sqlQuery.addScalar("REG_ID", Hibernate.LONG); 
		sqlQuery.addScalar("REGISTRATION_NO", Hibernate.STRING); 
	    sqlQuery.addScalar("REGISTRATION_DATE", Hibernate.DATE); 
	    sqlQuery.addScalar("REGISTRATION_PROGRESS", Hibernate.STRING); 
	    sqlQuery.addScalar("REGISTRATION_TYPE", Hibernate.STRING); 
	    sqlQuery.addScalar("ORG_ID", Hibernate.LONG);
	    sqlQuery.addScalar("RECORD_STATUS", Hibernate.STRING);
	    sqlQuery.addScalar("TRADER_ID", Hibernate.LONG); 
	    sqlQuery.addScalar("TRADER_TYPE", Hibernate.STRING);
	    sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("TRADER_NAME_EN", Hibernate.STRING);
	    sqlQuery.addScalar("TRADER_CATEGORY", Hibernate.STRING);
	    sqlQuery.addScalar("PERSON_ID", Hibernate.LONG);
	    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);
	    sqlQuery.addScalar("PREFIX_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("FIRST_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("LAST_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("POSTFIX_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("ADDRESS_NO", Hibernate.STRING);
	    sqlQuery.addScalar("ROAD_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("SOI", Hibernate.STRING);
	    sqlQuery.addScalar("TAMBOL_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("AMPHUR_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("PROVINCE_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("POST_CODE", Hibernate.STRING);
		


	     
	    



//	    sqlQuery.addScalar("PREFIX_NAME", Hibernate.STRING);
//	    sqlQuery.addScalar("USER_NAME", Hibernate.STRING);
//	    sqlQuery.addScalar("USER_LASTNAME", Hibernate.STRING);
		
	    
	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
  
	    return result;
	}
}
