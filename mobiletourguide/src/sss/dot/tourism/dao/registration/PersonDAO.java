package sss.dot.tourism.dao.registration;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.Person;

@Repository("personDAO")
public class PersonDAO extends BaseDAO{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1610360359920656534L;

	public PersonDAO()
	{
		this.domainObj = Person.class;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Person> findByIdentityNo(String indentityNo , String recordStatus)
	  {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  Person as dep ");
		hql.append(" where dep.indentityNo = ?  ");
		hql.append(" and  dep.recordStatus = ? ");
		
		params.add(indentityNo);
		params.add(recordStatus);
		

		hql.append(" order by dep.indentityNo ");
		
		
		return (List<Person>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	  }
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Person> findByPersonId(long personId)
	  {
		//String recordStatus = "N";
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  Person as dep ");
		hql.append(" where dep.personId = ?  ");
		hql.append(" and  dep.recordStatus in ('N','H') ");
		
		params.add(personId);
		//params.add(recordStatus);

		
		
		return (List<Person>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	  }
	
}
