package sss.dot.tourism.dao.registration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import com.sss.aut.service.User;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.Receipt;
import sss.dot.tourism.dto.registration.ReceiptDTO;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.ProgressStatus;
import sss.dot.tourism.util.TraderType;

@Repository("receiptDAO")
public class ReceiptDAO extends BaseDAO{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5293124659273692259L;

	public ReceiptDAO()
	{
		this.domainObj = Receipt.class;
	}
	
	public List<Object[]> findSummaryReceipt(ReceiptDTO dto) throws Exception
	{
		
Map<String,Object> parames = new HashMap<String,Object>();
	    
	    StringBuilder hql = new StringBuilder();
	   
	    
	    hql.append(" SELECT ");
	    hql.append(" RE.RECEIPT_ID ");
	    hql.append(" ,RE.BOOK_NO ");
	    hql.append(" ,RE.RECEIPT_NO ");
	    hql.append(" ,RE.RECEIPT_NAME ");
	    hql.append("  ");
	    hql.append(" ,TR.TRADER_ID ");
	    hql.append(" ,TR.LICENSE_NO ");
	    hql.append(" ,TR.TRADER_NAME ");
	    hql.append(" ,TR.TRADER_CATEGORY ");
	    hql.append(" ,TR.TRADER_TYPE ");
	    hql.append("  ");
	    hql.append(" ,REG.REG_ID ");
	    hql.append(" ,REG.REGISTRATION_TYPE ");
	    hql.append("  ");
	    hql.append(" ,RDL.FEE_MNY ");
	    hql.append("  ");
	    hql.append(" ,GU.GUARANTEE_MNY ");
	    hql.append(" ,GU.CASH_ACCOUNT_MNY ");
	    hql.append(" ,GU.BANK_GUARANTEE_MNY ");
	    hql.append(" ,GU.GOVERNMENT_BOND_MNY ");
	    hql.append(" ,RE.RECEIVE_OFFICER_DATE ");
	    hql.append(" FROM  RECEIPT RE ");
	    hql.append(" INNER JOIN RECEIPT_MAP_REGISTRATION RM ");
	    hql.append(" ON(RM.RECEIPT_ID = RE.RECEIPT_ID) ");
	    hql.append(" INNER JOIN REGISTRATION REG ");
	    hql.append(" ON(REG.REG_ID = RM.REG_ID) ");
	    hql.append(" INNER JOIN TRADER TR ");
	    hql.append(" ON(TR.TRADER_ID = REG.TRADER_ID) ");
	    hql.append(" INNER JOIN (  ");
	    hql.append(" SELECT DTL.RECEIPT_ID,SUM(DTL.FEE_MNY) AS FEE_MNY FROM RECEIPT_DETAIL DTL ");
	    hql.append(" WHERE DTL.RECORD_STATUS = 'N' ");
	    hql.append(" GROUP BY DTL.RECEIPT_ID ");
	    hql.append(" ) ");
	    hql.append(" RDL ");
	    hql.append(" ON(RDL.RECEIPT_ID = RE.RECEIPT_ID) ");
	    hql.append(" LEFT JOIN GUARANTEE GU ");
	    hql.append(" ON((GU.LICENSE_NO = TR.LICENSE_NO) AND (GU.TRADER_TYPE = TR.TRADER_TYPE)) ");
	    hql.append("  ");
	    hql.append(" WHERE RE.RECEIPT_STATUS = 'P' ");
	    hql.append(" AND TR.RECORD_STATUS = 'N' ");
	    hql.append(" AND REG.RECORD_STATUS = 'N' ");	
	    
	    if((null != dto.getTraderType()) && (dto.getTraderType().equals(TraderType.TOUR_COMPANIES.getStatus())))
	    {
	    	hql.append(" AND GU.GUARANTEE_STATUS = 'N' ");
	    }
	    
	    
	    hql.append(" AND TR.BRANCH_TYPE IS NULL ");
	    hql.append(" AND REG.APPROVE_STATUS = 'A' ");
	    
	   
	    
	    
	    
	   
	 
	    if((null != dto.getReceiveOfficerDateFrom()) && (!dto.getReceiveOfficerDateFrom().equals("")))
	    {
	    	if((null != dto.getReceiveOfficerDateTo()) && (!dto.getReceiveOfficerDateTo().equals("")))
	    	{
				hql.append("  AND ( RE.RECEIVE_OFFICER_DATE >=  convert(datetime, :REC_START_DATE ,20) ");
				hql.append(" AND RE.RECEIVE_OFFICER_DATE <  convert(datetime, :REC_END_DATE ,20) ) ");
				
				String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getReceiveOfficerDateFrom(), "00:00");
				String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getReceiveOfficerDateTo(), "23:59");
					
				parames.put("REC_START_DATE", sendDtmFrom);
				parames.put("REC_END_DATE", sendDtmTo);
	    	}
	    }
	    
	    if((null != dto.getTraderType()) && (!dto.getTraderType().isEmpty()))
	    {
	    	hql.append(" AND TR.TRADER_TYPE = :TRADER_TYPE ");
	    	parames.put("TRADER_TYPE", dto.getTraderType());
	    }
	    
	    if((null != dto.getRegistrationType()) && (!dto.getRegistrationType().isEmpty()))
	    {
	    	hql.append(" AND REG.REGISTRATION_TYPE = :REGISTRATION_TYPE ");
	    	parames.put("REGISTRATION_TYPE", dto.getRegistrationType());
	    }
	    if(dto.getOrgId() > 0)
	    {
	    	 hql.append(" AND REG.ORG_ID = :ORG_ID ");
	    	 parames.put("ORG_ID", dto.getOrgId());
	    }
	    
	    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
	    
	    
//	    RE.RECEIPT_ID
//	    ,RE.BOOK_NO
//	    ,RE.RECEIPT_NO
//	    ,RE.RECEIPT_NAME
//	    ,TR.TRADER_ID
//	    ,TR.LICENSE_NO
//	    ,TR.TRADER_NAME
//	    ,TR.TRADER_CATEGORY
//	    ,TR.TRADER_TYPE
//	    ,REG.REG_ID
//	    ,REG.REGISTRATION_TYPE
//	    ,RDL.FEE_MNY
//	    ,GU.GUARANTEE_MNY
//	    ,GU.CASH_ACCOUNT_MNY
//	    ,GU.BANK_GUARANTEE_MNY
//	    ,GU.GOVERNMENT_BOND_MNY
	    
	    sqlQuery.addScalar("RECEIPT_ID", Hibernate.LONG); //0
		sqlQuery.addScalar("BOOK_NO", Hibernate.STRING); //1
	    sqlQuery.addScalar("RECEIPT_NO", Hibernate.STRING); //2
	    sqlQuery.addScalar("RECEIPT_NAME", Hibernate.STRING); //3
	    
	    sqlQuery.addScalar("TRADER_ID", Hibernate.LONG);//4
	    sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING);//5
	    sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING);//6
	    sqlQuery.addScalar("TRADER_CATEGORY", Hibernate.STRING);//7
	    sqlQuery.addScalar("TRADER_TYPE", Hibernate.STRING);//8
	    
	    sqlQuery.addScalar("REG_ID", Hibernate.STRING);//9
	    sqlQuery.addScalar("REGISTRATION_TYPE", Hibernate.STRING);//10
	    sqlQuery.addScalar("FEE_MNY", Hibernate.BIG_DECIMAL);//11
	    sqlQuery.addScalar("GUARANTEE_MNY", Hibernate.BIG_DECIMAL);//12
	    sqlQuery.addScalar("CASH_ACCOUNT_MNY", Hibernate.BIG_DECIMAL);//13
	    sqlQuery.addScalar("BANK_GUARANTEE_MNY", Hibernate.BIG_DECIMAL);//14
	    sqlQuery.addScalar("GOVERNMENT_BOND_MNY", Hibernate.BIG_DECIMAL);//15
	    
	    sqlQuery.addScalar("RECEIVE_OFFICER_DATE", Hibernate.DATE);//15

	    
	    
	    
	    
	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
  
	    return result;
		
	}
	
	public List<Object[]> findReceiptPaging(RegistrationDTO dto, User user ,int start, int limit)throws Exception
	{
		Map<String,Object> parames = new HashMap<String,Object>();
	    
	    StringBuilder hql = new StringBuilder();
	    
	    hql.append(" 	    SELECT RE.REG_ID AS REG_ID, ");
	    hql.append(" 	    RE.REGISTRATION_NO AS REGISTRATION_NO, ");
	    hql.append(" 	    RE.REGISTRATION_DATE AS REGISTRATION_DATE, ");
	    hql.append(" 	    RE.REGISTRATION_PROGRESS AS REGISTRATION_PROGRESS, ");
	    hql.append(" 	    RE.REGISTRATION_TYPE AS REGISTRATION_TYPE, ");
	    hql.append(" 	    RE.ORG_ID AS ORG_ID, ");
	    hql.append(" 	    RE.RECORD_STATUS AS RECORD_STATUS, ");
	    hql.append(" 	    TR.TRADER_ID AS TRADER_ID, ");
	    hql.append(" 	    TR.TRADER_TYPE AS TRADER_TYPE, ");
	    hql.append(" 	    TR.TRADER_NAME AS TRADER_NAME, ");
	    hql.append(" 	    TR.TRADER_NAME_EN AS TRADER_NAME_EN, ");
	    hql.append(" 	    TR.TRADER_CATEGORY AS TRADER_CATEGORY, ");
	    hql.append(" 	    PE.PERSON_ID AS PERSON_ID, ");
	    hql.append(" 	    PE.PERSON_TYPE AS PERSON_TYPE, ");
	    hql.append(" 	    MP.PREFIX_NAME AS PREFIX_NAME, ");
	    hql.append(" 	    PE.FIRST_NAME AS FIRST_NAME, ");
	    hql.append(" 	    PE.LAST_NAME AS LAST_NAME, ");
	    hql.append(" 	    POSF.POSTFIX_NAME AS POSTFIX_NAME, ");
	    hql.append(" 	    MP.PREFIX_ID AS PREFIX_ID, ");
	    hql.append(" 	    POSF.POSTFIX_ID AS POSTFIX_ID, ");
	    hql.append(" 	    PEAMP.AMPHUR_ID AS PERSON_AMPHUR_ID, ");
	    hql.append(" 	    TAXPEAMP.AMPHUR_ID AS PERSON_TAXAMPHUR_ID, ");
	    hql.append(" 	    PEPROV.PROVINCE_ID AS PERSON_PROVINCE_ID, ");
	    hql.append(" 	    TAXPEPROV.PROVINCE_ID AS PERSON_TAXPROVINCE_ID, ");
	    hql.append(" 	    TR.TRADER_GUIDE_ID AS TRADER_GUIDE_ID, ");
	    hql.append(" 	    TR.PRONUNCIATION_NAME AS PRONUNCIATION_NAME, ");
	    hql.append(" 	    TR.LICENSE_GUIDE_NO AS LICENSE_GUIDE_NO, ");
	    hql.append(" 	    PE.PERSON_TYPE AS PERSON_TYPE, ");
	    hql.append(" 	    PE.IDENTITY_NO AS IDENTITY_NO, ");
	    hql.append(" 	    PE.COMMITTEE_NAME1 AS COMMITTEE_NAME1, ");
	    hql.append(" 	    PE.COMMITTEE_NAME2 AS COMMITTEE_NAME2, ");
	    hql.append(" 	    PE.COMMITTEE_NAME_SIGN AS COMMITTEE_NAME_SIGN, ");
	    hql.append(" 	    PE.PASSPORT_NO AS PASSPORT_NO, ");
	    hql.append(" 	    PE.FIRST_NAME_EN AS FIRST_NAME_EN, ");
	    hql.append(" 	    PE.LAST_NAME_EN AS LAST_NAME_EN, ");
	    hql.append(" 	    PE.GENDER AS GENDER, ");
	    hql.append(" 	    PE.PERSON_NATIONALITY AS PERSON_NATIONALITY, ");
	    hql.append(" 	    PE.BIRTH_DATE AS BIRTH_DATE, ");
	    hql.append(" 	    PE.AGE_YEAR AS AGE_YEAR, ");
	    hql.append(" 	    PE.IDENTITY_NO_EXPIRE AS IDENTITY_NO_EXPIRE, ");
	    hql.append(" 	    PE.CORPORATE_TYPE AS CORPORATE_TYPE, ");
	    hql.append(" 	    PE.TAX_IDENTITY_NO AS TAX_IDENTITY_NO, ");
	    hql.append(" 	    PE.IDENTITY_DATE AS IDENTITY_DATE, ");
	    hql.append(" 	    TR.RECORD_STATUS AS RECORD_STATUS, ");
	    hql.append(" 	    PEAMP.AMPHUR_NAME AS PERSON_AMPHUR_NAME, ");
	    hql.append(" 	    TAXPEAMP.AMPHUR_NAME AS PERSON_TAXAMPHUR_NAME, ");
	    hql.append(" 	    PEPROV.PROVINCE_NAME AS PERSON_PROVINCE_NAME, ");
	    hql.append(" 	    TAXPEPROV.PROVINCE_NAME AS PERSON_TAXPROVINCE_NAME, ");
	    hql.append(" 	    MP.PREFIX_NAME_EN AS PREFIX_NAME_EN, ");
	    hql.append(" 	    TR.LICENSE_NO AS LICENSE_NO, ");
	    hql.append(" 	    REPT.RECEIPT_ID AS RECEIPT_ID, ");
	    hql.append(" 	    REPT.BOOK_NO AS BOOK_NO, ");
	    hql.append(" 	    REPT.RECEIPT_NO AS RECEIPT_NO, ");
	    hql.append(" 	    REPT.RECEIPT_STATUS AS RECEIPT_STATUS ");
	
	    hql.append(" 	    FROM REGISTRATION RE ");

	    hql.append(" 	    INNER JOIN RECEIPT_MAP_REGISTRATION REREG ");
	    hql.append(" 	    ON(REREG.REG_ID = RE.REG_ID) ");

	    hql.append(" 	    INNER JOIN RECEIPT REPT ");
	    hql.append(" 	    ON(REPT.RECEIPT_ID = REREG.RECEIPT_ID) ");

	    hql.append(" 	    INNER JOIN TRADER TR ");
	    hql.append(" 	    ON(RE.TRADER_ID = TR.TRADER_ID) ");

	    hql.append(" 	    INNER JOIN PERSON PE ");
	    hql.append(" 	    ON(PE.PERSON_ID = TR.PERSON_ID) ");

	    hql.append(" 	    INNER JOIN MAS_PREFIX MP ");
	    hql.append(" 	    ON(PE.PREFIX_ID = MP.PREFIX_ID) ");

	    hql.append(" 	    LEFT JOIN MAS_POSFIX POSF ");
	    hql.append(" 	    ON(POSF.POSTFIX_ID = PE.POSTFIX_ID) ");

	    hql.append(" 	    LEFT JOIN MAS_AMPHUR PEAMP ");
	    hql.append(" 	    ON(PEAMP.AMPHUR_ID = PE.AMPHUR_ID) ");

	    hql.append(" 	    LEFT JOIN MAS_AMPHUR TAXPEAMP ");
	    hql.append(" 	    ON(TAXPEAMP.AMPHUR_ID = PE.TAX_AMPHUR_ID) ");

	    hql.append(" 	    LEFT JOIN MAS_PROVINCE PEPROV ");
	    hql.append(" 	    ON(PEPROV.PROVINCE_ID = PE.PROVINCE_ID) ");

	    hql.append(" 	    LEFT JOIN MAS_PROVINCE TAXPEPROV ");
	    hql.append(" 	    ON(TAXPEPROV.PROVINCE_ID = PE.TAX_PROVINCE_ID) ");
		
	    hql.append(" WHERE RE.ORG_ID = :REGISTRATION_ORG_ID ");
	    
	    parames.put("REGISTRATION_ORG_ID", user.getUserData().getOrgId());
	    
//	    hql.append(" AND TRD.ADDRESS_TYPE = 'S' ");
	    hql.append(" AND TR.BRANCH_TYPE IS NULL ");

	    if((null != dto.getTraderName()) && (!"".equals(dto.getTraderName())))
	    {
		    hql.append(" AND TR.TRADER_NAME LIKE :TRADER_TRADER_NAME ");
		    
		    parames.put("TRADER_TRADER_NAME", "%"+dto.getTraderName()+"%");
	    }
	    
	    if((null != dto.getTraderNameEn()) && (!"".equals(dto.getTraderNameEn())))
	    {
		    hql.append(" AND TR.TRADER_NAME_EN LIKE :TRADER_TRADER_NAME_EN ");
		    
		    parames.put("TRADER_TRADER_NAME_EN", "%"+dto.getTraderNameEn()+"%");
	    }
	    
	    if((null != dto.getFirstName()) && (!"".equals(dto.getFirstName())))
	    {
		    hql.append(" AND PE.FIRST_NAME LIKE :TRADER_FIRST_NAME ");
		    
		    parames.put("TRADER_FIRST_NAME", "%"+dto.getFirstName()+"%");
	    }
	    
	    if(dto.getTraderId() > 0)
	    {
	    	 hql.append(" AND TR.TRADER_ID = :TRADER_TRADER_ID ");
			    
			 parames.put("TRADER_TRADER_ID", dto.getTraderId());
	    }
	    if((null != dto.getTraderType()) && (!"".equals(dto.getTraderType())))
	    {
	    	 hql.append(" AND TR.TRADER_TYPE = :TRADER_TRADER_TYPE ");
			    
			 parames.put("TRADER_TRADER_TYPE", dto.getTraderType());
	    }
	    if((null != dto.getTraderCategory()) && (!"".equals(dto.getTraderCategory())))
	    {
	    	 hql.append(" AND TR.TRADER_CATEGORY = :TRADER_TRADER_CATEGORY ");
			    
			 parames.put("TRADER_TRADER_CATEGORY", dto.getTraderCategory());
	    }
		
	    if((null != dto.getRegistrationType()) && (!"".equals(dto.getRegistrationType())))
	    {
		    hql.append(" AND RE.REGISTRATION_TYPE = :REGISTRATION_REGISTRATION_TYPE ");
		    
		    parames.put("REGISTRATION_REGISTRATION_TYPE", dto.getRegistrationType());
	    }
	    
	    if((null != dto.getRegistrationNo()) && (!"".equals(dto.getRegistrationNo())))
	    {
		    hql.append(" AND RE.REGISTRATION_NO = :REGISTRATION_REGISTRATION_NO ");
		    
		    parames.put("REGISTRATION_REGISTRATION_NO", dto.getRegistrationNo());
	    }
	    
	    if((null != dto.getRegistrationDateFrom()) && (!dto.getRegistrationDateFrom().equals("")))
	    {
	    	if((null != dto.getRegistrationDateTo()) && (!dto.getRegistrationDateTo().equals("")))
	    	{
				hql.append("  and ( RE.REGISTRATION_DATE >=  convert(datetime, :REG_START_DATE ,20) ");
				hql.append(" and RE.REGISTRATION_DATE <  convert(datetime, :REG_END_DATE ,20) ) ");
				
				String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getRegistrationDateFrom(), "00:00");
				String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getRegistrationDateTo(), "23:59");
					
				parames.put("REG_START_DATE", sendDtmFrom);
				parames.put("REG_END_DATE", sendDtmTo);
	    	}
	    }
		
	    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
	    sqlQuery.setFirstResult(start);
	    sqlQuery.setMaxResults(limit);
	    
		sqlQuery.addScalar("REG_ID", Hibernate.LONG); //1
		sqlQuery.addScalar("REGISTRATION_NO", Hibernate.STRING); //2
	    sqlQuery.addScalar("REGISTRATION_DATE", Hibernate.DATE); //3
	    sqlQuery.addScalar("REGISTRATION_PROGRESS", Hibernate.STRING); //4
	    sqlQuery.addScalar("REGISTRATION_TYPE", Hibernate.STRING);//5 
	    sqlQuery.addScalar("ORG_ID", Hibernate.LONG);//6
	    sqlQuery.addScalar("RECORD_STATUS", Hibernate.STRING);//7
	    sqlQuery.addScalar("TRADER_ID", Hibernate.LONG); //8
	    sqlQuery.addScalar("TRADER_TYPE", Hibernate.STRING);//9
	    sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING);//10
	    sqlQuery.addScalar("TRADER_NAME_EN", Hibernate.STRING);//11
	    sqlQuery.addScalar("TRADER_CATEGORY", Hibernate.STRING);//12
	    sqlQuery.addScalar("PERSON_ID", Hibernate.LONG);//13
	    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);//14
	    sqlQuery.addScalar("PREFIX_NAME", Hibernate.STRING);//15
	    sqlQuery.addScalar("FIRST_NAME", Hibernate.STRING);//16
	    sqlQuery.addScalar("LAST_NAME", Hibernate.STRING);//17
	    sqlQuery.addScalar("POSTFIX_NAME", Hibernate.STRING);//18

	    sqlQuery.addScalar("PREFIX_ID", Hibernate.LONG);//26
	    sqlQuery.addScalar("POSTFIX_ID", Hibernate.LONG);//27
	    sqlQuery.addScalar("PERSON_AMPHUR_ID", Hibernate.LONG);//28
	    sqlQuery.addScalar("PERSON_TAXAMPHUR_ID", Hibernate.LONG);//29
	    sqlQuery.addScalar("PERSON_PROVINCE_ID", Hibernate.LONG);//30
	    sqlQuery.addScalar("PERSON_TAXPROVINCE_ID", Hibernate.LONG);//31
	    
	    sqlQuery.addScalar("TRADER_GUIDE_ID", Hibernate.LONG);
	    sqlQuery.addScalar("PRONUNCIATION_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("LICENSE_GUIDE_NO", Hibernate.STRING);
	    sqlQuery.addScalar("PERSON_TYPE", Hibernate.STRING);
	    sqlQuery.addScalar("IDENTITY_NO", Hibernate.STRING);
	    sqlQuery.addScalar("COMMITTEE_NAME1", Hibernate.STRING);
	    sqlQuery.addScalar("COMMITTEE_NAME2", Hibernate.STRING);
	    sqlQuery.addScalar("COMMITTEE_NAME_SIGN", Hibernate.STRING);
	    sqlQuery.addScalar("PASSPORT_NO", Hibernate.STRING);
	    sqlQuery.addScalar("FIRST_NAME_EN", Hibernate.STRING);
	    sqlQuery.addScalar("LAST_NAME_EN", Hibernate.STRING);
	    sqlQuery.addScalar("GENDER", Hibernate.STRING);
	    sqlQuery.addScalar("PERSON_NATIONALITY", Hibernate.STRING);
	    sqlQuery.addScalar("BIRTH_DATE", Hibernate.DATE);
	    sqlQuery.addScalar("AGE_YEAR", Hibernate.STRING);
	    sqlQuery.addScalar("IDENTITY_NO_EXPIRE", Hibernate.DATE);
	    sqlQuery.addScalar("CORPORATE_TYPE", Hibernate.STRING);
	    sqlQuery.addScalar("TAX_IDENTITY_NO", Hibernate.STRING);
	    sqlQuery.addScalar("IDENTITY_DATE", Hibernate.DATE);
	    sqlQuery.addScalar("RECORD_STATUS", Hibernate.STRING);
	    
	    sqlQuery.addScalar("PERSON_AMPHUR_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("PERSON_TAXAMPHUR_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("PERSON_PROVINCE_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("PERSON_TAXPROVINCE_NAME", Hibernate.STRING);
	    sqlQuery.addScalar("PREFIX_NAME_EN", Hibernate.STRING);
	    sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING);
	    sqlQuery.addScalar("RECEIPT_ID", Hibernate.LONG);
	    sqlQuery.addScalar("BOOK_NO", Hibernate.STRING);
	    sqlQuery.addScalar("RECEIPT_NO", Hibernate.STRING);
	    sqlQuery.addScalar("RECEIPT_STATUS", Hibernate.STRING);

	    sqlQuery.setProperties(parames);
	    List<Object[]>  result = sqlQuery.list();
  
	    return result;
	}
	
	public List<Object[]> findReceiptFee(ReceiptDTO dto)throws Exception
	{
		 Map<String,Object> parames = new HashMap<String,Object>();
		    
		  StringBuilder hql = new StringBuilder(); 
		  hql.append(" SELECT  RD.FEE AS Fee, ");
		  hql.append(" SUM(RD.FEE_MNY) AS FEE_MNY, ");
		  //hql.append(" FEE.REGISTRATION_TYPE  AS REGISTRATION_TYPE, ");
		  //hql.append(" RD.RECEIPT_ID AS RECEIPT_ID, ");
		  hql.append(" RE.RECEIVE_OFFICER_DATE AS RECEIVE_OFFICER_DATE ");
		  hql.append(" FROM RECEIPT RE ");
          hql.append(" INNER JOIN RECEIPT_DETAIL RD ON RE.RECEIPT_ID = RD.RECEIPT_ID ");
		  hql.append(" INNER JOIN MAS_FEE FEE ON FEE.FEE_ID = RD.FEE "); 
          hql.append(" WHERE RE.RECEIPT_STATUS = 'P' ");
          hql.append(" AND RE.RECORD_STATUS = 'N' ");
		  hql.append(" AND RD.RECORD_STATUS = 'N' ");
		  hql.append(" AND RE.RECEIVE_OFFICER_DATE IS NOT NULL");

		  if((null != dto.getReceiveOfficerDateFrom()) && (!dto.getReceiveOfficerDateFrom().equals("")))
		    {
		    	if((null != dto.getReceiveOfficerDateTo()) && (!dto.getReceiveOfficerDateTo().equals("")))
		    	{
					hql.append("  and ( RE.RECEIVE_OFFICER_DATE >=  convert(datetime, :RE_START_DATE ,20) ");
					hql.append(" and RE.RECEIVE_OFFICER_DATE <=  convert(datetime, :RE_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getReceiveOfficerDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getReceiveOfficerDateTo(), "23:00");
						
					parames.put("RE_START_DATE", sendDtmFrom);
					parames.put("RE_END_DATE", sendDtmTo);
		    	}else
		    	{
		    		hql.append("  and ( RE.RECEIVE_OFFICER_DATE >=  convert(datetime, :RE_START_DATE ,20) ");
					hql.append(" and RE.RECEIVE_OFFICER_DATE <=  convert(datetime, :RE_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getReceiveOfficerDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getReceiveOfficerDateTo(), "23:00");
						
					parames.put("RE_START_DATE", sendDtmFrom);
					parames.put("RE_END_DATE", sendDtmFrom);
		    	}
		    }
		  
		  hql.append(" GROUP BY RD.FEE,RE.RECEIVE_OFFICER_DATE ");
		  hql.append(" ORDER BY RE.RECEIVE_OFFICER_DATE,RD.FEE ASC");
		  
		  SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
			
		  sqlQuery.addScalar("Fee", Hibernate.LONG); 
		  sqlQuery.addScalar("FEE_MNY", Hibernate.BIG_DECIMAL); 
		  //sqlQuery.addScalar("REGISTRATION_TYPE", Hibernate.STRING); 
		  //sqlQuery.addScalar("RECEIPT_ID", Hibernate.LONG);
		  sqlQuery.addScalar("RECEIVE_OFFICER_DATE", Hibernate.DATE);
		  sqlQuery.setProperties(parames);
		  List<Object[]>  result = sqlQuery.list();
	  
		    return result;
	}
	public List<Object[]> findReceiptFeeDateForMonth(ReceiptDTO dto)throws Exception
	{
		 Map<String,Object> parames = new HashMap<String,Object>();
		    
		  StringBuilder hql = new StringBuilder(); 
		  hql.append(" SELECT  RE.RECORD_STATUS AS RECORD_STATUS, ");
		  //hql.append(" SUM(RD.FEE_MNY) AS FEE_MNY, ");
		  //hql.append(" FEE.REGISTRATION_TYPE  AS REGISTRATION_TYPE, ");
		  //hql.append(" RD.RECEIPT_ID AS RECEIPT_ID, ");
		  hql.append(" RE.RECEIVE_OFFICER_DATE AS RECEIVE_OFFICER_DATE ");
		  hql.append(" FROM RECEIPT RE ");
          hql.append(" INNER JOIN RECEIPT_DETAIL RD ON RE.RECEIPT_ID = RD.RECEIPT_ID ");
		  hql.append(" INNER JOIN MAS_FEE FEE ON FEE.FEE_ID = RD.FEE "); 
          hql.append(" WHERE RE.RECEIPT_STATUS = 'P' ");
          hql.append(" AND RE.RECORD_STATUS = 'N' ");
		  hql.append(" AND RD.RECORD_STATUS = 'N' ");
		  hql.append(" AND RE.RECEIVE_OFFICER_DATE IS NOT NULL");

		  if((null != dto.getReceiveOfficerDateFrom()) && (!dto.getReceiveOfficerDateFrom().equals("")))
		    {
		    	if((null != dto.getReceiveOfficerDateTo()) && (!dto.getReceiveOfficerDateTo().equals("")))
		    	{
					hql.append("  and ( RE.RECEIVE_OFFICER_DATE >=  convert(datetime, :RE_START_DATE ,20) ");
					hql.append(" and RE.RECEIVE_OFFICER_DATE <=  convert(datetime, :RE_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getReceiveOfficerDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getReceiveOfficerDateTo(), "23:00");
						
					parames.put("RE_START_DATE", sendDtmFrom);
					parames.put("RE_END_DATE", sendDtmTo);
		    	}else
		    	{
		    		hql.append("  and ( RE.RECEIVE_OFFICER_DATE >=  convert(datetime, :RE_START_DATE ,20) ");
					hql.append(" and RE.RECEIVE_OFFICER_DATE <=  convert(datetime, :RE_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getReceiveOfficerDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getReceiveOfficerDateTo(), "23:00");
						
					parames.put("RE_START_DATE", sendDtmFrom);
					parames.put("RE_END_DATE", sendDtmFrom);
		    	}
		    }
		  
		  hql.append(" GROUP BY RE.RECORD_STATUS,RE.RECEIVE_OFFICER_DATE ");
		  hql.append(" ORDER BY RE.RECEIVE_OFFICER_DATE ASC");
		  
		  SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
			
		  sqlQuery.addScalar("RECORD_STATUS", Hibernate.STRING); 
		  sqlQuery.addScalar("RECEIVE_OFFICER_DATE", Hibernate.DATE);
		  sqlQuery.setProperties(parames);
		  List<Object[]>  result = sqlQuery.list();
	  
		    return result;
	}
	public List<Object[]> findReceiptFeeDateForDay(ReceiptDTO dto)throws Exception
	{
		 Map<String,Object> parames = new HashMap<String,Object>();
		    
		  StringBuilder hql = new StringBuilder(); 
		  hql.append(" SELECT  RD.FEE AS Fee, ");
		  hql.append(" SUM(RD.FEE_MNY) AS FEE_MNY, ");
		  //hql.append(" FEE.REGISTRATION_TYPE  AS REGISTRATION_TYPE, ");
		  //hql.append(" RD.RECEIPT_ID AS RECEIPT_ID, ");
		  hql.append(" RE.RECEIVE_OFFICER_DATE AS RECEIVE_OFFICER_DATE, ");
		  hql.append(" RD.FEE_NAME AS FEE_NAME ");
		  hql.append(" FROM RECEIPT RE ");
          hql.append(" INNER JOIN RECEIPT_DETAIL RD ON RE.RECEIPT_ID = RD.RECEIPT_ID ");
		  hql.append(" INNER JOIN MAS_FEE FEE ON FEE.FEE_ID = RD.FEE "); 
          hql.append(" WHERE RE.RECEIPT_STATUS = 'P' ");
          hql.append(" AND RE.RECORD_STATUS = 'N' ");
		  hql.append(" AND RD.RECORD_STATUS = 'N' ");
		  hql.append(" AND RE.RECEIVE_OFFICER_DATE IS NOT NULL");

		  if((null != dto.getReceiveOfficerDateFrom()) && (!dto.getReceiveOfficerDateFrom().equals("")))
		    {
		    	if((null != dto.getReceiveOfficerDateTo()) && (!dto.getReceiveOfficerDateTo().equals("")))
		    	{
					hql.append("  and ( RE.RECEIVE_OFFICER_DATE >=  convert(datetime, :RE_START_DATE ,20) ");
					hql.append(" and RE.RECEIVE_OFFICER_DATE <=  convert(datetime, :RE_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getReceiveOfficerDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getReceiveOfficerDateTo(), "23:00");
						
					parames.put("RE_START_DATE", sendDtmFrom);
					parames.put("RE_END_DATE", sendDtmTo);
		    	}else
		    	{
		    		hql.append("  and ( RE.RECEIVE_OFFICER_DATE >=  convert(datetime, :RE_START_DATE ,20) ");
					hql.append(" and RE.RECEIVE_OFFICER_DATE <=  convert(datetime, :RE_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getReceiveOfficerDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getReceiveOfficerDateFrom(), "23:00");
						
					parames.put("RE_START_DATE", sendDtmFrom);
					parames.put("RE_END_DATE", sendDtmTo);
		    	}
		    }
		  
		  hql.append(" GROUP BY RD.FEE,RE.RECEIVE_OFFICER_DATE,RD.FEE_NAME ");
		  hql.append(" ORDER BY RE.RECEIVE_OFFICER_DATE,RD.FEE ASC");
		  
		  SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
			
		  sqlQuery.addScalar("Fee", Hibernate.LONG); 
		  sqlQuery.addScalar("FEE_MNY", Hibernate.BIG_DECIMAL); 
		  sqlQuery.addScalar("RECEIVE_OFFICER_DATE", Hibernate.DATE);
		  sqlQuery.addScalar("FEE_NAME", Hibernate.STRING);
		  sqlQuery.setProperties(parames);
		  List<Object[]>  result = sqlQuery.list();
	  
		    return result;
	}
	
	public List<Object[]> findReceiptFeeTotal(ReceiptDTO dto,String fee)throws Exception
	{
		 Map<String,Object> parames = new HashMap<String,Object>();
		    
		  StringBuilder hql = new StringBuilder();
		  hql.append(" SELECT RE.RECEIPT_ID AS RECEIPT_ID, ");
		  hql.append(" RE.BOOK_NO AS BOOK_NO, ");
		  hql.append(" RE.RECEIPT_NO AS RECEIPT_NO, ");
		  hql.append(" SUM(RD.FEE_MNY) AS FEE_MNY");
		  hql.append(" FROM RECEIPT RE ");
		  hql.append(" INNER JOIN RECEIPT_DETAIL RD ON RE.RECEIPT_ID = RD.RECEIPT_ID ");
		  hql.append(" WHERE RE.RECEIPT_STATUS = 'P' ");
		  hql.append(" AND RE.RECORD_STATUS = 'N' ");
		  hql.append(" AND RD.RECORD_STATUS = 'N' ");
		  
		  if(fee!=null && !fee.equals("") && !fee.equals("7"))
		  {
			  hql.append(" AND RD.FEE =:RD_FEE_ID ");
			  parames.put("RD_FEE_ID", fee);
		  }else
		  {
			  hql.append(" AND RD.FEE IN ('7','8','9','10','11','13') ");
		  }
		  
		  if(dto.getBookNo()!=null && !dto.getBookNo().equals(""))
		  {
			  hql.append(" AND RE.BOOK_NO =:RD_BOOK_NO ");
			  parames.put("RD_BOOK_NO", dto.getBookNo());
		  }
		  
		  if((null != dto.getReceiveOfficerDateFrom()) && (!dto.getReceiveOfficerDateFrom().equals("")))
		    {
				hql.append("  and ( RE.RECEIVE_OFFICER_DATE >=  convert(datetime, :RE_START_DATE ,20) ");
				hql.append(" and RE.RECEIVE_OFFICER_DATE <=  convert(datetime, :RE_END_DATE ,20) ) ");
					
				String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getReceiveOfficerDateFrom(), "00:00");
				String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getReceiveOfficerDateFrom(), "23:00");
						
				parames.put("RE_START_DATE", sendDtmFrom);
				parames.put("RE_END_DATE", sendDtmTo);
		    	
		    }
		  hql.append(" GROUP BY RE.RECEIPT_ID,RE.BOOK_NO,RE.RECEIPT_NO,RD.FEE ");
		  SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
		  
		  sqlQuery.addScalar("RECEIPT_ID", Hibernate.LONG); 
		  sqlQuery.addScalar("BOOK_NO", Hibernate.STRING); 
		  sqlQuery.addScalar("RECEIPT_NO", Hibernate.STRING);
		  sqlQuery.addScalar("FEE_MNY", Hibernate.BIG_DECIMAL);
		  sqlQuery.setProperties(parames);
		  List<Object[]>  result = sqlQuery.list();
		
		  return result; 
	}
	public List<Object[]> findReceiptFeeTotalBookNo(ReceiptDTO dto,String fee)throws Exception
	{
		 Map<String,Object> parames = new HashMap<String,Object>();
		    
		  StringBuilder hql = new StringBuilder();
		  hql.append(" SELECT  ");
		  hql.append(" RE.BOOK_NO AS BOOK_NO, ");
		  hql.append(" RD.FEE AS FEE ");
		 // hql.append(" SUM(RD.FEE_MNY) AS FEE_MNY");
		  hql.append(" FROM RECEIPT RE ");
		  hql.append(" INNER JOIN RECEIPT_DETAIL RD ON RE.RECEIPT_ID = RD.RECEIPT_ID ");
		  hql.append(" WHERE RE.RECEIPT_STATUS = 'P' ");
		  hql.append(" AND RE.RECORD_STATUS = 'N' ");
		  hql.append(" AND RD.RECORD_STATUS = 'N' ");
		  
		  if(fee!=null && !fee.equals("") && !fee.equals("7"))
		  {
			  hql.append(" AND RD.FEE =:RD_FEE_ID ");
			  parames.put("RD_FEE_ID", fee);
		  }else
		  {
			  hql.append(" AND RD.FEE IN ('7','8','9','10','11','13') ");
		  }
		  
		  if((null != dto.getReceiveOfficerDateFrom()) && (!dto.getReceiveOfficerDateFrom().equals("")))
		    {
				hql.append("  and ( RE.RECEIVE_OFFICER_DATE >=  convert(datetime, :RE_START_DATE ,20) ");
				hql.append(" and RE.RECEIVE_OFFICER_DATE <=  convert(datetime, :RE_END_DATE ,20) ) ");
					
				String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getReceiveOfficerDateFrom(), "00:00");
				String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getReceiveOfficerDateFrom(), "23:00");
						
				parames.put("RE_START_DATE", sendDtmFrom);
				parames.put("RE_END_DATE", sendDtmTo);
		    	
		    }
		  hql.append(" GROUP BY RE.BOOK_NO,RD.FEE ");
		  SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
		  
		  //sqlQuery.addScalar("RECEIPT_ID", Hibernate.LONG); 
		  sqlQuery.addScalar("BOOK_NO", Hibernate.STRING); 
		  sqlQuery.addScalar("FEE", Hibernate.DOUBLE);
		  //sqlQuery.addScalar("FEE_MNY", Hibernate.BIG_DECIMAL);
		  sqlQuery.setProperties(parames);
		  List<Object[]>  result = sqlQuery.list();
		
		  return result; 
	}
	public List<Object[]> findReceiptSlip(ReceiptDTO dto)throws Exception
	{
		 Map<String,Object> parames = new HashMap<String,Object>();
		  StringBuilder hql = new StringBuilder();
		  hql.append(" SELECT ");
		  hql.append(" 	RE.RECEIPT_ID AS RECEIPT_ID, ");
		  hql.append(" 	RE.BOOK_NO AS BOOK_NO, ");
		  hql.append(" 	RE.RECEIPT_NO AS RECEIPT_NO, ");
		  hql.append(" 	RE.RECEIPT_NAME AS RECEIPT_NAME, ");
		  hql.append(" 	RE.RECEIVE_OFFICER_DATE AS RECEIVE_OFFICER_DATE, ");
		  hql.append(" 	RE.RECEIVE_OFFICER_NAME AS RECEIVE_OFFICER_NAME, ");
		  hql.append(" 	RE.AUTHORITY AS AUTHORITY, ");
		  hql.append(" 	RD.FEE_MNY AS FEE_MNY, ");
		  hql.append(" 	RD.FEE_NAME AS FEE_NAME, ");
		  hql.append(" 	PE.IDENTITY_NO AS IDENTITY_NO, ");
		  hql.append(" 	REG.ORG_ID AS ORG_ID, ");
		  hql.append(" 	MP.POS_NAME AS POS_NAME ");
		  hql.append(" FROM ");
		  hql.append(" 	RECEIPT RE ");
		  hql.append(" 		INNER JOIN ");
		  hql.append(" 		RECEIPT_DETAIL RD ");
		  hql.append(" 		ON ");
		  hql.append(" 		RE.RECEIPT_ID = RD.RECEIPT_ID ");
		  hql.append(" 		INNER JOIN RECEIPT_MAP_REGISTRATION RMG ");
		  hql.append(" 		ON RMG.RECEIPT_ID = RE.RECEIPT_ID ");
		  hql.append(" 		INNER JOIN REGISTRATION REG ");
		  hql.append(" 		ON REG.REG_ID = RMG.REG_ID ");
		  hql.append(" 		INNER JOIN TRADER TR ");
		  hql.append(" 		ON TR.TRADER_ID = REG.TRADER_ID ");
		  hql.append(" 		INNER JOIN PERSON PE ");
		  hql.append(" 		ON PE.PERSON_ID = TR.PERSON_ID ");
		  hql.append(" 		LEFT JOIN ADM_USER AU ");
		  hql.append(" 		ON AU.USER_LOGIN = RE.CREATE_USER ");
		  hql.append(" 		LEFT JOIN MAS_POSITION MP ");
		  hql.append(" 		ON MP.POS_ID = AU.POS_ID ");
		  hql.append(" WHERE ");
		  hql.append(" 	RE.RECEIPT_STATUS = 'P' AND ");
		  hql.append(" 	RE.RECORD_STATUS = 'N' AND ");
		  hql.append(" 	RD.RECORD_STATUS = 'N' AND ");
		  hql.append("  RD.FEE_MNY <> 0 AND ");
		  if(dto.getReceiptId() > 0)
		  {
			  hql.append(" RE.RECEIPT_ID =:RE_RECEIPT_ID ");
			  parames.put("RE_RECEIPT_ID", dto.getReceiptId());
		  }
		  
		  hql.append(" GROUP BY ");
		  hql.append(" 	RE.RECEIPT_ID,RE.BOOK_NO,RE.RECEIPT_NO,RD.FEE,RD.FEE_MNY,RD.FEE_NAME, ");
		  hql.append(" 	RE.RECEIPT_NAME,RE.RECEIVE_OFFICER_DATE,RE.RECEIVE_OFFICER_NAME,RE.AUTHORITY,PE.IDENTITY_NO,PE.IDENTITY_NO,REG.ORG_ID,MP.POS_NAME ");
		  SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
		  
		  sqlQuery.addScalar("RECEIPT_ID", Hibernate.LONG);
		  sqlQuery.addScalar("BOOK_NO", Hibernate.STRING);
		  sqlQuery.addScalar("RECEIPT_NO", Hibernate.STRING);
		  sqlQuery.addScalar("RECEIPT_NAME", Hibernate.STRING);
		  sqlQuery.addScalar("RECEIVE_OFFICER_DATE", Hibernate.DATE);
		  sqlQuery.addScalar("RECEIVE_OFFICER_NAME", Hibernate.STRING);
		  sqlQuery.addScalar("AUTHORITY", Hibernate.STRING);
		  sqlQuery.addScalar("FEE_MNY", Hibernate.BIG_DECIMAL);
		  sqlQuery.addScalar("FEE_NAME", Hibernate.STRING);
		  sqlQuery.addScalar("IDENTITY_NO", Hibernate.STRING);
		  sqlQuery.addScalar("ORG_ID", Hibernate.LONG);
		  sqlQuery.addScalar("POS_NAME", Hibernate.STRING);
		  sqlQuery.setProperties(parames);
		  List<Object[]>  result = sqlQuery.list();

		  return result; 
	}
	
	public List<Object[]> findPrintReportGuaranteeAndFeeRegistrationType(ReceiptDTO dto)throws Exception
	{
		 Map<String,Object> parames = new HashMap<String,Object>();
		 StringBuilder hql = new StringBuilder();
		 hql.append(" SELECT ");
		 hql.append("  	RE.RECEIVE_OFFICER_DATE AS RECEIVE_OFFICER_DATE, ");
		 hql.append("  	RE.BOOK_NO AS BOOK_NO, ");
		 hql.append("  	RE.RECEIPT_NO AS RECEIPT_NO, ");
		 hql.append("  	TR.TRADER_NAME AS TRADER_NAME, ");
		 hql.append("  	TR.LICENSE_NO AS LICENSE_NO, ");
		 hql.append("  	RD.FEE_MNY AS FEE_MNY, ");
		 hql.append("  	GU.BANK_GUARANTEE_MNY AS BANK_GUARANTEE_MNY, ");
		 hql.append("  	GU.CASH_ACCOUNT_MNY AS CASH_ACCOUNT_MNY, ");
		 hql.append("  	GU.GOVERNMENT_BOND_MNY AS GOVERNMENT_BOND_MNY, ");
		 hql.append("  	TR.TRADER_CATEGORY  AS TRADER_CATEGORY ");
		 hql.append(" FROM RECEIPT RE  ");
		 hql.append(" INNER JOIN RECEIPT_DETAIL RD ON  RD.RECEIPT_ID = RE.RECEIPT_ID ");
		 hql.append(" INNER JOIN RECEIPT_MAP_REGISTRATION RM ON RM.RECEIPT_ID = RD.RECEIPT_ID ");
		 hql.append(" INNER JOIN REGISTRATION REG ON REG.REG_ID = RM.REG_ID ");
		 hql.append(" INNER JOIN TRADER TR ON TR.TRADER_ID = REG.TRADER_ID ");
		 hql.append(" INNER JOIN GUARANTEE GU ON GU.TRADER_TYPE = TR.TRADER_TYPE AND GU.LICENSE_NO = TR.LICENSE_NO ");
		 hql.append(" WHERE RE.RECEIPT_STATUS = 'P' ");
		 hql.append(" AND RE.RECORD_STATUS = 'N' ");
		 hql.append(" AND RD.RECORD_STATUS = 'N' ");
		 hql.append(" AND GU.GUARANTEE_STATUS = 'N' ");
		 
		 if((null != dto.getReceiveOfficerDateFrom()) && (!dto.getReceiveOfficerDateFrom().equals("")))
		    {
		    	if((null != dto.getReceiveOfficerDateTo()) && (!dto.getReceiveOfficerDateTo().equals("")))
		    	{
					hql.append("  and ( RE.RECEIVE_OFFICER_DATE >=  convert(datetime, :RE_START_DATE ,20) ");
					hql.append(" and RE.RECEIVE_OFFICER_DATE <=  convert(datetime, :RE_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getReceiveOfficerDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getReceiveOfficerDateTo(), "23:00");
						
					parames.put("RE_START_DATE", sendDtmFrom);
					parames.put("RE_END_DATE", sendDtmTo);
		    	}else
		    	{
		    		hql.append("  and ( RE.RECEIVE_OFFICER_DATE >=  convert(datetime, :RE_START_DATE ,20) ");
					hql.append(" and RE.RECEIVE_OFFICER_DATE <=  convert(datetime, :RE_END_DATE ,20) ) ");
					
					String sendDtmFrom = DateUtils.getDateMSSQLFormat(dto.getReceiveOfficerDateFrom(), "00:00");
					String sendDtmTo = DateUtils.getDateMSSQLFormat(dto.getReceiveOfficerDateFrom(), "23:00");
						
					parames.put("RE_START_DATE", sendDtmFrom);
					parames.put("RE_END_DATE", sendDtmTo);
		    	}
		    }
		 
		 hql.append(" GROUP BY  ");
		 hql.append(" 	RE.RECEIVE_OFFICER_DATE, ");
		 hql.append("  	RE.BOOK_NO, ");
		 hql.append("  	RE.RECEIPT_NO, ");
		 hql.append("  	TR.TRADER_NAME, ");
		 hql.append("  	TR.LICENSE_NO, ");
		 hql.append("  	RD.FEE_MNY, ");
		 hql.append("  	GU.BANK_GUARANTEE_MNY, ");
		 hql.append("  	GU.CASH_ACCOUNT_MNY, ");
		 hql.append("  	GU.GOVERNMENT_BOND_MNY, ");
		 hql.append("  	TR.TRADER_CATEGORY ");
		 
		 SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
		  
		  sqlQuery.addScalar("RECEIVE_OFFICER_DATE", Hibernate.DATE);
		  sqlQuery.addScalar("BOOK_NO", Hibernate.STRING);
		  sqlQuery.addScalar("RECEIPT_NO", Hibernate.STRING);
		  sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING);
		  sqlQuery.addScalar("LICENSE_NO", Hibernate.DATE);
		  sqlQuery.addScalar("FEE_MNY", Hibernate.BIG_DECIMAL);
		  sqlQuery.addScalar("BANK_GUARANTEE_MNY", Hibernate.BIG_DECIMAL);
		  sqlQuery.addScalar("CASH_ACCOUNT_MNY", Hibernate.BIG_DECIMAL);
		  sqlQuery.addScalar("GOVERNMENT_BOND_MNY", Hibernate.BIG_DECIMAL);
		  
		  sqlQuery.setProperties(parames);
		  List<Object[]>  result = sqlQuery.list();

		  return result;
	}
}
