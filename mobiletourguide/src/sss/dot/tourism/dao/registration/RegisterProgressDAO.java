package sss.dot.tourism.dao.registration;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.sf.cglib.reflect.FastClass;
import net.sf.cglib.reflect.FastMethod;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.Plantrip;
import sss.dot.tourism.domain.RegisterProgress;

@Repository("registerProgressDAO")
public class RegisterProgressDAO extends BaseDAO{
	
	public RegisterProgressDAO()
	{
		this.domainObj = RegisterProgress.class;
	}
	
	public Object insert(Object persistence)
	  {

	    return this.getHibernateTemplate().save(persistence);
	  }

	  public void update(Object persistence)
	  {

	    try
	    {
	      FastClass clazz = FastClass.create(persistence.getClass());

	      FastMethod method = clazz.getMethod("setLastUpdDtm", new Class[] { Date.class });

	      method.invoke(persistence, new Object[] { new Date() });

	    }
	    catch (InvocationTargetException e)
	    {

	      this.error(e.getMessage(), e);

	    }
	    this.getHibernateTemplate().update(persistence);
	  }

		@SuppressWarnings("unchecked")
		public List<RegisterProgress> findRoleProgressbyRegistration(long regId,String role)
				throws Exception {
			
			@SuppressWarnings("rawtypes")
			ArrayList params = new ArrayList();
			
			StringBuilder hql = new StringBuilder();
			hql.append(" from  RegisterProgress as tr ");
			
			hql.append(" where tr.progress = ? ");
			params.add(role);
			
			hql.append(" and tr.registration.regId = ? ");
			params.add(regId);
			
			hql.append(" order by progressDate desc ");

			List<RegisterProgress> list = (List<RegisterProgress>) this
					.getHibernateTemplate().find(hql.toString(), params.toArray());

			return list;
		}
		
		@SuppressWarnings("unchecked")
		public List<RegisterProgress> findProgressBackStatusbyRegistration(long regId,String role)
				throws Exception {
			
			@SuppressWarnings("rawtypes")
			ArrayList params = new ArrayList();
			
			StringBuilder hql = new StringBuilder();
			hql.append(" from  RegisterProgress as tr ");
			
			hql.append(" where tr.progressBackStatus = 'B' ");
			
			hql.append(" and tr.progress = ? ");
			params.add(role);
			
			hql.append(" and tr.registration.regId = ? ");
			params.add(regId);
			
			hql.append(" order by progressDate desc ");

			List<RegisterProgress> list = (List<RegisterProgress>) this
					.getHibernateTemplate().find(hql.toString(), params.toArray());

			return list;
		}
		
		@SuppressWarnings("unchecked")
		public List<RegisterProgress> findRegisterProgressbyRegistration(long regId,String progressStatus)
				throws Exception {
			
			@SuppressWarnings("rawtypes")
			ArrayList params = new ArrayList();
			
			StringBuilder hql = new StringBuilder();
			hql.append(" from  RegisterProgress as tr ");
			

			
			hql.append(" where tr.registration.regId = ? ");
			params.add(regId);
			
			if((null != progressStatus ) && (!progressStatus.isEmpty()))
			{
				hql.append(" and tr.progressStatus = ? ");
				params.add(progressStatus);
			}
			
			
			hql.append(" order by progressDate desc ");

			List<RegisterProgress> list = (List<RegisterProgress>) this
					.getHibernateTemplate().find(hql.toString(), params.toArray());

			return list;
		}
}
