package sss.dot.tourism.dao.registration;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.TraderAddress;

@Repository("traderAddressDAO")
public class TraderAddressDAO extends BaseDAO{
 /**
	 * 
	 */
	private static final long serialVersionUID = -100289054974147759L;

public TraderAddressDAO()
 {
	 this.domainObj = TraderAddress.class;
 }


	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<TraderAddress> findAllByTraderPl(long traderId,String traderAddressType, String recordStatus)
	{
		String recordStatusIn = "('N','H')";
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  TraderAddress as dep ");
		hql.append(" where dep.recordStatus in ('N','H') ");
		//params.add(" "+recordStatusIn);
		hql.append(" and dep.trader.traderId = ? ");
		params.add(traderId);
		
		if(traderAddressType != null)
		{
			hql.append(" and dep.addressType = ? ");
			params.add(traderAddressType);
		}
		
	
		
		return (List<TraderAddress>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<TraderAddress> findAllByTraderName(long traderId,String traderAddressType, String recordStatus)
	{
		System.out.println("OK");
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  TraderAddress as dep ");
		hql.append(" where dep.recordStatus in ('N','H','T') ");
		hql.append(" and dep.trader.traderId = ? ");
		params.add(traderId);
		
		if(traderAddressType != null)
		{
			hql.append(" and dep.addressType = ? ");
			params.add(traderAddressType);
		}
		
	
		
		return (List<TraderAddress>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<TraderAddress> findAllEmaiByTrader(long traderId, String recordStatus)
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  TraderAddress as dep ");
		hql.append(" where dep.recordStatus = ? ");
		hql.append(" and dep.trader.traderId = ? ");
		
		
		params.add(recordStatus);
		params.add(traderId);
		
//		if(traderAddressType != null)
//		{
//			hql.append(" and dep.addressType = ? ");
//			params.add(traderAddressType);
//		}
		
	
		
		return (List<TraderAddress>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<TraderAddress> findAllByTrader(long traderId,String traderAddressType, String recordStatus)
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  TraderAddress as dep ");
		hql.append(" where dep.trader.traderId = ? ");		
		
		params.add(traderId);
		
		if(StringUtils.isNotEmpty(recordStatus)){
			hql.append(" and dep.recordStatus = ? ");
			params.add(recordStatus);
		}
		
		if(traderAddressType != null)
		{
			hql.append(" and dep.addressType = ? ");
			params.add(traderAddressType);
		}
		
		return (List<TraderAddress>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<TraderAddress> findAllByTraderAndEmail(long traderId,String email, String recordStatus)
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" select dep from  TraderAddress as dep ");
		hql.append(" where dep.recordStatus = ? ");
		hql.append(" and dep.trader.traderId = ? ");
		hql.append(" and dep.trader.branchType is null ");
		
		
		params.add(recordStatus);
		params.add(traderId);
		
		if(email != null)
		{
			hql.append(" and dep.email like ? ");
			params.add(email+"%");
		}
		
		List<TraderAddress> list = (List<TraderAddress>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	
		
		
		return list;
	}
}
