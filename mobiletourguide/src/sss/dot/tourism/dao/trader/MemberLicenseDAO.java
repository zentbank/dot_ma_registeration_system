package sss.dot.tourism.dao.trader;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.Committee;
import sss.dot.tourism.domain.MemberLicense;
import sss.dot.tourism.util.RecordStatus;

@Repository("memberLicenseDAO")
public class MemberLicenseDAO extends BaseDAO{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7005413965668932612L;

	public MemberLicenseDAO()
	{
		this.domainObj = MemberLicense.class;
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<MemberLicense> authenUser(String userName, String password)
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  MemberLicense as dep ");
		hql.append(" where dep.recordStatus = ? ");
		hql.append(" and dep.memberLoginName = ? ");
		hql.append(" and dep.memberLoginPassword = ? ");
		
		
		params.add(RecordStatus.NORMAL.getStatus());
		params.add(userName);
		params.add(password);
		
		
	
		
		return (List<MemberLicense>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<MemberLicense> findUserByIdentityNo(String identityNo)
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  MemberLicense as dep ");
		hql.append(" where dep.recordStatus = ? ");
		hql.append(" and dep.indentityNo = ? ");
		
		
		
		params.add(RecordStatus.NORMAL.getStatus());
		params.add(identityNo);
		
		return (List<MemberLicense>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<MemberLicense> findUserByLoginName(String loginName)
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  MemberLicense as dep ");
		hql.append(" where dep.recordStatus = ? ");
		hql.append(" and dep.memberLoginName = ? ");
		
		
		
		params.add(RecordStatus.NORMAL.getStatus());
		params.add(loginName);
		
		return (List<MemberLicense>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
}
