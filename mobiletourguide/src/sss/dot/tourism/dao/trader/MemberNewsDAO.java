package sss.dot.tourism.dao.trader;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.Journal;
import sss.dot.tourism.domain.MemberNews;
import sss.dot.tourism.util.RecordStatus;


@Repository("memberNewsDAO")
public class MemberNewsDAO extends BaseDAO{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3533569983879057234L;

	public MemberNewsDAO()
	{
		this.domainObj = MemberNews.class;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<MemberNews> getMemberPublicNews(String tradertype)
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  MemberNews as ml ");
		hql.append(" where ml.recordStatus = ? ");
		
		params.add(RecordStatus.NORMAL.getStatus());
		
		hql.append(" and ml.traderType = ? ");
		params.add(tradertype);
		
		Calendar cal = Calendar.getInstance();
		hql.append(" and ml.newsStartDate < ? ");
		params.add(cal.getTime());
		hql.append(" and ml.newsEndDate > ? ");
		params.add(cal.getTime());
	
		
		return (List<MemberNews>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<MemberNews> getMemberNews(String token)
	{
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" select dep from  MemberNews as dep ");
		hql.append(" inner join  dep.memberLicense as ml ");
		hql.append(" where ml.recordStatus = ? ");
		hql.append(" and ml.indentityNo = ? ");
		hql.append(" and dep.recordStatus = ? ");
		
		
		params.add(RecordStatus.NORMAL.getStatus());
		params.add(token);
		params.add(RecordStatus.NORMAL.getStatus());
		
		
	
		
		return (List<MemberNews>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
	
	  public List<MemberNews> findAllByPaging(String newsHeader,String tradertype,int firstRow, int pageSize)
	  {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  MemberNews as dep ");
		hql.append(" where dep.recordStatus = ? ");
		params.add(RecordStatus.NORMAL.getStatusString());
		
		if((tradertype != null) && (!tradertype.equals("")))
		{
			hql.append(" and dep.traderType = ? ");
			params.add(tradertype);
		}
		
		
		
		if((newsHeader != null) && !(newsHeader.isEmpty()))
		{
			hql.append(" and dep.newsHeader like ? ");
			params.add("%"+newsHeader+"%");
		}
		
		
		
		hql.append(" order by dep.newsStartDate ASC ");
		
		return (List<MemberNews>)this.findByAddPaging(hql.toString(), firstRow, pageSize, params.toArray());
	  }
}
