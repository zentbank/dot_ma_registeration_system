package sss.dot.tourism.dao.training;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import com.sss.aut.service.User;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.Country;
import sss.dot.tourism.domain.Education;
import sss.dot.tourism.dto.training.EducationDTO;
import sss.dot.tourism.util.DateUtils;

@Repository("educationDAO")
public class EducationDAO extends BaseDAO{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1128024589488815026L;

	public EducationDAO()
	{
		this.domainObj = Education.class;
			
	}
	
	@SuppressWarnings("unchecked")
	public List<Education> findEducationAll(EducationDTO dto)
			throws Exception {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  Education as tr ");
		hql.append(" where tr.recordStatus = ? ");
		params.add(dto.getRecordStatus());
		
		if (dto.getPersonId() > 0) {
			hql.append(" and tr.person.personId = ? ");
			params.add(dto.getPersonId());
		}
		
		if((dto.getEducationType() != null) && (!dto.getEducationType().equals("")))
		{
			hql.append(" and tr.educationType = ? ");
			params.add(dto.getEducationType());
		}

		List<Education> list = (List<Education>) this
				.getHibernateTemplate().find(hql.toString(), params.toArray());

		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<Education> findEducationByPerson(long personId, String recordStatus, String educationType)
			throws Exception {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  Education as tr ");
		hql.append(" where tr.recordStatus = ? ");
		params.add(recordStatus);
		
		if (personId > 0) {
			hql.append(" and tr.person.personId = ? ");
			params.add(personId);
		}
		
		if((educationType != null) && (!educationType.equals("")))
		{
			hql.append(" and tr.educationType = ? ");
			params.add(educationType);
		}

		List<Education> list = (List<Education>) this
				.getHibernateTemplate().find(hql.toString(), params.toArray());

		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> findEducationCourseAll(EducationDTO dto, User user ,int start, int limit)
			throws Exception{
		
		Map<String,Object> params = new HashMap<String,Object>();
		StringBuilder hql = new StringBuilder();
		
		hql.append(" SELECT ED.EDU_ID AS EDU_ID, ");
		hql.append(" ED.STUDY_DATE AS STUDY_DATE, ");
		hql.append(" ED.GRADUATION_DATE AS GRADUATION_DATE, ");
		hql.append(" ED.GRADUATION_YEAR AS GRADUATION_YEAR, ");
		hql.append(" ED.GRADUATION_COURSE AS GRADUATION_COURSE, ");
		hql.append(" ED.MAS_UNIVERSITY_ID AS MAS_UNIVERSITY_ID, ");
		hql.append(" ED.GENERATION_GRADUATE AS GENERATION_GRADUATE");
		hql.append(" FROM EDUCATION AS ED ");
		hql.append(" WHERE ED.RECORD_STATUS = :EDUCATION_RECORD_STATUS ");
		params.put("EDUCATION_RECORD_STATUS", dto.getRecordStatus());
		
		if(dto.getEduId() > 0)
		{
			hql.append(" AND ED.EDU_ID = :EDU_EDU_ID ");
			params.put("EDU_EDU_ID", dto.getEduId());
		}
		
		if(dto.getGraduationCourse()!= null && !dto.getGraduationCourse().equals(""))
		{
			hql.append(" AND ED.GRADUATION_COURSE LIKE :EDU_GRADUATION_COURSE");
			params.put("EDU_GRADUATION_COURSE","%"+dto.getGraduationCourse()+"%");
		}


		if ((dto.getStudyDate() != null) && (!dto.getStudyDate().equals(""))) {
			if((dto.getGraduationDate() != null) && (!dto.getGraduationDate().equals(""))) 
			{
			hql.append("  AND ED.STUDY_DATE >=  convert(datetime, :EDU_STUDY_DATE ,20) ");

			String studyDate = DateUtils.getDateMSSQLFormat(dto.getStudyDate(),
					"00:00");
			System.out.println("EDU_STUDY_DATE = " + studyDate);

			params.put("EDU_STUDY_DATE",studyDate);
			}else
			{
				hql.append("  AND ED.STUDY_DATE =  convert(datetime, :EDU_STUDY_DATE ,20) ");

				String studyDate = DateUtils.getDateMSSQLFormat(dto.getStudyDate(),
						"00:00");
				System.out.println("EDU_STUDY_DATE = " + studyDate);

				params.put("EDU_STUDY_DATE",studyDate);
			}
		}
		
		if ((dto.getGraduationDate() != null) && (!dto.getGraduationDate().equals(""))) {
			if((dto.getStudyDate() != null) && (!dto.getStudyDate().equals(""))) 
			{
			hql.append(" AND ED.GRADUATION_DATE <=  convert(datetime, :EDU_GRADUATION_DATE ,20) ");

			String graduationDate = DateUtils.getDateMSSQLFormat(
					dto.getGraduationDate(), "23:00");

			System.out.println("EDU_GRADUATION_DATE = " + graduationDate);

			params.put("EDU_GRADUATION_DATE",graduationDate);
			}else
			{
				hql.append(" AND ED.GRADUATION_DATE =  convert(datetime, :EDU_GRADUATION_DATE ,20) ");

				String graduationDate = DateUtils.getDateMSSQLFormat(
						dto.getGraduationDate(), "23:00");

				System.out.println("EDU_GRADUATION_DATE = " + graduationDate);

				params.put("EDU_GRADUATION_DATE",graduationDate);
			}
		}
		
		if(dto.getMasUniversityId() > 0)
		{
			hql.append(" AND ED.MAS_UNIVERSITY_ID = :EDU_MAS_UNIVERSITY_ID ");
			params.put("EDU_MAS_UNIVERSITY_ID", dto.getMasUniversityId());
		}
		
		
		SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
	    sqlQuery.setFirstResult(start);
	    sqlQuery.setMaxResults(limit);
	    
	    
	    sqlQuery.addScalar("EDU_ID", Hibernate.LONG); //1
		sqlQuery.addScalar("STUDY_DATE", Hibernate.DATE); //2
	    sqlQuery.addScalar("GRADUATION_DATE", Hibernate.DATE); //3
	    sqlQuery.addScalar("GRADUATION_COURSE", Hibernate.STRING); //4
	    sqlQuery.addScalar("MAS_UNIVERSITY_ID", Hibernate.STRING);//5 
	    sqlQuery.addScalar("GENERATION_GRADUATE", Hibernate.INTEGER);//6
	    //sqlQuery.addScalar("RECORD_STATUS", Hibernate.STRING);//7
		
	    sqlQuery.setProperties(params);
	    List<Object[]>  result = sqlQuery.list();
  
	    return result;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Education> findCourseAll()
	{
		
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  Education as tr ");
		hql.append(" where tr.recordStatus = 'E' ");

		return (List<Education>)this.getHibernateTemplate().find(hql.toString(), params.toArray());
	}
}








