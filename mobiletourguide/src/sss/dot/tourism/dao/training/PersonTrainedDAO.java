package sss.dot.tourism.dao.training;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import com.sss.aut.service.User;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.PersonTrained;
import sss.dot.tourism.dto.training.EducationDTO;
import sss.dot.tourism.util.DateUtils;
@Repository("personTrainedDAO")
public class PersonTrainedDAO extends BaseDAO{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -182662766998024565L;

	public PersonTrainedDAO()
	{
		this.domainObj = PersonTrained.class;
	}
	
	@SuppressWarnings("unchecked")
	public List<PersonTrained> findPersonTrainedAll(EducationDTO dto)
			throws Exception {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" select tr from  PersonTrained as tr ");
		hql.append(" inner join tr.education eu ");
		hql.append(" where tr.recordStatus = 'N' ");
		hql.append(" and eu.recordStatus = 'E' ");

		if ((dto.getIdentityNo() != null) && (!dto.getIdentityNo().equals(""))) {
			hql.append(" and tr.identityNo = ? ");
			params.add(dto.getIdentityNo());
		}

		if (dto.getMasUniversityId() > 0) {
			hql.append(" and eu.masUniversity.masUniversityId = ? ");
			params.add(dto.getMasUniversityId());
		}

		if ((dto.getGraduationCourse() != null)
				&& (!dto.getGraduationCourse().equals(""))) {
			hql.append(" and eu.graduationCourse like ? ");
			params.add("%" + dto.getGraduationCourse() + "%");
		}

		if ((dto.getStudyDate() != null) && (!dto.getStudyDate().equals(""))) {

			hql.append("  and eu.studyDate >=  convert(datetime, ? ,20) ");
			hql.append(" and eu.graduationDate <=  convert(datetime, ? ,20) ");

			String studyDate = DateUtils.getDateMSSQLFormat(dto.getStudyDate(),
					"00:00");
			String graduationDate = DateUtils.getDateMSSQLFormat(
					dto.getGraduationDate(), "23:00");

			System.out.println("studyDate = " + studyDate);
			System.out.println("graduationDate = " + graduationDate);

			params.add(studyDate);
			params.add(graduationDate);

		}

		List<PersonTrained> list = (List<PersonTrained>) this
				.getHibernateTemplate().find(hql.toString(), params.toArray());

		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> findPersonTrainedAll(EducationDTO dto, User user ,int start, int limit)
			throws Exception{
		
		Map<String,Object> params = new HashMap<String,Object>();
		StringBuilder hql = new StringBuilder();
		
		hql.append(" SELECT PR.PERSON_TRAINED_ID AS PERSON_TRAINED_ID, ");
		hql.append(" PR.IDENTITY_NO AS IDENTITY_NO, ");
		hql.append(" PR.EDU_ID AS EDU_ID, ");
		hql.append(" PR.GENDER AS GENDER, ");
		hql.append(" PR.PASSPORT_NO AS PASSPORT_NO, ");
		hql.append(" MP.PREFIX_NAME AS PREFIX_NAME, ");
	    hql.append(" PR.FIRST_NAME AS FIRST_NAME, ");
	    hql.append(" PR.LAST_NAME AS LAST_NAME, ");
		hql.append(" PR.FIRST_NAME_EN AS FIRST_NAME_EN, ");
		hql.append(" PR.LAST_NAME_EN AS LAST_NAME_EN, ");
		//hql.append(" PR.PERSON_NATIONALITY AS PERSON_NATIONALITY, ");
		hql.append(" PR.AGE_YEAR AS AGE_YEAR, ");
		hql.append(" PR.IDENTITY_NO_EXPIRE AS IDENTITY_NO_EXPIRE, ");
		hql.append(" PEPROV.PROVINCE_NAME AS PROVINCE_NAME, ");
		hql.append(" PEAMP.AMPHUR_NAME AS AMPHUR_NAME, ");
		//hql.append(" PR.BIRTH_DATE AS BIRTH_DATE, ");
		hql.append(" ED.STUDY_DATE AS STUDY_DATE, ");
		hql.append(" ED.GRADUATION_DATE AS GRADUATION_DATE, ");
		hql.append(" ED.GRADUATION_YEAR AS GRADUATION_YEAR, ");
		hql.append(" ED.GRADUATION_COURSE AS GRADUATION_COURSE, ");
		hql.append(" ED.MAS_UNIVERSITY_ID AS MAS_UNIVERSITY_ID, ");
		hql.append(" ED.GENERATION_GRADUATE AS GENERATION_GRADUATE, ");
		
		hql.append(" PR.PROVINCE_ID AS PROVINCE_ID, ");
		hql.append(" PR.AMPHUR_ID AS AMPHUR_ID, ");
		hql.append(" PR.PREFIX_ID AS PREFIX_ID, ");
		hql.append(" PR.BIRTH_DATE AS BIRTH_DATE ");
		
		hql.append(" FROM PERSON_TRAINED AS PR ");
		
		
		
		hql.append(" INNER JOIN EDUCATION ED ");
	    hql.append(" ON(ED.EDU_ID = PR.EDU_ID) ");
	
	    hql.append(" LEFT JOIN MAS_PREFIX MP ");
	    hql.append(" ON(PR.PREFIX_ID = MP.PREFIX_ID) ");
	    
	    hql.append(" LEFT JOIN MAS_AMPHUR PEAMP ");
	    hql.append(" ON(PEAMP.AMPHUR_ID = PR.AMPHUR_ID) ");
	    
	    hql.append(" LEFT JOIN MAS_PROVINCE PEPROV ");
	    hql.append(" ON(PEPROV.PROVINCE_ID = PR.PROVINCE_ID) ");
	    
	    hql.append(" WHERE PR.RECORD_STATUS = :PERSON_TRAINED_RECORD_STATUS ");
		params.put("PERSON_TRAINED_RECORD_STATUS", dto.getRecordStatus());
		
		if(dto.getPersonTrainedId() > 0)
		{
			hql.append(" AND PR.PERSON_TRAINED_ID =:PERSONTRAINED_PERSON_TRAINED_ID");
			params.put("PERSONTRAINED_PERSON_TRAINED_ID", dto.getPersonTrainedId());
		}
		
		if(dto.getIdentityNo()!=null && !dto.getIdentityNo().equals(""))
		{
			hql.append(" AND PR.IDENTITY_NO =:PERSONTRAINED_IDENTITY_NO");
			params.put("PERSONTRAINED_IDENTITY_NO", dto.getIdentityNo());
		}
		
		if(dto.getFirstName()!=null && !dto.getFirstName().equals(""))
		{
			hql.append(" AND PR.FIRST_NAME =:PERSONTRAINED_FIRST_NAME");
			params.put("PERSONTRAINED_FIRST_NAME", dto.getFirstName());
		}
		
		if(dto.getLastName()!=null && !dto.getLastName().equals(""))
		{
			hql.append(" AND PR.LAST_NAME =:PERSONTRAINED_LAST_NAME");
			params.put("PERSONTRAINED_LAST_NAME", dto.getLastName());
		}
		
		if(dto.getEduId() > 0)
		{
			hql.append(" AND PR.EDU_ID =:PERSONTRAINED_EDU_ID");
			params.put("PERSONTRAINED_EDU_ID", dto.getEduId());
		}
		
		hql.append(" AND ED.RECORD_STATUS = 'E' ");
		
		
		SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
	    sqlQuery.setFirstResult(start);
	    sqlQuery.setMaxResults(limit);
	    
	    
	    sqlQuery.addScalar("PERSON_TRAINED_ID", Hibernate.LONG); //1
		sqlQuery.addScalar("IDENTITY_NO", Hibernate.STRING); //2
	    sqlQuery.addScalar("EDU_ID", Hibernate.LONG); //3
	    sqlQuery.addScalar("GENDER", Hibernate.STRING); //4
	    sqlQuery.addScalar("PASSPORT_NO", Hibernate.STRING);//5 
	    sqlQuery.addScalar("PREFIX_NAME", Hibernate.STRING);//6
	    sqlQuery.addScalar("FIRST_NAME", Hibernate.STRING);//7
	    sqlQuery.addScalar("LAST_NAME",Hibernate.STRING);//8
	    sqlQuery.addScalar("FIRST_NAME_EN",Hibernate.STRING);//9
	    sqlQuery.addScalar("LAST_NAME_EN",Hibernate.STRING);//10
	   // sqlQuery.addScalar("PERSON_NATIONALITY",Hibernate.STRING);//11
	    sqlQuery.addScalar("AGE_YEAR",Hibernate.INTEGER);//11
	    sqlQuery.addScalar("IDENTITY_NO_EXPIRE",Hibernate.DATE);//12
	    sqlQuery.addScalar("PROVINCE_NAME",Hibernate.STRING);//13
	    sqlQuery.addScalar("AMPHUR_NAME",Hibernate.STRING);//14
	    //sqlQuery.addScalar("BIRTH_DATE",Hibernate.DATE);//
	    sqlQuery.addScalar("STUDY_DATE", Hibernate.DATE); //15
	    sqlQuery.addScalar("GRADUATION_DATE", Hibernate.DATE); //16
	    sqlQuery.addScalar("GRADUATION_COURSE", Hibernate.STRING); //17
	    sqlQuery.addScalar("MAS_UNIVERSITY_ID", Hibernate.STRING);//18
	    sqlQuery.addScalar("GENERATION_GRADUATE", Hibernate.INTEGER);//19
	    sqlQuery.addScalar("PROVINCE_ID", Hibernate.LONG);//20
	    sqlQuery.addScalar("AMPHUR_ID", Hibernate.LONG);//21
	    sqlQuery.addScalar("PREFIX_ID", Hibernate.LONG);//22
	    sqlQuery.addScalar("BIRTH_DATE",Hibernate.DATE);//23
		sqlQuery.setProperties(params);
		List<Object[]>  result = sqlQuery.list();
  
	    return result;
	}
}
