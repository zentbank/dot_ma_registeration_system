package sss.dot.tourism.dto.complaint;

import java.util.Date;

public class ComplaintDocDTO {
	private long complaintDocId;

	private String complaintDocType;
	private String complaintDocName;
	private String complaintDocPath;
	
	private long complaintId;
	private String questionType;
	private String complaintDesc;
	private String complaintCardId;
	private String complaintName;
	private String complaintTel;
	private String complaintEmail;
	private String complaintNo;
	private Date complaintDate;
	private String complaintStatus;
	private String authorityComment;
	private String complaintProgress;
	
	private String traderType;
	private String licenseNo;
	
	
	
	public String getTraderType() {
		return traderType;
	}
	public void setTraderType(String traderType) {
		this.traderType = traderType;
	}
	public String getLicenseNo() {
		return licenseNo;
	}
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	public long getComplaintDocId() {
		return complaintDocId;
	}
	public void setComplaintDocId(long complaintDocId) {
		this.complaintDocId = complaintDocId;
	}
	public String getComplaintDocType() {
		return complaintDocType;
	}
	public void setComplaintDocType(String complaintDocType) {
		this.complaintDocType = complaintDocType;
	}
	public String getComplaintDocName() {
		return complaintDocName;
	}
	public void setComplaintDocName(String complaintDocName) {
		this.complaintDocName = complaintDocName;
	}
	public String getComplaintDocPath() {
		return complaintDocPath;
	}
	public void setComplaintDocPath(String complaintDocPath) {
		this.complaintDocPath = complaintDocPath;
	}
	public long getComplaintId() {
		return complaintId;
	}
	public void setComplaintId(long complaintId) {
		this.complaintId = complaintId;
	}
	public String getQuestionType() {
		return questionType;
	}
	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}
	public String getComplaintDesc() {
		return complaintDesc;
	}
	public void setComplaintDesc(String complaintDesc) {
		this.complaintDesc = complaintDesc;
	}
	public String getComplaintCardId() {
		return complaintCardId;
	}
	public void setComplaintCardId(String complaintCardId) {
		this.complaintCardId = complaintCardId;
	}
	public String getComplaintName() {
		return complaintName;
	}
	public void setComplaintName(String complaintName) {
		this.complaintName = complaintName;
	}
	public String getComplaintTel() {
		return complaintTel;
	}
	public void setComplaintTel(String complaintTel) {
		this.complaintTel = complaintTel;
	}
	public String getComplaintEmail() {
		return complaintEmail;
	}
	public void setComplaintEmail(String complaintEmail) {
		this.complaintEmail = complaintEmail;
	}
	public String getComplaintNo() {
		return complaintNo;
	}
	public void setComplaintNo(String complaintNo) {
		this.complaintNo = complaintNo;
	}
	public Date getComplaintDate() {
		return complaintDate;
	}
	public void setComplaintDate(Date complaintDate) {
		this.complaintDate = complaintDate;
	}
	public String getComplaintStatus() {
		return complaintStatus;
	}
	public void setComplaintStatus(String complaintStatus) {
		this.complaintStatus = complaintStatus;
	}
	public String getAuthorityComment() {
		return authorityComment;
	}
	public void setAuthorityComment(String authorityComment) {
		this.authorityComment = authorityComment;
	}
	public String getComplaintProgress() {
		return complaintProgress;
	}
	public void setComplaintProgress(String complaintProgress) {
		this.complaintProgress = complaintProgress;
	}
	
	
}
