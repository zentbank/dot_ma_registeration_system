package sss.dot.tourism.dto.info;

import java.util.List;

public class Fruit {
	String name;
	int quality;
	List mylist;
	
	
	
	public List getMylist() {
		return mylist;
	}


	public void setMylist(List mylist) {
		this.mylist = mylist;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}

	public int getQuality() {
		return quality;
	}


	public void setQuality(int quality) {
		this.quality = quality;
	}

	public Fruit(String name, int quality) {
		this.name = name;
		this.quality = quality;
	}

	public Fruit() {
	}
}
