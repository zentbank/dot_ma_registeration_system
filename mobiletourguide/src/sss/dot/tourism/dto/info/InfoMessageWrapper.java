package sss.dot.tourism.dto.info;

import java.io.Serializable;
import java.util.List;

public class InfoMessageWrapper<E> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7128431674112730820L;
	private List<E> data;
	private String status;
	private int unread;
	private String data_type;
	private String message;


	public InfoMessageWrapper(String status) {
		this.status = status;
	}

	public InfoMessageWrapper(String status, String message) {
		this.message = message;
		this.status = status;
	}

	public InfoMessageWrapper(List<E> data, String data_type,String status) {
		this.data = (List<E>) data;
		this.data_type = data_type;
		this.status = status;
	}

	public List<E> getData() {
		return data;
	}

	public void setData(List<E> data) {
		this.data = data;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getData_type() {
		return data_type;
	}

	public void setData_type(String data_type) {
		this.data_type = data_type;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getUnread() {
		return unread;
	}

	public void setUnread(int unread) {
		this.unread = unread;
	}


}
