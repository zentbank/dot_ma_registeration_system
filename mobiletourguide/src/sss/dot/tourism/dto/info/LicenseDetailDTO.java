package sss.dot.tourism.dto.info;

import java.io.Serializable;
import java.util.List;

public class LicenseDetailDTO implements Serializable{
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 1874616398255731962L;
	private long traderId;
	private String traderName;
	private String traderNameEn;
	private String licenseNo;
	private String licenseStatus;
	private String traderCategory;
	private String traderAddress;
	private String traderType;
	private String traderTypeName;
	private String IndentityNo;
	private String inspectionDate;
	private String licenseType;
	private long authorityId;
	private long verifyId;
	
	private List<LicenseRegistrationDTO> registration;
	private List<String> complaint;
	private List<String> committee;
	
	private String imgUrl;
	
	private String errMsg;
	
private long complaintLicenseId;
	
	private long masComplaintTypeId;
	private String questionType;
	private String complaintDesc;
	private String complaintCardId;
	private String complaintName;
	private String complaintTel;
	private String complaintEmail;
	private String complaintNo;
	private String complaintDate;
	private String complaintStatus;
	private String authorityComment;
	private String complaintProgress;
	private String authority;
	
	
	
	public long getAuthorityId() {
		return authorityId;
	}
	public void setAuthorityId(long authorityId) {
		this.authorityId = authorityId;
	}
	public String getLicenseType() {
		return licenseType;
	}
	public void setLicenseType(String licenseType) {
		this.licenseType = licenseType;
	}
	public String getInspectionDate() {
		return inspectionDate;
	}
	public void setInspectionDate(String inspectionDate) {
		this.inspectionDate = inspectionDate;
	}
	public long getComplaintLicenseId() {
		return complaintLicenseId;
	}
	public void setComplaintLicenseId(long complaintLicenseId) {
		this.complaintLicenseId = complaintLicenseId;
	}
	public long getMasComplaintTypeId() {
		return masComplaintTypeId;
	}
	public void setMasComplaintTypeId(long masComplaintTypeId) {
		this.masComplaintTypeId = masComplaintTypeId;
	}
	public String getQuestionType() {
		return questionType;
	}
	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}
	public String getComplaintDesc() {
		return complaintDesc;
	}
	public void setComplaintDesc(String complaintDesc) {
		this.complaintDesc = complaintDesc;
	}
	public String getComplaintCardId() {
		return complaintCardId;
	}
	public void setComplaintCardId(String complaintCardId) {
		this.complaintCardId = complaintCardId;
	}
	public String getComplaintName() {
		return complaintName;
	}
	public void setComplaintName(String complaintName) {
		this.complaintName = complaintName;
	}
	public String getComplaintTel() {
		return complaintTel;
	}
	public void setComplaintTel(String complaintTel) {
		this.complaintTel = complaintTel;
	}
	public String getComplaintEmail() {
		return complaintEmail;
	}
	public void setComplaintEmail(String complaintEmail) {
		this.complaintEmail = complaintEmail;
	}
	public String getComplaintNo() {
		return complaintNo;
	}
	public void setComplaintNo(String complaintNo) {
		this.complaintNo = complaintNo;
	}
	public String getComplaintDate() {
		return complaintDate;
	}
	public void setComplaintDate(String complaintDate) {
		this.complaintDate = complaintDate;
	}
	public String getComplaintStatus() {
		return complaintStatus;
	}
	public void setComplaintStatus(String complaintStatus) {
		this.complaintStatus = complaintStatus;
	}
	public String getAuthorityComment() {
		return authorityComment;
	}
	public void setAuthorityComment(String authorityComment) {
		this.authorityComment = authorityComment;
	}
	public String getComplaintProgress() {
		return complaintProgress;
	}
	public void setComplaintProgress(String complaintProgress) {
		this.complaintProgress = complaintProgress;
	}
	public String getAuthority() {
		return authority;
	}
	public void setAuthority(String authority) {
		this.authority = authority;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	public String getTraderTypeName() {
		return traderTypeName;
	}
	public void setTraderTypeName(String traderTypeName) {
		this.traderTypeName = traderTypeName;
	}
	public String getIndentityNo() {
		return IndentityNo;
	}
	public void setIndentityNo(String indentityNo) {
		IndentityNo = indentityNo;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public String getTraderType() {
		return traderType;
	}
	public void setTraderType(String traderType) {
		this.traderType = traderType;
	}
	public long getTraderId() {
		return traderId;
	}
	public void setTraderId(long traderId) {
		this.traderId = traderId;
	}
	public String getTraderName() {
		return traderName;
	}
	public void setTraderName(String traderName) {
		this.traderName = traderName;
	}
	public String getTraderNameEn() {
		return traderNameEn;
	}
	public void setTraderNameEn(String traderNameEn) {
		this.traderNameEn = traderNameEn;
	}
	public String getLicenseNo() {
		return licenseNo;
	}
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	public String getLicenseStatus() {
		return licenseStatus;
	}
	public void setLicenseStatus(String licenseStatus) {
		this.licenseStatus = licenseStatus;
	}

	public String getTraderCategory() {
		return traderCategory;
	}
	public void setTraderCategory(String traderCategory) {
		this.traderCategory = traderCategory;
	}
	public String getTraderAddress() {
		return traderAddress;
	}
	public void setTraderAddress(String traderAddress) {
		this.traderAddress = traderAddress;
	}
	public List<LicenseRegistrationDTO> getRegistration() {
		return registration;
	}
	public void setRegistration(List<LicenseRegistrationDTO> registration) {
		this.registration = registration;
	}
	public List<String> getComplaint() {
		return complaint;
	}
	public void setComplaint(List<String> complaint) {
		this.complaint = complaint;
	}
	public List<String> getCommittee() {
		return committee;
	}
	public void setCommittee(List<String> committee) {
		this.committee = committee;
	}
	public long getVerifyId() {
		return verifyId;
	}
	public void setVerifyId(long verifyId) {
		this.verifyId = verifyId;
	}
	
	
	
}
