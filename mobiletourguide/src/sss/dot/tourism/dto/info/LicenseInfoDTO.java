package sss.dot.tourism.dto.info;

import java.io.Serializable;

public class LicenseInfoDTO implements Serializable {

	
/*
 * 	“id” :  [Key ที่จะ Pass กลับไปในหน้า Show Result],
	“name_th” : [ชื่อไทย],
	“name_en” : [ชื่ออังกฤษ],
	“reg_id” : [เลขที่ใบอนุญาต]
	“reg_status” : [สถานะการจดทะเบียน],
 * */
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7757561066716520609L;
	private String id;
	private String name_th;
	private String name_en;
	private String reg_id;
	private String reg_status;
	private String img_url;
	
	public String getImg_url() {
		return img_url;
	}
	public void setImg_url(String img_url) {
		this.img_url = img_url;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName_th() {
		return name_th;
	}
	public void setName_th(String name_th) {
		this.name_th = name_th;
	}
	public String getName_en() {
		return name_en;
	}
	public void setName_en(String name_en) {
		this.name_en = name_en;
	}
	public String getReg_id() {
		return reg_id;
	}
	public void setReg_id(String reg_id) {
		this.reg_id = reg_id;
	}
	public String getReg_status() {
		return reg_status;
	}
	public void setReg_status(String reg_status) {
		this.reg_status = reg_status;
	}
	
	
	
}
