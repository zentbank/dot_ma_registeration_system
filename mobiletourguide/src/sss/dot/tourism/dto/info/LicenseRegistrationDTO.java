package sss.dot.tourism.dto.info;

import java.io.Serializable;

public class LicenseRegistrationDTO implements Serializable{
		
		/**
	 * 
	 */
	private static final long serialVersionUID = 6120777655650841051L;
		private String registrationTypeName;
		private String registrationDate;
		public String getRegistrationTypeName() {
			return registrationTypeName;
		}
		public void setRegistrationTypeName(String registrationTypeName) {
			this.registrationTypeName = registrationTypeName;
		}
		public String getRegistrationDate() {
			return registrationDate;
		}
		public void setRegistrationDate(String registrationDate) {
			this.registrationDate = registrationDate;
		}
		
		
		
}
