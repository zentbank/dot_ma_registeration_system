package sss.dot.tourism.dto.info;

import java.io.Serializable;

public class MemberDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6364630361858768414L;

	private String token;
	
	private long memberId;
	private String licenseNo;
	private String traderType;
	private String memberLoginName;
	private String memberLoginPassword;
	private String indentityNo;
	
	private boolean isValid;
	
	

	public boolean isValid() {
		return isValid;
	}

	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public long getMemberId() {
		return memberId;
	}

	public void setMemberId(long memberId) {
		this.memberId = memberId;
	}

	public String getLicenseNo() {
		return licenseNo;
	}

	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}

	public String getTraderType() {
		return traderType;
	}

	public void setTraderType(String traderType) {
		this.traderType = traderType;
	}

	public String getMemberLoginName() {
		return memberLoginName;
	}

	public void setMemberLoginName(String memberLoginName) {
		this.memberLoginName = memberLoginName;
	}

	public String getMemberLoginPassword() {
		return memberLoginPassword;
	}

	public void setMemberLoginPassword(String memberLoginPassword) {
		this.memberLoginPassword = memberLoginPassword;
	}

	public String getIndentityNo() {
		return indentityNo;
	}

	public void setIndentityNo(String indentityNo) {
		this.indentityNo = indentityNo;
	}
	
	
}
