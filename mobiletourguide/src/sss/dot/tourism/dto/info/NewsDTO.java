package sss.dot.tourism.dto.info;

import java.io.Serializable;

public class NewsDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5007266153819163999L;
	
	private long id;
	private String header;
	private String details;
	private String news_img;
	
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getHeader() {
		return header;
	}
	public void setHeader(String header) {
		this.header = header;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public String getNews_img() {
		return news_img;
	}
	public void setNews_img(String news_img) {
		this.news_img = news_img;
	}
	
	
}
