package sss.dot.tourism.dto.info;

import java.io.Serializable;

import org.springframework.web.bind.annotation.PathVariable;

public class RequestParamDTO implements Serializable{
	private String language;
	private String searchType;
	private String SearchParam;
	private String licenseNo;
	
	
	
	public String getLicenseNo() {
		return licenseNo;
	}
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getSearchType() {
		return searchType;
	}
	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}
	public String getSearchParam() {
		return SearchParam;
	}
	public void setSearchParam(String searchParam) {
		SearchParam = searchParam;
	}
	
}
