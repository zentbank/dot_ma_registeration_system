package sss.dot.tourism.dto.mas;

import java.io.Serializable;

public class AdmGroupDTO  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8855655616878535956L;
	private long groupId;
	private String groupName;
	private String groupRole;
	private Integer officerGroup;
	private long userId;
	
	private boolean active;
	
	
	
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public long getGroupId() {
		return groupId;
	}
	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getGroupRole() {
		return groupRole;
	}
	public void setGroupRole(String groupRole) {
		this.groupRole = groupRole;
	}
	public Integer getOfficerGroup() {
		return officerGroup;
	}
	public void setOfficerGroup(Integer officerGroup) {
		this.officerGroup = officerGroup;
	}
	
	
}
