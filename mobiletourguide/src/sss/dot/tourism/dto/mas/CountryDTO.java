package sss.dot.tourism.dto.mas;

import java.io.Serializable;

public class CountryDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8840241138721419328L;
	private long countryId;
	private String countryName;
	private String countryNameEn;
	private String language;
	public long getCountryId() {
		return countryId;
	}
	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getCountryNameEn() {
		return countryNameEn;
	}
	public void setCountryNameEn(String countryNameEn) {
		this.countryNameEn = countryNameEn;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	
	
}
