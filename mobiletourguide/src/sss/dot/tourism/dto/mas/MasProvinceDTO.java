package sss.dot.tourism.dto.mas;

import java.io.Serializable;

public class MasProvinceDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6559753339633866275L;
	private long provinceId;
	private String provinceName;
	private String provinceNameEn;
	
	public long getProvinceId() {
		return provinceId;
	}
	public void setProvinceId(long provinceId) {
		this.provinceId = provinceId;
	}
	public String getProvinceName() {
		return provinceName;
	}
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}
	public String getProvinceNameEn() {
		return provinceNameEn;
	}
	public void setProvinceNameEn(String provinceNameEn) {
		this.provinceNameEn = provinceNameEn;
	}
	
	
}
