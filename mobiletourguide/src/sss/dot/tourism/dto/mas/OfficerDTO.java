package sss.dot.tourism.dto.mas;

import java.io.Serializable;

public class OfficerDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1324950144391113868L;
	private long officerId;
	private String officerName;
	private String officerType;
	private String position;
		
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getOfficerType() {
		return officerType;
	}
	public void setOfficerType(String officerType) {
		this.officerType = officerType;
	}
	public long getOfficerId() {
		return officerId;
	}
	public void setOfficerId(long officerId) {
		this.officerId = officerId;
	}
	public String getOfficerName() {
		return officerName;
	}
	public void setOfficerName(String officerName) {
		this.officerName = officerName;
	}
	
}
