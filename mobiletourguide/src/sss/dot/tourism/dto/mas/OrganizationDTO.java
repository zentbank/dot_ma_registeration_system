package sss.dot.tourism.dto.mas;

import java.io.Serializable;

public class OrganizationDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2742962564030098122L;
	private long orgId;
	private String orgName;
	public long getOrgId() {
		return orgId;
	}
	public void setOrgId(long orgId) {
		this.orgId = orgId;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	
	
}
