package sss.dot.tourism.dto.punishment;

import java.io.Serializable;
import java.util.Date;

public class MasActDTO  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -801979869898118721L;
	private long masActId;
	private String actType;
	private String actDesc;
	private String traderType;
	private boolean active;
	
	private long  suspendId;
	private long  revokeId;
	
	

	public long getRevokeId() {
		return revokeId;
	}

	public void setRevokeId(long revokeId) {
		this.revokeId = revokeId;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public long getSuspendId() {
		return suspendId;
	}

	public void setSuspendId(long suspendId) {
		this.suspendId = suspendId;
	}

	public long getMasActId() {
		return masActId;
	}

	public void setMasActId(long masActId) {
		this.masActId = masActId;
	}

	public String getActType() {
		return actType;
	}

	public void setActType(String actType) {
		this.actType = actType;
	}

	public String getActDesc() {
		return actDesc;
	}

	public void setActDesc(String actDesc) {
		this.actDesc = actDesc;
	}

	public String getTraderType() {
		return traderType;
	}

	public void setTraderType(String traderType) {
		this.traderType = traderType;
	}
	
	
}
