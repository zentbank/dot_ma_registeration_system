package sss.dot.tourism.dto.punishment;

import java.io.Serializable;
import java.util.Date;

import sss.dot.tourism.domain.SuspensionLicense;

public class SuspensionAlertDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3924911841491136468L;

	private long suspendAlertId;
	private String alertType;
	private String alertTypeName;
	private String alertDate;
	private String responseDate;
	private String alertStatus;
	private String alertStatusName;
	
	private long suspendId;
	
	private String emailAlertDate;
	private String emailResponseDate;
	private String emailAlertStatus;
	
	private long revokeId;
	
	private long revokeAlertId;

	private String traderName;
	private String licenseNo;
	private String traderType;
	private String personType;
	private String prefixName;
	private String postfixName;
	private String firstName;
	private String lastName;
	private String actDesc;
	private String suspendPeriod;
	private String suspendPeriodDay;
	private String suspendDetail;
	private String traderOwnerName;
	private String period;
	private String suspendNo;
	private long traderId;
	private String traderCategoryType;
	private String email;
	private String traderCategory;
	private String revokeDetail;
	private String receiveNo;
	
	public long getRevokeAlertId() {
		return revokeAlertId;
	}

	public void setRevokeAlertId(long revokeAlertId) {
		this.revokeAlertId = revokeAlertId;
	}

	public long getRevokeId() {
		return revokeId;
	}

	public void setRevokeId(long revokeId) {
		this.revokeId = revokeId;
	}

	public String getEmailAlertDate() {
		return emailAlertDate;
	}

	public void setEmailAlertDate(String emailAlertDate) {
		this.emailAlertDate = emailAlertDate;
	}

	public String getEmailResponseDate() {
		return emailResponseDate;
	}

	public void setEmailResponseDate(String emailResponseDate) {
		this.emailResponseDate = emailResponseDate;
	}

	public String getEmailAlertStatus() {
		return emailAlertStatus;
	}

	public void setEmailAlertStatus(String emailAlertStatus) {
		this.emailAlertStatus = emailAlertStatus;
	}

	public String getAlertTypeName() {
		return alertTypeName;
	}

	public void setAlertTypeName(String alertTypeName) {
		this.alertTypeName = alertTypeName;
	}

	public String getAlertStatusName() {
		return alertStatusName;
	}

	public void setAlertStatusName(String alertStatusName) {
		this.alertStatusName = alertStatusName;
	}

	public long getSuspendAlertId() {
		return suspendAlertId;
	}

	public void setSuspendAlertId(long suspendAlertId) {
		this.suspendAlertId = suspendAlertId;
	}

	public String getAlertType() {
		return alertType;
	}

	public void setAlertType(String alertType) {
		this.alertType = alertType;
	}

	public String getAlertDate() {
		return alertDate;
	}

	public void setAlertDate(String alertDate) {
		this.alertDate = alertDate;
	}

	public String getResponseDate() {
		return responseDate;
	}

	public void setResponseDate(String responseDate) {
		this.responseDate = responseDate;
	}

	public String getAlertStatus() {
		return alertStatus;
	}

	public void setAlertStatus(String alertStatus) {
		this.alertStatus = alertStatus;
	}

	public long getSuspendId() {
		return suspendId;
	}

	public void setSuspendId(long suspendId) {
		this.suspendId = suspendId;
	}

	public String getTraderName() {
		return traderName;
	}

	public void setTraderName(String traderName) {
		this.traderName = traderName;
	}

	public String getLicenseNo() {
		return licenseNo;
	}

	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}

	public String getTraderType() {
		return traderType;
	}

	public void setTraderType(String traderType) {
		this.traderType = traderType;
	}

	public String getPersonType() {
		return personType;
	}

	public void setPersonType(String personType) {
		this.personType = personType;
	}

	public String getPrefixName() {
		return prefixName;
	}

	public void setPrefixName(String prefixName) {
		this.prefixName = prefixName;
	}

	public String getPostfixName() {
		return postfixName;
	}

	public void setPostfixName(String postfixName) {
		this.postfixName = postfixName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getActDesc() {
		return actDesc;
	}

	public void setActDesc(String actDesc) {
		this.actDesc = actDesc;
	}

	public String getSuspendPeriod() {
		return suspendPeriod;
	}

	public void setSuspendPeriod(String suspendPeriod) {
		this.suspendPeriod = suspendPeriod;
	}

	public String getSuspendPeriodDay() {
		return suspendPeriodDay;
	}

	public void setSuspendPeriodDay(String suspendPeriodDay) {
		this.suspendPeriodDay = suspendPeriodDay;
	}

	public String getSuspendDetail() {
		return suspendDetail;
	}

	public void setSuspendDetail(String suspendDetail) {
		this.suspendDetail = suspendDetail;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getTraderOwnerName() {
		return traderOwnerName;
	}

	public void setTraderOwnerName(String traderOwnerName) {
		this.traderOwnerName = traderOwnerName;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getSuspendNo() {
		return suspendNo;
	}

	public void setSuspendNo(String suspendNo) {
		this.suspendNo = suspendNo;
	}

	public long getTraderId() {
		return traderId;
	}

	public void setTraderId(long traderId) {
		this.traderId = traderId;
	}

	public String getTraderCategoryType() {
		return traderCategoryType;
	}

	public void setTraderCategoryType(String traderCategoryType) {
		this.traderCategoryType = traderCategoryType;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTraderCategory() {
		return traderCategory;
	}

	public void setTraderCategory(String traderCategory) {
		this.traderCategory = traderCategory;
	}

	public String getRevokeDetail() {
		return revokeDetail;
	}

	public void setRevokeDetail(String revokeDetail) {
		this.revokeDetail = revokeDetail;
	}

	public String getReceiveNo() {
		return receiveNo;
	}

	public void setReceiveNo(String receiveNo) {
		this.receiveNo = receiveNo;
	}
	
	
	
}
