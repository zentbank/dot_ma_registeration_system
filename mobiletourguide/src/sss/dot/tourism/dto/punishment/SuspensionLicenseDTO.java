package sss.dot.tourism.dto.punishment;

import java.io.Serializable;
import java.util.Date;

public class SuspensionLicenseDTO  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2007561862833621550L;
	private long suspendId;
	private String suspensionNo;
	private String suspensionDate;
	private Integer suspendPeriod;
	private Integer suspendPeriodDay;
	private String suspendStatus;
	private String suspendStatusName;
	private String suspendFrom;
	private String suspendTo;
	private String cancelDate;
	private String cancelRemark;
	private String suspendDetail;
	private String officerName;
	private long officerId;
	private String masAct;
	private String officerAsOwner;
	
	private long traderId;
	private String traderType;
	private String licenseNo;
	private String traderTypeName;
	private String identityNo;
	private String firstName;
	private String lastName;
	private String traderOwnerName;
	
	private String traderCategory;
	private String traderCategoryName;

	private String traderName;
	private String traderNameEn;
	
	
	private int page;
	private int start;
	private int limit;
   
	
	private String email;
	
	public Integer getSuspendPeriodDay() {
		return suspendPeriodDay;
	}
	public void setSuspendPeriodDay(Integer suspendPeriodDay) {
		this.suspendPeriodDay = suspendPeriodDay;
	}
	public String getOfficerAsOwner() {
		return officerAsOwner;
	}
	public void setOfficerAsOwner(String officerAsOwner) {
		this.officerAsOwner = officerAsOwner;
	}
	public long getOfficerId() {
		return officerId;
	}
	public void setOfficerId(long officerId) {
		this.officerId = officerId;
	}
	public String getSuspendStatusName() {
		return suspendStatusName;
	}
	public void setSuspendStatusName(String suspendStatusName) {
		this.suspendStatusName = suspendStatusName;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public String getMasAct() {
		return masAct;
	}
	public void setMasAct(String masAct) {
		this.masAct = masAct;
	}
	public String getTraderName() {
		return traderName;
	}
	public void setTraderName(String traderName) {
		this.traderName = traderName;
	}
	public String getTraderNameEn() {
		return traderNameEn;
	}
	public void setTraderNameEn(String traderNameEn) {
		this.traderNameEn = traderNameEn;
	}
	public long getTraderId() {
		return traderId;
	}
	public void setTraderId(long traderId) {
		this.traderId = traderId;
	}
	public String getTraderCategory() {
		return traderCategory;
	}
	public void setTraderCategory(String traderCategory) {
		this.traderCategory = traderCategory;
	}
	public String getTraderCategoryName() {
		return traderCategoryName;
	}
	public void setTraderCategoryName(String traderCategoryName) {
		this.traderCategoryName = traderCategoryName;
	}
	public String getIdentityNo() {
		return identityNo;
	}
	public void setIdentityNo(String identityNo) {
		this.identityNo = identityNo;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getTraderOwnerName() {
		return traderOwnerName;
	}
	public void setTraderOwnerName(String traderOwnerName) {
		this.traderOwnerName = traderOwnerName;
	}
	public String getTraderTypeName() {
		return traderTypeName;
	}
	public void setTraderTypeName(String traderTypeName) {
		this.traderTypeName = traderTypeName;
	}
	public long getSuspendId() {
		return suspendId;
	}
	public void setSuspendId(long suspendId) {
		this.suspendId = suspendId;
	}
	public String getSuspensionNo() {
		return suspensionNo;
	}
	public void setSuspensionNo(String suspensionNo) {
		this.suspensionNo = suspensionNo;
	}
	public String getSuspensionDate() {
		return suspensionDate;
	}
	public void setSuspensionDate(String suspensionDate) {
		this.suspensionDate = suspensionDate;
	}
	public Integer getSuspendPeriod() {
		return suspendPeriod;
	}
	public void setSuspendPeriod(Integer suspendPeriod) {
		this.suspendPeriod = suspendPeriod;
	}
	public String getSuspendStatus() {
		return suspendStatus;
	}
	public void setSuspendStatus(String suspendStatus) {
		this.suspendStatus = suspendStatus;
	}
	public String getSuspendFrom() {
		return suspendFrom;
	}
	public void setSuspendFrom(String suspendFrom) {
		this.suspendFrom = suspendFrom;
	}
	public String getSuspendTo() {
		return suspendTo;
	}
	public void setSuspendTo(String suspendTo) {
		this.suspendTo = suspendTo;
	}
	public String getCancelDate() {
		return cancelDate;
	}
	public void setCancelDate(String cancelDate) {
		this.cancelDate = cancelDate;
	}
	public String getCancelRemark() {
		return cancelRemark;
	}
	public void setCancelRemark(String cancelRemark) {
		this.cancelRemark = cancelRemark;
	}
	public String getSuspendDetail() {
		return suspendDetail;
	}
	public void setSuspendDetail(String suspendDetail) {
		this.suspendDetail = suspendDetail;
	}
	public String getOfficerName() {
		return officerName;
	}
	public void setOfficerName(String officerName) {
		this.officerName = officerName;
	}
	public String getTraderType() {
		return traderType;
	}
	public void setTraderType(String traderType) {
		this.traderType = traderType;
	}
	public String getLicenseNo() {
		return licenseNo;
	}
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	
}
