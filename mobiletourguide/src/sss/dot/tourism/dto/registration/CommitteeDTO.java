package sss.dot.tourism.dto.registration;

import java.io.Serializable;
import java.util.Date;

public class CommitteeDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6877171543820673047L;
	private long committeeId;
	private String committeeType;
	private String committeeTypeName;
	private String committeeStatus;
	private String committeeStatusName;
	private String committeeIdentityNo;
	private String committeeIdentityDate;
	private String committeeName;
	private String committeeLastname;
	private String committeePersonType;
	private String committeePersonTypeName;

	
	private String prefixNameCommittee;
	private String postfixNameCommittee;
	
	
	private String committeeNameEn;
	private String committeeLastnameEn;
	private String birthDate;
	private String corporateType;
	private String corporateSign;
	private String taxIdentityNo;
	private String gender;
	private String committeeNationality;
	private String recordStatus;

	
	private String punishmentDateCommittee;
	private String punishmentExpireDateCommittee;

	private String period;
	
	/**
	 * sss.dot.tourism.domain.Person
	 */
	private long personId;
	private long provinceId;
	private String provinceName;
	
	private long amphurId;
	private String amphurName;

	private long prefixId;
	private long postfixId;

	private String prefixName;
	private String postfixName;
	
	private long regId;
	private long traderId;
	
	private long taxProvinceId;
	private String taxProvinceName;
	private long taxAmphurId;
	private String taxAmphurName;
	private String personType;
	private String personIdentityNo;
	private String firstName;
	private String lastName;
	private String personStatus;
	private String punishmentDatePerson;
	private String punishmentExpireDatePerson;
	
	private String traderName;
	private String committeeOwnerName;
	
	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getAmphurName() {
		return amphurName;
	}

	public void setAmphurName(String amphurName) {
		this.amphurName = amphurName;
	}

	public String getTaxProvinceName() {
		return taxProvinceName;
	}

	public void setTaxProvinceName(String taxProvinceName) {
		this.taxProvinceName = taxProvinceName;
	}

	public String getTaxAmphurName() {
		return taxAmphurName;
	}

	public void setTaxAmphurName(String taxAmphurName) {
		this.taxAmphurName = taxAmphurName;
	}

	public String getCommitteePersonTypeName() {
		return committeePersonTypeName;
	}

	public void setCommitteePersonTypeName(String committeePersonTypeName) {
		this.committeePersonTypeName = committeePersonTypeName;
	}

	public String getCommitteeTypeName() {
		return committeeTypeName;
	}

	public void setCommitteeTypeName(String committeeTypeName) {
		this.committeeTypeName = committeeTypeName;
	}

	public String getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}

	public long getRegId() {
		return regId;
	}

	public void setRegId(long regId) {
		this.regId = regId;
	}

	public long getTraderId() {
		return traderId;
	}

	public void setTraderId(long traderId) {
		this.traderId = traderId;
	}

	public String getCommitteeNameEn() {
		return committeeNameEn;
	}

	public void setCommitteeNameEn(String committeeNameEn) {
		this.committeeNameEn = committeeNameEn;
	}

	public String getCommitteeLastnameEn() {
		return committeeLastnameEn;
	}

	public void setCommitteeLastnameEn(String committeeLastnameEn) {
		this.committeeLastnameEn = committeeLastnameEn;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getCorporateType() {
		return corporateType;
	}

	public void setCorporateType(String corporateType) {
		this.corporateType = corporateType;
	}

	public String getCorporateSign() {
		return corporateSign;
	}

	public void setCorporateSign(String corporateSign) {
		this.corporateSign = corporateSign;
	}

	public String getTaxIdentityNo() {
		return taxIdentityNo;
	}

	public void setTaxIdentityNo(String taxIdentityNo) {
		this.taxIdentityNo = taxIdentityNo;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public long getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(long provinceId) {
		this.provinceId = provinceId;
	}

	public long getAmphurId() {
		return amphurId;
	}

	public void setAmphurId(long amphurId) {
		this.amphurId = amphurId;
	}

	public long getCommitteeId() {
		return committeeId;
	}

	public void setCommitteeId(long committeeId) {
		this.committeeId = committeeId;
	}

	public String getCommitteeType() {
		return committeeType;
	}

	public void setCommitteeType(String committeeType) {
		this.committeeType = committeeType;
	}

	public String getCommitteeStatus() {
		return committeeStatus;
	}

	public void setCommitteeStatus(String committeeStatus) {
		this.committeeStatus = committeeStatus;
	}

	public String getCommitteeName() {
		return committeeName;
	}

	public void setCommitteeName(String committeeName) {
		this.committeeName = committeeName;
	}

	public String getCommitteeLastname() {
		return committeeLastname;
	}

	public void setCommitteeLastname(String committeeLastname) {
		this.committeeLastname = committeeLastname;
	}

	public String getCommitteePersonType() {
		return committeePersonType;
	}

	public void setCommitteePersonType(String committeePersonType) {
		this.committeePersonType = committeePersonType;
	}

	public long getPrefixId() {
		return prefixId;
	}

	public void setPrefixId(long prefixId) {
		this.prefixId = prefixId;
	}

	public long getPostfixId() {
		return postfixId;
	}

	public void setPostfixId(long postfixId) {
		this.postfixId = postfixId;
	}

	public long getPersonId() {
		return personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}

	public String getCommitteeIdentityNo() {
		return committeeIdentityNo;
	}

	public void setCommitteeIdentityNo(String committeeIdentityNo) {
		this.committeeIdentityNo = committeeIdentityNo;
	}

	public String getCommitteeNationality() {
		return committeeNationality;
	}

	public void setCommitteeNationality(String committeeNationality) {
		this.committeeNationality = committeeNationality;
	}

	public String getCommitteeIdentityDate() {
		return committeeIdentityDate;
	}

	public void setCommitteeIdentityDate(String committeeIdentityDate) {
		this.committeeIdentityDate = committeeIdentityDate;
	}
	public long getTaxProvinceId() {
		return taxProvinceId;
	}

	public void setTaxProvinceId(long taxProvinceId) {
		this.taxProvinceId = taxProvinceId;
	}

	public long getTaxAmphurId() {
		return taxAmphurId;
	}

	public void setTaxAmphurId(long taxAmphurId) {
		this.taxAmphurId = taxAmphurId;
	}

	public String getPrefixName() {
		return prefixName;
	}

	public void setPrefixName(String prefixName) {
		this.prefixName = prefixName;
	}

	public String getPostfixName() {
		return postfixName;
	}

	public void setPostfixName(String postfixName) {
		this.postfixName = postfixName;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getCommitteeStatusName() {
		return committeeStatusName;
	}

	public void setCommitteeStatusName(String committeeStatusName) {
		this.committeeStatusName = committeeStatusName;
	}

	public String getPersonType() {
		return personType;
	}

	public void setPersonType(String personType) {
		this.personType = personType;
	}

	public String getPrefixNameCommittee() {
		return prefixNameCommittee;
	}

	public void setPrefixNameCommittee(String prefixNameCommittee) {
		this.prefixNameCommittee = prefixNameCommittee;
	}

	public String getPostfixNameCommittee() {
		return postfixNameCommittee;
	}

	public void setPostfixNameCommittee(String postfixNameCommittee) {
		this.postfixNameCommittee = postfixNameCommittee;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPersonStatus() {
		return personStatus;
	}

	public void setPersonStatus(String personStatus) {
		this.personStatus = personStatus;
	}

	public String getPunishmentDateCommittee() {
		return punishmentDateCommittee;
	}

	public void setPunishmentDateCommittee(String punishmentDateCommittee) {
		this.punishmentDateCommittee = punishmentDateCommittee;
	}

	public String getPunishmentExpireDateCommittee() {
		return punishmentExpireDateCommittee;
	}

	public void setPunishmentExpireDateCommittee(String punishmentExpireDateCommittee) {
		this.punishmentExpireDateCommittee = punishmentExpireDateCommittee;
	}

	public String getPunishmentDatePerson() {
		return punishmentDatePerson;
	}

	public void setPunishmentDatePerson(String punishmentDatePerson) {
		this.punishmentDatePerson = punishmentDatePerson;
	}

	public String getPunishmentExpireDatePerson() {
		return punishmentExpireDatePerson;
	}

	public void setPunishmentExpireDatePerson(String punishmentExpireDatePerson) {
		this.punishmentExpireDatePerson = punishmentExpireDatePerson;
	}

	public String getTraderName() {
		return traderName;
	}

	public void setTraderName(String traderName) {
		this.traderName = traderName;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getCommitteeOwnerName() {
		return committeeOwnerName;
	}

	public void setCommitteeOwnerName(String committeeOwnerName) {
		this.committeeOwnerName = committeeOwnerName;
	}

	public String getPersonIdentityNo() {
		return personIdentityNo;
	}

	public void setPersonIdentityNo(String personIdentityNo) {
		this.personIdentityNo = personIdentityNo;
	}
	
	
}
