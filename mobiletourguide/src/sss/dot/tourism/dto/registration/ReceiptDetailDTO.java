package sss.dot.tourism.dto.registration;

import java.io.Serializable;
import java.math.BigDecimal;

public class ReceiptDetailDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8350018537092345800L;
	private long receiptDetailId;
	private String feeName;
	private BigDecimal feeMny;
	
	private long receiptId;
	private long feeId;
	
	private String registrationType;
	private String traderType;
	private long regId;
	
	
	
	public long getRegId() {
		return regId;
	}
	public void setRegId(long regId) {
		this.regId = regId;
	}
	public long getReceiptDetailId() {
		return receiptDetailId;
	}
	public void setReceiptDetailId(long receiptDetailId) {
		this.receiptDetailId = receiptDetailId;
	}
	public String getFeeName() {
		return feeName;
	}
	public void setFeeName(String feeName) {
		this.feeName = feeName;
	}
	public BigDecimal getFeeMny() {
		return feeMny;
	}
	public void setFeeMny(BigDecimal feeMny) {
		this.feeMny = feeMny;
	}
	public long getReceiptId() {
		return receiptId;
	}
	public void setReceiptId(long receiptId) {
		this.receiptId = receiptId;
	}
	public long getFeeId() {
		return feeId;
	}
	public void setFeeId(long feeId) {
		this.feeId = feeId;
	}
	public String getRegistrationType() {
		return registrationType;
	}
	public void setRegistrationType(String registrationType) {
		this.registrationType = registrationType;
	}
	public String getTraderType() {
		return traderType;
	}
	public void setTraderType(String traderType) {
		this.traderType = traderType;
	}
	
	
}
