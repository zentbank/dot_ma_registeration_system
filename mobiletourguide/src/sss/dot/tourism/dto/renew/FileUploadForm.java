package sss.dot.tourism.dto.renew;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public class FileUploadForm {
 
	private long traderId;
    private List<MultipartFile> files;
    private String email;
    
	public long getTraderId() {
		return traderId;
	}
	public void setTraderId(long traderId) {
		this.traderId = traderId;
	}
	public List<MultipartFile> getFiles() {
		return files;
	}
	public void setFiles(List<MultipartFile> files) {
		this.files = files;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
     

}
