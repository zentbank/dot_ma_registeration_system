package sss.dot.tourism.http;




public class HttpMessageFactory
{
	public static HttpMessage create(HttpMessageType type)
	{
		HttpMessage httpMessage = null;
		switch (type)
		{
		case JSON:
		{
			//httpMessage = new JsonHttpMessage(response, new JSONPerform());
			httpMessage = new JsonHttpMessage();
			break;
		}
		case XML:
		{
			//httpMessage = new XMLHttpMessage(response, new XMLPerform());
			httpMessage = new XMLHttpMessage();
			break;
		}
		

		}
		return httpMessage;
	}
}
