package sss.dot.tourism.http;



import java.io.IOException;
import java.io.Writer;
import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.basic.BigDecimalConverter;
import com.thoughtworks.xstream.converters.basic.DoubleConverter;
import com.thoughtworks.xstream.converters.basic.IntConverter;
import com.thoughtworks.xstream.converters.basic.LongConverter;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;
import com.thoughtworks.xstream.io.json.JsonHierarchicalStreamDriver;
import com.thoughtworks.xstream.io.json.JsonWriter;

public class JsonHttpMessage extends HttpMessage
{

	public JsonHttpMessage(HttpServletResponse response,
			IWriteMessagePerform writeMsgPerform)
	{
		super(response, writeMsgPerform);
	}

	public JsonHttpMessage()
	{
		super();
		this.init();
	}
	
	private void init()
	  {
	    /*
	     * used JsonHierarchicalStreamDriver which A driver for JSON that writes
	     * optimized JSON format, but is not able to deserialize the result.
	     */
	    XStream serializer = new XStream(new JsonHierarchicalStreamDriver()
	    {
	        public HierarchicalStreamWriter createWriter(Writer writer)
	        {
	          return new JsonWriter(writer, JsonWriter.DROP_ROOT_MODE);
	        }
	      });
	    /*
	     * Since JSON has no possibility to express references, you should always
	     * set the NO_REFERENCES mode writing JSON
	     */
	    serializer.setMode(XStream.NO_REFERENCES);
	    /**
	     * convert "" to 0 or null
	     */
	    serializer.registerConverter(new JsonHttpMessage.BigDecimalExtendConverter());
	    serializer.registerConverter(new JsonHttpMessage.IntegerExtendConverter());
	    serializer.registerConverter(new JsonHttpMessage.LongExtendConverter());
//	    serializer.registerConverter(new JsonHttpMessage.ExtDateConverter());
	    serializer.registerConverter(new JsonHttpMessage.DoubleExtendConverter());
	    
	    serializer.aliasSystemAttribute(null, "class");
	    
	    super.setSerializer(serializer);
	    
	    XStream deserializer = new XStream(new JettisonMappedXmlDriver());


	    deserializer.registerConverter(new JsonHttpMessage.BigDecimalExtendConverter());
	    deserializer.registerConverter(new JsonHttpMessage.IntegerExtendConverter());
	    deserializer.registerConverter(new JsonHttpMessage.LongExtendConverter());
//	    deserializer.registerConverter(new JsonHttpMessage.ExtDateConverter());
	    deserializer.registerConverter(new JsonHttpMessage.DoubleExtendConverter());
	    
	    super.setDeserializer(deserializer);


	  }

	@Override
	public String transform(Object object) throws IOException
	{
		return super.serialize(object);
		
	}

	@Override
	public void write(Object object) throws IOException
	{
		//super.serialize(object);

	}
	
	public String writePagingMessage(Object object, Number total) throws Exception
	  {
		MessageWrapper<?> message =  new MessageWrapper((List<?>)object, true , total);
	    return super.serialize(message);
	  }
	
	  public String writeMessage(Object object) throws Exception
	  {
		  MessageWrapper<?> message = null;
		  if(object instanceof java.util.List)
		  {
			  message =  new MessageWrapper((List<?>)object, true);
		  }
		  else
		  {
			  message =  new MessageWrapper(object, true);
		  }
		  
		  return super.serialize(message);
	  }
	  
	  public String writeInfoMessage(Object message)
	  {	

		  
		  try{
			  return super.serialize(message);
		  }
		  catch(Exception e)
		  {
			  e.printStackTrace();
			  return null;
		  }
	  }
	  

	  
	  public String writeErrorMessage(Object object)
	  {
		  try{
			  MessageWrapper<?> message =  new MessageWrapper(object , false);
			  return super.serialize(message);
		  }
		  catch(Exception e)
		  {
			  e.printStackTrace();
			  return null;
		  }

	  }
	  
	  
	  public String writeMessage() throws Exception
	  {
		  MessageWrapper<?> message =  new MessageWrapper(true);
		  return super.serialize(message);
	  }
	

	@Override
	public Object[] toArrayObject(Class alias, String data)
	{
	
		data = "{\"" + alias.getName() + "-array\":{\"" + alias.getName() + "\":" + data+ "}}";
		
		System.out.println("json= " + data);
	    return super.deserializeToArray(alias, data);
	}

	@Override
	public Object toObject(Class alias, String data)
	{
	    return super.deserializeTo(data);
	}

	  @Override
	public Object[] toArrayObject(String registerName, Class alias, String data) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object toObject(String registerName, Class alias, String data) {
		// TODO Auto-generated method stub
		return null;
	}

	private static class LongExtendConverter extends LongConverter
	  {

	    /**
	     * @see com.thoughtworks.xstream.converters.basic.BigDecimalConverter#canConvert(java.lang.Class)
	     */

	    public boolean canConvert(Class type)
	    {
	      return super.canConvert(type);
	    }

	    /**
	     * @see com.thoughtworks.xstream.converters.basic.BigDecimalConverter#fromString(java.lang.String)
	     */

	    public Object fromString(String str)
	    {
	      if (str == null || str.trim().length() == 0)
	      {
	        str = "0";
	      }
	      return super.fromString(str);
	    }

	  }
	  
	  
	  /**
	   * inner class BigDecimalExtendConverter
	   * 
	   * @author Pradit
	   * @since 25 Dec 2008
	   * @version 1.0.1
	   * @category CommonUtil JsonHttpMessage.java
	   */
	  private static class BigDecimalExtendConverter extends BigDecimalConverter
	  {
	    protected static final String ZERO = "0";

	    /**
	     * @see com.thoughtworks.xstream.converters.basic.BigDecimalConverter#canConvert(java.lang.Class)
	     */

	    public boolean canConvert(Class type)
	    {
	      return super.canConvert(type);
	    }

	    /**
	     * @see com.thoughtworks.xstream.converters.basic.BigDecimalConverter#fromString(java.lang.String)
	     */

	    public Object fromString(String str)
	    {
	      if (str == null || str.trim().length() == 0)
	      {
	        str = ZERO;
	      }
	      return super.fromString(str);
	    }

	  }

	  /**
	   * inner class IntegerExtendConverter
	   * 
	   * @author Pradit
	   * @since 25 Dec 2008
	   * @version 1.0.1
	   * @category CommonUtil JsonHttpMessage.java
	   */
	  private static class IntegerExtendConverter extends IntConverter
	  {

	    /**
	     * @see com.thoughtworks.xstream.converters.basic.BigDecimalConverter#canConvert(java.lang.Class)
	     */

	    public boolean canConvert(Class type)
	    {
	      return super.canConvert(type);
	    }

	    /**
	     * @see com.thoughtworks.xstream.converters.basic.BigDecimalConverter#fromString(java.lang.String)
	     */

	    public Object fromString(String str)
	    {
	      if (str == null || str.trim().length() == 0)
	      {
	        str = JsonHttpMessage.BigDecimalExtendConverter.ZERO;
	      }
	      return super.fromString(str);
	    }

	  }

	  /**
	   * 
	   * @author Pradit
	   * @since 25 Dec 2008
	   * @version 1.0.1
	   * @category CommonUtil JsonHttpMessage.java
	   */
	  private static class DoubleExtendConverter extends DoubleConverter
	  {

	    /**
	     * @see com.thoughtworks.xstream.converters.basic.BigDecimalConverter#canConvert(java.lang.Class)
	     */

	    public boolean canConvert(Class type)
	    {
	      return super.canConvert(type);
	    }

	    /**
	     * @see com.thoughtworks.xstream.converters.basic.BigDecimalConverter#fromString(java.lang.String)
	     */

	    public Object fromString(String str)
	    {

	      if (str == null || str.trim().length() == 0)
	      {
	        str = JsonHttpMessage.BigDecimalExtendConverter.ZERO;
	      }
	      Number number = null;
	      try
	      {
	    	  number = NumberFormatFactory.getCurrencyFormat().parse(str);
	      }
	      catch (ParseException px)
	      {
	        number = Double.valueOf(JsonHttpMessage.BigDecimalExtendConverter.ZERO);
	      }

	      return super.fromString(String.valueOf(number));
	    }

	  }
	

}
