package sss.dot.tourism.http;



import java.io.Serializable;
import java.util.List;

public class MessageWrapper<E> implements Serializable {
	
	private Integer totalCount = 0;
	private List<E> list;
	private boolean success;
	private String msg;
	private Object data;

	public MessageWrapper(boolean success) {
		this.success = success;
	}

	public MessageWrapper(String msg ,boolean success) {
		this.msg = msg;
		this.success = success;
	}

	public MessageWrapper(List<E> data, boolean success) {
		this.list = (List<E>) data;
		this.success = success;
	}


	public MessageWrapper(Object data, boolean success) {
		this.data = data;
		this.success = success;
	}

	public MessageWrapper(List<E> data, boolean success, Integer total) {
		this.list = (List<E>) data;
		this.success = success;
		this.totalCount = total;
	}
	
	public MessageWrapper(List<E> data, boolean success, Number total) {
		this.list = (List<E>) data;
		this.success = success;
		this.totalCount = total.intValue();
	}



	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public List<E> getList() {
		return list;
	}

	public void setList(List<E> list) {
		this.list = list;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}
