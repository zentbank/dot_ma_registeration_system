package sss.dot.tourism.joborder.controller;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import sss.dot.tourism.joborder.dto.JobOrderDTO;
import sss.dot.tourism.joborder.service.IJobOrderService;

@Controller
@RequestMapping("/joborder/itinerary")
public class ItineraryController {
	
	@Autowired
	private IJobOrderService jobOrderService;
	
	@RequestMapping( value = "/showAddLocal", method=RequestMethod.POST )
	public  String view(@ModelAttribute JobOrderDTO param, Model model) {

		try {
			this.jobOrderService.getLocalItinerary(param);
			model.addAttribute("model", param);
			return "/page/joborder/LocalItineraryAddEdit.jsp";

		} catch (Exception e) {
			e.printStackTrace();			
			return "/page/pageerror.jsp";
	
		}
	}
	
	@RequestMapping(value = "/showItinerary/{token}/{jobId}", method = {RequestMethod.GET, RequestMethod.POST})
	public String shawItinerary(@PathVariable("token") String token ,@PathVariable("jobId") String jobId, Model model) {
		JobOrderDTO param = new JobOrderDTO();
		try {
			
			if(StringUtils.isNotEmpty(jobId)){
				param.setJobId(Long.valueOf(jobId));
			}
			
			param.setToken(token);
			
			Object obj = this.jobOrderService.getItinerary(param);
			
			model.addAttribute("model", obj);

			return "/page/joborder/ItineraryView.jsp";

		} catch (Exception e) {
			e.printStackTrace();
			param.setErrMsg(e.getMessage());
			model.addAttribute("model", param);
			return "/page/joborder/ItineraryView.jsp";

		}
	}
	
	@RequestMapping(value = "/saveItinerary", method = RequestMethod.POST)
	public String saveItinerary(@ModelAttribute JobOrderDTO param, Model model) {

		try {

			this.jobOrderService.saveItinerary(param);
			return "redirect:/service/joborder/itinerary/showLocalItinerary/"+ param.getToken()+"/"+param.getJobId();

		} catch (Exception e) {
			e.printStackTrace();
			param.setErrMsg(e.getMessage());
			model.addAttribute("error", "ไม่สามารถบันทึกข้อมูลได้");
			return "/page/joborder/ItineraryView.jsp";

		}
	}
	
	@RequestMapping(value = "/showLocalItinerary/{token}/{jobId}", method = {RequestMethod.GET, RequestMethod.POST})
	public String showLocalItinerary(@PathVariable("token") String token ,@PathVariable("jobId") String jobId, Model model) {

		JobOrderDTO param = new JobOrderDTO();
		try {
			
			if(StringUtils.isNotEmpty(jobId)){
				param.setJobId(Long.valueOf(jobId));
			}
			
			param.setToken(token);			
			List list = this.jobOrderService.getLocalItineraryAll(param);
			model.addAttribute("model", param);
			model.addAttribute("itinerarys", list);

			return "/page/joborder/LocalItineraryView.jsp";

		} catch (Exception e) {
			e.printStackTrace();
			param.setErrMsg(e.getMessage());
			model.addAttribute("model", param);
			return "/page/joborder/ItineraryView.jsp";

		}
	}
	
	@RequestMapping(value = "/addLocalItinerary", method = RequestMethod.POST)
	public String addLocalItinerary(@ModelAttribute JobOrderDTO param, Model model) {

		try {

			jobOrderService.saveLocalItinerary(param);
			model.addAttribute("model", param);
			
			return "redirect:/service/joborder/itinerary/showLocalItinerary/"+ param.getToken()+"/"+param.getJobId();

		} catch (Exception e) {
			e.printStackTrace();
			param.setErrMsg(e.getMessage());
			model.addAttribute("model", param);
			return "/page/joborder/ItineraryView.jsp";

		}
	}
}
