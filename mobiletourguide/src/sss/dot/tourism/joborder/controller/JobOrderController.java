package sss.dot.tourism.joborder.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import sss.dot.tourism.controller.report.ReportExcelBuilder;
import sss.dot.tourism.joborder.dto.JobOrderDTO;
import sss.dot.tourism.joborder.service.IJobOrderService;


@Controller
@RequestMapping("/joborder")
public class JobOrderController {

	@Autowired
	private IJobOrderService jobOrderService;
	
	@RequestMapping(value = "/read", method = RequestMethod.POST)
	public String read(@RequestParam("token") String token, Model model) {

		try {

			System.out.println("joborder");
			
			JobOrderDTO param = new JobOrderDTO();
			param.setToken(token);
			List list = this.jobOrderService.getJobOrderAll(param);
			
			String licenseNo = "";
			
			model.addAttribute("token", param.getToken());
			model.addAttribute("licenseNo", param.getLicenseNo());
			model.addAttribute("joborders",list);

			return "/page/joborder/JobOrderListView.jsp";

		} catch (Exception e) {
			e.printStackTrace();
			return "/page/pageerror.jsp";

		}
	}
	
	@RequestMapping(value = "/jobOwnerdetail", method = RequestMethod.POST)
	public String jobguidedetail(@RequestParam("token") String token,@RequestParam("jobId") String jobId, Model model) {

		try {

			JobOrderDTO param = new JobOrderDTO();
			param.setToken(token);
			if(StringUtils.isNotEmpty(jobId)){
				param.setJobId(Long.valueOf(jobId));
			}
			
			System.out.println("token= " + token);
			param.setToken(token);
			Object obj = this.jobOrderService.getJobOrder(param);
			
			model.addAttribute("model", obj);

			return "/page/joborder/JobOwnerDetail.jsp";

		} catch (Exception e) {
			e.printStackTrace();
			return "/page/pageerror.jsp";

		}
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String jobguidedetail(@ModelAttribute JobOrderDTO param, Model model) {

		try {

			this.jobOrderService.saveJobOrder(param);
			return "redirect:/service/joborder/itinerary/showItinerary/"+ param.getToken()+"/"+param.getJobId();

		} catch (Exception e) {
			e.printStackTrace();
			param.setErrMsg(e.getMessage());
			model.addAttribute("model", param);
			return "/page/joborder/JobOwnerDetail.jsp";

		}
	}

	
	@RequestMapping(value = "/tourisName", method = RequestMethod.POST)
	public String tourisName(@ModelAttribute JobOrderDTO param, Model model) {

		try {

			System.out.println("getJobId= " + param.getJobId());
			System.out.println("getToken= " + param.getToken());
			
			model.addAttribute("token", param.getToken());
			
			model.addAttribute("model", param);

			return "/page/joborder/TourisNameView.jsp";

		} catch (Exception e) {
			e.printStackTrace();
			param.setErrMsg(e.getMessage());
			model.addAttribute("model", param);
			return "/page/joborder/ItineraryView.jsp";

		}
	}
	
	
	 @RequestMapping(value = "/excel/read" , method = RequestMethod.GET)
	    public void printJoborder(@RequestParam("jobId") long jobId,HttpServletRequest request, HttpServletResponse resp)
	    {
//	      	try {
//		    	
//	      		JobOrderDTO param = new JobOrderDTO();
//		    	param.setJobId(jobId);
//		    	this.jobOrderService.getPrintData(param, model);
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//			
//	        return "joborderDocx";
		   
			try
		    {
				JobOrderDTO param = new JobOrderDTO();
		    	param.setJobId(jobId);
		    	
				resp.setContentType("application/vnd.ms-excel");
				resp.setHeader("Content-Disposition",
						"attachment; filename=joborder.xls");
		
				String templateFileName = "joborder.xls";
				Map model = new HashMap();
				ReportExcelBuilder builder = new ReportExcelBuilder(templateFileName);
				
				this.jobOrderService.getPrintData(param, model);
				
				builder.build(resp, model);

		    }
			catch(Exception e)
			{
				e.printStackTrace();
			}
	    }
}
