package sss.dot.tourism.joborder.controller;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import sss.dot.tourism.joborder.dto.JobOrderDTO;
import sss.dot.tourism.joborder.service.IJobOrderService;

@Controller
@RequestMapping("/joborder/travelprogram")
public class TravelProgramController {
	
	@Autowired
	private IJobOrderService jobOrderService;
	
	@RequestMapping(value = "/view/{token}/{jobId}", method={RequestMethod.GET, RequestMethod.POST} )
	public  String view(@PathVariable("token") String token ,@PathVariable("jobId") String jobId, Model model) {

		JobOrderDTO param = new JobOrderDTO();
		try {
			
			if(StringUtils.isNotEmpty(jobId)){
				param.setJobId(Long.valueOf(jobId));
			}
			
			param.setToken(token);
			
			List list = this.jobOrderService.getTravelProgramAll(param);
			
			model.addAttribute("model", param);
			model.addAttribute("travel", list);
			
			return "/page/joborder/TravelProgram.jsp";

		} catch (Exception e) {
			e.printStackTrace();			
			return "/page/pageerror.jsp";
	
		}
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String add(@ModelAttribute JobOrderDTO param, Model model) {

		try {

			this.jobOrderService.getTravelProgram(param);
			
			model.addAttribute("model", param);

			return "/page/joborder/TravelProgramAddEdit.jsp";

		} catch (Exception e) {
			e.printStackTrace();
			param.setErrMsg(e.getMessage());
			model.addAttribute("model", param);
			return "/page/joborder/TravelProgram.jsp";

		}
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(@ModelAttribute JobOrderDTO param, Model model) {

		try {

			this.jobOrderService.saveTravelProgram(param);
			return "redirect:/service/joborder/travelprogram/view/"+ param.getToken()+"/"+param.getJobId();


		} catch (Exception e) {
			e.printStackTrace();
			param.setErrMsg(e.getMessage());
			model.addAttribute("model", param);
			return "/page/joborder/TravelProgramAddEdit.jsp";

		}
	}
}
