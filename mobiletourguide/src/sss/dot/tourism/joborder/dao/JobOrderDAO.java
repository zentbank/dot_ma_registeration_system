package sss.dot.tourism.joborder.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.JobOrder;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.joborder.dto.JobOrderDTO;

@Repository("jobOrderDAO")
public class JobOrderDAO extends BaseDAO{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4305356174219290099L;

	public JobOrderDAO()
	{
		this.domainObj = JobOrder.class;
	}
	
	public List<JobOrder> findAll(JobOrderDTO param)
	{
		ArrayList params = new ArrayList();
		
		StringBuilder hql = new StringBuilder();
		hql.append(" from  JobOrder as tr ");
		hql.append(" where tr.recordStatus = 'N' ");

		if ((param.getLicenseNo() != null) && (!param.getLicenseNo().equals(""))) {
			hql.append(" and tr.licenseNo = ? ");
			params.add(param.getLicenseNo());
		}
		
		List<JobOrder> list = (List<JobOrder>) this.getHibernateTemplate().find(hql.toString(), params.toArray());

		return list;
	}
	
	public List<Object[]> findAll(JobOrderDTO param,int start, int limit)
	  {
		   Map<String,Object> parames = new HashMap<String,Object>();
		    
		    StringBuilder hql = new StringBuilder();
		    
		    hql.append(" SELECT  ");
		    hql.append(" TR.LICENSE_NO AS LICENSE_NO ");
		    hql.append(" ,TR.TRADER_NAME AS TRADER_NAME ");
		    hql.append(" ,J.JOB_NO AS JOB_NO ");
		    hql.append(" ,J.JOB_DATE AS JOB_DATE ");
		    hql.append(" ,J.FOREIGN_NAME AS FOREIGN_NAME ");
		    hql.append(" ,J.FOREIGN_LICENSE_NO AS FOREIGN_LICENSE_NO ");
		    hql.append(" ,J.GUIDE_LICENSE_NO AS GUIDE_LICENSE_NO ");
		    hql.append(" ,J.GUIDE_NAME AS GUIDE_NAME ");
		    hql.append(" ,J.CREATE_DTM AS CREATE_DTM ");
		    hql.append(" ,J.JOB_ID AS JOB_ID ");
		    hql.append(" ,J.GUIDE_SALARY AS GUIDE_SALARY ");
		    hql.append("  FROM JOB_ORDER J ");
		    hql.append(" INNER JOIN TRADER TR ");
		    hql.append(" ON (J.LICENSE_NO = TR.LICENSE_NO) ");
		    hql.append(" WHERE  TR.RECORD_STATUS = 'N' ");
		    hql.append(" AND TR.BRANCH_TYPE IS NULL ");
		    hql.append(" AND J.RECORD_STATUS = 'N' ");
		    
		    
		    if((param.getLicenseNo() != null) && (!param.getLicenseNo().isEmpty()))
		    {
		    	hql.append("  AND TR.LICENSE_NO = :LICENSE_NO ");
		    	parames.put("LICENSE_NO", param.getLicenseNo());
		    }
		    
		    if((param.getTraderName() != null) && (!param.getTraderName().isEmpty()))
		    {
		    	hql.append("  AND TR.TRADER_NAME LIKE :TRADER_NAME ");
		    	parames.put("TRADER_NAME", "%"+param.getTraderName()+"%");
		    }
		    if(param.getJobId() > 0){
		    	hql.append("  AND J.JOB_ID = :JOB_ID ");
		    	parames.put("JOB_ID", param.getJobId());
		    }
		   
		    hql.append(" ORDER BY J.CREATE_DTM DESC; ");
		    
		    SQLQuery sqlQuery = getSession().createSQLQuery(hql.toString());
		    sqlQuery.setFirstResult(start);
		    sqlQuery.setMaxResults(limit);
		    
		   
			
		    sqlQuery.addScalar("LICENSE_NO", Hibernate.STRING); //0
		    sqlQuery.addScalar("TRADER_NAME", Hibernate.STRING); //1
		    sqlQuery.addScalar("JOB_NO", Hibernate.STRING); //2
		    sqlQuery.addScalar("JOB_DATE", Hibernate.STRING); //3
		    sqlQuery.addScalar("FOREIGN_NAME", Hibernate.STRING); //4
		    sqlQuery.addScalar("FOREIGN_LICENSE_NO", Hibernate.STRING); //5
		    sqlQuery.addScalar("GUIDE_LICENSE_NO", Hibernate.STRING); //6
		    sqlQuery.addScalar("GUIDE_NAME", Hibernate.STRING); //7
		    sqlQuery.addScalar("CREATE_DTM", Hibernate.DATE); //8
		    sqlQuery.addScalar("JOB_ID", Hibernate.LONG); //9
		    sqlQuery.addScalar("GUIDE_SALARY", Hibernate.BIG_DECIMAL); //10
		    
		    sqlQuery.setProperties(parames);
		    List<Object[]>  result = sqlQuery.list();
	  
		    return result;
	  }
	
}
