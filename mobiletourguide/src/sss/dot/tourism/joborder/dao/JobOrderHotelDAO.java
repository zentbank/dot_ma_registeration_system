package sss.dot.tourism.joborder.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.JobOrderHotel;
import sss.dot.tourism.domain.JobOrderItinerary;
import sss.dot.tourism.joborder.dto.JobOrderDTO;

@Repository("jobOrderHotelDAO")
public class JobOrderHotelDAO extends BaseDAO {


	/**
	 * 
	 */
	private static final long serialVersionUID = -5800132875043253251L;

	public JobOrderHotelDAO()
	{
		this.domainObj = JobOrderHotel.class;
	}
	
	public List<JobOrderHotel> findHotel(JobOrderDTO param)
	{
		ArrayList params = new ArrayList();
		
		StringBuilder hql = new StringBuilder();
		hql.append(" from  JobOrderHotel as tr ");
		hql.append(" where tr.recordStatus = 'N' ");
		
		if(StringUtils.isNotEmpty(param.getTourisType())){
			hql.append(" and tr.tourisType = ? ");
			params.add(param.getTourisType());
		}
		if(param.getJobId() > 0){
			hql.append(" and tr.jobOrder.jobId = ? ");
			params.add(param.getJobId());
		}

		List<JobOrderHotel> list = (List<JobOrderHotel>) this.getHibernateTemplate().find(hql.toString(), params.toArray());

		return list;
	}

}
