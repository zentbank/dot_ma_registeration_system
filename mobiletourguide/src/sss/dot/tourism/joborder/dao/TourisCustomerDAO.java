package sss.dot.tourism.joborder.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.JobOrder;
import sss.dot.tourism.domain.JobOrderItinerary;
import sss.dot.tourism.domain.TourisCustomer;
import sss.dot.tourism.joborder.dto.JobOrderDTO;

@Repository("tourisCustomerDAO")
public class TourisCustomerDAO extends BaseDAO{
	public TourisCustomerDAO()
	{
		this.domainObj = TourisCustomer.class;
	}
	
	public List<TourisCustomer> findAllTourisCustomer(JobOrderDTO param)
	{
		ArrayList params = new ArrayList();
		
		StringBuilder hql = new StringBuilder();
		hql.append(" from  TourisCustomer as tr ");
		hql.append(" where tr.recordStatus = 'N' ");
		
		if(StringUtils.isNotEmpty(param.getTourisType())){
			hql.append(" and tr.tourisType = ? ");
			params.add(param.getTourisType());
		}
		if(param.getJobId() > 0){
			hql.append(" and tr.jobOrder.jobId = ? ");
			params.add(param.getJobId());
		}

		List<TourisCustomer> list = (List<TourisCustomer>) this.getHibernateTemplate().find(hql.toString(), params.toArray());

		return list;
	}
}
