package sss.dot.tourism.joborder.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.TourisCustomer;
import sss.dot.tourism.domain.TravelProgram;
import sss.dot.tourism.joborder.dto.JobOrderDTO;

@Repository("travelProgramDAO")
public class TravelProgramDAO extends BaseDAO {


	/**
	 * 
	 */
	private static final long serialVersionUID = -2926907917211502552L;

	public TravelProgramDAO()
	{
		this.domainObj = TravelProgram.class;
	}
	public List<TravelProgram> findAllProgram(JobOrderDTO param)
	{
		ArrayList params = new ArrayList();
		
		StringBuilder hql = new StringBuilder();
		hql.append(" from  TravelProgram as tr ");
		hql.append(" where tr.recordStatus = 'N' ");
		
		
		if(param.getJobId() > 0){
			hql.append(" and tr.jobOrder.jobId = ? ");
			params.add(param.getJobId());
		}

		List<TravelProgram> list = (List<TravelProgram>) this.getHibernateTemplate().find(hql.toString(), params.toArray());

		return list;
	}
}
