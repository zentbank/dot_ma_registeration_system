package sss.dot.tourism.joborder.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import sss.dot.tourism.dto.info.LicenseDetailDTO;

public class JobOrderDTO extends LicenseDetailDTO implements Serializable,Comparator<JobOrderDTO> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -593407793042741320L;
	
	private long jobId;
	private String jobNo;
	private String jobDate;
	private String foreignName;
	private String foreignLicenseNo;
	private String guideName;
	private String guideLicenseNo;
	
	private BigDecimal guideSalary;
	private String traderName;
	
	private String token;
	
	private long itrId;
	private String itineraryType;
	private String itineraryBy;
	private String itineraryDate;
	private String itineraryFlight;
	private String itineraryFrom;
	private String leaveTime;
	private String itineraryTo;
	private String arriveTime;
	private String roundTrip;
	
	
//	เดินทางโดย
	private long arriveId;
	private String arriveBy;
	private String arriveType;
//	วัน เดือน ปี เดินทางมาถึง
	private String arriveDate;
//	เที่ยวบิน
	private String arriveFlightNo;
	//จาก
	private String arriveFrom;
	//ถึงกรุงเทพเวลา
//	private String arriveTime;

	//ประเภทการเดินทาง
	private long leaveId;
	private String leaveBy;
	private String leaveType;
	//วัน เดือน ปี เดินทางกลับ
	private String leaveDate;
	//เที่ยวบิน
	private String leaveFlightNo;
	private String leaveTo;
	//ออกจากกรุงเทพเวลา
//	private String leaveTime;
	
	private int tourisAdult;
	private int tourisChild;
	private int tourisStraff;
	
	private long tourisId;
	private String tourisName;
	private String tourisType;
	private String cusNatinality;
	
	private long typeAHotelId;
	private Integer typeARoomType1;
	private Integer typeARoomType2;
	private Integer typeARoomType3;
	private String typeARemark;
	
	private long typeCHotelId;
	private Integer typeCRoomType1;
	private Integer typeCRoomType2;
	private Integer typeCRoomType3;
	private String typeCRemark;
	
	private long typeSHotelId;
	private Integer typeSRoomType1;
	private Integer typeSRoomType2;
	private Integer typeSRoomType3;
	private String typeSRemark;
	
	private long progId;
	private String progDate;
	private String progTime;
	private String progFood;
	private String progDesc;
	private String progHotel;
	private String createUser;
	
	private List<MultipartFile> tourisNames;
	
	

	
	public String getCusNatinality() {
		return cusNatinality;
	}
	public void setCusNatinality(String cusNatinality) {
		this.cusNatinality = cusNatinality;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public long getProgId() {
		return progId;
	}
	public void setProgId(long progId) {
		this.progId = progId;
	}
	public String getProgDate() {
		return progDate;
	}
	public void setProgDate(String progDate) {
		this.progDate = progDate;
	}
	public String getProgTime() {
		return progTime;
	}
	public void setProgTime(String progTime) {
		this.progTime = progTime;
	}
	public String getProgFood() {
		return progFood;
	}
	public void setProgFood(String progFood) {
		this.progFood = progFood;
	}
	public String getProgDesc() {
		return progDesc;
	}
	public void setProgDesc(String progDesc) {
		this.progDesc = progDesc;
	}
	public String getProgHotel() {
		return progHotel;
	}
	public void setProgHotel(String progHotel) {
		this.progHotel = progHotel;
	}
	public long getTypeAHotelId() {
		return typeAHotelId;
	}
	public void setTypeAHotelId(long typeAHotelId) {
		this.typeAHotelId = typeAHotelId;
	}
	public Integer getTypeARoomType1() {
		return typeARoomType1;
	}
	public void setTypeARoomType1(Integer typeARoomType1) {
		this.typeARoomType1 = typeARoomType1;
	}
	public Integer getTypeARoomType2() {
		return typeARoomType2;
	}
	public void setTypeARoomType2(Integer typeARoomType2) {
		this.typeARoomType2 = typeARoomType2;
	}
	public Integer getTypeARoomType3() {
		return typeARoomType3;
	}
	public void setTypeARoomType3(Integer typeARoomType3) {
		this.typeARoomType3 = typeARoomType3;
	}
	public String getTypeARemark() {
		return typeARemark;
	}
	public void setTypeARemark(String typeARemark) {
		this.typeARemark = typeARemark;
	}
	public long getTypeCHotelId() {
		return typeCHotelId;
	}
	public void setTypeCHotelId(long typeCHotelId) {
		this.typeCHotelId = typeCHotelId;
	}
	public Integer getTypeCRoomType1() {
		return typeCRoomType1;
	}
	public void setTypeCRoomType1(Integer typeCRoomType1) {
		this.typeCRoomType1 = typeCRoomType1;
	}
	public Integer getTypeCRoomType2() {
		return typeCRoomType2;
	}
	public void setTypeCRoomType2(Integer typeCRoomType2) {
		this.typeCRoomType2 = typeCRoomType2;
	}
	public Integer getTypeCRoomType3() {
		return typeCRoomType3;
	}
	public void setTypeCRoomType3(Integer typeCRoomType3) {
		this.typeCRoomType3 = typeCRoomType3;
	}
	public String getTypeCRemark() {
		return typeCRemark;
	}
	public void setTypeCRemark(String typeCRemark) {
		this.typeCRemark = typeCRemark;
	}
	public long getTypeSHotelId() {
		return typeSHotelId;
	}
	public void setTypeSHotelId(long typeSHotelId) {
		this.typeSHotelId = typeSHotelId;
	}
	public Integer getTypeSRoomType1() {
		return typeSRoomType1;
	}
	public void setTypeSRoomType1(Integer typeSRoomType1) {
		this.typeSRoomType1 = typeSRoomType1;
	}
	public Integer getTypeSRoomType2() {
		return typeSRoomType2;
	}
	public void setTypeSRoomType2(Integer typeSRoomType2) {
		this.typeSRoomType2 = typeSRoomType2;
	}
	public Integer getTypeSRoomType3() {
		return typeSRoomType3;
	}
	public void setTypeSRoomType3(Integer typeSRoomType3) {
		this.typeSRoomType3 = typeSRoomType3;
	}
	public String getTypeSRemark() {
		return typeSRemark;
	}
	public void setTypeSRemark(String typeSRemark) {
		this.typeSRemark = typeSRemark;
	}
	public long getTourisId() {
		return tourisId;
	}
	public void setTourisId(long tourisId) {
		this.tourisId = tourisId;
	}
	public long getArriveId() {
		return arriveId;
	}
	public void setArriveId(long arriveId) {
		this.arriveId = arriveId;
	}
	public long getLeaveId() {
		return leaveId;
	}
	public void setLeaveId(long leaveId) {
		this.leaveId = leaveId;
	}
	public String getLeaveBy() {
		return leaveBy;
	}
	public void setLeaveBy(String leaveBy) {
		this.leaveBy = leaveBy;
	}
	public String getItineraryType() {
		return itineraryType;
	}
	public void setItineraryType(String itineraryType) {
		this.itineraryType = itineraryType;
	}
	public String getItineraryBy() {
		return itineraryBy;
	}
	public void setItineraryBy(String itineraryBy) {
		this.itineraryBy = itineraryBy;
	}
	public String getItineraryDate() {
		return itineraryDate;
	}
	public void setItineraryDate(String itineraryDate) {
		this.itineraryDate = itineraryDate;
	}
	public String getItineraryFlight() {
		return itineraryFlight;
	}
	public void setItineraryFlight(String itineraryFlight) {
		this.itineraryFlight = itineraryFlight;
	}
	public String getRoundTrip() {
		return roundTrip;
	}
	public void setRoundTrip(String roundTrip) {
		this.roundTrip = roundTrip;
	}
	public String getArriveBy() {
		return arriveBy;
	}
	public void setArriveBy(String arriveBy) {
		this.arriveBy = arriveBy;
	}
	public String getArriveType() {
		return arriveType;
	}
	public void setArriveType(String arriveType) {
		this.arriveType = arriveType;
	}
	public String getArriveDate() {
		return arriveDate;
	}
	public void setArriveDate(String arriveDate) {
		this.arriveDate = arriveDate;
	}
	public String getArriveFlightNo() {
		return arriveFlightNo;
	}
	public void setArriveFlightNo(String arriveFlightNo) {
		this.arriveFlightNo = arriveFlightNo;
	}
	public String getArriveFrom() {
		return arriveFrom;
	}
	public void setArriveFrom(String arriveFrom) {
		this.arriveFrom = arriveFrom;
	}
	public String getLeaveType() {
		return leaveType;
	}
	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}
	public String getLeaveDate() {
		return leaveDate;
	}
	public void setLeaveDate(String leaveDate) {
		this.leaveDate = leaveDate;
	}
	public String getLeaveFlightNo() {
		return leaveFlightNo;
	}
	public void setLeaveFlightNo(String leaveFlightNo) {
		this.leaveFlightNo = leaveFlightNo;
	}
	public String getLeaveTo() {
		return leaveTo;
	}
	public void setLeaveTo(String leaveTo) {
		this.leaveTo = leaveTo;
	}
	public String getTraderName() {
		return traderName;
	}
	public void setTraderName(String traderName) {
		this.traderName = traderName;
	}
	public String getJobNo() {
		return jobNo;
	}
	public void setJobNo(String jobNo) {
		this.jobNo = jobNo;
	}
	
	public String getTourisName() {
		return tourisName;
	}
	public void setTourisName(String tourisName) {
		this.tourisName = tourisName;
	}
	public String getTourisType() {
		return tourisType;
	}
	public void setTourisType(String tourisType) {
		this.tourisType = tourisType;
	}
	public int getTourisStraff() {
		return tourisStraff;
	}
	public void setTourisStraff(int tourisStraff) {
		this.tourisStraff = tourisStraff;
	}
	public String getItineraryTo() {
		return itineraryTo;
	}
	public void setItineraryTo(String itineraryTo) {
		this.itineraryTo = itineraryTo;
	}
	public List<MultipartFile> getTourisNames() {
		return tourisNames;
	}
	public void setTourisNames(List<MultipartFile> tourisNames) {
		this.tourisNames = tourisNames;
	}
	public int getTourisAdult() {
		return tourisAdult;
	}
	public void setTourisAdult(int tourisAdult) {
		this.tourisAdult = tourisAdult;
	}
	public int getTourisChild() {
		return tourisChild;
	}
	public void setTourisChild(int tourisChild) {
		this.tourisChild = tourisChild;
	}
	
	public String getLeaveTime() {
		return leaveTime;
	}
	public void setLeaveTime(String leaveTime) {
		this.leaveTime = leaveTime;
	}
	
	public String getItineraryFrom() {
		return itineraryFrom;
	}
	public void setItineraryFrom(String itineraryFrom) {
		this.itineraryFrom = itineraryFrom;
	}
	public String getArriveTime() {
		return arriveTime;
	}
	public void setArriveTime(String arriveTime) {
		this.arriveTime = arriveTime;
	}
	
	public long getJobId() {
		return jobId;
	}
	public void setJobId(long jobId) {
		this.jobId = jobId;
	}
	public String getForeignName() {
		return foreignName;
	}
	public void setForeignName(String foreignName) {
		this.foreignName = foreignName;
	}
	public String getForeignLicenseNo() {
		return foreignLicenseNo;
	}
	public void setForeignLicenseNo(String foreignLicenseNo) {
		this.foreignLicenseNo = foreignLicenseNo;
	}
	public String getGuideName() {
		return guideName;
	}
	public void setGuideName(String guideName) {
		this.guideName = guideName;
	}
	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}

	public String getJobDate() {
		return jobDate;
	}
	public void setJobDate(String jobDate) {
		this.jobDate = jobDate;
	}
	public String getGuideLicenseNo() {
		return guideLicenseNo;
	}
	public void setGuideLicenseNo(String guideLicenseNo) {
		this.guideLicenseNo = guideLicenseNo;
	}
	public BigDecimal getGuideSalary() {
		return guideSalary;
	}
	public void setGuideSalary(BigDecimal guideSalary) {
		this.guideSalary = guideSalary;
	}
	public long getItrId() {
		return itrId;
	}
	public void setItrId(long itrId) {
		this.itrId = itrId;
	}
	
	 @Override
	    public int compare(JobOrderDTO o1, JobOrderDTO o2) {
		 int number1 = 0;
		 int number2 =0;
			try{
				number1 = Integer.parseInt(o1.getCreateUser());
			}catch(Exception e){}
			try{
				number2 = Integer.parseInt(o2.getCreateUser());
			}catch(Exception e){}
			
			if(number2 <  number1) return 1;
		       if(number1 == number2) return 0;
		       return -1;
			
	    }

	
}
