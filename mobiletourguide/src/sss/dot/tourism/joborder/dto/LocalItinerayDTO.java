package sss.dot.tourism.joborder.dto;

import java.io.Serializable;

public class LocalItinerayDTO implements Serializable {
	private long itrId;
	private String itineraryType;
	private String itineraryBy;
	private String itineraryDate;
	private String itineraryFlight;
	private String itineraryFrom;
	private String leaveTime;
	private String itineraryTo;
	private String arriveTime;
	private String roundTrip;
	public long getItrId() {
		return itrId;
	}
	public void setItrId(long itrId) {
		this.itrId = itrId;
	}
	public String getItineraryType() {
		return itineraryType;
	}
	public void setItineraryType(String itineraryType) {
		this.itineraryType = itineraryType;
	}
	public String getItineraryBy() {
		return itineraryBy;
	}
	public void setItineraryBy(String itineraryBy) {
		this.itineraryBy = itineraryBy;
	}
	public String getItineraryDate() {
		return itineraryDate;
	}
	public void setItineraryDate(String itineraryDate) {
		this.itineraryDate = itineraryDate;
	}
	public String getItineraryFlight() {
		return itineraryFlight;
	}
	public void setItineraryFlight(String itineraryFlight) {
		this.itineraryFlight = itineraryFlight;
	}
	public String getItineraryFrom() {
		return itineraryFrom;
	}
	public void setItineraryFrom(String itineraryFrom) {
		this.itineraryFrom = itineraryFrom;
	}
	public String getLeaveTime() {
		return leaveTime;
	}
	public void setLeaveTime(String leaveTime) {
		this.leaveTime = leaveTime;
	}
	public String getItineraryTo() {
		return itineraryTo;
	}
	public void setItineraryTo(String itineraryTo) {
		this.itineraryTo = itineraryTo;
	}
	public String getArriveTime() {
		return arriveTime;
	}
	public void setArriveTime(String arriveTime) {
		this.arriveTime = arriveTime;
	}
	public String getRoundTrip() {
		return roundTrip;
	}
	public void setRoundTrip(String roundTrip) {
		this.roundTrip = roundTrip;
	}
	
	
}
