package sss.dot.tourism.joborder.service;

import java.util.List;
import java.util.Map;

public interface IJobOrderService {
	public List getJobOrderAll(Object obj) throws Exception;
	public Object getJobOrder(Object obj) throws Exception;
	public void saveJobOrder(Object obj) throws Exception;
	
	public List getDometic(Object obj) throws Exception;
	public List getTouris(Object obj) throws Exception;
	
	public Object getLicenseProfile(Object obj) throws Exception;
	
	public Object getItinerary (Object obj) throws Exception;
	public void saveItinerary (Object obj) throws Exception;
	public List getLocalItineraryAll(Object obj) throws Exception;
	public Object getLocalItinerary(Object obj) throws Exception;
	public void saveLocalItinerary(Object obj) throws Exception;
	
	public List getTourisCustomerAll(Object obj) throws Exception;
	public Object getTourisCustomer(Object obj) throws Exception;
	public void saveTourisCustomer(Object obj) throws Exception;
	
	public Object getHotel(Object obj) throws Exception;
	public void saveHotel(Object obj) throws Exception;
	
	public List getTravelProgramAll(Object obj) throws Exception;
	public Object getTravelProgram(Object obj) throws Exception;
	public void saveTravelProgram(Object obj) throws Exception;
	
	public void getPrintData(Object object, Map model) throws Exception;
}
