package sss.dot.tourism.joborder.service;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.domain.JobOrder;
import sss.dot.tourism.domain.JobOrderHotel;
import sss.dot.tourism.domain.JobOrderItinerary;
import sss.dot.tourism.domain.TourisCustomer;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.domain.TravelProgram;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.joborder.dao.JobOrderDAO;
import sss.dot.tourism.joborder.dao.JobOrderHotelDAO;
import sss.dot.tourism.joborder.dao.JobOrderItineraryDAO;
import sss.dot.tourism.joborder.dao.TourisCustomerDAO;
import sss.dot.tourism.joborder.dao.TravelProgramDAO;
import sss.dot.tourism.joborder.dto.JobOrderDTO;
import sss.dot.tourism.joborder.dto.JobOrderHotelDTO;
import sss.dot.tourism.joborder.dto.LocalItinerayDTO;
import sss.dot.tourism.joborder.dto.ProgramTourDTO;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.RecordStatus;

@Repository("jobOrderService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class JobOrderService implements IJobOrderService {

	@Autowired
	private JobOrderDAO jobOrderDAO;
	@Autowired
	private JobOrderHotelDAO jobOrderHotelDAO;
	@Autowired
	private JobOrderItineraryDAO jobOrderItineraryDAO;
	@Autowired
	private TravelProgramDAO travelProgramDAO;
	@Autowired
	private TraderDAO traderlicenseDAO;
	@Autowired
	private TourisCustomerDAO tourisCustomerDAO;
	
	@Override
	public List getJobOrderAll(Object obj) throws Exception {
		
		if (!(obj instanceof JobOrderDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		List<JobOrderDTO> list = new ArrayList<JobOrderDTO>();
		
		JobOrderDTO param = (JobOrderDTO)obj;
		
		RegistrationDTO regParam = new RegistrationDTO();
		regParam.setIdentityNo(param.getToken());
		regParam.setRecordStatus(RecordStatus.NORMAL.getStatus());
		List<Trader> listTrader = (List<Trader>)this.traderlicenseDAO.findAllTrader(regParam);
		
		if(!listTrader.isEmpty()){
			Trader trader = listTrader.get(0);
			
			param.setLicenseNo(trader.getLicenseNo());
			List<JobOrder> listOrder = (List<JobOrder>)this.jobOrderDAO.findAll(param);
			if(!listOrder.isEmpty()){
				for(JobOrder order: listOrder){
					JobOrderDTO dto = new JobOrderDTO();
					ObjectUtil.copy(order, dto);
					System.out.println(dto.getJobId());
					list.add(dto);
				}
			}
		}
		
		return list;
	}
	@Override
	public Object getJobOrder(Object obj) throws Exception {
		
		if (!(obj instanceof JobOrderDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		JobOrderDTO param = (JobOrderDTO)obj;
		
		JobOrderDTO dto = new JobOrderDTO();
		dto.setToken(param.getToken());
		if(param.getJobId() > 0){
			JobOrder order = (JobOrder)this.jobOrderDAO.findByPrimaryKey(param.getJobId());
			ObjectUtil.copy(order, dto);
		}
		
		RegistrationDTO regParam = new RegistrationDTO();
		regParam.setIdentityNo(param.getToken());
//		regParam.setLicenseNoFrom(dto.getLicenseNo());
		regParam.setRecordStatus(RecordStatus.NORMAL.getStatus());
		List<Trader> listTrader = (List<Trader>)this.traderlicenseDAO.findAllTrader(regParam);
		
		if(!listTrader.isEmpty()){
			Trader trader = listTrader.get(0);
			dto.setTraderName(trader.getTraderName());
			dto.setLicenseNo(trader.getLicenseNo());
		
		}
		
		return dto;
	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
	public void saveJobOrder(Object obj) throws Exception {
		if (!(obj instanceof JobOrderDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		JobOrderDTO param = (JobOrderDTO)obj;
		
		if(!(param.getJobId() > 0)){
			createJobOrder(param);
		}else{
			updateJobOrder(param);
		}
		
		
	}
	
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
	private void createJobOrder(JobOrderDTO param) throws Exception{
		JobOrder order = new JobOrder();
		this.toPresistence(param, order);
		order.setCreateDtm(new Date());
		order.setRecordStatus(RecordStatus.NORMAL.getStatus());
		Long jobId = (Long) this.jobOrderDAO.insert(order);
		param.setJobId(jobId);
	}
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
	private void updateJobOrder(JobOrderDTO param) throws Exception{
		
		JobOrder order = (JobOrder)this.jobOrderDAO.findByPrimaryKey(param.getJobId());
		this.toPresistence(param, order);
		order.setLastUpdDtm(new Date());
		this.jobOrderDAO.update(order);
	}
	private void toPresistence(JobOrderDTO param, JobOrder order) throws Exception{
		
//		DateFormat df = DateUtils.getProcessDateFormatThai();
				
		order.setLicenseNo(param.getLicenseNo());
//		if(param.getJobDate() != null){
//			order.setJobDate(df.parse(param.getJobDate()));
//		}
		order.setJobDate(param.getJobDate());
		order.setJobNo(param.getJobNo());
		order.setForeignName(param.getForeignName());
		order.setForeignLicenseNo(param.getForeignLicenseNo());
		order.setGuideName(param.getGuideName());
		order.setGuideLicenseNo(param.getGuideLicenseNo());
		order.setGuideSalary(param.getGuideSalary());
	}
	@Override
	public List getDometic(Object obj) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List getTouris(Object obj) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getLicenseProfile(Object obj) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Object getItinerary(Object obj) throws Exception {
		

		if (!(obj instanceof JobOrderDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		JobOrderDTO param = (JobOrderDTO)obj;
		param.setItineraryType("I");
		List<JobOrderItinerary> listAll = this.jobOrderItineraryDAO.findAllItinerary(param);
		
		JobOrderDTO dto = new JobOrderDTO();
		dto.setToken(param.getToken());
		dto.setJobId(param.getJobId());
		dto.setLicenseNo(param.getLicenseNo());
		
		if(!listAll.isEmpty()){
			for(JobOrderItinerary itr: listAll){
				if(StringUtils.isNotEmpty(itr.getArriveTime())){
					dto.setArriveId(itr.getItrId());
					dto.setArriveBy(itr.getItineraryBy());
					dto.setArriveDate(itr.getItineraryDate());
					dto.setArriveFlightNo(itr.getItineraryFlight());
					dto.setArriveFrom(itr.getItineraryFrom());
					dto.setArriveTime(itr.getArriveTime());
				}else{
					dto.setLeaveId(itr.getItrId());
					dto.setLeaveBy(itr.getItineraryBy());
					dto.setLeaveDate(itr.getItineraryDate());
					dto.setLeaveFlightNo(itr.getItineraryFlight());
					dto.setLeaveTime(itr.getLeaveTime());
				}
			}
		}
		
		return dto;
	}
	
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
	public void saveItinerary(Object obj) throws Exception {
		if (!(obj instanceof JobOrderDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		JobOrderDTO param = (JobOrderDTO)obj;
		
		if(!((param.getArriveId() > 0) && (param.getLeaveId() > 0))){
			
			if((StringUtils.isNotEmpty(param.getArriveDate())) && (StringUtils.isNotEmpty(param.getLeaveDate()))){
				//create new Itinerary
				JobOrderItinerary arriveItr = new JobOrderItinerary();
				arriveToItinerary(arriveItr, param);
				arriveItr.setItineraryType("I");//international
				arriveItr.setRecordStatus(RecordStatus.NORMAL.getStatus());
				arriveItr.setCreateDtm(new Date());
				
				JobOrder arriveOrder = new JobOrder();
				arriveOrder.setJobId(param.getJobId());
				arriveItr.setJobOrder(arriveOrder);
				
				JobOrderItinerary leaveItr = new JobOrderItinerary();
				leaveToItinerary(leaveItr, param);
				leaveItr.setItineraryType("I");//international
				leaveItr.setRecordStatus(RecordStatus.NORMAL.getStatus());
				leaveItr.setCreateDtm(new Date());
				
				JobOrder leaveOrder = new JobOrder();
				leaveOrder.setJobId(param.getJobId());
				leaveItr.setJobOrder(leaveOrder);
				
				this.jobOrderItineraryDAO.insert(arriveItr);
				this.jobOrderItineraryDAO.insert(leaveItr);
			}
		}else{
			//update Itinerary
			JobOrderItinerary arriveItr = (JobOrderItinerary)this.jobOrderItineraryDAO.findByPrimaryKey(param.getArriveId());
			arriveToItinerary(arriveItr, param);
			arriveItr.setLastUpdDtm(new Date());
			
			JobOrderItinerary leaveItr = (JobOrderItinerary)this.jobOrderItineraryDAO.findByPrimaryKey(param.getLeaveId());
			leaveToItinerary(leaveItr, param);
			leaveItr.setLastUpdDtm(new Date());
			
			this.jobOrderItineraryDAO.update(arriveItr);
			this.jobOrderItineraryDAO.update(leaveItr);
		}
		
	}
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
	private void arriveToItinerary(JobOrderItinerary itr, JobOrderDTO dto)throws Exception {
		itr.setItineraryBy(dto.getArriveBy());
		itr.setItineraryDate(dto.getArriveDate());
		itr.setItineraryFlight(dto.getArriveFlightNo());
		itr.setItineraryFrom(dto.getArriveFrom());
		itr.setArriveTime(dto.getArriveTime());
	}
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
	private void leaveToItinerary(JobOrderItinerary itr, JobOrderDTO dto)throws Exception {
		itr.setItineraryBy(dto.getLeaveBy());
		itr.setItineraryDate(dto.getLeaveDate());
		itr.setItineraryFlight(dto.getLeaveFlightNo());
		itr.setLeaveTime(dto.getLeaveTime());
	}
	//Local Itinerary
	@Override
	public List getLocalItineraryAll(Object obj) throws Exception {
		if (!(obj instanceof JobOrderDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		JobOrderDTO param = (JobOrderDTO)obj;
		param.setItineraryType("D");
		List<JobOrderItinerary> listAll = this.jobOrderItineraryDAO.findAllItinerary(param);
		
		List<JobOrderDTO> list = new ArrayList<JobOrderDTO>();
		
		if(!listAll.isEmpty()){
			for(JobOrderItinerary itr: listAll){
				
				JobOrderDTO dto = new JobOrderDTO();
				dto.setToken(param.getToken());
				dto.setJobId(param.getJobId());
				dto.setLicenseNo(param.getLicenseNo());
				
				ObjectUtil.copy(itr, dto);
				list.add(dto);
			}
		}

		return list;
	}
	
	
	@Override
	public Object getLocalItinerary(Object obj) throws Exception {
		if (!(obj instanceof JobOrderDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		JobOrderDTO param = (JobOrderDTO)obj;
		if(param.getItrId() > 0){
			JobOrderItinerary local = (JobOrderItinerary) this.jobOrderItineraryDAO.findByPrimaryKey(param.getItrId());
			ObjectUtil.copy(local, param);
		}

		return param;
	}
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
	public void saveLocalItinerary(Object obj) throws Exception {
		if (!(obj instanceof JobOrderDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		JobOrderDTO param = (JobOrderDTO)obj;
		
		if(!(param.getItrId() > 0)){
			
			JobOrderItinerary itr = new JobOrderItinerary();
			ObjectUtil.copy(param, itr);
			itr.setItineraryType("D");
			itr.setRecordStatus(RecordStatus.NORMAL.getStatus());
			itr.setCreateDtm(new Date());
			
			JobOrder jobOrder = new JobOrder();
			jobOrder.setJobId(param.getJobId());
			itr.setJobOrder(jobOrder);
			
			Long itrId = (Long)this.jobOrderItineraryDAO.insert(itr);
			param.setItrId(itrId);
		}else{
			JobOrderItinerary itr = (JobOrderItinerary)this.jobOrderItineraryDAO.findByPrimaryKey(param.getItrId());
			ObjectUtil.copy(param, itr);
			itr.setLastUpdDtm(new Date());
			this.jobOrderItineraryDAO.update(itr);
		}
		
	}
	//Customer
	@Override
	public List getTourisCustomerAll(Object obj) throws Exception {
		if (!(obj instanceof JobOrderDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		JobOrderDTO param = (JobOrderDTO)obj;
		List<TourisCustomer> listAll = this.tourisCustomerDAO.findAllTourisCustomer(param);
		
		List<JobOrderDTO> list = new ArrayList<JobOrderDTO>();
		
		if(!listAll.isEmpty()){
			for(TourisCustomer tour: listAll){
				
				JobOrderDTO dto = new JobOrderDTO();
				dto.setToken(param.getToken());
				dto.setJobId(param.getJobId());
				dto.setLicenseNo(param.getLicenseNo());
				ObjectUtil.copy(tour, dto);
				
				if(dto.getTourisType().equals("A")){
					dto.setTourisType("ผู้ใหญ่");
				}else if(dto.getTourisType().equals("C")){
					dto.setTourisType("เด็ก");
				}else if(dto.getTourisType().equals("S")){
					dto.setTourisType("ผู้ติดตาม");
				}
				list.add(dto);
			}
		}
		param.setTourisType("A");
		listAll = this.tourisCustomerDAO.findAllTourisCustomer(param);
		if(!listAll.isEmpty()){
			param.setTourisAdult(listAll.size());
		}
		param.setTourisType("C");
		listAll = this.tourisCustomerDAO.findAllTourisCustomer(param);
		if(!listAll.isEmpty()){
			param.setTourisChild(listAll.size());
		}
		param.setTourisType("S");
		listAll = this.tourisCustomerDAO.findAllTourisCustomer(param);
		if(!listAll.isEmpty()){
			param.setTourisStraff(listAll.size());
		}

		return list;
	}
	@Override
	public Object getTourisCustomer(Object obj) throws Exception {
		if (!(obj instanceof JobOrderDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		JobOrderDTO param = (JobOrderDTO)obj;
		if(param.getTourisId() > 0){
			TourisCustomer local = (TourisCustomer) this.tourisCustomerDAO.findByPrimaryKey(param.getTourisId());
			ObjectUtil.copy(local, param);
		}
		
		return param;
	}
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
	public void saveTourisCustomer(Object obj) throws Exception {
		if (!(obj instanceof JobOrderDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		JobOrderDTO param = (JobOrderDTO)obj;
		if(!(param.getTourisId() > 0)){
			//create
			TourisCustomer cus = new TourisCustomer();
			ObjectUtil.copy(param, cus);
			cus.setRecordStatus(RecordStatus.NORMAL.getStatus());
			cus.setCreateDtm(new Date());
			JobOrder jobOrder = new JobOrder();
			jobOrder.setJobId(param.getJobId());
			
			cus.setJobOrder(jobOrder);
			
			this.tourisCustomerDAO.insert(cus);
		}else{
			//update
			TourisCustomer cus = (TourisCustomer)this.tourisCustomerDAO.findByPrimaryKey(param.getTourisId());
			ObjectUtil.copy(param, cus);
			cus.setLastUpdDtm(new Date());
			this.tourisCustomerDAO.update(cus);
		}
		
	}
	@Override
	public Object getHotel(Object obj) throws Exception {
		if (!(obj instanceof JobOrderDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		JobOrderDTO param = (JobOrderDTO)obj;
		List<JobOrderHotel> list = (List<JobOrderHotel>)this.jobOrderHotelDAO.findHotel(param);
		
		param.setTourisType("A");
		List<TourisCustomer> listAll = this.tourisCustomerDAO.findAllTourisCustomer(param);
		if(!listAll.isEmpty()){
			param.setTourisAdult(listAll.size());
		}
		param.setTourisType("C");
		listAll = this.tourisCustomerDAO.findAllTourisCustomer(param);
		if(!listAll.isEmpty()){
			param.setTourisChild(listAll.size());
		}
		param.setTourisType("S");
		listAll = this.tourisCustomerDAO.findAllTourisCustomer(param);
		if(!listAll.isEmpty()){
			param.setTourisStraff(listAll.size());
		}
		
		if(!list.isEmpty()){
			for(JobOrderHotel hotel: list){
				if("A".equals(hotel.getTourisType())){
					param.setTypeAHotelId(hotel.getHotelId());
					param.setTypeARoomType1(hotel.getRoomType1());
					param.setTypeARoomType2(hotel.getRoomType2());
					param.setTypeARoomType3(hotel.getRoomType3());
					param.setTypeARemark(hotel.getRemark());
					
				}
				if("C".equals(hotel.getTourisType())){
					param.setTypeCHotelId(hotel.getHotelId());
					param.setTypeCRoomType1(hotel.getRoomType1());
					param.setTypeCRoomType2(hotel.getRoomType2());
					param.setTypeCRoomType3(hotel.getRoomType3());
					param.setTypeCRemark(hotel.getRemark());
					
				}
				if("S".equals(hotel.getTourisType())){
					param.setTypeSHotelId(hotel.getHotelId());
					param.setTypeSRoomType1(hotel.getRoomType1());
					param.setTypeSRoomType2(hotel.getRoomType2());
					param.setTypeSRoomType3(hotel.getRoomType3());
					param.setTypeSRemark(hotel.getRemark());
					
				}
				
				
			}
		}
		
		return param;
	}
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
	public void saveHotel(Object obj) throws Exception {
		if (!(obj instanceof JobOrderDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		JobOrderDTO param = (JobOrderDTO)obj;
		
		JobOrder jobOrder = new JobOrder();
		jobOrder.setJobId(param.getJobId());
		
		if(!(param.getTypeAHotelId() >0)){
			//create
			JobOrderHotel hotel = new JobOrderHotel();
			this.toTypeAHotel(param, hotel);
			hotel.setRecordStatus(RecordStatus.NORMAL.getStatus());
			hotel.setCreateDtm(new Date());
			hotel.setJobOrder(jobOrder);
			this.jobOrderHotelDAO.insert(hotel);
		}else{
			//update
			JobOrderHotel hotel = (JobOrderHotel)this.jobOrderHotelDAO.findByPrimaryKey(param.getTypeAHotelId());
			this.toTypeAHotel(param, hotel);
			hotel.setLastUpdDtm(new Date());
			this.jobOrderHotelDAO.update(hotel);
		}
		
		if(!(param.getTypeCHotelId() >0)){
			//create
			JobOrderHotel hotel = new JobOrderHotel();
			this.toTypeCHotel(param, hotel);
			hotel.setRecordStatus(RecordStatus.NORMAL.getStatus());
			hotel.setCreateDtm(new Date());
			hotel.setJobOrder(jobOrder);
			this.jobOrderHotelDAO.insert(hotel);
		}else{
			//update
			JobOrderHotel hotel = (JobOrderHotel)this.jobOrderHotelDAO.findByPrimaryKey(param.getTypeCHotelId());
			this.toTypeCHotel(param, hotel);
			hotel.setLastUpdDtm(new Date());
			this.jobOrderHotelDAO.update(hotel);
		}
		
		if(!(param.getTypeSHotelId() >0)){
			//create
			JobOrderHotel hotel = new JobOrderHotel();
			this.toTypeSHotel(param, hotel);
			hotel.setRecordStatus(RecordStatus.NORMAL.getStatus());
			hotel.setCreateDtm(new Date());
			hotel.setJobOrder(jobOrder);
			this.jobOrderHotelDAO.insert(hotel);
		}else{
			//update
			JobOrderHotel hotel = (JobOrderHotel)this.jobOrderHotelDAO.findByPrimaryKey(param.getTypeSHotelId());
			this.toTypeSHotel(param, hotel);
			hotel.setLastUpdDtm(new Date());
			this.jobOrderHotelDAO.update(hotel);
		}
		
	}
	private void toTypeAHotel(JobOrderDTO param, JobOrderHotel hotel) throws Exception {
		hotel.setTourisType("A");
		hotel.setRoomType1(param.getTypeARoomType1());
		hotel.setRoomType2(param.getTypeARoomType2());
		hotel.setRoomType3(param.getTypeARoomType3());
		hotel.setRemark(param.getTypeARemark());
	}
	
	private void toTypeCHotel(JobOrderDTO param, JobOrderHotel hotel) throws Exception {
		hotel.setTourisType("C");
		hotel.setRoomType1(param.getTypeCRoomType1());
		hotel.setRoomType2(param.getTypeCRoomType2());
		hotel.setRoomType3(param.getTypeCRoomType3());
		hotel.setRemark(param.getTypeCRemark());
	}
	
	private void toTypeSHotel(JobOrderDTO param, JobOrderHotel hotel) throws Exception {
		hotel.setTourisType("S");
		hotel.setRoomType1(param.getTypeSRoomType1());
		hotel.setRoomType2(param.getTypeSRoomType2());
		hotel.setRoomType3(param.getTypeSRoomType3());
		hotel.setRemark(param.getTypeSRemark());
	}
	//travel program
	@Override
	public List getTravelProgramAll(Object obj) throws Exception {
		if (!(obj instanceof JobOrderDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		JobOrderDTO param = (JobOrderDTO)obj;
		List<TravelProgram> listAll = (List<TravelProgram>)this.travelProgramDAO.findAllProgram(param);
		
		List<JobOrderDTO> list = new ArrayList<JobOrderDTO>();
		if(!listAll.isEmpty()){
			for(TravelProgram p: listAll){
				JobOrderDTO dto = new JobOrderDTO();
				ObjectUtil.copy(p, dto);
				list.add(dto);
			}
			Collections.sort(list, new JobOrderDTO());
		}
		return list;
	}
	@Override
	public Object getTravelProgram(Object obj) throws Exception {
		if (!(obj instanceof JobOrderDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		JobOrderDTO param = (JobOrderDTO)obj;
		if(param.getProgId() > 0){
			TravelProgram tp = (TravelProgram) this.travelProgramDAO.findByPrimaryKey(param.getProgId());
			ObjectUtil.copy(tp, param);
		}
		
		return param;
	}
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
	public void saveTravelProgram(Object obj) throws Exception {
		if (!(obj instanceof JobOrderDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		JobOrderDTO param = (JobOrderDTO)obj;
		if(!(param.getProgId() > 0)){
			//create
			TravelProgram tp = new TravelProgram();
			ObjectUtil.copy(param, tp);
			tp.setRecordStatus(RecordStatus.NORMAL.getStatus());
			tp.setCreateDtm(new Date());
			
			JobOrder jobOrder = new JobOrder();
			jobOrder.setJobId(param.getJobId());
			tp.setJobOrder(jobOrder);
			
			this.travelProgramDAO.insert(tp);
		}else{
			//update
			TravelProgram tp = (TravelProgram) this.travelProgramDAO.findByPrimaryKey(param.getProgId());
			ObjectUtil.copy(param, tp);
			tp.setLastUpdDtm(new Date());
			
			this.travelProgramDAO.update(tp);
		}
		
	}
	
	

	@Override
	public void getPrintData(Object object, Map model) throws Exception {
		if (!(object instanceof JobOrderDTO)) {
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		JobOrderDTO param = (JobOrderDTO)object;
		
		List<Object[]> listAll = (List<Object[]>)this.jobOrderDAO.findAll(param, 0, Integer.MAX_VALUE);
		
		
		if(!listAll.isEmpty()){
			
			Object[] obj = listAll.get(0);
			JobOrderDTO dto = new JobOrderDTO();
			
			dto.setJobId(Long.valueOf(obj[9].toString()));
			dto.setLicenseNo(obj[0]==null?"":obj[0].toString());
			dto.setTraderName(obj[1]==null?"":obj[1].toString());
			dto.setTourisName(dto.getTraderName());
			dto.setJobNo(obj[2]==null?"":obj[2].toString());
			dto.setJobDate(obj[3]==null?"":obj[3].toString());
			dto.setForeignName(obj[4]==null?"":obj[4].toString());
			dto.setForeignLicenseNo(obj[5]==null?"":obj[5].toString());
			dto.setGuideLicenseNo(obj[6]==null?"":obj[6].toString());
			dto.setGuideName(obj[7]==null?"":obj[7].toString());
			DateFormat df = DateUtils.getProcessDateFormatThai();
			dto.setGuideSalary(obj[10]==null?new BigDecimal(0): new BigDecimal(obj[10].toString()));
			
			this.getItinerary(dto);
			model.put("joborder", dto);
			
			List local = this.getLocalItineraryAll(dto);
			model.put("locals", local);
			
			List hotel = this.getHotel(dto);
			model.put("listhotel", hotel);
			
			List programs = this.getTravelProgramAll(dto);
			model.put("lists", programs);
			
		}
	}
	
	private void getItinerary(JobOrderDTO dto) throws Exception {
		
		dto.setItineraryType("I");
		List<JobOrderItinerary> listAll = this.jobOrderItineraryDAO.findAllItinerary(dto);
		
		if(!listAll.isEmpty()){
			for(JobOrderItinerary itr: listAll){
				if(StringUtils.isNotEmpty(itr.getArriveTime())){
					dto.setArriveId(itr.getItrId());
					dto.setArriveBy(itr.getItineraryBy());
					dto.setArriveDate(itr.getItineraryDate());
					dto.setArriveFlightNo(itr.getItineraryFlight());
					dto.setArriveFrom(itr.getItineraryFrom());
					dto.setArriveTime(itr.getArriveTime());
				}else{
					dto.setLeaveId(itr.getItrId());
					dto.setLeaveBy(itr.getItineraryBy());
					dto.setLeaveDate(itr.getItineraryDate());
					dto.setLeaveFlightNo(itr.getItineraryFlight());
					dto.setLeaveTime(itr.getLeaveTime());
				}
			}
		}
	}
	private List getLocalItineraryAll(JobOrderDTO param) throws Exception {
		
		param.setItineraryType("D");
		List<JobOrderItinerary> listAll = this.jobOrderItineraryDAO.findAllItinerary(param);
		
		List<LocalItinerayDTO> list = new ArrayList<LocalItinerayDTO>();
		
		if(!listAll.isEmpty()){
			for(JobOrderItinerary itr: listAll){
				
				LocalItinerayDTO dto = new LocalItinerayDTO();
				
				ObjectUtil.copy(itr, dto);
				list.add(dto);
			}
		}

		return list;
	}
	
	public List getHotel(JobOrderDTO param) throws Exception {
		
		List<JobOrderHotel> listAll = (List<JobOrderHotel>)this.jobOrderHotelDAO.findHotel(param);
		
		List<JobOrderHotelDTO> list = new ArrayList<JobOrderHotelDTO>();
		
		JobOrderHotelDTO totalRoom = new JobOrderHotelDTO();
		totalRoom.setTourisType("รวม");
		totalRoom.setRoomType1(0);
		totalRoom.setRoomType2(0);
		totalRoom.setRoomType3(0);
		totalRoom.setTotalRoom(0);
		totalRoom.setTotalTouris(0);
		
		if(!listAll.isEmpty()){
			
			for(JobOrderHotel hotel: listAll){
				JobOrderHotelDTO dto = new JobOrderHotelDTO();
				ObjectUtil.copy(hotel, dto);
				
				dto.setRoomType1(dto.getRoomType1()==null?0:dto.getRoomType1());
				dto.setRoomType2(dto.getRoomType2()==null?0:dto.getRoomType2());
				dto.setRoomType3(dto.getRoomType3()==null?0:dto.getRoomType3());
				dto.setTotalTouris(dto.getTotalTouris()==null?0:dto.getTotalTouris());
				
				int totalRoomType = dto.getRoomType1()+dto.getRoomType2()+dto.getRoomType3();
				dto.setTotalRoom(totalRoomType);
				
				totalRoom.setRoomType1(totalRoom.getRoomType1()+dto.getRoomType1());
				totalRoom.setRoomType2(totalRoom.getRoomType2()+dto.getRoomType2());
				totalRoom.setRoomType3(totalRoom.getRoomType3()+dto.getRoomType3());
				totalRoom.setTotalRoom(totalRoom.getTotalRoom()+totalRoomType);
				
				
				if(dto.getTourisType().equals("A")){
					dto.setTourisType("ผู้ใหญ่");
					
					param.setTourisType("A");
					List<TourisCustomer> listTouris = this.tourisCustomerDAO.findAllTourisCustomer(param);
					if(!listTouris.isEmpty()){
						dto.setTotalTouris(listTouris.size());
					}
					
				}else if(dto.getTourisType().equals("C")){
					dto.setTourisType("เด็ก");
					
					param.setTourisType("C");
					List<TourisCustomer> listTouris = this.tourisCustomerDAO.findAllTourisCustomer(param);
					if(!listTouris.isEmpty()){
						dto.setTotalTouris(listTouris.size());
					}
					
				}else if(dto.getTourisType().equals("S")){
					dto.setTourisType("ผู้ติดตาม");
					
					param.setTourisType("S");
					List<TourisCustomer> listTouris = this.tourisCustomerDAO.findAllTourisCustomer(param);
					if(!listTouris.isEmpty()){
						dto.setTotalTouris(listTouris.size());
					}
				}
				
				totalRoom.setTotalTouris(totalRoom.getTotalTouris()+ dto.getTotalTouris());
				
				list.add(dto);
			}
			
			
		}
		list.add(totalRoom);
		return list;
	}
	
	public List getTravelProgramAll(JobOrderDTO param) throws Exception {
		
		List<TravelProgram> listAll = (List<TravelProgram>)this.travelProgramDAO.findAllProgram(param);
		
		List<ProgramTourDTO> list = new ArrayList<ProgramTourDTO>();
		if(!listAll.isEmpty()){
			for(TravelProgram p: listAll){
				ProgramTourDTO dto = new ProgramTourDTO();
				ObjectUtil.copy(p, dto);
				list.add(dto);
			}
			Collections.sort(list, new ProgramTourDTO());
		}
		return list;
	}
	
	
}
