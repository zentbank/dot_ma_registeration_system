package sss.dot.tourism.journal.controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.journal.dto.JournalDTO;
import sss.dot.tourism.journal.service.IJournalService;


@Controller
@RequestMapping("/journal")
public class JournalController {
	@Autowired
	IJournalService journalService;
	
	@RequestMapping(value = "/publicnews/read", method = RequestMethod.POST)
	public @ResponseBody String readPublicNews(@ModelAttribute JournalDTO param ) 
	{
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			
			Long numAll = new Long(journalService.getPublicNews(param,
					0,
					Integer.MAX_VALUE).size());

			List list = this.journalService.getPublicNews(param,
					param.getStart(), 
					param.getLimit());
			
			return jsonmsg.writePagingMessage(list, numAll);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
//	
//	@RequestMapping(value = "/readDocument", method = RequestMethod.POST)
//	public @ResponseBody String readDocument(@ModelAttribute MasDocumentDTO param ) 
//	{
//		System.out.println("###Controller DocumentRegistrationController method readDocument");
//		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
//		try {
//			List list = this.documentRegistrationService.getDocument(param, DummyUser.getInstance().getUser());
//			return jsonmsg.writeMessage(list);
//
//		} catch (Exception e) {
//			e.printStackTrace();
//			return jsonmsg.writeErrorMessage(e.getMessage());
//	
//		}
//	}
	
	@RequestMapping(value = "/savepublicnews", method = RequestMethod.POST)
//	public @ResponseBody String upload(@ModelAttribute JournalDTO params) 
	public @ResponseBody String upload(@RequestParam("newsImg") MultipartFile f ,@RequestParam("newsHeader") String newsHeader,@RequestParam("newsDesc") String newsDesc) 
	{

	    HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
	    try{
	    	System.out.println("savepublicnews");
	    	
	    	JournalDTO params = new JournalDTO();
	    	
	    	params.setFile(f);
	    	params.setNewsHeader(newsHeader);
	    	params.setNewsDesc(newsDesc);
	    	
	    	this.journalService.savePublicNews(params);
	    	return jsonmsg.writeMessage();
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    	return jsonmsg.writeErrorMessage(e.getMessage());
	    }
	
	}
	
	@RequestMapping(value="/publicnews/viewimg/{newsId}", method=RequestMethod.GET )
	public void viewfile(@PathVariable long newsId ,HttpServletResponse resp){
	
		try {
			
			resp.setContentType("image/jpeg");
			
			JournalDTO dto = (JournalDTO)this.journalService.getPublicNewsById(newsId);
			
			
			String fileType = dto.getNewsImgPath().substring(dto.getNewsImgPath().lastIndexOf(".") + 1, dto.getNewsImgPath().length());

			
			if(!fileType.equals("pdf"))
			{
				File f = new File(dto.getNewsImgPath());
			
				resp.setContentType("image/" + fileType);
				BufferedImage bi = ImageIO.read(f);
				OutputStream out = resp.getOutputStream();
				ImageIO.write(bi, fileType, out);
				out.close();
			}
			else
			{
				File pdfFile = new File(dto.getNewsImgPath());
				
				String fileName = dto.getNewsImgPath().substring(dto.getNewsImgPath().lastIndexOf("/") + 1, dto.getNewsImgPath().length());

				resp.setContentType("application/pdf");
				resp.addHeader("Content-Disposition", "attachment; filename=" + fileName);
				resp.setContentLength((int) pdfFile.length());

				FileInputStream fileInputStream = new FileInputStream(pdfFile);
				OutputStream responseOutputStream = resp.getOutputStream();
				int bytes;
				while ((bytes = fileInputStream.read()) != -1) {
					responseOutputStream.write(bytes);
				
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			
	
		}
	}
	
	@RequestMapping(value = "/publicnews/delete", method = RequestMethod.POST)
	public @ResponseBody
	String deleteNews(@RequestBody String json) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			Object[] object = (Object[]) jsonmsg.toArrayObject(
					JournalDTO.class, json);

			this.journalService.deletePublicNews(object);

			return jsonmsg.writeMessage();
		} catch (Exception e) {

			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
			// return null;
		}

	}
}
