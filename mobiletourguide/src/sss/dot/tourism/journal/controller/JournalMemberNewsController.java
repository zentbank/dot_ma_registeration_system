package sss.dot.tourism.journal.controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import sss.dot.tourism.http.HttpMessage;
import sss.dot.tourism.http.HttpMessageFactory;
import sss.dot.tourism.http.HttpMessageType;
import sss.dot.tourism.journal.dto.JournalDTO;
import sss.dot.tourism.journal.dto.MemberNewsDTO;
import sss.dot.tourism.journal.service.IJournalService;

@Controller
@RequestMapping("/journal/membernews")
public class JournalMemberNewsController {
	@Autowired
	IJournalService journalService;
	
	@RequestMapping(value = "/read", method = RequestMethod.POST)
	public @ResponseBody String readMemberNews(@ModelAttribute MemberNewsDTO param ) 
	{
		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {
			
			Long numAll = new Long(journalService.getMemberNews(param,
					0,
					Integer.MAX_VALUE).size());

			List list = this.journalService.getMemberNews(param,
					param.getStart(), 
					param.getLimit());
			
			return jsonmsg.writePagingMessage(list, numAll);

		} catch (Exception e) {
			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
	
		}
	}
	
	@RequestMapping(value = "/savemembernews", method = RequestMethod.POST)
//	public @ResponseBody String upload(@ModelAttribute JournalDTO params) 
	public @ResponseBody String upload(@RequestParam("newsImg") MultipartFile f 
			,@RequestParam("memberNewsHeader") String memberNewsHeader
			,@RequestParam("memberNewsDesc") String memberNewsDesc
			,@RequestParam("newsStartDate") String newsStartDate
			,@RequestParam("newsEndDate") String newsEndDate
			,@RequestParam("traderType") String traderType) 
	{

	    HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
	    try{
	    	
	    	
	    	MemberNewsDTO params = new MemberNewsDTO();
	    	
	    	params.setFile(f);
	    	params.setMemberNewsHeader(memberNewsHeader);
	    	params.setMemberNewsDesc(memberNewsDesc);
	    	params.setNewsStartDate(newsStartDate);
	    	params.setNewsEndDate(newsEndDate);
	    	params.setTraderType(traderType);
	    	
	    	this.journalService.saveMemberNews(params);
	    	return jsonmsg.writeMessage();
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    	return jsonmsg.writeErrorMessage(e.getMessage());
	    }
	
	}
	
	@RequestMapping(value="/viewimg/{newsId}", method=RequestMethod.GET )
	public void viewfile(@PathVariable long newsId ,HttpServletResponse resp){
	
		try {
			
			resp.setContentType("image/jpeg");
			
			MemberNewsDTO dto = (MemberNewsDTO)this.journalService.getMemberNewsById(newsId);
			
			String fileType = dto.getMewsImgPath().substring(dto.getMewsImgPath().lastIndexOf(".") + 1, dto.getMewsImgPath().length());
			
			if(!fileType.equals("pdf"))
			{
				File f = new File(dto.getMewsImgPath());
			
				resp.setContentType("image/" + fileType);
				BufferedImage bi = ImageIO.read(f);
				OutputStream out = resp.getOutputStream();
				ImageIO.write(bi, fileType, out);
				out.close();
			}
			else
			{
				File pdfFile = new File(dto.getMewsImgPath());
				
				String fileName = dto.getMewsImgPath().substring(dto.getMewsImgPath().lastIndexOf("/") + 1, dto.getMewsImgPath().length());

				resp.setContentType("application/pdf");
				resp.addHeader("Content-Disposition", "attachment; filename=" + fileName);
				resp.setContentLength((int) pdfFile.length());

				FileInputStream fileInputStream = new FileInputStream(pdfFile);
				OutputStream responseOutputStream = resp.getOutputStream();
				int bytes;
				while ((bytes = fileInputStream.read()) != -1) {
					responseOutputStream.write(bytes);
				
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
	
		}
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody
	String deleteNews(@RequestBody String json) {

		HttpMessage jsonmsg = HttpMessageFactory.create(HttpMessageType.JSON);
		try {

			Object[] object = (Object[]) jsonmsg.toArrayObject(
					MemberNewsDTO.class, json);

			this.journalService.deleteMemberNews(object);

			return jsonmsg.writeMessage();
		} catch (Exception e) {

			e.printStackTrace();
			return jsonmsg.writeErrorMessage(e.getMessage());
			// return null;
		}

	}
}
