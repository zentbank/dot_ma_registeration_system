package sss.dot.tourism.journal.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import sss.dot.tourism.dao.BaseDAO;
import sss.dot.tourism.domain.Journal;
import sss.dot.tourism.util.RecordStatus;

@Repository("journalDAO")
public class JournalDAO extends BaseDAO{

	public JournalDAO()
	{
		this.domainObj = Journal.class;
	}
	
	  public List<Journal> findAllByPaging(String newsHeader,int firstRow, int pageSize)
	  {
		ArrayList params = new ArrayList();
		StringBuilder hql = new StringBuilder();
		hql.append(" from  Journal as dep ");
		hql.append(" where dep.recordStatus = ? ");
		params.add(RecordStatus.NORMAL.getStatusString());
		
		if((newsHeader != null) && !(newsHeader.isEmpty()))
		{
			hql.append(" and dep.newsHeader like ? ");
			params.add("%"+newsHeader+"%");
		}
		
		
		
		hql.append(" order by dep.createDtm ASC ");
		
		System.out.println("hql = "+hql.toString());
		
		return (List<Journal>)this.findByAddPaging(hql.toString(), firstRow, pageSize, params.toArray());
	  }
	
}
