package sss.dot.tourism.journal.dto;

import java.io.Serializable;
import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

public class JournalDTO implements Serializable{
	private long newsId;
	private String newsHeader;
	private String newsDesc;
	private String newsImgPath;
	private String memberNews;
	private String licenseNo;
	private String traderType;
	private String createDtm;
	private String newsTextPath;
	
	private MultipartFile file;
	
	private int page;
	private int start;
	private int limit;
	
	

	public String getCreateDtm() {
		return createDtm;
	}

	public void setCreateDtm(String createDtm) {
		this.createDtm = createDtm;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public long getNewsId() {
		return newsId;
	}

	public void setNewsId(long newsId) {
		this.newsId = newsId;
	}

	public String getNewsHeader() {
		return newsHeader;
	}

	public void setNewsHeader(String newsHeader) {
		this.newsHeader = newsHeader;
	}

	public String getNewsDesc() {
		return newsDesc;
	}

	public void setNewsDesc(String newsDesc) {
		this.newsDesc = newsDesc;
	}

	public String getNewsImgPath() {
		return newsImgPath;
	}

	public void setNewsImgPath(String newsImgPath) {
		this.newsImgPath = newsImgPath;
	}

	public String getMemberNews() {
		return memberNews;
	}

	public void setMemberNews(String memberNews) {
		this.memberNews = memberNews;
	}

	public String getLicenseNo() {
		return licenseNo;
	}

	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}

	public String getTraderType() {
		return traderType;
	}

	public void setTraderType(String traderType) {
		this.traderType = traderType;
	}

	public String getNewsTextPath() {
		return newsTextPath;
	}

	public void setNewsTextPath(String newsTextPath) {
		this.newsTextPath = newsTextPath;
	}
	
	
}
