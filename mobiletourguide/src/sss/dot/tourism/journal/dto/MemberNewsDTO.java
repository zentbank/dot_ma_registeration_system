package sss.dot.tourism.journal.dto;

import java.io.Serializable;
import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

import sss.dot.tourism.domain.MemberLicense;

public class MemberNewsDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4740880840431696160L;
	private long memberNewsId;
	private String memberNewsHeader;
	private String memberNewsDesc;
	
	private String newsStartDate;
	private String newsEndDate;
	private String mewsImgPath;
	private String traderType;
	
	private long memberId;
	private String licenseNo;
	private String indentityNo;
	
	private MultipartFile file;
	
	private int page;
	private int start;
	private int limit;
	
	
	
	public MultipartFile getFile() {
		return file;
	}
	public void setFile(MultipartFile file) {
		this.file = file;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public long getMemberNewsId() {
		return memberNewsId;
	}
	public void setMemberNewsId(long memberNewsId) {
		this.memberNewsId = memberNewsId;
	}
	public String getMemberNewsHeader() {
		return memberNewsHeader;
	}
	public void setMemberNewsHeader(String memberNewsHeader) {
		this.memberNewsHeader = memberNewsHeader;
	}
	public String getMemberNewsDesc() {
		return memberNewsDesc;
	}
	public void setMemberNewsDesc(String memberNewsDesc) {
		this.memberNewsDesc = memberNewsDesc;
	}
	
	public String getNewsStartDate() {
		return newsStartDate;
	}
	public void setNewsStartDate(String newsStartDate) {
		this.newsStartDate = newsStartDate;
	}
	public String getNewsEndDate() {
		return newsEndDate;
	}
	public void setNewsEndDate(String newsEndDate) {
		this.newsEndDate = newsEndDate;
	}
	public String getMewsImgPath() {
		return mewsImgPath;
	}
	public void setMewsImgPath(String mewsImgPath) {
		this.mewsImgPath = mewsImgPath;
	}
	public String getTraderType() {
		return traderType;
	}
	public void setTraderType(String traderType) {
		this.traderType = traderType;
	}
	public long getMemberId() {
		return memberId;
	}
	public void setMemberId(long memberId) {
		this.memberId = memberId;
	}
	public String getLicenseNo() {
		return licenseNo;
	}
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	public String getIndentityNo() {
		return indentityNo;
	}
	public void setIndentityNo(String indentityNo) {
		this.indentityNo = indentityNo;
	}
	
	
}
