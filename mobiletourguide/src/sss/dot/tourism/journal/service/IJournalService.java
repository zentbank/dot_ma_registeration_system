package sss.dot.tourism.journal.service;

import java.util.List;

import com.sss.aut.service.User;

public interface IJournalService {

	public List getPublicNews(Object obj, int firstRow, int pageSize) throws Exception;
	
	public void savePublicNews(Object obj) throws Exception;
	
	public void deletePublicNews(Object[] object ) throws Exception;
	
	public Object getPublicNewsById(long newsId) throws Exception;
	
	
	public List getMemberNews(Object obj, int firstRow, int pageSize) throws Exception;
	
	public void saveMemberNews(Object obj) throws Exception;
	
	public void deleteMemberNews(Object[] object ) throws Exception;
	
	public Object getMemberNewsById(long newsId) throws Exception;
	
	
}
