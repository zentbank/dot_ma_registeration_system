package sss.dot.tourism.journal.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.info.TraderDAO;
import sss.dot.tourism.dao.trader.MemberNewsDAO;
import sss.dot.tourism.domain.Journal;
import sss.dot.tourism.domain.MemberLicense;
import sss.dot.tourism.domain.MemberNews;
import sss.dot.tourism.domain.Person;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.journal.dao.JournalDAO;
import sss.dot.tourism.journal.dto.JournalDTO;
import sss.dot.tourism.journal.dto.MemberNewsDTO;
import sss.dot.tourism.util.ConstantUtil;
import sss.dot.tourism.util.FileManage;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.PersonType;
import sss.dot.tourism.util.RecordStatus;
import sss.dot.tourism.util.TraderType;

@Repository("journalService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class JournalService implements IJournalService {

	@Autowired
	JournalDAO journalDAO;
	@Autowired
	MemberNewsDAO memberNewsDAO;
	@Autowired
	TraderDAO traderDAO;
	
	public List getPublicNews(Object obj,int firstRow, int pageSize) throws Exception {
		
		if(!(obj instanceof JournalDTO))
		{
			throw new IllegalArgumentException();
		}
		JournalDTO param = (JournalDTO)obj;
		
		List<JournalDTO> list = new ArrayList<JournalDTO>();
		
		List<Journal> listAll = new ArrayList<Journal>();
		if(pageSize > 0)
		{
			listAll = this.journalDAO.findAllByPaging(param.getNewsHeader(), firstRow, pageSize);
		}
		else
		{
			listAll = (List<Journal>)this.journalDAO.findAll();
		}
		
		if(!listAll.isEmpty())
		{
			for(Journal journal: listAll)
			{
				JournalDTO dto = new JournalDTO();
				
				ObjectUtil.copy(journal, dto);
				
				list.add(dto);
			}
		}
		
		return list;
	}
	
	public Object getPublicNewsById(long newsId) throws Exception {
		
		Journal journal = (Journal)this.journalDAO.findByPrimaryKey(newsId);
		
		JournalDTO dto = new JournalDTO();
		ObjectUtil.copy(journal ,dto);
		
		return dto;
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void savePublicNews(Object obj) throws Exception {
		
		if(!(obj instanceof JournalDTO))
		{
			throw new IllegalArgumentException("obj instanceof JournalDTO");
		}
		
		JournalDTO params = (JournalDTO)obj;
		
		if(params.getFile() != null)
		{
			
	    	String extension = ".txt";
			String originalFilename = params.getFile().getOriginalFilename();
			int dot = originalFilename.lastIndexOf('.');
			if(dot != -1)
			{
				extension = originalFilename.substring(dot);
			}
			
			String fileName = String.valueOf(Calendar.getInstance().getTimeInMillis())+extension;
			String path = ConstantUtil.IMG_ROOT_PATH + "publicnews"+java.io.File.separator;
			
			
			
			FileManage fm = new FileManage();
	    	boolean b = fm.saveFile(params.getFile().getInputStream(), path, fileName);
	    	

	    	
	    	String fullpath = path+ fileName;
	    	
	    	//Save RegDocument
	    	if(b)
	    	{
	    		Journal journal = new Journal();
	    		
	    		journal.setNewsHeader(params.getNewsHeader());
	    		journal.setNewsDesc(params.getNewsDesc());
	    		journal.setRecordStatus(RecordStatus.NORMAL.getStatus());
	    		journal.setCreateDtm(new Date());
	    		journal.setNewsImgPath(fullpath);
	    		
	    		this.journalDAO.insert(journal);
	    		
	    	}
		}		
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void deletePublicNews(Object[] object  ) throws Exception {
		
		for (Object obj : object) {
			JournalDTO dto = (JournalDTO) obj;
			Journal news = (Journal)journalDAO.findByPrimaryKey(dto.getNewsId());
			journalDAO.delete(news);
			
		}
		
	}

	public List getMemberNews(Object obj, int firstRow, int pageSize)
			throws Exception {
		if(!(obj instanceof MemberNewsDTO))
		{
			throw new IllegalArgumentException();
		}
		MemberNewsDTO param = (MemberNewsDTO)obj;
		
		List<MemberNewsDTO> list = new ArrayList<MemberNewsDTO>();
		
		List<MemberNews> listAll = this.memberNewsDAO.findAllByPaging(param.getMemberNewsHeader(),param.getTraderType(), firstRow, pageSize);
		
		
		if(!listAll.isEmpty())
		{
			for(MemberNews journal: listAll)
			{
				MemberNewsDTO dto = new MemberNewsDTO();
				
				ObjectUtil.copy(journal, dto);
				
				if( journal.getMemberLicense() != null)
				{
//					MemberLicense mLic = journal.getMemberLicense();
					ObjectUtil.copy(journal.getMemberLicense(), dto);
					
					List<Trader> listTrader = traderDAO.findTraderByLicenseNo(dto.getLicenseNo(), dto.getTraderType(), RecordStatus.NORMAL.getStatus(), 0);
					String traderOwnerName = "";
					if(!listTrader.isEmpty())
					{
						Trader trader = listTrader.get(0);
						Person person = trader.getPerson();
						
						
						if(TraderType.TOUR_COMPANIES.equals(trader.getTraderType()))
						{
							traderOwnerName = trader.getLicenseNo() + " : " + trader.getTraderName();
							
						}
						else
						{
							traderOwnerName = trader.getLicenseNo() + " : " + person.getMasPrefix().getPrefixName() + person.getFirstName() + " " + person.getLastName();
							
						}
					}
					dto.setMemberNewsHeader(traderOwnerName+" "+ journal.getMemberNewsHeader());
				}
				
				
				list.add(dto);
			}
		}
		
		return list;
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveMemberNews(Object obj) throws Exception {
		if(!(obj instanceof MemberNewsDTO))
		{
			throw new IllegalArgumentException("obj instanceof MemberNewsDTO");
		}
		
		MemberNewsDTO params = (MemberNewsDTO)obj;
		
		if(params.getFile() != null)
		{
			
	    	String extension = ".txt";
			String originalFilename = params.getFile().getOriginalFilename();
			int dot = originalFilename.lastIndexOf('.');
			if(dot != -1)
			{
				extension = originalFilename.substring(dot);
			}
			
			String fileName = String.valueOf(Calendar.getInstance().getTimeInMillis())+extension;
			String path = ConstantUtil.IMG_ROOT_PATH + "membernews"+java.io.File.separator+params.getTraderType()+java.io.File.separator;
			
			
			
			FileManage fm = new FileManage();
	    	boolean b = fm.saveFile(params.getFile().getInputStream(), path, fileName);
	    	

	    	
	    	String fullpath = path+ fileName;
	    	
	    	//Save RegDocument
	    	if(b)
	    	{
	    		MemberNews journal = new MemberNews();
	    		
	    		ObjectUtil.copy(params, journal);
	    		
	    
	    		journal.setRecordStatus(RecordStatus.NORMAL.getStatus());
	    		journal.setCreateDtm(new Date());
	    		journal.setMewsImgPath(fullpath);
	    		
	    		this.memberNewsDAO.insert(journal);
	    		
	    	}
		}
		
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void deleteMemberNews(Object[] object) throws Exception {
		for (Object obj : object) {
			MemberNewsDTO dto = (MemberNewsDTO) obj;
			MemberNews news = (MemberNews)memberNewsDAO.findByPrimaryKey(dto.getMemberNewsId());
			journalDAO.delete(news);
			
		}
	}

	public Object getMemberNewsById(long newsId) throws Exception {
		MemberNews journal = (MemberNews)this.memberNewsDAO.findByPrimaryKey(newsId);
		
		MemberNewsDTO dto = new MemberNewsDTO();
		ObjectUtil.copy(journal ,dto);
		
		return dto;
	}
	
	

}
