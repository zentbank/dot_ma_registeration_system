package sss.dot.tourism.reqcertificate.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import sss.dot.tourism.controller.renew.LicenseRenewController;
import sss.dot.tourism.dto.info.LicenseDetailDTO;
import sss.dot.tourism.dto.renew.FileUploadForm;
import sss.dot.tourism.reqcertificate.dto.ReqLicenseCertificateDTO;
import sss.dot.tourism.reqcertificate.service.IReqLicenseCertificateService;
import sss.dot.tourism.service.info.ILicenseService;

import com.sss.aut.service.User;

@Controller
@RequestMapping("/license/reqcertificate")
public class ReqcertificateController {
	
	@Autowired 
	private ILicenseService licenseService;
	
	@Autowired 
	private IReqLicenseCertificateService reqLicenseCertificateService;
	
	
	@Autowired
	private HttpServletRequest req;
	
	private static final Logger logger = Logger.getLogger(LicenseRenewController.class);
	
	@RequestMapping(value = "/read/{token}", method = RequestMethod.POST)
	public	String memberWebProfile(@PathVariable String token, ModelMap model) {

		try {

			String identityNo = token;//getIdentityNo(token);
			LicenseDetailDTO param = new LicenseDetailDTO();
			param.setIndentityNo(identityNo);
			List list = this.licenseService.getByIdentityNo(param);
			LicenseDetailDTO licenseDetail = (LicenseDetailDTO)list.get(0);
			
			model.addAttribute("model", licenseDetail.getImgUrl());
			model.addAttribute("list", list);
			
			String forwordPage = "/page/TourReqCertificate.jsp";
			
			return forwordPage;


		} catch (Exception e) {
			e.printStackTrace();
			return "/page/pageerror.jsp";

		}
	}
	
	@RequestMapping(value = "/request", method = RequestMethod.POST)
    public String save(
            @ModelAttribute("uploadForm") FileUploadForm uploadForm,
                    Model map) {
		
		User user = (User) req.getSession(true).getAttribute("member");
		
		try{
	        List<MultipartFile> files = uploadForm.getFiles();
	        
	        ReqLicenseCertificateDTO param = new ReqLicenseCertificateDTO();
	        param.setTraderId(uploadForm.getTraderId());
	        
	        this.reqLicenseCertificateService.saveAll(param, user);
	        
	        if(null != files && files.size() > 0) {
	            
	        	this.reqLicenseCertificateService.saveDocumentAll(param, files, user);
	        }
		}
		catch(Exception e)
		{
			this.logger.error(e.getMessage());
			e.printStackTrace();
		}
         
        return "/page/MemberWebNewsMenu.jsp";
    }
	
	@RequestMapping(value = "/joborder/read", method = RequestMethod.POST)
	public String read(@RequestParam("token") String token, Model model) {

		try {

			System.out.println("joborder");

			return "/page/JobOrderListView.jsp";

		} catch (Exception e) {
			e.printStackTrace();
			return "/page/pageerror.jsp";

		}
	}
}
