package sss.dot.tourism.reqcertificate.dto;





import java.io.Serializable;
import java.util.Date;


public class ReqLicenseCertificateDTO implements Serializable{
	
	private long cerId;
	private String licenseNo;
	private String licenseNoEn;
	private String reqDate;
	private String printDate;
	private String bookNo;
	private String bookDocToManagerNo;
	private String bookDocManagerDate;
	private String bookDate;
	private String approveStatus;
	private String approveDate;
	private String approveRemark;
	
	private String reqDateFrom;
	private String reqDateTo;
	
	private long reqDocId;
	private String reqDocPath;
	
	private long masDocId;
	private Integer docIndex;
	private String documentName;

	private long orgId;
	private String orgName;
	
	private String ttAaNo;
	private String ttAaDate;
	/**
	 * sss.dot.tourism.domain.Trader
	 */
	private long traderId;
	private String traderType;
	private String traderTypeName;
	
	private String traderName;
	private String traderOwnerName;
	private String traderNameEn;
	private String traderCategory;
	private String traderCategoryName;
	private int page;
	private int start;
	private int limit;
	
	private String viewfile;
	
	public boolean isCerIdNotEmpty(){
		return (cerId > 0);
	}
	
	public long getCerId() {
		return cerId;
	}
	public void setCerId(long cerId) {
		this.cerId = cerId;
	}
	public String getLicenseNo() {
		return licenseNo;
	}
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	public String getReqDate() {
		return reqDate;
	}
	public void setReqDate(String reqDate) {
		this.reqDate = reqDate;
	}
	public String getPrintDate() {
		return printDate;
	}
	public void setPrintDate(String printDate) {
		this.printDate = printDate;
	}
	public String getApproveStatus() {
		return approveStatus;
	}
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	public String getApproveDate() {
		return approveDate;
	}
	public void setApproveDate(String approveDate) {
		this.approveDate = approveDate;
	}
	public String getApproveRemark() {
		return approveRemark;
	}
	public void setApproveRemark(String approveRemark) {
		this.approveRemark = approveRemark;
	}
	public long getReqDocId() {
		return reqDocId;
	}
	public void setReqDocId(long reqDocId) {
		this.reqDocId = reqDocId;
	}
	public String getReqDocPath() {
		return reqDocPath;
	}
	public void setReqDocPath(String reqDocPath) {
		this.reqDocPath = reqDocPath;
	}
	public long getMasDocId() {
		return masDocId;
	}
	public void setMasDocId(long masDocId) {
		this.masDocId = masDocId;
	}
	public Integer getDocIndex() {
		return docIndex;
	}
	public void setDocIndex(Integer docIndex) {
		this.docIndex = docIndex;
	}
	public String getDocumentName() {
		return documentName;
	}
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	public long getOrgId() {
		return orgId;
	}
	public void setOrgId(long orgId) {
		this.orgId = orgId;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public long getTraderId() {
		return traderId;
	}
	public void setTraderId(long traderId) {
		this.traderId = traderId;
	}
	public String getTraderType() {
		return traderType;
	}
	public void setTraderType(String traderType) {
		this.traderType = traderType;
	}
	public String getTraderTypeName() {
		return traderTypeName;
	}
	public void setTraderTypeName(String traderTypeName) {
		this.traderTypeName = traderTypeName;
	}
	public String getTraderName() {
		return traderName;
	}
	public void setTraderName(String traderName) {
		this.traderName = traderName;
	}
	public String getTraderNameEn() {
		return traderNameEn;
	}
	public void setTraderNameEn(String traderNameEn) {
		this.traderNameEn = traderNameEn;
	}
	public String getReqDateFrom() {
		return reqDateFrom;
	}
	public void setReqDateFrom(String reqDateFrom) {
		this.reqDateFrom = reqDateFrom;
	}
	public String getReqDateTo() {
		return reqDateTo;
	}
	public void setReqDateTo(String reqDateTo) {
		this.reqDateTo = reqDateTo;
	}
	public String getTraderCategory() {
		return traderCategory;
	}
	public void setTraderCategory(String traderCategory) {
		this.traderCategory = traderCategory;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public String getTraderOwnerName() {
		return traderOwnerName;
	}

	public void setTraderOwnerName(String traderOwnerName) {
		this.traderOwnerName = traderOwnerName;
	}

	public String getTraderCategoryName() {
		return traderCategoryName;
	}

	public void setTraderCategoryName(String traderCategoryName) {
		this.traderCategoryName = traderCategoryName;
	}

	public String getViewfile() {
		return viewfile;
	}

	public void setViewfile(String viewfile) {
		this.viewfile = viewfile;
	}

	public String getBookNo() {
		return bookNo;
	}

	public void setBookNo(String bookNo) {
		this.bookNo = bookNo;
	}

	public String getBookDate() {
		return bookDate;
	}

	public void setBookDate(String bookDate) {
		this.bookDate = bookDate;
	}



	public String getBookDocToManagerNo() {
		return bookDocToManagerNo;
	}

	public void setBookDocToManagerNo(String bookDocToManagerNo) {
		this.bookDocToManagerNo = bookDocToManagerNo;
	}

	public String getBookDocManagerDate() {
		return bookDocManagerDate;
	}

	public void setBookDocManagerDate(String bookDocManagerDate) {
		this.bookDocManagerDate = bookDocManagerDate;
	}

	public String getTtAaNo() {
		return ttAaNo;
	}

	public void setTtAaNo(String ttAaNo) {
		this.ttAaNo = ttAaNo;
	}

	public String getTtAaDate() {
		return ttAaDate;
	}

	public void setTtAaDate(String ttAaDate) {
		this.ttAaDate = ttAaDate;
	}

	public String getLicenseNoEn() {
		return licenseNoEn;
	}

	public void setLicenseNoEn(String licenseNoEn) {
		this.licenseNoEn = licenseNoEn;
	}
	
	
}
