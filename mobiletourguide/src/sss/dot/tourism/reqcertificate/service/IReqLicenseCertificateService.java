package sss.dot.tourism.reqcertificate.service;



import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.sss.aut.service.User;

public interface IReqLicenseCertificateService {
	
	
	public Object getById(Object obj, User user) throws Exception;
	
	public void saveAll(Object obj, User user)throws Exception;
	
	public void delete(Object obj, User user)throws Exception;
	
	
	public List getByPage(Object object, User user ,int start, int limit) throws Exception;
	
	public int countAll(Object object, User user ,int start, int limit) throws Exception;
	
	public List getDocuments(Object obj, User user)throws Exception;
	
	public void saveDocument(long reqDocId,MultipartFile documentFile, User user)throws Exception;
	
	public void saveDocumentAll(Object object, List<MultipartFile> files, User user)throws Exception;
	
	public Object getDocumentsById(Object obj, User user)throws Exception;
	
	public Object getReqCertificateDocx(Object obj, User user) throws Exception;
	
	public Map<String, ?> getReqCertificateExcel(Object object, User user)throws Exception;
	
	
}
