package sss.dot.tourism.service.admin;

import java.util.List;

import com.sss.aut.service.User;

public interface IUserAccountService {

	public Object getUser(String user, String password) throws Exception;
	
	public Object getUserById(long userId) throws Exception;
	
	public List getAdmGroup(Object obj, User user) throws Exception;
	
	public void saveAdmUser(Object obj, User user)throws Exception;
	
	public void deleteAdmUser(Object[]  obj, User user)throws Exception;
	
	public List getAdmUserAll(Object object, User user ,int start, int limit) throws Exception;
	
	public List countAdmUserAll(Object object, User user ,int start, int limit) throws Exception;
}
