package sss.dot.tourism.service.admin;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.AdmGroupDAO;
import sss.dot.tourism.dao.AdmUserDAO;
import sss.dot.tourism.dao.AdmUserGroupDAO;
import sss.dot.tourism.domain.AdmGroup;
import sss.dot.tourism.domain.AdmUser;
import sss.dot.tourism.domain.AdmUserGroup;
import sss.dot.tourism.domain.DeactivateDetail;
import sss.dot.tourism.domain.MasDeactivateType;
import sss.dot.tourism.domain.MasPrefix;
import sss.dot.tourism.domain.Organization;
import sss.dot.tourism.dto.mas.AdmGroupDTO;
import sss.dot.tourism.dto.mas.AdmUserDTO;
import sss.dot.tourism.dto.mas.OfficerDTO;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.RecordStatus;

import com.sss.aut.service.User;
import com.sss.aut.service.UserData;
import com.sss.aut.service.imp.DefaultUser;
import com.sss.aut.service.imp.DefaultUserData;
import com.sss.aut.service.imp.PasswordEncoding;

@Repository("userAccountService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class UserAccountService implements IUserAccountService{

	@Autowired
	AdmUserDAO admUserDAO;
	@Autowired
	AdmUserGroupDAO admUserGroupDAO;
	@Autowired
	AdmGroupDAO admGroupDAO;
	
	
	

	public Object getUserById(long userId) throws Exception {
		AdmUser admUser =  (AdmUser)this.admUserDAO.findByPrimaryKey(userId);
		
		OfficerDTO dto = new OfficerDTO();
		if(admUser != null)
		{
			

			dto.setOfficerId(admUser.getUserId());
			
			String userFullName = admUser.getMasPrefix().getPrefixName() + admUser.getUserName() +" "+admUser.getUserLastname();
			dto.setOfficerName(userFullName);
			dto.setPosition("เจ้าหน้าที่สำนักทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์");
		}
		
		return dto;
	}


	public Object getUser(String userName, String password) throws Exception {
		
		User user = null;
		String fromParam = PasswordEncoding.toEncodeSHA256(userName, password);
		System.out.println(fromParam);
		List<AdmUser> listAll = this.admUserDAO.findUser(userName, fromParam);
		
		if(!listAll.isEmpty())
		{
			AdmUser admUser = listAll.get(0);
			
			List<AdmGroup> listRole = this.admUserGroupDAO.findRole(admUser.getUserId());
			
			Map role = new HashMap();
			if(!listRole.isEmpty())
			{
				for(AdmGroup group: listRole)
				{
					role.put(group.getGroupRole(), group.getGroupId());
				}
			}
			
	    	String userFullName = admUser.getMasPrefix().getPrefixName() + admUser.getUserName() +" "+admUser.getUserLastname();
	    	
	    	UserData userData = new DefaultUserData(
	    			admUser.getUserId()
	    			, admUser.getUserLogin()
	    			, userFullName
	    			, admUser.getUserName()
	    			, admUser.getUserLastname()
	    			, admUser.getMasPrefix().getPrefixName()
	    			, role
	    			, "เจ้าหน้าที่สำนักทะเบียนธุรกิจนำเที่ยวและมัคคุเทศก์"
	    			, admUser.getOrganization().getOrgId());
	    
	    	user = new DefaultUser(userData.getUserId() , userData.getUserName(),userData);
		}
		
		return user;
	}


	public List getAdmGroup(Object obj, User user) throws Exception {
		if(!(obj instanceof AdmGroupDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		AdmGroupDTO param =  (AdmGroupDTO)obj;

		List<AdmGroupDTO> list = new ArrayList<AdmGroupDTO>();
		
		if(param.getUserId() > 0)
		{
			list = this.admUserGroupDAO.findbyUserId(param.getUserId(), param.getOfficerGroup());
		}
		else
		{
			List<AdmGroup> listAll = (List<AdmGroup>) this.admGroupDAO.findGroup(param.getOfficerGroup());
			
			
			if(!listAll.isEmpty())
			{
				for(AdmGroup act: listAll)
				{
					AdmGroupDTO dto = new AdmGroupDTO();
					ObjectUtil.copy(act, dto);
					list.add(dto);
				}
			}
		}
		
		return list;
	}

	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveAdmUser(Object obj, User user) throws Exception
	{
		
		if(!(obj instanceof AdmUserDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		AdmUserDTO param = (AdmUserDTO)obj;
		AdmUser admUser = null;
		if(param.getUserId() <= 0)
		{
			admUser = new AdmUser();
			
			ObjectUtil.copy(param, admUser);
			
		    String passEncode = PasswordEncoding.encodeSHA256(param.getLoginPassword());
		    passEncode = PasswordEncoding.toEncodeSHA256(param.getUserLogin(), passEncode);
		    
		    admUser.setLoginPassword(passEncode);
			
			Organization org = new Organization();
			org.setOrgId(param.getOrgId());
			admUser.setOrganization(org);
			
			MasPrefix masPrefix = new MasPrefix();
			masPrefix.setPrefixId(param.getPrefixId());
			admUser.setMasPrefix(masPrefix);
						
			admUser.setRecordStatus(RecordStatus.NORMAL.getStatus());
			admUser.setCreateUser(user.getUserName());
			admUser.setCreateDtm(new Date());
			
			Long userId = (Long)this.admUserDAO.insert(admUser);
			param.setUserId(userId);

		}
		else
		{
			admUser = (AdmUser) this.admUserDAO.findByPrimaryKey(param.getUserId());
			
			ObjectUtil.copy(param, admUser);
			
			if((param.getChangePassword() != null)&& (param.getChangePassword().equals("1")))
			{
			    String passEncode = PasswordEncoding.encodeSHA256(param.getLoginPassword());
			    passEncode = PasswordEncoding.toEncodeSHA256(param.getUserLogin(), passEncode);
			    
			    admUser.setLoginPassword(passEncode);
			}
			
			Organization org = new Organization();
			org.setOrgId(param.getOrgId());
			admUser.setOrganization(org);
			
			MasPrefix masPrefix = new MasPrefix();
			masPrefix.setPrefixId(param.getPrefixId());
			admUser.setMasPrefix(masPrefix);
						
			admUser.setRecordStatus(RecordStatus.NORMAL.getStatus());
			admUser.setLastUpdUser(user.getUserName());
			admUser.setLastUpdDtm(new Date());
			
			this.admUserDAO.update(admUser);
		}
		
		if((param.getListUserMenu() != null) && (param.getListUserMenu().length > 0))
		{
			this.addAdmUserGroup(param.getListUserMenu(), admUser, user);
		}
		if((param.getListUserGroup() != null) && (param.getListUserGroup().length > 0))
		{
			this.addAdmUserGroup(param.getListUserGroup(), admUser, user);
		}
		
		
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	private void addAdmUserGroup(Object[] object, AdmUser admUser, User user) throws Exception
	{
		if(object.length > 0)
		{
			List<AdmUserGroup> listOldAct = this.admUserGroupDAO.findMapUserGroup(admUser.getUserId());
			if(!listOldAct.isEmpty())
			{
				for(AdmUserGroup admUserGroup: listOldAct)
				{
					this.admUserGroupDAO.delete(admUserGroup);
				}
			}
			
			for(Object obj: object)
			{
				AdmGroupDTO groupDTO = (AdmGroupDTO)obj;
				AdmUserGroup admUserGroup = new AdmUserGroup();
				AdmGroup admGroup = new AdmGroup();
				
				admGroup.setGroupId(groupDTO.getGroupId());
				
				admUserGroup.setAdmGroup(admGroup);
				admUserGroup.setAdmUser(admUser);
				
				admUserGroupDAO.insert(admUserGroup);
			}
		}
	}


	public List getAdmUserAll(Object obj, User user, int start, int limit)
			throws Exception {
		if(!(obj instanceof AdmUserDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		List<AdmUserDTO> list = new ArrayList<AdmUserDTO>();
		
		AdmUserDTO param = (AdmUserDTO)obj;
		List<AdmUser> listAdmUser = (List<AdmUser>) this.admUserGroupDAO.findUser(param, start, limit);
		
		if(!listAdmUser.isEmpty())
		{
			for(AdmUser admUser: listAdmUser)
			{
				AdmUserDTO dto = new AdmUserDTO();
				
				ObjectUtil.copy(admUser, dto);
				
				if(admUser.getOrganization() != null)
				{
					dto.setOrgId(admUser.getOrganization().getOrgId());
					dto.setOrgName(admUser.getOrganization().getOrgName());
				}
				
				if(admUser.getMasPrefix() != null)
				{
					dto.setPrefixId(admUser.getMasPrefix().getPrefixId());
					dto.setPrefixName(admUser.getMasPrefix().getPrefixName());
				}
				
				String userFullName = dto.getPrefixName()==null?"":dto.getPrefixName() + dto.getUserName() +" "+ dto.getUserLastname();
				dto.setUserFullName(userFullName);
				list.add(dto);
			}
		}
		
		return list;
	}


	public List countAdmUserAll(Object obj, User user, int start, int limit)
			throws Exception {
		if(!(obj instanceof AdmUserDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		AdmUserDTO param = (AdmUserDTO)obj;
		return (List<AdmUser>) this.admUserGroupDAO.findUser(param, start, limit);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void deleteAdmUser(Object[] object, User user) throws Exception {
		if(!(object instanceof AdmUserDTO[]))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		for(Object obj: object)
		{
			AdmUserDTO param = (AdmUserDTO)obj;
			AdmUser admUser = (AdmUser) this.admUserDAO.findByPrimaryKey(param.getUserId());
			
			List<AdmUserGroup> listOldAct = this.admUserGroupDAO.findMapUserGroup(admUser.getUserId());
			if(!listOldAct.isEmpty())
			{
				for(AdmUserGroup admUserGroup: listOldAct)
				{
					this.admUserGroupDAO.delete(admUserGroup);
				}
			}
			
			
			this.admUserDAO.deleteWithStatus(admUser);
		}
		

		
	}
	
	
}
