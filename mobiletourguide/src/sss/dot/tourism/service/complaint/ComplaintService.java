package sss.dot.tourism.service.complaint;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import sss.dot.tourism.dao.complaint.ComplaintDocDAO;
import sss.dot.tourism.dao.complaint.ComplaintLicenseDAO;
import sss.dot.tourism.dao.complaint.ComplaintMapTraderDAO;
import sss.dot.tourism.dao.complaint.ComplaintProgressDAO;
import sss.dot.tourism.dao.info.TraderDAO;
import sss.dot.tourism.dao.mas.MasRunningNoDAO;
import sss.dot.tourism.domain.ComplaintDoc;
import sss.dot.tourism.domain.ComplaintLicense;
import sss.dot.tourism.domain.ComplaintMapTrader;
import sss.dot.tourism.domain.ComplaintProgress;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.dto.complaint.ComplaintDocDTO;
import sss.dot.tourism.dto.complaint.ComplaintLicenseDTO;
import sss.dot.tourism.util.ComplaintStatus;
import sss.dot.tourism.util.ConstantUtil;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.FileManage;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.RecordStatus;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.User;

@Repository("complaintService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ComplaintService implements IComplaintService{

	@Autowired
	ComplaintLicenseDAO complaintLicenseDAO; 
	@Autowired
	ComplaintDocDAO complaintDocDAO;
	@Autowired
	MasRunningNoDAO masRunningNoDAO;
	@Autowired
	ComplaintMapTraderDAO complaintMapTraderDAO;
	@Autowired
	ComplaintProgressDAO complaintProgressDAO;
	@Autowired
	TraderDAO traderDAO;
	

	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveCompDoc(ComplaintDocDTO object, MultipartFile file)throws Exception {
		// TODO Auto-generated method stub
		

		ComplaintDocDTO params = (ComplaintDocDTO)object;

		String realFileName = Calendar.getInstance().getTime().getTime()+"";
		
		Calendar cal = Calendar.getInstance();
		DateFormat f = DateUtils.getProcessDateFormatEng();
		
		String date = f.format(cal.getTime());
		String fold = date.replaceAll("[-]", "");
		
		//postfix FileName
		String extension = ".txt";
		String originalFilename = file.getOriginalFilename();
		int dot = originalFilename.lastIndexOf('.');
		if(dot != -1)
		{
			extension = originalFilename.substring(dot);
		}
		
		//TraderType
		String traderTypeName = "";
		if(params.getTraderType().equals(TraderType.TOUR_COMPANIES.getStatus()))
		{
			traderTypeName = "Business";
		}
		else if(params.getTraderType().equals(TraderType.GUIDE.getStatus()))
		{
			traderTypeName = "Guide";
		}
		else if(params.getTraderType().equals(TraderType.LEADER.getStatus()))
		{
			traderTypeName = "Tourleader";
		}
		else
		{
			traderTypeName = "FakeLicense";
		}
		
		//LicenseNo
		String licenseNoStr = params.getLicenseNo();
		licenseNoStr = licenseNoStr.replaceAll("[.]", "");
		licenseNoStr = licenseNoStr.replaceAll("[/]", "");
		licenseNoStr = licenseNoStr.replaceAll("[-]", "");

		
		String root = ConstantUtil.IMG_ROOT_PATH; //"/Users/Apple/Desktop/fileUpload/"; 
    	String path = root+"Complaint"+java.io.File.separator+traderTypeName+java.io.File.separator+licenseNoStr+java.io.File.separator+fold;
    	String fileName = java.io.File.separator+realFileName+extension;
    	
    	String fullpath = path+fileName;
    	System.out.println("full path = "+fullpath);
    	
    	if(file != null)
	    {
   			System.out.println("###CREATE");
			//Upload File
	    	FileManage fm = new FileManage();
	    	boolean b = fm.saveFile(file.getInputStream(), path, fileName);
	    	
	    	//Save ComplaintDoc
	    	if(b)
	    	{	
	    		ComplaintDoc persis = new ComplaintDoc();
	    		persis.setComplaintDocType(params.getComplaintDocType());
	    		persis.setComplaintDocName(params.getComplaintDocName());
	    		persis.setComplaintDocPath(fullpath);
	    		persis.setComplaintLicense((ComplaintLicense)this.complaintLicenseDAO.findByPrimaryKey(params.getComplaintId()));
	    		
	    		persis.setRecordStatus(RecordStatus.NORMAL.getStatus());
	    		persis.setCreateUser("People");
	    		persis.setCreateDtm(new Date());
	    		
	    		this.complaintDocDAO.insert(persis);
	    		
	    	}
	    	
	    }
    	
	}
	
	

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveComplaintReceive(Object object, MultipartFile[] file) throws Exception {
		// TODO Auto-generated method stub
		if(!(object instanceof ComplaintLicenseDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถบันทึกข้อมูลการร้องเรียนได้");
		}
		
		try
		{
			
			
			ComplaintLicenseDTO dto = (ComplaintLicenseDTO) object;
			Trader trader = null;
			String licenseNo = null;
			String traderType = null;
			String traderName = null;
			String genuineLicense = null;
			if(dto.getTraderId() >0)
			{
				trader = (Trader)this.traderDAO.findByPrimaryKey(dto.getTraderId());
				licenseNo = trader.getLicenseNo();
				traderType = trader.getTraderType();
				traderName = trader.getTraderName();
				genuineLicense = "G";
			}
			else
			{
				traderName = dto.getTraderName();
				genuineLicense = "F";
				
				Calendar cal = Calendar.getInstance();
				DateFormat f = DateUtils.getProcessDateFormatEng();
				
				String date = f.format(cal.getTime());
				String fold = date.replaceAll("[-]", "");
				
				licenseNo = "99-" + fold;
				traderType = dto.getTraderType();
			}
			
						
			ComplaintLicense comp = new ComplaintLicense();
			
			ObjectUtil.copy(dto, comp);
			
			//ComplaintNo
			String complaintNo = this.masRunningNoDAO.getComplaintNo(trader.getOrganization().getOrgId(), traderType);
			comp.setComplaintNo(complaintNo);
			
			//ComplaintStatus ,ComplaintProgress
			comp.setComplaintProgress("RL");
			comp.setComplaintStatus(ComplaintStatus.ADD.getStatus());
			//AuthorityComment
			comp.setAuthorityComment(dto.getAuthorityComment());
			comp.setComplaintDate(new Date());
			comp.setCreateUser("People");
			comp.setCreateDtm(new Date());
			comp.setRecordStatus(RecordStatus.NORMAL.getStatus());
			
			Long complaintLicenseId =  (Long)complaintLicenseDAO.insert(comp);
			
			comp.setComplaintId(complaintLicenseId);
			
			//Save ComplaintMapTrader
			ComplaintMapTrader complaintMapTraderPersis = new ComplaintMapTrader();
			complaintMapTraderPersis.setComplaintLicense(comp);
			complaintMapTraderPersis.setLicenseNo(licenseNo);
			complaintMapTraderPersis.setTraderType(traderType);
			
			complaintMapTraderPersis.setTraderName(traderName);
			
			//Check licenseNo มีอยู่จริงไหม
			complaintMapTraderPersis.setGenuineLicense(genuineLicense);
			//
			
			this.complaintMapTraderDAO.insert(complaintMapTraderPersis);
			
					
			//INSERT ComplaintProgress RL A
			ComplaintProgress complaintProgressPersis = new ComplaintProgress();
			complaintProgressPersis.setProgressRole("PE");
			complaintProgressPersis.setProgressStatus(comp.getComplaintStatus());
			
			complaintProgressPersis.setProgressDate(new Date());
			complaintProgressPersis.setAuthority(comp.getComplaintName());
			complaintProgressPersis.setRecordStatus(RecordStatus.NORMAL.getStatus());
			complaintProgressPersis.setCreateDtm(new Date());
			complaintProgressPersis.setCreateUser("People");
			complaintProgressPersis.setComplaintLicense(comp);
			if(comp.getQuestionType().equals("C"))
			{
				complaintProgressPersis.setProgressDesc("เพิ่มเรื่องร้องเรียน");
			}
			else
			{
				complaintProgressPersis.setProgressDesc("เพิ่มข้อซักถาม");
			}

			this.complaintProgressDAO.insert(complaintProgressPersis);
			
			//INSERT ComplaintProgress RL W
			ComplaintProgress complaintProgressPersis2 = new ComplaintProgress();
			complaintProgressPersis2.setProgressRole(comp.getComplaintProgress());
			complaintProgressPersis2.setProgressStatus(ComplaintStatus.WAIT.getStatus());
			
			complaintProgressPersis2.setProgressDate(new Date());
			complaintProgressPersis2.setAuthority("นิติการรับเรื่อง");
			complaintProgressPersis2.setRecordStatus(RecordStatus.NORMAL.getStatus());
			complaintProgressPersis2.setCreateDtm(new Date());
			complaintProgressPersis2.setCreateUser("People");
			complaintProgressPersis2.setComplaintLicense(comp);
			complaintProgressPersis2.setProgressDesc(ComplaintStatus.WAIT.getMeaning());
			
			this.complaintProgressDAO.insert(complaintProgressPersis2);

			
			for(MultipartFile f: file)
			{
				if(!f.isEmpty())
				{
					ComplaintDocDTO param = new ComplaintDocDTO();
					
					param.setComplaintId(comp.getComplaintId());
					param.setComplaintDocType("E");
					param.setComplaintDocName("หลักฐาน");
					param.setLicenseNo(licenseNo);
					param.setTraderType(traderType);
					
					this.saveCompDoc(param, f);
				}

			}
			
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("ไม่สามารถบันทึกข้อมูลการร้องเรียนได้");
		}
	}

	public static void main(String[] arg)
	{
//		Calendar cal = Calendar.getInstance();
//		DateFormat f = DateUtils.getProcessDateFormatEng();
//		System.out.println(f.format(cal.getTime()));
		
		ComplaintLicenseDTO param =  new ComplaintLicenseDTO();

	}
}












