package sss.dot.tourism.service.complaint;

import org.springframework.web.multipart.MultipartFile;

import sss.dot.tourism.dto.complaint.ComplaintDocDTO;

public interface IComplaintService {
	
	public void saveCompDoc(ComplaintDocDTO object, MultipartFile file)throws Exception;
	

	
	public void saveComplaintReceive(Object object, MultipartFile[] file) throws Exception ;
}
