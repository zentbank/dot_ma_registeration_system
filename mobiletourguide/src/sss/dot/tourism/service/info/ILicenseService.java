package sss.dot.tourism.service.info;

import java.util.List;

import com.sss.aut.service.User;

public interface ILicenseService {
	
	public List getAll(Object obj) throws Exception;
	
	public Object getByLicenseId(Object object) throws Exception;
	
	
	public List getByIdentityNo(Object object) throws Exception;
	
	public Object getByLicenseNo(String licenseNo, String licenseType) throws Exception;
	
	public List getByOfficer(long officerId, int start, int limit) throws Exception;
	
	public List saveVerify(Object[] object)throws Exception;
	
	public String getImage(long traderId) throws Exception;
}
