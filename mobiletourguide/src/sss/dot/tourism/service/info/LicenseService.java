package sss.dot.tourism.service.info;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.AdmUserDAO;
import sss.dot.tourism.dao.VerifyLicenseDAO;
import sss.dot.tourism.dao.info.TraderDAO;
import sss.dot.tourism.dao.registration.CommitteeDAO;
import sss.dot.tourism.dao.registration.PersonDAO;
import sss.dot.tourism.dao.registration.RegDocumentDAO;
import sss.dot.tourism.dao.registration.RegistrationDAO;
import sss.dot.tourism.dao.registration.TraderAddressDAO;
import sss.dot.tourism.domain.AdmUser;
import sss.dot.tourism.domain.Committee;
import sss.dot.tourism.domain.Person;
import sss.dot.tourism.domain.RegDocument;
import sss.dot.tourism.domain.Registration;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.domain.TraderAddress;
import sss.dot.tourism.domain.VerifyLicense;
import sss.dot.tourism.dto.info.LicenseDetailDTO;
import sss.dot.tourism.dto.info.LicenseInfoDTO;
import sss.dot.tourism.dto.info.LicenseRegistrationDTO;
import sss.dot.tourism.dto.info.RequestParamDTO;
import sss.dot.tourism.util.ConstantUtil;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.LicenseStatus;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.PersonType;
import sss.dot.tourism.util.RecordStatus;
import sss.dot.tourism.util.RegistrationType;
import sss.dot.tourism.util.TraderAddressType;
import sss.dot.tourism.util.TraderCategory;
import sss.dot.tourism.util.TraderType;
@Repository("licenseService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class LicenseService implements ILicenseService {

	@Autowired
	private TraderDAO traderDAO;
	

	@Autowired
	RegistrationDAO registrationDAO;

	@Autowired
	PersonDAO personDAO;

	@Autowired
	TraderAddressDAO traderAddressDAO;
	@Autowired
	CommitteeDAO committeeDAO;
	@Autowired
	VerifyLicenseDAO verifyLicenseDAO;
	@Autowired
	AdmUserDAO admUserDAO;
	@Autowired
	RegDocumentDAO regDocumentDAO;


	/*
	 * [Language] ภาษาที่ใช้ในการค้นหา ประกอบด้วย
TH: ภาษาไทย
EN : ภาษาอังกฤษ
[SearchType] ประเภทการค้นหา ประกอบด้วย
T : เป็นการ Search ฐานข้อมูล Tourist Name
G : เป็นการ Search ฐานข้อมูล Guide
TL : เป็นการ Search ฐานข้อมูล Tour Leader
[SearchParam] ข้อความที่ต้องการค้นหาจาก Tex*/
	public List getAll(Object object) throws Exception {
		
		RequestParamDTO param = (RequestParamDTO)object;
		List<LicenseInfoDTO> list = new ArrayList<LicenseInfoDTO>();
		String traderType = "B";
		if(param.getSearchType().equals("T"))
		{
			traderType = TraderType.TOUR_COMPANIES.getStatus();
		}
		
		if(param.getSearchType().equals("G"))
		{
			
			traderType = TraderType.GUIDE.getStatus();
		}
		
		if(param.getSearchType().equals("TL"))
		{
			
			traderType = TraderType.LEADER.getStatus();
		}
		
		List<Object[]> listAll = this.traderDAO.findLicenseByTypeAndParam(traderType, param.getSearchParam());
		if(!listAll.isEmpty())
		{
			for(Object[] obj: listAll)
			{
				LicenseInfoDTO dto = new LicenseInfoDTO();
				/*
				 * /*
		 * 	“id” :  [Key ที่จะ Pass กลับไปในหน้า Show Result],
			“name_th” : [ชื่อไทย],
			“name_en” : [ชื่ออังกฤษ],
			“reg_id” : [เลขที่ใบอนุญาต]
			“reg_status” : [สถานะการจดทะเบียน],
			
				    hql.append(" 	TR.TRADER_NAME, ");//0
	    hql.append(" 	TR.TRADER_NAME_EN, ");//1
	    hql.append(" 	TR.LICENSE_NO, ");//2
	    hql.append(" 	TR.LICENSE_STATUS, ");//3
	    hql.append(" 	PE.FIRST_NAME, ");//4
	    hql.append(" 	PE.LAST_NAME, ");//5
	    hql.append(" 	PE.FIRST_NAME_EN, ");//6
	    hql.append(" 	PE.LAST_NAME_EN, ");//7
	    hql.append(" 	PX.PREFIX_NAME, ");//8
	    hql.append(" 	PX.PREFIX_NAME_EN, ");//9
	    hql.append(" 	PO.POSTFIX_NAME ");//10
	    TRADER_ID//11
			
			*/
				String licenseStatus = LicenseStatus.getMeaning(String.valueOf(obj[3]));
				
				if(param.getSearchType().equals("T"))
				{
					String prefixName = obj[8]==null?"":obj[8].toString();
	    			String prefixNameEn = obj[9]==null?"":obj[9].toString();
	    			String postFixname = obj[10]==null?"":obj[10].toString();
	    			StringBuffer nameTh = new StringBuffer();
//	    			nameTh.append(prefixName);
	    			nameTh.append(obj[0]==null?"":obj[0].toString());
//	    			nameTh.append(postFixname);
	    			StringBuffer nameEn = new StringBuffer();
//	    			nameEn.append(prefixNameEn);
	    			nameEn.append(obj[1]==null?"":obj[1].toString());
//	    			nameEn.append(postFixname);
				
					dto.setId(obj[11]==null?"0":String.valueOf(obj[11]));
					dto.setName_th(param.getLanguage().equals("TH")?nameTh.toString():nameEn.toString());
					dto.setName_en(param.getLanguage().equals("EN")?nameTh.toString():nameEn.toString());
					dto.setReg_id(obj[2]==null?"":String.valueOf(obj[2]));
					dto.setReg_status(licenseStatus);
				}else
				{
					/*
				    hql.append(" 	TR.TRADER_NAME, ");//0
	    hql.append(" 	TR.TRADER_NAME_EN, ");//1
	    hql.append(" 	TR.LICENSE_NO, ");//2
	    hql.append(" 	TR.LICENSE_STATUS, ");//3
	    hql.append(" 	PE.FIRST_NAME, ");//4
	    hql.append(" 	PE.LAST_NAME, ");//5
	    hql.append(" 	PE.FIRST_NAME_EN, ");//6
	    hql.append(" 	PE.LAST_NAME_EN, ");//7
	    hql.append(" 	PX.PREFIX_NAME, ");//8
	    hql.append(" 	PX.PREFIX_NAME_EN, ");//9
	    hql.append(" 	PO.POSTFIX_NAME ");//10
	    TRADER_ID//11
	    */
	    			String prefixName = obj[8]==null?"":obj[8].toString();
	    			String prefixNameEn = obj[9]==null?"":obj[9].toString();
	    			StringBuffer nameTh = new StringBuffer();
	    			nameTh.append(prefixName);
	    			nameTh.append(obj[4]==null?"":obj[4].toString() + " ");
	    			nameTh.append(obj[5]==null?"":obj[5].toString());
	    			
	    			StringBuffer nameEn = new StringBuffer();
					nameEn.append(prefixNameEn);
					nameEn.append(obj[6]==null?"":obj[6].toString() + " ");
					nameEn.append(obj[7]==null?"":obj[7].toString());
					
					
					dto.setId(obj[11]==null?"0":String.valueOf(obj[11]));
					dto.setName_th(param.getLanguage().equals("TH")?nameTh.toString():nameEn.toString());
					dto.setName_en(param.getLanguage().equals("EN")?nameTh.toString():nameEn.toString());
					dto.setReg_id(obj[2]==null?"":String.valueOf(obj[2]));
					dto.setReg_status(licenseStatus);
					

					dto.setImg_url(ConstantUtil.ROOT_PATH+"image/id/" + dto.getId());
				}
				
				list.add(dto);
			}
		}
		else
		{
			LicenseInfoDTO dto = new LicenseInfoDTO();
			dto.setName_th("ไม่พบข้อมูล");
			
			list.add(dto);
		}
		
		return list;
	}
	
	



	public Object getByLicenseId(Object object) throws Exception {

		if (!(object instanceof LicenseDetailDTO)) {
			throw new IllegalArgumentException("ไม่พบข้อมูล");
		}
		
		DateFormat dateformat = DateUtils.getProcessDateFormatThai();

		LicenseDetailDTO obj = (LicenseDetailDTO) object;
		
		LicenseDetailDTO dto = new LicenseDetailDTO();
		
		System.out.println(obj.getTraderId());

		Trader trader = (Trader) this.traderDAO.findByPrimaryKey(obj
				.getTraderId());
		
		
		Person person = trader.getPerson();
		
		ObjectUtil.copy(trader, person);

		dto.setTraderId(trader.getTraderId());
		dto.setLicenseNo(trader.getLicenseNo());
		dto.setTraderType(trader.getTraderType());
		dto.setTraderTypeName(TraderType.getMeaning(trader.getTraderType()));
//		dto.setTraderName(trader.getTraderName());
		
		
		if ((null != trader.getLicenseStatus())
				&& (!trader.getLicenseStatus().isEmpty())) {
			dto.setLicenseStatus(LicenseStatus.getMeaning(trader
					.getLicenseStatus()));
		}

		if ((null != trader.getTraderCategory())
				&& (!trader.getTraderCategory().isEmpty())) {
			dto.setTraderCategory(TraderCategory.getMeaning(
					trader.getTraderType(), trader.getTraderCategory()));
		}

		List<TraderAddress> listTraderAddress = (List<TraderAddress>) this.traderAddressDAO
				.findAllByTrader(dto.getTraderId(),
						TraderAddressType.OFFICE_ADDRESS.getStatus(),
						null);

		if (!listTraderAddress.isEmpty()) {
			TraderAddress add = listTraderAddress.get(0);
			StringBuilder traderAddress = new StringBuilder();

			traderAddress.append("ตั้งอยู่เลขที่ ");
			traderAddress.append(add.getAddressNo());
			traderAddress.append(" ถนน ");
			traderAddress.append(add.getRoadName());
			traderAddress.append(" ตรอก/ซอย ");
			traderAddress.append(add.getSoi());

			if (add.getMasTambol() != null) {
				traderAddress.append("<br /> ตำบล/แขวง ");
				traderAddress.append(add.getMasTambol().getTambolName());
			}
			if (add.getMasAmphur() != null) {
				traderAddress.append("<br /> อำเภอ/เขต ");
				traderAddress.append(add.getMasAmphur().getAmphurName());
			}
			if (add.getMasProvince() != null) {
				traderAddress.append("<br /> จังหวัด ");
				traderAddress.append(add.getMasProvince().getProvinceName());
			}

			traderAddress.append("<br /> รหัสไปรษณีย์ ");
			traderAddress.append(add.getPostCode() == null ? "" : add
					.getPostCode());
			
			traderAddress.append("<br /> หมายเลขโทรศัพท์ ");
			traderAddress.append(add.getMobileNo() == null ? "" : add
					.getMobileNo());
			
			traderAddress.append("<br /> อีเมล ");
			traderAddress.append(add.getEmail() == null ? "" : add
					.getEmail());
			
			traderAddress.append("<br /> เวปไซด์ ");
			traderAddress.append(add.getWebsite() == null ? "" : add
					.getWebsite());

			dto.setTraderAddress(traderAddress.toString());
		}
		
		List<Registration> listReg = (List<Registration>) this.registrationDAO.findRegistrationByLicenseNo(trader.getLicenseNo(), trader.getTraderType());
		if(!listReg.isEmpty())
		{
			List<LicenseRegistrationDTO> listRegDTO = new ArrayList<LicenseRegistrationDTO>();
			for(Registration reg: listReg)
			{
				Trader regTrader = reg.getTrader();
				LicenseRegistrationDTO rdto = new LicenseRegistrationDTO();
				rdto.setRegistrationTypeName(RegistrationType.getMeaning(regTrader.getTraderType(), reg.getRegistrationType())+"เมื่อวันที่");
				
				if(regTrader.getEffectiveDate() != null)
				{
					String regDate = dateformat.format(regTrader.getEffectiveDate());
					String[] regDArr = regDate.split("/");
					String montName = DateUtils.getMonthName(regDArr[1]);
					rdto.setRegistrationDate(regDArr[0] +" "+ montName +" "+ regDArr[2]);
				}
				
				listRegDTO.add(rdto);
				
				
			}
			

			Registration reg = listReg.get(listReg.size()-1);

			Trader regTrader = reg.getTrader();
			if(regTrader.getExpireDate() != null)
			{
				LicenseRegistrationDTO rdto = new LicenseRegistrationDTO();
				rdto.setRegistrationTypeName("ครบกำหนดชำระค่าธรรมเนียมครั้งต่อไปวันที่");
			
			
				String regDate = dateformat.format(regTrader.getExpireDate());
				String[] regDArr = regDate.split("/");
				String montName = DateUtils.getMonthName(regDArr[1]);
				rdto.setRegistrationDate(regDArr[0] +" "+ montName +" "+ regDArr[2]);
				
				listRegDTO.add(rdto);
				
			}
			
			dto.setRegistration(listRegDTO);
		}
		
		if(TraderType.TOUR_COMPANIES.getStatus().equals(trader.getTraderType()))
		{
			dto.setTraderName(trader.getTraderName());
			dto.setTraderNameEn(trader.getTraderNameEn());
			
			List<Committee>  listComm = this.committeeDAO.findAllByPerson(person.getPersonId(), null, null);
			
			if(!listComm.isEmpty())
			{
				List<String> listStrCom = new ArrayList<String>();
				for(Committee comm: listComm)
				{
					StringBuilder name = new StringBuilder();
					if(comm.getMasPrefix() != null)
					{
						name.append(comm.getMasPrefix().getPrefixName());
						
					}
					name.append(comm.getCommitteeName());
					name.append(" ");
					name.append(comm.getCommitteeLastname());
					
					listStrCom.add(name.toString());
				}
				dto.setCommittee(listStrCom);
			}
		}
		else
		{
			StringBuffer name = new StringBuffer();
			StringBuffer nameEn = new StringBuffer();
			
			if(person.getMasPrefix() != null)
			{
				name.append(person.getMasPrefix().getPrefixName());
				nameEn.append(person.getMasPrefix().getPrefixNameEn());
			}
			name.append(person.getFirstName());
			name.append(" ");
			name.append(person.getLastName());
			
			nameEn.append(person.getFirstNameEn());
			nameEn.append(" ");
			nameEn.append(person.getLastNameEn());
			
			dto.setTraderName(name.toString());
			dto.setTraderNameEn(nameEn.toString());
			dto.setImgUrl(ConstantUtil.ROOT_PATH+"image/id/" + dto.getTraderId());
			
		}
		
		if(TraderType.GUIDE.getStatus().equals(trader.getTraderType()))
		{
			String provinceName = "";
			if(trader.getMasProvince() != null)
			{
				provinceName = trader.getMasProvince().getProvinceName();
			}
			String areaName = trader.getTraderArea()==null?"":trader.getTraderArea();
			
			StringBuffer str = new StringBuffer();
			str.append(dto.getTraderCategory());
			str.append("<br />");
			str.append(provinceName);
			str.append(areaName);
			
			dto.setTraderCategory(str.toString());
			
		}
		
		
		
		//complaintName
		List listComplaint = new ArrayList();
		dto.setComplaint(listComplaint);

		return dto;
	}



	public List getByIdentityNo(Object object) throws Exception {

		if (!(object instanceof LicenseDetailDTO)) {
			throw new IllegalArgumentException("ไม่พบข้อมูล");
		}
		
		
		List<LicenseDetailDTO> list = new ArrayList<LicenseDetailDTO>();

		LicenseDetailDTO param = (LicenseDetailDTO) object;
		
		List<Trader> listTrader = (List<Trader>) this.traderDAO.findLicense(param.getIndentityNo(), null, null, null, RecordStatus.NORMAL.getStatus(), RecordStatus.NORMAL.getStatus(),null);
		
		if(!listTrader.isEmpty())
		{
			for(Trader trader: listTrader)
			{
				LicenseDetailDTO arg = new LicenseDetailDTO();
				arg.setTraderId(trader.getTraderId());
				LicenseDetailDTO dto = (LicenseDetailDTO)this.getByLicenseId(arg);
			
				list.add(dto);
			}
		}
		
		return list;
	}



	public Object getByLicenseNo(String licenseNo, String licenseType)
			throws Exception {
				
		LicenseDetailDTO dto = new LicenseDetailDTO();
		
		List<Trader> listTrader = (List<Trader>) this.traderDAO.findLicense(null, licenseNo, null, null, RecordStatus.NORMAL.getStatus(), RecordStatus.NORMAL.getStatus(),licenseType);
		
		if(!listTrader.isEmpty())
		{
				Trader trader = listTrader.get(0);
			
				LicenseDetailDTO arg = new LicenseDetailDTO();
				arg.setTraderId(trader.getTraderId());
				dto = (LicenseDetailDTO)this.getByLicenseId(arg);
		}
		
		return dto;
	}



	public List getByOfficer(long officerId, int start, int limit)
			throws Exception {
		List<VerifyLicense> listAll = (List<VerifyLicense>) this.verifyLicenseDAO.findVerifyTrader(officerId ,start, limit);
		
		List<LicenseDetailDTO> list = new ArrayList<LicenseDetailDTO>();
		
		if(!listAll.isEmpty())
		{
			for(VerifyLicense verify: listAll)
			{
				List<Trader> listTrader = (List<Trader>)this.traderDAO.findTraderByLicenseNo(verify.getLicenseNo(), verify.getLicenseType(), RecordStatus.NORMAL.getStatus(), 0);
				
				if(!listTrader.isEmpty())
				{
					Trader trader = (Trader) listTrader.get(0);
					
					LicenseDetailDTO dto = new LicenseDetailDTO();
					
					dto.setLicenseNo(trader.getLicenseNo()==null?"":trader.getLicenseNo());
				
					if((trader.getTraderCategory() != null) && (!trader.getTraderCategory().isEmpty()))
					{
						dto.setTraderCategory(TraderCategory.getMeaning(trader.getTraderType(), trader.getTraderCategory()));
					}
					
					if(trader.getPerson() != null)
					{
						Person person = trader.getPerson();
						
						if(PersonType.CORPORATE.getStatus().equals(person.getPersonType()))
						{
							String traderOwnerName = person.getMasPrefix()==null?"":person.getMasPrefix().getPrefixName() + person.getFirstName() + (person.getMasPosfix()==null?"":person.getMasPosfix().getPostfixName());
							dto.setTraderName(traderOwnerName);
						}
						else
						{
							String traderOwnerName =  person.getMasPrefix()==null?"":person.getMasPrefix().getPrefixName() + person.getFirstName() + " " + person.getLastName();
							dto.setTraderName(traderOwnerName);
							
						}
					}
					DateFormat df = DateUtils.getProcessDateFormatThai();
					dto.setInspectionDate(df.format(verify.getVerifyDate()));
					
					list.add(dto);
				}
				
				
			}
		}
		
		return list;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List saveVerify(Object[] object) throws Exception {
		
		List<LicenseDetailDTO> list = new ArrayList<LicenseDetailDTO>();
		
		for (Object obj : object)
		{
			LicenseDetailDTO dto =  (LicenseDetailDTO)obj;
			
			VerifyLicense license = new VerifyLicense();
			
			license.setLicenseNo(dto.getLicenseNo());
			license.setLicenseType(dto.getLicenseType());
			
			AdmUser admUser = (AdmUser)this.admUserDAO.findByPrimaryKey(dto.getAuthorityId());
			
			license.setAdmUser(admUser);
			license.setRecordStatus(RecordStatus.NORMAL.getStatus());
			license.setCreateUser(admUser.getUserName());
			license.setVerifyDate(new Date());
			license.setCreateDtm(new Date());
			
			Long verifyId = (Long)this.verifyLicenseDAO.insert(license);
			dto.setVerifyId(verifyId);
			
			list.add(dto);
		}
		
		return list;
	}





	public String getImage(long traderId) throws Exception {
		
		Trader trader = (Trader) this.traderDAO.findByPrimaryKey(traderId);
		long docImg = 0;
		if(TraderType.GUIDE.getStatus().equals(trader.getTraderType()))
		{
			docImg = ConstantUtil.GUIDE_DOC_IMG_ID;
		}
		else
		{
			docImg = ConstantUtil.TOURLEADER_DOC_IMG_ID;
		}
		
		List<RegDocument> listRecDoc = (List<RegDocument>) this.regDocumentDAO.findAllByTraderAndMasDoc(trader.getTraderId(), docImg);
		
		if(!listRecDoc.isEmpty())
		{
			RegDocument regDoc = listRecDoc.get(0);
			
			return regDoc.getDocPath();
		}
		else
		{
			return ConstantUtil.TEMPLATE_GUIDE_FILE_LOCATION+"blank.jpg";
		}
		
	}

	
}
