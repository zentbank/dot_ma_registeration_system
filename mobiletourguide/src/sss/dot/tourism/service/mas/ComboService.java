package sss.dot.tourism.service.mas;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.mas.MasAmphurDAO;
import sss.dot.tourism.dao.mas.MasPosfixDAO;
import sss.dot.tourism.dao.mas.MasPrefixDAO;
import sss.dot.tourism.dao.mas.MasProvinceDAO;
import sss.dot.tourism.dao.mas.MasTambolDAO;
import sss.dot.tourism.dao.mas.MasUniversityDAO;
import sss.dot.tourism.dao.registration.ForeignLanguageDAO;
import sss.dot.tourism.domain.Country;
import sss.dot.tourism.domain.MasAmphur;
import sss.dot.tourism.domain.MasPosfix;
import sss.dot.tourism.domain.MasPrefix;
import sss.dot.tourism.domain.MasProvince;
import sss.dot.tourism.domain.MasTambol;
import sss.dot.tourism.domain.MasUniversity;
import sss.dot.tourism.dto.mas.ForeignLanguageDTO;
import sss.dot.tourism.dto.mas.MasAmphurDTO;
import sss.dot.tourism.dto.mas.MasPosfixDTO;
import sss.dot.tourism.dto.mas.MasPrefixDTO;
import sss.dot.tourism.dto.mas.MasProvinceDTO;
import sss.dot.tourism.dto.mas.MasTambolDTO;
import sss.dot.tourism.dto.mas.MasUniversityDTO;
import sss.dot.tourism.util.ObjectUtil;


@Service("comboService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ComboService implements IComboService {
	
	@Autowired
	private MasPrefixDAO masPrefixDAO;
	
	@Autowired
	private MasPosfixDAO masPosfixDAO;
	
	@Autowired
	MasProvinceDAO masProvinceDAO;
	
	@Autowired
	MasAmphurDAO masAmphurDAO;
	
	@Autowired
	MasTambolDAO masTambolDAO;
	
	@Autowired
	MasUniversityDAO masUniversityDAO;
	
	@Autowired
	ForeignLanguageDAO foreignLanguageDAO;
	

	public List getPrefix(Object obj) throws Exception {
		
		if(!(obj instanceof MasPrefixDTO))
		{
			throw new IllegalArgumentException("argument is not instance of MasPrefixDTO");  
		}
		
		MasPrefixDTO param = new MasPrefixDTO();
		if(obj != null)
		{
			param = (MasPrefixDTO)obj;
		}

		
		List<MasPrefix> listAll = (List<MasPrefix>)this.masPrefixDAO.findAll(param);
		
		List<MasPrefixDTO> list = new ArrayList<MasPrefixDTO>();
		
		if(!listAll.isEmpty())
		{
			for(MasPrefix prefix: listAll)
			{
				MasPrefixDTO dto = new MasPrefixDTO();
				
				ObjectUtil.copy(prefix, dto);
				
				list.add(dto);
				
			}
		}
		
		return list;
	}
	
	public List getPostfix () throws Exception {
		
		List<MasPosfix> listAll = (List<MasPosfix>) this.masPosfixDAO.findAll();
		
		List<MasPosfixDTO> list = new ArrayList<MasPosfixDTO>();
		
		if(!listAll.isEmpty())
		{
			for(MasPosfix postfix: listAll)
			{
				MasPosfixDTO dto = new MasPosfixDTO();
				
				ObjectUtil.copy(postfix, dto);
				
				list.add(dto);
			}
		}
		
		return list;
	}

	public List getProvince(Object obj) throws Exception {
		
		if(!(obj instanceof MasProvinceDTO))
		{
			throw new IllegalArgumentException("argument is not instance of MasProvinceDTO");  
		}
		
		MasProvinceDTO param = new MasProvinceDTO();
		if(obj != null)
		{
			param = (MasProvinceDTO)obj;
		}
		
		List<MasProvince> listAll = (List<MasProvince>) this.masProvinceDAO.findAll(param);
		
		List<MasProvinceDTO> list = new ArrayList<MasProvinceDTO>();
		
		if(!listAll.isEmpty())
		{
			for(MasProvince postfix: listAll)
			{
				MasProvinceDTO dto = new MasProvinceDTO();
				
				ObjectUtil.copy(postfix, dto);
				
				list.add(dto);
			}
		}
		
		return list;
	}

	public List getAmphur(Object obj) throws Exception {
		
		if(!(obj instanceof MasAmphurDTO))
		{
			throw new IllegalArgumentException("argument is not instance of MasAmphurDTO");  
		}
		
		MasAmphurDTO param = new MasAmphurDTO();
		if(obj != null)
		{
			param = (MasAmphurDTO)obj;
		}
		
		List<MasAmphur> listAll = (List<MasAmphur>)this.masAmphurDAO.findAll(param);
		List<MasAmphurDTO> list = new ArrayList<MasAmphurDTO>();
		
		if(!listAll.isEmpty())
		{
			for(MasAmphur amphur : listAll)
			{
				MasAmphurDTO dto = new MasAmphurDTO();
				ObjectUtil.copy(amphur, dto);
				
				if(amphur.getMasProvince() != null)
				{
					dto.setProvinceId(amphur.getMasProvince().getProvinceId());
				}
				
				list.add(dto);
			}
		}
		
		return list;
	}

	public List getTambol(Object obj) throws Exception {

		if(!(obj instanceof MasTambolDTO))
		{
			throw new IllegalArgumentException("argument is not instance of MasTambolDTO");  
		}
		
		MasTambolDTO param = new MasTambolDTO();
		if(obj != null)
		{
			param = (MasTambolDTO)obj;
		}
		
		List<MasTambol> listAll = (List<MasTambol>)this.masTambolDAO.findAll(param);
		List<MasTambolDTO> list = new ArrayList<MasTambolDTO>();
		
		if(!listAll.isEmpty())
		{
			for(MasTambol tambol : listAll)
			{
				MasTambolDTO dto = new MasTambolDTO();
				ObjectUtil.copy(tambol, dto);
				
				if(tambol.getMasAmphur() != null)
				{
					dto.setAmphurId(tambol.getMasAmphur().getAmphurId());
				}
				
				list.add(dto);
			}
		}
		
		return list;
	}


	public List getUniversity(Object obj) throws Exception {
		List<MasUniversity> listAll = (List<MasUniversity>) this.masUniversityDAO.findAll();
		
		List<MasUniversityDTO> list = new ArrayList<MasUniversityDTO>();
		
		if(!listAll.isEmpty())
		{
			for(MasUniversity university: listAll)
			{
				MasUniversityDTO dto = new MasUniversityDTO();
				
				ObjectUtil.copy(university, dto);
				
				list.add(dto);
			}
		}
		
		return list;
	}


	
	
	public List getForeignLanguage(Object obj) throws Exception {
		
		if(!(obj instanceof ForeignLanguageDTO))
		{
			throw new IllegalArgumentException("argument is not instance of ForeignLanguageDTO");  
		}
		
		ForeignLanguageDTO param = new ForeignLanguageDTO();
		if(obj != null)
		{
			param = (ForeignLanguageDTO)obj;
		}
		
		List<Country> listAll = (List<Country>) this.foreignLanguageDAO.findAll(param);
		
		List<ForeignLanguageDTO> list = new ArrayList<ForeignLanguageDTO>();
		
		if(!listAll.isEmpty())
		{
			for(Country lang: listAll)
			{
				ForeignLanguageDTO dto = new ForeignLanguageDTO();
				
				ObjectUtil.copy(lang, dto);
				
				list.add(dto);
			}
		}
		
		return list;
	}
	
	

}
