package sss.dot.tourism.service.mas;

import java.util.List;

public interface IComboService {
	
	public List getPrefix(Object obj) throws Exception;
	public List getPostfix() throws Exception;
	
	public List getProvince(Object obj) throws Exception;
	
	public List getAmphur(Object obj) throws Exception;
	public List getTambol(Object obj) throws Exception;
	
	public List getUniversity(Object obj) throws Exception;
	public List getForeignLanguage(Object obj) throws Exception;

}
