package sss.dot.tourism.service.punishment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.punishment.PunishmentEvidenceDAO;
import sss.dot.tourism.domain.PunishmentEvidence;
import sss.dot.tourism.domain.RevokeLicense;
import sss.dot.tourism.domain.SuspensionLicense;
import sss.dot.tourism.dto.punishment.PunishmentEvidenceDTO;
import sss.dot.tourism.dto.punishment.RevokeLicenseDTO;
import sss.dot.tourism.dto.punishment.SuspensionLicenseDTO;
import sss.dot.tourism.util.ObjectUtil;

import com.sss.aut.service.User;
@Repository("punishmentEvidenceService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PunishmentEvidenceService implements IPunishmentEvidenceService {

	@Autowired
	PunishmentEvidenceDAO punishmentEvidenceDAO;
	@Autowired
	ISuspensionService suspensionService;
	@Autowired
	IRevokeService revokeService;
	
	
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
	public Object getById(Object obj, User user) throws Exception {
		if(!(obj instanceof PunishmentEvidenceDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		PunishmentEvidenceDTO param = (PunishmentEvidenceDTO)obj;
		
		PunishmentEvidence edv = (PunishmentEvidence)this.punishmentEvidenceDAO.findByPrimaryKey(param.getPunishmentId());
		
		PunishmentEvidenceDTO dto = new PunishmentEvidenceDTO();
		
		ObjectUtil.copy(edv, dto);
		
		if(edv.getSuspensionLicense() != null)
		{
			dto.setSuspendId(edv.getSuspensionLicense().getSuspendId());
			
			SuspensionLicenseDTO susDTO = new SuspensionLicenseDTO();
			
			SuspensionLicense sus = edv.getSuspensionLicense();
			
			ObjectUtil.copy(sus, susDTO);
			
			suspensionService.suspend(susDTO, user);
		}
		if(edv.getRevokeLicense() != null)
		{
			dto.setRevokeId(edv.getRevokeLicense().getRevokeId());
			
			RevokeLicenseDTO revDTO = new RevokeLicenseDTO();
			
			RevokeLicense rev = edv.getRevokeLicense();
			
			ObjectUtil.copy(rev, revDTO);
			
			
			revokeService.revoke(revDTO, user);
		}
		
		return dto;
	}

}
