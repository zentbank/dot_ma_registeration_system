package sss.dot.tourism.service.punishment;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.AdmUserDAO;
import sss.dot.tourism.dao.mas.MasPrefixDAO;
import sss.dot.tourism.dao.mas.MasRunningNoDAO;
import sss.dot.tourism.dao.punishment.RevokeAlertDAO;
import sss.dot.tourism.dao.punishment.RevokeDetailDAO;
import sss.dot.tourism.dao.punishment.RevokeLicenseDAO;
import sss.dot.tourism.dao.punishment.RevokeMapTraderDAO;
import sss.dot.tourism.domain.RevokeAlert;
import sss.dot.tourism.domain.RevokeLicense;
import sss.dot.tourism.domain.RevokeMapTrader;
import sss.dot.tourism.dto.punishment.RevokeLicenseDTO;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.service.trader.ITraderService;
import sss.dot.tourism.util.AlertStatus;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.RevokeStatus;

import com.sss.aut.service.User;
@Repository("revokeService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class RevokeService implements IRevokeService {


	
	@Autowired
	MasRunningNoDAO masRunningNoDAO;
	@Autowired
	AdmUserDAO admUserDAO;
	@Autowired
	MasPrefixDAO masPrefixDAO;
	@Autowired
	ITraderService traderService;
	
	@Autowired
	RevokeLicenseDAO revokeLicenseDAO;
	@Autowired
	RevokeMapTraderDAO revokeMapTraderDAO;
	@Autowired
	RevokeDetailDAO revokeDetailDAO;
	@Autowired
	RevokeAlertDAO revokeAlertDAO;
	
	
	

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
	public void revoke(Object obj, User user) throws Exception {
		if(!(obj instanceof RevokeLicenseDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		RevokeLicenseDTO param = (RevokeLicenseDTO)obj;
		RevokeLicense revokeLicense = (RevokeLicense)this.revokeLicenseDAO.findByPrimaryKey(param.getRevokeId());
		
		DateFormat df = DateUtils.getProcessDateFormatThai();
		
		Date revokeDate = new Date();
		revokeLicense.setRevokeDate(revokeDate);
		
		revokeLicense.setRevokeStatus(RevokeStatus.REVOKE.getStatus());
		revokeLicense.setLastUpdUser(user.getUserName());
		revokeLicense.setLastUpdDtm(new Date());
		
		this.revokeLicenseDAO.update(revokeLicense);
		
		List<RevokeMapTrader> listRevokeMapTrader =  this.revokeMapTraderDAO.findByRevoleLicense(revokeLicense.getRevokeId());
		
		RegistrationDTO regDTO = new RegistrationDTO();
		
		if(!listRevokeMapTrader.isEmpty())
		{
			RevokeMapTrader map = listRevokeMapTrader.get(0);
			regDTO.setLicenseNo(map.getLicenseNo());
			regDTO.setTraderType(map.getTraderType());
			
			regDTO.setPunishmentDate(revokeLicense.getRevokeDate());
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(revokeLicense.getRevokeDate());
			
			cal.add(Calendar.MONTH, 60);
			cal.add(Calendar.DATE, -1);
			
			regDTO.setPunishmentExpireDate(cal.getTime());
		}
		
		// suspend all trader
		this.traderService.revoke(regDTO, user);
		
		List<RevokeAlert> listRevoke = (List<RevokeAlert>) this.revokeAlertDAO.findByRevokeLicense(revokeLicense.getRevokeId());

		if(!listRevoke.isEmpty())
		{
			RevokeAlert alert = listRevoke.get(0);
			alert.setResponseDate(new Date());
			alert.setAlertStatus(AlertStatus.ACCEPT.getStatus());
			this.revokeAlertDAO.update(alert);
		}
	}



	public List getActAll(Object obj, User user) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}



	public Object getRevokeById(Object obj, User user) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}



	public Object getTraderPrepareRevoke(Object obj, User user)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}



	public void saveRevoke(Object revoke, Object[] object, User user)
			throws Exception {
		// TODO Auto-generated method stub
		
	}



	public List getAll(Object obj, User user) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}



	public List countAll(Object obj, User user) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}



	public void unRevoke(Object object, User user) throws Exception {
		// TODO Auto-generated method stub
		
	}



	public void cancelRevoke(Object object, User user) throws Exception {
		// TODO Auto-generated method stub
		
	}



	public Object getAlertAll(Object object, User user) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}



	public Object saveAlert(Object obj, User user) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}



	

}
