package sss.dot.tourism.service.punishment;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.AdmUserDAO;
import sss.dot.tourism.dao.mas.MasPrefixDAO;
import sss.dot.tourism.dao.mas.MasRunningNoDAO;
import sss.dot.tourism.dao.punishment.SuspensionAlertDAO;
import sss.dot.tourism.dao.punishment.SuspensionDetailDAO;
import sss.dot.tourism.dao.punishment.SuspensionLicenseDAO;
import sss.dot.tourism.dao.punishment.SuspensionMapTraderDAO;
import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.domain.SuspensionAlert;
import sss.dot.tourism.domain.SuspensionLicense;
import sss.dot.tourism.domain.SuspensionMapTrader;
import sss.dot.tourism.dto.punishment.SuspensionLicenseDTO;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.service.trader.ITraderService;
import sss.dot.tourism.util.AlertStatus;
import sss.dot.tourism.util.AlertType;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.RecordStatus;
import sss.dot.tourism.util.SuspendStatus;

import com.sss.aut.service.User;

@Repository("suspensionService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class SuspensionService implements ISuspensionService {
	@Autowired
	TraderDAO traderDAO;
	
	@Autowired
	SuspensionLicenseDAO suspensionLicenseDAO;
	@Autowired
	SuspensionMapTraderDAO suspensionMapTraderDAO;
	@Autowired
	SuspensionDetailDAO suspensionDetailDAO;
	
	@Autowired
	SuspensionAlertDAO suspensionAlertDAO;
	
	@Autowired
	MasRunningNoDAO masRunningNoDAO;
	@Autowired
	AdmUserDAO admUserDAO;
	@Autowired
	MasPrefixDAO masPrefixDAO;
	@Autowired
	ITraderService traderService;
	
	
	
	/**
	 * พักใช้ใบอนุญาต
	 */

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void suspend(Object obj, User user) throws Exception {

		if(!(obj instanceof SuspensionLicenseDTO))
		{
			throw new IllegalArgumentException("เกิดข้อผิดพลาด");
		}
		
		SuspensionLicenseDTO param = (SuspensionLicenseDTO)obj;
		SuspensionLicense suspendLicense = (SuspensionLicense)this.suspensionLicenseDAO.findByPrimaryKey(param.getSuspendId());
		
		DateFormat df = DateUtils.getProcessDateFormatThai();
		
		Date suspendFrom = new Date();
		suspendLicense.setSuspendFrom(suspendFrom);
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(suspendFrom);
		
		if(suspendLicense.getSuspendPeriod() > 0)
		{
			cal.add(Calendar.MONTH, suspendLicense.getSuspendPeriod());
		}
		
		if(suspendLicense.getSuspendPeriodDay() > 0)
		{
			cal.add(Calendar.DATE, suspendLicense.getSuspendPeriodDay());
		}
		
		cal.add(Calendar.DATE, -1);
		suspendLicense.setSuspendTo(cal.getTime());
		
		suspendLicense.setSuspendStatus(SuspendStatus.SUSPENSION.getStatus());
		suspendLicense.setLastUpdUser(user.getUserName());
		suspendLicense.setLastUpdDtm(new Date());
		
		this.suspensionLicenseDAO.update(suspendLicense);
		
		List<SuspensionMapTrader> listSuspensionMapTrader =  this.suspensionMapTraderDAO.findBySuspendLicense(suspendLicense.getSuspendId());
		
		RegistrationDTO regDTO = new RegistrationDTO();
		
		if(!listSuspensionMapTrader.isEmpty())
		{
			SuspensionMapTrader map = listSuspensionMapTrader.get(0);
			regDTO.setLicenseNo(map.getLicenseNo());
			regDTO.setTraderType(map.getTraderType());
			
			regDTO.setPunishmentDate(suspendLicense.getSuspendFrom());
			regDTO.setPunishmentExpireDate(suspendLicense.getSuspendTo());
		}
		
		// suspend all trader
		this.traderService.suspend(regDTO, user);
		
		List<SuspensionAlert> listAlert = (List<SuspensionAlert>) this.suspensionAlertDAO.findBySuspendLicense(suspendLicense.getSuspendId());
		
		if(!listAlert.isEmpty())
		{
			SuspensionAlert alert = listAlert.get(0);
			
//			alert.setAlertType(AlertType.EMAIl.getStatus());
			alert.setResponseDate(new Date());
			alert.setAlertStatus(AlertStatus.ACCEPT.getStatus());
			this.suspensionAlertDAO.update(alert);
		}
		
		
		
	}



	public List getActAll(Object obj, User user) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}



	public Object getSuspensionById(Object obj, User user) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}



	public Object getTraderPrepareSuspension(Object obj, User user)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}



	public void saveSuspension(Object suspension, Object[] object, User user)
			throws Exception {
		// TODO Auto-generated method stub
		
	}



	public List getAll(Object obj, User user) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}



	public List countAll(Object obj, User user) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}



	public void unSuspend(Object object, User user) throws Exception {
		// TODO Auto-generated method stub
		
	}



	public void cancelSuspend(Object object, User user) throws Exception {
		// TODO Auto-generated method stub
		
	}



	public void deactivateSuspend(Object object, User user) throws Exception {
		// TODO Auto-generated method stub
		
	}

	
	
	
}
