package sss.dot.tourism.service.renew;

import java.util.Map;

public interface ILicenseRenewService {

	public Object applyForLicenseRenew(Object object) throws Exception;
	
	public Map getPayments(Object object) throws Exception;
}
