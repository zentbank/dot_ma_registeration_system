package sss.dot.tourism.service.renew;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import sss.dot.tourism.dao.AdmUserDAO;
import sss.dot.tourism.dao.AdmUserGroupDAO;
import sss.dot.tourism.dao.info.TraderDAO;
import sss.dot.tourism.dao.mas.MasDocumentDAO;
import sss.dot.tourism.dao.mas.MasFeeDAO;
import sss.dot.tourism.dao.mas.MasRunningNoDAO;
import sss.dot.tourism.dao.registration.BranchDAO;
import sss.dot.tourism.dao.registration.CommitteeDAO;
import sss.dot.tourism.dao.registration.ForeignLanguageDAO;
import sss.dot.tourism.dao.registration.PersonDAO;
import sss.dot.tourism.dao.registration.PlantripDAO;
import sss.dot.tourism.dao.registration.PrintingCardDAO;
import sss.dot.tourism.dao.registration.PrintingLicenseDAO;
import sss.dot.tourism.dao.registration.RegDocumentDAO;
import sss.dot.tourism.dao.registration.RegisterFlowDAO;
import sss.dot.tourism.dao.registration.RegisterProgressDAO;
import sss.dot.tourism.dao.registration.RegistrationDAO;
import sss.dot.tourism.dao.registration.TraderAddressDAO;
import sss.dot.tourism.dao.training.EducationDAO;
import sss.dot.tourism.domain.AdmUser;
import sss.dot.tourism.domain.Committee;
import sss.dot.tourism.domain.Education;
import sss.dot.tourism.domain.ForeignLanguage;
import sss.dot.tourism.domain.MasDocument;
import sss.dot.tourism.domain.MasFee;
import sss.dot.tourism.domain.Organization;
import sss.dot.tourism.domain.Person;
import sss.dot.tourism.domain.Plantrip;
import sss.dot.tourism.domain.RegDocument;
import sss.dot.tourism.domain.RegisterProgress;
import sss.dot.tourism.domain.Registration;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.domain.TraderAddress;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.dto.renew.FileUploadForm;
import sss.dot.tourism.http.NumberFormatFactory;
import sss.dot.tourism.util.ConstantUtil;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.FileManage;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.PersonType;
import sss.dot.tourism.util.ProgressStatus;
import sss.dot.tourism.util.RecordStatus;
import sss.dot.tourism.util.RegistrationType;
import sss.dot.tourism.util.RoleStatus;
import sss.dot.tourism.util.ThaiBahtUtil;
import sss.dot.tourism.util.TraderType;

@Repository("licenseRenewService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class LicenseRenewService implements ILicenseRenewService {
	
	@Autowired
	MasRunningNoDAO masRunningNoDAO;
	@Autowired
	RegistrationDAO registrationDAO;
	@Autowired
	TraderDAO traderDAO;
	@Autowired
	RegisterProgressDAO registerProgressDAO;
	@Autowired
	PersonDAO personDAO;
	
	@Autowired
	TraderAddressDAO traderAddressDAO;
	@Autowired
	PlantripDAO plantripDAO;
	
	@Autowired
	RegisterFlowDAO registerFlowDAO;
	
	
	@Autowired
	CommitteeDAO committeeDAO;
	@Autowired
	BranchDAO branchDAO;
	@Autowired
	EducationDAO educationDAO;
	@Autowired
	ForeignLanguageDAO foreignLanguageDAO;
	
	@Autowired
	PrintingLicenseDAO printingLicenseDAO;
	@Autowired
	PrintingCardDAO printingCardDAO;
	
	@Autowired
	AdmUserDAO admUserDAO;
	@Autowired
	AdmUserGroupDAO admUserGroupDAO;
	
	@Autowired
	MasDocumentDAO masDocumentDAO;
	
	@Autowired
	RegDocumentDAO regDocumentDAO;
	
	@Autowired
	MasFeeDAO masFeeDAO;
	
	private final String REGISTER_USER = "ONLINE";
	private final long BUSINESS_DOC_LICENSE_PERSON = 91;
	private final long BUSINESS_DOC_LICENSE_COMPANY = 77;
	private final long GUIDE_DOC_LICENSE = 108;

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public Object applyForLicenseRenew(Object object) throws Exception {
		
		if(!(object instanceof FileUploadForm))
		{
			throw new IllegalArgumentException("ไม่สามารถ เพิ่มการทำรายการทะเบียนใบอนุญาตใหม่ได้");
		}	
		
		FileUploadForm form = (FileUploadForm)object;
		
		RegistrationDTO param = new RegistrationDTO();
		param.setTraderId(form.getTraderId());
		param.setEmail(form.getEmail());
		param.setRegistrationType(RegistrationType.TOUR_COMPANIES_RENEW_REGISTRATION.getStatus());
		RegistrationDTO dto = (RegistrationDTO)this.addNewRegistration(param);
		
		this.saveAttachedFile(form, dto);
		
		return dto;
		
	}
	
	public boolean saveAttachedFile(FileUploadForm form, RegistrationDTO newReg) throws Exception
	{
		boolean ret = true;
		long docId = 0;
		
		if(!form.getFiles().isEmpty())
		{
			Trader trader =  (Trader)this.traderDAO.findByPrimaryKey(newReg.getTraderId());
			Registration reg = (Registration) this.registrationDAO.findByPrimaryKey(newReg.getRegId());
			Person person = trader.getPerson();
			
			//TraderType
			String traderTypeName = "";
			//LicenseNo
			String licenseNoStr = trader.getLicenseNo();
			if(trader.getTraderType().equals(TraderType.TOUR_COMPANIES.getStatus()))
			{
				traderTypeName = "Business";
				licenseNoStr = licenseNoStr.replaceAll("[/]", "");
				
				if(PersonType.INDIVIDUAL.equals(person.getPersonType()))
				{
					docId = this.BUSINESS_DOC_LICENSE_PERSON;
				}
				else
				{
					docId = this.BUSINESS_DOC_LICENSE_COMPANY;
				}
				
				
				
			}
			if(trader.getTraderType().equals(TraderType.GUIDE.getStatus()))
			{
				traderTypeName = "Guide";
				licenseNoStr = licenseNoStr.replaceAll("[-]", "");
				
				docId = this.GUIDE_DOC_LICENSE;
			}
			if(trader.getTraderType().equals(TraderType.LEADER.getStatus()))
			{
				traderTypeName = "Tourleader";
				licenseNoStr = licenseNoStr.replaceAll("[.]", "");
			}
			
			//RegistrationType
			List<Registration> listReg = (List<Registration>)this.registrationDAO.findAllByTrader(trader.getTraderId());
			String registrationType = (listReg.get(0)).getRegistrationType();

			
			for(MultipartFile file: form.getFiles())
			{
				//RealFileName
				
				MasDocument document = (MasDocument)this.masDocumentDAO.findByPrimaryKey(docId);
				String realFileName = document.getMasDocId()+"";
			
				String extension = ".txt";
				String originalFilename = file.getOriginalFilename();
				int dot = originalFilename.lastIndexOf('.');
				if(dot != -1)
				{
					extension = originalFilename.substring(dot);
				}
				
				String root = ConstantUtil.DOC_ROOT_PATH; //"/Users/Apple/Desktop/fileUpload/"; 
				String path = root+"Registration"+java.io.File.separator+traderTypeName+java.io.File.separator+licenseNoStr+java.io.File.separator+registrationType;
		    	String fileName = java.io.File.separator+realFileName+extension;
		    	
		    	String fullpath = path+fileName;
		    	
		    	FileManage fm = new FileManage();
		    	fm.saveFile(file.getInputStream(), path, fileName);
		    	
		    	
				RegDocument regDocPersis = new RegDocument();
				regDocPersis.setDocPath(fullpath);
				regDocPersis.setTrader(trader);
				regDocPersis.setRegistration(reg);
				regDocPersis.setMasDocument(document);
				
				regDocPersis.setRecordStatus(RecordStatus.NORMAL.getStatus());
				regDocPersis.setCreateUser(this.REGISTER_USER);
				regDocPersis.setCreateDtm(new Date());
				this.regDocumentDAO.insert(regDocPersis);
			}
		}		
		
		return ret;
	}
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)	
	public Object addNewRegistration(Object object) throws Exception {
		
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถ เพิ่มการทำรายการทะเบียนใบอนุญาตใหม่ได้");
		}	
		RegistrationDTO params = (RegistrationDTO)object;
		if((null == params.getRegistrationType()) && (params.getRegistrationType().isEmpty()))
		{
			throw new IllegalArgumentException("ไม่มีข้อมูล  RegistrationType");
		}
		
		Trader trader =  (Trader)this.traderDAO.findByPrimaryKey(params.getTraderId());
		Person person = trader.getPerson();
		
		String registrationNo = "0";
		if(!TraderType.LEADER.equals(trader.getTraderType()))
		{
			registrationNo = masRunningNoDAO.getRegistrationNo(trader.getOrganization().getOrgId() ,trader.getTraderType(), null);

		}
		
		Organization org = new Organization();
		org.setOrgId(trader.getOrganization().getOrgId());
		
		Person newRegPerson = new Person();
		
		ObjectUtil.copy(person, newRegPerson);
		
		newRegPerson.setPersonNationality("ไทย");

		if(person.getMasPrefix() != null)
		{
			newRegPerson.setMasPrefix(person.getMasPrefix());
		}
		
		if(person.getMasPosfix() != null)
		{
			newRegPerson.setMasPosfix(person.getMasPosfix());
		}
		
		if(person.getMasAmphurByAmphurId() != null)
		{
			newRegPerson.setMasAmphurByAmphurId(person.getMasAmphurByAmphurId());
		}
		
		if(person.getMasAmphurByTaxAmphurId() != null)
		{
			newRegPerson.setMasAmphurByTaxAmphurId(person.getMasAmphurByTaxAmphurId());
		}
		
		if(person.getMasProvinceByProvinceId() != null)
		{
			newRegPerson.setMasProvinceByProvinceId(person.getMasProvinceByProvinceId());
		}	
		
		if(person.getMasProvinceByTaxProvinceId() != null)
		{
			newRegPerson.setMasProvinceByTaxProvinceId(person.getMasProvinceByTaxProvinceId());
		}
		
		newRegPerson.setCreateUser(REGISTER_USER);
		newRegPerson.setRecordStatus(RecordStatus.TEMP.getStatus());
		
		Long personId = (Long)personDAO.insert(newRegPerson);
		newRegPerson.setPersonId(personId);
		
		Trader newRegTrader = new Trader();
		
		ObjectUtil.copy(trader, newRegTrader);
		
		
		if(trader.getTraderByTraderGuideId() != null)
		{
			newRegTrader.setTraderByTraderGuideId(trader.getTraderByTraderGuideId());
		}
		if(trader.getMasProvince() != null)
		{
			newRegTrader.setMasProvince(trader.getMasProvince());
		}
		newRegTrader.setOrganization(org);
		newRegTrader.setPerson(newRegPerson);
		
		if(TraderType.TOUR_COMPANIES.getStatus().equals(newRegTrader.getTraderType()))
		{
			Calendar effectiveDate = Calendar.getInstance();
			effectiveDate.setTime(trader.getEffectiveDate());
			
			Calendar expireDate = Calendar.getInstance();
			expireDate.setTime(trader.getExpireDate());
			
			if(RegistrationType.TOUR_COMPANIES_RENEW_REGISTRATION.getStatus().equals(params.getRegistrationType()))
			{
				
				effectiveDate.set(Calendar.YEAR, expireDate.get(Calendar.YEAR));
				expireDate.add(Calendar.YEAR, 2);
				
//				expireDate.add(Calendar.DATE, 1);
//				
//				Date newEffectiveDate = expireDate.getTime();
//				expireDate.add(Calendar.YEAR, 2);
//				Date newExpireDate = expireDate.getTime();
				
				newRegTrader.setEffectiveDate(effectiveDate.getTime());
				newRegTrader.setExpireDate(expireDate.getTime());
			}
			
			if(RegistrationType.TOUR_COMPANIES_TEMPORARY_LICENSE.getStatus().equals(params.getRegistrationType()))
			{
				newRegTrader.setEffectiveDate(new Date());
				newRegTrader.setExpireDate(trader.getExpireDate());
			}
		}
		
		if(TraderType.GUIDE.getStatus().equals(newRegTrader.getTraderType()))
		{
			Calendar effectiveDate = Calendar.getInstance();
			effectiveDate.setTime(trader.getEffectiveDate());
			
			Calendar expireDate = Calendar.getInstance();
			expireDate.setTime(trader.getExpireDate());
			
			if(RegistrationType.GUIDE_RENEW_REGISTRATION.getStatus().equals(params.getRegistrationType()))
			{
				Date day = new Date();
				effectiveDate.setTime(day);
				expireDate.setTime(day);
				expireDate.add(Calendar.YEAR, 5);
				expireDate.add(Calendar.DATE, -1);
				
//				effectiveDate.set(Calendar.YEAR, expireDate.get(Calendar.YEAR));
//				expireDate.add(Calendar.YEAR, 5);
				
//				expireDate.add(Calendar.DATE, 1);
//				
//				Date newEffectiveDate = expireDate.getTime();
//				expireDate.add(Calendar.YEAR, 5);
//				Date newExpireDate = expireDate.getTime();
				
				newRegTrader.setEffectiveDate(effectiveDate.getTime());
				newRegTrader.setExpireDate(expireDate.getTime());
			}
			
			if(RegistrationType.GUIDE_TEMPORARY_LICENSE.getStatus().equals(params.getRegistrationType()))
			{
				newRegTrader.setEffectiveDate(new Date());
				newRegTrader.setExpireDate(trader.getExpireDate());
			}
		}
		
		newRegTrader.setCreateUser(REGISTER_USER);
		newRegTrader.setRecordStatus(RecordStatus.TEMP.getStatus());
		trader.setLicenseStatus(RecordStatus.TEMP.getStatus());
		
		Long traderId =  (Long)traderDAO.insert(newRegTrader);
		newRegTrader.setTraderId(traderId);
		
		Registration reg = new Registration();
		reg.setTrader(newRegTrader);
		reg.setOrganization(org);
		reg.setRegistrationType(params.getRegistrationType());
		reg.setRegistrationNo(registrationNo==null?"":registrationNo);
		reg.setRegistrationDate(Calendar.getInstance().getTime());
		reg.setRegistrationProgress(RoleStatus.DIRECTOR.getStatus());
		reg.setRecordStatus(RecordStatus.TEMP.getStatus());
		reg.setCreateDtm(new Date());
		reg.setCreateUser(this.REGISTER_USER);
		
		Long regId =  (Long)this.registrationDAO.insert(reg);
		reg.setRegId(regId);
		
		RegisterProgress progress =  new RegisterProgress();
		progress.setRegistration(reg);
		
		List<AdmUser>  listAdmUser = (List<AdmUser>)this.admUserDAO.findTempUserByOrg(trader.getOrganization().getOrgId());		
		if(!listAdmUser.isEmpty())
		{
			AdmUser admUser = listAdmUser.get(0);
			progress.setAdmUser(admUser);
		}
		
		progress.setProgress(RoleStatus.DIRECTOR.getStatus());
		progress.setProgressDate(Calendar.getInstance().getTime());
//		String positionName = user.getUserData().getPositionName()==null?"":user.getUserData().getPositionName();
		progress.setProgressDesc("ยื่นขอจดทะเบียน Online");
		progress.setProgressStatus(ProgressStatus.WAITING.getStatus());
		registerProgressDAO.insert(progress);
		
		// trader addres
		List<TraderAddress> listAddress = (List<TraderAddress>) this.traderAddressDAO.findAllByTrader(trader.getTraderId(), null, RecordStatus.NORMAL.getStatus());
		if(!listAddress.isEmpty())
		{
			for(TraderAddress address: listAddress)
			{
				TraderAddress newRegTraderAddress = new TraderAddress();
				
				ObjectUtil.copy(address ,newRegTraderAddress);
				
				newRegTraderAddress.setMasAmphur(address.getMasAmphur());
				newRegTraderAddress.setMasProvince(address.getMasProvince());
				newRegTraderAddress.setTrader(newRegTrader);
				newRegTraderAddress.setMasTambol(address.getMasTambol());
				
				newRegTraderAddress.setRecordStatus(RecordStatus.TEMP.getStatus());
				newRegTraderAddress.setCreateUser(this.REGISTER_USER);
				newRegTraderAddress.setCreateDtm(new Date());
				
				if((null != params.getEmail()) && (!params.getEmail().equals("")))
				{
					newRegTraderAddress.setEmail(params.getEmail());
				}
				
				this.traderAddressDAO.insert(newRegTraderAddress);
			}
		}
		//Branch
		List<Trader> listBranch = (List<Trader>) this.branchDAO.findAllByBranchParent(trader.getTraderId(), RecordStatus.NORMAL.getStatus());
		if(!listBranch.isEmpty())
		{
			for(Trader branch: listBranch)
			{
				Trader newBranch = new Trader();
				
				ObjectUtil.copy(branch, newBranch);
				
				newBranch.setPerson(newRegPerson);
				newBranch.setTraderByBranchParentId(newRegTrader);
				newBranch.setRecordStatus(RecordStatus.TEMP.getStatus());
				newBranch.setCreateUser(this.REGISTER_USER);
				newBranch.setCreateDtm(new Date());
				
				this.branchDAO.insert(newBranch);
				
				List<TraderAddress> listBranchAddress = (List<TraderAddress>) this.traderAddressDAO.findAllByTrader(branch.getTraderId(), null, RecordStatus.NORMAL.getStatus());
				if(!listBranchAddress.isEmpty())
				{
					for(TraderAddress branchAddress: listBranchAddress)
					{
						TraderAddress newBranchAddress = new TraderAddress();
						
						ObjectUtil.copy(branchAddress, newBranchAddress);
						
						if(branchAddress.getMasAmphur() != null)
						{
							newBranchAddress.setMasAmphur(branchAddress.getMasAmphur());
						}
						
						if(branchAddress.getMasProvince() != null)
						{
							newBranchAddress.setMasProvince(branchAddress.getMasProvince());
						}
						newBranchAddress.setTrader(newBranch);
						
						newBranchAddress.setRecordStatus(RecordStatus.TEMP.getStatus());
						newBranchAddress.setCreateUser(this.REGISTER_USER);
						newBranchAddress.setCreateDtm(new Date());
						this.traderAddressDAO.insert(newBranchAddress);
					}
				}
			}
		}	
		// plantrip
		List<Plantrip> listPlanTrip = (List<Plantrip>) this.plantripDAO.findPlanTripbyTrader(trader.getTraderId(), RecordStatus.NORMAL.getStatus());
		if(!listPlanTrip.isEmpty())
		{
			for(Plantrip plantrip: listPlanTrip)
			{
				Plantrip newRegPlantrip = new Plantrip();
				ObjectUtil.copy(plantrip, newRegPlantrip);
				
				newRegPlantrip.setCountry(plantrip.getCountry());
				newRegPlantrip.setTrader(newRegTrader);
				newRegPlantrip.setRecordStatus(RecordStatus.TEMP.getStatus());
				newRegPlantrip.setCreateUser(this.REGISTER_USER);
				newRegPlantrip.setCreateDtm(new Date());
				this.plantripDAO.insert(newRegPlantrip);
			}
		}
		//committee
		List<Committee> listCommittee =  (List<Committee>) this.committeeDAO.findAllByPerson(person.getPersonId(), null, RecordStatus.NORMAL.getStatus());
		if(!listCommittee.isEmpty())
		{
			for(Committee comm: listCommittee)
			{
				Committee newCommittee = new Committee();
				
				ObjectUtil.copy(comm, newCommittee);
				newCommittee.setPerson(newRegPerson);
				
				newCommittee.setMasPrefix(comm.getMasPrefix());
				newCommittee.setMasAmphurByAmphurId(comm.getMasAmphurByAmphurId());
				newCommittee.setMasAmphurByTaxAmphurId(comm.getMasAmphurByTaxAmphurId());
				newCommittee.setMasProvinceByProvinceId(comm.getMasProvinceByProvinceId());
				newCommittee.setMasProvinceByTaxProvinceId(comm.getMasProvinceByTaxProvinceId());
				
				newCommittee.setMasPosfix(comm.getMasPosfix());
				
				newCommittee.setRecordStatus(RecordStatus.TEMP.getStatus());
				newCommittee.setCreateUser(this.REGISTER_USER);
				newCommittee.setCreateDtm(new Date());
				this.committeeDAO.insert(newCommittee);
			}
		}
		//education and traning
		List<Education> listEducation = (List<Education>) this.educationDAO.findEducationByPerson(person.getPersonId(), RecordStatus.NORMAL.getStatus(), null);
		if(!listEducation.isEmpty())
		{
			for(Education edu: listEducation)
			{
				Education newRegEdu = new Education();
				
				ObjectUtil.copy(edu, newRegEdu);
				
				newRegEdu.setMasUniversity(edu.getMasUniversity());
				newRegEdu.setMasEducationLevel(edu.getMasEducationLevel());
				newRegEdu.setPerson(newRegPerson);
				
				newRegEdu.setRecordStatus(RecordStatus.TEMP.getStatus());
				newRegEdu.setCreateUser(this.REGISTER_USER);
				newRegEdu.setCreateDtm(new Date());
				this.committeeDAO.insert(newRegEdu);
			}
		}
		//ภาษา
		List<ForeignLanguage> listLang = (List<ForeignLanguage>) this.foreignLanguageDAO.findForeignLanguageByPerson(person.getPersonId(), RecordStatus.NORMAL.getStatus());
		if(!listLang.isEmpty())
		{
			for(ForeignLanguage lang: listLang)
			{
				ForeignLanguage newRegLang = new ForeignLanguage();
				
				ObjectUtil.copy(lang, newRegLang);
				newRegLang.setCountry(lang.getCountry());
				newRegLang.setPerson(newRegPerson);
				newRegLang.setRecordStatus(RecordStatus.TEMP.getStatus());
				newRegLang.setCreateUser(this.REGISTER_USER);
				newRegLang.setCreateDtm(new Date());
				this.foreignLanguageDAO.insert(newRegLang);
			}
		}
		
		RegistrationDTO dto = new RegistrationDTO();
		ObjectUtil.copy(reg, dto);
		dto.setTraderId(newRegTrader.getTraderId());
		

		
		return dto;
		
	}

	public Map getPayments(Object object) throws Exception {
		
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถ เพิ่มการทำรายการทะเบียนใบอนุญาตใหม่ได้");
		}
		Map map = new HashMap();
		DateFormat df = DateUtils.getProcessDateFormatEnShortYear();
		
		DateFormat dfThai = DateUtils.getProcessDateFormatThai();
		
		RegistrationDTO dto = (RegistrationDTO)object;
		
		Registration reg = (Registration)this.registrationDAO.findByPrimaryKey(dto.getRegId());
		Trader trader = reg.getTrader();
		
		
		String licenseNo = trader.getLicenseNo();
		String[] lic = licenseNo.split("/");
		
		if(trader.getTraderType().equals(TraderType.TOUR_COMPANIES.getStatus()))
		{
			lic = licenseNo.split("/");
		}
		if(trader.getTraderType().equals(TraderType.GUIDE.getStatus()))
		{
			lic = licenseNo.split("-");
		}
		
		String[] zo = {"0","00"};
		
		
		
		String ls = zo[(9 - lic[1].length())-3];
		
		StringBuffer sbuf = new StringBuffer();
		sbuf.append(lic[0]);
		sbuf.append(ls);
		sbuf.append(lic[1]);
		sbuf.append(TraderType.getCode(trader.getTraderType()));
		sbuf.append(RegistrationType.getRegCode(trader.getTraderType(), reg.getRegistrationType()));
		
//		String ref1 = lic[0] + ls+lic[1] + trader.getTraderCategory() + reg.getRegistrationType();
		
		map.put("ref1", sbuf.toString());
		map.put("traderType", trader.getTraderType());

		
		List<MasFee> listAll = (List<MasFee>) this.masFeeDAO.findFee(reg.getRegistrationType(), trader.getTraderType());
		
		BigDecimal amount = new BigDecimal(0);
		if(!listAll.isEmpty())
		{
			for(MasFee fee: listAll)
			{
				amount = amount.add(fee.getFeeMny());
			}
		}
		
		List<Trader> listCurr = this.traderDAO.findLicense(null, trader.getLicenseNo(), null, null, RecordStatus.NORMAL.getStatus(), RecordStatus.NORMAL.getStatus(), trader.getTraderType());

		Trader currTrader = null;
		if(!listCurr.isEmpty())
		{
			currTrader = listCurr.get(0);
		}
		
		BigDecimal totalfee = new BigDecimal(0);
		if((null != currTrader) && (currTrader.getExpireDate().before(new Date())))
		{
			Double fee = new Double(1000);
			fee = fee * 0.02;
			
			
			Date today = new Date();
			long diff =  today.getTime() - currTrader.getExpireDate().getTime();
			diff = Math.abs(diff);
			long diffDays = diff / (24 * 60 * 60 * 1000);
		
			BigDecimal diffD = new BigDecimal(diffDays);
			BigDecimal diffMonth = diffD.divide(new BigDecimal(30), BigDecimal.ROUND_UP);
			diffMonth.setScale(0, BigDecimal.ROUND_UP);
			
			
//			int diffMonth = today.getMonth() - currTrader.getExpireDate().getMonth();
//			diffMonth = Math.abs(diffMonth);
			
	

			
			BigDecimal  newfee = diffMonth.multiply(new BigDecimal(fee) );
			
			totalfee = totalfee.add(newfee);
			
//			Calendar cal = Calendar.getInstance();
//			
//			cal.add(Calendar.DATE, 3);
//			
//			map.put("ref2", df.format(cal.getTime()));
//			
//			map.put("dueDate", dfThai.format(cal.getTime()));
			
			
		}
//		else
//		{
//			map.put("ref2", df.format(currTrader.getExpireDate()));
//			
//			map.put("dueDate", dfThai.format(currTrader.getExpireDate()));
//		}
		
		Calendar cal = Calendar.getInstance();
		
		cal.add(Calendar.DATE, 3);
		
		map.put("ref2", df.format(cal.getTime()));
		
		map.put("dueDate", dfThai.format(cal.getTime()));
		
		totalfee = totalfee.setScale(2, BigDecimal.ROUND_UP);
		
		NumberFormat nf = NumberFormatFactory.getCurrencyFormat();
		
		totalfee = totalfee.add(amount);
		
		map.put("amount", nf.format(totalfee.doubleValue()));
		map.put("amountTxt", ThaiBahtUtil.getBahtText(totalfee.toString()));

		
		
		return map;
	}

	
	public static void main(String arg[])
	{
		String[] zo = {"0","00"};
		
		String x = "11-45678";
		
		String[] x1 = x.split("-");
		System.out.println(x1[1]);
		
		String ls = zo[(9 - x1[1].length())-3];
		
		System.out.println(x1[0] + ls+x1[1]);
	}
	
	
}
