package sss.dot.tourism.service.trader;

import java.util.List;

public interface IMemberLicenseService {
	public void doLogin(String userName, String password, Object obj) throws Exception;
	
	public List getMemberNews(String token) throws Exception;
	
	public Object getMemberNewsById(long newId) throws Exception;
	
	public void changePassword(String token, String password) throws Exception;
	
	public void resetPass(String userName, String email) throws Exception;
	
	public void addMember() throws Exception;
	
}
