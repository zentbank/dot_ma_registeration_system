package sss.dot.tourism.service.trader;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.info.TraderDAO;
import sss.dot.tourism.dao.registration.TraderAddressDAO;
import sss.dot.tourism.dao.trader.MemberLicenseDAO;
import sss.dot.tourism.dao.trader.MemberNewsDAO;
import sss.dot.tourism.domain.MemberLicense;
import sss.dot.tourism.domain.MemberNews;
import sss.dot.tourism.domain.Person;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.domain.TraderAddress;
import sss.dot.tourism.dto.info.MemberDTO;
import sss.dot.tourism.dto.info.NewsDTO;
import sss.dot.tourism.util.ConstantUtil;
import sss.dot.tourism.util.RecordStatus;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.imp.PasswordEncoding;

@Repository("memberLicenseService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class MemberLicenseService implements IMemberLicenseService {
	@Autowired
	private MemberLicenseDAO memberLicenseDAO;

	@Autowired
	private MemberNewsDAO memberNewsDAO;
	@Autowired
	private TraderDAO traderDAO;

	@Autowired
	private TraderAddressDAO traderAddressDAO;

	public void doLogin(String userName, String password, Object obj)
			throws Exception {

		if (!(obj instanceof MemberDTO)) {
			throw new IllegalArgumentException();
		}

		MemberDTO dto = (MemberDTO) obj;
		
		System.out.println("password " + password);

		String passwordFormParam = PasswordEncoding.toEncodeSHA256(userName,
				password);
		
		System.out.println("passwordFormParam " + passwordFormParam);

		List<MemberLicense> listAll = (List<MemberLicense>) this.memberLicenseDAO
				.authenUser(userName, passwordFormParam);

		String identityNo = "false";
		if (!listAll.isEmpty()) {
			MemberLicense license = listAll.get(0);
			dto.setToken(license.getIndentityNo());
			dto.setIndentityNo(license.getIndentityNo());
			dto.setValid(true);
			dto.setLicenseNo(license.getLicenseNo());
		} else {
			dto.setValid(false);
		}

	}

	public List getMemberNews(String token) throws Exception {

		List<NewsDTO> list = new ArrayList<NewsDTO>();

		List<MemberNews> listAll = (List<MemberNews>) this.memberNewsDAO
				.getMemberNews(token);

		if (!listAll.isEmpty()) {
			MemberNews memNews = listAll.get(0);

			MemberLicense mLic = memNews.getMemberLicense();

			List<MemberNews> listPublic = this.memberNewsDAO
					.getMemberPublicNews(mLic.getTraderType());

			if (!listPublic.isEmpty()) {
				for (MemberNews publicNews : listPublic) {
					NewsDTO newsDTO = new NewsDTO();

					newsDTO.setHeader(publicNews.getMemberNewsHeader());
					newsDTO.setDetails(publicNews.getMemberNewsDesc());
					newsDTO.setId(publicNews.getMemberNewsId());

					list.add(newsDTO);
				}
			}

			for (MemberNews news : listAll) {
				NewsDTO newsDTO = new NewsDTO();

				newsDTO.setHeader(news.getMemberNewsHeader());
				newsDTO.setDetails(news.getMemberNewsDesc());
				newsDTO.setId(news.getMemberNewsId());

				list.add(newsDTO);
			}
		}

		return list;
	}

	public Object getMemberNewsById(long newId) throws Exception {
		MemberNews news = (MemberNews) this.memberNewsDAO
				.findByPrimaryKey(newId);

		NewsDTO newsDTO = new NewsDTO();
		newsDTO.setHeader(news.getMemberNewsHeader());
		newsDTO.setDetails(news.getMemberNewsDesc());
		newsDTO.setId(news.getMemberNewsId());

		if (news.getMewsImgPath() != null) {
			newsDTO.setNews_img(ConstantUtil.ROOT_PATH
					+ "/journal/membernews/viewimg/" + news.getMemberNewsId());
		} else {
			newsDTO.setNews_img("");
		}

		return newsDTO;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void changePassword(String token, String password) throws Exception {

		List<MemberLicense> listMem = (List<MemberLicense>) this.memberLicenseDAO
				.findUserByIdentityNo(token);

		if (!listMem.isEmpty()) {
			MemberLicense mem = listMem.get(0);

			String passwordFormParam = PasswordEncoding.toEncodeSHA256(
					mem.getMemberLoginName(), password);

			mem.setMemberLoginPassword(passwordFormParam);

			this.memberLicenseDAO.update(mem);
		} else {
			throw new IllegalAccessError("ไม่มี token นี้");
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void resetPass(String userName, String email) throws Exception {

		List<MemberLicense> listMem = (List<MemberLicense>) this.memberLicenseDAO
				.findUserByLoginName(userName);

		if (!listMem.isEmpty()) {
			MemberLicense mem = listMem.get(0);

			List<Trader> listTrader = (List<Trader>) this.traderDAO
					.findTraderByLicenseNo(mem.getLicenseNo(),
							mem.getTraderType(),
							RecordStatus.NORMAL.getStatus(), 0);

			if (!listTrader.isEmpty()) {
				Trader trader = listTrader.get(0);
				List<TraderAddress> listAddr = (List<TraderAddress>) traderAddressDAO
						.findAllByTraderAndEmail(trader.getTraderId(), email,
								RecordStatus.NORMAL.getStatus());

				if (!listAddr.isEmpty()) {
					String p = mem.getMemberLoginName();
					String pass = PasswordEncoding.encodeSHA256(p.substring(
							p.length() - 4, p.length()));
					String passwordFormParam = PasswordEncoding.toEncodeSHA256(
							mem.getMemberLoginName(), pass);

					mem.setMemberLoginPassword(passwordFormParam);

					this.memberLicenseDAO.update(mem);
				} else {
					throw new IllegalAccessError("ไม่มี email นี้");
				}
			} else {
				throw new IllegalAccessError("ไม่มี ชื่อผู้ใช้นี้");
			}
		} else {
			throw new IllegalAccessError("ไม่มี ชื่อผู้ใช้นี้");
		}

	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addMember() throws Exception {
		List<Trader> listAll = (List<Trader>) this.traderDAO.findLicense(null,
				null, null, null, RecordStatus.NORMAL.getStatus(),
				RecordStatus.NORMAL.getStatus(),
				null);
		
		if(!listAll.isEmpty()){
			for(Trader trader: listAll){
				Person person = trader.getPerson();		
				if(StringUtils.isNotEmpty(person.getIdentityNo())){
					
					try{
						MemberLicense mem = new MemberLicense();
						
						mem.setLicenseNo(trader.getLicenseNo());
						mem.setIndentityNo(person.getIdentityNo());
						mem.setTraderType(trader.getTraderType());
						mem.setMemberLoginName(person.getIdentityNo());
						
						String p = mem.getMemberLoginName();
						String pass = PasswordEncoding.encodeSHA256(p.substring(
								p.length() - 4, p.length()));
						String passwordFormParam = PasswordEncoding.toEncodeSHA256(
								mem.getMemberLoginName(), pass);

						mem.setMemberLoginPassword(passwordFormParam);

						this.memberLicenseDAO.insert(mem);
					}catch(Exception e){
						e.printStackTrace();
					}
					
				}
				
			}
		}
	}

	public static void main(String arg[]) {
		String p = "0105549045451";
		
		
		String pass = PasswordEncoding.encodeSHA256(p.substring(
				p.length() - 4, p.length()));
		System.out.println(p.substring(
				p.length() - 4, p.length()));
		System.out.println(pass);
		
		String passwordFormParam = PasswordEncoding.toEncodeSHA256(
				p, pass);

		System.out.println(passwordFormParam);
		
	}
}
