package sss.dot.tourism.service.trader;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sss.dot.tourism.dao.registration.TraderDAO;
import sss.dot.tourism.domain.Person;
import sss.dot.tourism.domain.Trader;
import sss.dot.tourism.dto.registration.RegistrationDTO;
import sss.dot.tourism.util.DateUtils;
import sss.dot.tourism.util.LicenseStatus;
import sss.dot.tourism.util.ObjectUtil;
import sss.dot.tourism.util.PersonType;
import sss.dot.tourism.util.RecordStatus;
import sss.dot.tourism.util.TraderCategory;
import sss.dot.tourism.util.TraderType;

import com.sss.aut.service.User;

@Repository("traderService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class TraderService implements ITraderService {

	@Autowired
	TraderDAO traderlicenseDAO;

	
	public List getTraderAll(Object object, User user) throws Exception {

		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		RegistrationDTO params = (RegistrationDTO)object;
		
		params.setRecordStatus(RecordStatus.NORMAL.getStatus());
		
		List<Trader>  listTrader = (List<Trader>)this.traderlicenseDAO.findAllTrader(params);
		if(!listTrader.isEmpty())
		{
			for(Trader trader: listTrader)
			{
				RegistrationDTO dto = new RegistrationDTO();
				ObjectUtil.copy(trader, dto);
				
				dto.setLicenseStatusName(LicenseStatus.getMeaning(dto.getLicenseStatus()));

				Person person = trader.getPerson();
				
				ObjectUtil.copy(person,dto);
				

				
				if(PersonType.CORPORATE.getStatus().equals(person.getPersonType()))
				{
//					System.out.println(person.getMasPrefix()==null?"a":person.getMasPrefix().getPrefixName());
//					System.out.println(person.getMasPosfix()==null?"a":person.getMasPosfix().getPostfixName());
//					System.out.println(person.getFirstName());
					
					
					String traderOwnerName = person.getMasPrefix()==null?"":person.getMasPrefix().getPrefixName() + person.getFirstName() + (person.getMasPosfix()==null?"":person.getMasPosfix().getPostfixName());
					dto.setTraderOwnerName(traderOwnerName);
				}
				else
				{
					String traderOwnerName =  person.getMasPrefix()==null?"":person.getMasPrefix().getPrefixName() + person.getFirstName() + " " + person.getLastName();
					dto.setTraderOwnerName(traderOwnerName);
					
				}	
				
				//Oat Add
				if(dto.getTraderType().equals(TraderType.TOUR_COMPANIES.getStatus()))
				{
					dto.setTraderTypeName(TraderType.TOUR_COMPANIES.getMeaning());
				}
					
				if(dto.getTraderType().equals(TraderType.GUIDE.getStatus()))
				{
					dto.setTraderTypeName(TraderType.GUIDE.getMeaning());
					dto.setTraderName(dto.getTraderOwnerName());
				}
					
				if(dto.getTraderType().equals(TraderType.LEADER.getStatus()))
				{
					dto.setTraderTypeName(TraderType.LEADER.getMeaning());
					dto.setTraderName(dto.getTraderOwnerName());
				}
					
				//End Oat Add
				
				dto.setTraderCategoryName(
						TraderCategory.getTraderCategory(
							dto.getTraderType()
							, dto.getTraderCategory()
						).getMeaning()
				);
				
				
				list.add(dto);
			}
			
		}
		
		return list;
	}
	
	public List getTraderByLicenseNo(Object object, User user) throws Exception {

		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		
		RegistrationDTO params = (RegistrationDTO)object;
		
		List<Trader>  listTrader = (List<Trader>)this.traderlicenseDAO.findTraderByLicenseNo(params.getLicenseNo(), params.getTraderType(), RecordStatus.NORMAL.getStatus(), user.getUserData().getOrgId());
		if(!listTrader.isEmpty())
		{
			for(Trader trader: listTrader)
			{
				RegistrationDTO dto = new RegistrationDTO();
				ObjectUtil.copy(trader, dto);

				Person person = trader.getPerson();
				
				ObjectUtil.copy(person,dto);
				
				if(PersonType.CORPORATE.getStatus().equals(person.getPersonType()))
				{
//					System.out.println(person.getMasPrefix()==null?"a":person.getMasPrefix().getPrefixName());
//					System.out.println(person.getMasPosfix()==null?"a":person.getMasPosfix().getPostfixName());
//					System.out.println(person.getFirstName());
					
					
					String traderOwnerName = person.getMasPrefix()==null?"":person.getMasPrefix().getPrefixName() + person.getFirstName() + (person.getMasPosfix()==null?"":person.getMasPosfix().getPostfixName());
					dto.setTraderOwnerName(traderOwnerName);
				}
				else
				{
					String traderOwnerName =  person.getMasPrefix()==null?"":person.getMasPrefix().getPrefixName() + person.getFirstName() + " " + person.getLastName();
					dto.setTraderOwnerName(traderOwnerName);
				}	
				
				dto.setTraderCategoryName(
						TraderCategory.getTraderCategory(
							dto.getTraderType()
							, dto.getTraderCategory()
						).getMeaning()
				);
				
				list.add(dto);
			}
			
		}
		
		return list;
	}
	
	public void updateLicense (Object object, User user) throws Exception
	{
			
	}

	public void suspend(Object object, User user) throws Exception {
		
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		RegistrationDTO params = (RegistrationDTO)object;
		params.setLicenseStatus(LicenseStatus.SUSPENSION.getStatus());
		
		this.updateLicense(params, user);
		
	}

	public void unSuspend(Object object, User user) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		RegistrationDTO params = (RegistrationDTO)object;
		params.setLicenseStatus(LicenseStatus.NORMAL.getStatus());
		this.updateLicense(params, user);
		
	}

	public void revoke(Object object, User user) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		RegistrationDTO params = (RegistrationDTO)object;
		params.setLicenseStatus(LicenseStatus.REVOKE.getStatus());
		
		this.updateLicense(params, user);
		
	}

	public void unRevoke(Object object, User user) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		RegistrationDTO params = (RegistrationDTO)object;
		params.setLicenseStatus(LicenseStatus.NORMAL.getStatus());
		this.updateLicense(params, user);
		
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
	public void deActivate(Object object, User user) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		RegistrationDTO params = (RegistrationDTO)object;
		params.setLicenseStatus(LicenseStatus.CANCEL.getStatus());
		
		this.updateLicense(params, user);
		
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
	public void activate(Object object, User user) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		RegistrationDTO params = (RegistrationDTO)object;
		params.setLicenseStatus(LicenseStatus.NORMAL.getStatus());
		this.updateLicense(params, user);
		
	}

	public List getTraderAllName(Object object, User user) throws Exception {
		if(!(object instanceof RegistrationDTO))
		{
			throw new IllegalArgumentException("ไม่สามารถอ่านข้อมูลได้");
		}
		
		List<RegistrationDTO> list = new ArrayList<RegistrationDTO>();
		DateFormat ft = DateUtils.getProcessDateFormatThai();
		RegistrationDTO params = (RegistrationDTO)object;
		
		//params.setRecordStatus(RecordStatus.NORMAL.getStatus());
		
		List<Trader>  listTrader = (List<Trader>)this.traderlicenseDAO.findAllTraderName(params);
		if(!listTrader.isEmpty())
		{
			for(Trader trader: listTrader)
			{
				RegistrationDTO dto = new RegistrationDTO();
				ObjectUtil.copy(trader, dto);
				System.out.println(dto.getTraderId());
				//dto.setLicenseStatusName(LicenseStatus.getMeaning(dto.getLicenseStatus()));

				Person person = trader.getPerson();
				
				ObjectUtil.copy(person,dto);
				
				if(PersonType.CORPORATE.getStatus().equals(person.getPersonType()))
				{					
					String traderOwnerName = person.getMasPrefix()==null?"":person.getMasPrefix().getPrefixName() + person.getFirstName() + (person.getMasPosfix()==null?"":person.getMasPosfix().getPostfixName());
					dto.setTraderOwnerName(traderOwnerName);
				}
				else
				{
					String traderOwnerName =  person.getMasPrefix()==null?"":person.getMasPrefix().getPrefixName() + person.getFirstName() + " " + person.getLastName();
					dto.setTraderOwnerName(traderOwnerName);
				}	
				
				if(dto.getTraderCategory() !=null && !dto.getTraderCategory().equals(""))
				{
				dto.setTraderCategoryName(
						TraderCategory.getTraderCategory(
							dto.getTraderType()
							, dto.getTraderCategory()
						).getMeaning()
				);
				}
				/*List<TraderAddress> listTraderAddress =  (List<TraderAddress>)this.traderAddressDAO.findAllByTraderName(dto.getTraderId(),TraderAddressType.SHOP_ADDRESS.getStatus(), RecordStatus.TEMP.getStatus());

				if(!listTraderAddress.isEmpty())
				{
					TraderAddress add = listTraderAddress.get(0);
				StringBuilder traderAddress = new StringBuilder();
			
				traderAddress.append("ตั้งอยู่เลขที่ ");
				traderAddress.append(add.getAddressNo());
				traderAddress.append(" ถนน ");
				traderAddress.append(add.getRoadName());
				traderAddress.append(" ตรอก/ซอย ");
				traderAddress.append(add.getSoi());
					
					if(add.getMasTambol() != null)
					{
						traderAddress.append(" ตำบล/แขวง ");
						traderAddress.append(add.getMasTambol().getTambolName());
					}
					if(add.getMasAmphur()!= null)
					{
						traderAddress.append(" อำเภอ/เขต ");
						traderAddress.append(add.getMasAmphur().getAmphurName());
					}
					if(add.getMasProvince() != null)
					{
						traderAddress.append(" จังหวัด ");
						traderAddress.append(add.getMasProvince().getProvinceName());
					}
					
					
					traderAddress.append(" รหัสไปรษณีย์ ");
					traderAddress.append(add.getPostCode()==null?"":add.getPostCode());
					
					dto.setTraderAddress(traderAddress.toString());
				}*/
				if(trader.getEffectiveDate()!=null && !trader.getEffectiveDate().equals(""))
				{
					dto.setEffectiveDate(ft.format(trader.getEffectiveDate()));
				}
				
				if(trader.getExpireDate()!=null && !trader.getExpireDate().equals(""))
				{
					dto.setExpireDate(ft.format(trader.getExpireDate()));
				}
				
				list.add(dto);
			}
			
		}
		
		return list;
	
	}
}
