package sss.dot.tourism.util;

public enum AlertStatus {
	WAIT("W" , "รอการตอบรับ"),
	ACCEPT("A" , "ตอบรับแล้ว");
	
	private final String status;
	private final String meaning;
	
	AlertStatus(String status, String meaning){
		this.status = status;
		this.meaning = meaning;
	}
	
	/**
	 * @return the category
	 */
	public String getStatusString() {
		return status.toString();
	}
	public String getStatus() {
		return status;
	}
	/**
	 * @return the meaning
	 */
	public String getMeaning() {
		return meaning;
	}
	/**
	 * overloading
	 * @param flag
	 * @return
	 */
	public boolean equals(String status){
		AlertStatus eStatus =  getAlertStatus(status);
		return super.equals(eStatus);
	}
	public static AlertStatus getAlertStatus(String status) {
		AlertStatus record = null;
	
		if(WAIT.getStatus().equals(status)){
			record = WAIT;
		}
		else if(ACCEPT.getStatus().equals(status)){
			record = ACCEPT;
		}
		
		
		return record;
	}
	public static String getMeaning(String status) {
		return getAlertStatus(status ).getMeaning();
	}
}
