package sss.dot.tourism.util;



import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Authenticator
{
  public Authenticator()
  {

  }
  
  public static boolean isAuthorizable(String user, String password, String dbPassword)
  {
	  password = Authenticator.getPasswordEncoding(user, password);
	  
	  return password.equalsIgnoreCase(dbPassword);
  }

  public static String getPasswordEncoding(String user, String password)
  {

    String fromParam = "";
    try
    {
      MessageDigest digest = MessageDigest.getInstance("SHA-1");
      digest.update(password.getBytes("UTF-8"));
      digest.update(user.getBytes("UTF-8"));
      byte[] barray = digest.digest();
      digest.update(password.getBytes("UTF-8"));
      digest.update(barray);
      barray = digest.digest();
      StringBuffer sbuf = new StringBuffer(40);
      for (int i = 0; i < barray.length; ++i)
      {
        String hex = Integer.toHexString((barray[i] + 256) & 0xFF).toUpperCase();
        sbuf.append(hex.length() == 1 ? "0" + hex : hex);
      }
      fromParam = sbuf.toString();
    }
    catch (UnsupportedEncodingException e)
    {
      throw new RuntimeException(e);
    }
    catch (NoSuchAlgorithmException e1)
    {
      throw new RuntimeException(e1);
    }
    
    return fromParam;
  }
  
  public static void main(String arg[])
  {
//    Authenticator suth = new Authenticator();
    
    String passe = Authenticator.getPasswordEncoding("admin", "123");

    System.out.println(Authenticator.isAuthorizable("admin", "123", "8754D4E17FD20F53CE99F519EEA242544B1D7FB8"));
    
    System.out.println("en = " + passe);
  }

}
