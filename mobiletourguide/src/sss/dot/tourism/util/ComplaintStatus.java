package sss.dot.tourism.util;

public enum ComplaintStatus {
	WAIT("W" , "กำลังดำเนินการ"),
	SUCCESS("S" , "ตอบข้อซักถามหรือยุติเรื่อง"),
	REJECT("R" , "ไม่รับเรื่องร้อง"),
	GETIT("G" , "รับเรื่องร้องเรียน"),
	ADD("A","เรื่องร้องเรียนใหม่");
	
	private final String status;
	private final String meaning;
	
	ComplaintStatus(String status, String meaning){
		this.status = status;
		this.meaning = meaning;
	}
	
	public String getStatusString() {
		return status.toString();
	}
	public String getStatus() {
		return status;
	}
	public String getMeaning() {
		return meaning;
	}
	
	public boolean equals(String status){
		ComplaintStatus cStatus =  getComplaintStatus(status);
		return super.equals(cStatus);
	}
	public static ComplaintStatus getComplaintStatus(String status) {
		ComplaintStatus record = null;

		if(WAIT.getStatus().equals(status)){
			record = WAIT;
		}
		else if(SUCCESS.getStatus().equals(status)){
			record = SUCCESS;
		}
		else if(REJECT.getStatus().equals(status)){
			record = REJECT;
		}
		else if(GETIT.getStatus().equals(status)){
			record = GETIT;
		}
		else if(ADD.getStatus().equals(status)){
			record = ADD;
		}
		return record;
	}
	public static String getMeaning(String status) {
		return getComplaintStatus(status).getMeaning();
	}
	
	
}
