package sss.dot.tourism.util;



public class Digit2Text
{
  public static String getBahtText(String amount)
  {
    try
    {
      StringBuffer result = new StringBuffer();
      String satang, baht;

      if (amount.startsWith("-"))
      {
        amount = amount.substring(1);
        result.append("ลบ");
      }
      if (amount.endsWith("."))
      {
        amount = amount.substring(0, amount.length() - 1);
      }

      if (amount.indexOf('.') > -1)
      {
        String money[] = amount.split("\\.");
        baht = money[0];
        satang = money[1];
      }
      else
      {
        baht = amount;
        satang = "";
      }

      if (baht.length() > 0)
      {
        result.append(changeNum(baht));
        result.append("");
      }

      if (satang.length() > 0)
      {
        result.append("จุด");
        result.append(changeNum(satang));
        result.append("");
      }
      else
      {
        result.append("");
      }

      return result.toString();
    }
    catch (Exception e)
    {
      return "จำนวนไม่ถูกต้อง";
    }
  }

  private static String changeNum(String num)
  {
    int max;
    String r, n;
    num = num.trim();
    max = num.length();

    StringBuffer result = new StringBuffer();
    for (int i = 0; i < max; i++)
    {
      r = chooseUnit((max - i) % 6);
      n = chooseNum(Integer.parseInt(num.substring(i, i + 1)));

      if (r.equals("สิบ") && n.equals("หนึ่ง"))
      {
        n = "";
      }
      if (n.equals("หนึ่ง") && r.equals("") && max != 1)
      {
        n = "เอ็ด";
      }
      if (i == 0 && n.equals("เอ็ด") && max > 1)
      {
        n = "หนึ่ง";
      }
      if (r.equals("สิบ") && n.equals("สอง"))
      {
        n = "ยี่";
      }
      if (r.equals("") && (max - i) > 6)
      {
        r = "ล้าน";
      }

      if (!n.equals("ศูนย์"))
      {
        result.append(n);
        result.append(r);
      }
      else
      {
        if (r.equals("ล้าน"))
        {
          result.append(r);
        }
      }
    }
    if (result.length() == 0)
    {
      return "ศูนย์";
    }
    return result.toString();
  }

  private static String chooseUnit(int indx)
  {
    String[] unit = { "แสน", "", "สิบ", "ร้อย", "พัน", "หมื่น" };
    return unit[indx];
  }

  private static String chooseNum(int indx)
  {
    String[] num = { "ศูนย์", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า" };
    return num[indx];
  }

  public static String convertText2Numthai(String str)
  {	  
	  try 
	  {
	      
	      for(int index = -1; ++index < str.length(); )
	      {
	    	switch (str.charAt(index)) {
				case '1': str =   str.replace('1','๑');
					break;
				case '2': str =   str.replace('2','๒');
					break;
				case '3': str =   str.replace('3','๓');
					break;
				case '4': str =   str.replace('4','๔');
					break;
				case '5': str =   str.replace('5','๕');
					break;
				case '6': str =   str.replace('6','๖');
					break;
				case '7': str =   str.replace('7','๗');
				break;
				case '8': str =   str.replace('8','๘');
				break;
				case '9': str =   str.replace('9','๙');
				break;
				case '0': str =   str.replace('0','๐');
				break;
	    	}
	      } 

		  
		return str.toString();
	} catch (Exception e) {
		
		return str.toString();
		
	}
  }
  
  public static void main(String args[])
  {
    System.out.println(getBahtText("212112.89"));
    System.out.println(convertText2Numthai("1234/df"));
  }
}
