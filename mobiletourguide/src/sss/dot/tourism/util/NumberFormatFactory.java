package sss.dot.tourism.util;



import static java.lang.Integer.MAX_VALUE;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.FieldPosition;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.ParsePosition;

public class NumberFormatFactory 
{
  private static ThaiCharNumberFormat charNumberFormat = new ThaiCharNumberFormat();
  
  public NumberFormatFactory() {
  }
  
  public static NumberFormat getDecimalNumberFormat()  {
    DecimalFormat result = new DecimalFormat("###,##0.##");
    result.setMaximumFractionDigits(MAX_VALUE);    
    return result;
  }
  
  public static NumberFormat getDecimalNumberFormatTH()  {
    DecimalFormat result = new ThaiDecimalFormat("###,##0.##");
    result.setMaximumFractionDigits(MAX_VALUE);    
    return result;
  }
  
  public static NumberFormat getIntegerNumberFormat()  {
    return new DecimalFormat("###,##0");
  }
  
  public static NumberFormat getIntegerNumberFormatTH()  {
    return new ThaiDecimalFormat("###,##0");
  }
  
  public static NumberFormat getCurrencyFormat() {
    return new DecimalFormat("###,##0.00");
  }

  public static NumberFormat getCurrencyFormatCondo() {
    return new DecimalFormat("###,##0.00000");
  }

  public static NumberFormat getCurrencyFormatTHCondo()  {
    return new ThaiDecimalFormat("###,##0.00000");
  }
  
  public static NumberFormat getShortCurrencyFormat()  {
    return new DecimalFormat("###,##0.0");
  }
  public static NumberFormat getCurrencyFormatTH()  {
    return new ThaiDecimalFormat("###,##0.00");
  }

  
  public static NumberFormat getIdNumberFormat() {
    return new DecimalFormat("#####");
  }
  
  public static NumberFormat getIdNumberFormatTH() {
    return new ThaiDecimalFormat("#####");
  }
  
  private static class ThaiCharNumberFormat {
    private char[] arabicNumber;
    private char[] thaiNumber;
    
    public ThaiCharNumberFormat()
    {
      arabicNumber = new char[10];
      arabicNumber[0] = '0';
      arabicNumber[1] = '1';
      arabicNumber[2] = '2';
      arabicNumber[3] = '3';
      arabicNumber[4] = '4';
      arabicNumber[5] = '5';
      arabicNumber[6] = '6';
      arabicNumber[7] = '7';
      arabicNumber[8] = '8';
      arabicNumber[9] = '9';
      
      thaiNumber = new char[10];
      thaiNumber[0] = '\u0e50';
      thaiNumber[1] = '\u0e51';
      thaiNumber[2] = '\u0e52';
      thaiNumber[3] = '\u0e53';
      thaiNumber[4] = '\u0e54';
      thaiNumber[5] = '\u0e55';
      thaiNumber[6] = '\u0e56';
      thaiNumber[7] = '\u0e57';
      thaiNumber[8] = '\u0e58';
      thaiNumber[9] = '\u0e59';
    }
    
    private static char getTargetNumber(char[] currentCharset, char[] targetCharset, char currentChar)
    {
      int numIndex = -1;
      
      boolean found = false;
      while((!found) && (++numIndex < currentCharset.length))
      {
        if(currentCharset[numIndex] == currentChar)
        {
          found = true;
        }
      } //while((!found) && (++numIndex < arabicNumber.length))
      
      return (found) ? targetCharset[numIndex] : currentChar;
    }
    
    public char getThaiNumber(char arabic)
    {
      return getTargetNumber(this.arabicNumber, this.thaiNumber, arabic);
    }
    
    public char getArabicNumber(char thai)
    {
      return getTargetNumber(this.thaiNumber, this.arabicNumber, thai);
    }
  }
  
  private static class ThaiDecimalFormat extends DecimalFormat
  {
    public ThaiDecimalFormat()
    {
      super();
    }
    
    public ThaiDecimalFormat(String pattern)
    {
      super(pattern);
    }
    
    public ThaiDecimalFormat(String pattern, DecimalFormatSymbols symbols)
    {
      super(pattern, symbols);
    }
    
    private StringBuffer filterResult(StringBuffer result, StringBuffer resultBuffer)
    {
      for(int index = -1; ++index < resultBuffer.length(); )
      {
        result.append(charNumberFormat.getThaiNumber(resultBuffer.charAt(index)));
      } //for(int index = -1; ++index < resultBuffer.length(); )
      
      return result;
    }
    
    @Override
	public StringBuffer format(double number, StringBuffer result, FieldPosition fieldPosition) 
    {
      return filterResult(result, super.format(number, new StringBuffer(), fieldPosition));
    }
    
    @Override
	public StringBuffer format(long number, StringBuffer result, FieldPosition fieldPosition) 
    {
      return filterResult(result, super.format(number, new StringBuffer(), fieldPosition));
    }
    
    @Override
	public Number parse(String text, ParsePosition pos)
    {
      StringBuffer textBuffer = new StringBuffer(text.substring(0, pos.getIndex()));
      for(int index = pos.getIndex() - 1; ++index < text.length(); )
      {
        textBuffer.append(charNumberFormat.getArabicNumber(text.charAt(index)));
      } //for(int index = -1; ++index < text.length(); )
      
      return super.parse(textBuffer.toString(), pos);
    }
  }
  
  public static void main(String[] args) 
  {

    try
    {
     // System.out.println(xxNum);
      
      NumberFormat format = NumberFormatFactory.getIdNumberFormatTH();
      StringBuffer n = format.format(456, new StringBuffer(), new FieldPosition(NumberFormat.INTEGER_FIELD));
      
      System.out.println(n.toString());
    }
    catch(Exception pe)
    {
      pe.printStackTrace();
    }
  }
}
