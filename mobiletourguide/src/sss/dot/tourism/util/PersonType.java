package sss.dot.tourism.util;

public enum PersonType {
	INDIVIDUAL("I","บุคคลธรรมดา"),
	CORPORATE("C","นิติบุคคล");
	

	private final String status;
	private final String meaning;

	PersonType(String status,String meaning){
		this.status = status;
		this.meaning = meaning;
	}

	/**
	 * @return the status
	 */
	public String getStatusString() {
		return status.toString();
	}
	public String getStatus() {
		return status;
	}
	/**
	 * @return the meaning
	 */
	public String getMeaning() {
		return meaning;
	}
	/**
	 * overloading
	 * @param flag
	 * @return
	 */
	public boolean equals(String status){
		PersonType eStatus =  getPersonType(status);
		return super.equals(eStatus);
	}
	public static PersonType getPersonType(String status) {
		PersonType record = null;

		
		if(INDIVIDUAL.getStatus().equals(status)){
			record = INDIVIDUAL;
		}
		else if(CORPORATE.getStatus().equals(status)){
			record = CORPORATE;
		}
		
		return record;
	}
	public static String getMeaning(String status) {
		return getPersonType(status).getMeaning();
	}
}
