package sss.dot.tourism.util;

/*
 * ประเภทการขึ้นทะเบียนใบอนุญาต
N=จดทะเบียนใหม่
R=ชำระค่าธรรมเนียมราย 2 ปี /ต่ออายุ
B=เพิ่มข้อมูลสาขา
C=เปลี่ยนแปลงรายการ
T=Temporary ออกใบแทน*/
public enum RegistrationType {
	TOUR_COMPANIES_NEW_REGISTRATION("B" ,"N","จดทะเบียนใบอนุญาตใหม่",true, "1"),
	TOUR_COMPANIES_NEW_REGISTRATION_TRAN("B" ,"N1","จดทะเบียนใบอนุญาตใหม่",true, "9"),
	TOUR_COMPANIES_RENEW_REGISTRATION("B","R","ชำระค่าธรรมเนียมราย 2 ปี",true, "2"),
	TOUR_COMPANIES_ADD_NEW_BRANCH("B","B","เพิ่มข้อมูลสาขา",false, "3"),
	TOUR_COMPANIES_MINOR_CHANGE("B","C","เปลี่ยนแปลงรายการ",false, "4"),
	TOUR_COMPANIES_TEMPORARY_LICENSE("B","T","ออกใบแทน",true ,"5"),
	TOUR_COMPANIES_CERTIFICATE("B","G","หนังสือรับรองใบอนุญาต",false ,"5"),
	
	TOUR_TOURLEADER_NEW_REGISTRATION("L" ,"N","จดทะเบียนใบอนุญาตใหม่",false ,"1"),
	TOUR_TOURLEADER_NEW_REGISTRATION_TRAN("L" ,"N1","จดทะเบียนใบอนุญาตใหม่",false, "9" ),
	TOUR_TOURLEADER_MINOR_CHANGE("L","C","เปลี่ยนแปลงรายการ",false ,"4"),
	TOUR_TOURLEADER_TEMPORARY_LICENSE("L","T","ออกใบแทน",false, "5"),
	REQ_TOUR_LEADER_PASSPORT("L","P","การขอหนังสือเดินทางเล่มสอง",false, "5"),
	
	GUIDE_NEW_REGISTRATION("G" ,"N","จดทะเบียนใบอนุญาตใหม่",true, "1"),
	GUIDE_NEW_REGISTRATION_TRAN("G" ,"N1","จดทะเบียนใบอนุญาตใหม่",true ,"9"),
	GUIDE_RENEW_REGISTRATION("G" ,"R","ต่ออายุใบอนุญาต",true, "2"),
	GUIDE_MINOR_CHANGE("G","C","เปลี่ยนแปลงรายการ",false, "4"),
	GUIDE_TEMPORARY_LICENSE("G","T","ออกใบแทน",true ,"5"),
	GUIDE_DAMAGE_CARD("G","D","ขอบัตรใหม่ (บัตรชำรุด สูญหาย หรือถูกทำลาย)",false ,"6");
	
	private final String traderType;
	private final String status;
	private final String meaning;
	private final boolean payFee;
	private final String regCode;

	RegistrationType(String traderType ,String status,String meaning, boolean payFee, String regCode){
		this.traderType = traderType;
		this.status = status;
		this.meaning = meaning;
		this.payFee = payFee;
		this.regCode = regCode;
	}

	/**
	 * @return the status
	 */
	public String getStatusString() {
		return status.toString();
	}
	public String getStatus() {
		return status;
	}
	
	public String getRegCode() {
		return regCode;
	}
	/**
	 * @return the meaning
	 */
	public String getMeaning() {
		return meaning;
	}
	
	public boolean getPayFee() {
		return payFee;
	}
	/**
	 * overloading
	 * @param flag
	 * @return
	 */
	public boolean equals(String traderType ,String status){
		RegistrationType eStatus =  getRegistrationType(traderType, status);
		return super.equals(eStatus);
	}
	public static RegistrationType getRegistrationType(String traderType ,String status) {
		RegistrationType record = null;
		
		if(traderType.equals("B"))
		{
			if(TOUR_COMPANIES_NEW_REGISTRATION.getStatus().equals(status)){
				record = TOUR_COMPANIES_NEW_REGISTRATION;
			}
			else if(TOUR_COMPANIES_RENEW_REGISTRATION.getStatus().equals(status)){
				record = TOUR_COMPANIES_RENEW_REGISTRATION;
			}
			else if(TOUR_COMPANIES_ADD_NEW_BRANCH.getStatus().equals(status)){
				record = TOUR_COMPANIES_ADD_NEW_BRANCH;
			}
			else if(TOUR_COMPANIES_MINOR_CHANGE.getStatus().equals(status)){
				record = TOUR_COMPANIES_MINOR_CHANGE;
			}
			else if(TOUR_COMPANIES_TEMPORARY_LICENSE.getStatus().equals(status)){
				record = TOUR_COMPANIES_TEMPORARY_LICENSE;
			}
			else if(TOUR_COMPANIES_NEW_REGISTRATION_TRAN.getStatus().equals(status)){
				record = TOUR_COMPANIES_NEW_REGISTRATION_TRAN;
			}
			else if(TOUR_COMPANIES_CERTIFICATE.getStatus().equals(status)){
				record = TOUR_COMPANIES_CERTIFICATE;
			}
		}
		
		if(traderType.equals("G"))
		{			
			if(GUIDE_NEW_REGISTRATION.getStatus().equals(status)){
				record = GUIDE_NEW_REGISTRATION;
			}
			else if(GUIDE_RENEW_REGISTRATION.getStatus().equals(status)){
				record = GUIDE_RENEW_REGISTRATION;
			}
			else if(GUIDE_MINOR_CHANGE.getStatus().equals(status)){
				record = GUIDE_MINOR_CHANGE;
			}
			else if(GUIDE_TEMPORARY_LICENSE.getStatus().equals(status)){
				record = GUIDE_TEMPORARY_LICENSE;
			}
			else if(GUIDE_NEW_REGISTRATION_TRAN.getStatus().equals(status)){
				record = GUIDE_NEW_REGISTRATION_TRAN;
			}
			else if(GUIDE_NEW_REGISTRATION_TRAN.getStatus().equals(status)){
				record = GUIDE_NEW_REGISTRATION_TRAN;
			}else if(GUIDE_DAMAGE_CARD.getStatus().equals(status)){
				record = GUIDE_DAMAGE_CARD;
			}
			
			
		}
			
		if(traderType.equals("L"))
		{
			if(TOUR_TOURLEADER_NEW_REGISTRATION.getStatus().equals(status)){
				record = TOUR_TOURLEADER_NEW_REGISTRATION;
			}
			else if(TOUR_TOURLEADER_MINOR_CHANGE.getStatus().equals(status)){
				record = TOUR_TOURLEADER_MINOR_CHANGE;
			}
			else if(TOUR_TOURLEADER_TEMPORARY_LICENSE.getStatus().equals(status)){
				record = TOUR_TOURLEADER_TEMPORARY_LICENSE;
			}
			else if(REQ_TOUR_LEADER_PASSPORT.getStatus().equals(status)){
				record = REQ_TOUR_LEADER_PASSPORT;
			}
		}
		return record;
	}
	
	
	public static RegistrationType getRegistrationTypeByCode(String traderType ,String regCode) {
		RegistrationType record = null;
		
		if(traderType.equals("B"))
		{
			if(TOUR_COMPANIES_NEW_REGISTRATION.getRegCode().equals(regCode)){
				record = TOUR_COMPANIES_NEW_REGISTRATION;
			}
			else if(TOUR_COMPANIES_RENEW_REGISTRATION.getRegCode().equals(regCode)){
				record = TOUR_COMPANIES_RENEW_REGISTRATION;
			}
			else if(TOUR_COMPANIES_ADD_NEW_BRANCH.getRegCode().equals(regCode)){
				record = TOUR_COMPANIES_ADD_NEW_BRANCH;
			}
			else if(TOUR_COMPANIES_MINOR_CHANGE.getRegCode().equals(regCode)){
				record = TOUR_COMPANIES_MINOR_CHANGE;
			}
			else if(TOUR_COMPANIES_TEMPORARY_LICENSE.getRegCode().equals(regCode)){
				record = TOUR_COMPANIES_TEMPORARY_LICENSE;
			}
			else if(TOUR_COMPANIES_NEW_REGISTRATION_TRAN.getRegCode().equals(regCode)){
				record = TOUR_COMPANIES_NEW_REGISTRATION_TRAN;
			}
		}
		
		if(traderType.equals("G"))
		{			
			if(GUIDE_NEW_REGISTRATION.getRegCode().equals(regCode)){
				record = GUIDE_NEW_REGISTRATION;
			}
			else if(GUIDE_RENEW_REGISTRATION.getRegCode().equals(regCode)){
				record = GUIDE_RENEW_REGISTRATION;
			}
			else if(GUIDE_MINOR_CHANGE.getRegCode().equals(regCode)){
				record = GUIDE_MINOR_CHANGE;
			}
			else if(GUIDE_TEMPORARY_LICENSE.getRegCode().equals(regCode)){
				record = GUIDE_TEMPORARY_LICENSE;
			}
			else if(GUIDE_NEW_REGISTRATION_TRAN.getRegCode().equals(regCode)){
				record = GUIDE_NEW_REGISTRATION_TRAN;
			}
			else if(GUIDE_DAMAGE_CARD.getRegCode().equals(regCode)){
				record = GUIDE_DAMAGE_CARD;
			}
			
			
		}
			
		if(traderType.equals("L"))
		{
			if(TOUR_TOURLEADER_NEW_REGISTRATION.getRegCode().equals(regCode)){
				record = TOUR_TOURLEADER_NEW_REGISTRATION;
			}
			else if(TOUR_TOURLEADER_MINOR_CHANGE.getRegCode().equals(regCode)){
				record = TOUR_TOURLEADER_MINOR_CHANGE;
			}
			else if(TOUR_TOURLEADER_TEMPORARY_LICENSE.getRegCode().equals(regCode)){
				record = TOUR_TOURLEADER_TEMPORARY_LICENSE;
			}
		}
		return record;
	}
	
	public static String getMeaning(String traderType , String status) {
		return getRegistrationType(traderType,status).getMeaning();
	}
	
	public static boolean isPayFee(String traderType , String status) {
		return getRegistrationType(traderType,status).getPayFee();
	}
	
	public static String getRegCode(String traderType , String status) {
		return getRegistrationType(traderType,status).getRegCode();
	}
	
	public static String getRegTypeByCode(String traderType , String regCode) {
		return getRegistrationTypeByCode(traderType,regCode).getStatus();
	}
}
