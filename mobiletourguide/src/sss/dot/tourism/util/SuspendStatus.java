package sss.dot.tourism.util;

/*
 สถานะการพักใช้
S=Suspend พักใช้ใบอนุญาต
C=Cancel ยกเลิกการพักใช้
W=wail อยู่ระหว่างดำเนินการพักใช้
N=ครบกำหนดการพักใช้
 **/

public enum SuspendStatus {
	WAIT("W" , "อยู่ระหว่างดำเนินการพักใช้"),
	APPROVE("A" , "อนุมัติให้พักใช้"),
	CANCEL("C" , "ยกเลิกการพักใช้"),
	SUSPENSION("S" , "พักใช้ใบอนุญาต"),
	NORMAL("N","ครบกำหนดการพักใช้");
	
	private final String status;
	private final String meaning;
	
	SuspendStatus(String status, String meaning){
		this.status = status;
		this.meaning = meaning;
	}
	
	/**
	 * @return the category
	 */
	public String getStatusString() {
		return status.toString();
	}
	public String getStatus() {
		return status;
	}
	/**
	 * @return the meaning
	 */
	public String getMeaning() {
		return meaning;
	}
	/**
	 * overloading
	 * @param flag
	 * @return
	 */
	public boolean equals(String status){
		SuspendStatus eStatus =  getSuspendStatus(status);
		return super.equals(eStatus);
	}
	public static SuspendStatus getSuspendStatus(String status) {
		SuspendStatus record = null;

		if(NORMAL.getStatus().equals(status)){
			record = NORMAL;
		}
		else if(CANCEL.getStatus().equals(status)){
			record = CANCEL;
		}
		else if(SUSPENSION.getStatus().equals(status)){
			record = SUSPENSION;
		}
		else if(WAIT.getStatus().equals(status)){
			record = WAIT;
		}
		else if(APPROVE.getStatus().equals(status)){
			record = APPROVE;
		}
		
		
		return record;
	}
	public static String getMeaning(String status) {
		return getSuspendStatus(status ).getMeaning();
	}
}
