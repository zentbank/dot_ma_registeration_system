package sss.dot.tourism.util;



/**
 * @description@
 * @author eark
 * @see
 * 
 */
public class ThaiBahtUtil
{

  /**
   * @param amount
   * @return
   */
  public static String getBahtText(String amount)
  {
    try
    {
      StringBuffer result = new StringBuffer();
      String satang, baht;

      if (amount.startsWith("-"))
      {
        amount = amount.substring(1);
        result.append("\u0e25\u0e1a");
      }
      if (amount.endsWith("."))
      {
        amount = amount.substring(0, amount.length() - 1);
      }

      if (amount.indexOf('.') > -1)
      {
        String money[] = amount.split("\\.");
        baht = money[0];
        satang = money[1];
      }
      else
      {
        baht = amount;
        satang = "";
      }

      if (baht.length() > 0)
      {
        result.append(changeNum(baht));
        result.append("\u0e1a\u0e32\u0e17");
      }

      if (satang.length() > 0)
      {
    	int sat =   Integer.parseInt(satang);
    	  if(sat > 0)
    	  {
    		  result.append(changeNum(satang));
    	      result.append("\u0e2a\u0e15\u0e32\u0e07\u0e04\u0e4c");
    	  }
    	  else
    	  {
    		  result.append("\u0e16\u0e49\u0e27\u0e19");
    	  }
        
      }
      else
      {
        result.append("\u0e16\u0e49\u0e27\u0e19");
      }

      return result.toString();
    }
    catch (Exception e)
    {
      return "\u0e08\u0e33\u0e19\u0e27\u0e19\u0e40\u0e07\u0e34\u0e19\u0e44\u0e21\u0e48\u0e16\u0e39\u0e01\u0e15\u0e49\u0e2d\u0e07";
    }
  }

  private static String changeNum(String num)
  {
    int max;
    String r, n;
    num = num.trim();
    max = num.length();

    StringBuffer result = new StringBuffer();
    for (int i = 0; i < max; i++)
    {
      r = chooseUnit((max - i) % 6);
      n = chooseNum(Integer.parseInt(num.substring(i, i + 1)));

      if (r.equals("\u0e2a\u0e34\u0e1a") && n.equals("\u0e2b\u0e19\u0e36\u0e48\u0e07"))
      {
        n = "";
      }
      if (n.equals("\u0e2b\u0e19\u0e36\u0e48\u0e07") && r.equals("") && max != 1)
      {
        n = "\u0e40\u0e2d\u0e47\u0e14";
      }
      if (i == 0 && n.equals("\u0e40\u0e2d\u0e47\u0e14") && max > 1)
      {
        n = "\u0e2b\u0e19\u0e36\u0e48\u0e07";
      }
      if (r.equals("\u0e2a\u0e34\u0e1a") && n.equals("\u0e2a\u0e2d\u0e07"))
      {
        n = "\u0e22\u0e35\u0e48";
      }
      if (r.equals("") && (max - i) > 6)
      {
        r = "\u0e25\u0e49\u0e32\u0e19";
      }

      if (!n.equals("\u0e28\u0e39\u0e19\u0e22\u0e4c"))
      {
        result.append(n);
        result.append(r);
      }
      else
      {
        if (r.equals("\u0e25\u0e49\u0e32\u0e19"))
        {
          result.append(r);
        }
      }
    }
    if (result.length() == 0)
    {
      return "\u0e28\u0e39\u0e19\u0e22\u0e4c";
    }
    return result.toString();
  }

  private static String chooseUnit(int indx)
  {
    
    String[] unit = { "\u0e41\u0e2a\u0e19", "", "\u0e2a\u0e34\u0e1a", "\u0e23\u0e49\u0e2d\u0e22", "\u0e1e\u0e31\u0e19", "\u0e2b\u0e21\u0e37\u0e48\u0e19" };
    return unit[indx];
  }

  private static String chooseNum(int indx)
  {

    String[] num = { "\u0e28\u0e39\u0e19\u0e22\u0e4c", "\u0e2b\u0e19\u0e36\u0e48\u0e07", "\u0e2a\u0e2d\u0e07", "\u0e2a\u0e32\u0e21", "\u0e2a\u0e35\u0e48", "\u0e2b\u0e49\u0e32", "\u0e2b\u0e01", "\u0e40\u0e08\u0e47\u0e14", "\u0e41\u0e1b\u0e14", "\u0e40\u0e01\u0e49\u0e32" };
    return num[indx];
  }

  public static String convertText2Numthai(String str)
  {	  
	  try 
	  {
	      
	      for(int index = -1; ++index < str.length(); )
	      {
	    	switch (str.charAt(index)) {
				case '1': str =   str.replace('1','๑');
					break;
				case '2': str =   str.replace('2','๒');
					break;
				case '3': str =   str.replace('3','๓');
					break;
				case '4': str =   str.replace('4','๔');
					break;
				case '5': str =   str.replace('5','๕');
					break;
				case '6': str =   str.replace('6','๖');
					break;
				case '7': str =   str.replace('7','๗');
				break;
				case '8': str =   str.replace('8','๘');
				break;
				case '9': str =   str.replace('9','๙');
				break;
				case '0': str =   str.replace('0','๐');
				break;
	    	}
	      } 

		  
		return str.toString();
	} catch (Exception e) {
		
		return str.toString();
		
	}
  }
  
  public static void main(String args[])
  {
    System.out.println(getBahtText("212112.89"));
    System.out.println(convertText2Numthai("1234/df"));
  }
  
  /*
   * package com.prestigesoft.util;

public class ThaiBahtUtil {
	
	
	public static String getBahtText(String amount){
		try{
			StringBuffer result = new StringBuffer();
			String satang, baht;
			
			if(amount.startsWith("-")){
				amount = amount.substring(1);
				result.append("ลบ");
			}
			if(amount.endsWith(".")){
				amount = amount.substring(0, amount.length()-1);
			}
			
			if(amount.indexOf('.') > -1){
				String money[] = amount.split("\\.");
				baht = money[0];
				satang = money[1];
			}
			else{
				baht = amount;
				satang = "";
			}
			
			if(baht.length() > 0){
				result.append(changeNum(baht));
				result.append("บาท");
			}
			
			if(satang.length() > 0){
				result.append(changeNum(satang));
				result.append("สตางค์");
			}
			else{
				result.append("ถ้วน");
			}
			
			return result.toString();
		}
		catch(Exception e){
			return "จำนวนเงินไม่ถูกต้อง";
		}
	}
	
	private static String  changeNum(String num){
		int max;
		String r, n;
		num = num.trim();
		max = num.length();
		
		StringBuffer result = new StringBuffer();
		for(int i=0; i<max; i++){
			r = chooseUnit((max - i) % 6);
			n = chooseNum(Integer.parseInt(num.substring(i, i+1)));
			
			if(r.equals("สิบ") && n.equals("หนึ่ง")){
				n = "";
			}
			if(n.equals("หนึ่ง") && r.equals("") && max != 1){
				n = "เอ็ด";
			}
			if(i == 0 && n.equals("เอ็ด") && max > 1){
				n = "หนึ่ง";
			}
			if(r.equals("สิบ") && n.equals("สอง")){
				n = "ยี่";
			}
			if(r.equals("") && (max - i) > 6){
				r = "ล้าน";
			}
			
			if(!n.equals("ศูนย์")){
				result.append(n);
				result.append(r);
			}
			else{
				if(r.equals("ล้าน")){
					result.append(r);
				}
			}
		}
		if(result.length() == 0){
			return "ศูนย์";
		}
		return result.toString();
	}
	
	private static String chooseUnit(int indx){
		String[] unit = { "แสน", "", "สิบ", "ร้อย", "พัน", "หมื่น" };
		return unit[indx];
	}
	
	private static String chooseNum(int indx){
		String[] num = { "ศูนย์", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า" };
		return num[indx];
	}
  
  public static void main(String args[]) {
  System.out.println(getBahtText("212112.89"));
  }
  
}
*/

}
