package sss.dot.tourism.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidatorUtil {
	
	  private static final String CARD_ID = "\\d{10,13}";
	  private static final String PHONE = "\\d{8,10}";
	  
		private static final String EMAIL_PATTERN = 
				"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
				+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		
	  private static Pattern numberCardIdPattern = Pattern.compile(CARD_ID), numberPhonePattern = Pattern.compile(PHONE), emailPattern = Pattern.compile(EMAIL_PATTERN);
	  private static Matcher matcher;



//	  public ValidatorUtil(){
//		  
//		  numberCardIdPattern = Pattern.compile(CARD_ID);
//		  numberPhonePattern = Pattern.compile(PHONE);
//		  emailPattern = Pattern.compile(EMAIL_PATTERN);
//	  }

	  public static boolean phone(final String str){
		 
		  matcher = numberPhonePattern.matcher(str);
		  return matcher.matches();
	  }
	  
	  public static boolean cardId(final String str){
		 
		  
		  matcher = numberCardIdPattern.matcher(str);
		  return matcher.matches();
	  }
	  
	  public static boolean email(final String str){
		 
		  
		  matcher = emailPattern.matcher(str);
		  return matcher.matches();
	  }
	  
	  
	  public static void main(String[] arg)
	  {

		  
		  System.out.println(ValidatorUtil.cardId("454"));
		  System.out.println(ValidatorUtil.phone("08134774"));
		  System.out.println(ValidatorUtil.email("sha@xx.cc"));
	  }
}
